﻿'************************************************************************************************************************************
'Class Name : clsCashDenomination.vb
'Purpose    :
'Date       :16/12/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsCashDenomination
    Private Const mstrModuleName = "clsCashDenomination"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintCashdenominationunkid As Integer = -1
    Private mintPeriodId As Integer = -1
    Private mintCashdenominationTranunkid As Integer = -1

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintYearUnkid As Integer = -1
    Private mintUserunkid As Integer = -1
    'Sohail (18 May 2013) -- End

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _CashDenominationId() As Integer
        Set(ByVal value As Integer)
            mintCashdenominationunkid = value
        End Set
    End Property

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _YearUnkid() As Integer
        Set(ByVal value As Integer)
            mintYearUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Userunkid() As Integer
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property
    'Sohail (18 May 2013) -- End

#End Region

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update  Database Table (prcashdenomenation_master) </purpose>
    Public Function Update(ByVal dtTable As DataTable, ByVal intYearUnkid As Integer, ByVal intUserunkid As Integer) As Boolean
        'Public Function Update(ByVal dtTable As DataTable) As Boolean 'Sohail (21 Aug 2015)
        Dim dsList As DataSet = Nothing
        Dim StrQM As String = ""
        Dim StrQT As String = ""
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "prcashdenomenation_master", "cashdenominationunkid", mintCashdenominationunkid, "prcashdenomenation_tran", "cashdenominationtranunkid", 2, 3) = False Then
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            StrQ = "DELETE FROM prcashdenomenation_tran WHERE cashdenominationunkid = " & mintCashdenominationunkid

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQM = "UPDATE prcashdenomenation_master SET " & _
                    "  employeeunkid = @employeeunkid" & _
                    ", exchagerateunkid = @exchagerateunkid" & _
                    ", currency_sign = @currency_sign" & _
                    ", amount = @amount" & _
                    ", isreceipt = @isreceipt" & _
                    ", yearunkid = @yearunkid" & _
                    ", userunkid = @userunkid" & _
                    ", isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voidreason = @voidreason" & _
                    ", voiddatetime = @voiddatetime " & _
                    ", countryunkid = @countryunkid " & _
                    "WHERE cashdenominationunkid = @cashdenominationunkid "
            'Sohail (03 Sep 2012)[countryunkid]

            StrQT = "INSERT INTO prcashdenomenation_tran ( " & _
                       "  cashdenominationunkid " & _
                       ", denominatonunkid " & _
                       ", denomination " & _
                       ", quantity " & _
                       ", total " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime" & _
                    ") VALUES (" & _
                       "  @cashdenominationunkid " & _
                       ", @denominatonunkid " & _
                       ", @denomination " & _
                       ", @quantity " & _
                       ", @total " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                    "); SELECT @@identity"




            For Each dtRow As DataRow In dtTable.Rows
                If CDec(dtRow.Item("total")) <= 0 Then Continue For 'Sohail (11 May 2011)

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@cashdenominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCashdenominationunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("employeeunkid").ToString)
                objDataOperation.AddParameter("@exchagerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("exchagerateunkid").ToString)
                objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtRow.Item("currency_sign").ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dtRow.Item("amount").ToString) 'Sohail (11 May 2011)
                objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid.ToString)
                'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                'Sohail (21 Aug 2015) -- End
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("countryunkid"))
                'Sohail (03 Sep 2012) -- End
                

                dsList = objDataOperation.ExecQuery(StrQM, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim objDenome As New clsDenomination
                Dim decTotal As Decimal = 0 'Sohail (11 May 2011)
                For i As Integer = 3 To dtRow.ItemArray.Length - 5 'Sohail (03 Sep 2012)[Original : 5], 'Sohail (18 May 2013) - [Original : 6]
                    Dim strColumn As String = dtRow.Table.Columns(i).ColumnName.ToString
                    If CInt(dtRow.Item(strColumn)) = 0 Then Continue For
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@cashdenominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCashdenominationunkid.ToString)
                    objDenome._Denomunkid = CInt(dtRow.Table.Columns(i).ColumnName.ToString.Replace("Column", "").Trim)
                    objDataOperation.AddParameter("@denominatonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objDenome._Denomunkid.ToString)
                    objDataOperation.AddParameter("@denomination", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, objDenome._Denomination.ToString) 'Sohail (11 May 2011)
                    objDataOperation.AddParameter("@quantity", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item(strColumn).ToString)
                    decTotal = objDenome._Denomination * dtRow.Item(strColumn)
                    objDataOperation.AddParameter("@total", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decTotal.ToString) 'Sohail (11 May 2011)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

                    'Sohail (12 Oct 2011) -- Start
                    'objDataOperation.ExecNonQuery(StrQT)
                    dsList = objDataOperation.ExecQuery(StrQT, "List")
                    'Sohail (12 Oct 2011) -- End

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (12 Oct 2011) -- Start
                    mintCashdenominationTranunkid = dsList.Tables(0).Rows(0)(0)
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prcashdenomenation_master", "cashdenominationunkid", mintCashdenominationunkid, "prcashdenomenation_tran", "cashdenominationtranunkid", mintCashdenominationTranunkid, 2, 1) = False Then
                        Return False
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'DisplayError.Show("-1", ex.Message, "Insert", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            'Sohail (18 May 2013) -- End
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prcashdenomenation_master) </purpose>
    Public Function Insert(ByVal intEmployeeId As Integer, ByVal intPaymentId As Integer, ByVal dtRow As DataRow, ByVal intYearUnkid As Integer, ByVal intUserunkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'Sohail (21 Aug 2015)
        'Public Function Insert(ByVal intEmployeeId As Integer, ByVal intPaymentId As Integer, ByVal dtRow As DataRow, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END - 'Sohail (21 Aug 2015)
        'Public Function Insert(ByVal intEmployeeId As Integer, ByVal intPaymentId As Integer, ByVal dtRow As DataRow) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQM As String = ""
        Dim StrQT As String = ""
        Dim exForce As Exception
        Try

            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOpr
                objDataOperation.ClearParameters()
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END


            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mintYearUnkid <= 0 Then mintYearUnkid = FinancialYear._Object._YearUnkid
            'If mintUserunkid <= 0 Then mintUserunkid = User._Object._Userunkid
            mintYearUnkid = intYearUnkid
            mintUserunkid = intUserunkid
            'Sohail (21 Aug 2015) -- End
            'Sohail (18 May 2013) -- End

            StrQM = "INSERT INTO prcashdenomenation_master ( " & _
                       "  paymenttranunkid " & _
                       ", employeeunkid " & _
                       ", exchagerateunkid " & _
                       ", currency_sign " & _
                       ", amount " & _
                       ", isreceipt " & _
                       ", yearunkid " & _
                       ", periodunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime" & _
                       ", countryunkid " & _
                     ") VALUES (" & _
                       "  @paymenttranunkid " & _
                       ", @employeeunkid " & _
                       ", @exchagerateunkid " & _
                       ", @currency_sign " & _
                       ", @amount " & _
                       ", @isreceipt " & _
                       ", @yearunkid " & _
                       ", @periodunkid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                       ", @countryunkid " & _
                     "); SELECT @@identity"
            'Sohail (03 Sep 2012)[countryunkid]

            StrQT = "INSERT INTO prcashdenomenation_tran ( " & _
                       "  cashdenominationunkid " & _
                       ", denominatonunkid " & _
                       ", denomination " & _
                       ", quantity " & _
                       ", total " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime" & _
                    ") VALUES (" & _
                       "  @cashdenominationunkid " & _
                       ", @denominatonunkid " & _
                       ", @denomination " & _
                       ", @quantity " & _
                       ", @total " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                    "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            objDataOperation.AddParameter("@exchagerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("exchagerateunkid").ToString)
            objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtRow("currency_sign").ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dtRow("amount").ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@isreceipt", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearUnkid.ToString)
            'Sohail (18 May 2013) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (18 May 2013) -- End
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("countryunkid"))
            'Sohail (03 Sep 2012) -- End

            dsList = objDataOperation.ExecQuery(StrQM, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCashdenominationunkid = dsList.Tables(0).Rows(0).Item(0)
            Dim objDenome As New clsDenomination
            Dim decTotal As Decimal = 0 'Sohail (11 May 2011)

            For i As Integer = 3 To dtRow.ItemArray.Length - 5 'Sohail (03 Sep 2012)[Original : 5], 'Sohail (18 May 2013) - [Original : 6]
                Dim strColumn As String = dtRow.Table.Columns(i).ColumnName.ToString
                If CInt(dtRow(strColumn)) = 0 Then Continue For
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@cashdenominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCashdenominationunkid.ToString)
                objDenome._Denomunkid = CInt(dtRow.Table.Columns(i).ColumnName.ToString.Replace("Column", "").Trim)
                objDataOperation.AddParameter("@denominatonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objDenome._Denomunkid.ToString)
                objDataOperation.AddParameter("@denomination", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, objDenome._Denomination.ToString) 'Sohail (11 May 2011)
                objDataOperation.AddParameter("@quantity", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item(strColumn).ToString)
                decTotal = objDenome._Denomination * dtRow.Item(strColumn)
                objDataOperation.AddParameter("@total", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decTotal.ToString) 'Sohail (11 May 2011)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

                'Sohail (12 Oct 2011) -- Start
                'objDataOperation.ExecNonQuery(StrQT)
                dsList = objDataOperation.ExecQuery(StrQT, "List")
                'Sohail (12 Oct 2011) -- End

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (12 Oct 2011) -- Start
                mintCashdenominationTranunkid = dsList.Tables(0).Rows(0)(0)
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "prcashdenomenation_master", "cashdenominationunkid", mintCashdenominationunkid, "prcashdenomenation_tran", "cashdenominationtranunkid", mintCashdenominationTranunkid, 1, 1, , mintUserunkid) = False Then
                    Return False
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            Next
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END
            Return True
        Catch ex As Exception

            'Nilay (28-Aug-2015) -- Start
            'In order to avoid Parallel Transaction problem in Web
            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            'Nilay (28-Aug-2015) -- End

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'DisplayError.Show("-1", ex.Message, "Insert", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            'Sohail (18 May 2013) -- End
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Data </purpose>
    Public Function VoidCashDenomination(ByVal intUnkid As Integer, _
                                         ByVal blnIsvvoid As Boolean, _
                                         ByVal intVoidUserId As Integer, _
                                         ByVal dtVoidDate As DateTime, _
                                         ByVal strVoidReason As String, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        'Public Function VoidCashDenomination(ByVal intUnkid As Integer, _
        '                                     ByVal blnIsvvoid As Boolean, _
        '                                     ByVal intVoidUserId As Integer, _
        '                                     ByVal dtVoidDate As DateTime, _
        '                                     ByVal strVoidReason As String) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intCashDenomeId As Integer = -1
        Try

            'S.SANDEEP [ 04 DEC 2013 ] -- START
            If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOpr
                objDataOperation.ClearParameters()
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END

            StrQ = "SELECT cashdenominationunkid AS Id FROM prcashdenomenation_master WHERE paymenttranunkid = @PayTranId AND ISNULL(isvoid,0) = 0 "

            objDataOperation.AddParameter("@PayTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intCashDenomeId = dsList.Tables("List").Rows(0)("Id")
            Else
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'objDataOperation.ReleaseTransaction(True)
                If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                End If
                'Sohail (07 May 2015) -- End
                Return True
            End If

            StrQ = "UPDATE prcashdenomenation_master SET " & _
                    "  isvoid = @isvoid " & _
                    ", voiddatetime = @VoidDate " & _
                    ", voidreason = @VoidReason " & _
                    ", voiduserunkid = @Voiduser " & _
                    "WHERE paymenttranunkid = @PayTranId; "

            StrQ &= "UPDATE prcashdenomenation_tran SET " & _
                    "  isvoid = @isvoid " & _
                    ", voiddatetime = @VoidDate " & _
                    ", voidreason = @VoidReason " & _
                    ", voiduserunkid = @Voiduser " & _
                    "WHERE cashdenominationunkid = @CashDenomId "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsvvoid)
            objDataOperation.AddParameter("@Voiduser", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@VoidDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@VoidReason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidReason)
            objDataOperation.AddParameter("@CashDenomId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCashDenomeId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "prcashdenomenation_master", "cashdenominationunkid", intCashDenomeId, "prcashdenomenation_tran", "cashdenominationtranunkid", 3, 3, , , , intVoidUserId) = False Then
                objDataOperation.ReleaseTransaction(False) 'Sohail (07 May 2015)
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'DisplayError.Show("-1", ex.Message, "VoidCashDenomination", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: VoidCashDenomination; Module Name: " & mstrModuleName)
            'Sohail (18 May 2013) -- End
            Return False
        End Try
    End Function

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetList(Optional ByVal intUnkid As Integer = -1, Optional ByVal strList As String = "List") As DataTable
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strList As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            , Optional ByVal intUnkid As Integer = -1 _
                            ) As DataTable
        'Sohail (21 Aug 2015) -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As New DataTable
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    "   ,ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                    "	,ISNULL(prcashdenomenation_master.amount,0) AS amount " & _
                    "	,ISNULL(prcashdenomenation_tran.quantity,0) AS qualtity " & _
                    "	,hremployee_master.employeeunkid " & _
                    "	,prcashdenomenation_master.exchagerateunkid " & _
                    "	,prcashdenomenation_master.currency_sign " & _
                    "	,prcashdenomenation_tran.cashdenominationtranunkid " & _
                    "	,prcashdenomenation_master.cashdenominationunkid " & _
                    "	,prcashdenomenation_master.periodunkid " & _
                    "	,ISNULL(cfcommon_period_tran.period_name,'') AS pname " & _
                    "   ,CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                    "   ,CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                    "   ,prcashdenomenation_tran.denominatonunkid " 'Sohail (28 Jan 2012) - [employeecode]
            StrQ &= "   ,ISNULL(prcashdenomenation_master.countryunkid,0) AS countryunkid " 'Sohail (03 Sep 2012)
            'Sohail (21 Aug 2015) - [start_date, end_date]
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            StrQ &= " ,ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
                     ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
                     ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
                     ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
                     ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
                     ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
                     ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
                     ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
                     ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
                     ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
                     ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
                     ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
                     ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
                     ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
                     ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid "
            'Anjan (21 Nov 2011)-End 

            StrQ &= "FROM prcashdenomenation_tran " & _
                    "	JOIN prcashdenomenation_master ON prcashdenomenation_tran.cashdenominationunkid = prcashdenomenation_master.cashdenominationunkid " & _
                    "	JOIN cfcommon_period_tran ON prcashdenomenation_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "	JOIN hremployee_master ON prcashdenomenation_master.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END
            
            

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'StrQ &= "WHERE prcashdenomenation_tran.isvoid = 0 AND prcashdenomenation_master.isvoid = 0 AND hremployee_master.isactive = 1 " & _
            '        "AND prcashdenomenation_master.yearunkid = " & FinancialYear._Object._YearUnkid & " "
            StrQ &= "WHERE prcashdenomenation_tran.isvoid = 0 AND prcashdenomenation_master.isvoid = 0 AND hremployee_master.isactive = 1 " & _
                    "AND prcashdenomenation_master.yearunkid = " & xYearUnkid & " "
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                StrQ &= " AND " & strFilerString
            End If
            'Sohail (21 Aug 2015) -- End

            If intUnkid > -1 Then
                StrQ &= " AND prcashdenomenation_master.cashdenominationunkid =@Unkid "
                objDataOperation.AddParameter("@Unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            'Sohail (24 Jun 2011) -- Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'StrQ &= UserAccessLevel._AccessLevelFilterString 'Sohail (21 Aug 2015)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Sohail (24 Jun 2011) -- End

            dtTable = objDataOperation.ExecQuery(StrQ, strList).Tables(strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dtTable

        Catch ex As Exception
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'DisplayError.Show("-1", ex.Message, "GetList", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
            'Sohail (18 May 2013) -- End
            Return Nothing
        End Try
    End Function
End Class
