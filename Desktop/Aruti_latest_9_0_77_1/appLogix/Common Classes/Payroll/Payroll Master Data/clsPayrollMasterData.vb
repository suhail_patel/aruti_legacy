﻿Option Strict On
Imports eZeeCommonLib
Public Class clsPayrollMasterData
    Dim mstrModuleName As String = "clsPayrollMasterData"
    Dim objDataOperation As clsDataOperation

    'Public Function GetEmployeeList(Optional ByVal strList As String = "List", Optional ByVal mblnAddSelect As Boolean = False, Optional ByVal blnOnlyActive As Boolean = True _
    '                                 , Optional ByVal intEmployeeID As Integer = 0 _
    '                                , Optional ByVal intDeptID As Integer = 0 _
    '                                , Optional ByVal intSectionID As Integer = 0 _
    '                                , Optional ByVal intUnitID As Integer = 0 _
    '                                , Optional ByVal intGradeID As Integer = 0 _
    '                                , Optional ByVal intAccessID As Integer = 0 _
    '                                , Optional ByVal intClassID As Integer = 0 _
    '                                , Optional ByVal intCostCenterID As Integer = 0 _
    '                                , Optional ByVal intServiceID As Integer = 0 _
    '                                , Optional ByVal intJobID As Integer = 0 _
    '                                , Optional ByVal intPayPointID As Integer = 0 _
    '                                ) As DataSet

    '    Dim strQ As String = ""
    '    'Dim strTmp As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet

    '    Try
    '        objDataOperation = New clsDataOperation

    '        'If mblnAddSelect = True Then
    '        '    strQ = "SELECT 0 AS employeeunkid , ' ' AS employeecode, ' ' + @Select AS employeename" & _
    '        '   " UNION "

    '        '    objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")
    '        'End If

    '        'strQ += "SELECT employeeunkid, employeecode, firstname + ' ' + surname as employeename FROM hremployee_master AS E "

    '        'If intDeptID > 0 Then
    '        '    strTmp += " AND A.departmentunkid = @departmentunkid"
    '        '    objDataOperation.AddParameter("@departmentunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intDeptID)
    '        'End If

    '        'If intSectionID > 0 Then
    '        '    strTmp += " AND A.sectionunkid = @sectionunkid"
    '        '    objDataOperation.AddParameter("@sectionunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intSectionID)
    '        'End If

    '        'If intUnitID > 0 Then
    '        '    strTmp += " AND A.unitunkid = @unitunkid"
    '        '    objDataOperation.AddParameter("@unitunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnitID)
    '        'End If

    '        'If intJobID > 0 Then
    '        '    strTmp += " AND A.jobunkid = @jobunkid"
    '        '    objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intJobID)
    '        'End If
    '        'If intGradeID > 0 Then
    '        '    strTmp += " AND A.gradeunkid = @gradeunkid"
    '        '    objDataOperation.AddParameter("@gradeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intGradeID)
    '        'End If

    '        'If intAccessID > 0 Then
    '        '    strTmp += " AND A.accessunkid = @accessunkid"
    '        '    objDataOperation.AddParameter("@accessunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intAccessID)
    '        'End If

    '        'If intClassID > 0 Then
    '        '    strTmp += " AND A.classunkid = @classunkid"
    '        '    objDataOperation.AddParameter("@classunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intClassID)
    '        'End If

    '        'If intServiceID > 0 Then
    '        '    strTmp += " AND A.serviceunkid = @serviceunkid"
    '        '    objDataOperation.AddParameter("@serviceunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intServiceID)
    '        'End If

    '        'If intCostCenterID > 0 Then
    '        '    strTmp += " AND A.costcenterunkid = @costcenterunkid"
    '        '    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCostCenterID)
    '        'End If

    '        'If strTmp.Length > 0 Then
    '        '    strQ += " INNER JOIN hremployee_allocation_master AS A " & _
    '        '        " ON E.employeeunkid =  A.employeeunkid"
    '        'End If

    '        'strQ += " WHERE 1=1"

    '        'strQ += strTmp

    '        'If intEmployeeID > 0 Then
    '        '    strQ += " AND E.employeeunkid = @employeeunkid"
    '        '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeID)
    '        'End If

    '        'If intPayPointID > 0 Then
    '        '    strQ += " AND E.paypointunkid = @paypointunkid"
    '        '    objDataOperation.AddParameter("@paypointunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPayPointID)
    '        'End If

    '        'If blnOnlyActive = True Then
    '        '    strQ += " AND E.IsActive = 1"
    '        'End If

    '        If mblnAddSelect = True Then
    '            strQ = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid UNION "

    '            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")
    '        End If

    '        strQ += "SELECT " & _
    '                  "employeecode AS employeecode " & _
    '                ",firstname+' '+othername+' '+surname AS employeename " & _
    '                ",employeeunkid As employeeunkid " & _
    '                "FROM hremployee_master " & _
    '                "WHERE 1=1 "

    '        If blnOnlyActive = True Then
    '            strQ += " AND IsActive = 1"
    '        End If

    '        If intEmployeeID > 0 Then
    '            strQ += " AND employeeunkid = @employeeunkid"
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeID)
    '        End If

    '        If intPayPointID > 0 Then
    '            strQ += " AND paypointunkid = @paypointunkid"
    '            objDataOperation.AddParameter("@paypointunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPayPointID)
    '        End If

    '        If intDeptID > 0 Then
    '            strQ += " AND departmentunkid = @departmentunkid"
    '            objDataOperation.AddParameter("@departmentunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intDeptID)
    '        End If

    '        If intSectionID > 0 Then
    '            strQ += " AND sectionunkid = @sectionunkid"
    '            objDataOperation.AddParameter("@sectionunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intSectionID)
    '        End If

    '        If intUnitID > 0 Then
    '            strQ += " AND unitunkid = @unitunkid"
    '            objDataOperation.AddParameter("@unitunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnitID)
    '        End If

    '        If intJobID > 0 Then
    '            strQ += " AND jobunkid = @jobunkid"
    '            objDataOperation.AddParameter("@jobunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intJobID)
    '        End If
    '        If intGradeID > 0 Then
    '            strQ += " AND gradeunkid = @gradeunkid"
    '            objDataOperation.AddParameter("@gradeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intGradeID)
    '        End If

    '        If intAccessID > 0 Then
    '            strQ += " AND accessunkid = @accessunkid"
    '            objDataOperation.AddParameter("@accessunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intAccessID)
    '        End If

    '        If intClassID > 0 Then
    '            strQ += " AND classunkid = @classunkid"
    '            objDataOperation.AddParameter("@classunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intClassID)
    '        End If

    '        If intServiceID > 0 Then
    '            strQ += " AND serviceunkid = @serviceunkid"
    '            objDataOperation.AddParameter("@serviceunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intServiceID)
    '        End If

    '        If intCostCenterID > 0 Then
    '            strQ += " AND costcenterunkid = @costcenterunkid"
    '            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCostCenterID)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        dsList = Nothing
    '        If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
    '        objDataOperation = Nothing

    '        exForce = Nothing
    '    End Try
    'End Function

    'Public Function GetCurrencyList(Optional ByVal strList As String = "List", Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet

    '    Try
    '        objDataOperation = New clsDataOperation

    '        strQ = "SELECT 0 AS exchangerateunkid , @Select AS currency_sign " & _
    '                " UNION " & _
    '                "SELECT exchangerateunkid, currency_sign FROM cfexchange_rate "
    '        If blnOnlyActive = True Then
    '            strQ += " WHERE Isactive = 1"
    '        End If

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")

    '        dsList = objDataOperation.ExecQuery(strQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetCurrencyList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        dsList = Nothing
    '        If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
    '        objDataOperation = Nothing

    '        exForce = Nothing
    '    End Try
    'End Function

    'Public Function GetCurrencyByUnkID(ByVal intCurrUnkID As Integer, Optional ByVal strList As String = "List") As DataSet
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet

    '    Try
    '        objDataOperation = New clsDataOperation


    '        strQ = "SELECT * FROM cfexchange_rate " & _
    '            " WHERE exchangerateunkid = @currencyunkid"

    '        objDataOperation.AddParameter("@currencyunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrUnkID)

    '        dsList = objDataOperation.ExecQuery(strQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetCurrencyByUnkID", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        dsList = Nothing
    '        If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
    '        objDataOperation = Nothing

    '        exForce = Nothing
    '    End Try
    'End Function

    'Public Function GetBaseCurrencyID(Optional ByVal strList As String = "List") As Integer
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet

    '    Try
    '        objDataOperation = New clsDataOperation

    '        strQ = "SELECT exchangerateunkid, currency_sign FROM cfexchange_rate WHERE Isactive = 1" & _
    '            " AND isbasecurrency = 1"

    '        dsList = objDataOperation.ExecQuery(strQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables(strList).Rows.Count > 0 Then
    '            Return CInt(dsList.Tables(strList).Rows(0).Item("exchangerateunkid").ToString)
    '        Else
    '            Return 0
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetCurrencyList", mstrModuleName)
    '        Return 0
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        dsList = Nothing
    '        If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
    '        objDataOperation = Nothing

    '        exForce = Nothing
    '    End Try
    'End Function

    Public Function GetGradeGroupLevelList(ByVal intGradeGroupId As Integer, Optional ByVal strList As String = "List", Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                "  hrgrade_master.gradegroupunkid " & _
                ", hrgradegroup_master.name AS GradeGroupName " & _
                ", hrgrade_master.gradeunkid " & _
                ", hrgrade_master.name AS GradeName " & _
                ", hrgradelevel_master.gradelevelunkid " & _
                ", hrgradelevel_master.name as LevelName " & _
                " FROM hrgradegroup_master " & _
                " INNER JOIN hrgrade_master " & _
                " ON hrgradegroup_master.gradegroupunkid = hrgrade_master.gradegroupunkid  " & _
                " INNER JOIN hrgradelevel_master " & _
                " ON hrgrade_master.gradeunkid = hrgradelevel_master.gradeunkid " & _
                " WHERE hrgrade_master.gradegroupunkid = @GradeGroupId "

            If blnOnlyActive Then
                strQ &= " AND hrgrade_master.isactive = 1 "
                strQ &= " AND hrgradelevel_master.isactive = 1"
            End If

            objDataOperation.AddParameter("@GradeGroupId", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeGroupId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGradeGroupLevelList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

End Class
