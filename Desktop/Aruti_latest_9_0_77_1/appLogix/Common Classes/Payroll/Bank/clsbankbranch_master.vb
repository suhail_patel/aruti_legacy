﻿'************************************************************************************************************************************
'Class Name : clsbankbranch_master.vb
'Purpose    :  All Bank Branch Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :01/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 4
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsbankbranch_master
    Private Shared ReadOnly mstrModuleName As String = "clsbankbranch_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBranchunkid As Integer
    Private mstrBranchcode As String = String.Empty
    Private mstrBranchname As String = String.Empty
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mintPincodeunkid As Integer
    Private mintBankgroupunkid As Integer
    Private mstrContactperson As String = String.Empty
    Private mstrContactno As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mblnIsclearing As Boolean 'Sohail (24 Jul 2013)

    'Pinkal (12-Oct-2011) -- Start
    Private mstrSortCode As String = String.Empty
    'Pinkal (12-Oct-2011) -- End


#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Branchcode() As String
        Get
            Return mstrBranchcode
        End Get
        Set(ByVal value As String)
            mstrBranchcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Branchname() As String
        Get
            Return mstrBranchname
        End Get
        Set(ByVal value As String)
            mstrBranchname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pincodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Pincodeunkid() As Integer
        Get
            Return mintPincodeunkid
        End Get
        Set(ByVal value As Integer)
            mintPincodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bankgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Bankgroupunkid() As Object
        Get
            Return mintBankgroupunkid
        End Get
        Set(ByVal value As Object)
            mintBankgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contactperson
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contactperson() As String
        Get
            Return mstrContactperson
        End Get
        Set(ByVal value As String)
            mstrContactperson = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contactno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contactno() As String
        Get
            Return mstrContactno
        End Get
        Set(ByVal value As String)
            mstrContactno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- Start

    ''' <summary>
    ''' Purpose: Get or Set Sortcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sortcode() As String
        Get
            Return mstrSortCode
        End Get
        Set(ByVal value As String)
            mstrSortCode = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End

    'Sohail (24 Jul 2013) -- Start
    'TRA - ENHANCEMENT

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isclearing() As Boolean
        Get
            Return mblnIsclearing
        End Get
        Set(ByVal value As Boolean)
            mblnIsclearing = value
        End Set
    End Property
    'Sohail (24 Jul 2013) -- End
#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "SELECT " & _
            '  "  branchunkid " & _
            '  ", branchcode " & _
            '  ", branchname " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", countryunkid " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", pincodeunkid " & _
            '  ", bankgroupunkid " & _
            '  ", contactperson " & _
            '  ", contactno " & _
            '  ", isactive " & _
            ' "FROM hrmsConfiguration..cfbankbranch_master " & _
            ' "WHERE branchunkid = @branchunkid "


            strQ = "SELECT " & _
              "  branchunkid " & _
              ", branchcode " & _
              ", branchname " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", pincodeunkid " & _
              ", bankgroupunkid " & _
              ", contactperson " & _
              ", contactno " & _
              ", isactive " & _
             ",sortcode " & _
              ", ISNULL(isclearing, 0) AS isclearing " & _
             "FROM hrmsConfiguration..cfbankbranch_master " & _
             "WHERE branchunkid = @branchunkid "
            'Sohail (24 Jul 2013) - [isclearing]

            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mstrBranchcode = dtRow.Item("branchcode").ToString
                mstrBranchname = dtRow.Item("branchname").ToString
                mstrAddress1 = dtRow.Item("address1").ToString
                mstrAddress2 = dtRow.Item("address2").ToString
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mintPincodeunkid = CInt(dtRow.Item("pincodeunkid"))
                mintBankgroupunkid = CInt(dtRow.Item("bankgroupunkid"))
                mstrContactperson = dtRow.Item("contactperson").ToString
                mstrContactno = dtRow.Item("contactno").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsclearing = CBool(dtRow.Item("isclearing")) 'Sohail (24 Jul 2013)

                'Pinkal (12-Oct-2011) -- Start
                If Not IsDBNull(dtRow.Item("sortcode")) Then
                    mstrSortCode = dtRow.Item("sortcode").ToString()
                End If
                'Pinkal (12-Oct-2011) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start

            'strQ = "SELECT " & _
            '  "  branchunkid " & _
            '  ", branchcode " & _
            '  ", branchname " & _
            '  ", isnull(address1,'' ) + isnull(address2,'') as address " & _
            '  ", hrmsConfiguration..cfbankbranch_master.countryunkid " & _
            '  ", hrmsConfiguration..cfcountry_master.country_name as country " & _
            '  ", hrmsConfiguration..cfbankbranch_master.stateunkid " & _
            '  ", hrmsConfiguration..cfstate_master.name as state " & _
            '  ", hrmsConfiguration..cfbankbranch_master.cityunkid " & _
            '  ", hrmsConfiguration..cfcity_master.name as city " & _
            '  ", hrmsConfiguration..cfbankbranch_master.pincodeunkid " & _
            '  ", hz.zipcode_no as pincode " & _
            '  ", hrmsConfiguration..cfbankbranch_master.bankgroupunkid " & _
            '  ", hrmsConfiguration..cfpayrollgroup_master.groupname as bankgroup " & _
            '  ", contactperson " & _
            '  ", contactno " & _
            '  ", hrmsConfiguration..cfbankbranch_master.isactive " & _
            ' "FROM hrmsConfiguration..cfbankbranch_master " & _
            ' " LEFT JOIN hrmsConfiguration..cfpayrollgroup_master on hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = hrmsConfiguration..cfbankbranch_master.bankgroupunkid " & _
            ' " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = hrmsConfiguration..cfbankbranch_master.countryunkid " & _
            ' " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hrmsConfiguration..cfbankbranch_master.stateunkid " & _
            ' " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hrmsConfiguration..cfbankbranch_master.cityunkid " & _
            ' " LEFT JOIN hrmsConfiguration..cfzipcode_master hz on hz.zipcodeunkid = hrmsConfiguration..cfbankbranch_master.pincodeunkid "


            strQ = "SELECT " & _
              "  branchunkid " & _
              ", branchcode " & _
              ", branchname " & _
              ", isnull(address1,'' ) + isnull(address2,'') as address " & _
              ", hrmsConfiguration..cfbankbranch_master.countryunkid " & _
              ", hrmsConfiguration..cfcountry_master.country_name as country " & _
              ", hrmsConfiguration..cfbankbranch_master.stateunkid " & _
              ", hrmsConfiguration..cfstate_master.name as state " & _
              ", hrmsConfiguration..cfbankbranch_master.cityunkid " & _
              ", hrmsConfiguration..cfcity_master.name as city " & _
              ", hrmsConfiguration..cfbankbranch_master.pincodeunkid " & _
              ", hz.zipcode_no as pincode " & _
              ", hrmsConfiguration..cfbankbranch_master.bankgroupunkid " & _
              ", hrmsConfiguration..cfpayrollgroup_master.groupname as bankgroup " & _
              ", contactperson " & _
              ", contactno " & _
              ", hrmsConfiguration..cfbankbranch_master.isactive " & _
                      ", hrmsConfiguration..cfbankbranch_master.sortcode " & _
              ", ISNULL(hrmsConfiguration..cfbankbranch_master.isclearing, 0) AS isclearing " & _
             "FROM hrmsConfiguration..cfbankbranch_master " & _
             " LEFT JOIN hrmsConfiguration..cfpayrollgroup_master on hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = hrmsConfiguration..cfbankbranch_master.bankgroupunkid " & _
             " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = hrmsConfiguration..cfbankbranch_master.countryunkid " & _
             " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hrmsConfiguration..cfbankbranch_master.stateunkid " & _
             " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hrmsConfiguration..cfbankbranch_master.cityunkid " & _
             " LEFT JOIN hrmsConfiguration..cfzipcode_master hz on hz.zipcodeunkid = hrmsConfiguration..cfbankbranch_master.pincodeunkid "
            'Sohail (24 Jul 2013) - [isclearing]
            'Pinkal (12-Oct-2011) -- End


            If blnOnlyActive Then
                strQ &= " WHERE hrmsConfiguration..cfbankbranch_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmsConfiguration..cfbankbranch_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrBranchcode, , , mintBankgroupunkid) Then ' Anjan (04 Feb 2013)
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Branch Code is already defined. Please define new Branch Code.")
            Return False
        ElseIf isExist("", mstrBranchname, , mintBankgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Branch Name is already defined. Please define new Branch Name.")
            Return False
        End If


        'Pinkal (24-Aug-2013) -- Start
        'Enhancement : TRA Changes

        If mstrSortCode <> "" Then
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Support Issue Id # 4210 : Bank Branches should be allowed to have same sort codes for same bank group.
            'If isSortCodeExist(mstrSortCode, -1, mstrBranchcode, mstrBranchname) Then
            'Hemant (20 Dec 2019) -- Start
            'ISSUE (Mainspring Resourcing Ghana): Unable to Add/Edit New Branch Entry in Bank Branch Screen with Same Sort Code in Multiple Bank Group.
            'If isSortCodeExist(mstrSortCode, -1, "", "", mintBankgroupunkid) Then
            '    'Sohail (30 Oct 2019) -- End
            '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This Sort Code is already defined. Please define new Sort Code.")
            '    Return False
            'End If
            'Hemant (20 Dec 2019) -- End
        End If
        'Pinkal (24-Aug-2013) -- End


       Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@branchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchcode.ToString)
            objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPincodeunkid.ToString)
            'Sohail (02 Nov 2018) -- Start
            'AT Issue - AT Log Issue in 75.1.
            'objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.NChar, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
            'Sohail (02 Nov 2018) -- End
            objDataOperation.AddParameter("@contactperson", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContactperson.ToString)
            objDataOperation.AddParameter("@contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContactno.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isclearing", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclearing.ToString) 'Sohail (24 Jul 2013)


            'Pinkal (12-Oct-2011) -- Start

            objDataOperation.AddParameter("@sortcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSortCode.ToString)

            'strQ = "INSERT INTO hrmsConfiguration..cfbankbranch_master ( " & _
            '  "  branchcode " & _
            '  ", branchname " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", countryunkid " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", pincodeunkid " & _
            '  ", bankgroupunkid " & _
            '  ", contactperson " & _
            '  ", contactno " & _
            '  ", isactive" & _
            '") VALUES (" & _
            '  "  @branchcode " & _
            '  ", @branchname " & _
            '  ", @address1 " & _
            '  ", @address2 " & _
            '  ", @countryunkid " & _
            '  ", @stateunkid " & _
            '  ", @cityunkid " & _
            '  ", @pincodeunkid " & _
            '  ", @bankgroupunkid " & _
            '  ", @contactperson " & _
            '  ", @contactno " & _
            '  ", @isactive" & _
            '"); SELECT @@identity"


            strQ = "INSERT INTO hrmsConfiguration..cfbankbranch_master ( " & _
              "  branchcode " & _
              ", branchname " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", pincodeunkid " & _
              ", bankgroupunkid " & _
              ", contactperson " & _
              ", contactno " & _
              ", isactive" & _
                     ", sortcode " & _
              ", isclearing " & _
            ") VALUES (" & _
              "  @branchcode " & _
              ", @branchname " & _
              ", @address1 " & _
              ", @address2 " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @pincodeunkid " & _
              ", @bankgroupunkid " & _
              ", @contactperson " & _
              ", @contactno " & _
              ", @isactive" & _
                     ", @sortcode " & _
              ", @isclearing " & _
            "); SELECT @@identity"
            'Sohail (24 Jul 2013) - [isclearing]
            'Pinkal (12-Oct-2011) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBranchunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfbankbranch_master", "branchunkid", mintBranchunkid, True) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmsConfiguration..cfbankbranch_master) </purpose>
    Public Function Update() As Boolean


        'Pinkal (12-Oct-2011) -- Start

        If isExist(mstrBranchcode, "", mintBranchunkid, mintBankgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Branch Code is already defined. Please define new Branch Code.")
            Return False
        ElseIf isExist("", mstrBranchname, mintBranchunkid, mintBankgroupunkid) Then 'Anjan (04 May 2019) ' # 3790, Included different bannk group to be allowed with same bank branch.
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Branch Name is already defined. Please define new Branch Name.")
            Return False
        End If


        'Pinkal (24-Aug-2013) -- Start
        'Enhancement : TRA Changes

        If mstrSortCode <> "" Then
            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Support Issue Id # 4210 : Bank Branches should be allowed to have same sort codes for same bank group.
            'If isSortCodeExist(mstrSortCode, mintBranchunkid, "", "") Then
            'Hemant (20 Dec 2019) -- Start
            'ISSUE (Mainspring Resourcing Ghana): Unable to Add/Edit New Branch Entry in Bank Branch Screen with Same Sort Code in Multiple Bank Group.
            'If isSortCodeExist(mstrSortCode, mintBranchunkid, "", "", mintBankgroupunkid) Then
            '    'Sohail (30 Oct 2019) -- End
            '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This Sort Code is already defined. Please define new Sort Code.")
            '    Return False
            'End If
            'Hemant (20 Dec 2019) -- End
        End If

        'Pinkal (24-Aug-2013) -- End

Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)
        Try
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@branchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchcode.ToString)
            objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPincodeunkid.ToString)
            'Sohail (02 Nov 2018) -- Start
            'AT Issue - AT Log Issue in 75.1.
            'objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.NChar, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
            'Sohail (02 Nov 2018) -- End
            objDataOperation.AddParameter("@contactperson", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContactperson.ToString)
            objDataOperation.AddParameter("@contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContactno.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isclearing", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclearing.ToString)  'Sohail (24 Jul 2013)

            'Pinkal (12-Oct-2011) -- Start

            objDataOperation.AddParameter("@sortcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSortCode.ToString)

            'strQ = "UPDATE hrmsConfiguration..cfbankbranch_master SET " & _
            '  "  branchcode = @branchcode" & _
            '  ", branchname = @branchname" & _
            '  ", address1 = @address1" & _
            '  ", address2 = @address2" & _
            '  ", countryunkid = @countryunkid" & _
            '  ", stateunkid = @stateunkid" & _
            '  ", cityunkid = @cityunkid" & _
            '  ", pincodeunkid = @pincodeunkid" & _
            '  ", bankgroupunkid = @bankgroupunkid" & _
            '  ", contactperson = @contactperson" & _
            '  ", contactno = @contactno" & _
            '  ", isactive = @isactive " & _
            '"WHERE branchunkid = @branchunkid "

            strQ = "UPDATE hrmsConfiguration..cfbankbranch_master SET " & _
              "  branchcode = @branchcode" & _
              ", branchname = @branchname" & _
              ", address1 = @address1" & _
              ", address2 = @address2" & _
              ", countryunkid = @countryunkid" & _
              ", stateunkid = @stateunkid" & _
              ", cityunkid = @cityunkid" & _
              ", pincodeunkid = @pincodeunkid" & _
              ", bankgroupunkid = @bankgroupunkid" & _
              ", contactperson = @contactperson" & _
              ", contactno = @contactno" & _
              ", isactive = @isactive " & _
           ", sortcode  = @sortcode " & _
              ", isclearing = @isclearing " & _
            "WHERE branchunkid = @branchunkid "
            'Sohail (24 Jul 2013) - [isclearing]
            'Pinkal (12-Oct-2011) -- End


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfbankbranch_master", mintBranchunkid, "branchunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfbankbranch_master", "branchunkid", mintBranchunkid, True) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True

            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmsConfiguration..cfbankbranch_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Branch. Reason: This Branch is in use.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Hemant (26 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            'strQ = "DELETE FROM hrmsConfiguration..cfbankbranch_master " & _
            '"WHERE branchunkid = @branchunkid "
            strQ = "UPDATE hrmsConfiguration..cfbankbranch_master " & _
                    " SET isactive = 0 " & _
            "WHERE branchunkid = @branchunkid "
            'Hemant (26 Feb 2019) -- End

            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfbankbranch_master", "branchunkid", intUnkid, True) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strDatabaseName As String 'Sohail (05 Mar 2013)

        objDataOperation = New clsDataOperation

        Try

            'Sohail (05 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            strQ = "SELECT  database_name " & _
                    "FROM    hrmsConfiguration..cffinancial_year_tran " & _
                            "JOIN hrmsConfiguration..cfcompany_master ON hrmsConfiguration.dbo.cffinancial_year_tran.companyunkid = hrmsConfiguration.dbo.cfcompany_master.companyunkid " & _
                    "WHERE   cfcompany_master.isactive = 1 " & _
                    "ORDER BY cffinancial_year_tran.companyunkid "

            dsList = objDataOperation.ExecQuery(strQ, "Database")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (05 Mar 2013) -- End

            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"
            'Sohail (05 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT  branchunkid " & _
            '                   "FROM    premployee_bank_tran " & _
            '                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                           "AND branchunkid = @branchunkid " & _
            '                   "UNION " & _
            '                   "SELECT  branchunkid " & _
            '                   "FROM    prpayment_tran " & _
            '                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                           "AND branchunkid = @branchunkid " & _
            '                   "UNION " & _
            '                   "SELECT  branchunkid " & _
            '                   "FROM    prbankedi_master " & _
            '                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                            "AND branchunkid = @branchunkid "
            'Sohail (19 Nov 2010) -- End

            'objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Return dsList.Tables(0).Rows.Count > 0

            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dsRow As DataRow In dsList.Tables("Database").Rows
                strDatabaseName = dsRow.Item("database_name").ToString

            strQ = "SELECT  branchunkid " & _
                               "FROM    " & strDatabaseName & "..premployee_bank_tran " & _
                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
                           "AND branchunkid = @branchunkid " & _
                   "UNION " & _
                   "SELECT  branchunkid " & _
                               "FROM    " & strDatabaseName & "..prpayment_tran " & _
                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
                           "AND branchunkid = @branchunkid " & _
                   "UNION " & _
                   "SELECT  branchunkid " & _
                               "FROM    " & strDatabaseName & "..prbankedi_master " & _
                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND branchunkid = @branchunkid "



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Return True
                End If
            Next

            Return False
            'Sohail (05 Mar 2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal intBankGroupId As Integer = -1, Optional ByRef intId As Integer = 0) As Boolean
        'Sohail (02 Aug 2017) - [intId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            intId = 0 'Sohail (02 Aug 2017)

            'Pinkal (12-Oct-2011) -- Start

            'strQ = "SELECT " & _
            '  "  branchunkid " & _
            '  ", branchcode " & _
            '  ", branchname " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", countryunkid " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", pincodeunkid " & _
            '  ", bankgroupunkid " & _
            '  ", contactperson " & _
            '  ", contactno " & _
            '  ", isactive " & _
            ' "FROM hrmsConfiguration..cfbankbranch_master " & _
            ' "WHERE 1=1"

            strQ = "SELECT " & _
              "  branchunkid " & _
              ", branchcode " & _
              ", branchname " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", pincodeunkid " & _
              ", bankgroupunkid " & _
              ", contactperson " & _
              ", contactno " & _
              ", isactive " & _
           ", sortcode " & _
              ", isclearing " & _
             "FROM hrmsConfiguration..cfbankbranch_master " & _
             "WHERE 1=1"
            'Sohail (24 Jul 2013) - [isclearing]
            'Pinkal (12-Oct-2011) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Trim.Length > 0 Then
                strQ &= " AND branchcode = @branchcode "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND branchname = @branchname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND branchunkid <> @branchunkid"
            End If

            'Anjan [ 04 Feb 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            If intBankGroupId > 0 Then
                strQ &= " AND bankgroupunkid = @bankgroupunkid "
            End If

            'Anjan [ 04 Feb 2013 ] -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@branchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            'Anjan [ 04 Feb 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGroupId)
            'Anjan [ 04 Feb 2013 ] -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("branchunkid"))
            End If
            'Sohail (02 Aug 2017) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intBankGrpId As Integer = -1) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ = "SELECT 0 as branchunkid, ' ' +  @name  as name UNION "
                strQ = "SELECT 0 as branchunkid, '' AS Code, ' ' +  @name  as name UNION "
                'Sohail (10 Aug 2012) -- End
            End If
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ &= "SELECT branchunkid, branchname as name FROM hrmsConfiguration..cfbankbranch_master where isactive = 1 "
            strQ &= "SELECT branchunkid, branchcode AS Code, branchname as name FROM hrmsConfiguration..cfbankbranch_master where isactive = 1 "
            'Sohail (10 Aug 2012) -- End

            If intBankGrpId > -1 Then
                strQ &= " AND bankgroupunkid = @bankgroupunkid "
                objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGrpId.ToString)
            End If
            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function


    'Sandeep [ 25 APRIL 2011 ] -- Start
    Public Function GetBankBranchUnkid(ByVal StrName As String, Optional ByVal intBankGrpId As Integer = -1) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet

        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                       "   branchunkid " & _
                       "FROM hrmsConfiguration..cfbankbranch_master " & _
                       "WHERE branchname = @BName " & _
                       "   AND isactive = 1 "

            objDataOperation.AddParameter("@BName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrName)

            If intBankGrpId > 0 Then
                StrQ &= "   AND bankgroupunkid = @BGrpId "
                objDataOperation.AddParameter("@BGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGrpId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("branchunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

'Pinkal (12-Oct-2011) -- Start

    Public Function isSortCodeExist(Optional ByVal strSortcode As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal strBranchCode As String = "", Optional ByVal strBranchname As String = "", Optional ByVal intBankGroupUnkId As Integer = 0) As Boolean
        'Sohail (30 Oct 2019) - [intBankGroupUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                       " sortcode " & _
                       " FROM hrmsConfiguration..cfbankbranch_master " & _
                       " WHERE 1=1 "

            If strSortcode.Trim.Length > 0 Then
                strQ &= " AND sortcode = @sortcode "
            End If

            If intUnkid > 0 Then
                strQ &= " AND branchunkid <> @branchunkid"
            End If


            'Pinkal (24-Aug-2013) -- Start
            'Enhancement : TRA Changes

            objDataOperation.ClearParameters()

            If strBranchCode.Trim.Length > 0 Then
                strQ &= " AND branchcode = @branchcode "
                objDataOperation.AddParameter("@branchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBranchCode)
            End If

            If strBranchname.Trim.Length > 0 Then
                strQ &= " AND branchname = @branchname "
                objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBranchname)
            End If

            'Pinkal (24-Aug-2013) -- End

            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Support Issue Id # 4210 : Bank Branches should be allowed to have same sort codes for same bank group.
            If intBankGroupUnkId > 0 Then
                strQ &= " AND bankgroupunkid <> @bankgroupunkid"
                objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGroupUnkId)
            End If
            'Sohail (30 Oct 2019) -- End

            objDataOperation.AddParameter("@sortcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSortcode)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSortCodeExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End

    'Sandeep [ 25 APRIL 2011 ] -- End 

    'Sohail (06 Aug 2016) -- Start
    'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
    Public Function GetBankBranchIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.BANK)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBankBranchIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetBankBranchIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.BANK)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBankBranchIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (06 Aug 2016) -- End

    'Sohail (03 Jan 2019) -- Start
    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
    Public Function GetCompanyBankBranchIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.COMPANY_BANK)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBankBranchIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetCompanyBankBranchIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.COMPANY_BANK)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBankBranchIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (03 Jan 2019) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Branch Code is already defined. Please define new Branch Code.")
			Language.setMessage(mstrModuleName, 2, "This Branch Name is already defined. Please define new Branch Name.")
			Language.setMessage(mstrModuleName, 3, "This Sort Code is already defined. Please define new Sort Code.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

''************************************************************************************************************************************
''Class Name : clsbankbranch_master.vb
''Purpose    :  All Bank Branch Opration like getList, Insert, Update, Delete, checkDuplicate
''Date       :01/07/2010
''Written By :Pinkal
''Modified   :
''Last Message Index = 3
''************************************************************************************************************************************

'Imports eZeeCommonLib
'''' <summary>
'''' Purpose: 
'''' Developer: Pinkal
'''' </summary>
'Public Class clsbankbranch_master
'    Private Shared Readonly mstrModuleName As String = "clsbankbranch_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintBranchunkid As Integer
'    Private mstrBranchcode As String = String.Empty
'    Private mstrBranchname As String = String.Empty
'    Private mstrAddress1 As String = String.Empty
'    Private mstrAddress2 As String = String.Empty
'    Private mintCountryunkid As Integer
'    Private mintStateunkid As Integer
'    Private mintCityunkid As Integer
'    Private mintPincodeunkid As Integer
'    Private mintBankgroupunkid As Integer
'    Private mstrContactperson As String = String.Empty
'    Private mstrContactno As String = String.Empty
'    Private mblnIsactive As Boolean = True
'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set branchunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Branchunkid() As Integer
'        Get
'            Return mintBranchunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintBranchunkid = value
'            Call GetData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set branchcode
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Branchcode() As String
'        Get
'            Return mstrBranchcode
'        End Get
'        Set(ByVal value As String)
'            mstrBranchcode = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set branchname
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Branchname() As String
'        Get
'            Return mstrBranchname
'        End Get
'        Set(ByVal value As String)
'            mstrBranchname = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set address1
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Address1() As String
'        Get
'            Return mstrAddress1
'        End Get
'        Set(ByVal value As String)
'            mstrAddress1 = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set address2
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Address2() As String
'        Get
'            Return mstrAddress2
'        End Get
'        Set(ByVal value As String)
'            mstrAddress2 = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set countryunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Countryunkid() As Integer
'        Get
'            Return mintCountryunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCountryunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set stateunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Stateunkid() As Integer
'        Get
'            Return mintStateunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintStateunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set cityunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Cityunkid() As Integer
'        Get
'            Return mintCityunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCityunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set pincodeunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Pincodeunkid() As Integer
'        Get
'            Return mintPincodeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintPincodeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set bankgroupunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Bankgroupunkid() As Object
'        Get
'            Return mintBankgroupunkid
'        End Get
'        Set(ByVal value As Object)
'            mintBankgroupunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set contactperson
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Contactperson() As String
'        Get
'            Return mstrContactperson
'        End Get
'        Set(ByVal value As String)
'            mstrContactperson = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set contactno
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Contactno() As String
'        Get
'            Return mstrContactno
'        End Get
'        Set(ByVal value As String)
'            mstrContactno = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  branchunkid " & _
'              ", branchcode " & _
'              ", branchname " & _
'              ", address1 " & _
'              ", address2 " & _
'              ", countryunkid " & _
'              ", stateunkid " & _
'              ", cityunkid " & _
'              ", pincodeunkid " & _
'              ", bankgroupunkid " & _
'              ", contactperson " & _
'              ", contactno " & _
'              ", isactive " & _
'             "FROM prbankbranch_master " & _
'             "WHERE branchunkid = @branchunkid "

'            objDataOperation.AddParameter("@branchunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBranchUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintbranchunkid = CInt(dtRow.Item("branchunkid"))
'                mstrbranchcode = dtRow.Item("branchcode").ToString
'                mstrbranchname = dtRow.Item("branchname").ToString
'                mstraddress1 = dtRow.Item("address1").ToString
'                mstraddress2 = dtRow.Item("address2").ToString
'                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
'                mintstateunkid = CInt(dtRow.Item("stateunkid"))
'                mintcityunkid = CInt(dtRow.Item("cityunkid"))
'                mintpincodeunkid = CInt(dtRow.Item("pincodeunkid"))
'                mintBankgroupunkid = CInt(dtRow.Item("bankgroupunkid"))
'                mstrcontactperson = dtRow.Item("contactperson").ToString
'                mstrcontactno = dtRow.Item("contactno").ToString
'                mblnisactive = CBool(dtRow.Item("isactive"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  branchunkid " & _
'              ", branchcode " & _
'              ", branchname " & _
'              ", isnull(address1,'' ) + isnull(address2,'') as address " & _
'              ", prbankbranch_master.countryunkid " & _
'              ", cfcommon_master.name as country " & _
'              ", prbankbranch_master.stateunkid " & _
'              ", hrstate_master.name as state " & _
'              ", prbankbranch_master.cityunkid " & _
'              ", hrcity_master.name as city " & _
'              ", prbankbranch_master.pincodeunkid " & _
'              ", hz.zipcode_no as pincode " & _
'              ", prbankbranch_master.bankgroupunkid " & _
'              ", prpayrollgroup_master.groupname as bankgroup " & _
'              ", contactperson " & _
'              ", contactno " & _
'              ", prbankbranch_master.isactive " & _
'             "FROM prbankbranch_master " & _
'             " LEFT JOIN prpayrollgroup_master on prpayrollgroup_master.groupmasterunkid = prbankbranch_master.bankgroupunkid " & _
'             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = prbankbranch_master.countryunkid " & _
'             " LEFT JOIN hrstate_master on hrstate_master.stateunkid = prbankbranch_master.stateunkid " & _
'             " LEFT JOIN hrcity_master on hrcity_master.cityunkid = prbankbranch_master.cityunkid " & _
'             " LEFT JOIN hrzipcode_master hz on hz.zipcodeunkid = prbankbranch_master.pincodeunkid "

'            If blnOnlyActive Then
'                strQ &= " WHERE prbankbranch_master.isactive = 1 "
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (prbankbranch_master) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mstrBranchcode) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Branch Code is already defined. Please define new Branch Code.")
'            Return False
'        ElseIf isExist("", mstrBranchname) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Branch Name is already defined. Please define new Branch Name.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@branchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchcode.ToString)
'            objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchname.ToString)
'            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
'            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
'            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
'            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
'            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPincodeunkid.ToString)
'            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.NChar, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
'            objDataOperation.AddParameter("@contactperson", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContactperson.ToString)
'            objDataOperation.AddParameter("@contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContactno.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

'            strQ = "INSERT INTO prbankbranch_master ( " & _
'              "  branchcode " & _
'              ", branchname " & _
'              ", address1 " & _
'              ", address2 " & _
'              ", countryunkid " & _
'              ", stateunkid " & _
'              ", cityunkid " & _
'              ", pincodeunkid " & _
'              ", bankgroupunkid " & _
'              ", contactperson " & _
'              ", contactno " & _
'              ", isactive" & _
'            ") VALUES (" & _
'              "  @branchcode " & _
'              ", @branchname " & _
'              ", @address1 " & _
'              ", @address2 " & _
'              ", @countryunkid " & _
'              ", @stateunkid " & _
'              ", @cityunkid " & _
'              ", @pincodeunkid " & _
'              ", @bankgroupunkid " & _
'              ", @contactperson " & _
'              ", @contactno " & _
'              ", @isactive" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintBranchunkid = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (prbankbranch_master) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mstrBranchcode, "", mintBranchunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Branch Code is already defined. Please define new Branch Code.")
'            Return False
'        ElseIf isExist("", mstrBranchname, mintBranchunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Branch Name is already defined. Please define new Branch Name.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@branchunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbranchunkid.ToString)
'            objDataOperation.AddParameter("@branchcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrbranchcode.ToString)
'            objDataOperation.AddParameter("@branchname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrbranchname.ToString)
'            objDataOperation.AddParameter("@address1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress1.ToString)
'            objDataOperation.AddParameter("@address2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress2.ToString)
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
'            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
'            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
'            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpincodeunkid.ToString)
'            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.NChar, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
'            objDataOperation.AddParameter("@contactperson", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactperson.ToString)
'            objDataOperation.AddParameter("@contactno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactno.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

'            StrQ = "UPDATE prbankbranch_master SET " & _
'              "  branchcode = @branchcode" & _
'              ", branchname = @branchname" & _
'              ", address1 = @address1" & _
'              ", address2 = @address2" & _
'              ", countryunkid = @countryunkid" & _
'              ", stateunkid = @stateunkid" & _
'              ", cityunkid = @cityunkid" & _
'              ", pincodeunkid = @pincodeunkid" & _
'              ", bankgroupunkid = @bankgroupunkid" & _
'              ", contactperson = @contactperson" & _
'              ", contactno = @contactno" & _
'              ", isactive = @isactive " & _
'            "WHERE branchunkid = @branchunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (prbankbranch_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Branch. Reason: This Branch is in use.")
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "DELETE FROM prbankbranch_master " & _
'            "WHERE branchunkid = @branchunkid "

'            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  branchunkid " & _
'              ", branchcode " & _
'              ", branchname " & _
'              ", address1 " & _
'              ", address2 " & _
'              ", countryunkid " & _
'              ", stateunkid " & _
'              ", cityunkid " & _
'              ", pincodeunkid " & _
'              ", bankgroupunkid " & _
'              ", contactperson " & _
'              ", contactno " & _
'              ", isactive " & _
'             "FROM prbankbranch_master " & _
'             "WHERE 1=1"


'            If strCode.Length > 0 Then
'                strQ &= " AND branchcode = @branchcode "
'            End If

'            If strName.Length > 0 Then
'                strQ &= " AND branchname = @branchname "
'            End If

'            If intUnkid > 0 Then
'                strQ &= " AND branchunkid <> @branchunkid"
'            End If

'            objDataOperation.AddParameter("@branchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'            objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intBankGrpId As Integer = -1) As DataSet
'        Dim dsList As New DataSet
'        Dim objDataOperation As New clsDataOperation
'        Dim strQ As String = String.Empty
'        Dim exForce As Exception
'        Try
'            If mblFlag = True Then
'                strQ = "SELECT 0 as branchunkid, ' ' +  @name  as name UNION "
'            End If
'            strQ &= "SELECT branchunkid, branchname as name FROM prbankbranch_master where isactive = 1 "

'            If intBankGrpId > -1 Then
'                strQ &= " AND bankgroupunkid = @bankgroupunkid "
'                objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGrpId.ToString)
'            End If
'            strQ &= " ORDER BY name "

'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
'            dsList = objDataOperation.ExecQuery(strQ, strListName)
'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            Return dsList
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            objDataOperation = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'        End Try

'    End Function


'End Class