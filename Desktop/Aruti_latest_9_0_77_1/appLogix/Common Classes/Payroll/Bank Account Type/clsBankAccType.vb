﻿'************************************************************************************************************************************
'Class Name : clsbankacctype.vb
'Purpose    :
'Date       :01/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsBankAccType
    Private Shared ReadOnly mstrModuleName As String = "clsBankAccType"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAccounttypeunkid As Integer
    Private mstrAccounttype_Alias As String = String.Empty
    Private mstrAccounttype_Code As String = String.Empty
    Private mstrAccounttype_Name As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrAccounttype_Name1 As String = String.Empty
    Private mstrAccounttype_Name2 As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttypeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Accounttypeunkid() As Integer
        Get
            Return mintAccounttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintAccounttypeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttype_alias
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Accounttype_Alias() As String
        Get
            Return mstrAccounttype_Alias
        End Get
        Set(ByVal value As String)
            mstrAccounttype_Alias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttype_code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Accounttype_Code() As String
        Get
            Return mstrAccounttype_Code
        End Get
        Set(ByVal value As String)
            mstrAccounttype_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttype_name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Accounttype_Name() As String
        Get
            Return mstrAccounttype_Name
        End Get
        Set(ByVal value As String)
            mstrAccounttype_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttype_name1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Accounttype_Name1() As String
        Get
            Return mstrAccounttype_Name1
        End Get
        Set(ByVal value As String)
            mstrAccounttype_Name1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttype_name2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Accounttype_Name2() As String
        Get
            Return mstrAccounttype_Name2
        End Get
        Set(ByVal value As String)
            mstrAccounttype_Name2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  accounttypeunkid " & _
                      ", accounttype_alias " & _
                      ", accounttype_code " & _
                      ", accounttype_name " & _
                      ", description " & _
                      ", isactive " & _
                      ", accounttype_name1 " & _
                      ", accounttype_name2 " & _
                    "FROM hrmsConfiguration..cfbankacctype_master " & _
                    "WHERE accounttypeunkid = @accounttypeunkid " 'Sohail (28 Dec 2010)

            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAccounttypeUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintaccounttypeunkid = CInt(dtRow.Item("accounttypeunkid"))
                mstraccounttype_alias = dtRow.Item("accounttype_alias").ToString
                mstraccounttype_code = dtRow.Item("accounttype_code").ToString
                mstraccounttype_name = dtRow.Item("accounttype_name").ToString
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstraccounttype_name1 = dtRow.Item("accounttype_name1").ToString
                mstraccounttype_name2 = dtRow.Item("accounttype_name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  accounttypeunkid " & _
                      ", accounttype_alias " & _
                      ", accounttype_code " & _
                      ", accounttype_name " & _
                      ", description " & _
                      ", isactive " & _
                      ", accounttype_name1 " & _
                      ", accounttype_name2 " & _
                   "FROM hrmsConfiguration..cfbankacctype_master " 'Sohail (28 Dec 2010)

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmsConfiguration..cfbankacctype_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrAccounttype_Code) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Bank Account Type Code is already defined. Please define new Bank Account Type Code.")
            Return False
        End If

        If isExist(, mstrAccounttype_Name, ) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Bank Account Type Name is already defined. Please define new Bank Account Type Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@accounttype_alias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_alias.ToString)
            objDataOperation.AddParameter("@accounttype_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_code.ToString)
            objDataOperation.AddParameter("@accounttype_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_name.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@accounttype_name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_name1.ToString)
            objDataOperation.AddParameter("@accounttype_name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_name2.ToString)

            strQ = "INSERT INTO hrmsConfiguration..cfbankacctype_master ( " & _
                      "  accounttype_alias " & _
                      ", accounttype_code " & _
                      ", accounttype_name " & _
                      ", description " & _
                      ", isactive " & _
                      ", accounttype_name1 " & _
                      ", accounttype_name2" & _
                    ") VALUES (" & _
                      "  @accounttype_alias " & _
                      ", @accounttype_code " & _
                      ", @accounttype_name " & _
                      ", @description " & _
                      ", @isactive " & _
                      ", @accounttype_name1 " & _
                      ", @accounttype_name2" & _
                    "); SELECT @@identity" 'Sohail (28 Dec 2010)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAccounttypeUnkId = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfbankacctype_master", "accounttypeunkid", mintAccounttypeunkid, True) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmsConfiguration..cfbankacctype_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mstrAccounttype_Code, , mintAccounttypeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "This Account Type Code is already defined. Please define new Account Type Code.")
            Return False
        End If

        If isExist(, mstrAccounttype_Name, mintAccounttypeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "This Account Type Name is already defined. Please define new Account Type Name.")
            Return False
        End If


        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccounttypeunkid.ToString)
            objDataOperation.AddParameter("@accounttype_alias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_alias.ToString)
            objDataOperation.AddParameter("@accounttype_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_code.ToString)
            objDataOperation.AddParameter("@accounttype_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_name.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@accounttype_name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_name1.ToString)
            objDataOperation.AddParameter("@accounttype_name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccounttype_name2.ToString)

            strQ = "UPDATE hrmsConfiguration..cfbankacctype_master SET " & _
                      "  accounttype_alias = @accounttype_alias" & _
                      ", accounttype_code = @accounttype_code" & _
                      ", accounttype_name = @accounttype_name" & _
                      ", description = @description" & _
                      ", isactive = @isactive" & _
                      ", accounttype_name1 = @accounttype_name1" & _
                      ", accounttype_name2 = @accounttype_name2 " & _
                    "WHERE accounttypeunkid = @accounttypeunkid " 'Sohail (28 Dec 2010)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfbankacctype_master", mintAccounttypeunkid, "accounttypeunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfbankacctype_master", "accounttypeunkid", mintAccounttypeunkid, True) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmsConfiguration..cfbankacctype_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (12 Oct 2011) -- Start
            'strQ = "DELETE FROM hrmsConfiguration..cfbankacctype_master " & _
            '        "WHERE accounttypeunkid = @accounttypeunkid " 'Sohail (28 Dec 2010)
            strQ = "UPDATE hrmsConfiguration..cfbankacctype_master SET isactive = 0 " & _
                                "WHERE accounttypeunkid = @accounttypeunkid "
            'Sohail (12 Oct 2011) -- End
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfbankacctype_master", "accounttypeunkid", intUnkid, True) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"
            strQ = "SELECT  accounttypeunkid " & _
                   "FROM    premployee_bank_tran " & _
                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
                           "AND accounttypeunkid = @accounttypeunkid " & _
                   "UNION " & _
                   "SELECT  accounttypeunkid " & _
                   "FROM    prbankedi_master " & _
                   "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND accounttypeunkid = @accounttypeunkid "
            'Sohail (19 Nov 2010) -- End

            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "  accounttypeunkid " & _
                     ", accounttype_alias " & _
                     ", accounttype_code " & _
                     ", accounttype_name " & _
                     ", description " & _
                     ", isactive " & _
                     ", accounttype_name1 " & _
                     ", accounttype_name2 " & _
                "FROM hrmsConfiguration..cfbankacctype_master " & _
                "WHERE 1=1 " 'Sohail (28 Dec 2010)



            If strCode.Length > 0 Then
                strQ &= "AND accounttype_code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If
            If strName.Length > 0 Then
                strQ &= "AND accounttype_name = @name"
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND accounttypeunkid <> @accounttypeunkid"
                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    ''' Purpose  : Get combo List for particular Bank Account Type
    ''' Modify By: Anjan
    ''' </summary>
    ''' <param name="strListName"> </param>
    ''' <returns>DataSet</returns>
    ''' <purpose> </purpose>

    Public Function getComboList(Optional ByVal blnNA As Boolean = False, Optional ByVal strListName As String = "List", Optional ByVal intLanguageID As Integer = -1) As DataSet
        Dim strQ As String = Nothing
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation = Nothing
        Try
            objDataOperation = New clsDataOperation
            '<To Do> uncomment when language is ready
            'If Not intLanguageID > 0 Then
            '    intLanguageID = User._Object._LanguageId
            'End If


            If blnNA Then
                strQ = "SELECT 0 as accounttypeunkid " & _
                        ", '' + @SELECT AS name " & _
                       " UNION "
                objDataOperation.AddParameter("@SELECT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If


            strQ &= "SELECT accounttypeunkid " & _
                        ", CASE @Language WHEN 1 THEN accounttype_name1 " & _
                                    "WHEN 2 THEN accounttype_name2 " & _
                                    "ELSE accounttype_name END AS name " & _
                     "FROM hrmsConfiguration..cfbankacctype_master " & _
                     "WHERE isactive = 1 " & _
                     "ORDER BY  accounttypeunkid " 'Sohail (28 Dec 2010) 'Anjan (04 Mar 2011)- Issue: Select was coming below account type name. so order by accountid
            objDataOperation.AddParameter("@Language", SqlDbType.Int, eZeeDataType.INT_SIZE, intLanguageID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing

        End Try
    End Function

    'Sandeep [ 25 APRIL 2011 ] -- Start
    Public Function GetBankAccType(ByVal StrAccName As String) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                       "  accounttypeunkid " & _
                        "FROM hrmsConfiguration..cfbankacctype_master " & _
                        "WHERE accounttype_name = @name AND isactive = 1 "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrAccName)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("accounttypeunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetBankAccType", mstrModuleName)
        End Try
    End Function
    'Sandeep [ 25 APRIL 2011 ] -- End 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Bank Account Type Code is already defined. Please define new Bank Account Type Code.")
			Language.setMessage(mstrModuleName, 2, "This Bank Account Type Name is already defined. Please define new Bank Account Type Name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "This Account Type Code is already defined. Please define new Account Type Code.")
			Language.setMessage(mstrModuleName, 5, "This Account Type Name is already defined. Please define new Account Type Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class