﻿'************************************************************************************************************************************
'Class Name : clsGRMemail_master.vb
'Purpose    :
'Date       : 21 Aug 2010
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.Text
Imports System.IO
Imports System.Windows.Documents

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmail_Master
    Private ReadOnly mstrModuleName As String = "clsEmail_Master"

#Region " Private variables "
    Private mintEmailmasterUnkId As Integer = -1
    Private mdtEmailDateTime As Date = Nothing
    Private mintLettertypeUnkId As Integer = -1
    Private mstrSubject As String = ""
    Private mstrMessage As String = ""
    Private mblnIscampaign As Boolean = False
    Private mchrIstype_of As Char = ""
    Private mstrAttachmentpath As String = ""
    Private mintUserUnkId As Integer = -1
    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsapplicant As Boolean = False
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdicDisciplineFiles As New Dictionary(Of String, Integer)
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get or Set EmailmasterUnkId
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _EmailmasterUnkId() As Integer
        Get
            Return mintEmailmasterUnkId
        End Get
        Set(ByVal value As Integer)
            mintEmailmasterUnkId = value
            getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmailDateTime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _EmailDateTime() As Date
        Get
            Return mdtEmailDateTime
        End Get
        Set(ByVal value As Date)
            mdtEmailDateTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LettertypeUnkId
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LettertypeUnkId() As Integer
        Get
            Return mintLettertypeUnkId
        End Get
        Set(ByVal value As Integer)
            mintLettertypeUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Subject
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Subject() As String
        Get
            Return mstrSubject
        End Get
        Set(ByVal value As String)
            mstrSubject = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Message
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Iscampaign
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscampaign() As Boolean
        Get
            Return mblnIscampaign
        End Get
        Set(ByVal value As Boolean)
            mblnIscampaign = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Istype_of
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Istype_of() As Char
        Get
            Return mchrIstype_of
        End Get
        Set(ByVal value As Char)
            mchrIstype_of = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Attachmentpath
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Attachmentpath() As String
        Get
            Return mstrAttachmentpath
        End Get
        Set(ByVal value As String)
            mstrAttachmentpath = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set UserUnkId
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _UserUnkId() As Integer
        Get
            Return mintUserUnkId
        End Get
        Set(ByVal value As Integer)
            mintUserUnkId = value
        End Set
    End Property

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set isapplicant
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isapplicant() As Boolean
        Get
            Return mblnIsapplicant
        End Get
        Set(ByVal value As Boolean)
            mblnIsapplicant = value
        End Set
    End Property

    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _dicDisciplineFiles() As Dictionary(Of String, Integer)
        Set(ByVal value As Dictionary(Of String, Integer))
            mdicDisciplineFiles = value
        End Set
    End Property
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub getData()
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '           "  emailmasterunkid " & _
            '           ", emaildatetime " & _
            '           ", lettertypeunkid " & _
            '           ", subject " & _
            '           ", message " & _
            '           ", istype_of " & _
            '           ", attachmentpath " & _
            '           ", userunkid " & _
            '       "FROM hremail_master " & _
            '       "WHERE emailmasterunkid = @emailmasterunkid "
            strQ = "SELECT " & _
                       "  emailmasterunkid " & _
                       ", emaildatetime " & _
                       ", lettertypeunkid " & _
                       ", subject " & _
                       ", message " & _
                       ", istype_of " & _
                       ", attachmentpath " & _
                       ", userunkid " & _
                       ", ISNULL(isapplicant,0) AS isapplicant " & _
                   "FROM hremail_master " & _
                   "WHERE emailmasterunkid = @emailmasterunkid "
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            objDataOperation.AddParameter("@emailmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailmasterUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmailmasterUnkId = dtRow.Item("emailmasterunkid")
                mdtEmailDateTime = dtRow.Item("emaildatetime")
                mintLettertypeUnkId = dtRow.Item("lettertypeunkid")
                mstrSubject = dtRow.Item("subject")
                mstrMessage = dtRow.Item("message")
                mblnIscampaign = dtRow.Item("iscampaign")
                mchrIstype_of = dtRow.Item("istype_of")
                mstrAttachmentpath = dtRow.Item("attachmentpath")
                mintUserUnkId = dtRow.Item("userunkid")
                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mblnIsapplicant = CBool(dtRow.Item("isapplicant"))
                'S.SANDEEP [ 07 NOV 2011 ] -- END
                Exit For
            Next
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Get_GRMemail_master", mstrModuleName)
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremail_master) </purpose>
    Public Function Insert(ByVal mstrReceipientId As String) As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@emaildatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmailDateTime)
            objDataOperation.AddParameter("@lettertypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettertypeUnkId.ToString)
            objDataOperation.AddParameter("@subject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSubject.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@message", SqlDbType.NText, 4000, mstrMessage.ToString)
            objDataOperation.AddParameter("@message", SqlDbType.VarChar, 8000, mstrMessage.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- END
            objDataOperation.AddParameter("@istype_of", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIstype_of.ToString)
            objDataOperation.AddParameter("@attachmentpath", SqlDbType.NVarChar, 2000, mstrAttachmentpath.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserUnkId.ToString)

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)

            'strQ = "INSERT INTO hremail_master ( " & _
            '           "  emaildatetime " & _
            '           ", lettertypeunkid " & _
            '           ", subject " & _
            '           ", message " & _
            '           ", istype_of " & _
            '           ", attachmentpath " & _
            '           ", userunkid " & _
            '       ") VALUES (" & _
            '           "  @emaildatetime " & _
            '           ", @lettertypeunkid " & _
            '           ", @subject " & _
            '           ", @message " & _
            '           ", @istype_of " & _
            '           ", @attachmentpath " & _
            '           ", @userunkid " & _
            '       "); SELECT @@identity "

            strQ = "INSERT INTO hremail_master ( " & _
                       "  emaildatetime " & _
                       ", lettertypeunkid " & _
                       ", subject " & _
                       ", message " & _
                       ", istype_of " & _
                       ", attachmentpath " & _
                       ", userunkid " & _
                       ", isapplicant" & _
                   ") VALUES (" & _
                       "  @emaildatetime " & _
                       ", @lettertypeunkid " & _
                       ", @subject " & _
                       ", @message " & _
                       ", @istype_of " & _
                       ", @attachmentpath " & _
                       ", @userunkid " & _
                       ", @isapplicant" & _
                   "); SELECT @@identity "
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmailmasterUnkId = dsList.Tables(0).Rows(0).Item(0)
            Dim strUnkidData As String() = mstrReceipientId.ToString.Split(",")
            Dim i As Integer = 0
            Dim dicIsAdded As New Dictionary(Of Integer, Integer)
            For Each iReceipientId As String In strUnkidData
                strQ = "INSERT INTO hremail_tran ( " & _
                                           "  emailmasterunkid " & _
                                           ", employeeunkid " & _
                                           ", isread " & _
                                           ", isdelete " & _
                                        ") VALUES (" & _
                                           "  @EmailmasterUnkId " & _
                                           ", @employeeunkid " & _
                                           ", 0 " & _
                                           ", 0 " & _
                                           ") " & _
                                        ";SELECT @@identity "

                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@EmailmasterUnkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailmasterUnkId.ToString)
                'Sandeep [ 25 APRIL 2011 ] -- Start
                'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iReceipientId.ToString)
                If iReceipientId.Trim.Length > 0 Then
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iReceipientId.ToString.Trim)
                Else
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                End If
                'Sandeep [ 25 APRIL 2011 ] -- End 


                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objDataOperation.ExecNonQuery(strQ)

                Dim dsTran As New DataSet
                Dim intEmailTranId As Integer = 0
                dsTran = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                intEmailTranId = dsTran.Tables(0).Rows(0)(0)
                If mdicDisciplineFiles.Keys.Count > 0 Then
                    For Each SKey As String In mdicDisciplineFiles.Keys
                        If SKey.Split("|")(0).Trim = iReceipientId.Trim Then
                            If dicIsAdded.ContainsKey(SKey.Split("|")(1)) = True Then
                                Continue For
                            Else
                                dicIsAdded.Add(CInt(SKey.Split("|")(1)), CInt(SKey.Split("|")(1)))
                                Dim objDisciplineFile As New clsDiscipline_File

                                objDisciplineFile._Disciplinefileunkid = CInt(CInt(SKey.Split("|")(1)))
                                objDisciplineFile._Emailtranunkid = intEmailTranId
                                objDisciplineFile._Emaildate = mdtEmailDateTime
                                If objDisciplineFile.Update() = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                Exit For
                            End If
                        End If
                    Next
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END


                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show(-1, ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
            objDataOperation.ReleaseTransaction(False)
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intEmailTranUnkid As Integer, _
                                ByVal intEmailMasterUnkid As Integer, _
                            Optional ByVal pblnDirectDelete As Boolean = False) As Boolean

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnAllDeleted As Boolean = False
        Dim objDataOperation As New clsDataOperation

        Try

            objDataOperation.AddParameter("@messageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmailMasterUnkid.ToString)
            objDataOperation.AddParameter("@messagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmailTranUnkid.ToString)

            If pblnDirectDelete = True Then
                strQ = "SELECT isdelete " & _
                       "FROM hremail_tran " & _
                       "WHERE hremail_tran.emailmasterunkid = @messageunkid " & _
                       "ORDER BY isdelete DESC "
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                strQ = ""
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    If dtRow.Item("isdelete") = True Then
                        blnAllDeleted = True
                        Continue For
                    Else
                        blnAllDeleted = False
                        Continue For
                    End If
                Next

                If blnAllDeleted = True Then
                    strQ = "UPDATE hremail_master SET " & _
                                "isfinaldelete = 1 " & _
                           "WHERE emailmasterunkid = @messageunkid; "
                End If


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'strQ &= "DELETE FROM hremail_tran " & _
                '        "WHERE emailtranunkid = @messagetranunkid "
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Else
                strQ = "UPDATE hremail_tran SET " & _
                            "isdelete=1 " & _
                       "WHERE emailtranunkid = @messagetranunkid "
            End If

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Delete", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function MarkAsReaded(ByVal pintMessageTranUnkid As Integer) As Boolean

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.AddParameter("@messagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, pintMessageTranUnkid)

            strQ = "UPDATE hremail_tran SET " & _
                          "isread = 1 " & _
                   "WHERE emailtranunkid = @messagetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "MarkAsReaded", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetList(ByVal intUserId As Integer, _
                            ByVal intMessageType As Integer, _
                            Optional ByVal StrListName As String = "List", _
                            Optional ByVal blnShowUnreadOnly As Boolean = False) As DataTable
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '                        "	 hremail_master.emailmasterunkid " & _
            '                        "	,hremail_tran.emailtranunkid " & _
            '                        "	,hremail_master.emaildatetime " & _
            '                        "	,hremail_master.lettertypeunkid " & _
            '                        "	,ISNULL(hremail_master.subject,'') AS subject " & _
            '                        "	,hremail_master.message " & _
            '                        "	,hremail_master.istype_of " & _
            '                        "	,hremail_master.attachmentpath " & _
            '                        "	,hremail_tran.employeeunkid " & _
            '                        "	,hremail_tran.isread " & _
            '                        "	,hremail_tran.isdelete " & _
            '                        "	,hrmsConfiguration..cfuser_master.username AS Sender "
            'If ConfigParameter._Object._FirstNamethenSurname Then
            '    strQ &= " ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Recipient "
            'Else
            '    strQ &= " ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'') AS Recipient "
            'End If
            'strQ &= " FROM hremail_tran " & _
            '             "	    LEFT JOIN hremployee_master ON hremail_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '             "	    LEFT JOIN hremail_master ON hremail_tran.emailmasterunkid = hremail_master.emailmasterunkid " & _
            '             "	    LEFT JOIN hrmsConfiguration..cfuser_master ON hremail_master.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '             "WHERE hremail_master.userunkid = @UserId "
            strQ &= "SELECT " & _
                    " emailmasterunkid " & _
                    ",emailtranunkid " & _
                    ",emaildatetime " & _
                    ",lettertypeunkid " & _
                    ",subject " & _
                    ",message " & _
                    ",istype_of " & _
                    ",attachmentpath " & _
                    ",employeeunkid " & _
                    ",isread " & _
                    ",isdelete " & _
                    ",Sender " & _
                    ",Recipient " & _
                    ",isapplicant " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                        "	 hremail_master.emailmasterunkid " & _
                        "	,hremail_tran.emailtranunkid " & _
                        "	,hremail_master.emaildatetime " & _
                        "	,hremail_master.lettertypeunkid " & _
                        "	,ISNULL(hremail_master.subject,'') AS subject " & _
                        "	,hremail_master.message " & _
                        "	,hremail_master.istype_of " & _
                        "	,hremail_master.attachmentpath " & _
                        "	,hremail_tran.employeeunkid " & _
                        "	,hremail_tran.isread " & _
                        "	,hremail_tran.isdelete " & _
                            ",hrmsConfiguration..cfuser_master.username AS Sender " & _
                            ",ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.surname, '') AS Recipient " & _
                            ",hremail_master.isapplicant " & _
                         "FROM hremail_tran " & _
                              "LEFT JOIN hremployee_master ON hremail_tran.employeeunkid = hremployee_master.employeeunkid " & _
                              "LEFT JOIN hremail_master ON hremail_tran.emailmasterunkid = hremail_master.emailmasterunkid " & _
                              "LEFT JOIN hrmsConfiguration..cfuser_master ON hremail_master.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                         "WHERE hremail_master.userunkid = @UserId AND hremail_master.isapplicant = 0 "
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If intMessageType = 2 Then
                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ &= " AND hremail_tran.isdelete = 1 "
                strQ &= " AND hremail_tran.isdelete = 1 AND ISNULL(hremail_master.isfinaldelete,0) = 0 "
                'S.SANDEEP [ 04 FEB 2012 ] -- END
            Else
                strQ &= " AND hremail_tran.isdelete = 0 "
            End If

            If blnShowUnreadOnly Then
                strQ &= "AND hremail_tran.isread = 0 "
            End If

            strQ &= "UNION ALL " & _
                         "SELECT " & _
                          " hremail_master.emailmasterunkid " & _
                          ",hremail_tran.emailtranunkid " & _
                          ",hremail_master.emaildatetime " & _
                          ",hremail_master.lettertypeunkid " & _
                          ",ISNULL(hremail_master.subject, '') AS subject " & _
                          ",hremail_master.message " & _
                          ",hremail_master.istype_of " & _
                          ",hremail_master.attachmentpath " & _
                          ",hremail_tran.employeeunkid " & _
                          ",hremail_tran.isread " & _
                          ",hremail_tran.isdelete " & _
                          ",hrmsConfiguration..cfuser_master.username AS Sender " & _
                          ",ISNULL(rcapplicant_master.firstname, '') + ' '+ ISNULL(rcapplicant_master.surname, '') AS Recipient " & _
                          ",hremail_master.isapplicant " & _
                         "FROM hremail_tran " & _
                         " LEFT JOIN rcapplicant_master ON hremail_tran.employeeunkid = rcapplicant_master.applicantunkid " & _
                         "	    LEFT JOIN hremail_master ON hremail_tran.emailmasterunkid = hremail_master.emailmasterunkid " & _
                         "	    LEFT JOIN hrmsConfiguration..cfuser_master ON hremail_master.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                         "WHERE hremail_master.userunkid = @UserId AND hremail_master.isapplicant = 1 "

            If intMessageType = 2 Then
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ &= " AND hremail_tran.isdelete = 1 "
                strQ &= " AND hremail_tran.isdelete = 1 AND ISNULL(hremail_master.isfinaldelete,0) = 0 "
                'S.SANDEEP [ 18 FEB 2012 ] -- END
            Else
                strQ &= " AND hremail_tran.isdelete = 0 "
            End If

            If blnShowUnreadOnly Then
                strQ &= "AND hremail_tran.isread = 0 "
            End If

            strQ &= ")AS A " & _
                    "WHERE 1 = 1 ORDER BY emaildatetime DESC "

            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

            dsList = objDataOperation.ExecQuery(strQ, StrListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(StrListName).Copy

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList = Nothing
        End Try
    End Function

    Public Function GetRecipient(ByVal intEmailMasterId As Integer, Optional ByVal blnOnlyId As Boolean = False, Optional ByVal blnIsApplicant As Boolean = False) As String
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim strReturn As String = ""
        Try


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '                        "	 hremail_tran.emailmasterunkid " & _
            '                        "	,hremail_tran.emailtranunkid " & _
            '                        "	,hremail_tran.employeeunkid "
            'If ConfigParameter._Object._FirstNamethenSurname Then
            '    strQ &= "  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Recipient  "
            'Else
            '    strQ &= "  ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'') AS Recipient  "
            'End If
            'strQ &= " FROM hremail_tran " & _
            '             "	    LEFT JOIN hremployee_master ON hremail_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '             " WHERE hremail_tran.emailtranunkid = @emailTranunkid "

            strQ = "SELECT " & _
                        "	 hremail_tran.emailmasterunkid " & _
                        "	,hremail_tran.emailtranunkid " & _
                        "	,hremail_tran.employeeunkid "
            If ConfigParameter._Object._FirstNamethenSurname Then
                If blnIsApplicant = False Then
                    strQ &= "  ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Recipient  "
                Else
                    strQ &= "  ,ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.surname,'') AS Recipient  "
                End If
            Else
                If blnIsApplicant = False Then
                    strQ &= "  ,ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'') AS Recipient  "
                Else
                    strQ &= "  ,ISNULL(rcapplicant_master.surname,'')+' '+ISNULL(rcapplicant_master.firstname,'') AS Recipient  "
                End If
            End If
            strQ &= " FROM hremail_tran "
            If blnIsApplicant = False Then
                strQ &= " LEFT JOIN hremployee_master ON hremail_tran.employeeunkid = hremployee_master.employeeunkid "
            Else
                strQ &= " LEFT JOIN rcapplicant_master ON hremail_tran.employeeunkid = rcapplicant_master.applicantunkid "
            End If
            strQ &= " WHERE hremail_tran.emailtranunkid = @emailTranunkid "
            'S.SANDEEP [ 07 NOV 2011 ] -- END



            objDataOperation.AddParameter("@emailTranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmailMasterId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If blnOnlyId = True Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strReturn &= ", " & dtRow.Item("employeeunkid")
                Next
            Else
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strReturn &= ", " & dtRow.Item("Recipient")
                Next

            End If

            If strReturn.Length > 2 Then
                Return strReturn.Substring(2)
            Else
                Return strReturn
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetRecipient", mstrModuleName)
            Return strReturn
        End Try
    End Function

    '#Region " Conversion RTF TO HTML "

    '    Private Structure CodeList
    '        Dim Code As String
    '        Dim Status As String 'P=Pending;A=Active;G=Paragraph;D=Dead;K=Killed
    '        '"Dead" means the code is active but will be killed at next text
    '        '"Pending" means it's waiting for text - if the code is canceled before text appears it will be killed
    '        '"Active" means there is text using the code at this moment
    '        '"Paragraph" means that the code stays active until the next paragraph: "/pard" or "/pntext"
    '    End Structure

    '    Public strCurPhrase As String
    '    Dim strHTML As String
    '    Dim Codes() As CodeList
    '    Dim CodesBeg() As CodeList 'beginning codes
    '    Public NextCodes() As String
    '    Public NextCodesBeg() As String 'beginning codes for next text
    '    Dim CodesTmp() As String 'temp stack for copying
    '    Dim CodesTmpBeg() As String 'temp stack for copying beg

    '    Public strCR As String 'string to use for CRs - blank if +CR not chosen in options
    '    Dim strBeforeText As String
    '    Dim strBeforeText2 As String
    '    Dim strBeforeText3 As String
    '    Dim gPlain As Boolean 'true if all codes shouls be popped before next text
    '    Dim gWBPlain As Boolean 'plain will be true after next text
    '    Dim strColorTable() As String 'table of colors
    '    Dim lColors As Integer '# of colors
    '    Dim strEOL As String 'string to include before <br>
    '    Dim strBOL As String 'string to include after <br>
    '    Dim lSkipWords As Integer 'number od words to skip from current
    '    Dim gBOL As Boolean 'a <br> was inserted but no non-whitespace text has been inserted
    '    Dim gPar As Boolean 'true if paragraph was reached since last text
    '    Dim lBrLev As Integer 'bracket level when finding matching brackets
    '    Dim strSecTmp As String 'temporary section buffer
    '    Dim gIgnorePard As Boolean 'should pard end list items or not?

    '    Dim strFontTable() As String 'table of fonts
    '    Dim lFonts As Integer '# of fonts
    '    Dim strFont As String
    '    Dim strTable As String
    '    Dim strFace As String 'current font face for setting up fontstring
    '    Dim strFontColor As String 'current font color for setting up fontstring
    '    Dim strFontSize As String 'current font size for setting up fontstring
    '    Dim lFontSize As Integer
    '    Dim iDefFontSize As Short 'default font size
    '    Dim gUseFontFace As Boolean 'use different fonts or always use default font

    '    Public gDebug As Boolean 'for debugging
    '    Public gStep As Boolean 'for debugging

    '    Private Sub ClearCodes()
    '        ReDim Codes(0)
    '        ReDim CodesBeg(0)
    '        ClearNext()
    '    End Sub

    '    Private Sub ClearNext(Optional ByRef strExcept As String = "")
    '        If strExcept.Length > 0 Then
    '            If InNext(strExcept) Then
    '                While NextCodes(1) <> strExcept
    '                    ShiftNext()
    '                    ShiftNextBeg()
    '                End While
    '                Exit Sub
    '            End If
    '        End If

    '        ReDim NextCodes(0)
    '        ReDim NextCodesBeg(0)


    '    End Sub

    '    Private Sub ClearFont()
    '        strFont = ""
    '        strTable = ""
    '        strFontColor = ""
    '        strFace = ""
    '        strFontSize = ""
    '        lFontSize = 0
    '    End Sub

    '    Private Sub Codes2NextTill(ByRef strCode As String)
    '        For l As Integer = 1 To Codes.GetUpperBound(0)
    '            If Codes(l).Code = strCode Then Exit For
    '            If Codes(l).Status <> "K" And Codes(l).Status <> "D" Then
    '                If Not InNext(strCode) Then
    '                    UnShiftNext(Codes(l).Code)
    '                    UnShiftNextBeg(CodesBeg(l).Code)
    '                End If
    '            End If
    '        Next l
    '    End Sub

    '    Private Sub GetColorTable(ByRef strSecTmp As String, ByRef strColorTable() As String)
    '        'get color table data and fill in strColorTable array
    '        Dim lEOS As Integer
    '        Dim strTmp As String = ""

    '        Dim lBOS As Integer = (strSecTmp.IndexOf("\colortbl") + 1)
    '        ReDim strColorTable(0)
    '        Dim lColors As Integer = 1
    '        '(CType("X", IFormatProvider)).Trim()
    '        If lBOS <> 0 Then
    '            lBOS = Strings.InStr(lBOS, strSecTmp, ";")
    '            lEOS = Strings.InStr(lBOS, strSecTmp, ";}")
    '            If lEOS <> 0 Then
    '                lBOS = Strings.InStr(lBOS, strSecTmp, "\red")
    '                While ((lBOS <= lEOS) And (lBOS <> 0))
    '                    ReDim Preserve strColorTable(lColors)
    '                    strTmp = CInt(strSecTmp.Substring(lBOS + 3, 1) & CStr((IIf(IsNumeric(strSecTmp.Substring(lBOS + 4, 1)), strSecTmp.Substring(lBOS + 4, 1), ""))) & (CStr(IIf(IsNumeric(strSecTmp.Substring(lBOS + 5, 1)), strSecTmp.Substring(lBOS + 5, 1), "")))).ToString("X").Trim()
    '                    If strTmp.Length = 1 Then strTmp = "0" & strTmp
    '                    strColorTable(lColors) = strColorTable(lColors) & strTmp
    '                    lBOS = Strings.InStr(lBOS, strSecTmp, "\green")
    '                    strTmp = CInt(strSecTmp.Substring(lBOS + 5, 1) & CStr((IIf(IsNumeric(strSecTmp.Substring(lBOS + 6, 1)), strSecTmp.Substring(lBOS + 6, 1), ""))) & (CStr(IIf(IsNumeric(strSecTmp.Substring(lBOS + 7, 1)), strSecTmp.Substring(lBOS + 7, 1), "")))).ToString("X").Trim()
    '                    If strTmp.Length = 1 Then strTmp = "0" & strTmp
    '                    strColorTable(lColors) = strColorTable(lColors) & strTmp
    '                    lBOS = Strings.InStr(lBOS, strSecTmp, "\blue")
    '                    strTmp = CInt(strSecTmp.Substring(lBOS + 4, 1) & CStr((IIf(IsNumeric(strSecTmp.Substring(lBOS + 5, 1)), strSecTmp.Substring(lBOS + 5, 1), ""))) & (CStr(IIf(IsNumeric(strSecTmp.Substring(lBOS + 6, 1)), strSecTmp.Substring(lBOS + 6, 1), "")))).ToString("X").Trim()
    '                    If strTmp.Length = 1 Then strTmp = "0" & strTmp
    '                    strColorTable(lColors) = strColorTable(lColors) & strTmp
    '                    lBOS = Strings.InStr(lBOS, strSecTmp, "\red")
    '                    lColors += 1
    '                End While
    '            End If
    '        End If
    '    End Sub

    '    Private Sub GetFontTable(ByRef strSecTmp As String, ByRef strFontTable() As String)
    '        'get font table data and fill in strFontTable array
    '        Dim lEOS As Integer
    '        Dim strTmp As String = ""
    '        Dim lLvl As Integer
    '        Dim strNextChar As String = ""

    '        Dim lBOS As Integer = (strSecTmp.IndexOf("\fonttbl") + 1)
    '        ReDim strFontTable(0)
    '        Dim lFonts As Integer = 0
    '        If lBOS <> 0 Then
    '            lEOS = Strings.InStr(lBOS, strSecTmp, ";}}")
    '            If lEOS <> 0 Then
    '                lBOS = Strings.InStr(lBOS, strSecTmp, "\f0")
    '                While ((lBOS <= lEOS) And (lBOS <> 0))
    '                    ReDim Preserve strFontTable(lFonts)
    '                    strNextChar = strSecTmp.Substring(lBOS - 1, 1)
    '                    While (((strNextChar <> " ") And (lBOS <= lEOS)) Or (lLvl > 0))
    '                        lBOS += 1
    '                        If strNextChar = "{" Then
    '                            lLvl += 1
    '                            strNextChar = strSecTmp.Substring(lBOS - 1, 1)
    '                        ElseIf strNextChar = "}" Then
    '                            lLvl -= 1
    '                            If lLvl = 0 Then
    '                                strNextChar = " "
    '                                lBOS -= 1
    '                            Else
    '                                strNextChar = strSecTmp.Substring(lBOS - 1, 1)
    '                            End If
    '                        Else
    '                            strNextChar = strSecTmp.Substring(lBOS - 1, 1)
    '                        End If
    '                    End While
    '                    lBOS += 1
    '                    strTmp = strSecTmp.Substring(lBOS - 1, Math.Min(strSecTmp.Length, Strings.InStr(lBOS, strSecTmp, ";") - lBOS))
    '                    strFontTable(lFonts) = strFontTable(lFonts) & strTmp
    '                    lBOS = Strings.InStr(lBOS, strSecTmp, "\f" & (CStr(lFonts + 1)))
    '                    lFonts += 1
    '                End While
    '            End If
    '        End If
    '    End Sub

    '    Function InNext(ByRef strTmp As String) As Boolean

    '        Dim l As Integer = 1
    '        Dim gTmp As Boolean = False
    '        While l <= NextCodes.GetUpperBound(0) And Not gTmp
    '            If NextCodes(l) = strTmp Then gTmp = True
    '            l += 1
    '        End While
    '        Return gTmp
    '    End Function

    '    Function InNextBeg(ByRef strTmp As String) As Boolean

    '        Dim l As Integer = 1
    '        Dim gTmp As Boolean = False
    '        While l <= NextCodesBeg.GetUpperBound(0) And Not gTmp
    '            If NextCodesBeg(l) = strTmp Then gTmp = True
    '            l += 1
    '        End While
    '        Return gTmp
    '    End Function

    '    Function InCodes(ByRef strTmp As String, Optional ByRef gActiveOnly As Boolean = False) As Boolean

    '        Dim l As Integer = 1
    '        Dim gTmp As Boolean = False
    '        While l <= Codes.GetUpperBound(0) And Not gTmp
    '            If gActiveOnly Then
    '                If Codes(l).Code = strTmp And (Codes(l).Status = "A" Or Codes(l).Status = "G") Then gTmp = True
    '            Else
    '                If Codes(l).Code = strTmp Then gTmp = True
    '            End If
    '            l += 1
    '        End While
    '        Return gTmp
    '    End Function

    '    Function InCodesBeg(ByRef strTmp As String) As Boolean

    '        Dim l As Integer = 1
    '        Dim gTmp As Boolean = False
    '        While l <= CodesBeg.GetUpperBound(0) And Not gTmp
    '            If CodesBeg(l).Code = strTmp Then gTmp = True
    '            l += 1
    '        End While
    '        Return gTmp
    '    End Function

    '    Function NabNextLine(ByRef strRTF As String) As String

    '        Dim result As String = String.Empty
    '        Dim l As Integer = (strRTF.IndexOf(Strings.Chr(13) & Strings.Chr(10)) + 1)
    '        If l = 0 Then l = strRTF.Length
    '        result = TrimAll(strRTF.Substring(0, Math.Min(strRTF.Length, l)))
    '        If l = strRTF.Length Then
    '            strRTF = ""
    '        Else
    '            strRTF = TrimAll(strRTF.Substring(l - 1))
    '        End If
    '        Return result
    '    End Function

    '    Function NabNextWord(ByRef strLine As String) As String
    '        Dim result As String = String.Empty
    '        Dim gEndofWord As Boolean
    '        Dim lTmp As Integer = 0

    '        Dim gInCommand As Boolean = False 'current word is command instead of plain word
    '        Dim l As Integer = 0
    '        Dim lvl As Integer = 0
    '        'strLine = TrimifCmd(strLine)
    '        If strLine.StartsWith("}") Then
    '            strLine = strLine.Substring(1)
    '            result = "}"
    '        Else
    '            If strLine.StartsWith("{") Then
    '                strLine = strLine.Substring(1)
    '                result = "{"
    '            Else
    '                If strLine.StartsWith("\'") Then
    '                    result = strLine.Substring(0, Math.Min(strLine.Length, 4))
    '                    strLine = strLine.Substring(4)
    '                Else
    '                    If strLine.StartsWith("\\") Or strLine.StartsWith("\{") Or strLine.StartsWith("\}") Then
    '                        result = strLine.Substring(0, Math.Min(strLine.Length, 2))
    '                        strLine = strLine.Substring(2)
    '                    Else
    '                        While Not gEndofWord
    '                            l += 1
    '                            If l >= strLine.Length Then
    '                                If l = strLine.Length Then l += 1
    '                                gEndofWord = True
    '                            ElseIf CBool((("\{}").IndexOf(strLine.Substring(l - 1, 1)) + 1)) Then
    '                                If l = 1 And strLine.Substring(l - 1, 1) = "\" Then gInCommand = True
    '                                '            If Mid(strLine, l + 1, 1) <> "\" And l > 1 And lvl = 0 Then    'avoid...what?
    '                                If l > 1 And lvl = 0 Then
    '                                    gEndofWord = True
    '                                End If
    '                            ElseIf strLine.Substring(l - 1, 1) = " " And lvl = 0 And gInCommand Then
    '                                gEndofWord = True
    '                            End If
    '                        End While

    '                        If l = 0 Then l = strLine.Length
    '                        result = strLine.Substring(0, Math.Min(strLine.Length, l - 1))
    '                        While CBool(CInt(result.Length > 0) And (("{}").IndexOf(result.Substring(result.Length - Math.Min(result.Length, 1))) + 1) And CInt(l > 0))
    '                            result = result.Substring(0, Math.Min(result.Length, Strings.Len(result) - 1))
    '                            l -= 1
    '                        End While
    '                        strLine = strLine.Substring(l - 1)
    '                        If strLine.StartsWith(" ") Then strLine = strLine.Substring(1)
    '                    End If
    '                End If
    '            End If
    '        End If

    '        Return result
    '    End Function

    '    Function NabSection(ByRef strRTF As String, ByRef lPos As Integer) As String
    '        'grab section surrounding lPos, strip section out of strRTF and return it

    '        Dim result As String = String.Empty
    '        Dim lRTFLen As Integer = strRTF.Length

    '        Dim lBOS As Integer = lPos 'beginning of section
    '        Dim strChar As String = strRTF.Substring(lBOS - 1, 1)
    '        Dim lLev As Integer = 1 'level of brackets/parens
    '        While lLev > 0
    '            lBOS -= 1
    '            If lBOS <= 0 Then
    '                lLev -= 1
    '            Else
    '                strChar = strRTF.Substring(lBOS - 1, 1)
    '                If strChar = "}" Then
    '                    lLev += 1
    '                ElseIf strChar = "{" Then
    '                    lLev -= 1
    '                End If
    '            End If
    '        End While
    '        lBOS -= 1
    '        If lBOS < 1 Then lBOS = 1

    '        Dim lEOS As Integer = lPos 'ending of section
    '        strChar = strRTF.Substring(lEOS - 1, 1)
    '        lLev = 1
    '        While lLev > 0
    '            lEOS += 1
    '            If lEOS >= lRTFLen Then
    '                lLev -= 1
    '            Else
    '                strChar = strRTF.Substring(lEOS - 1, 1)
    '                If strChar = "{" Then
    '                    lLev += 1
    '                ElseIf strChar = "}" Then
    '                    lLev -= 1
    '                End If
    '            End If
    '        End While
    '        lEOS += 1
    '        If lEOS > lRTFLen Then lEOS = lRTFLen
    '        result = strRTF.Substring(lBOS, Math.Min(strRTF.Length, lEOS - lBOS - 1))
    '        strRTF = strRTF.Substring(0, Math.Min(strRTF.Length, lBOS)) & strRTF.Substring(lEOS - 1)
    '        strRTF = rtf2html_replace(strRTF, Strings.Chr(13) & Strings.Chr(10) & Strings.Chr(13) & Strings.Chr(10), Strings.Chr(13) & Strings.Chr(10))
    '        Return result
    '    End Function

    '    Private Sub Next2Codes()
    '        'move codes from pending ("next") stack to front of current stack
    '        Dim lNumCodes, lNumNext As Integer

    '        If NextCodes.GetUpperBound(0) > 0 Then
    '            If InNext("</li>") Then
    '                For l As Integer = NextCodes.GetUpperBound(0) To 1 Step -1
    '                    If NextCodes(l) = "</li>" And l > 1 Then
    '                        NextCodes(l) = NextCodes(l - 1)
    '                        NextCodesBeg(l) = NextCodesBeg(l - 1)
    '                        NextCodes(l - 1) = "</li>"
    '                        NextCodesBeg(l - 1) = "<li>"
    '                    End If
    '                Next l
    '            End If

    '            lNumCodes = Codes.GetUpperBound(0)
    '            lNumNext = NextCodes.GetUpperBound(0)
    '            ReDim Preserve Codes(lNumCodes + lNumNext)
    '            ReDim Preserve CodesBeg(lNumCodes + lNumNext)
    '            For l As Integer = Codes.GetUpperBound(0) To 1 Step -1
    '                If l > lNumNext Then
    '                    Codes(l) = Codes(l - lNumNext)
    '                    CodesBeg(l) = CodesBeg(l - lNumNext)
    '                Else
    '                    Codes(l).Code = NextCodes(lNumNext - l + 1)
    '                    CodesBeg(l).Code = NextCodesBeg(lNumNext - l + 1)
    '                    Select Case Codes(l).Code
    '                        Case "</td></tr></table>", "</li>"
    '                            Codes(l).Status = "PG"
    '                            CodesBeg(l).Status = "PG"
    '                        Case Else
    '                            Codes(l).Status = "P"
    '                            CodesBeg(l).Status = "P"
    '                    End Select
    '                End If
    '            Next l
    '            ReDim NextCodes(0)
    '            ReDim NextCodesBeg(0)
    '        End If
    '    End Sub

    '    Private Sub Codes2Next()
    '        'move codes from "current" stack to pending ("next") stack
    '        Dim lNumCodes As Integer

    '        If Codes.GetUpperBound(0) > 0 Then
    '            lNumCodes = NextCodes.GetUpperBound(0)
    '            ReDim Preserve NextCodes(lNumCodes + Codes.GetUpperBound(0))
    '            ReDim Preserve NextCodesBeg(lNumCodes + Codes.GetUpperBound(0))
    '            For l As Integer = 1 To Codes.GetUpperBound(0)
    '                NextCodes(lNumCodes + l) = Codes(l).Code
    '                NextCodesBeg(lNumCodes + l) = CodesBeg(l).Code
    '            Next l
    '            ReDim Codes(0)
    '            ReDim CodesBeg(0)
    '        End If
    '    End Sub

    '    Function ParseFont(ByRef strColor As String, ByRef strSize As String, ByRef strFace As String) As String
    '        Dim strTmpFont As String = ""

    '        If strColor & strSize & strFace = "" Then
    '            strTmpFont = ""
    '        Else
    '            strTmpFont = "<font"
    '            If strFace <> "" Then
    '                strTmpFont = strTmpFont & " face=""" & strFace & """"
    '            End If
    '            If strColor <> "" Then
    '                strTmpFont = strTmpFont & " color=""" & strColor & """"
    '            End If
    '            If strSize <> "" And Conversion.Val(strSize) <> iDefFontSize Then
    '                strTmpFont = strTmpFont & " size=" & strSize
    '            End If
    '            strTmpFont = strTmpFont & ">"
    '        End If
    '        Return strTmpFont
    '    End Function

    '    Function PopCode() As String
    '        Dim result As String = String.Empty
    '        If Codes.GetUpperBound(0) > 0 Then
    '            result = Codes(Codes.GetUpperBound(0)).Code
    '            ReDim Preserve Codes(Codes.GetUpperBound(0) - 1)
    '        End If
    '        Return result
    '    End Function

    '    Function ProcessAfterTextCodes() As String
    '        Dim lLastKilled, lRetVal As Integer

    '        'check for/handle font change
    '        If strFont <> GetLastFont() Then
    '            KillCode("</font>")
    '            If strFont.Length > 0 Then
    '                lRetVal = ReplaceInNextBeg("</font>", strFont)
    '                If lRetVal = 0 Then
    '                    PushNext("</font>")
    '                    PushNextBeg(strFont)
    '                End If
    '            End If
    '        Else
    '            If Not InNext("</li>") Then ReviveCode("</font>")
    '        End If

    '        'now handle everything killed and move codes farther in to next
    '        '    ie: \b B\i B \u B\i0 B \u0\b0 => <b>B<i>B<u>B</u>B</i><u>B</u></b>
    '        Dim strTmp As New StringBuilder
    '        If Codes.GetUpperBound(0) > 0 Then
    '            lLastKilled = 0
    '            For l As Integer = Codes.GetUpperBound(0) To 1 Step -1
    '                If Codes(l).Status = "K" Then
    '                    lLastKilled = l
    '                    Exit For
    '                End If
    '            Next l
    '            If lLastKilled > 0 Then
    '                For l As Integer = 1 To lLastKilled
    '                    strTmp.Append(Codes(l).Code)
    '                    If Codes(l).Code = "</li>" Then strTmp.Append(strCR)
    '                Next l
    '                For l As Integer = lLastKilled To 1 Step -1
    '                    If Codes(l).Status <> "D" And Codes(l).Status <> "K" Then
    '                        If Not InNext(Codes(l).Code) Then
    '                            PushNext(Codes(l).Code)
    '                            PushNextBeg(CodesBeg(l).Code)
    '                        End If
    '                        Codes(l).Status = "K"
    '                        CodesBeg(l).Status = "K"
    '                    End If
    '                Next l
    '            End If
    '        End If
    '        Return strTmp.ToString()
    '    End Function

    '    Function GetActiveCodes() As String

    '        Dim strTmp As New StringBuilder
    '        If Codes.GetUpperBound(0) > 0 Then
    '            For l As Integer = 1 To Codes.GetUpperBound(0)
    '                strTmp.Append(Codes(l).Code)
    '            Next l
    '        End If
    '        Return strTmp.ToString()
    '    End Function

    '    Function GetLastFont() As String

    '        Dim strTmp As String = ""
    '        If Codes.GetUpperBound(0) > 0 Then
    '            For l As Integer = Codes.GetUpperBound(0) To 1 Step -1
    '                If Codes(l).Code = "</font>" Then
    '                    strTmp = CodesBeg(l).Code
    '                    Exit For
    '                End If
    '            Next l
    '        End If
    '        Return strTmp
    '    End Function

    '    Private Sub SetPendingCodesActive()

    '        Dim strTmp As String = ""
    '        If Codes.GetUpperBound(0) > 0 Then
    '            For l As Integer = 1 To Codes.GetUpperBound(0)
    '                If Codes(l).Status = "P" Then
    '                    Codes(l).Status = "A"
    '                    CodesBeg(l).Status = "A"
    '                ElseIf Codes(l).Status = "PG" Then
    '                    Codes(l).Status = "G"
    '                    CodesBeg(l).Status = "G"
    '                End If
    '            Next l
    '        End If
    '    End Sub

    '    Function KillCode(ByRef strCode As String, Optional ByRef strExcept As String = "") As Integer
    '        'mark all codes of type strCode as killed
    '        '    except where status = strExcept
    '        '    if strCode = "*" then mark all killed

    '        Dim strTmp As String = ""
    '        If Codes.GetUpperBound(0) > 0 Then
    '            If strExcept.StartsWith("<") Then 'strExcept is either a code or a status
    '                For l As Integer = 1 To Codes.GetUpperBound(0)
    '                    If (Codes(l).Code = strCode Or strCode = "*") And Codes(l).Code <> strExcept Then
    '                        Codes(l).Status = "K"
    '                        CodesBeg(l).Status = "K"
    '                    End If
    '                    If strCode = "*" And Codes(l).Code = strExcept Then Exit For
    '                Next l
    '            Else
    '                For l As Integer = 1 To Codes.GetUpperBound(0)
    '                    If (Codes(l).Code = strCode Or strCode = "*") And Codes(l).Status <> strExcept Then
    '                        Codes(l).Status = "K"
    '                        CodesBeg(l).Status = "K"
    '                    End If
    '                Next l
    '            End If
    '        End If
    '    End Function

    '    Function GetAllCodesTill(ByRef strTill As String) As String
    '        'get all codes except strTill

    '        Dim strTmp As New StringBuilder
    '        If Codes.GetUpperBound(0) > 0 Then
    '            For l As Integer = Codes.GetUpperBound(0) To 1 Step -1
    '                If Codes(l).Code = strTill Then
    '                    Exit For
    '                Else
    '                    If Not InNextBeg(CodesBeg(l).Code) And Codes(l).Status <> "D" Then
    '                        strTmp.Append(Codes(l).Code)
    '                        Codes(l).Status = "K"
    '                        CodesBeg(l).Status = "K"
    '                    End If
    '                End If
    '            Next l
    '        End If
    '        Return strTmp.ToString()
    '    End Function

    '    Function GetAllCodesBeg() As String

    '        Dim strTmp As New StringBuilder
    '        If CodesBeg.GetUpperBound(0) > 0 Then
    '            For l As Integer = CodesBeg.GetUpperBound(0) To 1 Step -1
    '                If CodesBeg(l).Status = "P" Then
    '                    strTmp.Append(CodesBeg(l).Code)
    '                    CodesBeg(l).Status = "A"
    '                    Codes(l).Status = "A"
    '                ElseIf CodesBeg(l).Status = "PG" Then
    '                    strTmp.Append(CodesBeg(l).Code)
    '                    CodesBeg(l).Status = "G"
    '                    Codes(l).Status = "G"
    '                End If
    '            Next l
    '        End If
    '        Return strTmp.ToString()
    '    End Function

    '    Function GetAllCodesBegTill(ByRef strTill As String) As String
    '        'get all codes except strTill - stop if strTill reached
    '        '"<table"

    '        Dim strTmp As New StringBuilder
    '        If CodesBeg.GetUpperBound(0) > 0 Then
    '            For l As Integer = 1 To CodesBeg.GetUpperBound(0)
    '                If Codes(l).Code = strTill Then
    '                    Exit For
    '                Else
    '                    If CodesBeg(l).Status = "P" Then
    '                        strTmp.Append(CodesBeg(l).Code)
    '                        Codes(l).Status = "A"
    '                        CodesBeg(l).Status = "A"
    '                    ElseIf CodesBeg(l).Status = "PG" Then
    '                        strTmp.Append(CodesBeg(l).Code)
    '                        Codes(l).Status = "G"
    '                        CodesBeg(l).Status = "G"
    '                    End If
    '                End If
    '            Next l
    '        End If
    '        Return strTmp.ToString()
    '    End Function

    '    Function ShiftNext() As String
    '        'get 1st code off list and shorten list
    '        Dim result As String = String.Empty

    '        If NextCodes.GetUpperBound(0) > 0 Then
    '            result = NextCodes(1)
    '            For l As Integer = 1 To NextCodes.GetUpperBound(0) - 1
    '                NextCodes(l) = NextCodes(l + 1)
    '            Next l
    '            ReDim Preserve NextCodes(NextCodes.GetUpperBound(0) - 1)
    '        End If
    '        Return result
    '    End Function

    '    Function ShiftNextBeg() As String
    '        'get 1st code off list and shorten list
    '        Dim result As String = String.Empty

    '        If NextCodesBeg.GetUpperBound(0) > 0 Then
    '            result = NextCodesBeg(1)
    '            For l As Integer = 1 To NextCodesBeg.GetUpperBound(0) - 1
    '                NextCodesBeg(l) = NextCodesBeg(l + 1)
    '            Next l
    '            ReDim Preserve NextCodesBeg(NextCodesBeg.GetUpperBound(0) - 1)
    '        End If
    '        Return result
    '    End Function

    '    Private Sub ProcessWord(ByRef strWord As String)
    '        Dim strTmp As String = ""
    '        Dim l As Integer
    '        Dim lRetVal As Integer

    '        Dim strTableAlign As String = "" 'current table alignment for setting up tablestring
    '        Dim strTableWidth As String = "" 'current table width for setting up tablestring

    '        If lSkipWords > 0 Then
    '            lSkipWords -= 1
    '            Exit Sub
    '        End If
    '        If (strWord.StartsWith("\") Or strWord.StartsWith("{") Or strWord.StartsWith("}")) And (Not strWord.StartsWith("\\") And Not strWord.StartsWith("\{") And Not strWord.StartsWith("\}")) Then
    '            strWord = strWord.Trim()
    '            Select Case strWord.Substring(0, Math.Min(strWord.Length, 2))
    '                Case "}"
    '                    If lBrLev = 0 Then
    '                        lRetVal = KillCode("*", "G")
    '                        ClearNext("</li>")
    '                        ClearFont()
    '                    End If
    '                Case "\'" 'special characters
    '                    strTmp = HTMLCode(strWord.Substring(2))
    '                    If strTmp.StartsWith("<rtf>:") Then
    '                        strSecTmp = strTmp.Substring(6) & " " & strSecTmp
    '                    Else
    '                        strSecTmp = strTmp & strSecTmp
    '                    End If
    '                Case "\b" 'bold
    '                    If strWord = "\b" Then
    '                        If InCodes("</b>", True) Then
    '                            '                    Codes2NextTill ("</b>")
    '                        Else
    '                            PushNext("</b>")
    '                            PushNextBeg("<b>")
    '                        End If
    '                    ElseIf strWord = "\bullet" Then
    '                        'If Not (Codes(UBound(Codes)).Code = "</li>" And Codes(UBound(Codes)).Status = "A") Then
    '                        PushNext("</li>")
    '                        PushNextBeg("<li>")
    '                        'End If
    '                    ElseIf strWord = "\b0" Then  'bold off
    '                        If InCodes("</b>") Then
    '                            Codes2NextTill("</b>")
    '                            KillCode("</b>")
    '                        End If
    '                        If InNext("</b>") Then
    '                            RemoveFromNext("</b>")
    '                        End If
    '                    End If
    '                Case "\c"
    '                    If strWord = "\cf0" Then 'color font off
    '                        strFontColor = ""
    '                        strFont = ParseFont(strFontColor, strFontSize, strFace)
    '                    ElseIf strWord.StartsWith("\cf") AndAlso CBool(strWord.Substring(3)) Then  'color font
    '                        'get color code
    '                        l = CInt(Conversion.Val(strWord.Substring(3)))
    '                        If l <= strColorTable.GetUpperBound(0) And l > 0 Then
    '                            strFontColor = "#" & strColorTable(l)
    '                        End If

    '                        'insert color
    '                        If strFontColor <> "#" Then
    '                            strFont = ParseFont(strFontColor, strFontSize, strFace)
    '                            If InNext("</font>") Then
    '                                ReplaceInNextBeg("</font>", strFont)
    '                            ElseIf InCodes("</font>") Then
    '                                PushNext("</font>")
    '                                PushNextBeg(strFont)
    '                                Codes2NextTill("</font>")
    '                                KillCode("</font>")
    '                            Else
    '                                PushNext("</font>")
    '                                PushNextBeg(strFont)
    '                            End If
    '                        End If
    '                    End If
    '                Case "\f"
    '                    If strWord.StartsWith("\fs") And IsNumeric(strWord.Substring(3)) Then 'font size
    '                        l = CInt(Conversion.Val(strWord.Substring(3)))
    '                        lFontSize = CInt(Math.Floor((l / 7) - 0)) 'calc to convert RTF to HTML sizes
    '                        If lFontSize > 8 Then lFontSize = 8
    '                        If lFontSize < 1 Then lFontSize = 1
    '                        strFontSize = CStr(lFontSize).Trim()
    '                        If Conversion.Val(strFontSize) = iDefFontSize Then strFontSize = ""
    '                        'insert size
    '                        strFont = ParseFont(strFontColor, strFontSize, strFace)
    '                    ElseIf strWord.StartsWith("\f0") And IsNumeric(strWord.Substring(2)) And gUseFontFace Then  'font type
    '                        strFace = strFontTable(CInt(Conversion.Val(strWord.Substring(2))))
    '                        strFont = ParseFont(strFontColor, strFontSize, strFace)
    '                    End If
    '                Case "\i"
    '                    If strWord = "\i" Then 'italics
    '                        If InCodes("</i>", True) Then
    '                            '                    Codes2NextTill ("</i>")
    '                        Else
    '                            PushNext("</i>")
    '                            PushNextBeg("<i>")
    '                        End If
    '                    ElseIf strWord = "\i0" Then  'italics off
    '                        If InCodes("</i>") Then
    '                            Codes2NextTill("</i>")
    '                            KillCode("</i>")
    '                        End If
    '                        If InNext("</i>") Then
    '                            RemoveFromNext("</i>")
    '                        End If
    '                    End If
    '                Case "\l"
    '                    'If strWord = "\listname" Then
    '                    '    lSkipWords = 1
    '                    'End If
    '                Case "\n"
    '                    If strWord = "\nosupersub" Then 'superscript/subscript off
    '                        If InCodes("</sub>", True) Then
    '                            Codes2NextTill("</sub>")
    '                            KillCode("</sub>")
    '                        End If
    '                        If InNext("</sub>") Then
    '                            RemoveFromNext("</sub>")
    '                        End If
    '                        If InCodes("</sup>", True) Then
    '                            Codes2NextTill("</sup>")
    '                            KillCode("</sup>")
    '                        End If
    '                        If InNext("</sup>") Then
    '                            RemoveFromNext("</sup>")
    '                        End If
    '                    End If
    '                Case "\p"
    '                    If strWord = "\par" Then
    '                        If Not (InCodes("</ul>") Or InCodes("</li>")) Then
    '                            strBeforeText2 = strBeforeText2 & strEOL & "<br>" & strCR
    '                        Else
    '                            lRetVal = KillCode("</li>")
    '                            RemoveFromNext("</li>")
    '                        End If
    '                        gBOL = True
    '                        gPar = True
    '                        'If InCodes("</ul>") Then
    '                        '    PushNext ("</li>")
    '                        '    PushNextBeg ("<li>")
    '                        'End If
    '                    ElseIf strWord = "\pard" Then
    '                        For l = 1 To CodesBeg.GetUpperBound(0)
    '                            If Codes(l).Status = "G" Or Codes(l).Status = "PG" Then
    '                                Codes(l).Status = "K"
    '                                CodesBeg(l).Status = "K"
    '                            End If
    '                        Next l
    '                        If Not gIgnorePard Then
    '                            If InCodes("</li>") Then
    '                                lRetVal = KillCode("</li>")
    '                                RemoveFromNext("</li>")
    '                            End If
    '                        End If
    '                        gPar = True
    '                    ElseIf strWord = "\plain" Then
    '                        lRetVal = KillCode("*", "G")
    '                        ClearFont()
    '                    ElseIf strWord = "\pnlvlblt" Then  'bulleted list
    '                        If Not InNext("</li>") Then
    '                            PushNext("</li>")
    '                            PushNextBeg("<li>")
    '                        End If
    '                        'PushNext ("</ul>")
    '                        'PushNextBeg ("<ul>")
    '                    ElseIf strWord = "\pntxta" Then  'numbered list?
    '                        lSkipWords = 1
    '                    ElseIf strWord = "\pntxtb" Then  'numbered list?
    '                        lSkipWords = 1
    '                    ElseIf strWord = "\pntext" Then  'bullet
    '                        If Not InNext("</li>") Then
    '                            PushNext("</li>")
    '                            PushNextBeg("<li>")
    '                            Codes2NextTill("</table>")
    '                            KillCode("*")
    '                        End If
    '                    End If
    '                Case "\q"
    '                    If strWord = "\qc" Then 'centered
    '                        strTableAlign = "center"
    '                        strTableWidth = "100%"
    '                        If InNext("</td></tr></table>") Then
    '                            '?
    '                        Else
    '                            strTable = "<table width=" & strTableWidth & "><tr><td align=""" & strTableAlign & """>"
    '                        End If
    '                        If InNext("</td></tr></table>") Then
    '                            ReplaceInNextBeg("</td></tr></table>", strTable)
    '                        ElseIf InCodes("</td></tr></table>") Then
    '                            PushNext("</td></tr></table>")
    '                            PushNextBeg(strTable)
    '                            Codes2NextTill("</td></tr></table>")
    '                        Else
    '                            PushNext("</td></tr></table>")
    '                            PushNextBeg(strTable)
    '                        End If
    '                    ElseIf strWord = "\qr" Then  'right justified
    '                        strTableAlign = "right"
    '                        strTableWidth = "100%"
    '                        If InNext("</td></tr></table>") Then
    '                            '?
    '                        Else
    '                            strTable = "<table width=" & strTableWidth & "><tr><td align=""" & strTableAlign & """>"
    '                        End If
    '                        If InNext("</td></tr></table>") Then
    '                            ReplaceInNextBeg("</td></tr></table>", strTable)
    '                        ElseIf InCodes("</td></tr></table>") Then
    '                            PushNext("</td></tr></table>")
    '                            PushNextBeg(strTable)
    '                            Codes2NextTill("</td></tr></table>")
    '                        Else
    '                            PushNext("</td></tr></table>")
    '                            PushNextBeg(strTable)
    '                        End If
    '                    End If
    '                Case "\s"
    '                    If strWord = "\strike" Then 'strike text
    '                        If Codes(Codes.GetUpperBound(0)).Code <> "</s>" Or (Codes(Codes.GetUpperBound(0)).Code = "</s>" And CodesBeg(Codes.GetUpperBound(0)).Code = "") Then
    '                            PushNext("</s>")
    '                            PushNextBeg("<s>")
    '                        End If
    '                    ElseIf strWord = "\strike0" Then  'strike off
    '                        If InCodes("</s>") Then
    '                            Codes2NextTill("</s>")
    '                            KillCode("</s>")
    '                        End If
    '                        If InNext("</s>") Then
    '                            RemoveFromNext("</s>")
    '                        End If
    '                    ElseIf strWord = "\super" Then  'superscript
    '                        If Codes(Codes.GetUpperBound(0)).Code <> "</sup>" Or (Codes(Codes.GetUpperBound(0)).Code = "</sup>" And CodesBeg(Codes.GetUpperBound(0)).Code = "") Then
    '                            PushNext("</sup>")
    '                            PushNextBeg("<sup>")
    '                        End If
    '                    ElseIf strWord = "\sub" Then  'subscript
    '                        If Codes(Codes.GetUpperBound(0)).Code <> "</sub>" Or (Codes(Codes.GetUpperBound(0)).Code = "</sub>" And CodesBeg(Codes.GetUpperBound(0)).Code = "") Then
    '                            PushNext("</sub>")
    '                            PushNextBeg("<sub>")
    '                        End If
    '                    End If

    '                    'If strWord = "\snext0" Then    'style
    '                    '    lSkipWords = 1
    '                    'End If
    '                Case "\t"
    '                    If strWord = "\tab" Then 'tab
    '                        strSecTmp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & strSecTmp
    '                    End If
    '                Case "\u"
    '                    If strWord = "\ul" Then 'underline
    '                        If InCodes("</u>", True) Then
    '                            '                    Codes2NextTill ("</u>")
    '                        Else
    '                            PushNext("</u>")
    '                            PushNextBeg("<u>")
    '                        End If
    '                    ElseIf strWord = "\ulnone" Then  'stop underline
    '                        If InCodes("</u>") Then
    '                            Codes2NextTill("</u>")
    '                            KillCode("</u>")
    '                        End If
    '                        If InNext("</u>") Then
    '                            RemoveFromNext("</u>")
    '                        End If
    '                    End If
    '            End Select
    '        Else
    '            If strWord.Length > 0 Then
    '                If strWord = "\\" Or strWord = "\{" Or strWord = "\}" Then strWord = strWord.Substring(strWord.Length - Math.Min(strWord.Length, 1))
    '                If strWord.Trim() = "" Then
    '                    If gBOL Then strWord = rtf2html_replace(strWord, " ", "&nbsp;")
    '                    strCurPhrase = strCurPhrase & strBeforeText3 & strWord
    '                Else
    '                    'regular text
    '                    If gPar Then
    '                        strBeforeText = strBeforeText & ProcessAfterTextCodes()
    '                        Next2Codes()
    '                        strBeforeText3 = GetAllCodesBeg()
    '                        gPar = False
    '                    Else
    '                        strBeforeText = strBeforeText & ProcessAfterTextCodes()
    '                        Next2Codes()
    '                        strBeforeText3 = GetAllCodesBegTill("</td></tr></table>")
    '                    End If
    '                    RemoveBlanks()

    '                    strCurPhrase = strCurPhrase & strBeforeText
    '                    strBeforeText = ""
    '                    strCurPhrase = strCurPhrase & strBeforeText2
    '                    strBeforeText2 = ""

    '                    'S.SANDEEP [ 07 MAY 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    'If Asc(strWord) >= 65 And Asc(strWord) <= 90 Then
    '                    '    strWord = "<span style=""text-transform: uppercase"">" & strWord & "</span>"
    '                    'End If
    '                    'S.SANDEEP [ 07 MAY 2012 ] -- END

    '                    strCurPhrase = strCurPhrase & strBeforeText3 & strWord
    '                    strBeforeText3 = ""
    '                    gBOL = False
    '                End If
    '            End If
    '        End If
    '    End Sub

    '    Private Sub PushNext(ByRef strCode As String)
    '        If strCode.Length > 0 Then
    '            ReDim Preserve NextCodes(NextCodes.GetUpperBound(0) + 1)
    '            NextCodes(NextCodes.GetUpperBound(0)) = strCode
    '        End If
    '    End Sub

    '    Private Sub UnShiftNext(ByRef strCode As String)
    '        'stick strCode on front of list and move everything over to make room

    '        If strCode.Length > 0 Then
    '            ReDim Preserve NextCodes(NextCodes.GetUpperBound(0) + 1)
    '            If NextCodes.GetUpperBound(0) > 1 Then
    '                For l As Integer = NextCodes.GetUpperBound(0) To 1 Step -1
    '                    NextCodes(l) = NextCodes(l - 1)
    '                Next l
    '            End If
    '            NextCodes(1) = strCode
    '        End If
    '    End Sub

    '    Private Sub UnShiftNextBeg(ByRef strCode As String)

    '        If strCode.Length > 0 Then
    '            ReDim Preserve NextCodesBeg(NextCodesBeg.GetUpperBound(0) + 1)
    '            If NextCodesBeg.GetUpperBound(0) > 1 Then
    '                For l As Integer = NextCodesBeg.GetUpperBound(0) To 1 Step -1
    '                    NextCodesBeg(l) = NextCodesBeg(l - 1)
    '                Next l
    '            End If
    '            NextCodesBeg(1) = strCode
    '        End If
    '    End Sub

    '    Private Sub PushNextBeg(ByRef strCode As String)
    '        ReDim Preserve NextCodesBeg(NextCodesBeg.GetUpperBound(0) + 1)
    '        NextCodesBeg(NextCodesBeg.GetUpperBound(0)) = strCode
    '    End Sub

    '    Private Sub RemoveBlanks()

    '        Dim l As Integer = 1
    '        Dim lOffSet As Integer = 0
    '        While l <= CodesBeg.GetUpperBound(0) And l + lOffSet <= CodesBeg.GetUpperBound(0)
    '            If CodesBeg(l).Status = "K" Or CodesBeg(l).Status = "" Then 'And Not (Codes(l) = "</font>" And Len(strFont) > 0) Then
    '                lOffSet += 1
    '            Else
    '                l += 1
    '            End If
    '            If l + lOffSet <= CodesBeg.GetUpperBound(0) Then
    '                Codes(l) = Codes(l + lOffSet)
    '                CodesBeg(l) = CodesBeg(l + lOffSet)
    '            End If
    '        End While
    '        If lOffSet > 0 Then
    '            ReDim Preserve Codes(Codes.GetUpperBound(0) - lOffSet)
    '            ReDim Preserve CodesBeg(CodesBeg.GetUpperBound(0) - lOffSet)
    '        End If
    '    End Sub

    '    Private Sub RemoveFromNext(ByRef strRem As String)
    '        Dim l, m As Integer

    '        If Not (NextCodes.GetUpperBound(0) < 1) Then
    '            l = 1
    '            While l < NextCodes.GetUpperBound(0)
    '                If NextCodes(l) = strRem Then
    '                    For m = l To NextCodes.GetUpperBound(0) - 1
    '                        NextCodes(m) = NextCodes(m + 1)
    '                        NextCodesBeg(m) = NextCodesBeg(m + 1)
    '                    Next m
    '                    l = m
    '                Else
    '                    l += 1
    '                End If
    '            End While
    '            ReDim Preserve NextCodes(NextCodes.GetUpperBound(0) - 1)
    '            ReDim Preserve NextCodesBeg(NextCodesBeg.GetUpperBound(0) - 1)
    '        End If

    '    End Sub

    '    Function rtf2html_replace(ByVal strIn As String, ByVal strRepl As String, ByVal strWith As String) As String
    '        'replace all instances of strRepl in strIn with strWith

    '        If (strRepl.Length = 0) Or (strIn.Length = 0) Then
    '            Return strIn
    '        End If
    '        Dim i As Integer = (strIn.IndexOf(strRepl) + 1)
    '        While i <> 0
    '            strIn = strIn.Substring(0, Math.Min(strIn.Length, i - 1)) & strWith & strIn.Substring(i + strRepl.Length - 1)
    '            i = Strings.InStr(i + strWith.Length, strIn, strRepl)
    '        End While
    '        Return strIn
    '    End Function

    '    Private Sub ReviveCode(ByRef strCode As String)

    '        For l As Integer = 1 To Codes.GetUpperBound(0)
    '            If Codes(l).Code = strCode Then
    '                Codes(l).Status = "A"
    '                CodesBeg(l).Status = "A"
    '            End If
    '        Next l
    '    End Sub

    '    Function ReplaceInNextBeg(ByRef strCode As String, ByRef strWith As String) As Integer

    '        Dim lCount As Integer = 0 'number of codes replaced
    '        For l As Integer = 1 To NextCodes.GetUpperBound(0)
    '            If NextCodes(l) = strCode Then
    '                NextCodesBeg(l) = strWith
    '                lCount += 1
    '            End If
    '        Next l
    '        Return lCount
    '    End Function

    '    Private Sub ReplaceInCodesBeg(ByRef strCode As String, ByRef strWith As String)

    '        Dim l As Integer = 1
    '        While l <= Codes.GetUpperBound(0) And Codes(l).Code <> strCode
    '            l += 1
    '        End While
    '        If Codes(l).Code = strCode Then
    '            If CodesBeg(l).Code <> strWith Then
    '                CodesBeg(l).Code = strWith
    '                Codes(l).Status = "P"
    '                CodesBeg(l).Status = "P"
    '            Else
    '                Codes(l).Status = "P"
    '                CodesBeg(l).Status = "P"
    '            End If
    '        End If
    '    End Sub

    '    Public Function rtf2html3(ByRef strRTF As String, Optional ByRef strOptions As String = "") As String
    '        'Dim ShowCodes() As Object
    '        'Options:
    '        '+H              add an HTML header and footer
    '        '+G              add a generator Metatag
    '        '+T="MyTitle"    add a title (only works if +H is used)
    '        '+CR             add a carraige return after all <br>s
    '        '+I              keep html codes intact
    '        '+F=X            default font size (blanks out any changes to this size - saves on space)
    '        '-FF             ignore font faces

    '        Dim l As Integer = 0
    '        Dim lTmp As Integer = 0
    '        Dim lTmp2 As Integer = 0
    '        Dim lTmp3 As Integer = 0
    '        Dim lRTFLen As Integer = 0
    '        Dim lEOS As Integer = 0 'end of section
    '        Dim strTmp As String = ""
    '        Dim strTmp2 As String = ""
    '        Dim strEOS As String = "" 'string to be added to end of section
    '        Dim strBOS As String = "" 'string to be added to beginning of section
    '        Dim strEOP As String = "" 'string to be added to end of paragraph
    '        Dim strBOL As String = "" 'string to be added to the begining of each new line
    '        Dim strEOL As String = "" 'string to be added to the end of each new line
    '        Dim strEOLL As String = "" 'string to be added to the end of previous line
    '        'Const gHellFrozenOver As Boolean = False 'always false
    '        Dim gSkip As Boolean = False 'skip to next word/command
    '        Dim strCodes As String = "" 'codes for ascii to HTML char conversion
    '        Dim strCurLine As String = "" 'temp storage for text for current line before being added to strHTML
    '        Dim strFontCodes As String = "" 'list of font code modifiers
    '        Dim gSeekingText As Boolean = False 'True if we have to hit text before inserting a </FONT>
    '        Dim gText As Boolean = False 'true if there is text (as opposed to a control code) in strTmp
    '        Dim strAlign As String = "" '"center" or "right"
    '        Dim gAlign As Boolean = False 'if current text is aligned
    '        Dim strGen As String = "" 'Temp store for Generator Meta Tag if requested
    '        Dim strTitle As String = "" 'Temp store for Title if requested
    '        Dim gHTML As Boolean 'true if html codes should be left intact
    '        Dim strWordTmp As String = "" 'temporary word buffer
    '        Dim strEndText As String = "" 'ending text

    '        ClearCodes()
    '        Dim strHTML As String = ""
    '        gPlain = False
    '        gBOL = True
    '        gPar = False
    '        strCurPhrase = ""

    '        'setup +CR option
    '        strCR = CStr(IIf(strOptions.IndexOf("+CR") >= 0, Strings.Chr(13) & Strings.Chr(10), ""))
    '        'setup +HTML option
    '        gHTML = CBool(IIf(strOptions.IndexOf("+I") >= 0, True, False))
    '        'setup default font size option
    '        If strOptions.IndexOf("+F=") >= 0 Then
    '            l = (strOptions.IndexOf("+F=") + 1) + 3
    '            strTmp = strOptions.Substring(l - 1, 1)
    '            iDefFontSize = 0
    '            While IsDig(strTmp)
    '                iDefFontSize = CShort(iDefFontSize * 10 + Conversion.Val(strTmp))
    '                l += 1
    '                strTmp = strOptions.Substring(l - 1, 1)
    '            End While
    '        End If
    '        'setup to use different fonts or not
    '        'CBool(IIf(strOptions.IndexOf("-FF") >= 0, False, True))
    '        gUseFontFace = True

    '        Dim strRTFTmp As String = TrimAll(strRTF)

    '        If strRTFTmp.StartsWith("{") And strRTFTmp.EndsWith("}") Then strRTFTmp = strRTFTmp.Substring(1, strRTFTmp.Length - 2)

    '        'setup list (bullets) status
    '        If strRTFTmp.IndexOf("\list\") >= 0 Then
    '            'I'm not sure if this is in any way correct but it seems to work for me
    '            'sometimes \pard ends a list item sometimes it doesn't
    '            gIgnorePard = True
    '        Else
    '            gIgnorePard = False
    '        End If

    '        'setup color table
    '        Dim lBOS As Integer = (strRTFTmp.IndexOf("\colortbl") + 1) 'beginning of section
    '        If lBOS > 0 Then
    '            strSecTmp = NabSection(strRTFTmp, lBOS)
    '            GetColorTable(strSecTmp, strColorTable)
    '        End If

    '        'setup font table
    '        lBOS = (strRTFTmp.IndexOf("\fonttbl") + 1)
    '        If lBOS > 0 Then
    '            strSecTmp = NabSection(strRTFTmp, lBOS)
    '            GetFontTable(strSecTmp, strFontTable)
    '        End If

    '        'setup stylesheets
    '        lBOS = (strRTFTmp.IndexOf("\stylesheet") + 1)
    '        If lBOS > 0 Then
    '            strSecTmp = NabSection(strRTFTmp, lBOS)
    '            'ignore stylesheets for now
    '        End If

    '        'setup info
    '        lBOS = (strRTFTmp.IndexOf("\info") + 1)
    '        If lBOS > 0 Then
    '            strSecTmp = NabSection(strRTFTmp, lBOS)
    '            'ignore info for now
    '        End If

    '        'list table
    '        lBOS = (strRTFTmp.IndexOf("\listtable") + 1)
    '        If lBOS > 0 Then
    '            strSecTmp = NabSection(strRTFTmp, lBOS)
    '            'ignore info for now
    '        End If

    '        'list override table
    '        lBOS = (strRTFTmp.IndexOf("\listoverridetable") + 1)
    '        If lBOS > 0 Then
    '            strSecTmp = NabSection(strRTFTmp, lBOS)
    '            'ignore info for now
    '        End If

    '        lBrLev = 0
    '        Dim strLastWord As String = "" 'previous "word"
    '        While strRTFTmp.Length > 0
    '            strSecTmp = NabNextLine(strRTFTmp)
    '            While strSecTmp.Length > 0
    '                strLastWord = strWordTmp
    '                strWordTmp = NabNextWord(strSecTmp)
    '                If lBrLev > 0 Then
    '                    If strWordTmp = "{" Then
    '                        lBrLev += 1
    '                    ElseIf strWordTmp = "}" Then
    '                        lBrLev -= 1
    '                    End If
    '                    strWordTmp = ""
    '                ElseIf strWordTmp = "\*" Or strWordTmp = "\pict" Then
    '                    'skip \pnlvlbt stuff
    '                    lBrLev = 1
    '                    strWordTmp = ""
    '                ElseIf strWordTmp = "\pntext" Then
    '                    'get bullet codes but skip rest for now
    '                    lBrLev = 1
    '                ElseIf strWordTmp.StartsWith("\caps") Then
    '                    strWordTmp = ""
    '                End If
    '                If strWordTmp.Length > 0 Then
    '                    'If gDebug Then Dim tempAuxVar As Object = ShowCodes(CInt(strWordTmp)) 'for debugging only
    '                    If strWordTmp.Length > 0 Then ProcessWord(strWordTmp)
    '                End If
    '            End While
    '        End While

    '        'get any remaining codes in stack
    '        strEndText = strEndText & GetActiveCodes()
    '        strBeforeText2 = rtf2html_replace(strBeforeText2, "<br>", "")
    '        strBeforeText2 = rtf2html_replace(strBeforeText2, Strings.Chr(13) & Strings.Chr(10), "")
    '        strCurPhrase = strCurPhrase & strBeforeText & strBeforeText2 & strEndText
    '        strBeforeText = ""
    '        strBeforeText2 = ""
    '        strBeforeText3 = ""
    '        strHTML = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 3.2//EN"">" & vbCrLf
    '        strHTML &= "<HTML>" & vbCrLf
    '        strHTML &= "<HEAD>" & vbCrLf
    '        strHTML &= "<TITLE>" & "" & "</TITLE>" & vbCrLf
    '        strHTML &= "<META NAME=""GENERATOR"" CONTENT=""RTF2HTML By eZee Technosys"">"
    '        strHTML &= "</HEAD>" & vbCrLf
    '        strHTML &= "<BODY>" & vbCrLf
    '        strHTML &= strCurPhrase & "<BR/><BR/>" & "<BR/><BR/>"
    '        strHTML &= "<center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center> "
    '        strHTML &= "</BODY>" & vbCrLf
    '        strHTML &= "</HTML>" & vbCrLf
    '        strCurPhrase = ""
    '        ClearFont()
    '        Return strHTML
    '    End Function

    '    Function IsDig(ByRef strChar As String) As Boolean
    '        If strChar.Length = 0 Then
    '            Return False
    '        Else
    '            Return CBool(("1234567890").IndexOf(strChar) + 1)
    '        End If
    '    End Function

    '    Function GetCodes(ByRef strWordTmp As String) As String

    '        Dim strTmp As New StringBuilder
    '        strTmp.Append("CurWord: ")
    '        If strWordTmp.Length > 20 Then
    '            strTmp.Append(strWordTmp.Substring(0, Math.Min(strWordTmp.Length, 20)) & "...")
    '        Else
    '            strTmp.Append(strWordTmp)
    '        End If
    '        strTmp.Append(Strings.Chr(13) & Strings.Chr(10) & Strings.Chr(13) & Strings.Chr(10) & "BegCodes: ")
    '        For l As Integer = 1 To CodesBeg.GetUpperBound(0)
    '            strTmp.Append(CodesBeg(l).Code & " (" & CodesBeg(l).Status & "), ")
    '        Next l
    '        strTmp.Append(Strings.Chr(13) & Strings.Chr(10) & "Codes: ")
    '        For l As Integer = 1 To Codes.GetUpperBound(0)
    '            strTmp.Append(Codes(l).Code & " (" & Codes(l).Status & "), ")
    '        Next l
    '        strTmp.Append(Strings.Chr(13) & Strings.Chr(10) & Strings.Chr(13) & Strings.Chr(10) & "NextBegCodes: ")
    '        For l As Integer = 1 To NextCodesBeg.GetUpperBound(0)
    '            strTmp.Append(NextCodesBeg(l) & ", ")
    '        Next l
    '        strTmp.Append(Strings.Chr(13) & Strings.Chr(10) & "NextCodes: ")
    '        For l As Integer = 1 To NextCodes.GetUpperBound(0)
    '            strTmp.Append(NextCodes(l) & ", ")
    '        Next l
    '        strTmp.Append(Strings.Chr(13) & Strings.Chr(10) & Strings.Chr(13) & Strings.Chr(10) & "Font String: " & strFont)
    '        strTmp.Append(Strings.Chr(13) & Strings.Chr(10) & Strings.Chr(13) & Strings.Chr(10) & "Before Text: " & strBeforeText2)
    '        Return strTmp.ToString()
    '    End Function

    '    Function TrimAll(ByVal strTmp As String) As String

    '        strTmp = strTmp.Trim()
    '        Dim l As Integer = strTmp.Length + 1
    '        While l <> strTmp.Length
    '            l = strTmp.Length
    '            If strTmp.Substring(strTmp.Length - Math.Min(strTmp.Length, 1)) = Strings.Chr(13) & Strings.Chr(10) Then strTmp = strTmp.Substring(0, strTmp.Length - 1)
    '            If strTmp.Substring(0, Math.Min(strTmp.Length, 1)) = Strings.Chr(13) & Strings.Chr(10) Then strTmp = strTmp.Substring(strTmp.Length - (strTmp.Length - 1))
    '            If strTmp.Substring(strTmp.Length - Math.Min(strTmp.Length, 1)) = Strings.Chr(13) Then strTmp = strTmp.Substring(0, strTmp.Length - 1)
    '            If strTmp.Substring(0, Math.Min(strTmp.Length, 1)) = Strings.Chr(13) Then strTmp = strTmp.Substring(strTmp.Length - (strTmp.Length - 1))
    '            If strTmp.Substring(strTmp.Length - Math.Min(strTmp.Length, 1)) = Constants.vbLf Then strTmp = strTmp.Substring(0, strTmp.Length - 1)
    '            If strTmp.Substring(0, Math.Min(strTmp.Length, 1)) = Constants.vbLf Then strTmp = strTmp.Substring(strTmp.Length - (strTmp.Length - 1))
    '        End While
    '        Return strTmp
    '    End Function

    '    Function HTMLCode(ByRef strRTFCode As String) As String
    '        'given rtf code return html code
    '        Dim result As String = String.Empty
    '        Select Case strRTFCode
    '            Case "00"
    '                result = "&nbsp;"
    '            Case "a9"
    '                result = "&copy;"
    '            Case "b4"
    '                result = "&acute;"
    '            Case "ab"
    '                result = "&laquo;"
    '            Case "bb"
    '                result = "&raquo;"
    '            Case "a1"
    '                result = "&iexcl;"
    '            Case "bf"
    '                result = "&iquest;"
    '            Case "c0"
    '                result = "&Agrave;"
    '            Case "e0"
    '                result = "&agrave;"
    '            Case "c1"
    '                result = "&Aacute;"
    '            Case "e1"
    '                result = "&aacute;" '
    '            Case "c2"
    '                result = "&Acirc;"
    '            Case "e2"
    '                result = "&acirc;"
    '            Case "c3"
    '                result = "&Atilde;"
    '            Case "e3"
    '                result = "&atilde;"
    '            Case "c4"
    '                result = "&Auml;"
    '            Case "e4", "99"
    '                result = "<rtf>:\super TM\nosupersub"
    '            Case "c5"
    '                result = "&Aring;"
    '            Case "e5"
    '                result = "&aring;"
    '            Case "c6"
    '                result = "&AElig;"
    '            Case "e6"
    '                result = "&aelig;"
    '            Case "c7"
    '                result = "&Ccedil;"
    '            Case "e7"
    '                result = "&ccedil;"
    '            Case "d0"
    '                result = "&ETH;"
    '            Case "f0"
    '                result = "&eth;"
    '            Case "c8"
    '                result = "&Egrave;"
    '            Case "e8"
    '                result = "&egrave;"
    '            Case "c9"
    '                result = "&Eacute;"
    '            Case "e9"
    '                result = "&eacute;"
    '            Case "ca"
    '                result = "&Ecirc;"
    '            Case "ea"
    '                result = "&ecirc;"
    '            Case "cb"
    '                result = "&Euml;"
    '            Case "eb"
    '                result = "&euml;"
    '            Case "cc"
    '                result = "&Igrave;"
    '            Case "ec"
    '                result = "&igrave;"
    '            Case "cd"
    '                result = "&Iacute;"
    '            Case "ed"
    '                result = "&iacute;" '
    '            Case "ce"
    '                result = "&Icirc;"
    '            Case "ee"
    '                result = "&icirc;"
    '            Case "cf"
    '                result = "&Iuml;"
    '            Case "ef"
    '                result = "&iuml;"
    '            Case "d1"
    '                result = "&Ntilde;"
    '            Case "f1"
    '                result = "&ntilde;"
    '            Case "d2"
    '                result = "&Ograve;"
    '            Case "f2"
    '                result = "&ograve;"
    '            Case "d3"
    '                result = "&Oacute;"
    '            Case "f3"
    '                result = "&oacute;"
    '            Case "d4"
    '                result = "&Ocirc;"
    '            Case "f4"
    '                result = "&ocirc;"
    '            Case "d5"
    '                result = "&Otilde;"
    '            Case "f5"
    '                result = "&otilde;"
    '            Case "d6"
    '                result = "&Ouml;"
    '            Case "f6"
    '                result = "&ouml;"
    '            Case "d8"
    '                result = "&Oslash;"
    '            Case "f8"
    '                result = "&oslash;"
    '            Case "d9"
    '                result = "&Ugrave;"
    '            Case "f9"
    '                result = "&ugrave;"
    '            Case "da"
    '                result = "&Uacute;"
    '            Case "fa"
    '                result = "&uacute;"
    '            Case "db"
    '                result = "&Ucirc;"
    '            Case "fb"
    '                result = "&ucirc;"
    '            Case "dc"
    '                result = "&Uuml;"
    '            Case "fc"
    '                result = "&uuml;"
    '            Case "dd"
    '                result = "&Yacute;"
    '            Case "fd"
    '                result = "&yacute;"
    '            Case "ff"
    '                result = "&yuml;"
    '            Case "de"
    '                result = "&THORN;"
    '            Case "fe"
    '                result = "&thorn;"
    '            Case "df"
    '                result = "&szlig;"
    '            Case "a7"
    '                result = "&sect;"
    '            Case "b6"
    '                result = "&para;"
    '            Case "b5"
    '                result = "&micro;"
    '            Case "a6"
    '                result = "&brvbar;"
    '            Case "b1"
    '                result = "&plusmn;"
    '            Case "b7"
    '                result = "&middot;"
    '            Case "a8"
    '                result = "&uml;"
    '            Case "b8"
    '                result = "&cedil;"
    '            Case "aa"
    '                result = "&ordf;"
    '            Case "ba"
    '                result = "&ordm;"
    '            Case "ac"
    '                result = "&not;"
    '            Case "ad"
    '                result = "&shy;"
    '            Case "af"
    '                result = "&macr;"
    '            Case "b0"
    '                result = "&deg;"
    '            Case "b9"
    '                result = "&sup1;"
    '            Case "b2"
    '                result = "&sup2;"
    '            Case "b3"
    '                result = "&sup3;"
    '            Case "bc"
    '                result = "&frac14;"
    '            Case "bd"
    '                result = "&frac12;"
    '            Case "be"
    '                result = "&frac34;"
    '            Case "d7"
    '                result = "&times;"
    '            Case "f7"
    '                result = "&divide;"
    '            Case "a2"
    '                result = "&cent;"
    '            Case "a3"
    '                result = "&pound;"
    '            Case "a4"
    '                result = "&curren;"
    '            Case "a5"
    '                result = "&yen;"
    '            Case "85"
    '                result = "..."
    '            Case "9e"
    '                result = "" '
    '            Case "9a"
    '                result = "" '
    '        End Select
    '        Return result
    '    End Function

    '    Function TrimifCmd(ByVal strTmp As String) As String

    '        Dim l As Integer = 1
    '        While strTmp.Substring(l - 1, 1) = " "
    '            l += 1
    '        End While
    '        If strTmp.Substring(l - 1, 1) = "\" Or strTmp.Substring(l - 1, 1) = "{" Then
    '            strTmp = strTmp.Trim()
    '        Else
    '            If strTmp.StartsWith(" ") Then strTmp = strTmp.Substring(1)
    '            strTmp = strTmp.TrimEnd()
    '        End If
    '        Return strTmp
    '    End Function


    '    ''S.SANDEEP [05-Apr-2018] -- START
    '    ''ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    '    Private Function ExtractImgHex(ByVal s As String) As String
    '        Try
    '            s = s.TrimEnd(" "c, ControlChars.Tab, ControlChars.Cr, ControlChars.Lf) ' Remove trailing white space.
    '            If s.EndsWith("}") Then s = s.Remove(s.Length - 1)
    '            Dim sParagraphs() As String = s.Split(New String() {"\par"}, StringSplitOptions.None)
    '            For i As Integer = 0 To sParagraphs.Length - 1
    '                Dim sParagraph As String = sParagraphs(i)
    '                sParagraph = sParagraph.TrimEnd(" "c, ControlChars.Tab, ControlChars.Cr, ControlChars.Lf) ' Remove trailing white space.
    '                sParagraph = sParagraph.TrimStart(" "c, ControlChars.Tab, ControlChars.Cr, ControlChars.Lf) ' Remove trailing white space.
    '                If sParagraph.StartsWith("{\pict\") Then
    '                    Dim sPics() As String = sParagraph.Split(New String() {"{\pict\"}, StringSplitOptions.RemoveEmptyEntries)
    '                    For Each sPic In sPics
    '                        sPic = sPic.Trim
    '                        If sPic <> "" Then

    '                        End If
    '                    Next
    '                End If
    '            Next
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
    '        Finally
    '        End Try
    '        'If s.StartsWith("{\pict\") Then
    '        '    Dim pictTagIdx As Integer = s.IndexOf("{\pict\")
    '        '    Dim startIndex As Integer = s.IndexOf(" ", pictTagIdx) + 1
    '        '    Dim endIndex As Integer = s.IndexOf("}", startIndex)
    '        '    Return s.Substring(startIndex, endIndex - startIndex)
    '        'End If
    '    End Function



    '    'Private Function ExtractImgHex(ByVal s As String) As String
    '    '    Dim pictTagIdx As Integer = s.IndexOf("{\pict\")
    '    '    Dim startIndex As Integer = s.IndexOf(" ", pictTagIdx) + 1
    '    '    Dim endIndex As Integer = s.IndexOf("}", startIndex)
    '    '    Return s.Substring(startIndex, endIndex - startIndex)
    '    'End Function

    '    Public Shared Function ToBinary(ByVal imageDataHex As String) As Byte()
    '        If imageDataHex Is Nothing Then
    '            Throw New ArgumentNullException("imageDataHex")
    '        End If

    '        Dim hexDigits As Integer = imageDataHex.Length
    '        Dim dataSize As Integer = hexDigits \ 2
    '        Dim imageDataBinary(dataSize - 1) As Byte

    '        Dim hex As New StringBuilder(2)

    '        Dim dataPos As Integer = 0
    '        For i As Integer = 0 To hexDigits - 1
    '            Dim c As Char = imageDataHex.Chars(i)
    '            If Char.IsWhiteSpace(c) Then
    '                Continue For
    '            End If
    '            hex.Append(imageDataHex.Chars(i))
    '            If hex.Length = 2 Then
    '                imageDataBinary(dataPos) = Byte.Parse(hex.ToString(), System.Globalization.NumberStyles.HexNumber)
    '                dataPos += 1
    '                hex.Remove(0, 2)
    '            End If
    '        Next i
    '        Return imageDataBinary
    '    End Function
    '    ''S.SANDEEP [05-Apr-2018] -- END
    '#End Region

End Class
