﻿'************************************************************************************************************************************
'Class Name :clsexpenseapprover_master.vb
'Purpose    :
'Date       :06-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsExpenseApprover_Master
    Private Shared ReadOnly mstrModuleName As String = "clsExpenseApprover_Master"
    Private objDataOperation As clsDataOperation
    Private mstrMessage As String = ""
    Private objExpenseApprTran As New clsExpenseApprover_Tran
    Private objExpenseMapping As New clsExpenseApproverMapping

#Region " Private variables "

    Private mintcrApproverunkid As Integer
    Private mintExpensetypeid As Integer
    Private mintcrLevelunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintMappedUserId As Integer = 0
    Private mstrEmployeeName As String = String.Empty


    'Pinkal (19-Mar-2015) -- Start
    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
    Private mintSwapFromApproverID As Integer = 0
    Private mintSwapToApproverID As Integer = 0
    Private mblnIsSwap As Boolean = False
    'Pinkal (19-Mar-2015) -- End

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Private mblnIsActive As Boolean = True
    'Shani(17-Aug-2015) -- End


    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private mblnIsexternalapprover As Boolean = False
    'Pinkal (01-Mar-2016) -- End


#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crapproverunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.

    'Public Property _crApproverunkid() As Integer
    '    Get
    '        Return mintcrApproverunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintcrApproverunkid = value
    '        Call GetData()
    '    End Set
    'End Property

    Public Property _crApproverunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintcrApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintcrApproverunkid = value
            Call GetData()
        End Set
    End Property
    'Gajanan [23-SEP-2019] -- End



    ''' <summary>
    ''' Purpose: Get or Set expensetypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expensetypeid() As Integer
        Get
            Return mintExpensetypeid
        End Get
        Set(ByVal value As Integer)
            mintExpensetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crlevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _crLevelunkid() As Integer
        Get
            Return mintcrLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintcrLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappeduserid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _MappedUserId() As Integer
        Get
            Return mintMappedUserId
        End Get
        Set(ByVal value As Integer)
            mintMappedUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Employee Name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    'Pinkal (19-Mar-2015) -- Start
    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

    ''' <summary>
    ''' Purpose: Get or Set isswap
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isswap() As Boolean
        Get
            Return mblnIsSwap
        End Get
        Set(ByVal value As Boolean)
            mblnIsSwap = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SwapFromApproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SwapFromApproverunkid() As Integer
        Get
            Return mintSwapFromApproverID
        End Get
        Set(ByVal value As Integer)
            mintSwapFromApproverID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SwapToApproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SwapToApproverunkid() As Integer
        Get
            Return mintSwapToApproverID
        End Get
        Set(ByVal value As Integer)
            mintSwapToApproverID = value
        End Set
    End Property

    'Pinkal (19-Mar-2015) -- End

    ''' <summary>
    ''' Purpose: Get or Set IsActive
    ''' Modify By: Shani
    ''' </summary>
    Public Property _IsActive() As Boolean
        Get
            Return mblnIsActive
        End Get
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Shani(17-Aug-2015) -- End

    ''' <summary>
    ''' Purpose: Get or Set isexternalapprover
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _Isexternalapprover() As Boolean
        Get
            Return mblnIsexternalapprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternalapprover = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        'Gajanan [23-SEP-2019] -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (01-Mar-2016) -- Start
        'Enhancement - Implementing External Approver in Claim Request & Leave Module.
        Dim StrFinalQurey As String = String.Empty
        Dim StrQCondition As String = String.Empty
        'Pinkal (01-Mar-2016) -- End



        'Pinkal (19-Mar-2015) -- Start
        'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
        ' objDataOperation = New clsDataOperation


        'Gajanan [23-SEP-2019] -- Start    
        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [23-SEP-2019] -- End


        'Pinkal (19-Mar-2015) -- End
        Try


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'strQ = "SELECT " & _
            '       "  cmexpapprover_master.crapproverunkid " & _
            '       ", cmexpapprover_master.expensetypeid " & _
            '       ", cmexpapprover_master.crlevelunkid " & _
            '       ", cmexpapprover_master.employeeunkid " & _
            '       ", cmexpapprover_master.userunkid " & _
            '       ", cmexpapprover_master.isvoid " & _
            '       ", cmexpapprover_master.voiddatetime " & _
            '       ", cmexpapprover_master.voidreason " & _
            '       ", cmexpapprover_master.voiduserunkid " & _
            '       " ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserid " & _
            '       " ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname+' '+hremployee_master.surname AS employee " & _
            '       " ,cmexpapprover_master.isswap " & _
            '      "FROM cmexpapprover_master " & _
            '      " LEFT JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
            '      " LEFT JOIN hrapprover_usermapping ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid AND usertypeid = '" & enUserType.crApprover & "' " & _
            '      "WHERE cmexpapprover_master.crapproverunkid = @crapproverunkid "

            strQ = "SELECT " & _
                   "  cmexpapprover_master.crapproverunkid " & _
                   ", cmexpapprover_master.expensetypeid " & _
                   ", cmexpapprover_master.crlevelunkid " & _
                   ", cmexpapprover_master.employeeunkid " & _
                   ", cmexpapprover_master.userunkid " & _
                   ", cmexpapprover_master.isvoid " & _
                   ", cmexpapprover_master.voiddatetime " & _
                   ", cmexpapprover_master.voidreason " & _
                   ", cmexpapprover_master.voiduserunkid " & _
                   " ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserid " & _
                      " ,#APPR_NAME#  AS employee " & _
                   " ,cmexpapprover_master.isswap " & _
                      ", cmexpapprover_master.isexternalapprover " & _
                  "FROM cmexpapprover_master " & _
                  " LEFT JOIN hrapprover_usermapping ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid AND usertypeid = '" & enUserType.crApprover & "' " & _
                      " #EMPL_JOIN# "


            StrFinalQurey = strQ

            StrQCondition &= "WHERE cmexpapprover_master.crapproverunkid = @crapproverunkid  AND cmexpapprover_master.isexternalapprover = #isExternal# "

            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master on hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid ")
            strQ &= StrQCondition.Replace("#isExternal# ", "0")

            'Pinkal (01-Mar-2016) -- End

            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim dsCompany As New DataSet
            dsCompany = GetClaimExternalApproverList(objDataOperation, "List", True)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                Else
                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")

                End If

                strQ &= StrQCondition.Replace("#isExternal# ", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid.ToString)

                dstmp = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintcrApproverunkid = CInt(dtRow.Item("crapproverunkid"))
                mintExpensetypeid = CInt(dtRow.Item("expensetypeid"))
                mintcrLevelunkid = CInt(dtRow.Item("crlevelunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintMappedUserId = CInt(dtRow.Item("mapuserid"))
                mstrEmployeeName = dtRow.Item("employee")


                'Pinkal (19-Mar-2015) -- Start
                'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
                mblnIsSwap = CBool(dtRow.Item("isswap"))
                'Pinkal (19-Mar-2015) -- End


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                mblnIsexternalapprover = CBool(dtRow.Item("isexternalapprover"))
                'Pinkal (01-Mar-2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            'objDataOperation = Nothing
            'Pinkal (19-Mar-2015) -- End

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Gajanan [23-SEP-2019] -- End
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '' <summary>
    '' Modify By: Sandeep Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        'Pinkal (19-Mar-2015) -- Start
    '        'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.


    '        'SHANI (06 JUN 2015) -- Start
    '        'Enhancement : Changes in C & R module given by Mr.Andrew.

    '        'strQ = "SELECT " & _
    '        '       "	 cmexpapprover_master.crapproverunkid " & _
    '        '       "	,cmexpapprover_master.expensetypeid " & _
    '        '       "	,cmexpapprover_master.crlevelunkid " & _
    '        '       "	,cmexpapprover_master.employeeunkid " & _
    '        '       "	,cmexpapprover_master.userunkid " & _
    '        '       "	,cmexpapprover_master.isvoid " & _
    '        '       "	,cmexpapprover_master.voiddatetime " & _
    '        '       "	,cmexpapprover_master.voidreason " & _
    '        '       "	,cmexpapprover_master.voiduserunkid " & _
    '        '       "	,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS ename " & _
    '        '       "	,hrdepartment_master.name AS department " & _
    '        '       "	,ISNULL(Usr.username,'') AS usermapped " & _
    '        '       "	,hrjob_master.job_name AS jobname " & _
    '        '       "	,cmapproverlevel_master.crlevelname AS clevel " & _
    '        '       "    ,CASE WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
    '        '       "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
    '        '       "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training END AS expensetype " & _
    '        '       "    ,cmexpapprover_master.isswap " & _
    '        '       "FROM cmexpapprover_master " & _
    '        '       "	JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
    '        '       "	JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '        '       "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '        '       "	LEFT JOIN hrapprover_usermapping ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid " & _
    '        '       "		AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & " " & _
    '        '       "	LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
    '        '       "WHERE cmexpapprover_master.isvoid = 0 "

    '        'strQ &= " AND cmexpapprover_master.isswap = 0 "

    '        strQ = "SELECT " & _
    '               "	 cmexpapprover_master.crapproverunkid " & _
    '               "	,cmexpapprover_master.expensetypeid " & _
    '               "	,cmexpapprover_master.crlevelunkid " & _
    '               "	,cmexpapprover_master.employeeunkid " & _
    '               "	,cmexpapprover_master.userunkid " & _
    '               "	,cmexpapprover_master.isvoid " & _
    '               "	,cmexpapprover_master.voiddatetime " & _
    '               "	,cmexpapprover_master.voidreason " & _
    '               "	,cmexpapprover_master.voiduserunkid " & _
    '               "	,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS ename " & _
    '               "	,hrdepartment_master.name AS department " & _
    '               "	,ISNULL(Usr.username,'') AS usermapped " & _
    '               "	,hrjob_master.job_name AS jobname " & _
    '               "	,cmapproverlevel_master.crlevelname AS clevel " & _
    '               "    ,CASE WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
    '               "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
    '               "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
    '               "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training END AS expensetype " & _
    '               "    ,cmexpapprover_master.isswap " & _
    '               "FROM cmexpapprover_master " & _
    '               "	JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
    '               "	JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
    '               "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '               "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '               "	LEFT JOIN hrapprover_usermapping ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid " & _
    '               "		AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & " " & _
    '               "	LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
    '               "WHERE cmexpapprover_master.isvoid = 0 "

    '        strQ &= " AND cmexpapprover_master.isswap = 0 "


    '        'SHANI (06 JUN 2015) -- End

    '        'Pinkal (19-Mar-2015) -- End

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If

    '        If strUserAccessLevelFilterString.Trim.Length <= 0 Then
    '            strUserAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        strQ &= strUserAccessLevelFilterString

    '        objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
    '        objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
    '        objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
    '        objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal strEmployeeAsOnDate As String _
                                      , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                      , Optional ByVal mstrFilter As String = "" _
                                      , Optional ByVal blnIsActiveApprover As Boolean = True _
                                      , Optional ByVal intApproverTranUnkid As Integer = 0) As DataSet 'Shani(17-Aug-2015) -- [blnIsActiveApprover]


        'Pinkal (01-Mar-2016) -   'Enhancement - Implementing External Approver in Claim Request & Leave Module.[Optional ByVal intApproverTranUnkid As Integer = 0]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            'Pinkal (01-Mar-2016) -- End


            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'strQ = "SELECT " & _
            '       "	 cmexpapprover_master.crapproverunkid " & _
            '       "	,cmexpapprover_master.expensetypeid " & _
            '       "	,cmexpapprover_master.crlevelunkid " & _
            '       "	,cmexpapprover_master.employeeunkid " & _
            '       "	,cmexpapprover_master.userunkid " & _
            '       "	,cmexpapprover_master.isvoid " & _
            '       "	,cmexpapprover_master.voiddatetime " & _
            '       "	,cmexpapprover_master.voidreason " & _
            '       "	,cmexpapprover_master.voiduserunkid " & _
            '       "	,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS ename " & _
            '       "	,hrdepartment_master.name AS department " & _
            '       "	,ISNULL(Usr.username,'') AS usermapped " & _
            '       "	,hrjob_master.job_name AS jobname " & _
            '       "	,cmapproverlevel_master.crlevelname AS clevel " & _
            '       "    ,CASE WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
            '       "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
            '       "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
            '       "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training END AS expensetype " & _
            '       "    ,cmexpapprover_master.isswap " & _
            '       "FROM cmexpapprover_master " & _
            '       "	JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
            '       "	JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
            '       "    JOIN " & _
            '       "   ( " & _
            '       "        SELECT " & _
            '       "            jobunkid " & _
            '       "            ,employeeunkid " & _
            '       "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '       "        FROM hremployee_categorization_tran " & _
            '       "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
            '           ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
            '       "	JOIN hrjob_master ON  hrjob_master.jobunkid = Jobs.jobunkid " & _
            '       "    JOIN " & _
            '       "   ( " & _
            '       "        SELECT " & _
            '       "         departmentunkid " & _
            '       "        ,employeeunkid " & _
            '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '       "    FROM hremployee_transfer_tran " & _
            '       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
            '       "  ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
            '       "	JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
            '       "	LEFT JOIN hrapprover_usermapping ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & " " & _
            '       "	LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid "

            strQ = "SELECT " & _
                   "	 cmexpapprover_master.crapproverunkid " & _
                   "	,cmexpapprover_master.expensetypeid " & _
                   "	,cmexpapprover_master.crlevelunkid " & _
                   "	,cmexpapprover_master.employeeunkid " & _
                   "	,cmexpapprover_master.userunkid " & _
                   "	,cmexpapprover_master.isvoid " & _
                   "	,cmexpapprover_master.voiddatetime " & _
                   "	,cmexpapprover_master.voidreason " & _
                   "	,cmexpapprover_master.voiduserunkid " & _
                   "	, #APPR_NAME#   AS ename " & _
                   "	, #DEPT_NAME#  AS department " & _
                   "	, #JOB_NAME#  AS jobname " & _
                   "	,cmapproverlevel_master.crlevelname AS clevel " & _
                   "	,ISNULL(Usr.username,'') AS usermapped " & _
                   "    ,CASE WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                   "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                   "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                   "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                   "          WHEN cmexpapprover_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
                   "    ,cmexpapprover_master.isswap " & _
                   "   ,CASE WHEN cmexpapprover_master.isexternalapprover = 1 THEN @YES ELSE @NO END AS ExAppr " & _
                   "   ,cmexpapprover_master.isexternalapprover " & _
                   "FROM cmexpapprover_master " & _
                   "	JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
                   "	LEFT JOIN hrapprover_usermapping ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & " " & _
                   "	LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
                   " #EMPL_JOIN# " & _
                   " #DATA_JOIN# "

            StrFinalQurey = strQ

            'Gajanan [23-SEP-2019] -- Add [isexternalapprover]    

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If
            'Pinkal (05-Sep-2020) -- End


            StrQCondition &= " WHERE 1 = 1 AND cmexpapprover_master.isexternalapprover = #ExAppr# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            StrQCondition &= " AND cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 " & _
                                      " AND cmexpapprover_master.isactive = " & IIf(blnIsActiveApprover, "1", "0") & " "

            If intApproverTranUnkid > 0 Then
                StrQCondition &= " AND cmexpapprover_master.crapproverunkid = '" & intApproverTranUnkid & "' "
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= "AND " & mstrFilter
            End If


            Dim StrDataJoin As String = " LEFT JOIN " & _
                   "   ( " & _
                   "        SELECT " & _
                   "            jobunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                       "        FROM #DB_Name#hremployee_categorization_tran " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                       ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                       "	JOIN #DB_Name#hrjob_master ON  hrjob_master.jobunkid = Jobs.jobunkid " & _
                                                       "    LEFT JOIN " & _
                   "   ( " & _
                   "        SELECT " & _
                   "         departmentunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                       "    FROM #DB_Name#hremployee_transfer_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                   "  ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                                       "	JOIN #DB_Name#hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid "
                                                     



            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master on hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid ")
            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#DB_Name#", "")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")
            strQ &= StrQCondition
            strQ &= StrQDtFilters
            strQ = strQ.Replace("#ExAppr#", "0")


            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Yes"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "No"))


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (11-Sep-2019) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet
            dsCompany = GetClaimExternalApproverList(objDataOperation, "List", True)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey : StrQDtFilters = ""
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    strQ = strQ.Replace("#ExAppr#", "1")
                    strQ = strQ.Replace("#DB_Name#", "")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#SEC_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate")), dr("DName"))

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
            End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                End If
                    'Pinkal (05-Sep-2020) -- End

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                            StrQDtFilters &= xDateFilterQry & " "
                End If
            End If
            End If

                strQ &= StrQCondition
                strQ &= StrQDtFilters
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

                objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
                'Pinkal (11-Sep-2019) -- End

                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Yes"))
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "No"))

                dstmp = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If

            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmexpapprover_master) </purpose>
    Public Function Insert(ByVal dtAccess As DataTable, Optional ByVal dtMapping As DataTable = Nothing) As Boolean


        'Pinkal (01-Mar-2016) -- Start
        'Enhancement - Implementing External Approver in Claim Request & Leave Module.
        'If isExist(mintExpensetypeid, mintEmployeeunkid, mintcrLevelunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Expense Approver already exists. Please define new Expense Approver.")
        '    Return False
        'End If
        If isExist(mintExpensetypeid, mintEmployeeunkid, mintcrLevelunkid, , mblnIsexternalapprover) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Expense Approver already exists. Please define new Expense Approver.")
            Return False
        End If
        'Pinkal (01-Mar-2016) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrLevelunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)


            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            'Shani(17-Aug-2015) -- End

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap.ToString)


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            'Pinkal (01-Mar-2016) -- End



            strQ = "INSERT INTO cmexpapprover_master ( " & _
                       "  expensetypeid " & _
                       ", crlevelunkid " & _
                       ", employeeunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", voiduserunkid" & _
                       ",isswap " & _
                       ",isactive " & _
                       ",isexternalapprover " & _
                   ") VALUES (" & _
                       "  @expensetypeid " & _
                       ", @crlevelunkid " & _
                       ", @employeeunkid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @voiduserunkid" & _
                       ", @isswap " & _
                       ", @isactive " & _
                       ", @isexternalapprover " & _
                   "); SELECT @@identity"

            'Shani(17-Aug-2015) -- [isactive]
            
            'Pinkal (19-Mar-2015) -- End


            'Pinkal (01-Mar-2016) 'Enhancement - Implementing External Approver in Claim Request & Leave Module. [isexternalapprover] 


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintcrApproverunkid = dsList.Tables(0).Rows(0).Item(0)

            objExpenseApprTran._ExpApproverMasterId = mintcrApproverunkid
            objExpenseApprTran._DataTable = dtAccess.Copy

            With objExpenseApprTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objExpenseApprTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'MULTIPLE EXPENSES IN SINGLE CLAIM FORM, HOW TO APPROVE EXPENSES IF EACH EXPENSE MAPPED TO DIFFERENT APPROVERS. -- DISCUSSED WITH ANDREW ON 05-MAR-2014
            'If dtMapping IsNot Nothing Then
            '    objExpenseMapping._ExpenseTypeId = mintExpensetypeid
            '    objExpenseMapping._UserId = mintUserunkid
            '    objExpenseMapping._ExApproverUnkid = mintcrApproverunkid
            '    objExpenseMapping._DataTable = dtMapping.Copy

            '    If objExpenseMapping.Insert_Update_Delete(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            'End If

            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.crApprover, mintcrApproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintcrApproverunkid
            objUserMapping._Userunkid = mintMappedUserId
            objUserMapping._UserTypeid = enUserType.crApprover
            objUserMapping._AuditUserunkid = mintUserunkid
            With objUserMapping
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmexpapprover_master) </purpose>
    Public Function Update(ByVal dtAccess As DataTable, Optional ByVal dtMapping As DataTable = Nothing) As Boolean

        'Pinkal (01-Mar-2016) -- Start
        'Enhancement - Implementing External Approver in Claim Request & Leave Module.

        'If isExist(mintExpensetypeid, mintEmployeeunkid, mintcrLevelunkid, mintcrApproverunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Expense Approver already exists. Please define new Expense Approver.")
        '    Return False
        'End If

        If isExist(mintExpensetypeid, mintEmployeeunkid, mintcrLevelunkid, mintcrApproverunkid, mblnIsexternalapprover) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Expense Approver already exists. Please define new Expense Approver.")
            Return False
        End If

        'Pinkal (01-Mar-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrLevelunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap.ToString)


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            'Pinkal (01-Mar-2016) -- End


            strQ = "UPDATE cmexpapprover_master SET " & _
                      "  expensetypeid = @expensetypeid" & _
                      ", crlevelunkid = @crlevelunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", isswap = @isswap" & _
                      ", isexternalapprover = @isexternalapprover" & _
                   " WHERE crapproverunkid = @crapproverunkid "

            'Pinkal (19-Mar-2015) -- End


            'Pinkal (01-Mar-2016) 'Enhancement - Implementing External Approver in Claim Request & Leave Module. [isexternalapprover = @isexternalapprover] 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objExpenseApprTran._ExpApproverMasterId = mintcrApproverunkid
            objExpenseApprTran._DataTable = dtAccess.Copy

            With objExpenseApprTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objExpenseApprTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'MULTIPLE EXPENSES IN SINGLE CLAIM FORM, HOW TO APPROVE EXPENSES IF EACH EXPENSE MAPPED TO DIFFERENT APPROVERS. -- DISCUSSED WITH ANDREW ON 05-MAR-2014
            'If dtMapping IsNot Nothing Then
            '    objExpenseMapping._ExpenseTypeId = mintExpensetypeid
            '    objExpenseMapping._UserId = mintUserunkid
            '    objExpenseMapping._ExApproverUnkid = mintcrApproverunkid
            '    objExpenseMapping._DataTable = dtMapping.Copy

            '    If objExpenseMapping.Insert_Update_Delete(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If


            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.crApprover, mintcrApproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintcrApproverunkid
            objUserMapping._Userunkid = mintMappedUserId
            objUserMapping._UserTypeid = enUserType.crApprover
            objUserMapping._AuditUserunkid = mintUserunkid

            With objUserMapping
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> DELETE FROM Database Table (cmexpapprover_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        Try
            StrQ = "UPDATE cmexpapprover_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   "WHERE crapproverunkid = @crapproverunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cmexpapprover_master", "crapproverunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'dsList = clsCommonATLog.GetChildList(objDataOperation, "cmexpapprover_tran", "crapproverunkid", intUnkid)

            'Pinkal (01-Mar-2016) -- End

            StrQ = "UPDATE cmexpapprover_tran SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   "WHERE crapproverunkid = @crapproverunkid "


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If dsList.Tables(0).Rows.Count > 0 Then
            '    For Each dtRow As DataRow In dsList.Tables(0).Rows
            '        objDataOperation.ClearParameters()
            '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            '        objDataOperation.AddParameter("@crapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("crapprovertranunkid"))

            '        Call objDataOperation.ExecNonQuery(StrQ)

            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If

            '        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cmexpapprover_tran", "crapprovertranunkid", dtRow.Item("crapprovertranunkid")) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If

            '    Next
            'End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "cmexpapprover_master", "crapproverunkid", intUnkid, "cmexpapprover_tran", "crapprovertranunkid", 3, 3, "", "", False, mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END




            StrQ = "SELECT mappingunkid FROM hrapprover_usermapping WHERE approverunkid = '" & intUnkid & "' AND usertypeid = '" & enUserType.crApprover & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", dtRow.Item("mappingunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    StrQ = "DELETE FROM hrapprover_usermapping WHERE mappingunkid = '" & dtRow.Item("mappingunkid") & "' "

                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If

            objCommonATLog = New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "cmapprover_expense_mapping", "crapproverunkid", intUnkid)
            objCommonATLog = Nothing
            StrQ = "UPDATE cmapprover_expense_mapping SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   "WHERE expensemappingunkid = @expensemappingunkid "

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@expensemappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("expensemappingunkid"))

                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cmapprover_expense_mapping", "expensemappingunkid", dtRow.Item("expensemappingunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                Next
            End If

            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            DisplayError.Show("-1", ex.Message, "Delete", mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        mstrMessage = ""
        Try
            strQ = "SELECT " & _
                   "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='crapproverunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                Select Case dtRow.Item("TableName")
                    Case "cmapprover_expense_mapping", "cmexpapprover_master", "cmexpapprover_tran"
                        Continue For
                End Select
                strQ = "SELECT crapproverunkid FROM " & dtRow.Item("TableName").ToString & " WHERE crapproverunkid = @crapproverunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry you cannot delete this approver. Reason this approver is already linked with some transaction(s).")
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iExpenseTypeId As Integer, _
                            ByVal iEmployeeId As Integer, _
                            ByVal iLevelId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal blnIsExternalApprover As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (01-Mar-2016) -- 'Enhancement - Implementing External Approver in Claim Request & Leave Module.[Optional ByVal blnIsExternalApprover As Boolean = False]


        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  crapproverunkid " & _
                   ", expensetypeid " & _
                   ", crlevelunkid " & _
                   ", employeeunkid " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", voiduserunkid " & _
                   ", isexternalapprover " & _
                   "FROM cmexpapprover_master " & _
                   "WHERE isvoid = 0 " & _
                   "  AND expensetypeid = @expensetypeid " & _
                   "  AND crlevelunkid = @crlevelunkid " & _
                   "  AND employeeunkid = @employeeunkid "


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            strQ &= " AND isswap = 0 "
            'Pinkal (19-Mar-2015) -- End


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            strQ &= " AND isexternalapprover = @isexternalapprover "
            'Pinkal (01-Mar-2016) -- End



            If intUnkid > 0 Then
                strQ &= " AND crapproverunkid <> @crapproverunkid"
                objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iLevelId)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpenseTypeId)


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)
            'Pinkal (01-Mar-2016) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '' <summary>
    '' Modify By: Sandeep Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Function getListForCombo(ByVal iExTypeId As Integer, _
    '                                Optional ByVal iFlag As Boolean = False, _
    '                                Optional ByVal iList As String = "List", _
    '                                Optional ByVal iExcludeAppr As String = "", _
    '                                Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                                Optional ByVal strEmployeeAsOnDate As String = "") As DataSet
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim strQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        If iFlag = True Then
    '            strQ = " SELECT 0 AS Id, @Select AS NAME, 0 AS ApproverEmpunkid UNION "
    '        End If
    '        strQ &= "SELECT " & _
    '                "   crapproverunkid AS Id, employeecode+' - '+firstname+' ' +surname , cmexpapprover_master.employeeunkid as  ApproverEmpunkid " & _
    '                "FROM cmexpapprover_master " & _
    '                "   JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE cmexpapprover_master.expensetypeid = '" & iExTypeId & "' AND cmexpapprover_master.isvoid = 0 "

    '        If iExcludeAppr.Trim.Length > 0 Then
    '            strQ &= " AND cmexpapprover_master.crapproverunkid NOT IN(" & iExcludeAppr & ") "
    '        End If


    '        'Pinkal (19-Mar-2015) -- Start
    '        'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
    '        strQ &= " AND isswap = 0 "
    '        'Pinkal (19-Mar-2015) -- End


    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

    '        dsList = objDataOperation.ExecQuery(strQ, iList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "getListForCombo", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function



    'Gajanan [23-May-2020] -- Start
    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

    'Public Function getListForCombo(ByVal iExTypeId As Integer, _
    '                            Optional ByVal iFlag As Boolean = False, _
    '                            Optional ByVal iList As String = "List", _
    '                            Optional ByVal iExcludeAppr As String = "", _
    '                            Optional ByVal blnIsActive As Boolean = True) As DataSet

    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim strQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try

    '        'Pinkal (01-Mar-2016) -- Start
    '        'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    '        Dim strFlagQuery As String = ""
    '        Dim StrFinalQurey As String = String.Empty
    '        Dim StrQCondition As String = String.Empty
    '        'Pinkal (01-Mar-2016) -- End

    '        If iFlag = True Then
    '            strFlagQuery = " SELECT 0 AS Id, @Select AS NAME, 0 AS ApproverEmpunkid UNION "
    '        End If


    '        'Pinkal (01-Mar-2016) -- Start
    '        'Enhancement - Implementing External Approver in Claim Request & Leave Module.

    '        'strQ &= "SELECT " & _
    '        '        "   crapproverunkid AS Id, employeecode+' - '+firstname+' ' +surname , cmexpapprover_master.employeeunkid as  ApproverEmpunkid " & _
    '        '        "FROM cmexpapprover_master " & _
    '        '        "   JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE cmexpapprover_master.expensetypeid = '" & iExTypeId & "' AND cmexpapprover_master.isvoid = 0 "

    '        strQ &= "SELECT " & _
    '                    " crapproverunkid AS Id, #APPR_NAME# AS NAME , cmexpapprover_master.employeeunkid as  ApproverEmpunkid " & _
    '                "FROM cmexpapprover_master " & _
    '                    " #EMPL_JOIN# "

    '        StrFinalQurey = strQ

    '        strQ = strFlagQuery & StrFinalQurey

    '        StrQCondition &= " WHERE cmexpapprover_master.expensetypeid = '" & iExTypeId & "' AND cmexpapprover_master.isvoid = 0  AND cmexpapprover_master.isexternalapprover = #isExternal# "

    '        If blnIsActive Then
    '            StrQCondition &= " AND cmexpapprover_master.isactive = 1 "
    '        Else
    '            StrQCondition &= " AND cmexpapprover_master.isactive = 0 "
    '        End If

    '        If iExcludeAppr.Trim.Length > 0 Then
    '            StrQCondition &= " AND cmexpapprover_master.crapproverunkid NOT IN(" & iExcludeAppr & ") "
    '        End If

    '        StrQCondition &= " AND isswap = 0 "

    '        strQ = strQ.Replace("#APPR_NAME#", " ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
    '        strQ = strQ.Replace("#EMPL_JOIN#", " JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid ")
    '        strQ &= StrQCondition.Replace("#isExternal# ", "0")

    '        'Pinkal (01-Mar-2016) -- End



    '        'Pinkal (24-Aug-2015) -- Start
    '        'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '        'If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '        '    strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        'End If

    '        'If CBool(strIncludeInactiveEmployee) = False Then
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        'End If

    '        'Pinkal (24-Aug-2015) -- End

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

    '        dsList = objDataOperation.ExecQuery(strQ, iList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsCompany As New DataSet
    '        dsCompany = GetClaimExternalApproverList(objDataOperation, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dr In dsCompany.Tables(0).Rows
    '            strQ = StrFinalQurey
    '            Dim dstmp As New DataSet
    '            If dr("DName").Trim.Length <= 0 Then
    '                strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
    '                strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
    '            Else
    '                strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
    '                                                   "ELSE ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') END ")
    '                strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
    '                                                   "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
    '                strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")

    '            End If

    '            strQ &= StrQCondition.Replace("#isExternal# ", "1")

    '            strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
    '            dstmp = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables.Count <= 0 Then
    '                dsList.Tables.Add(dstmp.Tables(0).Copy)
    '            Else
    '                dsList.Tables(0).Merge(dstmp.Tables(0), True)
    '            End If
    '        Next


    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "getListForCombo", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function


    Public Function getListForCombo(ByVal iExTypeId As Integer, _
                                    ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, _
                                    ByVal strEmployeeAsOnDate As String, _
                                    ByVal xUserModeSetting As String, _
                                    Optional ByVal iFlag As Boolean = False, _
                                    Optional ByVal iList As String = "List", _
                                    Optional ByVal iExcludeAppr As String = "", _
                                    Optional ByVal blnIsActive As Boolean = True, _
                                    Optional ByVal mblnIncludeInActiveEmployee As Boolean = True) As DataSet

 'Gajanan [23-May-2020] -- 'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.[  ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal strEmployeeAsOnDate As String,ByVal xUserModeSetting As String ]


        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            Dim strFlagQuery As String = ""
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            'Pinkal (01-Mar-2016) -- End



            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)
            'Gajanan [23-May-2020] -- End



            If iFlag = True Then
                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'strFlagQuery = " SELECT 0 AS Id, @Select AS NAME, 0 AS ApproverEmpunkid UNION "
                strFlagQuery = " SELECT 0 AS Id, @Select AS NAME, 0 AS ApproverEmpunkid,CAST(0 AS BIT) AS isexternalapprover UNION "
                'Pinkal (18-Jun-2020) -- End

            End If


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'strQ &= "SELECT " & _
            '           " crapproverunkid AS Id, #APPR_NAME# AS NAME , cmexpapprover_master.employeeunkid as  ApproverEmpunkid" & _
            '       "FROM cmexpapprover_master " & _
            '           " #EMPL_JOIN# "

            strQ &= "SELECT " & _
                        " crapproverunkid AS Id, #APPR_NAME# AS NAME , cmexpapprover_master.employeeunkid as  ApproverEmpunkid, cmexpapprover_master.isexternalapprover " & _
                        " FROM cmexpapprover_master " & _
                        " #EMPL_JOIN# "
            'Pinkal (18-Jun-2020) -- End



            StrFinalQurey = strQ

            strQ = strFlagQuery & StrFinalQurey


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Gajanan [23-May-2020] -- End


            StrQCondition &= " WHERE cmexpapprover_master.expensetypeid = '" & iExTypeId & "' AND cmexpapprover_master.isvoid = 0  AND cmexpapprover_master.isexternalapprover = #isExternal# "


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.


            'Pinkal (31-Dec-2021)-- Start
            'Voltamp Approver Migration Issue.
            'If mblnIncludeInActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQCondition &= xDateFilterQry & " "
            '    End If
            'End If

            If mblnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (31-Dec-2021) -- End

            'Gajanan [23-May-2020] -- End


            If blnIsActive Then
                StrQCondition &= " AND cmexpapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND cmexpapprover_master.isactive = 0 "
            End If

            If iExcludeAppr.Trim.Length > 0 Then
                StrQCondition &= " AND cmexpapprover_master.crapproverunkid NOT IN(" & iExcludeAppr & ") "
            End If

            StrQCondition &= " AND isswap = 0 "

            strQ = strQ.Replace("#APPR_NAME#", " ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", " JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid ")
            strQ &= StrQCondition.Replace("#isExternal# ", "0")


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet
            dsCompany = GetClaimExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")

                Else
                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")

                End If


                'Pinkal (31-Dec-2021)-- Start
                'Voltamp Approver Migration Issue.
                If dr("DName").Trim.Length > 0 Then
                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If
                'Gajanan [23-May-2020] -- End
                End If
                'Pinkal (31-Dec-2021)-- End

                strQ &= StrQCondition.Replace("#isExternal# ", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")


                'Pinkal (31-Dec-2021)-- Start
                'Voltamp Approver Migration Issue.

                ''Gajanan [23-May-2020] -- Start
                ''Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'If mblnIncludeInActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrQCondition &= xDateFilterQry & " "
                '    End If
                'End If
                ''Gajanan [23-May-2020] -- End

                If dr("DName").Trim.Length > 0 Then
                If mblnIncludeInActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry & " "
                    End If
                End If
                End If
                'Pinkal (31-Dec-2021)-- End

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
                dstmp = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next


            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'Gajanan [23-May-2020] -- End




    'Pinkal (24-Aug-2015) -- End


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 

    'Pinkal (18-Jun-2020) -- Start
    'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
    'Public Function GetExpApproverLevels(ByVal iExpTypeId As Integer, ByVal iApproverID As Integer, ByVal iExpApprEmpId As Integer, Optional ByVal iFlag As Boolean = False, Optional ByVal iList As String = "List") As DataSet
    Public Function GetExpApproverLevels(ByVal iExpTypeId As Integer, ByVal iApproverID As Integer, ByVal iExpApprEmpId As Integer _
                                                          , ByVal blnExternalApprover As Boolean, Optional ByVal iFlag As Boolean = False _
                                                          , Optional ByVal iList As String = "List") As DataSet
        'Pinkal (18-Jun-2020) -- End


        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If iFlag = True Then
                strQ = " SELECT 0 AS Id, @Select AS NAME UNION "
            End If

            strQ &= "SELECT " & _
                    "   cmapproverlevel_master.crlevelunkid AS Id, crlevelname AS NAME " & _
                    "FROM cmexpapprover_master " & _
                    "   JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
                    "WHERE cmexpapprover_master.expensetypeid = '" & iExpTypeId & "' AND employeeunkid = '" & iExpApprEmpId & "' AND cmexpapprover_master.isvoid = 0 "


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            If iApproverID > 0 Then
            strQ &= " AND cmexpapprover_master.crapproverunkid = " & iApproverID
            End If

            strQ &= " AND cmexpapprover_master.isexternalapprover = " & IIf(blnExternalApprover, 1, 0)

            'Pinkal (18-Jun-2020) -- End

            strQ &= " AND isswap = 0 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetExpApproverLevels", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function



    'Gajanan [23-May-2020] -- Start
    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

    'Public Function GetApproverAccess(ByVal iApproverunkid As Integer, ByVal iClaimApproverEmpUnkid As Integer, ByVal iLevel As Integer, ByVal mdtEmployeeAsonDate As Date) As DataSet
    Public Function GetApproverAccess(ByVal xDatabaseName As String, ByVal iApproverunkid As Integer, ByVal iClaimApproverEmpUnkid As Integer, ByVal iLevel As Integer, ByVal mdtEmployeeAsonDate As Date, Optional ByVal mblnIncludeInActiveEmployee As Boolean = True) As DataSet
        'Gajanan [23-May-2020] -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)
            'Gajanan [23-May-2020] -- End


            strQ = "SELECT " & _
                   "  CAST(0 AS BIT) as 'ischeck' " & _
                   " ,cmexpapprover_master.crapproverunkid " & _
                   " ,cmexpapprover_tran.crapprovertranunkid " & _
                   " ,cmexpapprover_master.employeeunkid AS approverempid " & _
                   " ,cmexpapprover_tran.employeeunkid " & _
                   " ,ISNULL(hremployee_master.employeecode,'') as ecode " & _
                   " ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') as ename " & _
                   " ,cmapproverlevel_master.crlevelunkid " & _
                   " ,crlevelname " & _
                   " ,crpriority " & _
                   ", hremployee_master.gender " & _
                  ", hremployee_master.maritalstatusunkid " & _
                  ", hremployee_master.employmenttypeunkid " & _
                  ", hremployee_master.paytypeunkid " & _
                  ", hremployee_master.paypointunkid " & _
                  ", hremployee_master.nationalityunkid " & _
                  ", hremployee_master.religionunkid " & _
                  ", Alloc.stationunkid " & _
                  ", Alloc.deptgroupunkid " & _
                  ", Alloc.departmentunkid " & _
                  ", ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                  ", Alloc.sectionunkid " & _
                  ", ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                  ", Alloc.unitunkid " & _
                  ", Alloc.classgroupunkid " & _
                  ", Alloc.classunkid " & _
                  ", hremployee_master.jobgroupunkid " & _
                  ", hremployee_master.jobunkid " & _
                  ", hremployee_master.gradegroupunkid " & _
                  ", hremployee_master.gradeunkid " & _
                  ", hremployee_master.gradelevelunkid " & _
                  ", hremployee_master.costcenterunkid " & _
                  ", ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
                  ", ''  AS  AUD " & _
                  ", cmexpapprover_tran.isvoid " & _
                  ", cmexpapprover_tran.voiddatetime  " & _
                  ", cmexpapprover_tran.voidreason " & _
                  ", cmexpapprover_tran.voiduserunkid " & _
                  ", cmexpapprover_tran.crapproverunkid " & _
                  ", cmexpapprover_tran.crapprovertranunkid " & _
                  ", cmexpapprover_tran.userunkid " & _
                   "FROM cmexpapprover_master " & _
                   " JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
                   " JOIN cmexpapprover_tran ON cmexpapprover_master.crapproverunkid = cmexpapprover_tran.crapproverunkid " & _
                   " JOIN hremployee_master ON cmexpapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                  " LEFT JOIN " & _
                  "( " & _
                  "    SELECT " & _
                  "         stationunkid " & _
                  "        ,deptgroupunkid " & _
                  "        ,departmentunkid " & _
                  "        ,sectiongroupunkid " & _
                  "        ,sectionunkid " & _
                  "        ,unitgroupunkid " & _
                  "        ,unitunkid " & _
                  "        ,teamunkid " & _
                  "        ,classgroupunkid " & _
                  "        ,classunkid " & _
                  "        ,employeeunkid " & _
                  "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                  "    FROM hremployee_transfer_tran " & _
                  "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                  " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                  "  LEFT JOIN " & _
                  " ( " & _
                  "    SELECT " & _
                  "         jobunkid " & _
                  "        ,jobgroupunkid " & _
                  "        ,employeeunkid " & _
                  "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                  "    FROM hremployee_categorization_tran " & _
                  "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                  " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                  " JOIN " & _
                  " ( " & _
                  "    SELECT " & _
                  "         gradegroupunkid " & _
                  "        ,gradeunkid " & _
                  "        ,gradelevelunkid " & _
                  "        ,employeeunkid " & _
                  "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                  "    FROM prsalaryincrement_tran " & _
                  "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                  "  ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                  " LEFT JOIN " & _
                  " ( " & _
                  "     SELECT " & _
                  "         cctranheadvalueid as costcenterunkid " & _
                  "        ,employeeunkid " & _
                  "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                  "    FROM hremployee_cctranhead_tran " & _
                  "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                  "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                  " ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "

            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            strQ &= "WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.employeeunkid = '" & iClaimApproverEmpUnkid & "' "


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            If iApproverunkid > 0 Then
                strQ &= " AND cmexpapprover_master.crapproverunkid = '" & iApproverunkid & "' "
            End If


            strQ &= " AND cmapproverlevel_master.crlevelunkid = '" & iLevel & "' AND hremployee_master.isapproved = 1 AND isswap = 0  AND cmexpapprover_tran.isvoid = 0 "

            'Pinkal (18-Jun-2020) -- End


            If mblnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If
            'Gajanan [23-May-2020] -- End


            strQ &= " ORDER By crlevelname,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverAccess", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (01-Mar-2016) -- End 

    

    'Pinkal (19-Mar-2015) -- End


    'Pinkal (15-Sep-2017) -- Start
    'Enhancement - Solving Cancel Expense Issue.

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeApprovers(ByVal iExTypeId As Integer, ByVal iEmpId As Integer _
                                                            , Optional ByVal iList As String = "List", Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                                            , Optional ByVal xClaimRequestMstID As Integer = -1, Optional ByVal xSkipExtType As Boolean = False) As DataSet

        'Pinkal (15-Sep-2017) --  'Enhancement - Solving Cancel Expense Issue.[   , Optional ByVal xClaimRequestMstID As Integer = -1]

        'Gajanan [23-SEP-2019] -- Add [xSkipExtType] For Get All Expense Approver Without Passing ExpenseType
        'Enhancement:Enforcement of approval migration From Transfer and recategorization.

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If

        Try

            objDataOperation.ClearParameters()

            If iEmpId > 0 AndAlso xClaimRequestMstID <= 0 Then


                'StrQ = "SELECT " & _
                '       "  cmexpapprover_master.crapproverunkid " & _
                '       " ,cmexpapprover_master.employeeunkid " & _
                '       " ,cmapproverlevel_master.crlevelunkid " & _
                '       " ,cmapproverlevel_master.crpriority " & _
                '       "FROM cmexpapprover_tran " & _
                '       " JOIN cmexpapprover_master ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid " & _
                '       " JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
                '           "WHERE cmexpapprover_tran.isvoid = 0 AND cmexpapprover_tran.employeeunkid = @EmployeeID " & _
                '           " AND cmexpapprover_master.expensetypeid = @ExpenseTypeID AND cmexpapprover_master.isvoid = 0 "


                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.

            StrQ = "SELECT " & _
                   "  cmexpapprover_master.crapproverunkid " & _
                   " ,cmexpapprover_master.employeeunkid " & _
                   " ,cmapproverlevel_master.crlevelunkid " & _
                   " ,cmapproverlevel_master.crpriority " & _
                   "FROM cmexpapprover_tran " & _
                   " JOIN cmexpapprover_master ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid " & _
                   " JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid " & _
                       "WHERE cmexpapprover_tran.isvoid = 0 AND cmexpapprover_tran.employeeunkid = @EmployeeID " & _
                          " AND cmexpapprover_master.isvoid = 0 "

                If xSkipExtType = False Then
                    StrQ &= "AND cmexpapprover_master.expensetypeid = @ExpenseTypeID"
                    objDataOperation.AddParameter("@ExpenseTypeID", SqlDbType.Int, eZeeDataType.INT_SIZE, iExTypeId)
                End If

                'Gajanan [23-SEP-2019] -- End


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            StrQ &= " AND cmexpapprover_master.isswap = 0 "
            'Pinkal (19-Mar-2015) -- End


            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            StrQ &= " AND cmexpapprover_master.isactive = 1 "
            'Shani(17-Aug-2015) -- End


            ElseIf iEmpId > 0 AndAlso xClaimRequestMstID > 0 Then

                StrQ = " SELECT " & _
                          "  cmclaim_approval_tran.crapprovaltranunkid " & _
                          "  ,cmexpapprover_master.crapproverunkid " & _
                          "  ,cmexpapprover_master.employeeunkid " & _
                          "  ,cmapproverlevel_master.crpriority " & _
                          "  ,cmclaim_approval_tran.statusunkid " & _
                          " FROM cmclaim_approval_tran " & _
                          " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                          " JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                          " JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                          " WHERE cmclaim_request_master.employeeunkid = @EmployeeID AND cmclaim_request_master.crmasterunkid = @CrMasterID " & _
                          " AND cmclaim_approval_tran.isvoid = 0 "

                objDataOperation.AddParameter("@CrMasterID", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimRequestMstID)

            End If

            objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpId)
            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (15-Sep-2017) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function SwapApprover(ByVal dtFromEmployee As DataTable, ByVal dtToEmployee As DataTable) As Boolean
    Public Function SwapApprover(ByVal dtFromEmployee As DataTable, ByVal dtToEmployee As DataTable, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try


            '==================================START FOR FROM APPROVER=====================================

            Dim objClaimForm As New clsclaim_request_master
            Dim mstrFormId As String = ""

            If dtFromEmployee IsNot Nothing Then

                strQ = "UPDATE cmexpapprover_master SET isswap = 1 WHERE crapproverunkid = @approverunkid and isswap = 0  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpapprover_master", "crapproverunkid", mintSwapFromApproverID) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


                _crApproverunkid = mintSwapFromApproverID

                strQ = "INSERT INTO cmexpapprover_master ( " & _
                            "  expensetypeid " & _
                            ", crlevelunkid " & _
                            ", employeeunkid " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", voiduserunkid" & _
                            ",isswap " & _
                            ",isactive " & _
                            ", isexternalapprover " & _
                        ") VALUES (" & _
                            "  @expensetypeid " & _
                            ", @levelunkid " & _
                            ", @employeeunkid " & _
                            ", @userunkid " & _
                            ", @isvoid " & _
                            ", @voiddatetime " & _
                            ", @voidreason " & _
                            ", @voiduserunkid" & _
                            ", @isswap " & _
                            ", @isactive " & _
                            ", @isexternalapprover " & _
                        "); SELECT @@identity"

                'Shani(17-Aug-2015) -- [isactive]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrLevelunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                'Shani(17-Aug-2015) -- End


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover)
                'Pinkal (01-Mar-2016) -- End


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintcrApproverunkid = dsList.Tables(0).Rows(0).Item(0)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cmexpapprover_master", "crapproverunkid", mintcrApproverunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END



                strQ = " SELECT  ISNULL(mappingunkid,0) as mappingunkid,userunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.crApprover
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    strQ = " DELETE FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.crApprover
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                    objDataOperation.ExecNonQuery(strQ)


                    Dim objUserMapping As New clsapprover_Usermapping
                    objUserMapping._Approverunkid = mintcrApproverunkid
                    objUserMapping._Userunkid = CInt(dsList.Tables(0).Rows(0)("userunkid"))
                    objUserMapping._UserTypeid = enUserType.crApprover

                    With objUserMapping
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With

                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objUserMapping = Nothing

                End If


                dsList = Nothing
                For Each drRow As DataRow In dtFromEmployee.Rows

                    strQ = " UPDATE cmexpapprover_tran SET crapproverunkid = @newapproverunkid WHERE isvoid = 0 AND crapproverunkid = @oldapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid)
                    objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmexpapprover_master", "crapproverunkid", mintcrApproverunkid, "cmexpapprover_tran", "crapprovertranunkid", CInt(drRow("crapprovertranunkid")), 2, 2, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    mstrFormId = objClaimForm.GetApproverPendingClaimForm(mintSwapToApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If mstrFormId.Trim.Length > 0 Then

                        strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newapproverunkid,crapproverunkid = @newapprovertranunkid  WHERE crmasterunkid in (" & mstrFormId & ") AND crapproverunkid = @approvertranunkid AND cmclaim_approval_tran.isvoid = 0 "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran WHERE approveremployeeunkid = @approverunkid AND crapproverunkid = @approvertranunkid AND crmasterunkid  in (" & mstrFormId & ") AND isvoid = 0"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Next

                        End If

                    End If

                    mstrEmployeeID &= drRow("employeeunkid").ToString() & ","

                Next

                If mstrEmployeeID.Trim.Length > 0 Then
                    mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
                End If

                Dim objMigration As New clsMigration

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationdate = dtCurrentDateAndTime
                'Shani(24-Aug-2015) -- End

                objMigration._Migrationfromunkid = mintSwapFromApproverID
                objMigration._Migrationtounkid = mintSwapToApproverID
                objMigration._Usertypeid = enUserType.crApprover
                objMigration._Userunkid = mintUserunkid
                objMigration._Migratedemployees = mstrEmployeeID

                'Pinkal (07-Jan-2019) -- Start
                'AT Testing- Working on AT Testing for All Modules.
                objMigration._FormName = mstrFormName
                objMigration._LoginEmployeeunkid = mintLoginEmployeeunkid
                objMigration._ClientIP = mstrClientIP
                objMigration._HostName = mstrHostName
                objMigration._FromWeb = mblnIsWeb
                objMigration._AuditUserId = mintAuditUserId
                objMigration._CompanyUnkid = mintCompanyUnkid
                objMigration._AuditDate = mdtAuditDate
                'Pinkal (07-Jan-2019) -- End

                If objMigration.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            End If

            '==================================END FOR FROM APPROVER=====================================

            '==================================START FOR TO APPROVER=====================================


            mstrFormId = ""
            mstrEmployeeID = ""

            If dtToEmployee IsNot Nothing Then

                strQ = "UPDATE cmexpapprover_master SET isswap = 1 WHERE crapproverunkid = @approverunkid and isswap = 0  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpapprover_master", "crapproverunkid", mintSwapToApproverID) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


                _crApproverunkid = mintSwapToApproverID

                strQ = "INSERT INTO cmexpapprover_master ( " & _
                          "  expensetypeid " & _
                          ", crlevelunkid " & _
                          ", employeeunkid " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", voidreason " & _
                          ", voiduserunkid" & _
                          ",isswap " & _
                          ", isactive " & _
                          ", isexternalapprover " & _
                      ") VALUES (" & _
                          "  @expensetypeid " & _
                          ", @levelunkid " & _
                          ", @employeeunkid " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiddatetime " & _
                          ", @voidreason " & _
                          ", @voiduserunkid" & _
                          ", @isswap " & _
                          ", @isactive " & _
                          ", @isexternalapprover " & _
                      "); SELECT @@identity"


                'Pinkal (01-Mar-2016) -- 'Enhancement - Implementing External Approver in Claim Request & Leave Module.[@isexternalapprover]

                'Shani(17-Aug-2015) -- [isactive]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrLevelunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                'Shani(17-Aug-2015) -- End


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover)
                'Pinkal (01-Mar-2016) -- End


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintcrApproverunkid = dsList.Tables(0).Rows(0).Item(0)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cmexpapprover_master", "crapproverunkid", mintcrApproverunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END



                strQ = " SELECT  ISNULL(mappingunkid,0) as mappingunkid,userunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.crApprover
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    strQ = " DELETE FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.crApprover
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                    objDataOperation.ExecNonQuery(strQ)


                    Dim objUserMapping As New clsapprover_Usermapping
                    objUserMapping._Approverunkid = mintcrApproverunkid
                    objUserMapping._Userunkid = CInt(dsList.Tables(0).Rows(0)("userunkid"))
                    objUserMapping._UserTypeid = enUserType.crApprover

                    With objUserMapping
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With

                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objUserMapping = Nothing
                End If


                For Each drRow As DataRow In dtToEmployee.Rows

                    strQ = " UPDATE cmexpapprover_tran SET crapproverunkid = @newapproverunkid WHERE isvoid = 0 AND crapproverunkid = @oldapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid)
                    objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmexpapprover_master", "crapproverunkid", mintcrApproverunkid, "cmexpapprover_tran", "crapprovertranunkid", CInt(drRow("crapprovertranunkid")), 2, 2, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    mstrFormId = objClaimForm.GetApproverPendingClaimForm(mintSwapFromApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If mstrFormId.Trim.Length > 0 Then

                        strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newapproverunkid,crapproverunkid = @newapprovertranunkid  WHERE crmasterunkid in (" & mstrFormId & ") AND crapproverunkid = @approvertranunkid AND cmclaim_approval_tran.isvoid = 0 "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran WHERE approveremployeeunkid = @approverunkid AND crapproverunkid = @approvertranunkid AND crmasterunkid  in (" & mstrFormId & ") AND isvoid = 0"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcrApproverunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Next
                        End If
                    End If

                    mstrEmployeeID &= drRow("employeeunkid").ToString() & ","

                Next

                If mstrEmployeeID.Trim.Length > 0 Then
                    mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
                End If

                Dim objMigration As New clsMigration

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationdate = dtCurrentDateAndTime
                'Shani(24-Aug-2015) -- End

                objMigration._Migrationfromunkid = mintSwapToApproverID
                objMigration._Migrationtounkid = mintSwapFromApproverID
                objMigration._Usertypeid = enUserType.crApprover
                objMigration._Userunkid = mintUserunkid
                objMigration._Migratedemployees = mstrEmployeeID

                'Pinkal (07-Jan-2019) -- Start
                'AT Testing- Working on AT Testing for All Modules.
                objMigration._FormName = mstrFormName
                objMigration._LoginEmployeeunkid = mintLoginEmployeeunkid
                objMigration._ClientIP = mstrClientIP
                objMigration._HostName = mstrHostName
                objMigration._FromWeb = mblnIsWeb
                objMigration._AuditUserId = mintAuditUserId
                objMigration._CompanyUnkid = mintCompanyUnkid
                objMigration._AuditDate = mdtAuditDate
                'Pinkal (07-Jan-2019) -- End

                If objMigration.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            End If

            '==================================END FOR TO APPROVER=====================================


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SwapApprover; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Leave Enhancement : Putting Activate Feature in Leave Approver Master</purpose>
    ''' Shani(17-Aug-2015) -- Start
    Public Function InActiveApprover(ByVal intApproverunkid As Integer, ByVal intCompanyID As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " Update cmexpapprover_master set isactive = 1 where crapproverunkid = @crapproverunkid AND isvoid = 0 AND isswap = 0 "
            Else
                strQ = " Update cmexpapprover_master set isactive = 0 where crapproverunkid = @crapproverunkid AND isvoid = 0 AND isswap = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpapprover_master", "crapproverunkid", intApproverunkid, False, mintUserunkid, intCompanyID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function
    'Shani(17-Aug-2015) -- End


    'Pinkal (18-Feb-2016) -- Start
    'Enhancement - CR Changes for ASP as per Rutta's Request.
    Public Function GetApproverWithFromUserLogin(ByVal mblnPaymentApprovalwithLeaveApproval As Boolean, ByVal intUserId As Integer, ByVal intExpenseCategory As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            'Pinkal (01-Mar-2016) -- End

            Dim objDataOperation As New clsDataOperation

            If mblnPaymentApprovalwithLeaveApproval And intExpenseCategory = enExpenseType.EXP_LEAVE Then

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'strQ = "SELECT " & _
                '          " ISNULL(hremployee_master.employeecode,'') AS ApproverCode " & _
                '          ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS ApproverName " & _
                '          ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(lvapproverlevel_master.levelname,'') AS Approver " & _
                '          ",ISNULL(lvapproverlevel_master.levelunkid,0) AS levelunkid " & _
                '          ",ISNULL(lvapproverlevel_master.levelname,'') AS levelname " & _
                '          ",ISNULL(lvapproverlevel_master.priority,0) AS priority " & _
                '          ",lvleaveapprover_master.approverunkid " & _
                '          ",lvleaveapprover_master.leaveapproverunkid AS ApproverEmployeeID " & _
                '          " FROM hrapprover_usermapping " & _
                '          " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = hrapprover_usermapping.approverunkid AND lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isactive = 1 AND lvleaveapprover_master.isswap = 0  " & _
                '          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
                '          " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                '          " WHERE hrapprover_usermapping.userunkid = @loginUserID AND hrapprover_usermapping.usertypeid =  " & enUserType.Approver

                strQ = "SELECT " & _
                        " #APPR_CODE#  AS ApproverCode " & _
                        ",#APPR_NAME#  AS ApproverName " & _
                        ",#APPR_LEVELNAME#  AS Approver " & _
                          ",ISNULL(lvapproverlevel_master.levelunkid,0) AS levelunkid " & _
                          ",ISNULL(lvapproverlevel_master.levelname,'') AS levelname " & _
                          ",ISNULL(lvapproverlevel_master.priority,0) AS priority " & _
                          ",lvleaveapprover_master.approverunkid " & _
                          ",lvleaveapprover_master.leaveapproverunkid AS ApproverEmployeeID " & _
                        ",ISNULL(lvleaveapprover_master.isexternalapprover,0) AS isexternalapprover " & _
                          " FROM hrapprover_usermapping " & _
                          " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = hrapprover_usermapping.approverunkid AND lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isactive = 1 AND lvleaveapprover_master.isswap = 0  " & _
                          " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                        " #EMPL_JOIN# "

                StrFinalQurey = strQ

                StrQCondition &= " WHERE hrapprover_usermapping.userunkid = @loginUserID AND hrapprover_usermapping.usertypeid =  " & enUserType.Approver & _
                                           " AND lvleaveapprover_master.isexternalapprover = #ExAppr# "


                strQ = strQ.Replace("#APPR_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
                strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')")
                strQ = strQ.Replace("#APPR_LEVELNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(lvapproverlevel_master.levelname,'')")
                strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid")
                strQ = strQ.Replace("#DB_Name#", "")
                strQ &= StrQCondition
                strQ = strQ.Replace("#ExAppr#", "0")

                'Pinkal (01-Mar-2016) -- End

            Else

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'strQ = "SELECT " & _
                '         " ISNULL(hremployee_master.employeecode,'') AS ApproverCode " & _
                '         ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS ApproverName " & _
                '         ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'') AS Approver " & _
                '         ",ISNULL(cmapproverlevel_master.crlevelunkid,0) AS levelunkid " & _
                '         ",ISNULL(cmapproverlevel_master.crlevelname,'') AS levelname " & _
                '         ",ISNULL(cmapproverlevel_master.crpriority,0) AS priority " & _
                '         ",cmexpapprover_master.crapproverunkid AS approverunkid " & _
                '         ",cmexpapprover_master.employeeunkid AS ApproverEmployeeID " & _
                '         " FROM hrapprover_usermapping " & _
                '         " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid AND cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isactive = 1 AND cmexpapprover_master.isswap = 0  " & _
                '         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid " & _
                '         " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                '         " WHERE hrapprover_usermapping.userunkid = @loginUserID AND hrapprover_usermapping.usertypeid =  " & enUserType.crApprover & _
                '         " AND cmexpapprover_master.expensetypeid = " & intExpenseCategory

                strQ = "SELECT " & _
                       " #APPR_CODE# AS ApproverCode " & _
                       ",#APPR_NAME#  AS ApproverName " & _
                       ",#APPR_LEVELNAME# AS Approver " & _
                         ",ISNULL(cmapproverlevel_master.crlevelunkid,0) AS levelunkid " & _
                         ",ISNULL(cmapproverlevel_master.crlevelname,'') AS levelname " & _
                         ",ISNULL(cmapproverlevel_master.crpriority,0) AS priority " & _
                         ",cmexpapprover_master.crapproverunkid AS approverunkid " & _
                         ",cmexpapprover_master.employeeunkid AS ApproverEmployeeID " & _
                       ",ISNULL(cmexpapprover_master.isexternalapprover,0) AS isexternalapprover " & _
                         " FROM hrapprover_usermapping " & _
                         " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = hrapprover_usermapping.approverunkid AND cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isactive = 1 AND cmexpapprover_master.isswap = 0  " & _
                         " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                       " #EMPL_JOIN# "

                StrFinalQurey = strQ

                StrQCondition &= " WHERE hrapprover_usermapping.userunkid = @loginUserID AND hrapprover_usermapping.usertypeid =  " & enUserType.crApprover & _
                                          " AND cmexpapprover_master.expensetypeid = " & intExpenseCategory & " AND cmexpapprover_master.isexternalapprover = #ExAppr# "


                strQ = strQ.Replace("#APPR_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
                strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')")
                strQ = strQ.Replace("#APPR_LEVELNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'')")
                strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
                strQ = strQ.Replace("#DB_Name#", "")
                strQ &= StrQCondition
                strQ = strQ.Replace("#ExAppr#", "0")


                'Pinkal (01-Mar-2016) -- End


            End If

            strQ &= " Order BY Approver "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@ExpenseCategoryID", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseCategory)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim dsCompany As New DataSet
            dsCompany = GetClaimExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then

                    strQ = strQ.Replace("#APPR_CODE#", "''")
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")

                    If mblnPaymentApprovalwithLeaveApproval AndAlso intExpenseCategory = enExpenseType.EXP_LEAVE Then
                        strQ = strQ.Replace("#APPR_LEVELNAME#", "ISNULL(cfuser_master.username,'') + ' - ' +  ISNULL(lvapproverlevel_master.levelname,'')")
                        strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid ")
                    Else
                        strQ = strQ.Replace("#APPR_LEVELNAME#", "ISNULL(cfuser_master.username,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'')")
                        strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")
                    End If
                    strQ = strQ.Replace("#DB_Name#", "")
                Else

                    strQ = strQ.Replace("#APPR_CODE#", "CASE WHEN ISNULL(hremployee_master.employeecode,'') = '' THEN '' ELSE ISNULL(hremployee_master.employeecode,'') END")

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') END ")

                    If mblnPaymentApprovalwithLeaveApproval AndAlso intExpenseCategory = enExpenseType.EXP_LEAVE Then

                        strQ = strQ.Replace("#APPR_LEVELNAME#", "CASE WHEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') = '' THEN ISNULL(cfuser_master.username,'') + ' - ' +  ISNULL(lvapproverlevel_master.levelname,'') " & _
                                                     "ELSE ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(lvapproverlevel_master.levelname,'') END")

                        strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                                                           "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    Else
                        strQ = strQ.Replace("#APPR_LEVELNAME#", "CASE WHEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') = '' THEN ISNULL(cfuser_master.username,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'') " & _
                                                   "ELSE ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'') END")

                        strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                           "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    End If
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                End If

                strQ &= StrQCondition
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loginUserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objDataOperation.AddParameter("@ExpenseCategoryID", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseCategory)

                dstmp = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If

            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverWithFromUserLogin; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (18-Feb-2016) -- End


    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetClaimExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List", Optional ByVal mblnFromGetData As Boolean = False) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                   "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                   "FROM cmexpapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   ") AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   ") AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE cmexpapprover_master.isvoid = 0  AND cmexpapprover_master.isexternalapprover = 1 "

            If mblnFromGetData = False Then
                StrQ &= " AND cmexpapprover_master.isswap = 0 And cmexpapprover_master.isactive = 1"
            End If

            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimExternalApproverList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsApprover
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetClaimApproverEmployeeId(ByVal iExpenseCategoryID As Integer, ByVal ApproveID As Integer, ByVal blnExternalApprover As Boolean) As String
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT  ISNULL(STUFF(( SELECT   ','  + CAST(cmexpapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      " FROM  cmexpapprover_master " & _
                      " JOIN cmexpapprover_tran ON cmexpapprover_master.crapproverunkid = cmexpapprover_tran.crapproverunkid AND cmexpapprover_tran.isvoid = 0" & _
                      " WHERE cmexpapprover_master.employeeunkid = " & ApproveID & " AND cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 AND cmexpapprover_master.isactive = 1 " & _
                      " AND cmexpapprover_master.isexternalapprover = @isexternalapprover AND  cmexpapprover_master.expensetypeid = " & iExpenseCategoryID & _
                      " ORDER BY cmexpapprover_tran.employeeunkid " & _
                      " FOR XML PATH('') ), 1, 1, ''), '') EmployeeIds "

            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetClaimApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function


    'Pinkal (01-Mar-2016) -- End    


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function GetApproverFromEmployeeId(ByVal xDatabaseName As String, _
                                              ByVal mdtEmployeeAsonDate As Date, _
                                              ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                              ByVal intEmployeeId As Integer, _
                                              Optional ByVal xenExpenseType As Integer = 0) As DataSet

        'Gajanan [23-May-2020] -- 'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.[ByVal xDatabaseName As String,ByVal mdtEmployeeAsonDate As Date,ByVal xIncludeIn_ActiveEmployee As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty

            Dim objDataOperation As New clsDataOperation



            'Gajanan [02-June-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            Dim xDateJoinQry, xDateFilterQry
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)
            'Gajanan [02-June-2020] -- End


            strQ = "SELECT " & _
                   " #APPR_CODE# AS ApproverCode " & _
                   ",#APPR_NAME#  AS ApproverName " & _
                   ",#APPR_LEVELNAME# AS Approver " & _
                     ",ISNULL(cmapproverlevel_master.crlevelunkid,0) AS levelunkid " & _
                     ",ISNULL(cmapproverlevel_master.crlevelname,'') AS levelname " & _
                     ",ISNULL(cmapproverlevel_master.crpriority,0) AS priority " & _
                     ",cmexpapprover_master.crapproverunkid AS approverunkid " & _
                     ",cmexpapprover_master.employeeunkid AS ApproverEmployeeID " & _
                     ",ISNULL(cmexpapprover_master.isexternalapprover,0) AS isexternalapprover " & _
                     ",CASE WHEN cmexpapprover_master.expensetypeid = 1 THEN @Leave " & _
                     "      WHEN cmexpapprover_master.expensetypeid = 2  THEN @Medical " & _
                     "      WHEN cmexpapprover_master.expensetypeid = 4 THEN @Miscellaneous " & _
                     "      WHEN cmexpapprover_master.expensetypeid =3 THEN @Training END AS expensetype " & _
                     ",cmexpapprover_master.expensetypeid "


            'Gajanan [02-June-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If xIncludeIn_ActiveEmployee Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= ", Case When" & xDateFilterQry.ToString().Trim().Substring(xDateFilterQry.ToString().Trim().IndexOf("AND") + "AND".Length) & " THEN 1 ELSE 0 END AS IsActive "
                End If
            End If
            'Gajanan [02-June-2020] -- End

            strQ &= " FROM  cmexpapprover_tran  " & _
                     " LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmexpapprover_tran.crapproverunkid AND cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isactive = 1 AND cmexpapprover_master.isswap = 0  " & _
                     " LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                     " #EMPL_JOIN# " & _
                     " #DATE_JOIN# "

            StrFinalQurey = strQ

            StrQCondition &= " WHERE cmexpapprover_tran.employeeunkid = @employeeunkid AND cmexpapprover_tran.isvoid = 0 " & _
                                      " AND cmexpapprover_master.isexternalapprover = #ExAppr# "

            strQ = strQ.Replace("#APPR_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')")
            strQ = strQ.Replace("#APPR_LEVELNAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'')")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid")
            strQ = strQ.Replace("#DB_Name#", "")
            strQ &= StrQCondition
            strQ = strQ.Replace("#ExAppr#", "0")

            'Gajanan [02-June-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            strQ = strQ.Replace("#DATE_JOIN#", xDateJoinQry)
            'Gajanan [02-June-2020] -- End


            If xenExpenseType > 0 Then
                strQ &= " and cmexpapprover_master.expensetypeid = " & CInt(xenExpenseType) & " "
            End If


            'Gajanan [02-June-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If
            'Gajanan [02-June-2020] -- End

            strQ &= " Order BY cmexpapprover_master.expensetypeid,Approver "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim dsCompany As New DataSet
            dsCompany = GetClaimExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then

                    strQ = strQ.Replace("#APPR_CODE#", "''")
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")

                    strQ = strQ.Replace("#APPR_LEVELNAME#", "ISNULL(cfuser_master.username,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'')")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid  ")

                    strQ = strQ.Replace("#DB_Name#", "")
                Else

                    strQ = strQ.Replace("#APPR_CODE#", "CASE WHEN ISNULL(hremployee_master.employeecode,'') = '' THEN '' ELSE ISNULL(hremployee_master.employeecode,'') END")

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') END ")


                    strQ = strQ.Replace("#APPR_LEVELNAME#", "CASE WHEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') = '' THEN ISNULL(cfuser_master.username,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'') " & _
                                               "ELSE ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') + ' - ' +  ISNULL(cmapproverlevel_master.crlevelname,'') END")

                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                End If

                strQ &= StrQCondition
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
                objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
                objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
                objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

                dstmp = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If

            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverWithFromUserLogin; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function GetClaimApproverUnkid(ByVal iExpenseCategoryID As Integer, _
                                          ByVal intClaimApproverEmpUnkid As Integer, _
                                          ByVal blnExternalApprover As Boolean, _
                                          Optional ByVal intLevelId As Integer = -1, _
                                          Optional ByVal objDataOpr As clsDataOperation = Nothing) As String

        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim strApproverUnkids As String = ""
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            StrQ = "SELECT " & _
                   "    @approverunkid = ISNULL(STUFF((SELECT ',' + CAST(crapproverunkid AS NVARCHAR(10)) " & _
                   "                                   FROM cmexpapprover_master " & _
                   "                                   WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isactive = 1 AND cmexpapprover_master.isswap = 0 " & _
                   "                                   AND cmexpapprover_master.employeeunkid = " & intClaimApproverEmpUnkid & " AND " & _
                   "                                   cmexpapprover_master.isexternalapprover = @ExternalApprover and cmexpapprover_master.expensetypeid = " & iExpenseCategoryID & ""

            If intLevelId >= 0 Then
                StrQ &= " AND cmexpapprover_master.crlevelunkid = " & intLevelId
            End If

            StrQ &= " FOR XML PATH('')),1,1,''),'') "



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ExternalApprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strApproverUnkids, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strApproverUnkids = objDataOperation.GetParameterValue("@approverunkid")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetClaimApproverUnkid ; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return strApproverUnkids
    End Function
    'Gajanan [23-SEP-2019] -- End






    '<Language> This Auto Generated Text Please Do Not Modify it.


#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Expense Approver already exists. Please define new Expense Approver.")
            Language.setMessage("clsExpCommonMethods", 2, "Leave")
            Language.setMessage("clsExpCommonMethods", 3, "Medical")
            Language.setMessage("clsExpCommonMethods", 4, "Training")
			Language.setMessage(mstrModuleName, 5, "No")
			Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Sorry you cannot delete this approver. Reason this approver is already linked with some transaction(s).")
			Language.setMessage(mstrModuleName, 4, "Yes")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
