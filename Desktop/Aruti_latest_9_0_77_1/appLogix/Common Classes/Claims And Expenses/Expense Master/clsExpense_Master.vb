﻿'************************************************************************************************************************************
'Class Name : clsExpense_Master.vb
'Purpose    :
'Date       :04-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsExpense_Master
    Private Shared ReadOnly mstrModuleName As String = "clsExpense_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintExpenseunkid As Integer
    Private mintExpensetypeid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mintleavetypeunkid As Integer = 0
    Private mintUomunkid As Integer = 0
    Private minttranheadTypeunkid As Integer = 0
    Private mblnIsaccrue As Boolean = False
    Private mblnIsLeaveEncashment As Boolean = False
    Private mblnIsConsiderForPayroll As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mblnIsSecRouteMandatory As Boolean = False
    Private mblnIsExpenseInvisibleInCR As Boolean = False
    Private mblnIsAttachDocMandetory As Boolean = False

    'Pinkal (07-Jul-2018) -- Start
    'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
    Private mintAccrueSetting As Integer = enExpAccrueSetting.None
    'Pinkal (07-Jul-2018) -- End


    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsConsiderDependants As Boolean = False
    'Pinkal (25-Oct-2018) -- End


    Private mblnIsBudgetMandatory As Boolean = False

    'Pinkal (15-Feb-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    'Private mblnIsHRExpense As Boolean = False
    Private mblnIsHRExpense As Boolean = True
    'Pinkal (15-Feb-2020) -- End

    Private mintExpenditureTypeunkid As Integer = enP2PExpenditureType.None
    Private mintGLCodeunkid As Integer = 0


    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
    Private mblnShowOnESS As Boolean = True
    'Pinkal (26-Dec-2018) -- End


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Private mblnUnitPriceEditable As Boolean = True
    'Pinkal (25-May-2019) -- End


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Private mblnIsImprest As Boolean = False
    Private mintLinkedExpenseunkid As Integer = 0
    'Pinkal (11-Sep-2019) -- End


    'Pinkal (10-Jun-2020) -- Start
    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
    Private mintExpense_MaxQuantity As Integer = 0
    'Pinkal (10-Jun-2020) -- End

    'Hemant (06 Jul 2020) -- Start
    'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
    Private mintCostCenterUnkId As Integer
    'Hemant (06 Jul 2020) -- End


    'Pinkal (16-Sep-2020) -- Start
    'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
    Private mblnDoNotAllowToApplyForBackDate As Boolean = False
    'Pinkal (16-Sep-2020) -- End


#End Region

#Region "Enum"

    Public Enum enExpAccrueSetting
        None = 0
        Issue_Qty_TotalBalance = 1
        Issue_Qty_Balance_Ason_Date = 2
    End Enum
#End Region

    'Pinkal (07-Jul-2018) -- End

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property



    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    'Public Property _Expenseunkid() As Integer
    '    Get
    '        Return mintExpenseunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintExpenseunkid = value
    '        Call getData()
    '    End Set
    'End Property

    Public Property _Expenseunkid(Optional ByVal objDOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenseunkid = value
            Call GetData(objDOperation)
        End Set
    End Property
    'Pinkal (26-Feb-2019) -- End

    

    ''' <summary>
    ''' Purpose: Get or Set expensetypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expensetypeid() As Integer
        Get
            Return mintExpensetypeid
        End Get
        Set(ByVal value As Integer)
            mintExpensetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set uomunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintleavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintleavetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set uomunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Uomunkid() As Integer
        Get
            Return mintUomunkid
        End Get
        Set(ByVal value As Integer)
            mintUomunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _tranheadtypeunkid() As Integer
        Get
            Return minttranheadTypeunkid
        End Get
        Set(ByVal value As Integer)
            minttranheadTypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isaccrue
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isaccrue() As Boolean
        Get
            Return mblnIsaccrue
        End Get
        Set(ByVal value As Boolean)
            mblnIsaccrue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isleaveencashment
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsLeaveEncashment() As Boolean
        Get
            Return mblnIsLeaveEncashment
        End Get
        Set(ByVal value As Boolean)
            mblnIsLeaveEncashment = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isconsiderforpayroll
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsConsiderForPayroll() As Boolean
        Get
            Return mblnIsConsiderForPayroll
        End Get
        Set(ByVal value As Boolean)
            mblnIsConsiderForPayroll = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isSecRouteMandatory
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsSecRouteMandatory() As Boolean
        Get
            Return mblnIsSecRouteMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnIsSecRouteMandatory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ExpenseInvisibleInCR
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ExpenseInvisibleInCR() As Boolean
        Get
            Return mblnIsExpenseInvisibleInCR
        End Get
        Set(ByVal value As Boolean)
            mblnIsExpenseInvisibleInCR = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsAttachDocMandetory
    ''' Modify By: Shani Sheladiya
    ''' </summary>
    Public Property _IsAttachDocMandetory() As Boolean
        Get
            Return mblnIsAttachDocMandetory
        End Get
        Set(ByVal value As Boolean)
            mblnIsAttachDocMandetory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set BalanceSetting
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _AccrueSetting() As Integer
        Get
            Return mintAccrueSetting
        End Get
        Set(ByVal value As Integer)
            mintAccrueSetting = value
        End Set
    End Property

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    ''' <summary>
    ''' Purpose: Get or Set IsConsiderDependants
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsConsiderDependants() As Boolean
        Get
            Return mblnIsConsiderDependants
        End Get
        Set(ByVal value As Boolean)
            mblnIsConsiderDependants = value
        End Set
    End Property

    'Pinkal (25-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    ''' <summary>
    ''' Purpose: Get or Set IsBudgetMandatory
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsBudgetMandatory() As Boolean
        Get
            Return mblnIsBudgetMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnIsBudgetMandatory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsHRExpense
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsHRExpense() As Boolean
        Get
            Return mblnIsHRExpense
        End Get
        Set(ByVal value As Boolean)
            mblnIsHRExpense = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ExpenditureTypeId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ExpenditureTypeId() As Integer
        Get
            Return mintExpenditureTypeunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenditureTypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set GLCodeId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _GLCodeId() As Integer
        Get
            Return mintGLCodeunkid
        End Get
        Set(ByVal value As Integer)
            mintGLCodeunkid = value
        End Set
    End Property

    'Pinkal (20-Nov-2018) -- End

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ShowOnESS
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ShowOnESS() As Boolean
        Get
            Return mblnShowOnESS
        End Get
        Set(ByVal value As Boolean)
            mblnShowOnESS = value
        End Set
    End Property

    'Pinkal (26-Dec-2018) -- End


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.

    ''' <summary>
    ''' Purpose: Get or Set IsUnitPriceEditable
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsUnitPriceEditable() As Boolean
        Get
            Return mblnUnitPriceEditable
        End Get
        Set(ByVal value As Boolean)
            mblnUnitPriceEditable = value
        End Set
    End Property

    'Pinkal (25-May-2019) -- End


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    ''' <summary>
    ''' Purpose: Get or Set IsImprest
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsImprest() As Boolean
        Get
            Return mblnIsImprest
        End Get
        Set(ByVal value As Boolean)
            mblnIsImprest = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LinkedExpenseId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _LinkedExpenseId() As Integer
        Get
            Return mintLinkedExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintLinkedExpenseunkid = value
        End Set
    End Property

    'Pinkal (11-Sep-2019) -- End


    'Pinkal (10-Jun-2020) -- Start
    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.

    ''' <summary>
    ''' Purpose: Get or Set Expense_MaxQuantity
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expense_MaxQuantity() As Integer
        Get
            Return mintExpense_MaxQuantity
        End Get
        Set(ByVal value As Integer)
            mintExpense_MaxQuantity = value
        End Set
    End Property

    'Pinkal (10-Jun-2020) -- End

    'Hemant (06 Jul 2020) -- Start
    'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
    Public Property _CostCenterUnkId() As Integer
        Get
            Return mintCostCenterUnkId
        End Get
        Set(ByVal value As Integer)
            mintCostCenterUnkId = value
        End Set
    End Property
    'Hemant (06 Jul 2020) -- End


    'Pinkal (16-Sep-2020) -- Start
    'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.

    Public Property _DoNotAllowToApplyForBackDate() As Boolean
        Get
            Return mblnDoNotAllowToApplyForBackDate
        End Get
        Set(ByVal value As Boolean)
            mblnDoNotAllowToApplyForBackDate = value
        End Set
    End Property

    'Pinkal (16-Sep-2020) -- End


#End Region


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    'Public Sub GetData()

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        'Pinkal (26-Feb-2019) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (26-Feb-2019) -- Start
        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
        'objDataOperation = New clsDataOperation
        If objDoOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        'Pinkal (26-Feb-2019) -- End



        Try
            strQ = "SELECT " & _
                   "  cmexpense_master.expenseunkid " & _
                   ", cmexpense_master.expensetypeid " & _
                   ", cmexpense_master.code " & _
                   ", cmexpense_master.name " & _
                   ", cmexpense_master.name1 " & _
                   ", cmexpense_master.name2 " & _
                   ", cmexpense_master.description " & _
                   ", cmexpense_master.leavetypeunkid " & _
                   ", cmexpense_master.uomunkid " & _
                   ", cmexpense_master.trnheadtype_id " & _
                   ", cmexpense_master.isaccrue " & _
                   ", cmexpense_master.isleaveencashment " & _
                   ", cmexpense_master.isconsiderforpayroll " & _
                   ", cmexpense_master.isactive " & _
                   ", cmexpense_master.issecroutemandatory " & _
                   ", ISNULL(cmexpense_master.cr_expinvisible,0) AS cr_expinvisible " & _
                   ", ISNULL(cmexpense_master.isattachdocmandatory,0) AS isattachdocmandatory " & _
                   ", ISNULL(cmexpense_master.accruesetting,0) AS accruesetting " & _
                   ", ISNULL(cmexpense_master.isconsider_dependants,0) AS isconsider_dependants " & _
                   ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                   ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                   ", ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                   ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                   ", ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                    ", ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                   ", ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                   ", ISNULL(cmexpense_master.linkedexpenseunkid,0) AS linkedexpenseunkid " & _
                   ", ISNULL(cmexpense_master.exp_maxqty,0) AS exp_maxqty " & _
                   ", ISNULL(cmexpense_master.default_costcenterunkid,0) AS default_costcenterunkid " & _
                   ", ISNULL(cmexpense_master.donotapplybackdate,0) AS donotapplybackdate " & _
                   "FROM cmexpense_master " & _
                   "WHERE expenseunkid = @expenseunkid "

            'Pinkal (16-Sep-2020) -- Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not. [", ISNULL(cmexpense_master.donotapplybackdate,0) AS donotapplybackdate " & _]

            'Hemant (06 Jul 2020) -- [default_costcenterunkid]

            'Pinkal (10-Jun-2020) -- Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.

            'Pinkal (11-Sep-2019) -- Start           'Enhancement NMB - Working On Claim Retirement for NMB.[", ISNULL(cmexpense_master.isimprest,0) AS isimprest", ISNULL(cmexpense_master.linkedexpenseunkid,0) AS linkedexpenseunkid " ]

            'Pinkal (25-May-2019) -- Start 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", ISNULL(cmexpense_master.isunitpriceeditable,0) AS isunitpriceeditable " & _]

            'Pinkal (26-Dec-2018) --   'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.[ ", ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _]

            'Pinkal (20-Nov-2018) --'Enhancement - Working on P2P Integration for NMB. [", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense  ", ISNULL(expenditure_typeid,0) AS expenditure_typeid , ISNULL(glcodeunkid,0) AS glcodeunkid "]

            'Pinkal (25-Oct-2018) --   'Enhancement - Implementing Claim & Request changes For NMB .[", ISNULL(cmexpense_master.isconsider_dependants,0) AS isconsider_dependants " & _]

            'Pinkal (07-Jul-2018) --  'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224] [", ISNULL(cmexpense_master.accruesetting,0) AS accruesetting " & _]


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.ClearParameters()
            'Pinkal (26-Feb-2019) -- End

            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintExpenseunkid = CInt(dtRow.Item("expenseunkid"))
                mintExpensetypeid = CInt(dtRow.Item("expensetypeid"))
                mstrCode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mstrDescription = dtRow.Item("description").ToString
                mintleavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mintUomunkid = CInt(dtRow.Item("uomunkid"))
                minttranheadTypeunkid = CInt(dtRow.Item("trnheadtype_id"))
                mblnIsaccrue = CBool(dtRow.Item("isaccrue"))
                mblnIsLeaveEncashment = CBool(dtRow.Item("isleaveencashment"))
                mblnIsConsiderForPayroll = CBool(dtRow.Item("isconsiderforpayroll"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsSecRouteMandatory = CBool(dtRow.Item("issecroutemandatory"))
                mblnIsExpenseInvisibleInCR = CBool(dtRow.Item("cr_expinvisible"))
                mblnIsAttachDocMandetory = CBool(dtRow.Item("isattachdocmandatory"))
                mintAccrueSetting = CInt(dtRow.Item("accruesetting"))

                'Pinkal (25-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                mblnIsConsiderDependants = CBool(dtRow.Item("isconsider_dependants"))
                'Pinkal (25-Oct-2018) -- End


                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                mblnIsBudgetMandatory = CBool(dtRow.Item("isbudgetmandatory"))
                mblnIsHRExpense = CBool(dtRow.Item("ishrexpense"))
                mintExpenditureTypeunkid = CInt(dtRow.Item("expenditure_typeid"))
                mintGLCodeunkid = CInt(dtRow.Item("glcodeunkid"))
                'Pinkal (20-Nov-2018) -- End


                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                mblnShowOnESS = CBool(dtRow.Item("isshowoness"))
                'Pinkal (26-Dec-2018) -- End


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                mblnUnitPriceEditable = CBool(dtRow.Item("isunitpriceeditable"))
                'Pinkal (25-May-2019) -- End


                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                mblnIsImprest = CBool(dtRow.Item("isimprest"))
                mintLinkedExpenseunkid = CInt(dtRow.Item("linkedexpenseunkid"))
                'Pinkal (11-Sep-2019) -- End

                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                mintExpense_MaxQuantity = CInt(dtRow.Item("exp_maxqty"))
                'Pinkal (10-Jun-2020) -- End

                'Hemant (06 Jul 2020) -- Start
                'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
                mintCostCenterUnkId = CInt(dtRow.Item("default_costcenterunkid"))
                'Hemant (06 Jul 2020) -- End



                'Pinkal (16-Sep-2020) -- Start
                'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
                mblnDoNotAllowToApplyForBackDate = CBool(dtRow.Item("donotapplybackdate"))
                'Pinkal (16-Sep-2020) -- End



                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (26-Feb-2019) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "  expenseunkid " & _
                   ", expensetypeid " & _
                   ", code " & _
                   ", name " & _
                   ", name1 " & _
                   ", name2 " & _
                   ", description " & _
                   ", CASE WHEN expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous" & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest  " & _
                   "   END AS expensetype " & _
                   ", CASE WHEN uomunkid = 1 THEN @Qty " & _
                   "       WHEN uomunkid = 2 THEN @Amt END AS uom " & _
                   ", cmexpense_master.trnheadtype_id " & _
                   ", leavetypeunkid " & _
                   ", uomunkid " & _
                   ", isaccrue " & _
                   ", isleaveencashment " & _
                   ", isconsiderforpayroll " & _
                   ", cmexpense_master.isactive " & _
                   ", issecroutemandatory " & _
                   ", ISNULL(cmexpense_master.cr_expinvisible,0) AS cr_expinvisible " & _
                   ", ISNULL(cmexpense_master.isattachdocmandatory,0) AS isattachdocmandatory " & _
                   ", ISNULL(cmexpense_master.accruesetting,0) AS AccrueSettingId " & _
                   ", CASE WHEN ISNULL(cmexpense_master.accruesetting,0) = '" & enExpAccrueSetting.None & "' THEN @None " & _
                   "       WHEN ISNULL(cmexpense_master.accruesetting,0) = '" & enExpAccrueSetting.Issue_Qty_TotalBalance & "' THEN @QtyTotalBalance " & _
                   "       WHEN ISNULL(cmexpense_master.accruesetting,0) = '" & enExpAccrueSetting.Issue_Qty_Balance_Ason_Date & "' THEN @QtyBalanceAsonDate" & _
                   "  END AS AccrueSettingType " & _
                   ", ISNULL(cmexpense_master.isconsider_dependants,0) AS isconsider_dependants " & _
                   ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                   ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                   ", ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                   ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                   ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                   ", ISNULL(praccount_master.account_name,'') AS glcodename " & _
                   ", ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                   ", ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                   ", ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                   ", ISNULL(cmexpense_master.linkedexpenseunkid,0) AS linkedexpenseunkid " & _
                   ", ISNULL(cmexpense_master.exp_maxqty,0) AS exp_maxqty " & _
                   ", ISNULL(cmexpense_master.default_costcenterunkid,0) AS default_costcenterunkid " & _
                   ", ISNULL(cmexpense_master.donotapplybackdate,0) AS donotapplybackdate " & _
                   " FROM cmexpense_master " & _
                   " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid "

            'Pinkal (16-Sep-2020) -- Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.[ISNULL(cmexpense_master.donotapplybackdate,0) AS donotapplybackdate]

            'Hemant (06 Jul 2020) -- [default_costcenterunkid]

            'Pinkal (10-Jun-2020) -- Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.[", ISNULL(cmexpense_master.exp_maxqty,0) AS exp_maxqty " & _]

            'Pinkal (11-Sep-2019) -- Start Enhancement NMB - Working On Claim Retirement for NMB.[", ISNULL(cmexpense_master.isimprest,0) AS isimprest , ISNULL(cmexpense_master.linkedexpenseunkid,0) AS linkedexpenseunkid " & _]

            'Pinkal (25-May-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _]

            'Pinkal (26-Dec-2018) --  'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.[]

            'Pinkal (20-Nov-2018) -- 'Enhancement - Working on P2P Integration for NMB.[ ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory , ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense  ", ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid , ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _]

            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[", ISNULL(cmexpense_master.isconsider_dependants,0) AS isconsider_dependants " & _]

            'Pinkal (07-Jul-2018) -- 'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]   ", ISNULL(cmexpense_master.accruesetting,0) AS AccrueSettingId " & _
            '", CASE WHEN ISNULL(cmexpense_master.accruesetting,0) = '" & enExpAccrueSetting.None & "' THEN @None " & _
            '"       WHEN ISNULL(cmexpense_master.accruesetting,0) = '" & enExpAccrueSetting.Issue_Qty_TotalBalance & "' THEN @QtyTotalBalance " & _
            '"       WHEN ISNULL(cmexpense_master.accruesetting,0) = '" & enExpAccrueSetting.Issue_Qty_Balance_Ason_Date & "' THEN @QtyBalanceAsonDate" & _
            '"  END AS AccrueSettingType " & _


            If blnOnlyActive Then

                strQ &= " WHERE cmexpense_master.isactive = 1 " & _
                        " ORDER BY expensetype "
            End If

            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (11-Sep-2019) -- End


            objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
            objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))


            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            objDataOperation.AddParameter("@None", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "None"))
            objDataOperation.AddParameter("@QtyTotalBalance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Issue Qty As Per Total Balance"))
            objDataOperation.AddParameter("@QtyBalanceAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Issue Qty As Per Balance As On Date"))
            'Pinkal (07-Jul-2018) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmexpense_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode, , mintExpensetypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Expense Code is already defined. Please define new Expense Code for the selected Expense Category.")
            Return False
        End If
        If isExist(, mstrName, mintExpensetypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Expense Name is already defined. Please define new Expense Name for the selected Expense Category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleavetypeunkid.ToString)
            objDataOperation.AddParameter("@uomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUomunkid.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, minttranheadTypeunkid.ToString)
            objDataOperation.AddParameter("@isaccrue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaccrue.ToString)
            objDataOperation.AddParameter("@isleaveencashment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLeaveEncashment.ToString)
            objDataOperation.AddParameter("@isconsiderforpayroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsConsiderForPayroll.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@issecroutemandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSecRouteMandatory.ToString)
            objDataOperation.AddParameter("@cr_expinvisible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExpenseInvisibleInCR.ToString)
            objDataOperation.AddParameter("@isattachdocmandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAttachDocMandetory.ToString)

            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting)
            'Pinkal (07-Jul-2018) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            objDataOperation.AddParameter("@isconsider_dependants", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsConsiderDependants.ToString)
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            objDataOperation.AddParameter("@isbudgetmandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBudgetMandatory.ToString)
            objDataOperation.AddParameter("@ishrexpense", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsHRExpense.ToString)
            objDataOperation.AddParameter("@expenditure_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenditureTypeunkid)
            objDataOperation.AddParameter("@glcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGLCodeunkid)
            'Pinkal (20-Nov-2018) -- End


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowOnESS.ToString)
            'Pinkal (26-Dec-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objDataOperation.AddParameter("@isunitpriceeditable", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUnitPriceEditable.ToString)
            'Pinkal (25-May-2019) -- End


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@isimprest", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImprest.ToString)
            objDataOperation.AddParameter("@linkedexpenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLinkedExpenseunkid.ToString)
            'Pinkal (11-Sep-2019) -- End


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            objDataOperation.AddParameter("@exp_maxqty", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpense_MaxQuantity)
            'Pinkal (10-Jun-2020) -- End

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            objDataOperation.AddParameter("@default_costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterUnkId.ToString)
            'Hemant (06 Jul 2020) -- End


            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            objDataOperation.AddParameter("@donotapplybackdate", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnDoNotAllowToApplyForBackDate)
            'Pinkal (16-Sep-2020) -- End


            strQ = "INSERT INTO cmexpense_master ( " & _
                       "  expensetypeid " & _
                       ", code " & _
                       ", name " & _
                       ", name1 " & _
                       ", name2 " & _
                       ", description " & _
                       ", leavetypeunkid " & _
                       ", uomunkid " & _
                       ", trnheadtype_id " & _
                       ", isaccrue " & _
                       ", isleaveencashment " & _
                       ", isconsiderforpayroll " & _
                       ", isactive" & _
                       ", issecroutemandatory " & _
                       ", cr_expinvisible " & _
                       ", isattachdocmandatory " & _
                       ", accruesetting " & _
                       ", isconsider_dependants " & _
                       ", isbudgetmandatory " & _
                       ", ishrexpense " & _
                       ", expenditure_typeid " & _
                       ", glcodeunkid " & _
                       ", isshowoness " & _
                       ", isunitpriceeditable " & _
                       ", isimprest " & _
                       ", linkedexpenseunkid " & _
                       ", exp_maxqty " & _
                       ", default_costcenterunkid " & _
                       ", donotapplybackdate " & _
                   ") VALUES (" & _
                       "  @expensetypeid " & _
                       ", @code " & _
                       ", @name " & _
                       ", @name1 " & _
                       ", @name2 " & _
                       ", @description " & _
                       ", @leavetypeunkid " & _
                       ", @uomunkid " & _
                       ", @trnheadtype_id " & _
                       ", @isaccrue " & _
                       ", @isleaveencashment " & _
                       ", @isconsiderforpayroll " & _
                       ", @isactive" & _
                       ", @issecroutemandatory " & _
                       ", @cr_expinvisible " & _
                       ", @isattachdocmandatory " & _
                       ", @accruesetting " & _
                       ", @isconsider_dependants " & _
                       ", @isbudgetmandatory " & _
                       ", @ishrexpense " & _
                       ", @expenditure_typeid " & _
                       ", @glcodeunkid " & _
                       ", @isshowoness " & _
                       ", @isunitpriceeditable " & _
                       ", @isimprest " & _
                       ", @linkedexpenseunkid " & _
                       ", @exp_maxqty " & _
                       ", @default_costcenterunkid " & _
                       ", @donotapplybackdate " & _
                   "); SELECT @@identity"

            'Pinkal (16-Sep-2020) -- Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not. [ ", @donotapplybackdate " & _]

            'Hemant (06 Jul 2020) -- [default_costcenterunkid]

            'Pinkal (10-Jun-2020) -- Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.[@exp_maxqty]

            'Pinkal (11-Sep-2019) -- Start Enhancement NMB - Working On Claim Retirement for NMB.[@isimprest,@linkedexpenseunkid]

            'Pinkal (25-May-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES.[@isunitpriceeditable]

            'Pinkal (26-Dec-2018) --  'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.[isshowoness]

            'Pinkal (20-Nov-2018) --  'Enhancement - Working on P2P Integration for NMB.[isbudgetmandatory,ishrexpense,expenditure_typeid,glcodeunkid]

            'Pinkal (25-Oct-2018) --  'Enhancement - Implementing Claim & Request changes For NMB .[   ", isconsider_dependants " & _]

            'Pinkal (07-Jul-2018) -- 'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224] [ ", accruesetting " & _] 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintExpenseunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cmexpense_master", "expenseunkid", mintExpenseunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmexpense_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintExpensetypeid, mintExpenseunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Expense Code is already defined. Please define new Expense Code for the selected Expense Category.")
            Return False
        End If
        If isExist(, mstrName, mintExpensetypeid, mintExpenseunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Expense Name is already defined. Please define new Expense Name for the selected Expense Category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.int, eZeeDataType.INT_SIZE, mintexpensetypeid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleavetypeunkid.ToString)
            objDataOperation.AddParameter("@uomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUomunkid.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, minttranheadTypeunkid.ToString)
            objDataOperation.AddParameter("@isaccrue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaccrue.ToString)
            objDataOperation.AddParameter("@isleaveencashment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLeaveEncashment.ToString)
            objDataOperation.AddParameter("@isconsiderforpayroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsConsiderForPayroll.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@issecroutemandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSecRouteMandatory.ToString)
            objDataOperation.AddParameter("@cr_expinvisible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExpenseInvisibleInCR.ToString)
            objDataOperation.AddParameter("@isattachdocmandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAttachDocMandetory.ToString)

            'Pinkal (07-Jul-2018) -- Start
            'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224]
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting)
            'Pinkal (07-Jul-2018) -- End


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            objDataOperation.AddParameter("@isconsider_dependants", SqlDbType.Int, eZeeDataType.INT_SIZE, mblnIsConsiderDependants)
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            objDataOperation.AddParameter("@isbudgetmandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBudgetMandatory.ToString)
            objDataOperation.AddParameter("@ishrexpense", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsHRExpense.ToString)
            objDataOperation.AddParameter("@expenditure_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenditureTypeunkid)
            objDataOperation.AddParameter("@glcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGLCodeunkid)
            'Pinkal (20-Nov-2018) -- End


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowOnESS)
            'Pinkal (26-Dec-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objDataOperation.AddParameter("@isunitpriceeditable", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUnitPriceEditable)
            'Pinkal (25-May-2019) -- End



            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@isimprest", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImprest)
            objDataOperation.AddParameter("@linkedexpenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLinkedExpenseunkid.ToString)
            'Pinkal (11-Sep-2019) -- End

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            objDataOperation.AddParameter("@exp_maxqty", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpense_MaxQuantity)
            'Pinkal (10-Jun-2020) -- End

            'Hemant (06 Jul 2020) -- Start
            'ENHANCEMENT (NMB): Posting a transaction head to one cost centre for all employees without mapping on employee cost centre screen (cost center dropdown list on head master and claim expense master and pick cost center from head master instead of employee master on process payroll if not mapped on employee cost center screen)
            objDataOperation.AddParameter("@default_costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterUnkId.ToString)
            'Hemant (06 Jul 2020) -- End


            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            objDataOperation.AddParameter("@donotapplybackdate", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnDoNotAllowToApplyForBackDate)
            'Pinkal (16-Sep-2020) -- End



            strQ = "UPDATE cmexpense_master SET " & _
                       "  expensetypeid = @expensetypeid" & _
                       ", code = @code" & _
                       ", name = @name" & _
                       ", name1 = @name1" & _
                       ", name2 = @name2" & _
                       ", description = @description" & _
                       ", leavetypeunkid = @leavetypeunkid" & _
                       ", uomunkid = @uomunkid" & _
                       ", trnheadtype_id = @trnheadtype_id " & _
                       ", isaccrue = @isaccrue" & _
                       ", isconsiderforpayroll = @isconsiderforpayroll " & _
                       ", isleaveencashment = @isleaveencashment " & _
                       ", isactive = @isactive " & _
                       ", issecroutemandatory = @issecroutemandatory " & _
                       ", cr_expinvisible =  @cr_expinvisible " & _
                       ", isattachdocmandatory =  @isattachdocmandatory " & _
                       ", accruesetting =  @accruesetting " & _
                       ", isconsider_dependants =  @isconsider_dependants " & _
                       ", isbudgetmandatory = @isbudgetmandatory " & _
                       ", ishrexpense = @ishrexpense " & _
                       ", expenditure_typeid = @expenditure_typeid " & _
                       ", glcodeunkid = @glcodeunkid " & _
                       ", isshowoness  = @isshowoness " & _
                       ", isunitpriceeditable  = @isunitpriceeditable " & _
                       ", isimprest  = @isimprest " & _
                       ", linkedexpenseunkid  = @linkedexpenseunkid  " & _
                       ", exp_maxqty = @exp_maxqty " & _
                       ", default_costcenterunkid = @default_costcenterunkid " & _
                       ", donotapplybackdate = @donotapplybackdate " & _
                       "  WHERE expenseunkid = @expenseunkid "


            'Pinkal (16-Sep-2020) -- Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.[", donotapplybackdate = @donotapplybackdate " & _]

            'Hemant (06 Jul 2020) -- [default_costcenterunkid]

            'Pinkal (10-Jun-2020) -- Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.[", exp_maxqty = @exp_maxqty " & _]

            'Pinkal (11-Sep-2019) -- Start 'Enhancement NMB - Working On Claim Retirement for NMB. [", isimprest  = @isimprest , linkedexpenseunkid  = @linkedexpenseunkid  " & _]

            'Pinkal (25-May-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", isunitpriceeditable  = @isunitpriceeditable " & _]

            'Pinkal (26-Dec-2018) -- 'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.[", isshowoness  = @isshowoness " & _]

            'Pinkal (20-Nov-2018) -- 'Enhancement - Working on P2P Integration for NMB.[", isbudgetmandatory = @isbudgetmandatory  ", ishrexpense = @ishrexpense ", expenditure_typeid = @expenditure_typeid  ", glcodeunkid = @glcodeunkid " & _]

            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[", isconsider_dependants =  @isconsider_dependants " & _]

            'Pinkal (07-Jul-2018) -- 'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224] [ ", accruesetting =  @accruesetting " & _] 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "cmexpense_master", mintExpenseunkid, "expenseunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpense_master", "expenseunkid", mintExpenseunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmexpense_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Expense. Reason : This Expense is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE cmexpense_master SET isactive  = 0 " & _
                   "WHERE expenseunkid = @expenseunkid "

            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cmexpense_master", "expenseunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal mstrTable As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='expenseunkid' "


            If mstrTable.Trim.Length > 0 Then
                strQ &= " AND TABLE_NAME = '" & mstrTable & "'"
            End If
            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsTables.Tables("List").Rows

                'Pinkal (20-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'If dtRow.Item("TableName") = "cmexpense_master" OrElse dtRow.Item("TableName") = "atcmclaim_process_tran" Then Continue For

                'Pinkal (24-Oct-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                'If dtRow.Item("TableName") = "cmexpense_master" OrElse dtRow.Item("TableName") = "atcmclaim_process_tran" OrElse dtRow.Item("TableName") = "atcmassignexpense_sector" Then Continue For
                If dtRow.Item("TableName") = "cmexpense_master" OrElse dtRow.Item("TableName") = "atcmclaim_process_tran" OrElse dtRow.Item("TableName") = "atcmassignexpense_sector" OrElse _
                 dtRow.Item("TableName") = "atcmclaim_retirement_master" OrElse dtRow.Item("TableName") = "atcmclaim_retirement_tran" OrElse dtRow.Item("TableName") = "atcmclaim_retirement_approval_tran" OrElse _
                 dtRow.Item("TableName") = "atcmretire_process_tran" OrElse dtRow.Item("TableName") = "atcrexpadjustment_tran" Then Continue For


                'Pinkal (18-Mar-2021) -- Bug Claim/Retirement -   Working Claim/Retirement Bug.[dtRow.Item("TableName") = "atcrexpadjustment_tran"]

                'Pinkal (09-Oct-2020) -- Bug Expense NMB:  Giving Error when Editing any Expense "Isvoid" an invalid column. [dtRow.Item("TableName") = "atcmretire_process_tran"]

                'Pinkal (24-Oct-2019) -- End

                'Pinkal (20-Feb-2019) -- End
                strQ = "SELECT expenseunkid FROM " & dtRow.Item("TableName").ToString & " WHERE expenseunkid = @expenseunkid AND isvoid = 0 "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            mstrMessage = ""
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal iExpTypeId As Integer = 0, Optional ByVal intUnkid As Integer = -1, Optional ByRef intId As Integer = 0) As Boolean
        'Sohail (02 Aug 2017) - [intId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            intId = 0 'Sohail (02 Aug 2017)

            strQ = "SELECT " & _
                   "  expenseunkid " & _
                   ", expensetypeid " & _
                   ", code " & _
                   ", name " & _
                   ", name1 " & _
                   ", name2 " & _
                   ", description " & _
                   ", leavetypeunkid " & _
                   ", uomunkid " & _
                   ", trnheadtype_id " & _
                   ", isaccrue " & _
                   ", isleaveencashment " & _
                   ", isconsiderforpayroll " & _
                   ", isactive " & _
                   ", issecroutemandatory " & _
                   ", cr_expinvisible " & _
                   ", isattachdocmandatory " & _
                   ", accruesetting " & _
                   ", ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _
                   ", ISNULL(isbudgetmandatory,0) AS isbudgetmandatory " & _
                   ", ISNULL(ishrexpense,0) AS ishrexpense " & _
                   ", ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                   ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                   ", ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                   ", ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                   ", ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                   ", ISNULL(cmexpense_master.linkedexpenseunkid,0) AS linkedexpenseunkid " & _
                   ", ISNULL(cmexpense_master.exp_maxqty,0) AS exp_maxqty " & _
                   ", ISNULL(cmexpense_master.default_costcenterunkid,0) AS default_costcenterunkid " & _
                   ", ISNULL(cmexpense_master.donotapplybackdate,0) AS donotapplybackdate " & _
                   " FROM cmexpense_master " & _
                   " WHERE isactive = 1 "

            'Pinkal (16-Sep-2020) -- Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.[", ISNULL(cmexpense_master.donotapplybackdate,0) AS donotapplybackdate " & _]

            'Hemant (06 Jul 2020) -- [default_costcenterunkid]

            'Pinkal (10-Jun-2020) -- Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.[", ISNULL(cmexpense_master.exp_maxqty,0) AS exp_maxqty " & _]

            'Pinkal (11-Sep-2019) -- Start Enhancement NMB - Working On Claim Retirement for NMB.[", ISNULL(cmexpense_master.isimprest,0) AS isimprest , ISNULL(cmexpense_master.linkedexpenseunkid,0) AS linkedexpenseunkid " & _]

            'Pinkal (25-May-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES.[ ", ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _]

            'Pinkal (26-Dec-2018) -- 'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.[", ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _]

            'Pinkal (20-Nov-2018) --  'Enhancement - Working on P2P Integration for NMB.[", ISNULL(isbudgetmandatory,0) AS isbudgetmandatory , ISNULL(ishrexpense,0) AS ishrexpense, ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid, ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid]

            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[", ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _]

            'Pinkal (07-Jul-2018) -- 'Enhancement - Adding Claim Expense Balance Setting for Pacra [Ref #224] [ ", accruesetting " & _] 


            If strCode.ToString.Trim.Length > 0 Then
                strQ &= " AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.ToString.Trim.Length > 0 Then
                strQ &= " AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND expenseunkid <> @expenseunkid "
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If iExpTypeId > 0 Then
                strQ &= " AND expensetypeid = @expensetypeid "
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpTypeId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("expenseunkid"))
            End If
            'Sohail (02 Aug 2017) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal iExpenseTypeId As Integer, _
                                 Optional ByVal iFlag As Boolean = False, _
                                 Optional ByVal iList As String = "List", _
                                 Optional ByVal iEmpId As Integer = 0, _
                                 Optional ByVal iOnlyAssigned As Boolean = False, _
                                 Optional ByVal intTrnHeadTypeID As Integer = 0, _
                                 Optional ByVal strFilter As String = "", _
                                 Optional ByVal strOrderByQuery As String = "") As DataSet

        'Hemant (23 May 2019) -- [strOrderByQuery] 
        'Hemant (31 Aug 2018) -- [strFilter]
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            If iFlag Then
                strQ = "SELECT " & _
                       "     0 AS Id " & _
                       "    ,'' AS Code " & _
                       "    ,@Select AS Name " & _
                       "    ,0 AS isleaveencashment" & _
                       "    ,0 AS cr_expinvisible" & _
                       "    ,0 AS isaccrue " & _
                       "    ,0 AS isattachdocmandatory " & _
                       "    ,0 AS isconsider_dependants " & _
                       "    ,0 AS isbudgetmandatory " & _
                       "    ,0 AS ishrexpense " & _
                       "    ,0 AS expenditure_typeid " & _
                       "    ,0 AS glcodeunkid " & _
                       "    ,0 AS isshowoness " & _
                       "    ,1 AS isunitpriceeditable " & _
                       "    ,0 AS isimprest " & _
                       "UNION "
            End If

            'Pinkal (11-Sep-2019) -- Start Enhancement NMB - Working On Claim Retirement for NMB.[ "    ,0 AS isimprest " & _]

            'Pinkal (25-May-2019) -- Start   'Enhancement - NMB FINAL LEAVE UAT CHANGES.[ "    ,1 AS isunitpriceeditable " & _]

            'Pinkal (26-Dec-2018) --  'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.["    ,0 AS isshowoness " & _]

            'Pinkal (20-Nov-2018) -- 'Enhancement - Working on P2P Integration for NMB.[, ISNULL(isbudgetmandatory,0) AS isbudgetmandatory , ISNULL(ishrexpense,0) AS ishrexpense, ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid,ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid]

            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .["    ,0 AS isconsider_dependants " & _]

            If iOnlyAssigned = False Then
                strQ &= "SELECT  " & _
                        "    expenseunkid AS Id " & _
                        "   ,code AS Code " & _
                        "   ,name AS Name " & _
                        "   ,ISNULL(isleaveencashment,0) AS isleaveencashment " & _
                        "   ,ISNULL(cr_expinvisible,0) AS cr_expinvisible " & _
                        "   ,ISNULL(isaccrue,0) AS isaccrue  " & _
                        "   ,ISNULL(isattachdocmandatory,0) AS isattachdocmandatory  " & _
                        "   ,ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _
                        "   ,ISNULL(isbudgetmandatory,0) AS isbudgetmandatory " & _
                        "   ,ISNULL(ishrexpense,0) AS ishrexpense " & _
                        "   ,ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                        "   ,ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                                "   ,ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                        "   ,ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                        "   ,ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                        " FROM cmexpense_master  " & _
                        " WHERE isactive = 1 "
            Else
                strQ &= "SELECT " & _
                        "    cmexpense_master.expenseunkid AS Id " & _
                        "   ,code AS Code " & _
                        "   ,name AS Name " & _
                        "   ,ISNULL(isleaveencashment,0) AS isleaveencashment " & _
                        "   ,ISNULL(cr_expinvisible,0) AS cr_expinvisible " & _
                        "   ,ISNULL(isaccrue,0) AS isaccrue " & _
                        "   ,ISNULL(isattachdocmandatory,0) AS isattachdocmandatory " & _
                        "   , ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _
                        "   , ISNULL(isbudgetmandatory,0) AS isbudgetmandatory " & _
                        "   , ISNULL(ishrexpense,0) AS ishrexpense " & _
                        "   ,ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                        "   ,ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                                "   ,ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                        "   ,ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                        "   ,ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                        " FROM cmexpense_master " & _
                        "	JOIN cmexpbalance_tran ON cmexpense_master.expenseunkid = cmexpbalance_tran.expenseunkid " & _
                        " WHERE isactive = 1 AND employeeunkid = '" & iEmpId & "' AND isvoid = 0 "
            End If

            'Pinkal (11-Sep-2019) --  'Enhancement NMB - Working On Claim Retirement for NMB. [  "   ,ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _]

            'Pinkal (25-May-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES.["   ,ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _]

            'Pinkal (26-Dec-2018) --  'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.[ "   ,ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _]

            'Pinkal (20-Nov-2018) -- 'Enhancement - Working on P2P Integration for NMB.[, ISNULL(isbudgetmandatory,0) AS isbudgetmandatory , ISNULL(ishrexpense,0) AS ishrexpense, ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid,ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid]

            'Pinkal (25-Oct-2018) --  'Enhancement - Implementing Claim & Request changes For NMB .["   , ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _]

            If iExpenseTypeId > 0 Then
                strQ &= " AND expensetypeid = " & iExpenseTypeId
            End If

            If intTrnHeadTypeID > 0 Then
                strQ &= " AND trnheadtype_id = " & intTrnHeadTypeID & " "
            End If

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Hemant (31 Aug 2018)) -- End

            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            If strOrderByQuery.Trim <> "" Then
                strQ &= strOrderByQuery
            End If
            'Hemant (23 May 2019) -- End

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetExpenseIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.CR_EXPENSE)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExpenseIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetExpenseIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.CR_EXPENSE)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExpenseIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getImprestExpesneComboList(Optional ByVal iExpenseTypeId As Integer = 0, _
                                                                     Optional ByVal iFlag As Boolean = False, _
                                                                     Optional ByVal iList As String = "List", _
                                                                     Optional ByVal iEmpId As Integer = 0, _
                                                                     Optional ByVal iOnlyAssigned As Boolean = False, _
                                                                     Optional ByVal intTrnHeadTypeID As Integer = 0, _
                                                                     Optional ByVal strFilter As String = "", _
                                                                     Optional ByVal strOrderByQuery As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            If iFlag Then
                strQ = "SELECT " & _
                       "     0 AS Id " & _
                       "    ,'' AS Code " & _
                       "    ,@Select AS Name " & _
                       "    ,0 AS isleaveencashment" & _
                       "    ,0 AS cr_expinvisible" & _
                       "    ,0 AS isaccrue " & _
                       "    ,0 AS isattachdocmandatory " & _
                       "    ,0 AS isconsider_dependants " & _
                       "    ,0 AS isbudgetmandatory " & _
                       "    ,0 AS ishrexpense " & _
                       "    ,0 AS expenditure_typeid " & _
                       "    ,0 AS glcodeunkid " & _
                       "    ,0 AS isshowoness " & _
                       "    ,1 AS isunitpriceeditable " & _
                       "    ,0 AS isimprest " & _
                       "UNION "
            End If

            If iOnlyAssigned = False Then
                strQ &= "SELECT  " & _
                        "    expenseunkid AS Id " & _
                        "   ,code AS Code " & _
                        "   ,name AS Name " & _
                        "   ,ISNULL(isleaveencashment,0) AS isleaveencashment " & _
                        "   ,ISNULL(cr_expinvisible,0) AS cr_expinvisible " & _
                        "   ,ISNULL(isaccrue,0) AS isaccrue  " & _
                        "   ,ISNULL(isattachdocmandatory,0) AS isattachdocmandatory  " & _
                        "   ,ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _
                        "   ,ISNULL(isbudgetmandatory,0) AS isbudgetmandatory " & _
                        "   ,ISNULL(ishrexpense,0) AS ishrexpense " & _
                        "   ,ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                        "   ,ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                        "   ,ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                        "   ,ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                        "   ,ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                        " FROM cmexpense_master  " & _
                        " WHERE isactive = 1 AND isimprest = 1 AND expensetypeid <> @expensetypeid "
            Else
                strQ &= "SELECT " & _
                        "    cmexpense_master.expenseunkid AS Id " & _
                        "   ,code AS Code " & _
                        "   ,name AS Name " & _
                        "   ,ISNULL(isleaveencashment,0) AS isleaveencashment " & _
                        "   ,ISNULL(cr_expinvisible,0) AS cr_expinvisible " & _
                        "   ,ISNULL(isaccrue,0) AS isaccrue " & _
                        "   ,ISNULL(isattachdocmandatory,0) AS isattachdocmandatory " & _
                        "   , ISNULL(isconsider_dependants,0) AS isconsider_dependants " & _
                        "   , ISNULL(isbudgetmandatory,0) AS isbudgetmandatory " & _
                        "   , ISNULL(ishrexpense,0) AS ishrexpense " & _
                        "   ,ISNULL(cmexpense_master.expenditure_typeid,0) AS expenditure_typeid " & _
                        "   ,ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                        "   ,ISNULL(cmexpense_master.isshowoness,0) AS isshowoness " & _
                        "   ,ISNULL(cmexpense_master.isunitpriceeditable,1) AS isunitpriceeditable " & _
                        "   ,ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                        " FROM cmexpense_master " & _
                        "	JOIN cmexpbalance_tran ON cmexpense_master.expenseunkid = cmexpbalance_tran.expenseunkid " & _
                        " WHERE isactive = 1 AND isimprest = 1 AND employeeunkid = '" & iEmpId & "' AND isvoid = 0 "
            End If

            If iExpenseTypeId > 0 Then
                strQ &= " AND expensetypeid = " & iExpenseTypeId
            End If

            If intTrnHeadTypeID > 0 Then
                strQ &= " AND trnheadtype_id = " & intTrnHeadTypeID & " "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            
            If strOrderByQuery.Trim <> "" Then
                strQ &= strOrderByQuery
            End If

            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enExpenseType.EXP_IMPREST)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getImprestExpesneComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Pinkal (11-Sep-2019) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Expense Code is already defined. Please define new Expense Code for the selected Expense Category.")
			Language.setMessage("clsExpCommonMethods", 2, "Leave")
			Language.setMessage("clsExpCommonMethods", 3, "Medical")
			Language.setMessage("clsExpCommonMethods", 4, "Training")
			Language.setMessage(mstrModuleName, 5, "None")
			Language.setMessage("clsExpCommonMethods", 6, "Quantity")
			Language.setMessage("clsExpCommonMethods", 7, "Amount")
			Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
			Language.setMessage(mstrModuleName, 2, "Sorry, Expense Name is already defined. Please define new Expense Name for the selected Expense Category.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Expense. Reason : This Expense is already linked with some transaction.")
			Language.setMessage(mstrModuleName, 6, "Issue Qty As Per Total Balance")
			Language.setMessage(mstrModuleName, 7, "Issue Qty As Per Balance As On Date")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class