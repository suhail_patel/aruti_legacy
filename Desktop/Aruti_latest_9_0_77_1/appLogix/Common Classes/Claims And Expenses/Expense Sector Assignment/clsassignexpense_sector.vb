﻿'************************************************************************************************************************************
'Class Name : clsassignexpense_sector.vb
'Purpose    :
'Date       :20-Feb-2019
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsassignexpense_sector
    Private Shared ReadOnly mstrModuleName As String = "clsassignexpense_sector"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCrexpsectorrouteunkid As Integer
    Private mintExpensetypeid As Integer
    Private mintExpenseunkid As Integer
    Private mintSecrouteunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebhostName As String = ""
    Private mstrWebFormName As String = String.Empty

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crexpsectorrouteunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crexpsectorrouteunkid(Optional ByVal objDOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintCrexpsectorrouteunkid
        End Get
        Set(ByVal value As Integer)
            mintCrexpsectorrouteunkid = value
            Call GetData(objDOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expensetypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expensetypeid() As Integer
        Get
            Return mintExpensetypeid
        End Get
        Set(ByVal value As Integer)
            mintExpensetypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expenseunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expenseunkid() As Integer
        Get
            Return mintExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenseunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set secrouteunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Secrouteunkid() As Integer
        Get
            Return mintSecrouteunkid
        End Get
        Set(ByVal value As Integer)
            mintSecrouteunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = "SELECT " & _
                          "  crexpsectorrouteunkid " & _
                          ", expensetypeid " & _
                          ", expenseunkid " & _
                          ", secrouteunkid " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", voiduserunkid " & _
                          ", voidreason " & _
                         "FROM cmassignexpense_sector " & _
                         "WHERE crexpsectorrouteunkid = @crexpsectorrouteunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpsectorrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpsectorrouteunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCrexpsectorrouteunkid = CInt(dtRow.Item("crexpsectorrouteunkid"))
                mintExpensetypeid = CInt(dtRow.Item("expensetypeid"))
                mintExpenseunkid = CInt(dtRow.Item("expenseunkid"))
                mintSecrouteunkid = CInt(dtRow.Item("secrouteunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal xExpenseTypeId As Integer = -1, Optional ByVal xExpenseId As Integer = -1, Optional ByVal xSectopID As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT " & _
                  "     ischeck " & _
                  "    ,Sector " & _
                  "    ,isgrp " & _
                  "    ,sortid " & _
                  "    ,grpid " & _
                  "    ,crexpsectorrouteunkid " & _
                  "    ,expenseunkid " & _
                  "    ,SectorId " & _
                  "FROM " & _
                  "( " & _
                  "    SELECT DISTINCT " & _
                  "         CAST(0 AS BIT) AS ischeck " & _
                  "        , cmexpense_master.expenseunkid " & _
                  "        , -1 SectorId " & _
                  "        , ISNULL(cmexpense_master.code,'') + ' - ' + ISNULL(cmexpense_master.name,'')  AS Sector " & _
                  "        ,1 AS isgrp " & _
                  "        ,cmassignexpense_sector.expenseunkid AS grpid " & _
                  "        ,-1 AS sortid " & _
                  "        ,-1 AS crexpsectorrouteunkid " & _
                  "    FROM cmassignexpense_sector " & _
                  "    LEFT JOIN cmexpense_master ON cmassignexpense_sector.expenseunkid = cmexpense_master.expenseunkid AND cmassignexpense_sector.expensetypeid =cmexpense_master.expensetypeid " & _
                  "    WHERE cmassignexpense_sector.isvoid = 0 "

            If xExpenseTypeId > 0 Then
                strQ &= " AND cmassignexpense_sector.expensetypeid = @expensetypeid "
            End If

            If xExpenseId > 0 Then
                strQ &= " AND cmassignexpense_sector.expenseunkid = @expenseunkid "
            End If

            If xSectopID > 0 Then
                strQ &= " AND cmassignexpense_sector.secrouteunkid =  @secrouteunkid "
            End If


            strQ &= " UNION ALL " & _
                         "    SELECT " & _
                         "     CAST(0 AS BIT) AS ischeck " & _
                         "      ,  cmexpense_master.expenseunkid " & _
                         "     ,ISNULL(cfcommon_master.masterunkid,'') AS SectorId " & _
                         "     ,ISNULL(cfcommon_master.name,'') AS Sector " & _
                         "     ,0 AS isgrp " & _
                         "     ,cmassignexpense_sector.expenseunkid AS grpid " & _
                         "     ,0 AS sortid " & _
                         "     ,cmassignexpense_sector.crexpsectorrouteunkid AS crexpsectorrouteunkid " & _
                        " FROM cmassignexpense_sector " & _
                        " LEFT JOIN cmexpense_master ON cmassignexpense_sector.expenseunkid = cmexpense_master.expenseunkid AND cmassignexpense_sector.expensetypeid =cmexpense_master.expensetypeid " & _
                        " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmassignexpense_sector.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                        " WHERE cmassignexpense_sector.isvoid= 0 "

            If xExpenseTypeId > 0 Then
                strQ &= " AND cmassignexpense_sector.expensetypeid = @expensetypeid "
            End If

            If xExpenseId > 0 Then
                strQ &= " AND cmassignexpense_sector.expenseunkid = @expenseunkid "
            End If

            If xSectopID > 0 Then
                strQ &= " AND cmassignexpense_sector.secrouteunkid =  @secrouteunkid "
            End If

            strQ &= ") AS A WHERE 1 = 1  ORDER BY grpid,sortid,A.Sector "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseTypeId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSectopID)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmassignexpense_sector) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO cmassignexpense_sector ( " & _
              "  expensetypeid " & _
              ", expenseunkid " & _
              ", secrouteunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @expensetypeid " & _
              ", @expenseunkid " & _
              ", @secrouteunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCrexpsectorrouteunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAudiTrailForExpenseSector(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmassignexpense_sector) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpsectorrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpsectorrouteunkid.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE cmassignexpense_sector SET " & _
              "  expensetypeid = @expensetypeid" & _
              ", expenseunkid = @expenseunkid" & _
              ", secrouteunkid = @secrouteunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason " & _
            "WHERE crexpsectorrouteunkid = @crexpsectorrouteunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrailForExpenseSector(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmassignexpense_sector) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " Update  cmassignexpense_sector SET isvoid = 1,voiduserunkid = @voiduserunkid,voiddatetime = @voiddatetime,voidreason = @voidreason " & _
                         " WHERE crexpsectorrouteunkid = @crexpsectorrouteunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpsectorrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Crexpsectorrouteunkid(objDataOperation) = intUnkid

            If InsertAudiTrailForExpenseSector(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intExpenseId As Integer, ByVal intSectorId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT cmclaim_request_tran.crmasterunkid FROM cmclaim_request_tran " & _
                        " WHERE  cmclaim_request_tran.isvoid = 0 AND cmclaim_request_tran.expenseunkid= @expenseunkid " & _
                        " AND cmclaim_request_tran.secrouteunkid = @sectorunkid " & _
                        " UNION " & _
                        " SELECT cmclaim_approval_tran.crmasterunkid FROM cmclaim_approval_tran " & _
                       " where  cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.expenseunkid= @expenseunkid " & _
                       " AND cmclaim_approval_tran.secrouteunkid = @sectorunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
            objDataOperation.AddParameter("@sectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectorId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iExpenseId As Integer, ByVal iSectorRouteId As Integer) As Boolean
        'Pinkal (26-Dec-2018) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                         " crexpsectorrouteunkid " & _
                        ",expensetypeid " & _
                       ",expenseunkid " & _
                        ",secrouteunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiddatetime " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                      "FROM cmassignexpense_sector " & _
                      " WHERE isvoid = 0 "

            If iExpenseId > 0 Then
                strQ &= " AND expenseunkid = @expenseunkid"
            End If
            If iSectorRouteId > 0 Then
                strQ &= " AND secrouteunkid = @secrouteunkid"
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpenseId)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iSectorRouteId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetSectorFromExpense(ByVal intExpenseID As Integer, Optional ByVal iFlag As Boolean = False) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Try

            If iFlag Then
                strQ = "SELECT 0 AS crexpsectorrouteunkid, 0 AS expenseunkid,0 AS secrouteunkid,'' AS ExpenseCode,'' AS Expense , @Select AS Sector UNION "
            End If

            strQ &= " SELECT ISNULL(cmassignexpense_sector.crexpsectorrouteunkid ,0) AS crexpsectorrouteunkid " & _
                      ", ISNULL(cmassignexpense_sector.expenseunkid ,0) AS expenseunkid " & _
                      ", ISNULL(cmassignexpense_sector.secrouteunkid ,0) AS secrouteunkid " & _
                      ",ISNULL(cmexpense_master.code,'') As ExpenseCode	" & _
                      ",ISNULL(cmexpense_master.name,'') AS Expense " & _
                      ",ISNULL(cfcommon_master.name,'') AS Sector " & _
                      " FROM cmassignexpense_sector " & _
                      " LEFT Join cmexpense_master ON cmassignexpense_sector.expenseunkid = cmexpense_master.expenseunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmassignexpense_sector.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                      " WHERE cmassignexpense_sector.isvoid = 0  AND cmassignexpense_sector.expenseunkid  =  @ExpenseID "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@ExpenseID", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseID)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSectorFromExpense; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAudiTrailForExpenseSector(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atcmassignexpense_sector ( " & _
                         " crexpsectorrouteunkid " & _
                         ",expensetypeid " & _
                         ",expenseunkid" & _
                         ", secrouteunkid" & _
                         ", audittype" & _
                         ", audituserunkid" & _
                         ", auditdatetime" & _
                         ", ip" & _
                         ", machine_name" & _
                         ", form_name" & _
                         ", isweb " & _
                         " ) VALUES (" & _
                         " @crexpsectorrouteunkid " & _
                         ",@expensetypeid " & _
                         ",@expenseunkid" & _
                         ",@secrouteunkid" & _
                         ",@audittype" & _
                         ",@audituserunkid" & _
                         ",Getdate()" & _
                         ",@ip" & _
                         ",@machine_name" & _
                         ",@form_name" & _
                         ",@isweb " & _
                "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpsectorrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpsectorrouteunkid.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            End If

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForExpenseSector; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class