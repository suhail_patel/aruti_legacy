﻿'************************************************************************************************************************************
'Class Name : clsEmployeeExpenseBalance.vb
'Purpose    :
'Date       :14-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsEmployeeExpenseBalance
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeExpenseBalance"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCrexpbalanceunkid As Integer
    Private mintYearunkid As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mintExpenseunkid As Integer = -1
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mdblAccrue_Amount As Double = 0
    Private mdblIssue_Amount As Double = 0
    Private mdblDaily_Amount As Double = 0
    Private mdblRemaining_Bal As Double = 0
    Private mintUserunkid As Integer
    Private mdblExpensebf As Double = 0
    Private mdblActualamount As Double = 0
    Private mdblCfamount As Double = 0
    Private mintEligibilityafter As Integer
    Private mblnIsnoaction As Boolean = False
    Private mblnIsopenelc As Boolean = False
    Private mblnIselc As Boolean = False
    Private mblnIsclose_Fy As Boolean = False
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty

    'Shani(13-Aug-2015) -- Start
    'Changes in Employe Expense Assignment(C&R).
    Private mintExpeseTypeId As Integer
    Private mintLeavebalanceSetting As Integer
    'Shani(13-Aug-2015) -- End


    'Pinkal (29-Feb-2016) -- Start
    'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
    Private mintOccurrence As Integer = 0
    Private mintRemaining_Occurrence As Integer = 0
    'Pinkal (29-Feb-2016) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private mintFrequencyCycleId As Integer = enExpenseFrequencyCycle.Financial_ELC
    Private mintEligibleYear As Integer = 0
    'Pinkal (26-Feb-2019) -- End
'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
    Private mdecAdjustmentAmt As Decimal
    'Pinkal (03-Mar-2021) -- End
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crexpbalanceunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crexpbalanceunkid() As Integer
        Get
            Return mintCrexpbalanceunkid
        End Get
        Set(ByVal value As Integer)
            mintCrexpbalanceunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expenseunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expenseunkid() As Integer
        Get
            Return mintExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accrue_amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Accrue_Amount() As Double
        Get
            Return mdblAccrue_Amount
        End Get
        Set(ByVal value As Double)
            mdblAccrue_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issue_amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issue_Amount() As Double
        Get
            Return mdblIssue_Amount
        End Get
        Set(ByVal value As Double)
            mdblIssue_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set daily_amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Daily_Amount() As Double
        Get
            Return mdblDaily_Amount
        End Get
        Set(ByVal value As Double)
            mdblDaily_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remaining_bal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remaining_Bal() As Double
        Get
            Return mdblRemaining_Bal
        End Get
        Set(ByVal value As Double)
            mdblRemaining_Bal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expensebf
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expensebf() As Double
        Get
            Return mdblExpensebf
        End Get
        Set(ByVal value As Double)
            mdblExpensebf = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualamount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Actualamount() As Double
        Get
            Return mdblActualamount
        End Get
        Set(ByVal value As Double)
            mdblActualamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cfamount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cfamount() As Double
        Get
            Return mdblCfamount
        End Get
        Set(ByVal value As Double)
            mdblCfamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set eligibilityafter
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Eligibilityafter() As Integer
        Get
            Return mintEligibilityafter
        End Get
        Set(ByVal value As Integer)
            mintEligibilityafter = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isnoaction
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isnoaction() As Boolean
        Get
            Return mblnIsnoaction
        End Get
        Set(ByVal value As Boolean)
            mblnIsnoaction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isopenelc
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isopenelc() As Boolean
        Get
            Return mblnIsopenelc
        End Get
        Set(ByVal value As Boolean)
            mblnIsopenelc = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iselc
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Iselc() As Boolean
        Get
            Return mblnIselc
        End Get
        Set(ByVal value As Boolean)
            mblnIselc = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isclose_fy
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isclose_Fy() As Boolean
        Get
            Return mblnIsclose_Fy
        End Get
        Set(ByVal value As Boolean)
            mblnIsclose_Fy = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    'Shani(13-Aug-2015) -- Start
    'Changes in Employe Expense Assignment(C&R).

    ''' <summary>
    ''' Purpose: Get or Set ExpenseTypeId
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _ExpenseTypeId() As Integer
        Get
            Return mintExpeseTypeId
        End Get
        Set(ByVal value As Integer)
            mintExpeseTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveBalanceSetting
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _LeaveBalanceSetting() As Integer
        Get
            Return mintLeavebalanceSetting
        End Get
        Set(ByVal value As Integer)
            mintLeavebalanceSetting = value
        End Set
    End Property

    'Shani(13-Aug-2015) -- End


    'Pinkal (29-Feb-2016) -- Start
    'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.

    ''' <summary>
    ''' Purpose: Get or Set Occurrence
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _Occurrence() As Integer
        Get
            Return mintOccurrence
        End Get
        Set(ByVal value As Integer)
            mintOccurrence = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Remaining_Occurrence
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _Remaining_Occurrence() As Integer
        Get
            Return mintRemaining_Occurrence
        End Get
        Set(ByVal value As Integer)
            mintRemaining_Occurrence = value
        End Set
    End Property

    'Pinkal (29-Feb-2016) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    ''' <summary>
    ''' Purpose: Get or Set FrequencyCycle
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _FrequencyCycleId() As Integer
        Get
            Return mintFrequencyCycleId
        End Get
        Set(ByVal value As Integer)
            mintFrequencyCycleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EligibleYear
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _EligibleYear() As Integer
        Get
            Return mintEligibleYear
        End Get
        Set(ByVal value As Integer)
            mintEligibleYear = value
        End Set
    End Property

    'Pinkal (26-Feb-2019) -- End


    'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
    ''' <summary>
    ''' Purpose: Get or Set AdjustmentAmt
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _AdjustmentAmt() As Decimal
        Get
            Return mdecAdjustmentAmt
        End Get
        Set(ByVal value As Decimal)
            mdecAdjustmentAmt = value
        End Set
    End Property
    'Pinkal (03-Mar-2021) -- End


#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  crexpbalanceunkid " & _
              ", yearunkid " & _
              ", employeeunkid " & _
              ", expenseunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", accrue_amount " & _
              ", issue_amount " & _
              ", daily_amount " & _
              ", remaining_bal " & _
              ", userunkid " & _
              ", expensebf " & _
              ", actualamount " & _
              ", cfamount " & _
              ", eligibilityafter " & _
              ", isnoaction " & _
              ", isopenelc " & _
              ", iselc " & _
              ", isclose_fy " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(occurrence,0) AS occurrence " & _
              ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                      ", ISNULL(freq_cycleunkid,1) AS freq_cycleunkid " & _
                      ", ISNULL(eligible_year,0) AS eligible_year " & _
              ", ISNULL(adjustment_amt,0) AS adjustment_amt " & _
             "FROM cmexpbalance_tran " & _
             "WHERE crexpbalanceunkid = @crexpbalanceunkid "

            'Pinkal (03-Mar-2021)-- KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.[ ", ISNULL(adjustment_amt,0) AS adjustment_amt " & _]

            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", ISNULL(freq_cycleunkid,1) AS freq_cycleunkid  , ISNULL(eligible_year,0) AS eligible_year _]

            'Pinkal (29-Feb-2016) 'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            '[ISNULL(occurrence,0) AS occurrence ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " ]

            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpbalanceunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCrexpbalanceunkid = CInt(dtRow.Item("crexpbalanceunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintExpenseunkid = CInt(dtRow.Item("expenseunkid"))
                mdtStartdate = dtRow.Item("startdate")
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mdblAccrue_Amount = CDbl(dtRow.Item("accrue_amount"))
                mdblIssue_Amount = CDbl(dtRow.Item("issue_amount"))
                mdblDaily_Amount = CDbl(dtRow.Item("daily_amount"))
                mdblRemaining_Bal = CDbl(dtRow.Item("remaining_bal"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mdblExpensebf = CDbl(dtRow.Item("expensebf"))
                mdblActualamount = CDbl(dtRow.Item("actualamount"))
                mdblCfamount = CDbl(dtRow.Item("cfamount"))
                mintEligibilityafter = CInt(dtRow.Item("eligibilityafter"))
                mblnIsnoaction = CBool(dtRow.Item("isnoaction"))
                mblnIsopenelc = CBool(dtRow.Item("isopenelc"))
                mblnIselc = CBool(dtRow.Item("iselc"))
                mblnIsclose_Fy = CBool(dtRow.Item("isclose_fy"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString


                'Pinkal (29-Feb-2016) -- Start
                'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
                mintOccurrence = CInt(dtRow.Item("occurrence"))
                mintRemaining_Occurrence = CInt(dtRow.Item("remaining_occurrence"))
                'Pinkal (29-Feb-2016) -- End


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mintFrequencyCycleId = CInt(dtRow.Item("freq_cycleunkid"))
                mintEligibleYear = CInt(dtRow.Item("eligible_year"))
                'Pinkal (26-Feb-2019) -- End


                'Pinkal (03-Mar-2021)-- Start
                'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
                If Not IsDBNull(dtRow.Item("adjustment_amt")) Then
                    mdecAdjustmentAmt = CDec(dtRow.Item("adjustment_amt"))
                End If
                'Pinkal (03-Mar-2021) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                                    Optional ByVal blnOnlyActive As Boolean = True, _
                                    Optional ByVal iEmployeeId As Integer = -1, _
                                    Optional ByVal iExpenseId As Integer = -1, _
                                    Optional ByVal IsopenELC As Boolean = False, _
                                    Optional ByVal IsELC As Boolean = False, _
                                    Optional ByVal IsCloseFy As Boolean = False, _
                                    Optional ByVal mstrUserAccessFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  cmexpbalance_tran.crexpbalanceunkid " & _
                   " ,cmexpbalance_tran.yearunkid " & _
                   " ,cmexpbalance_tran.employeeunkid " & _
                   " ,cmexpbalance_tran.expenseunkid " & _
                   " ,cmexpbalance_tran.startdate " & _
                   " ,cmexpbalance_tran.enddate " & _
                   " ,cmexpbalance_tran.accrue_amount " & _
                   " ,cmexpbalance_tran.issue_amount " & _
                   " ,cmexpbalance_tran.daily_amount " & _
                   " ,cmexpbalance_tran.remaining_bal " & _
                   " ,cmexpbalance_tran.userunkid " & _
                   " ,cmexpbalance_tran.expensebf " & _
                   " ,cmexpbalance_tran.actualamount " & _
                   " ,cmexpbalance_tran.cfamount " & _
                   " ,cmexpbalance_tran.eligibilityafter " & _
                   " ,cmexpbalance_tran.isnoaction " & _
                   " ,cmexpbalance_tran.isopenelc " & _
                   " ,cmexpbalance_tran.iselc " & _
                   " ,cmexpbalance_tran.isclose_fy " & _
                   " ,cmexpbalance_tran.isvoid " & _
                   " ,cmexpbalance_tran.voiduserunkid " & _
                   " ,cmexpbalance_tran.voiddatetime " & _
                   " ,cmexpbalance_tran.voidreason " & _
                   " ,hremployee_master.employeecode+' - '+hremployee_master.firstname+' '+hremployee_master.surname AS ename " & _
                   " ,cmexpense_master.name AS expname " & _
                   " ,CASE WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                   "       WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                   "       WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
                   "       WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _
                   "  END AS exptype " & _
                   " ,ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.startdate,112),'') AS sdate " & _
                   " ,ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.enddate,112),'') AS edate " & _
                   " , ISNULL(occurrence,0) AS occurrence " & _
                   ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                   ", ISNULL(freq_cycleunkid,1) AS freq_cycleunkid " & _
                   ", ISNULL(eligible_year,0) AS eligible_year " & _
                   ", ISNULL(adjustment_amt,0) AS adjustment_amt " & _
                   " FROM cmexpbalance_tran " & _
                   " JOIN cmexpense_master ON cmexpbalance_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                   " JOIN hremployee_master ON cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE 1 = 1 "

            'Pinkal (03-Mar-2021)-- KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.[ ", ISNULL(adjustment_amt,0) AS adjustment_amt " & _]

            'Pinkal (21-Dec-2019) -- Enhancement NMB Claim Retirement Changes.[WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _]


            'Pinkal (26-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", ISNULL(freq_cycleunkid,1) AS freq_cycleunkid , ISNULL(eligible_year,0) AS eligible_year ]

            'Pinkal (29-Feb-2016) 'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            '[ISNULL(occurrence,0) AS occurrence ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " ]

            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If

            If iEmployeeId > 0 Then
                strQ &= " AND cmexpbalance_tran.employeeunkid = '" & iEmployeeId & "' "
            End If

            If iExpenseId > 0 Then
                strQ &= " AND cmexpbalance_tran.expenseunkid = '" & iExpenseId & "' "
            End If

            If IsELC Then
                If IsopenELC Then
                    strQ &= " AND cmexpbalance_tran.isopenelc = 1 "
                Else
                    strQ &= " AND cmexpbalance_tran.isopenelc = 0 "
                End If
            End If

            If IsCloseFy Then
                strQ &= " AND ISNULL(cmexpbalance_tran.isclose_fy,0) = 1 "
            Else
                strQ &= " AND ISNULL(cmexpbalance_tran.isclose_fy,0) = 0 "
            End If

            If mstrUserAccessFilter = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            strQ &= mstrUserAccessFilter

            strQ &= " ORDER BY hremployee_master.firstname+' '+hremployee_master.surname "

            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))

            'Pinkal (21-Dec-2019) -- Start
            'Enhancement - Claim Retirement for NMB.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (21-Dec-2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr As DataRow In dsList.Tables(0).Rows
                If dr.Item("sdate").ToString.Trim.Length > 0 Then dr.Item("sdate") = eZeeDate.convertDate(dr.Item("sdate").ToString).ToShortDateString
                If dr.Item("edate").ToString.Trim.Length > 0 Then dr.Item("edate") = eZeeDate.convertDate(dr.Item("edate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Display_List(Optional ByVal iListName As String = "List", _
                                 Optional ByVal mstrUserAccessFilter As String = "", _
                                 Optional ByVal iEmpId As Integer = 0, _
                                 Optional ByVal iExpTypeId As Integer = 0, _
                                 Optional ByVal iExpId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "     ischeck " & _
                   "    ,expname " & _
                   "    ,exptype " & _
                   "    ,accrue " & _
                   "    ,sdate " & _
                   "    ,edate " & _
                   "    ,isgrp " & _
                   "    ,grpid " & _
                   "    ,sortid " & _
                   "    ,crunkid " & _
                   "    ,exptypeid " & _
                   "    ,expid " & _
                   "    , occurrence " & _
                   "    ,remaining_occurrence " & _
                   "    ,freq_cycleunkid " & _
                   "    ,eligible_year " & _
                   "    ,adjustment_amt " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT DISTINCT " & _
                   "         CAST(0 AS BIT) AS ischeck " & _
                   "        ,employeecode+' - '+ firstname+' '+surname AS expname " & _
                   "        ,'' AS exptype " & _
                   "        ,'' AS accrue " & _
                   "        ,'' AS sdate " & _
                   "        ,'' AS edate " & _
                   "        ,1 AS isgrp " & _
                   "        ,cmexpbalance_tran.employeeunkid AS grpid " & _
                   "        ,-1 AS sortid " & _
                   "        ,0 AS crunkid " & _
                   "        ,0 AS exptypeid " & _
                   "        ,0 AS expid " & _
                   "        ,1 AS isopenelc " & _
                   "        ,0 AS occurrence " & _
                   "        ,0 AS remaining_occurrence " & _
                   "       ,1 AS freq_cycleunkid " & _
                   "       ,0 AS eligible_year " & _
                   "       ,0.00 adjustment_amt " & _
                   "    FROM cmexpbalance_tran " & _
                   "        JOIN hremployee_master ON cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "    WHERE isvoid = 0 "

            'Pinkal (03-Mar-2021)-- KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.[  "       ,0.00 adjustment_amt " & _]

            'Shani (13 Aug 2015 ) -- [Add isopenelc Filed]
            If iEmpId > 0 Then
                strQ &= " AND cmexpbalance_tran.employeeunkid = '" & iEmpId & "' "
            End If
            If mstrUserAccessFilter = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            strQ &= mstrUserAccessFilter

            'SHANI (06 JUN 2015) -- Start
            'Enhancement : Changes in C & R module given by Mr.Andrew.
            'strQ &= "UNION ALL " & _
            '        "   SELECT " & _
            '        "        CAST(0 AS BIT) AS ischeck " & _
            '        "       ,SPACE(5) + '' + ISNULL(cmexpense_master.name,'') AS expname " & _
            '        "       ,CASE WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
            '        "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
            '        "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
            '        "        END AS exptype " & _
            '        "       ,CASE WHEN cmexpense_master.expensetypeid = '1' THEN CAST(accrue_amount AS NVARCHAR(MAX)) ELSE '' END AS accrue " & _
            '        "       ,ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.startdate,112),'') AS sdate " & _
            '        "       ,ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.enddate,112),'') AS edate " & _
            '        "       ,0 AS isgrp " & _
            '        "       ,cmexpbalance_tran.employeeunkid AS grpid " & _
            '        "       ,0 AS sortid " & _
            '        "       ,crexpbalanceunkid AS crunkid " & _
            '        "       ,cmexpense_master.expensetypeid AS exptypeid " & _
            '        "       ,cmexpense_master.expenseunkid AS expid " & _
            '        "   FROM cmexpbalance_tran " & _
            '        "       JOIN cmexpense_master ON cmexpbalance_tran.expenseunkid = cmexpense_master.expenseunkid " & _
            '        "       JOIN hremployee_master ON cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "   WHERE isvoid = 0 "
            strQ &= "UNION ALL " & _
                    "   SELECT " & _
                    "        CAST(0 AS BIT) AS ischeck " & _
                    "       ,SPACE(5) + '' + ISNULL(cmexpense_master.name,'') AS expname " & _
                    "       ,CASE WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                    "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                    "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous" & _
                    "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
                    "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _
                    "        END AS exptype " & _
                    "       ,CASE WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN CAST(accrue_amount AS NVARCHAR(MAX)) " & _
                    "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN CAST(accrue_amount AS NVARCHAR(MAX))" & _
                    "             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN CAST(accrue_amount AS NVARCHAR(MAX)) " & _
                    "             ELSE '' END AS accrue " & _
                    "       ,ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.startdate,112),'') AS sdate " & _
                    "       ,ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.enddate,112),'') AS edate " & _
                    "       ,0 AS isgrp " & _
                    "       ,cmexpbalance_tran.employeeunkid AS grpid " & _
                    "       ,0 AS sortid " & _
                    "       ,crexpbalanceunkid AS crunkid " & _
                    "       ,cmexpense_master.expensetypeid AS exptypeid " & _
                    "       ,cmexpense_master.expenseunkid AS expid " & _
                    "       ,CASE WHEN cmexpbalance_tran.iselc = 1 AND cmexpense_master.expensetypeid = 1 THEN " & _
                    "               cmexpbalance_tran.isopenelc ELSE 1 END " & _
                    "       AS isopenelc " & _
                    "      , ISNULL(occurrence,0) AS occurrence " & _
                    "      , ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                    "      , ISNULL(freq_cycleunkid,1) AS freq_cycleunkid " & _
                    "      , ISNULL(eligible_year,0) AS eligible_year " & _
                    "      , ISNULL(adjustment_amt,0.00) AS adjustment_amt " & _
                    "   FROM cmexpbalance_tran " & _
                    "       JOIN cmexpense_master ON cmexpbalance_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                    "       JOIN hremployee_master ON cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "   WHERE isvoid = 0 "

            'Pinkal (03-Mar-2021)--KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.[  "      , ISNULL(adjustment_amt,0.00) AS adjustment_amt " & _]

            'Pinkal (21-Dec-2019) -- Enhancement - Claim Retirement for NMB.[WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _]


            'Pinkal (26-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.["      , ISNULL(freq_cycleunkid,1) AS freq_cycleunkid " & _]

            'Shani(30-Jan-2016) -- Start
            'OLD  : -  "       ,CASE WHEN cmexpense_master.expensetypeid = '1' THEN CAST(accrue_amount AS NVARCHAR(MAX)) " & _
            '"             WHEN cmexpense_master.expensetypeid = '2' THEN CAST(accrue_amount AS NVARCHAR(MAX))" & _
            '"             WHEN cmexpense_master.expensetypeid = '4' THEN CAST(accrue_amount AS NVARCHAR(MAX)) " & _
            '"             ELSE '' END AS accrue " & _
            'New  : -"       ,CASE WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN CAST(accrue_amount AS NVARCHAR(MAX)) " & _
            '"             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN CAST(accrue_amount AS NVARCHAR(MAX))" & _
            '"             WHEN cmexpense_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN CAST(accrue_amount AS NVARCHAR(MAX)) " & _
            '"             ELSE '' END AS accrue " & _
            'Shani(30-Jan-2016) -- End

            'Shani (13 Aug 2015 ) -- [Add isopenelc Filed]
            'SHANI (06 JUN 2015) -- End



            'Pinkal (29-Feb-2016) 'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            '[ISNULL(occurrence,0) AS occurrence ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " ]

            If iEmpId > 0 Then
                strQ &= " AND cmexpbalance_tran.employeeunkid = '" & iEmpId & "' "
            End If
            If iExpTypeId > 0 Then
                strQ &= " AND cmexpense_master.expensetypeid = '" & iExpTypeId & "' "
            End If
            If iExpId > 0 Then
                strQ &= " AND cmexpense_master.expenseunkid  = '" & iExpId & "' "
            End If
            If mstrUserAccessFilter = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            strQ &= mstrUserAccessFilter
            strQ &= ") AS A WHERE 1 = 1 AND isopenelc = 1 " & _
                    "ORDER BY grpid,sortid,exptype,expname "

            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

            'Pinkal (21-Dec-2019) -- Start
            'Enhancement - Claim Retirement for NMB.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (21-Dec-2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, iListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr As DataRow In dsList.Tables(0).Rows
                If dr.Item("sdate").ToString.Trim.Length > 0 Then dr.Item("sdate") = eZeeDate.convertDate(dr.Item("sdate").ToString).ToShortDateString
                If dr.Item("edate").ToString.Trim.Length > 0 Then dr.Item("edate") = eZeeDate.convertDate(dr.Item("edate").ToString).ToShortDateString
            Next

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Display_List", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmexpbalance_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try


            'Shani(13-Aug-2015) -- Start
            'Changes in Employe Expense Assignment(C&R).
            If mintExpeseTypeId = enExpenseType.EXP_LEAVE AndAlso mintLeavebalanceSetting = enLeaveBalanceSetting.ELC Then
                Dim mdtELCEnddate As Date = GetEmployeeDates(mintEmployeeunkid, mintExpenseunkid, True)
                If mdtELCEnddate <> Nothing AndAlso mdtELCEnddate < mdtStartdate Then
                    If isExist(mintYearunkid, mintEmployeeunkid, mintExpenseunkid, -1, True, True, False, Nothing) Then
                        If CloseDueDateEmployeeELC(objDataOperation, mintCrexpbalanceunkid, mintYearunkid, mintEmployeeunkid, mintExpenseunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End If
            End If
            'Shani(13-Aug-2015) -- End


            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAccrue_Amount.ToString)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblIssue_Amount.ToString)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDaily_Amount.ToString)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_Bal.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@expensebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblExpensebf.ToString)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblActualamount.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblCfamount.ToString)
            objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityafter.ToString)
            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnoaction.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurrence.ToString)
            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)
            'Pinkal (29-Feb-2016) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@freq_cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFrequencyCycleId.ToString)
            objDataOperation.AddParameter("@eligible_year", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibleYear.ToString)
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecAdjustmentAmt.ToString)
            'Pinkal (03-Mar-2021) -- End


            strQ = "INSERT INTO cmexpbalance_tran ( " & _
                       "  yearunkid " & _
                       ", employeeunkid " & _
                       ", expenseunkid " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", accrue_amount " & _
                       ", issue_amount " & _
                       ", daily_amount " & _
                       ", remaining_bal " & _
                       ", userunkid " & _
                       ", expensebf " & _
                       ", actualamount " & _
                       ", cfamount " & _
                       ", eligibilityafter " & _
                       ", isnoaction " & _
                       ", isopenelc " & _
                       ", iselc " & _
                       ", isclose_fy " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                       ", occurrence " & _
                       ", remaining_occurrence  " & _
                       ", freq_cycleunkid " & _
                       ", eligible_year " & _
                       ", adjustment_amt " & _
                   ") VALUES (" & _
                       "  @yearunkid " & _
                       ", @employeeunkid " & _
                       ", @expenseunkid " & _
                       ", @startdate " & _
                       ", @enddate " & _
                       ", @accrue_amount " & _
                       ", @issue_amount " & _
                       ", @daily_amount " & _
                       ", @remaining_bal " & _
                       ", @userunkid " & _
                       ", @expensebf " & _
                       ", @actualamount " & _
                       ", @cfamount " & _
                       ", @eligibilityafter " & _
                       ", @isnoaction " & _
                       ", @isopenelc " & _
                       ", @iselc " & _
                       ", @isclose_fy " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                       ", @occurrence " & _
                       ", @remaining_occurrence  " & _
                       ", @freq_cycleunkid " & _
                       ", @eligible_year " & _
                       ", @adjustment_amt " & _
                   "); SELECT @@identity"

            'Pinkal (03-Mar-2021)-- KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.[adjustment_amt]

            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ ", @freq_cycleunkid", @eligible_year " & _]

            'Pinkal (29-Feb-2016) 'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            '[ ", occurrence, remaining_occurrence  "]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCrexpbalanceunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cmexpbalance_tran", "crexpbalanceunkid", mintCrexpbalanceunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmexpbalance_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpbalanceunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAccrue_Amount.ToString)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblIssue_Amount.ToString)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDaily_Amount.ToString)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_Bal.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@expensebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblExpensebf.ToString)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblActualamount.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblCfamount.ToString)
            objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityafter.ToString)
            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnoaction.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurrence.ToString)
            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)
            'Pinkal (29-Feb-2016) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@freq_cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFrequencyCycleId.ToString)
            objDataOperation.AddParameter("@eligible_year", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibleYear.ToString)
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecAdjustmentAmt.ToString)
            'Pinkal (03-Mar-2021) -- End


            strQ = "UPDATE cmexpbalance_tran SET " & _
                   "  yearunkid = @yearunkid" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", expenseunkid = @expenseunkid" & _
                   ", startdate = @startdate" & _
                   ", enddate = @enddate" & _
                   ", accrue_amount = @accrue_amount" & _
                   ", issue_amount = @issue_amount" & _
                   ", daily_amount = @daily_amount" & _
                   ", remaining_bal = @remaining_bal" & _
                   ", userunkid = @userunkid" & _
                   ", expensebf = @expensebf" & _
                   ", actualamount = @actualamount" & _
                   ", cfamount = @cfamount" & _
                   ", eligibilityafter = @eligibilityafter" & _
                   ", isnoaction = @isnoaction" & _
                   ", isopenelc = @isopenelc" & _
                   ", iselc = @iselc" & _
                   ", isclose_fy = @isclose_fy" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", occurrence = @occurrence" & _
                   ", remaining_occurrence = @remaining_occurrence " & _
                   ", freq_cycleunkid = @freq_cycleunkid " & _
                   ", eligible_year = @eligible_year " & _
                   ", adjustment_amt = @adjustment_amt " & _
                   "WHERE crexpbalanceunkid = @crexpbalanceunkid "

            'Pinkal (03-Mar-2021)-- KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.[ ", adjustment_amt = @adjustment_amt " & _]

            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", freq_cycleunkid = @freq_cycleunkid " & _]

            'Pinkal (29-Feb-2016) 'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            '[ ", occurrence = @occurrence, remaining_occurrence = @remaining_occurrence " ]


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "cmexpbalance_tran", mintCrexpbalanceunkid, "crexpbalanceunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", mintCrexpbalanceunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmexpbalance_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE cmexpbalance_tran SET " & _
                   " isvoid = @isvoid " & _
                   ",voiduserunkid = @voiduserunkid " & _
                   ",voiddatetime = @voiddatetime " & _
                   ",voidreason = @voidreason " & _
                   "WHERE crexpbalanceunkid = @crexpbalanceunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cmexpbalance_tran", "crexpbalanceunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Sandeep Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    Public Function isUsed(ByVal xExpenseId As Integer, ByVal xEmployeeId As Integer) As Boolean
        'Pinkal (26-Feb-2019) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'strQ = "SELECT issue_amount FROM cmexpbalance_tran WHERE crexpbalanceunkid = @crexpbalanceunkid AND issue_amount > 0"

            strQ = " SELECT COUNT(*) TotalCount " & _
                         " FROM cmclaim_request_master " & _
                         " LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_tran.isvoid = 0 AND cmclaim_request_tran.expenseunkid = @expenseunkid " & _
                         " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.expenseunkid = @expenseunkid " & _
                         " LEFT JOIN cmclaim_process_tran ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_process_tran.isvoid = 0 AND cmclaim_process_tran.expenseunkid = @expenseunkid " & _
                         " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.employeeunkid = @employeeunkid  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "Used")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnIsUsed As Boolean = False
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0)("TotalCount")) > 0 Then blnIsUsed = True
            End If

            Return blnIsUsed

            'Pinkal (26-Feb-2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeDates(ByVal intEmployeeId As Integer, ByVal intExpenseUnkid As Integer, ByVal mblnStartdate As Boolean) As Date
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mdtDate As Date = Nothing
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT   " & _
                   " cmexpbalance_tran.startdate as  startdate " & _
                   ",cmexpbalance_tran.enddate as stopdate " & _
                   " FROM cmexpbalance_tran " & _
                   " WHERE employeeunkid = @employeeunkid  AND expenseunkid = @expenseunkid AND isopenelc = 1 AND iselc = 1 AND isvoid = 0 "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "AccrueEmployee")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If mblnStartdate Then
                    mdtDate = CDate(dsList.Tables(0).Rows(0)("startdate"))
                Else
                    mdtDate = CDate(dsList.Tables(0).Rows(0)("stopdate"))
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeDates", mstrModuleName)
        End Try
        Return mdtDate
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function GetNewEmployeeForELC(ByVal iExpenseUnkid As Integer, ByVal intYearId As Integer) As String
    Public Function GetNewEmployeeForELC(ByVal strDatabaseName As String, _
                                         ByVal intUserUnkid As Integer, _
                                         ByVal intYearUnkid As Integer, _
                                         ByVal intCompanyUnkid As Integer, _
                                         ByVal dtPeriodStart As Date, _
                                         ByVal dtPeriodEnd As Date, _
                                         ByVal strUserModeSetting As String, _
                                         ByVal blnOnlyApproved As Boolean, _
                                         ByVal iExpenseUnkid As Integer, _
                                         ByVal intYearId As Integer) As String
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrEmployeeID As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, True, , strDatabaseName)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'strQ = " SELECT STUFF((SELECT ',' +  CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) " & _
            '         " FROM hremployee_master " & _
            '         " WHERE employeeunkid NOT IN (SELECT  employeeunkid  FROM cmexpbalance_tran WHERE isvoid = 0 AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1) " & _
            '         " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '         " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '         " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '         " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate  " & _
            '         " AND ISNULL(CONVERT(CHAR(8),reinstatement_date,112),@startdate) <=  @startdate AND hremployee_master.isapproved = 1 " & _
            '         " ORDER BY hremployee_master.employeeunkid FOR XML PATH('')),1,1,'') AS EmployeeID "

            strQ = " SELECT STUFF((SELECT ',' +  CAST(hremployee_master.employeeunkid AS NVARCHAR(max)) " & _
                   " FROM hremployee_master "

            strQ &= xDateJoinQry

            strQ &= " WHERE employeeunkid NOT IN (SELECT  employeeunkid  FROM cmexpbalance_tran WHERE isvoid = 0 AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1) "

            strQ &= xDateFilterQry

            strQ &= " AND hremployee_master.isapproved = 1 " & _
                     " ORDER BY hremployee_master.employeeunkid FOR XML PATH('')),1,1,'') AS EmployeeID "

            'Shani(24-Aug-2015) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpenseUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'Shani(24-Aug-2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "AccrueEmployee")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeID = dsList.Tables(0).Rows(0)("EmployeeID").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNewEmployeeForELC", mstrModuleName)
        End Try
        Return mstrEmployeeID
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeForDueDateELC(ByVal dtDate As Date, ByVal intExpenseUnkid As Integer, ByVal intYearId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrEmployeeID As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT STUFF(( SELECT  ',' + CAST(employeeunkid AS NVARCHAR(50)) " & _
                      " FROM cmexpbalance_tran " & _
                      " WHERE isvoid = 0  " & _
                      " AND CONVERT(CHAR(8), cmexpbalance_tran.enddate, 112) < @Date AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1 " & _
                      " ORDER BY cmexpbalance_tran.employeeunkid " & _
                      " FOR  XML PATH('')), 1, 1, '') AS EmployeeID "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            dsList = objDataOperation.ExecQuery(strQ, "AccrueEmployee")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeID = dsList.Tables(0).Rows(0)("EmployeeID").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeForDueDateELC", mstrModuleName)
        End Try
        Return mstrEmployeeID
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iYearId As Integer, _
                            ByVal iEmployeeId As Integer, _
                            ByVal iExpenseId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal blnIsOpenELC As Boolean = False, _
                            Optional ByVal blnIsELC As Boolean = False, _
                            Optional ByVal IsCloseFy As Boolean = False, _
                            Optional ByVal mdtDueDate As Date = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT crexpbalanceunkid FROM cmexpbalance_tran WHERE isvoid = 0 " & _
                   "AND employeeunkid = '" & iEmployeeId & "' AND yearunkid = '" & iYearId & "' AND expenseunkid = '" & iExpenseId & "' "

            If intUnkid > 0 Then
                strQ &= " AND crexpbalanceunkid <> '" & intUnkid & "' "
            End If

            If blnIsELC Then
                If blnIsOpenELC Then
                    strQ &= " AND iselc = 1 AND  isopenelc = 1 "
                    'Shani(13-Aug-2015) -- Start
                    'Changes in Employe Expense Assignment(C&R).

                    If mdtDueDate <> Nothing Then
                        strQ &= " AND convert(char(8),enddate,112) > @duedate "
                        objDataOperation.AddParameter("@duedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDueDate))
                    End If

                    'Shani(13-Aug-2015) -- End


                Else
                    strQ &= " AND iselc = 1 AND isopenelc = 0 "
                End If
            End If

            If IsCloseFy Then
                strQ &= " AND ISNULL(isclose_fy,0) = 1 "
            Else
                strQ &= " AND ISNULL(isclose_fy,0) = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani(13-Aug-2015) -- Start
            'Changes in Employe Expense Assignment(C&R).
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintCrexpbalanceunkid = CInt(dsList.Tables(0).Rows(0)("crexpbalanceunkid"))
            End If
            'Shani(13-Aug-2015) -- End

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_Balance(ByVal iEmpId As Integer, _
                           ByVal iExpenseId As Integer, _
                           ByVal iYearId As Integer, _
                           ByVal xAsonDate As Date, _
                           Optional ByVal blnIsOpenELC As Boolean = False, _
                           Optional ByVal blnIsELC As Boolean = False, _
                           Optional ByVal IsCloseFy As Boolean = False) As DataSet

        'Pinkal (30-Apr-2018) - Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   " startdate " & _
                   ", enddate " & _
                   ",  accrue_amount AS accrue_amount " & _
                   ", issue_amount " & _
                   ",  daily_amount AS daily_amount " & _
                   ", remaining_bal  AS bal " & _
                   ", CAST(0 AS FLOAT) AS balasondate " & _
                   ", uomunkid " & _
                   ", CASE WHEN uomunkid = 1 THEN @Qty " & _
                   "       WHEN uomunkid = 2 THEN @Amt END AS uom " & _
                   ", ISNULL(occurrence,0) AS occurrence " & _
                   ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                   ", ISNULL(freq_cycleunkid,1) AS freq_cycleunkid " & _
                   ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
                   ", ISNULL(eligible_year,0) AS eligible_year " & _
                   "FROM cmexpbalance_tran " & _
                   "    JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmexpbalance_tran.expenseunkid " & _
                   "WHERE isvoid = 0 " & _
                   " AND employeeunkid = '" & iEmpId & "' AND yearunkid = '" & iYearId & "' AND cmexpbalance_tran.expenseunkid = '" & iExpenseId & "' "


            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", ISNULL(freq_cycleunkid,1) AS freq_cycleunkid ", ISNULL(eligibilityafter,0) AS eligibilityafter " ]

            'Pinkal (03-Aug-2018) -- Start   'Bug - Changes For PACRA [Ticket No: 2425] remaining_bal  AS bal, CAST(0 AS FLOAT) AS balasondate 


            'Pinkal (30-Apr-2018) - Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.[ "issue_amount, uomunkid , CAST(remaining_bal AS DECIMAL(10,2)) AS bal, CAST(0 AS DECIMAL(10,2)) AS balasondate " & _]

            'Pinkal (29-Feb-2016) 'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            '[ISNULL(occurrence,0) AS occurrence ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " ]

            If blnIsELC Then
                If blnIsOpenELC Then
                    strQ &= " AND iselc = 1 AND  isopenelc = 1 "
                Else
                    strQ &= " AND iselc = 1 AND isopenelc = 0 "
                End If
            End If

            If IsCloseFy Then
                strQ &= " AND ISNULL(isclose_fy,0) = 1 "
            Else
                strQ &= " AND ISNULL(isclose_fy,0) = 0 "
            End If

            objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
            objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            If xAsonDate <> Nothing Then
                For Each dr As DataRow In dsList.Tables(0).Rows
                    If CInt(dr("uomunkid")) = enExpUoM.UOM_QTY Then
                        If CDate(dr("enddate")).Date <= xAsonDate.Date Then
                            dr("balasondate") = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dr("startdate")), CDate(dr("enddate")).AddDays(1)) * CDec(dr("daily_amount"))) - CDec(dr("issue_amount")), 2).ToString()
                        Else
                            dr("balasondate") = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dr("startdate")), xAsonDate) * CDec(dr("daily_amount"))) - CDec(dr("issue_amount")), 2).ToString()
                        End If
                    ElseIf CInt(dr("uomunkid")) = enExpUoM.UOM_AMOUNT Then
                        dr("balasondate") = CDec(dr("bal"))
                    End If
                Next
                dsList.Tables(0).AcceptChanges()
            End If
            'Pinkal (30-Apr-2018) - End


            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Balance; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function CloseDueDateEmployeeELC(ByVal objDataOperation As clsDataOperation, ByVal intBalanceunkid As Integer, ByVal intYearunkid As Integer, ByVal intEmployeeId As Integer, ByVal intExpenseId As Integer) As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            strQ = " UPDATE cmexpbalance_tran Set " & _
                   " isopenelc = 0 " & _
                   " WHERE employeeunkid = @employeeunkid AND expenseunkid = @expenseunkid AND isopenelc = 1 AND iselc = 1 AND isvoid = 0 AND yearunkid = @yearunkid " & _
                   " AND crexpbalanceunkid = " & intBalanceunkid

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCrexpbalanceunkid = intBalanceunkid
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", mintCrexpbalanceunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "CloseDueDateEmployeeELC", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
        End Try
    End Function


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateEligibleYearOFEmployee(ByVal objOperation As clsDataOperation, ByVal xEmployeeId As Integer, ByVal xExpenseID As Integer _
                                                                                                    , ByVal xYearID As Integer, ByVal xLeaveBalanceSetting As Integer, ByVal xClaimDate As Date _
                                                                                                    , ByVal xCrexpbalanceunkid As Integer, ByVal xUserId As Integer, Optional ByVal blnIsCancel As Boolean = False) As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Try

            If objOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objOperation
            End If

            strQ = " UPDATE cmexpbalance_tran Set "

            If blnIsCancel Then
                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strQ &= " eligible_year = YEAR(DATEADD(MONTH,eligibilityafter * -1,@claimDate)) "
                strQ &= " eligible_year = eligible_year  - (eligible_year -  YEAR(@claimDate))  "
                'Pinkal (07-Mar-2019) -- End
            Else
                strQ &= " eligible_year = YEAR(DATEADD(MONTH,eligibilityafter,@claimDate)) "
            End If

            strQ &= " WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND expenseunkid = @expenseunkid  AND yearunkid = @yearunkid "

            If xLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " AND isopenelc = 1 AND iselc =  1"
            ElseIf xLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                strQ &= " AND isclose_fy = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearID)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseID)
            objDataOperation.AddParameter("@claimDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xClaimDate)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Dim objCommonATLog As New clsCommonATLog
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END
            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", xCrexpbalanceunkid, False, xUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEligibleYearOFEmployee; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            dsList = Nothing
            If objOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (26-Feb-2019) -- End


    'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeExpesneDataForAdjustment(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                  , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                  , ByVal intLeaveBalanceSetting As Integer, ByVal intExpenseId As Integer, ByVal dtDate As Date _
                                                                                  , ByVal dbEndDate As Date, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                                  , Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            strQ = " SELECT cmexpbalance_tran.crexpbalanceunkid " & _
                       ",Cast(0 AS Bit) AS ischeck " & _
                       ",cmexpbalance_tran.employeeunkid " & _
                       ",hremployee_master.employeecode " & _
                       ",ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       ",hremployee_master.appointeddate " & _
                       ",hrjob_master.job_name AS Job " & _
                       ",ISNULL(cmexpbalance_tran.accrue_amount,0.00) AS Accrue_amount " & _
                       ",ISNULL(cmexpbalance_tran.issue_amount,0.00) AS Issue_amount" & _
                       ",ISNULL(cmexpbalance_tran.Remaining_Bal,0.00) AS Remaining_Bal" & _
                       ", cmexpbalance_tran.startdate " & _
                       ", cmexpbalance_tran.enddate " & _
                       ", cmexpbalance_tran.isopenelc as isopenelc " & _
                       ", cmexpbalance_tran.iselc as iselc " & _
                       ", cmexpbalance_tran.isclose_fy as isclose_fy " & _
                       ", 0.00 as Adjustment_Amt " & _
                       " FROM cmexpbalance_tran " & _
                       " JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpbalance_tran.employeeunkid " & _
                       " LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         jobunkid " & _
                       "        ,employeeunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "    FROM hremployee_categorization_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsondate " & _
                       ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                       " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid "


            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry & " "
                End If
            End If


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry & " "
            End If

            strQ &= " WHERE isvoid = 0 AND cmexpbalance_tran.expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                strQ &= " AND ISNULL(CONVERT(CHAR(8),cmexpbalance_tran.enddate,112), @dbEndDate ) >= @Asondate  " & _
                            " AND isclose_fy = 0 "
            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " AND CONVERT(CHAR(8), cmexpbalance_tran.enddate, 112) >= @Asondate " & _
                        " AND isopenelc = 1 AND iselc = 1 "
            End If
            If mstrFilter.Trim.Length > 0 Then
                strQ &= "AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearUnkid)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
            objDataOperation.AddParameter("@Asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            objDataOperation.AddParameter("@EmpAsondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                objDataOperation.AddParameter("@dbEndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dbEndDate))
            End If
            dsList = objDataOperation.ExecQuery(strQ, "ExpenseData")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExpesneDataForAdjustment; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Pinkal (03-Mar-2021) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsExpCommonMethods", 2, "Leave")
            Language.setMessage("clsExpCommonMethods", 3, "Medical")
            Language.setMessage("clsExpCommonMethods", 4, "Training")
			Language.setMessage("clsExpCommonMethods", 6, "Quantity")
			Language.setMessage("clsExpCommonMethods", 7, "Amount")
			Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
			Language.setMessage("clsExpCommonMethods", 9, "Imprest")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
