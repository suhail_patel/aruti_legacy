﻿'************************************************************************************************************************************
'Class Name : clscmclaim_request_master.vb
'Purpose    :
'Date       :01-Mar-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Web
Imports System.Threading

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsclaim_request_master
    Private Shared ReadOnly mstrModuleName As String = "clsclaim_request_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private objCRTran As New clsclaim_request_tran

#Region " Private variables "

    Private mintCrmasterunkid As Integer
    Private mstrClaimrequestno As String = String.Empty
    Private mintExpensetypeid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtTransactiondate As Date
    Private mstrClaim_Remark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintCancelFromModuleId As Integer = 0
    Private mblnIscancel As Boolean
    Private mintCanceluserunkid As Integer
    Private mstrCancel_Remark As String = String.Empty
    Private mdtCancel_Datetime As Date
    Private mintLoginemployeeunkid As Integer
    Private mintStatusunkid As Integer
    Private mintModulerefunkid As Integer = 0
    Private mintReferenceunkid As Integer = 0
    Private mintLeaveTypeId As Integer = 0
    Private mstrRemark As String = String.Empty
    Private mintFromModuleId As Integer = 0
    Private mblnLeaveApproverForLeaveType As Boolean = False
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private mblnIsBalanceDeduct As Boolean = False
    Private mintYearId As Integer = 0
    Private mintLeaveBalanceSetting As Integer = 0


    'Pinkal (16-Dec-2014) -- Start
    'Enhancement - Claim & Request For Web.
    Private mintVoidLoginemployeeunkid As Integer = 0
    'Pinkal (16-Dec-2014) -- End


    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
    Private mintCompanyId As Integer
    Private mstrArutiSelfServiceURL As String = ""
    Private mintLoginMode As enLogin_Mode
    Private mstrWebFormName As String = ""
    Private mstrEmpFirstName As String = ""
    Private mstrEmpMiddleName As String = ""
    Private mstrEmpSurName As String = ""
    Private mstrEmpCode As String = ""
    Private mstrEmpMail As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Dim objDoOperation As clsDataOperation
    Private dtLeaveApprover As DataTable = Nothing

    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    Private mintApproverTranID As Integer = 0
    Private mintApproverID As Integer = 0
    'Pinkal (10-Jan-2017) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    Private mintTrainingRequisitionMasterId As Integer = 0
    'S.SANDEEP [09-OCT-20


    'Pinkal (22-Jul-2019) -- Start
    'Enhancement - Working on P2P Document attachment in Claim Request for NMB.
    Private mP2PPostDatabyte As String = ""
    'Pinkal (22-Jul-2019) -- End

    'Pinkal (19-Jul-2021)-- Start
    'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
    Private mblnIsPrinted As Boolean = False
    'Pinkal (19-Jul-2021)-- End

    Private objThread As Thread

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crmasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crmasterunkid() As Integer
        Get
            Return mintCrmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCrmasterunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimrequestno
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Claimrequestno() As String
        Get
            Return mstrClaimrequestno
        End Get
        Set(ByVal value As String)
            mstrClaimrequestno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expensetypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expensetypeid() As Integer
        Get
            Return mintExpensetypeid
        End Get
        Set(ByVal value As Integer)
            mintExpensetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claim_remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Claim_Remark() As String
        Get
            Return mstrClaim_Remark
        End Get
        Set(ByVal value As String)
            mstrClaim_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CancelFromModuleId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CancelFromModuleId() As Integer
        Get
            Return mintCancelFromModuleId
        End Get
        Set(ByVal value As Integer)
            mintCancelFromModuleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Canceluserunkid() As Integer
        Get
            Return mintCanceluserunkid
        End Get
        Set(ByVal value As Integer)
            mintCanceluserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cancel_Remark() As String
        Get
            Return mstrCancel_Remark
        End Get
        Set(ByVal value As String)
            mstrCancel_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_datetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cancel_Datetime() As Date
        Get
            Return mdtCancel_Datetime
        End Get
        Set(ByVal value As Date)
            mdtCancel_Datetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set modulerefunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Modulerefunkid() As Integer
        Get
            Return mintModulerefunkid
        End Get
        Set(ByVal value As Integer)
            mintModulerefunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referenceunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Referenceunkid() As Integer
        Get
            Return mintReferenceunkid
        End Get
        Set(ByVal value As Integer)
            mintReferenceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get LeaveTypeId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _LeaveTypeId() As Integer
        Get
            Return mintLeaveTypeId
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FromModuleID
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _FromModuleId() As Integer
        Get
            Return mintFromModuleId
        End Get
        Set(ByVal value As Integer)
            mintFromModuleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set LeaveApproverForLeaveType
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _LeaveApproverForLeaveType() As Boolean
        Get
            Return mblnLeaveApproverForLeaveType
        End Get
        Set(ByVal value As Boolean)
            mblnLeaveApproverForLeaveType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set IsPaymentApprovalwithLeaveApproval
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsPaymentApprovalwithLeaveApproval() As Boolean
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set IsBalanceDeduct
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsBalanceDeduct() As Boolean
        Get
            Return mblnIsBalanceDeduct
        End Get
        Set(ByVal value As Boolean)
            mblnIsBalanceDeduct = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set YearId
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _YearId() As Integer
        Get
            Return mintYearId
        End Get
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveBalanceSetting
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _LeaveBalanceSetting() As Integer
        Get
            Return mintLeaveBalanceSetting
        End Get
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property


    'Pinkal (16-Dec-2014) -- Start
    'Enhancement - Claim & Request For Web.
    ''' <summary>
    ''' Purpose: Set VoidLoginEmployeeID
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _VoidLoginEmployeeID() As Integer
        Get
            Return mintVoidLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginemployeeunkid = value
        End Set
    End Property
    'Pinkal (16-Dec-2014) -- End


    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.

    ''' <summary>
    ''' Purpose: Get or Set _CompanyID
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _CompanyID() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _ArutiSelfServiceURL
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ArutiSelfServiceURL() As String
        Get
            Return mstrArutiSelfServiceURL
        End Get
        Set(ByVal value As String)
            mstrArutiSelfServiceURL = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _LoginMode
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LoginMode() As enLogin_Mode
        Get
            Return mintLoginMode
        End Get
        Set(ByVal value As enLogin_Mode)
            mintLoginMode = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set _EmployeeCode
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _EmployeeCode() As String
        Get
            Return mstrEmpCode
        End Get
        Set(ByVal value As String)
            mstrEmpCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _EmployeeFirstName
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _EmployeeFirstName() As String
        Get
            Return mstrEmpFirstName
        End Get
        Set(ByVal value As String)
            mstrEmpFirstName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _EmployeeMiddleName
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _EmployeeMiddleName() As String
        Get
            Return mstrEmpMiddleName
        End Get
        Set(ByVal value As String)
            mstrEmpMiddleName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _EmployeeSurName
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _EmployeeSurName() As String
        Get
            Return mstrEmpSurName
        End Get
        Set(ByVal value As String)
            mstrEmpSurName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _EmpMail
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _EmpMail() As String
        Get
            Return mstrEmpMail
        End Get
        Set(ByVal value As String)
            mstrEmpMail = value
        End Set
    End Property


    'Pinkal (13-Jul-2015) -- End

    'Pinkal (22-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE ON WEB.

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property
    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Pinkal (22-Oct-2015) -- End


    'Pinkal (26-Oct-2015) -- Start
    'Enhancement - Working on TRA Claim & Request Report Issue.

    Public WriteOnly Property _DoOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDoOperation = value
        End Set
    End Property

    'Pinkal (26-Oct-2015) -- End.

    'Pinkal (19-Oct-2016) -- Start
    'Enhancement - Solving Bug In Leave & Claim Approver for specific Leave application.

    Public Property _dtLeaveApprover() As DataTable
        Get
            Return dtLeaveApprover
        End Get
        Set(ByVal value As DataTable)
            dtLeaveApprover = value
        End Set
    End Property

    'Pinkal (19-Oct-2016) -- End


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

    ''' <summary>
    ''' Purpose: Get or Set _ApproverTranID
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ApproverTranID() As Integer
        Get
            Return mintApproverTranID
        End Get
        Set(ByVal value As Integer)
            mintApproverTranID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _ApproverID
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ApproverID() As Integer
        Get
            Return mintApproverID
        End Get
        Set(ByVal value As Integer)
            mintApproverID = value
        End Set
    End Property

    'Pinkal (10-Jan-2017) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    ''' <summary>
    ''' Purpose: Get linkedmasterid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _TrainingRequisitionMasterId() As Integer
        Get
            Return mintTrainingRequisitionMasterId
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequisitionMasterId = value
        End Set
    End Property
    'S.SANDEEP [09-OCT-2018] -- END


    'Pinkal (19-Jul-2021)-- Start
    'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
    Public Property _IsPrinted() As Boolean
        Get
            Return mblnIsPrinted
        End Get
        Set(ByVal value As Boolean)
            mblnIsPrinted = value
        End Set
    End Property
    'Pinkal (19-Jul-2021)-- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (26-Oct-2015) -- Start
        'Enhancement - Working on TRA Claim & Request Report Issue.
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        'Pinkal (26-Oct-2015) -- End.


        Try
            strQ = "SELECT " & _
                    "  cmclaim_request_master.crmasterunkid " & _
                    ", cmclaim_request_master.claimrequestno " & _
                    ", cmclaim_request_master.expensetypeid " & _
                    ", cmclaim_request_master.employeeunkid " & _
                    ", cmclaim_request_master.transactiondate " & _
                    ", cmclaim_request_master.claim_remark " & _
                    ", cmclaim_request_master.userunkid " & _
                    ", cmclaim_request_master.isvoid " & _
                    ", cmclaim_request_master.voiduserunkid " & _
                    ", cmclaim_request_master.voiddatetime " & _
                    ", cmclaim_request_master.voidreason " & _
                    ", cmclaim_request_master.cancelfrommoduleid " & _
                    ", cmclaim_request_master.iscancel " & _
                    ", cmclaim_request_master.canceluserunkid " & _
                    ", cmclaim_request_master.cancel_remark " & _
                    ", cmclaim_request_master.cancel_datetime " & _
                    ", cmclaim_request_master.loginemployeeunkid " & _
                    ", ISNULL(cmclaim_request_master.voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                    ", cmclaim_request_master.statusunkid " & _
                    ", cmclaim_request_master.modulerefunkid " & _
                    ", cmclaim_request_master.referenceunkid " & _
                    ", cmclaim_request_master.remark " & _
                    ", cmclaim_request_master.frommoduleid " & _
                    ", cmclaim_request_master.isbalancededuct " & _
                    ", cmclaim_request_master.linkedmasterid " & _
                    ", ISNULL(CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN leavetypeunkid ELSE 0 END,0) AS leavetypeid " & _
                    ", cmclaim_request_master.p2pposteddata " & _
                    ", cmclaim_request_master.isprinted " & _
                    "FROM cmclaim_request_master " & _
                    " LEFT JOIN lvleaveform ON cmclaim_request_master.referenceunkid = lvleaveform.formunkid AND modulerefunkid = '" & enModuleReference.Leave & "' " & _
                    "WHERE crmasterunkid = @crmasterunkid "

            'Pinkal (19-Jul-2021)-- Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.[cmclaim_request_master.isprinted]

            'Pinkal (22-Jul-2019) -- 'Enhancement - Working on P2P Document attachment in Claim Request for NMB.[ ", cmclaim_request_master.p2pposteddata " & _]

            'S.SANDEEP [09-OCT-2018] -- START {linkedmasterid} -- END
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCrmasterunkid = CInt(dtRow.Item("crmasterunkid"))
                mstrClaimrequestno = dtRow.Item("claimrequestno").ToString
                mintExpensetypeid = CInt(dtRow.Item("expensetypeid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mstrClaim_Remark = dtRow.Item("claim_remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintCancelFromModuleId = CInt(dtRow.Item("cancelfrommoduleid"))
                mblnIscancel = CBool(dtRow.Item("iscancel"))
                mintCanceluserunkid = CInt(dtRow.Item("canceluserunkid"))
                mstrCancel_Remark = dtRow.Item("cancel_remark").ToString

                If IsDBNull(dtRow.Item("cancel_datetime")) = False Then
                    mdtCancel_Datetime = dtRow.Item("cancel_datetime")
                Else
                    mdtCancel_Datetime = Nothing
                End If
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidLoginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintModulerefunkid = CInt(dtRow.Item("modulerefunkid"))
                mintReferenceunkid = CInt(dtRow.Item("referenceunkid"))
                mintFromModuleId = CInt(dtRow.Item("frommoduleid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintLeaveTypeId = CInt(dtRow.Item("leavetypeid"))
                mblnIsBalanceDeduct = CBool(dtRow.Item("isbalancededuct"))
                'S.SANDEEP [09-OCT-2018] -- START
                mintTrainingRequisitionMasterId = CInt(dtRow("linkedmasterid"))
                'S.SANDEEP [09-OCT-2018] -- END


                'Pinkal (22-Jul-2019) -- Start
                'Enhancement - Working on P2P Document attachment in Claim Request for NMB.
                If IsDBNull(dtRow.Item("p2pposteddata")) = False Then
                    mP2PPostDatabyte = System.Text.Encoding.Unicode.GetString(dtRow.Item("p2pposteddata"))
                End If
                'Pinkal (22-Jul-2019) -- End

                'Pinkal (19-Jul-2021)-- Start
                'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.
                If IsDBNull(dtRow.Item("isprinted")) = False Then
                    mblnIsPrinted = CBool(dtRow.Item("isprinted"))
                End If
                'Pinkal (19-Jul-2021)-- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (26-Oct-2015) -- Start
            'Enhancement - Working on TRA Claim & Request Report Issue.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (26-Oct-2015) -- End.
        End Try
    End Sub


    '' <summary>
    '' Modify By: Sandeep Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 

    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    'Public Function GetList(ByVal strTableName As String, ByVal blnPaymentApprovalwithLeaveApproval As Boolean, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
    '                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
    '                                  , ByVal strEmployeeAsOnDate As String _
    '                                  , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
    '                                  , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
    '                                  , Optional ByVal mstrFilter As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation


    '    Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '    xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)
    '    Try
    '        strQ = "SELECT " & _
    '        "	 cmclaim_request_master.crmasterunkid " & _
    '        "	,cmclaim_request_master.claimrequestno " & _
    '        "	,cmclaim_request_master.expensetypeid " & _
    '        "	,cmclaim_request_master.employeeunkid " & _
    '        "	,cmclaim_request_master.transactiondate " & _
    '        "	,cmclaim_request_master.claim_remark " & _
    '        "	,cmclaim_request_master.userunkid " & _
    '        "	,cmclaim_request_master.isvoid " & _
    '        "	,cmclaim_request_master.voiduserunkid " & _
    '        "	,cmclaim_request_master.voiddatetime " & _
    '        "	,cmclaim_request_master.voidreason " & _
    '        "	,cmclaim_request_master.cancelfrommoduleid " & _
    '        "	,cmclaim_request_master.iscancel " & _
    '        "	,cmclaim_request_master.canceluserunkid " & _
    '        "	,cmclaim_request_master.cancel_remark " & _
    '        "	,cmclaim_request_master.cancel_datetime " & _
    '        "	,cmclaim_request_master.loginemployeeunkid " & _
    '        "	,cmclaim_request_master.voidloginemployeeunkid " & _
    '        "   ,cmclaim_request_master.statusunkid " & _
    '        "   ,cmclaim_request_master.modulerefunkid " & _
    '        "   ,cmclaim_request_master.referenceunkid " & _
    '        "   ,cmclaim_request_master.frommoduleid " & _
    '        "   ,cmclaim_request_master.remark " & _
    '        "	,ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS ename " & _
    '        "	,CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
    '        "   ,CASE WHEN expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
    '        "         WHEN expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
    '                "         WHEN expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
    '                "         WHEN expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
    '                "         WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
    '        "   ,CASE WHEN cmclaim_request_master.statusunkid = 1 then @Approve " & _
    '        "         WHEN cmclaim_request_master.statusunkid = 2 then @Pending  " & _
    '        "         WHEN cmclaim_request_master.statusunkid = 3 then @Reject  " & _
    '        "         WHEN cmclaim_request_master.statusunkid = 4 then @ReSchedule " & _
    '        "         WHEN cmclaim_request_master.statusunkid = 6 then @Cancel " & _
    '                "         WHEN cmclaim_request_master.statusunkid = 7 then @Issued " & _
    '                "         WHEN cmclaim_request_master.statusunkid = 8 then @Return " & _
    '                "   END as status " & _
    '        "   ,CASE WHEN expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN lvleaveform.formno " & _
    '        "         WHEN expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN '' " & _
    '                "         WHEN expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN '' " & _
    '                "         WHEN expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN ''  " & _
    '                "         WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN '' END AS reference " & _
    '                    ",  ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
    '                    ",  ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
    '                    ",  ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
    '                    ",  ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                    ",  ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
    '                    ",  ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
    '                    ",  ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
    '                    ",  ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
    '                    ",  ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
    '                    ",  ISNULL(Alloc.classunkid,0) AS classunkid " & _
    '                    ",  ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
    '                    ",  ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
    '                    ",  ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
    '                    ",  ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
    '                    ",  ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
    '         ", cmclaim_request_master.isbalancededuct " & _
    '                    ",  cmclaim_request_master.linkedmasterid " & _
    '                    ", ISNULL(cmclaim_request_master.p2prequisitionid,'') AS p2prequisitionid "




    '        'Pinkal (05-Jun-2020) -- Start
    '        'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON. 
    '        If blnPaymentApprovalwithLeaveApproval = False Then
    '            strQ &= ", ISNULL(Req.ReqAmount,0.00) AS ReqAmount " & _
    '                        ", ISNULL(App.ApprovedAmt,0.00) AS ApprovedAmt "

    '            'Pinkal (04-Nov-2020) -- Start
    '            'Bug NMB -  Bug Resolved for payment approval ith leave approval. 
    '        Else
    '            strQ &= ", 0.00 AS ReqAmount " & _
    '                        ", 0.00 AS ApprovedAmt "
    '            'Pinkal (04-Nov-2020) -- End
    '        End If
    '        'Pinkal (05-Jun-2020) -- End


    '        strQ &= " FROM cmclaim_request_master " & _
    '        "	JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        "   LEFT JOIN lvleaveform ON cmclaim_request_master.referenceunkid = lvleaveform.formunkid AND modulerefunkid = '" & enModuleReference.Leave & "' " & _
    '                    "   LEFT JOIN " & _
    '                    "   ( " & _
    '                    "    SELECT " & _
    '                    "         stationunkid " & _
    '                    "        ,deptgroupunkid " & _
    '                    "        ,departmentunkid " & _
    '                    "        ,sectiongroupunkid " & _
    '                    "        ,sectionunkid " & _
    '                    "        ,unitgroupunkid " & _
    '                    "        ,unitunkid " & _
    '                    "        ,teamunkid " & _
    '                    "        ,classgroupunkid " & _
    '                    "        ,classunkid " & _
    '                    "        ,employeeunkid " & _
    '                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                    "    FROM hremployee_transfer_tran " & _
    '                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
    '                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
    '                    "   LEFT JOIN " & _
    '                    "  ( " & _
    '                    "           SELECT " & _
    '                    "         jobunkid " & _
    '                    "        ,jobgroupunkid " & _
    '                    "        ,employeeunkid " & _
    '                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                    "    FROM hremployee_categorization_tran " & _
    '                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
    '                    "  ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
    '                    "   JOIN " & _
    '                    "   ( " & _
    '                    "    SELECT " & _
    '                    "         gradegroupunkid " & _
    '                    "        ,gradeunkid " & _
    '                    "        ,gradelevelunkid " & _
    '                    "        ,employeeunkid " & _
    '                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
    '                    "    FROM prsalaryincrement_tran " & _
    '                    "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & strEmployeeAsOnDate & "' " & _
    '                    " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 "


    '        'Pinkal (28-Apr-2020) -- Start
    '        'Optimization  - Working on Process Optimization and performance for require module.	

    '        If blnPaymentApprovalwithLeaveApproval = False Then
    '            strQ &= " JOIN ( " & _
    '                         " SELECT " & _
    '                         "      crmasterunkid " & _
    '                         "	   ,ISNULL(SUM(amount), 0) AS ReqAmount " & _
    '                         "  FROM cmclaim_request_tran " & _
    '                         "  WHERE cmclaim_request_tran.isvoid = 0 " & _
    '                         " GROUP BY crmasterunkid " & _
    '                         " ) AS Req ON Req.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
    '                         " LEFT JOIN ( " & _
    '                         "  SELECT " & _
    '                         "      crmasterunkid " & _
    '                         "      ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
    '                         "   FROM cmclaim_approval_tran " & _
    '                         "   WHERE isvoid = 0 And statusunkid = 1 AND " & _
    '                         "   crapproverunkid IN ( " & _
    '                         "                  SELECT crapproverunkid FROM  " & _
    '                         "                  ( " & _
    '                         "                                  SELECT " & _
    '                         "                                           ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
    '                         "                                          ,cm.crmasterunkid " & _
    '                         "                                          ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
    '                         "                                  FROM cmclaim_approval_tran cm " & _
    '                         "                                  JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
    '                         "                                  JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
    '                         "                                  WHERE cm.isvoid = 0 " & _
    '                         "                  ) as cnt WHERE cnt.rno= 1 AND cnt.crmasterunkid =  cmclaim_approval_tran.crmasterunkid " & _
    '                         "                                  ) " & _
    '                         "	GROUP BY crmasterunkid" & _
    '                         " ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid "
    '        End If

    '        'Pinkal (27-Aug-2020) -- Bug NMB:  Working on IIS Freezing and Dump Issue for NMB. [ AND cnt.crmasterunkid =  cmclaim_approval_tran.crmasterunkid]

    '        'Pinkal (28-Apr-2020) -- End

    '        'Pinkal (11-Sep-2019) --  'Enhancement NMB - Working On Claim Retirement for NMB. [ "WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype ,]

    '        'Pinkal (13-Mar-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ ", ISNULL(cmclaim_request_master.p2prequisitionid,'') AS p2prequisitionid " & _]

    '        'S.SANDEEP [09-OCT-2018] -- START {linkedmasterid} -- END

    '        'Pinkal (07-Mar-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.["         WHEN cmclaim_request_master.statusunkid = 8 then @Return " & _]

    '        'Pinkal (04-Feb-2019) -- Start
    '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '        If blnApplyUserAccessFilter Then
    '            If xUACQry.Trim.Length > 0 Then
    '                strQ &= " " & xUACQry
    '            End If
    '        End If
    '        'Pinkal (04-Feb-2019) -- End



    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            strQ &= " " & xAdvanceJoinQry
    '        End If

    '        strQ &= "   WHERE 1 = 1 AND cmclaim_request_master.isvoid = 0 "

    '        If blnPaymentApprovalwithLeaveApproval Then
    '            strQ &= " AND cmclaim_request_master.frommoduleid <> " & enExpFromModuleID.FROM_LEAVE
    '        End If


    '        If blnApplyUserAccessFilter = True Then
    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                strQ &= " AND " & xUACFiltrQry & " "
    '            End If
    '        End If


    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= "AND " & mstrFilter & " "
    '        End If

    '        objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
    '        objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
    '        objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
    '        objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
    '        'Pinkal (11-Sep-2019) -- End


    '        objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '        objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
    '        objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
    '        objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))

    '        'Pinkal (07-Mar-2019) -- Start
    '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '        objDataOperation.AddParameter("@Return", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 899, "Return"))
    '        'Pinkal (07-Mar-2019) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    Public Function GetList(ByVal strTableName As String, ByVal blnPaymentApprovalwithLeaveApproval As Boolean, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal strEmployeeAsOnDate As String _
                                      , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                      , Optional ByVal mstrFilter As String = "" _
                                      , Optional ByVal xIncludeIn_ActiveEmployee As Boolean = False) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""

        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
        If blnApplyUserAccessFilter Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

        Try



            strQ = " SELECT   hremployee_master.employeeunkid " & _
                       ", hremployee_master.employeecode " & _
                       ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
                       ", hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS ename " & _
                       ",  ISNULL(ADF.stationunkid,0) AS stationunkid " & _
                       ",  ISNULL(ADF.deptgroupunkid,0) AS deptgroupunkid " & _
                       ",  ISNULL(ADF.departmentunkid,0) AS departmentunkid " & _
                       ",  ISNULL(ADF.sectiongroupunkid,0) AS sectiongroupunkid " & _
                       ",  ISNULL(ADF.sectionunkid,0) AS sectionunkid " & _
                       ",  ISNULL(ADF.unitgroupunkid,0) AS unitgroupunkid " & _
                       ",  ISNULL(ADF.unitunkid,0) AS unitunkid " & _
                       ",  ISNULL(ADF.teamunkid,0) AS teamunkid " & _
                       ",  ISNULL(ADF.classgroupunkid,0) AS classgroupunkid " & _
                       ",  ISNULL(ADF.classunkid,0) AS classunkid " & _
                       ",  ISNULL(ADF.jobgroupunkid,0) AS jobgroupunkid " & _
                       ",  ISNULL(ADF.jobunkid,0) AS jobunkid " & _
                       ",  ISNULL(ADF.gradegroupunkid,0) AS gradegroupunkid " & _
                       ",  ISNULL(ADF.gradeunkid,0) AS gradeunkid " & _
                       ",  ISNULL(ADF.gradelevelunkid,0) AS gradelevelunkid " & _
                       " INTO #tblEmp " & _
                       " FROM " & xDatabaseName & "..hremployee_master "


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= " " & xAdvanceJoinQry & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            strQ &= "WHERE 1 = 1 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If



            strQ &= "SELECT " & _
                    "	 cmclaim_request_master.crmasterunkid " & _
                    "	,cmclaim_request_master.claimrequestno " & _
                    "	,cmclaim_request_master.expensetypeid " & _
                    "	,cmclaim_request_master.employeeunkid " & _
                    "	,cmclaim_request_master.transactiondate " & _
                    "	,cmclaim_request_master.claim_remark " & _
                    "	,cmclaim_request_master.userunkid " & _
                    "	,cmclaim_request_master.isvoid " & _
                    "	,cmclaim_request_master.voiduserunkid " & _
                    "	,cmclaim_request_master.voiddatetime " & _
                    "	,cmclaim_request_master.voidreason " & _
                    "	,cmclaim_request_master.cancelfrommoduleid " & _
                    "	,cmclaim_request_master.iscancel " & _
                    "	,cmclaim_request_master.canceluserunkid " & _
                    "	,cmclaim_request_master.cancel_remark " & _
                    "	,cmclaim_request_master.cancel_datetime " & _
                    "	,cmclaim_request_master.loginemployeeunkid " & _
                    "	,cmclaim_request_master.voidloginemployeeunkid " & _
                    "   ,cmclaim_request_master.statusunkid " & _
                    "   ,cmclaim_request_master.modulerefunkid " & _
                    "   ,cmclaim_request_master.referenceunkid " & _
                    "   ,cmclaim_request_master.frommoduleid " & _
                    "   ,cmclaim_request_master.remark " & _
                    "	,#tblEmp.ename " & _
                    "	,CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
                    "   ,CASE WHEN expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
                    "   ,CASE WHEN cmclaim_request_master.statusunkid = 1 then @Approve " & _
                    "         WHEN cmclaim_request_master.statusunkid = 2 then @Pending  " & _
                    "         WHEN cmclaim_request_master.statusunkid = 3 then @Reject  " & _
                    "         WHEN cmclaim_request_master.statusunkid = 4 then @ReSchedule " & _
                    "         WHEN cmclaim_request_master.statusunkid = 6 then @Cancel " & _
                    "         WHEN cmclaim_request_master.statusunkid = 7 then @Issued " & _
                    "         WHEN cmclaim_request_master.statusunkid = 8 then @Return " & _
                    "   END as status " & _
                    "   ,CASE WHEN expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN lvleaveform.formno " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN '' " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN '' " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN ''  " & _
                    "         WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN '' END AS reference " & _
                        ",  ISNULL(#tblEmp.stationunkid,0) AS stationunkid " & _
                        ",  ISNULL(#tblEmp.deptgroupunkid,0) AS deptgroupunkid " & _
                        ",  ISNULL(#tblEmp.departmentunkid,0) AS departmentunkid " & _
                        ",  ISNULL(#tblEmp.sectiongroupunkid,0) AS sectiongroupunkid " & _
                        ",  ISNULL(#tblEmp.sectionunkid,0) AS sectionunkid " & _
                        ",  ISNULL(#tblEmp.unitgroupunkid,0) AS unitgroupunkid " & _
                        ",  ISNULL(#tblEmp.unitunkid,0) AS unitunkid " & _
                        ",  ISNULL(#tblEmp.teamunkid,0) AS teamunkid " & _
                        ",  ISNULL(#tblEmp.classgroupunkid,0) AS classgroupunkid " & _
                        ",  ISNULL(#tblEmp.classunkid,0) AS classunkid " & _
                        ",  ISNULL(#tblEmp.jobgroupunkid,0) AS jobgroupunkid " & _
                        ",  ISNULL(#tblEmp.jobunkid,0) AS jobunkid " & _
                        ",  ISNULL(#tblEmp.gradegroupunkid,0) AS gradegroupunkid " & _
                        ",  ISNULL(#tblEmp.gradeunkid,0) AS gradeunkid " & _
                        ",  ISNULL(#tblEmp.gradelevelunkid,0) AS gradelevelunkid " & _
                     ", cmclaim_request_master.isbalancededuct " & _
                        ",  cmclaim_request_master.linkedmasterid " & _
                        ", ISNULL(cmclaim_request_master.p2prequisitionid,'') AS p2prequisitionid "

		            If blnPaymentApprovalwithLeaveApproval = False Then
                strQ &= ", ISNULL(Req.ReqAmount,0.00) AS ReqAmount " & _
                            ", ISNULL(App.ApprovedAmt,0.00) AS ApprovedAmt "

            Else
                strQ &= ", 0.00 AS ReqAmount " & _
                            ", 0.00 AS ApprovedAmt "
            End If

            strQ &= " FROM cmclaim_request_master " & _
                    "   LEFT JOIN lvleaveform ON cmclaim_request_master.referenceunkid = lvleaveform.formunkid AND lvleaveform.isvoid = 0 AND modulerefunkid = '" & enModuleReference.Leave & "' " & _
                    "	JOIN #tblEmp ON cmclaim_request_master.employeeunkid = #tblEmp.employeeunkid "


            If blnPaymentApprovalwithLeaveApproval = False Then
                strQ &= " LEFT JOIN ( " & _
                             " SELECT " & _
                             "      crmasterunkid " & _
                             "	   ,ISNULL(SUM(amount), 0) AS ReqAmount " & _
                             "  FROM cmclaim_request_tran " & _
                             "  WHERE cmclaim_request_tran.isvoid = 0 " & _
                             " GROUP BY crmasterunkid " & _
                             " ) AS Req ON Req.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                             " LEFT JOIN ( " & _
                             "  SELECT " & _
                             "      crmasterunkid " & _
                             "      ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                             "      ,statusunkid " & _
                             "   FROM cmclaim_approval_tran " & _
                             "   WHERE isvoid = 0 And statusunkid = 1 AND " & _
                             "   crapproverunkid IN ( " & _
                             "                  SELECT crapproverunkid FROM  " & _
                             "                  ( " & _
                             "                                  SELECT " & _
                             "                                           ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                             "                                          ,cm.crmasterunkid " & _
                             "                                          ,cm.approveremployeeunkid " & _
                             "                                          ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                             "                                  FROM cmclaim_approval_tran cm " & _
                             "                                  JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                             "                                  JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                             "                                  WHERE cm.isvoid = 0 " & _
                             "                  ) as cnt WHERE cnt.rno= 1 " & _
                             "                          AND cnt.crmasterunkid =  cmclaim_approval_tran.crmasterunkid " & _
                             "                          AND cnt.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                 "                          AND cnt.approveremployeeunkid= cmclaim_approval_tran.approveremployeeunkid " & _
                             "                                  ) " & _
                             "	GROUP BY crmasterunkid,statusunkid" & _
                             " ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                             " AND app.statusunkid = cmclaim_request_master.statusunkid "
            End If



            strQ &= "   WHERE 1 = 1 AND cmclaim_request_master.isvoid = 0 "

            If blnPaymentApprovalwithLeaveApproval Then
                strQ &= " AND cmclaim_request_master.frommoduleid <> " & enExpFromModuleID.FROM_LEAVE
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= "AND " & mstrFilter & " "
            End If


            strQ &= " DROP TABLE #tblEmp "


            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))
            objDataOperation.AddParameter("@Return", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 899, "Return"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (12-Oct-2021)-- End


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.


    '''' <summary>
    '''' Modify By: Sandeep Sharma        
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (cmclaim_request_master) </purpose>
    '''' </summary>
    'Public Function Insert(ByVal mdtExpense As DataTable, Optional ByVal iCompanyId As Integer = 0, _
    '                                 Optional ByVal _objOperation As clsDataOperation = Nothing, Optional ByVal blnPaymentApprovalwithLeaveApproval As Boolean = False) As Boolean

    '    Dim intVocNoType As Integer = 0
    '    Dim objConfig As New clsConfigOptions
    '    objConfig._Companyunkid = IIf(iCompanyId = 0, Company._Object._Companyunkid, iCompanyId)
    '    intVocNoType = objConfig._ClaimRequestVocNoType
    '    If intVocNoType = 0 Then
    '        If isExists(mstrClaimrequestno) Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim Number already exists. Please define new Claim Number.")
    '            Return False
    '        End If
    '    ElseIf intVocNoType = 1 Then
    '        mstrClaimrequestno = ""
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    If _objOperation Is Nothing Then
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()
    '    Else
    '        objDataOperation = _objOperation
    '    End If

    '    Try
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@claimrequestno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimrequestno.ToString)
    '        objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
    '        objDataOperation.AddParameter("@claim_remark", SqlDbType.NText, eZeeDataType.DESC_SIZE, mstrClaim_Remark.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime <> Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
    '        objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId.ToString)
    '        objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
    '        objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
    '        objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Remark.ToString)
    '        If mdtCancel_Datetime <> Nothing Then
    '            objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime)
    '        Else
    '            objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        End If
    '        objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

    '        'Pinkal (16-Dec-2014) -- Start
    '        'Enhancement - Claim & Request For Web.
    '        objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid.ToString)
    '        'Pinkal (16-Dec-2014) -- End

    '        objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '        objDataOperation.AddParameter("@modulerefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefunkid.ToString)
    '        objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceunkid.ToString)
    '        objDataOperation.AddParameter("@frommoduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromModuleId.ToString)
    '        objDataOperation.AddParameter("@isbalancededuct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBalanceDeduct.ToString)
    '        objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)

    '        strQ = "INSERT INTO cmclaim_request_master ( " & _
    '                   "  claimrequestno " & _
    '                   ", expensetypeid " & _
    '                   ", employeeunkid " & _
    '                   ", transactiondate " & _
    '                   ", claim_remark " & _
    '                   ", userunkid " & _
    '                   ", isvoid " & _
    '                   ", voiduserunkid " & _
    '                   ", voiddatetime " & _
    '                   ", voidreason " & _
    '                   ", cancelfrommoduleid " & _
    '                   ", iscancel " & _
    '                   ", canceluserunkid " & _
    '                   ", cancel_remark " & _
    '                   ", cancel_datetime " & _
    '                   ", loginemployeeunkid " & _
    '                   ", voidloginemployeeunkid " & _
    '                   ", statusunkid " & _
    '                   ", modulerefunkid " & _
    '                   ", referenceunkid " & _
    '                   ", frommoduleId " & _
    '                   ", isbalancededuct " & _
    '                   ", remark " & _
    '               ") VALUES (" & _
    '                   "  @claimrequestno " & _
    '                   ", @expensetypeid " & _
    '                   ", @employeeunkid " & _
    '                   ", @transactiondate " & _
    '                   ", @claim_remark " & _
    '                   ", @userunkid " & _
    '                   ", @isvoid " & _
    '                   ", @voiduserunkid " & _
    '                   ", @voiddatetime " & _
    '                   ", @voidreason " & _
    '                   ", @cancelfrommoduleid " & _
    '                   ", @iscancel " & _
    '                   ", @canceluserunkid " & _
    '                   ", @cancel_remark " & _
    '                   ", @cancel_datetime " & _
    '                   ", @loginemployeeunkid " & _
    '                   ", @voidloginemployeeunkid " & _
    '                   ", @statusunkid " & _
    '                   ", @modulerefunkid " & _
    '                   ", @referenceunkid " & _
    '                   ", @frommoduleId " & _
    '                   ", @isbalancededuct " & _
    '                   ", @remark " & _
    '               "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintCrmasterunkid = dsList.Tables(0).Rows(0).Item(0)

    '        If intVocNoType = 1 Then
    '            If Set_AutoNumber(objDataOperation, mintCrmasterunkid, "cmclaim_request_master", "claimrequestno", "crmasterunkid", "NextClaimRequestVocNo", objConfig._ClaimRequestPrefix, iCompanyId) = False Then
    '                If objDataOperation.ErrorMessage <> "" Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If

    '            If Get_Saved_Number(objDataOperation, mintCrmasterunkid, "cmclaim_request_master", "claimrequestno", "crmasterunkid", mstrClaimrequestno) = False Then
    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            End If
    '        End If

    '        objCRTran._StatusId = mintStatusunkid
    '        objCRTran._ClaimRequestMasterId = mintCrmasterunkid
    '        objCRTran._DataTable = mdtExpense.Copy
    '        objCRTran._LeaveTypeId = mintLeaveTypeId
    '        objCRTran._IsPaymentApprovalwithLeaveApproval = blnPaymentApprovalwithLeaveApproval
    '        objCRTran._LeaveApproverForLeaveType = mblnLeaveApproverForLeaveType
    '        objCRTran._YearId = mintYearId
    '        objCRTran._LeaveBalanceSetting = mintLeaveBalanceSetting
    '        objCRTran._LoginEmployeeID = mintLoginemployeeunkid
    '        objCRTran._VoidLoginEmployeeID = mintVoidLoginemployeeunkid

    '        If objCRTran.Insert_Update_Delete(objDataOperation, mintUserunkid) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If objCRTran.Insert_Update_Approver(objDataOperation, mintEmployeeunkid, mintExpensetypeid, mintUserunkid, mintFromModuleId, , mintCrmasterunkid) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        'Pinkal (13-Jul-2015) -- Start
    '        'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
    '        If mintFromModuleId = enExpFromModuleID.FROM_EXPENSE Then
    '            Dim mstrPriorityFilter As String = ""
    '            Dim mintPriority As Integer = -1
    '            Dim objExpApproverTran As New clsclaim_request_approval_tran
    '            Dim dsApproverList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, mblnPaymentApprovalwithLeaveApproval, mintExpensetypeid, True, True, -1, -1, "", mintCrmasterunkid)
    '            If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
    '                mintPriority = dsApproverList.Tables(0).Compute("Min(crpriority)", "1=1")
    '                mstrPriorityFilter = "crpriority = " & mintPriority
    '            End If
    '            objExpApproverTran.SendMailToApprover(mintExpensetypeid, blnPaymentApprovalwithLeaveApproval, mintCrmasterunkid, mstrClaimrequestno, mintEmployeeunkid, mintPriority, 1, mstrPriorityFilter, mintCompanyId, mstrArutiSelfServiceURL, mintLoginMode, mintLoginemployeeunkid, mintUserunkid, mstrWebFormName)
    '            objExpApproverTran = Nothing
    '        End If
    '        'Pinkal (13-Jul-2015) -- End


    '        If _objOperation Is Nothing Then
    '            objDataOperation.ReleaseTransaction(True)
    '        End If

    '        Return True

    '    Catch ex As Exception
    '        If _objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        If _objOperation Is Nothing Then objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sandeep Sharma    
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (cmclaim_request_master) </purpose>
    '''' </summary>
    'Public Function Update(ByVal mdtExpense As DataTable, Optional ByVal _objOperation As clsDataOperation = Nothing, Optional ByVal blnPaymentApprovalwithLeaveApproval As Boolean = False) As Boolean

    '    If isExists(mstrClaimrequestno, mintCrmasterunkid) Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim Number already exists. Please define new Claim Number.")
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    If _objOperation Is Nothing Then
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()
    '    Else
    '        objDataOperation = _objOperation
    '    End If

    '    Try
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
    '        objDataOperation.AddParameter("@claimrequestno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimrequestno.ToString)
    '        objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
    '        objDataOperation.AddParameter("@claim_remark", SqlDbType.NText, eZeeDataType.DESC_SIZE, mstrClaim_Remark.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime <> Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
    '        objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId.ToString)
    '        objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
    '        objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
    '        objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Remark.ToString)
    '        If mdtCancel_Datetime <> Nothing Then
    '            objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime)
    '        Else
    '            objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        End If
    '        objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

    '        'Pinkal (16-Dec-2014) -- Start
    '        'Enhancement - Claim & Request For Web.
    '        objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid.ToString)
    '        'Pinkal (16-Dec-2014) -- End

    '        objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '        objDataOperation.AddParameter("@modulerefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefunkid.ToString)
    '        objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceunkid.ToString)
    '        objDataOperation.AddParameter("@frommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromModuleId.ToString)
    '        objDataOperation.AddParameter("@isbalancededuct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBalanceDeduct.ToString)
    '        objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)

    '        strQ = "UPDATE cmclaim_request_master SET " & _
    '               "  claimrequestno = @claimrequestno" & _
    '               ", expensetypeid = @expensetypeid" & _
    '               ", employeeunkid = @employeeunkid" & _
    '               ", transactiondate = @transactiondate" & _
    '               ", claim_remark = @claim_remark" & _
    '               ", userunkid = @userunkid" & _
    '               ", isvoid = @isvoid" & _
    '               ", voiduserunkid = @voiduserunkid" & _
    '               ", voiddatetime = @voiddatetime" & _
    '               ", voidreason = @voidreason" & _
    '               ", cancelfrommoduleid = @cancelfrommoduleid " & _
    '               ", iscancel = @iscancel" & _
    '               ", canceluserunkid = @canceluserunkid" & _
    '               ", cancel_remark = @cancel_remark" & _
    '               ", cancel_datetime = @cancel_datetime" & _
    '               ", loginemployeeunkid = @loginemployeeunkid " & _
    '               ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
    '               ", statusunkid = @statusunkid " & _
    '               ", modulerefunkid = @modulerefunkid" & _
    '               ", referenceunkid = @referenceunkid " & _
    '               ", frommoduleid = @frommoduleid " & _
    '               ", isbalancededuct = @isbalancededuct " & _
    '               ", remark = @remark " & _
    '               "WHERE crmasterunkid = @crmasterunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objCRTran._StatusId = mintStatusunkid
    '        objCRTran._ClaimRequestMasterId = mintCrmasterunkid
    '        objCRTran._DataTable = mdtExpense.Copy
    '        objCRTran._LeaveTypeId = mintLeaveTypeId
    '        objCRTran._IsPaymentApprovalwithLeaveApproval = blnPaymentApprovalwithLeaveApproval
    '        objCRTran._LeaveApproverForLeaveType = mblnLeaveApproverForLeaveType
    '        objCRTran._YearId = mintYearId
    '        objCRTran._LeaveBalanceSetting = mintLeaveBalanceSetting

    '        'Pinkal (16-Dec-2014) -- Start
    '        'Enhancement - Claim & Request For Web.
    '        objCRTran._LoginEmployeeID = mintLoginemployeeunkid
    '        objCRTran._VoidLoginEmployeeID = mintVoidLoginemployeeunkid
    '        'Pinkal (16-Dec-2014) -- End

    '        If objCRTran.Insert_Update_Delete(objDataOperation, mintUserunkid) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If objCRTran.Insert_Update_Approver(objDataOperation, mintEmployeeunkid, mintExpensetypeid, mintUserunkid, mintFromModuleId, , mintCrmasterunkid) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If _objOperation Is Nothing Then
    '            objDataOperation.ReleaseTransaction(True)
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        If _objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        If _objOperation Is Nothing Then objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma        
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_request_master) </purpose>
    ''' </summary>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal mdtExpense As DataTable _
                                    , ByVal dtCurrentDateAndTime As DateTime _
                                    , ByVal dtAttchement As DataTable _
                                    , Optional ByVal _objOperation As clsDataOperation = Nothing, Optional ByVal blnPaymentApprovalwithLeaveApproval As Boolean = False) As Boolean

        Dim intVocNoType As Integer = 0
        Dim objConfig As New clsConfigOptions


        'Pinkal (27-Aug-2020) -- Start
        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        'objConfig._Companyunkid = IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid)
        'intVocNoType = objConfig._ClaimRequestVocNoType
        'Dim mstrNewRequisitionRequestP2PServiceURL As String = objConfig._NewRequisitionRequestP2PServiceURL.Trim()
        'Dim mblnEmpNotificationClaimApplicationByManager As Boolean = objConfig._EmpNotificationForClaimApplicationByManager
        'mstrArutiSelfServiceURL = objConfig._ArutiSelfServiceURL

        intVocNoType = objConfig.GetKeyValue(IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid), "ClaimRequestVocNoType", Nothing)
        Dim mstrNewRequisitionRequestP2PServiceURL As String = objConfig.GetKeyValue(IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid), "NewRequisitionRequestP2PServiceURL", Nothing)

        'Pinkal (20-Nov-2020) -- Start
        'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
        'Dim mblnEmpNotificationClaimApplicationByManager As Boolean = objConfig.GetKeyValue(IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid), "EmpNotificationForClaimApplicationbyManager", Nothing)
        Dim mblnEmpNotificationClaimApplicationByManager As Boolean = False
        Dim mstrEmpNotificationClaimApplicationByManager As String = objConfig.GetKeyValue(IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid), "EmpNotificationForClaimApplicationbyManager", Nothing)
        If mstrEmpNotificationClaimApplicationByManager.Trim.Length > 0 Then
            mblnEmpNotificationClaimApplicationByManager = CBool(mstrEmpNotificationClaimApplicationByManager)
        End If
        'Pinkal (20-Nov-2020) -- End


        mstrArutiSelfServiceURL = objConfig.GetKeyValue(IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid), "ArutiSelfServiceURL", Nothing)
        Dim mstrClaimRequestPrefix As String = objConfig.GetKeyValue(IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid), "ClaimRequestPrefix", Nothing)
        'Pinkal (27-Aug-2020) -- End


        If intVocNoType = 0 Then
            If isExists(mstrClaimrequestno) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim Number already exists. Please define new Claim Number.")
                Return False
            End If
        ElseIf intVocNoType = 1 Then
            mstrClaimrequestno = ""
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If _objOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = _objOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimrequestno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimrequestno.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            objDataOperation.AddParameter("@claim_remark", SqlDbType.NText, eZeeDataType.DESC_SIZE, mstrClaim_Remark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Remark.ToString)
            If mdtCancel_Datetime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@modulerefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefunkid.ToString)
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceunkid.ToString)
            objDataOperation.AddParameter("@frommoduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromModuleId.ToString)
            objDataOperation.AddParameter("@isbalancededuct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBalanceDeduct.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@linkedmasterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequisitionMasterId)


            strQ = "INSERT INTO cmclaim_request_master ( " & _
                       "  claimrequestno " & _
                       ", expensetypeid " & _
                       ", employeeunkid " & _
                       ", transactiondate " & _
                       ", claim_remark " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", cancelfrommoduleid " & _
                       ", iscancel " & _
                       ", canceluserunkid " & _
                       ", cancel_remark " & _
                       ", cancel_datetime " & _
                       ", loginemployeeunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", statusunkid " & _
                       ", modulerefunkid " & _
                       ", referenceunkid " & _
                       ", frommoduleId " & _
                       ", isbalancededuct " & _
                       ", remark " & _
                       ", linkedmasterid " & _
                   ") VALUES (" & _
                       "  @claimrequestno " & _
                       ", @expensetypeid " & _
                       ", @employeeunkid " & _
                       ", @transactiondate " & _
                       ", @claim_remark " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @cancelfrommoduleid " & _
                       ", @iscancel " & _
                       ", @canceluserunkid " & _
                       ", @cancel_remark " & _
                       ", @cancel_datetime " & _
                       ", @loginemployeeunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @statusunkid " & _
                       ", @modulerefunkid " & _
                       ", @referenceunkid " & _
                       ", @frommoduleId " & _
                       ", @isbalancededuct " & _
                       ", @remark " & _
                       ", @linkedmasterid " & _
                   "); SELECT @@identity"
            'S.SANDEEP [09-OCT-2018] -- START {linkedmasterid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCrmasterunkid = dsList.Tables(0).Rows(0).Item(0)

            If intVocNoType = 1 Then

                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                'If Set_AutoNumber(objDataOperation, mintCrmasterunkid, "cmclaim_request_master", "claimrequestno", "crmasterunkid", "NextClaimRequestVocNo", objConfig._ClaimRequestPrefix, xCompanyUnkid) = False Then
                If Set_AutoNumber(objDataOperation, mintCrmasterunkid, "cmclaim_request_master", "claimrequestno", "crmasterunkid", "NextClaimRequestVocNo", mstrClaimRequestPrefix, xCompanyUnkid) = False Then
                    'Pinkal (27-Aug-2020) -- End
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintCrmasterunkid, "cmclaim_request_master", "claimrequestno", "crmasterunkid", mstrClaimrequestno) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            With objCRTran
                ._FormName = mstrFormName
                ._LoginEmployeeID = mintLoginemployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            objCRTran._StatusId = mintStatusunkid
            objCRTran._ClaimRequestMasterId = mintCrmasterunkid
            objCRTran._DataTable = mdtExpense.Copy()
            objCRTran._LeaveTypeId = mintLeaveTypeId
            objCRTran._IsPaymentApprovalwithLeaveApproval = blnPaymentApprovalwithLeaveApproval
            objCRTran._LeaveApproverForLeaveType = mblnLeaveApproverForLeaveType
            objCRTran._YearId = mintYearId
            objCRTran._LeaveBalanceSetting = mintLeaveBalanceSetting
            objCRTran._LoginEmployeeID = mintLoginemployeeunkid
            objCRTran._VoidLoginEmployeeID = mintVoidLoginemployeeunkid

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            objCRTran._ApproverTranID = mintApproverTranID
            objCRTran._ApproverID = mintApproverID
            'Pinkal (10-Jan-2017) -- End

            If objCRTran.Insert_Update_Delete(objDataOperation, mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            mdtExpense = objCRTran._DataTable

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'Dim mblnIsClaimFormBudgetMandatory As Boolean = False
            Dim mblnIsHRExpense As Boolean = True
            If mstrNewRequisitionRequestP2PServiceURL.Trim.Length > 0 AndAlso mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
                'If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                '    mblnIsClaimFormBudgetMandatory = True
                'End If
                If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                    mblnIsHRExpense = False
                End If
            End If

            If mblnIsHRExpense Then

                'Pinkal (04-Feb-2019) -- End
            objCRTran._dtLeaveApprover = dtLeaveApprover

            If objCRTran.Insert_Update_Approver(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                , xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeunkid _
                                                                , mintExpensetypeid, mintUserunkid, mintFromModuleId, dtCurrentDateAndTime, 0, mintCrmasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintFromModuleId = enExpFromModuleID.FROM_EXPENSE Then
                Dim mstrPriorityFilter As String = ""
                Dim mintPriority As Integer = -1
                Dim objExpApproverTran As New clsclaim_request_approval_tran
                Dim dsApproverList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, blnPaymentApprovalwithLeaveApproval, xDatabaseName _
                                                                                                                                    , mintUserunkid, eZeeDate.convertDate(mdtEmployeeAsonDate), mintExpensetypeid, True, True, -1, "", mintCrmasterunkid)

                If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
                    mintPriority = dsApproverList.Tables(0).Compute("Min(crpriority)", "1=1")
                    mstrPriorityFilter = "crpriority = " & mintPriority
                End If

                    'Pinkal (22-Oct-2021)-- Start
                    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                    objExpApproverTran._ClientIP = mstrWebClientIP
                    objExpApproverTran._FormName = mstrWebFormName
                    objExpApproverTran._HostName = mstrWebHostName
                    'Pinkal (22-Oct-2021)-- End


                    'Pinkal (27-Apr-2019) -- Start
                    'Enhancement - Audit Trail changes.
                    With objExpApproverTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginemployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    'Pinkal (27-Apr-2019) -- End

                'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'objExpApproverTran.SendMailToApprover(mintExpensetypeid, blnPaymentApprovalwithLeaveApproval, mintCrmasterunkid, mstrClaimrequestno _
                    '                                                            , mintEmployeeunkid, mintPriority, 1, mstrPriorityFilter, xDatabaseName, eZeeDate.convertDate(mdtEmployeeAsonDate) _
                    '                                                            , mintCompanyId, mstrArutiSelfServiceURL, mintLoginMode, mintLoginemployeeunkid, mintUserunkid, mstrWebFormName)

                objExpApproverTran.SendMailToApprover(mintExpensetypeid, blnPaymentApprovalwithLeaveApproval, mintCrmasterunkid, mstrClaimrequestno _
                                                                            , mintEmployeeunkid, mintPriority, 1, mstrPriorityFilter, xDatabaseName, eZeeDate.convertDate(mdtEmployeeAsonDate) _
                                                                              , xCompanyUnkid, mstrArutiSelfServiceURL, mintLoginMode, mintLoginemployeeunkid, mintUserunkid, mstrWebFormName)
                    'Pinkal (04-Feb-2020) -- End
                objExpApproverTran = Nothing
            End If

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                If mblnEmpNotificationClaimApplicationByManager AndAlso mintUserunkid > 0 Then
                    If mintFromModuleId = enExpFromModuleID.FROM_LEAVE AndAlso blnPaymentApprovalwithLeaveApproval = False Then
                        SendEmployeeNotificationClaimApplicationByManager(mintExpensetypeid, mintEmployeeunkid, mintCrmasterunkid, mstrClaimrequestno, xDatabaseName, mintUserunkid _
                                                                                                     , mdtEmployeeAsonDate, xCompanyUnkid, mstrArutiSelfServiceURL, mintLoginMode, mstrWebFormName)
                    ElseIf mintFromModuleId <> enExpFromModuleID.FROM_LEAVE Then
                        SendEmployeeNotificationClaimApplicationByManager(mintExpensetypeid, mintEmployeeunkid, mintCrmasterunkid, mstrClaimrequestno, xDatabaseName, mintUserunkid _
                                                                                                     , mdtEmployeeAsonDate, xCompanyUnkid, mstrArutiSelfServiceURL, mintLoginMode, mstrWebFormName)
                    End If
                End If
                'Pinkal (04-Feb-2020) -- End

            End If

            'Pinkal (20-Nov-2018) -- End

            If dtAttchement IsNot Nothing AndAlso dtAttchement.Rows.Count > 0 Then
                If dtAttchement.Select("AUD = 'A'").Length > 0 Then
                    For Each dRow As DataRow In dtAttchement.Select("AUD = 'A'")
                        Dim dXRow As DataRow = dRow
                        Dim intTran = mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") = dXRow.Item("GUID").ToString).Select(Function(X) X.Field(Of Integer)("crtranunkid")).First
                        dRow.Item("transactionunkid") = intTran
                        dRow.AcceptChanges()
                    Next
                End If

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = dtAttchement
                With objDocument
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing

            End If


            If _objOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            If _objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If _objOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma    
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmclaim_request_master) </purpose>
    ''' </summary>
    Public Function Update(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal mdtExpense As DataTable _
                                    , ByVal dtCurrentDateAndTime As DateTime _
                                    , ByVal dtAttchement As DataTable _
                                    , Optional ByVal _objOperation As clsDataOperation = Nothing, Optional ByVal blnPaymentApprovalwithLeaveApproval As Boolean = False) As Boolean

        If isExists(mstrClaimrequestno, mintCrmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim Number already exists. Please define new Claim Number.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (04-Feb-2019) -- Start
        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
        Dim objConfig As New clsConfigOptions
        Dim mstrNewRequisitionRequestP2PServiceURL As String = objConfig.GetKeyValue(xCompanyUnkid, "NewRequisitionRequestP2PServiceURL")
        objConfig = Nothing
        'Pinkal (04-Feb-2019) -- End


        If _objOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = _objOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@claimrequestno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimrequestno.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            objDataOperation.AddParameter("@claim_remark", SqlDbType.NText, eZeeDataType.DESC_SIZE, mstrClaim_Remark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Remark.ToString)
            If mdtCancel_Datetime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@modulerefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefunkid.ToString)
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceunkid.ToString)
            objDataOperation.AddParameter("@frommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromModuleId.ToString)
            objDataOperation.AddParameter("@isbalancededuct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBalanceDeduct.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)

            'S.SANDEEP [09-OCT-2018] -- START
            objDataOperation.AddParameter("@linkedmasterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequisitionMasterId)
            'S.SANDEEP [09-OCT-2018] -- END

            strQ = "UPDATE cmclaim_request_master SET " & _
                   "  claimrequestno = @claimrequestno" & _
                   ", expensetypeid = @expensetypeid" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", transactiondate = @transactiondate" & _
                   ", claim_remark = @claim_remark" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason" & _
                   ", cancelfrommoduleid = @cancelfrommoduleid " & _
                   ", iscancel = @iscancel" & _
                   ", canceluserunkid = @canceluserunkid" & _
                   ", cancel_remark = @cancel_remark" & _
                   ", cancel_datetime = @cancel_datetime" & _
                   ", loginemployeeunkid = @loginemployeeunkid " & _
                   ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                   ", statusunkid = @statusunkid " & _
                   ", modulerefunkid = @modulerefunkid" & _
                   ", referenceunkid = @referenceunkid " & _
                   ", frommoduleid = @frommoduleid " & _
                   ", isbalancededuct = @isbalancededuct " & _
                   ", remark = @remark " & _
                   ", linkedmasterid = @linkedmasterid " & _
                   "WHERE crmasterunkid = @crmasterunkid "
            'S.SANDEEP [09-OCT-2018] -- START {linkedmasterid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            With objCRTran
                ._FormName = mstrFormName
                ._LoginEmployeeID = mintLoginemployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            objCRTran._StatusId = mintStatusunkid
            objCRTran._ClaimRequestMasterId = mintCrmasterunkid
            objCRTran._DataTable = mdtExpense.Copy
            objCRTran._LeaveTypeId = mintLeaveTypeId
            objCRTran._IsPaymentApprovalwithLeaveApproval = blnPaymentApprovalwithLeaveApproval
            objCRTran._LeaveApproverForLeaveType = mblnLeaveApproverForLeaveType
            objCRTran._YearId = mintYearId
            objCRTran._LeaveBalanceSetting = mintLeaveBalanceSetting
            objCRTran._LoginEmployeeID = mintLoginemployeeunkid
            objCRTran._VoidLoginEmployeeID = mintVoidLoginemployeeunkid

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            objCRTran._ApproverTranID = mintApproverTranID
            objCRTran._ApproverID = mintApproverID
            'Pinkal (10-Jan-2017) -- End

            If objCRTran.Insert_Update_Delete(objDataOperation, mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'Dim mblnIsClaimFormBudgetMandatory As Boolean = False
            'If mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
            '    If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
            '        mblnIsClaimFormBudgetMandatory = True
            '    End If
            'End If

            'If mblnIsClaimFormBudgetMandatory Then
            '    mdtExpense = New DataView(mdtExpense, "AUD = 'D'", "", DataViewRowState.CurrentRows).ToTable()
            '    objCRTran._DataTable = mdtExpense.Copy()
            'Else
            '    mdtExpense = objCRTran._DataTable
            'End If
            Dim mblnIsHRExpense As Boolean = True
            If mstrNewRequisitionRequestP2PServiceURL.Trim.Length > 0 AndAlso mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
                If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                    mblnIsHRExpense = False
                End If
            End If

            If mblnIsHRExpense Then
                mdtExpense = objCRTran._DataTable
            Else
                mdtExpense = New DataView(mdtExpense, "AUD = 'D'", "", DataViewRowState.CurrentRows).ToTable()
                objCRTran._DataTable = mdtExpense.Copy()
            End If

            'Pinkal (04-Feb-2019) -- End

            'Pinkal (20-Nov-2018) -- End

            If objCRTran.Insert_Update_Approver(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                , xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeunkid _
                                                                , mintExpensetypeid, mintUserunkid, mintFromModuleId, dtCurrentDateAndTime, , mintCrmasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            If dtAttchement IsNot Nothing AndAlso dtAttchement.Rows.Count > 0 Then
                For Each dtRow As DataRow In mdtExpense.Select("AUD = 'A'")
                    For Each dRow As DataRow In dtAttchement.Select("GUID = '" & dtRow.Item("GUID") & "'")
                        dRow.Item("transactionunkid") = dtRow.Item("crtranunkid")
                        dRow.AcceptChanges()
                    Next
                Next

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = dtAttchement
                With objDocument
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If



            If _objOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If _objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If _objOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        mstrMessage = ""
        Try
            strQ = "SELECT " & _
                   "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='crmasterunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                Select Case dtRow.Item("TableName")
                    Case "cmclaim_request_master", "cmclaim_request_tran", "cmclaim_approval_tran"
                        Continue For
                End Select
                strQ = "SELECT crmasterunkid FROM " & dtRow.Item("TableName").ToString & " WHERE crmasterunkid = @crmasterunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            If blnIsUsed = True Then mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry you cannot delete this claim form. Reason this form is already linked with some transactions.")
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (19-Mar-2015) -- Start
    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '' <summary>
    '' Modify By: Sandeep Sharma
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Function Delete(ByVal iUnkid As Integer, ByVal iUserId As Integer, Optional ByVal _objOperation As clsDataOperation = Nothing) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception

    '    If _objOperation Is Nothing Then
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()
    '    Else
    '        objDataOperation = _objOperation
    '    End If
    '    Try
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

    '        If mintVoidLoginemployeeunkid > 0 Then
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
    '        Else
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '        End If

    '        objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)

    '        StrQ = "UPDATE cmclaim_request_master SET " & _
    '               "  isvoid = 1 " & _
    '               ", voiduserunkid = @voiduserunkid " & _
    '               ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
    '               ", voiddatetime = @voiddatetime " & _
    '               ", voidreason = @voidreason " & _
    '               "WHERE crmasterunkid = @crmasterunkid "

    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "UPDATE cmclaim_request_tran SET " & _
    '               "  isvoid = 1 " & _
    '               ", voiduserunkid = @voiduserunkid " & _
    '               ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
    '               ", voiddatetime = @voiddatetime " & _
    '               ", voidreason = @voidreason " & _
    '               "WHERE crmasterunkid = @crmasterunkid AND isvoid = 0"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

    '        If mintVoidLoginemployeeunkid > 0 Then
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
    '        Else
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '        End If

    '        objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)
    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", iUnkid, "cmclaim_request_tran", "crtranunkid", 3, 3, , , , iUserId) = False Then
    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '        End If

    '        StrQ = "UPDATE cmclaim_approval_tran SET " & _
    '              "  isvoid = 1 " & _
    '              ", voiduserunkid = @voiduserunkid " & _
    '               ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
    '              ", voiddatetime = @voiddatetime " & _
    '              ", voidreason = @voidreason " & _
    '              "WHERE crmasterunkid = @crmasterunkid AND isvoid = 0 "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

    '        If mintVoidLoginemployeeunkid > 0 Then
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
    '        Else
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '        End If

    '        objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)
    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", iUnkid, "cmclaim_approval_tran", "crapprovaltranunkid", 3, 3, , , , iUserId) = False Then
    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '        End If


    '        'Pinkal (13-Jul-2015) -- Start
    '        'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
    '        If mintFromModuleId = enExpFromModuleID.FROM_EXPENSE Then
    '            Dim mstrPriorityFilter As String = ""
    '            Dim mintPriority As Integer = -1
    '            Dim objExpApproverTran As New clsclaim_request_approval_tran
    '            Dim dsApproverList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, mblnPaymentApprovalwithLeaveApproval, mintExpensetypeid, True, True, -1, -1, "", mintCrmasterunkid)
    '            If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
    '                mintPriority = dsApproverList.Tables(0).Compute("Min(crpriority)", "1=1")
    '                mstrPriorityFilter = "crpriority = " & mintPriority
    '            End If
    '            objExpApproverTran.SendMailToApprover(mintExpensetypeid, mblnPaymentApprovalwithLeaveApproval, mintCrmasterunkid, mstrClaimrequestno, mintEmployeeunkid, mintPriority, 1, mstrPriorityFilter, mintCompanyId, mstrArutiSelfServiceURL, mintLoginMode, mintLoginemployeeunkid, mintUserunkid, mstrWebFormName)
    '            objExpApproverTran = Nothing
    '        End If
    '        'Pinkal (13-Jul-2015) -- End

    '        If _objOperation Is Nothing Then
    '            objDataOperation.ReleaseTransaction(True)
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        If _objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function



    Public Function Delete(ByVal iUnkid As Integer, _
                           ByVal iUserId As Integer, _
                           ByVal xDatabaseName As String, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal strEmployeeAsOnDate As String, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal dtAttachment As DataTable, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                                   Optional ByVal _objOperation As clsDataOperation = Nothing, _
                                                   Optional ByVal mblnIsClaimFormBudgetMandatory As Boolean = False) As Boolean

        'Pinkal (20-Nov-2018) -- 'Enhancement - Working on P2P Integration for NMB.[Optional ByVal mblnIsClaimFormBudgetMandatory As Boolean = False]

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        If _objOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = _objOperation
        End If
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            If mintVoidLoginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
            Else
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            End If

            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)

            StrQ = "UPDATE cmclaim_request_master SET " & _
                   "  isvoid = 1 " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voidreason = @voidreason " & _
                   "WHERE crmasterunkid = @crmasterunkid "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "UPDATE cmclaim_request_tran SET " & _
                   "  isvoid = 1 " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voidreason = @voidreason " & _
                   "WHERE crmasterunkid = @crmasterunkid AND isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            If mintVoidLoginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
            Else
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            End If

            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", iUnkid, "cmclaim_request_tran", "crtranunkid", 3, 3, , , , iUserId) = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            StrQ = "UPDATE cmclaim_approval_tran SET " & _
                  "  isvoid = 1 " & _
                  ", voiduserunkid = @voiduserunkid " & _
                   ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                  ", voiddatetime = @voiddatetime " & _
                  ", voidreason = @voidreason " & _
                  "WHERE crmasterunkid = @crmasterunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            If mintVoidLoginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
            Else
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            End If

            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", iUnkid, "cmclaim_approval_tran", "crapprovaltranunkid", 3, 3, , , , iUserId) = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            If mblnIsClaimFormBudgetMandatory = False Then
            If mintFromModuleId = enExpFromModuleID.FROM_EXPENSE Then
                Dim mstrPriorityFilter As String = ""
                Dim mintPriority As Integer = -1
                Dim objExpApproverTran As New clsclaim_request_approval_tran
                Dim dsApproverList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, mblnPaymentApprovalwithLeaveApproval, xDatabaseName, iUserId _
                                                                                                                                   , strEmployeeAsOnDate, mintExpensetypeid, True, True, -1, "", mintCrmasterunkid, objDataOperation)

                If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then
                    mintPriority = dsApproverList.Tables(0).Compute("Min(crpriority)", "1=1")
                    mstrPriorityFilter = "crpriority = " & mintPriority
                End If

                objExpApproverTran.SendMailToApprover(mintExpensetypeid, mblnPaymentApprovalwithLeaveApproval, mintCrmasterunkid, mstrClaimrequestno, mintEmployeeunkid, mintPriority, 1, mstrPriorityFilter _
                                                                            , xDatabaseName, strEmployeeAsOnDate, mintCompanyId, mstrArutiSelfServiceURL, mintLoginMode, mintLoginemployeeunkid, mintUserunkid, mstrWebFormName, objDataOperation)
                    objExpApproverTran = Nothing

                End If
            End If

            If dtAttachment IsNot Nothing Then
            Dim objDocument As New clsScan_Attach_Documents
            objDocument._Datatable = dtAttachment
            With objDocument
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginemployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
               ._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                Throw New Exception(objDocument._Message)
            End If
            objDocument = Nothing
            End If

            'Pinkal (20-Nov-2018) -- End

            If _objOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If _objOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Cancel(ByVal iUnkid As Integer, ByVal iUserId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelFromModuleId)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Remark)
            objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)

            StrQ = "UPDATE cmclaim_request_master SET " & _
                   " cancelfrommoduleid = @cancelfrommoduleid " & _
                   ", iscancel = 1 " & _
                   ", canceluserunkid = @canceluserunkid " & _
                   ", cancel_remark = @cancel_remark " & _
                   ", cancel_datetime = @cancel_datetime " & _
                   "WHERE crmasterunkid = @crmasterunkid "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "UPDATE cmclaim_request_tran SET " & _
                   "  iscancel = 1 " & _
                   ", canceluserunkid = @canceluserunkid " & _
                   ", cancel_remark = @cancel_remark " & _
                   ", cancel_datetime = @cancel_datetime " & _
                   "WHERE crmasterunkid = @crmasterunkid "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", iUnkid, "cmclaim_request_tran", "crtranunkid", 3, 3, , , , iUserId) = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Cancel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExists(ByVal iClaimRequestNo As String, Optional ByVal iUnkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "  crmasterunkid " & _
                   " ,claimrequestno " & _
                   " ,expensetypeid " & _
                   " ,employeeunkid " & _
                   " ,transactiondate " & _
                   " ,claim_remark " & _
                   " ,userunkid " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   " ,cancelfrommoduleid " & _
                   " ,iscancel " & _
                   " ,canceluserunkid " & _
                   " ,cancel_remark " & _
                   " ,cancel_datetime " & _
                   " ,loginemployeeunkid " & _
                   " ,frommoduleid " & _
                   " ,isbalancededuct " & _
                   "FROM cmclaim_request_master " & _
                   "WHERE isvoid = 0 AND iscancel = 0 " & _
                   "AND claimrequestno = @claimrequestno "

            If iUnkid > 0 Then
                StrQ &= " AND crmasterunkid <> @crmasterunkid"
                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            End If

            objDataOperation.AddParameter("@claimrequestno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iClaimRequestNo)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExists; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFinalApproverApprovedAmount(ByVal intEmpID As Integer, ByVal intExpenseTypeId As Integer, ByVal intRequestMstId As Integer _
                                                                          , Optional ByVal mblnIncludeOnlyImprest As Boolean = False, Optional ByVal xExpenseId As Integer = 0) As Decimal

        'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[Optional ByVal mblnIncludeOnlyImprest As Boolean = False, Optional ByVal xExpenseId As Integer = 0]

        Dim mdecAmount As Decimal = 0
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try


            Dim mstrApproverID As String = ""

            If mblnPaymentApprovalwithLeaveApproval AndAlso intExpenseTypeId = enExpenseType.EXP_LEAVE Then
                Dim dtTab As DataTable = Nothing
                Dim objpending As New clspendingleave_Tran
                dsList = objpending.GetEmployeeApproverListWithPriority(intEmpID, -1, Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(priority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("priority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("approverunkid").ToString() & ","
                        Next
                    End If
                End If
                objpending = Nothing
            Else

                Dim objExpAppr As New clsExpenseApprover_Master
                dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmpID, "List", Nothing, intRequestMstId)


                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(crpriority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("crapproverunkid").ToString() & ","
                        Next
                    End If
                End If
                objExpAppr = Nothing
            End If


            If mstrApproverID.Trim.Length > 0 Then
                mstrApproverID = mstrApproverID.Trim.Substring(0, mstrApproverID.Trim.Length - 1)
            Else
                mstrApproverID = "0"
            End If


            Dim objDataOperation As New clsDataOperation


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'strQ = "SELECT ISNULL(SUM(Amount),0.00) AS Amount FROM cmclaim_approval_tran WHERE crapproverunkid IN (" & mstrApproverID & ") AND isvoid = 0 AND crmasterunkid = @masterunkid AND statusunkid = 1 "

            strQ = "SELECT ISNULL(SUM(Amount),0.00) AS Amount FROM cmclaim_approval_tran "

            If mblnIncludeOnlyImprest Then
                strQ &= " JOIN cmexpense_master on cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isimprest =  1 "
            End If

            strQ &= " WHERE crapproverunkid IN (" & mstrApproverID & ") AND isvoid = 0 AND crmasterunkid = @masterunkid AND statusunkid = 1 "

            If xExpenseId > 0 Then
                strQ &= " AND cmclaim_approval_tran.expenseunkid = @expenseunkid"
            End If

            'Pinkal (11-Sep-2019) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRequestMstId)

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If xExpenseId > 0 Then objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            'Pinkal (11-Sep-2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecAmount = CDec(dsList.Tables(0).Rows(0)("Amount"))
            End If

            'Pinkal (24-Dec-2019) -- Start
            'Enhancement - Claim Retirement Enhancements for NMB.

            If mblnIncludeOnlyImprest AndAlso mdecAmount <= 0 Then

                strQ = " SELECT ISNULL(SUM(Amount),0.00) AS Amount FROM cmclaim_request_tran " & _
                          " JOIN cmexpense_master on cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid AND cmexpense_master.isimprest =  1 " & _
                          " WHERE  isvoid = 0 AND crmasterunkid = @masterunkid "

                If xExpenseId > 0 Then
                    strQ &= " AND cmclaim_request_tran.expenseunkid = @expenseunkid"
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRequestMstId)
                If xExpenseId > 0 Then objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    mdecAmount = CDec(dsList.Tables(0).Rows(0)("Amount"))
                End If
            End If
            'Pinkal (24-Dec-2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFinalApproverApprovedAmount; Module Name: " & mstrModuleName)
        End Try
        Return mdecAmount
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetClaimRequestMstIDFromLeaveForm(ByVal intLeaveFormId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Dim mintMstID As Integer = 0
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try

            'Pinkal (02-Dec-2015) -- Start
            'Enhancement - Solving Leave bug in Self Service.
            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If
            'Pinkal (02-Dec-2015) -- End

            objDataOperation.ClearParameters()
            strQ = " SELECT  ISNULL(crmasterunkid,0) AS crmasterunkid FROM cmclaim_request_master WHERE referenceunkid = @referenceunkid AND isvoid = 0  AND modulerefunkid = " & enModuleReference.Leave
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveFormId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintMstID = CInt(dsList.Tables(0).Rows(0)("crmasterunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimRequestMstIDFromLeaveForm; Module Name: " & mstrModuleName)
        Finally
            'Pinkal (02-Dec-2015) -- Start
            'Enhancement - Solving Leave bug in Self Service.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (02-Dec-2015) -- End
        End Try
        Return mintMstID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFinalApproverApprovedQtyFromSector(ByVal intEmpID As Integer, ByVal intExpenseTypeId As Integer, _
                                                                                       ByVal intSectorId As Integer, _
                                                                                       ByVal blnPaymentApprovalwithLeaveApproval As Boolean) As Decimal
        Dim mdecAmount As Decimal = 0
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try
            Dim mstrApproverID As String = ""

            If blnPaymentApprovalwithLeaveApproval Then
                Dim dtTab As DataTable = Nothing
                Dim objpending As New clspendingleave_Tran
                dsList = objpending.GetEmployeeApproverListWithPriority(intEmpID, -1, Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(priority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("priority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("approverunkid").ToString() & ","
                        Next
                    End If
                End If
                objpending = Nothing
            Else

                Dim objExpAppr As New clsExpenseApprover_Master
                dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmpID, "List", Nothing)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(crpriority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("crapproverunkid").ToString() & ","
                        Next
                    End If
                End If
                objExpAppr = Nothing
            End If


            If mstrApproverID.Trim.Length > 0 Then
                mstrApproverID = mstrApproverID.Trim.Substring(0, mstrApproverID.Trim.Length - 1)
            Else
                mstrApproverID = "0"
            End If

            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT ISNULL(SUM(quantity),0.00) AS quantity FROM cmclaim_approval_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
                      "  AND cmclaim_request_master.employeeunkid = @employeeunkid " & _
                      " WHERE cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverID & ") AND cmclaim_request_master.statusunkid = 1 AND secrouteunkid = @sectorunkid  AND cmclaim_approval_tran.isvoid = 0  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            objDataOperation.AddParameter("@sectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectorId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecAmount = CDec(dsList.Tables(0).Rows(0)("quantity"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFinalApproverApprovedQtyFromSector; Module Name: " & mstrModuleName)
        End Try
        Return mdecAmount
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetSectorLastAvailedDate(ByVal intEmpID As Integer, ByVal intExpenseTypeId As Integer, _
                                                                                       ByVal intSectorId As Integer, _
                                                                                       ByVal blnPaymentApprovalwithLeaveApproval As Boolean) As String
        Dim mdtDate As String = ""
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try
            Dim mstrApproverID As String = ""

            If blnPaymentApprovalwithLeaveApproval Then
                Dim dtTab As DataTable = Nothing
                Dim objpending As New clspendingleave_Tran
                dsList = objpending.GetEmployeeApproverListWithPriority(intEmpID, -1, Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(priority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("priority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("approverunkid").ToString() & ","
                        Next
                    End If
                End If
                objpending = Nothing
            Else

                Dim objExpAppr As New clsExpenseApprover_Master
                dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmpID, "List", Nothing)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(crpriority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("crapproverunkid").ToString() & ","
                        Next
                    End If
                End If
                objExpAppr = Nothing
            End If


            If mstrApproverID.Trim.Length > 0 Then
                mstrApproverID = mstrApproverID.Trim.Substring(0, mstrApproverID.Trim.Length - 1)
            Else
                mstrApproverID = "0"
            End If

            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT top 1 transactiondate FROM cmclaim_approval_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 " & _
                      " AND cmclaim_request_master.employeeunkid = @employeeunkid " & _
                      " WHERE cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverID & ") AND cmclaim_request_master.statusunkid = 1 AND secrouteunkid = @sectorunkid  AND cmclaim_approval_tran.isvoid = 0  " & _
                      " ORDER BY transactiondate DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            objDataOperation.AddParameter("@sectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectorId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdtDate = eZeeDate.convertDate(CDate(dsList.Tables(0).Rows(0)("transactiondate")).Date)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSectorLastAvailedDate; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return mdtDate
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverPendingExpenseFormCount(ByVal intApproverID As Integer, ByVal mstrEmpID As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintCount As Integer = 0
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'strQ = " SELECT COUNT(*) PendingFormCount FROM cmclaim_request_master  " & _
            '                      " JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.crapproverunkid  = " & intApproverID & _
            '                      " WHERE cmclaim_request_master.statusunkid = 2 AND cmclaim_request_master.employeeunkid IN (" & mstrEmpID & " ) AND cmclaim_request_master.isvoid = 0 "

            strQ = " SELECT COUNT(*) PendingFormCount FROM cmclaim_request_master  " & _
                      " JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.crapproverunkid  = " & intApproverID & _
                      " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 2 "

            If mstrEmpID.Trim.Length > 0 Then
                strQ &= " AND cmclaim_request_master.employeeunkid IN (" & mstrEmpID & " )  "
            End If
            'Shani(17-Aug-2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "PedingCount")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCount = CInt(dsList.Tables(0).Rows(0)("PendingFormCount"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverPendingExpenseFormCount; Module Name: " & mstrModuleName)
        End Try
        Return mintCount
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverPendingClaimForm(ByVal intApproverID As Integer, ByVal mstrEmpID As String, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrFormID As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()


            strQ = " SELECT ISNULL(STUFF( " & _
                      " (SELECT  DISTINCT ',' +  CAST(cmclaim_request_master.crmasterunkid AS NVARCHAR(max)) " & _
                      "   FROM cmclaim_request_master " & _
                      "   JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_approval_tran.crapproverunkid  = " & intApproverID & _
                      "  WHERE cmclaim_request_master.statusunkid in (2) AND cmclaim_request_master.employeeunkid IN (" & mstrEmpID & ") AND cmclaim_request_master.isvoid = 0  AND cmclaim_approval_tran.isvoid = 0 " & _
                      "  FOR XML PATH('')),1,1,''),'') AS CSV"

            dsList = objDataOperation.ExecQuery(strQ, "FormList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrFormID = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverPendingClaimForm; Module Name: " & mstrModuleName)
        End Try
        Return mstrFormID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetClaimRequestMstID(ByVal intModuleRefID As Integer, ByVal intExpenseTypeID As Integer, ByVal intReferenceID As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintClaimID As Integer = -1
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            strQ = " Select ISNULL(crmasterunkid,0) AS crmasterunkid From cmclaim_request_master WHERE isvoid = 0 AND modulerefunkid = @modulerefunkid AND referenceunkid = @referenceunkid AND expensetypeid = @expensetypeid "
            objDataOperation.AddParameter("@modulerefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefID)
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReferenceID)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseTypeID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintClaimID = CInt(dsList.Tables(0).Rows(0)("crmasterunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimRequestMstID; Module Name: " & mstrModuleName)
        End Try
        Return mintClaimID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendMailToEmployee(ByVal intEmpId As Integer, _
                                  ByVal mstrClaimFormNo As String, _
                                  ByVal intStatusID As Integer, _
                                  ByVal intCompanyId As Integer, _
                                  Optional ByVal strPath As String = "", _
                                  Optional ByVal strFileName As String = "", _
                                  Optional ByVal iLoginTypeId As Integer = 0, _
                                  Optional ByVal iLoginEmployeeId As Integer = 0, _
                                  Optional ByVal iUserId As Integer = 0, _
                                  Optional ByVal mstrRejectionRemark As String = "")
        'Sohail (30 Nov 2017) - [intCompanyId]
        Try
            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If intStatusID = 2 Then Exit Sub

            Dim mstrStatus As String = ""


            Select Case intStatusID
                Case 1
                    mstrStatus = Language.getMessage("clsMasterData", 110, "Approved")
                Case 3
                    mstrStatus = Language.getMessage("clsMasterData", 112, "Rejected")
                Case 6
                    mstrStatus = Language.getMessage("clsMasterData", 115, "Cancelled")
            End Select


            'Dim objMail As New clsSendMail
            'objMail._Subject = Language.getMessage(mstrModuleName, 6, "Claim Status Notification")
            Dim mstrSubject As String = Language.getMessage(mstrModuleName, 6, "Claim Status Notification")
            Dim strMessage As String = ""
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & getTitleCase(mstrEmpFirstName & "  " & mstrEmpSurName) & ", <BR><BR>"


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 7, " This is to inform you that your claim application no ") & mstrClaimFormNo & Language.getMessage(mstrModuleName, 8, " has been ") & mstrStatus & "."


            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            'strMessage &= " " & Language.getMessage(mstrModuleName, 7, " This is to inform you that your claim application no ") & mstrClaimFormNo & Language.getMessage(mstrModuleName, 8, " has been ") & mstrStatus & "."
            strMessage &= " " & Language.getMessage(mstrModuleName, 7, " This is to inform you that your claim application no ") & " <B>(" & mstrClaimFormNo & ")</B> " & Language.getMessage(mstrModuleName, 8, " has been ") & mstrStatus & "."
            'Pinkal (01-Apr-2019) -- End

            'Gajanan [27-Mar-2019] -- End

            If intStatusID <> 6 Then
                If intStatusID = 3 Then
                    strMessage &= "<BR></BR>"

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'strMessage &= " " & Language.getMessage(mstrModuleName, 9, "Remarks/Comments:") & " " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                    strMessage &= " " & Language.getMessage(mstrModuleName, 14, "with below") & " " & Language.getMessage(mstrModuleName, 9, "Remarks/Comments:") & "<BR></BR> " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                    'Pinkal (01-Apr-2019) -- End
                End If
            Else
                strMessage &= "<BR></BR>"

                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.
                'strMessage &= " " & Language.getMessage(mstrModuleName, 9, "Remarks/Comments:") & " " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                strMessage &= " " & Language.getMessage(mstrModuleName, 14, "with below") & " " & Language.getMessage(mstrModuleName, 9, "Remarks/Comments:") & "<BR></BR> " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                'Pinkal (01-Apr-2019) -- End
            End If

            If intStatusID = 1 Then
                strMessage &= Language.getMessage(mstrModuleName, 10, "Kindly take note of it.")
            Else
                strMessage &= "."
            End If

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            'Pinkal (06-Sep-2021)-- Start
            'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.

            'objMail._Message = strMessage
            'objMail._ToEmail = mstrEmpMail

            'If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            'If mstrWebFormName.Trim.Length > 0 Then
            '    objMail._Form_Name = mstrWebFormName
            '    objMail._WebClientIP = mstrWebClientIP
            '    objMail._WebHostName = mstrWebHostName
            'End If
            'objMail._LogEmployeeUnkid = iLoginEmployeeId
            'objMail._OperationModeId = iLoginTypeId
            'objMail._UserUnkid = iUserId
            'objMail._SenderAddress = mstrEmpFirstName & " " & mstrEmpSurName
            'objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT

            'If intStatusID = 1 AndAlso strPath <> "" Then
            '    objMail._AttachedFiles = strFileName
            '    'Sohail (30 Nov 2017) -- Start
            '    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '    'objMail.SendMail(True, strPath)
            '    objMail.SendMail(intCompanyId, True, strPath)
            '    'Sohail (30 Nov 2017) -- End
            'Else
            '    'Sohail (30 Nov 2017) -- Start
            '    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '    'objMail.SendMail()
            '    objMail.SendMail(intCompanyId)
            '    'Sohail (30 Nov 2017) -- End
            'End If

            Dim objEmailColl As New clsEmailCollection(mstrEmpMail, mstrSubject, strMessage, mstrWebFormName, _
                                                                  iLoginEmployeeId, mstrWebClientIP, mstrWebHostName, _
                                                                  iUserId, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT, _
                                                                  mstrEmpFirstName & " " & mstrEmpSurName)

            gobjEmailList.Add(objEmailColl)

            'Pinkal (06-Sep-2021)-- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToEmployee; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveEncashmentForEmployee(ByVal intYearID As Integer, ByVal intEmployeeID As Integer, Optional ByVal intLeaveTypeID As Integer = 0 _
                                                                                , Optional ByVal intExpenseID As Integer = 0 _
                                                                                , Optional ByVal mdtStartDate As Date = Nothing _
                                                                                , Optional ByVal mdtEndDate As Date = Nothing _
                                                                                , Optional ByVal mdtdbStartDate As DateTime = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                                                , Optional ByVal strDatabaseName As String = "") As Decimal
        'Sohail (15 Jan 2019) - [strDatabaseName]
        'Sohail (03 May 2018) - [xDataOp]
        Dim mdcLeaveEncashment As Decimal = 0
        Dim mdecPreLeaveEncashment As Decimal = 0
        Dim dsList As DataSet = Nothing
        Dim mstrClaimFormIDs As String = ""
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sohail (15 Jan 2019) -- Start
        'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
        Dim strDB As String = ""
        If strDatabaseName.Trim <> "" Then
            strDB = strDatabaseName & ".."
        End If
        'Sohail (15 Jan 2019) -- End

        'Pinkal (01-Mar-2016) -- Start
        'Enhancement - Implementing External Approver in Claim Request & Leave Module.
        Dim mblnIsPaymentApprovalWithLeaveApproval As Boolean = False
        'Pinkal (01-Mar-2016) -- End


        Try

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            'Sohail (03 May 2018) -- End
            objDataOperation.ClearParameters()


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            Dim mintCompany As Integer = 0
            Dim mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year

            strQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & intYearID & "  "
            Dim dsCompany As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                mintCompany = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
            End If

            If mintCompany > 0 Then
                strQ = "SELECT ISNULL(key_value,1) AS key_value  from hrmsConfiguration..cfconfiguration WHERE key_name = 'LeaveBalanceSetting' and companyunkid = " & mintCompany
                Dim dsLeaveBalanceSetting As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsLeaveBalanceSetting IsNot Nothing AndAlso dsLeaveBalanceSetting.Tables(0).Rows.Count > 0 Then
                    mintLeaveBalanceSetting = CInt(dsLeaveBalanceSetting.Tables(0).Rows(0)("key_value"))
                End If


                strQ = "SELECT ISNULL(key_value,1) AS key_value  from hrmsConfiguration..cfconfiguration WHERE key_name = 'ClaimRequest_PaymentApprovalwithLeaveApproval' and companyunkid = " & mintCompany
                Dim dsPaymentApproval As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsPaymentApproval IsNot Nothing AndAlso dsPaymentApproval.Tables(0).Rows.Count > 0 Then
                    mblnIsPaymentApprovalWithLeaveApproval = CBool(dsPaymentApproval.Tables(0).Rows(0)("key_value"))
                End If

                'Pinkal (01-Mar-2016) -- End


                If mdtStartDate <> Nothing AndAlso mdtStartDate < mdtdbStartDate.Date Then
                    Dim mstrPreviousDBName As String = ""
                    Dim mdtFYStartDate As Date = Nothing
                    Dim mdtFYEndDate As Date = Nothing


                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                        strQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompany & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                        Dim dsDBName As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
                            mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
                            mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
                            mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))


                            'Pinkal (01-Mar-2016) -- Start
                            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                            strQ = "SELECT  SUM(quantity) AS Encashment  " & _
                                     ",cmclaim_approval_tran.expenseunkid  " & _
                                     ",cmclaim_request_master.employeeunkid  " & _
                                         ",cmexpense_master.leavetypeunkid  "

                            If mblnIsPaymentApprovalWithLeaveApproval Then
                                strQ &= ", lvapproverlevel_master.priority AS crpriority "
                            Else
                                strQ &= ", cmapproverlevel_master.crpriority "
                            End If

                            strQ &= " FROM  " & mstrPreviousDBName & "..cmclaim_request_master " & _
                                 " JOIN " & mstrPreviousDBName & "..cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                 " AND cmclaim_approval_tran.statusunkid = 1 and cmclaim_approval_tran.approvaldate is NOT NULL " & _
                                     " LEFT JOIN " & mstrPreviousDBName & "..cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid "

                            'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.[AND cmclaim_approval_tran.statusunkid = 1 and cmclaim_approval_tran.approvaldate is NOT NULL]

                            If mblnIsPaymentApprovalWithLeaveApproval Then

                                'Pinkal (25-Oct-2018) -- 'Bug -Inconsistencies in the encashed leave days.[Issue No 2924] AND lvleaveapprover_master.isvoid = 0
                                strQ &= " LEFT JOIN " & mstrPreviousDBName & "..lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid  " & _
                                                                      " LEFT JOIN " & mstrPreviousDBName & "..lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid  " & _
                                                                      " LEFT JOIN ( " & _
                                                                      "               SELECT lvleaveapprover_master.approverunkid AS crapproverunkid " & _
                                                                                ",lvleaveapprover_tran.employeeunkid  " & _
                                                                                ",lvapproverlevel_master.priority AS crpriority  " & _
                                                                                ",ROW_NUMBER() OVER ( PARTITION BY lvleaveapprover_tran.employeeunkid ORDER BY lvapproverlevel_master.priority DESC ) AS rno " & _
                                                                                " FROM  " & mstrPreviousDBName & "..lvleaveapprover_master " & _
                                                                                " LEFT JOIN " & mstrPreviousDBName & "..lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid  " & _
                                                                                " LEFT JOIN " & mstrPreviousDBName & "..lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                                                                                " WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isswap = 0 "



                                'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports. ALSO REPLACE JOIN TO LEFT JOIN IN THIS.
                                '["LEFT JOIN " & mstrPreviousDBName & "..lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid = 0 " & _]

                                If intEmployeeID > 0 Then
                                    strQ &= "        AND lvleaveapprover_tran.employeeunkid = " & intEmployeeID
                                End If

                                strQ &= "           GROUP BY lvleaveapprover_master.approverunkid " & _
                                            "           ,lvleaveapprover_tran.employeeunkid  " & _
                                            "           ,lvapproverlevel_master.priority " & _
                                            "  ) AS B ON b.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND b.employeeunkid = cmclaim_request_master.employeeunkid AND B.rno = 1 "

                            Else

                                'Pinkal (25-Oct-2018) -- 'Bug -Inconsistencies in the encashed leave days.[Issue No 2924] AND cmexpapprover_master.isvoid = 0

                                strQ &= " LEFT JOIN " & mstrPreviousDBName & "..cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid  " & _
                                 " LEFT JOIN " & mstrPreviousDBName & "..cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmexpense_master.expensetypeid = cmclaim_request_master.expensetypeid " & _
                                 " JOIN ( " & _
                                 "               SELECT cmexpapprover_master.crapproverunkid " & _
                                                 ",cmexpapprover_tran.employeeunkid  " & _
                                                 ",cmapproverlevel_master.crpriority  " & _
                                                 ",ROW_NUMBER() OVER ( PARTITION BY cmexpapprover_tran.employeeunkid ORDER BY cmapproverlevel_master.crpriority DESC ) AS rno " & _
                                                 " FROM  " & mstrPreviousDBName & "..cmexpapprover_master " & _
                                                 " LEFT JOIN " & mstrPreviousDBName & "..cmexpapprover_tran ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid " & _
                                                 " LEFT JOIN " & mstrPreviousDBName & "..cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                                 " WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 AND cmapproverlevel_master.expensetypeid =  " & enExpenseType.EXP_LEAVE


                                'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports. ALSO REPLACE JOIN TO LEFT JOIN IN THIS.
                                '[" LEFT JOIN " & mstrPreviousDBName & "..cmexpapprover_tran ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid AND cmexpapprover_tran.isvoid = 0 " & _]

                                If intEmployeeID > 0 Then
                                    strQ &= "        AND cmexpapprover_tran.employeeunkid = " & intEmployeeID
                                End If

                                strQ &= "           GROUP BY cmexpapprover_master.crapproverunkid " & _
                                            "           ,cmexpapprover_tran.employeeunkid  " & _
                                            "           ,cmapproverlevel_master.crpriority " & _
                                                    "  ) AS B ON b.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND b.employeeunkid = cmclaim_request_master.employeeunkid AND B.rno = 1 "

                            End If




                            strQ &= " WHERE  cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmexpense_master.isleaveencashment = 1 " & _
                                    " AND cmclaim_request_master.expensetypeid =  " & enExpenseType.EXP_LEAVE

                            If intEmployeeID > 0 Then
                                strQ &= " AND cmclaim_request_master.employeeunkid = " & intEmployeeID
                            End If

                            If intLeaveTypeID > 0 Then
                                strQ &= " AND cmexpense_master.leavetypeunkid = " & intLeaveTypeID
                            End If

                            If intExpenseID > 0 Then
                                strQ &= " AND cmclaim_approval_tran.expenseunkid = " & intExpenseID
                            End If

                            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                                strQ &= " AND CONVERT(char(8),cmclaim_request_master.transactiondate,112) BETWEEN @startdate AND @enddate"
                            End If

                            If mblnIsPaymentApprovalWithLeaveApproval Then
                                strQ &= " GROUP BY cmclaim_request_master.employeeunkid  " & _
                                            ", lvapproverlevel_master.priority  " & _
                                            ",  cmclaim_approval_tran.expenseunkid  " & _
                                            ", cmexpense_master.leavetypeunkid "

                                'Pinkal (10-Oct-2018) -- Start
                                'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                                strQ &= " ORDER by lvapproverlevel_master.priority DESC "
                                'Pinkal (10-Oct-2018) -- End
                            Else
                                strQ &= " GROUP BY cmclaim_request_master.employeeunkid  " & _
                                            ", cmapproverlevel_master.crpriority  " & _
                                            ",  cmclaim_approval_tran.expenseunkid  " & _
                                            ", cmexpense_master.leavetypeunkid "

                                'Pinkal (10-Oct-2018) -- Start
                                'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                                strQ &= " ORDER by  cmapproverlevel_master.crpriority DESC "
                                'Pinkal (10-Oct-2018) -- End
                            End If

                            objDataOperation.ClearParameters()

                            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
                            End If

                            dsList = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                mdecPreLeaveEncashment = CDec(dsList.Tables("List").Rows(0)("Encashment"))
                            End If



                            objDataOperation.ClearParameters()

                            strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), cmclaim_request_master.crmasterunkid ) " & _
                                    " FROM  " & mstrPreviousDBName & "..cmclaim_request_master " & _
                                    " JOIN " & mstrPreviousDBName & "..cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                                    " AND cmclaim_approval_tran.statusunkid = 1 and cmclaim_approval_tran.approvaldate is NOT NULL " & _
                                        " LEFT JOIN " & mstrPreviousDBName & "..cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid "

                            'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.[AND cmclaim_approval_tran.statusunkid = 1 and cmclaim_approval_tran.approvaldate is NOT NULL]

                            If mblnIsPaymentApprovalWithLeaveApproval Then

                                'Pinkal (25-Oct-2018) -- 'Bug -Inconsistencies in the encashed leave days.[Issue No 2924] AND lvleaveapprover_master.isvoid = 0

                                strQ &= " LEFT JOIN " & mstrPreviousDBName & "..lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid " & _
                                       " LEFT JOIN " & mstrPreviousDBName & "..lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                                       " LEFT JOIN ( " & _
                                       "               SELECT lvleaveapprover_master.approverunkid AS crapproverunkid " & _
                                                       ",lvleaveapprover_tran.employeeunkid  " & _
                                                       ",lvapproverlevel_master.priority AS crpriority  " & _
                                                       ",ROW_NUMBER() OVER ( PARTITION BY lvleaveapprover_tran.employeeunkid ORDER BY lvapproverlevel_master.priority DESC ) AS rno " & _
                                                       " FROM  " & mstrPreviousDBName & "..lvleaveapprover_master " & _
                                                       " LEFT JOIN " & mstrPreviousDBName & "..lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid  " & _
                                                       " LEFT JOIN " & mstrPreviousDBName & "..lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                                                       " WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isswap = 0 "

                                'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports. ALSO REPLACE JOIN TO LEFT JOIN IN THIS.
                                '[" LEFT JOIN " & mstrPreviousDBName & "..lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid = 0 " & _]

                                If intEmployeeID > 0 Then
                                    strQ &= "        AND lvleaveapprover_tran.employeeunkid = " & intEmployeeID
                                End If

                                strQ &= "           GROUP BY lvleaveapprover_master.approverunkid " & _
                                            "           ,lvleaveapprover_tran.employeeunkid  " & _
                                            "           ,lvapproverlevel_master.priority " & _
                                            "  ) AS B ON b.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND b.employeeunkid = cmclaim_request_master.employeeunkid AND B.rno = 1 " & _
                                            " WHERE  cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmexpense_master.isleaveencashment = 1 " & _
                                            " AND cmclaim_request_master.expensetypeid =  " & enExpenseType.EXP_LEAVE

                            Else

                                'Pinkal (25-Oct-2018) -- 'Bug -Inconsistencies in the encashed leave days.[Issue No 2924] AND cmexpapprover_master.isvoid = 0

                                strQ &= " LEFT JOIN " & mstrPreviousDBName & "..cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid  " & _
                                " LEFT JOIN " & mstrPreviousDBName & "..cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmexpense_master.expensetypeid = cmclaim_request_master.expensetypeid " & _
                                            " LEFT JOIN ( " & _
                                "               SELECT cmexpapprover_master.crapproverunkid " & _
                                                ",cmexpapprover_tran.employeeunkid  " & _
                                                ",cmapproverlevel_master.crpriority  " & _
                                                ",ROW_NUMBER() OVER ( PARTITION BY cmexpapprover_tran.employeeunkid ORDER BY cmapproverlevel_master.crpriority DESC ) AS rno " & _
                                                " FROM  " & mstrPreviousDBName & "..cmexpapprover_master " & _
                                                            " LEFT JOIN " & mstrPreviousDBName & "..cmexpapprover_tran ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid  " & _
                                                " LEFT JOIN " & mstrPreviousDBName & "..cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                                " WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 AND cmapproverlevel_master.expensetypeid =  " & enExpenseType.EXP_LEAVE

                                'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports. ALSO REPLACE JOIN TO LEFT JOIN IN THIS.
                                '[" LEFT JOIN " & mstrPreviousDBName & "..cmexpapprover_tran ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid AND cmexpapprover_tran.isvoid = 0 " & _ ]

                                If intEmployeeID > 0 Then
                                    strQ &= "        AND cmexpapprover_tran.employeeunkid = " & intEmployeeID
                                End If

                                strQ &= "           GROUP BY cmexpapprover_master.crapproverunkid " & _
                                            "           ,cmexpapprover_tran.employeeunkid  " & _
                                            "           ,cmapproverlevel_master.crpriority " & _
                                            "  ) AS B ON b.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND b.employeeunkid = cmclaim_request_master.employeeunkid AND B.rno = 1 " & _
                                            " WHERE  cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmexpense_master.isleaveencashment = 1 " & _
                                            " AND cmclaim_request_master.expensetypeid =  " & enExpenseType.EXP_LEAVE

                            End If

                            If intEmployeeID > 0 Then
                                strQ &= " AND cmclaim_request_master.employeeunkid = " & intEmployeeID
                            End If

                            If intLeaveTypeID > 0 Then
                                strQ &= " AND cmexpense_master.leavetypeunkid = " & intLeaveTypeID
                            End If

                            If intExpenseID > 0 Then
                                strQ &= " AND cmclaim_approval_tran.expenseunkid = " & intExpenseID
                            End If

                            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                                strQ &= " AND CONVERT(char(8),cmclaim_request_master.transactiondate,112) BETWEEN @startdate AND @enddate"
                            End If

                            strQ &= " FOR XML PATH('')),1,1,''),'') AS formunkid "


                            'Pinkal (01-Mar-2016) -- End

                            If mdtStartDate <> Nothing Then
                                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                            End If

                            If mdtEndDate <> Nothing Then
                                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
                            End If

                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
                            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID)
                            dsList = objDataOperation.ExecQuery(strQ, "List")


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                mstrClaimFormIDs = dsList.Tables("List").Rows(0)("formunkid").ToString()
                            End If

                        End If  'dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0

                    End If 'mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC

                End If 'mintCompany > 0



            End If

            strQ = "SELECT  SUM(quantity) AS Encashment  " & _
                    ",cmclaim_approval_tran.expenseunkid  " & _
                    ",cmclaim_request_master.employeeunkid  " & _
                    ",cmexpense_master.leavetypeunkid  "

            If mblnIsPaymentApprovalWithLeaveApproval Then
                strQ &= ", lvapproverlevel_master.priority AS crpriority "
            Else
                strQ &= ", cmapproverlevel_master.crpriority "
            End If

            strQ &= " FROM  " & strDB & "cmclaim_request_master " & _
                    " JOIN " & strDB & "cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_approval_tran.isvoid = 0  AND cmclaim_approval_tran.statusunkid = 1 and cmclaim_approval_tran.approvaldate is NOT NULL" & _
                    " LEFT JOIN " & strDB & "cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid "
            'Sohail (15 Jan 2019) - [strDB]
            'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.[AND cmclaim_approval_tran.statusunkid = 1 and cmclaim_approval_tran.approvaldate is NOT NULL]

            If mblnIsPaymentApprovalWithLeaveApproval Then

                'Pinkal (25-Oct-2018) -- 'Bug -Inconsistencies in the encashed leave days.[Issue No 2924] AND lvleaveapprover_master.isvoid = 0

                strQ &= " LEFT JOIN " & strDB & "lvleaveapprover_master ON lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid  " & _
                          " LEFT JOIN " & strDB & "lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid  " & _
                          " LEFT JOIN ( " & _
                          "               SELECT lvleaveapprover_master.approverunkid AS crapproverunkid " & _
                                          ",lvleaveapprover_tran.employeeunkid  " & _
                                          ",lvapproverlevel_master.priority AS crpriority  " & _
                                          ",ROW_NUMBER() OVER ( PARTITION BY lvleaveapprover_tran.employeeunkid ORDER BY lvapproverlevel_master.priority DESC ) AS rno " & _
                                          " FROM  " & strDB & "lvleaveapprover_master " & _
                                          " LEFT JOIN " & strDB & "lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid  " & _
                                          " LEFT JOIN " & strDB & "lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvapproverlevel_master.levelunkid " & _
                                          " WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isswap = 0 "
                'Sohail (15 Jan 2019) - [strDB]

                'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports. ALSO REPLACE JOIN TO LEFT JOIN IN THIS.
                '[" LEFT JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid = 0 " & _]


                If intEmployeeID > 0 Then
                    strQ &= "        AND lvleaveapprover_tran.employeeunkid = " & intEmployeeID
                End If

                strQ &= "           GROUP BY lvleaveapprover_master.approverunkid " & _
                            "           ,lvleaveapprover_tran.employeeunkid  " & _
                            "           ,lvapproverlevel_master.priority " & _
                            "  ) AS B ON b.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND b.employeeunkid = cmclaim_request_master.employeeunkid AND B.rno = 1 "
            Else

                'Pinkal (25-Oct-2018) -- 'Bug -Inconsistencies in the encashed leave days.[Issue No 2924] AND cmexpapprover_master.isvoid = 0

                strQ &= " LEFT JOIN " & strDB & "cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid  " & _
                    " LEFT JOIN " & strDB & "cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmexpense_master.expensetypeid = cmclaim_request_master.expensetypeid " & _
                    " LEFT JOIN ( " & _
                    "               SELECT cmexpapprover_master.crapproverunkid " & _
                                    ",cmexpapprover_tran.employeeunkid  " & _
                                    ",cmapproverlevel_master.crpriority  " & _
                                    ",ROW_NUMBER() OVER ( PARTITION BY cmexpapprover_tran.employeeunkid ORDER BY cmapproverlevel_master.crpriority DESC ) AS rno " & _
                                    " FROM  " & strDB & "cmexpapprover_master " & _
                                    " LEFT JOIN " & strDB & "cmexpapprover_tran ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid  " & _
                                    " LEFT JOIN " & strDB & "cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                    " WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 AND cmapproverlevel_master.expensetypeid =  " & enExpenseType.EXP_LEAVE
                'Sohail (15 Jan 2019) - [strDB]
                'Pinkal (10-Oct-2018) -- 'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports. ALSO REPLACE JOIN TO LEFT JOIN IN THIS.
                '[" LEFT JOIN cmexpapprover_tran ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid AND cmexpapprover_tran.isvoid = 0 " & ]

                If intEmployeeID > 0 Then
                    strQ &= "        AND cmexpapprover_tran.employeeunkid = " & intEmployeeID
                End If

                strQ &= "           GROUP BY cmexpapprover_master.crapproverunkid " & _
                            "           ,cmexpapprover_tran.employeeunkid  " & _
                            "           ,cmapproverlevel_master.crpriority " & _
                                "  ) AS B ON b.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND b.employeeunkid = cmclaim_request_master.employeeunkid AND B.rno = 1 "
            End If

            strQ &= " WHERE  cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmexpense_master.isleaveencashment = 1 " & _
                        " AND cmclaim_request_master.expensetypeid =  " & enExpenseType.EXP_LEAVE

            If intEmployeeID > 0 Then
                strQ &= " AND cmclaim_request_master.employeeunkid = " & intEmployeeID
            End If

            If intLeaveTypeID > 0 Then
                strQ &= " AND cmexpense_master.leavetypeunkid = " & intLeaveTypeID
            End If

            If intExpenseID > 0 Then
                strQ &= " AND cmclaim_approval_tran.expenseunkid = " & intExpenseID
            End If

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                strQ &= " AND CONVERT(char(8),cmclaim_request_master.transactiondate,112) BETWEEN @startdate AND @enddate"
            End If

            If mstrClaimFormIDs.Trim.Length > 0 Then
                strQ &= " AND cmclaim_request_master.crmasterunkid NOT in (" & mstrClaimFormIDs & ")"
            End If


            If mblnIsPaymentApprovalWithLeaveApproval Then

                strQ &= " GROUP BY cmclaim_request_master.employeeunkid  " & _
                        ", lvapproverlevel_master.priority  " & _
                        ",  cmclaim_approval_tran.expenseunkid  " & _
                        ", cmexpense_master.leavetypeunkid "

                'Pinkal (10-Oct-2018) -- Start
                'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                strQ &= " ORDER by lvapproverlevel_master.priority DESC "
                'Pinkal (10-Oct-2018) -- End

            Else

                strQ &= " GROUP BY cmclaim_request_master.employeeunkid  " & _
                            ", cmapproverlevel_master.crpriority  " & _
                            ",  cmclaim_approval_tran.expenseunkid  " & _
                            ", cmexpense_master.leavetypeunkid "

                'Pinkal (10-Oct-2018) -- Start
                'Bug - Issue No : 2622 Encashed leave days aren't showing on all leave balance screens and reports.
                strQ &= " ORDER by cmapproverlevel_master.crpriority DESC "
                'Pinkal (10-Oct-2018) -- End

            End If



            objDataOperation.ClearParameters()

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdcLeaveEncashment = CDec(dsList.Tables(0).Rows(0)("Encashment"))
            End If

            mdcLeaveEncashment += mdecPreLeaveEncashment

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLeaveEncashmentForEmployee; Module Name: " & mstrModuleName)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return mdcLeaveEncashment
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    'Public Function IsValid_Expense(ByVal iEmpId As Integer, ByVal iExpenseId As Integer) As String
    Public Function IsValid_Expense(ByVal iEmpId As Integer, ByVal iExpenseId As Integer, ByVal iYearId As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                , ByVal mdtClaimAppDate As Date, ByVal xLeaveBalanceSetting As enLeaveBalanceSetting _
                                                , Optional ByVal xClaimRequestMasterId As Integer = 0 _
                                                , Optional ByVal mblnFromClaimApplication As Boolean = True) As String


        'Pinkal (22-Jan-2020) -- Enhancements -  Working on OT Requisistion Reports for NMB.[Optional ByVal mblnFromClaimApplication As Boolean = True]

        'Pinkal (07-Mar-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[Optional ByVal xClaimRequestMasterId As Integer = 0]

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim sMessage As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try


            'If xFrequecnyCycle = enExpenseFrequencyCycle.AppointmentDate AndAlso xEligibityAfter > 0 Then
            '    Dim objEmp As New clsEmployee_Master
            '    objEmp._Employeeunkid(mdtEmployeeAsonDate) = iEmpId
            '    If (DateDiff(DateInterval.Year, objEmp._Appointeddate, mdtClaimAppDate) * 12) <= xEligibityAfter Then
            '        sMessage = Language.getMessage(mstrModuleName, 3, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense  does not match with eligibility months set on selected expense. Eligible Months set for the selected expense is") & " " & xEligibityAfter.ToString())
            '        objEmp = Nothing
            '        Return mstrMessage
            '    End If
            '    objEmp = Nothing
            'End If


            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            If mblnFromClaimApplication Then

            Dim objExpBalance As New clsEmployeeExpenseBalance
            Dim dsBalanceList As DataSet = objExpBalance.Get_Balance(iEmpId, iExpenseId, iYearId, mdtEmployeeAsonDate, IIf(xLeaveBalanceSetting = enLeaveBalanceSetting.ELC, True, False), IIf(xLeaveBalanceSetting = enLeaveBalanceSetting.ELC, True, False), False)
            objExpBalance = Nothing

            If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
                Dim xEligibityAfter As Integer = CInt(dsBalanceList.Tables(0).Rows(0)("eligibilityafter"))
                Dim xFrequecnyCycle As enExpenseFrequencyCycle = CType((dsBalanceList.Tables(0).Rows(0)("freq_cycleunkid")), enExpenseFrequencyCycle)
                If xFrequecnyCycle = enExpenseFrequencyCycle.AppointmentDate AndAlso xEligibityAfter > 0 Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(mdtEmployeeAsonDate) = iEmpId

                    'Pinkal (15-Apr-2019) -- Start
                    'Enhancement - Working on Leave & CR Changes for NMB.
                    'If (DateDiff(DateInterval.Year, objEmp._Appointeddate.Date, mdtClaimAppDate.Date) * 12) <= xEligibityAfter Then
                    If (DateDiff(DateInterval.Year, objEmp._Appointeddate.Date, mdtClaimAppDate.Date) * 12) < xEligibityAfter Then
                        'Pinkal (15-Apr-2019) -- End
                        sMessage = Language.getMessage(mstrModuleName, 11, "Sorry,You cannot apply this expense.Reason : Eligibility to apply expense does not match with eligibility months set on selected expense. Eligiblilty set for the selected expense is") & " " & xEligibityAfter.ToString() & " " & Language.getMessage(mstrModuleName, 12, "months.")
                        objEmp = Nothing
                        Return sMessage
                    ElseIf mdtClaimAppDate.Year <> CInt(dsBalanceList.Tables(0).Rows(0)("eligible_year")) Then
                        sMessage = Language.getMessage(mstrModuleName, 11, "Sorry,You cannot apply this expense.Reason : Eligibility to apply expense does not match with eligibility months set on selected expense. Eligiblilty set for the selected expense is") & " " & xEligibityAfter.ToString() & " " & Language.getMessage(mstrModuleName, 12, "months.")
                        Return sMessage
                    End If
                    objEmp = Nothing
                End If
                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim xStatusId As Integer = GetEmployeeLastExpenseStatus(iEmpId, iExpenseId, 0, xClaimRequestMasterId)
                If xStatusId = 2 Then
                    sMessage = Language.getMessage(mstrModuleName, 13, "Sorry,you cannot apply for this expense now as your previous claim application for this expense has not been approved yet.")
                    Return sMessage
                End If
                'Pinkal (07-Mar-2019) -- End
            End If

            End If

            'Pinkal (15-Jan-2020) -- End


            StrQ = " SELECT TOP 1 occurrence AS iOcc,remaining_occurrence AS rOcc " & _
                       " FROM cmexpbalance_tran " & _
                       " WHERE cmexpbalance_tran.isvoid = 0 AND cmexpbalance_tran.employeeunkid = '" & iEmpId & "' AND cmexpbalance_tran.expenseunkid = '" & iExpenseId & "' AND occurrence > 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0).Item("rOcc")) <= 0 Then
                    sMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsList.Tables(0).Rows(0).Item("iOcc")) & _
                               Language.getMessage(mstrModuleName, 4, " ] time(s).")
                    GoTo lFound
                End If
            End If

lFound:     Return sMessage

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValid_Expense; Module Name: " & mstrModuleName)
            Return ex.Message
        Finally

        End Try
    End Function

    'Pinkal (26-Feb-2019) -- End


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApprovedClaimApprovedAmount(ByVal mblnPaymentApprovalwithLeaveApproval As Boolean, ByVal intEmpID As Integer _
                                                                           , ByVal intExpenseTypeId As Integer, ByVal intExpenseId As Integer _
                                                                           , ByVal isAccrueExpense As Boolean) As Decimal
        Dim mdecAmount As Decimal = 0
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try
            Dim mstrApproverID As String = ""

            If mblnPaymentApprovalwithLeaveApproval AndAlso intExpenseTypeId = enExpenseType.EXP_LEAVE Then
                Dim dtTab As DataTable = Nothing
                Dim objpending As New clspendingleave_Tran
                dsList = objpending.GetEmployeeApproverListWithPriority(intEmpID, -1, Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(priority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("priority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("approverunkid").ToString() & ","
                        Next
                    End If
                End If
                objpending = Nothing
            Else

                Dim objExpAppr As New clsExpenseApprover_Master
                dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmpID, "List", Nothing)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(crpriority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("crapproverunkid").ToString() & ","
                        Next
                    End If
                End If
                objExpAppr = Nothing
            End If


            If mstrApproverID.Trim.Length > 0 Then
                mstrApproverID = mstrApproverID.Trim.Substring(0, mstrApproverID.Trim.Length - 1)
            Else
                mstrApproverID = "0"
            End If

            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT TOP 1 ISNULL(Amount,0.00) AS Amount " & _
                      " FROM cmclaim_approval_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid  " & _
                      " WHERE cmclaim_request_master.employeeunkid = @employeeunkid AND cmclaim_approval_tran.expenseunkid = @expenseunkid  " & _
                      " AND cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverID & ") AND cmclaim_request_master.isvoid = 0 AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 " & _
                      " ORDER BY cmclaim_approval_tran.crmasterunkid DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecAmount = CDec(dsList.Tables(0).Rows(0)("Amount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApprovedClaimApprovedAmount; Module Name: " & mstrModuleName)
        End Try
        Return mdecAmount
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetClaimNoForTran(ByVal intTranUnkId As Integer, _
                                      ByVal blnFalg As Boolean, _
                                      Optional ByVal strList As String = "List") As DataSet
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try

            Dim objDataOperation As New clsDataOperation

            strQ = ""

	    'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

            'If blnFalg Then
            '    strQ = "SELECT DISTINCT " & _
            '           "     '" & Language.getMessage(mstrModuleName, 51, "Select") & "' AS claimrequestno" & _
            '           "    ,0 AS crmasterunkid " & _
            '           "    ,0 AS crtranunkid " & _
            '           "UNION ALL "
            'End If


            'If intTranUnkId > 0 Then
            '    strQ &= " SELECT DISTINCT " & _
            '            "     cmclaim_request_master.claimrequestno " & _
            '            "    ,cmclaim_request_tran.crtranunkid AS crtranunkid " & _
            '            " FROM cmclaim_request_tran " & _
            '            "    JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid " & _
            '            " where cmclaim_request_master.isvoid = 0 AND cmclaim_request_tran.isvoid = 0 AND cmclaim_request_tran.crtranunkid = '" & intTranUnkId & "' "

            'Else
            '    strQ &= " SELECT DISTINCT " & _
            '            "     cmclaim_request_master.claimrequestno " & _
            '            "    ,cmclaim_request_master.crmasterunkid AS crtranunkid " & _
            '            " FROM cmclaim_request_master " & _
            '            " where cmclaim_request_master.isvoid = 0 "
            'End If

            If blnFalg Then
                strQ = "SELECT DISTINCT " & _
                       "     '" & Language.getMessage(mstrModuleName, 51, "Select") & "' AS claimrequestno" & _
                       "    ,0 AS crmasterunkid " & _
                       "    ,0 AS crtranunkid " & _
                       "UNION ALL "
            End If

                strQ &= " SELECT DISTINCT " & _
                        "     cmclaim_request_master.claimrequestno " & _
                        "    ,cmclaim_request_tran.crmasterunkid  " & _
                        "    ,cmclaim_request_tran.crtranunkid  " & _
                        " FROM cmclaim_request_tran " & _
                        "    JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid " & _
                        " where cmclaim_request_master.isvoid = 0 AND cmclaim_request_tran.isvoid = 0 AND cmclaim_request_tran.crtranunkid = @crtranunkid "

			objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranUnkId)

            'Pinkal (28-Nov-2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimNoForTran; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function


    'Pinkal (30-Apr-2018) - Start
    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetEmployeeLastExpenseStatus(ByVal xEmployeeId As Integer, ByVal xExpenseId As Integer, ByVal xSectorRouteId As Integer, Optional ByVal xClaimFormId As Integer = 0) As Integer
        Dim mintStatusId As Integer = -1
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.ClearParameters()



            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'StrQ = " SELECT TOP 1 ISNULL(cmclaim_request_master.statusunkid, 0) AS StatusId " & _
            '          ", cmclaim_request_master.claimrequestno " & _
            '          ", cmclaim_request_master.employeeunkid " & _
            '          " FROM cmclaim_request_master " & _
            '          " JOIN cmclaim_approval_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
            '          " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.statusunkid = 2 " & _
            '          " AND cmclaim_approval_tran.expenseunkid = @expenseunkid  AND cmclaim_request_master.employeeunkid = @employeeunkid "


            'If xSectorRouteId > 0 Then
            '    StrQ &= " AND cmclaim_approval_tran.secrouteunkid = @secrouteunkid "
            '    objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSectorRouteId)
            'End If

            StrQ = " SELECT TOP 1 ISNULL(cmclaim_request_master.statusunkid, 0) AS StatusId " & _
                      ", cmclaim_request_master.claimrequestno " & _
                      ", cmclaim_request_master.employeeunkid " & _
                      " FROM cmclaim_request_master " & _
                    " LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                    " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_tran.isvoid = 0 AND cmclaim_request_master.statusunkid = 2 " & _
                    " AND cmclaim_request_tran.expenseunkid = @expenseunkid  AND cmclaim_request_master.employeeunkid = @employeeunkid "


            If xSectorRouteId > 0 Then
                StrQ &= " AND cmclaim_request_tran.secrouteunkid = @secrouteunkid "
                objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSectorRouteId)
            End If

            'Pinkal (13-Mar-2019) -- End

            If xClaimFormId > 0 Then
                StrQ &= " AND cmclaim_request_master.crmasterunkid <> @crmasterunkid "
                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimFormId)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintStatusId = CInt(dsList.Tables(0).Rows(0)("StatusId"))
            Else
                mintStatusId = -1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLastExpenseStatus; Module Name: " & mstrModuleName)
        End Try
        Return mintStatusId
    End Function
    'Pinkal (30-Apr-2018) - End


    'Pinkal (12-May-2018) -- Start
    'Enhancement - Ref # 205 Claim Request Form For Abood Group.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeClaimForm(ByVal xEmployeeId As Integer, ByVal xExpenseType As Integer _
                                                          , Optional ByVal mblFlag As Boolean = False, Optional ByVal mstrFilter As String = "") As DataTable


        'Pinkal (30-May-2020) -- Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.[Optional ByVal mstrFilter As String = ""]

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.ClearParameters()

            If mblFlag = True Then
                StrQ = "SELECT 0 as crmasterunkid, ' ' +  @name  as claimrequestno UNION "
            End If

            StrQ &= "SELECT ISNULL(crmasterunkid,0) as crmasterunkid,ISNULL(claimrequestno,0) as claimrequestno " & _
                   " FROM cmclaim_request_master " & _
                   " WHERE employeeunkid =@employeeunkid and expensetypeid=@expensetypeid"


            'Pinkal (04-Jan-2019) -- Start
            'Bug - [0003308] Error when exporting claims form for one employee.
            StrQ &= " AND cmclaim_request_master.isvoid = 0 "
            'Pinkal (04-Jan-2019) -- End

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            If mstrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilter
            End If
            'Pinkal (30-May-2020) -- End

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Select"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseType)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeClaimForm; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList.Tables(0)
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFinalApproverApprovedDetails(ByVal xPaymentApprovalwithLeaveApproval As Boolean, ByVal intEmpID As Integer _
                                                                          , ByVal intExpenseTypeId As Integer, ByVal intRequestMstId As Integer _
                                                                          , Optional ByVal xExpenseId As Integer = 0, Optional ByVal mblnIsImprest As Boolean = False) As DataSet

        'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[Optional ByVal mblnIsImprest As Boolean = False]


        Dim mdecAmount As Decimal = 0
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try
            Dim mstrApproverID As String = ""

            If xPaymentApprovalwithLeaveApproval AndAlso intExpenseTypeId = enExpenseType.EXP_LEAVE Then
                Dim dtTab As DataTable = Nothing
                Dim objpending As New clspendingleave_Tran
                dsList = objpending.GetEmployeeApproverListWithPriority(intEmpID, -1, Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(priority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("priority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("approverunkid").ToString() & ","
                        Next
                    End If
                End If
                objpending = Nothing
            Else

                Dim objExpAppr As New clsExpenseApprover_Master
                dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmpID, "List", Nothing, intRequestMstId)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(crpriority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverID &= dr("crapproverunkid").ToString() & ","
                        Next
                    End If
                End If
                objExpAppr = Nothing
            End If


            If mstrApproverID.Trim.Length > 0 Then
                mstrApproverID = mstrApproverID.Trim.Substring(0, mstrApproverID.Trim.Length - 1)
            Else
                mstrApproverID = "0"
            End If

            Dim objDataOperation As New clsDataOperation
            strQ = " SELECT cmclaim_approval_tran.expenseunkid,ISNULL(cmexpense_master.name,'') AS Expense, ISNULL(Amount,0.00) AS Amount  " & _
                      " FROM cmclaim_approval_tran  " & _
                      " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                      " WHERE crapproverunkid IN (" & mstrApproverID & ") AND isvoid = 0  " & _
                      " AND crmasterunkid = @masterunkid AND statusunkid = 1 "


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If mblnIsImprest Then
                strQ &= " AND cmexpense_master.isimprest = 1 "
            End If
            'Pinkal (11-Sep-2019) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRequestMstId)

            If xExpenseId > 0 Then
                strQ &= " AND cmclaim_approval_tran.expenseunkid = @expenseunkid "
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            End If



            'Pinkal (24-Dec-2019) -- Start
            'Enhancement - Claim Retirement Enhancements for NMB.

            If mblnIsImprest Then

                strQ &= " UNION " & _
                            " SELECT cmclaim_request_tran.expenseunkid,ISNULL(cmexpense_master.name,'') AS Expense, ISNULL(Amount,0.00) AS Amount  " & _
                            " FROM cmclaim_request_tran  " & _
                            " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid " & _
                            " WHERE isvoid = 0  " & _
                            " AND crmasterunkid = @masterunkid  AND cmexpense_master.isimprest = 1 "

                If xExpenseId > 0 Then
                    strQ &= " AND cmclaim_approval_tran.expenseunkid = @expenseunkid "
                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
                End If

            End If

            'Pinkal (24-Dec-2019) -- End



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFinalApproverApprovedDetails; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (12-May-2018) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    '********************************* THIS METHOD WILL UPDATE GIVEN COLUMN IN CLAIM REQUEST TABLE WITH THE VALUE PROVIDE
    '********************************* THIS METHOD IS USED IN TRAINING REQUISITION MODULE
    Public Function UpdateColumnValue(ByVal eExpCategory As enExpenseType, _
                                      ByVal intCrMasterId As Integer, _
                                      ByVal strColumnValue As String, _
                                      ByVal oValue As Object, _
                                      ByVal intUserId As Integer, _
                                      Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        If xDataOper Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOper
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "UPDATE cmclaim_request_master SET " & _
                   " " & strColumnValue & " = @oValue " & _
                   "WHERE crmasterunkid = @crmasterunkid "

            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCrMasterId)
            objDataOperation.AddParameter("@oValue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, oValue.ToString())

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", intCrMasterId, "", "", 0, enAuditType.EDIT, 0, False, intUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objCommonATLog = Nothing

            If eExpCategory = enExpenseType.EXP_TRAINING Then
                objDataOperation.ClearParameters()

                StrQ = "SELECT " & _
                       "    A.crapprovaltranunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         AT.crapprovaltranunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY AT.crmasterunkid,AT.crtranunkid ORDER BY AL.crpriority) AS rno " & _
                       "    FROM cmclaim_approval_tran AS AT " & _
                       "        JOIN cmexpapprover_master AS AM ON AM.crapproverunkid = AT.crapproverunkid " & _
                       "        JOIN cmapproverlevel_master AS AL ON AL.crlevelunkid = AM.crlevelunkid " & _
                       "    WHERE AT.crmasterunkid = @crmasterunkid AND AM.isvoid = 0 AND AL.isactive = 1 AND AM.isactive = 1 " & _
                       ") AS A WHERE A.rno = 1 "

                Dim ds As New DataSet()

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCrMasterId)

                ds = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If ds.Tables("List").Rows.Count > 0 Then
                    Dim strids As String = String.Empty
                    strids = String.Join(",", ds.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("crapprovaltranunkid").ToString()).ToArray())

                    If strids.Trim.Length > 0 Then
                        StrQ = "UPDATE cmclaim_approval_tran SET visibleid = 2 WHERE crapprovaltranunkid IN (" & strids & ") "

                        objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateColumnValue ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [09-OCT-2018] -- END



    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GenerateNewRequisitionRequestForP2P(ByVal xDatabaseName As String, ByVal xUserId As Integer, ByVal xYearId As Integer, ByVal xCompnayId As Integer _
                                                                                                                       , ByVal xUserModeSetting As String, ByVal xEmployeeId As Integer, ByVal xClaimRequestNo As String, ByVal xClaimDate As DateTime _
                                                                                                                       , ByVal mstrClaimRemark As String, ByVal xApplierName As String, ByVal mdtClaimDetail As DataTable _
                                                                                                                       , ByVal mdtAttachment As DataTable, ByRef objP2P As clsCRP2PIntegration) As String

        'Pinkal (04-Feb-2019) --      'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ ByRef objP2P As clsCRP2PIntegration ]

        Dim mstrRequestRequisition As String = ""
        Try
            If xClaimRequestNo.Trim.Length > 0 Then
                Dim xDepartment As String = ""
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                '  Dim xCostCenter As String = ""
                Dim xEmployeeCode As String = ""
                'Pinkal (04-Feb-2019) -- End

                If xEmployeeId > 0 Then
                    '/* START TO GET EMPLOYEE DEPARTMENT/COST CENTER AS ON CLAIM DATE REQUESTED BY EMPLOYEE/USER.

                    Dim objEmployee As New clsEmployee_Master
                    Dim StrCheck_Fields As String = String.Empty


                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center
                    StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department
                    'Pinkal (04-Feb-2019) -- End



                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'Dim dsList As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, xDatabaseName, xUserId, xYearId, xCompnayId, xClaimDate, xClaimDate _
                    '                                                                                                                                , xUserModeSetting, True, False, "List", xEmployeeId, False, "", False, False, False, False, Nothing)
                    Dim dsList As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, xDatabaseName, xUserId, xYearId, xCompnayId, xClaimDate, xClaimDate _
                                                                                                                                                    , xUserModeSetting, True, False, "List", xEmployeeId, False, "", False, False, False, False, Nothing, False)
                    'Pinkal (07-Mar-2019) -- End


                    objEmployee = Nothing

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        xDepartment = dsList.Tables(0).Rows(0)(Language.getMessage("clsEmployee_Master", 120, "Department")).ToString()
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        'xCostCenter = dsList.Tables(0).Rows(0)(Language.getMessage("clsEmployee_Master", 206, "Cost Center")).ToString()
                        xEmployeeCode = dsList.Tables(0).Rows(0)(Language.getMessage("clsEmployee_Master", 42, "Code")).ToString()
                        'Pinkal (04-Feb-2019) -- End

                    End If
                    dsList.Clear()
                    dsList = Nothing
                    '/* END TO GET EMPLOYEE DEPARTMENT/COST CENTER AS ON CLAIM DATE REQUESTED BY EMPLOYEE/USER.



                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'If xDepartment.Trim().Length > 0 AndAlso xCostCenter.Trim.Length > 0 Then
                    If xDepartment.Trim().Length > 0 Then
                        'Pinkal (04-Feb-2019) -- End
                        Dim xTotalClaimAmount As Decimal = 0
                        If mdtClaimDetail IsNot Nothing AndAlso mdtClaimDetail.Rows.Count > 0 Then
                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'xTotalClaimAmount = mdtClaimDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("amount"))
                            xTotalClaimAmount = mdtClaimDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("base_amount"))
                            mdtClaimDetail = New DataView(mdtClaimDetail, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable()
                            'Pinkal (04-Feb-2019) -- End
                        End If


                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                        'mstrRequestRequisition = "{""extRefNo"" : """ & xClaimRequestNo.Trim() & """,""budgetInLimit"":""Y"",""requisitionType"":""SERVICE"",""deptId"": """ & xDepartment.Trim() & _
                        '                                                       """, ""costCenter"":""" & xCostCenter.Trim() & """,""reqExceptedDate"":""" & Now.ToString("yyyy-MM-dd HH:mm:ss") & """,""remarks"":""" & mstrClaimRemark & _
                        '                                                       """,""maker"":""" & xApplierName.Trim() & """,""makerDtStamp"":""" & xClaimDate.ToString("yyyy-MM-dd HH:mm:ss") & """,""checker:""" & "" & _
                        '                                                       """,""checkerDtStamp"":""" & " " & """,""reqStatus"":""PENDING"",""category"":""Staff expenses"",""plannedBudget"":" & xTotalClaimAmount


                        'If mdtClaimDetail IsNot Nothing AndAlso mdtClaimDetail.Rows.Count > 0 Then
                        '    Dim xCount As Integer = 0
                        '    mstrRequestRequisition &= ",""requisitionItemList"":["
                        '    For Each dr In mdtClaimDetail.Rows
                        '        If xCount > 0 Then
                        '            mstrRequestRequisition &= ","
                        '        End If
                        '        mstrRequestRequisition &= "{""glCode"":""" & dr("glCode").ToString() & """,""glDesc"":""" & dr("gldesc").ToString() & """,""budget"":" & CDec(dr("amount")) & ",""itemDesc"":""" & dr("expense_remark") & """}"
                        '        xCount += 1
                        '    Next
                        '    mstrRequestRequisition &= "]"
                        'End If

                        If mdtClaimDetail IsNot Nothing AndAlso mdtClaimDetail.Rows.Count > 0 Then

                            '/* START-- DON'T CHANGE ANYTHING TAKEN ONLY FIRST EMPLOYEE BANK ACCOUNT NO OF EMPLOYEE AS PER RUTT'S GUIDANCE.IF HE WANTS TO CHANGE IT DON'T CHANGE FOR NMB.
                            Dim xBankAcNo As String = ""
                            Dim objEmpBank As New clsEmployeeBanks
                            Dim dsEmpBankList As DataSet = objEmpBank.GetEmployeeBanks(xDatabaseName, xUserId, xYearId, xCompnayId, xClaimDate, xClaimDate, xUserModeSetting, True, False, xEmployeeId, False, "")
                            If dsEmpBankList IsNot Nothing AndAlso dsEmpBankList.Tables(0).Rows.Count > 0 Then
                                xBankAcNo = dsEmpBankList.Tables(0).Rows(0)("accountno").ToString()
                            Else

                                End If
                            objEmpBank = Nothing
                            '/* END-- DON'T CHANGE ANYTHING TAKEN ONLY FIRST EMPLOYEE BANK ACCOUNT NO OF EMPLOYEE AS PER RUTT'S GUIDANCE.IF HE WANTS TO CHANGE IT DON'T CHANGE FOR NMB.

                            mstrRequestRequisition = "{""category"":""Staff expenses"",""mainline"":""" & mdtClaimDetail.Rows(0)("gldesc").ToString() & """,""dept"": """ & xDepartment.Trim() & _
                                                                                  """,""glCode"":""" & mdtClaimDetail.Rows(0)("glCode").ToString() & """,""costCenter"":""" & mdtClaimDetail.Rows(0)("costcentercode").ToString().Trim() & """,""ccy"": ""TZS""" & _
                                                                                  ",""invAmount"": """ & xTotalClaimAmount & """,""invoiceNo"": """ & xClaimRequestNo.Trim() & """,""maker"":""" & xApplierName.Trim() & _
                                                                                  """,""remarks"":""" & mdtClaimDetail.Rows(0)("expense_remark").ToString().Trim() & """,""status"": ""SUBMIT"",""supplierId"":""" & xBankAcNo.Trim() & """,""extRefNo"" : """ & xClaimRequestNo.Trim() & """"

                            objP2P.category = "Staff expenses"
                            objP2P.mainline = mdtClaimDetail.Rows(0)("gldesc").ToString().Trim()
                            objP2P.dept = xDepartment.Trim()
                            objP2P.glCode = mdtClaimDetail.Rows(0)("glCode").ToString().Trim()
                            objP2P.costCenter = mdtClaimDetail.Rows(0)("costcentercode").ToString().Trim()
                            objP2P.ccy = "TZS"
                            objP2P.invAmount = xTotalClaimAmount
                            objP2P.invoiceNo = xClaimRequestNo.Trim()
                            objP2P.maker = xApplierName.Trim()
                            objP2P.remarks = mdtClaimDetail.Rows(0)("expense_remark").ToString().Trim()
                            objP2P.status = "SUBMIT"
                            objP2P.supplierId = xBankAcNo.Trim()
                            objP2P.extRefNo = xClaimRequestNo.Trim()
                        End If

                        'Pinkal (04-Feb-2019) -- End

    'Pinkal (22-Jul-2019) -- Start
    'Enhancement - Working on P2P Document attachment in Claim Request for NMB.

                        'If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                        '    Dim xCount As Integer = 0
                        '    mstrRequestRequisition &= ",""documentsList"":["
                        '    For Each dr In mdtAttachment.Rows
                        '        If xCount > 0 Then
                        '            mstrRequestRequisition &= ","
                        '        End If

                        '        'Pinkal (04-Feb-2019) -- Start
                        '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        '        'mstrRequestRequisition &= "{""documentTitle"":""" & dr("fileuniquename").ToString()  & """,""documentType"":""" & dr("Documentype").ToString() & """,""documentExtension"":""" & dr("fileextension").ToString() & """,""value"":""" & dr("DocBase64Value").ToString() & """}"
                        '        mstrRequestRequisition &= "{""documentTitle"":""" & dr("fileuniquename").ToString().Substring(0, dr("fileuniquename").ToString().IndexOf(".")) & """,""documentType"":""" & dr("Documentype").ToString() & """,""documentExtension"":""" & dr("fileextension").ToString() & """, ""fileName:""" & dr("fileuniquename").ToString() & """,""value"":""" & dr("DocBase64Value").ToString() & """}"
                        '        'Pinkal (04-Feb-2019) -- End
                        '        xCount += 1
                        '    Next
                        '    mstrRequestRequisition &= "]"
                        'End If

                        'mstrRequestRequisition &= "}"

                        If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                            For Each dr In mdtAttachment.Rows
                                objP2P.documentList.Add(New DocumentList(dr("fileuniquename").ToString().Substring(0, dr("fileuniquename").ToString().IndexOf(".")), dr("Documentype").ToString(), dr("fileextension").ToString(), dr("fileuniquename").ToString(), dr("DocBase64Value").ToString()))
                            Next
                                End If

  'Pinkal (22-Jul-2019) -- End

                    End If

                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateNewRequisitionRequestForP2P; Module Name: " & mstrModuleName)
        End Try
        Return mstrRequestRequisition
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 

    'Pinkal (31-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    'Public Function UpdateP2PResponseToClaimMst(ByVal dtTable As DataTable, ByVal xClaimRequestID As Integer, ByVal xUserId As Integer) As Boolean
    Public Function UpdateP2PResponseToClaimMst(ByVal dtTable As DataTable, ByVal xClaimRequestID As Integer, ByVal xUserId As Integer, ByVal mstrPostedData As String) As Boolean
        'Pinkal (31-May-2019) -- End
        Try
            If dtTable Is Nothing Then Return True
            Dim strQ As String = ""
            Dim exForce As Exception

            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = "UPDATE cmclaim_request_master SET " & _
                         " p2pposteddata = @p2pposteddata " & _
                         ",p2prequisitionid = @p2prequisitionid " & _
                         ",p2pmessage = @p2pmessage " & _
                         ",p2pstatuscode = @p2pstatuscode " & _
                         ",p2ptimestamp = @p2ptimestamp " & _
                         " WHERE crmasterunkid  = @crmasterunkid "

            'Pinkal (31-May-2019) -- Start           'Enhancement - NMB FINAL LEAVE UAT CHANGES.[" p2pposteddata = @p2pposteddata " & _]

            For Each dr As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@p2prequisitionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("PmtReferenceNo").ToString())
                objDataOperation.AddParameter("@p2pmessage", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr("message").ToString())
                objDataOperation.AddParameter("@p2pstatuscode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("status").ToString())

                If IsDBNull(dr("timeStamp")) OrElse dr("timeStamp").ToString().Trim.Length <= 0 Then
                    objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    Dim dt As New DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)
                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'dt = dt.AddSeconds(CInt(dr("timestamp"))).ToLocalTime()
                    dt = dt.AddMilliseconds(CDbl(dr("timeStamp"))).ToLocalTime()
                    'Pinkal (04-Feb-2019) -- End
                    objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dt)
                End If


                'Pinkal (31-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.

                'Pinkal (22-Jul-2019) -- Start
                'Enhancement - Working on P2P Document attachment in Claim Request for NMB.
                'objDataOperation.AddParameter("@p2pposteddata", SqlDbType.Nvarchar, mstrPostedData.Length, mstrPostedData)
                objDataOperation.AddParameter("@p2pposteddata", SqlDbType.VarBinary, System.Text.Encoding.Unicode.GetBytes(mstrPostedData).Length, System.Text.Encoding.Unicode.GetBytes(mstrPostedData))
                'Pinkal (22-Jul-2019) -- End

                'Pinkal (31-May-2019) -- End

                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimRequestID)
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                Dim objCommonATLog As New clsCommonATLog
                With objCommonATLog
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", xClaimRequestID, "cmclaim_request_tran", "crtranunkid", -1, 2, 2, False, xUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objCommonATLog = Nothing
            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateP2PResponseToClaimMst; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (20-Nov-2018) -- End

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendEmployeeNotificationClaimApplicationByManager(ByVal intExpCategoryID As Integer, ByVal intEmpId As Integer, _
                                                                                                 ByVal intClaimFormID As Integer, ByVal mstrClaimFormNo As String, _
                                                                                                 ByVal xDatabaseName As String, ByVal iUserId As Integer, _
                                                                                                 ByVal mdtEmployeAsOnDate As Date, _
                                                                                                 Optional ByVal intCompanyUnkId As Integer = 0, _
                                                                                                 Optional ByVal strArutiSelfServiceURL As String = "", _
                                                                                                 Optional ByVal iLoginTypeId As Integer = 0, _
                                                                                                 Optional ByVal mstrWebFrmName As String = "", _
                                                                                                 Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Try
            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub



            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = mintUserunkid

            Dim mstrUserName As String = ""

            If objUser._Firstname.Trim.Length > 0 AndAlso objUser._Lastname.Trim.Length > 0 Then
                mstrUserName = objUser._Firstname.Trim() & " " & objUser._Lastname.Trim()
            Else
                mstrUserName = objUser._Username
            End If

            objUser = Nothing


            Dim objEmp As New clsEmployee_Master
            objEmp._Employeeunkid(mdtEmployeAsOnDate, objDoOperation) = intEmpId


            Dim strMessage As String = ""
            Dim strLink As String = strArutiSelfServiceURL & "/Claims_And_Expenses/wPg_ClaimAndRequestList.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & intEmpId.ToString() & "|" & intClaimFormID.ToString() & "|" & intExpCategoryID))


            Dim objMail As New clsSendMail
            objMail._Subject = Language.getMessage(mstrModuleName, 15, "Claim Application Submission Notification")

            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & getTitleCase(objEmp._Firstname & "  " & objEmp._Surname) & ", <BR><BR>"
            strMessage &= " " & Language.getMessage(mstrModuleName, 16, "Please note that claim application no ") & " <B>(" & mstrClaimFormNo & ")</B> " & Language.getMessage(mstrModuleName, 17, "has been made on your behalf by") & " <B>(" & mstrUserName & ")</B>." & Language.getMessage(mstrModuleName, 18, "Kindly click on the following link to view the claim form.")

            strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            objMail._Message = strMessage
            objMail._ToEmail = objEmp._Email

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
                objMail._ClientIP = mstrWebClientIP
                objMail._HostName = mstrWebHostName
            End If
            objMail._LogEmployeeUnkid = -1
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = iUserId
            objMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT

            objMail.SendMail(intCompanyUnkId)

            objEmp = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmployeeNotificationClaimApplicationByManager; Module Name: " & mstrModuleName)
        End Try
    End Sub



    'Pinkal (30-May-2020) -- Start
    'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
    'THIS METHOD IS ONLY USE KADDCO FUEL/LUBRICANT APPLICATION FORM REPORT.IT WILL TAKE ONLY 1 EXPENSE AT A TIME.

    Public Function GetApprovedAmountAsOnDate(ByVal xPaymentApprovalwithLeaveApproval As Boolean, ByVal xEmpAsonDate As Date, ByVal intEmpID As Integer _
                                                                      , ByVal intExpenseTypeId As Integer, ByVal xExpenseId As Integer) As Decimal
        Dim mdecAmount As Decimal = 0
        Try

            Dim objDataOperation As New clsDataOperation

            Dim strQ As String = " SELECT " & _
                                           " ISNULL(STUFF((SELECT DISTINCT  ',' + CAST(cmclaim_request_master.crmasterunkid AS NVARCHAR(MAX)) " & _
                                           " FROM cmclaim_request_master " & _
                                           " JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                                           " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 AND cmclaim_approval_tran.expenseunkid = @expenseunkid " & _
                                           " AND cmclaim_request_master.employeeunkid= @employeeunkid " & _
                                           " AND CONVERT(char(8),cmclaim_request_master.transactiondate,112) <= @EmpAsonDate 	FOR XML PATH ('')) , 1, 1, ''), '0') AS ClaimIds "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            'Pinkal (27-Jun-2020) -- Start
            'Enhancement NMB -   Working on Employee Signature Report.
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            'Pinkal (27-Jun-2020) -- End
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEmpAsonDate))
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim mstrClaimIds As String = dsList.Tables(0).Rows(0)("ClaimIds").ToString()
                Dim arClaimIds As String() = mstrClaimIds.Trim.Split(",")

                If arClaimIds IsNot Nothing AndAlso arClaimIds.Length > 0 Then

                    strQ = " SELECT ISNULL(ApprovedAmt,0.00) AS ApprovedAmt FROM cmclaim_request_master " & _
                              " LEFT JOIN ( " & _
                              "                     SELECT " & _
                              "                             crmasterunkid " & _
                              "                             ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                              "                     FROM cmclaim_approval_tran " & _
                              "                     WHERE cmclaim_approval_tran.isvoid = 0 And cmclaim_approval_tran.statusunkid = 1 " & _
                              "                     AND cmclaim_approval_tran.expenseunkid = @expenseunkid " & _
                              "                     AND cmclaim_approval_tran.crapproverunkid IN " & _
                              "                     ( " & _
                              "                          SELECT crapproverunkid FROM  " & _
                              "                                 ( " & _
                              "                                     SELECT " & _
                              "                                               ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                              "                                              ,cm.crmasterunkid " & _
                              "                                              ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                              "                                     FROM cmclaim_approval_tran cm " & _
                              "                                     JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                              "                                     JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                              "                                     WHERE cm.isvoid = 0 AND cm.expenseunkid = @expenseunkid  AND cm.crmasterunkid = @crmasterunkid " & _
                              "                                 ) as cnt where cnt.rno= 1" & _
                              "                     ) " & _
                              "	                GROUP BY crmasterunkid" & _
                              "              ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                              " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.crmasterunkid = @crmasterunkid"


                    'Pinkal (19-Nov-2020) -- Enhancement KADCO Employee Claim Form Report [AND cm.crmasterunkid = @crmasterunkid]

                    For i As Integer = 0 To arClaimIds.Length - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(arClaimIds(i)))
                        objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
                        Dim dsApprovedAmt As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        If dsApprovedAmt IsNot Nothing AndAlso dsApprovedAmt.Tables(0).Rows.Count > 0 Then
                            mdecAmount += CDec(dsApprovedAmt.Tables(0).Rows(0)("ApprovedAmt"))
                        End If
                    Next

                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApprovedAmountAsOnDate; Module Name: " & mstrModuleName)
        End Try
        Return mdecAmount
    End Function

    'Pinkal (30-May-2020) -- End


    'Pinkal (19-Jul-2021)-- Start
    'Kadco Enhancements : Working on Fuel Application Form Printing Enhancement.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdatePrintInfoForFuelApplicationForm(ByVal xClaimMasterId As Integer, ByVal mblnIsPrinted As Boolean, ByVal mdtPrintedDateTime As DateTime _
                                                                                , ByVal xUserId As Integer, ByVal mstrIP As String, ByVal mstrHost As String, ByVal mblnIsWebPrint As Boolean) As Boolean
        Dim mblnFlag As Boolean = False
        Dim exForce As Exception = Nothing
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()

            Dim strQ As String = " UPDATE cmclaim_request_master SET " & _
                                           "  isprinted = @isprinted " & _
                                           ", printdatetime = @printdatetime " & _
                                           ", printeduserunkid = @printeduserunkid " & _
                                           ", ip = @ip " & _
                                           ", host = @host " & _
                                           ", iswebprint = @iswebprint " & _
                                           " WHERE isvoid  = 0 AND crmasterunkid = @crmasterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isprinted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPrinted)
            objDataOperation.AddParameter("@printdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPrintedDateTime)
            objDataOperation.AddParameter("@printeduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost)
            objDataOperation.AddParameter("@iswebprint", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWebPrint)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimMasterId)
            objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", xClaimMasterId, "cmclaim_request_tran", "crtranunkid", 0, 2, 0, False, xUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            mblnFlag = True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePrintInfoForFuelApplicationForm; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    'Pinkal (19-Jul-2021)-- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Claim Number already exists. Please define new Claim Number.")
            Language.setMessage(mstrModuleName, 2, "Sorry you cannot delete this claim form. Reason this form is already linked with some transactions.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Language.setMessage(mstrModuleName, 4, " ] time(s).")
            Language.setMessage(mstrModuleName, 5, "Dear")
            Language.setMessage(mstrModuleName, 6, "Claim Status Notification")
            Language.setMessage(mstrModuleName, 7, " This is to inform you that your claim application no")
            Language.setMessage(mstrModuleName, 8, " has been")
            Language.setMessage(mstrModuleName, 9, "Remarks/Comments:")
            Language.setMessage(mstrModuleName, 10, "Kindly take note of it.")
            Language.setMessage(mstrModuleName, 11, "Sorry,You cannot apply this expense.Reason : Eligibility to apply expense does not match with eligibility months set on selected expense. Eligiblilty set for the selected expense is")
            Language.setMessage(mstrModuleName, 12, "months.")
            Language.setMessage(mstrModuleName, 13, "Sorry,you cannot apply for this expense now as your previous claim application for this expense has not been approved yet.")
            Language.setMessage(mstrModuleName, 14, "with below")
            Language.setMessage(mstrModuleName, 15, "Claim Application Submission Notification")
            Language.setMessage(mstrModuleName, 16, "Please note that claim application no")
            Language.setMessage(mstrModuleName, 17, " has been made on your behalf by")
            Language.setMessage(mstrModuleName, 18, "Kindly click on the following link to view the claim form.")
            Language.setMessage(mstrModuleName, 51, "Select")

            Language.setMessage("clsExpCommonMethods", 2, "Leave")
            Language.setMessage("clsExpCommonMethods", 3, "Medical")
            Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
			Language.setMessage("clsExpCommonMethods", 9, "Imprest")

            Language.setMessage("clsEmployee_Master", 42, "Code")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
			Language.setMessage("clsEmployee_Master", 120, "Department")
            Language.setMessage("clsMasterData", 277, "Issued")
            Language.setMessage("clsMasterData", 899, "Return")


        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class