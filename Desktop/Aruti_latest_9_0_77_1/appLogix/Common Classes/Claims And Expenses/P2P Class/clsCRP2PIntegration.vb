﻿
'Pinkal (22-Jul-2019) -- Start
'Enhancement - Working on P2P Document attachment in Claim Request for NMB.

Public Class clsCRP2PIntegration

    Dim mstrCategory As String = ""
    Dim mstrmainline As String = ""
    Dim mstrdept As String = ""
    Dim mstrglCode As String = ""
    Dim mstrcostCenter As String = ""
    Dim mstrccy As String = ""
    Dim mdecinvAmount As Decimal = 0
    Dim mstrinvoiceNo As String = ""
    Dim mstrmaker As String = ""
    Dim mstrremarks As String = ""
    Dim mstrstatus As String = ""
    Dim mstrsupplierId As String = ""
    Dim mstrextRefNo As String = ""
    Dim IDocumentList As New List(Of DocumentList)

    Public Property category() As String
        Get
            Return mstrCategory
        End Get
        Set(ByVal value As String)
            mstrCategory = value
        End Set
    End Property

    Public Property mainline() As String
        Get
            Return mstrmainline
        End Get
        Set(ByVal value As String)
            mstrmainline = value
        End Set
    End Property

    Public Property dept() As String
        Get
            Return mstrdept
        End Get
        Set(ByVal value As String)
            mstrdept = value
        End Set
    End Property

    Public Property glCode() As String
        Get
            Return mstrglCode
        End Get
        Set(ByVal value As String)
            mstrglCode = value
        End Set
    End Property

    Public Property costCenter() As String
        Get
            Return mstrcostCenter
        End Get
        Set(ByVal value As String)
            mstrcostCenter = value
        End Set
    End Property

    Public Property ccy() As String
        Get
            Return mstrccy
        End Get
        Set(ByVal value As String)
            mstrccy = value
        End Set
    End Property

    Public Property invAmount() As Decimal
        Get
            Return mdecinvAmount
        End Get
        Set(ByVal value As Decimal)
            mdecinvAmount = value
        End Set
    End Property

    Public Property invoiceNo() As String
        Get
            Return mstrinvoiceNo
        End Get
        Set(ByVal value As String)
            mstrinvoiceNo = value
        End Set
    End Property

    Public Property maker() As String
        Get
            Return mstrmaker
        End Get
        Set(ByVal value As String)
            mstrmaker = value
        End Set
    End Property

    Public Property remarks() As String
        Get
            Return mstrremarks
        End Get
        Set(ByVal value As String)
            mstrremarks = value
        End Set
    End Property

    Public Property status() As String
        Get
            Return mstrstatus
        End Get
        Set(ByVal value As String)
            mstrstatus = value
        End Set
    End Property
 
    Public Property supplierId() As String
        Get
            Return mstrsupplierId
        End Get
        Set(ByVal value As String)
            mstrsupplierId = value
        End Set
    End Property

    Public Property extRefNo() As String
        Get
            Return mstrextRefNo
        End Get
        Set(ByVal value As String)
            mstrextRefNo = value
        End Set
    End Property

    Public Property documentList() As List(Of DocumentList)
        Get
            Return IDocumentList
        End Get
        Set(ByVal value As List(Of DocumentList))
            IDocumentList = value
        End Set
    End Property

End Class

Public Class DocumentList

    Public mstrDocumentTitle As String = String.Empty
    Public mstrDocumentType As String = String.Empty
    Public mstrDocumentExtension As String = String.Empty
    Public mstrFileName As String = String.Empty
    Public mstrValue As String = String.Empty

#Region "Constructor"

    Public Sub New()
    End Sub
    Public Sub New(ByVal _documentTitle As String, ByVal _documentType As String, ByVal _documentExtension As String, ByVal _filename As String, ByVal _value As String)
        mstrDocumentTitle = _documentTitle
        mstrDocumentType = _documentType
        mstrDocumentExtension = _documentExtension
        mstrFileName = _filename
        mstrValue = _value
    End Sub

#End Region

#Region "Properties"

    Public Property documentTitle() As String
        Get
            Return mstrDocumentTitle
        End Get
        Set(ByVal value As String)
            mstrDocumentTitle = value
        End Set
    End Property

    Public Property documentType() As String
        Get
            Return mstrDocumentType
        End Get
        Set(ByVal value As String)
            mstrDocumentType = value
        End Set
    End Property

    Public Property documentExtension() As String
        Get
            Return mstrDocumentExtension
        End Get
        Set(ByVal value As String)
            mstrDocumentExtension = value
        End Set
    End Property

    Public Property fileName() As String
        Get
            Return mstrFileName
        End Get
        Set(ByVal value As String)
            mstrFileName = value
        End Set
    End Property

    Public Property value() As String
        Get
            Return mstrValue
        End Get
        Set(ByVal val As String)
            mstrValue = val
        End Set
    End Property

#End Region

End Class

'Pinkal (22-Jul-2019) -- End


Public Class clsCRP2PUserDetail
    Dim mstrUserName As String = "aruti"
    Dim mstrPassword As String = "123456"

    Public Property username() As String
        Get
            Return mstrUserName
        End Get
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public Property password() As String
        Get
            Return mstrPassword
        End Get
        Set(ByVal value As String)
            mstrPassword = value
        End Set
    End Property

End Class


'Pinkal (11-Sep-2019) -- Start
'Enhancement NMB - Working On Claim Retirement for NMB.

Public Class clsClaimRetirementP2PIntegration

    Dim mstrClaimType As String = ""
    Dim mstrCategory As String = ""
    Dim mstrmainline As String = ""
    Dim mstrdept As String = ""
    Dim mstrglCode As String = ""
    Dim mstrcostCenter As String = ""
    Dim mstrccy As String = ""
    Dim mdecinvAmount As Decimal = 0
    Dim mstrinvoiceNo As String = ""
    Dim mstrmaker As String = ""
    Dim mstrremarks As String = ""
    Dim mstrstatus As String = ""
    Dim mstrsupplierId As String = ""
    Dim mstrextRefNo As String = ""
    Dim mstrclaimRefNo As String = ""
    Dim IDocumentList As New List(Of DocumentList)

    Public Property claimType() As String
        Get
            Return mstrClaimType
        End Get
        Set(ByVal value As String)
            mstrClaimType = value
        End Set
    End Property

    Public Property category() As String
        Get
            Return mstrCategory
        End Get
        Set(ByVal value As String)
            mstrCategory = value
        End Set
    End Property

    Public Property mainline() As String
        Get
            Return mstrmainline
        End Get
        Set(ByVal value As String)
            mstrmainline = value
        End Set
    End Property

    Public Property dept() As String
        Get
            Return mstrdept
        End Get
        Set(ByVal value As String)
            mstrdept = value
        End Set
    End Property

    Public Property glCode() As String
        Get
            Return mstrglCode
        End Get
        Set(ByVal value As String)
            mstrglCode = value
        End Set
    End Property

    Public Property costCenter() As String
        Get
            Return mstrcostCenter
        End Get
        Set(ByVal value As String)
            mstrcostCenter = value
        End Set
    End Property

    Public Property ccy() As String
        Get
            Return mstrccy
        End Get
        Set(ByVal value As String)
            mstrccy = value
        End Set
    End Property

    Public Property invAmount() As Decimal
        Get
            Return mdecinvAmount
        End Get
        Set(ByVal value As Decimal)
            mdecinvAmount = value
        End Set
    End Property

    Public Property invoiceNo() As String
        Get
            Return mstrinvoiceNo
        End Get
        Set(ByVal value As String)
            mstrinvoiceNo = value
        End Set
    End Property

    Public Property maker() As String
        Get
            Return mstrmaker
        End Get
        Set(ByVal value As String)
            mstrmaker = value
        End Set
    End Property

    Public Property remarks() As String
        Get
            Return mstrremarks
        End Get
        Set(ByVal value As String)
            mstrremarks = value
        End Set
    End Property

    Public Property status() As String
        Get
            Return mstrstatus
        End Get
        Set(ByVal value As String)
            mstrstatus = value
        End Set
    End Property

    Public Property supplierId() As String
        Get
            Return mstrsupplierId
        End Get
        Set(ByVal value As String)
            mstrsupplierId = value
        End Set
    End Property

    Public Property extRefNo() As String
        Get
            Return mstrextRefNo
        End Get
        Set(ByVal value As String)
            mstrextRefNo = value
        End Set
    End Property

    Public Property claimRefNo() As String
        Get
            Return mstrclaimRefNo
        End Get
        Set(ByVal value As String)
            mstrclaimRefNo = value
        End Set
    End Property

    Public Property documentList() As List(Of DocumentList)
        Get
            Return IDocumentList
        End Get
        Set(ByVal value As List(Of DocumentList))
            IDocumentList = value
        End Set
    End Property

End Class

'Pinkal (11-Sep-2019) -- End
