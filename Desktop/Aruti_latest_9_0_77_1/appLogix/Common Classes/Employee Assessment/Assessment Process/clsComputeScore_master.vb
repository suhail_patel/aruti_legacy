﻿'************************************************************************************************************************************
'Class Name : clsComputeScore_master.vb
'Purpose    :
'Date       :25/05/2016
'Written By :Shani K. Sheladiya
'Modified   :
'************************************************************************************************************************************

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class clsComputeScore_master

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsComputeScore_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
#End Region

#Region "Enum"
    Public Enum enAssessMode
        SELF_ASSESSMENT = 1
        APPRAISER_ASSESSMENT = 2
        REVIEWER_ASSESSMENT = 3
        ALL_ASSESSMENT = 4
        'S.SANDEEP |16-AUG-2019| -- START
        'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
        SUBMIT_CALIBRATION = 5
        'S.SANDEEP |16-AUG-2019| -- END
    End Enum

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public Enum enScoreMode
        PROVISIONAL_SCORE = 1
        CALIBRATED_SCORE = 2
    End Enum
    'S.SANDEEP |27-MAY-2019| -- END
#End Region

#Region " Property Variables "
    Private mintComputescoremasterunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintAnalysisunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintAssessmodeid As Integer
    Private mintAssessormasterunkid As Integer
    Private mintScoretypeid As Integer
    Private mintUserunkid As Integer
    Private mdtComputationdate As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private msinFinaloverallscore As Decimal
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Computescoremasterunkid() As Integer
        Get
            Return mintComputescoremasterunkid
        End Get
        Set(ByVal value As Integer)
            mintComputescoremasterunkid = value
            'Call getData()
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Analysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
        End Set
    End Property

    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _Assessmodeid() As Integer
        Get
            Return mintAssessmodeid
        End Get
        Set(ByVal value As Integer)
            mintAssessmodeid = value
        End Set
    End Property

    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    Public Property _Scoretypeid() As Integer
        Get
            Return mintScoretypeid
        End Get
        Set(ByVal value As Integer)
            mintScoretypeid = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Computationdate() As Date
        Get
            Return mdtComputationdate
        End Get
        Set(ByVal value As Date)
            mdtComputationdate = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Finaloverallscore() As Decimal
        Get
            Return msinFinaloverallscore
        End Get
        Set(ByVal value As Decimal)
            msinFinaloverallscore = value
        End Set
    End Property

#End Region

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal xPeriodUnkId As Integer, _
                            ByVal xIncludeUncommitedEmp As Boolean, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal blnIsForCalibrationSubmit As Boolean = False, _
                            Optional ByVal blnIsReviewerNeeded As Boolean = True) As DataSet
        'S.SANDEEP |16-AUG-2019| -- START {blnIsForCalibrationSubmit,blnIsReviewerNeeded} -- END

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        Try
            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ &= "SELECT DISTINCT " & _
                        "    hremployee_master.employeecode " & _
                        "   ,ISNULL(hremployee_master.firstname,'') + ' ' +  ISNULL(hremployee_master.surname,'') AS employeename " & _
                        "   ,ISNULL(ETBL.EColor,CAST(0 AS BIT)) AS E_IsProcess " & _
                        "   ,ISNULL(ATBL.AColor,CAST(0 AS BIT)) AS A_IsProcess " & _
                        "   ,ISNULL(RTBL.RColor,CAST(0 AS BIT)) AS R_IsProcess " & _
                        "   ,ISNULL(ETBL.computescoremasterunkid,0) AS E_MstID " & _
                        "   ,ISNULL(ATBL.computescoremasterunkid,0) AS A_MstID " & _
                        "   ,ISNULL(RTBL.computescoremasterunkid,0) AS R_MstID " & _
                        "   ,CAST(0 AS BIT) AS ischeck " & _
                        "   ,hremployee_master.employeeunkid AS employeeunkid " & _
                        "   ,ISNULL(EAEMP.iscommitted,CAST(0 AS BIT)) AS E_Commited " & _
                        "   ,ISNULL(EAEMP.analysisunkid,CAST(0 AS BIT)) AS E_Analysisid " & _
                        "   ,ISNULL(AAEMP.iscommitted,CAST(0 AS BIT)) AS A_Commited " & _
                        "   ,ISNULL(AAEMP.analysisunkid,CAST(0 AS BIT)) AS A_Analysisid " & _
                        "   ,ISNULL(RAEMP.iscommitted,CAST(0 AS BIT)) AS R_Commited " & _
                        "   ,ISNULL(RAEMP.analysisunkid,CAST(0 AS BIT)) AS R_Analysisid "
                If blnIsForCalibrationSubmit Then
                    StrQ &= "   ,ISNULL(AP.UName,'') AS UName " & _
                            "   ,ISNULL(AP.UEmail,'') AS UEmail " & _
                            "   ,ISNULL(AP.UId,0) AS UId " & _
                            "   ,CASE WHEN ISNULL(AP.UId,0) <= 0 THEN 1 ELSE CASE WHEN ISNULL(CB.employeeunkid,0) > 0 THEN 1 ELSE 0 END END AS iRead " & _
                            "   ,CASE WHEN ISNULL(AP.UName,'') = '' THEN @CNA ELSE CASE WHEN ISNULL(CB.employeeunkid,0) > 0 THEN @UC ELSE '' END END AS iMsg "
                Else
                    StrQ &= "   ,CASE WHEN ISNULL(CB.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS iRead " & _
                            "   ,CASE WHEN ISNULL(CB.employeeunkid,0) > 0 THEN @UC ELSE '' END AS iMsg "
                End If
                StrQ &= "FROM hremployee_master " & _
                        "   JOIN " & _
                        "   ( " & _
                        "       SELECT DISTINCT " & _
                        "           CASE WHEN hrevaluation_analysis_master.selfemployeeunkid <= 0 THEN hrevaluation_analysis_master.assessedemployeeunkid ELSE hrevaluation_analysis_master.selfemployeeunkid END AS employeeids " & _
                        "       FROM hrevaluation_analysis_master " & _
                        "       where hrevaluation_analysis_master.isvoid = 0 and hrevaluation_analysis_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS A ON A.employeeids = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT DISTINCT " & _
                        "           employeeunkid " & _
                        "       FROM hrassess_computescore_approval_tran " & _
                        "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS CB ON CB.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            1 AS EColor " & _
                        "           ,hrassess_compute_score_master.assessmodeid " & _
                        "           ,hrassess_compute_score_master.employeeunkid " & _
                        "           ,hrassess_compute_score_master.computescoremasterunkid " & _
                        "       FROM hrassess_compute_score_master " & _
                        "       where hrassess_compute_score_master.isvoid = 0 and hrassess_compute_score_master.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " " & _
                        "           AND hrassess_compute_score_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS ETBL ON ETBL.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            1 AS AColor " & _
                        "           ,hrassess_compute_score_master.assessmodeid " & _
                        "           ,hrassess_compute_score_master.employeeunkid " & _
                        "           ,hrassess_compute_score_master.computescoremasterunkid " & _
                        "       FROM hrassess_compute_score_master " & _
                        "       where hrassess_compute_score_master.isvoid = 0 and hrassess_compute_score_master.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " " & _
                        "           AND hrassess_compute_score_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS ATBL ON ATBL.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            1 AS RColor " & _
                        "           ,hrassess_compute_score_master.assessmodeid " & _
                        "           ,hrassess_compute_score_master.employeeunkid " & _
                        "           ,hrassess_compute_score_master.computescoremasterunkid " & _
                        "       FROM hrassess_compute_score_master " & _
                        "       where hrassess_compute_score_master.isvoid = 0 and hrassess_compute_score_master.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " " & _
                        "           AND hrassess_compute_score_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS RTBL ON RTBL.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            hrevaluation_analysis_master.selfemployeeunkid AS employeeunkid " & _
                        "           ,hrevaluation_analysis_master.iscommitted " & _
                        "           ,hrevaluation_analysis_master.analysisunkid AS analysisunkid " & _
                        "       FROM hrevaluation_analysis_master " & _
                        "       WHERE hrevaluation_analysis_master.isvoid = 0 and hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " " & _
                        "           AND hrevaluation_analysis_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS EAEMP ON EAEMP.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            hrevaluation_analysis_master.assessedemployeeunkid AS employeeunkid " & _
                        "           ,hrevaluation_analysis_master.iscommitted " & _
                        "           ,hrevaluation_analysis_master.analysisunkid AS analysisunkid " & _
                        "           ,ROW_NUMBER()OVER(PARTITION BY hrevaluation_analysis_master.assessedemployeeunkid ORDER BY hrevaluation_analysis_master.committeddatetime DESC) AS Rno " & _
                        "       FROM hrevaluation_analysis_master " & _
                        "       WHERE hrevaluation_analysis_master.isvoid = 0 and hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " " & _
                        "           AND hrevaluation_analysis_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS AAEMP ON AAEMP.employeeunkid = hremployee_master.employeeunkid AND AAEMP.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            hrevaluation_analysis_master.assessedemployeeunkid AS employeeunkid " & _
                        "           ,hrevaluation_analysis_master.iscommitted " & _
                        "           ,hrevaluation_analysis_master.analysisunkid AS analysisunkid " & _
                        "       FROM hrevaluation_analysis_master " & _
                        "       WHERE hrevaluation_analysis_master.isvoid = 0 and hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " " & _
                        "           AND hrevaluation_analysis_master.periodunkid = '" & xPeriodUnkId & "' " & _
                        "   ) AS RAEMP ON RAEMP.employeeunkid = hremployee_master.employeeunkid "

                'S.SANDEEP |16-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                If blnIsForCalibrationSubmit Then
                    Dim intMatchValue As Integer = 0
                    If blnIsReviewerNeeded Then
                        intMatchValue = 3
                    Else
                        intMatchValue = 2
                    End If
                    'S.SANDEEP |22-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : Calibration Issues
                    'StrQ &= "   JOIN " & _
                    '        "   ( " & _
                    '        "       SELECT " & _
                    '        "            employeeunkid " & _
                    '        "           ,COUNT(1) AS iAll " & _
                    '        "       FROM hrassess_compute_score_master " & _
                    '        "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' AND calibratnounkid <= 0 " & _
                    '        "       GROUP BY employeeunkid HAVING COUNT(1) = " & intMatchValue & " " & _
                    '        "   ) AS D ON D.employeeunkid = hremployee_master.employeeunkid "
                    'StrQ &= " JOIN " & _
                    '        "   ( " & _
                    '        "       SELECT " & _
                    '        "            employeeunkid " & _
                    '        "           ,COUNT(1) AS iAll " & _
                    '        "       FROM hrassess_compute_score_master " & _
                    '        "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' AND calibratnounkid <= 0 AND isnotified = 0 " & _
                    '        "       AND finaloverallscore > 0 " & _
                    '        "       GROUP BY employeeunkid HAVING COUNT(1) = " & intMatchValue & " " & _
                    '        "   ) AS D ON D.employeeunkid = hremployee_master.employeeunkid "


                    StrQ &= " JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            employeeunkid " & _
                            "       FROM hrassess_compute_score_master " & _
                            "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' AND calibratnounkid <= 0 AND isnotified = 0 " & _
                            "       AND finaloverallscore > 0 AND assessmodeid = 1 " & _
                            "       INTERSECT " & _
                            "       SELECT " & _
                            "            employeeunkid " & _
                            "       FROM hrassess_compute_score_master " & _
                            "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' AND calibratnounkid <= 0 AND isnotified = 0 " & _
                            "       AND finaloverallscore > 0 AND assessmodeid = 2 "
                    If blnIsReviewerNeeded Then
                        StrQ &= "       INTERSECT " & _
                                "       SELECT " & _
                                "            employeeunkid " & _
                                "       FROM hrassess_compute_score_master " & _
                                "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' AND calibratnounkid <= 0 AND isnotified = 0 " & _
                                "       AND finaloverallscore > 0 AND assessmodeid = 3 "
                    End If
                    StrQ &= "   ) AS D ON D.employeeunkid = hremployee_master.employeeunkid "

                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        CT.employeeunkid " & _
                            "       ,CM.mapuserunkid " & _
                            "       ,CASE WHEN ISNULL(UM.firstname,'')+' '+ISNULL(UM.lastname,'') = '' THEN UM.username ELSE ISNULL(UM.firstname,'')+' '+ISNULL(UM.lastname,'') END AS UName " & _
                            "       ,ISNULL(UM.email,'') AS UEmail " & _
                            "       ,ISNULL(UM.userunkid, 0) AS UId " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid,CM.mapuserunkid ORDER BY CM.mappingunkid) AS rno " & _
                            "   FROM hrscore_calibration_approver_master AS CM " & _
                            "       JOIN hrscore_calibration_approver_tran AS CT ON CM.mappingunkid = CT.mappingunkid " & _
                            "       JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = CM.mapuserunkid " & _
                            "       JOIN hrmsConfiguration..cfuser_privilege AS UP ON UP.userunkid = UM.userunkid " & _
                            "   WHERE CM.isvoid = 0 AND CM.isactive = 1 AND CM.iscalibrator = 1 AND CT.isvoid = 0 " & _
                            "   AND CM.visibletypeid = 1 AND CT.visibletypeid = 1 AND UM.isactive = 1 AND UP.privilegeunkid = '" & CInt(enUserPriviledge.AllowtoCalibrateProvisionalScore) & "' " & _
                            "   AND ISNULL(UM.email,'') <> '' " & _
                            ") AS AP ON D.employeeunkid = AP.employeeunkid AND AP.rno = 1 "
                    'S.SANDEEP |22-NOV-2019| -- END
                End If
                    'S.SANDEEP |16-AUG-2019| -- END

                'S.SANDEEP |10-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                ''''''''''''''''''''''''''''' ------------------------------------ ADDED
                '"   LEFT JOIN " & _
                '        "   ( " & _
                '        "       SELECT DISTINCT " & _
                '        "           employeeunkid " & _
                '        "       FROM hrassess_computescore_approval_tran " & _
                '        "       WHERE isvoid = 0 AND periodunkid = '" & xPeriodUnkId & "' " & _
                '        "   ) AS CB ON CB.employeeunkid = hremployee_master.employeeunkid " & _
                '"   ,CASE WHEN ISNULL(CB.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS iRead " & _
                '"   ,CASE WHEN ISNULL(CB.employeeunkid,0) > 0 THEN @UC ELSE '' END AS iMsg " & _
                'S.SANDEEP |10-OCT-2019| -- END


                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " WHERE 1=1 "

                If mstrFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrFilter
                End If

                'S.SANDEEP |10-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                objDo.AddParameter("@UC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 200, "Under Calibration"))
                objDo.AddParameter("@CNA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 600, "Calibrator Not Assigned"))
                'S.SANDEEP |10-OCT-2019| -- END


                'StrQ &= " ORDER BY hremployee_master.firstname,hremployee_master.surname "
                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    'Public Function GetAssessMode(ByVal strTableName As String, ByVal blnFlag As Boolean, ByVal blnCompanyNeedRevewer As Boolean) As DataSet
    Public Function GetAssessMode(ByVal strTableName As String, ByVal blnFlag As Boolean, ByVal blnCompanyNeedRevewer As Boolean, ByVal blnIsCalibrationActive As Boolean) As DataSet
        'S.SANDEEP |16-AUG-2019| -- END
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation

                If blnFlag Then
                    StrQ = "SELECT 0 AS id,@select AS name UNION "
                    objDo.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
                End If

                StrQ &= "       SELECT " & enAssessMode.ALL_ASSESSMENT & " AS id , @ALL_ASSESSMENT AS Name " & _
                        " UNION SELECT " & enAssessMode.SELF_ASSESSMENT & " AS id , @SelfAssesment AS Name " & _
                        " UNION SELECT " & enAssessMode.APPRAISER_ASSESSMENT & " AS id , @AssessorAssesment AS Name "

                If blnCompanyNeedRevewer Then
                    StrQ &= " UNION SELECT " & enAssessMode.REVIEWER_ASSESSMENT & " AS id , @ReviewerAssesment AS Name "
                    objDo.AddParameter("@ReviewerAssesment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Reviewer Assessmemt"))
                End If

                'S.SANDEEP |16-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                If blnIsCalibrationActive Then
                    StrQ &= " UNION SELECT " & enAssessMode.SUBMIT_CALIBRATION & " AS id , @SUBMIT_CALIBRATION AS Name "
                    objDo.AddParameter("@SUBMIT_CALIBRATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Submit for Calibration"))
                End If
                'S.SANDEEP |16-AUG-2019| -- END

                objDo.AddParameter("@ALL_ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "All Assessment"))
                objDo.AddParameter("@SelfAssesment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Self Assessment"))
                objDo.AddParameter("@AssessorAssesment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Assessor Assessment"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessMode; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Shani (23-Nov-2016) -- Start
    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
    'Public Function GetAssessmentScore(ByVal xPeriodUnkId As Integer, ByVal strEmpIds As String, Optional ByVal strTableName As String = "List") As DataSet
    Public Function GetAssessmentScore(ByVal xPeriodUnkId As Integer, ByVal strEmpIds As String, ByVal blnIsUsedAgreedScore As Boolean, Optional ByVal strTableName As String = "List") As DataSet
        'Shani (23-Nov123-2016-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try

            Using objDo As New clsDataOperation

                'S.SANDEEP [22-MAR-2017] -- START
                'ISSUE/ENHANCEMENT :  PA Computation Process Data Issue
                Dim xLinkedFieldId, xExOrder As Integer
                xLinkedFieldId = 0 : xExOrder = 0

                Dim xTableName, xColName, xParentCol, xParentTabName As String
                xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

                Dim objFMapping As New clsAssess_Field_Mapping
                xLinkedFieldId = objFMapping.Get_Map_FieldId(xPeriodUnkId)
                objFMapping = Nothing

                Dim objFMaster As New clsAssess_Field_Master
                xExOrder = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
                objFMaster = Nothing
                If xExOrder <= 0 Then
                    Return Nothing
                End If

                Select Case xExOrder
                    Case enWeight_Types.WEIGHT_FIELD1
                        xTableName = "hrassess_empfield1_master"
                        xColName = "empfield1unkid"
                        xParentCol = "empfield1unkid"
                        xParentTabName = "hrassess_empfield1_master"
                    Case enWeight_Types.WEIGHT_FIELD2
                        xTableName = "hrassess_empfield2_master"
                        xColName = "empfield2unkid"
                        xParentCol = "empfield1unkid"
                        xParentTabName = "hrassess_empfield1_master"
                    Case enWeight_Types.WEIGHT_FIELD3
                        xTableName = "hrassess_empfield3_master"
                        xColName = "empfield3unkid"
                        xParentCol = "empfield2unkid"
                        xParentTabName = "hrassess_empfield2_master"
                    Case enWeight_Types.WEIGHT_FIELD4
                        xTableName = "hrassess_empfield4_master"
                        xColName = "empfield4unkid"
                        xParentCol = "empfield3unkid"
                        xParentTabName = "hrassess_empfield3_master"
                    Case enWeight_Types.WEIGHT_FIELD5
                        xTableName = "hrassess_empfield5_master"
                        xColName = "empfield5unkid"
                        xParentCol = "empfield4unkid"
                        xParentTabName = "hrassess_empfield4_master"
                End Select
                'S.SANDEEP [22-MAR-2017] -- END


                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : CHANGE THE SCORE PICKING METHOD
                'StrQ &= "SELECT " & _
                '       "     A.iPeriodId " & _
                '       "    ,A.iItemUnkid " & _
                '       "    ,A.iEmployeeId AS employeeunkid " & _
                '       "    ,A.iScore " & _
                '       "    ,A.assessgroupunkid " & _
                '       "    ,A.assessmodeid " & _
                '       "    ,A.iweight " & _
                '       "    ,A.max_scale " & _
                '       "    ,A.assessormasterunkid " & _
                '       "FROM " & _
                '       "( " & _
                '       "    SELECT " & _
                '       "         EM.periodunkid AS iPeriodId " & _
                '       "        ,CT.competenciesunkid AS iItemUnkid " & _
                '        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END AS iEmployeeId " & _
                '        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnIsUsedAgreedScore, "CT.agreedscore", "CT.result") & " ELSE CT.result END AS iScore " & _
                '       "        ,CT.assessgroupunkid " & _
                '       "        ,EM.assessmodeid " & _
                '       "        ,CT.item_weight AS iweight " & _
                '       "        ,CT.max_scale AS max_scale " & _
                '       "        ,EM.assessormasterunkid " & _
                '       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_master.visibletypeid END as mvisibletypeid " & _
                '       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_tran.visibletypeid END as tvisibletypeid " & _
                '       "    FROM hrcompetency_analysis_tran AS CT " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = CT.analysisunkid " & _
                '       "        LEFT JOIN hrassessor_master ON EM.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                '       "        LEFT JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_tran.employeeunkid = CASE WHEN EM.assessmodeid = 1 THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END " & _
                '       "    WHERE EM.isvoid = 0 AND CT.isvoid = 0 AND EM.periodunkid = '" & xPeriodUnkId & "' " & _
                '       "UNION ALL " & _
                '       "    SELECT " & _
                '       "         EM.periodunkid AS iPeriodId " & _
                '       "        ,CASE WHEN hrassess_field_mapping.fieldunkid = 1 THEN GT.empfield1unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 2 THEN GT.empfield2unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 3 THEN GT.empfield3unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 4 THEN GT.empfield4unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 5 THEN GT.empfield5unkid " & _
                '       "         END AS iItemUnkid " & _
                '        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END AS iEmployeeId " & _
                '        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END AS iScore " & _
                '       "        ,0 AS assessgroupunkid " & _
                '       "        ,EM.assessmodeid " & _
                '       "        ,GT.item_weight AS iweight " & _
                '       "        ,GT.max_scale AS max_scale " & _
                '       "        ,EM.assessormasterunkid " & _
                '       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_master.visibletypeid END as mvisibletypeid " & _
                '       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_tran.visibletypeid END as tvisibletypeid " & _
                '       "    FROM hrgoals_analysis_tran AS GT " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = GT.analysisunkid " & _
                '       "        LEFT JOIN hrassessor_master ON EM.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                '       "        LEFT JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_tran.employeeunkid = CASE WHEN EM.assessmodeid = 1 THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END " & _
                '       "        JOIN hrassess_field_mapping ON EM.periodunkid = hrassess_field_mapping.periodunkid " & _
                '       "        JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = CASE WHEN hrassess_field_mapping.fieldunkid = 1 THEN GT.empfield1unkid WHEN hrassess_field_mapping.fieldunkid = 2 THEN GT.empfield2unkid WHEN hrassess_field_mapping.fieldunkid = 3 THEN GT.empfield3unkid WHEN hrassess_field_mapping.fieldunkid = 4 THEN GT.empfield4unkid WHEN hrassess_field_mapping.fieldunkid = 5 THEN GT.empfield5unkid END AND " & xTableName & ".employeeunkid = CASE WHEN EM.assessmodeid = 1 THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END  " & _
                '       "    WHERE  EM.isvoid = 0 AND GT.isvoid = 0 AND hrassess_field_mapping.isactive = 1 AND EM.periodunkid = '" & xPeriodUnkId & "' AND GT.item_weight > 0 " & _
                '       ") AS A " & _
                '       "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.iEmployeeId "
                ''S.SANDEEP [22-MAR-2017] -- START
                ''ISSUE/ENHANCEMENT : PA Computation Process Data Issue

                'StrQ &= " WHERE 1 = 1 AND A.mvisibletypeid = 1 AND A.tvisibletypeid = 1 "
                'If strEmpIds.Trim.Length > 0 Then
                '    StrQ &= " AND A.iEmployeeId IN (" & strEmpIds & ") "
                'End If
                ''S.SANDEEP [22-MAR-2017] -- END



                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'StrQ &= "SELECT " & _
                '       "     A.iPeriodId " & _
                '       "    ,A.iItemUnkid " & _
                '       "    ,A.iEmployeeId AS employeeunkid " & _
                '       "    ,A.iScore " & _
                '       "    ,A.assessgroupunkid " & _
                '       "    ,A.assessmodeid " & _
                '       "    ,A.iweight " & _
                '       "    ,A.max_scale " & _
                '       "    ,A.assessormasterunkid " & _
                '       "FROM " & _
                '       "( " & _
                '       "    SELECT " & _
                '       "         EM.periodunkid AS iPeriodId " & _
                '       "        ,CT.competenciesunkid AS iItemUnkid " & _
                '       "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END AS iEmployeeId " & _
                '       "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnIsUsedAgreedScore, "CT.agreedscore", "CT.result") & " ELSE CT.result END AS iScore " & _
                '       "        ,CT.assessgroupunkid " & _
                '       "        ,EM.assessmodeid " & _
                '       "        ,CT.item_weight AS iweight " & _
                '       "        ,CT.max_scale AS max_scale " & _
                '       "        ,EM.assessormasterunkid " & _
                '       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_master.visibletypeid END as mvisibletypeid " & _
                '       "    FROM hrcompetency_analysis_tran AS CT " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = CT.analysisunkid " & _
                '       "        LEFT JOIN hrassessor_master ON EM.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                '       "    WHERE EM.isvoid = 0 AND CT.isvoid = 0 AND EM.periodunkid = '" & xPeriodUnkId & "' " & _
                '       "UNION ALL " & _
                '       "    SELECT " & _
                '       "         EM.periodunkid AS iPeriodId " & _
                '       "        ,CASE WHEN hrassess_field_mapping.fieldunkid = 1 THEN GT.empfield1unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 2 THEN GT.empfield2unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 3 THEN GT.empfield3unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 4 THEN GT.empfield4unkid " & _
                '       "              WHEN hrassess_field_mapping.fieldunkid = 5 THEN GT.empfield5unkid " & _
                '       "         END AS iItemUnkid " & _
                '       "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END AS iEmployeeId " & _
                '       "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END AS iScore " & _
                '       "        ,0 AS assessgroupunkid " & _
                '       "        ,EM.assessmodeid " & _
                '       "        ,GT.item_weight AS iweight " & _
                '       "        ,GT.max_scale AS max_scale " & _
                '       "        ,EM.assessormasterunkid " & _
                '       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_master.visibletypeid END as mvisibletypeid " & _
                '       "    FROM hrgoals_analysis_tran AS GT " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = GT.analysisunkid " & _
                '       "        LEFT JOIN hrassessor_master ON EM.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                '       "        JOIN hrassess_field_mapping ON EM.periodunkid = hrassess_field_mapping.periodunkid " & _
                '       "        JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = CASE WHEN hrassess_field_mapping.fieldunkid = 1 THEN GT.empfield1unkid WHEN hrassess_field_mapping.fieldunkid = 2 THEN GT.empfield2unkid WHEN hrassess_field_mapping.fieldunkid = 3 THEN GT.empfield3unkid WHEN hrassess_field_mapping.fieldunkid = 4 THEN GT.empfield4unkid WHEN hrassess_field_mapping.fieldunkid = 5 THEN GT.empfield5unkid END AND " & xTableName & ".employeeunkid = CASE WHEN EM.assessmodeid = 1 THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END  " & _
                '       "    WHERE  EM.isvoid = 0 AND GT.isvoid = 0 AND hrassess_field_mapping.isactive = 1 AND EM.periodunkid = '" & xPeriodUnkId & "' AND GT.item_weight > 0 " & _
                '       ") AS A " & _
                '       "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.iEmployeeId "

                StrQ &= "SELECT " & _
                       "     A.iPeriodId " & _
                       "    ,A.iItemUnkid " & _
                       "    ,A.iEmployeeId AS employeeunkid " & _
                       "    ,CAST(A.iScore AS DECIMAL(36,2)) AS iScore " & _
                       "    ,A.assessgroupunkid " & _
                       "    ,A.assessmodeid " & _
                       "    ,A.iweight " & _
                       "    ,A.max_scale " & _
                       "    ,A.assessormasterunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         EM.periodunkid AS iPeriodId " & _
                       "        ,CT.competenciesunkid AS iItemUnkid " & _
                        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END AS iEmployeeId " & _
                        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnIsUsedAgreedScore, "CT.agreedscore", "CT.result") & " ELSE CT.result END AS iScore " & _
                       "        ,CT.assessgroupunkid " & _
                       "        ,EM.assessmodeid " & _
                       "        ,CT.item_weight AS iweight " & _
                       "        ,CT.max_scale AS max_scale " & _
                       "        ,EM.assessormasterunkid " & _
                       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_master.visibletypeid END as mvisibletypeid " & _
                       "    FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                       "        JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                       "        LEFT JOIN hrassessor_master WITH (NOLOCK) ON EM.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                       "    WHERE EM.isvoid = 0 AND CT.isvoid = 0 AND EM.periodunkid = '" & xPeriodUnkId & "' " & _
                       "UNION ALL " & _
                       "    SELECT " & _
                       "         EM.periodunkid AS iPeriodId " & _
                       "        ,CASE WHEN hrassess_field_mapping.fieldunkid = 1 THEN GT.empfield1unkid " & _
                       "              WHEN hrassess_field_mapping.fieldunkid = 2 THEN GT.empfield2unkid " & _
                       "              WHEN hrassess_field_mapping.fieldunkid = 3 THEN GT.empfield3unkid " & _
                       "              WHEN hrassess_field_mapping.fieldunkid = 4 THEN GT.empfield4unkid " & _
                       "              WHEN hrassess_field_mapping.fieldunkid = 5 THEN GT.empfield5unkid " & _
                       "         END AS iItemUnkid " & _
                        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END AS iEmployeeId " & _
                        "        ,CASE WHEN EM.assessmodeid = " & enAssessMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END AS iScore " & _
                       "        ,0 AS assessgroupunkid " & _
                       "        ,EM.assessmodeid " & _
                       "        ,GT.item_weight AS iweight " & _
                       "        ,GT.max_scale AS max_scale " & _
                       "        ,EM.assessormasterunkid " & _
                       "        ,CASE WHEN EM.assessmodeid = 1 THEN 1 ELSE hrassessor_master.visibletypeid END as mvisibletypeid " & _
                       "    FROM hrgoals_analysis_tran AS GT WITH (NOLOCK) " & _
                       "        JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                       "        LEFT JOIN hrassessor_master WITH (NOLOCK) ON EM.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                       "        JOIN hrassess_field_mapping WITH (NOLOCK) ON EM.periodunkid = hrassess_field_mapping.periodunkid " & _
                       "        JOIN " & xTableName & " WITH (NOLOCK) ON " & xTableName & "." & xColName & " = CASE WHEN hrassess_field_mapping.fieldunkid = 1 THEN GT.empfield1unkid WHEN hrassess_field_mapping.fieldunkid = 2 THEN GT.empfield2unkid WHEN hrassess_field_mapping.fieldunkid = 3 THEN GT.empfield3unkid WHEN hrassess_field_mapping.fieldunkid = 4 THEN GT.empfield4unkid WHEN hrassess_field_mapping.fieldunkid = 5 THEN GT.empfield5unkid END AND " & xTableName & ".employeeunkid = CASE WHEN EM.assessmodeid = 1 THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END  " & _
                       "    WHERE  EM.isvoid = 0 AND GT.isvoid = 0 AND hrassess_field_mapping.isactive = 1 AND EM.periodunkid = '" & xPeriodUnkId & "' AND GT.item_weight > 0 " & _
                       ") AS A " & _
                       "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.iEmployeeId "
                'S.SANDEEP |21-AUG-2019| -- END

                StrQ &= " WHERE 1 = 1 AND A.mvisibletypeid = 1 "

                If strEmpIds.Trim.Length > 0 Then
                    StrQ &= " AND A.iEmployeeId IN (" & strEmpIds & ") "
                End If
                'S.SANDEEP [04-AUG-2017] -- END





                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComputeScore; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function GetTranData(ByVal intEmpId As Integer, ByVal intPeriodId As Integer, ByVal xAssessMode As enAssessMode, ByRef strMessage As String, Optional ByVal strTableName As String = "List", Optional ByVal xDataOper As clsDataOperation = Nothing) As DataTable 'S.SANDEEP [27-APR-2017] -- START {xDataOper} -- END
        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = ""
        strMessage = ""
        Try
            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'Using objDo As New clsDataOperation
            If xDataOper IsNot Nothing Then
                objDataOperation = xDataOper
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'hrassess_compute_score_tran.competency_value ----- Added
            'S.SANDEEP [27-APR-2017] -- END
            StrQ = "SELECT " & _
                    "    hrassess_compute_score_tran.computescoretranunkid " & _
                    "    ,hrassess_compute_score_tran.computescoremasterunkid " & _
                    "    ,hrassess_compute_score_tran.computationunkid " & _
                    "    ,CAST(hrassess_compute_score_tran.formula_value AS DECIMAL(36,2)) AS formula_value " & _
                    "    ,hrassess_compute_score_tran.isvoid " & _
                    "    ,hrassess_compute_score_tran.voiduserunkid " & _
                    "    ,hrassess_compute_score_tran.voiddatetime " & _
                    "    ,hrassess_compute_score_tran.voidreason " & _
                    "    ,ISNULL(hrassess_compute_score_tran.competency_value,'') AS competency_value " & _
                    "    ,'' AS AUD " & _
                    "from hrassess_compute_score_tran WITH (NOLOCK) " & _
                    "   LEFT JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid = hrassess_compute_score_tran.computescoremasterunkid " & _
                    "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 " & _
                    "   AND hrassess_compute_score_master.employeeunkid = '" & intEmpId & "' " & _
                    "   AND hrassess_compute_score_master.periodunkid  = '" & intPeriodId & "' "
            If xAssessMode <> enAssessMode.ALL_ASSESSMENT Then
                StrQ &= "   AND hrassess_compute_score_master.assessmodeid = '" & xAssessMode & "' "
            End If

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            '------------------------- REMOVED
            'hrassess_compute_score_tran.formula_value
            '------------------------- ADDED
            'CAST(hrassess_compute_score_tran.formula_value AS DECIMAL(36,2)) AS formula_value
            'S.SANDEEP |21-AUG-2019| -- END


            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'dsList = objDo.ExecQuery(StrQ, "Compute")

            'If objDo.ErrorMessage <> "" Then
            '    dsList = Nothing
            '    strMessage = objDo.ErrorMessage
            'End If
            'If dsList IsNot Nothing Then
            '    dtTable = dsList.Tables("Compute")
            'End If
            'End Using

            dsList = objDataOperation.ExecQuery(StrQ, "Compute")

            If objDataOperation.ErrorMessage <> "" Then
                dsList = Nothing
                strMessage = objDataOperation.ErrorMessage
            End If
            If dsList IsNot Nothing Then
                dtTable = dsList.Tables("Compute")
            End If
            'S.SANDEEP [27-APR-2017] -- END



        Catch ex As Exception
            strMessage = ex.Message & "; Procedure Name: GetTranData; Module Name: "
        End Try
        Return dtTable
    End Function


    Public Function Insert(ByVal dtTran As DataTable, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [27-APR-2017] -- START {xDataOper} -- END

        If isExist(mintEmployeeunkid, mintPeriodunkid, mintAssessmodeid, mintAssessormasterunkid, xDataOper) Then 'S.SANDEEP [27-APR-2017] -- START {xDataOper} -- END
            mstrMessage = Language.getMessage(mstrModuleName, 6, "Compute score process already done")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [27-APR-2017] -- START
        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27-APR-2017] -- END
        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@scoretypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScoretypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@computationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtComputationdate.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else

                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToShortDateString)
            End If


            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@finaloverallscore", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinFinaloverallscore.ToString)

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            objDataOperation.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, 0)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(0, Boolean))
            objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            objDataOperation.AddParameter("@calibration_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
            objDataOperation.AddParameter("@submit_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@overall_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(0, Boolean))
            'S.SANDEEP |27-MAY-2019| -- END

            strQ = "INSERT INTO hrassess_compute_score_master ( " & _
              "  employeeunkid " & _
              ", analysisunkid " & _
              ", periodunkid " & _
              ", assessmodeid " & _
              ", assessormasterunkid " & _
              ", scoretypeid " & _
              ", userunkid " & _
              ", computationdate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", finaloverallscore" & _
              ", calibrated_score " & _
              ", issubmitted " & _
              ", calibratnounkid " & _
              ", calibration_remark " & _
              ", submit_date " & _
              ", overall_remark " & _
              ", isprocessed " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @analysisunkid " & _
              ", @periodunkid " & _
              ", @assessmodeid " & _
              ", @assessormasterunkid " & _
              ", @scoretypeid " & _
              ", @userunkid " & _
              ", @computationdate " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @finaloverallscore" & _
              ", @calibrated_score " & _
              ", @issubmitted " & _
              ", @calibratnounkid " & _
              ", @calibration_remark " & _
              ", @submit_date " & _
              ", @overall_remark " & _
              ", @isprocessed " & _
            "); SELECT @@identity"
            'S.SANDEEP |27-MAY-2019| -- START {calibrated_score,issubmitted,calibratnounkid,calibration_remark,submit_date,overall_remark,isprocessed} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            mintComputescoremasterunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_compute_score_master", "computescoremasterunkid", mintComputescoremasterunkid) = False Then
                If xDataOper Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

                mstrMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                Return False
            End If

            _FormName = mstrFormName
            _LoginEmployeeunkid = mintLoginEmployeeunkid
            _ClientIP = mstrClientIP
            _HostName = mstrHostName
            _FromWeb = mblnIsWeb
            _AuditUserId = mintAuditUserId
            _AuditDate = mdtAuditDate

            If dtTran IsNot Nothing AndAlso dtTran.Rows.Count > 0 Then
                If Insert_Update_Delete(objDataOperation, mintComputescoremasterunkid, dtTran) = False Then
                    mstrMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'objDataOperation.ReleaseTransaction(True)
            If xDataOper Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [27-APR-2017] -- END
            Return True
        Catch ex As Exception
            mstrMessage = ex.Message
            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'objDataOperation.ReleaseTransaction(False)
            If xDataOper Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [27-APR-2017] -- END
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'objDataOperation = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [27-APR-2017] -- END
        End Try

    End Function

    Public Function Update(ByVal dtTran As DataTable) As Boolean
        'If isExist(mstrName, mintComputescoremasterunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputescoremasterunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@scoretypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScoretypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@computationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtComputationdate.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@finaloverallscore", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinFinaloverallscore.ToString)

            strQ = "UPDATE hrassess_compute_score_master SET " & _
                   "      employeeunkid = @employeeunkid" & _
                   "    , analysisunkid = @analysisunkid" & _
                   "    , periodunkid = @periodunkid" & _
                   "    , assessmodeid = @assessmodeid" & _
                   "    , assessormasterunkid = @assessormasterunkid" & _
                   "    , scoretypeid = @scoretypeid" & _
                   "    , userunkid = @userunkid" & _
                   "    , computationdate = @computationdate" & _
                   "    , isvoid = @isvoid" & _
                   "    , voiduserunkid = @voiduserunkid" & _
                   "    , voiddatetime = @voiddatetime" & _
                   "    , voidreason = @voidreason" & _
                   "    , finaloverallscore = @finaloverallscore " & _
                   "WHERE computescoremasterunkid = @computescoremasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_compute_score_master", "computescoremasterunkid", mintComputescoremasterunkid) = False Then
                mstrMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            _FormName = mstrFormName
            _LoginEmployeeunkid = mintLoginEmployeeunkid
            _ClientIP = mstrClientIP
            _HostName = mstrHostName
            _FromWeb = mblnIsWeb
            _AuditUserId = mintAuditUserId
            _AuditDate = mdtAuditDate

            If dtTran IsNot Nothing AndAlso dtTran.Rows.Count > 0 Then
                If Insert_Update_Delete(objDataOperation, mintComputescoremasterunkid, dtTran) Then
                    mstrMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByRef strMessage As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""

        Try
            Using objDataOperation As New clsDataOperation
                objDataOperation.BindTransaction()

                StrQ = "UPDATE hrassess_compute_score_master SET " & _
                       "  isvoid = @isvoid " & _
                       " ,voiddatetime = @voiddatetime " & _
                       " ,voidreason = @voidreason " & _
                       " ,voiduserunkid = @voiduserunkid " & _
                       "WHERE computescoremasterunkid = @computescoremasterunkid "

                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    strMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                    Return False
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_compute_score_master", "computescoremasterunkid", intUnkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    strMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                    Return False
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

                objCommonATLog = New clsCommonATLog
                dsList = objCommonATLog.GetChildList(objDataOperation, "hrassess_compute_score_tran", "computescoremasterunkid", intUnkid)
                objCommonATLog = Nothing

                StrQ = "UPDATE hrassess_compute_score_tran SET " & _
                       "  isvoid = @isvoid " & _
                       " ,voiddatetime = @voiddatetime " & _
                       " ,voidreason = @voidreason " & _
                       " ,voiduserunkid = @voiduserunkid " & _
                       "WHERE computescoretranunkid = @computescoretranunkid "

                If dsList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                        objDataOperation.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("computescoretranunkid").ToString)

                        Call objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            strMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                            Return False
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_compute_score_master", "computescoremasterunkid", dRow.Item("computescoretranunkid")) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            strMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                            Return False
                        End If
                    Next
                End If

                        'S.SANDEEP |12-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : PA CHANGES
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

                'S.SANDEEP |15-JAN-2020| -- START {Instead of voiding whole batch only particular employee is voided} -- END
                'StrQ = "DECLARE @calibratnounkid INT " & _
                '       "SELECT @calibratnounkid = calibratnounkid FROM hrassess_compute_score_master WHERE computescoremasterunkid = @computescoremasterunkid " & _
                '       "IF @calibratnounkid IS NOT NULL " & _
                '       "BEGIN " & _
                '       "    UPDATE hrassess_computescore_approval_tran SET " & _
                '       "         isvoid = @isvoid " & _
                '       "        ,voidreason = @voidreason " & _
                '       "        ,audittype = 3 " & _
                '       "        ,audituserunkid = @voiduserunkid " & _
                '       "        ,auditdatetime = @voiddatetime " & _
                '       "        ,ip = '" & getIP() & "' " & _
                '       "        ,host = '" & getHostName() & "' " & _
                '       "        ,form_name = 'frmComputeScore' " & _
                '       "    WHERE calibratnounkid = @calibratnounkid " & _
                '       "END "
                StrQ = "DECLARE @calibratnounkid INT, @EmpId INT, @PrdId INT " & _
                               "SELECT @calibratnounkid = calibratnounkid FROM hrassess_compute_score_master WHERE computescoremasterunkid = @computescoremasterunkid " & _
                       "SELECT @EmpId = employeeunkid FROM hrassess_compute_score_master WHERE computescoremasterunkid = @computescoremasterunkid " & _
                       "SELECT @PrdId = periodunkid FROM hrassess_compute_score_master WHERE computescoremasterunkid = @computescoremasterunkid " & _
                               "IF @calibratnounkid IS NOT NULL " & _
                               "BEGIN " & _
                               "    UPDATE hrassess_computescore_approval_tran SET " & _
                               "         isvoid = @isvoid " & _
                               "        ,voidreason = @voidreason " & _
                               "        ,audittype = 3 " & _
                               "        ,audituserunkid = @voiduserunkid " & _
                               "        ,auditdatetime = @voiddatetime " & _
                               "        ,ip = '" & getIP() & "' " & _
                               "        ,host = '" & getHostName() & "' " & _
                               "        ,form_name = 'frmComputeScore' " & _
                               "    WHERE calibratnounkid = @calibratnounkid " & _
                       "    AND employeeunkid = @EmpId AND periodunkid = @PrdId " & _
                               "END "

                        Call objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            strMessage = objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage
                            Return False
                        End If
                'S.SANDEEP |12-JUL-2019| -- END

                objDataOperation.ReleaseTransaction(True)
                Return True
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function isExist(ByVal intEmpId As Integer, ByVal intPeriodId As Integer, ByVal xAssessMode As enAssessmentMode, Optional ByVal intAssessmstId As Integer = -1, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [27-APR-2017] -- START {xDataOper} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [27-APR-2017] -- START
        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27-APR-2017] -- END
        Try
            strQ = "SELECT " & _
              "  computescoremasterunkid " & _
              ", employeeunkid " & _
              ", analysisunkid " & _
              ", periodunkid " & _
              ", assessmodeid " & _
              ", assessormasterunkid " & _
              ", scoretypeid " & _
              ", userunkid " & _
              ", computationdate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", finaloverallscore " & _
             "FROM hrassess_compute_score_master WITH (NOLOCK) " & _
             "WHERE isvoid = 0 " & _
             "  AND employeeunkid = '" & intEmpId & "' " & _
             "  AND periodunkid = '" & intPeriodId & "' " & _
             "  AND assessmodeid = '" & xAssessMode & "' "
            If intAssessmstId > 0 Then
                strQ &= " AND assessormasterunkid = '" & intAssessmstId & "' "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            If xDataOper Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [27-APR-2017] -- END
        End Try
    End Function

    Public Function DeleteEmployeeWise(ByVal xAnalysisUnkid As Integer, ByVal xEmpid As Integer, ByVal xPeriodId As Integer, ByVal xAssessMode As enAssessmentMode) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0
        Try
            Using objDataOperation As New clsDataOperation
                objDataOperation.BindTransaction()

                StrQ = "SELECT " & _
                       "    computescoremasterunkid " & _
                       "FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' "
                If xAnalysisUnkid > 0 Then
                    StrQ &= "AND analysisunkid = '" & xAnalysisUnkid & "' "
                End If

                If xEmpid > 0 Then
                    StrQ &= "AND employeeunkid = '" & xEmpid & "' "
                End If

                Select Case xAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= "AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= "AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= "AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
                End Select

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count <= 0 Then
                    objDataOperation.ReleaseTransaction(True)
                    Return True
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    intUnkId = CInt(dsList.Tables(0).Rows(0)("computescoremasterunkid"))
                Else
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                StrQ = "UPDATE hrassess_compute_score_master SET " & _
                       "  isvoid = @isvoid " & _
                       " ,voiddatetime = @voiddatetime " & _
                       " ,voidreason = @voidreason " & _
                       " ,voiduserunkid = @voiduserunkid " & _
                       "WHERE computescoremasterunkid = @computescoremasterunkid "

                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId.ToString)

                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_compute_score_master", "computescoremasterunkid", intUnkId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

                objCommonATLog = New clsCommonATLog
                dsList = objCommonATLog.GetChildList(objDataOperation, "hrassess_compute_score_tran", "computescoremasterunkid", intUnkId)
                objCommonATLog = Nothing
                StrQ = "UPDATE hrassess_compute_score_tran SET " & _
                       "  isvoid = @isvoid " & _
                       " ,voiddatetime = @voiddatetime " & _
                       " ,voidreason = @voidreason " & _
                       " ,voiduserunkid = @voiduserunkid " & _
                       "WHERE computescoretranunkid = @computescoretranunkid "

                If dsList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                        objDataOperation.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("computescoretranunkid").ToString)

                        Call objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_compute_score_master", "computescoremasterunkid", dRow.Item("computescoretranunkid")) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END

                    Next
                End If

                'S.SANDEEP |12-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

                StrQ = "DECLARE @calibratnounkid INT " & _
                       "SELECT @calibratnounkid = calibratnounkid FROM hrassess_compute_score_master WITH (NOLOCK) WHERE computescoremasterunkid = @computescoremasterunkid " & _
                       "IF @calibratnounkid IS NOT NULL " & _
                       "BEGIN " & _
                       "    UPDATE hrassess_computescore_approval_tran SET " & _
                       "         isvoid = @isvoid " & _
                       "        ,voidreason = @voidreason " & _
                       "        ,audittype = 3 " & _
                       "        ,audituserunkid = @voiduserunkid " & _
                       "        ,auditdatetime = @voiddatetime " & _
                       "        ,ip = '" & getIP() & "' " & _
                       "        ,host = '" & getHostName() & "' " & _
                       "        ,form_name = 'frmComputeScore' " & _
                       "    WHERE calibratnounkid = @calibratnounkid " & _
                       "    AND employeeunkid = '" & xEmpid & "' AND periodunkid = '" & xPeriodId & "' " & _
                       "END " 'S.SANDEEP |15-JAN-2020| -- START {Instead of voiding whole batch only particular employee is voided} -- END
                'AND employeeunkid = '" & xEmpid & "' AND periodunkid = '" & xPeriodId & "' -- ADDED
                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP |12-JUL-2019| -- END

                objDataOperation.ReleaseTransaction(True)
                Return True
            End Using
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "DeleteEmployeeWise", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Function DeleteEmployeeWise(ByVal xAnalysisUnkid As Integer, ByVal xEmpid As Integer, ByVal xPeriodId As Integer, ByVal xAssessMode As enAssessmentMode, ByVal enFormulaType As enAssess_Computation_Formulas, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0
        Try
            StrQ = "SELECT " & _
                       "    computescoremasterunkid " & _
                       "FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' "
            If xAnalysisUnkid > 0 Then
                StrQ &= "AND analysisunkid = '" & xAnalysisUnkid & "' "
            End If

            If xEmpid > 0 Then
                StrQ &= "AND employeeunkid = '" & xEmpid & "' "
            End If

            Select Case xAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    StrQ &= "AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    StrQ &= "AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ &= "AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0)("computescoremasterunkid"))
            Else
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            StrQ = "SELECT " & _
                   "   computescoretranunkid " & _
                   "FROM hrassess_compute_score_tran WITH (NOLOCK) " & _
                   "   JOIN hrassess_computation_master WITH (NOLOCK) ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                   "WHERE computescoremasterunkid = @computescoremasterunkid AND formula_typeid = @formula_typeid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)
            objDataOperation.AddParameter("@formula_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enFormulaType)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "UPDATE hrassess_compute_score_tran SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   "WHERE computescoretranunkid = @computescoretranunkid "

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("computescoretranunkid").ToString)

                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END
                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_compute_score_master", "computescoremasterunkid", dRow.Item("computescoretranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objCommonATLog = Nothing
                Next
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            StrQ = "DECLARE @calibratnounkid INT " & _
                   "SELECT @calibratnounkid = calibratnounkid FROM hrassess_compute_score_master WITH (NOLOCK) WHERE computescoremasterunkid = @computescoremasterunkid " & _
                   "IF @calibratnounkid IS NOT NULL " & _
                   "BEGIN " & _
                   "    UPDATE hrassess_computescore_approval_tran SET " & _
                   "         isvoid = @isvoid " & _
                   "        ,voidreason = @voidreason " & _
                   "        ,audittype = 3 " & _
                   "        ,audituserunkid = @voiduserunkid " & _
                   "        ,auditdatetime = @voiddatetime " & _
                   "        ,ip = '" & getIP() & "' " & _
                   "        ,host = '" & getHostName() & "' " & _
                   "        ,form_name = 'frmComputeScore' " & _
                   "    WHERE calibratnounkid = @calibratnounkid " & _
                   "    AND employeeunkid = '" & xEmpid & "' AND periodunkid = '" & xPeriodId & "' " & _
                   "END "
            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "DeleteEmployeeWise", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP |18-JAN-2020| -- END


    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public Function GetComputeScore(ByVal intEmpids As String, _
                                    ByVal intPeriodId As Integer, _
                                    ByVal xassessMode As enAssessMode, _
                                    ByVal IsCalibrationSettingActive As Boolean) As DataTable
        'S.SANDEEP |27-MAY-2019| -- END

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing
        Try
            Using objDo As New clsDataOperation
                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'StrQ = "SELECT " & _
                '       "     hrassess_compute_score_master.employeeunkid " & _
                '       "    ,hrassess_compute_score_master.finaloverallscore " & _
                '       "    ,hrassess_computation_master.formula_typeid " & _
                '       "    ,hrassess_compute_score_tran.formula_value " & _
                '       "    ,hrassess_compute_score_master.assessmodeid " & _
                '       "    ,hrassess_compute_score_tran.competency_value " & _
                '       "FROM hrassess_compute_score_tran " & _
                '       "    JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                '       "    JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                '       "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 "

                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(IsCalibrationSettingActive = True, 1, 0) & " " & _
                       "SELECT " & _
                       "     hrassess_compute_score_master.employeeunkid " & _
                       "    ,CAST(CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                       "          WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                       "          WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                       "     END AS DECIMAL(36,2)) AS finaloverallscore " & _
                       "    ,hrassess_computation_master.formula_typeid " & _
                       "    ,CAST(hrassess_compute_score_tran.formula_value AS DECIMAL(36,2)) AS formula_value " & _
                       "    ,hrassess_compute_score_master.assessmodeid " & _
                       "    ,hrassess_compute_score_tran.competency_value " & _
                       "FROM hrassess_compute_score_tran WITH (NOLOCK) " & _
                       "    JOIN hrassess_compute_score_master WITH (NOLOCK) ON hrassess_compute_score_master.computescoremasterunkid =  hrassess_compute_score_tran.computescoremasterunkid " & _
                       "    JOIN hrassess_computation_master WITH (NOLOCK) ON hrassess_computation_master.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                       "WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 "
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                '--------------------------- REMOVED
                '"    ,CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                '"          WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                '"          WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                '"     END AS finaloverallscore " & _

                'hrassess_compute_score_tran.formula_value

                '--------------------------- ADDED
                '"    ,CAST(CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                '"          WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                '"          WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                '"     END AS DECIMAL(36,2)) AS finaloverallscore " & _

                'CAST(hrassess_compute_score_tran.formula_value AS DECIMAL(36,2)) AS formula_value

                'S.SANDEEP |21-AUG-2019| -- END

                'S.SANDEEP |27-MAY-2019| -- END

                'S.SANDEEP [27-APR-2017] -- START {competency_value} -- END

                If intEmpids.Trim.Length > 0 Then
                    StrQ &= " AND hrassess_compute_score_master.employeeunkid IN (" & intEmpids & ") "
                End If

                If intPeriodId > 0 Then
                    StrQ &= "AND hrassess_computation_master.periodunkid = '" & intPeriodId & "' "
                End If

                Select Case xassessMode
                    Case enAssessMode.SELF_ASSESSMENT
                        StrQ &= " AND hrassess_compute_score_master.assessmodeid = '" & enAssessMode.SELF_ASSESSMENT & "' "
                    Case enAssessMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND hrassess_compute_score_master.assessmodeid = '" & enAssessMode.APPRAISER_ASSESSMENT & "' "
                    Case enAssessMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND hrassess_compute_score_master.assessmodeid = '" & enAssessMode.REVIEWER_ASSESSMENT & "' "
                End Select

                dsList = objDo.ExecQuery(StrQ, "Compute")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
                If dsList IsNot Nothing Then
                    dtTable = dsList.Tables("Compute")

                    'S.SANDEEP |24-DEC-2019| -- START
                    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                    StrQ = "SELECT TOP 1 " & _
                           "    @val = CAST(c.key_value AS DECIMAL(36,4)) " & _
                           "FROM cfcommon_period_tran cpt WITH (NOLOCK) " & _
                           "    JOIN hrmsConfiguration..cffinancial_year_tran cyt WITH (NOLOCK) ON cpt.yearunkid = cyt.yearunkid " & _
                           "    JOIN hrmsConfiguration..cfconfiguration c WITH (NOLOCK) ON cyt.companyunkid = c.companyunkid " & _
                           "WHERE cpt.modulerefid = 5 AND cpt.periodunkid = '" & intPeriodId & "' AND UPPER(c.[key_name]) = 'PASCORINGROUDINGFACTOR' "

                    Dim iFactor As Decimal = 0
                    objDo.ClearParameters()
                    objDo.AddParameter("@val", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iFactor, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If
                    Try
                        iFactor = objDo.GetParameterValue("@val")
                    Catch ex As Exception
                        iFactor = 0
                    End Try
                    dtTable.AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, iFactor))
                    'S.SANDEEP |24-DEC-2019| -- END
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessMode; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dtTable
    End Function

    'S.SANDEEP |24-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
    Private Function PerformingRounding(ByVal x As DataRow, ByVal dblFactor As Decimal) As Boolean
        Try
            If IsDBNull(x("finaloverallscore")) = False Then
                x("finaloverallscore") = Rounding.BRound(CDec(x("finaloverallscore")), dblFactor)
            Else
                x("finaloverallscore") = 0
            End If
            If IsDBNull(x("formula_value")) = False Then
                x("formula_value") = Rounding.BRound(CDec(x("formula_value")), dblFactor)
            Else
                x("formula_value") = 0
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformingRounding; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |24-DEC-2019| -- END


    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation, ByVal iComputeScoreMasterId As Integer, ByVal mdtTran As DataTable) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassess_compute_score_tran ( " & _
                                            "  computescoremasterunkid " & _
                                            ", computationunkid " & _
                                            ", formula_value " & _
                                            ", isvoid " & _
                                            ", voiddatetime " & _
                                            ", voiduserunkid " & _
                                            ", voidreason" & _
                                            ", competency_value " & _
                                       ") VALUES (" & _
                                            "  @computescoremasterunkid " & _
                                            ", @computationunkid " & _
                                            ", @formula_value " & _
                                            ", @isvoid " & _
                                            ", @voiddatetime " & _
                                            ", @voiduserunkid " & _
                                            ", @voidreason" & _
                                            ", @competency_value " & _
                                       "); SELECT @@identity " 'S.SANDEEP [27-APR-2017] -- START {competency_value} -- END
                                objData.ClearParameters()
                                objData.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iComputeScoreMasterId.ToString)
                                objData.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computationunkid").ToString)
                                objData.AddParameter("@formula_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("formula_value").ToString)
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'S.SANDEEP [27-APR-2017] -- START
                                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                                objData.AddParameter("@competency_value", SqlDbType.NVarChar, .Item("competency_value").ToString.Length, .Item("competency_value").ToString)
                                'S.SANDEEP [27-APR-2017] -- END

                                dsList = objData.ExecQuery(StrQ, "List")

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim iComputeScoreTranId As Integer = CInt(dsList.Tables(0).Rows(0)(0))

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objData, 1, "hrassess_compute_score_tran", "computescoretranunkid", iComputeScoreTranId) = False Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Case "U"
                                StrQ = "UPDATE hrassess_compute_score_tran SET " & _
                                       "     computescoremasterunkid = @computescoremasterunkid " & _
                                       "   , computationunkid = @computationunkid " & _
                                       "   , formula_value = @formula_value" & _
                                       "   , isvoid = @isvoid " & _
                                       "   , voiddatetime = @voiddatetime " & _
                                       "   , voiduserunkid = @voiduserunkid " & _
                                       "   , voidreason = @voidreason " & _
                                       "   , competency_value = @competency_value " & _
                                       " WHERE computescoretranunkid = @computescoretranunkid" 'S.SANDEEP [27-APR-2017] -- START {competency_value} -- END

                                objData.ClearParameters()
                                objData.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iComputeScoreMasterId.ToString)
                                objData.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computescoretranunkid").ToString)
                                objData.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computationunkid").ToString)
                                objData.AddParameter("@formula_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("formula_value").ToString)
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'S.SANDEEP [27-APR-2017] -- START
                                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                                objData.AddParameter("@competency_value", SqlDbType.NVarChar, .Item("competency_value").ToString.Length, .Item("competency_value").ToString)
                                'S.SANDEEP [27-APR-2017] -- END
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                dsList = objData.ExecQuery(StrQ, "List")

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [20-JUN-2018] -- START
                                'ISSUE/ENHANCEMENT : {Ref#244}
                                'Dim iComputeScoreTranId As Integer = CInt(dsList.Tables(0).Rows(0)(0))
                                Dim iComputeScoreTranId As Integer = CInt(.Item("computescoretranunkid"))
                                'S.SANDEEP [20-JUN-2018] -- END

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objData, 2, "hrassess_compute_score_tran", "computescoretranunkid", .Item("computescoretranunkid")) = False Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Case "D"

                                'Shani(05-JUL-2016) -- Start
                                'If clsCommonATLog.Insert_AtLog(objData, 3, "hrassess_compute_score_tran", "computescoretranunkid", .Item("computescoretranunkid")) = False Then
                                '    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                '    Throw exForce
                                'End If
                                'Shani(05-JUL-2016) -- End

                                StrQ = "UPDATE hrassess_compute_score_tran SET " & _
                                       " isvoid = @isvoid " & _
                                       ",voiddatetime = @voiddatetime " & _
                                       ",voiduserunkid = @voiduserunkid " & _
                                       ",voidreason = @voidreason " & _
                                       "WHERE computescoretranunkid = @computescoretranunkid "
                                objData.ClearParameters()
                                objData.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computescoretranunkid").ToString)
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objData, 3, "hrassess_compute_score_tran", "computescoretranunkid", .Item("computescoretranunkid")) = False Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Shani [16-Jan-2017] -- START
    'Issue : TRA Compute Process Take Time 
    Public Function Get_Formula_value(ByVal xEmpid As Integer, ByVal xPeriodid As Integer, ByVal xTypeid As Integer) As Decimal
        Dim decValue As Decimal = 0
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation
                objDo.ClearParameters()
                StrQ = "SELECT DISTINCT " & _
                       "    @frmvalue = CAST(CASE WHEN " & xTypeid & " = " & enAssess_Computation_Formulas.FINAL_RESULT_SCORE & " THEN hrassess_compute_score_master.finaloverallscore ELSE hrassess_compute_score_tran.formula_value END AS DECIMAL(36,6)) " & _
                   "FROM hrassess_compute_score_tran " & _
                   "    JOIN hrassess_compute_score_master ON hrassess_compute_score_master.computescoremasterunkid = hrassess_compute_score_tran.computescoremasterunkid " & _
                   "    JOIN hrassess_computation_master ON hrassess_compute_score_tran.computationunkid = hrassess_computation_master.computationunkid " & _
                   "        AND hrassess_compute_score_master.periodunkid = hrassess_computation_master.periodunkid " & _
                   "WHERE hrassess_compute_score_master.periodunkid = '" & xPeriodid & "' AND hrassess_compute_score_master.employeeunkid = '" & xEmpid & "' " & _
                   " AND hrassess_computation_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 "
                If xTypeid <> enAssess_Computation_Formulas.FINAL_RESULT_SCORE Then
                    StrQ &= " AND hrassess_computation_master.formula_typeid = '" & xTypeid & "' "
                End If

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                '--------------------- REMOVED
                '"    @frmvalue = CASE WHEN " & xTypeid & " = " & enAssess_Computation_Formulas.FINAL_RESULT_SCORE & " THEN hrassess_compute_score_master.finaloverallscore ELSE hrassess_compute_score_tran.formula_value END " & _

                '--------------------- ADDED
                '"    @frmvalue = CAST(CASE WHEN " & xTypeid & " = " & enAssess_Computation_Formulas.FINAL_RESULT_SCORE & " THEN hrassess_compute_score_master.finaloverallscore ELSE hrassess_compute_score_tran.formula_value END AS DECIMAL(36,6)) " & _
                'S.SANDEEP |21-AUG-2019| -- END


                'S.SANDEEP [17-May-2018] -- START
                'ISSUE/ENHANCEMENT : {VOID CONDITION WAS NOT THERE}
                '-> {ADDED} AND hrassess_computation_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0
                'S.SANDEEP [17-May-2018] -- END

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 40
                '@frmvalue = CASE WHEN " & xTypeid & " = " & enAssess_Computation_Formulas.FINAL_RESULT_SCORE & " THEN hrassess_compute_score_master.finaloverallscore ELSE hrassess_compute_score_tran.formula_value END  -- ADDED
                '@frmvalue = hrassess_compute_score_tran.formula_value -- REMOVED
                'S.SANDEEP [29-NOV-2017] -- END

                objDo.AddParameter("@frmvalue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, decValue, ParameterDirection.InputOutput)
                StrQ = objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                decValue = objDo.GetParameterValue("@frmvalue")
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Formula_value; Module Name: " & mstrModuleName)
        End Try
        'decValue = 0
        Return decValue
    End Function

    Public Function Update_Final_score(ByVal xEmpid As Integer, ByVal xPeriodId As Integer, ByVal xFinalValue As Decimal) As Boolean
        Dim StrQ As String = ""
        Try
            Using objDo As New clsDataOperation
                objDo.ClearParameters()
                StrQ = "UPDATE hrassess_compute_score_master SET " & _
                       "    finaloverallscore = @finaloverallscore " & _
                       "WHERE isvoid = 0 AND " & _
                       "    employeeunkid = @employeeunkid AND " & _
                       "    periodunkid = @periodunkid "
                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmpid)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@finaloverallscore", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xFinalValue)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    mstrMessage = objDo.ErrorNumber & ": " & objDo.ErrorMessage
                    objDo.ReleaseTransaction(False)
                    Return False
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update_Final_score; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function
    'Shani [16-Jan-2017] -- END


    'S.SANDEEP [27-APR-2017] -- START
    'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
    Public Function GetComputeMasterId(ByVal intEmpId As Integer, _
                                       ByVal intPeriodid As Integer, _
                                       ByVal eAssessMode As enAssessmentMode, _
                                       Optional ByVal iAnalysisId As Integer = 0, _
                                       Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim intComputescoremasterunkid As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT " & _
                   " @computescoretranunkid = hrassess_compute_score_master.computescoremasterunkid " & _
                   "FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                   "WHERE hrassess_compute_score_master.isvoid = 0 " & _
                   "  AND hrassess_compute_score_master.employeeunkid = @employeeunkid " & _
                   "  AND hrassess_compute_score_master.periodunkid  = @periodunkid " & _
                   "  AND hrassess_compute_score_master.assessmodeid  = @assessmodeid "

            If iAnalysisId > 0 Then
                StrQ &= " AND hrassess_compute_score_master.analysisunkid = @analysisunkid "
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAnalysisId)
            End If

            objDataOperation.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intComputescoremasterunkid, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAssessMode)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorNumber)
                Throw exForce
            End If

            intComputescoremasterunkid = objDataOperation.GetParameterValue("@computescoretranunkid")


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComputeMasterId; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intComputescoremasterunkid
    End Function
    'S.SANDEEP [27-APR-2017] -- END

    'S.SANDEEP [07-JUL-2017] -- START
    'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
    Public Function GetComputationUnkid(ByVal intFormulaTypeId As Integer, _
                                        ByVal intPeriodUnkid As Integer, _
                                        Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim intComputationunkid As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT " & _
                   "    @computationunkid = hrassess_computation_master.computationunkid " & _
                   "FROM hrassess_computation_master WITH (NOLOCK) " & _
                   "WHERE hrassess_computation_master.periodunkid = @periodunkid " & _
                   "AND hrassess_computation_master.formula_typeid = @formula_typeid " & _
                   "AND hrassess_computation_master.isvoid = 0 "

            objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intComputationunkid, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@formula_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormulaTypeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorNumber)
                Throw exForce
            End If

            intComputationunkid = objDataOperation.GetParameterValue("@computationunkid")


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComputationUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intComputationunkid
    End Function
    'S.SANDEEP [07-JUL-2017] -- END

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 40
    Public Function UpdateAvgFinalScore(ByVal xFormulaType As Integer, _
                                        ByVal intPeriodId As Integer, _
                                        ByVal intEmployeeId As Integer, _
                                        ByVal intComputeMstId As Integer, _
                                        ByVal xFormulaValue As Decimal) As Boolean
        Try
            Using objDo As New clsDataOperation
                Dim StrQ As String = String.Empty
                Dim strMasterId As String = String.Empty
                StrQ = "SELECT " & _
                       "  @MstrId = ISNULL(STUFF((SELECT ','+ CAST(CSM.computescoremasterunkid AS NVARCHAR(MAX)) " & _
                       "FROM hrassess_compute_score_master AS CSM " & _
                       "WHERE CSM.isvoid = 0 AND CSM.employeeunkid = @employeeunkid AND CSM.periodunkid = @periodunkid " & _
                       "FOR XML PATH('')),1,1,''),'') "

                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDo.AddParameter("@MstrId", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strMasterId, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                If IsDBNull(objDo.GetParameterValue("@MstrId")) = False Then
                    strMasterId = objDo.GetParameterValue("@MstrId")
                End If

                If strMasterId <> "" AndAlso strMasterId <> "0" Then
                    Dim StrQI, StrQU As String

                    StrQI = "" : StrQU = ""

                    StrQI = "INSERT INTO hrassess_compute_score_tran " & _
                            "(computescoremasterunkid,computationunkid,formula_value,isvoid,voiduserunkid,voiddatetime,voidreason,competency_value) " & _
                            "VALUES " & _
                            "(@computescoremasterunkid,@computationunkid,@formula_value,@isvoid,@voiduserunkid,@voiddatetime,@voidreason,@competency_value) "

                    StrQU = "UPDATE hrassess_compute_score_tran SET " & _
                            "    computescoremasterunkid = @computescoremasterunkid " & _
                            "   ,computationunkid = @computationunkid " & _
                            "   ,formula_value = formula_value " & _
                            "   ,competency_value = @competency_value " & _
                            "WHERE computescoretranunkid = @computescoretranunkid "

                    Dim value() As String = strMasterId.Split(",")
                    If value.Length > 0 Then
                        Dim intTransactionId As Integer = 0
                        For iVal As Integer = 0 To value.Length - 1
                            objDo.ClearParameters() : intTransactionId = 0

                            StrQ = "SELECT " & _
                                   "    @TranId = ISNULL(CST.computescoretranunkid,0) " & _
                                   "FROM hrassess_compute_score_master AS CSM " & _
                                   "   JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                                   "   JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                                   "WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.employeeunkid = @employeeunkid AND CSM.periodunkid = @periodunkid " & _
                                   "AND CM.formula_typeid = @formula_typeid AND CM.isvoid = 0 AND CSM.computescoremasterunkid = @MstrId "

                            objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                            objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                            objDo.AddParameter("@formula_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormulaType)
                            objDo.AddParameter("@TranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId, ParameterDirection.InputOutput)
                            objDo.AddParameter("@MstrId", SqlDbType.Int, eZeeDataType.INT_SIZE, value(iVal))

                            objDo.ExecNonQuery(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            If IsDBNull(objDo.GetParameterValue("@TranId")) = False Then
                                intTransactionId = objDo.GetParameterValue("@TranId")
                            End If

                            If intTransactionId > 0 Then
                                StrQ = StrQU
                            Else
                                StrQ = StrQI
                            End If

                            objDo.ClearParameters()

                            objDo.AddParameter("@computescoremasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, value(iVal))
                            objDo.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intComputeMstId)
                            objDo.AddParameter("@computescoretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId)
                            objDo.AddParameter("@formula_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, xFormulaValue)
                            objDo.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, "False")
                            objDo.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                            objDo.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            objDo.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                            objDo.AddParameter("@competency_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")


                            objDo.ExecNonQuery(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                        Next
                    End If
                End If


            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ComputeAvgFinalScore; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [29-NOV-2017] -- END

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public Function GetCalibrationNoUnkid(ByVal strCalibrationNo As String, Optional ByVal xDataOperation As clsDataOperation = Nothing) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As clsDataOperation = Nothing
        Dim intCalibrationUnkid As Integer = 0
        If xDataOperation IsNot Nothing Then
            objData = xDataOperation
        Else
            objData = New clsDataOperation
        End If
        objData.ClearParameters()
        Try
            StrQ = "SELECT calibratnounkid FROM hrassess_calibration_number WHERE calibration_no = @calibration_no "

            objData.AddParameter("@calibration_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCalibrationNo)

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intCalibrationUnkid = dsList.Tables("List").Rows(0)("calibratnounkid")
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCalibrationNoUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intCalibrationUnkid
    End Function

    Public Function IsCalibrationNoExist(ByVal strCalibrationNo As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As New clsDataOperation
        Dim blnFlag As Boolean = False
        Try
            StrQ = "SELECT calibratnounkid FROM hrassess_calibration_number WHERE calibration_no = @calibration_no "

            objData.AddParameter("@calibration_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCalibrationNo)

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsCalibrationNoExist; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function InsertCalibrationNumber(ByVal strCalibrationNumber As String, _
                                            ByVal intCompanyId As Integer, _
                                            Optional ByVal xDataOperation As clsDataOperation = Nothing) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As clsDataOperation = Nothing
        Dim intCalibrationUnkid As Integer = 0
        If xDataOperation IsNot Nothing Then
            objData = xDataOperation
        Else
            objData = New clsDataOperation
        End If
        objData.ClearParameters()
        Dim blnFlag As Boolean = True
        Try
            If strCalibrationNumber.Trim.Length <= 0 Then
                While (blnFlag = True)
                    StrQ = "SELECT " & _
                           "	 CASE WHEN ISNULL(key_value,'') = '' THEN VNO ELSE key_value + VNO END AS VocNo " & _
                           "	,VNO " & _
                           "	,key_value " & _
                           "FROM hrmsConfiguration..cfconfiguration " & _
                           "JOIN " & _
                           "( " & _
                           "	SELECT " & _
                           "		 key_value  AS VNO " & _
                           "		,companyunkid AS Cid " & _
                           "	FROM hrmsConfiguration..cfconfiguration " & _
                           "	WHERE companyunkid = '" & intCompanyId & "' AND key_name = 'NextScoreCalibrationFormNo' " & _
                           ")AS B ON B.Cid = hrmsConfiguration..cfconfiguration.companyunkid " & _
                           "WHERE companyunkid = '" & intCompanyId & "' AND key_name = 'ScoreCalibrationFormNoPrifix' "

                    dsList = objData.ExecQuery(StrQ, "List")

                    If objData.ErrorMessage <> "" Then
                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    End If

                    If dsList.Tables("List").Rows.Count > 0 Then
                        strCalibrationNumber = dsList.Tables("List").Rows(0)("VocNo")
                    End If

                    If IsCalibrationNoExist(strCalibrationNumber) Then
                        strCalibrationNumber = ""

                        StrQ = "UPDATE hrmsConfiguration..cfconfiguration SET " & _
                               "    key_value = CAST(key_value AS INT) + 1 " & _
                               "WHERE companyunkid = '" & intCompanyId & "' AND key_name = 'NextScoreCalibrationFormNo'"

                        objData.ExecNonQuery(StrQ)
                        If objData.ErrorMessage <> "" Then
                            Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                        End If
                    Else
                        blnFlag = False
                    End If
                    If blnFlag = False Then Exit While
                End While
            End If

            StrQ = "INSERT INTO hrassess_calibration_number (calibration_no) VALUES (@calibration_no); SELECT @@identity"

            objData.AddParameter("@calibration_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCalibrationNumber.ToString)

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            intCalibrationUnkid = dsList.Tables(0).Rows(0).Item(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertCalibrationNumber; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intCalibrationUnkid
    End Function

    Public Function Update_Calibration(ByVal dtCalibratTable As DataTable, _
                                       ByVal strDataBaseName As String, _
                                       ByVal intCompanyId As Integer, _
                                       ByVal intYearId As Integer, _
                                       ByVal strRemark As String, _
                                       ByVal eStatus As clsScoreCalibrationApproval.enCalibrationStatus, _
                                       ByVal dtAuditdatetime As DateTime, _
                                       ByVal eAuditType As enAuditType, _
                                       ByVal intUserId As Integer, _
                                       ByVal strIPAddress As String, _
                                       ByVal strHostName As String, _
                                       ByVal strFormName As String, _
                                       ByVal blnIsWeb As Boolean, _
                                       Optional ByVal strCalibrationNo As String = "", _
                                       Optional ByVal blnIsSubmitted As Boolean = False, _
                                       Optional ByRef strGeneratedCalibNumber As String = "", _
                                       Optional ByRef intCalibUnkid As Integer = 0) As Boolean 'S.SANDEEP |16-AUG-2019| -- START {strGeneratedCalibNumber} -- END
        'S.SANDEEP |05-SEP-2019| -- START {intCalibUnkid} -- END
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim StrVocNo As String = String.Empty
        Dim objData As New clsDataOperation
        Dim intCalibrationUnkid As Integer = 0
        Dim objScoreApproval As New clsScoreCalibrationApproval
        objData.BindTransaction()
        Try
            If strCalibrationNo.Trim.Length <= 0 Then
                intCalibrationUnkid = InsertCalibrationNumber(strCalibrationNo, intCompanyId, objData)
            Else
                intCalibrationUnkid = GetCalibrationNoUnkid(strCalibrationNo, objData)
                If intCalibrationUnkid <= 0 Then
                    intCalibrationUnkid = InsertCalibrationNumber(strCalibrationNo, intCompanyId, objData)
                End If
            End If

            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            strGeneratedCalibNumber = GetCalibrationNo(intCalibrationUnkid, objData)
            'S.SANDEEP |16-AUG-2019| -- END

            'S.SANDEEP |05-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
            intCalibUnkid = intCalibrationUnkid
            'S.SANDEEP |05-SEP-2019| -- END


            StrQ = "UPDATE hrassess_compute_score_master " & _
                   "SET calibrated_score = @calibrated_score " & _
                   " ,calibratnounkid = @calibratnounkid "
            If blnIsSubmitted Then
                StrQ &= " ,issubmitted = 1 " & _
                        " ,submit_date = (SELECT GETDATE()) "
            End If
            StrQ &= " ,calibration_remark = @calibration_remark " & _
                    " ,overall_remark = @overall_remark " & _
                    "WHERE employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

            For Each iRow As DataRow In dtCalibratTable.Rows

                If objScoreApproval.IsExists(CInt(iRow("employeeunkid")), CInt(iRow("periodunkid")), 0, objData) = False Then
                objData.ClearParameters()
                objData.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iRow("calibrated_score"))
                objData.AddParameter("@calibration_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, iRow("calibration_remark"))
                objData.AddParameter("@overall_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strRemark)
                objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iRow("periodunkid"))
                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iRow("employeeunkid"))
                objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                End If
                Else
                    StrQ = "UPDATE hrassess_compute_score_master " & _
                           "SET calibrated_score = @calibrated_score "
                    If blnIsSubmitted Then
                        StrQ &= " ,issubmitted = 1 " & _
                                " ,submit_date = (SELECT GETDATE()) "
                End If
                    StrQ &= " ,calibration_remark = @calibration_remark " & _
                            " ,overall_remark = @overall_remark " & _
                            "WHERE employeeunkid = @employeeunkid AND periodunkid = @periodunkid AND calibratnounkid = @calibratnounkid "


                    objData.ClearParameters()
                    objData.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iRow("calibrated_score"))
                    objData.AddParameter("@calibration_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, iRow("calibration_remark"))
                    objData.AddParameter("@overall_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strRemark)
                    objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iRow("periodunkid"))
                    objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iRow("employeeunkid"))
                    objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationUnkid)

                    objData.ExecNonQuery(StrQ)

                    If objData.ErrorMessage <> "" Then
                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    End If
                End If
                With objScoreApproval
                    ._Approvalremark = ""
                    ._Auditdatetime = dtAuditdatetime
                    ._Audittype = eAuditType
                    ._AuditUserunkid = intUserId
                    ._Calibrated_Score = iRow("calibrated_score")
                    ._CalibrationId = intCalibrationUnkid
                    ._Employeunkid = iRow("employeeunkid")
                    ._Form_Name = strFormName
                    ._Host = strHostName
                    ._Ip = strIPAddress
                    ._IsFinal = False
                    ._IsProcessed = False
                    ._Isvoid = False
                    ._Isweb = blnIsWeb
                    ._Loginemployeeunkid = 0
                    ._Mappingunkid = 0
                    ._Periodunkid = iRow("periodunkid")
                    ._Statusunkid = eStatus
                    ._Tranguid = Guid.NewGuid.ToString
                    ._TransactionDate = dtAuditdatetime
                    ._Voidreason = ""
                    ._Calibration_Remark = iRow("calibration_remark")
                    If .Insert(strDataBaseName, intUserId, intYearId, intCompanyId, objData) = False Then
                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    End If
                End With
            Next

            StrQ = "UPDATE hrmsConfiguration..cfconfiguration SET " & _
                   "    key_value = CAST(key_value AS INT) + 1 " & _
                   "WHERE companyunkid = '" & intCompanyId & "' AND key_name = 'NextScoreCalibrationFormNo'"

            objData.ExecNonQuery(StrQ)
            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            objData.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objData.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update_Calibration_Number; Module Name: " & mstrModuleName)
        Finally
            objData = Nothing
        End Try
    End Function

    Public Function GetDisplayScoreOption(ByVal strList As String, ByVal blnAddSelect As Boolean, Optional ByVal xDataOperation As clsDataOperation = Nothing) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As clsDataOperation = Nothing
        Dim intCalibrationUnkid As Integer = 0
        If xDataOperation IsNot Nothing Then
            objData = xDataOperation
        Else
            objData = New clsDataOperation
        End If
        objData.ClearParameters()
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= " SELECT " & enScoreMode.PROVISIONAL_SCORE & " AS Id, @PS AS Name " & _
                    "UNION SELECT " & enScoreMode.CALIBRATED_SCORE & " AS Id, @CS AS Name "

            objData.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
            objData.AddParameter("@PS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Provisional Score"))
            objData.AddParameter("@CS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Calibrated Score"))


            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDisplayScoreOption; Module Name: GetDisplayScoreOption" & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP |27-MAY-2019| -- END

    'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Public Function GetCalibrationNo(ByVal intCalibrationId As Integer, Optional ByVal xDataOperation As clsDataOperation = Nothing) As String
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As clsDataOperation = Nothing
        Dim strCalibrationNo As String = ""
        If xDataOperation IsNot Nothing Then
            objData = xDataOperation
        Else
            objData = New clsDataOperation
        End If
        objData.ClearParameters()
        Try
            StrQ = "SELECT calibration_no FROM hrassess_calibration_number WHERE calibratnounkid = @calibratnounkid "

            objData.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                strCalibrationNo = dsList.Tables("List").Rows(0)("calibration_no")
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCalibrationNo; Module Name: " & mstrModuleName)
        End Try
        Return strCalibrationNo
    End Function
    'S.SANDEEP |16-AUG-2019| -- END

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Public Function GetCalibrationNoList(ByVal intPeriodId As Integer, Optional ByVal strList As String = "List", Optional ByVal xDataOperation As clsDataOperation = Nothing) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As clsDataOperation = Nothing
        Dim strCalibrationNo As String = ""
        If xDataOperation IsNot Nothing Then
            objData = xDataOperation
        Else
            objData = New clsDataOperation
        End If
        objData.ClearParameters()
        Try
            StrQ = "SELECT DISTINCT " & _
                   "     CAST(0 AS BIT) AS icheck " & _
                   "    ,CBN.calibratnounkid " & _
                   "    ,CBN.calibration_no " & _
                   "FROM hrassess_calibration_number AS CBN " & _
                   "    JOIN hrassess_computescore_approval_tran AS CAT ON CAT.calibratnounkid  = CBN.calibratnounkid " & _
                   "WHERE CAT.periodunkid = @periodunkid AND CAT.isvoid = 0 "

            objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objData.ExecQuery(StrQ, strList)

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCalibrationNoList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'S.SANDEEP |25-OCT-2019| -- END

    'S.SANDEEP |01-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : Calibration Import
    Public Sub ImportCalibrationData(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal intCompanyId As Integer, _
                                     ByVal intCalibrationId As Integer, _
                                     ByVal strCalibrationNo As String, _
                                     ByVal intCalibratorUserId As Integer, _
                                     ByVal intEmployeeId As Integer, _
                                     ByVal intPeriodId As Integer, _
                                     ByVal decCalibrationScore As Decimal, _
                                     ByVal strCalibrationRemark As String, _
                                     ByVal blnWithSubmitStatus As Boolean, _
                                     ByVal strIPAddress As String, _
                                     ByVal strHostName As String, _
                                     ByVal strScreenName As String, _
                                     ByVal blnIsWeb As Boolean, _
                                     ByVal dtCurrentDate As DateTime, _
                                     ByRef xMessage As String, _
                                     ByRef intStatusId As Integer, _
                                     ByRef strGeneratedNo As String)

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsExists As New DataSet
        Dim objScrApprl As New clsScoreCalibrationApproval
        Try
            Using objDataOperation As New clsDataOperation
                If intCalibrationId <= 0 Then
                    If IsCalibrationNoExist(strCalibrationNo) = False Then
                        intCalibrationId = InsertCalibrationNumber(strCalibrationNo, intCompanyId, objDataOperation)
                    Else
                        intCalibrationId = GetCalibrationNoUnkid(strCalibrationNo, objDataOperation)
                    End If
                End If
                strGeneratedNo = GetCalibrationNo(intCalibrationId, objDataOperation)

                StrQ = "UPDATE hrassess_calibration_number SET calibuserunkid = '" & intCalibratorUserId & "' WHERE calibratnounkid = '" & intCalibrationId & "' "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                StrQ = "SELECT DISTINCT " & _
                       "    ISNULL(calibration_no,'') AS cno " & _
                       "FROM hrassess_compute_score_master " & _
                       "    JOIN hrassess_calibration_number ON hrassess_compute_score_master.calibratnounkid = hrassess_calibration_number.calibratnounkid " & _
                       "WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid AND isprocessed = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                dsExists = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If dsExists.Tables(0).Rows.Count > 0 Then
                    xMessage = Language.getMessage(mstrModuleName, 500, "Sorry, Employee is already under calibration process with calibration number:") & " " & dsExists.Tables(0).Rows(0)("cno") & ". "
                    intStatusId = 2
                    Exit Try
                End If

                StrQ = "SELECT DISTINCT " & _
                       "    hcat.audituserunkid " & _
                       "FROM hrassess_computescore_approval_tran hcat " & _
                       "WHERE hcat.isvoid = 0 AND hcat.calibratnounkid = @calibratnounkid AND hcat.statusunkid IN (0,1) "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)

                dsExists = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If dsExists.Tables(0).Rows.Count > 0 Then
                    Dim iGUsrIdx As Integer = dsExists.Tables(0).Rows(0)("audituserunkid")
                    If iGUsrIdx <> intCalibratorUserId Then
                        xMessage = Language.getMessage(mstrModuleName, 501, "Sorry, Calibration number is already linked with other calibrator.")
                        intStatusId = 2
                        Exit Try
                    End If
                End If

                With objScrApprl
                    ._Approvalremark = ""
                    ._Auditdatetime = dtCurrentDate
                    ._Audittype = enAuditType.ADD
                    ._AuditUserunkid = intCalibratorUserId
                    ._Calibrated_Score = decCalibrationScore
                    ._CalibrationId = intCalibrationId
                    ._Employeunkid = intEmployeeId
                    ._Form_Name = strScreenName
                    ._Host = strHostName
                    ._Ip = strIPAddress
                    ._IsFinal = False
                    ._IsProcessed = False
                    ._Isvoid = False
                    ._Isweb = blnIsWeb
                    ._Loginemployeeunkid = 0
                    ._Mappingunkid = 0
                    ._Periodunkid = intPeriodId
                    If blnWithSubmitStatus Then
                        ._Statusunkid = clsScoreCalibrationApproval.enCalibrationStatus.Submitted
                    Else
                        ._Statusunkid = clsScoreCalibrationApproval.enCalibrationStatus.NotSubmitted
                    End If
                    ._Tranguid = Guid.NewGuid.ToString
                    ._TransactionDate = dtCurrentDate
                    ._Voidreason = ""
                    ._Calibration_Remark = strCalibrationRemark

                    If .Insert(xDatabaseName, xUserUnkid, xYearUnkid, intCompanyId, objDataOperation) = False Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If
                End With

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                StrQ = "UPDATE hrassess_compute_score_master SET " & _
                       "     calibrated_score = @calibrated_score " & _
                       "    ,calibratnounkid = @calibratnounkid " & _
                       "    ,calibration_remark = @calibration_remark " & _
                       "    ,submit_date = @submit_date " & _
                       "    ,overall_remark = @calibration_remark " & _
                       "    ,isnotified = @issubmitted " & _
                       "    ,issubmitted = @issubmitted " & _
                       "WHERE employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@calibrated_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decCalibrationScore)
                objDataOperation.AddParameter("@calibratnounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalibrationId)
                objDataOperation.AddParameter("@calibration_remark", SqlDbType.NVarChar, strCalibrationRemark.Length, strCalibrationRemark)
                If blnWithSubmitStatus Then
                    objDataOperation.AddParameter("@submit_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDate)
                    objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, 1)
                Else
                    objDataOperation.AddParameter("@submit_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, 0)
                End If
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                xMessage = Language.getMessage(mstrModuleName, 502, "Successfully Imported")
                intStatusId = 1

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ImportCalibrationData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |01-JAN-2020| -- END

    'S.SANDEEP |13-FEB-2021| -- START
    'ISSUE/ENHANCEMENT : BONUS LETTER
    Public Function GetRating(ByVal intEmpId As Integer, ByVal intPrdId As Integer, ByVal blnCalibrationSetting As Boolean) As String
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim strRating As String = ""
        Dim objDataOperation = New clsDataOperation
        Try
            StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(blnCalibrationSetting = True, 1, 0) & " "
            StrQ &= "   SELECT DISTINCT " & _
                    "        1 " & _
                    "       ,ISNULL((SELECT TOP 1 grade_award " & _
                    "                FROM hrapps_ratings " & _
                    "                WHERE isvoid = 0 " & _
                    "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                    "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                    "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) >= score_from " & _
                    "                AND ISNULL(CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 1 THEN CSM.calibrated_score " & _
                    "                                WHEN @Setting = 1 AND ISNULL(CSM.isprocessed, 0) = 0 THEN 0 " & _
                    "                                WHEN @Setting = 0 THEN CSM.finaloverallscore END, 0) <= score_to " & _
                    "        ), '') AS PF " & _
                    "   FROM hrassess_compute_score_master AS CSM " & _
                    "   WHERE CSM.isvoid = 0 " & _
                    "   AND CSM.periodunkid = '" & intPrdId & "' AND CSM.employeeunkid = '" & intEmpId & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then

                If dsList.Tables(0).Rows.Count > 0 Then
                    strRating = dsList.Tables(0).Rows(0)("PF")
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strRating
    End Function
    'S.SANDEEP |13-FEB-2021| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "All Assessment")
            Language.setMessage(mstrModuleName, 3, "Self Assessment")
            Language.setMessage(mstrModuleName, 4, "Assessor Assessment")
            Language.setMessage(mstrModuleName, 5, "Reviewer Assessmemt")
            Language.setMessage(mstrModuleName, 6, "Compute score process already done")
            Language.setMessage(mstrModuleName, 7, "Select")
            Language.setMessage(mstrModuleName, 8, "Provisional Score")
            Language.setMessage(mstrModuleName, 9, "Calibrated Score")
            Language.setMessage(mstrModuleName, 10, "Submit for Calibration")
            Language.setMessage(mstrModuleName, 200, "Under Calibration")
            Language.setMessage(mstrModuleName, 500, "Sorry, Employee is already under calibration process with calibration number:")
            Language.setMessage(mstrModuleName, 501, "Sorry, Calibration number is already linked with other calibrator.")
            Language.setMessage(mstrModuleName, 502, "Successfully Imported")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
