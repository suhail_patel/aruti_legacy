﻿'************************************************************************************************************************************
'Class Name : clsassess_custom_fields.vb
'Purpose    :
'Date       :03-Jul-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_custom_items
    Private Shared ReadOnly mstrModuleName As String = "clsassess_custom_items"
    Dim mstrMessage As String = ""

#Region " ENUMS "

    Public Enum enCustomType
        FREE_TEXT = 1
        SELECTION = 2
        DATE_SELECTION = 3
        NUMERIC_DATA = 4
    End Enum

    Public Enum enSelectionMode
        TRAINING_OBJECTIVE = 1
        EMPLOYEE_COMPETENCIES = 2
        EMPLOYEE_GOALS = 3
        'S.SANDEEP [04 OCT 2016] -- START
        JOB_CAPABILITIES_COURSES = 4
        CAREER_DEVELOPMENT_COURSES = 5
        'S.SANDEEP [04 OCT 2016] -- END

        'S.SANDEEP |16-AUG-2019| -- START
        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
        PERFORMANCE_CUSTOM_ITEM = 6
        'S.SANDEEP |16-AUG-2019| -- END

    End Enum

#End Region

#Region " Private variables "

    Private mintCustomitemunkid As Integer = 0
    Private mintCustomheaderunkid As Integer = 0
    Private mstrCustom_Field As String = String.Empty
    Private mintPeriodunkid As Integer = 0
    Private mblnIsactive As Boolean = True
    Private mintItemTypeId As Integer = 1
    Private mintSelectionModeid As Integer = 0
    Private mintViewModeId As Integer = 0
    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mblnIsDefualtEntry As Boolean = False
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP |08-JAN-2019| -- START
    Private mblnIsCompletedTraining = False
    'S.SANDEEP |08-JAN-2019| -- END

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customitemunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Customitemunkid() As Integer
        Get
            Return mintCustomitemunkid
        End Get
        Set(ByVal value As Integer)
            mintCustomitemunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customheaderunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Customheaderunkid() As Integer
        Get
            Return mintCustomheaderunkid
        End Get
        Set(ByVal value As Integer)
            mintCustomheaderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set custom_item
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Custom_Field() As String
        Get
            Return mstrCustom_Field
        End Get
        Set(ByVal value As String)
            mstrCustom_Field = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ItemTypeId() As Integer
        Get
            Return mintItemTypeId
        End Get
        Set(ByVal value As Integer)
            mintItemTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selectionmodeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _SelectionModeid() As Integer
        Get
            Return mintSelectionModeid
        End Get
        Set(ByVal value As Integer)
            mintSelectionModeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set viewmodeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ViewModeId() As Integer
        Get
            Return mintViewModeId
        End Get
        Set(ByVal value As Integer)
            mintViewModeId = value
        End Set
    End Property


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)


    Public Property _IsDefualtEntry() As Boolean
        Get
            Return mblnIsDefualtEntry
        End Get
        Set(ByVal value As Boolean)
            mblnIsDefualtEntry = value
        End Set
    End Property

    'Shani (26-Sep-2016) -- End

    'S.SANDEEP |08-JAN-2019| -- START
    Public Property _IsCompletedTraining() As Boolean
        Get
            Return mblnIsCompletedTraining
        End Get
        Set(ByVal value As Boolean)
            mblnIsCompletedTraining = value
        End Set
    End Property
    'S.SANDEEP |08-JAN-2019| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  customitemunkid " & _
              ", customheaderunkid " & _
              ", custom_item " & _
              ", periodunkid " & _
              ", isactive " & _
              ", itemtypeid " & _
              ", ISNULL(selectionmodeid," & enSelectionMode.TRAINING_OBJECTIVE & ") AS selectionmodeid " & _
              ", viewmodeid " & _
              ", isdefaultentry " & _
              ", ISNULL(iscompletedtraining,0) AS iscompletedtraining " & _
             "FROM hrassess_custom_items " & _
             "WHERE customitemunkid = @customitemunkid "
            'Shani (26-Sep-2016) -- [isdefaultentry]
            'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END
            'S.SANDEEP |05-APR-2019| -- START
            'REMOVED : iscompletedtraining
            'ADDED   : ISNULL(iscompletedtraining,0) AS iscompletedtraining
            'S.SANDEEP |05-APR-2019| -- END
            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCustomitemunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCustomitemunkid = CInt(dtRow.Item("customitemunkid"))
                mintCustomheaderunkid = CInt(dtRow.Item("customheaderunkid"))
                mstrCustom_Field = dtRow.Item("custom_item").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintItemTypeId = CInt(dtRow.Item("itemtypeid"))
                mintSelectionModeid = CInt(dtRow.Item("selectionmodeid"))
                mintViewModeId = CInt(dtRow.Item("viewmodeid"))
                'S.SANDEEP |08-JAN-2019| -- START
                mblnIsCompletedTraining = CBool(dtRow.Item("iscompletedtraining"))
                'S.SANDEEP |08-JAN-2019| -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_custom_items.customitemunkid " & _
                   ", hrassess_custom_items.customheaderunkid " & _
                   ", hrassess_custom_items.custom_item " & _
                   ", hrassess_custom_items.periodunkid " & _
                   ", hrassess_custom_items.isactive " & _
                   ", hrassess_custom_items.itemtypeid " & _
                   ", CASE WHEN hrassess_custom_items.itemtypeid = '" & enCustomType.FREE_TEXT & "' THEN @FT " & _
                   "       WHEN hrassess_custom_items.itemtypeid = '" & enCustomType.SELECTION & "' THEN @ST " & _
                   "       WHEN hrassess_custom_items.itemtypeid = '" & enCustomType.DATE_SELECTION & "' THEN @DS " & _
                   "       WHEN hrassess_custom_items.itemtypeid = '" & enCustomType.NUMERIC_DATA & "' THEN @ND END AS iType " & _
                   ", cfcommon_period_tran.period_name as pname " & _
                   ", hrassess_custom_headers.name AS cheader " & _
                   ", cfcommon_period_tran.statusid " & _
                   ", hrassess_custom_items.selectionmodeid " & _
                   ", hrassess_custom_items.viewmodeid " & _
                   ", CASE WHEN hrassess_custom_items.selectionmodeid = '" & enSelectionMode.TRAINING_OBJECTIVE & "' THEN @TO " & _
                   "       WHEN hrassess_custom_items.selectionmodeid = '" & enSelectionMode.EMPLOYEE_COMPETENCIES & "' THEN @EC " & _
                   "       WHEN hrassess_custom_items.selectionmodeid = '" & enSelectionMode.EMPLOYEE_GOALS & "' THEN @EG " & _
                   "       WHEN hrassess_custom_items.selectionmodeid = '" & enSelectionMode.JOB_CAPABILITIES_COURSES & "' THEN @JCC " & _
                   "       WHEN hrassess_custom_items.selectionmodeid = '" & enSelectionMode.CAREER_DEVELOPMENT_COURSES & "' THEN @CDC " & _
                   "       WHEN hrassess_custom_items.selectionmodeid = '" & enSelectionMode.PERFORMANCE_CUSTOM_ITEM & "' THEN @PCI " & _
                   "  ELSE '' END AS iSType " & _
                   ", CASE WHEN hrassess_custom_items.viewmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' THEN @A " & _
                   "       WHEN hrassess_custom_items.viewmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' THEN @R ELSE @E END AS iViewType " & _
                   ",hrassess_custom_items.isdefaultentry " & _
                   ",CASE WHEN hrassess_custom_items.isdefaultentry = 1 THEN @Y ELSE @N END AS Defualt_Entry " & _
                   ",hrassess_custom_items.iscompletedtraining " & _
                   "FROM hrassess_custom_items " & _
                   " JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                   " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_custom_items.periodunkid " & _
                   " WHERE hrassess_custom_items.isactive = 1 "

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'Add
            '",hrassess_custom_items.isdefaultentry " & _
            '",CASE WHEN hrassess_custom_items.isdefaultentry = 1 THEN @Y ELSE @N THEN Defualt_Entry " & _
            'Shani (26-Sep-2016) -- End


            'S.SANDEEP |16-AUG-2019| -- ADD [PERFORMANCE_CUSTOM_ITEM]


            objDataOperation.AddParameter("@FT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Free Text"))
            objDataOperation.AddParameter("@ST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Selection"))
            objDataOperation.AddParameter("@DS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Date"))
            objDataOperation.AddParameter("@TO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Training or Learning Objective"))
            objDataOperation.AddParameter("@EC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Employee Competencies"))
            objDataOperation.AddParameter("@EG", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Employee Goals"))
            objDataOperation.AddParameter("@ND", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Numeric Data"))

            objDataOperation.AddParameter("@E", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "All"))
            objDataOperation.AddParameter("@A", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Assessor"))
            objDataOperation.AddParameter("@R", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Reviewer"))


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@Y", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Yes"))
            objDataOperation.AddParameter("@N", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "No"))
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [04 OCT 2016] -- START
            objDataOperation.AddParameter("@JCC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Job Capability Courses"))
            objDataOperation.AddParameter("@CDC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Career Development Courses"))
            'S.SANDEEP [04 OCT 2016] -- END


            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
            objDataOperation.AddParameter("@PCI", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Performance Custom Item"))
            'S.SANDEEP |16-AUG-2019| -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_custom_items) </purpose>
    ''' Shani (26-Sep-2016) -- Start
    ''' Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    ''' Public Function Insert(ByVal iUserId As Integer) As Boolean
    Public Function Insert(ByVal iUserId As Integer, Optional ByVal objDo As clsDataOperation = Nothing) As Boolean
        'Shani (26-Sep-2016) -- End


        If isExist(mstrCustom_Field, mintCustomheaderunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Custom Item is already defined for the selected group and period. Please define new Custom Item.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Shani (26-Sep-2016) -- Start
        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
        'Dim objDataOperation As New clsDataOperation
        'objDataOperation.BindTransaction()
        Dim objDataOperation As clsDataOperation
        If objDo IsNot Nothing Then
            objDataOperation = objDo
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        'Shani (26-Sep-2016) -- End
        Try
            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCustomheaderunkid.ToString)
            objDataOperation.AddParameter("@custom_item", SqlDbType.NVarChar, mstrCustom_Field.Length, mstrCustom_Field.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeId)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid)
            objDataOperation.AddParameter("@viewmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewModeId)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@isdefaultentry", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefualtEntry.ToString)

            'strQ = "INSERT INTO hrassess_custom_items ( " & _
            '           "  customheaderunkid " & _
            '           ", custom_item " & _
            '           ", periodunkid " & _
            '           ", isactive" & _
            '           ", itemtypeid " & _
            '           ", selectionmodeid " & _
            '           ", viewmodeid " & _
            '       ") VALUES (" & _
            '           "  @customheaderunkid " & _
            '           ", @custom_item " & _
            '           ", @periodunkid " & _
            '           ", @isactive " & _
            '           ", @itemtypeid " & _
            '           ", @selectionmodeid " & _
            '           ", @viewmodeid " & _
            '       "); SELECT @@identity"

            objDataOperation.AddParameter("@iscompletedtraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedTraining) 'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END

            strQ = "INSERT INTO hrassess_custom_items ( " & _
                       "  customheaderunkid " & _
                       ", custom_item " & _
                       ", periodunkid " & _
                       ", isactive" & _
                       ", itemtypeid " & _
                       ", selectionmodeid " & _
                       ", viewmodeid " & _
                       ", isdefaultentry " & _
                       ", iscompletedtraining " & _
                   ") VALUES (" & _
                       "  @customheaderunkid " & _
                       ", @custom_item " & _
                       ", @periodunkid " & _
                       ", @isactive " & _
                       ", @itemtypeid " & _
                       ", @selectionmodeid " & _
                       ", @viewmodeid " & _
                       ", @isdefaultentry " & _
                       ", @iscompletedtraining " & _
                   "); SELECT @@identity"
            'Shani (26-Sep-2016) -- End
            'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCustomitemunkid = dsList.Tables(0).Rows(0).Item(0)


        'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_custom_items", "customitemunkid", mintCustomitemunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objDataOperation.ReleaseTransaction(True)
            If objDo Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Shani (26-Sep-2016) -- End


            Return True
        Catch ex As Exception
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objDataOperation.ReleaseTransaction(False)
            If objDo Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Shani (26-Sep-2016) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objDataOperation = Nothing
            If objDo Is Nothing Then objDataOperation = Nothing
            'Shani (26-Sep-2016) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_custom_items) </purpose>
    Public Function Update(ByVal iUserId As Integer) As Boolean
        If isExist(mstrCustom_Field, mintCustomheaderunkid, mintPeriodunkid, mintCustomitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Custom Item is already defined for the selected group and period. Please define new Custom Item.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCustomitemunkid.ToString)
            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCustomheaderunkid.ToString)
            objDataOperation.AddParameter("@custom_item", SqlDbType.NVarChar, mstrCustom_Field.Length, mstrCustom_Field.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeId)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid)
            objDataOperation.AddParameter("@viewmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewModeId)
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@isdefaultentry", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDefualtEntry.ToString)

            'strQ = "UPDATE hrassess_custom_items SET " & _
            '       "  customheaderunkid = @customheaderunkid" & _
            '       ", custom_item = @custom_item" & _
            '       ", periodunkid = @periodunkid" & _
            '       ", isactive = @isactive " & _
            '       ", itemtypeid = @itemtypeid " & _
            '       ", selectionmodeid = @selectionmodeid " & _
            '       ", viewmodeid = @viewmodeid " & _
            '       "WHERE customitemunkid = @customitemunkid "

            objDataOperation.AddParameter("@iscompletedtraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedTraining) 'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END

            strQ = "UPDATE hrassess_custom_items SET " & _
                   "  customheaderunkid = @customheaderunkid" & _
                   ", custom_item = @custom_item" & _
                   ", periodunkid = @periodunkid" & _
                   ", isactive = @isactive " & _
                   ", itemtypeid = @itemtypeid " & _
                   ", selectionmodeid = @selectionmodeid " & _
                   ", viewmodeid = @viewmodeid " & _
                  ", isdefaultentry = @isdefaultentry " & _
                   ", iscompletedtraining = @iscompletedtraining " & _
                   "WHERE customitemunkid = @customitemunkid "
            'Shani (26-Sep-2016) -- End
            'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_custom_items", mintCustomitemunkid, "customitemunkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_custom_items", "customitemunkid", iUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_custom_items) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal iUserId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If isUsed(intUnkid) = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, You can not delete this item, reason this item alrady used")
                Return False
            End If
            'Shani (26-Sep-2016) -- End


            strQ = "UPDATE hrassess_custom_items SET " & _
                   " isactive = 0 " & _
                   "WHERE customitemunkid = @customitemunkid "

            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_custom_items", "customitemunkid", intUnkid, , iUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        mstrMessage = ""
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'S.SANDEEP [30-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001951}
            'strQ = "SELECT 1 FROM hrcompetency_customitem_tran WHERE customitemunkid = @customitemunkid AND isvoid = 0 "
            strQ = "SELECT 1 FROM hrcompetency_customitem_tran WHERE customitemunkid = @customitemunkid AND isvoid = 0 " & _
                   "UNION SELECT 1 FROM hrassess_plan_customitem_tran WHERE customitemunkid = @customitemunkid AND isvoid = 0 "
            'S.SANDEEP [30-Jan-2018] -- END

            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    ''' Shani (26-Sep-2016) -- Start
    ''' Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    ''' Public Function isExist(ByVal iFieldData As String, ByVal iHeaderId As Integer, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal iFieldData As String, ByVal iHeaderId As Integer, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef intItemUnkId As Integer = -1) As Boolean
        'Shani (26-Sep-2016) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'strQ = "SELECT " & _
            '       "  customitemunkid " & _
            '       ", customheaderunkid " & _
            '       ", custom_item " & _
            '       ", periodunkid " & _
            '       ", isactive " & _
            '       ", selectionmodeid " & _
            '       "FROM hrassess_custom_items " & _
            '       "WHERE custom_item = @custom_item " & _
            '       "AND customheaderunkid = @customheaderunkid " & _
            '       "AND periodunkid = @periodunkid AND isactive = 1 "

            strQ = "SELECT " & _
                   "  customitemunkid " & _
                   ", customheaderunkid " & _
                   ", custom_item " & _
                   ", periodunkid " & _
                   ", isactive " & _
                   ", selectionmodeid " & _
                   ", isdefaultentry " & _
                   "FROM hrassess_custom_items " & _
                   "WHERE custom_item = @custom_item " & _
                   "AND customheaderunkid = @customheaderunkid " & _
                   "AND periodunkid = @periodunkid AND isactive = 1 "
            'Shani (26-Sep-2016) -- End
            If intUnkid > 0 Then
                strQ &= " AND customitemunkid <> @customitemunkid"
            End If

            objDataOperation.AddParameter("@custom_item", SqlDbType.NVarChar, iFieldData.Length, iFieldData)
            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iHeaderId)
            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If dsList.Tables(0).Rows.Count > 0 Then
                intItemUnkId = CInt(dsList.Tables(0).Rows(0)("customitemunkid"))
            End If
            'Shani (26-Sep-2016) -- End


            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList_CustomTypes(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT '" & enCustomType.FREE_TEXT & "' AS Id, @FT AS Name UNION " & _
                   "SELECT '" & enCustomType.SELECTION & "' AS Id, @ST AS Name UNION " & _
                   "SELECT '" & enCustomType.DATE_SELECTION & "' AS Id, @DS AS Name UNION " & _
                   "SELECT '" & enCustomType.NUMERIC_DATA & "' AS Id, @ND AS Name "

            objDataOpr.AddParameter("@FT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Free Text"))
            objDataOpr.AddParameter("@ST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Selection"))
            objDataOpr.AddParameter("@DS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Date"))
            objDataOpr.AddParameter("@ND", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Numeric Data"))
            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))

            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_CustomTypes", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList_SelectionMode(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try

            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            'S.SANDEEP [04 OCT 2016] -- START
            'StrQ &= "SELECT '" & enSelectionMode.TRAINING_OBJECTIVE & "' AS Id, @TO AS Name UNION " & _
            '       "SELECT '" & enSelectionMode.EMPLOYEE_COMPETENCIES & "' AS Id, @EC AS Name UNION " & _
            '       "SELECT '" & enSelectionMode.EMPLOYEE_GOALS & "' AS Id, @EG AS Name "


            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}



            'StrQ &= "SELECT '" & enSelectionMode.TRAINING_OBJECTIVE & "' AS Id, @TO AS Name UNION " & _
            '       "SELECT '" & enSelectionMode.EMPLOYEE_COMPETENCIES & "' AS Id, @EC AS Name UNION " & _
            '        "SELECT '" & enSelectionMode.EMPLOYEE_GOALS & "' AS Id, @EG AS Name UNION " & _
            '        "SELECT '" & enSelectionMode.JOB_CAPABILITIES_COURSES & "' AS Id, @JCC AS Name UNION " & _
            '        "SELECT '" & enSelectionMode.CAREER_DEVELOPMENT_COURSES & "' AS Id, @CDC AS Name  "


            StrQ &= "SELECT '" & enSelectionMode.TRAINING_OBJECTIVE & "' AS Id, @TO AS Name UNION " & _
                   "SELECT '" & enSelectionMode.EMPLOYEE_COMPETENCIES & "' AS Id, @EC AS Name UNION " & _
                    "SELECT '" & enSelectionMode.EMPLOYEE_GOALS & "' AS Id, @EG AS Name UNION " & _
                    "SELECT '" & enSelectionMode.JOB_CAPABILITIES_COURSES & "' AS Id, @JCC AS Name UNION " & _
                    "SELECT '" & enSelectionMode.CAREER_DEVELOPMENT_COURSES & "' AS Id, @CDC AS Name UNION " & _
                    "SELECT '" & enSelectionMode.PERFORMANCE_CUSTOM_ITEM & "' AS Id, @PCI AS Name "

            'S.SANDEEP |16-AUG-2019| -- END

            'S.SANDEEP [04 OCT 2016] -- END




            objDataOpr.AddParameter("@TO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Training or Learning Objective"))
            objDataOpr.AddParameter("@EC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Employee Competencies"))
            objDataOpr.AddParameter("@EG", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Employee Goals"))
            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))
            'S.SANDEEP [04 OCT 2016] -- START
            objDataOpr.AddParameter("@JCC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Job Capability Courses"))
            objDataOpr.AddParameter("@CDC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Career Development Courses"))
            'S.SANDEEP [04 OCT 2016] -- END

            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
            objDataOpr.AddParameter("@PCI", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Performance Custom Item"))
            'S.SANDEEP |16-AUG-2019| -- END


            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_SelectionMode", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP |14-MAR-2019| -- START
    'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
    'Public Function getComboList(ByVal iPeriodId As Integer, ByVal iHeaderId As Integer, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal iList As String = "List") As DataSet
    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal iPeriodId As Integer, ByVal iHeaderId As Integer, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal iList As String = "List", Optional ByVal xModeId As Integer = 0) As DataSet
        'S.SANDEEP |14-MAR-2019| -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            If iList.Trim.Length <= 0 Then iList = "List"

            If blnAddSelect = True Then
                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'StrQ = "SELECT 0 AS Id, @Select AS Name,0 AS periodunkid  UNION "
                StrQ = "SELECT 0 AS Id, @Select AS Name,0 AS periodunkid,0 AS isdefaultentry, 0 AS iscompletedtraining, -1 AS viewmodeid  UNION "
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP |25-MAR-2019| -- START
                'ADDED : -1 AS viewmodeid
                'S.SANDEEP |25-MAR-2019| -- END

            End If
            'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'StrQ &= "SELECT customitemunkid AS Id, custom_item AS Name,periodunkid AS periodunkid FROM hrassess_custom_items WHERE isactive = 1 AND periodunkid = '" & iPeriodId & "' " & _
            '        "AND customheaderunkid = '" & iHeaderId & "' "
            StrQ &= "SELECT " & _
                    "    customitemunkid AS Id " & _
                    "   ,custom_item AS Name " & _
                    "   ,periodunkid AS periodunkid " & _
                    "   ,isdefaultentry AS isdefaultentry " & _
                    "   ,iscompletedtraining AS iscompletedtraining " & _
                    "   ,viewmodeid AS viewmodeid " & _
                    "FROM hrassess_custom_items " & _
                    "WHERE isactive = 1 AND periodunkid = '" & iPeriodId & "' " & _
                    "AND customheaderunkid = '" & iHeaderId & "' "
            'Shani (26-Sep-2016) -- End
            'S.SANDEEP |08-JAN-2019| -- START [iscompletedtraining] -- END
            'S.SANDEEP |25-MAR-2019| -- START [viewmodeid] -- END

            'S.SANDEEP |14-MAR-2019| -- START
            If xModeId > 0 Then
                Select Case xModeId
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= " AND hrassess_custom_items.viewmodeid = 0 "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND hrassess_custom_items.viewmodeid = 2 "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND hrassess_custom_items.viewmodeid = 3 "
                End Select
            End If
            'S.SANDEEP |14-MAR-2019| -- END

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsValidItemCount(ByVal xCustomHeaderId As Integer, ByVal intPeriodId As Integer) As String 'S.SANDEEP [03 OCT 2016] -- START {intPeriodId} -- END
        Dim xMsg As String = String.Empty
        Dim xCnt As Integer = -1
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT customitemunkid FROM hrassess_custom_items WHERE isactive = 1 AND customheaderunkid = @customheaderunkid AND periodunkid = @periodunkid "

            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCustomHeaderId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            xCnt = objDataOperation.RecordCount(StrQ)
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If
            If xCnt >= 10 Then
                xMsg = Language.getMessage(mstrModuleName, 14, "Sorry, You are allowed to define max of 10 custom items for each custom header.")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidItemCount", mstrModuleName)
        Finally
        End Try
        Return xMsg
    End Function

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Public Function isUsed_MultipleItemIds(ByVal strUserIds As String) As Boolean
        mstrMessage = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT 1 FROM hrcompetency_customitem_tran WHERE customitemunkid IN (" & strUserIds & ") AND isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Shani (26-Sep-2016) -- End


    'Shani(23-FEB-2017) -- Start
    'Enhancement - Add new custom item saving setting requested by (aga khan)
    Public Function GetCutomItemList(ByVal intPeriodUnkid As Integer, ByVal menAssessedMode As enAssessmentMode) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "SELECT " & _
                   "    customitemunkid " & _
                   "FROM hrassess_custom_items " & _
                   "WHERE isactive = 1 AND periodunkid = '" & intPeriodUnkid & "' AND hrassess_custom_items.viewmodeid IN ("
            'S.SANDEEP |25-MAR-2019| -- START
            'If menAssessedMode <> enAssessmentMode.SELF_ASSESSMENT Then
            '    strQ &= "," & menAssessedMode
            'End If
            Select Case menAssessedMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= "0"
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= "2"
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    strQ &= "3"
            End Select
            'S.SANDEEP |25-MAR-2019| -- END
            strQ &= ") "

            Using objDataOperation As New clsDataOperation
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End Using

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCutomItemList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Shani(23-FEB-2017) -- End

    'S.SANDEEP [07-NOV-2018] -- START
    Public Function CheckPlannedItem(ByVal intPeriodId As Integer, ByVal intEmployeeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim strMsg As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim iRowCount As Integer = -1
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "    customheaderunkid " & _
                   "FROM hrassess_custom_headers " & _
                   "WHERE hrassess_custom_headers.isactive = 1 AND hrassess_custom_headers.isinclude_planning = 1 "
            Dim dsHeaders As New DataSet
            dsHeaders = objDataOperation.ExecQuery(StrQ, "List")

            If dsHeaders.Tables("List").Rows.Count > 0 Then

                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

                For Each ro As DataRow In dsHeaders.Tables("List").Rows
                    StrQ = "SELECT 1 " & _
                           "FROM hrassess_plan_customitem_tran AS PCT " & _
                           "    JOIN hrassess_custom_items AS CT ON CT.customitemunkid = PCT.customitemunkid " & _
                           "    JOIN hrassess_custom_headers AS CH ON CH.customheaderunkid = CT.customheaderunkid AND PCT.customheaderunkid = CH.customheaderunkid " & _
                           "WHERE PCT.periodunkid = @periodunkid AND PCT.employeeunkid = @employeeunkid " & _
                           "AND PCT.isvoid = 0 AND CT.isactive = 1 AND CH.isactive = 1 AND CH.isinclude_planning = 1 AND CH.customheaderunkid = '" & ro("customheaderunkid").ToString() & "' "

                    'S.SANDEEP |18-JAN-2019| -- START
                    ' ----- REMOVED  : AND CT.viewmodeid = 0
                    'S.SANDEEP |18-JAN-2019| -- END


                    iRowCount = objDataOperation.RecordCount(StrQ)


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If iRowCount <= 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 17, "Sorry, you cannot do submit goals for approval. Reason, there are no custom items you have planned for selected period and employee.")
                        Exit For
                    End If
                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CheckPlannedItem; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return strMsg
    End Function
    'S.SANDEEP [07-NOV-2018] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This Custom Item is already defined for the selected group and period. Please define new Custom Item.")
			Language.setMessage(mstrModuleName, 2, "Free Text")
			Language.setMessage(mstrModuleName, 3, "Selection")
			Language.setMessage(mstrModuleName, 4, "Date")
			Language.setMessage(mstrModuleName, 5, "Training or Learning Objective")
			Language.setMessage(mstrModuleName, 6, "Employee Competencies")
			Language.setMessage(mstrModuleName, 7, "Employee Goals")
			Language.setMessage(mstrModuleName, 8, "Numeric Data")
			Language.setMessage(mstrModuleName, 9, "Select")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Language.setMessage(mstrModuleName, 11, "All")
			Language.setMessage(mstrModuleName, 12, "Assessor")
			Language.setMessage(mstrModuleName, 13, "Reviewer")
            Language.setMessage(mstrModuleName, 14, "Sorry, You are allowed to define max of 10 custom items for each custom header.")
            Language.setMessage(mstrModuleName, 15, "Yes")
            Language.setMessage(mstrModuleName, 16, "No")
            Language.setMessage(mstrModuleName, 18, "Performance Custom Item")


		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
