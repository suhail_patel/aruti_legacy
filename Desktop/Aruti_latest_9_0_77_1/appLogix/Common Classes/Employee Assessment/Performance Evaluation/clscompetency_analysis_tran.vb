﻿'************************************************************************************************************************************
'Class Name :clscompetency_analysis_tran.vb
'Purpose    :
'Date       :08-Aug-2014
'Written By :Sandeep J. Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clscompetency_analysis_tran
    Private Shared ReadOnly mstrModuleName As String = "clscompetency_analysis_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As New clsDataOperation

#Region " Private Variables "

    Private mintAnalysisTranId As Integer = -1
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mblnConsiderWeightAsNumber As Boolean = False
    'S.SANDEEP [09 OCT 2015] -- START
    Private mblnSelfAssignCompetencies As Boolean = False
    'S.SANDEEP [09 OCT 2015] -- END

'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Private ianalysistranunkid As Integer = -1
    Private ianalysisunkid As Integer = -1
    Private iassessgroupunkid As Integer = -1
    Private icompetenciesunkid As Integer = -1
    Private iresult As Decimal = 0
    Private iremark As String = String.Empty
    Private iisvoid As Boolean = False
    Private ivoiduserunkid As Integer = -1
    Private ivoiddatetime As DateTime = Nothing
    Private ivoidreason As String = String.Empty
    Private iitem_weight As Decimal = 0
    Private imax_scale As Decimal = 0
    Private iagreedscore As Decimal = 0
    'S.SANDEEP |18-JAN-2020| -- END
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
            Call GetAnalysisTran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    Public Property _ConsiderWeightAsNumber() As Boolean
        Get
            Return mblnConsiderWeightAsNumber
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderWeightAsNumber = value
        End Set
    End Property

    'S.SANDEEP [09 OCT 2015] -- START
    Public WriteOnly Property _SelfAssignCompetencies() As Boolean
        Set(ByVal value As Boolean)
            mblnSelfAssignCompetencies = value
        End Set
    End Property
    'S.SANDEEP [09 OCT 2015] -- END

'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Property _ianalysistranunkid() As Integer
        Get
            Return ianalysistranunkid
        End Get
        Set(ByVal value As Integer)
            ianalysistranunkid = value
        End Set
    End Property
    Public Property _ianalysisunkid() As Integer
        Get
            Return ianalysisunkid
        End Get
        Set(ByVal value As Integer)
            ianalysisunkid = value
        End Set
    End Property
    Public Property _iassessgroupunkid() As Integer
        Get
            Return iassessgroupunkid
        End Get
        Set(ByVal value As Integer)
            iassessgroupunkid = value
        End Set
    End Property
    Public Property _icompetenciesunkid() As Integer
        Get
            Return icompetenciesunkid
        End Get
        Set(ByVal value As Integer)
            icompetenciesunkid = value
        End Set
    End Property
    Public Property _iresult() As Decimal
        Get
            Return iresult
        End Get
        Set(ByVal value As Decimal)
            iresult = value
        End Set
    End Property
    Public Property _iremark() As String
        Get
            Return iremark
        End Get
        Set(ByVal value As String)
            iremark = value
        End Set
    End Property
    Public Property _iisvoid() As Boolean
        Get
            Return iisvoid
        End Get
        Set(ByVal value As Boolean)
            iisvoid = value
        End Set
    End Property
    Public Property _ivoiduserunkid() As Integer
        Get
            Return ivoiduserunkid
        End Get
        Set(ByVal value As Integer)
            ivoiduserunkid = value
        End Set
    End Property
    Public Property _ivoiddatetime() As DateTime
        Get
            Return ivoiddatetime
        End Get
        Set(ByVal value As DateTime)
            ivoiddatetime = value
        End Set
    End Property
    Public Property _ivoidreason() As String
        Get
            Return ivoidreason
        End Get
        Set(ByVal value As String)
            ivoidreason = value
        End Set
    End Property
    Public Property _iitem_weight() As Decimal
        Get
            Return iitem_weight
        End Get
        Set(ByVal value As Decimal)
            iitem_weight = value
        End Set
    End Property
    Public Property _imax_scale() As Decimal
        Get
            Return imax_scale
        End Get
        Set(ByVal value As Decimal)
            imax_scale = value
        End Set
    End Property
    Public Property _iagreedscore() As Decimal
        Get
            Return iagreedscore
        End Get
        Set(ByVal value As Decimal)
            iagreedscore = value
        End Set
    End Property
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("AnalysisTran")

            mdtTran.Columns.Add("analysistranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("assessgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("competenciesunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("result", System.Type.GetType("System.Decimal")).DefaultValue = -1
            mdtTran.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("dresult", System.Type.GetType("System.Decimal")).DefaultValue = 0

            'S.SANDEEP [21 JAN 2015] -- START
            ''S.SANDEEP [ 01 JAN 2015 ] -- START
            'mdtTran.Columns.Add("computed_value", System.Type.GetType("System.Decimal")).DefaultValue = 0
            'mdtTran.Columns.Add("formula_used", System.Type.GetType("System.String")).DefaultValue = ""
            ''S.SANDEEP [ 01 JAN 2015 ] -- END
            mdtTran.Columns.Add("item_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTran.Columns.Add("max_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0

            '****************************************** COMPUTATION PURPOSE
            mdtTran.Columns.Add("iPeriodId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("iItemUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("iEmployeeId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("iScore", System.Type.GetType("System.Decimal")).DefaultValue = 0
            '****************************************** COMPUTATION PURPOSE

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            mdtTran.Columns.Add("agreed_score", System.Type.GetType("System.Decimal")).DefaultValue = 0
            'Shani (23-Nov-2016) -- End


            'S.SANDEEP [21 JAN 2015] -- END


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub New(ByVal _analysistranunkid As Integer, _
                   ByVal _analysisunkid As Integer, _
                   ByVal _assessgroupunkid As Integer, _
                   ByVal _competenciesunkid As Integer, _
                   ByVal _result As Decimal, _
                   ByVal _remark As String, _
                   ByVal _isvoid As Boolean, _
                   ByVal _voiduserunkid As Integer, _
                   ByVal _voiddatetime As DateTime, _
                   ByVal _voidreason As String, _
                   ByVal _item_weight As Decimal, _
                   ByVal _max_scale As Decimal, _
                   ByVal _agreedscore As Decimal)

        ianalysistranunkid = _analysistranunkid
        ianalysisunkid = _analysisunkid
        iassessgroupunkid = _assessgroupunkid
        icompetenciesunkid = _competenciesunkid
        iresult = _result
        iremark = _remark
        iisvoid = _isvoid
        ivoiduserunkid = _voiduserunkid
        ivoiddatetime = _voiddatetime
        ivoidreason = _voidreason
        iitem_weight = _item_weight
        imax_scale = _max_scale
        iagreedscore = _agreedscore
    End Sub

#End Region

#Region " Private/Public Methods "

    Private Sub GetAnalysisTran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try

            'S.SANDEEP [09 OCT 2015] -- START
            'StrQ = "SELECT " & _
            '       "     hrcompetency_analysis_tran.analysistranunkid " & _
            '       "    ,hrcompetency_analysis_tran.analysisunkid " & _
            '       "    ,hrcompetency_analysis_tran.assessgroupunkid " & _
            '       "    ,hrcompetency_analysis_tran.competenciesunkid " & _
            '       "    ,(CAST(hrcompetency_analysis_tran.result AS DECIMAL(20,2)) * CAST(hrassess_competence_assign_tran.weight AS DECIMAL(20,2))) AS dresult " & _
            '       "    ,hrcompetency_analysis_tran.result " & _
            '       "    ,hrcompetency_analysis_tran.remark " & _
            '       "    ,hrcompetency_analysis_tran.isvoid " & _
            '       "    ,hrcompetency_analysis_tran.voiduserunkid " & _
            '       "    ,hrcompetency_analysis_tran.voiddatetime " & _
            '       "    ,hrcompetency_analysis_tran.voidreason " & _
            '       "    ,'' AS AUD " & _
            '       "    ,hrcompetency_analysis_tran.item_weight " & _
            '       "    ,hrcompetency_analysis_tran.max_scale " & _
            '       "    ,hrevaluation_analysis_master.periodunkid AS iPeriodId " & _
            '       "    ,CASE WHEN hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN selfemployeeunkid ELSE assessedemployeeunkid END AS iEmployeeId " & _
            '       "    ,hrcompetency_analysis_tran.competenciesunkid AS iItemUnkid " & _
            '       "    ,hrcompetency_analysis_tran.result AS iScore " & _
            '       "FROM hrcompetency_analysis_tran " & _
            '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
            '       "    JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '       "    JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '       "        AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '       "WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 " & _
            '       "    AND hrcompetency_analysis_tran.analysisunkid = @analysisunkid " 'S.SANDEEP [ 01 JAN 2015 ] -- START {computed_value,formula_used} -- END
            ''S.SANDEEP [21 JAN 2015] -- START {REMOVED : (computed_value,formula_used) ; ADDED : (item_weight,max_scale)} -- END

            Dim StrOtherCondition = ""

            If mblnSelfAssignCompetencies = True Then
                StrOtherCondition = " AND hrassess_competence_assign_master.employeeunkid = (CASE WHEN hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN selfemployeeunkid ELSE assessedemployeeunkid END) "
            End If

            StrQ = "SELECT " & _
                   "     hrcompetency_analysis_tran.analysistranunkid " & _
                   "    ,hrcompetency_analysis_tran.analysisunkid " & _
                   "    ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "    ,hrcompetency_analysis_tran.competenciesunkid " & _
                   "    ,(CAST(hrcompetency_analysis_tran.result AS DECIMAL(20,2)) * CAST(hrassess_competence_assign_tran.weight AS DECIMAL(20,2))) AS dresult " & _
                   "    ,hrcompetency_analysis_tran.result " & _
                   "    ,hrcompetency_analysis_tran.remark " & _
                   "    ,hrcompetency_analysis_tran.isvoid " & _
                   "    ,hrcompetency_analysis_tran.voiduserunkid " & _
                   "    ,hrcompetency_analysis_tran.voiddatetime " & _
                   "    ,hrcompetency_analysis_tran.voidreason " & _
                   "    ,'' AS AUD " & _
                   "    ,hrcompetency_analysis_tran.item_weight " & _
                   "    ,hrcompetency_analysis_tran.max_scale " & _
                   "    ,hrevaluation_analysis_master.periodunkid AS iPeriodId " & _
                   "    ,CASE WHEN hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN selfemployeeunkid ELSE assessedemployeeunkid END AS iEmployeeId " & _
                   "    ,hrcompetency_analysis_tran.competenciesunkid AS iItemUnkid " & _
                   "    ,hrcompetency_analysis_tran.result AS iScore " & _
                   "    ,ISNULL(hrcompetency_analysis_tran.agreedscore,0) AS agreed_score " & _
                   "FROM hrcompetency_analysis_tran " & _
                   "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "    JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   "    JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   "        AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                   "        AND hrassess_competence_assign_master.periodunkid = hrevaluation_analysis_master.periodunkid "

            'Shani (23-Nov-2016) -- [agreedscore]


            If StrOtherCondition.Trim.Length > 0 Then
                StrQ &= StrOtherCondition
            End If
            StrQ &= "WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 " & _
                   "    AND hrcompetency_analysis_tran.analysisunkid = @analysisunkid "

            'S.SANDEEP [09 OCT 2015] -- END
            

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                mdtTran.ImportRow(dtRow)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAnalysisTran", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_AnalysisTran(Optional ByVal intUserUnkid As Integer = 0) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrcompetency_analysis_tran ( " & _
                                           "  analysisunkid " & _
                                           ", assessgroupunkid " & _
                                           ", competenciesunkid " & _
                                           ", result " & _
                                           ", remark " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voiddatetime " & _
                                           ", voidreason" & _
                                           ", item_weight " & _
                                           ", max_scale " & _
                                           ", agreedscore " & _
                                       ") VALUES (" & _
                                           "  @analysisunkid " & _
                                           ", @assessgroupunkid " & _
                                           ", @competenciesunkid " & _
                                           ", @result " & _
                                           ", @remark " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason " & _
                                           ", @item_weight " & _
                                           ", @max_scale " & _
                                           ", @agreedscore " & _
                                       "); SELECT @@identity" 'S.SANDEEP [ 01 JAN 2015 ] -- START {computed_value,formula_used} -- END
                                'S.SANDEEP [21 JAN 2015] -- START {REMOVED : (computed_value,formula_used) ; ADDED : (item_weight,max_scale)} -- END

                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessgroupunkid").ToString)
                                objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid").ToString)
                                objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("result").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                'S.SANDEEP [21 JAN 2015] -- START
                                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                                'objDataOperation.AddParameter("@computed_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("computed_value"))
                                'objDataOperation.AddParameter("@formula_used", SqlDbType.NVarChar, .Item("formula_used").ToString.Length, .Item("formula_used"))
                                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                                objDataOperation.AddParameter("@item_weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("item_weight"))
                                objDataOperation.AddParameter("@max_scale", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("max_scale"))
                                'S.SANDEEP [21 JAN 2015] -- END

                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                objDataOperation.AddParameter("@agreedscore", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("agreed_score"))
                                'Shani (23-Nov-2016) -- End



                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintAnalysisTranId = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("analysisunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrcompetency_analysis_tran", "analysistranunkid", mintAnalysisTranId, 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrcompetency_analysis_tran", "analysistranunkid", mintAnalysisTranId, 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"
                                StrQ = "UPDATE hrcompetency_analysis_tran SET " & _
                                        "  analysisunkid = @analysisunkid" & _
                                        ", assessgroupunkid = @assessgroupunkid" & _
                                        ", competenciesunkid = @competenciesunkid" & _
                                        ", result = @result" & _
                                        ", remark = @remark" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                        ", item_weight = @item_weight " & _
                                        ", max_scale = @max_scale " & _
                                        ", agreedscore = @agreedscore " & _
                                      "WHERE analysistranunkid = @analysistranunkid " 'S.SANDEEP [ 01 JAN 2015 ] -- START {computed_value,formula_used} -- END
                                'S.SANDEEP [21 JAN 2015] -- START {REMOVED : (computed_value,formula_used) ; ADDED : (item_weight,max_scale)} -- END

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessgroupunkid").ToString)
                                objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid").ToString)
                                objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("result").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                'S.SANDEEP [21 JAN 2015] -- START
                                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                                'objDataOperation.AddParameter("@computed_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("computed_value"))
                                'objDataOperation.AddParameter("@formula_used", SqlDbType.NVarChar, .Item("formula_used").ToString.Length, .Item("formula_used"))
                                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                                objDataOperation.AddParameter("@item_weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("item_weight"))
                                objDataOperation.AddParameter("@max_scale", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("max_scale"))
                                'S.SANDEEP [21 JAN 2015] -- END
                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                objDataOperation.AddParameter("@agreedscore", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("agreed_score"))
                                'Shani (23-Nov-2016) -- End


                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrcompetency_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Case "D"

                                If .Item("analysistranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrcompetency_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                                StrQ = "UPDATE hrcompetency_analysis_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                     "WHERE analysistranunkid = @analysistranunkid "

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_AnalysisTran", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Function InsertUpdate(ByVal objDataOperation As clsDataOperation, ByVal intUserUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            StrQ = "IF NOT EXISTS(SELECT * FROM hrcompetency_analysis_tran WITH (NOLOCK) WHERE isvoid = 0 AND analysisunkid = @analysisunkid AND assessgroupunkid = @assessgroupunkid AND competenciesunkid = @competenciesunkid) " & _
                   "BEGIN " & _
                   "    INSERT INTO hrcompetency_analysis_tran ( " & _
                   "        analysisunkid " & _
                   "      , assessgroupunkid " & _
                   "      , competenciesunkid " & _
                   "      , result " & _
                   "      , remark " & _
                   "      , isvoid " & _
                   "      , voiduserunkid " & _
                   "      , voiddatetime " & _
                   "      , voidreason" & _
                   "      , item_weight " & _
                   "      , max_scale " & _
                   "      , agreedscore " & _
                   ") VALUES (" & _
                   "       @analysisunkid " & _
                   "     , @assessgroupunkid " & _
                   "     , @competenciesunkid " & _
                   "     , @result " & _
                   "     , @remark " & _
                   "     , @isvoid " & _
                   "     , @voiduserunkid " & _
                   "     , @voiddatetime " & _
                   "     , @voidreason " & _
                   "     , @item_weight " & _
                   "     , @max_scale " & _
                   "     , @agreedscore " & _
                   "); SELECT @@identity,1 AS atype " & _
                   "END " & _
                   "ELSE " & _
                   "BEGIN " & _
                   "    DECLARE @analysistranunkid INT " & _
                   "    SET @analysistranunkid = (SELECT analysistranunkid FROM hrcompetency_analysis_tran WITH (NOLOCK) WHERE isvoid = 0 AND analysisunkid = @analysisunkid AND assessgroupunkid = @assessgroupunkid AND competenciesunkid = @competenciesunkid) " & _
                   "    UPDATE hrcompetency_analysis_tran SET " & _
                   "      analysisunkid = @analysisunkid" & _
                   "    , assessgroupunkid = @assessgroupunkid" & _
                   "    , competenciesunkid = @competenciesunkid" & _
                   "    , result = @result" & _
                   "    , remark = @remark" & _
                   "    , isvoid = @isvoid" & _
                   "    , voiduserunkid = @voiduserunkid" & _
                   "    , voiddatetime = @voiddatetime" & _
                   "    , voidreason = @voidreason " & _
                   "    , item_weight = @item_weight " & _
                   "    , max_scale = @max_scale " & _
                   "    , agreedscore = @agreedscore " & _
                   "    WHERE analysistranunkid = @analysistranunkid " & _
                   "    ;SELECT @analysistranunkid,2 AS atype " & _
                   "END "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ianalysisunkid)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iassessgroupunkid)
            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, icompetenciesunkid)
            objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iresult)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, iremark.Length, iremark)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, iisvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ivoiduserunkid)
            If ivoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ivoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ivoidreason)
            objDataOperation.AddParameter("@item_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iitem_weight)
            objDataOperation.AddParameter("@max_scale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, imax_scale)
            objDataOperation.AddParameter("@agreedscore", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iagreedscore)

            Dim dsList As New DataSet
            Dim intAuditTypeId As Integer = 0

            If ianalysistranunkid <= 0 Then
                dsList = objDataOperation.ExecQuery(StrQ, "List")
            Else
                objDataOperation.ExecNonQuery(StrQ)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ianalysistranunkid <= 0 Then
                ianalysistranunkid = dsList.Tables(0).Rows(0).Item(0)
                intAuditTypeId = dsList.Tables(0).Rows(0).Item(1)
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", ianalysisunkid, "hrcompetency_analysis_tran", "analysistranunkid", ianalysistranunkid, 2, intAuditTypeId, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objCommonATLog = Nothing
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

End Class
