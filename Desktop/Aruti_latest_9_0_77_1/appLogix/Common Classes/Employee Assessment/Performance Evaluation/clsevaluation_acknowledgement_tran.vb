'************************************************************************************************************************************
'Class Name :clsevaluation_acknowledgement_tran.vb
'Purpose    :
'Date       :08-Jan-2017
'Written By :Sandeep J. Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports System
Imports eZee.Common.eZeeForm
Imports System.Globalization

#End Region

''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsevaluation_acknowledgement_tran
    Private Shared ReadOnly mstrModuleName As String = "clsevaluation_acknowledgement_tran"
    Private mstrMessage As String = ""

#Region " Private Variables "

    Private mintAcknowledgementtranunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintAnalysisunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mstrAcknowledgement_comments As String = String.Empty
    Private mdtAcknowledgement_date As DateTime = Nothing
    Private mblnIsVoid As Boolean = False
    Private mdtVoiddatetime As DateTime = Nothing
    Private mstrVoidReason As String = String.Empty
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
    Private mstrWebForm_Name As String = String.Empty
    Private mintLoginemployeeunkid As Integer
    Private mintUserunkid As Integer = 0

#End Region

#Region " Enum "

    Public Enum enEvaluationAcknowledgement
        AGREE = 1
        DISAGREE = 2
    End Enum

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Acknowledgementtranunkid() As Integer
        Get
            Return mintAcknowledgementtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAcknowledgementtranunkid = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _Analysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public Property _Acknowledgement_comments() As String
        Get
            Return mstrAcknowledgement_comments
        End Get
        Set(ByVal value As String)
            mstrAcknowledgement_comments = value
        End Set
    End Property

    Public Property _Acknowledgement_date() As DateTime
        Get
            Return mdtAcknowledgement_date
        End Get
        Set(ByVal value As DateTime)
            mdtAcknowledgement_date = value
        End Set
    End Property

    Public Property _IsVoid() As Boolean
        Get
            Return mblnIsVoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsVoid = value
        End Set
    End Property

    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    'Public WriteOnly Property _IpAddress() As String
    '    Set(ByVal value As String)
    '        mstrIp = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Machine_Name() As String
    '    Set(ByVal value As String)
    '        mstrMachine_Name = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebForm_Name() As String
    '    Set(ByVal value As String)
    '        mstrWebForm_Name = value
    '    End Set
    'End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

#End Region

#Region " Public Function/Procedures "

    Public Function Insert() As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Using objDataOperation As New clsDataOperation
                objDataOperation.BindTransaction()
                StrQ = "INSERT INTO hrassess_acknowledgement_tran " & _
                       "( " & _
                       "     employeeunkid " & _
                       "    ,periodunkid " & _
                       "    ,analysisunkid " & _
                       "    ,statusunkid " & _
                       "    ,acknowledgement_comments " & _
                       "    ,acknowledgement_date " & _
                       "    ,isvoid " & _
                       "    ,voiddatetime " & _
                       "    ,voidreason " & _
                       "    ,userunkid " & _
                       "    ,loginemployeeunkid " & _
                       ") " & _
                       "VALUES " & _
                       "( " & _
                       "     @employeeunkid " & _
                       "    ,@periodunkid " & _
                       "    ,@analysisunkid " & _
                       "    ,@statusunkid " & _
                       "    ,@acknowledgement_comments " & _
                       "    ,@acknowledgement_date " & _
                       "    ,@isvoid " & _
                       "    ,@voiddatetime " & _
                       "    ,@voidreason " & _
                       "    ,@userunkid " & _
                       "    ,@loginemployeeunkid " & _
                       "); SELECT @@identity "

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
                objDataOperation.AddParameter("@acknowledgement_comments", SqlDbType.NVarChar, mstrAcknowledgement_comments.Length, mstrAcknowledgement_comments)
                objDataOperation.AddParameter("@acknowledgement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcknowledgement_date)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid)
                If mdtVoiddatetime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintAcknowledgementtranunkid = dsList.Tables(0).Rows(0).Item(0)

                If InsertAuditTrails(enAuditType.ADD, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objDataOperation.ReleaseTransaction(True)

            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Update() As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Using objDataOperation As New clsDataOperation
                objDataOperation.BindTransaction()
                StrQ = "UPDATE hrassess_acknowledgement_tran " & _
                       "SET " & _
                       "     employeeunkid = @employeeunkid " & _
                       "    ,periodunkid = @periodunkid " & _
                       "    ,analysisunkid = @analysisunkid " & _
                       "    ,statusunkid = @statusunkid " & _
                       "    ,acknowledgement_comments = @acknowledgement_comments " & _
                       "    ,acknowledgement_date = @acknowledgement_date " & _
                       "    ,isvoid = @isvoid " & _
                       "    ,voiddatetime = @voiddatetime " & _
                       "    ,voidreason = @voidreason " & _
                       "    ,userunkid = @userunkid " & _
                       "    ,loginemployeeunkid = @loginemployeeunkid " & _
                       "WHERE acknowledgementtranunkid = @acknowledgementtranunkid "

                objDataOperation.AddParameter("@acknowledgementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAcknowledgementtranunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
                objDataOperation.AddParameter("@acknowledgement_comments", SqlDbType.NVarChar, mstrAcknowledgement_comments.Length, mstrAcknowledgement_comments)
                objDataOperation.AddParameter("@acknowledgement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcknowledgement_date)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid)
                If mdtVoiddatetime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(enAuditType.EDIT, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objDataOperation.ReleaseTransaction(True)

            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Delete() As Boolean
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function MakeLinkVisible(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Try
            Using objDataOperation As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ack.acknowledgementtranunkid " & _
                       "    ,ack.employeeunkid " & _
                       "    ,ack.periodunkid " & _
                       "    ,ack.analysisunkid " & _
                       "FROM hrassess_acknowledgement_tran AS ack " & _
                       "WHERE ack.isvoid = 0 AND ack.employeeunkid = @EmpId AND ack.periodunkid = @PrdId "

                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    blnFlag = True
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExists; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function IsExists(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Using objDataOperation As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ack.acknowledgementtranunkid " & _
                       "    ,ack.employeeunkid " & _
                       "    ,ack.periodunkid " & _
                       "    ,ack.analysisunkid " & _
                       "    ,ack.statusunkid " & _
                       "    ,CASE WHEN ack.statusunkid = 1 THEN @AGR " & _
                       "          WHEN ack.statusunkid = 2 THEN @DAG END AS ack_status " & _
                       "    ,ack.acknowledgement_comments " & _
                       "    ,ISNULL(hrevaluation_analysis_master.assessmodeid,0) AS assessmodeid " & _
                       "FROM hrassess_acknowledgement_tran AS ack " & _
                       "    LEFT JOIN hrevaluation_analysis_master ON ack.analysisunkid = hrevaluation_analysis_master.analysisunkid AND hrevaluation_analysis_master.isvoid = 0 " & _
                       "WHERE ack.isvoid = 0 AND ack.employeeunkid = @EmpId AND ack.periodunkid = @PrdId "

                objDataOperation.AddParameter("@AGR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Agree"))
                objDataOperation.AddParameter("@DAG", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Disagree"))

                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExists; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables("List")
    End Function

    Public Function GetList(Optional ByVal strFilter As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Using objDataOperation As New clsDataOperation
                StrQ = "SELECT " & _
                       "     ack.acknowledgementtranunkid " & _
                       "    ,ack.employeeunkid " & _
                       "    ,ack.periodunkid " & _
                       "    ,ack.analysisunkid " & _
                       "    ,ack.statusunkid " & _
                       "    ,ack.acknowledgement_comments " & _
                       "    ,ack.acknowledgement_date " & _
                       "    ,ack.isvoid " & _
                       "    ,ack.voiddatetime " & _
                       "    ,ack.voidreason " & _
                       "    ,ack.userunkid " & _
                       "    ,ack.loginemployeeunkid " & _
                       "    ,CASE WHEN ack.statusunkid = 1 THEN @AGR " & _
                       "          WHEN ack.statusunkid = 2 THEN @DAG END AS ack_status " & _
                       "    ,CONVERT(NVARCHAR(8),ack.acknowledgement_date,112) AS ack_date " & _
                       "    ,isnull(evm.assessmodeid,0) as assessmodeid " & _
                       "FROM hrassess_acknowledgement_tran AS ack WITH (NOLOCK) " & _
                       "  left join hrevaluation_analysis_master as evm WITH (NOLOCK) on ack.analysisunkid = evm.analysisunkid " & _
                       "WHERE ack.isvoid = 0 and evm.isvoid = 0 "

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                ' -- ADDED > ,isnull(evm.assessmodeid,0) as assessmodeid & and evm.isvoid = 0
                ' -- ADDED > left join hrevaluation_analysis_master as evm on ack.analysisunkid = evm.analysisunkid
                'S.SANDEEP [29-NOV-2017] -- END

                If strFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilter
                End If

                objDataOperation.AddParameter("@AGR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Agree"))
                objDataOperation.AddParameter("@DAG", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Disagree"))

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetEvalAnalysisIds(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer) As Dictionary(Of Integer, Integer)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mDicEvalIds As New Dictionary(Of Integer, Integer)
        Dim exForce As Exception = Nothing
        Try
            Using objDataOperation As New clsDataOperation

                StrQ = "SELECT DISTINCT " & _
                       "     hrevaluation_analysis_master.assessmodeid " & _
                       "    ,hrevaluation_analysis_master.analysisunkid " & _
                       "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                       "WHERE hrevaluation_analysis_master.isvoid = 0 " & _
                       "AND (hrevaluation_analysis_master.selfemployeeunkid = @EmpId OR hrevaluation_analysis_master.assessedemployeeunkid = @EmpId) " & _
                       "AND hrevaluation_analysis_master.periodunkid = @PrdId "

                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mDicEvalIds = dsList.Tables("List").AsEnumerable.ToDictionary(Of Integer, Integer)(Function(x) x.Field(Of Integer)("assessmodeid"), Function(x) x.Field(Of Integer)("analysisunkid"))


            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEvalAnalysisIds; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mDicEvalIds
    End Function

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # (38,41)

    'S.SANDEEP |18-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    'Public Sub Send_Notification(ByVal strTransactionIds As String, _
    '                             ByVal intEmployeeId As Integer, _
    '                             ByVal intPeriodId As Integer, _
    '                             ByVal mdtEmpAsOnDate As DateTime, _
    '                             ByVal xLoginMod As enLogin_Mode, _
    '                             ByVal xUserId As Integer, _
    '                             ByVal strCSVSelectedUserIds As String, _
    '                             ByVal xLogEmpId As Integer, ByVal strFormName As String, ByVal strDBName As String, ByVal intcompanyunkid As Integer)
    '    Try
    '        Dim dslst As New DataSet : dslst = GetList(" ack.acknowledgementtranunkid in(" & strTransactionIds & ")")
    '        Dim strMessage As New Text.StringBuilder
    '        If dslst.Tables(0).Rows.Count > 0 Then
    '            Dim objEmp As New clsEmployee_Master
    '            Dim objPrd As New clscommom_period_Tran
    '            objEmp._Employeeunkid(mdtEmpAsOnDate) = intEmployeeId
    '            objPrd._Periodunkid(strDBName) = intPeriodId
    '            Dim objeval As New clsevaluation_analysis_master
    '            Dim dsDetails As New DataSet
    '            dsDetails = objeval.GetEmployee_AssessorReviewerDetails(intPeriodId, strDBName, Nothing, intEmployeeId)
    '            Dim iEAssessorName As String = "" : Dim iEReviewerName As String = "" : Dim iEAssessorEmail As String = "" : Dim iEReviewerEmail As String = ""
    '            If dsDetails.Tables(0).Rows.Count > 0 Then
    '                Dim dtmp() As DataRow = Nothing
    '                If iEAssessorName.Trim.Length <= 0 Then
    '                    dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 1")
    '                    If dtmp.Length > 0 Then
    '                        iEAssessorName = dtmp(0).Item("arName")
    '                        iEAssessorEmail = dtmp(0).Item("arEmail")
    '                    Else
    '                        dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
    '                        If dtmp.Length > 0 Then
    '                            iEAssessorName = dtmp(0).Item("arName")
    '                            iEAssessorEmail = dtmp(0).Item("arEmail")
    '                        End If
    '                    End If
    '                End If
    '                If iEReviewerName.Trim.Length <= 0 Then
    '                    dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 1")
    '                    If dtmp.Length > 0 Then
    '                        iEReviewerName = dtmp(0).Item("arName")
    '                        iEReviewerEmail = dtmp(0).Item("arEmail")
    '                    Else
    '                        dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")
    '                        If dtmp.Length > 0 Then
    '                            iEReviewerName = dtmp(0).Item("arName")
    '                            iEReviewerEmail = dtmp(0).Item("arEmail")
    '                        End If
    '                    End If
    '                End If
    '            End If
    '            '********************************* ASSESSOR & REVIEWER **************************** | START
    '            For Each drow As DataRow In dslst.Tables(0).Rows
    '                strMessage = New Text.StringBuilder
    '                Dim strName As String = String.Empty
    '                Dim strComment As String = String.Empty
    '                Dim strStatus As String = String.Empty
    '                Dim strEmail As String = String.Empty
    '                If CInt(drow.Item("assessmodeid")) > 0 Then
    '                    Select Case CInt(drow.Item("assessmodeid"))
    '                        Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                            strName = iEAssessorName
    '                            strComment = drow.Item("acknowledgement_comments")
    '                            strStatus = drow.Item("ack_status")
    '                            strEmail = iEAssessorEmail
    '                        Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                            strName = iEReviewerName
    '                            strComment = drow.Item("acknowledgement_comments")
    '                            strStatus = drow.Item("ack_status")
    '                            strEmail = iEReviewerEmail
    '                    End Select
    '                    If strName.Trim().Length > 0 AndAlso strEmail.Trim.Length > 0 Then
    '                        strMessage.Append("<HTML> <BODY>")
    '                        strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & strName & "</B>, <BR><BR>")
    '                        strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to inform you that, I have done with assessing my final acknowledgement for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & " " & _
    '                                          Language.getMessage(mstrModuleName, 5, " with details below :") & "<BR><BR>")
    '                        strMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
    '                        strMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
    '                        strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 6, "Status") & "</span></b></TD>")
    '                        strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 7, "Comments") & "</span></b></TD>")
    '                        strMessage.Append("</TR>")
    '                        strMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
    '                        strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strStatus & "</span></TD>")
    '                        strMessage.Append("<TD align = 'LEFT' WIDTH = '60%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strComment & "</span></TD>")
    '                        strMessage.Append("</TR>" & vbCrLf)
    '                        strMessage.Append("</TABLE>")
    '                        strMessage.Append("<BR>")
    '                        strMessage.Append("<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>")
    '                        strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                        strMessage.Append("</span></p>")
    '                        strMessage.Append("</BODY></HTML>")

    '                        If strMessage.ToString.Trim.Length > 0 Then
    '                            Dim objSendMail As New clsSendMail
    '                            objSendMail._ToEmail = strEmail
    '                            objSendMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
    '                            objSendMail._Message = strMessage.ToString
    '                            objSendMail._Form_Name = strFormName
    '                            objSendMail._LogEmployeeUnkid = xLogEmpId
    '                            objSendMail._OperationModeId = xLoginMod
    '                            objSendMail._UserUnkid = xUserId
    '                            objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
    '                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                            Try
    '                                objSendMail.SendMail(intcompanyunkid)
    '                            Catch ex As Exception
    '                            End Try
    '                            objSendMail = Nothing
    '                        End If
    '                    End If
    '                End If
    '            Next
    '            '********************************* ASSESSOR & REVIEWER **************************** | END


    '            '********************************* FOR SELECTED USER **************************** | START
    '            'strMessage = New Text.StringBuilder() 'S.SANDEEP [15-Feb-2018] -- START {#41} -- END
    '            If strCSVSelectedUserIds.Trim.Length > 0 Then
    '                Dim dLst As New DataSet : Dim objUsr As New clsUserAddEdit
    '                dLst = objUsr.GetList("List", "hrmsConfiguration..cfuser_master.userunkid IN (" & strCSVSelectedUserIds & ")")
    '                If dslst.Tables("List").Rows.Count > 0 Then
    '                    For Each row As DataRow In dLst.Tables("List").Rows
    '                        If row.Item("email").ToString.Trim.Length > 0 Then
    '                            strMessage = New Text.StringBuilder() 'S.SANDEEP [15-Feb-2018] -- START {#41} -- END
    '                            Dim strName As String = row.Item("firstname") & " " & row.Item("lastname")
    '                            strMessage.Append("<HTML> <BODY>")
    '                            strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & IIf(strName.Trim.Length > 0, strName, row.Item("username")) & "</B>, <BR><BR>")
    '                            strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that, Employee") & " " & "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>" & " " & _
    '                                              Language.getMessage(mstrModuleName, 10, "has done with final acknowledgement of assessment for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & ". " & _
    '                                              Language.getMessage(mstrModuleName, 11, "And it has been submitted to his Assessor and Reviewer.") & "<BR>")
    '                            strMessage.Append("<B>" & Language.getMessage(mstrModuleName, 12, "Assessor :") & iEAssessorName & "</B><BR>")
    '                            strMessage.Append("<B>" & Language.getMessage(mstrModuleName, 13, "Reviewer :") & iEReviewerName & "</B><BR>")
    '                            strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                            strMessage.Append("</span></p>")
    '                            strMessage.Append("</BODY></HTML>")

    '                            If strMessage.ToString.Trim.Length > 0 Then
    '                                Dim objSendMail As New clsSendMail
    '                                objSendMail._ToEmail = row.Item("email").ToString
    '                                objSendMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
    '                                objSendMail._Message = strMessage.ToString
    '                                objSendMail._Form_Name = strFormName
    '                                objSendMail._LogEmployeeUnkid = xLogEmpId
    '                                objSendMail._OperationModeId = xLoginMod
    '                                objSendMail._UserUnkid = xUserId
    '                                objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
    '                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                                Try
    '                                    objSendMail.SendMail(intcompanyunkid)
    '                                Catch ex As Exception
    '                                End Try
    '                                objSendMail = Nothing
    '                            End If
    '                        End If
    '                    Next
    '                End If
    '            End If
    '            '********************************* FOR SELECTED USER **************************** | END


    '            '********************************* FOR EMPLOYEE PART **************************** | START
    '            strMessage = New Text.StringBuilder()
    '            If objEmp._Email <> "" Then
    '                strMessage.Append("<HTML> <BODY>")
    '                strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>, <BR><BR>")
    '                strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 14, "This is to inform you that your final acknowledgement of assessment for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & ". " & _
    '                                  Language.getMessage(mstrModuleName, 15, "has been submitted to your Assessor and Reviewer.") & "<BR>")
    '                strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                strMessage.Append("</span></p>")
    '                strMessage.Append("</BODY></HTML>")

    '                If strMessage.ToString.Trim.Length > 0 Then
    '                    Dim objSendMail As New clsSendMail
    '                    objSendMail._ToEmail = objEmp._Email
    '                    objSendMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
    '                    objSendMail._Message = strMessage.ToString
    '                    objSendMail._Form_Name = strFormName
    '                    objSendMail._LogEmployeeUnkid = xLogEmpId
    '                    objSendMail._OperationModeId = xLoginMod
    '                    objSendMail._UserUnkid = xUserId
    '                    objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
    '                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                    Try
    '                        objSendMail.SendMail(intcompanyunkid)
    '                    Catch ex As Exception
    '                    End Try
    '                    objSendMail = Nothing
    '                End If
    '            End If
    '            '********************************* FOR EMPLOYEE PART **************************** | END

    '            objEmp = Nothing : objPrd = Nothing
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Public Sub Send_Notification(ByVal strTransactionIds As String, _
                                 ByVal intEmployeeId As Integer, _
                                 ByVal intPeriodId As Integer, _
                                 ByVal mdtEmpAsOnDate As DateTime, _
                                 ByVal xLoginMod As enLogin_Mode, _
                                 ByVal xUserId As Integer, _
                                 ByVal strCSVSelectedUserIds As String, _
                                 ByVal xLogEmpId As Integer, ByVal strFormName As String, ByVal strDBName As String, ByVal intcompanyunkid As Integer)
        Try

            'Gajanan [26-Feb-2019] -- Start
            'Enhancement - Email Language Changes For NMB.
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
            'Gajanan [26-Feb-2019] -- End


            Dim dslst As New DataSet : dslst = GetList(" ack.acknowledgementtranunkid in(" & strTransactionIds & ")")
            Dim strMessage As New Text.StringBuilder
            If dslst.Tables(0).Rows.Count > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim objPrd As New clscommom_period_Tran
                objEmp._Employeeunkid(mdtEmpAsOnDate) = intEmployeeId
                objPrd._Periodunkid(strDBName) = intPeriodId
                Dim objeval As New clsevaluation_analysis_master
                Dim dsDetails As New DataSet
                dsDetails = objeval.GetEmployee_AssessorReviewerDetails(intPeriodId, strDBName, Nothing, intEmployeeId)
                Dim iEAssessorName As String = "" : Dim iEReviewerName As String = "" : Dim iEAssessorEmail As String = "" : Dim iEReviewerEmail As String = ""
                If dsDetails.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = Nothing
                    If iEAssessorName.Trim.Length <= 0 Then
                        dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                        If dtmp.Length > 0 Then
                            iEAssessorName = dtmp(0).Item("arName")
                            iEAssessorEmail = dtmp(0).Item("arEmail")
                        Else
                            dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                            If dtmp.Length > 0 Then
                                iEAssessorName = dtmp(0).Item("arName")
                                iEAssessorEmail = dtmp(0).Item("arEmail")
                            End If
                        End If
                    End If
                    If iEReviewerName.Trim.Length <= 0 Then
                        dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 1")
                        If dtmp.Length > 0 Then
                            iEReviewerName = dtmp(0).Item("arName")
                            iEReviewerEmail = dtmp(0).Item("arEmail")
                        Else
                            dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")
                            If dtmp.Length > 0 Then
                                iEReviewerName = dtmp(0).Item("arName")
                                iEReviewerEmail = dtmp(0).Item("arEmail")
                            End If
                        End If
                    End If
                End If
                '********************************* ASSESSOR & REVIEWER **************************** | START
                For Each drow As DataRow In dslst.Tables(0).Rows
                    strMessage = New Text.StringBuilder
                    Dim strName As String = String.Empty
                    Dim strComment As String = String.Empty
                    Dim strStatus As String = String.Empty
                    Dim strEmail As String = String.Empty
                    If CInt(drow.Item("assessmodeid")) > 0 Then
                        Select Case CInt(drow.Item("assessmodeid"))
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                strName = iEAssessorName
                                strComment = drow.Item("acknowledgement_comments")
                                strStatus = drow.Item("ack_status")
                                strEmail = iEAssessorEmail
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                strName = iEReviewerName
                                strComment = drow.Item("acknowledgement_comments")
                                strStatus = drow.Item("ack_status")
                                strEmail = iEReviewerEmail
                        End Select
                        If strName.Trim().Length > 0 AndAlso strEmail.Trim.Length > 0 Then
                            strMessage.Append("<HTML> <BODY>")

                            'Gajanan [26-Feb-2019] -- Start
                            'Enhancement - Email Language Changes For NMB.
                            strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & info1.ToTitleCase(strName.ToLower()) & ", <BR><BR>")
                            'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & strName.TitleCase() & ", <BR><BR>")

                            'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to inform you that, I have done with assessing my final acknowledgement for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & " " & _
                            '                  Language.getMessage(mstrModuleName, 5, " with details below :") & "<BR><BR>")

                            'Pinkal (22-Mar-2019) -- Start
                            'Enhancement - Working on PA Changes FOR NMB.
                            'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 18, "Please receive my final acknowledgment for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & ".")
                            strMessage.Append(Language.getMessage(mstrModuleName, 18, "Please receive my final acknowledgment for the period of") & " " & "<B>(" & objPrd._Period_Name & ")</B>" & ".")
                            'Pinkal (22-Mar-2019) -- End


                            'Gajanan [26-Feb-2019] -- End

                            strMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
                            strMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
                            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 6, "Status") & "</span></b></TD>")
                            strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 7, "Comments") & "</span></b></TD>")
                            strMessage.Append("</TR>")
                            strMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                            strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strStatus & "</span></TD>")
                            strMessage.Append("<TD align = 'LEFT' WIDTH = '60%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strComment & "</span></TD>")
                            strMessage.Append("</TR>" & vbCrLf)
                            strMessage.Append("</TABLE>")
                            strMessage.Append("<BR>")
                            strMessage.Append(Language.getMessage(mstrModuleName, 17, "Regards,"))
                            strMessage.Append("<BR>")

                            'Gajanan [26-Feb-2019] -- Start
                            'Enhancement - Email Language Changes For NMB.
                            strMessage.Append(info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()))
                            'strMessage.Append((objEmp._Firstname & " " & objEmp._Surname).TitleCase())
                            'Gajanan [26-Feb-2019] -- End

                            strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                            strMessage.Append("</span></p>")
                            strMessage.Append("</BODY></HTML>")

                            If strMessage.ToString.Trim.Length > 0 Then
                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = strEmail
                                objSendMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
                                objSendMail._Message = strMessage.ToString
                                'objSendMail._FormName = strFormName
                                With objSendMail
                                    ._FormName = mstrFormName
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                objSendMail._LoginEmployeeunkid = mintLoginemployeeunkid
                                objSendMail._OperationModeId = xLoginMod
                                objSendMail._UserUnkid = xUserId
                                objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                                Try
                                    objSendMail.SendMail(intcompanyunkid)
                                Catch ex As Exception
                                End Try
                                objSendMail = Nothing
                            End If
                        End If
                    End If
                Next
                '********************************* ASSESSOR & REVIEWER **************************** | END


                '********************************* FOR SELECTED USER **************************** | START
                'strMessage = New Text.StringBuilder() 'S.SANDEEP [15-Feb-2018] -- START {#41} -- END
                If strCSVSelectedUserIds.Trim.Length > 0 Then
                    Dim dLst As New DataSet : Dim objUsr As New clsUserAddEdit
                    dLst = objUsr.GetList("List", "hrmsConfiguration..cfuser_master.userunkid IN (" & strCSVSelectedUserIds & ")")
                    If dslst.Tables("List").Rows.Count > 0 Then
                        For Each row As DataRow In dLst.Tables("List").Rows
                            If row.Item("email").ToString.Trim.Length > 0 Then
                                strMessage = New Text.StringBuilder() 'S.SANDEEP [15-Feb-2018] -- START {#41} -- END
                                Dim strName As String = row.Item("firstname") & " " & row.Item("lastname")
                                strMessage.Append("<HTML> <BODY>")

                                'Gajanan [26-Feb-2019] -- Start
                                'Enhancement - Email Language Changes For NMB.
                                strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & info1.ToTitleCase(IIf(strName.Trim.Length > 0, strName, row.Item("username")).ToString().ToLower()) & ", <BR><BR>")
                                'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & IIf(strName.Trim.Length > 0, strName, row.Item("username")).ToString.TitleCase() & ", <BR><BR>")



                                'Pinkal (22-Mar-2019) -- Start
                                'Enhancement - Working on PA Changes FOR NMB.
                                'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that, Employee") & " " & " " & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & " " & " ")
                                strMessage.Append(Language.getMessage(mstrModuleName, 16, "This is to inform you that, Employee") & " " & " " & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & " " & " ")
                                'Pinkal (22-Mar-2019) -- End


                                'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that, Employee") & " " & " " & (objEmp._Firstname & " " & objEmp._Surname).TitleCase() & " ")

                                'Language.getMessage(mstrModuleName, 10, "has done with final acknowledgement of assessment for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & ". " & _


                                'Pinkal (22-Mar-2019) -- Start
                                'Enhancement - Working on PA Changes FOR NMB.
                                'strMessage.Append(Language.getMessage(mstrModuleName, 19, "has acknowledged assessment for the period") & " " & "<B>" & objPrd._Period_Name & "</B>" & ".")
                                strMessage.Append(Language.getMessage(mstrModuleName, 19, "has acknowledged assessment for the period") & " " & "<B>(" & objPrd._Period_Name & ")</B>" & ".")
                                'Pinkal (22-Mar-2019) -- End



                                'Language.getMessage(mstrModuleName, 11, "And it has been submitted to his Assessor and Reviewer.") & "<BR>")

                                strMessage.Append("<BR><BR>")
                                strMessage.Append("<B>" & Language.getMessage(mstrModuleName, 12, "Assessor :") & "</B>" & info1.ToTitleCase(iEAssessorName.ToLower()) & "<BR>")
                                strMessage.Append("<B>" & Language.getMessage(mstrModuleName, 13, "Reviewer :") & "</B>" & info1.ToTitleCase(iEReviewerName.ToLower()) & "<BR>")
                                'strMessage.Append("<B>" & Language.getMessage(mstrModuleName, 12, "Assessor :") & "</B>" & iEAssessorName.TitleCase() & "<BR>")
                                'strMessage.Append("<B>" & Language.getMessage(mstrModuleName, 13, "Reviewer :") & "</B>" & iEReviewerName.TitleCase() & "<BR>")
                                'Gajanan [26-Feb-2019] -- End

                                strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                                strMessage.Append("</span></p>")
                                strMessage.Append("</BODY></HTML>")

                                If strMessage.ToString.Trim.Length > 0 Then
                                    Dim objSendMail As New clsSendMail
                                    objSendMail._ToEmail = row.Item("email").ToString
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
                                    objSendMail._Message = strMessage.ToString
                                    'objSendMail._FormName = strFormName
                                    With objSendMail
                                        ._FormName = mstrFormName
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
                                    objSendMail._LoginEmployeeunkid = mintLoginemployeeunkid
                                    objSendMail._OperationModeId = xLoginMod
                                    objSendMail._UserUnkid = xUserId
                                    objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
                                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                                    Try
                                        objSendMail.SendMail(intcompanyunkid)
                                    Catch ex As Exception
                                    End Try
                                    objSendMail = Nothing
                                End If
                            End If
                        Next
                    End If
                End If
                '********************************* FOR SELECTED USER **************************** | END


                '********************************* FOR EMPLOYEE PART **************************** | START
                strMessage = New Text.StringBuilder()
                If objEmp._Email <> "" Then
                    strMessage.Append("<HTML> <BODY>")

                    'Gajanan [26-Feb-2019] -- Start
                    'Enhancement - Email Language Changes For NMB.
                    strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()) & ", <BR><BR>")
                    'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & (objEmp._Firstname & " " & objEmp._Surname).TitleCase() & ", <BR><BR>")
                    'Gajanan [26-Feb-2019] -- End


                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on PA Changes FOR NMB.
                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 14, "This is to inform you that your final acknowledgement of assessment for the period of") & " " & "<B>" & objPrd._Period_Name & "</B>" & ". " & _
                    '                  Language.getMessage(mstrModuleName, 15, "has been submitted to your Assessor and Reviewer.") & "<BR>")

                    'S.SANDEEP |2-APRIL-2019| -- START
                    'strMessage.Append(Language.getMessage(mstrModuleName, 14, "This is to inform you that your final acknowledgement of assessment for the period of") & " " & "<B>(" & objPrd._Period_Name & ")</B>" & "." & _
                    strMessage.Append(Language.getMessage(mstrModuleName, 14, "This is to inform you that your final acknowledgement of assessment for the period of") & " " & "<B>(" & objPrd._Period_Name & ")</B>" & " " & _
                                      Language.getMessage(mstrModuleName, 15, "has been submitted to your Assessor and Reviewer.") & "<BR>")
                    'S.SANDEEP |2-APRIL-2019| -- END
                    'Pinkal (22-Mar-2019) -- End

                    
                    strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                    strMessage.Append("</span></p>")
                    strMessage.Append("</BODY></HTML>")

                    If strMessage.ToString.Trim.Length > 0 Then
                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = objEmp._Email
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
                        objSendMail._Message = strMessage.ToString
                        'objSendMail._FormName = strFormName
                        With objSendMail
                            ._FormName = mstrFormName
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objSendMail._LoginEmployeeunkid = mintLoginemployeeunkid
                        objSendMail._OperationModeId = xLoginMod
                        objSendMail._UserUnkid = xUserId
                        objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        Try
                            objSendMail.SendMail(intcompanyunkid)
                        Catch ex As Exception
                        End Try
                        objSendMail = Nothing
                    End If
                End If
                '********************************* FOR EMPLOYEE PART **************************** | END

                objEmp = Nothing : objPrd = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-FEB-2019| -- END

    'S.SANDEEP [29-NOV-2017] -- END

#End Region

#Region " Private Function/Procedures "

    Private Function InsertAuditTrails(ByVal eType As enAuditType, ByVal objDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Try
            objDataOpr.ClearParameters()
            StrQ = "INSERT INTO athrassess_acknowledgement_tran " & _
                   "( " & _
                         " acknowledgementtranunkid " & _
                         ",employeeunkid " & _
                         ",periodunkid " & _
                         ",analysisunkid " & _
                         ",statusunkid " & _
                         ",acknowledgement_comments " & _
                         ",acknowledgement_date " & _
                         ",auditdatetime " & _
                         ",audituserunkid " & _
                         ",audittype " & _
                         ",ip " & _
                         ",host " & _
                         ",form_name " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         ",isweb " & _
                         ",loginemployeeunkid " & _
                    ") " & _
                    "VALUES " & _
                    "( " & _
                         " @acknowledgementtranunkid " & _
                         ",@employeeunkid " & _
                         ",@periodunkid " & _
                         ",@analysisunkid " & _
                         ",@statusunkid " & _
                         ",@acknowledgement_comments " & _
                         ",@acknowledgement_date " & _
                         ",GETDATE() " & _
                         ",@audituserunkid " & _
                         ",@audittype " & _
                         ",@ip " & _
                         ",@host " & _
                         ",@form_name " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         " " & _
                         ",@isweb " & _
                         ",@loginemployeeunkid " & _
                    ") "
            objDataOpr.AddParameter("@acknowledgementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAcknowledgementtranunkid)
            objDataOpr.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOpr.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
            objDataOpr.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
            objDataOpr.AddParameter("@acknowledgement_comments", SqlDbType.NVarChar, mstrAcknowledgement_comments.Length, mstrAcknowledgement_comments)
            objDataOpr.AddParameter("@acknowledgement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcknowledgement_date)
            objDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eType)
            objDataOpr.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOpr.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOpr.ExecNonQuery(StrQ)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "Agree")
            Language.setMessage(mstrModuleName, 3, "Disagree")
            Language.setMessage(mstrModuleName, 6, "Status")
            Language.setMessage(mstrModuleName, 7, "Comments")
            Language.setMessage(mstrModuleName, 8, "Notification of Assessment Acknowledgement")
            Language.setMessage(mstrModuleName, 12, "Assessor :")
            Language.setMessage(mstrModuleName, 13, "Reviewer :")
            Language.setMessage(mstrModuleName, 14, "This is to inform you that your final acknowledgement of assessment for the period of")
			Language.setMessage(mstrModuleName, 15, "has been submitted to your Assessor and Reviewer.")
            Language.setMessage(mstrModuleName, 16, "This is to inform you that, Employee")
			Language.setMessage(mstrModuleName, 17, "Regards,")
			Language.setMessage(mstrModuleName, 18, "Please receive my final acknowledgment for the period of")
			Language.setMessage(mstrModuleName, 19, "has acknowledged assessment for the period")
            Language.setMessage(mstrModuleName, 100, "Dear")
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
