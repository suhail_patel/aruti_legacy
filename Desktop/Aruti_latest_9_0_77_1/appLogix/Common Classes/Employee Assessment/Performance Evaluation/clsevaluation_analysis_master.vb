﻿'************************************************************************************************************************************
'Class Name :clscopetency_analysis_master.vb
'Purpose    :
'Date       :01-Sep-2014
'Written By :Sandeep J. Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System
Imports System.Globalization

''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsevaluation_analysis_master
    Private Shared ReadOnly mstrModuleName As String = "clsevaluation_analysis_master"
    Private mstrMessage As String = ""
    Private objGoalAnalysisTran As New clsgoal_analysis_tran
    Private objAnalysisTran As New clscompetency_analysis_tran
    Private objCustomTran As New clscompeteny_customitem_tran
    'S.SANDEEP [27-APR-2017] -- START
    'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
    Private objComputeScore As New clsComputeScore_master
    Private objComputeTran As New clsComputeScore_tran
    Private mdtComputeTran As DataTable
    'S.SANDEEP [27-APR-2017] -- END

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private objPDPFormTran As New clspdpform_tran
    Private objpdpItemMaster As New clspdpitem_master
    'S.SANDEEP |03-MAY-2021| -- END

#Region " Private variables "

    Private mintAnalysisunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintSelfemployeeunkid As Integer
    Private mintAssessormasterunkid As Integer
    Private mintAssessoremployeeunkid As Integer
    Private mintAssessedemployeeunkid As Integer
    Private mdtAssessmentdate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIscommitted As Boolean
    Private mintReviewerunkid As Integer
    Private mintAssessmodeid As Integer
    Private mintExt_Assessorunkid As Integer
    Private mdtCommitteddatetime As Date
    Private mstrWebFrmName As String = String.Empty
    'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Private mintEvalTypeId As Integer = 0
    'S.SANDEEP |13-NOV-2020| -- END
Private mstrDatabaseName As String = ""
#End Region

    'S.SANDEEP [18 DEC 2015] -- START
#Region " Public Enum "

    Public Enum enAssessmentType
        ASSESSMENT_NOT_DONE = 1     'NOT DONE AT ALL
        ASSESSMENT_ON_GOING = 2     'STARTED BUT NOT COMPLETED/NOT COMMITTED
        ASSESSMENT_COMMITTED = 3    'COMMITTED
    End Enum

    'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Public Enum enPAEvalTypeId
        EO_NONE = 0
        EO_BOTH = 1
        EO_SCORE_CARD = 2
        EO_COMPETENCE = 3
    End Enum
    'S.SANDEEP |13-NOV-2020| -- END

#End Region
    'S.SANDEEP [18 DEC 2015] -- END

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Analysisunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'Call GetData()
            Call GetData(xDataOpr)
            'S.SANDEEP [27 DEC 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selfemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Selfemployeeunkid() As Integer
        Get
            Return mintSelfemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintSelfemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessoremployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessoremployeeunkid() As Integer
        Get
            Return mintAssessoremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessoremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessedemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessedemployeeunkid() As Integer
        Get
            Return mintAssessedemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessedemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmentdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessmentdate() As Date
        Get
            Return mdtAssessmentdate
        End Get
        Set(ByVal value As Date)
            mdtAssessmentdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscommitted
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscommitted() As Boolean
        Get
            Return mblnIscommitted
        End Get
        Set(ByVal value As Boolean)
            mblnIscommitted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewerunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reviewerunkid() As Integer
        Get
            Return mintReviewerunkid
        End Get
        Set(ByVal value As Integer)
            mintReviewerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmodeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessmodeid() As Integer
        Get
            Return mintAssessmodeid
        End Get
        Set(ByVal value As Integer)
            mintAssessmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ext_assessorunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ext_Assessorunkid() As Integer
        Get
            Return mintExt_Assessorunkid
        End Get
        Set(ByVal value As Integer)
            mintExt_Assessorunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set committeddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Committeddatetime() As Date
        Get
            Return mdtCommitteddatetime
        End Get
        Set(ByVal value As Date)
            mdtCommitteddatetime = value
        End Set
    End Property

    'Public WriteOnly Property _WebFrmName() As String
    '    Set(ByVal value As String)
    '        mstrWebFrmName = value
    '    End Set
    'End Property

'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Public Property _EvalTypeId() As Integer
        Get
            Return mintEvalTypeId
        End Get
        Set(ByVal value As Integer)
            mintEvalTypeId = value
            If mintEvalTypeId <= 0 Then mintEvalTypeId = CInt(enPAEvalTypeId.EO_BOTH)
        End Set
    End Property
    'S.SANDEEP |13-NOV-2020| -- END

Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing) 'S.SANDEEP [27 DEC 2016] -- START {} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [27 DEC 2016] -- START
        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27 DEC 2016] -- END

        Try
            strQ = "SELECT " & _
              "  analysisunkid " & _
              ", periodunkid " & _
              ", selfemployeeunkid " & _
              ", assessormasterunkid " & _
              ", assessoremployeeunkid " & _
              ", assessedemployeeunkid " & _
              ", assessmentdate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", iscommitted " & _
              ", reviewerunkid " & _
              ", assessmodeid " & _
              ", ext_assessorunkid " & _
              ", committeddatetime " & _
              ", evaltypeid " & _
             "FROM hrevaluation_analysis_master " & _
             "WHERE analysisunkid = @analysisunkid "
            'S.SANDEEP |13-NOV-2020| -- START {evaltypeid} -- END


            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAnalysisunkid = CInt(dtRow.Item("analysisunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintSelfemployeeunkid = CInt(dtRow.Item("selfemployeeunkid"))
                mintAssessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                mintAssessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
                mintAssessedemployeeunkid = CInt(dtRow.Item("assessedemployeeunkid"))
                mdtAssessmentdate = dtRow.Item("assessmentdate")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIscommitted = CBool(dtRow.Item("iscommitted"))
                mintReviewerunkid = CInt(dtRow.Item("reviewerunkid"))
                mintAssessmodeid = CInt(dtRow.Item("assessmodeid"))
                mintExt_Assessorunkid = CInt(dtRow.Item("ext_assessorunkid"))
                If IsDBNull(dtRow.Item("committeddatetime")) Then
                    mdtCommitteddatetime = Nothing
                Else
                    mdtCommitteddatetime = dtRow.Item("committeddatetime")
                End If
                'S.SANDEEP |13-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : COMPETENCIES
                mintEvalTypeId = CInt(dtRow.Item("evaltypeid"))
                'S.SANDEEP |13-NOV-2020| -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [27 DEC 2016] -- END
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma (Shani [09 Mar 2016])
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal enAssessMode As enAssessmentMode, _
                            ByVal intPeriodId As Integer, _
                            ByVal mblnIsCalibrationSettingActive As Boolean, _
                            Optional ByVal strFilterString As String = "", _
                            Optional ByVal xInculde_UAC As Boolean = True) As DataSet 'S.SANDEEP |27-MAY-2019| -- START {mblnIsCalibrationSettingActive} -- END
        'S.SANDEEP [27-APR-2017] -- START {mblnIsCalibrationSettingActive} -- END


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'Using objDo As New clsDataOperation
            '    StrQ = "SELECT " & _
            '           "     hrevaluation_analysis_master.analysisunkid " & _
            '           "    ,hrevaluation_analysis_master.periodunkid " & _
            '           "    ,hrevaluation_analysis_master.selfemployeeunkid " & _
            '           "    ,hrevaluation_analysis_master.assessormasterunkid " & _
            '           "    ,hrevaluation_analysis_master.assessoremployeeunkid " & _
            '           "    ,hrevaluation_analysis_master.assessedemployeeunkid " & _
            '           "    ,CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) AS assessmentdate " & _
            '           "    ,hrevaluation_analysis_master.userunkid " & _
            '           "    ,hrevaluation_analysis_master.isvoid " & _
            '           "    ,hrevaluation_analysis_master.voiduserunkid " & _
            '           "    ,hrevaluation_analysis_master.voiddatetime " & _
            '           "    ,hrevaluation_analysis_master.voidreason " & _
            '           "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
            '           "    ,ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
            '           "    ,ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
            '           "    ,cfcommon_period_tran.yearunkid " & _
            '           "    ,ISNULL(iscommitted,0) AS iscommitted " & _
            '           "    ,ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
            '           "    ,cfcommon_period_tran.statusid AS Sid " & _
            '           "    ,hremployee_master.stationunkid " & _
            '           "    ,hremployee_master.deptgroupunkid " & _
            '           "    ,hremployee_master.departmentunkid " & _
            '           "    ,hremployee_master.sectionunkid " & _
            '           "    ,hremployee_master.unitunkid " & _
            '           "    ,hremployee_master.jobgroupunkid " & _
            '           "    ,hremployee_master.jobunkid " & _
            '           "    ,hremployee_master.gradegroupunkid " & _
            '           "    ,hremployee_master.gradeunkid " & _
            '           "    ,hremployee_master.gradelevelunkid " & _
            '           "    ,hremployee_master.classgroupunkid " & _
            '           "    ,hremployee_master.classunkid " & _
            '           "    ,hremployee_master.sectiongroupunkid " & _
            '           "    ,hremployee_master.unitgroupunkid " & _
            '           "    ,hremployee_master.teamunkid " & _
            '           "    ,hrevaluation_analysis_master.committeddatetime " & _
            '           "    ,ISNULL(rscore,0) rscore " & _
            '           "    ,ISNULL(smode,'') AS smode " & _
            '           "    ,ISNULL(smodeid,0) AS smodeid " & _
            '           "    ,ISNULL(assessgroupunkid,0) AS assessgroupunkid "
            '    Select Case enAssessMode
            '        Case enAssessmentMode.SELF_ASSESSMENT
            '            StrQ &= "   ,' ' as Assessor "
            '        Case enAssessmentMode.APPRAISER_ASSESSMENT
            '            StrQ &= "   ,CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Assessor " & _
            '                    "   ,hrapprover_usermapping.userunkid AS mapuserid "
            '        Case enAssessmentMode.REVIEWER_ASSESSMENT
            '            StrQ &= "   ,ISNULL(Reviewer.firstname,'')+' '+ISNULL(Reviewer.othername,'')+' '+ISNULL(Reviewer.surname,'') AS Reviewer " & _
            '                    "   ,hrapprover_usermapping.userunkid AS mapuserid "
            '    End Select

            '    StrQ &= "FROM hrevaluation_analysis_master " & _
            '            " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrevaluation_analysis_master.periodunkid " & _
            '            " JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
            '            " LEFT JOIN " & _
            '            " ( " & _
            '            "       SELECT " & _
            '            "            analysisunkid AS banalysisunkid " & _
            '            "           ,SUM(result) AS rscore " & _
            '            "           ,@BSC AS smode " & _
            '            "           ,1 AS smodeid " & _
            '            "           ,0 AS assessgroupunkid " & _
            '            "       FROM hrgoals_analysis_tran " & _
            '            "       WHERE isvoid = 0 GROUP BY analysisunkid " & _
            '            "   UNION ALL " & _
            '            "       SELECT " & _
            '            "            hrcompetency_analysis_tran.analysisunkid AS banalysisunkid " & _
            '            "           ,SUM(result) AS rscore " & _
            '            "           ,hrassess_group_master.assessgroup_name AS smode " & _
            '            "           ,2 AS smodeid " & _
            '            "           ,hrcompetency_analysis_tran.assessgroupunkid " & _
            '            "       FROM hrcompetency_analysis_tran " & _
            '            "           JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
            '            "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '            "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '            "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '            "           AND hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '            "       WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 " & _
            '            "       GROUP BY hrcompetency_analysis_tran.analysisunkid,hrassess_group_master.assessgroup_name,hrcompetency_analysis_tran.assessgroupunkid " & _
            '            " ) AS BSC ON BSC.banalysisunkid = hrevaluation_analysis_master.analysisunkid "

            '    Select Case enAssessMode
            '        Case enAssessmentMode.SELF_ASSESSMENT
            '            StrQ &= " JOIN hremployee_master ON hrevaluation_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid "

            '            If xDateJoinQry.Trim.Length > 0 Then
            '                StrQ &= xDateJoinQry
            '            End If

            '            If xUACQry.Trim.Length > 0 Then
            '                StrQ &= xUACQry
            '            End If

            '            If xAdvanceJoinQry.Trim.Length > 0 Then
            '                StrQ &= xAdvanceJoinQry
            '            End If

            '        Case enAssessmentMode.APPRAISER_ASSESSMENT
            '            StrQ &= " JOIN hremployee_master ON hrevaluation_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
            '                    " LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrevaluation_analysis_master.ext_assessorunkid " & _
            '                    " LEFT JOIN hremployee_master AS AEMP ON AEMP.employeeunkid = hrevaluation_analysis_master.assessoremployeeunkid AND hrevaluation_analysis_master.ext_assessorunkid = 0 " & _
            '                    " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
            '                    " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

            '            If xDateJoinQry.Trim.Length > 0 Then
            '                'StrQ &= xDateJoinQry.Replace("hremployee_master", "AEMP")
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xDateJoinQry, "\bhremployee_master\b", "AEMP")
            '            End If

            '            If xUACQry.Trim.Length > 0 Then
            '                'StrQ &= xUACQry
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xUACQry, "\bhremployee_master\b", "AEMP")
            '            End If

            '            If xAdvanceJoinQry.Trim.Length > 0 Then
            '                'StrQ &= xAdvanceJoinQry
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xAdvanceJoinQry, "\bhremployee_master\b", "AEMP")
            '            End If

            '        Case enAssessmentMode.REVIEWER_ASSESSMENT
            '            StrQ &= " JOIN hremployee_master ON hrevaluation_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
            '                    " JOIN hremployee_master AS Reviewer ON hrevaluation_analysis_master.reviewerunkid = Reviewer.employeeunkid " & _
            '                    " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
            '                    " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

            '            If xDateJoinQry.Trim.Length > 0 Then
            '                'StrQ &= xDateJoinQry.Replace("hremployee_master", "AEMP")
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xDateJoinQry, "\bhremployee_master\b", "Reviewer")
            '            End If

            '            If xUACQry.Trim.Length > 0 Then
            '                'StrQ &= xUACQry
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xUACQry, "\bhremployee_master\b", "Reviewer")
            '            End If

            '            If xAdvanceJoinQry.Trim.Length > 0 Then
            '                'StrQ &= xAdvanceJoinQry
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xAdvanceJoinQry, "\bhremployee_master\b", "Reviewer")
            '            End If

            '    End Select

            '    StrQ &= " WHERE hrevaluation_analysis_master.isvoid = 0 "

            'Select Case enAssessMode
            '    Case enAssessmentMode.SELF_ASSESSMENT
            '        If xUACFiltrQry.Trim.Length > 0 Then
            '            StrQ &= " AND " & xUACFiltrQry & " "
            '        End If

            '        If xIncludeIn_ActiveEmployee = False Then
            '            If xDateFilterQry.Trim.Length > 0 Then
            '                StrQ &= xDateFilterQry & " "
            '            End If
            '        End If
            '    Case enAssessmentMode.APPRAISER_ASSESSMENT

            '        'S.SANDEEP [29 OCT 2015] -- START
            '        'ISSUE : ASSESSOR NOT ABLE TO SEE HIS EMPLOYEE IF DEPARTMENT IS DIFFERENT AS OF USER ACCESS VOLTAMP
            '        'If xUACFiltrQry.Trim.Length > 0 Then
            '        '    'StrQ &= " AND " & xUACFiltrQry & " "
            '        '    StrQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "AEMP") & " "
            '        'End If
            '        'S.SANDEEP [29 OCT 2015] -- END

            '        If xIncludeIn_ActiveEmployee = False Then
            '            If xDateFilterQry.Trim.Length > 0 Then
            '                'StrQ &= xDateFilterQry & " "
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xDateFilterQry, "\bhremployee_master\b", "AEMP") & " "
            '            End If
            '        End If
            '    Case enAssessmentMode.REVIEWER_ASSESSMENT
            '        'S.SANDEEP [29 OCT 2015] -- START
            '        'ISSUE : ASSESSOR NOT ABLE TO SEE HIS EMPLOYEE IF DEPARTMENT IS DIFFERENT AS OF USER ACCESS VOLTAMP
            '        'If xUACFiltrQry.Trim.Length > 0 Then
            '        '    'StrQ &= " AND " & xUACFiltrQry & " "
            '        '    StrQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "Reviewer") & " "
            '        'End If
            '        'S.SANDEEP [29 OCT 2015] -- START

            '        If xIncludeIn_ActiveEmployee = False Then
            '            If xDateFilterQry.Trim.Length > 0 Then
            '                'StrQ &= xDateFilterQry & " "
            '                StrQ &= System.Text.RegularExpressions.Regex.Replace(xDateFilterQry, "\bhremployee_master\b", "Reviewer") & " "
            '            End If
            '        End If
            'End Select

            '    Select Case enAssessMode
            '        Case enAssessmentMode.APPRAISER_ASSESSMENT
            '            StrQ &= " AND CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END <> '' "
            '    End Select

            '    If strFilterString.Trim.Length > 0 Then
            '        StrQ &= "AND " & strFilterString
            '    End If

            '    objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Balanced Score Card"))

            '    dsList = objDo.ExecQuery(StrQ, strTableName)

            '    If objDo.ErrorMessage <> "" Then
            '        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
            '        Throw exForce
            '    End If

            'End Using


            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim iFactor As Decimal = 0
            Using objDo As New clsDataOperation
                StrQ = "SELECT TOP 1 " & _
                       "    @val = CAST(c.key_value AS DECIMAL(36,4)) " & _
                       "FROM cfcommon_period_tran cpt WITH (NOLOCK) " & _
                       "    JOIN hrmsConfiguration..cffinancial_year_tran cyt WITH (NOLOCK) ON cpt.yearunkid = cyt.yearunkid " & _
                       "    JOIN hrmsConfiguration..cfconfiguration c WITH (NOLOCK) ON cyt.companyunkid = c.companyunkid " & _
                       "WHERE cpt.modulerefid = 5 AND cpt.periodunkid = '" & intPeriodId & "' AND UPPER(c.[key_name]) = 'PASCORINGROUDINGFACTOR' "

                objDo.ClearParameters()
                objDo.AddParameter("@val", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iFactor, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
                Try
                    iFactor = objDo.GetParameterValue("@val")
                Catch ex As Exception
                    iFactor = 0
                End Try
            End Using
            'S.SANDEEP |24-DEC-2019| -- END

            Using objDo As New clsDataOperation

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'StrQ = "SELECT " & _
                '       "     hrevaluation_analysis_master.analysisunkid " & _
                '       "    ,hrevaluation_analysis_master.periodunkid " & _
                '       "    ,hrevaluation_analysis_master.selfemployeeunkid " & _
                '       "    ,hrevaluation_analysis_master.assessormasterunkid " & _
                '       "    ,hrevaluation_analysis_master.assessoremployeeunkid " & _
                '       "    ,hrevaluation_analysis_master.assessedemployeeunkid " & _
                '       "    ,CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) AS assessmentdate " & _
                '       "    ,hrevaluation_analysis_master.userunkid " & _
                '       "    ,hrevaluation_analysis_master.isvoid " & _
                '       "    ,hrevaluation_analysis_master.voiduserunkid " & _
                '       "    ,hrevaluation_analysis_master.voiddatetime " & _
                '       "    ,hrevaluation_analysis_master.voidreason " & _
                '       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                '       "    ,ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                '       "    ,ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                '       "    ,cfcommon_period_tran.yearunkid " & _
                '       "    ,ISNULL(iscommitted,0) AS iscommitted " & _
                '       "    ,ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
                '       "    ,cfcommon_period_tran.statusid AS Sid " & _
                '       "    ,hremployee_master.stationunkid " & _
                '       "    ,hremployee_master.deptgroupunkid " & _
                '       "    ,hremployee_master.departmentunkid " & _
                '       "    ,hremployee_master.sectionunkid " & _
                '       "    ,hremployee_master.unitunkid " & _
                '       "    ,hremployee_master.jobgroupunkid " & _
                '       "    ,hremployee_master.jobunkid " & _
                '       "    ,hremployee_master.gradegroupunkid " & _
                '       "    ,hremployee_master.gradeunkid " & _
                '       "    ,hremployee_master.gradelevelunkid " & _
                '       "    ,hremployee_master.classgroupunkid " & _
                '       "    ,hremployee_master.classunkid " & _
                '       "    ,hremployee_master.sectiongroupunkid " & _
                '       "    ,hremployee_master.unitgroupunkid " & _
                '       "    ,hremployee_master.teamunkid " & _
                '       "    ,hrevaluation_analysis_master.committeddatetime " & _
                '       "    ,ISNULL(rscore,0) rscore " & _
                '       "    ,ISNULL(smode,'') AS smode " & _
                '       "    ,ISNULL(smodeid,0) AS smodeid " & _
                '       "    ,ISNULL(assessgroupunkid,0) AS assessgroupunkid "

                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " " & _
                       "SELECT " & _
                       "     hrevaluation_analysis_master.analysisunkid " & _
                       "    ,hrevaluation_analysis_master.periodunkid " & _
                       "    ,hrevaluation_analysis_master.selfemployeeunkid " & _
                       "    ,hrevaluation_analysis_master.assessormasterunkid " & _
                       "    ,hrevaluation_analysis_master.assessoremployeeunkid " & _
                       "    ,hrevaluation_analysis_master.assessedemployeeunkid " & _
                       "    ,CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) AS assessmentdate " & _
                       "    ,hrevaluation_analysis_master.userunkid " & _
                       "    ,hrevaluation_analysis_master.isvoid " & _
                       "    ,hrevaluation_analysis_master.voiduserunkid " & _
                       "    ,hrevaluation_analysis_master.voiddatetime " & _
                       "    ,hrevaluation_analysis_master.voidreason " & _
                       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                       "    ,ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                       "    ,ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                       "    ,cfcommon_period_tran.yearunkid " & _
                       "    ,ISNULL(iscommitted,0) AS iscommitted " & _
                       "    ,ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
                       "    ,cfcommon_period_tran.statusid AS Sid " & _
                       "    ,hremployee_master.stationunkid " & _
                       "    ,hremployee_master.deptgroupunkid " & _
                       "    ,hremployee_master.departmentunkid " & _
                       "    ,hremployee_master.sectionunkid " & _
                       "    ,hremployee_master.unitunkid " & _
                       "    ,hremployee_master.jobgroupunkid " & _
                       "    ,hremployee_master.jobunkid " & _
                       "    ,hremployee_master.gradegroupunkid " & _
                       "    ,hremployee_master.gradeunkid " & _
                       "    ,hremployee_master.gradelevelunkid " & _
                       "    ,hremployee_master.classgroupunkid " & _
                       "    ,hremployee_master.classunkid " & _
                       "    ,hremployee_master.sectiongroupunkid " & _
                       "    ,hremployee_master.unitgroupunkid " & _
                       "    ,hremployee_master.teamunkid " & _
                       "    ,hrevaluation_analysis_master.committeddatetime " & _
                       "    ,ISNULL(rscore,0) rscore " & _
                       "    ,ISNULL(smode,'') AS smode " & _
                       "    ,ISNULL(smodeid,0) AS smodeid " & _
                       "    ,ISNULL(assessgroupunkid,0) AS assessgroupunkid "
                'S.SANDEEP |27-MAY-2019| -- END



                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                StrQ &= "   ,CASE WHEN BSC.smodeid = 1 THEN " & _
                        "          CASE WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE & " " & _
                        "               WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE & " " & _
                        "               WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE & " " & _
                        "          END " & _
                        "         WHEN BSC.smodeid = 2 THEN " & _
                        "          CASE WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE & " " & _
                        "               WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE & " " & _
                        "               WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE & " " & _
                        "          END " & _
                        "   END AS ftypeid " & _
                        "   ,ISNULL(CAST(Scr.formula_value AS DECIMAL(36,2)),0) AS total_score " & _
                        "   ,ISNULL(Scr.competency_value,'') AS competency_value " & _
                        "   ,hrevaluation_analysis_master.evaltypeid "
                'S.SANDEEP |13-NOV-2020| -- START {evaltypeid} -- END

                'S.SANDEEP [27-APR-2017] -- END




                'S.SANDEEP |16-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                '------------------ REMOVED : ISNULL(CAST(Scr.formula_value AS DECIMAL(10,2)),0) AS total_score
                '------------------ ADDED   : ISNULL(CAST(Scr.formula_value AS DECIMAL(36,2)),0) AS total_score
                'S.SANDEEP |16-AUG-2019| -- END


                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= "   ,' ' as Assessor "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= "   ,CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE #ASS_NAME# END AS Assessor " & _
                                "   ,hrapprover_usermapping.userunkid AS mapuserid "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= "   ,#ASS_NAME# AS Reviewer " & _
                                "   ,hrapprover_usermapping.userunkid AS mapuserid "
                End Select

                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                'StrQ &= "FROM hrevaluation_analysis_master " & _
                '        " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrevaluation_analysis_master.periodunkid " & _
                '        " JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
                '        " JOIN " & _
                '        " ( " & _
                '        "       SELECT " & _
                '        "            hrgoals_analysis_tran.analysisunkid AS banalysisunkid " & _
                '        "           ,SUM(result) AS rscore " & _
                '        "           ,@BSC AS smode " & _
                '        "           ,1 AS smodeid " & _
                '        "           ,0 AS assessgroupunkid " & _
                '        "       FROM hrgoals_analysis_tran " & _
                '        "           JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                '        "       WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode & " GROUP BY hrgoals_analysis_tran.analysisunkid " & _
                '        "   UNION ALL " & _
                '        "       SELECT " & _
                '        "            hrcompetency_analysis_tran.analysisunkid AS banalysisunkid " & _
                '        "           ,SUM(result) AS rscore " & _
                '        "           ,hrassess_group_master.assessgroup_name AS smode " & _
                '        "           ,2 AS smodeid " & _
                '        "           ,hrcompetency_analysis_tran.assessgroupunkid " & _
                '        "       FROM hrcompetency_analysis_tran " & _
                '        "           JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                '        "           AND hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                '        "       WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode & " " & _
                '        "       GROUP BY hrcompetency_analysis_tran.analysisunkid,hrassess_group_master.assessgroup_name,hrcompetency_analysis_tran.assessgroupunkid " & _
                '        " ) AS BSC ON BSC.banalysisunkid = hrevaluation_analysis_master.analysisunkid "

                StrQ &= "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                        " JOIN cfcommon_period_tran WITH (NOLOCK) on cfcommon_period_tran.periodunkid = hrevaluation_analysis_master.periodunkid " & _
                        " JOIN hrmsConfiguration..cffinancial_year_tran WITH (NOLOCK) on hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
                        " LEFT JOIN " & _
                        " ( " & _
                        "       SELECT DISTINCT " & _
                        "            hrgoals_analysis_tran.analysisunkid AS banalysisunkid " & _
                        "           ,0 AS rscore " & _
                        "           ,@BSC AS smode " & _
                        "           ,1 AS smodeid " & _
                        "           ,0 AS assessgroupunkid " & _
                        "       FROM hrgoals_analysis_tran WITH (NOLOCK) " & _
                        "           JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                        "       WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode & " " & _
                        "           AND hrevaluation_analysis_master.periodunkid = @Pid " & _
                        "   UNION ALL " & _
                        "       SELECT DISTINCT " & _
                        "            hrcompetency_analysis_tran.analysisunkid AS banalysisunkid " & _
                        "           ,0 AS rscore " & _
                        "           ,hrassess_group_master.assessgroup_name AS smode " & _
                        "           ,2 AS smodeid " & _
                        "           ,hrcompetency_analysis_tran.assessgroupunkid " & _
                        "       FROM hrcompetency_analysis_tran WITH (NOLOCK) " & _
                        "           JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                        "           JOIN hrassess_group_master WITH (NOLOCK) ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                        "           JOIN hrassess_competence_assign_master WITH (NOLOCK) ON hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                        "           JOIN hrassess_competence_assign_tran WITH (NOLOCK) ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                        "           AND hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid AND hrassess_competence_assign_master.periodunkid = hrevaluation_analysis_master.periodunkid " & _
                        "       WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode & " " & _
                        "           AND hrevaluation_analysis_master.periodunkid = @Pid " & _
                        " ) AS BSC ON BSC.banalysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        hrassess_compute_score_master.analysisunkid " & _
                        "       ,hrassess_compute_score_master.employeeunkid " & _
                        "       ,CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                        "             WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                        "             WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                        "        END AS finaloverallscore " & _
                        "       ,hrassess_compute_score_tran.formula_value " & _
                        "       ,Fr.formula_typeid " & _
                        "       ,hrassess_compute_score_tran.competency_value " & _
                        "   FROM hrassess_compute_score_master WITH (NOLOCK) " & _
                        "       JOIN hrassess_compute_score_tran WITH (NOLOCK) ON hrassess_compute_score_master.computescoremasterunkid = hrassess_compute_score_tran.computescoremasterunkid " & _
                        "       JOIN " & _
                        "       ( " & _
                        "          SELECT " & _
                        "               hrassess_computation_master.computationunkid " & _
                        "              ,hrassess_computation_master.formula_typeid " & _
                        "              ,hrassess_computation_master.periodunkid " & _
                        "          FROM hrassess_computation_master WITH (NOLOCK) " & _
                        "          WHERE hrassess_computation_master.periodunkid = @Pid AND hrassess_computation_master.isvoid = 0 " & _
                        "       )AS Fr ON Fr.periodunkid = hrassess_compute_score_master.periodunkid AND Fr.computationunkid = hrassess_compute_score_tran.computationunkid " & _
                        "   WHERE hrassess_compute_score_master.isvoid = 0 AND hrassess_compute_score_tran.isvoid = 0 " & _
                        "       AND hrassess_compute_score_master.periodunkid = @Pid AND hrassess_compute_score_master.assessmodeid = " & enAssessMode & " " & _
                        ") AS Scr ON Scr.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                        "  AND Scr.formula_typeid = " & _
                        "       CASE WHEN BSC.smodeid = 1 THEN " & _
                        "               CASE WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE & " " & _
                        "                    WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE & " " & _
                        "                    WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE & " " & _
                        "               END " & _
                        "            WHEN BSC.smodeid = 2 THEN " & _
                        "               CASE WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE & " " & _
                        "                    WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE & " " & _
                        "                    WHEN hrevaluation_analysis_master.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN " & enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE & " " & _
                        "               END " & _
                        "END "

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'REMOVED  ----------------- hrassess_compute_score_master.finaloverallscore
                '---------------------- ADDED -------------------------
                '"       ,CASE WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 1 THEN hrassess_compute_score_master.calibrated_score " & _
                '"             WHEN @Setting = 1 AND ISNULL(hrassess_compute_score_master.isprocessed,0) = 0 THEN 0 " & _
                '"             WHEN @Setting = 0 THEN hrassess_compute_score_master.finaloverallscore " & _
                '"        END AS finaloverallscore " & _
                '---------------------- ADDED -------------------------
                'S.SANDEEP |27-MAY-2019| -- END

                'S.SANDEEP [27-APR-2017] -- END



                'Shani(19-APR-2016) -- Start
                'Get Assessor Wise Employee Given by Andrew / matthew
                '   CHANGED LEFT JOIN OF BSC TABLE ABOVE TO JOIN
                '   JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid -- ADDED
                '   AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode & " -- ADDED
                'Shani(19-APR-2016) -- End 


                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= " JOIN hremployee_master WITH (NOLOCK) ON hrevaluation_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid "

                        'Shani(19-APR-2016) -- Start
                        'Get Assessor Wise Employee Given by Andrew / matthew
                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xDateJoinQry
                        'End If
                        'If xUACQry.Trim.Length > 0 Then
                        '    StrQ &= xUACQry
                        'End If
                        'If xAdvanceJoinQry.Trim.Length > 0 Then
                        '    StrQ &= xAdvanceJoinQry
                        'End If
                        If xInculde_UAC Then
                            If xAdvanceJoinQry.Trim.Length > 0 Then
                                StrQ &= xAdvanceJoinQry
                            End If
                            If xDateJoinQry.Trim.Length > 0 Then
                                StrQ &= xDateJoinQry
                            End If
                            If xUACQry.Trim.Length > 0 Then
                                StrQ &= xUACQry
                            End If
                        End If
                        'Shani(19-APR-2016) -- End 



                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " JOIN hremployee_master WITH (NOLOCK) ON hrevaluation_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid "

                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry
                        End If

                        If enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                            StrQ &= " LEFT JOIN hrexternal_assessor_master WITH (NOLOCK) ON hrexternal_assessor_master.ext_assessorunkid = hrevaluation_analysis_master.ext_assessorunkid "
                        End If

                        StrQ &= " #ASS_JOIN#" & _
                                " JOIN hrassessor_master WITH (NOLOCK) ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
                                " JOIN hrapprover_usermapping WITH (NOLOCK) ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

                        'If xDateJoinQry.Trim.Length > 0 Then
                        '    StrQ &= " #xDateJoinQry# "
                        'End If

                        'If xUACQry.Trim.Length > 0 Then
                        '    StrQ &= " #xUACQry# "
                        'End If

                        
                End Select

                'Shani(19-APR-2016) -- Start
                'Get Assessor Wise Employee Given by Andrew / matthew
                'StrQ &= " WHERE hrevaluation_analysis_master.isvoid = 0 "
                StrQ &= " WHERE hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode & " AND hrevaluation_analysis_master.periodunkid = @Pid " 'S.SANDEEP [27-APR-2017] -- START {@Pid} -- END


                'Shani(19-APR-2016) -- End 


                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT

                        'Shani(19-APR-2016) -- Start
                        'Get Assessor Wise Employee Given by Andrew / matthew
                        'If xUACFiltrQry.Trim.Length > 0 Then
                        '    StrQ &= " AND " & xUACFiltrQry & " "
                        'End If

                        'If xIncludeIn_ActiveEmployee = False Then
                        '    If xDateFilterQry.Trim.Length > 0 Then
                        '        StrQ &= xDateFilterQry & " "
                        '    End If
                        'End If

                        If xInculde_UAC Then
                            If xUACFiltrQry.Trim.Length > 0 Then
                                StrQ &= " AND " & xUACFiltrQry & " "
                            End If

                            If xIncludeIn_ActiveEmployee = False Then
                                If xDateFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDateFilterQry & " "
                                End If
                            End If
                        End If

                        'Shani(19-APR-2016) -- End
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND hrevaluation_analysis_master.assessmodeid = " & enAssessMode
                        'If xIncludeIn_ActiveEmployee = False Then
                        '    If xDateFilterQry.Trim.Length > 0 Then
                        '        StrQ &= " #xDateFilterQry# "
                        'End If
                        'End If
                End Select

                Select Case enAssessMode
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE #ASS_CND# END <> '' "
                End Select

                If strFilterString.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilterString
                End If
                StrQ &= " #COND#"

                Dim strFinalQ As String = ""
                strFinalQ = StrQ
                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        strFinalQ = strFinalQ.Replace("#COND#", "")
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        strFinalQ = strFinalQ.Replace("#ASS_NAME#", "ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'')")

                        If enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                            strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "LEFT JOIN hremployee_master AS AEMP WITH (NOLOCK) ON AEMP.employeeunkid = hrevaluation_analysis_master.assessoremployeeunkid AND hrevaluation_analysis_master.ext_assessorunkid = 0 ")
                        ElseIf enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                            strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "LEFT JOIN hremployee_master AS AEMP WITH (NOLOCK) ON AEMP.employeeunkid = hrevaluation_analysis_master.reviewerunkid ")
                        End If

                        strFinalQ = strFinalQ.Replace("#xDateJoinQry#", "")
                        strFinalQ = strFinalQ.Replace("#xUACQry#", "")
                        strFinalQ = strFinalQ.Replace("#xAdvanceJoinQry#", "")
                        strFinalQ = strFinalQ.Replace("#xDateFilterQry#", "")
                        strFinalQ = strFinalQ.Replace("#COND#", " AND hrassessor_master.isexternalapprover = 0 ")
                        strFinalQ = strFinalQ.Replace("#ASS_CND#", "ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'')")
                End Select


                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Balanced Score Card"))
                objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 125, "Objectives/Goals/Targets"))
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                objDo.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                'S.SANDEEP [27-APR-2017] -- END





                dsList = objDo.ExecQuery(strFinalQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If enAssessMode <> enAssessmentMode.SELF_ASSESSMENT Then
                    Dim dsTemp As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objDo, "List", , , , , IIf(enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT, clsAssessor.enAssessorType.ASSESSOR, clsAssessor.enAssessorType.REVIEWER))
                    For Each dRow As DataRow In dsTemp.Tables(0).Rows
                        objDo.ClearParameters()
                        strFinalQ = StrQ
                        If dRow.Item("companyunkid").ToString <= 0 AndAlso dRow.Item("DBName").ToString.Trim.Length <= 0 Then
                            strFinalQ = strFinalQ.Replace("#ASS_NAME#", "ISNULL(AEMP.username,'')")
                            If enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS AEMP WITH (NOLOCK) ON AEMP.userunkid = hrevaluation_analysis_master.assessoremployeeunkid AND hrevaluation_analysis_master.ext_assessorunkid = 0")
                            Else
                                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS AEMP WITH (NOLOCK) ON AEMP.userunkid = hrevaluation_analysis_master.reviewerunkid")
                            End If
                            strFinalQ = strFinalQ.Replace("#ASS_CND#", "ISNULL(AEMP.username,'')")
                            strFinalQ = strFinalQ.Replace("#COND#", " AND hrassessor_master.isexternalapprover = 1 AND AEMP.companyunkid = '" & dRow.Item("companyunkid").ToString & "' ")
                        Else
                            strFinalQ = strFinalQ.Replace("#ASS_NAME#", "ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'')")
                            If enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master  WITH (NOLOCK) ON cfuser_master.userunkid = hrevaluation_analysis_master.assessoremployeeunkid AND hrevaluation_analysis_master.ext_assessorunkid = 0" & _
                                                                            "LEFT JOIN #DBName#hremployee_master AS AEMP WITH (NOLOCK) ON AEMP.employeeunkid = cfuser_master.employeeunkid")
                            Else
                                strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master  WITH (NOLOCK) ON cfuser_master.userunkid = hrevaluation_analysis_master.reviewerunkid " & _
                                                                            "LEFT JOIN #DBName#hremployee_master AS AEMP WITH (NOLOCK) ON AEMP.employeeunkid = cfuser_master.employeeunkid")
                            End If
                            strFinalQ = strFinalQ.Replace("#ASS_CND#", "ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'')")
                            strFinalQ = strFinalQ.Replace("#COND#", " AND hrassessor_master.isexternalapprover = 1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid").ToString & "' ")
                        End If
                        strFinalQ = strFinalQ.Replace("#xDateJoinQry#", "")
                        strFinalQ = strFinalQ.Replace("#xUACQry#", "")
                        strFinalQ = strFinalQ.Replace("#xAdvanceJoinQry#", "")
                        strFinalQ = strFinalQ.Replace("#xDateFilterQry#", "")
                        strFinalQ = strFinalQ.Replace("#DBName#", dRow.Item("DBName") & "..")


                        'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                        'objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Balanced Score Card"))
                        objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 125, "Objectives/Goals/Targets"))
                        'Shani (26-Sep-2016) -- End

                        'S.SANDEEP [01-NOV-2017] -- START
                        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                        objDo.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                        'S.SANDEEP [01-NOV-2017] -- END

                        Dim dsTempData As DataSet = objDo.ExecQuery(strFinalQ, "List")

                        If objDo.ErrorMessage <> "" Then
                            exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                            Throw exForce
                        End If

                        If dsList Is Nothing Then dsList = New DataSet

                        If dsList.Tables.Count <= 0 Then
                            dsList.Tables.Add(dsTempData.Tables(0).Copy)
                        Else
                            dsList.Tables(0).Merge(dsTempData.Tables(0), True)
                        End If
                    Next
                End If
            End Using
            'Shani(01-MAR-2016) -- End

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, iFactor))
            'S.SANDEEP |24-DEC-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

        'Shani(01-MAR-2016) -- Start
        'Enhancement :PA External Approver Flow
        'analysisunkid
        If dsList IsNot Nothing Then
            Dim dTemp As New DataSet
            dTemp.Tables.Add(New DataView(dsList.Tables(0), "", "analysisunkid", DataViewRowState.CurrentRows).ToTable)
            dsList = dTemp
            dsList.Tables(0).TableName = strTableName
        End If
        'Shani(01-MAR-2016) -- End
        Return dsList
    End Function

    'S.SANDEEP |24-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
    Private Function PerformingRounding(ByVal x As DataRow, ByVal dblFactor As Decimal) As Boolean
        Try
            If IsDBNull(x("total_score")) = False Then
                x("total_score") = Rounding.BRound(CDec(x("total_score")), dblFactor)
            Else
                x("total_score") = 0
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformingRounding; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |24-DEC-2019| -- END

    'Public Function GetList(ByVal strTableName As String, _
    '                        ByVal enAssessMode As enAssessmentMode, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "", _
    '                        Optional ByVal intUserUnkId As Integer = 0) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        strQ = "SELECT " & _
    '                    " hrevaluation_analysis_master.analysisunkid " & _
    '                    ",hrevaluation_analysis_master.periodunkid " & _
    '                    ",hrevaluation_analysis_master.selfemployeeunkid " & _
    '                    ",hrevaluation_analysis_master.assessormasterunkid " & _
    '                    ",hrevaluation_analysis_master.assessoremployeeunkid " & _
    '                    ",hrevaluation_analysis_master.assessedemployeeunkid " & _
    '                    ",Convert(Char(8),hrevaluation_analysis_master.assessmentdate,112) AS assessmentdate " & _
    '                    ",hrevaluation_analysis_master.userunkid " & _
    '                    ",hrevaluation_analysis_master.isvoid " & _
    '                    ",hrevaluation_analysis_master.voiduserunkid " & _
    '                    ",hrevaluation_analysis_master.voiddatetime " & _
    '                    ",hrevaluation_analysis_master.voidreason " & _
    '                    ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
    '                    ",ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
    '                    ",ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
    '                    ",cfcommon_period_tran.yearunkid " & _
    '                    ",ISNULL(iscommitted,0) AS iscommitted " & _
    '                    ",ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
    '                    ",cfcommon_period_tran.statusid AS Sid " & _
    '                    ",hremployee_master.stationunkid " & _
    '                    ",hremployee_master.deptgroupunkid " & _
    '                    ",hremployee_master.departmentunkid " & _
    '                    ",hremployee_master.sectionunkid " & _
    '                    ",hremployee_master.unitunkid " & _
    '                    ",hremployee_master.jobgroupunkid " & _
    '                    ",hremployee_master.jobunkid " & _
    '                    ",hremployee_master.gradegroupunkid " & _
    '                    ",hremployee_master.gradeunkid " & _
    '                    ",hremployee_master.gradelevelunkid " & _
    '                    ",hremployee_master.classgroupunkid " & _
    '                    ",hremployee_master.classunkid " & _
    '                    ",hremployee_master.sectiongroupunkid " & _
    '                    ",hremployee_master.unitgroupunkid " & _
    '                    ",hremployee_master.teamunkid " & _
    '                    ",hrevaluation_analysis_master.committeddatetime " & _
    '                    ",ISNULL(rscore,0) rscore " & _
    '                    ",ISNULL(smode,'') AS smode " & _
    '                    ",ISNULL(smodeid,0) AS smodeid " & _
    '                    ",ISNULL(assessgroupunkid,0) AS assessgroupunkid "

    '        'S.SANDEEP [ 01 JAN 2015 ] -- START
    '        '   ",ISNULL(wscore,0) wscore " & _ ------ REMOVED
    '        'S.SANDEEP [ 01 JAN 2015 ] -- END

    '        Select Case enAssessMode
    '            Case enAssessmentMode.SELF_ASSESSMENT
    '                strQ &= ", ' ' as Assessor "
    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                strQ &= ", CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Assessor " & _
    '                        ", hrapprover_usermapping.userunkid AS mapuserid " 'S.SANDEEP [04 JUN 2015] -- START -- END

    '            Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                strQ &= ", ISNULL(Reviewer.firstname,'')+' '+ISNULL(Reviewer.othername,'')+' '+ISNULL(Reviewer.surname,'') AS Reviewer " & _
    '                        ", hrapprover_usermapping.userunkid AS mapuserid " 'S.SANDEEP [04 JUN 2015] -- START -- END
    '        End Select

    '        strQ &= "FROM hrevaluation_analysis_master " & _
    '                " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrevaluation_analysis_master.periodunkid " & _
    '                " JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid "

    '        'S.SANDEEP [21 JAN 2015] -- START
    '        'strQ &= "LEFT JOIN " & _
    '        '        "( " & _
    '        '        "   SELECT " & _
    '        '        "        analysisunkid AS banalysisunkid " & _
    '        '        "       ,SUM(computed_value) AS rscore " & _
    '        '        "       ,@BSC AS smode " & _
    '        '        "   FROM hrgoals_analysis_tran " & _
    '        '        "   WHERE isvoid = 0 GROUP BY analysisunkid " & _
    '        '        "UNION ALL " & _
    '        '        "   SELECT " & _
    '        '        "        hrcompetency_analysis_tran.analysisunkid AS banalysisunkid " & _
    '        '        "       ,SUM(computed_value) AS rscore " & _
    '        '        "       ,hrassess_group_master.assessgroup_name AS smode " & _
    '        '        "   FROM hrcompetency_analysis_tran " & _
    '        '        "       JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
    '        '        "       JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '        '        "       JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '        '        "       JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
    '        '        "           AND hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '        '        "       WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 " & _
    '        '        "       GROUP BY hrcompetency_analysis_tran.analysisunkid,hrassess_group_master.assessgroup_name " & _
    '        '        ") AS BSC ON BSC.banalysisunkid = hrevaluation_analysis_master.analysisunkid "

    '        strQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        analysisunkid AS banalysisunkid " & _
    '                "       ,SUM(result) AS rscore " & _
    '                "       ,@BSC AS smode " & _
    '                "       ,1 AS smodeid " & _
    '                "       ,0 AS assessgroupunkid " & _
    '                "   FROM hrgoals_analysis_tran " & _
    '                "   WHERE isvoid = 0 GROUP BY analysisunkid " & _
    '                "UNION ALL " & _
    '                "   SELECT " & _
    '                "        hrcompetency_analysis_tran.analysisunkid AS banalysisunkid " & _
    '                "       ,SUM(result) AS rscore " & _
    '                "       ,hrassess_group_master.assessgroup_name AS smode " & _
    '                "       ,2 AS smodeid " & _
    '                "       ,hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "   FROM hrcompetency_analysis_tran " & _
    '                "       JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
    '                "       JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "       JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
    '                "       JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
    '                "           AND hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '                "       WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 " & _
    '                "       GROUP BY hrcompetency_analysis_tran.analysisunkid,hrassess_group_master.assessgroup_name,hrcompetency_analysis_tran.assessgroupunkid " & _
    '                ") AS BSC ON BSC.banalysisunkid = hrevaluation_analysis_master.analysisunkid "
    '        'S.SANDEEP [21 JAN 2015] -- END

    '        Select Case enAssessMode
    '            Case enAssessmentMode.SELF_ASSESSMENT
    '                strQ &= " JOIN hremployee_master ON hrevaluation_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid "
    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                strQ &= " JOIN hremployee_master ON hrevaluation_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
    '                        " LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrevaluation_analysis_master.ext_assessorunkid " & _
    '                        " LEFT JOIN hremployee_master AS AEMP ON AEMP.employeeunkid = hrevaluation_analysis_master.assessoremployeeunkid AND hrevaluation_analysis_master.ext_assessorunkid = 0 "

    '                If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '                    strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '                End If

    '                If CBool(strIncludeInactiveEmployee) = False Then
    '                    strQ &= " AND CONVERT(CHAR(8),AEMP.appointeddate,112) <= @enddate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_from_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_to_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),AEMP.empl_enddate,112), @startdate) >= @startdate "
    '                End If

    '                strQ &= " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
    '                        " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '                        " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
    '                        " AND hrassessor_master.isvoid = 0 "

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                'AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " 
    '                'S.SANDEEP [04 JUN 2015] -- END

    '                If strUserAccessLevelFilterString = "" Then
    '                    strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "AEMP")
    '                Else
    '                    strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "AEMP")
    '                End If

    '            Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                strQ &= " JOIN hremployee_master ON hrevaluation_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
    '                        " JOIN hremployee_master AS Reviewer ON hrevaluation_analysis_master.reviewerunkid = Reviewer.employeeunkid " & _
    '                        " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrevaluation_analysis_master.assessormasterunkid " & _
    '                        " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '                        "  AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                '" AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " " & _
    '                'S.SANDEEP [04 JUN 2015] -- END

    '        End Select

    '        strQ &= " WHERE hrevaluation_analysis_master.isvoid = 0 "

    '        Select Case enAssessMode
    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                strQ &= " AND CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END <> '' "
    '        End Select

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            Select Case enAssessMode
    '                Case enAssessmentMode.SELF_ASSESSMENT

    '                    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))

    '                Case enAssessmentMode.APPRAISER_ASSESSMENT

    '                    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))

    '                Case enAssessmentMode.REVIEWER_ASSESSMENT

    '                    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " & _
    '                            " AND CONVERT(CHAR(8),Reviewer.appointeddate,112) <= @enddate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_from_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_to_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),Reviewer.empl_enddate,112), @startdate) >= @startdate " '

    '                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            End Select
    '        End If

    '        Select Case enAssessMode
    '            Case enAssessmentMode.SELF_ASSESSMENT
    '                If strUserAccessLevelFilterString = "" Then
    '                    strQ &= UserAccessLevel._AccessLevelFilterString
    '                Else
    '                    strQ &= strUserAccessLevelFilterString
    '                End If
    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                If strUserAccessLevelFilterString = "" Then
    '                    strQ &= UserAccessLevel._AccessLevelFilterString & " AND reviewerunkid < = 0 "
    '                Else
    '                    strQ &= strUserAccessLevelFilterString & " AND reviewerunkid < = 0 "
    '                End If
    '            Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                If strUserAccessLevelFilterString = "" Then
    '                    strQ &= UserAccessLevel._AccessLevelFilterString
    '                Else
    '                    strQ &= strUserAccessLevelFilterString
    '                End If

    '                If strUserAccessLevelFilterString = "" Then
    '                    strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "Reviewer")
    '                Else
    '                    strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "Reviewer")
    '                End If
    '        End Select

    '        objDataOperation.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Balanced Score Card"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrevaluation_analysis_master) </purpose>
    Public Function Insert(ByVal dtBSC_Evaluation As DataTable, _
                           ByVal dtGE_Evaluation As DataTable, _
                           ByVal dtCustomItems As DataTable, _
                           ByVal xScoreOptId As Integer, _
                           ByVal xEmployeeAsOnDate As Date, _
                           ByVal xUsedAgreedScore As Boolean, _
                           ByVal xSelfAssignedCompetencies As Boolean) As Boolean 'S.SANDEEP [27-APR-2017] -- START {xScoreOptId,xEmployeeAsOnDate,xUsedAgreedScore,xSelfAssignedCompetencies} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintPeriodunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If mintExt_Assessorunkid > 0 Then
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, mintExt_Assessorunkid, , , True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                Else
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, , mintAssessormasterunkid) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, , , mintAssessormasterunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
        End Select

        'S.SANDEEP |12-FEB-2019| -- START
        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
        If mintAnalysisunkid > 0 Then
            If CheckComputionIsExist(mintAnalysisunkid, mintAssessmodeid, mintPeriodunkid) Then
                objComputeScore._Isvoid = True
                objComputeScore._Voiddatetime = Now
                objComputeScore._Voiduserunkid = mintUserunkid
                objComputeScore._Voidreason = Language.getMessage(mstrModuleName, 131, "Edit Assessment.")
                If objComputeScore.DeleteEmployeeWise(mintAnalysisunkid, IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), mintPeriodunkid, mintAssessmodeid) = False Then
                    mstrMessage = Language.getMessage(mstrModuleName, 130, "Sorry, Fail to void exsisting computed score.")
                    Return False
                End If
            End If
        End If
        'S.SANDEEP |12-FEB-2019| -- END


        'S.SANDEEP [27-APR-2017] -- START
        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
        mdtComputeTran = objComputeScore.GetTranData(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                     mintPeriodunkid, mintAssessmodeid, _
                                                     mstrMessage, "List")
        'S.SANDEEP [27-APR-2017] -- END

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            If mintEvalTypeId <= 0 Then mintEvalTypeId = CInt(enPAEvalTypeId.EO_BOTH)
            objDataOperation.AddParameter("@evaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvalTypeId)
            'S.SANDEEP |13-NOV-2020| -- END

            strQ = "INSERT INTO hrevaluation_analysis_master ( " & _
                       "  periodunkid " & _
                       ", selfemployeeunkid " & _
                       ", assessormasterunkid " & _
                       ", assessoremployeeunkid " & _
                       ", assessedemployeeunkid " & _
                       ", assessmentdate " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", iscommitted " & _
                       ", reviewerunkid " & _
                       ", assessmodeid " & _
                       ", ext_assessorunkid" & _
                       ", committeddatetime " & _
                       ", evaltypeid " & _
                   ") VALUES (" & _
                       "  @periodunkid " & _
                       ", @selfemployeeunkid " & _
                       ", @assessormasterunkid " & _
                       ", @assessoremployeeunkid " & _
                       ", @assessedemployeeunkid " & _
                       ", @assessmentdate " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @iscommitted " & _
                       ", @reviewerunkid " & _
                       ", @assessmodeid " & _
                       ", @ext_assessorunkid" & _
                       ", @committeddatetime " & _
                       ", @evaltypeid " & _
                   "); SELECT @@identity"
            'S.SANDEEP |13-NOV-2020| -- START {evaltypeid} -- END
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)

            If dtBSC_Evaluation IsNot Nothing Then  'BALANCE SCORE CARD
                objDataOperation.ClearParameters()
                objGoalAnalysisTran._DataOperation = objDataOperation
                objGoalAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objGoalAnalysisTran._DataTable = dtBSC_Evaluation.Copy
                With objGoalAnalysisTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objGoalAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                Dim intComputScoreMasterId As Integer = 0
                intComputScoreMasterId = objComputeScore.GetComputeMasterId(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                                            mintPeriodunkid, mintAssessmodeid, , _
                                                                            objDataOperation)

                objComputeScore._Assessmodeid = mintAssessmodeid
                objComputeScore._Computationdate = Now
                objComputeScore._Isvoid = False
                objComputeScore._Periodunkid = mintPeriodunkid
                objComputeScore._Scoretypeid = xScoreOptId
                objComputeScore._Userunkid = mintUserunkid
                objComputeScore._Voiddatetime = Nothing
                objComputeScore._Voidreason = ""
                objComputeScore._Voiduserunkid = -1
                objComputeScore._Analysisunkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objComputeScore._Employeeunkid = mintSelfemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objComputeScore._Employeeunkid = mintAssessedemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                End Select
                Dim xTotalScore As Decimal = 0 : Dim intComputeFormulaId As Integer = 0 : Dim intComputeUnkid As Integer 'S.SANDEEP [07-JUL-2017] -- START
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE
                End Select

                'S.SANDEEP [07-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                intComputeUnkid = objComputeScore.GetComputationUnkid(intComputeFormulaId, mintPeriodunkid, objDataOperation)
                'S.SANDEEP [07-JUL-2017] -- END

                xTotalScore = Compute_Score(xAssessMode:=mintAssessmodeid, _
                                            IsBalanceScoreCard:=True, _
                                            xScoreOptId:=xScoreOptId, _
                                            xCompute_Formula:=intComputeFormulaId, _
                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                                            xEmployeeId:=mintSelfemployeeunkid, _
                                            xPeriodId:=mintPeriodunkid, _
                                            xUsedAgreedScore:=xUsedAgreedScore, _
                                            xSelfAssignedCompetencies:=xSelfAssignedCompetencies, xDataTable:=dtBSC_Evaluation)

                If intComputScoreMasterId > 0 Then
                    Dim xtmp() As DataRow = mdtComputeTran.Select("computescoremasterunkid = '" & intComputScoreMasterId & "' AND computationunkid = '" & intComputeFormulaId & "'")
                    If xtmp.Length > 0 Then
                        With xtmp(0)
                            .Item("computescoretranunkid") = .Item("computescoretranunkid")
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "U"
                            .Item("competency_value") = ""
                        End With
                    Else
                        Dim tmp = mdtComputeTran.NewRow()
                        With tmp
                            .Item("computescoretranunkid") = -1
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "A"
                            .Item("competency_value") = ""
                        End With
                        mdtComputeTran.Rows.Add(tmp)
                    End If
                Else
                    Dim xtmp As DataRow = mdtComputeTran.NewRow()
                    With xtmp
                        .Item("computescoretranunkid") = -1
                        .Item("computescoremasterunkid") = intComputScoreMasterId
                        .Item("computationunkid") = intComputeUnkid
                        .Item("formula_value") = xTotalScore
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = 0
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("competency_value") = ""
                    End With
                    mdtComputeTran.Rows.Add(xtmp)
                End If
                With objComputeScore
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With

                If intComputScoreMasterId > 0 Then
                    If objComputeScore.Insert_Update_Delete(objDataOperation, intComputScoreMasterId, mdtComputeTran) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If objComputeScore.Insert(mdtComputeTran, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [27-APR-2017] -- END
            End If

            If dtGE_Evaluation IsNot Nothing Then   'COMPETENCIES
                objDataOperation.ClearParameters()
                objAnalysisTran._DataOperation = objDataOperation
                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objAnalysisTran._DataTable = dtGE_Evaluation.Copy
                With objAnalysisTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                mdtComputeTran.Rows.Clear()
                Dim intComputScoreMasterId As Integer = 0
                intComputScoreMasterId = objComputeScore.GetComputeMasterId(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                                            mintPeriodunkid, mintAssessmodeid, , _
                                                                            objDataOperation)

                objComputeScore._Assessmodeid = mintAssessmodeid
                objComputeScore._Computationdate = Now
                objComputeScore._Isvoid = False
                objComputeScore._Periodunkid = mintPeriodunkid
                objComputeScore._Scoretypeid = xScoreOptId
                objComputeScore._Userunkid = mintUserunkid
                objComputeScore._Voiddatetime = Nothing
                objComputeScore._Voidreason = ""
                objComputeScore._Voiduserunkid = -1
                objComputeScore._Analysisunkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objComputeScore._Employeeunkid = mintSelfemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objComputeScore._Employeeunkid = mintAssessedemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                End Select
                Dim xTotalScore As Decimal = 0 : Dim intComputeFormulaId As Integer = 0 : Dim intComputeUnkid As Integer 'S.SANDEEP [07-JUL-2017] -- START
                Dim xCSV_CMP_Value As String = String.Empty

                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE
                End Select

                'S.SANDEEP [07-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                intComputeUnkid = objComputeScore.GetComputationUnkid(intComputeFormulaId, mintPeriodunkid, objDataOperation)
                'S.SANDEEP [07-JUL-2017] -- END

                Dim list = dtGE_Evaluation.AsEnumerable().Select(Function(x) x.Field(Of Integer)("assessgroupunkid")).Distinct.ToList()

                If list IsNot Nothing AndAlso list.Count > 0 Then
                    Dim xTable As DataTable = Nothing
                    For Each item In list
                        Dim xScore As Decimal = 0
                        xTable = New DataView(dtGE_Evaluation, "assessgroupunkid = '" & item.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                        xScore = Compute_Score(xAssessMode:=mintAssessmodeid, _
                                            IsBalanceScoreCard:=False, _
                                            xScoreOptId:=xScoreOptId, _
                                            xCompute_Formula:=intComputeFormulaId, _
                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                                            xEmployeeId:=mintAssessedemployeeunkid, _
                                            xPeriodId:=mintPeriodunkid, _
                                            xUsedAgreedScore:=xUsedAgreedScore, _
                                            xAssessorReviewerId:=mintAssessormasterunkid, _
                                            xSelfAssignedCompetencies:=xSelfAssignedCompetencies, _
                                            xDataTable:=xTable, xAssessGrpId:=item)
                        xTotalScore = xTotalScore + xScore
                        xCSV_CMP_Value &= "," & item.ToString & "|" & xScore.ToString
                    Next
                    If xCSV_CMP_Value.Trim.Length > 0 Then xCSV_CMP_Value = Mid(xCSV_CMP_Value, 2)
                End If



                If intComputScoreMasterId > 0 Then
                    Dim xtmp() As DataRow = mdtComputeTran.Select("computescoremasterunkid = '" & intComputScoreMasterId & "' AND computationunkid = '" & intComputeFormulaId & "'")
                    If xtmp.Length > 0 Then
                        With xtmp(0)
                            .Item("computescoretranunkid") = .Item("computescoretranunkid")
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "U"
                            .Item("competency_value") = xCSV_CMP_Value
                        End With
                    Else
                        Dim tmp = mdtComputeTran.NewRow()
                        With tmp
                            .Item("computescoretranunkid") = -1
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "A"
                            .Item("competency_value") = xCSV_CMP_Value
                        End With
                        mdtComputeTran.Rows.Add(tmp)
                    End If
                Else
                    Dim xtmp As DataRow = mdtComputeTran.NewRow()
                    With xtmp
                        .Item("computescoretranunkid") = -1
                        .Item("computescoremasterunkid") = intComputScoreMasterId
                        .Item("computationunkid") = intComputeUnkid
                        .Item("formula_value") = xTotalScore
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = 0
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("competency_value") = xCSV_CMP_Value
                    End With
                    mdtComputeTran.Rows.Add(xtmp)
                End If

                With objComputeScore
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With

                If intComputScoreMasterId > 0 Then
                    If objComputeScore.Insert_Update_Delete(objDataOperation, intComputScoreMasterId, mdtComputeTran) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If objComputeScore.Insert(mdtComputeTran, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [27-APR-2017] -- END

            End If

            If dtCustomItems IsNot Nothing Then 'CUSTOM ITEMS
                objCustomTran._DataOperation = objDataOperation
                objCustomTran._AnalysisUnkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objCustomTran._EmployeeId = mintSelfemployeeunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objCustomTran._EmployeeId = mintAssessedemployeeunkid
                End Select
                objCustomTran._DataTable = dtCustomItems.Copy
                With objCustomTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objCustomTran.InsertUpdateDelete_CustomAnalysisTran(mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrevaluation_analysis_master) </purpose>
    Public Function Update(ByVal dtBSC_Evaluation As DataTable, _
                           ByVal dtGE_Evaluation As DataTable, _
                           ByVal dtCustomItems As DataTable, _
                           ByVal xScoreOptId As Integer, _
                           ByVal xEmployeeAsOnDate As Date, _
                           ByVal xUsedAgreedScore As Boolean, _
                           ByVal xSelfAssignedCompetencies As Boolean, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnVoidComputation As Boolean = True) As Boolean 'S.SANDEEP |15-JAN-2020| -- START {blnVoidComputation} -- END
        'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
        'S.SANDEEP [27-APR-2017] -- START {xScoreOptId,xEmployeeAsOnDate,xUsedAgreedScore,xSelfAssignedCompetencies} -- END



        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintPeriodunkid, , , mintAnalysisunkid, , xDataOpr) = True Then 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If mintExt_Assessorunkid > 0 Then
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, mintExt_Assessorunkid, , mintAnalysisunkid, True, xDataOpr) = True Then 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                Else
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, mintAssessormasterunkid, , mintAnalysisunkid, , xDataOpr) = True Then 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, , mintAssessormasterunkid, mintAnalysisunkid, , xDataOpr) = True Then 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
        End Select

        'S.SANDEEP |12-FEB-2019| -- START
        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

        'S.SANDEEP |15-JAN-2020| -- START
        'ISSUE/ENHANCEMENT : PREVINTING COMPUTATION VOID FROM MIGRATION IF OPERATION TYPE IS OVERWRITE EXISTING ASSESSOR
        'If mintAnalysisunkid > 0 Then
        '    If CheckComputionIsExist(mintAnalysisunkid, mintAssessmodeid, mintPeriodunkid) Then
        '        objComputeScore._Isvoid = True
        '        objComputeScore._Voiddatetime = Now
        '        objComputeScore._Voiduserunkid = mintUserunkid
        '        objComputeScore._Voidreason = Language.getMessage(mstrModuleName, 131, "Edit Assessment.")
        '        If objComputeScore.DeleteEmployeeWise(mintAnalysisunkid, IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), mintPeriodunkid, mintAssessmodeid) = False Then
        '            mstrMessage = Language.getMessage(mstrModuleName, 130, "Sorry, Fail to void exsisting computed score.")
        '            Return False
        '        End If
        '    End If
        'End If

        If blnVoidComputation Then
        If mintAnalysisunkid > 0 Then
            If CheckComputionIsExist(mintAnalysisunkid, mintAssessmodeid, mintPeriodunkid) Then
                objComputeScore._Isvoid = True
                objComputeScore._Voiddatetime = Now
                objComputeScore._Voiduserunkid = mintUserunkid
                objComputeScore._Voidreason = Language.getMessage(mstrModuleName, 131, "Edit Assessment.")
                If objComputeScore.DeleteEmployeeWise(mintAnalysisunkid, IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), mintPeriodunkid, mintAssessmodeid) = False Then
                    mstrMessage = Language.getMessage(mstrModuleName, 130, "Sorry, Fail to void exsisting computed score.")
                    Return False
                End If
            End If
        End If
        End If
        'S.SANDEEP |15-JAN-2020| -- END

        
        'S.SANDEEP |12-FEB-2019| -- END


        'S.SANDEEP [27-APR-2017] -- START
        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
        mdtComputeTran = objComputeScore.GetTranData(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                     mintPeriodunkid, mintAssessmodeid, _
                                                     mstrMessage, "List", xDataOpr)
        'S.SANDEEP [27-APR-2017] -- END


        'S.SANDEEP [27 DEC 2016] -- START
        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
        'Dim objDataOperation As New clsDataOperation
        'objDataOperation.BindTransaction()
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27 DEC 2016] -- END

        Try
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            If mintEvalTypeId <= 0 Then mintEvalTypeId = CInt(enPAEvalTypeId.EO_BOTH)
            objDataOperation.AddParameter("@evaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvalTypeId)
            'S.SANDEEP |13-NOV-2020| -- END
            strQ = "UPDATE hrevaluation_analysis_master SET " & _
                   "  periodunkid = @periodunkid" & _
                   ", selfemployeeunkid = @selfemployeeunkid" & _
                   ", assessormasterunkid = @assessormasterunkid" & _
                   ", assessoremployeeunkid = @assessoremployeeunkid" & _
                   ", assessedemployeeunkid = @assessedemployeeunkid" & _
                   ", assessmentdate = @assessmentdate" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason" & _
                   ", iscommitted = @iscommitted" & _
                   ", reviewerunkid = @reviewerunkid" & _
                   ", assessmodeid = @assessmodeid" & _
                   ", ext_assessorunkid = @ext_assessorunkid " & _
                   ", committeddatetime = @committeddatetime " & _
                   ", evaltypeid = @evaltypeid " & _
                   "WHERE analysisunkid = @analysisunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtBSC_Evaluation IsNot Nothing Then  'BALANCE SCORE CARD
                objDataOperation.ClearParameters()
                objGoalAnalysisTran._DataOperation = objDataOperation
                objGoalAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objGoalAnalysisTran._DataTable = dtBSC_Evaluation.Copy
                With objGoalAnalysisTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objGoalAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                Dim intComputScoreMasterId As Integer = 0
                intComputScoreMasterId = objComputeScore.GetComputeMasterId(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                                            mintPeriodunkid, mintAssessmodeid, , _
                                                                            objDataOperation)

                objComputeScore._Assessmodeid = mintAssessmodeid
                objComputeScore._Computationdate = Now
                objComputeScore._Isvoid = False
                objComputeScore._Periodunkid = mintPeriodunkid
                objComputeScore._Scoretypeid = xScoreOptId
                objComputeScore._Userunkid = mintUserunkid
                objComputeScore._Voiddatetime = Nothing
                objComputeScore._Voidreason = ""
                objComputeScore._Voiduserunkid = -1
                objComputeScore._Analysisunkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objComputeScore._Employeeunkid = mintSelfemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objComputeScore._Employeeunkid = mintAssessedemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                End Select
                Dim xTotalScore As Decimal = 0 : Dim intComputeFormulaId As Integer = 0 : Dim intComputeUnkid As Integer 'S.SANDEEP [07-JUL-2017] -- START
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE
                End Select

                'S.SANDEEP [07-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                intComputeUnkid = objComputeScore.GetComputationUnkid(intComputeFormulaId, mintPeriodunkid, objDataOperation)
                'S.SANDEEP [07-JUL-2017] -- END

                xTotalScore = Compute_Score(xAssessMode:=mintAssessmodeid, _
                                            IsBalanceScoreCard:=True, _
                                            xScoreOptId:=xScoreOptId, _
                                            xCompute_Formula:=intComputeFormulaId, _
                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                                            xEmployeeId:=mintSelfemployeeunkid, _
                                            xPeriodId:=mintPeriodunkid, _
                                            xUsedAgreedScore:=xUsedAgreedScore, _
                                            xSelfAssignedCompetencies:=xSelfAssignedCompetencies, xDataTable:=dtBSC_Evaluation)

                If intComputScoreMasterId > 0 Then
                    Dim xtmp() As DataRow = mdtComputeTran.Select("computescoremasterunkid = '" & intComputScoreMasterId & "' AND computationunkid = '" & intComputeFormulaId & "'")
                    If xtmp.Length > 0 Then
                        With xtmp(0)
                            .Item("computescoretranunkid") = .Item("computescoretranunkid")
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "U"
                            .Item("competency_value") = ""
                        End With
                    Else
                        Dim tmp = mdtComputeTran.NewRow()
                        With tmp
                            .Item("computescoretranunkid") = -1
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "A"
                            .Item("competency_value") = ""
                        End With
                        mdtComputeTran.Rows.Add(tmp)
                    End If
                Else
                    Dim xtmp As DataRow = mdtComputeTran.NewRow()
                    With xtmp
                        .Item("computescoretranunkid") = -1
                        .Item("computescoremasterunkid") = intComputScoreMasterId
                        .Item("computationunkid") = intComputeUnkid
                        .Item("formula_value") = xTotalScore
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = 0
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("competency_value") = ""
                    End With
                    mdtComputeTran.Rows.Add(xtmp)
                End If

                With objComputeScore
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With

                If intComputScoreMasterId > 0 Then
                    If objComputeScore.Insert_Update_Delete(objDataOperation, intComputScoreMasterId, mdtComputeTran) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If objComputeScore.Insert(mdtComputeTran, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [27-APR-2017] -- END
            End If

            If dtGE_Evaluation IsNot Nothing Then   'COMPETENCIES
                objDataOperation.ClearParameters()
                objAnalysisTran._DataOperation = objDataOperation
                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objAnalysisTran._DataTable = dtGE_Evaluation.Copy
                With objAnalysisTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                mdtComputeTran.Rows.Clear()
                Dim intComputScoreMasterId As Integer = 0
                intComputScoreMasterId = objComputeScore.GetComputeMasterId(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                                            mintPeriodunkid, mintAssessmodeid, , _
                                                                            objDataOperation)

                objComputeScore._Assessmodeid = mintAssessmodeid
                objComputeScore._Computationdate = Now
                objComputeScore._Isvoid = False
                objComputeScore._Periodunkid = mintPeriodunkid
                objComputeScore._Scoretypeid = xScoreOptId
                objComputeScore._Userunkid = mintUserunkid
                objComputeScore._Voiddatetime = Nothing
                objComputeScore._Voidreason = ""
                objComputeScore._Voiduserunkid = -1
                objComputeScore._Analysisunkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objComputeScore._Employeeunkid = mintSelfemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objComputeScore._Employeeunkid = mintAssessedemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                End Select
                Dim xTotalScore As Decimal = 0 : Dim intComputeFormulaId As Integer = 0 : Dim intComputeUnkid As Integer 'S.SANDEEP [07-JUL-2017] -- START
                Dim xCSV_CMP_Value As String = String.Empty

                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE
                End Select

                'S.SANDEEP [07-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                intComputeUnkid = objComputeScore.GetComputationUnkid(intComputeFormulaId, mintPeriodunkid, objDataOperation)
                'S.SANDEEP [07-JUL-2017] -- END

                Dim list = dtGE_Evaluation.AsEnumerable().Select(Function(x) x.Field(Of Integer)("assessgroupunkid")).Distinct.ToList()

                If list IsNot Nothing AndAlso list.Count > 0 Then
                    Dim xTable As DataTable = Nothing
                    For Each item In list
                        Dim xScore As Decimal = 0
                        xTable = New DataView(dtGE_Evaluation, "assessgroupunkid = '" & item.ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                        xScore = Compute_Score(xAssessMode:=mintAssessmodeid, _
                                            IsBalanceScoreCard:=False, _
                                            xScoreOptId:=xScoreOptId, _
                                            xCompute_Formula:=intComputeFormulaId, _
                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                                            xEmployeeId:=mintAssessedemployeeunkid, _
                                            xPeriodId:=mintPeriodunkid, _
                                            xUsedAgreedScore:=xUsedAgreedScore, _
                                            xAssessorReviewerId:=mintAssessormasterunkid, _
                                            xSelfAssignedCompetencies:=xSelfAssignedCompetencies, _
                                            xDataTable:=xTable, xAssessGrpId:=item)
                        xTotalScore = xTotalScore + xScore
                        xCSV_CMP_Value &= "," & item.ToString & "|" & xScore.ToString
                    Next
                    If xCSV_CMP_Value.Trim.Length > 0 Then xCSV_CMP_Value = Mid(xCSV_CMP_Value, 2)
                End If



                If intComputScoreMasterId > 0 Then
                    Dim xtmp() As DataRow = mdtComputeTran.Select("computescoremasterunkid = '" & intComputScoreMasterId & "' AND computationunkid = '" & intComputeFormulaId & "'")
                    If xtmp.Length > 0 Then
                        With xtmp(0)
                            .Item("computescoretranunkid") = .Item("computescoretranunkid")
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "U"
                            .Item("competency_value") = xCSV_CMP_Value
                        End With
                    Else
                        Dim tmp = mdtComputeTran.NewRow()
                        With tmp
                            .Item("computescoretranunkid") = -1
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "A"
                            .Item("competency_value") = xCSV_CMP_Value
                        End With
                        mdtComputeTran.Rows.Add(tmp)
                    End If
                Else
                    Dim xtmp As DataRow = mdtComputeTran.NewRow()
                    With xtmp
                        .Item("computescoretranunkid") = -1
                        .Item("computescoremasterunkid") = intComputScoreMasterId
                        .Item("computationunkid") = intComputeUnkid
                        .Item("formula_value") = xTotalScore
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = 0
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("competency_value") = xCSV_CMP_Value
                    End With
                    mdtComputeTran.Rows.Add(xtmp)
                End If

                With objComputeScore
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With

                If intComputScoreMasterId > 0 Then
                    If objComputeScore.Insert_Update_Delete(objDataOperation, intComputScoreMasterId, mdtComputeTran) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If objComputeScore.Insert(mdtComputeTran, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [27-APR-2017] -- END
            End If

            If dtCustomItems IsNot Nothing Then 'CUSTOM ITEMS
                objCustomTran._DataOperation = objDataOperation
                objCustomTran._AnalysisUnkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objCustomTran._EmployeeId = mintSelfemployeeunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objCustomTran._EmployeeId = mintAssessedemployeeunkid
                End Select
                objCustomTran._DataTable = dtCustomItems.Copy
                With objCustomTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objCustomTran.InsertUpdateDelete_CustomAnalysisTran(mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation.ReleaseTransaction(True)
            If dtBSC_Evaluation Is Nothing AndAlso dtGE_Evaluation Is Nothing AndAlso dtCustomItems Is Nothing Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", mintAnalysisunkid, "", "", -1, 1, 1, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [27 DEC 2016] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [27 DEC 2016] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [27 DEC 2016] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrevaluation_analysis_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal enAssessment As enAssessmentMode, ByVal iPeriodId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
        'S.SANDEEP [04 JUN 2015] -- START
        mstrMessage = ""
        'S.SANDEEP [04 JUN 2015] -- END
        If isUsed(intUnkid, enAssessment, iPeriodId, xDataOpr) Then 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
            Select Case enAssessment
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assessment. Reason : Assessor has already assessed this employee.")
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Assessment. Reason : Reviewer has already assessed this employee.")
            End Select
            Return False
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [27 DEC 2016] -- START
        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
        'Dim objDataOperation As New clsDataOperation
        'objDataOperation.BindTransaction()
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27 DEC 2016] -- END
        Try
            strQ = "UPDATE hrevaluation_analysis_master SET " & _
                  "  isvoid = @isvoid" & _
                  ", voiduserunkid = @voiduserunkid" & _
                  ", voiddatetime = @voiddatetime" & _
                  ", voidreason = @voidreason " & _
                "WHERE analysisunkid = @analysisunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "hrgoals_analysis_tran", "analysisunkid", intUnkid)
            objCommonATLog = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strQ = "UPDATE hrgoals_analysis_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voidreason = @voidreason " & _
                           "WHERE analysistranunkid = '" & dtRow.Item("analysistranunkid") & "' "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", intUnkid, "hrgoals_analysis_tran", "analysistranunkid", dtRow.Item("analysistranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END

                    End If
                Next
            End If
            objCommonATLog = New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "hrcompetency_analysis_tran", "analysisunkid", intUnkid)
            objCommonATLog = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strQ = "UPDATE hrcompetency_analysis_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voidreason = @voidreason " & _
                           "WHERE analysistranunkid = '" & dtRow.Item("analysistranunkid") & "' "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", intUnkid, "hrcompetency_analysis_tran", "analysistranunkid", dtRow.Item("analysistranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END

                    End If
                Next
            End If

            objCommonATLog = New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "hrcompetency_customitem_tran", "analysisunkid", intUnkid)
            objCommonATLog = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strQ = "UPDATE hrcompetency_customitem_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voidreason = @voidreason " & _
                           "WHERE customanalysistranunkid = '" & dtRow.Item("customanalysistranunkid") & "' "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", intUnkid, "hrcompetency_customitem_tran", "customanalysistranunkid", dtRow.Item("customanalysistranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END

                    End If
                Next
            End If

'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            strQ = "SELECT " & _
                   "     pdpitem_master.categoryunkid " & _
                   "    ,pdpformunkid " & _
                   "    ,itemgrpguid " & _
                   "FROM pdpitemdatatran " & _
                   "    JOIN pdpitem_master ON pdpitemdatatran.itemunkid = pdpitem_master.itemunkid " & _
                   "WHERE isvoid = 0 AND analysisunkid = @analysisunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                objPDPFormTran._Pdpformunkid = dsList.Tables("List").Rows(0)("pdpformunkid")
                objPDPFormTran._Isvoid = True
                objPDPFormTran._Voiddatetime = mdtVoiddatetime
                objPDPFormTran._Voiduserunkid = mintVoiduserunkid
                objPDPFormTran._Voidreason = mstrVoidreason

                objPDPFormTran._AuditUserId = mintVoiduserunkid
                objPDPFormTran._FormName = mstrFormName
                objPDPFormTran._FromWeb = mblnIsWeb
                objPDPFormTran._HostName = mstrHostName
                objPDPFormTran._ClientIP = mstrClientIP
                objPDPFormTran._DatabaseName = mstrDatabaseName
                objPDPFormTran._LoginEmployeeUnkid = mintloginemployeeunkid
                objPDPFormTran._PAnalysisunkid = mintAnalysisunkid

                If objPDPFormTran.Delete(-1, dsList.Tables("List").Rows(0)("itemgrpguid").ToString(), CInt(dsList.Tables("List").Rows(0)("categoryunkid")), objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP |03-MAY-2021| -- END
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [27 DEC 2016] -- END

            Return True

        Catch ex As Exception
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [27 DEC 2016] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal enAssessment As enAssessmentMode, ByVal iPrdId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP [27 DEC 2016] -- START
        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27 DEC 2016] -- END
        Try
            strQ = "SELECT " & _
                       "    assessedemployeeunkid " & _
                       "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                       "WHERE assessedemployeeunkid " & _
                       "IN " & _
                       "( " & _
                             "SELECT " & _
                                  "selfemployeeunkid " & _
                             "FROM hrevaluation_analysis_master " & _
                             "WHERE analysisunkid = @analysisunkid AND periodunkid = @PrdId " & _
                       ") " & _
                       "AND isVoid = 0 " & _
                       "AND assessedemployeeunkid > 0 AND periodunkid = @PrdId "

            Select Case enAssessment
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
            End Select

            objDataOperation.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, iPrdId)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [27 DEC 2016] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal enAssessMode As enAssessmentMode, _
                            ByVal intEmpId As Integer, _
                            ByVal intPeriodId As Integer, _
                            Optional ByVal intAssessorId As Integer = -1, _
                            Optional ByVal intReviewerId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal isExAssessor As Boolean = False, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnOnlyCommitted As Boolean = True) As Boolean 'S.SANDEEP [27 DEC 2016] -- START {xDataOpr} -- END
        'S.SANDEEP [17-OCT-2017] -- START {blnOnlyCommitted : ISSUE:0001389} -- END




        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [27 DEC 2016] -- START
        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [27 DEC 2016] -- END

        Try
            strQ = "SELECT " & _
                      "  analysisunkid " & _
                      ", periodunkid " & _
                      ", selfemployeeunkid " & _
                      ", assessormasterunkid " & _
                      ", assessoremployeeunkid " & _
                      ", assessedemployeeunkid " & _
                      ", assessmentdate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscommitted " & _
                      ", reviewerunkid " & _
                      ", assessmodeid " & _
                      ", ext_assessorunkid " & _
                      "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                      "WHERE ISNULL(isvoid,0) = 0 AND hrevaluation_analysis_master.periodunkid  = @PeriodId "

            If intUnkid > 0 Then
                strQ &= " AND analysisunkid <> @analysisunkid"
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            Select Case enAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " AND hrevaluation_analysis_master.selfemployeeunkid = @EmpId "
                Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                    strQ &= " AND hrevaluation_analysis_master.assessedemployeeunkid = @EmpId AND assessmodeid = " & enAssessMode
            End Select

            If intAssessorId > 0 Then
                If isExAssessor = False Then
                    strQ &= "AND hrevaluation_analysis_master.assessormasterunkid = @AssessorId "
                Else
                    strQ &= "AND hrevaluation_analysis_master.ext_assessorunkid = @AssessorId "
                End If
                objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
            End If

            If intReviewerId > 0 Then
                strQ &= "AND hrevaluation_analysis_master.assessormasterunkid = @reviewerunkid "
                objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReviewerId)
            End If

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE:0001389 - Assessor/Reviewer is able to assess employee before employee saves and commits self assessment
            If blnOnlyCommitted = True Then
                strQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
            End If
            'S.SANDEEP [04-AUG-2017] -- END

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [27 DEC 2016] -- END

        End Try
    End Function

    Public Function GetAssessorEmpId(ByVal intAssessorId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intEmpId As Integer = -1
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT employeeunkid AS EId FROM hrassessor_master WHERE assessormasterunkid = @AssessorId AND hrassessor_master.isvoid = 0 "

            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intEmpId = dsList.Tables(0).Rows(0)(0)
            End If

            Return intEmpId

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessorEmpId; Module Name: " & mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''<summary>
    ''' Modify by Shani Sheladiya (01 March 2016)
    ''' </summary>
    Public Function getAssessorComboList(ByVal xDatabaseName As String, _
                                         ByVal xUserUnkid As Integer, _
                                         ByVal xPeriodStart As DateTime, _
                                         ByVal xPeriodEnd As DateTime, _
                                         ByVal xOnlyApproved As Boolean, _
                                         ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                         ByVal strList As String, _
                                         ByVal intVisiblilityTypeId As Integer, _
                                         Optional ByVal blnFlag As Boolean = False, _
                                         Optional ByVal blnIsReviewer As Boolean = False, _
                                         Optional ByVal intEmpId As Integer = 0) As DataSet
        'S.SANDEEP [27 DEC 2016] -- START {intVisiblilityTypeId} -- END


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        'Shani(01-MAR-2016) -- Start
        'Enhancement :PA External Approver Flow
        Dim strCommonQ As String = ""
        Dim strFinalQ As String = ""
        Dim strFilterQ As String = ""
        'Shani(01-MAR-2016) -- End

        Try
            Dim xDateJoinQry, xDateFilterQry As String

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'xDateJoinQry = "" : xDateFilterQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            'Using objDo As New clsDataOperation
            '    If blnFlag = True Then
            '        StrQ = "SELECT 0 AS Id, @Select AS Name,'' AS Code, 0 AS EmpId UNION "
            '    End If

            '    StrQ &= "SELECT " & _
            '            "	 assessormasterunkid AS Id " & _
            '            "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Name " & _
            '            "   ,ISNULL(hremployee_master.employeecode,'') AS Code " & _
            '            "   ,hrassessor_master.employeeunkid AS EmpId " & _
            '            "FROM hrassessor_master " & _
            '            "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
            '            "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid "
            '    'S.SANDEEP [05 DEC 2015] -- START {hrassessor_master.employeeunkid AS EmpId} -- END


            '    If xDateJoinQry.Trim.Length > 0 Then
            '        StrQ &= xDateJoinQry
            '    End If

            '    StrQ &= "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
            '            "	AND hrapprover_usermapping.userunkid = " & xUserUnkid & "  AND hrassessor_master.isvoid = 0 "

            '    If blnIsReviewer Then
            '        StrQ &= "	AND hrassessor_master.isreviewer = 1 "
            '    Else
            '        StrQ &= "	AND hrassessor_master.isreviewer = 0 "
            '    End If

            '    If xIncludeIn_ActiveEmployee = False Then
            '        If xDateFilterQry.Trim.Length > 0 Then
            '            StrQ &= xDateFilterQry & " "
            '        End If
            '    End If

            '    objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            '    dsList = objDo.ExecQuery(StrQ, strList)

            '    If objDo.ErrorMessage <> "" Then
            '        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '        Throw exForce
            '    End If


            Using objDo As New clsDataOperation
                xDateJoinQry = "" : xDateFilterQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

                If blnFlag = True Then
                    strCommonQ = "SELECT 0 AS Id, @Select AS Name,'' AS Code, 0 AS EmpId UNION "
                End If

                StrQ &= "SELECT DISTINCT" & _
                        "	 hrassessor_master.assessormasterunkid AS Id " & _
                        "	,#EMP_NAME# AS Name " & _
                        "   ,#EMP_CODE# AS Code " & _
                        "   ,hrassessor_master.employeeunkid AS EmpId " & _
                        "FROM hrassessor_master WITH (NOLOCK) " & _
                        "	JOIN hrapprover_usermapping WITH (NOLOCK) ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
                        "	JOIN hrassessor_tran WITH (NOLOCK) ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                        "	#EMP_JOIN# "

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= "  #DATE_JOIN# "
                'End If

                StrQ &= "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                        "	AND hrapprover_usermapping.userunkid = " & xUserUnkid & "  AND hrassessor_master.isvoid = 0 #CONDITION# "

                If intEmpId > 0 Then
                    StrQ &= "    AND hrassessor_tran.employeeunkid = '" & intEmpId & "' "
                End If

                If blnIsReviewer Then
                    StrQ &= "	AND hrassessor_master.isreviewer = 1 "
                Else
                    StrQ &= "	AND hrassessor_master.isreviewer = 0 "
                End If

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                StrQ &= " AND hrassessor_master.visibletypeid = '" & intVisiblilityTypeId & "' "
                'S.SANDEEP [27 DEC 2016] -- END



                'If xIncludeIn_ActiveEmployee = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrQ &= " #DATE_FILTER# "
                '    End If
                'End If

                strFinalQ = strCommonQ
                strFinalQ &= StrQ
                'S.SANDEEP |25-MAR-2019| -- START
                'strFinalQ = strFinalQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ")
                strFinalQ = strFinalQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') ")
                'S.SANDEEP |25-MAR-2019| -- END
                strFinalQ = strFinalQ.Replace("#EMP_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
                strFinalQ = strFinalQ.Replace("#EMP_JOIN#", "JOIN hremployee_master WITH (NOLOCK) ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid ")
                'strFinalQ = strFinalQ.Replace("#DATE_JOIN#", xDateJoinQry)
                strFinalQ = strFinalQ.Replace("#CONDITION#", " AND hrassessor_master.isexternalapprover = 0 ")
                'strFinalQ = strFinalQ.Replace("#DATE_FILTER#", xDateFilterQry)

                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

                dsList = objDo.ExecQuery(strFinalQ, strList)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                Dim dsTemp As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(Nothing, "List", , , intEmpId, mintUserunkid, IIf(blnIsReviewer, clsAssessor.enAssessorType.REVIEWER, clsAssessor.enAssessorType.ASSESSOR))

                For Each dRow As DataRow In dsTemp.Tables(0).Rows
                    'xDateJoinQry = "" : xDateFilterQry = ""
                    strFinalQ = StrQ
                    If dRow.Item("companyunkid") <= 0 And dRow.Item("DBName").ToString.Trim.Length <= 0 Then
                        strFinalQ = strFinalQ.Replace("#EMP_NAME#", "ISNULL(UEmp.username,'') ")
                        strFinalQ = strFinalQ.Replace("#EMP_CODE#", "''")
                        strFinalQ = strFinalQ.Replace("#EMP_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp WITH (NOLOCK) ON hrassessor_master.employeeunkid = UEmp.userunkid ")
                        'strFinalQ = strFinalQ.Replace("#DATE_JOIN#", "")
                        strFinalQ = strFinalQ.Replace("#CONDITION#", " AND hrassessor_master.isexternalapprover = 1 AND UEmp.companyunkid = '" & dRow.Item("companyunkid").ToString & "' ")
                        'strFinalQ = strFinalQ.Replace("#DATE_FILTER#", "")
                    Else
                        'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dRow.Item("EMPASONDATE").ToString), eZeeDate.convertDate(dRow.Item("EMPASONDATE").ToString), , , dRow.Item("DBName").ToString)

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strFinalQ = strFinalQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')")
                        strFinalQ = strFinalQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'')")
                        'S.SANDEEP |25-MAR-2019| -- END

                        strFinalQ = strFinalQ.Replace("#EMP_CODE#", "ISNULL(hremployee_master.employeecode,'')")
                        strFinalQ = strFinalQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp WITH (NOLOCK) ON UEmp.userunkid = hrassessor_master.employeeunkid " & _
                                                                    " JOIN #DName#hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = UEmp.employeeunkid ")
                        'strFinalQ = strFinalQ.Replace("#DATE_JOIN#", xDateJoinQry)
                        strFinalQ = strFinalQ.Replace("#CONDITION#", " AND hrassessor_master.isexternalapprover = 1 AND UEmp.companyunkid = '" & dRow.Item("companyunkid").ToString & "' ")
                        'strFinalQ = strFinalQ.Replace("#DATE_FILTER#", xDateFilterQry)
                    End If
                    strFinalQ = strFinalQ.Replace("#DName#", dRow.Item("DBName").ToString & "..")

                    Dim dsData As DataSet = objDo.ExecQuery(strFinalQ, strList)

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables.Count <= 0 Then
                        dsList.Tables.Add(dsData.Tables(0).Copy)
                    Else
                        dsList.Tables(0).Merge(dsData.Tables(0), True)
                    End If
                Next
                'Shani(01-MAR-2016) -- End

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getAssessorComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function getAssessorComboList(Optional ByVal strList As String = "List", _
    '                                     Optional ByVal blnFlag As Boolean = False, _
    '                                     Optional ByVal blnIsReviewer As Boolean = False, _
    '                                     Optional ByVal intUserUnkid As Integer = 0, _
    '                                     Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                                     Optional ByVal strEmployeeAsOnDate As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        If blnFlag = True Then
    '            StrQ = "SELECT 0 AS Id, @Select AS Name,'' AS Code UNION "
    '        End If

    '        StrQ &= "SELECT " & _
    '                "	 assessormasterunkid AS Id " & _
    '                "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Name " & _
    '                "   ,ISNULL(hremployee_master.employeecode,'') AS Code " & _
    '                "FROM hrassessor_master " & _
    '                "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
    '                "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
    '                "	AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkid = 0, User._Object._Userunkid, intUserUnkid) & "  AND hrassessor_master.isvoid = 0 "

    '        If blnIsReviewer Then
    '            StrQ &= "	AND hrassessor_master.isreviewer = 1 "
    '        Else
    '            StrQ &= "	AND hrassessor_master.isreviewer = 0 "
    '        End If

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

    '        dsList = objDataOperation.ExecQuery(StrQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: getAssessorComboList; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Public Function getEmployeeBasedAssessor(ByVal intAssessorId As Integer, _
    '                                         Optional ByVal strList As String = "List", _
    '                                         Optional ByVal blnFlag As Boolean = False, _
    '                                         Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                                         Optional ByVal strEmployeeAsOnDate As String = "", _
    '                                         Optional ByVal intUserUnkId As Integer = 0) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        If blnFlag = True Then
    '            StrQ = "SELECT  0 AS Id,@Select AS NAME,'' AS Code UNION "
    '        End If

    '        StrQ &= "SELECT " & _
    '                "	 employeeunkid AS Id " & _
    '                "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
    '                "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
    '                "FROM hremployee_master " & _
    '                "WHERE employeeunkid " & _
    '                "	IN " & _
    '                "	( " & _
    '                "		SELECT " & _
    '                "			hrassessor_tran.employeeunkid " & _
    '                "		FROM hrassessor_tran " & _
    '                "			JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '                "			JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '                "		WHERE hrassessor_master.assessormasterunkid = @AssessorId " & _
    '                "			AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId <= 0, User._Object._Userunkid, intUserUnkId) & " " & _
    '                "           AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
    '                "	) "

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If

    '        objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

    '        dsList = objDataOperation.ExecQuery(StrQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: getEmployeeBasedAssessor; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Public Function getEmployeeBasedAssessor(ByVal xDatabaseName As String, _
                                             ByVal xUserUnkid As Integer, _
                                             ByVal xPeriodStart As DateTime, _
                                             ByVal xPeriodEnd As DateTime, _
                                             ByVal xOnlyApproved As Boolean, _
                                             ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal intAssessorId As Integer, _
                                             Optional ByVal strList As String = "List", _
                                             Optional ByVal blnFlag As Boolean = False, _
                                             Optional ByVal blnFromPlanning As Boolean = False, _
                                             Optional ByVal intPeriodId As Integer = 0, _
                                             Optional ByVal intAGroupId As Integer = 0, _
                                             Optional ByVal intAllocationTypeId As Integer = 0, _
                                             Optional ByVal intAllocaitonUnkId As Integer = 0, _
                                             Optional ByRef intTotalReporting As Integer = 0, _
                                             Optional ByRef intTotalPlanned As Integer = 0) As DataSet

        'S.SANDEEP [10 DEC 2015] -- START {blnFromPlanning,intPeriodId,intAGroupId,intAllocationTypeId, _
        '                                  intAllocaitonUnkId,intTotalReporting,intTotalPlanned} -- END

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try

            'S.SANDEEP [10 DEC 2015] -- START
            Dim iMappedFieldId, iExOder As Integer
            iMappedFieldId = 0 : iExOder = 0
            If blnFromPlanning Then
                Dim objFMap As New clsAssess_Field_Mapping
                Dim objFMst As New clsAssess_Field_Master

                iMappedFieldId = objFMap.Get_Map_FieldId(intPeriodId)
                iExOder = objFMst.Get_Field_ExOrder(iMappedFieldId)

                objFMap = Nothing : objFMst = Nothing
            End If
            'S.SANDEEP [10 DEC 2015] -- END

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Using objDo As New clsDataOperation

                'S.SANDEEP [10 DEC 2015] -- START
                'If blnFlag = True Then
                '    StrQ = "SELECT  0 AS Id,@Select AS NAME,'' AS Code UNION "
                'End If

                'StrQ &= "SELECT " & _
                '        "	 employeeunkid AS Id " & _
                '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
                '        "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
                '        "FROM hremployee_master "


                If blnFlag = True Then
                    StrQ = "SELECT  0 AS Id,@Select AS NAME,'' AS Code,'' AS Department ,'' AS Job UNION "
                End If
                StrQ &= "SELECT " & _
                        "	 hremployee_master.employeeunkid AS Id " & _
                        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
                        "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
                        "   ,ISNULL(hrdepartment_master.name,'') AS Department " & _
                        "   ,ISNULL(hrjob_master.job_name,'') AS Job " & _
                        "FROM hremployee_master " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                        " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         jobgroupunkid " & _
                        "        ,jobunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                        " JOIN hrjob_master ON hrjob_master.jobunkid = J.jobunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "
                'S.SANDEEP [10 DEC 2015] -- END



                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                StrQ &= "WHERE hremployee_master.employeeunkid " & _
                        "IN " & _
                        "( " & _
                        "	SELECT " & _
                        "	    hrassessor_tran.employeeunkid " & _
                        "	FROM hrassessor_tran " & _
                        "	    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                        "	    JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                        "	WHERE hrassessor_master.assessormasterunkid = @AssessorId AND hrapprover_usermapping.userunkid = " & xUserUnkid & " " & _
                        "       AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                        "       AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                        "       AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                        ") "
                'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END
                'S.SANDEEP [29-JUN-2017] -- START {visibletypeid} -- END




                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                'S.SANDEEP [10 DEC 2015] -- START
                If intAllocaitonUnkId > 0 Then
                    Select Case CInt(intAllocationTypeId)
                        Case enAllocation.BRANCH : StrQ &= " AND T.stationunkid  = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.DEPARTMENT_GROUP : StrQ &= " AND T.deptgroupunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.DEPARTMENT : StrQ &= " AND T.departmentunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.SECTION_GROUP : StrQ &= " AND T.sectiongroupunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.SECTION : StrQ &= " AND T.sectionunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.UNIT_GROUP : StrQ &= " AND T.unitgroupunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.UNIT : StrQ &= " AND T.unitunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.TEAM : StrQ &= " AND T.teamunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.JOB_GROUP : StrQ &= " AND J.jobgroupunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.JOBS : StrQ &= " AND J.jobunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.EMPLOYEE : StrQ &= " AND hremployee_master.employeeunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.CLASS_GROUP : StrQ &= " AND T.classgroupunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.CLASSES : StrQ &= " AND T.classunkid = '" & intAllocaitonUnkId & "' "
                        Case enAllocation.COST_CENTER : StrQ &= " AND C.cctranheadvalueid = '" & intAllocaitonUnkId & "' "
                    End Select
                End If
                'S.SANDEEP [10 DEC 2015] -- END


                objDo.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

                dsList = objDo.ExecQuery(StrQ, strList)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [10 DEC 2015] -- START
                intTotalReporting = dsList.Tables(strList).Rows.Count

                If blnFromPlanning = True Then

                    dsList.Tables(strList).Columns.Add("isplan", System.Type.GetType("System.Boolean")).DefaultValue = False

                    Dim dsMatchedEmp As New DataSet
                    StrQ = "SELECT " & _
                           "	 hremployee_master.employeeunkid AS Id " & _
                           "FROM hremployee_master " & _
                           "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         stationunkid " & _
                           "        ,deptgroupunkid " & _
                           "        ,departmentunkid " & _
                           "        ,sectiongroupunkid " & _
                           "        ,sectionunkid " & _
                           "        ,unitgroupunkid " & _
                           "        ,unitunkid " & _
                           "        ,teamunkid " & _
                           "        ,classgroupunkid " & _
                           "        ,classunkid " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                           "    FROM hremployee_transfer_tran " & _
                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                           ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                           "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         jobgroupunkid " & _
                           "        ,jobunkid " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                           "    FROM hremployee_categorization_tran " & _
                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                           ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                           "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         cctranheadvalueid " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                           "    FROM hremployee_cctranhead_tran " & _
                           "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                           ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    StrQ &= "WHERE hremployee_master.employeeunkid " & _
                            "IN " & _
                            "( " & _
                            "	SELECT " & _
                            "	    hrassessor_tran.employeeunkid " & _
                            "	FROM hrassessor_tran " & _
                            "	    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                            "	    JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                            "	WHERE hrassessor_master.assessormasterunkid = @AssessorId AND hrapprover_usermapping.userunkid = " & xUserUnkid & " " & _
                            "       AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                            "       AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                            ") "
                    'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ &= xDateFilterQry & " "
                        End If
                    End If

                    Dim strInQry As String = ""

                    strInQry = "SELECT DISTINCT " & _
                               "     hrassess_group_master.assessgroupunkid " & _
                               "    ,referenceunkid " & _
                               "    ,STUFF((SELECT ',' + CAST(md.allocationunkid AS NVARCHAR(30)) " & _
                               "            FROM hrassess_group_tran md " & _
                               "            WHERE hrassess_competence_assign_master.assessgroupunkid = md.assessgroupunkid " & _
                               "            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS allocationids " & _
                               "FROM hrassess_competence_assign_master " & _
                               "    JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                               "    JOIN hrassess_group_tran ON hrassess_group_master.assessgroupunkid = hrassess_group_tran.assessgroupunkid " & _
                               "WHERE isvoid = 0 AND periodunkid = '" & intPeriodId & "' AND hrassess_group_master.isactive = 1 AND hrassess_group_tran.isactive = 1 "

                    If intAGroupId > 0 Then
                        strInQry &= " AND hrassess_group_master.assessgroupunkid = '" & intAGroupId & "'"
                    End If

                    dsMatchedEmp = objDo.ExecQuery(strInQry, strList)

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    If dsMatchedEmp.Tables(strList).Rows.Count > 0 Then
                        For Each dr As DataRow In dsMatchedEmp.Tables(strList).Rows
                            Select Case CInt(dr("referenceunkid"))
                                Case enAllocation.BRANCH : StrQ &= " AND T.stationunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.DEPARTMENT_GROUP : StrQ &= " AND T.deptgroupunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.DEPARTMENT : StrQ &= " AND T.departmentunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.SECTION_GROUP : StrQ &= " AND T.sectiongroupunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.SECTION : StrQ &= " AND T.sectionunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.UNIT_GROUP : StrQ &= " AND T.unitgroupunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.UNIT : StrQ &= " AND T.unitunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.TEAM : StrQ &= " AND T.teamunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.JOB_GROUP : StrQ &= " AND J.jobgroupunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.JOBS : StrQ &= " AND J.jobunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.EMPLOYEE : StrQ &= " AND hremployee_master.employeeunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.CLASS_GROUP : StrQ &= " AND T.classgroupunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.CLASSES : StrQ &= " AND T.classunkid IN (" & dr("allocationids") & ")"
                                Case enAllocation.COST_CENTER : StrQ &= " AND C.cctranheadvalueid IN (" & dr("allocationids") & ")"
                            End Select
                        Next
                    End If

                    Select Case iExOder
                        Case 1 : StrQ &= " AND hremployee_master.employeeunkid IN (SELECT DISTINCT employeeunkid FROM hrassess_empfield1_master WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0) "
                        Case 2 : StrQ &= " AND hremployee_master.employeeunkid IN (SELECT DISTINCT employeeunkid FROM hrassess_empfield2_master WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0) "
                        Case 3 : StrQ &= " AND hremployee_master.employeeunkid IN (SELECT DISTINCT employeeunkid FROM hrassess_empfield3_master WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0) "
                        Case 4 : StrQ &= " AND hremployee_master.employeeunkid IN (SELECT DISTINCT employeeunkid FROM hrassess_empfield4_master WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0) "
                        Case 5 : StrQ &= " AND hremployee_master.employeeunkid IN (SELECT DISTINCT employeeunkid FROM hrassess_empfield5_master WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0) "
                    End Select

                    If intAllocaitonUnkId > 0 Then
                        Select Case CInt(intAllocationTypeId)
                            Case enAllocation.BRANCH : StrQ &= " AND T.stationunkid  = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.DEPARTMENT_GROUP : StrQ &= " AND T.deptgroupunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.DEPARTMENT : StrQ &= " AND T.departmentunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.SECTION_GROUP : StrQ &= " AND T.sectiongroupunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.SECTION : StrQ &= " AND T.sectionunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.UNIT_GROUP : StrQ &= " AND T.unitgroupunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.UNIT : StrQ &= " AND T.unitunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.TEAM : StrQ &= " AND T.teamunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.JOB_GROUP : StrQ &= " AND J.jobgroupunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.JOBS : StrQ &= " AND J.jobunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.EMPLOYEE : StrQ &= " AND hremployee_master.employeeunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.CLASS_GROUP : StrQ &= " AND T.classgroupunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.CLASSES : StrQ &= " AND T.classunkid = '" & intAllocaitonUnkId & "' "
                            Case enAllocation.COST_CENTER : StrQ &= " AND C.cctranheadvalueid = '" & intAllocaitonUnkId & "' "
                        End Select
                    End If


                    dsMatchedEmp = New DataSet
                    dsMatchedEmp = objDo.ExecQuery(StrQ, strList)

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    intTotalPlanned = dsMatchedEmp.Tables(strList).Rows.Count

                    'Dim rowsOnlyInDt1 = From r In dsList.Tables(strList).AsEnumerable() Where Not dsMatchedEmp.Tables(strList).AsEnumerable().Any(Function(r2) r.Field(Of Integer)("Id") = r2.Field(Of Integer)("Id")) Select r
                    'dsList.Tables.Remove(strList)
                    'dsList.Tables.Add(rowsOnlyInDt1.CopyToDataTable())

                    Dim rowsOnlyInDt1 = From r In dsList.Tables(strList).AsEnumerable() Where dsMatchedEmp.Tables(strList).AsEnumerable().Any(Function(r2) r.Field(Of Integer)("Id") = r2.Field(Of Integer)("Id")) Select r

                    For Each r In rowsOnlyInDt1
                        r.Item("isplan") = True
                    Next

                    dsList.Tables(strList).AcceptChanges()

                End If
                'S.SANDEEP [10 DEC 2015] -- END

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getEmployeeBasedAssessor; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function
    'S.SANDEEP [04 JUN 2015] -- END

    Public Function GetUncommitedInfo(ByVal intPeriodId As Integer) As Integer
        Dim intUCnt As Integer = -1
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation
            StrQ = "SELECT " & _
                   "	analysisunkid " & _
                   "FROM hrevaluation_analysis_master " & _
                   "WHERE ISNULL(isvoid,0) = 0 " & _
                   "	AND ISNULL(iscommitted,0) = 0 " & _
                   "    AND periodunkid = @Pid "

            objDataOperation.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            intUCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return intUCnt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUncommitedInfo; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Unlock_Commit(ByVal intPeriodUnkid As Integer, ByVal iEmployeeId As Integer, ByRef strMsg As String) As Boolean 'S.SANDEEP [ 10 SEPT 2013 ] -- START -- END
        'S.SANDEEP |13-NOV-2019| -- START {strMsg} -- END
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim blnFlag As Boolean = True
        Try

            StrQ = "SELECT 1 FROM hrapps_finalemployee " & _
                   "	JOIN hrapps_shortlist_master ON hrapps_finalemployee.shortlistunkid = hrapps_shortlist_master.shortlistunkid " & _
                   "WHERE hrapps_finalemployee.isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND apprperiodunkid <= 0 AND (ISNULL(operationmodeid,0) > 0 OR isfinalshortlisted = 1) " & _
                   "AND hrapps_shortlist_master.periodunkid = '" & intPeriodUnkid & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
                'S.SANDEEP |13-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                strMsg = Language.getMessage("frmSelfEvaluationList", 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
                'S.SANDEEP |13-NOV-2019| -- END
            End If

            iCnt = -1
            StrQ = "SELECT 1 " & _
                   "FROM hrapps_finalemployee " & _
                   "WHERE isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND apprperiodunkid > 0 AND (ISNULL(operationmodeid,0) > 0 OR isfinalshortlisted = 1) " & _
                   "AND apprperiodunkid = '" & intPeriodUnkid & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
                'S.SANDEEP |13-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                strMsg = Language.getMessage("frmSelfEvaluationList", 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
                'S.SANDEEP |13-NOV-2019| -- END
            End If

            'S.SANDEEP |13-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
            iCnt = -1
            StrQ = "SELECT 1 " & _
                   "FROM hrassess_computescore_approval_tran hcat " & _
                   "WHERE hcat.isvoid = 0 AND hcat.employeeunkid = '" & iEmployeeId & "' AND hcat.periodunkid = '" & intPeriodUnkid & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
                'S.SANDEEP |13-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                strMsg = Language.getMessage(mstrModuleName, 164, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Calibration.")
                'S.SANDEEP |13-NOV-2019| -- END
            End If
            'S.SANDEEP |13-NOV-2019| -- END

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Unlock_Commit; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Public Function Get_BSC_Evaluation_Data(ByVal iMode As enAssessmentMode, _
    '                                        ByVal iEmployeeId As Integer, _
    '                                        ByVal iPeriodId As Integer, _
    '                                        ByVal iCascadingTypeId As Integer, _
    '                                        ByVal iAssessorId As Integer, _
    '                                        ByVal iReviewerId As Integer, _
    '                                        ByVal iAnalysisId As Integer, _
    '                                        Optional ByVal blnOnlyCommittedScore As Boolean = False) As DataTable



    Public Function Get_BSC_Evaluation_Data(ByVal iMode As enAssessmentMode, _
                                            ByVal iEmployeeId As Integer, _
                                            ByVal iPeriodId As Integer, _
                                            ByVal iCascadingTypeId As Integer, _
                                            ByVal iAssessorId As Integer, _
                                            ByVal iReviewerId As Integer, _
                                            ByVal iAnalysisId As Integer, _
                                            ByVal iIsEnableAutoRating As Boolean, _
                                            ByVal iScoringOptionid As Integer, _
                                            ByVal iDataBaseName As String, _
                                            ByVal iDontAllowExceed100 As Boolean, _
                                            Optional ByVal blnOnlyCommittedScore As Boolean = False, _
                                            Optional ByVal iFmtCurrency As String = "") As DataTable
        'S.SANDEEP |17-MAY-2021| -- START {iDontAllowExceed100} -- END
        'S.SANDEEP |18-FEB-2019| -- START {iFmtCurrency} -- END
        'S.SANDEEP [04-AUG-2017] -- START {iDataBaseName} -- END

        'Shani (26-Sep-2016) -- End
        'S.SANDEEP [04 JUN 2015] -- START {blnOnlyCommittedScore} -- END
        Dim StrQ As String = String.Empty
        Dim mdtFinal As DataTable = Nothing
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dCol As DataColumn
        Dim iCaptionName As String = String.Empty
        Dim iMappedLinkedFieldId As Integer = 0
        Try
            Dim objFMaster As New clsAssess_Field_Master(True)
            mdtFinal = New DataTable("Evaluation")

            'dCol = New DataColumn
            'dCol.ColumnName = "Perspective"
            'dCol.Caption = Language.getMessage(mstrModuleName, 101, "Perspective")
            'dCol.DataType = System.Type.GetType("System.String")
            'dCol.DefaultValue = ""
            'mdtFinal.Columns.Add(dCol)

            Dim objMap As New clsAssess_Field_Mapping
            iCaptionName = objMap.Get_Map_FieldName(iPeriodId)
            iMappedLinkedFieldId = objMap.Get_Map_FieldId(iPeriodId)
            objMap = Nothing


            Dim dsResult As New DataSet
            Dim iEAssessorId, iEReviewerId As Integer
            iEAssessorId = 0 : iEReviewerId = 0

            'S.SANDEEP [22 Jan 2016] -- START
            'MULTIPULE ASSESSOR/REVIEWER CASE
            'StrQ = "SELECT " & _
            '       "     hrassessor_master.assessormasterunkid " & _
            '       "    ,hrassessor_master.employeeunkid " & _
            '       "    ,hrassessor_master.isreviewer " & _
            '       "FROM hrassessor_tran " & _
            '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
            '       "WHERE hrassessor_tran.employeeunkid = " & iEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 "

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'StrQ = "SELECT " & _
            '       "     CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN ASR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
            '       "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN RVR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
            '       "     END AS assessormasterunkid " & _
            '       "    ,CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
            '       "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
            '       "     END AS isfound " & _
            '       "    ,hrassessor_master.employeeunkid " & _
            '       "    ,hrassessor_master.isreviewer " & _
            '       "    ,hrassessor_tran.visibletypeid " & _
            '       "FROM hrassessor_tran " & _
            '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
            '       "    LEFT JOIN hrevaluation_analysis_master ASR ON ASR.assessormasterunkid = hrassessor_master.assessormasterunkid AND ASR.periodunkid = '" & iPeriodId & "' AND ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
            '       "    LEFT JOIN hrevaluation_analysis_master RVR ON RVR.assessormasterunkid = hrassessor_master.assessormasterunkid AND RVR.periodunkid = '" & iPeriodId & "' AND RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
            '       "WHERE hrassessor_tran.employeeunkid = " & iEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid,ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid,RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid}-- END
            'S.SANDEEP [22 Jan 2016] -- END
            'dsResult = objDataOpr.ExecQuery(StrQ, "List")

            'If objDataOpr.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            '    Throw exForce
            'End If

            dsResult = GetEmployee_AssessorReviewerDetails(iPeriodId, iDataBaseName, Nothing, iEmployeeId, objDataOpr)
            'S.SANDEEP [04-AUG-2017] -- END





            If dsResult.Tables(0).Rows.Count > 0 Then
                If iAssessorId > 0 Then iEAssessorId = iAssessorId
                If iReviewerId > 0 Then iEReviewerId = iReviewerId
                Dim dtmp() As DataRow = Nothing
                If iEAssessorId <= 0 Then
                    'S.SANDEEP [22 Jan 2016] -- START
                    'MULTIPULE ASSESSOR/REVIEWER CASE
                    'dtmp = dsResult.Tables(0).Select("isreviewer = 0")
                    'If dtmp.Length > 0 Then
                    '    iEAssessorId = dtmp(0).Item("assessormasterunkid")
                    'End If

                    dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEAssessorId = dtmp(0).Item("assessormasterunkid")
                    Else
                        'S.SANDEEP [27 DEC 2016] -- START
                        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                        'dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 0")
                        dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                        'S.SANDEEP [27 DEC 2016] -- END
                        If dtmp.Length > 0 Then
                            iEAssessorId = dtmp(0).Item("assessormasterunkid")
                        End If
                    End If
                    'S.SANDEEP [22 Jan 2016] -- END
                End If

                If iEReviewerId <= 0 Then
                    'S.SANDEEP [22 Jan 2016] -- START
                    'MULTIPULE ASSESSOR/REVIEWER CASE
                    'dtmp = dsResult.Tables(0).Select("isreviewer = 1")
                    'If dtmp.Length > 0 Then
                    '    iEReviewerId = dtmp(0).Item("assessormasterunkid")
                    'End If

                    dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEReviewerId = dtmp(0).Item("assessormasterunkid")
                    Else
                        'S.SANDEEP [27 DEC 2016] -- START
                        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                        'dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 0")
                        dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")
                        'S.SANDEEP [27 DEC 2016] -- END
                        If dtmp.Length > 0 Then
                            iEReviewerId = dtmp(0).Item("assessormasterunkid")
                        End If
                    End If
                    'S.SANDEEP [22 Jan 2016] -- END
                End If
            End If

            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                dCol = New DataColumn
                dCol.ColumnName = "CoyField1"
                dCol.Caption = Language.getMessage(mstrModuleName, 102, "Company") & " " & objFMaster._Field1_Caption
                dCol.DataType = System.Type.GetType("System.String")
                dCol.DefaultValue = ""
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "OwrField1"
                dCol.Caption = Language.getMessage(mstrModuleName, 103, "Owner") & " " & objFMaster._Field1_Caption
                dCol.DataType = System.Type.GetType("System.String")
                dCol.DefaultValue = ""
                mdtFinal.Columns.Add(dCol)
            End If

            dsList = objFMaster.Get_Field_Mapping("List")
            'S.SANDEEP |08-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : INFO FIELD NOT DISPLAYED IN LIST (PA)
            'Dim i As Integer = 1
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    For Each dRow As DataRow In dsList.Tables(0).Rows
            '        dCol = New DataColumn
            '        dCol.ColumnName = "Field" & i.ToString
            '        dCol.Caption = dRow.Item("fieldcaption").ToString
            '        dCol.DataType = System.Type.GetType("System.String")
            '        dCol.DefaultValue = ""
            '        mdtFinal.Columns.Add(dCol)

            '        dCol = New DataColumn
            '        dCol.ColumnName = "Field" & i.ToString & "Id"
            '        dCol.DataType = System.Type.GetType("System.Int32")
            '        dCol.Caption = ""
            '        dCol.DefaultValue = 0
            '        mdtFinal.Columns.Add(dCol)
            '        i += 1
            '    Next
            'End If
            Dim i As Integer = 1
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    dCol = New DataColumn
                    If i <= 5 AndAlso CBool(dRow("isinformational")) = False Then
                    dCol.ColumnName = "Field" & i.ToString
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString
                    End If
                    dCol.Caption = dRow.Item("fieldcaption").ToString
                    dCol.DataType = System.Type.GetType("System.String")
                    dCol.DefaultValue = ""
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    If i <= 5 AndAlso CBool(dRow("isinformational")) = False Then
                    dCol.ColumnName = "Field" & i.ToString & "Id"
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString & "Id"
                    End If
                    dCol.DataType = System.Type.GetType("System.Int32")
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    mdtFinal.Columns.Add(dCol)
                    i += 1
                Next
            End If
            'S.SANDEEP |08-FEB-2019| -- END
            

            dCol = New DataColumn
            dCol.ColumnName = "Score"
            dCol.Caption = Language.getMessage(mstrModuleName, 112, "Score Guide")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = Language.getMessage(mstrModuleName, 112, "Score Guide")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Weight"
            'dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 104, "Weight")
            dCol.Caption = Language.getMessage(mstrModuleName, 104, "Wgt.")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim objEmp As New clsEmployee_Master
            'objEmp._Employeeunkid = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END


            dCol = New DataColumn
            dCol.ColumnName = "eself"
            'dCol.Caption = objEmp._Firstname & " " & objEmp._Surname & " - " & Language.getMessage(mstrModuleName, 105, "Self Assessment")
            dCol.Caption = Language.getMessage(mstrModuleName, 105, "Emp. Score")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            dCol = New DataColumn
            dCol.ColumnName = "analysistranunkid"
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP |18-JAN-2020| -- END

            dCol = New DataColumn
            dCol.ColumnName = "eremark"
            'dCol.Caption = objEmp._Firstname & " " & objEmp._Surname & " - " & Language.getMessage(mstrModuleName, 107, "Self Remark")
            dCol.Caption = Language.getMessage(mstrModuleName, 107, "Emp. Remark")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtFinal.Columns.Add(dCol)
            Dim objAssessor As New clsAssessor
            Select Case iMode
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "aself"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 108, "Assessor Self Assessment")
                    dCol.Caption = Language.getMessage(mstrModuleName, 108, "Sup. Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "aremark"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 109, "Assessor Self Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 109, "Sup. Remark")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreedscore"
                    dCol.Caption = Language.getMessage(mstrModuleName, 126, "Agreed Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End

                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "aself"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 108, "Assessor Self Assessment")
                    dCol.Caption = Language.getMessage(mstrModuleName, 108, "Sup. Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "aremark"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 109, "Assessor Self Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 109, "Sup. Remark")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreedscore"
                    dCol.Caption = Language.getMessage(mstrModuleName, 126, "Agreed Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End

                    objAssessor._Assessormasterunkid = iReviewerId
                    dCol = New DataColumn
                    dCol.ColumnName = "rself"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 110, "Reviewer Assessment")
                    dCol.Caption = Language.getMessage(mstrModuleName, 110, "Rev. Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "rremark"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 111, "Reviewer Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 111, "Rev. Remark")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)
            End Select

            dCol = New DataColumn
            dCol.ColumnName = "escore"
            dCol.Caption = ""
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtFinal.Columns.Add(dCol)

            Select Case iMode
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "ascore"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreed_score"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End

                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "ascore"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreed_score"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End

                    objAssessor._Assessormasterunkid = iReviewerId
                    dCol = New DataColumn
                    dCol.ColumnName = "rscore"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)
            End Select
            objAssessor = Nothing ': objEmp = Nothing

            dCol = New DataColumn
            dCol.ColumnName = "periodunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield1unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield2unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield3unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield4unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield5unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "IsGrp"
            dCol.DefaultValue = False
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GrpId"
            dCol.DefaultValue = 0
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "perspectiveunkid"
            dCol.DefaultValue = 0
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "St_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 113, "Start Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Ed_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "End Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatus"
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "Status")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "pct_complete"
            dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 11, "% Completed")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "scalemasterunkid"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [11-OCT-2018] -- START
            dCol = New DataColumn
            dCol.ColumnName = "dgoaltype"
            dCol.Caption = iCaptionName & " " & Language.getMessage("clsAssess_Field_Master", 14, "Goal Type")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = 0
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "dgoalvalue"
            dCol.Caption = iCaptionName & " " & Language.getMessage("clsAssess_Field_Master", 15, "Goal Value")
            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'dCol.DataType = GetType(System.Double)
            'dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE))
            'dCol.DefaultValue = 0
            dCol.DataType = GetType(System.String)
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE))
            dCol.DefaultValue = ""
            'S.SANDEEP |18-FEB-2019| -- END

            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [11-OCT-2018] -- END

            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If iFmtCurrency.Trim.Length <= 0 Then iFmtCurrency = GUI.fmtCurrency

            dCol = New DataColumn
            dCol.ColumnName = "UoMTypeId"
            dCol.Caption = ""
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "UoMType"
            dCol.Caption = ""
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP |18-FEB-2019| -- END


            'S.SANDEEP [21 JAN 2015] -- START
            '****************************************** COMPUTATION PURPOSE
            mdtFinal.Columns.Add("iPeriodId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFinal.Columns.Add("iItemUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFinal.Columns.Add("iEmployeeId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFinal.Columns.Add("iScore", System.Type.GetType("System.Decimal")).DefaultValue = 0

            mdtFinal.Columns.Add("eitem_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("emax_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0

            mdtFinal.Columns.Add("aitem_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("amax_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0

            mdtFinal.Columns.Add("ritem_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("rmax_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            '****************************************** COMPUTATION PURPOSE

            'S.SANDEEP [21 JAN 2015] -- END


            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            mdtFinal.Columns.Add("oWeight", GetType(System.Decimal)).DefaultValue = 0
            'S.SANDEEP |09-JUL-2019| -- END


            'StrQ = " SELECT '" & enBSCPerspective.FINANCIAL & "' AS Id ,@FINANCIAL AS NAME UNION " & _
            '       " SELECT '" & enBSCPerspective.CUSTOMER & "' AS Id ,@CUSTOMER AS NAME UNION " & _
            '       " SELECT '" & enBSCPerspective.BUSINESS_PROCESS & "' AS Id ,@BUSINESS_PROCESS AS NAME UNION " & _
            '       " SELECT '" & enBSCPerspective.ORGANIZATION_CAPACITY & "' AS Id ,@ORGANIZATION_CAPACITY AS NAME "

            'objDataOpr.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            'objDataOpr.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            'objDataOpr.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            'objDataOpr.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            StrQ = "SELECT perspectiveunkid AS Id,name AS Name FROM hrassess_perspective_master WITH (NOLOCK) WHERE isactive = 1 "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim dRow As DataRow = mdtFinal.NewRow
                If mdtFinal.Columns.Contains("Field1") Then
                    dRow.Item("Field1") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field2") Then
                    dRow.Item("Field2") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field3") Then
                    dRow.Item("Field3") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field4") Then
                    dRow.Item("Field4") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field5") Then
                    dRow.Item("Field5") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field6") Then
                    dRow.Item("Field6") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field7") Then
                    dRow.Item("Field7") = dtRow.Item("NAME")
                End If
                If mdtFinal.Columns.Contains("Field8") Then
                    dRow.Item("Field8") = dtRow.Item("NAME")
                End If
                dRow.Item("perspectiveunkid") = dtRow.Item("Id")
                dRow.Item("IsGrp") = True
                dRow.Item("GrpId") = dtRow.Item("Id")
                mdtFinal.Rows.Add(dRow)
            Next

            Dim iExOrder As Integer = objFMaster.Get_Field_ExOrder(iMappedLinkedFieldId, True)

            If mdtFinal.Rows.Count > 0 Then
                Dim iRCnt As Integer = mdtFinal.Rows.Count
                'S.SANDEEP [ 01 JAN 2015 ] -- START
                'StrQ = "DECLARE @LinkFieldId AS INT " & _
                '       "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WHERE periodunkid = '" & iPeriodId & "' AND isactive = 1),0) " & _
                '       "SELECT perspectiveunkid " & _
                '       ",CASE WHEN ISNULL(scalemasterunkid,0) <= 0 THEN ISNULL(scalemasterunkid1,0) ELSE ISNULL(scalemasterunkid,0) END AS scalemasterunkid "

                StrQ = "DECLARE @LinkFieldId AS INT " & _
                       "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WITH (NOLOCK) WHERE periodunkid = '" & iPeriodId & "' AND isactive = 1),0) " & _
                       "SELECT perspectiveunkid " & _
                       ",ISNULL(scalemasterunkid,0) AS scalemasterunkid "
                'S.SANDEEP [ 01 JAN 2015 ] -- END
                If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    StrQ &= ",CoyField1 " & _
                            ",OwrField1 "
                End If

                'S.SANDEEP [11-OCT-2018] -- START
                StrQ &= ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1GoalTypeId " & _
                        "              WHEN @LinkFieldId = Field2Id THEN f2GoalTypeId " & _
                        "              WHEN @LinkFieldId = Field3Id THEN f3GoalTypeId " & _
                        "              WHEN @LinkFieldId = Field4Id THEN f4GoalTypeId " & _
                        "              WHEN @LinkFieldId = Field5Id THEN f5GoalTypeId " & _
                        "   END, " & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
                        ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1GoalValue " & _
                        "              WHEN @LinkFieldId = Field2Id THEN f2GoalValue " & _
                        "              WHEN @LinkFieldId = Field3Id THEN f3GoalValue " & _
                        "              WHEN @LinkFieldId = Field4Id THEN f4GoalValue " & _
                        "              WHEN @LinkFieldId = Field5Id THEN f5GoalValue " & _
                        "   END, 0) AS goalvalue "

                StrQ &= ", CASE WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1GoalTypeId " & _
                        "                        WHEN @LinkFieldId = Field2Id THEN f2GoalTypeId " & _
                        "                        WHEN @LinkFieldId = Field3Id THEN f3GoalTypeId " & _
                        "                        WHEN @LinkFieldId = Field4Id THEN f4GoalTypeId " & _
                        "                        WHEN @LinkFieldId = Field5Id THEN f5GoalTypeId " & _
                        "                   END, " & CInt(enGoalType.GT_QUALITATIVE) & ") = " & CInt(enGoalType.GT_QUALITATIVE) & " THEN @GT_QUALITATIVE " & _
                        " ELSE @GT_QUANTITATIVE END AS dgoaltype "
                'S.SANDEEP [11-OCT-2018] -- END

                'S.SANDEEP |18-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                StrQ &= ",ISNULL(UoM.name,'') AS UoMType " & _
                        ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1UoMTypeId " & _
                        "             WHEN @LinkFieldId = Field2Id THEN f2UoMTypeId " & _
                        "             WHEN @LinkFieldId = Field3Id THEN f3UoMTypeId " & _
                        "             WHEN @LinkFieldId = Field4Id THEN f4UoMTypeId " & _
                        "             WHEN @LinkFieldId = Field5Id THEN f5UoMTypeId END,0) AS UoMTypeId "
                'S.SANDEEP |18-FEB-2019| -- END

                StrQ &= ",Field1 " & _
                         ",Field2 " & _
                         ",Field3 " & _
                         ",Field4 " & _
                         ",Field5 " & _
                         ",w1 " & _
                         ",w2 " & _
                         ",w3 " & _
                         ",w4 " & _
                         ",w5 " & _
                         ",Field1Id " & _
                         ",Field2Id " & _
                         ",Field3Id " & _
                         ",Field4Id " & _
                         ",Field5Id " & _
                         ",empfield1unkid " & _
                         ",empfield2unkid " & _
                         ",empfield3unkid " & _
                         ",empfield4unkid " & _
                         ",empfield5unkid " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_SDate " & _
                                      "WHEN @LinkFieldId = Field2Id THEN f2_SDate " & _
                                      "WHEN @LinkFieldId = Field3Id THEN f3_SDate " & _
                                      "WHEN @LinkFieldId = Field4Id THEN f4_SDate " & _
                                      "WHEN @LinkFieldId = Field5Id THEN f5_SDate " & _
                                "END, '') AS St_Date " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_EDate " & _
                                      "WHEN @LinkFieldId = Field2Id THEN f2_EDate " & _
                                      "WHEN @LinkFieldId = Field3Id THEN f3_EDate " & _
                                      "WHEN @LinkFieldId = Field4Id THEN f4_EDate " & _
                                      "WHEN @LinkFieldId = Field5Id THEN f5_EDate " & _
                                "END, '') AS Ed_Date " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN CASE WHEN f1pct <= 0 THEN '' ELSE CAST(f1pct AS NVARCHAR(MAX)) END " & _
                                      "WHEN @LinkFieldId = Field2Id THEN CASE WHEN f2pct <= 0 THEN '' ELSE CAST(f2pct AS NVARCHAR(MAX)) END " & _
                                      "WHEN @LinkFieldId = Field3Id THEN CASE WHEN f3pct <= 0 THEN '' ELSE CAST(f3pct AS NVARCHAR(MAX)) END " & _
                                      "WHEN @LinkFieldId = Field4Id THEN CASE WHEN f4pct <= 0 THEN '' ELSE CAST(f4pct AS NVARCHAR(MAX)) END " & _
                                      "WHEN @LinkFieldId = Field5Id THEN CASE WHEN f5pct <= 0 THEN '' ELSE CAST(f5pct AS NVARCHAR(MAX)) END " & _
                                "END, '') AS pct_complete " & _
                         ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN '" & enWeight_Types.WEIGHT_FIELD1 & "' " & _
                                      "WHEN @LinkFieldId = Field2Id THEN '" & enWeight_Types.WEIGHT_FIELD2 & "' " & _
                                      "WHEN @LinkFieldId = Field3Id THEN '" & enWeight_Types.WEIGHT_FIELD3 & "' " & _
                                      "WHEN @LinkFieldId = Field4Id THEN '" & enWeight_Types.WEIGHT_FIELD4 & "' " & _
                                      "WHEN @LinkFieldId = Field5Id THEN '" & enWeight_Types.WEIGHT_FIELD5 & "' " & _
                                "END, 0) AS CFieldTypeId " & _
                         ",ISNULL(CASE WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                       "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                       "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                       "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                       "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 1 THEN @ST_PENDING " & _
                                     "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                      "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                      "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                      "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                      "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 2 THEN @ST_INPROGRESS " & _
                                     "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                      "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                      "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                      "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                      "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 3 THEN @ST_COMPLETE " & _
                                     "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                      "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                      "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                      "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                      "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 4 THEN @ST_CLOSED " & _
                                     "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                      "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                      "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                      "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                      "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 5 THEN @ST_ONTRACK " & _
                                     "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                      "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                      "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                      "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                      "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 6 THEN @ST_ATRISK " & _
                                     "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                      "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                      "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                      "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                      "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                 "END, 0) = 7 THEN @ST_NOTAPPLICABLE " & _
                                     "END, '') AS CStatus " & _
                                     ",b.max_scale AS max_scale " & _
                    "FROM " & _
                    "( "

                StrQ &= "SELECT "
                If iCascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    StrQ &= " hrassess_empfield1_master.perspectiveunkid "
                Else
                    StrQ &= " ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid " & _
                            ",ISNULL(hrassess_coyfield1_master.field_data,'') AS CoyField1 " & _
                            ",ISNULL(hrassess_owrfield1_master.field_data,'') AS OwrField1 "
                End If
                StrQ &= " ,ISNULL(hrassess_empfield1_master.field_data,'') AS Field1 " & _
                        " ,ISNULL(hrassess_empfield2_master.field_data,'') AS Field2 " & _
                        " ,ISNULL(hrassess_empfield3_master.field_data,'') AS Field3 " & _
                        " ,ISNULL(hrassess_empfield4_master.field_data,'') AS Field4 " & _
                        " ,ISNULL(hrassess_empfield5_master.field_data,'') AS Field5 " & _
                        " ,ISNULL(hrassess_empfield1_master.weight,0) AS w1 " & _
                        " ,ISNULL(hrassess_empfield2_master.weight,0) AS w2 " & _
                        " ,ISNULL(hrassess_empfield3_master.weight,0) AS w3 " & _
                        " ,ISNULL(hrassess_empfield4_master.weight,0) AS w4 " & _
                        " ,ISNULL(hrassess_empfield5_master.weight,0) AS w5 " & _
                        " ,ISNULL(hrassess_empfield1_master.fieldunkid,0) AS Field1Id " & _
                        " ,ISNULL(hrassess_empfield2_master.fieldunkid,0) AS Field2Id " & _
                        " ,ISNULL(hrassess_empfield3_master.fieldunkid,0) AS Field3Id " & _
                        " ,ISNULL(hrassess_empfield4_master.fieldunkid,0) AS Field4Id " & _
                        " ,ISNULL(hrassess_empfield5_master.fieldunkid,0) AS Field5Id " & _
                        " ,ISNULL(hrassess_empfield1_master.empfield1unkid,0) AS empfield1unkid " & _
                        " ,ISNULL(hrassess_empfield2_master.empfield2unkid,0) AS empfield2unkid " & _
                        " ,ISNULL(hrassess_empfield3_master.empfield3unkid,0) AS empfield3unkid " & _
                        " ,ISNULL(hrassess_empfield4_master.empfield4unkid,0) AS empfield4unkid " & _
                        " ,ISNULL(hrassess_empfield5_master.empfield5unkid,0) AS empfield5unkid " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                        " ,ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                        " ,ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                        " ,ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                        " ,ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                        " ,ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                        " ,ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                        " ,ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                        " ,ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                        " ,ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                        " ,ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                        " ,ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
                        " ,ISNULL(hrassess_empfield1_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f1GoalTypeId" & _
                        " ,ISNULL(hrassess_empfield1_master.goalvalue, 0) AS f1GoalValue " & _
                        " ,ISNULL(hrassess_empfield2_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f2GoalTypeId" & _
                        " ,ISNULL(hrassess_empfield2_master.goalvalue, 0) AS f2GoalValue " & _
                        " ,ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f3GoalTypeId" & _
                        " ,ISNULL(hrassess_empfield3_master.goalvalue, 0) AS f3GoalValue " & _
                        " ,ISNULL(hrassess_empfield4_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f4GoalTypeId" & _
                        " ,ISNULL(hrassess_empfield4_master.goalvalue, 0) AS f4GoalValue " & _
                        " ,ISNULL(hrassess_empfield5_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f5GoalTypeId" & _
                        " ,ISNULL(hrassess_empfield5_master.goalvalue, 0) AS f5GoalValue " & _
                        " ,ISNULL(hrassess_empfield1_master.uomtypeid, 0) AS f1UoMTypeId " & _
                        " ,ISNULL(hrassess_empfield2_master.uomtypeid, 0) AS f2UoMTypeId " & _
                        " ,ISNULL(hrassess_empfield3_master.uomtypeid, 0) AS f3UoMTypeId " & _
                        " ,ISNULL(hrassess_empfield4_master.uomtypeid, 0) AS f4UoMTypeId " & _
                        " ,ISNULL(hrassess_empfield5_master.uomtypeid, 0) AS f5UoMTypeId " & _
                        "FROM hrassess_empfield1_master WITH (NOLOCK) "

                If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                            " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 "
                End If

                StrQ &= " LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                        "WHERE hrassess_empfield1_master.isfinal = 1 AND hrassess_empfield1_master.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield1_master.periodunkid = '" & iPeriodId & "' AND hrassess_empfield1_master.isvoid = 0 "

                Select Case iExOrder
                    Case enWeight_Types.WEIGHT_FIELD2
                        StrQ = StrQ.Replace("LEFT JOIN hrassess_empfield2_master", " JOIN hrassess_empfield2_master")
                    Case enWeight_Types.WEIGHT_FIELD3
                        StrQ = StrQ.Replace("LEFT JOIN hrassess_empfield3_master", " JOIN hrassess_empfield3_master")
                    Case enWeight_Types.WEIGHT_FIELD4
                        StrQ = StrQ.Replace("LEFT JOIN hrassess_empfield4_master", " JOIN hrassess_empfield4_master")
                    Case enWeight_Types.WEIGHT_FIELD5
                        StrQ = StrQ.Replace("LEFT JOIN hrassess_empfield5_master", " JOIN hrassess_empfield5_master")
                End Select

                'S.SANDEEP [ 01 JAN 2015 ] -- START
                'StrQ &= " ) AS A " & _
                '        "JOIN " & _
                '        "( " & _
                '        "   SELECT " & _
                '        "        scalemasterunkid AS scalemasterunkid " & _
                '        "       ,perspectiveunkid AS PerspId " & _
                '        "   FROM hrassess_scalemapping_tran " & _
                '        "       JOIN hrassess_field_mapping ON hrassess_field_mapping.mappingunkid = hrassess_scalemapping_tran.mappingunkid " & _
                '        "   WHERE fieldunkid = @LinkFieldId AND hrassess_field_mapping.periodunkid = '" & iPeriodId & "' AND isvoid = 0 AND isactive = 1 " & _
                '        ") AS B ON A.perspectiveunkid = B.PerspId " & _
                '        "LEFT JOIN " & _
                '        "( " & _
                '        "   SELECT " & _
                '        "        scalemasterunkid AS scalemasterunkid1 " & _
                '        "       ,fieldunkid AS fldId " & _
                '        "   FROM hrassess_scalemapping_tran " & _
                '        "       JOIN hrassess_field_mapping ON hrassess_field_mapping.mappingunkid = hrassess_scalemapping_tran.mappingunkid " & _
                '        "   WHERE fieldunkid = @LinkFieldId AND hrassess_field_mapping.periodunkid = '" & iPeriodId & "' AND isvoid = 0 AND isactive = 1 " & _
                '        ")AS C ON @LinkFieldId = C.fldId " & _
                '        " ORDER BY A.perspectiveunkid "

                'S.SANDEEP [29 JAN 2015] -- START
                'StrQ &= " ) AS A " & _
                '        "JOIN " & _
                '        "( " & _
                '        "   SELECT " & _
                '        "        scalemasterunkid AS scalemasterunkid " & _
                '        "       ,perspectiveunkid AS PerspId " & _
                '        "   FROM hrassess_scalemapping_tran " & _
                '        "       JOIN hrassess_field_mapping ON hrassess_field_mapping.mappingunkid = hrassess_scalemapping_tran.mappingunkid " & _
                '        "   WHERE fieldunkid = @LinkFieldId AND hrassess_field_mapping.periodunkid = '" & iPeriodId & "' AND isvoid = 0 AND isactive = 1 " & _
                '        ") AS B ON A.perspectiveunkid = B.PerspId " & _
                '        " ORDER BY A.perspectiveunkid "

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)



                'StrQ &= " ) AS A " & _
                '        "LEFT JOIN " & _
                '        "( " & _
                '        "   SELECT " & _
                '        "        scalemasterunkid AS scalemasterunkid " & _
                '        "       ,perspectiveunkid AS PerspId " & _
                '        "   FROM hrassess_scalemapping_tran " & _
                '        "       JOIN hrassess_field_mapping ON hrassess_field_mapping.mappingunkid = hrassess_scalemapping_tran.mappingunkid " & _
                '        "   WHERE fieldunkid = @LinkFieldId AND hrassess_field_mapping.periodunkid = '" & iPeriodId & "' AND isvoid = 0 AND isactive = 1 " & _
                '        ") AS B ON A.perspectiveunkid = B.PerspId " & _
                '        " ORDER BY A.perspectiveunkid "

                StrQ &= " ) AS A " & _
                        "LEFT JOIN cfcommon_master AS UoM WITH (NOLOCK) ON UoM.masterunkid = (CASE WHEN @LinkFieldId = Field1Id THEN f1UoMTypeId " & _
                        "                                                            WHEN @LinkFieldId = Field2Id THEN f2UoMTypeId " & _
                        "                                                            WHEN @LinkFieldId = Field3Id THEN f3UoMTypeId " & _
                        "                                                            WHEN @LinkFieldId = Field4Id THEN f4UoMTypeId " & _
                        "                                                            WHEN @LinkFieldId = Field5Id THEN f5UoMTypeId END) " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        hrassess_scalemapping_tran.scalemasterunkid AS scalemasterunkid " & _
                        "       ,hrassess_scalemapping_tran.perspectiveunkid AS PerspId " & _
                        "       ,ISNULL(MAX(hrassess_scale_master.scale),0) AS max_scale " & _
                        "   FROM hrassess_scalemapping_tran WITH (NOLOCK) " & _
                        "       JOIN hrassess_field_mapping WITH (NOLOCK) ON hrassess_field_mapping.mappingunkid = hrassess_scalemapping_tran.mappingunkid " & _
                        "       LEFT JOIN hrassess_scale_master WITH (NOLOCK) On hrassess_scale_master.scalemasterunkid = hrassess_scalemapping_tran.scalemasterunkid " & _
                        "   WHERE fieldunkid = @LinkFieldId " & _
                        "       AND hrassess_field_mapping.periodunkid = '" & iPeriodId & "' " & _
                        "       AND hrassess_scalemapping_tran.isvoid = 0 " & _
                        "       AND hrassess_field_mapping.isactive = 1 " & _
                        "       AND hrassess_scale_master.isactive = 1 " & _
                        "   GROUP BY hrassess_scalemapping_tran.scalemasterunkid " & _
                        "       ,hrassess_scalemapping_tran.perspectiveunkid " & _
                        ") AS B ON A.perspectiveunkid = B.PerspId " & _
                        " ORDER BY A.perspectiveunkid "

                'Shani (26-Sep-2016) -- End
                'S.SANDEEP [29 JAN 2015] -- END


                'S.SANDEEP [ 01 JAN 2015 ] -- END



                objDataOpr.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
                objDataOpr.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
                objDataOpr.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
                objDataOpr.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
                objDataOpr.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
                objDataOpr.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
                objDataOpr.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))

                'S.SANDEEP [11-OCT-2018] -- START
                objDataOpr.AddParameter("@GT_QUALITATIVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 843, "Qualitative"))
                objDataOpr.AddParameter("@GT_QUANTITATIVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 844, "Quantitative"))
                'S.SANDEEP [11-OCT-2018] -- END

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [11-NOV-2017] -- START
                Dim mview As DataView = dsList.Tables(0).DefaultView()
                Select Case iExOrder
                    Case enWeight_Types.WEIGHT_FIELD1
                        mview.Sort = "perspectiveunkid,empfield1unkid "
                    Case enWeight_Types.WEIGHT_FIELD2
                        mview.Sort = "perspectiveunkid,empfield2unkid "
                    Case enWeight_Types.WEIGHT_FIELD3
                        mview.Sort = "perspectiveunkid,empfield3unkid "
                    Case enWeight_Types.WEIGHT_FIELD4
                        mview.Sort = "perspectiveunkid,empfield4unkid "
                    Case enWeight_Types.WEIGHT_FIELD5
                        mview.Sort = "perspectiveunkid,empfield5unkid "
                End Select
                dsList.Tables.RemoveAt(0) : dsList.Tables.Add(mview.ToTable.Copy())
                'S.SANDEEP [11-NOV-2017] -- END
                


                'Dim dsResult As New DataSet
                'Dim iEAssessorId, iEReviewerId As Integer
                'iEAssessorId = 0 : iEReviewerId = 0

                'StrQ = "SELECT " & _
                '       "     hrassessor_master.assessormasterunkid " & _
                '       "    ,hrassessor_master.employeeunkid " & _
                '       "    ,hrassessor_master.isreviewer " & _
                '       "FROM hrassessor_tran " & _
                '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                '       "WHERE hrassessor_tran.employeeunkid = " & iEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 "

                'dsResult = objDataOpr.ExecQuery(StrQ, "List")

                'If objDataOpr.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                '    Throw exForce
                'End If

                'If dsResult.Tables(0).Rows.Count > 0 Then
                '    If iAssessorId > 0 Then iEAssessorId = iAssessorId
                '    If iReviewerId > 0 Then iEReviewerId = iReviewerId
                '    Dim dtmp() As DataRow = Nothing
                '    If iEAssessorId <= 0 Then
                '        dtmp = dsResult.Tables(0).Select("isreviewer = 0")
                '        If dtmp.Length > 0 Then
                '            iEAssessorId = dtmp(0).Item("assessormasterunkid")
                '        End If
                '    End If
                '    If iEReviewerId <= 0 Then
                '        dtmp = dsResult.Tables(0).Select("isreviewer = 1")
                '        If dtmp.Length > 0 Then
                '            iEReviewerId = dtmp(0).Item("assessormasterunkid")
                '        End If
                '    End If
                'End If

                'S.SANDEEP [21 JAN 2015] -- START
                'StrQ = "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '      " ,hrgoals_analysis_tran.result AS result " & _
                '      " ,hrgoals_analysis_tran.computed_value AS computed_value " & _
                '       " ,hrgoals_analysis_tran.remark " & _
                '       " ,hrgoals_analysis_tran.empfield1unkid " & _
                '       " ,hrgoals_analysis_tran.empfield2unkid " & _
                '       " ,hrgoals_analysis_tran.empfield3unkid " & _
                '       " ,hrgoals_analysis_tran.empfield4unkid " & _
                '       " ,hrgoals_analysis_tran.empfield5unkid " & _
                '       "FROM hrgoals_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                '       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.selfemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "

                StrQ = "SELECT " & _
                       "  hrevaluation_analysis_master.assessmodeid " & _
                       " ,CAST(hrgoals_analysis_tran.result AS DECIMAL(36,2)) AS result " & _
                       " ,hrgoals_analysis_tran.remark " & _
                       " ,hrgoals_analysis_tran.empfield1unkid " & _
                       " ,hrgoals_analysis_tran.empfield2unkid " & _
                       " ,hrgoals_analysis_tran.empfield3unkid " & _
                       " ,hrgoals_analysis_tran.empfield4unkid " & _
                       " ,hrgoals_analysis_tran.empfield5unkid " & _
                       " ,hrgoals_analysis_tran.item_weight " & _
                       " ,hrgoals_analysis_tran.max_scale " & _
                       " ,CAST(hrgoals_analysis_tran.agreedscore AS DECIMAL(36,2)) AS agreedscore " & _
                       " ,hrgoals_analysis_tran.analysistranunkid " & _
                       "FROM hrgoals_analysis_tran WITH (NOLOCK) " & _
                       "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.selfemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED --------- ,hrgoals_analysis_tran.result AS result
                'REMOVED --------- ,hrgoals_analysis_tran.agreedscore
                'ADDED   --------- ,CAST(hrgoals_analysis_tran.result AS DECIMAL(36,2)) AS result
                'ADDED   --------- ,CAST(hrgoals_analysis_tran.agreedscore AS DECIMAL(36,2)) AS agreedscore
                'S.SANDEEP |21-AUG-2019| -- END

                'Shani (23-Nov-2016) -- [agreedscore]
                'S.SANDEEP [21 JAN 2015] -- END

                If iAnalysisId > 0 Then
                    StrQ &= " AND hrevaluation_analysis_master.analysisunkid = '" & iAnalysisId & "' "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                If blnOnlyCommittedScore = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                'S.SANDEEP [ 01 JAN 2015 ] -- START
                'StrQ &= "UNION ALL " & _
                '       "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '       " ,hrgoals_analysis_tran.result " & _
                '       " ,hrgoals_analysis_tran.remark " & _
                '       " ,hrgoals_analysis_tran.empfield1unkid " & _
                '       " ,hrgoals_analysis_tran.empfield2unkid " & _
                '       " ,hrgoals_analysis_tran.empfield3unkid " & _
                '       " ,hrgoals_analysis_tran.empfield4unkid " & _
                '       " ,hrgoals_analysis_tran.empfield5unkid " & _
                '       "FROM hrgoals_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                '       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & iEAssessorId & "' " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "

                'S.SANDEEP [21 JAN 2015] -- START
                'StrQ &= "UNION ALL " & _
                '       "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '       " ,hrgoals_analysis_tran.result AS result " & _
                '       " ,hrgoals_analysis_tran.computed_value AS computed_value " & _
                '       " ,hrgoals_analysis_tran.remark " & _
                '       " ,hrgoals_analysis_tran.empfield1unkid " & _
                '       " ,hrgoals_analysis_tran.empfield2unkid " & _
                '       " ,hrgoals_analysis_tran.empfield3unkid " & _
                '       " ,hrgoals_analysis_tran.empfield4unkid " & _
                '       " ,hrgoals_analysis_tran.empfield5unkid " & _
                '       "FROM hrgoals_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                '       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & iEAssessorId & "' " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                StrQ &= "UNION ALL " & _
                       "SELECT " & _
                       "  hrevaluation_analysis_master.assessmodeid " & _
                       " ,CAST(hrgoals_analysis_tran.result AS DECIMAL(36,2)) AS result " & _
                       " ,hrgoals_analysis_tran.remark " & _
                       " ,hrgoals_analysis_tran.empfield1unkid " & _
                       " ,hrgoals_analysis_tran.empfield2unkid " & _
                       " ,hrgoals_analysis_tran.empfield3unkid " & _
                       " ,hrgoals_analysis_tran.empfield4unkid " & _
                       " ,hrgoals_analysis_tran.empfield5unkid " & _
                        " ,hrgoals_analysis_tran.item_weight " & _
                        " ,hrgoals_analysis_tran.max_scale " & _
                        " ,CAST(hrgoals_analysis_tran.agreedscore AS DECIMAL(36,2)) AS agreedscore " & _
                        " ,hrgoals_analysis_tran.analysistranunkid " & _
                       "FROM hrgoals_analysis_tran WITH (NOLOCK) " & _
                       "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & iEAssessorId & "' " & _
                       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "


                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED --------- ,hrgoals_analysis_tran.result AS result
                'REMOVED --------- ,hrgoals_analysis_tran.agreedscore
                'ADDED   --------- ,CAST(hrgoals_analysis_tran.result AS DECIMAL(36,2)) AS result
                'ADDED   --------- ,CAST(hrgoals_analysis_tran.agreedscore AS DECIMAL(36,2)) AS agreedscore
                'S.SANDEEP |21-AUG-2019| -- END

                'Shani (23-Nov-2016) -- [agreedscore]
                'S.SANDEEP [21 JAN 2015] -- END

                If iAnalysisId > 0 Then
                    StrQ &= " AND hrevaluation_analysis_master.analysisunkid = '" & iAnalysisId & "' "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                If blnOnlyCommittedScore = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'S.SANDEEP [ 01 JAN 2015 ] -- START
                'StrQ &= "UNION ALL " & _
                '       "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '       " ,hrgoals_analysis_tran.result " & _
                '       " ,hrgoals_analysis_tran.remark " & _
                '       " ,hrgoals_analysis_tran.empfield1unkid " & _
                '       " ,hrgoals_analysis_tran.empfield2unkid " & _
                '       " ,hrgoals_analysis_tran.empfield3unkid " & _
                '       " ,hrgoals_analysis_tran.empfield4unkid " & _
                '       " ,hrgoals_analysis_tran.empfield5unkid " & _
                '       "FROM hrgoals_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                '       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & iEReviewerId & " " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                'S.SANDEEP [21 JAN 2015] -- START
                'StrQ &= "UNION ALL " & _
                '        "SELECT " & _
                '        "  hrevaluation_analysis_master.assessmodeid " & _
                '        " ,hrgoals_analysis_tran.result AS result " & _
                '        " ,hrgoals_analysis_tran.computed_value AS computed_value " & _
                '        " ,hrgoals_analysis_tran.remark " & _
                '        " ,hrgoals_analysis_tran.empfield1unkid " & _
                '        " ,hrgoals_analysis_tran.empfield2unkid " & _
                '        " ,hrgoals_analysis_tran.empfield3unkid " & _
                '        " ,hrgoals_analysis_tran.empfield4unkid " & _
                '        " ,hrgoals_analysis_tran.empfield5unkid " & _
                '        "FROM hrgoals_analysis_tran " & _
                '        "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                '        "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & iEReviewerId & " " & _
                '        "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                StrQ &= "UNION ALL " & _
                       "SELECT " & _
                       "  hrevaluation_analysis_master.assessmodeid " & _
                       " ,CAST(hrgoals_analysis_tran.result AS DECIMAL(36,2)) AS result " & _
                       " ,hrgoals_analysis_tran.remark " & _
                       " ,hrgoals_analysis_tran.empfield1unkid " & _
                       " ,hrgoals_analysis_tran.empfield2unkid " & _
                       " ,hrgoals_analysis_tran.empfield3unkid " & _
                       " ,hrgoals_analysis_tran.empfield4unkid " & _
                       " ,hrgoals_analysis_tran.empfield5unkid " & _
                        " ,hrgoals_analysis_tran.item_weight " & _
                        " ,hrgoals_analysis_tran.max_scale " & _
                        " ,CAST(hrgoals_analysis_tran.agreedscore AS DECIMAL(36,2)) AS agreedscore " & _
                        " ,hrgoals_analysis_tran.analysistranunkid " & _
                       "FROM hrgoals_analysis_tran WITH (NOLOCK) " & _
                       "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrgoals_analysis_tran.analysisunkid " & _
                       "WHERE hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & iEReviewerId & " " & _
                       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED --------- ,hrgoals_analysis_tran.result AS result
                'REMOVED --------- ,hrgoals_analysis_tran.agreedscore
                'ADDED   --------- ,CAST(hrgoals_analysis_tran.result AS DECIMAL(36,2)) AS result
                'ADDED   --------- ,CAST(hrgoals_analysis_tran.agreedscore AS DECIMAL(36,2)) AS agreedscore
                'S.SANDEEP |21-AUG-2019| -- END

                'Shani (23-Nov-2016) -- [agreedscore]
                'S.SANDEEP [21 JAN 2015] -- END

                If iAnalysisId > 0 Then
                    StrQ &= " AND hrevaluation_analysis_master.analysisunkid = '" & iAnalysisId & "' "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                If blnOnlyCommittedScore = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                dsResult = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [16 JUN 2015] -- START
                'S.SANDEEP [16 JUN 2015] -- START
                Dim dsProgress As New DataSet
                Dim objProgress As New clsassess_empupdate_tran
                dsProgress = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime.Date, iEmployeeId, iPeriodId)
                objProgress = Nothing
                'S.SANDEEP [16 JUN 2015] -- END

                'S.SANDEEP [16 JUN 2015] -- END


                'S.SANDEEP [11-OCT-2018] -- START
                Dim objScaleMaster As New clsAssessment_Scale
                Dim dsScale As New DataSet
                Dim strScaleMstId As String = ""
                Dim iDict As List(Of Integer) = Nothing
                strScaleMstId = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("scalemasterunkid").ToString()).Distinct().ToArray())
                If strScaleMstId.Trim.Length <= 0 Then strScaleMstId = "0" 'S.SANDEEP [29-OCT-2018] -- START -- END
                dsScale = objScaleMaster.getPecentageDefined(iPeriodId, strScaleMstId, objDataOpr, iDict)
                objScaleMaster = Nothing
                'S.SANDEEP [11-OCT-2018] -- END


                'S.SANDEEP [29 JAN 2015] -- START
                'Dim iWeightCaption As String = String.Empty
                Dim iWeightCaption As Integer = 0
                'S.SANDEEP [29 JAN 2015] -- END
                For i = 0 To iRCnt - 1
                    If CInt(mdtFinal.Rows(i).Item("perspectiveunkid")) > 0 Then
                        Dim dTemp() As DataRow = dsList.Tables(0).Select("perspectiveunkid = '" & mdtFinal.Rows(i).Item("perspectiveunkid") & "'")
                        If dTemp.Length > 0 Then
                            For iCnt As Integer = 0 To dTemp.Length - 1
                                Dim iCRow As DataRow = mdtFinal.NewRow
                                iCRow.Item("periodunkid") = iPeriodId
                                iCRow.Item("IsGrp") = False
                                iCRow.Item("GrpId") = mdtFinal.Rows(i).Item("GrpId")
                                iCRow.Item("perspectiveunkid") = mdtFinal.Rows(i).Item("perspectiveunkid")
                                iCRow.Item("scalemasterunkid") = dTemp(iCnt).Item("scalemasterunkid")
                                iCRow.Item("empfield1unkid") = dTemp(iCnt).Item("empfield1unkid")
                                iCRow.Item("empfield2unkid") = dTemp(iCnt).Item("empfield2unkid")
                                iCRow.Item("empfield3unkid") = dTemp(iCnt).Item("empfield3unkid")
                                iCRow.Item("empfield4unkid") = dTemp(iCnt).Item("empfield4unkid")
                                iCRow.Item("empfield5unkid") = dTemp(iCnt).Item("empfield5unkid")

                                'S.SANDEEP [11-OCT-2018] -- START
                                iCRow.Item("dgoaltype") = dTemp(iCnt).Item("dgoaltype")

                                'S.SANDEEP |18-FEB-2019| -- START
                                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                                'iCRow.Item("dgoalvalue") = dTemp(iCnt).Item("goalvalue")
                                iCRow.Item("dgoalvalue") = Format(CDec(dTemp(iCnt).Item("goalvalue")), iFmtCurrency).ToString() & " " & dTemp(iCnt).Item("UoMType")
                                'S.SANDEEP |18-FEB-2019| -- END

                                'S.SANDEEP [11-OCT-2018] -- END

                                If mdtFinal.Columns.Contains("Field1Id") Then
                                    iCRow.Item("Field1Id") = dTemp(iCnt).Item("Field1Id")
                                    iCRow.Item("Field1") = Space(5) & dTemp(iCnt).Item("Field1")
                                End If

                                If mdtFinal.Columns.Contains("Field2Id") Then
                                    iCRow.Item("Field2Id") = dTemp(iCnt).Item("Field2Id")
                                    iCRow.Item("Field2") = Space(5) & dTemp(iCnt).Item("Field2")
                                End If

                                If mdtFinal.Columns.Contains("Field3Id") Then
                                    iCRow.Item("Field3Id") = dTemp(iCnt).Item("Field3Id")
                                    iCRow.Item("Field3") = Space(5) & dTemp(iCnt).Item("Field3")
                                End If

                                If mdtFinal.Columns.Contains("Field4Id") Then
                                    iCRow.Item("Field4Id") = dTemp(iCnt).Item("Field4Id")
                                    iCRow.Item("Field4") = Space(5) & dTemp(iCnt).Item("Field4")
                                End If

                                If mdtFinal.Columns.Contains("Field5Id") Then
                                    iCRow.Item("Field5Id") = dTemp(iCnt).Item("Field5Id")
                                    iCRow.Item("Field5") = Space(5) & dTemp(iCnt).Item("Field5")
                                End If

                                If dTemp(iCnt).Item("St_Date").ToString.Trim.Length > 0 Then
                                    iCRow.Item("St_Date") = eZeeDate.convertDate(dTemp(iCnt).Item("St_Date").ToString).ToShortDateString
                                End If

                                If dTemp(iCnt).Item("Ed_Date").ToString.Trim.Length > 0 Then
                                    iCRow.Item("Ed_Date") = eZeeDate.convertDate(dTemp(iCnt).Item("Ed_Date").ToString).ToShortDateString
                                End If

                                'S.SANDEEP [16 JUN 2015] -- START
                                'iCRow.Item("CStatus") = dTemp(iCnt).Item("CStatus")
                                'iCRow.Item("pct_complete") = dTemp(iCnt).Item("pct_complete")

                                If dsProgress IsNot Nothing AndAlso dsProgress.Tables(0).Rows.Count > 0 Then
                                    Dim intTableUnkid As Integer = 0
                                    Select Case CInt(dTemp(iCnt).Item("CFieldTypeId"))
                                        Case enWeight_Types.WEIGHT_FIELD1
                                            intTableUnkid = dTemp(iCnt).Item("empfield1unkid")
                                        Case enWeight_Types.WEIGHT_FIELD2
                                            intTableUnkid = dTemp(iCnt).Item("empfield2unkid")
                                        Case enWeight_Types.WEIGHT_FIELD3
                                            intTableUnkid = dTemp(iCnt).Item("empfield3unkid")
                                        Case enWeight_Types.WEIGHT_FIELD4
                                            intTableUnkid = dTemp(iCnt).Item("empfield4unkid")
                                        Case enWeight_Types.WEIGHT_FIELD5
                                            intTableUnkid = dTemp(iCnt).Item("empfield5unkid")
                                    End Select

                                    Dim pRow() As DataRow = dsProgress.Tables(0).Select("empfieldtypeid = '" & dTemp(iCnt).Item("CFieldTypeId") & "' AND empfieldunkid = '" & intTableUnkid & "'")
                                    If pRow.Length > 0 Then
                                        iCRow.Item("pct_complete") = pRow(0).Item("pct_completed")
                                        iCRow.Item("CStatus") = pRow(0).Item("dstatus")
                                    End If
                                End If
                                'S.SANDEEP [16 JUN 2015] -- END

                                Dim mdicFieldData As New Dictionary(Of Integer, String)
                                Dim objInfoField As New clsassess_empinfofield_tran
                                Select Case CInt(dTemp(iCnt).Item("CFieldTypeId"))
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        mdicFieldData = objInfoField.Get_Data(dTemp(iCnt).Item("empfield1unkid"), dTemp(iCnt).Item("CFieldTypeId"))
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        mdicFieldData = objInfoField.Get_Data(dTemp(iCnt).Item("empfield2unkid"), dTemp(iCnt).Item("CFieldTypeId"))
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        mdicFieldData = objInfoField.Get_Data(dTemp(iCnt).Item("empfield3unkid"), dTemp(iCnt).Item("CFieldTypeId"))
                                    Case enWeight_Types.WEIGHT_FIELD4
                                        mdicFieldData = objInfoField.Get_Data(dTemp(iCnt).Item("empfield4unkid"), dTemp(iCnt).Item("CFieldTypeId"))
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        mdicFieldData = objInfoField.Get_Data(dTemp(iCnt).Item("empfield5unkid"), dTemp(iCnt).Item("CFieldTypeId"))
                                End Select

                                If mdicFieldData.Keys.Count > 0 Then
                                    For Each iKey As Integer In mdicFieldData.Keys
                                        If mdtFinal.Columns.Contains("Field" & iKey.ToString) Then
                                            iCRow.Item("Field" & iKey.ToString & "Id") = iKey
                                            iCRow.Item("Field" & iKey.ToString) = Space(5) & mdicFieldData(iKey)
                                        End If
                                    Next
                                End If

                                'S.SANDEEP [21 JAN 2015] -- START
                                iCRow.Item("iPeriodId") = iPeriodId
                                iCRow.Item("iEmployeeId") = iEmployeeId
                                'S.SANDEEP [21 JAN 2015] -- END

                                Select Case iExOrder
                                    Case enWeight_Types.WEIGHT_FIELD1

                                        'S.SANDEEP [29 JAN 2015] -- START
                                        If iWeightCaption <> dTemp(iCnt).Item("empfield1unkid") Then

                                        'S.SANDEEP |09-JUL-2019| -- START
                                        'ISSUE/ENHANCEMENT : PA CHANGES
                                        iCRow.Item("oWeight") = dTemp(iCnt).Item("w1")
                                        'S.SANDEEP |09-JUL-2019| -- END

                                            'If iWeightCaption <> dTemp(iCnt).Item("Field1") Then
                                            'S.SANDEEP [29 JAN 2015] -- END
                                            iCRow.Item("Weight") = dTemp(iCnt).Item("w1")
                                            If dsResult.Tables(0).Rows.Count > 0 Then
                                                Dim dtmp() As DataRow = Nothing
                                                '********** SELF ASSESSMENT **********' -- START
                                                dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND empfield1unkid = '" & dTemp(iCnt).Item("empfield1unkid") & "'")
                                                If dtmp.Length > 0 Then
                                                    iCRow.Item("eself") = dtmp(0).Item("result")
                                                    iCRow.Item("eremark") = dtmp(0).Item("remark")
                                                    iCRow.Item("escore") = dtmp(0).Item("result")
                                                    'S.SANDEEP [21 JAN 2015] -- START
                                                    iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield1unkid")
                                                    iCRow.Item("iScore") = dtmp(0).Item("result")

                                                    iCRow.Item("eitem_weight") = dtmp(0).Item("item_weight")
                                                    iCRow.Item("emax_scale") = dtmp(0).Item("max_scale")
                                                    'S.SANDEEP [21 JAN 2015] -- END
                                                    iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                End If
                                                '********** SELF ASSESSMENT **********' -- END
                                                '********** ASSESSOR ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield1unkid = '" & dTemp(iCnt).Item("empfield1unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield1unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END

                                                        'Shani (23-Nov-2016) -- Start
                                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'Shani (23-Nov-2016) -- End
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** ASSESSOR ASSESSMENT **********' -- END
                                                '********** REVIEWER ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield1unkid = '" & dTemp(iCnt).Item("empfield1unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield1unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END

                                                        'Shani (23-Nov-2016) -- Start
                                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'Shani (23-Nov-2016) -- End
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND empfield1unkid = '" & dTemp(iCnt).Item("empfield1unkid") & "'")

                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("rself") = dtmp(0).Item("result")
                                                        iCRow.Item("rremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("rscore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield1unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("ritem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("rmax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** REVIEWER ASSESSMENT **********' -- END
                                                dtmp = Nothing
                                            End If
                                            'S.SANDEEP [29 JAN 2015] -- START
                                            'iWeightCaption = dTemp(iCnt).Item("Field1")
                                            iWeightCaption = dTemp(iCnt).Item("empfield1unkid")
                                            'S.SANDEEP [29 JAN 2015] -- END
                                        End If

                                    Case enWeight_Types.WEIGHT_FIELD2

                                        'S.SANDEEP [29 JAN 2015] -- START
                                        If iWeightCaption <> dTemp(iCnt).Item("empfield2unkid") Then

                                        'S.SANDEEP |09-JUL-2019| -- START
                                        'ISSUE/ENHANCEMENT : PA CHANGES
                                        iCRow.Item("oWeight") = dTemp(iCnt).Item("w2")
                                        'S.SANDEEP |09-JUL-2019| -- END

                                            'If iWeightCaption <> dTemp(iCnt).Item("Field2") Then
                                            'S.SANDEEP [29 JAN 2015] -- END
                                            iCRow.Item("Weight") = dTemp(iCnt).Item("w2")
                                            If dsResult.Tables(0).Rows.Count > 0 Then
                                                Dim dtmp() As DataRow = Nothing
                                                '********** SELF ASSESSMENT **********' -- START
                                                dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND empfield2unkid = '" & dTemp(iCnt).Item("empfield2unkid") & "'")
                                                If dtmp.Length > 0 Then
                                                    iCRow.Item("eself") = dtmp(0).Item("result")
                                                    iCRow.Item("eremark") = dtmp(0).Item("remark")
                                                    iCRow.Item("escore") = dtmp(0).Item("result")
                                                    'S.SANDEEP [21 JAN 2015] -- START
                                                    iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield2unkid")
                                                    iCRow.Item("iScore") = dtmp(0).Item("result")

                                                    iCRow.Item("eitem_weight") = dtmp(0).Item("item_weight")
                                                    iCRow.Item("emax_scale") = dtmp(0).Item("max_scale")
                                                    'S.SANDEEP [21 JAN 2015] -- END
                                                    iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                End If
                                                '********** SELF ASSESSMENT **********' -- END

                                                '********** ASSESSOR ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield2unkid = '" & dTemp(iCnt).Item("empfield2unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield2unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        'Shani (23-Nov-2016) -- Start
                                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'Shani (23-Nov-2016) -- End
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** ASSESSOR ASSESSMENT **********' -- END

                                                '********** REVIEWER ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield2unkid = '" & dTemp(iCnt).Item("empfield2unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield2unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END

                                                        'S.SANDEEP [20-SEP-2017] -- START
                                                        'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'S.SANDEEP [20-SEP-2017] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND empfield2unkid = '" & dTemp(iCnt).Item("empfield2unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("rself") = dtmp(0).Item("result")
                                                        iCRow.Item("rremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("rscore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield2unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("ritem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("rmax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** REVIEWER ASSESSMENT **********' -- END
                                                dtmp = Nothing
                                            End If
                                            'S.SANDEEP [29 JAN 2015] -- START
                                            'iWeightCaption = dTemp(iCnt).Item("Field2")
                                            iWeightCaption = dTemp(iCnt).Item("empfield2unkid")
                                            'S.SANDEEP [29 JAN 2015] -- END
                                        End If

                                    Case enWeight_Types.WEIGHT_FIELD3

                                        'S.SANDEEP [29 JAN 2015] -- START
                                        If iWeightCaption <> dTemp(iCnt).Item("empfield3unkid") Then

                                        'S.SANDEEP |09-JUL-2019| -- START
                                        'ISSUE/ENHANCEMENT : PA CHANGES
                                        iCRow.Item("oWeight") = dTemp(iCnt).Item("w3")
                                        'S.SANDEEP |09-JUL-2019| -- END

                                            'If iWeightCaption <> dTemp(iCnt).Item("Field3") Then
                                            'S.SANDEEP [29 JAN 2015] -- END
                                            iCRow.Item("Weight") = dTemp(iCnt).Item("w3")
                                            If dsResult.Tables(0).Rows.Count > 0 Then
                                                Dim dtmp() As DataRow = Nothing
                                                '********** SELF ASSESSMENT **********' -- START
                                                dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND empfield3unkid = '" & dTemp(iCnt).Item("empfield3unkid") & "'")
                                                If dtmp.Length > 0 Then
                                                    iCRow.Item("eself") = dtmp(0).Item("result")
                                                    iCRow.Item("eremark") = dtmp(0).Item("remark")
                                                    iCRow.Item("escore") = dtmp(0).Item("result")
                                                    'S.SANDEEP [21 JAN 2015] -- START
                                                    iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield3unkid")
                                                    iCRow.Item("iScore") = dtmp(0).Item("result")

                                                    iCRow.Item("eitem_weight") = dtmp(0).Item("item_weight")
                                                    iCRow.Item("emax_scale") = dtmp(0).Item("max_scale")
                                                    'S.SANDEEP [21 JAN 2015] -- END
                                                    iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                End If
                                                '********** SELF ASSESSMENT **********' -- END

                                                '********** ASSESSOR ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield3unkid = '" & dTemp(iCnt).Item("empfield3unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield3unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        'Shani (23-Nov-2016) -- Start
                                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'Shani (23-Nov-2016) -- End
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** ASSESSOR ASSESSMENT **********' -- END

                                                '********** REVIEWER ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield3unkid = '" & dTemp(iCnt).Item("empfield3unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield3unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END

                                                        'S.SANDEEP [20-SEP-2017] -- START
                                                        'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'S.SANDEEP [20-SEP-2017] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND empfield3unkid = '" & dTemp(iCnt).Item("empfield3unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("rself") = dtmp(0).Item("result")
                                                        iCRow.Item("rremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("rscore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield3unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("ritem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("rmax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** REVIEWER ASSESSMENT **********' -- END
                                                dtmp = Nothing
                                            End If
                                            'S.SANDEEP [29 JAN 2015] -- START
                                            'iWeightCaption = dTemp(iCnt).Item("Field3")
                                            iWeightCaption = dTemp(iCnt).Item("empfield3unkid")
                                            'S.SANDEEP [29 JAN 2015] -- END
                                        End If

                                    Case enWeight_Types.WEIGHT_FIELD4

                                        'S.SANDEEP [29 JAN 2015] -- START
                                        If iWeightCaption <> dTemp(iCnt).Item("empfield4unkid") Then

                                        'S.SANDEEP |09-JUL-2019| -- START
                                        'ISSUE/ENHANCEMENT : PA CHANGES
                                        iCRow.Item("oWeight") = dTemp(iCnt).Item("w4")
                                        'S.SANDEEP |09-JUL-2019| -- END

                                            'If iWeightCaption <> dTemp(iCnt).Item("Field4") Then
                                            'S.SANDEEP [29 JAN 2015] -- END
                                            iCRow.Item("Weight") = dTemp(iCnt).Item("w4")
                                            If dsResult.Tables(0).Rows.Count > 0 Then
                                                Dim dtmp() As DataRow = Nothing
                                                '********** SELF ASSESSMENT **********' -- START
                                                dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND empfield4unkid = '" & dTemp(iCnt).Item("empfield4unkid") & "'")
                                                If dtmp.Length > 0 Then
                                                    iCRow.Item("eself") = dtmp(0).Item("result")
                                                    iCRow.Item("eremark") = dtmp(0).Item("remark")
                                                    iCRow.Item("escore") = dtmp(0).Item("result")
                                                    'S.SANDEEP [21 JAN 2015] -- START
                                                    iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield4unkid")
                                                    iCRow.Item("iScore") = dtmp(0).Item("result")

                                                    iCRow.Item("eitem_weight") = dtmp(0).Item("item_weight")
                                                    iCRow.Item("emax_scale") = dtmp(0).Item("max_scale")
                                                    'S.SANDEEP [21 JAN 2015] -- END
                                                    iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                End If
                                                '********** SELF ASSESSMENT **********' -- END

                                                '********** ASSESSOR ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield4unkid = '" & dTemp(iCnt).Item("empfield4unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield4unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        'Shani (23-Nov-2016) -- Start
                                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'Shani (23-Nov-2016) -- End
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** ASSESSOR ASSESSMENT **********' -- END

                                                '********** REVIEWER ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield4unkid = '" & dTemp(iCnt).Item("empfield4unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield4unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END

                                                        'S.SANDEEP [20-SEP-2017] -- START
                                                        'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'S.SANDEEP [20-SEP-2017] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND empfield4unkid = '" & dTemp(iCnt).Item("empfield4unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("rself") = dtmp(0).Item("result")
                                                        iCRow.Item("rremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("rscore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield4unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("ritem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("rmax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** REVIEWER ASSESSMENT **********' -- END
                                                dtmp = Nothing
                                            End If
                                            'S.SANDEEP [29 JAN 2015] -- START
                                            'iWeightCaption = dTemp(iCnt).Item("Field4")
                                            iWeightCaption = dTemp(iCnt).Item("empfield4unkid")
                                            'S.SANDEEP [29 JAN 2015] -- END
                                        End If

                                    Case enWeight_Types.WEIGHT_FIELD5

                                        'S.SANDEEP [29 JAN 2015] -- START
                                        If iWeightCaption <> dTemp(iCnt).Item("empfield5unkid") Then

                                        'S.SANDEEP |09-JUL-2019| -- START
                                        'ISSUE/ENHANCEMENT : PA CHANGES
                                        iCRow.Item("oWeight") = dTemp(iCnt).Item("w5")
                                        'S.SANDEEP |09-JUL-2019| -- END

                                            'If iWeightCaption <> dTemp(iCnt).Item("Field5") Then
                                            'S.SANDEEP [29 JAN 2015] -- END
                                            iCRow.Item("Weight") = dTemp(iCnt).Item("w5")
                                            If dsResult.Tables(0).Rows.Count > 0 Then
                                                Dim dtmp() As DataRow = Nothing
                                                '********** SELF ASSESSMENT **********' -- START
                                                dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND empfield5unkid = '" & dTemp(iCnt).Item("empfield5unkid") & "'")
                                                If dtmp.Length > 0 Then
                                                    iCRow.Item("eself") = dtmp(0).Item("result")
                                                    iCRow.Item("eremark") = dtmp(0).Item("remark")
                                                    iCRow.Item("escore") = dtmp(0).Item("result")
                                                    'S.SANDEEP [21 JAN 2015] -- START
                                                    iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield5unkid")
                                                    iCRow.Item("iScore") = dtmp(0).Item("result")

                                                    iCRow.Item("eitem_weight") = dtmp(0).Item("item_weight")
                                                    iCRow.Item("emax_scale") = dtmp(0).Item("max_scale")
                                                    'S.SANDEEP [21 JAN 2015] -- END
                                                    iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                End If
                                                '********** SELF ASSESSMENT **********' -- END

                                                '********** ASSESSOR ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield5unkid = '" & dTemp(iCnt).Item("empfield5unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield5unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        'Shani (23-Nov-2016) -- Start
                                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'Shani (23-Nov-2016) -- End
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** ASSESSOR ASSESSMENT **********' -- END

                                                '********** REVIEWER ASSESSMENT **********' -- START
                                                If iMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND empfield5unkid = '" & dTemp(iCnt).Item("empfield5unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("aself") = dtmp(0).Item("result")
                                                        iCRow.Item("aremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("ascore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield5unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END

                                                        'S.SANDEEP [20-SEP-2017] -- START
                                                        'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                                                        iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                                        iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                                        'S.SANDEEP [20-SEP-2017] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                    dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND empfield5unkid = '" & dTemp(iCnt).Item("empfield5unkid") & "'")
                                                    If dtmp.Length > 0 Then
                                                        iCRow.Item("rself") = dtmp(0).Item("result")
                                                        iCRow.Item("rremark") = dtmp(0).Item("remark")
                                                        iCRow.Item("rscore") = dtmp(0).Item("result")
                                                        'S.SANDEEP [21 JAN 2015] -- START
                                                        iCRow.Item("iItemUnkid") = dtmp(0).Item("empfield5unkid")
                                                        iCRow.Item("iScore") = dtmp(0).Item("result")

                                                        iCRow.Item("ritem_weight") = dtmp(0).Item("item_weight")
                                                        iCRow.Item("rmax_scale") = dtmp(0).Item("max_scale")
                                                        'S.SANDEEP [21 JAN 2015] -- END
                                                        iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                                    End If
                                                End If
                                                '********** REVIEWER ASSESSMENT **********' -- END
                                                dtmp = Nothing
                                            End If
                                            'S.SANDEEP [29 JAN 2015] -- START
                                            'iWeightCaption = dTemp(iCnt).Item("Field5")
                                            iWeightCaption = dTemp(iCnt).Item("empfield5unkid")
                                            'S.SANDEEP [29 JAN 2015] -- END
                                        End If
                                End Select

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                If iIsEnableAutoRating Then

                                    Dim iEmpResult, iComPer, iItemWeight, iMaxscale As Decimal
                                    iEmpResult = 0 : iComPer = 0 : iItemWeight = 0 : iMaxscale = 0
                                    Decimal.TryParse(iCRow.Item("pct_complete"), iComPer)
                                    Decimal.TryParse(iCRow.Item("Weight"), iItemWeight)
                                    If IsDBNull(dTemp(iCnt).Item("max_scale")) = False Then
                                        Decimal.TryParse(dTemp(iCnt).Item("max_scale"), iMaxscale)
                                    End If

                                    If iScoringOptionid = enScoringOption.SC_WEIGHTED_BASED Then
                                        'S.SANDEEP |17-MAY-2021| -- START
                                        'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
                                        If iDontAllowExceed100 Then
                                            If iComPer > 100 Then iComPer = 100
                                        End If
                                        'S.SANDEEP |17-MAY-2021| -- END
                                        iEmpResult = (iComPer * iItemWeight) / 100
                                    Else
                                        'S.SANDEEP [11-OCT-2018] -- START
                                        'iEmpResult = (iComPer * iMaxscale) / 100
                                        If iDict IsNot Nothing Then 'S.SANDEEP [29-OCT-2018] -- START
                                        If iDict.IndexOf(CInt(iCRow.Item("scalemasterunkid"))) <> -1 Then
                                            Dim iRow() As DataRow = dsScale.Tables(0).Select("pct_from <= " & iComPer & " AND pct_to >= " & iComPer)
                                            If iRow.Length > 0 Then
                                                iEmpResult = CInt(iRow(0)("scale"))
                                            Else
                                                iEmpResult = 0
                                            End If
                                        Else
                                                'S.SANDEEP |17-MAY-2021| -- START
                                                'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
                                                If iDontAllowExceed100 Then
                                                    If iComPer > 100 Then iComPer = 100
                                                End If
                                                'S.SANDEEP |17-MAY-2021| -- END
                                                iEmpResult = (iComPer * iMaxscale) / 100
                                    End If
                                        Else
                                            'S.SANDEEP |17-MAY-2021| -- START
                                            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
                                            If iDontAllowExceed100 Then
                                                If iComPer > 100 Then iComPer = 100
                                            End If
                                            'S.SANDEEP |17-MAY-2021| -- END
                                            iEmpResult = (iComPer * iMaxscale) / 100
                                        End If 'S.SANDEEP [29-OCT-2018] -- END
                                        'S.SANDEEP [11-OCT-2018] -- END
                                    End If

                                    'S.SANDEEP |21-AUG-2019| -- START
                                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                    iEmpResult = Format(CDec(iEmpResult), "#######################0.#0")
                                    'S.SANDEEP |21-AUG-2019| -- END

                                    '********** SELF ASSESSMENT **********' --
                                    If dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'").Length <= 0 Then
                                        iCRow.Item("eself") = iEmpResult 'Employee assess score
                                        iCRow.Item("escore") = iEmpResult 'Employee assess score
                                        'iCRow.Item("iItemUnkid") = iCRow.Item("empfield1unkid")
                                        iCRow.Item("iScore") = iEmpResult 'Employee assess score
                                        iCRow.Item("eitem_weight") = iItemWeight 'Item Weight
                                        iCRow.Item("emax_scale") = iMaxscale 'is setting scale thrn put on max scale
                                    End If

                                    Select Case iMode
                                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                                            '********** Assessor ASSESSMENT **********' --
                                            If dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'").Length <= 0 Then
                                                iCRow.Item("aself") = iEmpResult
                                                iCRow.Item("ascore") = iEmpResult
                                                'iCRow.Item("iItemUnkid") = iCRow.Item("empfield1unkid")
                                                iCRow.Item("iScore") = iEmpResult
                                                iCRow.Item("aitem_weight") = iItemWeight
                                                iCRow.Item("amax_scale") = iMaxscale

                                            End If
                                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                                            '********** Assessor ASSESSMENT **********' --
                                            If dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'").Length <= 0 Then
                                                iCRow.Item("aself") = iEmpResult
                                                iCRow.Item("ascore") = iEmpResult
                                                'iCRow.Item("iItemUnkid") = iCRow.Item("empfield1unkid")
                                                iCRow.Item("iScore") = iEmpResult
                                                iCRow.Item("aitem_weight") = iItemWeight
                                                iCRow.Item("amax_scale") = iMaxscale

                                            End If
                                            '********** Reviewer ASSESSMENT **********' --
                                            'S.SANDEEP [20-SEP-2017] -- START
                                            'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                                            'If dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'").Length <= 0 Then
                                            '    iCRow.Item("rself") = iEmpResult
                                            '    iCRow.Item("rscore") = iEmpResult
                                            '    iCRow.Item("iItemUnkid") = iCRow.Item("empfield1unkid")
                                            '    iCRow.Item("iScore") = iEmpResult
                                            '    iCRow.Item("ritem_weight") = iItemWeight
                                            '    iCRow.Item("rmax_scale") = iMaxscale
                                            'End If
                                            If iEReviewerId > 0 Then
                                            If dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'").Length <= 0 Then
                                                iCRow.Item("rself") = iEmpResult
                                                iCRow.Item("rscore") = iEmpResult
                                                    'iCRow.Item("iItemUnkid") = iCRow.Item("empfield1unkid")
                                                iCRow.Item("iScore") = iEmpResult
                                                iCRow.Item("ritem_weight") = iItemWeight
                                                iCRow.Item("rmax_scale") = iMaxscale
                                            End If
                                            End If
                                            'S.SANDEEP [20-SEP-2017] -- END
                                    End Select



                                End If
                                'Shani (26-Sep-2016) -- End
                                mdtFinal.Rows.Add(iCRow)
                            Next
                        End If
                    End If
                Next
            End If

            Dim dView As DataView = mdtFinal.DefaultView
            dView.Sort = "perspectiveunkid,IsGrp DESC"
            mdtFinal = dView.ToTable
            Dim xGrp() As DataRow = mdtFinal.Select("IsGrp=True")
            If xGrp.Length > 0 Then
                For index As Integer = 0 To xGrp.Length - 1
                    If CInt(mdtFinal.Compute("COUNT(GrpId)", "GrpId = '" & xGrp(index)("GrpId") & "'")) <= 1 Then
                        mdtFinal.Rows.Remove(xGrp(index))
                    End If
                Next
                mdtFinal.AcceptChanges()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Evaluation_Data; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    Public Function Get_GE_Evaluation_Data(ByVal iMode As enAssessmentMode, _
                                           ByVal iEmployeeId As Integer, _
                                           ByVal iPeriodId As Integer, _
                                           ByVal iAssessorId As Integer, _
                                           ByVal iReviewerId As Integer, _
                                           ByVal iWeightAsNumber As Boolean, _
                                           ByVal iAnalysisId As Integer, _
                                           ByVal iSelfAssign As Boolean, _
                                           ByVal iEmployeeAsOnDate As DateTime, _
                                           ByVal iDataBaseName As String, _
                                           Optional ByVal blnOnlyCommittedScore As Boolean = False, _
                                           Optional ByVal blnAddAvgCategoryScore As Boolean = False) As DataTable 'S.SANDEEP [29 JAN 2015] -- START {iSelfAssign} -- END
        'S.SANDEEP [04 JUN 2015] -- START {blnOnlyCommittedScore} -- END
        'S.SANDEEP [04-AUG-2017] -- START {iDataBaseName} -- END


        Dim StrQ As String = String.Empty
        Dim mdtFinal As DataTable = Nothing
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dCol As DataColumn
        Try
            mdtFinal = New DataTable("Evaluation")

            Dim dsResult As New DataSet
            Dim iEAssessorId, iEReviewerId As Integer
            iEAssessorId = 0 : iEReviewerId = 0

            'S.SANDEEP [22 Jan 2016] -- START
            'MULTIPULE ASSESSOR/REVIEWER CASE
            'StrQ = "SELECT " & _
            '       "     hrassessor_master.assessormasterunkid " & _
            '       "    ,hrassessor_master.employeeunkid " & _
            '       "    ,hrassessor_master.isreviewer " & _
            '       "FROM hrassessor_tran " & _
            '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
            '       "WHERE hrassessor_tran.employeeunkid = " & iEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 "


            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'StrQ = "SELECT " & _
            '       "     CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN ASR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
            '       "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN RVR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
            '       "     END AS assessormasterunkid " & _
            '       "    ,CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
            '       "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
            '       "     END AS isfound " & _
            '       "    ,hrassessor_master.employeeunkid " & _
            '       "    ,hrassessor_master.isreviewer " & _
            '       "    ,hrassessor_tran.visibletypeid " & _
            '       "FROM hrassessor_tran " & _
            '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
            '       "    LEFT JOIN hrevaluation_analysis_master ASR ON ASR.assessormasterunkid = hrassessor_master.assessormasterunkid AND ASR.periodunkid = '" & iPeriodId & "' AND ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
            '       "    LEFT JOIN hrevaluation_analysis_master RVR ON RVR.assessormasterunkid = hrassessor_master.assessormasterunkid AND RVR.periodunkid = '" & iPeriodId & "' AND RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
            '       "WHERE hrassessor_tran.employeeunkid = " & iEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 "
            ''S.SANDEEP [27 DEC 2016] -- START {visibletypeid,ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid,RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid}-- END
            ''S.SANDEEP [22 Jan 2016] -- END
            'dsResult = objDataOpr.ExecQuery(StrQ, "List")

            'If objDataOpr.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            '    Throw exForce
            'End If
            dsResult = GetEmployee_AssessorReviewerDetails(iPeriodId, iDataBaseName, Nothing, iEmployeeId, objDataOpr)
            'S.SANDEEP [04-AUG-2017] -- END

            If dsResult.Tables(0).Rows.Count > 0 Then
                If iAssessorId > 0 Then iEAssessorId = iAssessorId
                If iReviewerId > 0 Then iEReviewerId = iReviewerId
                Dim dtmp() As DataRow = Nothing
                If iEAssessorId <= 0 Then
                    'S.SANDEEP [22 Jan 2016] -- START
                    'MULTIPULE ASSESSOR/REVIEWER CASE
                    'dtmp = dsResult.Tables(0).Select("isreviewer = 0")
                    'If dtmp.Length > 0 Then
                    '    iEAssessorId = dtmp(0).Item("assessormasterunkid")
                    'End If

                    dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEAssessorId = dtmp(0).Item("assessormasterunkid")
                    Else
                        dtmp = dsResult.Tables(0).Select("isreviewer = 0 AND isfound = 0")
                        If dtmp.Length > 0 Then
                            iEAssessorId = dtmp(0).Item("assessormasterunkid")
                        End If
                    End If
                    'S.SANDEEP [22 Jan 2016] -- END
                End If

                If iEReviewerId <= 0 Then
                    'S.SANDEEP [22 Jan 2016] -- START
                    'MULTIPULE ASSESSOR/REVIEWER CASE
                    'dtmp = dsResult.Tables(0).Select("isreviewer = 1")
                    'If dtmp.Length > 0 Then
                    '    iEReviewerId = dtmp(0).Item("assessormasterunkid")
                    'End If

                    dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEReviewerId = dtmp(0).Item("assessormasterunkid")
                    Else
                        dtmp = dsResult.Tables(0).Select("isreviewer = 1 AND isfound = 0")
                        If dtmp.Length > 0 Then
                            iEReviewerId = dtmp(0).Item("assessormasterunkid")
                        End If
                    End If
                    'S.SANDEEP [22 Jan 2016] -- END
                End If
            End If

            dCol = New DataColumn
            dCol.ColumnName = "eval_item"
            dCol.Caption = Language.getMessage(mstrModuleName, 114, "Competencies")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Score"
            dCol.Caption = Language.getMessage(mstrModuleName, 112, "Score Guide")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = Language.getMessage(mstrModuleName, 112, "Score Guide")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Weight"
            'dCol.Caption = Language.getMessage(mstrModuleName, 104, "Weight")
            dCol.Caption = Language.getMessage(mstrModuleName, 104, "Wgt.")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim objEmp As New clsEmployee_Master
            'objEmp._Employeeunkid = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END


            dCol = New DataColumn
            dCol.ColumnName = "eself"
            'dCol.Caption = objEmp._Firstname & " " & objEmp._Surname & " - " & Language.getMessage(mstrModuleName, 105, "Self Assessment")
            dCol.Caption = Language.getMessage(mstrModuleName, 105, "Emp. Score")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            dCol = New DataColumn
            dCol.ColumnName = "analysistranunkid"
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP |18-JAN-2020| -- END

            dCol = New DataColumn
            dCol.ColumnName = "eremark"
            'dCol.Caption = objEmp._Firstname & " " & objEmp._Surname & " - " & Language.getMessage(mstrModuleName, 107, "Self Remark")
            dCol.Caption = Language.getMessage(mstrModuleName, 107, "Emp. Remark")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtFinal.Columns.Add(dCol)

            Dim objAssessor As New clsAssessor
            Select Case iMode
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "aself"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 108, "Assessor Self Assessment")
                    dCol.Caption = Language.getMessage(mstrModuleName, 108, "Sup. Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "aremark"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 109, "Assessor Self Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 109, "Sup. Remark")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)


                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreedscore"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 109, "Assessor Self Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 126, "Agreed Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End


                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "aself"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 108, "Assessor Self Assessment")
                    dCol.Caption = Language.getMessage(mstrModuleName, 108, "Sup. Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "aremark"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 109, "Assessor Self Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 109, "Sup. Remark")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreedscore"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 109, "Assessor Self Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 126, "Agreed Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End


                    objAssessor._Assessormasterunkid = iReviewerId
                    dCol = New DataColumn
                    dCol.ColumnName = "rself"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 110, "Reviewer Assessment")
                    dCol.Caption = Language.getMessage(mstrModuleName, 110, "Rev. Score")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "rremark"
                    'dCol.Caption = objAssessor._EmployeeName & " " & Language.getMessage(mstrModuleName, 111, "Reviewer Remark")
                    dCol.Caption = Language.getMessage(mstrModuleName, 111, "Rev. Remark")
                    dCol.DefaultValue = ""
                    dCol.DataType = System.Type.GetType("System.String")
                    mdtFinal.Columns.Add(dCol)
            End Select

            dCol = New DataColumn
            dCol.ColumnName = "escore"
            dCol.Caption = ""
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtFinal.Columns.Add(dCol)

            Select Case iMode
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "ascore"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreed_score"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)
                    'Shani (23-Nov-2016) -- End

                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objAssessor._Assessormasterunkid = iEAssessorId
                    dCol = New DataColumn
                    dCol.ColumnName = "ascore"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    dCol = New DataColumn
                    dCol.ColumnName = "agreed_score"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)

                    'Shani (23-Nov-2016) -- End


                    objAssessor._Assessormasterunkid = iReviewerId
                    dCol = New DataColumn
                    dCol.ColumnName = "rscore"
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    mdtFinal.Columns.Add(dCol)
            End Select
            objAssessor = Nothing ': objEmp = Nothing

            dCol = New DataColumn
            dCol.ColumnName = "periodunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "scalemasterunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "competenciesunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "assessgroupunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "IsGrp"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "IsPGrp"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GrpId"
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [21 JAN 2015] -- START
            '****************************************** COMPUTATION PURPOSE
            mdtFinal.Columns.Add("iPeriodId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFinal.Columns.Add("iItemUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFinal.Columns.Add("iEmployeeId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFinal.Columns.Add("iScore", System.Type.GetType("System.Decimal")).DefaultValue = 0

            mdtFinal.Columns.Add("eitem_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("emax_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0

            mdtFinal.Columns.Add("aitem_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("amax_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0

            mdtFinal.Columns.Add("ritem_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("rmax_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            '****************************************** COMPUTATION PURPOSE
            'S.SANDEEP [21 JAN 2015] -- END

            Dim iStrGroupIds As String = String.Empty
            Dim objAssessGrp As New clsassess_group_master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(iEmployeeId)

            'S.SANDEEP [19 DEC 2016] -- START
            'iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(iEmployeeId, iEmployeeAsOnDate)
            iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(iEmployeeId, iEmployeeAsOnDate, iPeriodId)
            'S.SANDEEP [19 DEC 2016] -- END

            'S.SANDEEP [04 JUN 2015] -- END
            objAssessGrp = Nothing

            If iStrGroupIds.Trim.Length > 0 Then
                StrQ = "SELECT " & _
                       "     hrassess_group_master.assessgroup_name + ' - ' + @Caption + CAST(hrassess_group_master.weight AS NVARCHAR(MAX)) AS eval_item " & _
                       "    ,CAST(0 AS BIT) AS IsGrp " & _
                       "    ,0 AS GrpId " & _
                       "    ,'' AS Weight " & _
                       "    ,hrassess_group_master.assessgroupunkid AS assessgroupunkid " & _
                       "    ,1 AS IsPGrp " & _
                       "    ,0 AS competenciesunkid " & _
                       "    ,0 AS scalemasterunkid " & _
                       "    ,SUM(999) OVER(partition by hrassess_group_master.assessgroupunkid) AS Cnt " & _
                       "FROM hrassess_group_master WITH (NOLOCK) " & _
                       "    JOIN hrassess_competence_assign_master WITH (NOLOCK) ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                       "WHERE isactive = 1 AND hrassess_group_master.assessgroupunkid IN(" & iStrGroupIds & ") AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "

                'S.SANDEEP [16 MAR 2015] -- START
                StrQ &= " AND hrassess_competence_assign_master.isvoid = 0 "
                'S.SANDEEP [16 MAR 2015] -- END

                'S.SANDEEP [09 FEB 2015] -- START
                If iSelfAssign = True Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' " & _
                            " AND hrassess_competence_assign_master.isfinal = 1 "
                    'S.SANDEEP [12 FEB 2015] -- START
                Else
                    StrQ &= " AND ISNULL(hrassess_competence_assign_master.employeeunkid,0) <= 0 "
                    'S.SANDEEP [12 FEB 2015] -- END
                End If
                'S.SANDEEP [09 FEB 2015] -- END
                StrQ &= "UNION ALL " & _
                       "SELECT " & _
                       "     eval_item " & _
                       "    ,IsGrp " & _
                       "    ,GrpId " & _
                       "    ,weight " & _
                       "    ,assessgroupunkid " & _
                       "    ,CAST(0 AS BIT) AS iGrp " & _
                       "    ,competenciesunkid " & _
                       "    ,scalemasterunkid " & _
                       "    ,SUM(1) OVER(partition by GrpId) Cnt " & _
                       "FROM " & _
                       "( " & _
                       "        SELECT DISTINCT " & _
                       "             cfcommon_master.name AS Eval_Item " & _
                       "            ,CAST(1 AS BIT) AS IsGrp " & _
                       "            ,masterunkid AS GrpId " & _
                       "            ,'' AS weight " & _
                       "            ,assessgroupunkid AS assessgroupunkid " & _
                       "            ,0 AS competenciesunkid " & _
                       "            ,0 AS scalemasterunkid " & _
                       "        FROM hrassess_competence_assign_tran WITH (NOLOCK) " & _
                       "            JOIN hrassess_competence_assign_master WITH (NOLOCK) ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                       "            JOIN hrassess_competencies_master WITH (NOLOCK) ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                       "            JOIN cfcommon_master WITH (NOLOCK) ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid " & _
                       "        WHERE hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid IN(" & iStrGroupIds & ") " & _
                       "            AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
                'S.SANDEEP [09 FEB 2015] -- START
                If iSelfAssign = True Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' " & _
                            " AND hrassess_competence_assign_master.isfinal = 1 "
                    'S.SANDEEP [12 FEB 2015] -- START
                Else
                    StrQ &= " AND ISNULL(hrassess_competence_assign_master.employeeunkid,0) <= 0 "
                    'S.SANDEEP [12 FEB 2015] -- END
                End If
                'S.SANDEEP [09 FEB 2015] -- END
                StrQ &= "    UNION ALL " & _
                       "        SELECT " & _
                       "             SPACE(5) + ' ' + hrassess_competencies_master.name AS Eval_Item " & _
                       "            ,CAST(0 AS BIT) AS IsGrp " & _
                       "            ,masterunkid AS GrpId " & _
                       "            ,CAST(hrassess_competence_assign_tran.weight AS NVARCHAR(MAX)) AS weight " & _
                       "            ,assessgroupunkid AS AGrpId " & _
                       "            ,hrassess_competencies_master.competenciesunkid AS competenciesunkid " & _
                       "            ,hrassess_competencies_master.scalemasterunkid AS scalemasterunkid " & _
                       "        FROM hrassess_competence_assign_tran WITH (NOLOCK) " & _
                       "            JOIN hrassess_competence_assign_master WITH (NOLOCK) ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                       "            JOIN hrassess_competencies_master WITH (NOLOCK) ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                       "            JOIN cfcommon_master WITH (NOLOCK) ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid " & _
                       "        WHERE hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid IN(" & iStrGroupIds & ") " & _
                       "            AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
                If iSelfAssign = True Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' " & _
                            " AND hrassess_competence_assign_master.isfinal = 1 "
                    'S.SANDEEP [12 FEB 2015] -- START
                Else
                    StrQ &= " AND ISNULL(hrassess_competence_assign_master.employeeunkid,0) <= 0 "
                    'S.SANDEEP [12 FEB 2015] -- END
                End If
                StrQ &= ") AS A WHERE 1 = 1 " & _
                       "ORDER BY assessgroupunkid, GrpId, IsGrp DESC "

                objDataOpr.AddParameter("@Caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 115, "Total Weight Assigned :"))

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [21 JAN 2015] -- START
                'StrQ = "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '       " ,hrcompetency_analysis_tran.result " & _
                '       " ,hrcompetency_analysis_tran.computed_value " & _
                '       " ,hrcompetency_analysis_tran.remark " & _
                '       " ,hrcompetency_analysis_tran.competenciesunkid " & _
                '       "FROM hrcompetency_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                '       "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.selfemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "

                StrQ = "SELECT " & _
                       "  hrevaluation_analysis_master.assessmodeid " & _
                       " ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS result " & _
                       " ,hrcompetency_analysis_tran.remark " & _
                       " ,hrcompetency_analysis_tran.competenciesunkid " & _
                       " ,hrcompetency_analysis_tran.item_weight " & _
                       " ,hrcompetency_analysis_tran.max_scale " & _
                       " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                       " ,ISNULL(CAST(hrcompetency_analysis_tran.agreedscore AS DECIMAL(36,2)),0) AS agreedscore " & _
                       " ,hrcompetency_analysis_tran.analysistranunkid " & _
                       "FROM hrcompetency_analysis_tran WITH (NOLOCK) " & _
                       "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                       "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.selfemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED --------- ,hrcompetency_analysis_tran.result AS result
                'REMOVED --------- ,ISNULL(hrcompetency_analysis_tran.agreedscore,0) AS agreedscore
                'ADDED   --------- ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS result
                'ADDED   --------- ,ISNULL(CAST(hrcompetency_analysis_tran.agreedscore AS DECIMAL(36,2)),0) AS agreedscore
                'S.SANDEEP |21-AUG-2019| -- END

                'Shani (23-Nov-2016) -- [agreedscore]
                'S.SANDEEP [21 JAN 2015] -- END

                If iAnalysisId > 0 Then
                    StrQ &= " AND hrevaluation_analysis_master.analysisunkid = '" & iAnalysisId & "' "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                If blnOnlyCommittedScore = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'S.SANDEEP [21 JAN 2015] -- START
                'StrQ &= "UNION ALL " & _
                '       "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '       " ,hrcompetency_analysis_tran.result " & _
                '        " ,hrcompetency_analysis_tran.computed_value " & _
                '       " ,hrcompetency_analysis_tran.remark " & _
                '       " ,hrcompetency_analysis_tran.competenciesunkid " & _
                '       "FROM hrcompetency_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                '       "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & iEAssessorId & "' " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "

                StrQ &= "UNION ALL " & _
                        "SELECT " & _
                        "  hrevaluation_analysis_master.assessmodeid " & _
                        " ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS result " & _
                        " ,hrcompetency_analysis_tran.remark " & _
                        " ,hrcompetency_analysis_tran.competenciesunkid " & _
                        " ,hrcompetency_analysis_tran.item_weight " & _
                        " ,hrcompetency_analysis_tran.max_scale " & _
                        " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                        " ,CAST(CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END AS DECIMAL(36,2)) AS agreedscore " & _
                        " ,hrcompetency_analysis_tran.analysistranunkid " & _
                        "FROM hrcompetency_analysis_tran WITH (NOLOCK) " & _
                        "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                        "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & iEAssessorId & "' " & _
                        "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED --------- ,hrcompetency_analysis_tran.result AS result
                'REMOVED --------- ,CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END AS agreedscore
                'ADDED   --------- ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS result
                'ADDED   --------- ,CAST(CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END AS DECIMAL(36,2)) AS agreedscore
                'S.SANDEEP |21-AUG-2019| -- END

                'Shani (23-Nov-2016) -- [agreedscore]
                'S.SANDEEP [21 JAN 2015] -- END

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                '--- REMOVED (ISNULL(hrcompetency_analysis_tran.agreedscore,0))
                '--- ADDED (CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END)
                'S.SANDEEP [20-SEP-2017] -- END

                If iAnalysisId > 0 Then
                    StrQ &= " AND hrevaluation_analysis_master.analysisunkid = '" & iAnalysisId & "' "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                If blnOnlyCommittedScore = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'S.SANDEEP [21 JAN 2015] -- START
                'StrQ &= "UNION ALL " & _
                '       "SELECT " & _
                '       "  hrevaluation_analysis_master.assessmodeid " & _
                '       " ,hrcompetency_analysis_tran.result " & _
                '        " ,hrcompetency_analysis_tran.computed_value " & _
                '       " ,hrcompetency_analysis_tran.remark " & _
                '       " ,hrcompetency_analysis_tran.competenciesunkid " & _
                '       "FROM hrcompetency_analysis_tran " & _
                '       "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                '       "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & iEReviewerId & " " & _
                '       "  AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                StrQ &= "UNION ALL " & _
                        "SELECT " & _
                        "  hrevaluation_analysis_master.assessmodeid " & _
                        " ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS result " & _
                        " ,hrcompetency_analysis_tran.remark " & _
                        " ,hrcompetency_analysis_tran.competenciesunkid " & _
                        " ,hrcompetency_analysis_tran.item_weight " & _
                        " ,hrcompetency_analysis_tran.max_scale " & _
                        " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                        " ,CAST(CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END AS DECIMAL(36,2)) AS agreedscore " & _
                        " ,hrcompetency_analysis_tran.analysistranunkid " & _
                        "FROM hrcompetency_analysis_tran WITH (NOLOCK) " & _
                        "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                        "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & iEReviewerId & " " & _
                        " AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED --------- ,hrcompetency_analysis_tran.result AS result
                'REMOVED --------- ,CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END AS agreedscore
                'ADDED   --------- ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS result
                'ADDED   --------- ,CAST(CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END AS DECIMAL(36,2)) AS agreedscore
                'S.SANDEEP |21-AUG-2019| -- END

                'Shani (23-Nov-2016) -- [agreedscore]
                'S.SANDEEP [21 JAN 2015] -- END

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                '--- REMOVED (ISNULL(hrcompetency_analysis_tran.agreedscore,0))
                '--- ADDED (CASE WHEN hrcompetency_analysis_tran.agreedscore <=0 THEN hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.agreedscore END)
                'S.SANDEEP [20-SEP-2017] -- END

                If iAnalysisId > 0 Then
                    StrQ &= " AND hrevaluation_analysis_master.analysisunkid = '" & iAnalysisId & "' "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                If blnOnlyCommittedScore = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                dsResult = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                For Each drRow As DataRow In dsList.Tables(0).Rows

                    'S.SANDEEP [09 FEB 2015] -- START
                    If drRow.Item("Cnt") = 1 Then Continue For
                    'S.SANDEEP [09 FEB 2015] -- END

                    Dim iCRow As DataRow = mdtFinal.NewRow

                    iCRow.Item("eval_item") = drRow.Item("eval_item")
                    iCRow.Item("Weight") = drRow.Item("Weight")
                    iCRow.Item("assessgroupunkid") = drRow.Item("assessgroupunkid")
                    iCRow.Item("IsPGrp") = drRow.Item("IsPGrp")
                    iCRow.Item("competenciesunkid") = drRow.Item("competenciesunkid")
                    iCRow.Item("scalemasterunkid") = drRow.Item("scalemasterunkid")
                    iCRow.Item("IsGrp") = drRow.Item("IsGrp")
                    iCRow.Item("GrpId") = drRow.Item("GrpId")
                    iCRow.Item("periodunkid") = iPeriodId
                    'S.SANDEEP [21 JAN 2015] -- START
                    iCRow.Item("iPeriodId") = iPeriodId
                    iCRow.Item("iEmployeeId") = iEmployeeId
                    iCRow.Item("iItemUnkid") = drRow.Item("competenciesunkid")
                    'S.SANDEEP [21 JAN 2015] -- END


                    If iCRow.Item("competenciesunkid") > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        '********** SELF ASSESSMENT **********' -- START
                        'Shani(30-MAR-2016) -- Start
                        'dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "'")
                        dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "' AND assessgroupunkid = '" & drRow.Item("assessgroupunkid") & "'")
                        'Shani(30-MAR-2016) -- End 
                        If dtmp.Length > 0 Then
                            iCRow.Item("eself") = dtmp(0).Item("result")
                            iCRow.Item("eremark") = dtmp(0).Item("remark")
                            If iWeightAsNumber = True Then
                                iCRow.Item("escore") = dtmp(0).Item("result") * IIf(drRow.Item("Weight").ToString.Trim.Length <= 0, 1, drRow.Item("Weight"))
                            Else
                                iCRow.Item("escore") = dtmp(0).Item("result")
                            End If
                            'S.SANDEEP [21 JAN 2015] -- START
                            iCRow.Item("iScore") = dtmp(0).Item("result")

                            iCRow.Item("eitem_weight") = dtmp(0).Item("item_weight")
                            iCRow.Item("emax_scale") = dtmp(0).Item("max_scale")
                            iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                            'S.SANDEEP [21 JAN 2015] -- END
                        End If
                        '********** SELF ASSESSMENT **********' -- END

                        '********** ASSESSOR ASSESSMENT **********' -- START
                        If iMode = enAssessmentMode.APPRAISER_ASSESSMENT Then

                            'Shani(30-MAR-2016) -- Start
                            'dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "'")
                            dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "' AND assessgroupunkid = '" & drRow.Item("assessgroupunkid") & "'")
                            'Shani(30-MAR-2016) -- End 


                            If dtmp.Length > 0 Then
                                iCRow.Item("aself") = dtmp(0).Item("result")
                                iCRow.Item("aremark") = dtmp(0).Item("remark")
                                If iWeightAsNumber = True Then
                                    iCRow.Item("ascore") = dtmp(0).Item("result") * IIf(drRow.Item("Weight").ToString.Trim.Length <= 0, 1, drRow.Item("Weight"))
                                Else
                                    iCRow.Item("ascore") = dtmp(0).Item("result")
                                End If
                                'S.SANDEEP [21 JAN 2015] -- START
                                iCRow.Item("iScore") = dtmp(0).Item("result")

                                iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                'S.SANDEEP [21 JAN 2015] -- END

                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                'Shani (23-Nov-2016) -- End


                            End If
                        End If
                        '********** ASSESSOR ASSESSMENT **********' -- END

                        '********** REVIEWER ASSESSMENT **********' -- START
                        If iMode = enAssessmentMode.REVIEWER_ASSESSMENT Then

                            'Shani(30-MAR-2016) -- Start
                            'dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "'")
                            dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "' AND assessgroupunkid = '" & drRow.Item("assessgroupunkid") & "'")
                            'Shani(30-MAR-2016) -- End 


                            If dtmp.Length > 0 Then
                                iCRow.Item("aself") = dtmp(0).Item("result")
                                iCRow.Item("aremark") = dtmp(0).Item("remark")
                                If iWeightAsNumber = True Then
                                    iCRow.Item("ascore") = dtmp(0).Item("result") * IIf(drRow.Item("Weight").ToString.Trim.Length <= 0, 1, drRow.Item("Weight"))
                                Else
                                    iCRow.Item("ascore") = dtmp(0).Item("result")
                                End If
                                'S.SANDEEP [21 JAN 2015] -- START
                                iCRow.Item("iScore") = dtmp(0).Item("result")

                                iCRow.Item("aitem_weight") = dtmp(0).Item("item_weight")
                                iCRow.Item("amax_scale") = dtmp(0).Item("max_scale")
                                'S.SANDEEP [21 JAN 2015] -- END

                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                iCRow.Item("agreedscore") = dtmp(0).Item("agreedscore")
                                iCRow.Item("agreed_score") = dtmp(0).Item("agreedscore")
                                iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                'Shani (23-Nov-2016) -- End
                            End If

                            'Shani(30-MAR-2016) -- Start
                            'dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "'")
                            dtmp = dsResult.Tables(0).Select("assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND competenciesunkid = '" & drRow.Item("competenciesunkid") & "' AND assessgroupunkid = '" & drRow.Item("assessgroupunkid") & "'")
                            'Shani(30-MAR-2016) -- End 



                            If dtmp.Length > 0 Then
                                iCRow.Item("rself") = dtmp(0).Item("result")
                                iCRow.Item("rremark") = dtmp(0).Item("remark")
                                If iWeightAsNumber = True Then
                                    iCRow.Item("rscore") = dtmp(0).Item("result") * IIf(drRow.Item("Weight").ToString.Trim.Length <= 0, 1, drRow.Item("Weight"))
                                Else
                                    iCRow.Item("rscore") = dtmp(0).Item("result")
                                End If
                                'S.SANDEEP [21 JAN 2015] -- START
                                iCRow.Item("iScore") = dtmp(0).Item("result")

                                iCRow.Item("ritem_weight") = dtmp(0).Item("item_weight")
                                iCRow.Item("rmax_scale") = dtmp(0).Item("max_scale")
                                iCRow.Item("analysistranunkid") = dtmp(0).Item("analysistranunkid")
                                'S.SANDEEP [21 JAN 2015] -- END
                            End If
                        End If
                        '********** REVIEWER ASSESSMENT **********' -- END
                    End If

                    mdtFinal.Rows.Add(iCRow)
                Next

                Dim dView As DataView = mdtFinal.DefaultView
                dView.Sort = "assessgroupunkid, GrpId, IsGrp DESC"
                mdtFinal = dView.ToTable

                'S.SANDEEP |12-APR-2021| -- START
                'ISSUE/ENHANCEMENT : AVG CMP SCORE
                If blnAddAvgCategoryScore Then
                    Dim ilst As IEnumerable(Of Integer) = mdtFinal.AsEnumerable().Select(Function(x) x.Field(Of Integer)("GrpId")).Distinct().ToList()
                    If ilst IsNot Nothing AndAlso ilst.Count > 0 Then
                        For Each item In ilst
                            Dim xRow() As DataRow = mdtFinal.Select("GrpId = '" & item.ToString & "' AND IsGrp = 1")
                            If xRow.Length > 0 Then
                                Dim iRowCount = -1
                                iRowCount = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("GrpId") = item And x.Field(Of Boolean)("IsGrp") = False).Count()

                                Dim Score As Decimal = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("GrpId") = item And x.Field(Of Boolean)("IsGrp") = False).Select(Function(x) x.Field(Of Decimal)("escore")).Sum()

                                ' mdtFinal.Compute("SUM(eself)", "GrpId = '" & item.ToString & "' AND IsGrp = 0")
                                xRow(0)("eself") = Format(CDec(Score / iRowCount), "###################0.#0")

                                If mdtFinal.Columns.Contains("ascore") Then
                                    Score = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("GrpId") = item And x.Field(Of Boolean)("IsGrp") = False).Select(Function(x) x.Field(Of Decimal)("ascore")).Sum()
                                    xRow(0)("aself") = Format(CDec(Score / iRowCount), "###################0.#0")
                                End If
                                
                                If mdtFinal.Columns.Contains("rscore") Then
                                    Score = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("GrpId") = item And x.Field(Of Boolean)("IsGrp") = False).Select(Function(x) x.Field(Of Decimal)("rscore")).Sum()
                                    xRow(0)("rself") = Format(CDec(Score / iRowCount), "###################0.#0")
                                End If
                                
                                mdtFinal.AcceptChanges()
                            End If
                        Next
                    End If
                End If
                'S.SANDEEP |12-APR-2021| -- END

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Evaluation_Data; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    'S.SANDEEP |14-MAR-2019| -- START
    'Public Function Get_Custom_Items_List(ByVal iPeriodId As Integer, _
    '                                      ByVal iHeaderId As Integer, _
    '                                      ByVal iAnalysisUnkid As Integer, _
    '                                      ByVal iMode As enAssessmentMode, _
    '                                      ByVal iEmployeeId As Integer, _
    '                                      ByVal iAction As enAction, _
    '                                      Optional ByVal blnOnlyCommittedValue As Boolean = False) As DataTable


    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING

    
    'Public Function Get_Custom_Items_List(ByVal iPeriodId As Integer, _
    '                                      ByVal iHeaderId As Integer, _
    '                                      ByVal iAnalysisUnkid As Integer, _
    '                                      ByVal iMode As enAssessmentMode, _
    '                                      ByVal iEmployeeId As Integer, _
    '                                      ByVal iAction As enAction, _
    '                                      Optional ByVal blnOnlyCommittedValue As Boolean = False, _
    '                                      Optional ByVal blnAddAssessFilter As Boolean = False) As DataTable
    '    'S.SANDEEP |14-MAR-2019| -- END

    '    'S.SANDEEP [04 JUN 2015] -- START {blnOnlyCommittedValue} -- END

    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim mdtFinal As DataTable = Nothing
    '    Dim dCol As DataColumn
    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        mdtFinal = New DataTable("CTList")

    '        Dim objCHeader As New clsassess_custom_header
    '        objCHeader._Customheaderunkid = iHeaderId
    '        dCol = New DataColumn
    '        With dCol
    '            .ColumnName = "Header_Name"
    '            .Caption = Language.getMessage(mstrModuleName, 116, "Custom Header")
    '            .DataType = System.Type.GetType("System.String")
    '            .DefaultValue = objCHeader._Name
    '        End With
    '        mdtFinal.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        With dCol
    '            .ColumnName = "Header_Id"
    '            .Caption = ""
    '            .DataType = System.Type.GetType("System.Int32")
    '            .DefaultValue = objCHeader._Customheaderunkid
    '        End With
    '        mdtFinal.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        With dCol
    '            .ColumnName = "Is_Allow_Multiple"
    '            .Caption = ""
    '            .DataType = System.Type.GetType("System.Boolean")
    '            .DefaultValue = objCHeader._Is_Allow_Multiple
    '            'Shani (26-Sep-2016) -- Start
    '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '            .ExtendedProperties.Add("IsMatch_With_Competency", objCHeader._IsMatch_With_Competency)
    '            'Shani (26-Sep-2016) -- End
    '        End With
    '        mdtFinal.Columns.Add(dCol)
    '        objCHeader = Nothing

    '        Dim objCustomItems As New clsassess_custom_items
    '        'S.SANDEEP |14-MAR-2019| -- START
    '        'dsList = objCustomItems.getComboList(iPeriodId, iHeaderId, False, "List")
    '        If blnAddAssessFilter Then
    '            dsList = objCustomItems.getComboList(iPeriodId, iHeaderId, False, "List", iMode)
    '        Else
    '            dsList = objCustomItems.getComboList(iPeriodId, iHeaderId, False, "List")
    '        End If
    '        'S.SANDEEP |14-MAR-2019| -- END
    '        objCustomItems = Nothing

    '        'S.SANDEEP |25-MAR-2019| -- START
    '        Dim intViewModeId As Integer = -1
    '        If blnAddAssessFilter = False Then
    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                If IsDBNull(dsList.Tables(0).Compute("MAX(viewmodeid)", "")) = False Then
    '                    intViewModeId = CInt(dsList.Tables(0).Compute("MAX(viewmodeid)", ""))
    '                End If
    '            End If
    '        End If
    '        'S.SANDEEP |25-MAR-2019| -- END

    '        If dsList.Tables(0).Rows.Count > 0 Then

    '            'Shani (26-Sep-2016) -- Start
    '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '            '.Select("","isdefaultentry desc")
    '            'For Each drow As DataRow In dsList.Tables(0).Rows
    '            For Each drow As DataRow In dsList.Tables(0).Select("", "isdefaultentry DESC")
    '                'Shani (26-Sep-2016) -- End
    '                dCol = New DataColumn
    '                With dCol
    '                    .ColumnName = "CItem_" & drow.Item("Id").ToString
    '                    .Caption = drow.Item("Name").ToString
    '                    .DataType = System.Type.GetType("System.String")
    '                    .DefaultValue = ""
    '                End With
    '                mdtFinal.Columns.Add(dCol)

    '                dCol = New DataColumn
    '                With dCol
    '                    .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
    '                    .Caption = ""
    '                    .DataType = System.Type.GetType("System.Int32")
    '                    .DefaultValue = -1
    '                    'Shani (26-Sep-2016) -- Start
    '                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                    .ExtendedProperties.Add("IsSystemGen", drow.Item("isdefaultentry"))
    '                    'Shani (26-Sep-2016) -- End
    '                End With
    '                mdtFinal.Columns.Add(dCol)
    '            Next

    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "periodunkid"
    '                .Caption = ""
    '                .DataType = System.Type.GetType("System.Int32")
    '                .DefaultValue = -1
    '            End With
    '            mdtFinal.Columns.Add(dCol)

    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "itemtypeid"
    '                .Caption = ""
    '                .DataType = System.Type.GetType("System.Int32")
    '                .DefaultValue = -1
    '            End With
    '            mdtFinal.Columns.Add(dCol)

    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "selectionmodeid"
    '                .Caption = ""
    '                .DataType = System.Type.GetType("System.Int32")
    '                .DefaultValue = -1
    '            End With
    '            mdtFinal.Columns.Add(dCol)

    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "GUID"
    '                .Caption = ""
    '                .DataType = System.Type.GetType("System.String")
    '                .DefaultValue = ""
    '            End With
    '            mdtFinal.Columns.Add(dCol)


    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "analysisunkid"
    '                .Caption = ""
    '                .DataType = System.Type.GetType("System.Int32")
    '                .DefaultValue = 0
    '            End With
    '            mdtFinal.Columns.Add(dCol)

    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "ismanual"
    '                .Caption = ""
    '                .DataType = GetType(System.Boolean)
    '                .DefaultValue = True
    '            End With
    '            mdtFinal.Columns.Add(dCol)

    '            'S.SANDEEP |25-MAR-2019| -- START
    '            dCol = New DataColumn
    '            With dCol
    '                .ColumnName = "viewmodeid"
    '                .Caption = ""
    '                .DataType = GetType(System.Int32)
    '                If blnAddAssessFilter = False Then
    '                    .DefaultValue = intViewModeId
    '                Else
    '                    .DefaultValue = 0
    '                End If
    '            End With
    '            mdtFinal.Columns.Add(dCol)
    '            'S.SANDEEP |25-MAR-2019| -- END

    '            'StrQ = "SELECT " & _
    '            '       "     hrassess_custom_items.customheaderunkid " & _
    '            '       "    ,hrcompetency_customitem_tran.customitemunkid " & _
    '            '       "    ,hrcompetency_customitem_tran.custom_value " & _
    '            '       "    ,hrassess_custom_items.itemtypeid " & _
    '            '       "    ,hrassess_custom_items.selectionmodeid " & _
    '            '       "    ,hrcompetency_customitem_tran.periodunkid " & _
    '            '       "    ,customanalysistranguid AS customanalysistranguid " & _
    '            '       "FROM hrcompetency_customitem_tran " & _
    '            '       "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
    '            '       "WHERE isvoid = 0 AND analysisunkid = '" & iAnalysisUnkid & "' AND hrassess_custom_items.customheaderunkid = '" & iHeaderId & "' " & _
    '            '       "AND hrassess_custom_items.periodunkid = '" & iPeriodId & "' "

    '            StrQ = "SELECT " & _
    '                   "     hrassess_custom_items.customheaderunkid " & _
    '                   "    ,hrcompetency_customitem_tran.customitemunkid " & _
    '                   "    ,hrcompetency_customitem_tran.custom_value " & _
    '                   "    ,hrassess_custom_items.itemtypeid " & _
    '                   "    ,hrassess_custom_items.selectionmodeid " & _
    '                   "    ,hrcompetency_customitem_tran.periodunkid " & _
    '                   "    ,customanalysistranguid AS customanalysistranguid " & _
    '                   "    ,hrcompetency_customitem_tran.analysisunkid " & _
    '                   "    ,hrcompetency_customitem_tran.ismanual " & _
    '                   "    ,hrassess_custom_items.viewmodeid " & _
    '                   "FROM hrcompetency_customitem_tran " & _
    '                   "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
    '                   "    JOIN hrevaluation_analysis_master ON hrcompetency_customitem_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
    '                   "WHERE hrcompetency_customitem_tran.isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND hrassess_custom_items.customheaderunkid = '" & iHeaderId & "' " & _
    '                   "AND hrassess_custom_items.periodunkid = '" & iPeriodId & "' "
    '            'S.SANDEEP |25-MAR-2019| -- START
    '            'hrassess_custom_items.viewmodeid -- ADDED
    '            'S.SANDEEP |25-MAR-2019| -- END


    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'JOIN hrevaluation_analysis_master ON hrcompetency_customitem_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid --- ADDED
    '            If blnOnlyCommittedValue = True Then
    '                StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
    '            End If
    '            'S.SANDEEP [04 JUN 2015] -- END

    '            'S.SANDEEP |14-MAR-2019| -- START
    '            If blnAddAssessFilter Then
    '                Select Case iMode
    '                    Case enAssessmentMode.SELF_ASSESSMENT
    '                        StrQ &= " AND hrassess_custom_items.viewmodeid = 0 "
    '                    Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                        StrQ &= " AND hrassess_custom_items.viewmodeid = 2 "
    '                    Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                        StrQ &= " AND hrassess_custom_items.viewmodeid = 3 "
    '                End Select
    '            End If
    '            'S.SANDEEP |14-MAR-2019| -- END

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If iAction <> enAction.EDIT_ONE Then
    '                dsList.Tables(0).Rows.Clear()
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                Dim dFRow As DataRow = Nothing
    '                Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
    '                For Each dRow As DataRow In dsList.Tables(0).Rows
    '                    If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
    '                    iNewCustomId = dRow.Item("customitemunkid")
    '                    If iCustomId = iNewCustomId Then
    '                        dFRow = mdtFinal.NewRow
    '                        mdtFinal.Rows.Add(dFRow)
    '                    End If
    '                    Select Case CInt(dRow.Item("itemtypeid"))
    '                        Case clsassess_custom_items.enCustomType.FREE_TEXT
    '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
    '                        Case clsassess_custom_items.enCustomType.SELECTION
    '                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
    '                                Select Case CInt(dRow.Item("selectionmodeid"))
    '                                    'S.SANDEEP [04 OCT 2016] -- START
    '                                    'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
    '                                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
    '                                        'S.SANDEEP [04 OCT 2016] -- END
    '                                        Dim objCMaster As New clsCommon_Master
    '                                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
    '                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
    '                                        objCMaster = Nothing
    '                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                                        Dim objCMaster As New clsassess_competencies_master
    '                                        objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
    '                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
    '                                        objCMaster = Nothing
    '                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                                        Dim objEmpField1 As New clsassess_empfield1_master
    '                                        objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
    '                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
    '                                        objEmpField1 = Nothing

    '                                        'S.SANDEEP |16-AUG-2019| -- START
    '                                        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
    '                                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
    '                                        Dim objCMaster As New clsCommon_Master
    '                                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
    '                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
    '                                        objCMaster = Nothing
    '                                        'S.SANDEEP |16-AUG-2019| -- END

    '                                End Select
    '                            End If
    '                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
    '                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
    '                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
    '                            End If
    '                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
    '                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
    '                                If IsNumeric(dRow.Item("custom_value")) Then
    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
    '                                End If
    '                            End If
    '                    End Select
    '                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
    '                    dFRow.Item("periodunkid") = dRow.Item("periodunkid")
    '                    dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
    '                    dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
    '                    dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
    '                    dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
    '                    dFRow.Item("ismanual") = dRow.Item("ismanual")
    '                    'S.SANDEEP |25-MAR-2019| -- START
    '                    dFRow.Item("viewmodeid") = dRow.Item("viewmodeid")
    '                    'S.SANDEEP |25-MAR-2019| -- END
    '                Next
    '            Else
    '                mdtFinal.Rows.Add(mdtFinal.NewRow)
    '            End If

    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_Custom_Items; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return mdtFinal
    'End Function

    Public Function Get_Custom_Items_List(ByVal iPeriodId As Integer, _
                                          ByVal iHeaderId As Integer, _
                                          ByVal iAnalysisUnkid As Integer, _
                                          ByVal iMode As enAssessmentMode, _
                                          ByVal iEmployeeId As Integer, _
                                          ByVal iAction As enAction, _
                                          Optional ByVal blnOnlyCommittedValue As Boolean = False, _
                                          Optional ByVal blnAddAssessFilter As Boolean = False) As DataTable

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn
        Dim objDataOperation As New clsDataOperation
        Dim blnIsPDPItem As Boolean = False
        Dim strDBName As String = ""
        Try
            mdtFinal = New DataTable("CTList")

            If iHeaderId.ToString.StartsWith("9000") Then
                Dim intCoyId As Integer = 0
                Try
                    StrQ = "SELECT " & _
                           "   @coyid = companyunkid " & _
                           "FROM cfcommon_period_tran WITH (NOLOCK) " & _
                           "    JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                           "WHERE periodunkid = '" & iPeriodId & "' "

                    objDataOperation.AddParameter("@coyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCoyId, ParameterDirection.InputOutput)
                    
                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    intCoyId = objDataOperation.GetParameterValue("@coyid")

                    StrQ = "SELECT " & _
                           "  @sdb = database_name " & _
                           "FROM cfcommon_period_tran WITH (NOLOCK) " & _
                           "    JOIN hrmsConfiguration..cffinancial_year_tran WITH (NOLOCK) ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                           "WHERE companyunkid = '" & intCoyId & "' AND isclosed = 0 "

                    objDataOperation.AddParameter("@sdb", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDBName, ParameterDirection.InputOutput)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strDBName = objDataOperation.GetParameterValue("@sdb")

                Catch ex As Exception
                    intCoyId = 0 : strDBName = ""
                End Try

                If intCoyId > 0 Then
                    Dim blnIsIntegrated As Boolean = True
                    If blnIsIntegrated Then
                        blnIsPDPItem = True
                        Dim objCategory As New clspdpcategory_master
                        objCategory._Categoryunkid = CInt(iHeaderId.ToString().Replace("9000", ""))
                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "Header_Name"
                            .Caption = Language.getMessage(mstrModuleName, 116, "Custom Header")
                            .DataType = System.Type.GetType("System.String")
                            .DefaultValue = objCategory._Category
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "Header_Id"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = "9000" & objCategory._Categoryunkid.ToString()
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "Is_Allow_Multiple"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Boolean")
                            If iMode <> enAssessmentMode.SELF_ASSESSMENT Then
                                .DefaultValue = True
                            Else
                                .DefaultValue = False
                            End If
                            .ExtendedProperties.Add("IsMatch_With_Competency", False)
                        End With
                        mdtFinal.Columns.Add(dCol)
                        objCategory = Nothing

                        Dim objPDPItem As New clspdpitem_master
                        dsList = objPDPItem.getComboList("List", False, CInt(iHeaderId.ToString().Replace("9000", "")))
                        Dim dtTable As DataTable = Nothing
                        If blnAddAssessFilter Then
                            If iMode > 0 Then
                                dtTable = New DataView(dsList.Tables(0), "visibletoid = '" & iMode & "'", "", DataViewRowState.CurrentRows).ToTable.Copy
                            End If
                        End If
                        If dtTable IsNot Nothing Then
                            dsList.Tables.RemoveAt(0)
                            dsList.Tables.Add(dtTable.Copy)
                        End If
                        objPDPItem = Nothing

                        Dim intViewModeId As Integer = -1
                        If blnAddAssessFilter = False Then
                            If dsList.Tables(0).Rows.Count > 0 Then
                                If IsDBNull(dsList.Tables(0).Compute("MAX(visibletoid)", "")) = False Then
                                    intViewModeId = CInt(dsList.Tables(0).Compute("MAX(visibletoid)", ""))
                                End If
                            End If
                        End If

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For Each drow As DataRow In dsList.Tables(0).Rows
                                dCol = New DataColumn
                                With dCol
                                    .ColumnName = "CItem_" & drow.Item("Id").ToString
                                    .Caption = drow.Item("Name").ToString
                                    .DataType = System.Type.GetType("System.String")
                                    .DefaultValue = ""
                                End With
                                mdtFinal.Columns.Add(dCol)

                                dCol = New DataColumn
                                With dCol
                                    .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                                    .Caption = ""
                                    .DataType = System.Type.GetType("System.Int32")
                                    .DefaultValue = -1
                                    .ExtendedProperties.Add("IsSystemGen", False)
                                End With
                                mdtFinal.Columns.Add(dCol)
                            Next
                        End If

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "periodunkid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = -1
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "itemtypeid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = -1
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "selectionmodeid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = -1
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "GUID"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.String")
                            .DefaultValue = ""
                        End With
                        mdtFinal.Columns.Add(dCol)


                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "analysisunkid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = 0
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "ismanual"
                            .Caption = ""
                            .DataType = GetType(System.Boolean)
                            .DefaultValue = True
                        End With
                        mdtFinal.Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "viewmodeid"
                            .Caption = ""
                            .DataType = GetType(System.Int32)
                            If blnAddAssessFilter = False Then
                                .DefaultValue = intViewModeId
                            Else
                                .DefaultValue = 0
                            End If
                        End With
                        mdtFinal.Columns.Add(dCol)
                    End If
                End If
            Else
            Dim objCHeader As New clsassess_custom_header
            objCHeader._Customheaderunkid = iHeaderId
            dCol = New DataColumn
            With dCol
                .ColumnName = "Header_Name"
                .Caption = Language.getMessage(mstrModuleName, 116, "Custom Header")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = objCHeader._Name
            End With
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Header_Id"
                .Caption = ""
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = objCHeader._Customheaderunkid
            End With
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Is_Allow_Multiple"
                .Caption = ""
                .DataType = System.Type.GetType("System.Boolean")
                .DefaultValue = objCHeader._Is_Allow_Multiple
                .ExtendedProperties.Add("IsMatch_With_Competency", objCHeader._IsMatch_With_Competency)
            End With
            mdtFinal.Columns.Add(dCol)
            objCHeader = Nothing

            Dim objCustomItems As New clsassess_custom_items
            If blnAddAssessFilter Then
                dsList = objCustomItems.getComboList(iPeriodId, iHeaderId, False, "List", iMode)
            Else
            dsList = objCustomItems.getComboList(iPeriodId, iHeaderId, False, "List")
            End If
            objCustomItems = Nothing

            Dim intViewModeId As Integer = -1
            If blnAddAssessFilter = False Then
                If dsList.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsList.Tables(0).Compute("MAX(viewmodeid)", "")) = False Then
                        intViewModeId = CInt(dsList.Tables(0).Compute("MAX(viewmodeid)", ""))
                    End If
                End If
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each drow As DataRow In dsList.Tables(0).Select("", "isdefaultentry DESC")
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString
                        .Caption = drow.Item("Name").ToString
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                        .Caption = ""
                        .DataType = System.Type.GetType("System.Int32")
                        .DefaultValue = -1
                        .ExtendedProperties.Add("IsSystemGen", drow.Item("isdefaultentry"))
                    End With
                    mdtFinal.Columns.Add(dCol)
                Next

                dCol = New DataColumn
                With dCol
                    .ColumnName = "periodunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "itemtypeid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "selectionmodeid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "GUID"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)


                dCol = New DataColumn
                With dCol
                    .ColumnName = "analysisunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = 0
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "ismanual"
                    .Caption = ""
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = True
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "viewmodeid"
                    .Caption = ""
                    .DataType = GetType(System.Int32)
                    If blnAddAssessFilter = False Then
                        .DefaultValue = intViewModeId
                    Else
                        .DefaultValue = 0
                    End If
                End With
                mdtFinal.Columns.Add(dCol)
                End If
            End If

            If iHeaderId.ToString.StartsWith("9000") Then
                StrQ = "SELECT " & _
                   "     pdpitem_master.categoryunkid AS customheaderunkid " & _
                   "    ,pdpitemdatatran.itemunkid AS customitemunkid " & _
                   "    ,pdpitemdatatran.fieldvalue AS custom_value " & _
                   "    ,pdpitem_master.itemtypeid " & _
                   "    ,pdpitem_master.selectionmodeid " & _
                   "    ,hrevaluation_analysis_master.periodunkid " & _
                   "    ,itemgrpguid AS customanalysistranguid " & _
                   "    ,pdpitemdatatran.analysisunkid " & _
                   "    ,1 AS ismanual " & _
                   "    ,pdpitem_master.visibletoid AS viewmodeid " & _
                   "FROM pdpitemdatatran WITH (NOLOCK) " & _
                   "    JOIN pdpitem_master WITH (NOLOCK) ON pdpitem_master.itemunkid = pdpitemdatatran.itemunkid " & _
                   "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON pdpitemdatatran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                   "WHERE pdpitemdatatran.isvoid = 0 AND (CASE WHEN selfemployeeunkid <= 0 THEN assessedemployeeunkid ELSE selfemployeeunkid END) = '" & iEmployeeId & "' AND pdpitem_master.categoryunkid = '" & iHeaderId.ToString().Replace("9000", "") & "' " & _
                   "AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' "
            Else
                StrQ = "SELECT " & _
                       "     hrassess_custom_items.customheaderunkid " & _
                       "    ,hrcompetency_customitem_tran.customitemunkid " & _
                       "    ,hrcompetency_customitem_tran.custom_value " & _
                       "    ,hrassess_custom_items.itemtypeid " & _
                       "    ,hrassess_custom_items.selectionmodeid " & _
                       "    ,hrcompetency_customitem_tran.periodunkid " & _
                       "    ,customanalysistranguid AS customanalysistranguid " & _
                       "    ,hrcompetency_customitem_tran.analysisunkid " & _
                       "    ,hrcompetency_customitem_tran.ismanual " & _
                       "    ,hrassess_custom_items.viewmodeid " & _
                       "FROM hrcompetency_customitem_tran WITH (NOLOCK) " & _
                       "    JOIN hrassess_custom_items WITH (NOLOCK) ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
                       "    JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hrcompetency_customitem_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                       "WHERE hrcompetency_customitem_tran.isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND hrassess_custom_items.customheaderunkid = '" & iHeaderId & "' " & _
                       "AND hrassess_custom_items.periodunkid = '" & iPeriodId & "' "
            End If

                If blnOnlyCommittedValue = True Then
                    StrQ &= " AND hrevaluation_analysis_master.iscommitted = 1 "
                End If

                If blnAddAssessFilter Then
                    Select Case iMode
                        Case enAssessmentMode.SELF_ASSESSMENT
                        If blnIsPDPItem Then
                            StrQ &= " AND pdpitem_master.visibletoid = 1 "
                        Else
                            StrQ &= " AND hrassess_custom_items.viewmodeid = 0 "
                        End If
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If blnIsPDPItem Then
                            StrQ &= " AND pdpitem_master.visibletoid = 2 "
                        Else
                            StrQ &= " AND hrassess_custom_items.viewmodeid = 2 "
                        End If
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If blnIsPDPItem Then
                            StrQ &= " AND pdpitem_master.visibletoid = 3 "
                        Else
                            StrQ &= " AND hrassess_custom_items.viewmodeid = 3 "
                        End If
                    End Select
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iAction <> enAction.EDIT_ONE Then
                    dsList.Tables(0).Rows.Clear()
                End If


            ''
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    Dim dFRow As DataRow = Nothing
            '    Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
            '    For Each dRow As DataRow In dsList.Tables(0).Rows
            '        If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
            '        iNewCustomId = dRow.Item("customitemunkid")
            '        If iCustomId = iNewCustomId Then
            '            dFRow = mdtFinal.NewRow
            '            mdtFinal.Rows.Add(dFRow)
            '        End If
            '        Select Case CInt(dRow.Item("itemtypeid"))
            '            Case clsassess_custom_items.enCustomType.FREE_TEXT
            '                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
            '            Case clsassess_custom_items.enCustomType.SELECTION
            '                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
            '                    Select Case CInt(dRow.Item("selectionmodeid"))
            '                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
            '                            Dim objCMaster As New clsCommon_Master
            '                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
            '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
            '                            objCMaster = Nothing
            '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
            '                            Dim objCMaster As New clsassess_competencies_master
            '                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
            '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
            '                            objCMaster = Nothing
            '                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
            '                            Dim objEmpField1 As New clsassess_empfield1_master
            '                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
            '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
            '                            objEmpField1 = Nothing

            '                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
            '                            Dim objCMaster As New clsCommon_Master
            '                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
            '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
            '                            objCMaster = Nothing

            '                            'S.SANDEEP |03-MAY-2021| -- START
            '                            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            '                        Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_PERIOD
            '                            If blnIsPDPItem Then
            '                                Dim objPrd As New clscommom_period_Tran
            '                                objPrd._Periodunkid(strDBName) = CInt(dRow.Item("custom_value"))
            '                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objPrd._Period_Name
            '                                objPrd = Nothing
            '                            End If

            '                            'S.SANDEEP |03-MAY-2021| -- END

            '                    End Select
            '                End If
            '            Case clsassess_custom_items.enCustomType.DATE_SELECTION
            '                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
            '                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
            '                End If
            '            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
            '                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
            '                    If IsNumeric(dRow.Item("custom_value")) Then
            '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
            '                    End If
            '                End If
            '        End Select
            '        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
            '        dFRow.Item("periodunkid") = dRow.Item("periodunkid")
            '        dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
            '        dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
            '        dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
            '        dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
            '        dFRow.Item("ismanual") = dRow.Item("ismanual")
            '        dFRow.Item("viewmodeid") = dRow.Item("viewmodeid")
            '    Next
            'Else
            '    mdtFinal.Rows.Add(mdtFinal.NewRow)
            'End If
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim dFRow As DataRow = Nothing
                    Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                Dim iGuid As String = ""
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                        iNewCustomId = dRow.Item("customitemunkid")

                    If iGuid <> dRow.Item("customanalysistranguid") Then
                            dFRow = mdtFinal.NewRow
                            mdtFinal.Rows.Add(dFRow)
                        iGuid = dRow.Item("customanalysistranguid")
                        End If
                        Select Case CInt(dRow.Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                            Case clsassess_custom_items.enCustomType.SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    Select Case CInt(dRow.Item("selectionmodeid"))
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            Dim objCMaster As New clsassess_competencies_master
                                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            Dim objEmpField1 As New clsassess_empfield1_master
                                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                            objEmpField1 = Nothing

                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing

                                        'S.SANDEEP |03-MAY-2021| -- START
                                        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                                    Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_PERIOD
                                        If blnIsPDPItem Then
                                            Dim objPrd As New clscommom_period_Tran
                                            objPrd._Periodunkid(strDBName) = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objPrd._Period_Name
                                            objPrd = Nothing
                                        End If

                                        'S.SANDEEP |03-MAY-2021| -- END

                                    End Select
                                End If
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dRow.Item("custom_value")) Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                    End If
                                End If
                        End Select
                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                        dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                        dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                        dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                        dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                        dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                        dFRow.Item("ismanual") = dRow.Item("ismanual")
                        dFRow.Item("viewmodeid") = dRow.Item("viewmodeid")
                    Next
                Else
                    mdtFinal.Rows.Add(mdtFinal.NewRow)
                End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Custom_Items; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function
    'S.SANDEEP |03-MAY-2021| -- END
    

    'S.SANDEEP |14-MAR-2019| -- START
    'Public Function Get_CItems_ForAddEdit(ByVal iPeriodId As Integer, ByVal iHeaderId As Integer, ByVal iMode As enAssessmentMode) As DataTable

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    'Public Function Get_CItems_ForAddEdit(ByVal iPeriodId As Integer, _
    '                                      ByVal iHeaderId As Integer, _
    '                                      Optional ByVal iMode As Integer = 0, _
    '                                      Optional ByVal strEditGUID As String = "", _
    '                                      Optional ByVal blnAddAssessModeFilter As Boolean = False) As DataTable 'S.SANDEEP |18-JAN-2020| -- START {strEditGUID,blnAddAssessModeFilter} -- END
    '    'S.SANDEEP |14-MAR-2019| -- END
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim objDataOperation = New clsDataOperation
    '    Try
    '        'S.SANDEEP |18-JAN-2020| -- START
    '        'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '        StrQ = "SELECT " & _
    '               "     custom_item " & _
    '               "    ,ISNULL(CASE WHEN itemtypeid IN (1,3,4) THEN ISNULL(hrcompetency_customitem_tran.custom_value,'') " & _
    '               "                 WHEN itemtypeid = 2 AND selectionmodeid IN (1,5,4,6) THEN cfcommon_master.name " & _
    '               "                 WHEN itemtypeid = 2 AND selectionmodeid IN (2) THEN hrassess_competencies_master.name " & _
    '               "                 WHEN itemtypeid = 2 AND selectionmodeid IN (3) THEN '' " & _
    '               "     END,'') AS custom_value " & _
    '               "    ,itemtypeid " & _
    '               "    ,selectionmodeid " & _
    '               "    ,CAST(NULL AS DATETIME) AS ddate " & _
    '               "    ,0 AS selectedid " & _
    '               "    ,isnull(hrcompetency_customitem_tran.customitemunkid,hrassess_custom_items.customitemunkid) AS customitemunkid " & _
    '               "    ,CASE WHEN viewmodeid IN(0,@Mode) THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS rOnly " & _
    '               "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
    '               "    ,hrassess_custom_items.iscompletedtraining AS iscompletedtraining " & _
    '               "    ,hrassess_custom_items.customheaderunkid " & _
    '               "    ,ISNULL(hrcompetency_customitem_tran.custom_value,'') AS customvalueId " & _
    '               "FROM hrassess_custom_items " & _
    '               "    LEFT JOIN hrcompetency_customitem_tran ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
    '               "        AND hrassess_custom_items.periodunkid = hrcompetency_customitem_tran.periodunkid AND customanalysistranguid = '" & strEditGUID & "' " & _
    '               "    LEFT JOIN cfcommon_master ON hrcompetency_customitem_tran.custom_value = CAST(masterunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (1,5,4,6) AND cfcommon_master.isactive = 1 " & _
    '               "    LEFT JOIN hrassess_competencies_master ON hrcompetency_customitem_tran.custom_value = CAST(hrassess_competencies_master.competenciesunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (2) AND hrassess_competencies_master.isactive = 1 " & _
    '               "        AND hrassess_competencies_master.periodunkid = hrassess_custom_items.periodunkid " & _
    '               "    LEFT JOIN hrassess_empfield1_master ON hrcompetency_customitem_tran.custom_value = CAST(hrassess_empfield1_master.empfield1unkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (3) AND hrassess_empfield1_master.isvoid = 0 " & _
    '               "        AND hrassess_empfield1_master.periodunkid = hrassess_custom_items.periodunkid " & _
    '               "WHERE customheaderunkid = '" & iHeaderId & "' AND hrassess_custom_items.periodunkid = '" & iPeriodId & "' AND hrassess_custom_items.isactive = 1 "

    '        'StrQ = "SELECT " & _
    '        '       "     custom_item " & _
    '        '       "    ,'' AS custom_value " & _
    '        '       "    ,itemtypeid " & _
    '        '       "    ,selectionmodeid " & _
    '        '       "    ,CAST(NULL AS DATETIME) AS ddate " & _
    '        '       "    ,0 AS selectedid " & _
    '        '       "    ,customitemunkid " & _
    '        '       "    ,CASE WHEN viewmodeid IN(0,@Mode) THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS rOnly " & _
    '        '       "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
    '        '       "    ,hrassess_custom_items.iscompletedtraining AS iscompletedtraining " & _
    '        '       "    ,hrassess_custom_items.customheaderunkid " & _
    '        '       "FROM hrassess_custom_items " & _
    '        '       "WHERE customheaderunkid = '" & iHeaderId & "' AND periodunkid = '" & iPeriodId & "' AND isactive = 1 "

    '        'S.SANDEEP |18-JAN-2020| -- END

    '        'S.SANDEEP |08-JAN-2019| -- START {iscompletedtraining} -- END
    '        'Shani (26-Sep-2016) -- [isdefaultentry]
    '        'S.SANDEEP |25-MAR-2019| -- START {customheaderunkid} -- END


    '        'S.SANDEEP |14-MAR-2019| -- START
    '        'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
    '        'IT WAS AGAIN OPNED DUE TO RUTTA'S REQUEST FOR NMB TRAINING
    '        'Select Case iMode
    '        '    Case enAssessmentMode.SELF_ASSESSMENT
    '        '        StrQ &= " AND viewmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
    '        '    Case enAssessmentMode.APPRAISER_ASSESSMENT
    '        '        StrQ &= " AND viewmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
    '        '    Case enAssessmentMode.REVIEWER_ASSESSMENT
    '        '        StrQ &= " AND viewmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
    '        'End Select

    '        If blnAddAssessModeFilter Then
    '            If iMode > 0 Then
    '                Select Case iMode
    '                    Case enAssessmentMode.SELF_ASSESSMENT
    '                        StrQ &= " AND viewmodeid = 0 "
    '                    Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                        StrQ &= " AND viewmodeid = 2 "
    '                    Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                        StrQ &= " AND viewmodeid = 3 "
    '                End Select
    '            End If
    '        End If
    '        'S.SANDEEP |14-MAR-2019| -- END

    '        objDataOperation.AddParameter("@Mode", SqlDbType.Int, eZeeDataType.INT_SIZE, iMode)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'S.SANDEEP |18-JAN-2020| -- START
    '        'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '        For index As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '            If dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.DATE_SELECTION Then
    '                If dsList.Tables(0).Rows(index)("custom_value").ToString().Length > 0 Then
    '                    Dim dt As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).Date
    '                    dsList.Tables(0).Rows(index)("custom_value") = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).ToShortDateString()
    '                    dsList.Tables(0).Rows(index)("ddate") = dt
    '                End If
    '            ElseIf dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.SELECTION Then
    '                If Val(dsList.Tables(0).Rows(index)("customvalueId")) > 0 Then dsList.Tables(0).Rows(index)("selectedid") = CInt(Val(dsList.Tables(0).Rows(index)("customvalueId")))
    '            End If
    '        Next
    '        'S.SANDEEP |18-JAN-2020| -- END

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_CItems_ForAddEdit; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList.Tables(0)
    'End Function
    Public Function Get_CItems_ForAddEdit(ByVal iPeriodId As Integer, _
                                          ByVal iHeaderId As Integer, _
                                          Optional ByVal iMode As Integer = 0, _
                                          Optional ByVal strEditGUID As String = "", _
                                          Optional ByVal blnAddAssessModeFilter As Boolean = False) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation = New clsDataOperation
        Try
            If iHeaderId.ToString.StartsWith("9000") Then
                StrQ = "SELECT " & _
                       "     pdpitem_master.item as custom_item " & _
                       "    ,ISNULL(CASE WHEN itemtypeid IN (1,3,4) THEN ISNULL(pdpitemdatatran.fieldvalue,'') " & _
                       "                 WHEN itemtypeid = 2 AND selectionmodeid IN (1,5,4,6) THEN cfcommon_master.name " & _
                       "                 WHEN itemtypeid = 2 AND selectionmodeid IN (7) THEN cfcommon_period_tran.period_name " & _
                       "                 WHEN itemtypeid = 2 AND selectionmodeid IN (2) THEN hrassess_competencies_master.name " & _
                       "                 WHEN itemtypeid = 2 AND selectionmodeid IN (3) THEN '' " & _
                       "     END,'') AS custom_value " & _
                       "    ,itemtypeid " & _
                       "    ,selectionmodeid " & _
                       "    ,CAST(NULL AS DATETIME) AS ddate " & _
                       "    ,0 AS selectedid " & _
                       "    ,CAST('5000'+ CAST(pdpitem_master.itemunkid AS NVARCHAR(MAX)) AS INT) AS customitemunkid " & _
                       "    ,CASE WHEN visibletoid IN(1,@Mode) THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS rOnly " & _
                       "    ,0 AS isdefaultentry " & _
                       "    ,0 AS iscompletedtraining " & _
                       "    ,pdpitem_master.categoryunkid as customheaderunkid " & _
                       "    ,ISNULL(pdpitemdatatran.fieldvalue,'') AS customvalueId " & _
                       "    ,CASE WHEN orgitem = 'Current Role' THEN 1 ELSE 0 END AS iRole " & _
                       "FROM pdpitem_master WITH (NOLOCK) " & _
                       "   LEFT JOIN pdpitemdatatran WITH (NOLOCK) ON pdpitem_master.itemunkid = pdpitemdatatran.itemunkid AND itemgrpguid = '" & strEditGUID & "' " & _
                       "   LEFT JOIN cfcommon_period_tran WITH (NOLOCK) ON pdpitemdatatran.fieldvalue = CAST(cfcommon_period_tran.periodunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (7) AND cfcommon_period_tran.isactive = 1 AND modulerefid = 5 " & _
                       "   LEFT JOIN cfcommon_master WITH (NOLOCK) ON pdpitemdatatran.fieldvalue = CAST(masterunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (1,5,4,6) AND cfcommon_master.isactive = 1 " & _
                       "   LEFT JOIN hrassess_competencies_master WITH (NOLOCK) ON pdpitemdatatran.fieldvalue = CAST(hrassess_competencies_master.competenciesunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (2) AND hrassess_competencies_master.isactive = 1 " & _
                       "   LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON pdpitemdatatran.fieldvalue = CAST(hrassess_empfield1_master.empfield1unkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (3) AND hrassess_empfield1_master.isvoid = 0 " & _
                       "WHERE pdpitem_master.categoryunkid = '" & CInt(iHeaderId.ToString().Replace("9000", "")) & "' AND pdpitem_master.isactive = 1 "

                If blnAddAssessModeFilter Then
                    If iMode > 0 Then
                        Select Case iMode
                            Case enAssessmentMode.SELF_ASSESSMENT
                                StrQ &= " AND visibletoid = 1 "
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                StrQ &= " AND visibletoid = 2 "
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ &= " AND visibletoid = 3 "
                        End Select
                    End If
                End If
            Else
            StrQ = "SELECT " & _
                   "     custom_item " & _
                   "    ,ISNULL(CASE WHEN itemtypeid IN (1,3,4) THEN ISNULL(hrcompetency_customitem_tran.custom_value,'') " & _
                   "                 WHEN itemtypeid = 2 AND selectionmodeid IN (1,5,4,6) THEN cfcommon_master.name " & _
                   "                 WHEN itemtypeid = 2 AND selectionmodeid IN (2) THEN hrassess_competencies_master.name " & _
                   "                 WHEN itemtypeid = 2 AND selectionmodeid IN (3) THEN '' " & _
                   "     END,'') AS custom_value " & _
                   "    ,itemtypeid " & _
                   "    ,selectionmodeid " & _
                   "    ,CAST(NULL AS DATETIME) AS ddate " & _
                   "    ,0 AS selectedid " & _
                   "    ,isnull(hrcompetency_customitem_tran.customitemunkid,hrassess_custom_items.customitemunkid) AS customitemunkid " & _
                   "    ,CASE WHEN viewmodeid IN(0,@Mode) THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS rOnly " & _
                   "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
                   "    ,hrassess_custom_items.iscompletedtraining AS iscompletedtraining " & _
                   "    ,hrassess_custom_items.customheaderunkid " & _
                   "    ,ISNULL(hrcompetency_customitem_tran.custom_value,'') AS customvalueId " & _
                   "    ,0 AS iRole " & _
                       "FROM hrassess_custom_items WITH (NOLOCK) " & _
                       "    LEFT JOIN hrcompetency_customitem_tran WITH (NOLOCK) ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
                   "        AND hrassess_custom_items.periodunkid = hrcompetency_customitem_tran.periodunkid AND customanalysistranguid = '" & strEditGUID & "' " & _
                       "    LEFT JOIN cfcommon_master WITH (NOLOCK) ON hrcompetency_customitem_tran.custom_value = CAST(masterunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (1,5,4,6) AND cfcommon_master.isactive = 1 " & _
                       "    LEFT JOIN hrassess_competencies_master WITH (NOLOCK) ON hrcompetency_customitem_tran.custom_value = CAST(hrassess_competencies_master.competenciesunkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (2) AND hrassess_competencies_master.isactive = 1 " & _
                   "        AND hrassess_competencies_master.periodunkid = hrassess_custom_items.periodunkid " & _
                       "    LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrcompetency_customitem_tran.custom_value = CAST(hrassess_empfield1_master.empfield1unkid AS NVARCHAR(MAX)) AND itemtypeid = 2 AND selectionmodeid IN (3) AND hrassess_empfield1_master.isvoid = 0 " & _
                   "        AND hrassess_empfield1_master.periodunkid = hrassess_custom_items.periodunkid " & _
                   "WHERE customheaderunkid = '" & iHeaderId & "' AND hrassess_custom_items.periodunkid = '" & iPeriodId & "' AND hrassess_custom_items.isactive = 1 "

            If blnAddAssessModeFilter Then
            If iMode > 0 Then
                Select Case iMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= " AND viewmodeid = 0 "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND viewmodeid = 2 "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND viewmodeid = 3 "
                End Select
            End If
            End If
            End If


            objDataOperation.AddParameter("@Mode", SqlDbType.Int, eZeeDataType.INT_SIZE, iMode)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For index As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dsList.Tables(0).Rows(index)("custom_value").ToString().Length > 0 Then
                        Dim dt As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).Date
                        dsList.Tables(0).Rows(index)("custom_value") = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).ToShortDateString()
                        dsList.Tables(0).Rows(index)("ddate") = dt
                    End If
                ElseIf dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.SELECTION Then
                    If Val(dsList.Tables(0).Rows(index)("customvalueId")) > 0 Then dsList.Tables(0).Rows(index)("selectedid") = CInt(Val(dsList.Tables(0).Rows(index)("customvalueId")))
                End If
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_CItems_ForAddEdit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function
    'S.SANDEEP |03-MAY-2021| -- END

    'S.SANDEEP |18-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '''' <summary>
    '''' Modify by Shani Sheladiya (01 MARCH 2016)
    '''' </summary>
    '''' <remarks></remarks>
    'Public Sub Email_Notification(ByVal eAssessMode As enAssessmentMode, _
    '                              ByVal iEmployeeId As Integer, _
    '                              ByVal iPeriodId As Integer, _
    '                              ByVal isReviewerMandatory As Boolean, _
    '                              ByVal strDatabaseName As String, _
    '                              Optional ByVal iCompanyId As Integer = 0, _
    '                              Optional ByVal sArutiSSURL As String = "", _
    '                              Optional ByVal sName As String = "", _
    '                              Optional ByVal iLoginTypeId As Integer = 0, _
    '                              Optional ByVal iLoginEmployeeId As Integer = 0)
    '    'Sohail (21 Aug 2015) - [strDatabaseName]

    '    Dim dsList As DataSet = Nothing
    '    Dim strLink As String = String.Empty
    '    Dim StrQ As String = String.Empty
    '    Dim sYearName As String = String.Empty

    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
    '        If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
    '        Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail
    '        Dim objPeriod As New clscommom_period_Tran

    '        'Sohail (21 Aug 2015) -- Start
    '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '        'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid = iPeriodId

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid(strDatabaseName) = iPeriodId
    '        objPeriod._Periodunkid(strDatabaseName) = iPeriodId
    '        objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId
    '        'S.SANDEEP [04 JUN 2015] -- END


    '        'Sohail (21 Aug 2015) -- End

    '        'Shani(01-MAR-2016) -- Start
    '        'Enhancement :PA External Approver Flow
    '        'Select Case eAssessMode
    '        '    Case enAssessmentMode.SELF_ASSESSMENT
    '        '        StrQ = "SELECT " & _
    '        '               "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
    '        '               " ,ISNULL(email,'') AS Email " & _
    '        '               " ,hrassessor_master.assessormasterunkid " & _
    '        '               " ,hrapprover_usermapping.userunkid " & _
    '        '               "FROM hrapprover_usermapping " & _
    '        '               " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '        '               " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '        '               "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

    '        '    Case enAssessmentMode.APPRAISER_ASSESSMENT
    '        '        StrQ = "SELECT " & _
    '        '               "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
    '        '               " ,ISNULL(email,'') AS Email " & _
    '        '               " ,hrassessor_master.assessormasterunkid " & _
    '        '               " ,hrapprover_usermapping.userunkid " & _
    '        '               "FROM hrapprover_usermapping " & _
    '        '               " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '        '               " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '        '               "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

    '        '    Case enAssessmentMode.REVIEWER_ASSESSMENT
    '        '        StrQ = "SELECT " & _
    '        '               "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
    '        '               " ,ISNULL(email,'') AS Email " & _
    '        '               " ,employeeunkid AS userunkid " & _
    '        '               "FROM hremployee_master WHERE hremployee_master.employeeunkid = '" & iEmployeeId & "' "
    '        'End Select

    '        'dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        'If objDataOperation.ErrorMessage <> "" Then
    '        '    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
    '        'End If

    '        Select Case eAssessMode
    '            Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
    '                dsList = (New clsAssessor).GetEmailNotificationList(iEmployeeId, IIf(eAssessMode = enAssessmentMode.SELF_ASSESSMENT, False, True), objDataOperation, "List")
    '            Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                StrQ = "SELECT " & _
    '                       "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
    '                       " ,ISNULL(email,'') AS Email " & _
    '                       " ,employeeunkid AS userunkid " & _
    '                       "FROM hremployee_master WHERE hremployee_master.employeeunkid = '" & iEmployeeId & "' "

    '                dsList = objDataOperation.ExecQuery(StrQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
    '                End If
    '        End Select
    '        'Shani(01-MAR-2016) -- End

    '        Dim dsYear As New DataSet
    '        Dim objFY As New clsCompany_Master
    '        dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
    '        If dsYear.Tables("List").Rows.Count > 0 Then
    '            sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
    '        End If
    '        objFY = Nothing

    '        Select Case eAssessMode
    '            Case enAssessmentMode.SELF_ASSESSMENT
    '                If dsList Is Nothing Then Exit Select 'Shani(01-MAR-2016)
    '                For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                    If dtRow.Item("Email") = "" Then Continue For
    '                    Dim strMessage As String = ""
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 17, "Notification for Self-Assessment.")
    '                    strMessage = "<HTML> <BODY>"
    '                    strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " <B>" & dtRow.Item("Ename").ToString() & "</B>, <BR><BR>"
    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 118, "This is to inform you that, I have completed my Self-Assessment for the period of") & "&nbsp;&nbsp;"
    '                    strMessage &= "<B>" & objPeriod._Period_Name & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "<B>&nbsp;" & sYearName & "</B>.&nbsp;"
    '                    strMessage &= Language.getMessage(mstrModuleName, 9, "Please click the link below to assess me as my supervisor.")
    '                    strLink = sArutiSSURL & "/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString))
    '                    strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '                    strMessage &= "<BR><BR>"
    '                    strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
    '                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                    strMessage &= "</BODY></HTML>"
    '                    objMail._Message = strMessage
    '                    objMail._ToEmail = dtRow.Item("Email")
    '                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                    If mstrWebFrmName.Trim.Length > 0 Then
    '                        objMail._Form_Name = mstrWebFrmName
    '                    End If
    '                    objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                    objMail._OperationModeId = iLoginTypeId
    '                    objMail._UserUnkid = mintUserunkid
    '                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'objMail.SendMail()
    '                    objMail.SendMail(iCompanyId)
    '                    'Sohail (30 Nov 2017) -- End
    '                Next

    '                'S.SANDEEP |12-FEB-2019| -- START
    '                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '                Dim strMsg As String = ""
    '                objMail._Subject = Language.getMessage(mstrModuleName, 128, "Notification for Schedule a Meeting.")
    '                strMsg = "<HTML> <BODY>"
    '                strMsg &= Language.getMessage(mstrModuleName, 117, "Dear") & " <B>" & objEmp._Firstname & " " & objEmp._Surname.ToString() & "</B>, <BR><BR>"
    '                strMsg &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 129, "This is to remind you that you need to schedule a meeting meeting your line manager(assessor) assinged to you," & _
    '                                                                                       "In order to perform the assessor evaluation.") & "&nbsp;&nbsp;"
    '                strMsg &= "<B>" & objPeriod._Period_Name & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "<B>&nbsp;" & sYearName & "</B>.&nbsp;"
    '                strMsg &= ""
    '                strMsg &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                strMsg &= "</BODY></HTML>"
    '                objMail._Message = strMsg
    '                objMail._ToEmail = objEmp._Email
    '                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                If mstrWebFrmName.Trim.Length > 0 Then
    '                    objMail._Form_Name = mstrWebFrmName
    '                End If
    '                objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                objMail._OperationModeId = iLoginTypeId
    '                objMail._UserUnkid = mintUserunkid
    '                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                objMail.SendMail(iCompanyId)
    '                'S.SANDEEP |12-FEB-2019| -- END

    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                If dsList Is Nothing Then Exit Select 'Shani(01-MAR-2016)
    '                For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                    If dtRow.Item("Email") = "" Then Continue For
    '                    Dim strMessage As String = ""
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
    '                    strMessage = "<HTML> <BODY>"
    '                    strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " <B>" & dtRow.Item("Ename").ToString() & "</B>, <BR><BR>"
    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 12, "This is to inform you that, the Performance Assessment for Employee")
    '                    strMessage &= "&nbsp;<B>" & objEmp._Firstname & " " & objEmp._Surname & "&nbsp;</B>" & Language.getMessage(mstrModuleName, 13, "for the period of ") & "&nbsp;<B>" & objPeriod._Period_Name & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "<B>&nbsp;" & sYearName & "</B>&nbsp;" & _
    '                    Language.getMessage(mstrModuleName, 14, "is complete. Please click the link below to review it.")
    '                    strLink = sArutiSSURL & "/Assessment New/Performance Evaluation/wPgReviewerEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString))
    '                    strMessage &= "<BR></BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '                    strMessage &= "<BR><BR>"
    '                    strMessage &= "<B>" & sName & "</B>"
    '                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                    strMessage &= "</BODY></HTML>"
    '                    objMail._Message = strMessage
    '                    objMail._ToEmail = dtRow.Item("Email")
    '                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                    If mstrWebFrmName.Trim.Length > 0 Then
    '                        objMail._Form_Name = mstrWebFrmName
    '                    End If
    '                    objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                    objMail._OperationModeId = iLoginTypeId
    '                    objMail._UserUnkid = mintUserunkid
    '                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'objMail.SendMail()
    '                    objMail.SendMail(iCompanyId)
    '                    'Sohail (30 Nov 2017) -- End
    '                Next
    '        End Select
    '        Dim sMsg As String = ""
    '        If isReviewerMandatory = True Then
    '            Select Case eAssessMode
    '                Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                    If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
    '                    sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, sArutiSSURL, iCompanyId)
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
    '                    objMail._Message = sMsg
    '                    objMail._ToEmail = objEmp._Email
    '                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                    If mstrWebFrmName.Trim.Length > 0 Then
    '                        objMail._Form_Name = mstrWebFrmName
    '                    End If
    '                    objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                    objMail._OperationModeId = iLoginTypeId
    '                    objMail._UserUnkid = mintUserunkid
    '                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'objMail.SendMail()
    '                    objMail.SendMail(iCompanyId)
    '                    'Sohail (30 Nov 2017) -- End
    '            End Select
    '        Else
    '            Select Case eAssessMode
    '                Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                    If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
    '                    sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, sArutiSSURL, iCompanyId)
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
    '                    objMail._Message = sMsg
    '                    objMail._ToEmail = objEmp._Email
    '                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                    If mstrWebFrmName.Trim.Length > 0 Then
    '                        objMail._Form_Name = mstrWebFrmName
    '                    End If
    '                    objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                    objMail._OperationModeId = iLoginTypeId
    '                    objMail._UserUnkid = mintUserunkid
    '                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'objMail.SendMail()
    '                    objMail.SendMail(iCompanyId)
    '                    'Sohail (30 Nov 2017) -- End
    '            End Select
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Email_Notification; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Function Notify_Employee(ByVal eName As String, _
    '                                 ByVal ePeriodName As String, _
    '                                 ByVal eYearName As String, _
    '                                 ByVal eARName As String, _
    '                                 ByVal iEmpId As Integer, _
    '                                 ByVal iPeriodId As Integer, _
    '                                 ByVal iArutiURL As String, _
    '                                 ByVal iCompanyId As Integer) As String
    '    Dim strMessage As String = ""
    '    Try
    '        Dim strLink As String = String.Empty
    '        strMessage = "<HTML> <BODY>"
    '        strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " <B>" & eName & "</B>, <BR><BR>"
    '        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 15, "This is to inform you that, Your Performance Assessment for the Period of")
    '        strMessage &= "&nbsp;<B>" & ePeriodName & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "&nbsp;<B>" & eYearName & "</B>&nbsp;" & _
    '        Language.getMessage(mstrModuleName, 16, "is complete. Please login to Aruti Employee Self Service to view it in detail or click the link below for a summary report.")
    '        strLink = iArutiURL & "/Assessment New/Performance Evaluation/wPg_ViewPerfEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & iEmpId.ToString & "|" & iPeriodId.ToString))
    '        strMessage &= "<BR></BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '        strMessage &= "<BR><BR>"
    '        strMessage &= "<B>" & eARName & "</B>"
    '        strMessage &= "<BR><BR>"
    '        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '        strMessage &= "</BODY></HTML>"

    '        Return strMessage

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Notify_Employee; Module Name: " & mstrModuleName)
    '        Return ""
    '    Finally
    '    End Try
    'End Function

    Public Sub Email_Notification(ByVal eAssessMode As enAssessmentMode, _
                                  ByVal iEmployeeId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  ByVal isReviewerMandatory As Boolean, _
                                  ByVal strDatabaseName As String, _
                                  Optional ByVal iCompanyId As Integer = 0, _
                                  Optional ByVal sArutiSSURL As String = "", _
                                  Optional ByVal sName As String = "", _
                                  Optional ByVal iLoginTypeId As Integer = 0, _
                                  Optional ByVal iLoginEmployeeId As Integer = 0)

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim sYearName As String = String.Empty

        'S.SANDEEP |13-NOV-2020| -- START
        'ISSUE/ENHANCEMENT : COMPETENCIES
        Dim intAnalysisunkid As Integer = -1
        Dim iEvaluationTypeId As Integer = clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH
        intAnalysisunkid = GetAnalusisUnkid(iEmployeeId, eAssessMode, iPeriodId)
        If intAnalysisunkid > 0 Then
            _Analysisunkid = intAnalysisunkid
            iEvaluationTypeId = _EvalTypeId
        End If
        'S.SANDEEP |13-NOV-2020| -- END

        Dim objDataOperation As New clsDataOperation
        Try
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
            If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail
            Dim objPeriod As New clscommom_period_Tran

            objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId

            Select Case eAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                    dsList = (New clsAssessor).GetEmailNotificationList(iEmployeeId, IIf(eAssessMode = enAssessmentMode.SELF_ASSESSMENT, False, True), objDataOperation, "List")
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,employeeunkid AS userunkid " & _
                           "FROM hremployee_master WHERE hremployee_master.employeeunkid = '" & iEmployeeId & "' "

                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
            End Select

            Dim dsYear As New DataSet
            Dim objFY As New clsCompany_Master
            dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
            If dsYear.Tables("List").Rows.Count > 0 Then
                sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
            End If
            objFY = Nothing


            'Gajanan [26-Feb-2019] -- Start
            'Enhancement - Email Language Changes For NMB.
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
            'Gajanan [26-Feb-2019] -- End

            Select Case eAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    If dsList Is Nothing Then Exit Select
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        If dtRow.Item("Email") = "" Then Continue For
                        Dim strMessage As String = ""
                        objMail._Subject = Language.getMessage(mstrModuleName, 17, "Notification for Self-Assessment.")
                        strMessage = "<HTML> <BODY>"

                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.
                        strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & info1.ToTitleCase(dtRow.Item("Ename").ToString().ToLower()) & ", <BR><BR>"
                        'strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & dtRow.Item("Ename").ToString().TitleCase() & ", <BR><BR>"

                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 118, "This is to inform you that, I have completed my Self-Assessment for the period of") & "&nbsp;&nbsp;"

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 133, "Please note that, I have completed my Self-Assessment for the period of ") & "&nbsp;&nbsp;"
                        strMessage &= Language.getMessage(mstrModuleName, 133, "Please note that, I have completed my Self-Assessment for the period of ") & "&nbsp;"
                        'S.SANDEEP |25-MAR-2019| -- END


                        'strMessage &= "<B>" & objPeriod._Period_Name & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "<B>&nbsp;" & sYearName & "</B>.&nbsp;"

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "<B>" & objPeriod._Period_Name & "</B>"

                        'S.SANDEEP |30-MAR-2019| -- START
                        'strMessage &= "<B>(" & objPeriod._Period_Name & ")</B>&nbsp;"
                        strMessage &= "<B>(" & objPeriod._Period_Name & ")</B>."
                        'S.SANDEEP |30-MAR-2019| -- END

                        'S.SANDEEP |25-MAR-2019| -- END



                        
                        'strMessage &= Language.getMessage(mstrModuleName, 9, "Please click the link below to assess me as my supervisor.")
                        strMessage &= Language.getMessage(mstrModuleName, 134, "Click the link below for comments.")
                        'Gajanan [26-Feb-2019] -- End

                        'S.SANDEEP |13-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                        'strLink = sArutiSSURL & "/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString))
                        strLink = sArutiSSURL & "/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString & "|" & iEvaluationTypeId.ToString))
                        'S.SANDEEP |13-NOV-2020| -- END

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                        strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                        'S.SANDEEP |25-MAR-2019| -- END

                        strMessage &= "<BR><BR>"

                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 130, "Regards,")

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 132, "Regards,")
                        strMessage &= Language.getMessage(mstrModuleName, 132, "Regards,")
                        'S.SANDEEP |25-MAR-2019| -- END

                        'Gajanan [26-Feb-2019] -- End
                        strMessage &= "<BR>"
                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString()
                        strMessage &= info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString()
                        'S.SANDEEP |25-MAR-2019| -- END


                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & (objEmp._Firstname & " " & objEmp._Surname).TitleCase()
                        'Gajanan [26-Feb-2019] -- End

                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"
                        objMail._Message = strMessage
                        objMail._ToEmail = dtRow.Item("Email")
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        'If mstrWebFrmName.Trim.Length > 0 Then
                        '    objMail._FormName = mstrWebFrmName
                        'End If
                        With objMail
                            ._FormName = mstrFormName
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        objMail.SendMail(iCompanyId)
                    Next

                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    Dim strMsg As String = ""

                    'Gajanan [26-Feb-2019] -- Start
                    'Enhancement - Email Language Changes For NMB.
                    'objMail._Subject = Language.getMessage(mstrModuleName, 128, "Notification for Schedule a Meeting.")
                    objMail._Subject = Language.getMessage(mstrModuleName, 136, "Notification for one on one performance discussion.")
                    'Gajanan [26-Feb-2019] -- End

                    strMsg = "<HTML> <BODY>"

                    'Gajanan [26-Feb-2019] -- Start
                    'Enhancement - Email Language Changes For NMB.
                    'strMsg &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & objEmp._Firstname & " " & objEmp._Surname.ToString() & ", <BR><BR>"
                    strMsg &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & ", <BR><BR>"
                    'Gajanan [26-Feb-2019] -- End


                    'Gajanan [26-Feb-2019] -- Start
                    'Enhancement - Email Language Changes For NMB.



                    'strMsg &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 129, "This is to remind you that you need to schedule a meeting meeting your line manager(assessor) assinged to you," & _
                    '                                                                       "In order to perform the assessor evaluation.") & "&nbsp;&nbsp;"
                    'strMsg &= "<B>" & objPeriod._Period_Name & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "<B>&nbsp;" & sYearName & "</B>.&nbsp;"

                    'S.SANDEEP |25-MAR-2019| -- START
                    'strMsg &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 137, "Please note that, your self-performance assessment for the period ")
                    'strMsg &= "<B>" & objPeriod._Period_Name & "</B>" & " " & Language.getMessage(mstrModuleName, 138, "has been submitted to your line manager.")

                    strMsg &= Language.getMessage(mstrModuleName, 137, "Please note that, your self-performance assessment for the period") & "&nbsp;<B>("

                    'S.SANDEEP |30-MAR-2019| -- START
                    'strMsg &= objPeriod._Period_Name & ")</B>&nbsp;" & Language.getMessage(mstrModuleName, 138, "has been submitted to your line manager.")
                    'S.SANDEEP |2-APRIL-2019| -- START
                    'strMsg &= objPeriod._Period_Name & ")</B>." & Language.getMessage(mstrModuleName, 138, "has been submitted to your line manager.")
                    strMsg &= objPeriod._Period_Name & ")</B>&nbsp;" & Language.getMessage(mstrModuleName, 138, "has been submitted to your line manager.")
                    'S.SANDEEP |2-APRIL-2019| -- END

                    'S.SANDEEP |30-MAR-2019| -- END
                    'S.SANDEEP |25-MAR-2019| -- END
                    

                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 59}
                    'strMsg &= "<BR><BR>" & Language.getMessage(mstrModuleName, 143, "has been submitted to your line manager.")
                    'S.SANDEEP |12-MAR-2019| -- END

                    'Gajanan [26-Feb-2019] -- End

                    'S.SANDEEP |14-MAR-2019| -- START
                    strMsg &= "<BR><BR>" & Language.getMessage(mstrModuleName, 150, "Furthermore, be reminded to book a one on one performance discussion with your line manager.")
                    'S.SANDEEP |14-MAR-2019| -- END


                    'S.SANDEEP |25-MAR-2019| -- START
                    strMsg &= "<BR><BR>"
                    strMsg &= Language.getMessage(mstrModuleName, 132, "Regards,")
                    'S.SANDEEP |25-MAR-2019| -- END

                    strMsg &= ""
                    strMsg &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMsg &= "</BODY></HTML>"
                    objMail._Message = strMsg
                    objMail._ToEmail = objEmp._Email
                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP

                    'If mstrFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrFormName
                    'End If

                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = mintUserunkid
                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                    objMail.SendMail(iCompanyId)
                    'S.SANDEEP |12-FEB-2019| -- END

                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If dsList Is Nothing Then Exit Select 'Shani(01-MAR-2016)
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        If dtRow.Item("Email") = "" Then Continue For
                        Dim strMessage As String = ""
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
                        strMessage = "<HTML> <BODY>"

                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.
                        strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & info1.ToTitleCase(dtRow.Item("Ename").ToString().ToLower()) & ", <BR><BR>"
                        'strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & dtRow.Item("Ename").ToString().TitleCase & ", <BR><BR>"

                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 12, "This is to inform you that, the Performance Assessment for Employee")

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 139, "Please note that, performance assessment for")
                        strMessage &= Language.getMessage(mstrModuleName, 139, "Please note that, performance assessment for")
                        'S.SANDEEP |25-MAR-2019| -- END


                        'strMessage &= "&nbsp;<B>" & objEmp._Firstname & " " & objEmp._Surname & "&nbsp;</B>" & Language.getMessage(mstrModuleName, 13, "for the period of ") & "&nbsp;<B>" & objPeriod._Period_Name & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "<B>&nbsp;" & sYearName & "</B>&nbsp;" & _

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "&nbsp;<B>" & objEmp._Firstname & " " & objEmp._Surname & "&nbsp;</B>" & Language.getMessage(mstrModuleName, 140, "for period of ") & "&nbsp;<B>" & objPeriod._Period_Name & "</B>&nbsp;"

                        'S.SANDEEP |30-MAR-2019| -- START


                        'strMessage &= "&nbsp;" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()) & "&nbsp;" & Language.getMessage(mstrModuleName, 160, "for the period of") & "&nbsp;<B>(" & objPeriod._Period_Name & ")</B>&nbsp;"
                        strMessage &= "&nbsp;" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()) & "&nbsp;" & Language.getMessage(mstrModuleName, 160, "for the period of") & "&nbsp;<B>(" & objPeriod._Period_Name & ")</B>."
                        'S.SANDEEP |30-MAR-2019| -- END
                        'S.SANDEEP |25-MAR-2019| -- END


                        'Language.getMessage(mstrModuleName, 14, "is complete. Please click the link below to review it.")
                        strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 141, "is complete.")
                        strMessage &= "&nbsp;" & Language.getMessage(mstrModuleName, 142, "Please click the link below to review it.")

                        'Gajanan [26-Feb-2019] -- End   
                        strMessage &= "<BR><BR>"

                        'S.SANDEEP |13-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                        'strLink = sArutiSSURL & "/Assessment New/Performance Evaluation/wPgReviewerEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString))
                        strLink = sArutiSSURL & "/Assessment New/Performance Evaluation/wPgReviewerEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString & "|" & iEvaluationTypeId.ToString))
                        'S.SANDEEP |13-NOV-2020| -- END

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "<BR></BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                        strMessage &= "<BR><BR><a href='" & strLink & "'>" & strLink & "</a>"
                        'S.SANDEEP |25-MAR-2019| -- END
                        strMessage &= "<BR><BR>"

                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 130, "Regards,")

                        'S.SANDEEP |25-MAR-2019| -- START
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 132, "Regards,")
                        'strMessage &= "<BR>"
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & info1.ToTitleCase(sName.ToLower())
                        strMessage &= Language.getMessage(mstrModuleName, 132, "Regards,")
                        strMessage &= "<BR>"
                        strMessage &= info1.ToTitleCase(sName.ToLower())
                        'S.SANDEEP |25-MAR-2019| -- END

                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & sName.TitleCase()

                        'Gajanan [26-Feb-2019] -- End

                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"
                        objMail._Message = strMessage
                        objMail._ToEmail = dtRow.Item("Email")
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        'If mstrWebFrmName.Trim.Length > 0 Then
                        '    objMail._FormName = mstrWebFrmName
                        'End If
                        With objMail
                            ._FormName = mstrFormName
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
                            ._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        objMail.SendMail(iCompanyId)
                    Next
            End Select
            Dim sMsg As String = ""
            If isReviewerMandatory = True Then
                Select Case eAssessMode
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
                        'S.SANDEEP |13-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                        'sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, sArutiSSURL, iCompanyId)
                        sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, sArutiSSURL, iCompanyId, iEvaluationTypeId)
                        'S.SANDEEP |13-NOV-2020| -- END
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
                        objMail._Message = sMsg
                        objMail._ToEmail = objEmp._Email
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        'If mstrWebFrmName.Trim.Length > 0 Then
                        '    objMail._FormName = mstrWebFrmName
                        'End If
                        With objMail
                            ._FormName = mstrFormName
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        objMail.SendMail(iCompanyId)
                End Select
            Else
                Select Case eAssessMode
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
                        'S.SANDEEP |13-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                        'sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, sArutiSSURL, iCompanyId)
                        sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, sArutiSSURL, iCompanyId, iEvaluationTypeId)
                        'S.SANDEEP |13-NOV-2020| -- END
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
                        objMail._Message = sMsg
                        objMail._ToEmail = objEmp._Email
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        'If mstrWebFrmName.Trim.Length > 0 Then
                        '    objMail._FormName = mstrWebFrmName
                        'End If
                        objMail._LoginEmployeeunkid = mintLoginEmployeeunkid
                        With objMail
                            ._FormName = mstrFormName
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        objMail.SendMail(iCompanyId)
                End Select
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Email_Notification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Notify_Employee(ByVal eName As String, _
                                     ByVal ePeriodName As String, _
                                     ByVal eYearName As String, _
                                     ByVal eARName As String, _
                                     ByVal iEmpId As Integer, _
                                     ByVal iPeriodId As Integer, _
                                     ByVal iArutiURL As String, _
                                     ByVal iCompanyId As Integer, _
                                     ByVal iEvaluationTypeId As Integer) As String 'S.SANDEEP |13-NOV-2020| -- START {iEvaluationTypeId} -- END
        Dim strMessage As String = ""
        Try

            'Gajanan [26-Feb-2019] -- Start
            'Enhancement - Email Language Changes For NMB.
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
            'Gajanan [26-Feb-2019] -- End

            Dim strLink As String = String.Empty
            strMessage = "<HTML> <BODY>"

            'Gajanan [26-Feb-2019] -- Start
            'Enhancement - Email Language Changes For NMB.
            strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & info1.ToTitleCase(eName.ToLower()) & ", <BR><BR>"
            'strMessage &= Language.getMessage(mstrModuleName, 117, "Dear") & " " & eName.TitleCase() & ", <BR><BR>"

            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 15, "This is to inform you that, Your Performance Assessment for the Period of")

            'S.SANDEEP |25-MAR-2019| -- START
            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 144, "Please note that, performance assessment for the Period")
            strMessage &= Language.getMessage(mstrModuleName, 144, "Please note that, performance assessment for the Period") & "&nbsp;"
            'S.SANDEEP |25-MAR-2019| -- END


            'strMessage &= "&nbsp;<B>" & ePeriodName & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 8, "and Year") & "&nbsp;<B>" & eYearName & "</B>&nbsp;" & _

            'S.SANDEEP |25-MAR-2019| -- START
            'strMessage &= " " & "<B>" & ePeriodName & "</B>" & " " & Language.getMessage(mstrModuleName, 145, "is complete.")
            strMessage &= " " & "<B>(" & ePeriodName & ")</B>" & "&nbsp;" & Language.getMessage(mstrModuleName, 145, "is complete.")
            'S.SANDEEP |25-MAR-2019| -- END

            'Language.getMessage(mstrModuleName, 16, "is complete. Please login to Aruti Employee Self Service to view it in detail or click the link below for a summary report.")

            strMessage &= "<BR><BR>" & Language.getMessage(mstrModuleName, 146, "Login to Aruti Employee Self Service to view it in detail or click the link below for a summary report.")
            strMessage &= "<BR><BR>"
            'Gajanan [26-Feb-2019] -- End

            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            'strLink = iArutiURL & "/Assessment New/Performance Evaluation/wPg_ViewPerfEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & iEmpId.ToString & "|" & iPeriodId.ToString))
            strLink = iArutiURL & "/Assessment New/Performance Evaluation/wPg_ViewPerfEvaluation.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & iEmpId.ToString & "|" & iPeriodId.ToString & "|" & iEvaluationTypeId.ToString))
            'S.SANDEEP |13-NOV-2020| -- END

            'S.SANDEEP |25-MAR-2019| -- START
            'strMessage &= "<BR></BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
            'strMessage &= "<BR><BR>"
            ''Gajanan [26-Feb-2019] -- Start
            ''Enhancement - Email Language Changes For NMB.
            ''strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 130, "Regards,")
            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 132, "Regards,")
            ''Gajanan [26-Feb-2019] -- End
            'strMessage &= "<BR>"
            ''Gajanan [26-Feb-2019] -- Start
            ''Enhancement - Email Language Changes For NMB.
            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & info1.ToTitleCase(eARName.ToLower())
            ''strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & eARName.TitleCase()
            ''Gajanan [26-Feb-2019] -- End

            strMessage &= "<BR><BR><a href='" & strLink & "'>" & strLink & "</a>"
            strMessage &= "<BR><BR>"
            strMessage &= Language.getMessage(mstrModuleName, 132, "Regards,")
            strMessage &= "<BR>"
            strMessage &= info1.ToTitleCase(eARName.ToLower())

            'S.SANDEEP |25-MAR-2019| -- END



            strMessage &= "<BR><BR>"
            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            Return strMessage

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Notify_Employee; Module Name: " & mstrModuleName)
            Return ""
        Finally
        End Try
    End Function
    'S.SANDEEP |18-FEB-2019| -- END


    'Shani [ 10 DEC 2014 ] -- START
    Public Function GetCustom_Items_Data(ByVal iPeriodId As Integer, ByVal iEmployeeId As Integer, ByVal iHeaderId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtFinal As DataTable = Nothing
        Dim exForce As Exception
        Dim dCol As DataColumn
        Dim objDataOperation As New clsDataOperation
        Try
            mdtFinal = New DataTable("CTList")

            Dim objCustomItems As New clsassess_custom_items
            dsList = objCustomItems.getComboList(iPeriodId, iHeaderId, False, "List")
            objCustomItems = Nothing

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each drow As DataRow In dsList.Tables(0).Rows
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString
                        .Caption = drow.Item("Name").ToString
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                        .Caption = ""
                        .DataType = System.Type.GetType("System.Int32")
                        .DefaultValue = -1
                    End With
                    mdtFinal.Columns.Add(dCol)
                Next
            Else
                Exit Try
            End If

            dCol = New DataColumn
            With dCol
                .ColumnName = "periodunkid"
                .Caption = ""
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = -1
            End With
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GUID"
                .Caption = ""
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtFinal.Columns.Add(dCol)

            StrQ = "SELECT " & _
                   "     custom_item " & _
                   "    ,custom_value " & _
                   "    ,customanalysistranguid " & _
                   "    ,customanalysistranunkid " & _
                   "    ,periodunkid " & _
                   "    ,customitemunkid " & _
                   "    ,selectionmodeid " & _
                   "    ,itemtypeid " & _
                   "    ,assessmodeid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         CASE WHEN hrevaluation_analysis_master.assessmodeid=1 THEN @SelfAssessment " & _
                   "              WHEN hrevaluation_analysis_master.assessmodeid=2 THEN @AssessorAssessment " & _
                   "              WHEN hrevaluation_analysis_master.assessmodeid=3 THEN @ReviewerAssessment " & _
                   "         ELSE '' END + ' - (' + (hremployee_master.employeecode+' - '+hremployee_master.firstname+' '+hremployee_master.surname)+')' AS custom_item " & _
                   "        ,'' AS custom_value " & _
                   "        ,'' AS customanalysistranguid " & _
                   "        ,'' AS customanalysistranunkid " & _
                   "        ,-1 AS periodunkid " & _
                   "        ,-1 AS customitemunkid " & _
                   "        ,-1 AS selectionmodeid " & _
                   "        ,-1 AS itemtypeid " & _
                   "        ,hrevaluation_analysis_master.assessmodeid " & _
                   "        ,-1 AS Rno " & _
                   "    FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                   "        INNER JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid=hrevaluation_analysis_master.assessoremployeeunkid OR hremployee_master.employeeunkid=hrevaluation_analysis_master.selfemployeeunkid OR hremployee_master.employeeunkid=hrevaluation_analysis_master.reviewerunkid " & _
                   "    WHERE hrevaluation_analysis_master.isvoid = 0 AND (hrevaluation_analysis_master.selfemployeeunkid=" & iEmployeeId & " OR hrevaluation_analysis_master.assessedemployeeunkid=" & iEmployeeId & " ) " & _
                   "UNION " & _
                   "    SELECT " & _
                   "        hrassess_custom_items.custom_item " & _
                   "        ,hrcompetency_customitem_tran.custom_value " & _
                   "        ,customanalysistranguid AS customanalysistranguid " & _
                   "        ,hrcompetency_customitem_tran.customanalysistranunkid " & _
                   "        ,hrcompetency_customitem_tran.periodunkid " & _
                   "        ,hrcompetency_customitem_tran.customitemunkid " & _
                   "        ,hrassess_custom_items.selectionmodeid " & _
                   "        ,hrassess_custom_items.itemtypeid " & _
                   "        ,hrevaluation_analysis_master.assessmodeid " & _
                   "        ,ROW_NUMBER() OVER(Partition BY customanalysistranguid ORDER BY customanalysistranguid ) AS Rno " & _
                   "    FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                   "        INNER JOIN hrcompetency_customitem_tran WITH (NOLOCK) ON hrevaluation_analysis_master.analysisunkid=hrcompetency_customitem_tran.analysisunkid " & _
                   "        INNER JOIN hrassess_custom_items WITH (NOLOCK) ON hrassess_custom_items.customitemunkid=hrcompetency_customitem_tran.customitemunkid " & _
                   "        INNER JOIN hrassess_custom_headers WITH (NOLOCK) ON hrassess_custom_headers.customheaderunkid=hrassess_custom_items.customheaderunkid " & _
                   "        INNER JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid=hrevaluation_analysis_master.assessoremployeeunkid OR hremployee_master.employeeunkid=hrevaluation_analysis_master.selfemployeeunkid OR hremployee_master.employeeunkid=hrevaluation_analysis_master.reviewerunkid " & _
                   "    WHERE hrevaluation_analysis_master.isvoid=0 " & _
                   "        AND (hrevaluation_analysis_master.selfemployeeunkid=" & iEmployeeId & " OR hrevaluation_analysis_master.assessedemployeeunkid=" & iEmployeeId & " ) " & _
                   "        AND hrevaluation_analysis_master.periodunkid=" & iPeriodId & " AND hrassess_custom_items.customheaderunkid=" & iHeaderId & " " & _
                   ") AS LvList ORDER BY LvList.assessmodeid,LvList.customanalysistranguid,LvList.Rno "

            objDataOperation.AddParameter("@SelfAssessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 119, "Self Assessment"))
            objDataOperation.AddParameter("@AssessorAssessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 120, "Assessor Assessment"))
            objDataOperation.AddParameter("@ReviewerAssessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 121, "Reviewer Assessment"))
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dFRow As DataRow = Nothing
                Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                    iNewCustomId = dRow.Item("customitemunkid")
                    If iCustomId = iNewCustomId Or iNewCustomId = -1 Then
                        dFRow = mdtFinal.NewRow
                        mdtFinal.Rows.Add(dFRow)
                    End If
                    If dRow.Item("customanalysistranguid").ToString().Trim.Length <= 0 Then
                        Dim iColIndex As Integer = (From p In mdtFinal.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("CItem_")) Select (p.Ordinal)).ToList.First()
                        dFRow.Item(iColIndex) = dRow.Item("custom_item")
                    Else
                        Select Case CInt(dRow.Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                            Case clsassess_custom_items.enCustomType.SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    Select Case CInt(dRow.Item("selectionmodeid"))
                                        'S.SANDEEP [04 OCT 2016] -- START
                                        'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            'S.SANDEEP [04 OCT 2016] -- END
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            Dim objCMaster As New clsassess_competencies_master
                                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            Dim objEmpField1 As New clsassess_empfield1_master
                                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                            objEmpField1 = Nothing

                                            'S.SANDEEP |16-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                            'S.SANDEEP |16-AUG-2019| -- END
                                    End Select
                                End If
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dRow.Item("custom_value")) Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                    End If
                                End If
                        End Select
                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                        dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                        dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                    End If
                Next
            Else
                mdtFinal.Rows.Add(mdtFinal.NewRow)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCustom_Items_Data; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function
    'Shani [ 10 DEC 2014 ] -- END


    'S.SANDEEP [21 JAN 2015] -- START

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Compute_Score(ByVal xAssessMode As enAssessmentMode, _
                                  ByVal IsBalanceScoreCard As Boolean, _
                                  ByVal xScoreOptId As Integer, _
                                  ByVal xCompute_Formula As enAssess_Computation_Formulas, _
                                  ByVal xEmployeeAsOnDate As DateTime, _
                                  ByVal xEmployeeId As Integer, _
                                  ByVal xPeriodId As Integer, _
                                  ByVal xUsedAgreedScore As Boolean, _
                                  Optional ByVal xAssessGrpId As Integer = 0, _
                                  Optional ByVal xDataTable As DataTable = Nothing, _
                                  Optional ByVal xAssessorReviewerId As Integer = 0, _
                                  Optional ByVal xOnlyCommited As Boolean = False, _
                                  Optional ByVal xSelfAssignedCompetencies As Boolean = False) As Decimal

        'Shani (23-Nov-2016) -- [ByVal xUsedAgreedScore As Boolean]


        'S.SANDEEP [08 Jan 2016] -- START {xSelfAssignedCompetencies} -- END


        'Public Function Compute_Score(ByVal xAssessMode As enAssessmentMode, _
        '                          ByVal IsBalanceScoreCard As Boolean, _
        '                          ByVal xScoreOptId As Integer, _
        '                          ByVal xCompute_Formula As enAssess_Computation_Formulas, _
        '                          Optional ByVal xEmployeeId As Integer = 0, _
        '                          Optional ByVal xPeriodId As Integer = 0, _
        '                          Optional ByVal xAssessGrpId As Integer = 0, _
        '                          Optional ByVal xDataTable As DataTable = Nothing, _
        '                          Optional ByVal xAssessorReviewerId As Integer = 0, _
        '                          Optional ByVal xOnlyCommited As Boolean = False) As Decimal

        'S.SANDEEP [04 JUN 2015] -- END

        Dim xScore, xTotalScore As Decimal
        Dim StrQ As String = String.Empty
        Dim xTable As DataTable = Nothing
        Dim xFormulaRef As String = String.Empty
        Dim objCompute As New clsassess_computation_master
        Dim xList As New DataSet
        Dim mDecRoundingFactor As Decimal = 0 'S.SANDEEP |24-DEC-2019| -- START -- END
        Try
            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Using objDo As New clsDataOperation
                StrQ = "SELECT TOP 1 " & _
                       "    @val = CAST(c.key_value AS DECIMAL(36,4)) " & _
                       "FROM cfcommon_period_tran cpt WITH (NOLOCK) " & _
                       "    JOIN hrmsConfiguration..cffinancial_year_tran cyt ON cpt.yearunkid = cyt.yearunkid " & _
                       "    JOIN hrmsConfiguration..cfconfiguration c ON cyt.companyunkid = c.companyunkid " & _
                       "WHERE cpt.modulerefid = 5 AND cpt.periodunkid = '" & xPeriodId & "' AND UPPER(c.[key_name]) = 'PASCORINGROUDINGFACTOR' "

                objDo.ClearParameters()
                objDo.AddParameter("@val", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecRoundingFactor, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
                Try
                    mDecRoundingFactor = objDo.GetParameterValue("@val")
                Catch ex As Exception
                    mDecRoundingFactor = 0
                End Try
            End Using
            'S.SANDEEP |24-DEC-2019| -- END

            xScore = 0 : xTotalScore = 0
            If xDataTable IsNot Nothing Then
                If xDataTable.Columns.Contains("IsGrp") Then
                    xTable = New DataView(xDataTable, "IsGrp = false", "", DataViewRowState.CurrentRows).ToTable.Copy
                Else
                    xTable = xDataTable.Copy
                End If
            End If
            If xTable Is Nothing Then
                If IsBalanceScoreCard = True Then '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' BSC COMPUTATIONS
                    StrQ = "SELECT " & _
                           "  EM.periodunkid AS iPeriodId " & _
                           " ,CASE WHEN fieldunkid = 1 THEN GT.empfield1unkid " & _
                           "       WHEN fieldunkid = 2 THEN GT.empfield2unkid " & _
                           "       WHEN fieldunkid = 3 THEN GT.empfield3unkid " & _
                           "       WHEN fieldunkid = 4 THEN GT.empfield4unkid " & _
                           "       WHEN fieldunkid = 5 THEN GT.empfield5unkid " & _
                           "  END AS iItemUnkid "
                    Select Case xAssessMode
                        Case enAssessmentMode.SELF_ASSESSMENT
                            StrQ &= " ,EM.selfemployeeunkid AS iEmployeeId "
                        Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                            StrQ &= " ,EM.assessedemployeeunkid AS iEmployeeId "
                    End Select

                    'S.SANDEEP [23 MAR 2015] -- START
                    'StrQ &= " ,GT.result AS iScore " & _
                    '        "FROM hrgoals_analysis_tran AS GT " & _
                    '        " JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = GT.analysisunkid " & _
                    '        " JOIN hrassess_field_mapping ON EM.periodunkid = EM.periodunkid " & _
                    '        "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.assessmodeid = '" & xAssessMode & "' "

                    StrQ &= " ,CAST(GT.result AS DECIMAL(36,2)) AS iScore " & _
                            " ,CAST(ISNULL(GT.agreedscore,0) AS DECIMAL(36,2)) AS iAgreedScore " & _
                            "FROM hrgoals_analysis_tran AS GT WITH (NOLOCK) " & _
                            " JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                            " JOIN hrassess_field_mapping WITH (NOLOCK) ON EM.periodunkid = hrassess_field_mapping.periodunkid " & _
                            "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.assessmodeid = '" & xAssessMode & "' "

                    'S.SANDEEP |21-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                    'REMOVED ------------- GT.result AS iScore
                    'REMOVED ------------- ISNULL(GT.agreedscore,0) AS iAgreedScore
                    'ADDED --------------- CAST(GT.result AS DECIMAL(36,2)) AS iScore
                    'ADDED --------------- CAST(ISNULL(GT.agreedscore,0) AS DECIMAL(36,2)) AS iAgreedScore
                    'S.SANDEEP |21-AUG-2019| -- END

                    'S.SANDEEP [29 APR 2015] -- START
                    StrQ &= "AND hrassess_field_mapping.isactive = 1 "
                    'S.SANDEEP [29 APR 2015] -- END

                    'S.SANDEEP [23 MAR 2015] -- END

                    If xEmployeeId > 0 Then
                        Select Case xAssessMode
                            Case enAssessmentMode.SELF_ASSESSMENT
                                StrQ &= " AND EM.selfemployeeunkid = '" & xEmployeeId & "'"
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                StrQ &= " AND EM.assessedemployeeunkid = '" & xEmployeeId & "'"
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ &= " AND EM.assessedemployeeunkid = '" & xEmployeeId & "'"
                        End Select
                    End If
                    If xPeriodId > 0 Then
                        StrQ &= " AND EM.periodunkid = '" & xPeriodId & "'"
                    End If

                    'S.SANDEEP [19 FEB 2015] -- START
                    If xOnlyCommited = True Then
                        StrQ &= " AND EM.iscommitted = 1 "
                    End If
                    'S.SANDEEP [19 FEB 2015] -- END


                    If xAssessorReviewerId > 0 Then
                        Select Case xAssessMode
                            Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ &= " AND EM.assessormasterunkid = '" & xAssessorReviewerId & "' "
                        End Select
                    End If

                    Using objDo As New clsDataOperation
                        Dim dsList As New DataSet
                        dsList = objDo.ExecQuery(StrQ, "List")
                        If objDo.ErrorMessage <> "" Then
                            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        End If
                        xTable = dsList.Tables(0).Copy
                    End Using
                Else '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' COMPETENCY COMPUTATIONS
                    StrQ = "SELECT " & _
                           "  EM.periodunkid AS iPeriodId " & _
                           " ,CT.competenciesunkid AS iItemUnkid "
                    Select Case xAssessMode
                        Case enAssessmentMode.SELF_ASSESSMENT
                            StrQ &= " ,EM.selfemployeeunkid AS iEmployeeId "
                        Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                            StrQ &= " ,EM.assessedemployeeunkid AS iEmployeeId "
                    End Select
                    StrQ &= " ,CAST(CT.result AS DECIMAL(36,2)) AS iScore " & _
                            " ,CT.assessgroupunkid " & _
                            " ,CAST(ISNULL(CT.agreedscore,0) AS DECIMAL(36,2)) AS iAgreedScore " & _
                            "FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                            " JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = CT.analysisunkid " & _
                            "WHERE EM.isvoid = 0 AND CT.isvoid = 0 AND EM.assessmodeid = '" & xAssessMode & "' "

                    'S.SANDEEP |21-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                    'REMOVED ------------- CT.result AS iScore
                    'REMOVED ------------- ISNULL(CT.agreedscore,0) AS iAgreedScore
                    'ADDED --------------- CAST(CT.result AS DECIMAL(36,2)) AS iScore
                    'ADDED --------------- CAST(ISNULL(CT.agreedscore,0) AS DECIMAL(36,2)) AS iAgreedScore
                    'S.SANDEEP |21-AUG-2019| -- END

                    If xEmployeeId > 0 Then
                        Select Case xAssessMode
                            Case enAssessmentMode.SELF_ASSESSMENT
                                StrQ &= " AND EM.selfemployeeunkid = '" & xEmployeeId & "' "
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                StrQ &= " AND EM.assessedemployeeunkid = '" & xEmployeeId & "' "
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ &= " AND EM.assessedemployeeunkid = '" & xEmployeeId & "' "
                        End Select
                    End If
                    If xPeriodId > 0 Then
                        StrQ &= " AND EM.periodunkid = '" & xPeriodId & "' "
                    End If
                    If xAssessGrpId > 0 Then
                        StrQ &= " AND CT.assessgroupunkid = '" & xAssessGrpId & "' "
                    End If

                    'S.SANDEEP [19 FEB 2015] -- START
                    If xOnlyCommited = True Then
                        StrQ &= " AND EM.iscommitted = 1 "
                    End If
                    'S.SANDEEP [19 FEB 2015] -- END

                    Using objDo As New clsDataOperation
                        Dim dsList As New DataSet
                        dsList = objDo.ExecQuery(StrQ, "List")
                        If objDo.ErrorMessage <> "" Then
                            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        End If
                        xTable = dsList.Tables(0).Copy
                    End Using
                End If
            End If

            If xTable IsNot Nothing AndAlso xTable.Rows.Count > 0 Then

                Dim blnSummary As Boolean = False

                Dim objACM As New clsassess_computation_master
                Dim dtran As DataTable = Nothing
                dtran = objACM.Get_Computation_TranVariables(xPeriodId, xCompute_Formula)
                objACM = Nothing

                Dim xStringMatcingIn As String = enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                                 enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO

                Dim xView As DataView = New DataView(dtran, "computation_typeid IN(" & xStringMatcingIn & ")", "", DataViewRowState.CurrentRows)
                If xView.ToTable.Rows.Count > 0 Then
                    blnSummary = True
                End If
                If blnSummary = True Then
                    For Each xRow As DataRow In xTable.Rows
                        If IsBalanceScoreCard = True Then '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' BSC COMPUTATIONS

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, 0, xAssessMode, xFormulaRef, , xDataTable)

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, 0, xAssessMode, xEmployeeAsOnDate, xFormulaRef, , xDataTable, xSelfAssignedCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{xSelfAssignedCompetencies} -- END
                            xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, 0, xAssessMode, xEmployeeAsOnDate, xUsedAgreedScore, xFormulaRef, , xDataTable, xSelfAssignedCompetencies)
                            'Shani (23-Nov123-2016-2016) -- End


                            'S.SANDEEP [04 JUN 2015] -- END

                        Else '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' COMPETENCY COMPUTATIONS

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, 0, xAssessMode, xFormulaRef, xRow("assessgroupunkid"), xDataTable)

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, 0, xAssessMode, xEmployeeAsOnDate, xFormulaRef, xRow("assessgroupunkid"), xDataTable, xSelfAssignedCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{xSelfAssignedCompetencies} -- END
                            xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, 0, xAssessMode, xEmployeeAsOnDate, xUsedAgreedScore, xFormulaRef, xRow("assessgroupunkid"), xDataTable, xSelfAssignedCompetencies)
                            'Shani (23-Nov123-2016-2016) -- End


                            'S.SANDEEP [04 JUN 2015] -- END

                        End If
                        xTotalScore = xTotalScore + xScore
                        Exit For
                    Next
                Else
                    If IsBalanceScoreCard = True Then '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' BSC COMPUTATIONS
                        For Each xRow As DataRow In xTable.Rows
                            If xTable.Columns.Contains("AUD") = True Then
                                If xRow.Item("AUD") = "D" Then Continue For
                            End If
                            Dim xColName As String = "iScore"

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            If xAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT AndAlso xUsedAgreedScore Then
                                xColName = "iAgreedScore"
                            End If
                            'Shani (23-Nov-2016) -- End

                            If xDataTable IsNot Nothing Then
                                Select Case xAssessMode
                                    Case enAssessmentMode.SELF_ASSESSMENT
                                        If xTable.Columns.Contains("escore") = True Then
                                            xColName = "escore"
                                        End If
                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                        'Shani (23-Nov-2016) -- Start
                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                        'If xTable.Columns.Contains("ascore") = True Then
                                        '    xColName = "ascore"
                                        'End If
                                        If xUsedAgreedScore = False Then
                                            If xTable.Columns.Contains("ascore") = True Then
                                                xColName = "ascore"
                                            End If
                                        Else
                                            If xTable.Columns.Contains("agreed_score") = True Then
                                                xColName = "agreed_score"
                                            End If
                                        End If
                                        'Shani (23-Nov-2016) -- End
                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                        If xTable.Columns.Contains("rscore") = True Then
                                            xColName = "rscore"
                                        End If
                                End Select
                            End If

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xFormulaRef)

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xEmployeeAsOnDate, xFormulaRef, , , xSelfAssignedCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{xSelfAssignedCompetencies} -- END
                            xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xEmployeeAsOnDate, xUsedAgreedScore, xFormulaRef, , , xSelfAssignedCompetencies)
                            'Shani (23-Nov123-2016-2016) -- End


                            'S.SANDEEP [04 JUN 2015] -- END

                            xTotalScore = xTotalScore + xScore
                        Next
                    Else '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' COMPETENCY COMPUTATIONS
                        For Each xRow As DataRow In xTable.Rows
                            If xTable.Columns.Contains("AUD") = True Then
                                If xRow.Item("AUD") = "D" Then Continue For
                            End If
                            Dim xColName As String = "iScore"
                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            If xAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT AndAlso xUsedAgreedScore Then
                                xColName = "iAgreedScore"
                            End If
                            'Shani (23-Nov-2016) -- End
                            If xDataTable IsNot Nothing Then
                                Select Case xAssessMode
                                    Case enAssessmentMode.SELF_ASSESSMENT
                                        If xTable.Columns.Contains("escore") = True Then
                                            xColName = "escore"
                                        End If
                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                        'Shani (23-Nov-2016) -- Start
                                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...

                                        'If xTable.Columns.Contains("ascore") = True Then
                                        '    xColName = "ascore"
                                        'End If
                                        If xUsedAgreedScore = False Then
                                            If xTable.Columns.Contains("ascore") = True Then
                                                xColName = "ascore"
                                            End If
                                        Else
                                            If xTable.Columns.Contains("agreed_score") = True Then
                                                xColName = "agreed_score"
                                            End If
                                        End If
                                        'Shani (23-Nov-2016) -- End
                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                        If xTable.Columns.Contains("rscore") = True Then
                                            xColName = "rscore"
                                        End If
                                End Select
                            End If

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xFormulaRef, xRow("assessgroupunkid"))

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xEmployeeAsOnDate, xFormulaRef, xRow("assessgroupunkid"), , xSelfAssignedCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{xSelfAssignedCompetencies} -- END
                            xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xEmployeeAsOnDate, xUsedAgreedScore, xFormulaRef, xRow("assessgroupunkid"), , xSelfAssignedCompetencies)
                            'Shani (23-Nov123-2016-2016) -- End


                            'S.SANDEEP [04 JUN 2015] -- END

                            xTotalScore = xTotalScore + xScore
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Compute_Score; Module Name: " & mstrModuleName)
        Finally
            objCompute = Nothing
        End Try
        'S.SANDEEP |21-AUG-2019| -- START
        'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
        'Return xTotalScore

        'S.SANDEEP |24-DEC-2019| -- START
        'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
        'Return CDbl(xTotalScore).ToString("#######################0.#0")
        Return CDbl(Rounding.BRound(xTotalScore, mDecRoundingFactor)).ToString("#######################0.#0")
        'S.SANDEEP |24-DEC-2019| -- END

        'S.SANDEEP |21-AUG-2019| -- END
    End Function
    'S.SANDEEP [21 JAN 2015] -- END

    'S.SANDEEP [04 JUN 2015] -- START
    Public Function GetEvaluationUnkid(ByVal intEmployeeId As Integer, ByVal eMode As enAssessmentMode, ByVal intPeriodId As Integer) As DataSet
        Dim StrQ As String = "" : Dim dsEval As New DataSet
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "   EM.analysisunkid " & _
                       "  ,EM.assessmodeid " & _
                       "FROM hrevaluation_analysis_master EM " & _
                       "WHERE EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND assessmodeid = '" & eMode & "' "
                Select Case eMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= " AND EM.selfemployeeunkid = '" & intEmployeeId & "' "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND EM.assessedemployeeunkid = '" & intEmployeeId & "' "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND EM.assessedemployeeunkid = '" & intEmployeeId & "' "
                End Select

                dsEval = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEvaluationUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsEval
    End Function

    Public Function IsValidAssessmentDate(ByVal eMode As enAssessmentMode, ByVal dtDate As Date, ByVal intPeriod As Integer, ByVal intEmployeeId As Integer) As String
        Dim strMsg As String = "" : Dim StrQ As String = ""
        Try
            Using objDo As New clsDataOperation

                Dim dsEval As New DataSet

                StrQ = "SELECT CONVERT(CHAR(8),assessmentdate,112) AS adate FROM hrevaluation_analysis_master WITH (NOLOCK) WHERE isvoid = 0 " & _
                       " AND periodunkid = '" & intPeriod & "' "

                Select Case eMode
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND hrevaluation_analysis_master.selfemployeeunkid = '" & intEmployeeId & "' "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                End Select

                dsEval = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                End If

                If dsEval.Tables(0).Rows.Count > 0 Then

                    If dtDate < eZeeDate.convertDate(dsEval.Tables(0).Rows(0).Item("adate")) Then
                        Select Case eMode
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                strMsg = Language.getMessage(mstrModuleName, 123, "Sorry, Assessment date should be greater than or equal to [") & eZeeDate.convertDate(dsEval.Tables(0).Rows(0).Item("adate")) & _
                                         Language.getMessage(mstrModuleName, 122, "], Assessment done by employee. Please set date accordingly.")

                            Case enAssessmentMode.REVIEWER_ASSESSMENT

                                strMsg = Language.getMessage(mstrModuleName, 123, "Sorry, Assessment date should be greater than or equal to [") & eZeeDate.convertDate(dsEval.Tables(0).Rows(0).Item("adate")) & _
                                         Language.getMessage(mstrModuleName, 124, "], Assessment done by assessor. Please set date accordingly.")
                        End Select

                    End If

                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidAssessmentDate", mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function

    'S.SANDEEP [04 JUN 2015] -- END


    'S.SANDEEP [18 DEC 2015] -- START
    Public Function GetListNew(ByVal strDatabaseName As String, _
                                   ByVal intUserUnkid As Integer, _
                                   ByVal intYearUnkid As Integer, _
                                   ByVal intCompanyUnkid As Integer, _
                                   ByVal dtPeriodStart As DateTime, _
                                   ByVal dtPeriodEnd As DateTime, _
                                   ByVal strUserModeSetting As String, _
                                   ByVal blnOnlyApproved As Boolean, _
                                   ByVal blnIncludeIn_ActiveEmployee As Boolean, _
                                   ByVal strTableName As String, _
                                   ByVal intPeriodId As Integer, _
                                   ByVal enAssessMode As enAssessmentMode, _
                                   ByVal intAsrRevMasterId As Integer, _
                                   Optional ByVal intEmployeeId As Integer = 0, _
                                   Optional ByVal intYearId As Integer = 0, _
                                   Optional ByVal mdtAssessDate As Date = Nothing, _
                                   Optional ByVal intViewType As Integer = 0, _
                                   Optional ByVal strAdvanceFilter As String = "", _
                                   Optional ByVal eEvalTypeId As enPAEvalTypeId = enPAEvalTypeId.EO_BOTH) As DataTable 'S.SANDEEP |13-NOV-2020| -- START {eEvalTypeId} -- END
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim mdtFinal As New DataTable("List")
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            mdtFinal.Columns.Add("employee", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("assessor", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("reviewer", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("viewmode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("year", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("period", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("assessmentdate", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("sl_score", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("al_score", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("rl_score", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("sl_analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("al_analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("rl_analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = intPeriodId
            mdtFinal.Columns.Add("yearunkid", System.Type.GetType("System.Int32")).DefaultValue = intYearUnkid
            mdtFinal.Columns.Add("statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("assessgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("assessmodeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("smodeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("committeddatetime", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("assessormasterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("assessoremployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("reviewerunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("operationviewid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("onlyempname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("sl_iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("al_iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("rl_iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("sl_committeddatetime", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("al_committeddatetime", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("rl_committeddatetime", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("sl_assessmentdate", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("al_assessmentdate", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("rl_assessmentdate", System.Type.GetType("System.String")).DefaultValue = ""


            Using objDo As New clsDataOperation
                Dim intPeriodStatusId As Integer = -1
                If enAssessMode <> enAssessmentMode.SELF_ASSESSMENT Then
                    'Shani (13-Jan-2017) -- Start
                    'Issue -
                    'StrQ = "SELECT " & _
                    '       "     ISNULL(hremployee_master.employeecode,'')+ ' - ' + " & _
                    '       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                    '       "    ,hrassessor_master.assessormasterunkid " & _
                    '       "    ,hrassessor_tran.employeeunkid " & _
                    '       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As onlyempname " & _
                    '       "FROM hrassessor_master " & _
                    '       "    JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_tran.isvoid = 0 " & _
                    '       "    JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid "

                    'If xDateJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xDateJoinQry
                    'End If

                    'If xAdvanceJoinQry.Trim.Length > 0 Then
                    '    StrQ &= xAdvanceJoinQry
                    'End If

                    'StrQ &= "WHERE hrassessor_master.assessormasterunkid = '" & intAsrRevMasterId & "' AND hrassessor_master.isvoid = 0 AND isreviewer = " & IIf(enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT, 0, 1) & " "

                    'If xDateFilterQry.Trim.Length > 0 Then
                    '    StrQ &= xDateFilterQry
                    'End If

                    'If strAdvanceFilter.Trim.Length > 0 Then
                    '    StrQ &= " AND " & strAdvanceFilter
                    'End If

                    'If intEmployeeId > 0 Then
                    '    StrQ &= " AND hrassessor_tran.employeeunkid = '" & intEmployeeId & "'"
                    'End If

                    StrQ = "SELECT @statusid = statusid FROM cfcommon_period_tran WITH (NOLOCK) WHERE periodunkid = @periodunkid"

                    objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                    objDo.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodStatusId, ParameterDirection.InputOutput)

                    dsList = objDo.ExecQuery(StrQ, "List")

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    intPeriodStatusId = objDo.GetParameterValue("@statusid")

                    If intPeriodStatusId <= 0 Then intPeriodStatusId = CInt(enStatusType.OPEN)

                    If intPeriodStatusId = enStatusType.CLOSE Then
                        StrQ = "SELECT " & _
                               "     ISNULL(hremployee_master.employeecode,'')+ ' - ' + " & _
                               "        ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                               "    ,hrevaluation_analysis_master.assessormasterunkid " & _
                               "    ,hrevaluation_analysis_master.assessedemployeeunkid AS employeeunkid " & _
                               "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As onlyempname " & _
                               "    ,hrevaluation_analysis_master.evaltypeid " & _
                               "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                               "    JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid " & _
                               "WHERE hrevaluation_analysis_master.assessormasterunkid = '" & intAsrRevMasterId & "' " & _
                               "    AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' "
                        'S.SANDEEP |13-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                        If eEvalTypeId <> enPAEvalTypeId.EO_NONE Then
                        If intPeriodStatusId <> enStatusType.CLOSE Then StrQ &= " AND hrevaluation_analysis_master.evaltypeid = " & CInt(eEvalTypeId) & " "
                        End If                        
                        'S.SANDEEP |13-NOV-2020| -- END
                    Else
                        StrQ = "SELECT " & _
                               "     ISNULL(hremployee_master.employeecode,'')+ ' - ' + " & _
                               "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                               "    ,hrassessor_master.assessormasterunkid " & _
                               "    ,hrassessor_tran.employeeunkid " & _
                               "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As onlyempname " & _
                               "    ," & CInt(eEvalTypeId) & " AS evaltypeid " & _
                               "FROM hrassessor_master WITH (NOLOCK) " & _
                               "    JOIN hrassessor_tran WITH (NOLOCK) ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_tran.isvoid = 0 " & _
                               "    JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassessor_tran.employeeunkid "

                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry
                        End If

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry
                        End If

                        StrQ &= "WHERE hrassessor_master.assessormasterunkid = '" & intAsrRevMasterId & "' AND hrassessor_master.isvoid = 0 AND isreviewer = " & IIf(enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT, 0, 1) & " " & _
                                "   AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "

                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ &= xDateFilterQry
                        End If

                        If strAdvanceFilter.Trim.Length > 0 Then
                            StrQ &= " AND " & strAdvanceFilter
                        End If

                        If intEmployeeId > 0 Then
                            StrQ &= " AND hrassessor_tran.employeeunkid = '" & intEmployeeId & "'"
                        End If
                    End If

                    'Shani (13-Jan-2017) -- End

                    StrQ &= " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "

                Else
                    StrQ = "SELECT " & _
                           "     ISNULL(hremployee_master.employeecode,'')+ ' - ' + " & _
                           "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                           "    ,0 AS assessormasterunkid " & _
                           "    ,hremployee_master.employeeunkid " & _
                           "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As onlyempname " & _
                           "    ,ISNULL(hrevaluation_analysis_master.analysisunkid,0) AS analysisunkid " & _
                           "    ,hrevaluation_analysis_master.evaltypeid " & _
                           "FROM hremployee_master WITH (NOLOCK) " & _
                           "    LEFT JOIN hrevaluation_analysis_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid AND isvoid = 0 "

                    'S.SANDEEP |13-NOV-2020| -- START
                    'ISSUE/ENHANCEMENT : COMPETENCIES
                    If eEvalTypeId <> enPAEvalTypeId.EO_NONE Then
                    StrQ &= " AND hrevaluation_analysis_master.evaltypeid = " & CInt(eEvalTypeId) & " "
                    End If                    
                    'S.SANDEEP |13-NOV-2020| -- END

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                   

                    StrQ &= "WHERE 1 = 1 "

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If

                    If strAdvanceFilter.Trim.Length > 0 Then
                        StrQ &= " AND " & strAdvanceFilter
                    End If

                    If intEmployeeId > 0 Then
                        StrQ &= " AND hremployee_master.employeeunkid = '" & intEmployeeId & "'"
                    End If

                    StrQ &= " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "
                End If

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then

                    For Each row As DataRow In dsList.Tables("List").Rows
                        Dim dtRow As DataRow = mdtFinal.NewRow
                        dtRow("viewmode") = row("EmpName")
                        dtRow("employee") = row("EmpName")
                        dtRow("GrpId") = row("employeeunkid")
                        dtRow("onlyempname") = row("onlyempname")
                        dtRow("IsGrp") = True
                        mdtFinal.Rows.Add(dtRow)
                    Next

                    Dim dsTrans As New DataSet

                    'StrQ = "SELECT " & _
                    '         "     hrevaluation_analysis_master.analysisunkid " & _
                    '         "    ,hrevaluation_analysis_master.periodunkid " & _
                    '         "    ,CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) AS assessmentdate " & _
                    '         "    ,ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                    '         "    ,ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                    '         "    ,cfcommon_period_tran.yearunkid " & _
                    '         "    ,ISNULL(iscommitted,0) AS iscommitted " & _
                    '         "    ,cfcommon_period_tran.statusid AS Sid " & _
                    '         "    ,hrevaluation_analysis_master.committeddatetime " & _
                    '         "    ,hrevaluation_analysis_master.assessmodeid " & _
                    '         "    ,PE.rscore " & _
                    '         "    ,PE.smode " & _
                    '         "    ,PE.smodeid " & _
                    '         "    ,PE.assessgroupunkid " & _
                    '         "    ,PE.EmpId " & _
                    '         "    ,assessormasterunkid " & _
                    '         "    ,assessoremployeeunkid " & _
                    '         "    ,reviewerunkid " & _
                    '         "    ,ISNULL(hremployee_master.firstname,'') +' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employee " & _
                    '         "    ,ISNULL(ASR.firstname,'') +' '+ISNULL(ASR.othername,'')+' '+ISNULL(ASR.surname,'') AS assessor " & _
                    '         "    ,ISNULL(REV.firstname,'') +' '+ISNULL(REV.othername,'')+' '+ISNULL(REV.surname,'') AS reviewer " & _
                    '         "    ,hrevaluation_analysis_master.userunkid " & _
                    '         "    ,CASE WHEN assessmodeid = 1 THEN ISNULL(iscommitted,0) ELSE 0 END AS sl_iscommitted " & _
                    '         "    ,CASE WHEN assessmodeid = 2 THEN ISNULL(iscommitted,0) ELSE 0 END AS al_iscommitted " & _
                    '         "    ,CASE WHEN assessmodeid = 3 THEN ISNULL(iscommitted,0) ELSE 0 END AS rl_iscommitted " & _
                    '         "    ,CASE WHEN assessmodeid = 1 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.committeddatetime,112) ELSE '' END AS sl_committeddatetime " & _
                    '         "    ,CASE WHEN assessmodeid = 2 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.committeddatetime,112) ELSE '' END AS al_committeddatetime " & _
                    '         "    ,CASE WHEN assessmodeid = 3 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.committeddatetime,112) ELSE '' END AS rl_committeddatetime " & _
                    '         "    ,CASE WHEN assessmodeid = 1 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.assessmentdate,112) ELSE '' END AS sl_assessmentdate " & _
                    '         "    ,CASE WHEN assessmodeid = 2 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.assessmentdate,112) ELSE '' END AS al_assessmentdate " & _
                    '         "    ,CASE WHEN assessmodeid = 3 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.assessmentdate,112) ELSE '' END AS rl_assessmentdate " & _
                    '         "FROM hrevaluation_analysis_master " & _
                    '         "    LEFT JOIN hremployee_master AS ASR ON ASR.employeeunkid = hrevaluation_analysis_master.assessoremployeeunkid " & _
                    '         "    LEFT JOIN hremployee_master AS REV ON REV.employeeunkid = hrevaluation_analysis_master.reviewerunkid " & _
                    '         "    JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrevaluation_analysis_master.periodunkid " & _
                    '         "    JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
                    '         "    JOIN " & _
                    '         "    ( " & _
                    '         "        SELECT " & _
                    '         "             SCR.banalysisunkid " & _
                    '         "            ,SCR.rscore " & _
                    '         "            ,SCR.smode " & _
                    '         "            ,SCR.smodeid " & _
                    '         "            ,SCR.assessgroupunkid " & _
                    '         "            ,SCR.EmpId " & _
                    '         "        FROM " & _
                    '         "        ( " & _
                    '         "            SELECT DISTINCT " & _
                    '         "                 GA.analysisunkid AS banalysisunkid " & _
                    '         "                ,0 AS rscore " & _
                    '         "                ,'Balance Score Card' AS smode " & _
                    '         "                ,1 AS smodeid " & _
                    '         "                ,0 AS assessgroupunkid " & _
                    '         "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                    '         "            FROM hrgoals_analysis_tran AS GA " & _
                    '         "                JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = GA.analysisunkid " & _
                    '         "            WHERE GA.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' " & _
                    '         "         UNION ALL " & _
                    '         "            SELECT DISTINCT " & _
                    '         "                 CT.analysisunkid AS banalysisunkid " & _
                    '         "                ,0 AS rscore " & _
                    '         "                ,AGM.assessgroup_name AS smode " & _
                    '         "                ,2 AS smodeid " & _
                    '         "                ,CT.assessgroupunkid " & _
                    '         "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                    '         "            FROM hrcompetency_analysis_tran AS CT " & _
                    '         "                JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = CT.analysisunkid " & _
                    '         "                JOIN hrassess_group_master AS AGM ON AGM.assessgroupunkid = CT.assessgroupunkid " & _
                    '         "                JOIN hrassess_competence_assign_master AS CAM ON CAM.assessgroupunkid = CT.assessgroupunkid " & _
                    '         "                JOIN hrassess_competence_assign_tran AS CAT ON CAT.competenciesunkid = CT.competenciesunkid " & _
                    '         "                AND CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                    '         "            WHERE CT.isvoid = 0 AND CAT.isvoid = 0 AND CAM.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' " & _
                    '         "        ) AS SCR " & _
                    '         "    ) AS PE ON PE.banalysisunkid = analysisunkid " & _
                    '         "    JOIN hremployee_master ON hremployee_master.employeeunkid = PE.EmpId " & _
                    '         "WHERE hrevaluation_analysis_master.isvoid = 0 "

                    StrQ = "SELECT DISTINCT " & _
                           "     hrevaluation_analysis_master.analysisunkid " & _
                           "    ,hrevaluation_analysis_master.periodunkid " & _
                           "    ,CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) AS assessmentdate " & _
                           "    ,ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                           "    ,ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                           "    ,cfcommon_period_tran.yearunkid " & _
                           "    ,ISNULL(iscommitted,0) AS iscommitted " & _
                           "    ,cfcommon_period_tran.statusid AS Sid " & _
                           "    ,hrevaluation_analysis_master.committeddatetime " & _
                           "    ,hrevaluation_analysis_master.assessmodeid " & _
                           "    ,ISNULL(PE.rscore,0) AS rscore " & _
                           "    ,ISNULL(PE.smode,'') AS smode " & _
                           "    ,ISNULL(PE.smodeid,0) AS smodeid " & _
                           "    ,ISNULL(PE.assessgroupunkid,0) AS assessgroupunkid " & _
                           "    ,ISNULL(PE.EmpId,assessedemployeeunkid) AS EmpId " & _
                           "    ,assessormasterunkid " & _
                           "    ,assessoremployeeunkid " & _
                           "    ,reviewerunkid " & _
                           "    ,ISNULL(hremployee_master.firstname,'') +' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employee " & _
                           "    ,ISNULL(ASR.firstname,'') +' '+ISNULL(ASR.othername,'')+' '+ISNULL(ASR.surname,'') AS assessor " & _
                           "    ,ISNULL(REV.firstname,'') +' '+ISNULL(REV.othername,'')+' '+ISNULL(REV.surname,'') AS reviewer " & _
                           "    ,hrevaluation_analysis_master.userunkid " & _
                           "    ,CASE WHEN assessmodeid = 1 THEN ISNULL(iscommitted,0) ELSE 0 END AS sl_iscommitted " & _
                           "    ,CASE WHEN assessmodeid = 2 THEN ISNULL(iscommitted,0) ELSE 0 END AS al_iscommitted " & _
                           "    ,CASE WHEN assessmodeid = 3 THEN ISNULL(iscommitted,0) ELSE 0 END AS rl_iscommitted " & _
                           "    ,CASE WHEN assessmodeid = 1 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.committeddatetime,112) ELSE '' END AS sl_committeddatetime " & _
                           "    ,ISNULL(CASE WHEN assessmodeid = 2 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.committeddatetime,112) ELSE '' END,'') AS al_committeddatetime " & _
                           "    ,ISNULL(CASE WHEN assessmodeid = 3 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.committeddatetime,112) ELSE '' END,'') AS rl_committeddatetime " & _
                           "    ,CASE WHEN assessmodeid = 1 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.assessmentdate,112) ELSE '' END AS sl_assessmentdate " & _
                           "    ,CASE WHEN assessmodeid = 2 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.assessmentdate,112) ELSE '' END AS al_assessmentdate " & _
                           "    ,CASE WHEN assessmodeid = 3 THEN CONVERT(NVARCHAR(8),hrevaluation_analysis_master.assessmentdate,112) ELSE '' END AS rl_assessmentdate " & _
                           "FROM hrevaluation_analysis_master WITH (NOLOCK) " & _
                           "    LEFT JOIN hremployee_master AS ASR WITH (NOLOCK) ON ASR.employeeunkid = hrevaluation_analysis_master.assessoremployeeunkid " & _
                           "    LEFT JOIN hremployee_master AS REV WITH (NOLOCK) ON REV.employeeunkid = hrevaluation_analysis_master.reviewerunkid " & _
                           "    JOIN cfcommon_period_tran WITH (NOLOCK) on cfcommon_period_tran.periodunkid = hrevaluation_analysis_master.periodunkid " & _
                           "    JOIN hrmsConfiguration..cffinancial_year_tran WITH (NOLOCK) on hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             SCR.banalysisunkid " & _
                           "            ,SCR.rscore " & _
                           "            ,SCR.smode " & _
                           "            ,SCR.smodeid " & _
                           "            ,SCR.assessgroupunkid " & _
                           "            ,SCR.EmpId " & _
                           "        FROM " & _
                           "        ( " & _
                           "            SELECT DISTINCT " & _
                           "                 GA.analysisunkid AS banalysisunkid " & _
                           "                ,0 AS rscore " & _
                           "                ,@BSC AS smode " & _
                           "                ,1 AS smodeid " & _
                           "                ,0 AS assessgroupunkid " & _
                           "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                           "            FROM hrgoals_analysis_tran AS GA WITH (NOLOCK) " & _
                           "                JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GA.analysisunkid " & _
                           "            WHERE GA.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                           "        UNION ALL " & _
                           "            SELECT DISTINCT " & _
                           "                 CT.analysisunkid AS banalysisunkid " & _
                           "                ,0 AS rscore " & _
                           "                ,AGM.assessgroup_name AS smode " & _
                           "                ,2 AS smodeid " & _
                           "                ,CT.assessgroupunkid " & _
                           "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                           "            FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                           "                JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                           "                JOIN hrassess_group_master AS AGM WITH (NOLOCK) ON AGM.assessgroupunkid = CT.assessgroupunkid " & _
                           "                JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assessgroupunkid = CT.assessgroupunkid " & _
                           "                JOIN hrassess_competence_assign_tran AS CAT WITH (NOLOCK) ON CAT.competenciesunkid = CT.competenciesunkid " & _
                           "                AND CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                           "            WHERE CT.isvoid = 0 AND CAT.isvoid = 0 AND CAM.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                           "        ) AS SCR " & _
                           "    UNION ALL " & _
                           "        SELECT " & _
                           "             ACR.banalysisunkid " & _
                           "            ,ACR.rscore " & _
                           "            ,ACR.smode " & _
                           "            ,ACR.smodeid " & _
                           "            ,ACR.assessgroupunkid " & _
                           "            ,ACR.EmpId " & _
                           "        FROM " & _
                           "        ( " & _
                           "            SELECT DISTINCT " & _
                           "                 GA.analysisunkid AS banalysisunkid " & _
                           "                ,0 AS rscore " & _
                           "                ,@BSC AS smode " & _
                           "                ,1 AS smodeid " & _
                           "                ,0 AS assessgroupunkid " & _
                           "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                           "            FROM hrgoals_analysis_tran AS GA WITH (NOLOCK) " & _
                           "                JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GA.analysisunkid " & _
                           "            WHERE GA.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND EM.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                    If mdtAssessDate <> Nothing AndAlso enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        StrQ &= " AND CONVERT(CHAR(8),EM.assessmentdate,112) = '" & eZeeDate.convertDate(mdtAssessDate).ToString & "' "
                    End If
                    StrQ &= "        UNION ALL " & _
                            "            SELECT DISTINCT " & _
                            "                 CT.analysisunkid AS banalysisunkid " & _
                            "                ,0 AS rscore " & _
                            "                ,AGM.assessgroup_name AS smode " & _
                            "                ,2 AS smodeid " & _
                            "                ,CT.assessgroupunkid " & _
                            "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                            "            FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                            "                JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                            "                JOIN hrassess_group_master AS AGM WITH (NOLOCK) ON AGM.assessgroupunkid = CT.assessgroupunkid " & _
                            "                JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assessgroupunkid = CT.assessgroupunkid " & _
                            "                JOIN hrassess_competence_assign_tran AS CAT WITH (NOLOCK) ON CAT.competenciesunkid = CT.competenciesunkid " & _
                            "                AND CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                            "            WHERE CT.isvoid = 0 AND CAT.isvoid = 0 AND CAM.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND EM.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                    If mdtAssessDate <> Nothing AndAlso enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        StrQ &= " AND CONVERT(CHAR(8),EM.assessmentdate,112) = '" & eZeeDate.convertDate(mdtAssessDate).ToString & "' "
                    End If
                    StrQ &= "        ) AS ACR " & _
                            "    UNION ALL " & _
                            "        SELECT " & _
                            "             RCR.banalysisunkid " & _
                            "            ,RCR.rscore " & _
                            "            ,RCR.smode " & _
                            "            ,RCR.smodeid " & _
                            "            ,RCR.assessgroupunkid " & _
                            "            ,RCR.EmpId " & _
                            "        FROM " & _
                            "        ( " & _
                            "            SELECT DISTINCT " & _
                            "                 GA.analysisunkid AS banalysisunkid " & _
                            "                ,0 AS rscore " & _
                            "                ,@BSC AS smode " & _
                            "                ,1 AS smodeid " & _
                            "                ,0 AS assessgroupunkid " & _
                            "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                            "            FROM hrgoals_analysis_tran AS GA WITH (NOLOCK) " & _
                            "            JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GA.analysisunkid " & _
                            "            WHERE GA.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND EM.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
                    If mdtAssessDate <> Nothing AndAlso enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        StrQ &= " AND CONVERT(CHAR(8),EM.assessmentdate,112) = '" & eZeeDate.convertDate(mdtAssessDate).ToString & "' "
                    End If
                    StrQ &= "        UNION ALL " & _
                            "            SELECT DISTINCT " & _
                            "                 CT.analysisunkid AS banalysisunkid " & _
                            "                ,0 AS rscore " & _
                            "                ,AGM.assessgroup_name AS smode " & _
                            "                ,2 AS smodeid " & _
                            "                ,CT.assessgroupunkid " & _
                            "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS EmpId " & _
                            "            FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                            "                JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                            "                JOIN hrassess_group_master AS AGM WITH (NOLOCK) ON AGM.assessgroupunkid = CT.assessgroupunkid " & _
                            "                JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assessgroupunkid = CT.assessgroupunkid " & _
                            "                JOIN hrassess_competence_assign_tran AS CAT WITH (NOLOCK) ON CAT.competenciesunkid = CT.competenciesunkid " & _
                            "                AND CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                            "            WHERE CT.isvoid = 0 AND CAT.isvoid = 0 AND CAM.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND EM.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'"
                    If mdtAssessDate <> Nothing AndAlso enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        StrQ &= " AND CONVERT(CHAR(8),EM.assessmentdate,112) = '" & eZeeDate.convertDate(mdtAssessDate).ToString & "' "
                    End If
                    StrQ &= "        ) AS RCR " & _
                            "    ) AS PE ON PE.EmpId = (CASE WHEN assessmodeid = 1 THEN selfemployeeunkid ELSE assessedemployeeunkid END) /*PE.banalysisunkid = analysisunkid*/ " & _
                            "   LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = PE.EmpId " & _
                            "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' "

                    'S.SANDEEP |13-NOV-2020| -- START
                    'ISSUE/ENHANCEMENT : COMPETENCIES
                    If eEvalTypeId <> enPAEvalTypeId.EO_NONE Then
                    If intPeriodStatusId <> enStatusType.CLOSE Then StrQ &= " AND hrevaluation_analysis_master.evaltypeid = " & CInt(eEvalTypeId) & " "
                    End If                    
                    'S.SANDEEP |13-NOV-2020| -- END

                    If intYearId > 0 Then
                        StrQ &= " AND cfcommon_period_tran.yearunkid = '" & intYearId & "' "
                    End If

                    If intEmployeeId > 0 Then
                        StrQ &= " AND PE.EmpId = '" & intEmployeeId & "' "
                    End If

                    'StrQ &= " ORDER BY hrevaluation_analysis_master.assessmodeid,PE.EmpId "

                    'S.SANDEEP [06 Jan 2016] -- START

                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    'objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Balanced Score Card"))
                    objDo.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 125, "Objectives/Goals/Targets"))
                    'Shani (26-Sep-2016) -- End


                    'S.SANDEEP [06 Jan 2016] -- END

                    dsTrans = objDo.ExecQuery(StrQ, "List")

                    Dim dt As DataTable
                    dt = New DataView(dsTrans.Tables(0), "", "assessmodeid,EmpId", DataViewRowState.CurrentRows).ToTable.Copy
                    dsTrans.Tables.Remove("List")
                    dsTrans.Tables.Add(dt.Copy)
                    dt = Nothing

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    For Each dr As DataRow In dsList.Tables("List").Rows
                        Dim dtmp() As DataRow = Nothing

                        dtmp = dsTrans.Tables("List").Select("EmpId = '" & dr("employeeunkid") & "'")
                        If dtmp.Length <= 0 Then
                            Call AddBasedRow(mdtFinal, dr)
                            Continue For
                        End If

                        dtmp = dsTrans.Tables("List").Select("EmpId = '" & dr("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'", "smodeid")
                        If dtmp.Length > 0 Then
                            For index As Integer = 0 To dtmp.Length - 1
                                Call AddDataRow(mdtFinal, dr, dtmp(index), enAssessmentMode.SELF_ASSESSMENT, enAssessMode)
                            Next
                        End If

                        dtmp = dsTrans.Tables("List").Select("EmpId = '" & dr("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'", "smodeid")
                        If dtmp.Length > 0 Then
                            Dim dtRow() As DataRow = Nothing : Dim blnFlag As Boolean
                            dtRow = mdtFinal.Select("employeeunkid = '" & dr("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'", "smodeid")
                            If dtRow.Length <= 0 Then blnFlag = True
                            For index As Integer = 0 To dtmp.Length - 1
                                If blnFlag = True Then
                                    Call AddDataRow(mdtFinal, dr, dtmp(index), enAssessmentMode.APPRAISER_ASSESSMENT, enAssessMode)
                                Else

                                    'Shani(30-MAR-2016) -- Start
                                    'dtRow = mdtFinal.Select("employeeunkid = '" & dtmp(index)("EmpId") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND smodeid = '" & dtmp(index)("smodeid") & "' ", "smodeid")
                                    dtRow = mdtFinal.Select("employeeunkid = '" & dtmp(index)("EmpId") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND smodeid = '" & dtmp(index)("smodeid") & "' AND assessgroupunkid = '" & dtmp(index)("assessgroupunkid") & "'", "smodeid")
                                    'Shani(30-MAR-2016) -- End 


                                    If dtRow.Length > 0 Then
                                        dtRow(0)("al_score") = dtmp(index)("rscore")
                                        dtRow(0)("al_analysisunkid") = dtmp(index)("analysisunkid")
                                        dtRow(0)("al_iscommitted") = dtmp(index)("al_iscommitted")
                                        dtRow(0)("al_committeddatetime") = dtmp(index)("al_committeddatetime")
                                        dtRow(0)("al_assessmentdate") = dtmp(index)("al_assessmentdate")
                                        If CBool(dtmp(index)("al_iscommitted")) = True Then
                                            dtRow(0)("operationviewid") = enAssessmentType.ASSESSMENT_COMMITTED
                                        Else
                                            dtRow(0)("operationviewid") = enAssessmentType.ASSESSMENT_ON_GOING
                                        End If
                                    End If
                                End If
                            Next
                        End If

                        dtmp = dsTrans.Tables("List").Select("EmpId = '" & dr("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'", "smodeid")
                        If dtmp.Length > 0 Then
                            Dim dtRow() As DataRow = Nothing : Dim blnFlag As Boolean
                            dtRow = mdtFinal.Select("employeeunkid = '" & dr("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'", "smodeid")
                            If dtRow.Length <= 0 Then
                                dtRow = mdtFinal.Select("employeeunkid = '" & dr("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'", "smodeid")
                                If dtRow.Length <= 0 Then blnFlag = True
                            End If
                            For index As Integer = 0 To dtmp.Length - 1
                                If blnFlag = True Then
                                    Call AddDataRow(mdtFinal, dr, dtmp(index), enAssessmentMode.REVIEWER_ASSESSMENT, enAssessMode)
                                Else

                                    'Shani(30-MAR-2016) -- Start
                                    'dtRow = mdtFinal.Select("employeeunkid = '" & dtmp(index)("EmpId") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND smodeid = '" & dtmp(index)("smodeid") & "' ", "smodeid")
                                    dtRow = mdtFinal.Select("employeeunkid = '" & dtmp(index)("EmpId") & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND smodeid = '" & dtmp(index)("smodeid") & "' AND assessgroupunkid = '" & dtmp(index)("assessgroupunkid") & "' ", "smodeid")
                                    'Shani(30-MAR-2016) -- End 
                                    If dtRow.Length <= 0 Then

                                        'Shani(30-MAR-2016) -- Start
                                        'dtRow = mdtFinal.Select("employeeunkid = '" & dtmp(index)("EmpId") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND smodeid = '" & dtmp(index)("smodeid") & "' ", "smodeid")
                                        dtRow = mdtFinal.Select("employeeunkid = '" & dtmp(index)("EmpId") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND smodeid = '" & dtmp(index)("smodeid") & "' AND assessgroupunkid = '" & dtmp(index)("assessgroupunkid") & "' ", "smodeid")
                                        'Shani(30-MAR-2016) -- End 


                                    End If
                                    If dtRow.Length > 0 Then
                                        dtRow(0)("rl_score") = dtmp(index)("rscore")
                                        dtRow(0)("rl_analysisunkid") = dtmp(index)("analysisunkid")
                                        dtRow(0)("rl_iscommitted") = dtmp(index)("rl_iscommitted")
                                        dtRow(0)("rl_committeddatetime") = dtmp(index)("rl_committeddatetime")
                                        dtRow(0)("rl_assessmentdate") = dtmp(index)("rl_assessmentdate")
                                        If CBool(dtmp(index)("rl_iscommitted")) = True Then
                                            dtRow(0)("operationviewid") = enAssessmentType.ASSESSMENT_COMMITTED
                                        Else
                                            dtRow(0)("operationviewid") = enAssessmentType.ASSESSMENT_ON_GOING
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
            End Using

            Dim dtemp() As DataRow = mdtFinal.Select("operationviewid <= 0 AND IsGrp = True")
            If dtemp.Length > 0 Then
                For Indx As Integer = 0 To dtemp.Length - 1
                    Dim intOprvalue As Integer = 0
                    intOprvalue = Indx
                    Dim vDetails = mdtFinal.Rows.Cast(Of DataRow)().Where(Function(x) x.Field(Of Integer)("GrpId") = dtemp(intOprvalue)("GrpId") And x.Field(Of Integer)("operationviewid") > 0).ToList()
                    intOprvalue = 0
                    If vDetails.Count > 0 Then
                        intOprvalue = vDetails.Item(0).Item("operationviewid")
                    End If
                    dtemp(Indx)("operationviewid") = intOprvalue
                Next
            End If
            'Shani (28-Apr-2016) -- Start
            Dim drl = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") > 0).Distinct()
            For Each dr In drl
                Dim intEmpId As Integer = dr.Item("employeeunkid")
                'analysisid = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False And x.Field(Of Integer)("sl_analysisunkid") > 0 And x.Field(Of Integer)("employeeunkid") = intEmpId).Select(Function(y) y.Field(Of Integer)("sl_analysisunkid")).First()
                Dim row = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False And x.Field(Of Integer)("sl_analysisunkid") > 0 And x.Field(Of Integer)("employeeunkid") = intEmpId).Distinct()
                If row.Count > 0 Then
                    Call SetAnalysisUnkid(intEmpId, enAssessmentMode.SELF_ASSESSMENT, row(0).Item("sl_analysisunkid"), mdtFinal)
                End If

                row = Nothing
                row = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False And x.Field(Of Integer)("al_analysisunkid") > 0 And x.Field(Of Integer)("employeeunkid") = intEmpId).Distinct()
                If row.Count > 0 Then
                    Call SetAnalysisUnkid(intEmpId, enAssessmentMode.APPRAISER_ASSESSMENT, row(0).Item("al_analysisunkid"), mdtFinal)
                End If

                row = Nothing
                row = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False And x.Field(Of Integer)("rl_analysisunkid") > 0 And x.Field(Of Integer)("employeeunkid") = intEmpId).Distinct()
                If row.Count > 0 Then
                    Call SetAnalysisUnkid(intEmpId, enAssessmentMode.REVIEWER_ASSESSMENT, row(0).Item("rl_analysisunkid"), mdtFinal)
                End If
            Next
            'Shani (28-Apr-2016) -- End

            Dim dview = mdtFinal.DefaultView

            If intViewType > 0 Then
                dview.RowFilter = "operationviewid = '" & intViewType & "' "
            End If

            dview.Sort = "onlyempname,GrpId,IsGrp DESC"
            mdtFinal = dview.ToTable
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListNew; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    'Shani (28-Apr-2016) -- Start
    Private Sub SetAnalysisUnkid(ByVal intEmpId As Integer, ByVal eMode As enAssessmentMode, ByVal intAnalysisId As Integer, ByRef mdtTable As DataTable)
        Try
            'Dim rows = mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmpId And x.Field(Of Integer)("assessmodeid") = eMode And x.Field(Of Boolean)("IsGrp") = False)
            Dim rows = mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmpId And x.Field(Of Boolean)("IsGrp") = False)
            For Each r In rows
                Select Case eMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        r.Item("sl_analysisunkid") = intAnalysisId
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        r.Item("al_analysisunkid") = intAnalysisId
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        r.Item("rl_analysisunkid") = intAnalysisId
                End Select
            Next
            mdtTable.AcceptChanges()
        Catch ex As Exception

        End Try
    End Sub
    'Shani (28-Apr-2016) -- End
    Private Sub AddBasedRow(ByRef dtTable As DataTable, ByVal drMstRow As DataRow)
        Try
            Dim dtRow As DataRow = dtTable.NewRow
            dtRow("employee") = drMstRow("EmpName")
            dtRow("employeeunkid") = drMstRow("employeeunkid")
            dtRow("GrpId") = drMstRow("employeeunkid")
            dtRow("IsGrp") = False
            dtRow("onlyempname") = drMstRow("onlyempname")
            dtRow("operationviewid") = enAssessmentType.ASSESSMENT_NOT_DONE
            dtTable.Rows.Add(dtRow)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AddBasedRow; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub AddDataRow(ByRef dtTable As DataTable, ByVal drMstRow As DataRow, ByVal dtTranRow As DataRow, ByVal eMode As enAssessmentMode, ByVal eViewMode As enAssessmentMode)
        Try
            Dim dtRow As DataRow = dtTable.NewRow
            dtRow("employee") = drMstRow("EmpName")
            dtRow("assessor") = dtTranRow("assessor")
            dtRow("reviewer") = dtTranRow("reviewer")
            dtRow("viewmode") = Space(5) & dtTranRow("smode")
            dtRow("year") = dtTranRow("YearName")
            dtRow("period") = dtTranRow("PName")
            dtRow("assessmentdate") = dtTranRow("assessmentdate")
            If eMode = enAssessmentMode.SELF_ASSESSMENT Then
                dtRow("sl_score") = dtTranRow("rscore")
                dtRow("sl_analysisunkid") = dtTranRow("analysisunkid")
                dtRow("sl_iscommitted") = dtTranRow("sl_iscommitted")
                dtRow("sl_committeddatetime") = dtTranRow("sl_committeddatetime")
                dtRow("sl_assessmentdate") = dtTranRow("sl_assessmentdate")
            ElseIf eMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                dtRow("al_score") = dtTranRow("rscore")
                dtRow("al_analysisunkid") = dtTranRow("analysisunkid")
                dtRow("al_iscommitted") = dtTranRow("al_iscommitted")
                dtRow("al_committeddatetime") = dtTranRow("al_committeddatetime")
                dtRow("al_assessmentdate") = dtTranRow("al_assessmentdate")
            ElseIf eMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dtRow("rl_score") = dtTranRow("rscore")
                dtRow("rl_analysisunkid") = dtTranRow("analysisunkid")
                dtRow("rl_iscommitted") = dtTranRow("rl_iscommitted")
                dtRow("rl_committeddatetime") = dtTranRow("rl_committeddatetime")
                dtRow("rl_assessmentdate") = dtTranRow("rl_assessmentdate")
            End If
            dtRow("employeeunkid") = dtTranRow("EmpId")
            dtRow("periodunkid") = dtTranRow("periodunkid")
            dtRow("yearunkid") = dtTranRow("yearunkid")
            dtRow("statusunkid") = dtTranRow("Sid")
            dtRow("assessgroupunkid") = dtTranRow("assessgroupunkid")
            dtRow("assessmodeid") = dtTranRow("assessmodeid")
            dtRow("smodeid") = dtTranRow("smodeid")
            dtRow("iscommitted") = dtTranRow("iscommitted")
            dtRow("committeddatetime") = dtTranRow("committeddatetime")
            dtRow("assessormasterunkid") = dtTranRow("assessormasterunkid")
            dtRow("assessoremployeeunkid") = dtTranRow("assessoremployeeunkid")
            dtRow("userunkid") = dtTranRow("userunkid")
            dtRow("reviewerunkid") = dtTranRow("reviewerunkid")
            dtRow("GrpId") = dtTranRow("EmpId")
            dtRow("IsGrp") = False
            dtRow("onlyempname") = drMstRow("onlyempname")
            Dim blnCommitCheck As Boolean = False
            Select Case eViewMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    blnCommitCheck = CBool(dtTranRow("sl_iscommitted"))
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    blnCommitCheck = CBool(dtTranRow("al_iscommitted"))
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    blnCommitCheck = CBool(dtTranRow("rl_iscommitted"))
            End Select

            If blnCommitCheck = True Then
                dtRow("operationviewid") = enAssessmentType.ASSESSMENT_COMMITTED
            Else
                dtRow("operationviewid") = enAssessmentType.ASSESSMENT_ON_GOING
            End If

            dtTable.Rows.Add(dtRow)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AddDataRow; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [18 DEC 2015] -- END

    'S.SANDEEP [02 Jan 2016] -- START

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Public Function IsPlanningDone(ByVal strEvalOrder As String, ByVal intEmpId As Integer, ByVal intPeriodId As Integer, ByVal blnSelfAssign As Boolean, ByVal blnCustomItemInPlanning As Boolean) As String
    Public Function IsPlanningDone(ByVal strEvalOrder As String, _
                                   ByVal intEmpId As Integer, _
                                   ByVal intPeriodId As Integer, _
                                   ByVal blnSelfAssign As Boolean, _
                                   ByVal mAssessment As enAssessmentMode, _
                                   ByVal blnFromWeb As Boolean) As String
        'Shani (26-Sep-2016) -- End
        Dim StrMessage As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim iCnt As Integer = -1
        Try
            'S.SANDEEP [07-NOV-2018] -- START
            'If strEvalOrder.Trim.Length > 0 Then
            '    Dim strvalue() As String = strEvalOrder.Split("|")
            '    If strvalue.Length > 0 Then
            '        Using objDo As New clsDataOperation
            '            For ival As Integer = 0 To strvalue.Length - 1
            '                StrQ = ""
            '                Select Case strvalue(ival)
            '                    Case enEvaluationOrder.PE_BSC_SECTION
            '                        StrQ = "SELECT 1 FROM hrassess_empfield1_master WHERE employeeunkid = '" & intEmpId & "' AND periodunkid = '" & intPeriodId & "' AND isfinal = 1 AND isvoid = 0 "

            '                        If StrQ.Trim.Length > 0 Then
            '                            iCnt = objDo.RecordCount(StrQ)

            '                            If objDo.ErrorMessage <> "" Then
            '                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '                            End If

            '                            If iCnt <= 0 Then
            '                                StrMessage = Language.getMessage(mstrModuleName, 18, "You cannot assess/review this employee’s performance because there is no performance plans.")
            '                            End If
            '                        End If

            '                        'Exit For 'Shani (24-May-2016) -- Start
            '                    Case enEvaluationOrder.PE_COMPETENCY_SECTION
            '                        'Shani (24-May-2016) -- Start
            '                        'StrQ = "SELECT 1 FROM hrassess_competence_assign_master WHERE periodunkid = '" & intPeriodId & "' AND isfinal = 1 AND isvoid = 0 "
            '                        StrQ = "SELECT 1 FROM hrassess_competence_assign_master WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0 "
            '                        'Shani (24-May-2016) -- End

            '                        If blnSelfAssign Then
            '                            'Shani (24-May-2016) -- Start
            '                            'StrQ &= "AND employeeunkid  = '" & intEmpId & "' "
            '                            StrQ &= "AND isfinal = 1 AND employeeunkid  = '" & intEmpId & "' "
            '                            'Shani (24-May-2016) -- End
            '                        End If

            '                        If StrQ.Trim.Length > 0 Then
            '                            iCnt = objDo.RecordCount(StrQ)

            '                            If objDo.ErrorMessage <> "" Then
            '                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '                            End If

            '                            If iCnt <= 0 Then
            '                                StrMessage = Language.getMessage(mstrModuleName, 18, "You cannot assess/review this employee’s performance because there is no performance plans.")
            '                            End If
            '                        End If

            '                        'Exit For 'Shani (24-May-2016) -- Start
            '                    Case enEvaluationOrder.PE_CUSTOM_SECTION
            '                        'Shani (26-Sep-2016) -- Start
            '                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            '                        'If blnCustomItemInPlanning Then
            '                        '    StrQ = "SELECT 1 FROM hrassess_plan_customitem_tran WHERE isvoid = 0 AND employeeunkid = '" & intEmpId & "' AND periodunkid = '" & intPeriodId & "' "

            '                        '    If StrQ.Trim.Length > 0 Then
            '                        '        iCnt = objDo.RecordCount(StrQ)

            '                        '        If objDo.ErrorMessage <> "" Then
            '                        '            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '                        '        End If

            '                        '        If iCnt <= 0 Then
            '                        '            StrMessage = Language.getMessage(mstrModuleName, 18, "You cannot assess/review this employee’s performance because there is no performance plans.")
            '                        '        End If
            '                        '    End If
            '                        '    'Exit For 'Shani (24-May-2016) -- Start
            '                        'End If
            '                        StrQ = "IF Exists(SELECT 1 FROM  hrassess_custom_headers WHERE hrassess_custom_headers.isactive = 1 AND hrassess_custom_headers.isinclude_planning = 1) " & _
            '                               "    BEGIN " & _
            '                               "        SELECT " & _
            '                               "            1 " & _
            '                               "        FROM  hrassess_custom_headers " & _
            '                               "            LEFT JOIN hrassess_plan_customitem_tran ON hrassess_custom_headers.customheaderunkid = hrassess_plan_customitem_tran.customheaderunkid " & _
            '                               "        WHERE isvoid = 0 AND hrassess_custom_headers.isinclude_planning = 1 AND employeeunkid = '" & intEmpId & "' AND periodunkid = '" & intPeriodId & "' " & _
            '                               "    END " & _
            '                               "ELSE " & _
            '                               "    BEGIN " & _
            '                               "        SELECT 1 " & _
            '                               "    END "

            '                        If StrQ.Trim.Length > 0 Then
            '                            iCnt = objDo.RecordCount(StrQ)

            '                            If objDo.ErrorMessage <> "" Then
            '                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '                            End If

            '                            If iCnt <= 0 Then
            '                                StrMessage = Language.getMessage(mstrModuleName, 18, "You cannot assess/review this employee’s performance because there is no performance plans.")
            '                            End If
            '                        End If
            '                        'Shani (26-Sep-2016) -- End
            '                End Select
            '            Next
            '        End Using
            '    End If
            'End If

            Dim blnIsPlanningDone As Boolean = False

            If strEvalOrder IsNot Nothing AndAlso strEvalOrder.Trim.Length > 0 Then
                Dim strvalue() As String = strEvalOrder.Split("|")
                If strvalue.Length > 0 Then
                    Select Case mAssessment
                        Case enAssessmentMode.SELF_ASSESSMENT
                            StrMessage = Language.getMessage(mstrModuleName, 19, "Sorry, you cannot do performance assessment for the selected period. Reason following thing(s) are pending : ") & "\n"
                        Case Else
                            StrMessage = Language.getMessage(mstrModuleName, 20, "Sorry, you cannot do performance assessment for the selected period and employee. Reason following thing(s) are pending : ") & "\n"
                    End Select
                    Using objDo As New clsDataOperation
                        For ival As Integer = 0 To strvalue.Length - 1
                            StrQ = ""
                            Select Case strvalue(ival)
                                Case enEvaluationOrder.PE_BSC_SECTION
                                    StrQ = "SELECT 1 FROM hrassess_empfield1_master WITH (NOLOCK) WHERE employeeunkid = '" & intEmpId & "' AND periodunkid = '" & intPeriodId & "' AND isfinal = 1 AND isvoid = 0 "

                                    If StrQ.Trim.Length > 0 Then
                                        iCnt = objDo.RecordCount(StrQ)

                                        If objDo.ErrorMessage <> "" Then
                                            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                                        End If

                                        If iCnt <= 0 Then
                                            StrMessage &= (ival + 1).ToString & ". " & Language.getMessage(mstrModuleName, 21, "Goals are not defined.") & "\n"
                                            blnIsPlanningDone = False
                                        Else
                                            blnIsPlanningDone = True
                                        End If
                                    End If

                                Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                    StrQ = "SELECT 1 FROM hrassess_competence_assign_master WITH (NOLOCK) WHERE periodunkid = '" & intPeriodId & "' AND isvoid = 0 "

                                    If blnSelfAssign Then
                                        StrQ &= "AND isfinal = 1 AND employeeunkid  = '" & intEmpId & "' "
                                    End If

                                    If StrQ.Trim.Length > 0 Then
                                        iCnt = objDo.RecordCount(StrQ)

                                        If objDo.ErrorMessage <> "" Then
                                            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                                        End If

                                        If iCnt <= 0 Then
                                            StrMessage &= (ival + 1).ToString & ". " & Language.getMessage(mstrModuleName, 22, "Competencies are not defined/assigned.") & "\n"
                                            blnIsPlanningDone = False
                                        Else
                                            blnIsPlanningDone = True
                                        End If
                                    End If

                                Case enEvaluationOrder.PE_CUSTOM_SECTION

                                    StrQ = "IF Exists(SELECT 1 FROM  hrassess_custom_headers WITH (NOLOCK) WHERE hrassess_custom_headers.isactive = 1 AND hrassess_custom_headers.isinclude_planning = 1) " & _
                                           "    BEGIN " & _
                                           "        SELECT " & _
                                           "            1 " & _
                                           "        FROM  hrassess_custom_headers WITH (NOLOCK) " & _
                                           "            LEFT JOIN hrassess_plan_customitem_tran WITH (NOLOCK) ON hrassess_custom_headers.customheaderunkid = hrassess_plan_customitem_tran.customheaderunkid " & _
                                           "        WHERE isvoid = 0 AND hrassess_custom_headers.isinclude_planning = 1 AND employeeunkid = '" & intEmpId & "' AND periodunkid = '" & intPeriodId & "' " & _
                                           "    END " & _
                                           "ELSE " & _
                                           "    BEGIN " & _
                                           "        SELECT 1 " & _
                                           "    END "

                                    If StrQ.Trim.Length > 0 Then
                                        iCnt = objDo.RecordCount(StrQ)

                                        If objDo.ErrorMessage <> "" Then
                                            Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                                        End If

                                        If iCnt <= 0 Then
                                            StrMessage &= (ival + 1).ToString & ". " & Language.getMessage(mstrModuleName, 23, "Items are not defined/planned.") & "\n"
                                            blnIsPlanningDone = False
                                        Else
                                            blnIsPlanningDone = True
                                        End If
                                    End If
                            End Select
                        Next
                    End Using
                End If
            End If
            If blnIsPlanningDone Then
                StrMessage = ""
            Else
                StrMessage &= Language.getMessage(mstrModuleName, 24, "If you have already planned and it's not approved. Please contact you assessor.")
            End If
            'S.SANDEEP [07-NOV-2018] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPlanningDone ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMessage
    End Function
    'S.SANDEEP [02 Jan 2016] -- END

    'Shani (24-May-2016) -- Start
    Public Function GetAnalusisUnkid(ByVal intEmployeeId As Integer, ByVal eMode As enAssessmentMode, ByVal intPeriodId As Integer, Optional ByVal eVTypeId As enPAEvalTypeId = enPAEvalTypeId.EO_NONE) As Integer
        Dim intAnalysisUnkId As Integer = 0
        Dim StrQ As String = "" : Dim dsEval As New DataSet
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "   EM.analysisunkid " & _
                       "  ,EM.assessmodeid " & _
                       "FROM hrevaluation_analysis_master EM WITH (NOLOCK) " & _
                       "WHERE EM.isvoid = 0 AND EM.periodunkid = '" & intPeriodId & "' AND assessmodeid = '" & eMode & "' "
                If eVTypeId <> enPAEvalTypeId.EO_NONE Then
                    StrQ &= "AND evaltypeid = '" & eVTypeId & "'"
                End If
                Select Case eMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= " AND EM.selfemployeeunkid = '" & intEmployeeId & "' "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= " AND EM.assessedemployeeunkid = '" & intEmployeeId & "' "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= " AND EM.assessedemployeeunkid = '" & intEmployeeId & "' "
                End Select

                dsEval = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                End If

                If dsEval IsNot Nothing AndAlso dsEval.Tables(0).Rows.Count > 0 Then
                    intAnalysisUnkId = dsEval.Tables(0).Rows(0)("analysisunkid")
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEvaluationUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intAnalysisUnkId
    End Function

    Public Function CheckComputionIsExist(ByVal intAnalysisUnkid As Integer, ByVal xAssessmode As enAssessmentMode, ByVal intPeriodUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT computescoremasterunkid FROM hrassess_compute_score_master WITH (NOLOCK) WHERE isvoid = 0 AND periodunkid = @periodunkid AND analysisunkid = @analysisunkid "

            Select Case xAssessmode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
            End Select

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Shani (24-May-2016) -- End


    'S.SANDEEP [04-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
    Public Function GetEmployee_AssessorReviewerDetails(ByVal intPeriodId As Integer, _
                                                         ByVal strDataBaseName As String, _
                                                         ByVal dtEmpAsOnDate As Date, _
                                                         Optional ByVal intEmployeeId As Integer = 0, _
                                                         Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim objAPeriod As New clscommom_period_Tran
        Dim objDataOperation As clsDataOperation = Nothing
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            objAPeriod._xDataOperation = objDataOperation
            objAPeriod._Periodunkid(strDataBaseName) = intPeriodId
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, objAPeriod._Start_Date, objAPeriod._End_Date, True, False, strDataBaseName)
            objDataOperation.ClearParameters()
            If dtEmpAsOnDate = Nothing Then dtEmpAsOnDate = objAPeriod._End_Date

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            'S.SANDEEP |25-MAR-2019| -- START
            'Dim strFiled_Query As String = ",hremployee_master.firstname+' '+hremployee_master.othername+' '+hremployee_master.surname  AS arName " -- REMOVED
            'S.SANDEEP |25-MAR-2019| -- END
            Dim strFiled_Query As String = ",hremployee_master.firstname+' '+hremployee_master.surname  AS arName " & _
                                               ",ISNULL(hremployee_master.employeecode,'')  AS arCode " & _
                                               ",ISNULL(hremployee_master.email,'')  AS arEmail " & _
                                               ",ISNULL(hrstation_master.name,'') AS AR_Branch " & _
                                               ",ISNULL(hrdepartment_group_master.name,'') AS AR_DepartmentGroup " & _
                                               ",ISNULL(hrdepartment_master.name,'') AS AR_Department " & _
                                               ",ISNULL(hrsectiongroup_master.name, '') AS AR_SectionGroup " & _
                                               ",ISNULL(hrsection_master.name,'') AS AR_Section " & _
                                               ",ISNULL(hrunitgroup_master.name, '') AS AR_UnitGroup " & _
                                               ",ISNULL(hrunit_master.name,'') AS AR_Unit " & _
                                               ",ISNULL(hrteam_master.name,'') AS AR_Team " & _
                                               ",ISNULL(hrclassgroup_master.name,'') AS AR_ClassGroup " & _
                                               ",ISNULL(hrclasses_master.name,'') AS AR_Class " & _
                                               ",ISNULL(hrjobgroup_master.name,'') AS AR_JobGroup " & _
                                               ",ISNULL(hrjob_master.job_name,'') AS AR_Job " & _
                                               ",ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS AR_AppointmentDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),ECNF.confirmation_date,112),'') AS AR_ConfirmationDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS AR_BirthDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),ESUSP.suspended_from_date,112),'') AS AR_SuspendedFromDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),ESUSP.suspended_to_date,112),'') AS AR_SuspendedToDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),EPROB.probation_from_date,112),'') AS AR_ProbationFromDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),EPROB.probation_to_date, 112),'') AS AR_ProbationToDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate , 112),'') AS AR_EndOfContract_Date " & _
                                               ",ISNULL(CONVERT(CHAR(8),ETERM.termination_from_date,112),'') AS AR_LeavingDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),ERET.termination_to_date,112),'') AS AR_RetirementDate " & _
                                               ",ISNULL(CONVERT(CHAR(8),ERH.reinstatment_date , 112),'') AS AR_Reinstatement_Date " & _
                                               ",ISNULL(hremployee_master.displayname,'') AS arDisplayName "
            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # (38,41)
            '-------- ADDED :  ",ISNULL(hremployee_master.email,'')  AS arEmail "
            'S.SANDEEP [29-NOV-2017] -- END

            'S.SANDEEP |14-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : DISPLAY NAME
            ' ------- ADDED : ",ISNULL(hremployee_master.displayname,'') AS arDisplayName "
            'S.SANDEEP |14-JUN-2021| -- END


            Dim strJoin_Query As String = " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "          stationunkid " & _
                                          "         ,deptgroupunkid " & _
                                          "         ,departmentunkid " & _
                                          "         ,sectiongroupunkid " & _
                                          "         ,sectionunkid " & _
                                          "         ,unitgroupunkid " & _
                                          "         ,unitunkid " & _
                                          "         ,teamunkid " & _
                                          "         ,classgroupunkid " & _
                                          "         ,classunkid " & _
                                          "         ,employeeunkid " & _
                                          "         ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                          "     FROM #DBNAME#hremployee_transfer_tran WITH (NOLOCK) " & _
                                          "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '#EMPASONDATE#' " & _
                                          " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                          " LEFT JOIN #DBNAME#hrstation_master WITH (NOLOCK) ON hrstation_master.stationunkid = Alloc.stationunkid " & _
                                          " LEFT JOIN #DBNAME#hrdepartment_group_master WITH (NOLOCK) ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid " & _
                                          " LEFT JOIN #DBNAME#hrdepartment_master WITH (NOLOCK) ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                                          " LEFT JOIN #DBNAME#hrsectiongroup_master WITH (NOLOCK) ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                                          " LEFT JOIN #DBNAME#hrsection_master WITH (NOLOCK) ON hrsection_master.sectionunkid = Alloc.sectionunkid " & _
                                          " LEFT JOIN #DBNAME#hrunitgroup_master WITH (NOLOCK) ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid " & _
                                          " LEFT JOIN #DBNAME#hrunit_master WITH (NOLOCK) ON hrunit_master.unitunkid = Alloc.unitunkid " & _
                                          " LEFT JOIN #DBNAME#hrteam_master WITH (NOLOCK) ON hrteam_master.teamunkid = Alloc.teamunkid " & _
                                          " LEFT JOIN #DBNAME#hrclassgroup_master WITH (NOLOCK) ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid " & _
                                          " LEFT JOIN #DBNAME#hrclasses_master WITH (NOLOCK) ON hrclasses_master.classesunkid = Alloc.classunkid " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "          jobunkid " & _
                                          "         ,jobgroupunkid " & _
                                          "         ,employeeunkid " & _
                                          "         ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                          "     FROM #DBNAME#hremployee_categorization_tran WITH (NOLOCK) " & _
                                          "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                          " LEFT JOIN #DBNAME#hrjob_master WITH (NOLOCK) ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                                          " LEFT JOIN #DBNAME#hrjobgroup_master WITH (NOLOCK) ON hrjobgroup_master.jobgroupunkid = Jobs.jobgroupunkid " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "          SUSP.SDEmpId " & _
                                          "         ,SUSP.suspended_from_date " & _
                                          "         ,SUSP.suspended_to_date " & _
                                          "         ,SUSP.SDEfDt " & _
                                          "     FROM " & _
                                          "     ( " & _
                                          "         SELECT " & _
                                          "              SDT.employeeunkid AS SDEmpId " & _
                                          "             ,CONVERT(CHAR(8),SDT.date1,112) AS suspended_from_date " & _
                                          "             ,CONVERT(CHAR(8),SDT.date2,112) AS suspended_to_date " & _
                                          "             ,CONVERT(CHAR(8),SDT.effectivedate,112) AS SDEfDt " & _
                                          "             ,ROW_NUMBER()OVER(PARTITION BY SDT.employeeunkid ORDER BY SDT.effectivedate DESC) AS Rno " & _
                                          "         FROM #DBNAME#hremployee_dates_tran AS SDT WITH (NOLOCK) " & _
                                          "         WHERE isvoid = 0 AND SDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),SDT.effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS SUSP WHERE SUSP.Rno = 1 " & _
                                          " ) AS ESUSP ON ESUSP.SDEmpId = hremployee_master.employeeunkid /*AND ESUSP.SDEfDt >= CONVERT(CHAR(8),hremployee_master.appointeddate,112)*/ " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "          PROB.PDEmpId " & _
                                          "         ,PROB.probation_from_date " & _
                                          "         ,PROB.probation_to_date " & _
                                          "         ,PROB.PDEfDt " & _
                                          "     FROM " & _
                                          "     ( " & _
                                          "         SELECT " & _
                                          "             PDT.employeeunkid AS PDEmpId " & _
                                          "             ,CONVERT(CHAR(8),PDT.date1,112) AS probation_from_date " & _
                                          "             ,CONVERT(CHAR(8),PDT.date2,112) AS probation_to_date " & _
                                          "             ,CONVERT(CHAR(8),PDT.effectivedate,112) AS PDEfDt " & _
                                          "             ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                                          "         FROM #DBNAME#hremployee_dates_tran AS PDT WITH (NOLOCK) " & _
                                          "         WHERE isvoid = 0 AND PDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS PROB WHERE PROB.Rno = 1 " & _
                                          " ) AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid /*AND EPROB.PDEfDt >= CONVERT(CHAR(8),hremployee_master.appointeddate,112)*/ " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "         TERM.TEEmpId " & _
                                          "         ,TERM.empl_enddate " & _
                                          "         ,TERM.termination_from_date " & _
                                          "         ,TERM.TEfDt " & _
                                          "         ,TERM.isexclude_payroll " & _
                                          "         ,TERM.changereasonunkid " & _
                                          "     FROM " & _
                                          "     ( " & _
                                          "         SELECT " & _
                                          "             TRM.employeeunkid AS TEEmpId " & _
                                          "             ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                                          "             ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                                          "             ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                                          "             ,TRM.isexclude_payroll " & _
                                          "             ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                                          "             ,TRM.changereasonunkid " & _
                                          "         FROM #DBNAME#hremployee_dates_tran AS TRM WITH (NOLOCK) " & _
                                          "         WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS TERM WHERE TERM.Rno = 1 " & _
                                          " ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid /*AND ETERM.TEfDt >= CONVERT(CHAR(8),hremployee_master.appointeddate,112)*/ " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "          CNF.CEmpId " & _
                                          "         ,CNF.confirmation_date " & _
                                          "         ,CNF.CEfDt " & _
                                          "     FROM " & _
                                          "     ( " & _
                                          "         SELECT " & _
                                          "              CNF.employeeunkid AS CEmpId " & _
                                          "             ,CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                                          "             ,CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                                          "             ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                                          "             ,CNF.changereasonunkid " & _
                                          "         FROM #DBNAME#hremployee_dates_tran AS CNF WITH (NOLOCK) " & _
                                          "         WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS CNF WHERE CNF.Rno = 1 " & _
                                          " ) AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid /*AND ECNF.CEfDt >= CONVERT(CHAR(8),hremployee_master.appointeddate,112)*/ " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "         RET.REmpId " & _
                                          "         ,RET.termination_to_date " & _
                                          "         ,RET.REfDt " & _
                                          "     FROM " & _
                                          "     ( " & _
                                          "         SELECT " & _
                                          "              RTD.employeeunkid AS REmpId " & _
                                          "             ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                                          "             ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                                          "             ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                                          "         FROM #DBNAME#hremployee_dates_tran AS RTD WITH (NOLOCK) " & _
                                          "         WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS RET WHERE RET.Rno = 1 " & _
                                          " ) AS ERET ON ERET.REmpId = hremployee_master.employeeunkid /*AND ERET.REfDt >= CONVERT(CHAR(8),hremployee_master.appointeddate,112)*/ " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "     SELECT " & _
                                          "          RH.RHEmpId " & _
                                          "         ,RH.reinstatment_date " & _
                                          "         ,RH.RHEfDt " & _
                                          "         ,RH.changereasonunkid AS RH_changereasonunkid " & _
                                          "     FROM " & _
                                          "     ( " & _
                                          "         SELECT " & _
                                          "              ERT.employeeunkid AS RHEmpId " & _
                                          "             ,CONVERT(CHAR(8),ERT.reinstatment_date,112) AS reinstatment_date " & _
                                          "             ,CONVERT(CHAR(8),ERT.effectivedate,112) AS RHEfDt " & _
                                          "             ,ROW_NUMBER()OVER(PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " & _
                                          "             ,ERT.changereasonunkid " & _
                                          "         FROM #DBNAME#hremployee_rehire_tran AS ERT WITH (NOLOCK) " & _
                                          "         WHERE isvoid = 0 AND CONVERT(CHAR(8),ERT.effectivedate,112) <= '#EMPASONDATE#' " & _
                                          "     ) AS RH WHERE RH.Rno = 1 " & _
                                          " )AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid /*AND ERH.RHEfDt >= CONVERT(CHAR(8),hremployee_master.appointeddate,112)*/ "

            StrQ = "SELECT DISTINCT " & _
                   "     CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN ASR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
                   "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN RVR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
                   "     END AS assessormasterunkid " & _
                   "    ,CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
                   "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
                   "     END AS isfound " & _
                   "    ,hrassessor_master.employeeunkid " & _
                   "    ,hrassessor_master.isreviewer " & _
                   "    ,hrassessor_tran.visibletypeid " & _
                   "    ,hrassessor_tran.employeeunkid as assignempid " & _
                   "    #FILED_QUERY#" & _
                   "FROM hrassessor_tran WITH (NOLOCK) " & _
                   "    JOIN hrassessor_master WITH (NOLOCK) ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                   "    LEFT JOIN hrevaluation_analysis_master ASR WITH (NOLOCK) ON ASR.assessormasterunkid = hrassessor_master.assessormasterunkid AND ASR.periodunkid = @periodunkid AND ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid AND ASR.isvoid = 0 " & _
                   "    LEFT JOIN hrevaluation_analysis_master RVR WITH (NOLOCK) ON RVR.assessormasterunkid = hrassessor_master.assessormasterunkid AND RVR.periodunkid = @periodunkid AND RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid AND RVR.isvoid = 0 " & _
                   "    #EMPJOIN# " & _
                   "    #JOIN_QUERY# "
            'S.SANDEEP [28-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REPORT ENHANCEMENT
            '------ ADDED MATCHING EMPLOYEEID FOR MULTIPLE EMPLOYEE
            'S.SANDEEP [28-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
            '------ ADDED ISVOID IN (ASR & RVR)
            'S.SANDEEP [20-SEP-2017] -- END

            StrQ &= "WHERE hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 #COND#"

            If intEmployeeId > 0 Then
                StrQ &= " AND hrassessor_tran.employeeunkid = @employeeunkid "
            End If

            Dim strFinalQ As String = ""
            strFinalQ = StrQ
            strFinalQ = strFinalQ.Replace("#FILED_QUERY#", strFiled_Query)
            strFinalQ = strFinalQ.Replace("#JOIN_QUERY#", strJoin_Query)
            strFinalQ = strFinalQ.Replace("#EMPJOIN#", " JOIN #DBNAME#hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid ")
            strFinalQ = strFinalQ.Replace("#COND#", " AND hrassessor_master.isexternalapprover = 0")
            strFinalQ = strFinalQ.Replace("#EMPASONDATE#", eZeeDate.convertDate(dtEmpAsOnDate))
            strFinalQ = strFinalQ.Replace("#DBNAME#", "")

            dsList = objDataOperation.ExecQuery(strFinalQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If
            Dim dsTemp As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", 0, 0, intEmployeeId, 0, clsAssessor.enAssessorType.ALL_DATA)
            For Each dRow As DataRow In dsTemp.Tables(0).Rows

                'S.SANDEEP [12-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : PARAMETER DECLARATION ERROR FOR EXTERNAL APPROVER
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                'S.SANDEEP [12-SEP-2017] -- END
                

                strFinalQ = StrQ
                If dRow.Item("companyunkid") <= 0 AndAlso dRow.Item("DBName").ToString.Trim.Length <= 0 Then
                    strFinalQ = strFinalQ.Replace("#FILED_QUERY#", ",ISNULL(cfuser_master.username,'') AS arName " & _
                                                                       ",'' AS arCode " & _
                                                                       ",ISNULL(cfuser_master.email,'') AS arEmail" & _
                                                                       ",'' AS AR_Branch " & _
                                                                       ",'' AS AR_DepartmentGroup " & _
                                                                       ",'' AS AR_Department " & _
                                                                       ",'' AS AR_SectionGroup " & _
                                                                       ",'' AS AR_Section " & _
                                                                       ",'' AS AR_UnitGroup " & _
                                                                       ",'' AS AR_Unit " & _
                                                                       ",'' AS AR_Team " & _
                                                                       ",'' AS AR_ClassGroup " & _
                                                                       ",'' AS AR_Class " & _
                                                                       ",'' AS AR_JobGroup " & _
                                                                       ",'' AS AR_Job " & _
                                                                       ",'' AS AR_AppointmentDate " & _
                                                                       ",'' AS AR_ConfirmationDate " & _
                                                                       ",'' AS AR_BirthDate " & _
                                                                       ",'' AS AR_SuspendedFromDate " & _
                                                                       ",'' AS AR_SuspendedToDate " & _
                                                                       ",'' AS AR_ProbationFromDate " & _
                                                                       ",'' AS AR_ProbationToDate " & _
                                                                       ",'' AS AR_EndOfContract_Date " & _
                                                                       ",'' AS AR_LeavingDate " & _
                                                                       ",'' AS AR_RetirementDate " & _
                                                                       ",'' AS AR_Reinstatement_Date, '' AS arDisplayName ")
                    strFinalQ = strFinalQ.Replace("#EMPJOIN#", " JOIN hrmsConfiguration..cfuser_master WITH (NOLOCK) ON cfuser_master.userunkid = hrassessor_master.employeeunkid")
                    strFinalQ = strFinalQ.Replace("#JOIN_QUERY#", "")

                    'S.SANDEEP [29-NOV-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                    '-------- ADDED :  ",ISNULL(cfuser_master.email,'')  AS arEmail "
                    'S.SANDEEP [29-NOV-2017] -- END

                Else
                    strFinalQ = strFinalQ.Replace("#FILED_QUERY#", strFiled_Query)
                    strFinalQ = strFinalQ.Replace("#JOIN_QUERY#", strJoin_Query)
                    'S.SANDEEP [12-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : PARAMETER DECLARATION ERROR FOR EXTERNAL APPROVER
                    'strFinalQ = strFinalQ.Replace("#EMPJOIN#", " JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = hrassessor_master.employeeunkid " & _
                    '                                           " JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid")
                    strFinalQ = strFinalQ.Replace("#EMPJOIN#", " JOIN hrmsConfiguration..cfuser_master WITH (NOLOCK) ON cfuser_master.userunkid = hrassessor_master.employeeunkid " & _
                                                               " JOIN #DBNAME#hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = cfuser_master.employeeunkid")
                    'S.SANDEEP [12-SEP-2017] -- END
                    strFinalQ = strFinalQ.Replace("#EMPASONDATE#", dRow.Item("EMPASONDATE").ToString)
                End If
                strFinalQ = strFinalQ.Replace("#COND#", " AND hrassessor_master.isexternalapprover = 1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                strFinalQ = strFinalQ.Replace("#DBNAME#", dRow.Item("DBName") & "..")

                Dim dsTempData As DataSet = objDataOperation.ExecQuery(strFinalQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If dsList Is Nothing Then dsList = New DataSet

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsTempData.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dsTempData.Tables(0), True)
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_AssessorReviewerDetails; Module Name: " & mstrModuleName)
        Finally
            objAPeriod = Nothing
        End Try
        Return dsList
    End Function
    'S.SANDEEP [04-AUG-2017] -- END

    'S.SANDEEP |08-JAN-2019| -- START
    Public Function GetCompletedTrainingListForCustomItem(ByVal intEmployeeId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT 0 AS masterunkid, @select AS name, 99 AS coursetypeid UNION " & _
                   "SELECT DISTINCT CM.masterunkid,CM.name,CM.coursetypeid " & _
                   "FROM hrtraining_enrollment_tran AS TE " & _
                   "    JOIN hrtraining_scheduling AS TS ON TE.trainingschedulingunkid = TS.trainingschedulingunkid " & _
                   "    JOIN cfcommon_master AS CM ON CM.masterunkid = TS.course_title " & _
                   "WHERE TE.isvoid = 0 AND TE.iscancel = 0 AND TE.status_id = 3 AND TS.isvoid = 0 AND TS.iscancel = 0 " & _
                   " AND TE.employeeunkid = @employeeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 127, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCompletedTrainingListForCustomItem; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Public Function IsValidToStartAssessment(ByVal intEmpId As Integer, ByVal intPeriodId As Integer) As String
        Dim strMessage As String = String.Empty
        Dim iCnt As Integer = -1
        Dim StrQ As String = String.Empty
        Try
            Using objData As New clsDataOperation
                StrQ = "SELECT 1 FROM hrassess_empupdate_tran " & _
                       "WHERE isvoid = 0 AND approvalstatusunkid = 2 " & _
                       "AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                iCnt = objData.RecordCount(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                End If

                If iCnt <= 0 Then
                    strMessage = Language.getMessage(mstrModuleName, 161, "Sorry, you cannot start with assessment. Reason, Update progress of goals is not done yet.")
                End If
                'S.SANDEEP |11-SEP-2019| -- START
                objData.ClearParameters()
                If strMessage.Trim.Length <= 0 Then
                    StrQ = "SELECT " & _
                           "    1 " & _
                           "FROM hrassess_empupdate_tran " & _
                           "WHERE empfieldunkid IN " & _
                           "( " & _
                           "    SELECT " & _
                           "        empfieldunkid " & _
                           "    FROM hrassess_empupdate_tran WHERE periodunkid = @periodunkid AND isvoid = 0 and employeeunkid = @employeeunkid " & _
                           "    GROUP BY empfieldunkid HAVING COUNT(1) = 1 " & _
                           ") AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid AND isvoid = 0 AND approvalstatusunkid = @approvalstatusunkid "

                    objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                    objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                    objData.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)

                    iCnt = objData.RecordCount(StrQ)

                    If objData.ErrorMessage <> "" Then
                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    End If

                    If iCnt > 0 Then
                        strMessage = Language.getMessage(mstrModuleName, 162, "Sorry, you cannot start with assessment. Reason, Update progress of goals is in pending state.")
                    End If
                End If

                objData.ClearParameters()
                If strMessage.Trim.Length <= 0 Then
                    StrQ = "SELECT " & _
                           "    1 " & _
                           "FROM hrassess_empupdate_tran " & _
                           "WHERE empfieldunkid IN " & _
                           "( " & _
                           "    SELECT " & _
                           "        empfieldunkid " & _
                           "    FROM hrassess_empupdate_tran WHERE periodunkid = @periodunkid AND isvoid = 0 and employeeunkid = @employeeunkid " & _
                           "    GROUP BY empfieldunkid HAVING COUNT(1) = 1 " & _
                           ") AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid AND isvoid = 0 AND approvalstatusunkid = @approvalstatusunkid "

                    objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                    objData.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                    objData.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)

                    iCnt = objData.RecordCount(StrQ)

                    If objData.ErrorMessage <> "" Then
                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    End If

                    If iCnt > 0 Then
                        strMessage = Language.getMessage(mstrModuleName, 163, "Sorry, you cannot start with assessment. Reason, Update progress of goals is in disapproved state.")
                    End If
                End If
                'S.SANDEEP |11-SEP-2019| -- END
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidToStartAssessment; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMessage
    End Function
    'S.SANDEEP |16-AUG-2019| -- END


    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Function InsertUpdate(ByVal objGoalsTran As List(Of clsgoal_analysis_tran), _
                                 ByVal objCmptsTran As List(Of clscompetency_analysis_tran), _
                                 ByVal objItemsTran As List(Of clscompeteny_customitem_tran), _
                                 ByVal xScoreOptId As Integer, _
                                 ByVal xEmployeeAsOnDate As Date, _
                                 ByVal xUsedAgreedScore As Boolean, _
                                 ByVal xSelfAssignedCompetencies As Boolean, _
                                 ByRef xBSC_Score As Decimal, _
                                 ByRef xGE_Score As Decimal) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintPeriodunkid, , , , , , False) = True Then
                    mintAnalysisunkid = GetAnalusisUnkid(mintSelfemployeeunkid, mintAssessmodeid, mintPeriodunkid)
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, , mintAssessormasterunkid, , , , False) = True Then
                    mintAnalysisunkid = GetAnalusisUnkid(mintAssessedemployeeunkid, mintAssessmodeid, mintPeriodunkid)
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintPeriodunkid, , , mintAssessormasterunkid, , , False) = True Then
                    mintAnalysisunkid = GetAnalusisUnkid(mintAssessedemployeeunkid, mintAssessmodeid, mintPeriodunkid)
                End If
        End Select

        mdtComputeTran = objComputeScore.GetTranData(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                 mintPeriodunkid, mintAssessmodeid, _
                                                 mstrMessage, "List")

        'S.SANDEEP |03-MAY-2021| -- START
        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
        Dim intPDPFormId As Integer = 0
        Dim objpdpform As New clspdpform_master
        objpdpform.IsPDPFormExists(IIf(mintSelfemployeeunkid <= 0, mintAssessedemployeeunkid, mintSelfemployeeunkid), 0, intPDPFormId)
        objpdpform = Nothing
        'S.SANDEEP |03-MAY-2021| -- END

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(mdtAssessmentdate & " " & Now.ToString("hh:mm:ss tt")).ToString())
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            If mintEvalTypeId <= 0 Then mintEvalTypeId = CInt(enPAEvalTypeId.EO_BOTH)
            objDataOperation.AddParameter("@evaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvalTypeId)
            'S.SANDEEP |13-NOV-2020| -- END

            If mintAnalysisunkid <= 0 Then
                strQ = "INSERT INTO hrevaluation_analysis_master ( " & _
                       "  periodunkid " & _
                       ", selfemployeeunkid " & _
                       ", assessormasterunkid " & _
                       ", assessoremployeeunkid " & _
                       ", assessedemployeeunkid " & _
                       ", assessmentdate " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", iscommitted " & _
                       ", reviewerunkid " & _
                       ", assessmodeid " & _
                       ", ext_assessorunkid" & _
                       ", committeddatetime " & _
                       ", evaltypeid " & _
                   ") VALUES (" & _
                       "  @periodunkid " & _
                       ", @selfemployeeunkid " & _
                       ", @assessormasterunkid " & _
                       ", @assessoremployeeunkid " & _
                       ", @assessedemployeeunkid " & _
                       ", @assessmentdate " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @iscommitted " & _
                       ", @reviewerunkid " & _
                       ", @assessmodeid " & _
                       ", @ext_assessorunkid" & _
                       ", @committeddatetime " & _
                       ", @evaltypeid " & _
                   "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)

            Else
                strQ = "UPDATE hrevaluation_analysis_master SET " & _
                       "  periodunkid = @periodunkid" & _
                       ", selfemployeeunkid = @selfemployeeunkid" & _
                       ", assessormasterunkid = @assessormasterunkid" & _
                       ", assessoremployeeunkid = @assessoremployeeunkid" & _
                       ", assessedemployeeunkid = @assessedemployeeunkid" & _
                       ", assessmentdate = @assessmentdate" & _
                       ", userunkid = @userunkid" & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason" & _
                       ", iscommitted = @iscommitted" & _
                       ", reviewerunkid = @reviewerunkid" & _
                       ", assessmodeid = @assessmodeid" & _
                       ", ext_assessorunkid = @ext_assessorunkid " & _
                       ", committeddatetime = @committeddatetime " & _
                       ", evaltypeid = @evaltypeid " & _
                       "WHERE analysisunkid = @analysisunkid "
                'S.SANDEEP |13-NOV-2020| -- START {evaltypeid} -- END
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If



            If objGoalsTran IsNot Nothing AndAlso objGoalsTran.Count > 0 Then

                For Each item As clsgoal_analysis_tran In objGoalsTran

                    objGoalAnalysisTran._ianalysistranunkid = item._ianalysistranunkid
                    objGoalAnalysisTran._ianalysisunkid = mintAnalysisunkid
                    objGoalAnalysisTran._iperspectiveunkid = item._iperspectiveunkid
                    objGoalAnalysisTran._iempfield1unkid = item._iempfield1unkid
                    objGoalAnalysisTran._iempfield2unkid = item._iempfield2unkid
                    objGoalAnalysisTran._iempfield3unkid = item._iempfield3unkid
                    objGoalAnalysisTran._iempfield4unkid = item._iempfield4unkid
                    objGoalAnalysisTran._iempfield5unkid = item._iempfield5unkid
                    objGoalAnalysisTran._iresult = item._iresult
                    objGoalAnalysisTran._iremark = item._iremark
                    objGoalAnalysisTran._iisvoid = item._iisvoid
                    objGoalAnalysisTran._ivoiduserunkid = item._ivoiduserunkid
                    objGoalAnalysisTran._ivoiddatetime = item._ivoiddatetime
                    objGoalAnalysisTran._ivoidreason = item._ivoidreason
                    objGoalAnalysisTran._iitem_weight = item._iitem_weight
                    objGoalAnalysisTran._imax_scale = item._imax_scale
                    objGoalAnalysisTran._iagreedscore = item._iagreedscore

                    If objGoalAnalysisTran.InsertUpdate(objDataOperation, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                If mintAnalysisunkid > 0 Then
                    Dim eFormula As enAssess_Computation_Formulas
                    Select Case mintAssessmodeid
                        Case enAssessmentMode.SELF_ASSESSMENT
                            eFormula = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            eFormula = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            eFormula = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE
                    End Select

                    If CheckComputionIsExist(mintAnalysisunkid, mintAssessmodeid, mintPeriodunkid) Then
                        objComputeScore._Isvoid = True
                        objComputeScore._Voiddatetime = Now
                        objComputeScore._Voiduserunkid = mintUserunkid
                        objComputeScore._Voidreason = Language.getMessage(mstrModuleName, 131, "Edit Assessment.")
                        If objComputeScore.DeleteEmployeeWise(mintAnalysisunkid, IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), mintPeriodunkid, mintAssessmodeid, eFormula, objDataOperation) = False Then
                            mstrMessage = Language.getMessage(mstrModuleName, 130, "Sorry, Fail to void exsisting computed score.")
                            Return False
                        End If
                    End If
                End If

                Dim intComputScoreMasterId As Integer = 0
                intComputScoreMasterId = objComputeScore.GetComputeMasterId(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                                            mintPeriodunkid, mintAssessmodeid, , _
                                                                            objDataOperation)

                objComputeScore._Assessmodeid = mintAssessmodeid
                objComputeScore._Computationdate = Now
                objComputeScore._Isvoid = False
                objComputeScore._Periodunkid = mintPeriodunkid
                objComputeScore._Scoretypeid = xScoreOptId
                objComputeScore._Userunkid = mintUserunkid
                objComputeScore._Voiddatetime = Nothing
                objComputeScore._Voidreason = ""
                objComputeScore._Voiduserunkid = -1
                objComputeScore._Analysisunkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objComputeScore._Employeeunkid = mintSelfemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objComputeScore._Employeeunkid = mintAssessedemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                End Select
                Dim xTotalScore As Decimal = 0 : Dim intComputeFormulaId As Integer = 0 : Dim intComputeUnkid As Integer
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE
                End Select

                intComputeUnkid = objComputeScore.GetComputationUnkid(intComputeFormulaId, mintPeriodunkid, objDataOperation)

                xTotalScore = Compute_Score(xAssessMode:=mintAssessmodeid, _
                                            IsBalanceScoreCard:=True, _
                                            xScoreOptId:=xScoreOptId, _
                                            xCompute_Formula:=intComputeFormulaId, _
                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                                            xEmployeeId:=IIf(mintSelfemployeeunkid <= 0, mintAssessedemployeeunkid, mintSelfemployeeunkid), _
                                            xPeriodId:=mintPeriodunkid, _
                                            xUsedAgreedScore:=xUsedAgreedScore, _
                                            xSelfAssignedCompetencies:=xSelfAssignedCompetencies)

                xBSC_Score = xTotalScore

                If intComputScoreMasterId > 0 Then
                    Dim xtmp() As DataRow = mdtComputeTran.Select("computescoremasterunkid = '" & intComputScoreMasterId & "' AND computationunkid = '" & intComputeFormulaId & "'")
                    If xtmp.Length > 0 Then
                        With xtmp(0)
                            .Item("computescoretranunkid") = .Item("computescoretranunkid")
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "U"
                            .Item("competency_value") = ""
                        End With
                    Else
                        Dim tmp = mdtComputeTran.NewRow()
                        With tmp
                            .Item("computescoretranunkid") = -1
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "A"
                            .Item("competency_value") = ""
                        End With
                        mdtComputeTran.Rows.Add(tmp)
                    End If
                Else
                    Dim xtmp As DataRow = mdtComputeTran.NewRow()
                    With xtmp
                        .Item("computescoretranunkid") = -1
                        .Item("computescoremasterunkid") = intComputScoreMasterId
                        .Item("computationunkid") = intComputeUnkid
                        .Item("formula_value") = xTotalScore
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = 0
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("competency_value") = ""
                    End With
                    mdtComputeTran.Rows.Add(xtmp)
                End If

                If intComputScoreMasterId > 0 Then
                    If objComputeScore.Insert_Update_Delete(objDataOperation, intComputScoreMasterId, mdtComputeTran) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If objComputeScore.Insert(mdtComputeTran, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            If objCmptsTran IsNot Nothing AndAlso objCmptsTran.Count > 0 Then

                For Each item As clscompetency_analysis_tran In objCmptsTran
                    objAnalysisTran._ianalysisunkid = mintAnalysisunkid
                    objAnalysisTran._iagreedscore = item._iagreedscore
                    objAnalysisTran._ianalysistranunkid = item._ianalysistranunkid
                    objAnalysisTran._iassessgroupunkid = item._iassessgroupunkid
                    objAnalysisTran._icompetenciesunkid = item._icompetenciesunkid
                    objAnalysisTran._iisvoid = item._iisvoid
                    objAnalysisTran._iitem_weight = item._iitem_weight
                    objAnalysisTran._imax_scale = item._imax_scale
                    objAnalysisTran._iremark = item._iremark
                    objAnalysisTran._iresult = item._iresult
                    objAnalysisTran._ivoiddatetime = item._ivoiddatetime
                    objAnalysisTran._ivoidreason = item._ivoidreason
                    objAnalysisTran._ivoiduserunkid = item._ivoiduserunkid

                    If objAnalysisTran.InsertUpdate(objDataOperation, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                If mintAnalysisunkid > 0 Then
                    Dim eFormula As enAssess_Computation_Formulas
                    Select Case mintAssessmodeid
                        Case enAssessmentMode.SELF_ASSESSMENT
                            eFormula = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            eFormula = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            eFormula = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE
                    End Select
                    If CheckComputionIsExist(mintAnalysisunkid, mintAssessmodeid, mintPeriodunkid) Then
                        objComputeScore._Isvoid = True
                        objComputeScore._Voiddatetime = Now
                        objComputeScore._Voiduserunkid = mintUserunkid
                        objComputeScore._Voidreason = Language.getMessage(mstrModuleName, 131, "Edit Assessment.")
                        If objComputeScore.DeleteEmployeeWise(mintAnalysisunkid, IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), mintPeriodunkid, mintAssessmodeid, eFormula, objDataOperation) = False Then
                            mstrMessage = Language.getMessage(mstrModuleName, 130, "Sorry, Fail to void exsisting computed score.")
                            Return False
                        End If
                    End If
                End If

                mdtComputeTran.Rows.Clear()
                Dim intComputScoreMasterId As Integer = 0
                intComputScoreMasterId = objComputeScore.GetComputeMasterId(IIf(mintAssessmodeid = enAssessmentMode.SELF_ASSESSMENT, mintSelfemployeeunkid, mintAssessedemployeeunkid), _
                                                                            mintPeriodunkid, mintAssessmodeid, , _
                                                                            objDataOperation)

                objComputeScore._Assessmodeid = mintAssessmodeid
                objComputeScore._Computationdate = Now
                objComputeScore._Isvoid = False
                objComputeScore._Periodunkid = mintPeriodunkid
                objComputeScore._Scoretypeid = xScoreOptId
                objComputeScore._Userunkid = mintUserunkid
                objComputeScore._Voiddatetime = Nothing
                objComputeScore._Voidreason = ""
                objComputeScore._Voiduserunkid = -1
                objComputeScore._Analysisunkid = mintAnalysisunkid
                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        objComputeScore._Employeeunkid = mintSelfemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        objComputeScore._Employeeunkid = mintAssessedemployeeunkid
                        objComputeScore._Assessormasterunkid = mintAssessormasterunkid
                End Select
                Dim xTotalScore As Decimal = 0 : Dim intComputeFormulaId As Integer = 0 : Dim intComputeUnkid As Integer
                Dim xCSV_CMP_Value As String = String.Empty

                Select Case mintAssessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        intComputeFormulaId = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE
                End Select

                intComputeUnkid = objComputeScore.GetComputationUnkid(intComputeFormulaId, mintPeriodunkid, objDataOperation)

                Dim xComputed As New Dictionary(Of Integer, Integer)

                For Each item As clscompetency_analysis_tran In objCmptsTran
                    Dim xScore As Decimal = 0
                    If xComputed.ContainsKey(item._iassessgroupunkid) = False Then
                        xComputed.Add(item._iassessgroupunkid, item._iassessgroupunkid)
                    Else
                        Continue For
                    End If
                    xScore = Compute_Score(xAssessMode:=mintAssessmodeid, _
                                           IsBalanceScoreCard:=False, _
                                           xScoreOptId:=xScoreOptId, _
                                           xCompute_Formula:=intComputeFormulaId, _
                                           xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                                           xEmployeeId:=IIf(mintSelfemployeeunkid <= 0, mintAssessedemployeeunkid, mintSelfemployeeunkid), _
                                           xPeriodId:=mintPeriodunkid, _
                                           xUsedAgreedScore:=xUsedAgreedScore, _
                                           xAssessorReviewerId:=mintAssessormasterunkid, _
                                           xSelfAssignedCompetencies:=xSelfAssignedCompetencies, _
                                           xAssessGrpId:=item._iassessgroupunkid)
                    xTotalScore = xTotalScore + xScore
                    xCSV_CMP_Value &= "," & item._iassessgroupunkid.ToString & "|" & xScore.ToString
                Next
                If xCSV_CMP_Value.Trim.Length > 0 Then xCSV_CMP_Value = Mid(xCSV_CMP_Value, 2)

                xGE_Score = xTotalScore

                If intComputScoreMasterId > 0 Then
                    Dim xtmp() As DataRow = mdtComputeTran.Select("computescoremasterunkid = '" & intComputScoreMasterId & "' AND computationunkid = '" & intComputeFormulaId & "'")
                    If xtmp.Length > 0 Then
                        With xtmp(0)
                            .Item("computescoretranunkid") = .Item("computescoretranunkid")
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "U"
                            .Item("competency_value") = xCSV_CMP_Value
                        End With
                    Else
                        Dim tmp = mdtComputeTran.NewRow()
                        With tmp
                            .Item("computescoretranunkid") = -1
                            .Item("computescoremasterunkid") = intComputScoreMasterId
                            .Item("computationunkid") = intComputeUnkid
                            .Item("formula_value") = xTotalScore
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = 0
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("AUD") = "A"
                            .Item("competency_value") = xCSV_CMP_Value
                        End With
                        mdtComputeTran.Rows.Add(tmp)
                    End If
                Else
                    Dim xtmp As DataRow = mdtComputeTran.NewRow()
                    With xtmp
                        .Item("computescoretranunkid") = -1
                        .Item("computescoremasterunkid") = intComputScoreMasterId
                        .Item("computationunkid") = intComputeUnkid
                        .Item("formula_value") = xTotalScore
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = 0
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("competency_value") = xCSV_CMP_Value
                    End With
                    mdtComputeTran.Rows.Add(xtmp)
                End If

                If intComputScoreMasterId > 0 Then
                    If objComputeScore.Insert_Update_Delete(objDataOperation, intComputScoreMasterId, mdtComputeTran) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    If objComputeScore.Insert(mdtComputeTran, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

            End If

            If objItemsTran IsNot Nothing AndAlso objItemsTran.Count > 0 Then
                For Each item As clscompeteny_customitem_tran In objItemsTran
                    objCustomTran._ianalysisunkid = mintAnalysisunkid
                    objCustomTran._icustom_value = item._icustom_value
                    objCustomTran._icustomanalysistranguid = item._icustomanalysistranguid
                    objCustomTran._icustomanalysistranunkid = item._icustomanalysistranunkid
                    objCustomTran._icustomitemunkid = item._icustomitemunkid
                    objCustomTran._iemployeeunkid = item._iemployeeunkid
                    objCustomTran._iismanual = item._iismanual
                    objCustomTran._iisvoid = item._iisvoid
                    objCustomTran._iperiodunkid = item._iperiodunkid
                    objCustomTran._ivoiddatetime = item._ivoiddatetime
                    objCustomTran._ivoiduserunkid = item._ivoiduserunkid
                    objCustomTran._ivoidreason = item._ivoidreason

                    If objCustomTran.InsertUpdate(objDataOperation, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP |03-MAY-2021| -- START
                    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                    If item._icustomitemunkid.ToString().StartsWith("5000") AndAlso intPDPFormId > 0 Then
                        objpdpItemMaster._Itemunkid(objDataOperation) = CInt(item._icustomitemunkid.ToString().Replace("5000", ""))
                        objPDPFormTran._AuditUserId = mintUserunkid
                        objPDPFormTran._Categoryunkid = objpdpItemMaster._Categoryunkid                        
                        objPDPFormTran._Fieldvalue = item._icustom_value                    
                        objPDPFormTran._IsCompetencySelection = False
                        objPDPFormTran._Isvoid = False
                        objPDPFormTran._Itemgrpguid = item._icustomanalysistranguid
                        objPDPFormTran._Itemunkid = CInt(item._icustomitemunkid.ToString().Replace("5000", ""))
                       
                        objPDPFormTran._Pdpformunkid = intPDPFormId
                        objPDPFormTran._Voiddatetime = item._ivoiddatetime
                        objPDPFormTran._Voiduserunkid = item._ivoiduserunkid
                        objPDPFormTran._Voidreason = item._ivoidreason

                        objPDPFormTran._FormName = mstrFormName
                        objPDPFormTran._FromWeb = mblnIsWeb
                        objPDPFormTran._HostName = mstrHostName
                        objPDPFormTran._ClientIP = mstrClientIP
                        objPDPFormTran._DatabaseName = mstrDatabaseName
                        objPDPFormTran._LoginEmployeeUnkid = mintloginemployeeunkid
                        objPDPFormTran._PAnalysisunkid = mintAnalysisunkid

                        If objPDPFormTran.SaveCustomItemData(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    'S.SANDEEP |03-MAY-2021| -- END

                Next
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsValidToSubmitAssessment(ByVal intPeriodId As Integer, _
                                              ByVal intEmployeeId As Integer, _
                                              ByVal intAssessormasterunkid As Integer, _
                                              ByVal iMode As enAssessmentMode, _
                                              ByVal iAnalysisId As Integer, _
                                              ByVal iEmployeeAsOnDate As DateTime, _
                                              ByVal blnSelfAssign As Boolean, _
                                              ByVal strCoyGrpName As String, _
                                              ByVal strPerf_EvaluationOrder As String, _
                                              ByRef intMsgType As Integer, _
                                              ByVal intGItemCount As Integer, _
                                              ByVal intCItemCount As Integer, _
                                              ByVal blnIsPDPIntegrated As Boolean) As String 'S.SANDEEP |03-MAY-2021| -- START {blnIsPDPIntegrated} -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim StrMsg As String = String.Empty
        Try
            Dim strEvalOdr() As String = strPerf_EvaluationOrder.Split("|")

            If iAnalysisId <= 0 Then
                Select Case iMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        If isExist(iMode, intEmployeeId, intPeriodId, , , , , , False) = True Then
                            iAnalysisId = GetAnalusisUnkid(intEmployeeId, iMode, intPeriodId)
                        End If
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If isExist(iMode, intEmployeeId, intPeriodId, , intAssessormasterunkid, , , , False) = True Then
                            iAnalysisId = GetAnalusisUnkid(intEmployeeId, iMode, intPeriodId)
                        End If
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If isExist(iMode, intEmployeeId, intPeriodId, , , intAssessormasterunkid, , , False) = True Then
                            iAnalysisId = GetAnalusisUnkid(intEmployeeId, iMode, intPeriodId)
                        End If
                End Select
            End If
            Dim iTotCount, iAssessCount As Integer
            iTotCount = 0 : iAssessCount = 0

            If strEvalOdr.Length > 0 AndAlso Array.IndexOf(strEvalOdr, CInt(enEvaluationOrder.PE_BSC_SECTION).ToString()) <> -1 Then
                intMsgType = CInt(enEvaluationOrder.PE_BSC_SECTION)
                Dim intFieldId As Integer = 0
                Dim objLinkedFld As New clsAssess_Field_Mapping
                intFieldId = objLinkedFld.Get_Map_FieldId(intPeriodId)

                Dim strTabName As String = String.Empty
                Select Case intFieldId
                    Case enWeight_Types.WEIGHT_FIELD1
                        strTabName = "hrassess_empfield1_master"
                    Case enWeight_Types.WEIGHT_FIELD2
                        strTabName = "hrassess_empfield2_master"
                    Case enWeight_Types.WEIGHT_FIELD3
                        strTabName = "hrassess_empfield3_master"
                    Case enWeight_Types.WEIGHT_FIELD4
                        strTabName = "hrassess_empfield4_master"
                    Case enWeight_Types.WEIGHT_FIELD5
                        strTabName = "hrassess_empfield5_master"
                End Select

                StrQ = "SELECT 1 FROM " & strTabName & " WITH (NOLOCK) WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                iTotCount = objDataOperation.RecordCount(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iAnalysisId > 0 Then
                    StrQ = "SELECT 1 FROM hrgoals_analysis_tran WITH (NOLOCK) WHERE analysisunkid = @analysisunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAnalysisId)

                iAssessCount = objDataOperation.RecordCount(StrQ)
                End If
                
                If iAssessCount <= 0 Then iAssessCount = intGItemCount
                If intGItemCount > 0 AndAlso intGItemCount > iAssessCount Then iAssessCount = intGItemCount

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iTotCount <> iAssessCount Then
                    StrMsg = Language.getMessage("frmPerformanceEvaluation", 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")
                    Exit Try
                End If
            End If

            iTotCount = 0 : iAssessCount = 0

            If strEvalOdr.Length > 0 AndAlso Array.IndexOf(strEvalOdr, CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <> -1 Then
                intMsgType = CInt(enEvaluationOrder.PE_COMPETENCY_SECTION)
                Dim iStrGroupIds As String = String.Empty
                Dim objAssessGrp As New clsassess_group_master
                iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(intEmployeeId, iEmployeeAsOnDate, intPeriodId)
                objAssessGrp = Nothing
                If iStrGroupIds.Trim.Length <= 0 Then iStrGroupIds = "0"
                StrQ = "SELECT " & _
                       " 1 " & _
                       "FROM hrassess_competence_assign_tran WITH (NOLOCK) " & _
                       "    JOIN hrassess_competence_assign_master WITH (NOLOCK) ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                       "    JOIN hrassess_competencies_master WITH (NOLOCK) ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                       "    JOIN cfcommon_master WITH (NOLOCK) ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid " & _
                       "WHERE hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid IN(" & iStrGroupIds & ") " & _
                       "AND hrassess_competence_assign_master.periodunkid = @periodunkid "
                If blnSelfAssign = True Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = @employeeunkid " & _
                            " AND hrassess_competence_assign_master.isfinal = 1 "
                Else
                    StrQ &= " AND ISNULL(hrassess_competence_assign_master.employeeunkid,0) <= 0 "
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                iTotCount = objDataOperation.RecordCount(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iAnalysisId > 0 Then
                    StrQ = "SELECT 1 FROM hrcompetency_analysis_tran WITH (NOLOCK) WHERE analysisunkid = @analysisunkid AND isvoid = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAnalysisId)

                iAssessCount = objDataOperation.RecordCount(StrQ)
                End If

                If iAssessCount <= 0 Then iAssessCount = intCItemCount
                If intCItemCount > 0 AndAlso intCItemCount > iAssessCount Then iAssessCount = intCItemCount

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iTotCount <> iAssessCount Then
                    StrMsg = Language.getMessage("frmPerformanceEvaluation", 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group.")
                    Exit Try
                End If
            End If


            iTotCount = 0 : iAssessCount = 0

            If strEvalOdr.Length > 0 AndAlso Array.IndexOf(strEvalOdr, CInt(enEvaluationOrder.PE_CUSTOM_SECTION).ToString()) <> -1 Then
                intMsgType = CInt(enEvaluationOrder.PE_CUSTOM_SECTION)
                StrQ = "SELECT " & _
                       " 1 " & _
                       "FROM hrassess_custom_items WITH (NOLOCK) " & _
                       "    JOIN hrassess_custom_headers WITH (NOLOCK) ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                       "WHERE hrassess_custom_headers.isactive = 1 AND periodunkid = @periodunkid AND hrassess_custom_items.isactive = 1 "

                Select Case iMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= "AND viewmodeid IN (0," & CInt(iMode) & ") "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= "AND viewmodeid IN (" & CInt(iMode) & ") "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= "AND viewmodeid IN (" & CInt(iMode) & ") "
                End Select

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                iTotCount = objDataOperation.RecordCount(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                If blnIsPDPIntegrated Then
                    Dim objPDPFrm As New clspdpform_master
                    If objPDPFrm.IsPDPFormExists(intEmployeeId, 0, 0) Then
                        StrQ = "SELECT 1 FROM pdpitem_master WITH (NOLOCK) JOIN pdpcategory_master WITH (NOLOCK) ON pdpitem_master.categoryunkid = pdpcategory_master.categoryunkid WHERE isincludeinpm = 1 AND ismandatory = 1 AND pdpcategory_master.isactive = 1 AND pdpitem_master.isactive = 1 "

                        Select Case iMode
                            Case enAssessmentMode.SELF_ASSESSMENT
                                StrQ &= "AND pdpitem_master.visibletoid IN (0," & CInt(iMode) & ") "
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                StrQ &= "AND pdpitem_master.visibletoid IN (" & CInt(iMode) & ") "
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ &= "AND pdpitem_master.visibletoid IN (" & CInt(iMode) & ") "
                        End Select

                        iTotCount = iTotCount + objDataOperation.RecordCount(StrQ)
                    End If
                    objPDPFrm = Nothing
                End If
                'S.SANDEEP |03-MAY-2021| -- END
                

                If iAnalysisId > 0 Then
                    StrQ = "SELECT DISTINCT hrassess_custom_items.customitemunkid FROM hrcompetency_customitem_tran WITH (NOLOCK) " & _
                           " JOIN hrassess_custom_items WITH (NOLOCK) ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
                           " JOIN hrassess_custom_headers WITH (NOLOCK) ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                       "WHERE analysisunkid = @analysisunkid AND isvoid = 0 " & _
                       "AND hrassess_custom_headers.isactive = 1 AND hrassess_custom_items.periodunkid = @periodunkid AND hrassess_custom_items.isactive = 1 "

                Select Case iMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        StrQ &= "AND viewmodeid IN (0," & CInt(iMode) & ") "
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        StrQ &= "AND viewmodeid IN (" & CInt(iMode) & ") "
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        StrQ &= "AND viewmodeid IN (" & CInt(iMode) & ") "
                End Select

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAnalysisId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

                iAssessCount = objDataOperation.RecordCount(StrQ)

                    'S.SANDEEP |03-MAY-2021| -- START
                    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                    If blnIsPDPIntegrated Then
                        Dim objPDPFrm As New clspdpform_master
                        If objPDPFrm.IsPDPFormExists(intEmployeeId, 0, 0) Then
                            StrQ = "SELECT DISTINCT pdpitem_master.itemunkid FROM pdpitemdatatran WITH (NOLOCK) JOIN pdpitem_master WITH (NOLOCK) ON pdpitemdatatran.itemunkid = pdpitem_master.itemunkid JOIN pdpcategory_master WITH (NOLOCK) ON pdpitem_master.categoryunkid = pdpcategory_master.categoryunkid WHERE isincludeinpm = 1 AND ismandatory = 1 AND pdpcategory_master.isactive = 1 AND pdpitem_master.isactive = 1 AND isvoid = 0 AND analysisunkid = @analysisunkid "

                            Select Case iMode
                                Case enAssessmentMode.SELF_ASSESSMENT
                                    StrQ &= "AND pdpitem_master.visibletoid IN (0," & CInt(iMode) & ") "
                                Case enAssessmentMode.APPRAISER_ASSESSMENT
                                    StrQ &= "AND pdpitem_master.visibletoid IN (" & CInt(iMode) & ") "
                                Case enAssessmentMode.REVIEWER_ASSESSMENT
                                    StrQ &= "AND pdpitem_master.visibletoid IN (" & CInt(iMode) & ") "
                            End Select

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAnalysisId)
                            iAssessCount = iAssessCount + objDataOperation.RecordCount(StrQ)
                        End If
                        objPDPFrm = Nothing
                    End If
                    'S.SANDEEP |03-MAY-2021| -- END

                End If

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iTotCount <> iAssessCount Then
                    StrMsg = Language.getMessage(mstrModuleName, 165, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some custom header(s).")
                    Exit Try
                End If

                'If strCoyGrpName = "NMB PLC" Then
                '    Select Case iMode
                '        Case enAssessmentMode.SELF_ASSESSMENT
                '            StrQ &= "AND viewmodeid IN (0," & CInt(iMode) & ") "
                '        Case enAssessmentMode.APPRAISER_ASSESSMENT
                '            StrQ &= "AND viewmodeid IN (" & CInt(iMode) & ") "
                '        Case enAssessmentMode.REVIEWER_ASSESSMENT
                '            StrQ &= "AND viewmodeid IN (" & CInt(iMode) & ") "
                '    End Select
                'Else
                '    StrQ &= "AND viewmodeid IN (0," & CInt(iMode) & ") "
                'End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidToSubmitAssessment; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function

    Public Function IsValidCustomValue(ByVal intCustomId As Integer, _
                                       ByVal strCustomValue As String, _
                                       ByVal intEmployeeId As Integer, _
                                       ByVal intPeriodId As Integer) As String

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim StrMsg As String = "" : Dim strGUID As String = ""
        Try

            StrQ = "SELECT @customanalysistranguid = customanalysistranguid FROM hrcompetency_customitem_tran WITH (NOLOCK) WHERE isvoid = 0 AND customitemunkid = @customitemunkid AND custom_value = @custom_value AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGUID, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCustomId)
            objDataOperation.AddParameter("@custom_value", SqlDbType.NVarChar, strCustomValue.Length, strCustomValue)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            strGUID = objDataOperation.GetParameterValue("@customanalysistranguid")


            objDataOperation.ClearParameters()
            StrQ = "SELECT 1 FROM hrcompetency_customitem_tran WITH (NOLOCK) WHERE isvoid = 0 AND customitemunkid = @customitemunkid AND custom_value = @custom_value AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

            If strGUID.Trim.Length > 0 Then
                StrQ &= " AND customanalysistranguid <> @customanalysistranguid "
                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, strGUID.Length, strGUID)
            End If


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCustomId)
            objDataOperation.AddParameter("@custom_value", SqlDbType.NVarChar, strCustomValue.Length, strCustomValue)

            Dim iTotCount As Integer = -1
            iTotCount = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iTotCount > 0 Then
                StrMsg = Language.getMessage(mstrModuleName, 166, "Sorry, you cannot same custom value again for same custom item.")
                Exit Try
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidCustomValue; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function


    


    'Gajanan [17-July-2020] -- Start
    'Enhancement: #0004797
    Public Function IsValidCustomValue(ByVal mList As List(Of clscompeteny_customitem_tran), _
                                       ByVal intEmployeeId As Integer, _
                                       ByVal intPeriodId As Integer, _
                                       ByVal isEdit As Boolean, _
                                       ByVal strTranguid As String _
                                       ) As Boolean

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim StrMsg As String = "" : Dim strGUID As String = ""
        Dim Count As Integer = 0

        Try

            objDataOperation.ClearParameters()
            Dim dsList As DataSet = Nothing

            StrQ = "SELECT a.customanalysistranguid,COUNT(a.customanalysistranguid) from ( "
            For i As Integer = 0 To mList.Count - 1

                If i > 0 Then
                    StrQ &= " UNION all "
                End If

                StrQ &= " SELECT customanalysistranunkid,customanalysistranguid,employeeunkid from hrcompetency_customitem_tran where isvoid = 0 and customitemunkid = @customitemunkid_" & i & " AND custom_value = @custom_value_" & i & " AND employeeunkid =@employeeunkid AND periodunkid=@periodunkid "
                objDataOperation.AddParameter("@customitemunkid_" & i, SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mList(i)._icustomitemunkid.ToString()))
                objDataOperation.AddParameter("@custom_value_" & i, SqlDbType.NVarChar, mList(i).ToString().Length, mList(i)._icustom_value.ToString())

            Next

            StrQ &= ") as a " & _
                    "GROUP by a.customanalysistranguid " & _
                    "HAVING a.customanalysistranguid <> @customanalysistranguid  AND  COUNT(a.customanalysistranguid) >= " & mList.Count & " "



                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, strTranguid.Length, strTranguid)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables(0).Rows.Count > 0 Then
                Return False
                        End If
            Return True
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    If isEdit Then
            '        If dsList.Tables(0).Rows(0)("customanalysistranguid").ToString() <> mList(i)._icustomanalysistranguid.ToString() Then
            '            Count = Count + 1
            '        End If
            '    Else
            '        Count = Count + 1
            '    End If
            'End If

            'If Count <> mList.Count Then
            '    Return True
            'End If

            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidCustomValue; Module Name: " & mstrModuleName)
        Finally
        End Try

    End Function
    'Gajanan [17-July-2020] -- End

    'S.SANDEEP |18-JAN-2020| -- END

    'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Public Function IsValidPeriodSelected(ByVal intPeriodId As Integer, _
                                          ByVal eVTypeId As enPAEvalTypeId, _
                                          Optional ByVal xDataOpr As clsDataOperation = Nothing) As String
        Dim StrQ As String = ""
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim dsList As New DataSet
        Dim strMsg As String = ""
        Try
            StrQ = "SELECT TOP 1 evaltypeid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND periodunkid = @periodunkid "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim ieVType As Integer = 0
                ieVType = dsList.Tables(0).Rows(0)("evaltypeid")
                If ieVType <> eVTypeId Then
                    Select Case ieVType
                        Case enPAEvalTypeId.EO_SCORE_CARD
                            strMsg = Language.getMessage(mstrModuleName, 167, "Sorry, you cannot proceed with competence assessment for the selected period. Reason: Selected period has been used for Balance score card.")
                        Case enPAEvalTypeId.EO_COMPETENCE
                            strMsg = Language.getMessage(mstrModuleName, 168, "Sorry, you cannot proceed with competence assessment for the selected period. Reason: Selected period has been used for Competence.")
                    End Select
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidPeriodSelected; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function

    Public Function IsDataInserted(ByVal intCompanyId As Integer) As String
        Dim StrQ As String = ""
        Dim strMsg As String = ""
        Dim dsLst As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "DECLARE @CoyId AS INT, @DBName AS NVARCHAR(100), @YrId AS INT " & _
                       "SET @CoyId = '" & intCompanyId & "' ; SET @DBName = ''; SET @YrId = 0 " & _
                       "SELECT @DBName = [database_name], @YrId = yearunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = @CoyId AND isclosed = 0 " & _
                       "IF @DBName <> '' AND @YrId > 0 " & _
                       "BEGIN " & _
                       "    DECLARE @Qry AS NVARCHAR(MAX); SET @Qry = '' " & _
                       "    SET @Qry = 'SELECT DISTINCT period_name " & _
                       "                FROM '+ @DBName +'..hrevaluation_analysis_master " & _
                       "                    JOIN '+ @DBName +'..cfcommon_period_tran ON  '+ @DBName +'..hrevaluation_analysis_master.periodunkid = '+ @DBName +'..cfcommon_period_tran.periodunkid " & _
                       "                    AND yearunkid = '+ CAST(@YrId AS NVARCHAR(10)) +' AND modulerefid = 5 AND isactive = 1 " & _
                       "                WHERE isvoid = 0 AND evaltypeid = " & CInt(enPAEvalTypeId.EO_COMPETENCE) & "' " & _
                       "    EXEC(@Qry) " & _
                       "END "
                dsLst = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
                Dim StrPeriods As String = ""
                If dsLst.Tables(0).Rows.Count > 0 Then
                    StrPeriods = String.Join(",", dsLst.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("period_name")).ToArray())
                End If

                If StrPeriods.Trim.Length > 0 Then
                    strMsg = Language.getMessage(mstrModuleName, 170, "Sorry, you cannot revert this setting. Reason: Peformance Assessment has been done for the following periods.") & " [" & StrPeriods & "] " & vbCrLf & _
                             Language.getMessage(mstrModuleName, 171, "If you want to revert this setting. Please void the existing assessment for the mentioned periods.")
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDataInserted; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function
    'S.SANDEEP |13-NOV-2020| -- END

    'S.SANDEEP |04-JAN-2021| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Public Function GetEvalPeriodIds(ByVal eVTypeId As enPAEvalTypeId, _
                                     Optional ByVal xDataOpr As clsDataOperation = Nothing) As String
        Dim StrQ As String = ""
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim dsList As New DataSet
        Dim strPids As String = ""
        Try
            StrQ = "SELECT DISTINCT periodunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND evaltypeid <> @evaltypeid "

            objDataOperation.AddParameter("@evaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eVTypeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strPids = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("periodunkid").ToString()).ToArray())
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEvalPeriodIds", mstrModuleName)
        Finally
        End Try
        Return strPids
    End Function
    'S.SANDEEP |04-JAN-2021| -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.

    'S.SANDEEP |12-APR-2021| -- START
    'ISSUE/ENHANCEMENT : AVG CMP SCORE
    Public Function Get_GE_Evaluation_Data_ALL(ByVal iMode As enAssessmentMode, _
                                               ByVal iData As DataTable, _
                                               ByVal iSelfAssign As Boolean, _
                                               ByVal iAssessorId As Integer, _
                                               ByVal iReviewerId As Integer, _
                                               ByVal iEmployeeAsOnDate As DateTime, _
                                               ByVal intPeriodId As Integer, _
                                               ByVal iDataBaseName As String, _
                                               Optional ByVal blnOnlyCommittedScore As Boolean = False, _
                                               Optional ByVal blnAddAvgCategoryScore As Boolean = False) As DataTable
        Dim dsCollection As New DataSet
        Try
            Dim mdtFinal As DataTable = Nothing
            iData = New DataView(iData, "employeeunkid > 0", "", DataViewRowState.CurrentRows).ToTable
            For Each iR As DataRow In iData.Rows
                mdtFinal = Get_GE_Evaluation_Data(iMode, iR("employeeunkid"), iR("periodunkid"), iAssessorId, iReviewerId, False, -1, iSelfAssign, iEmployeeAsOnDate, iDataBaseName, blnOnlyCommittedScore, blnAddAvgCategoryScore)

                mdtFinal.Columns.Add("assessor", System.Type.GetType("System.String")).DefaultValue = iR("assessor")
                mdtFinal.Columns.Add("reviewer", System.Type.GetType("System.String")).DefaultValue = iR("reviewer")
                mdtFinal.Columns.Add("viewmode", System.Type.GetType("System.String")).DefaultValue = iR("viewmode")
                mdtFinal.Columns.Add("year", System.Type.GetType("System.String")).DefaultValue = iR("year")
                mdtFinal.Columns.Add("period", System.Type.GetType("System.String")).DefaultValue = iR("period")
                mdtFinal.Columns.Add("assessmentdate", System.Type.GetType("System.String")).DefaultValue = iR("assessmentdate")
                mdtFinal.Columns.Add("sl_score", System.Type.GetType("System.Decimal")).DefaultValue = iR("sl_score")
                mdtFinal.Columns.Add("al_score", System.Type.GetType("System.Decimal")).DefaultValue = iR("al_score")
                mdtFinal.Columns.Add("rl_score", System.Type.GetType("System.Decimal")).DefaultValue = iR("rl_score")
                mdtFinal.Columns.Add("sl_analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("sl_analysisunkid")
                mdtFinal.Columns.Add("al_analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("al_analysisunkid")
                mdtFinal.Columns.Add("rl_analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("rl_analysisunkid")
                mdtFinal.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("employeeunkid")
                mdtFinal.Columns.Add("statusunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("statusunkid")
                mdtFinal.Columns.Add("assessmodeid", System.Type.GetType("System.Int32")).DefaultValue = iR("assessmodeid")
                mdtFinal.Columns.Add("smodeid", System.Type.GetType("System.Int32")).DefaultValue = iR("smodeid")
                mdtFinal.Columns.Add("iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = iR("iscommitted")
                mdtFinal.Columns.Add("committeddatetime", System.Type.GetType("System.String")).DefaultValue = iR("committeddatetime")
                mdtFinal.Columns.Add("assessormasterunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("assessormasterunkid")
                mdtFinal.Columns.Add("assessoremployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("assessoremployeeunkid")
                mdtFinal.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("userunkid")
                mdtFinal.Columns.Add("reviewerunkid", System.Type.GetType("System.Int32")).DefaultValue = iR("reviewerunkid")
                mdtFinal.Columns.Add("operationviewid", System.Type.GetType("System.Int32")).DefaultValue = iR("operationviewid")
                mdtFinal.Columns.Add("sl_iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = iR("sl_iscommitted")
                mdtFinal.Columns.Add("al_iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = iR("al_iscommitted")
                mdtFinal.Columns.Add("rl_iscommitted", System.Type.GetType("System.Boolean")).DefaultValue = iR("rl_iscommitted")
                mdtFinal.Columns.Add("sl_committeddatetime", System.Type.GetType("System.String")).DefaultValue = iR("sl_committeddatetime")
                mdtFinal.Columns.Add("al_committeddatetime", System.Type.GetType("System.String")).DefaultValue = iR("al_committeddatetime")
                mdtFinal.Columns.Add("rl_committeddatetime", System.Type.GetType("System.String")).DefaultValue = iR("rl_committeddatetime")
                mdtFinal.Columns.Add("sl_assessmentdate", System.Type.GetType("System.String")).DefaultValue = iR("sl_assessmentdate")
                mdtFinal.Columns.Add("al_assessmentdate", System.Type.GetType("System.String")).DefaultValue = iR("al_assessmentdate")
                mdtFinal.Columns.Add("rl_assessmentdate", System.Type.GetType("System.String")).DefaultValue = iR("rl_assessmentdate")

                Dim xRow As DataRow = mdtFinal.NewRow
                xRow("eval_item") = iR("employee")
                Dim xCol As New DataColumn
                With xCol
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                    .ColumnName = "IsMGrp"
                End With
                mdtFinal.Columns.Add(xCol)
                xRow("IsMGrp") = True
                xRow("iPeriodId") = intPeriodId
                xRow("iEmployeeId") = iR("employeeunkid")
                xRow("periodunkid") = intPeriodId
                mdtFinal.Rows.InsertAt(xRow, 0)
                If dsCollection.Tables.Count > 0 Then
                    dsCollection.Tables(0).Merge(mdtFinal, True)
                Else
                    dsCollection.Tables.Add(mdtFinal.Copy)
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_GE_Evaluation_Data_ALL; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsCollection.Tables(0)
    End Function
    'S.SANDEEP |12-APR-2021| -- END


#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
			Language.setMessage("clsAssess_Field_Master", 14, "Goal Type")
			Language.setMessage("clsAssess_Field_Master", 15, "Goal Value")
			Language.setMessage("clsMasterData", 518, "Pending")
			Language.setMessage("clsMasterData", 519, "In progress")
			Language.setMessage("clsMasterData", 520, "Complete")
			Language.setMessage("clsMasterData", 521, "Closed")
			Language.setMessage("clsMasterData", 522, "On Track")
			Language.setMessage("clsMasterData", 523, "At Risk")
			Language.setMessage("clsMasterData", 524, "Not Applicable")
			Language.setMessage("clsMasterData", 843, "Qualitative")
			Language.setMessage("clsMasterData", 844, "Quantitative")
            Language.setMessage("frmPerformanceEvaluation", 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")
            Language.setMessage("frmPerformanceEvaluation", 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group.")
			Language.setMessage("frmSelfEvaluationList", 7, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
            Language.setMessage(mstrModuleName, 1, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assessment. Reason : Assessor has already assessed this employee.")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this Assessment. Reason : Reviewer has already assessed this employee.")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 6, "End Date")
            Language.setMessage(mstrModuleName, 7, "Status")
            Language.setMessage(mstrModuleName, 10, "Notification for Performance Assessment.")
            Language.setMessage(mstrModuleName, 11, "% Completed")
            Language.setMessage(mstrModuleName, 17, "Notification for Self-Assessment.")
			Language.setMessage(mstrModuleName, 19, "Sorry, you cannot do performance assessment for the selected period. Reason following thing(s) are pending :")
			Language.setMessage(mstrModuleName, 20, "Sorry, you cannot do performance assessment for the selected period and employee. Reason following thing(s) are pending :")
			Language.setMessage(mstrModuleName, 21, "Goals are not defined.")
			Language.setMessage(mstrModuleName, 22, "Competencies are not defined/assigned.")
			Language.setMessage(mstrModuleName, 23, "Items are not defined/planned.")
			Language.setMessage(mstrModuleName, 24, "If you have already planned and it's not approved. Please contact you assessor.")
            Language.setMessage(mstrModuleName, 102, "Company")
            Language.setMessage(mstrModuleName, 103, "Owner")
            Language.setMessage(mstrModuleName, 104, "Wgt.")
            Language.setMessage(mstrModuleName, 105, "Emp. Score")
            Language.setMessage(mstrModuleName, 107, "Emp. Remark")
            Language.setMessage(mstrModuleName, 108, "Sup. Score")
            Language.setMessage(mstrModuleName, 109, "Sup. Remark")
            Language.setMessage(mstrModuleName, 110, "Rev. Score")
            Language.setMessage(mstrModuleName, 111, "Rev. Remark")
            Language.setMessage(mstrModuleName, 112, "Score Guide")
            Language.setMessage(mstrModuleName, 113, "Start Date")
            Language.setMessage(mstrModuleName, 114, "Competencies")
            Language.setMessage(mstrModuleName, 115, "Total Weight Assigned :")
            Language.setMessage(mstrModuleName, 116, "Custom Header")
            Language.setMessage(mstrModuleName, 117, "Dear")
            Language.setMessage(mstrModuleName, 119, "Self Assessment")
            Language.setMessage(mstrModuleName, 120, "Assessor Assessment")
            Language.setMessage(mstrModuleName, 121, "Reviewer Assessment")
            Language.setMessage(mstrModuleName, 122, "], Assessment done by employee. Please set date accordingly.")
            Language.setMessage(mstrModuleName, 123, "Sorry, Assessment date should be greater than or equal to [")
            Language.setMessage(mstrModuleName, 124, "], Assessment done by assessor. Please set date accordingly.")
            Language.setMessage(mstrModuleName, 125, "Objectives/Goals/Targets")
            Language.setMessage(mstrModuleName, 126, "Agreed Score")
			Language.setMessage(mstrModuleName, 127, "Select")
			Language.setMessage(mstrModuleName, 130, "Sorry, Fail to void exsisting computed score.")
			Language.setMessage(mstrModuleName, 131, "Edit Assessment.")
			Language.setMessage(mstrModuleName, 132, "Regards,")
			Language.setMessage(mstrModuleName, 133, "Please note that, I have completed my Self-Assessment for the period of")
			Language.setMessage(mstrModuleName, 134, "Click the link below for comments.")
			Language.setMessage(mstrModuleName, 136, "Notification for one on one performance discussion.")
			Language.setMessage(mstrModuleName, 137, "Please note that, your self-performance assessment for the period")
			Language.setMessage(mstrModuleName, 138, "has been submitted to your line manager.")
			Language.setMessage(mstrModuleName, 139, "Please note that, performance assessment for")
			Language.setMessage(mstrModuleName, 141, "is complete.")
			Language.setMessage(mstrModuleName, 142, "Please click the link below to review it.")
			Language.setMessage(mstrModuleName, 144, "Please note that, performance assessment for the Period")
			Language.setMessage(mstrModuleName, 145, "is complete.")
			Language.setMessage(mstrModuleName, 146, "Login to Aruti Employee Self Service to view it in detail or click the link below for a summary report.")
            Language.setMessage(mstrModuleName, 150, "Furthermore, be reminded to book a one on one performance discussion with your line manager.")
			Language.setMessage(mstrModuleName, 160, "for the period of")
			Language.setMessage(mstrModuleName, 161, "Sorry, you cannot start with assessment. Reason, Update progress of goals is not done yet.")
			Language.setMessage(mstrModuleName, 162, "Sorry, you cannot start with assessment. Reason, Update progress of goals is in pending state.")
			Language.setMessage(mstrModuleName, 163, "Sorry, you cannot start with assessment. Reason, Update progress of goals is in disapproved state.")
			Language.setMessage(mstrModuleName, 164, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Calibration.")
            Language.setMessage(mstrModuleName, 165, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some custom header(s).")
            Language.setMessage(mstrModuleName, 166, "Sorry, you cannot same custom value again for same custom item.")
            Language.setMessage(mstrModuleName, 167, "Sorry, you cannot proceed with competence assessment for the selected period. Reason: Selected period has been used for Balance score card.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

' If IsBalanceScoreCard = True Then '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' BSC COMPUTATIONS
'                    For Each xRow As DataRow In xTable.Rows
'                        If xTable.Columns.Contains("AUD") = True Then
'                            If xRow.Item("AUD") = "D" Then Continue For
'                        End If
'                        If blnSummary = True Then Exit For
'Dim xColName As String = "iScore"
'                        If xDataTable IsNot Nothing Then
'                            Select Case xAssessMode
'                                Case enAssessmentMode.SELF_ASSESSMENT
'                                    If xTable.Columns.Contains("escore") = True Then
'                                        xColName = "escore"
'                                    End If
'                                Case enAssessmentMode.APPRAISER_ASSESSMENT
'                                    If xTable.Columns.Contains("ascore") = True Then
'                                        xColName = "ascore"
'                                    End If
'                                Case enAssessmentMode.REVIEWER_ASSESSMENT
'                                    If xTable.Columns.Contains("rscore") = True Then
'                                        xColName = "rscore"
'                                    End If
'                            End Select
'                        End If

'                        xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xFormulaRef)

'                        Using objDo As New clsDataOperation
'                            StrQ = "SELECT " & _
'                                   "  computation_typeid " & _
'                                   "FROM hrassess_computation_tran " & _
'                                   "  JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_computation_tran.computationunkid " & _
'                                   "WHERE hrassess_computation_tran.isvoid = 0 " & _
'                                   "  AND hrassess_computation_master.isvoid = 0 AND periodunkid = '" & xRow.Item("iPeriodId") & "' AND computation_formula = '" & xFormulaRef & "' "

'                            xList = objDo.ExecQuery(StrQ, "List")

'                            If objDo.ErrorMessage <> "" Then
'                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                            End If
'Dim xView As DataView = New DataView(xList.Tables(0), "computation_typeid IN(" & enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO & "," & enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO & "," & enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO & ")", "", DataViewRowState.CurrentRows)
'                            If xView.ToTable.Rows.Count > 0 Then
'                                blnSummary = True
'                            End If
'                        End Using

'                        xTotalScore = xTotalScore + xScore
'                    Next
'                Else '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''' COMPETENCY COMPUTATIONS
'                    For Each xRow As DataRow In xTable.Rows
'                        If blnSummary = True Then Exit For

'                        If xTable.Columns.Contains("AUD") = True Then
'                            If xRow.Item("AUD") = "D" Then Continue For
'                        End If

'Dim xColName As String = "iScore"
'                        If xDataTable IsNot Nothing Then
'                            Select Case xAssessMode
'                                Case enAssessmentMode.SELF_ASSESSMENT
'                                    If xTable.Columns.Contains("escore") = True Then
'                                        xColName = "escore"
'                                    End If
'                                Case enAssessmentMode.APPRAISER_ASSESSMENT
'                                    If xTable.Columns.Contains("ascore") = True Then
'                                        xColName = "ascore"
'                                    End If
'                                Case enAssessmentMode.REVIEWER_ASSESSMENT
'                                    If xTable.Columns.Contains("rscore") = True Then
'                                        xColName = "rscore"
'                                    End If
'                            End Select
'                        End If

'                        xScore = objCompute.Process_Assessment_Formula(xCompute_Formula, xRow.Item("iPeriodId"), xRow.Item("iItemUnkid"), xRow.Item("iEmployeeId"), xScoreOptId, xRow.Item(xColName), xAssessMode, xFormulaRef, xRow("assessgroupunkid"))

'                        Using objDo As New clsDataOperation
'                            StrQ = "SELECT " & _
'                                   "  computation_typeid " & _
'                                   "FROM hrassess_computation_tran " & _
'                                   "  JOIN hrassess_computation_master ON hrassess_computation_master.computationunkid = hrassess_computation_tran.computationunkid " & _
'                                   "WHERE hrassess_computation_tran.isvoid = 0 " & _
'                                   "  AND hrassess_computation_master.isvoid = 0 AND periodunkid = '" & xRow.Item("iPeriodId") & "' AND computation_formula = '" & xFormulaRef & "' "

'                            xList = objDo.ExecQuery(StrQ, "List")

'                            If objDo.ErrorMessage <> "" Then
'                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                            End If
'Dim xView As DataView = New DataView(xList.Tables(0), "computation_typeid IN(" & enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO & "," & enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO & "," & enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO & ")", "", DataViewRowState.CurrentRows)
'                            If xView.ToTable.Rows.Count > 0 Then
'                                blnSummary = True
'                            End If
'                        End Using

'                        xTotalScore = xTotalScore + xScore
'                    Next
'                End If
'            End If