﻿'************************************************************************************************************************************
'Class Name :clsassess_coyowner_tran.vb
'Purpose    :
'Date       :24-Apr-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_coyowner_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsassess_coyowner_tran"
    Private mstrMessage As String = ""
    Private mintCoyOwnertranunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _DatTable() As DataTable
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function Get_Data(ByVal iCoyFieldUnkid As Integer, ByVal iCoyFieldTypeId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "  ownertranunkid " & _
                   ", coyfieldunkid " & _
                   ", allocationid " & _
                   ", coyfieldtypeid " & _
                   ", '' AS AUD " & _
                   ", '' AS GUID " & _
                   "FROM hrassess_coyowner_tran " & _
                   "WHERE coyfieldunkid = @coyfieldunkid AND coyfieldtypeid = @coyfieldtypeid "

            objDataOpr.AddParameter("@coyfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldUnkid.ToString)
            objDataOpr.AddParameter("@coyfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function InsertDelete_Allocation(ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer, ByVal iCoyFieldUnkid As Integer, ByVal iCoyFieldTypeId As Integer) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOpr.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassess_coyowner_tran ( " & _
                                           "  coyfieldunkid " & _
                                           ", allocationid " & _
                                           ", coyfieldtypeid" & _
                                       ") VALUES (" & _
                                           "  @coyfieldunkid " & _
                                           ", @allocationid " & _
                                           ", @coyfieldtypeid" & _
                                       "); SELECT @@identity"

                                objDataOpr.AddParameter("@coyfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldUnkid)
                                objDataOpr.AddParameter("@coyfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldTypeId)
                                objDataOpr.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allocationid"))

                                Dim dsList As New DataSet
                                dsList = objDataOpr.ExecQuery(StrQ, "List")

                                If objDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                mintCoyOwnertranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOpr, 1, "hrassess_coyowner_tran", "ownertranunkid", mintCoyOwnertranunkid, , iUserId) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Case "D"

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOpr, 3, "hrassess_coyowner_tran", "ownertranunkid", .Item("ownertranunkid"), , iUserId) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                                StrQ = "DELETE FROM hrassess_coyowner_tran WHERE ownertranunkid = @ownertranunkid "

                                objDataOpr.AddParameter("@ownertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ownertranunkid"))

                                Call objDataOpr.ExecNonQuery(StrQ)

                                If objDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDelete_Allocation", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetCSV_OwnerName(ByVal iAllocationRefId As Integer, ByVal iCoyFieldId As Integer, ByVal iCoyFieldTypeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwners As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            If iAllocationRefId <= 0 Then Return ""

            Select Case iAllocationRefId
                Case enAllocation.BRANCH
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.DEPARTMENT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.DEPARTMENT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.SECTION_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.SECTION
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.UNIT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.UNIT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.TEAM
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.JOB_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.JOBS
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(job_name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.CLASS_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.CLASSES
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(name AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "
            End Select

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iOwners = dsList.Tables(0).Rows(0).Item("CSV")
            End If

            Return iOwners

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_OwnerName", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function GetCSV_OwnerIds(ByVal iAllocationRefId As Integer, ByVal iCoyFieldId As Integer, ByVal iCoyFieldTypeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwners As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            If iAllocationRefId <= 0 Then Return ""

            Select Case iAllocationRefId
                Case enAllocation.BRANCH
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(stationunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.DEPARTMENT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(deptgroupunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.DEPARTMENT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(departmentunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.SECTION_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(sectiongroupunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.SECTION
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(sectionunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.UNIT_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(unitgroupunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.UNIT
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(unitunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.TEAM
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(teamunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.JOB_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(jobgroupunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.JOBS
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(jobunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.CLASS_GROUP
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(classgroupunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                            " JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_coyowner_tran.allocationid " & _
                            "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

                Case enAllocation.CLASSES
                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(classesunkid AS NVARCHAR(MAX)) FROM hrassess_coyowner_tran " & _
                           " JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_coyowner_tran.allocationid " & _
                           "WHERE coyfieldunkid = '" & iCoyFieldId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "
            End Select

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iOwners = dsList.Tables(0).Rows(0).Item("CSV")
            End If

            Return iOwners

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_OwnerName", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function GetCSV_CoyFieldTableIds(ByVal iOwnerId As Integer, ByVal iCoyFieldTypeId As Integer, Optional ByVal iContionalString As Boolean = False) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwners As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try

            StrQ = "SELECT ISNULL(STUFF((SELECT ',' + '''' + CAST(coyfieldunkid AS NVARCHAR(MAX)) + '''' FROM hrassess_coyowner_tran " & _
                   "WHERE allocationid = '" & iOwnerId & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0)("CSV").ToString().Trim.Length > 0 Then
                If iContionalString = True Then
                    Select Case iCoyFieldTypeId
                        Case enWeight_Types.WEIGHT_FIELD1
                            iOwners = "AND coyfield1unkid IN(" & dsList.Tables(0).Rows(0).Item("CSV") & ")"
                        Case enWeight_Types.WEIGHT_FIELD2
                            iOwners = "AND coyfield2unkid IN(" & dsList.Tables(0).Rows(0).Item("CSV") & ")"
                        Case enWeight_Types.WEIGHT_FIELD3
                            iOwners = "AND coyfield3unkid IN(" & dsList.Tables(0).Rows(0).Item("CSV") & ")"
                        Case enWeight_Types.WEIGHT_FIELD4
                            iOwners = "AND coyfield4unkid IN(" & dsList.Tables(0).Rows(0).Item("CSV") & ")"
                        Case enWeight_Types.WEIGHT_FIELD5
                            iOwners = "AND coyfield5unkid IN(" & dsList.Tables(0).Rows(0).Item("CSV") & ")"
                    End Select
                Else
                    iOwners = dsList.Tables(0).Rows(0).Item("CSV")
                End If
            End If
            End If

            Return iOwners

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_OwnerName", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function Get_AllOwner(ByVal iCoyFieldTypeId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "  ownertranunkid " & _
                   ", coyfieldunkid " & _
                   ", allocationid " & _
                   ", coyfieldtypeid " & _
                   "FROM hrassess_coyowner_tran " & _
                   "WHERE coyfieldtypeid = @coyfieldtypeid "

            objDataOpr.AddParameter("@coyfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_AllOwner", mstrModuleName)
            Return Nothing
        Finally
        End Try
        Return dsList
    End Function

#End Region

End Class
