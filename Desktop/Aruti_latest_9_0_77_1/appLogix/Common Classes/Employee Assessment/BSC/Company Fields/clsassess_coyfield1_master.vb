﻿'************************************************************************************************************************************
'Class Name :clsassess_coyfield1_master.vb
'Purpose    :
'Date       :24-Apr-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_coyfield1_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_coyfield1_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCoyfield1unkid As Integer = 0
    Private mintPerspectiveunkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mintPeriodunkid As Integer = 0
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintOwnerrefid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mdblWeight As Double = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintCoyFieldTypeId As Integer = 0

#End Region

#Region " Properties "


    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coyfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Coyfield1unkid() As Integer
        Get
            Return mintCoyfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintCoyfield1unkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perspectiveunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Perspectiveunkid() As Integer
        Get
            Return mintPerspectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintPerspectiveunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ownerrefid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ownerrefid() As Integer
        Get
            Return mintOwnerrefid
        End Get
        Set(ByVal value As Integer)
            mintOwnerrefid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set coyfieldtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _CoyFieldTypeId() As Integer
        Set(ByVal value As Integer)
            mintCoyFieldTypeId = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  coyfield1unkid " & _
              ", perspectiveunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", periodunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", ownerrefid " & _
              ", userunkid " & _
              ", weight " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
             "FROM hrassess_coyfield1_master " & _
             "WHERE coyfield1unkid = @coyfield1unkid "

            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield1unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCoyfield1unkid = CInt(dtRow.Item("coyfield1unkid"))
                mintPerspectiveunkid = CInt(dtRow.Item("perspectiveunkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mstrField_Data = dtRow.Item("field_data").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                If IsDBNull(dtRow.Item("startdate")) = False Then
                    mdtStartdate = dtRow.Item("startdate")
                Else
                    mdtStartdate = Nothing
                End If
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintOwnerrefid = CInt(dtRow.Item("ownerrefid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mdblWeight = CDbl(dtRow.Item("weight"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                mstrVoidreason = dtRow.Item("voidreason").ToString

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       " coyfield1unkid " & _
                       ",perspectiveunkid " & _
                       ",fieldunkid " & _
                       ",field_data " & _
                       ",periodunkid " & _
                       ",startdate " & _
                       ",enddate " & _
                       ",statusunkid " & _
                       ",ownerrefid " & _
                       ",userunkid " & _
                       ",weight " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidreason " & _
                       ",voiddatetime " & _
                   "FROM hrassess_coyfield1_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Insert(Optional ByVal mdtOwner As DataTable = Nothing, Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing) As Boolean
        If isExist(mstrField_Data, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@ownerrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerrefid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Length, mstrVoidreason.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "INSERT INTO hrassess_coyfield1_master ( " & _
              "  perspectiveunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", periodunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", ownerrefid " & _
              ", userunkid " & _
              ", weight " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime" & _
            ") VALUES (" & _
              "  @perspectiveunkid " & _
              ", @fieldunkid " & _
              ", @field_data " & _
              ", @periodunkid " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @statusunkid " & _
              ", @ownerrefid " & _
              ", @userunkid " & _
              ", @weight " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidreason " & _
              ", @voiddatetime" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCoyfield1unkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_coyfield1_master", "coyfield1unkid", mintCoyfield1unkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_coyowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Allocation(objDataOperation, mintUserunkid, mintCoyfield1unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_coyinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintCoyfield1unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Update(Optional ByVal mdtOwner As DataTable = Nothing, Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing) As Boolean
        If isExist(mstrField_Data, mintPeriodunkid, mintCoyfield1unkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)
            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield1unkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@ownerrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerrefid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Length, mstrVoidreason.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            strQ = "UPDATE hrassess_coyfield1_master SET " & _
                   "  perspectiveunkid = @perspectiveunkid" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", field_data = @field_data" & _
                   ", periodunkid = @periodunkid" & _
                   ", startdate = @startdate" & _
                   ", enddate = @enddate" & _
                   ", statusunkid = @statusunkid" & _
                   ", ownerrefid = @ownerrefid" & _
                   ", userunkid = @userunkid" & _
                   ", weight = @weight" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE coyfield1unkid = @coyfield1unkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_coyfield1_master", mintCoyfield1unkid, "coyfield1unkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_coyfield1_master", "coyfield1unkid", mintCoyfield1unkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_coyowner_tran
                objOwner._DatTable = mdtOwner
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Allocation(objDataOperation, mintUserunkid, mintCoyfield1unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_coyinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintCoyfield1unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objCoyField2 As New clsassess_coyfield2_master

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_coyfield1_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE coyfield1unkid = @coyfield1unkid "

            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_coyfield1_master", "coyfield1unkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Dim dsChild As New DataSet : Dim dtmp As DataRow() = Nothing
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_coyfield2_master", "coyfield1unkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("isvoid = 0")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    objCoyField2._Isvoid = mblnIsvoid
                    objCoyField2._Voiddatetime = mdtVoiddatetime
                    objCoyField2._Voidreason = mstrVoidreason
                    objCoyField2._Voiduserunkid = mintVoiduserunkid

                    objCoyField2._FormName = mstrFormName
                    objCoyField2._LoginEmployeeunkid = mintLoginEmployeeunkid
                    objCoyField2._ClientIP = mstrClientIP
                    objCoyField2._HostName = mstrHostName
                    objCoyField2._FromWeb = mblnIsWeb
                    objCoyField2._AuditUserId = mintAuditUserId
                    objCoyField2._CompanyUnkid = mintCompanyUnkid
                    objCoyField2._AuditDate = mdtAuditDate

                    If objCoyField2.Delete(dr.Item("coyfield2unkid"), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_coyinfofield_tran", "coyfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_coyinfofield_tran", "coyinfofieldunkid", dr.Item("coyinfofieldunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
                strQ = "DELETE FROM hrassess_coyinfofield_tran WHERE coyfieldunkid = '" & intUnkid & "' AND coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_coyowner_tran", "coyfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_coyowner_tran", "ownertranunkid", dr.Item("ownertranunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
                strQ = "DELETE FROM hrassess_coyowner_tran WHERE coyfieldunkid = '" & intUnkid & "' AND coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            '==================|ENDING VOIDING GOAL OWNSER DATA IF MAPPED|==============='

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT owrfield1unkid FROM hrassess_owrfield1_master WHERE coyfield1unkid = '" & intUnkid & "' AND isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean 'S.SANDEEP [ 10 JAN 2015 ] -- START {iPeriodId} -- END
        'Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  coyfield1unkid " & _
              ", perspectiveunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", periodunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", ownerrefid " & _
              ", userunkid " & _
              ", weight " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
             "FROM hrassess_coyfield1_master " & _
             "WHERE field_data = @field_data " & _
             "AND isvoid = 0 " & _
             "AND periodunkid = @periodunkid " 'S.SANDEEP [ 10 JAN 2015 ] -- START {iPeriodId} -- END
            If intUnkid > 0 Then
                strQ &= " AND coyfield1unkid <> @coyfield1unkid"
            End If

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, strName.Length, strName)
            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'S.SANDEEP [ 10 JAN 2015 ] -- START
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            'S.SANDEEP [ 10 JAN 2015 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get Data Set for Combo </purpose>
    Public Function getComboList(ByVal iPeriodId As Integer, Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False, Optional ByVal iOnlyCommit As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT coyfield1unkid AS Id, field_data AS Name FROM hrassess_coyfield1_master WHERE isvoid = 0 " & _
                        "AND periodunkid = '" & iPeriodId & "' "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtTab As DataTable = Nothing
            If iOnlyCommit = True Then
                Dim iLastStatusId As Integer = GetLastStatus(iPeriodId)
                If iLastStatusId <> enObjective_Status.FINAL_COMMITTED Then
                    mdtTab = New DataView(dsList.Tables(0), "Id IN(0)", "", DataViewRowState.CurrentRows).ToTable
                    dsList.Tables.RemoveAt(0) : dsList.Tables.Add(mdtTab.Copy)
                End If
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get DataTable for Display </purpose>
    Public Function GetDisplayList(ByVal iPeriodId As Integer, Optional ByVal iList As String = "") As DataTable
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn = Nothing
        objDataOperation = New clsDataOperation
        Dim iCaptionName As String = String.Empty
        Try
            If iList.Trim.Length <= 0 Then iList = "List"
            mdtFinal = New DataTable(iList)
            dCol = New DataColumn
            dCol.ColumnName = "Perspective"
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "Perspective")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE))
            mdtFinal.Columns.Add(dCol)
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping(iList)
            Dim objMap As New clsAssess_Field_Mapping
            iCaptionName = objMap.Get_Map_FieldName(iPeriodId)
            objMap = Nothing
            Dim i As Integer = 1
            If dsList.Tables(iList).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(iList).Rows
                    dCol = New DataColumn
                    If i <= 5 Then
                        dCol.ColumnName = "Field" & i.ToString
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString
                    End If
                    dCol.Caption = dRow.Item("fieldcaption").ToString
                    dCol.DataType = System.Type.GetType("System.String")
                    dCol.DefaultValue = ""
                    dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(dRow.Item("fieldunkid")))

                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    If i <= 5 Then
                        dCol.ColumnName = "Field" & i.ToString & "Id"
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString & "Id"
                    End If
                    dCol.DataType = System.Type.GetType("System.Int32")
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    mdtFinal.Columns.Add(dCol)
                    i += 1
                Next
            End If

            dCol = New DataColumn
            dCol.ColumnName = "St_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "Start Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.ST_DATE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Ed_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "End Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.ED_DATE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatus"
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Status")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.STATUS))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Owner"
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "Goal Owner")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatusId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "COwnerId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Weight"
            'S.SANDEEP [ 16 JAN 2015 ] -- START
            'dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 6, "Weight")
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "Weight")
            'S.SANDEEP [ 16 JAN 2015 ] -- END
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.WEIGHT))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "coyfield1unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "coyfield2unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "coyfield3unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "coyfield4unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "coyfield5unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "perspectiveunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "sdate"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "edate"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "opstatusid"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "OwnerIds"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CFieldTypeId"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            StrQ = "DECLARE @LinkFieldId AS INT " & _
                    "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WHERE periodunkid = '" & iPeriodId & "' AND isactive = 1),0) " & _
                    "SELECT DISTINCT " & _
                    "    Perspective " & _
                    "	,Field1 " & _
                    "	,Field2 " & _
                    "	,Field3 " & _
                    "	,Field4 " & _
                    "	,Field5 " & _
                    "	,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_SDate " & _
                    "				 WHEN @LinkFieldId = Field2Id THEN f2_SDate " & _
                    "				 WHEN @LinkFieldId = Field3Id THEN f3_SDate " & _
                    "				 WHEN @LinkFieldId = Field4Id THEN f4_SDate " & _
                    "				 WHEN @LinkFieldId = Field5Id THEN f5_SDate " & _
                    "			END,'') AS St_Date " & _
                    "	,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_EDate " & _
                    "				 WHEN @LinkFieldId = Field2Id THEN f2_EDate " & _
                    "				 WHEN @LinkFieldId = Field3Id THEN f3_EDate " & _
                    "				 WHEN @LinkFieldId = Field4Id THEN f4_EDate " & _
                    "				 WHEN @LinkFieldId = Field5Id THEN f5_EDate " & _
                    "			END,'') AS Ed_Date " & _
                    "    ,ISNULL(CASE WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "								   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							 END,0) = 1 THEN @ST_PENDING " & _
                    "				  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "					 			   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							 END,0) = 2 THEN @ST_INPROGRESS " & _
                    "				  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "								   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							END,0) = 3 THEN @ST_COMPLETE " & _
                    "				  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "								   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							END,0) = 4 THEN @ST_CLOSED " & _
                    "				  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "								   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							END,0) = 5 THEN @ST_ONTRACK " & _
                    "				  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "								   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							END,0) = 6 THEN @ST_ATRISK " & _
                    "				  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "								   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "								   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "								   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "								   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "							END,0) = 7 THEN @ST_NOTAPPLICABLE " & _
                    "			END,'') AS CStatus " & _
                    "	,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                    "				 WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                    "				 WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                    "				 WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                    "				 WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                    "			END,0) AS CStatusId " & _
                    "	,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1ownerid " & _
                    "				 WHEN @LinkFieldId = Field2Id THEN f2ownerid " & _
                    "				 WHEN @LinkFieldId = Field3Id THEN f3ownerid " & _
                    "				 WHEN @LinkFieldId = Field4Id THEN f4ownerid " & _
                    "				 WHEN @LinkFieldId = Field5Id THEN f5ownerid " & _
                    "			END,0) AS COwnerId " & _
                    "	,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN '" & enWeight_Types.WEIGHT_FIELD1 & "' " & _
                    "				 WHEN @LinkFieldId = Field2Id THEN '" & enWeight_Types.WEIGHT_FIELD2 & "' " & _
                    "				 WHEN @LinkFieldId = Field3Id THEN '" & enWeight_Types.WEIGHT_FIELD3 & "' " & _
                    "				 WHEN @LinkFieldId = Field4Id THEN '" & enWeight_Types.WEIGHT_FIELD4 & "' " & _
                    "				 WHEN @LinkFieldId = Field5Id THEN '" & enWeight_Types.WEIGHT_FIELD5 & "' " & _
                    "			END,0) AS CFieldTypeId " & _
                    " ,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN CASE WHEN f1weight <=0 THEN '' ELSE CAST(f1weight AS NVARCHAR(MAX)) END " & _
                    "			   WHEN @LinkFieldId = Field2Id THEN CASE WHEN f2weight <=0 THEN '' ELSE CAST(f2weight AS NVARCHAR(MAX)) END " & _
                    "			   WHEN @LinkFieldId = Field3Id THEN CASE WHEN f3weight <=0 THEN '' ELSE CAST(f3weight AS NVARCHAR(MAX)) END " & _
                    "			   WHEN @LinkFieldId = Field4Id THEN CASE WHEN f4weight <=0 THEN '' ELSE CAST(f4weight AS NVARCHAR(MAX)) END " & _
                    "			   WHEN @LinkFieldId = Field5Id THEN CASE WHEN f5weight <=0 THEN '' ELSE CAST(f5weight AS NVARCHAR(MAX)) END " & _
                    "		  END,'') AS Weight " & _
                    "	,ISNULL(coyfield1unkid,0) AS coyfield1unkid " & _
                    "	,ISNULL(coyfield2unkid,0) AS coyfield2unkid " & _
                    "	,ISNULL(coyfield3unkid,0) AS coyfield3unkid " & _
                    "	,ISNULL(coyfield4unkid,0) AS coyfield4unkid " & _
                    "	,ISNULL(coyfield5unkid,0) AS coyfield5unkid " & _
                    "	,Field1Id " & _
                    "	,Field2Id " & _
                    "	,Field3Id " & _
                    "	,Field4Id " & _
                    "	,Field5Id " & _
                    "   ,perspectiveunkid " & _
                    "   ,'' AS Owner " & _
                    "   ,'' AS OwnerIds " & _
                    "FROM " & _
                    "( " & _
                    "		SELECT " & _
                    "			 ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid" & _
                    "           ,ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                    "			,ISNULL(hrassess_coyfield1_master.field_data,'') AS Field1 " & _
                    "			,ISNULL(hrassess_coyfield2_master.field_data,'') AS Field2 " & _
                    "			,ISNULL(hrassess_coyfield3_master.field_data,'') AS Field3 " & _
                    "			,ISNULL(hrassess_coyfield4_master.field_data,'') AS Field4 " & _
                    "			,ISNULL(hrassess_coyfield5_master.field_data,'') AS Field5 " & _
                    "			,ISNULL(hrassess_coyfield1_master.fieldunkid,0) AS Field1Id " & _
                    "			,ISNULL(hrassess_coyfield2_master.fieldunkid,0) AS Field2Id " & _
                    "			,ISNULL(hrassess_coyfield3_master.fieldunkid,0) AS Field3Id " & _
                    "			,ISNULL(hrassess_coyfield4_master.fieldunkid,0) AS Field4Id " & _
                    "			,ISNULL(hrassess_coyfield5_master.fieldunkid,0) AS Field5Id " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.startdate,112),'') AS f1_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.enddate,112),'') AS f1_EDate " & _
                    "			,ISNULL(hrassess_coyfield1_master.statusunkid,0) AS f1StatId " & _
                    "			,ISNULL(hrassess_coyfield1_master.ownerrefid,0) AS f1ownerid " & _
                    "			,ISNULL(hrassess_coyfield1_master.weight,0) AS f1weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.startdate,112),'') AS f2_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.enddate,112),'') AS f2_EDate " & _
                    "			,ISNULL(hrassess_coyfield2_master.statusunkid,0) AS f2StatId " & _
                    "			,ISNULL(hrassess_coyfield2_master.ownerrefid,0) AS f2ownerid " & _
                    "			,ISNULL(hrassess_coyfield2_master.weight,0) AS f2weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.startdate,112),'') AS f3_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.enddate,112),'') AS f3_EDate " & _
                    "			,ISNULL(hrassess_coyfield3_master.statusunkid,0) AS f3StatId " & _
                    "			,ISNULL(hrassess_coyfield3_master.ownerrefid,0) AS f3ownerid " & _
                    "			,ISNULL(hrassess_coyfield3_master.weight,0) AS f3weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.startdate,112),'') AS f4_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.enddate,112),'') AS f4_EDate " & _
                    "			,ISNULL(hrassess_coyfield4_master.statusunkid,0) AS f4StatId " & _
                    "			,ISNULL(hrassess_coyfield4_master.ownerrefid,0) AS f4ownerid " & _
                    "			,ISNULL(hrassess_coyfield4_master.weight,0) AS f4weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.startdate,112),'') AS f5_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.enddate,112),'') AS f5_EDate " & _
                    "			,ISNULL(hrassess_coyfield5_master.statusunkid,0) AS f5StatId " & _
                    "			,ISNULL(hrassess_coyfield5_master.ownerrefid,0) AS f5ownerid " & _
                    "			,ISNULL(hrassess_coyfield5_master.weight,0) AS f5weight " & _
                    "			,hrassess_coyfield1_master.coyfield1unkid " & _
                    "			,hrassess_coyfield2_master.coyfield2unkid " & _
                    "			,hrassess_coyfield3_master.coyfield3unkid " & _
                    "			,hrassess_coyfield4_master.coyfield4unkid " & _
                    "			,hrassess_coyfield5_master.coyfield5unkid " & _
                    "		FROM hrassess_coyfield1_master " & _
                    "           LEFT JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                    "			LEFT JOIN hrassess_coyfield2_master ON hrassess_coyfield1_master.coyfield1unkid = hrassess_coyfield2_master.coyfield1unkid AND hrassess_coyfield2_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield3_master ON hrassess_coyfield2_master.coyfield2unkid = hrassess_coyfield3_master.coyfield2unkid AND hrassess_coyfield3_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield4_master ON hrassess_coyfield3_master.coyfield3unkid = hrassess_coyfield4_master.coyfield3unkid AND hrassess_coyfield4_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield5_master ON hrassess_coyfield4_master.coyfield4unkid = hrassess_coyfield5_master.coyfield4unkid AND hrassess_coyfield5_master.isvoid = 0 " & _
                    "		WHERE hrassess_coyfield1_master.isvoid = 0 AND hrassess_coyfield1_master.periodunkid = '" & iPeriodId & "' " & _
                    "	UNION " & _
                    "		SELECT " & _
                    "			 ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid" & _
                    "           ,ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                    "			,ISNULL(hrassess_coyfield1_master.field_data,'') AS Field1 " & _
                    "			,ISNULL(hrassess_coyfield2_master.field_data,'') AS Field2 " & _
                    "			,ISNULL(hrassess_coyfield3_master.field_data,'') AS Field3 " & _
                    "			,ISNULL(hrassess_coyfield4_master.field_data,'') AS Field4 " & _
                    "			,ISNULL(hrassess_coyfield5_master.field_data,'') AS Field5 " & _
                    "			,ISNULL(hrassess_coyfield1_master.fieldunkid,0) AS Field1Id " & _
                    "			,ISNULL(hrassess_coyfield2_master.fieldunkid,0) AS Field2Id " & _
                    "			,ISNULL(hrassess_coyfield3_master.fieldunkid,0) AS Field3Id " & _
                    "			,ISNULL(hrassess_coyfield4_master.fieldunkid,0) AS Field4Id " & _
                    "			,ISNULL(hrassess_coyfield5_master.fieldunkid,0) AS Field5Id " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.startdate,112),'') AS f1_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.enddate,112),'') AS f1_EDate " & _
                    "			,ISNULL(hrassess_coyfield1_master.statusunkid,0) AS f1StatId " & _
                    "			,ISNULL(hrassess_coyfield1_master.ownerrefid,0) AS f1ownerid " & _
                    "			,ISNULL(hrassess_coyfield1_master.weight,0) AS f1weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.startdate,112),'') AS f2_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.enddate,112),'') AS f2_EDate " & _
                    "			,ISNULL(hrassess_coyfield2_master.statusunkid,0) AS f2StatId " & _
                    "			,ISNULL(hrassess_coyfield2_master.ownerrefid,0) AS f2ownerid " & _
                    "			,ISNULL(hrassess_coyfield2_master.weight,0) AS f2weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.startdate,112),'') AS f3_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.enddate,112),'') AS f3_EDate " & _
                    "			,ISNULL(hrassess_coyfield3_master.statusunkid,0) AS f3StatId " & _
                    "			,ISNULL(hrassess_coyfield3_master.ownerrefid,0) AS f3ownerid " & _
                    "			,ISNULL(hrassess_coyfield3_master.weight,0) AS f3weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.startdate,112),'') AS f4_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.enddate,112),'') AS f4_EDate " & _
                    "			,ISNULL(hrassess_coyfield4_master.statusunkid,0) AS f4StatId " & _
                    "			,ISNULL(hrassess_coyfield4_master.ownerrefid,0) AS f4ownerid " & _
                    "			,ISNULL(hrassess_coyfield4_master.weight,0) AS f4weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.startdate,112),'') AS f5_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.enddate,112),'') AS f5_EDate " & _
                    "			,ISNULL(hrassess_coyfield5_master.statusunkid,0) AS f5StatId " & _
                    "			,ISNULL(hrassess_coyfield5_master.ownerrefid,0) AS f5ownerid " & _
                    "			,ISNULL(hrassess_coyfield5_master.weight,0) AS f5weight " & _
                    "			,hrassess_coyfield1_master.coyfield1unkid " & _
                    "			,hrassess_coyfield2_master.coyfield2unkid " & _
                    "			,hrassess_coyfield3_master.coyfield3unkid " & _
                    "			,hrassess_coyfield4_master.coyfield4unkid " & _
                    "			,hrassess_coyfield5_master.coyfield5unkid " & _
                    "		FROM hrassess_coyfield2_master " & _
                    "			LEFT JOIN hrassess_coyfield1_master ON hrassess_coyfield2_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 AND hrassess_coyfield1_master.periodunkid = '" & iPeriodId & "'  " & _
                    "           LEFT JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                    "			LEFT JOIN hrassess_coyfield3_master ON hrassess_coyfield2_master.coyfield2unkid = hrassess_coyfield3_master.coyfield2unkid AND hrassess_coyfield3_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield4_master ON hrassess_coyfield3_master.coyfield3unkid = hrassess_coyfield4_master.coyfield3unkid AND hrassess_coyfield4_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield5_master ON hrassess_coyfield4_master.coyfield4unkid = hrassess_coyfield5_master.coyfield4unkid AND hrassess_coyfield5_master.isvoid = 0 " & _
                    "		WHERE hrassess_coyfield2_master.isvoid = 0 AND hrassess_coyfield2_master.periodunkid = '" & iPeriodId & "' " & _
                    "	UNION " & _
                    "		SELECT " & _
                    "			 ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid" & _
                    "           ,ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                    "			,ISNULL(hrassess_coyfield1_master.field_data,'') AS Field1 " & _
                    "			,ISNULL(hrassess_coyfield2_master.field_data,'') AS Field2 " & _
                    "			,ISNULL(hrassess_coyfield3_master.field_data,'') AS Field3 " & _
                    "			,ISNULL(hrassess_coyfield4_master.field_data,'') AS Field4 " & _
                    "			,ISNULL(hrassess_coyfield5_master.field_data,'') AS Field5 " & _
                    "			,ISNULL(hrassess_coyfield1_master.fieldunkid,0) AS Field1Id " & _
                    "			,ISNULL(hrassess_coyfield2_master.fieldunkid,0) AS Field2Id " & _
                    "			,ISNULL(hrassess_coyfield3_master.fieldunkid,0) AS Field3Id " & _
                    "			,ISNULL(hrassess_coyfield4_master.fieldunkid,0) AS Field4Id " & _
                    "			,ISNULL(hrassess_coyfield5_master.fieldunkid,0) AS Field5Id " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.startdate,112),'') AS f1_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.enddate,112),'') AS f1_EDate " & _
                    "			,ISNULL(hrassess_coyfield1_master.statusunkid,0) AS f1StatId " & _
                    "			,ISNULL(hrassess_coyfield1_master.ownerrefid,0) AS f1ownerid " & _
                    "			,ISNULL(hrassess_coyfield1_master.weight,0) AS f1weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.startdate,112),'') AS f2_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.enddate,112),'') AS f2_EDate " & _
                    "			,ISNULL(hrassess_coyfield2_master.statusunkid,0) AS f2StatId " & _
                    "			,ISNULL(hrassess_coyfield2_master.ownerrefid,0) AS f2ownerid " & _
                    "			,ISNULL(hrassess_coyfield2_master.weight,0) AS f2weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.startdate,112),'') AS f3_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.enddate,112),'') AS f3_EDate " & _
                    "			,ISNULL(hrassess_coyfield3_master.statusunkid,0) AS f3StatId " & _
                    "			,ISNULL(hrassess_coyfield3_master.ownerrefid,0) AS f3ownerid " & _
                    "			,ISNULL(hrassess_coyfield3_master.weight,0) AS f3weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.startdate,112),'') AS f4_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.enddate,112),'') AS f4_EDate " & _
                    "			,ISNULL(hrassess_coyfield4_master.statusunkid,0) AS f4StatId " & _
                    "			,ISNULL(hrassess_coyfield4_master.ownerrefid,0) AS f4ownerid " & _
                    "			,ISNULL(hrassess_coyfield4_master.weight,0) AS f4weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.startdate,112),'') AS f5_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.enddate,112),'') AS f5_EDate " & _
                    "			,ISNULL(hrassess_coyfield5_master.statusunkid,0) AS f5StatId " & _
                    "			,ISNULL(hrassess_coyfield5_master.ownerrefid,0) AS f5ownerid " & _
                    "			,ISNULL(hrassess_coyfield5_master.weight,0) AS f5weight " & _
                    "			,hrassess_coyfield1_master.coyfield1unkid " & _
                    "			,hrassess_coyfield2_master.coyfield2unkid " & _
                    "			,hrassess_coyfield3_master.coyfield3unkid " & _
                    "			,hrassess_coyfield4_master.coyfield4unkid " & _
                    "			,hrassess_coyfield5_master.coyfield5unkid " & _
                    "		FROM hrassess_coyfield3_master " & _
                    "			LEFT JOIN hrassess_coyfield2_master ON hrassess_coyfield3_master.coyfield2unkid = hrassess_coyfield2_master.coyfield2unkid AND hrassess_coyfield2_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield1_master ON hrassess_coyfield2_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 AND hrassess_coyfield1_master.periodunkid = '" & iPeriodId & "' " & _
                    "           LEFT JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                    "			LEFT JOIN hrassess_coyfield4_master ON hrassess_coyfield3_master.coyfield3unkid = hrassess_coyfield4_master.coyfield3unkid AND hrassess_coyfield4_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield5_master ON hrassess_coyfield4_master.coyfield4unkid = hrassess_coyfield5_master.coyfield4unkid AND hrassess_coyfield5_master.isvoid = 0 " & _
                    "		WHERE hrassess_coyfield3_master.isvoid = 0 AND hrassess_coyfield3_master.periodunkid = '" & iPeriodId & "' " & _
                    "	UNION " & _
                    "		SELECT " & _
                    "			 ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid" & _
                    "           ,ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                    "			,ISNULL(hrassess_coyfield1_master.field_data,'') AS Field1 " & _
                    "			,ISNULL(hrassess_coyfield2_master.field_data,'') AS Field2 " & _
                    "			,ISNULL(hrassess_coyfield3_master.field_data,'') AS Field3 " & _
                    "			,ISNULL(hrassess_coyfield4_master.field_data,'') AS Field4 " & _
                    "			,ISNULL(hrassess_coyfield5_master.field_data,'') AS Field5 " & _
                    "			,ISNULL(hrassess_coyfield1_master.fieldunkid,0) AS Field1Id " & _
                    "			,ISNULL(hrassess_coyfield2_master.fieldunkid,0) AS Field2Id " & _
                    "			,ISNULL(hrassess_coyfield3_master.fieldunkid,0) AS Field3Id " & _
                    "			,ISNULL(hrassess_coyfield4_master.fieldunkid,0) AS Field4Id " & _
                    "			,ISNULL(hrassess_coyfield5_master.fieldunkid,0) AS Field5Id " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.startdate,112),'') AS f1_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.enddate,112),'') AS f1_EDate " & _
                    "			,ISNULL(hrassess_coyfield1_master.statusunkid,0) AS f1StatId " & _
                    "			,ISNULL(hrassess_coyfield1_master.ownerrefid,0) AS f1ownerid " & _
                    "			,ISNULL(hrassess_coyfield1_master.weight,0) AS f1weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.startdate,112),'') AS f2_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.enddate,112),'') AS f2_EDate " & _
                    "			,ISNULL(hrassess_coyfield2_master.statusunkid,0) AS f2StatId " & _
                    "			,ISNULL(hrassess_coyfield2_master.ownerrefid,0) AS f2ownerid " & _
                    "			,ISNULL(hrassess_coyfield2_master.weight,0) AS f2weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.startdate,112),'') AS f3_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.enddate,112),'') AS f3_EDate " & _
                    "			,ISNULL(hrassess_coyfield3_master.statusunkid,0) AS f3StatId " & _
                    "			,ISNULL(hrassess_coyfield3_master.ownerrefid,0) AS f3ownerid " & _
                    "			,ISNULL(hrassess_coyfield3_master.weight,0) AS f3weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.startdate,112),'') AS f4_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.enddate,112),'') AS f4_EDate " & _
                    "			,ISNULL(hrassess_coyfield4_master.statusunkid,0) AS f4StatId " & _
                    "			,ISNULL(hrassess_coyfield4_master.ownerrefid,0) AS f4ownerid " & _
                    "			,ISNULL(hrassess_coyfield4_master.weight,0) AS f4weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.startdate,112),'') AS f5_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.enddate,112),'') AS f5_EDate " & _
                    "			,ISNULL(hrassess_coyfield5_master.statusunkid,0) AS f5StatId " & _
                    "			,ISNULL(hrassess_coyfield5_master.ownerrefid,0) AS f5ownerid " & _
                    "			,ISNULL(hrassess_coyfield5_master.weight,0) AS f5weight " & _
                    "			,hrassess_coyfield1_master.coyfield1unkid " & _
                    "			,hrassess_coyfield2_master.coyfield2unkid " & _
                    "			,hrassess_coyfield3_master.coyfield3unkid " & _
                    "			,hrassess_coyfield4_master.coyfield4unkid " & _
                    "			,hrassess_coyfield5_master.coyfield5unkid " & _
                    "		FROM hrassess_coyfield4_master " & _
                    "			LEFT JOIN hrassess_coyfield3_master ON hrassess_coyfield4_master.coyfield3unkid = hrassess_coyfield3_master.coyfield3unkid AND hrassess_coyfield3_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield2_master ON hrassess_coyfield3_master.coyfield2unkid = hrassess_coyfield2_master.coyfield2unkid AND hrassess_coyfield2_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield1_master ON hrassess_coyfield2_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 AND hrassess_coyfield1_master.periodunkid = '" & iPeriodId & "' " & _
                    "           LEFT JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                    "			LEFT JOIN hrassess_coyfield5_master ON hrassess_coyfield4_master.coyfield4unkid = hrassess_coyfield5_master.coyfield4unkid AND hrassess_coyfield5_master.isvoid = 0 " & _
                    "		WHERE hrassess_coyfield4_master.isvoid = 0 AND hrassess_coyfield4_master.periodunkid = '" & iPeriodId & "' " & _
                    "	UNION " & _
                    "		SELECT " & _
                    "			 ISNULL(hrassess_coyfield1_master.perspectiveunkid,0) AS perspectiveunkid" & _
                    "           ,ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                    "			,ISNULL(hrassess_coyfield1_master.field_data,'') AS Field1 " & _
                    "			,ISNULL(hrassess_coyfield2_master.field_data,'') AS Field2 " & _
                    "			,ISNULL(hrassess_coyfield3_master.field_data,'') AS Field3 " & _
                    "			,ISNULL(hrassess_coyfield4_master.field_data,'') AS Field4 " & _
                    "			,ISNULL(hrassess_coyfield5_master.field_data,'') AS Field5 " & _
                    "			,ISNULL(hrassess_coyfield1_master.fieldunkid,0) AS Field1Id " & _
                    "			,ISNULL(hrassess_coyfield2_master.fieldunkid,0) AS Field2Id " & _
                    "			,ISNULL(hrassess_coyfield3_master.fieldunkid,0) AS Field3Id " & _
                    "			,ISNULL(hrassess_coyfield4_master.fieldunkid,0) AS Field4Id " & _
                    "			,ISNULL(hrassess_coyfield5_master.fieldunkid,0) AS Field5Id " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.startdate,112),'') AS f1_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield1_master.enddate,112),'') AS f1_EDate " & _
                    "			,ISNULL(hrassess_coyfield1_master.statusunkid,0) AS f1StatId " & _
                    "			,ISNULL(hrassess_coyfield1_master.ownerrefid,0) AS f1ownerid " & _
                    "			,ISNULL(hrassess_coyfield1_master.weight,0) AS f1weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.startdate,112),'') AS f2_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield2_master.enddate,112),'') AS f2_EDate " & _
                    "			,ISNULL(hrassess_coyfield2_master.statusunkid,0) AS f2StatId " & _
                    "			,ISNULL(hrassess_coyfield2_master.ownerrefid,0) AS f2ownerid " & _
                    "			,ISNULL(hrassess_coyfield2_master.weight,0) AS f2weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.startdate,112),'') AS f3_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield3_master.enddate,112),'') AS f3_EDate " & _
                    "			,ISNULL(hrassess_coyfield3_master.statusunkid,0) AS f3StatId " & _
                    "			,ISNULL(hrassess_coyfield3_master.ownerrefid,0) AS f3ownerid " & _
                    "			,ISNULL(hrassess_coyfield3_master.weight,0) AS f3weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.startdate,112),'') AS f4_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield4_master.enddate,112),'') AS f4_EDate " & _
                    "			,ISNULL(hrassess_coyfield4_master.statusunkid,0) AS f4StatId " & _
                    "			,ISNULL(hrassess_coyfield4_master.ownerrefid,0) AS f4ownerid " & _
                    "			,ISNULL(hrassess_coyfield4_master.weight,0) AS f4weight " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.startdate,112),'') AS f5_SDate " & _
                    "			,ISNULL(CONVERT(CHAR(8),hrassess_coyfield5_master.enddate,112),'') AS f5_EDate " & _
                    "			,ISNULL(hrassess_coyfield5_master.statusunkid,0) AS f5StatId " & _
                    "			,ISNULL(hrassess_coyfield5_master.ownerrefid,0) AS f5ownerid " & _
                    "			,ISNULL(hrassess_coyfield5_master.weight,0) AS f5weight " & _
                    "			,hrassess_coyfield1_master.coyfield1unkid " & _
                    "			,hrassess_coyfield2_master.coyfield2unkid " & _
                    "			,hrassess_coyfield3_master.coyfield3unkid " & _
                    "			,hrassess_coyfield4_master.coyfield4unkid " & _
                    "			,hrassess_coyfield5_master.coyfield5unkid " & _
                    "		FROM hrassess_coyfield5_master " & _
                    "			LEFT JOIN hrassess_coyfield4_master ON hrassess_coyfield5_master.coyfield4unkid = hrassess_coyfield4_master.coyfield4unkid AND hrassess_coyfield4_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield3_master ON hrassess_coyfield4_master.coyfield3unkid = hrassess_coyfield3_master.coyfield3unkid AND hrassess_coyfield3_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield2_master ON hrassess_coyfield3_master.coyfield2unkid = hrassess_coyfield2_master.coyfield2unkid AND hrassess_coyfield2_master.isvoid = 0 " & _
                    "			LEFT JOIN hrassess_coyfield1_master ON hrassess_coyfield2_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 AND hrassess_coyfield1_master.periodunkid = '" & iPeriodId & "' " & _
                    "           LEFT JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                    "		WHERE hrassess_coyfield5_master.isvoid = 0 AND hrassess_coyfield5_master.periodunkid = '" & iPeriodId & "' " & _
                    ") AS iLst " & _
                    "WHERE 1 = 1 " & _
            "ORDER BY perspectiveunkid ASC,coyfield1unkid ASC, Field1 ASC "
            'S.SANDEEP [26 NOV 2015] -- START {REPLACED empfield1unkid WITH coyfield1unkid} -- END

            'S.SANDEEP [06 NOV 2015] -- START {REMOVED "ORDER BY Field1Id DESC, Field1 ASC "} -- END

            'objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            'objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            'objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            'objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objOwnerTran As New clsassess_coyowner_tran
            Dim iOwnerGroupName As String = String.Empty
            Dim objCoyInfoField As New clsassess_coyinfofield_tran
            Dim mdicFieldData As New Dictionary(Of Integer, String)
            For Each dRow As DataRow In dsList.Tables(iList).Rows
                iOwnerGroupName = "" : mdicFieldData = New Dictionary(Of Integer, String)
                Select Case CInt(dRow.Item("COwnerId"))
                    Case enAllocation.BRANCH
                        iOwnerGroupName = Language.getMessage("clsMasterData", 430, "Branch")
                    Case enAllocation.DEPARTMENT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 429, "Department Group")
                    Case enAllocation.DEPARTMENT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 428, "Department")
                    Case enAllocation.SECTION_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 427, "Section Group")
                    Case enAllocation.SECTION
                        iOwnerGroupName = Language.getMessage("clsMasterData", 426, "Section")
                    Case enAllocation.UNIT_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    Case enAllocation.UNIT
                        iOwnerGroupName = Language.getMessage("clsMasterData", 424, "Unit")
                    Case enAllocation.TEAM
                        iOwnerGroupName = Language.getMessage("clsMasterData", 423, "Team")
                    Case enAllocation.JOB_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 422, "Job Group")
                    Case enAllocation.JOBS
                        iOwnerGroupName = Language.getMessage("clsMasterData", 421, "Jobs")
                    Case enAllocation.CLASS_GROUP
                        iOwnerGroupName = Language.getMessage("clsMasterData", 420, "Class Group")
                    Case enAllocation.CLASSES
                        iOwnerGroupName = Language.getMessage("clsMasterData", 419, "Classes")
                End Select

                Select Case CInt(dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("Owner") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objOwnerTran.GetCSV_OwnerName(dRow.Item("COwnerId"), dRow.Item("coyfield1unkid"), dRow.Item("CFieldTypeId"))
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("COwnerId"), dRow.Item("coyfield1unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objCoyInfoField.Get_Data(dRow.Item("coyfield1unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("Owner") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objOwnerTran.GetCSV_OwnerName(dRow.Item("COwnerId"), dRow.Item("coyfield2unkid"), dRow.Item("CFieldTypeId"))
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("COwnerId"), dRow.Item("coyfield2unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objCoyInfoField.Get_Data(dRow.Item("coyfield2unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD3
                        dRow.Item("Owner") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objOwnerTran.GetCSV_OwnerName(dRow.Item("COwnerId"), dRow.Item("coyfield3unkid"), dRow.Item("CFieldTypeId"))
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("COwnerId"), dRow.Item("coyfield3unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objCoyInfoField.Get_Data(dRow.Item("coyfield3unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("Owner") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objOwnerTran.GetCSV_OwnerName(dRow.Item("COwnerId"), dRow.Item("coyfield4unkid"), dRow.Item("CFieldTypeId"))
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("COwnerId"), dRow.Item("coyfield4unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objCoyInfoField.Get_Data(dRow.Item("coyfield4unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("Owner") = IIf(iOwnerGroupName.Trim.Length > 0, iOwnerGroupName & " -> ", "") & objOwnerTran.GetCSV_OwnerName(dRow.Item("COwnerId"), dRow.Item("coyfield5unkid"), dRow.Item("CFieldTypeId"))
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("COwnerId"), dRow.Item("coyfield5unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objCoyInfoField.Get_Data(dRow.Item("coyfield5unkid"), dRow.Item("CFieldTypeId"))
                End Select
                If dRow.Item("St_Date").ToString.Trim.Length > 0 Then
                    dRow.Item("St_Date") = eZeeDate.convertDate(dRow.Item("St_Date").ToString).ToShortDateString
                End If
                If dRow.Item("Ed_Date").ToString.Trim.Length > 0 Then
                    dRow.Item("Ed_Date") = eZeeDate.convertDate(dRow.Item("Ed_Date").ToString).ToShortDateString
                End If
                mdtFinal.ImportRow(dRow)
                If dRow.Item("St_Date").ToString.Trim.Length > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("sdate") = eZeeDate.convertDate(CDate(dRow.Item("St_Date")))
                End If
                If dRow.Item("Ed_Date").ToString.Trim.Length > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("edate") = eZeeDate.convertDate(CDate(dRow.Item("Ed_Date")))
                End If

                Dim iLastStatusId As Integer = GetLastStatus(iPeriodId)

                If iLastStatusId > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("opstatusid") = iLastStatusId
                End If

                If mdicFieldData.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicFieldData.Keys
                        If mdtFinal.Columns.Contains("Field" & iKey.ToString) Then
                            mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("Field" & iKey.ToString) = mdicFieldData(iKey)
                            mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("Field" & iKey.ToString & "Id") = iKey
                        End If
                    Next
                End If
            Next
            objOwnerTran = Nothing
            objCoyInfoField = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDisplayList", mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get OwnerRefId for Default </purpose>
    Public Function GetOwnerRefId() As Integer
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT TOP 1 ISNULL(ownerrefid,0) AS Id FROM hrassess_coyfield1_master WHERE isvoid = 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("Id"))
            Else
                Return -1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOwnerRefId", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Update_Status(ByVal iStatusId As Integer, ByVal iComments As String, ByVal iUserId As Integer, ByVal iPeriodId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ = "INSERT INTO hrassess_coystatus_tran " & _
                   "  ( " & _
                   "     statustypeid " & _
                   "    ,status_date " & _
                   "    ,commtents " & _
                   "    ,userunkid " & _
                   "    ,periodunkid " & _
                   "  ) " & _
                   "VALUES " & _
                   "  ( " & _
                   "    @statustypeid " & _
                   "   ,@status_date " & _
                   "   ,@commtents " & _
                   "   ,@userunkid " & _
                   "   ,@periodunkid " & _
                   "  ) ; SELECT @@identity "

            objDataOperation.AddParameter("@statustypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iStatusId)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@commtents", SqlDbType.NVarChar, iComments.Length, iComments)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            Dim dsList As New DataSet
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim intStatustranunkid As Integer = CInt(dsList.Tables(0).Rows(0).Item(0))

            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_coystatus_tran", "statustranunkid", intStatustranunkid, , mintAuditUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_Status", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetLastStatus(ByVal iPeriodId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim iStatusId As Integer = -1
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT TOP 1 statustypeid FROM hrassess_coystatus_tran " & _
                   " WHERE periodunkid = '" & iPeriodId & "' " & _
                   "ORDER BY status_date DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iStatusId = dsList.Tables(0).Rows(0).Item("statustypeid")
            Else
                iStatusId = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLastStatus", mstrModuleName)
        Finally
        End Try
        Return iStatusId
    End Function

    Public Function Transfer_Goals_ToAllocation(ByVal iDataBaseName As String, ByVal iPeriodId As Integer) As Boolean
        Dim mdTran As DataTable = Nothing
        Try
            mdTran = GetDisplayList(iPeriodId)
            If mdTran.Rows.Count > 0 Then
                'Dim dsPrd As New DataSet : Dim objPrd As New clscommom_period_Tran
                'dsPrd = objPrd.getListForCombo(enModuleReference.Assessment, iYearId, "List", False, 1, , , iDataBaseName)
                'If dsPrd.Tables(0).Rows.Count > 0 Then
                For Each iORow As DataRow In mdTran.Rows
                    Dim iOwnr() As String = iORow.Item("OwnerIds").ToString.Split(",")
                    For iR As Integer = 0 To iOwnr.Length - 1
                        'For Each iPRow As DataRow In dsPrd.Tables(0).Rows
                        Application.DoEvents()

                        '************** SRART TRANSFERING FIELD DATA TO ALLOCATION ************** START
                        Dim iOwnerField1Unkid, iOwnerField2Unkid, iOwnerField3Unkid, iOwnerField4Unkid, iOwnerField5Unkid As Integer
                        iOwnerField1Unkid = 0 : iOwnerField2Unkid = 0 : iOwnerField3Unkid = 0 : iOwnerField4Unkid = 0 : iOwnerField5Unkid = 0

                        If iOwnr(iR).Trim.Length <= 0 Then Continue For

                        Dim mDicFieldInfo As New Dictionary(Of Integer, String)
                        Dim objInfoField As New clsassess_coyinfofield_tran
                        _Coyfield1unkid = iORow.Item("coyfield1unkid")
                        If mstrField_Data.Trim.Length > 0 Then
                            Dim objOField1 As New clsassess_owrfield1_master
                            iOwnerField1Unkid = objOField1.Get_OwnerField1Unkid(mstrField_Data, IIf(iOwnr(iR).Trim = "", 0, iOwnr(iR)), iPeriodId)
                            If iOwnerField1Unkid <= 0 Then
                                objOField1._Coyfield1unkid = mintCoyfield1unkid
                                objOField1._Enddate = mdtEnddate
                                objOField1._Field_Data = mstrField_Data
                                objOField1._Fieldunkid = mintFieldunkid
                                objOField1._Isvoid = False
                                objOField1._Owenerrefid = iORow.Item("COwnerId")
                                objOField1._Ownerunkid = iOwnr(iR)
                                objOField1._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                                objOField1._Pct_Completed = 0
                                objOField1._Periodunkid = iPeriodId
                                objOField1._Startdate = mdtStartdate
                                objOField1._Statusunkid = mintStatusunkid
                                objOField1._Userunkid = mintUserunkid
                                objOField1._Voiddatetime = mdtVoiddatetime
                                objOField1._Voidreason = mstrVoidreason
                                objOField1._Voiduserunkid = mintVoiduserunkid
                                objOField1._Weight = mdblWeight
                                objOField1._Yearunkid = 0
                                With objOField1
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(mintCoyfield1unkid, enWeight_Types.WEIGHT_FIELD1)
                                If objOField1.Insert(mDicFieldInfo) = True Then
                                    iOwnerField1Unkid = objOField1._Owrfield1unkid
                                End If
                            End If
                        End If

                        Dim objCField2 As New clsassess_coyfield2_master
                        objCField2._Coyfield2unkid = iORow.Item("coyfield2unkid")
                        If objCField2._Field_Data.Trim.Length > 0 Then
                            Dim objOField2 As New clsassess_owrfield2_master
                            iOwnerField2Unkid = objOField2.Get_OwnerField2Unkid(objCField2._Field_Data, iOwnerField1Unkid, iPeriodId)
                            If iOwnerField2Unkid <= 0 Then
                                objOField2._Enddate = objCField2._Enddate
                                objOField2._Periodunkid = iPeriodId
                                objOField2._Field_Data = objCField2._Field_Data
                                objOField2._Fieldunkid = objCField2._Fieldunkid
                                objOField2._Isvoid = objCField2._Isvoid
                                objOField2._Owrfield1unkid = iOwnerField1Unkid
                                objOField2._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                                objOField2._Pct_Completed = 0
                                objOField2._Startdate = objCField2._Startdate
                                objOField2._Statusunkid = objCField2._Statusunkid
                                objOField2._Userunkid = objCField2._Userunkid
                                objOField2._Voiddatetime = objCField2._Voiddatetime
                                objOField2._Voidreason = objCField2._Voidreason
                                objOField2._Voiduserunkid = objCField2._Voiduserunkid
                                objOField2._Weight = objCField2._Weight
                                objOField2._Ownerunkid = iOwnr(iR)
                                objOField2._Periodunkid = iPeriodId
                                With objOField2
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objCField2._Coyfield2unkid, enWeight_Types.WEIGHT_FIELD2)
                                If objOField2.Insert(mDicFieldInfo) = True Then
                                    iOwnerField2Unkid = objOField2._Owrfield2unkid
                                End If
                            End If
                        End If

                        Dim objCField3 As New clsassess_coyfield3_master
                        objCField3._Coyfield3unkid = iORow.Item("coyfield3unkid")
                        If objCField3._Field_Data.Trim.Length > 0 Then
                            Dim objOField3 As New clsassess_owrfield3_master
                            iOwnerField3Unkid = objOField3.Get_OwnerField3Unkid(objCField3._Field_Data, iOwnerField2Unkid, iPeriodId)
                            If iOwnerField3Unkid <= 0 Then
                                objCField3._Coyfield3unkid = iORow.Item("coyfield3unkid")
                                objOField3._Enddate = objCField3._Enddate
                                objOField3._Field_Data = objCField3._Field_Data
                                objOField3._Fieldunkid = objCField3._Fieldunkid
                                objOField3._Isvoid = objCField3._Isvoid
                                objOField3._Owrfield2unkid = iOwnerField2Unkid
                                objOField3._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                                objOField3._Pct_Completed = 0
                                objOField3._Startdate = objCField3._Startdate
                                objOField3._Statusunkid = objCField3._Statusunkid
                                objOField3._Userunkid = objCField3._Userunkid
                                objOField3._Voiddatetime = objCField3._Voiddatetime
                                objOField3._Voidreason = objCField3._Voidreason
                                objOField3._Voiduserunkid = objCField3._Voiduserunkid
                                objOField3._Weight = objCField3._Weight
                                objOField3._Ownerunkid = iOwnr(iR)
                                objOField3._Periodunkid = iPeriodId
                                With objOField3
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objCField3._Coyfield3unkid, enWeight_Types.WEIGHT_FIELD3)
                                If objOField3.Insert(mDicFieldInfo) = True Then
                                    iOwnerField3Unkid = objOField3._Owrfield3unkid
                                End If
                            End If
                        End If

                        Dim objCField4 As New clsassess_coyfield4_master
                        objCField4._Coyfield4unkid = iORow.Item("coyfield4unkid")
                        If objCField4._Field_Data.Trim.Length > 0 Then
                            Dim objOField4 As New clsassess_owrfield4_master
                            iOwnerField4Unkid = objOField4.Get_OwnerField4Unkid(objCField4._Field_Data, iOwnerField3Unkid, iPeriodId)
                            If iOwnerField4Unkid <= 0 Then
                                objOField4._Enddate = objCField4._Enddate
                                objOField4._Field_Data = objCField4._Field_Data
                                objOField4._Fieldunkid = objCField4._Fieldunkid
                                objOField4._Isvoid = objCField4._Isvoid
                                objOField4._Owrfield3unkid = iOwnerField3Unkid
                                objOField4._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                                objOField4._Pct_Completed = 0
                                objOField4._Startdate = objCField4._Startdate
                                objOField4._Statusunkid = objCField4._Statusunkid
                                objOField4._Userunkid = objCField4._Userunkid
                                objOField4._Voiddatetime = objCField4._Voiddatetime
                                objOField4._Voidreason = objCField4._Voidreason
                                objOField4._Voiduserunkid = objCField4._Voiduserunkid
                                objOField4._Weight = objCField4._Weight
                                objOField4._Ownerunkid = iOwnr(iR)
                                objOField4._Periodunkid = iPeriodId
                                With objOField4
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objCField4._Coyfield4unkid, enWeight_Types.WEIGHT_FIELD4)
                                If objOField4.Insert(mDicFieldInfo) = True Then
                                    iOwnerField4Unkid = objOField4._Owrfield4unkid
                                End If
                            End If
                        End If

                        Dim objCField5 As New clsassess_coyfield5_master
                        objCField5._Coyfield5unkid = iORow.Item("coyfield5unkid")
                        If objCField5._Field_Data.Trim.Length > 0 Then
                            Dim objOField5 As New clsassess_owrfield5_master
                            iOwnerField5Unkid = objOField5.Get_OwnerField5Unkid(objCField5._Field_Data, iOwnerField4Unkid, iPeriodId)
                            If iOwnerField5Unkid <= 0 Then
                                objOField5._Enddate = objCField5._Enddate
                                objOField5._Field_Data = objCField5._Field_Data
                                objOField5._Fieldunkid = objCField5._Fieldunkid
                                objOField5._Isvoid = objCField5._Isvoid
                                objOField5._Owrfield4unkid = iOwnerField4Unkid
                                objOField5._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD5
                                objOField5._Pct_Completed = 0
                                objOField5._Startdate = objCField5._Startdate
                                objOField5._Statusunkid = objCField5._Statusunkid
                                objOField5._Userunkid = objCField5._Userunkid
                                objOField5._Voiddatetime = objCField5._Voiddatetime
                                objOField5._Voidreason = objCField5._Voidreason
                                objOField5._Voiduserunkid = objCField5._Voiduserunkid
                                objOField5._Weight = objCField5._Weight
                                objOField5._Ownerunkid = iOwnr(iR)
                                objOField5._Periodunkid = iPeriodId
                                With objOField5
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
                                    ._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objCField5._Coyfield5unkid, enWeight_Types.WEIGHT_FIELD5)
                                If objOField5.Insert(mDicFieldInfo) = True Then
                                    iOwnerField5Unkid = objOField5._Owrfield5unkid
                                End If
                            End If
                        End If
                        '************** SRART TRANSFERING FIELD DATA TO ALLOCATION ************** END
                        'Next
                    Next
                Next
            End If
            'End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Transfer_Goals_ToAllocation", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 10 JAN 2015 ] -- START
    Public Function GetListForTansfer(ByVal xPeriodId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     coyfield1unkid " & _
                       "    ,perspectiveunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,periodunkid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "FROM hrassess_coyfield1_master " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' ORDER BY coyfield1unkid "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetListForTansfer", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetCoyFieldUnkid(ByVal xFieldData As String, ByVal xPeriodId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim xUnkid As Integer = -1
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT @xUnkid = coyfield1unkid FROM hrassess_coyfield1_master WHERE isvoid = 0 AND periodunkid = @xPeriodId AND field_data = @xFieldData "

                objDo.AddParameter("@xUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid, ParameterDirection.InputOutput)
                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFieldData", SqlDbType.NVarChar, xFieldData.Length, xFieldData)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xUnkid = objDo.GetParameterValue("@xUnkid")

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCoyFieldUnkid", mstrModuleName)
        Finally
        End Try
        Return xUnkid
    End Function
    'S.SANDEEP [ 10 JAN 2015 ] -- END

    'S.SANDEEP [27 Jan 2016] -- START
    Public Function AllowedToUnlock(ByVal intPeriod As Integer) As String
        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Dim iCnt As Integer = -1
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT 1 " & _
                       "FROM hrassess_owrfield1_master " & _
                       "WHERE hrassess_owrfield1_master.isvoid = 0 AND hrassess_owrfield1_master.coyfield1unkid > 0 " & _
                       "AND hrassess_owrfield1_master.periodunkid = '" & intPeriod & "' "

                iCnt = objDo.RecordCount(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                If iCnt > 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot unlock the committed goals for the selected period. Reason, Goals are already assigned to the allocation(s).")
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:AllowedToUnlock ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function
    'S.SANDEEP [27 Jan 2016] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "End Date")
            Language.setMessage(mstrModuleName, 4, "Status")
            Language.setMessage(mstrModuleName, 5, "Goal Owner")
            Language.setMessage(mstrModuleName, 6, "Weight")
            Language.setMessage(mstrModuleName, 7, "Perspective")
            Language.setMessage(mstrModuleName, 8, "Start Date")
            Language.setMessage(mstrModuleName, 9, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Language.setMessage("clsMasterData", 315, "Financial")
            Language.setMessage("clsMasterData", 316, "Customer")
            Language.setMessage("clsMasterData", 317, "Business Process")
            Language.setMessage("clsMasterData", 318, "Organization Capacity")
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 518, "Pending")
            Language.setMessage("clsMasterData", 519, "In progress")
            Language.setMessage("clsMasterData", 520, "Complete")
            Language.setMessage("clsMasterData", 521, "Closed")
            Language.setMessage("clsMasterData", 522, "On Track")
            Language.setMessage("clsMasterData", 523, "At Risk")
            Language.setMessage("clsMasterData", 524, "Not Applicable")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
