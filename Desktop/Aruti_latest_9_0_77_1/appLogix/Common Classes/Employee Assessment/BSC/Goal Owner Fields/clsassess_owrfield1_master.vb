﻿'************************************************************************************************************************************
'Class Name :clsassess_owrfield1_master.vb
'Purpose    :
'Date       :09-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_owrfield1_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_owrfield1_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintOwrfield1unkid As Integer = 0
    Private mintCoyfield1unkid As Integer = 0
    Private mintOwenerrefid As Integer = 0
    Private mintOwnerunkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mintYearunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mdblWeight As Double = 0
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintOwrFieldTypeId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mDecPct_Completed As Decimal = 0

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Owrfield1unkid() As Integer
        Get
            Return mintOwrfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintOwrfield1unkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coyfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Coyfield1unkid() As Integer
        Get
            Return mintCoyfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintCoyfield1unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ownerrefid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Owenerrefid() As Integer
        Get
            Return mintOwenerrefid
        End Get
        Set(ByVal value As Integer)
            mintOwenerrefid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ownerunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ownerunkid() As Integer
        Get
            Return mintOwnerunkid
        End Get
        Set(ByVal value As Integer)
            mintOwnerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Owrfieldtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _OwrFieldTypeId() As Integer
        Set(ByVal value As Integer)
            mintOwrFieldTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Periodname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _PeriodName() As String
        Get
            Return mstrPeriodName
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_completed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Pct_Completed() As Decimal
        Get
            Return mDecPct_Completed
        End Get
        Set(ByVal value As Decimal)
            mDecPct_Completed = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_owrfield1_master.owrfield1unkid " & _
                   ", hrassess_owrfield1_master.coyfield1unkid " & _
                   ", hrassess_owrfield1_master.ownerrefid " & _
                   ", hrassess_owrfield1_master.ownerunkid " & _
                   ", hrassess_owrfield1_master.fieldunkid " & _
                   ", hrassess_owrfield1_master.field_data " & _
                   ", hrassess_owrfield1_master.yearunkid " & _
                   ", hrassess_owrfield1_master.periodunkid " & _
                   ", hrassess_owrfield1_master.weight " & _
                   ", hrassess_owrfield1_master.startdate " & _
                   ", hrassess_owrfield1_master.enddate " & _
                   ", hrassess_owrfield1_master.statusunkid " & _
                   ", hrassess_owrfield1_master.userunkid " & _
                   ", hrassess_owrfield1_master.isvoid " & _
                   ", hrassess_owrfield1_master.voiduserunkid " & _
                   ", hrassess_owrfield1_master.voidreason " & _
                   ", hrassess_owrfield1_master.voiddatetime " & _
                   ", ISNULL(cfcommon_period_tran.period_name,'') AS period_name " & _
                   ", hrassess_owrfield1_master.pct_completed " & _
                   "FROM hrassess_owrfield1_master " & _
                   "  JOIN cfcommon_period_tran ON hrassess_owrfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "WHERE owrfield1unkid = @owrfield1unkid "

            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintOwrfield1unkid = CInt(dtRow.Item("owrfield1unkid"))
                mintCoyfield1unkid = CInt(dtRow.Item("coyfield1unkid"))
                mintOwenerrefid = CInt(dtRow.Item("ownerrefid"))
                mintOwnerunkid = CInt(dtRow.Item("ownerunkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mstrField_Data = dtRow.Item("field_data").ToString
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdblWeight = CDbl(dtRow.Item("weight"))
                If IsDBNull(dtRow.Item("startdate")) = False Then
                    mdtStartdate = dtRow.Item("startdate")
                Else
                    mdtStartdate = Nothing
                End If
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrPeriodName = dtRow.Item("period_name")
                mDecPct_Completed = dtRow.Item("pct_completed")
                Exit For
            Next

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dsUpdate As DataSet
            Dim objProgress As New clsassess_owrupdate_tran
            objProgress._DataOperation = objDataOperation
            dsUpdate = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime, mintOwnerunkid, mintPeriodunkid)
            If dsUpdate IsNot Nothing Then
                Dim pRow() As DataRow = dsUpdate.Tables(0).Select("owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "' AND owrfieldunkid = '" & mintOwrfield1unkid & "'")
                If pRow.Length > 0 Then
                    mDecPct_Completed = pRow(0).Item("pct_completed")
                    mintStatusunkid = pRow(0).Item("statusunkid")
                End If
            End If
            objProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  owrfield1unkid " & _
              ", coyfield1unkid " & _
              ", ownerrefid " & _
              ", ownerunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", weight " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", pct_completed " & _
             "FROM hrassess_owrfield1_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_owrfield1_master) </purpose>
    Public Function Insert(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, , mintOwnerunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data for the selected owner.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield1unkid.ToString)
            objDataOperation.AddParameter("@ownerrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwenerrefid.ToString)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed.ToString)

            strQ = "INSERT INTO hrassess_owrfield1_master ( " & _
                       "  coyfield1unkid " & _
                       ", ownerrefid " & _
                       ", ownerunkid " & _
                       ", fieldunkid " & _
                       ", field_data " & _
                       ", yearunkid " & _
                       ", periodunkid " & _
                       ", weight " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", statusunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime " & _
                       ", pct_completed" & _
                   ") VALUES (" & _
                       "  @coyfield1unkid " & _
                       ", @ownerrefid " & _
                       ", @ownerunkid " & _
                       ", @fieldunkid " & _
                       ", @field_data " & _
                       ", @yearunkid " & _
                       ", @periodunkid " & _
                       ", @weight " & _
                       ", @startdate " & _
                       ", @enddate " & _
                       ", @statusunkid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime " & _
                       ", @pct_completed" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintOwrfield1unkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_owrfield1_master", "owrfield1unkid", mintOwrfield1unkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_owrowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintOwrfield1unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintOwrfield1unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP [16 JUN 2015] -- START
            Dim objUpdateProgress As New clsassess_owrupdate_tran
            objUpdateProgress._DataOperation = objDataOperation
            objUpdateProgress._Fieldunkid = mintFieldunkid
            objUpdateProgress._Isvoid = False
            objUpdateProgress._Ownerunkid = mintOwnerunkid
            objUpdateProgress._Owrfieldunkid = mintOwrfield1unkid
            objUpdateProgress._Owrfieldtypeid = mintOwrFieldTypeId
            objUpdateProgress._Pct_Completed = mDecPct_Completed
            objUpdateProgress._Periodunkid = mintPeriodunkid
            objUpdateProgress._Remark = ""
            objUpdateProgress._Statusunkid = mintStatusunkid
            objUpdateProgress._Updatedate = ConfigParameter._Object._CurrentDateAndTime
            objUpdateProgress._Userunkid = mintUserunkid
            objUpdateProgress._Voiddatetime = Nothing
            objUpdateProgress._Voidreason = ""
            objUpdateProgress._Voiduserunkid = -1
            With objUpdateProgress
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objUpdateProgress.Insert() = False Then
                If objUpdateProgress._Message <> "" Then
                    mstrMessage = objUpdateProgress._Message & " " & Language.getMessage(mstrModuleName, 28, "You can track the changes by clicking Update Progress link from the list.")
                    Exit Try
                Else
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objUpdateProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_owrfield1_master) </purpose>
    Public Function Update(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, mintOwrfield1unkid, mintOwnerunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data for the selected owner.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield1unkid.ToString)
            objDataOperation.AddParameter("@ownerrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwenerrefid.ToString)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed.ToString)

            strQ = "UPDATE hrassess_owrfield1_master SET " & _
                   "  coyfield1unkid = @coyfield1unkid" & _
                   ", ownerrefid = @ownerrefid" & _
                   ", ownerunkid = @ownerunkid" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", field_data = @field_data" & _
                   ", yearunkid = @yearunkid" & _
                   ", periodunkid = @periodunkid" & _
                   ", weight = @weight" & _
                   ", startdate = @startdate" & _
                   ", enddate = @enddate" & _
                   ", statusunkid = @statusunkid" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   ", pct_completed = @pct_completed " & _
                   "WHERE owrfield1unkid = @owrfield1unkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_owrfield1_master", mintOwrfield1unkid, "owrfield1unkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_owrfield1_master", "owrfield1unkid", mintOwrfield1unkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_owrowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintOwrfield1unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintOwrfield1unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP [16 JUN 2015] -- START

            'S.SANDEEP [27 Jan 2016] -- START
            'Dim objUpdateProgress As New clsassess_owrupdate_tran
            'objUpdateProgress._DataOperation = objDataOperation
            'objUpdateProgress._Fieldunkid = mintFieldunkid
            'objUpdateProgress._Isvoid = False
            'objUpdateProgress._Ownerunkid = mintOwnerunkid
            'objUpdateProgress._Owrfieldunkid = mintOwrfield1unkid
            'objUpdateProgress._Owrfieldtypeid = mintOwrFieldTypeId
            'objUpdateProgress._Pct_Completed = mDecPct_Completed
            'objUpdateProgress._Periodunkid = mintPeriodunkid
            'objUpdateProgress._Remark = ""
            'objUpdateProgress._Statusunkid = mintStatusunkid
            'objUpdateProgress._Updatedate = ConfigParameter._Object._CurrentDateAndTime
            'objUpdateProgress._Userunkid = mintUserunkid
            'objUpdateProgress._Voiddatetime = Nothing
            'objUpdateProgress._Voidreason = ""
            'objUpdateProgress._Voiduserunkid = -1

            'If objUpdateProgress.Insert() = False Then
            '    If objUpdateProgress._Message <> "" Then
            '        mstrMessage = objUpdateProgress._Message & " " & Language.getMessage(mstrModuleName, 28, "You can track the changes by clicking Update Progress link from the list.")
            '        Exit Try
            '    Else
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'objUpdateProgress = Nothing
            'S.SANDEEP [27 Jan 2016] -- END

            'S.SANDEEP [16 JUN 2015] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 12, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objOwrField2 As New clsassess_owrfield2_master

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_owrfield1_master SET " & _
                   "  isvoid = @isvoid " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voidreason = @voidreason " & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE owrfield1unkid = @owrfield1unkid "

            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_owrfield1_master", "owrfield1unkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Dim dsChild As New DataSet : Dim dtmp As DataRow() = Nothing
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_owrfield2_master", "owrfield1unkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("isvoid = 0")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    objOwrField2._Isvoid = mblnIsvoid
                    objOwrField2._Voiddatetime = mdtVoiddatetime
                    objOwrField2._Voidreason = mstrVoidreason
                    objOwrField2._Voiduserunkid = mintVoiduserunkid

                    objOwrField2._FormName = mstrFormName
                    objOwrField2._LoginEmployeeunkid = mintLoginEmployeeunkid
                    objOwrField2._ClientIP = mstrClientIP
                    objOwrField2._HostName = mstrHostName
                    objOwrField2._FromWeb = mblnIsWeb
                    objOwrField2._AuditUserId = mintAuditUserId
objOwrField2._CompanyUnkid = mintCompanyUnkid
                    objOwrField2._AuditDate = mdtAuditDate

                    If objOwrField2.Delete(dr.Item("owrfield2unkid"), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_owrinfofield_tran", "owrfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_owrinfofield_tran", "owrinfofieldunkid", dr.Item("owrinfofieldunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
                strQ = "DELETE FROM hrassess_owrinfofield_tran WHERE owrfieldunkid = '" & intUnkid & "' AND owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_owrupdate_tran", "owrfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    Dim objProgress As New clsassess_owrupdate_tran

                    objProgress._DataOperation = objDataOperation
                    objProgress._Isvoid = mblnIsvoid
                    objProgress._Voiddatetime = mdtVoiddatetime
                    objProgress._Voidreason = mstrVoidreason
                    objProgress._Voiduserunkid = mintVoiduserunkid

                    objProgress._FormName = mstrFormName
                    objProgress._LoginEmployeeunkid = mintLoginEmployeeunkid
                    objProgress._ClientIP = mstrClientIP
                    objProgress._HostName = mstrHostName
                    objProgress._FromWeb = mblnIsWeb
                    objProgress._AuditUserId = mintAuditUserId
objProgress._CompanyUnkid = mintCompanyUnkid
                    objProgress._AuditDate = mdtAuditDate

                    If objProgress.Delete(dr.Item("owrupdatetranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objProgress = Nothing
                Next
            End If
            '==================|ENDING  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            'S.SANDEEP [16 JUN 2015] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT owrfield1unkid FROM hrassess_empfield1_master WHERE owrfield1unkid = '" & intUnkid & "' AND isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iFieldData As String, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal iOwnerId As Integer = 0, _
                            Optional ByVal iPeriodId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  owrfield1unkid " & _
                    ", coyfield1unkid " & _
                    ", ownerrefid " & _
                    ", ownerunkid " & _
                    ", fieldunkid " & _
                    ", field_data " & _
                    ", yearunkid " & _
                    ", periodunkid " & _
                    ", weight " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voidreason " & _
                    ", voiddatetime " & _
                    "FROM hrassess_owrfield1_master " & _
                    "WHERE field_data = @field_data AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND owrfield1unkid <> @owrfield1unkid"
            End If

            If iOwnerId > 0 Then
                strQ &= " AND ownerunkid = @ownerunkid "
            End If

            If iPeriodId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If


            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, iFieldData)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, iFieldData.Length, iFieldData)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwnerId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get Data Set for Combo </purpose>
    Public Function getComboList(ByVal mdtEmployeeAsOnDate As DateTime, _
                                 Optional ByVal iList As String = "List", _
                                 Optional ByVal iAddSelect As Boolean = False, _
                                 Optional ByVal iPeriodId As Integer = -1, _
                                 Optional ByVal iEmployeeId As Integer = 0, _
                                 Optional ByVal iOwnerId As Integer = 0, _
                                 Optional ByVal iOnlyCommit As Boolean = False) As DataSet

        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        ' mdtEmployeeAsOnDate ----- ADDED
        'S.SANDEEP [04 JUN 2015] -- END

        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim iValueId As Integer = 0
        objDataOperation = New clsDataOperation
        Try
            If iEmployeeId > 0 Then
                Dim objEmp As New clsEmployee_Master
                StrQ = "SELECT DISTINCT Top 1 ownerrefid FROM hrassess_owrfield1_master WHERE isvoid = 0 "
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid(mdtEmployeeAsOnDate) = iEmployeeId
                objEmp._Employeeunkid(mdtEmployeeAsOnDate) = iEmployeeId
                'S.SANDEEP [04 JUN 2015] -- END

                If dsList.Tables(0).Rows.Count > 0 Then
                    Select Case CInt(dsList.Tables(0).Rows(0).Item("ownerrefid"))
                        Case enAllocation.BRANCH
                            iValueId = objEmp._Stationunkid
                        Case enAllocation.DEPARTMENT_GROUP
                            iValueId = objEmp._Deptgroupunkid
                        Case enAllocation.DEPARTMENT
                            iValueId = objEmp._Departmentunkid
                        Case enAllocation.SECTION_GROUP
                            iValueId = objEmp._Sectiongroupunkid
                        Case enAllocation.SECTION
                            iValueId = objEmp._Sectionunkid
                        Case enAllocation.UNIT_GROUP
                            iValueId = objEmp._Unitgroupunkid
                        Case enAllocation.UNIT
                            iValueId = objEmp._Unitunkid
                        Case enAllocation.TEAM
                            iValueId = objEmp._Teamunkid
                        Case enAllocation.JOB_GROUP
                            iValueId = objEmp._Jobgroupunkid
                        Case enAllocation.JOBS
                            iValueId = objEmp._Jobunkid
                        Case enAllocation.CLASS_GROUP
                            iValueId = objEmp._Classgroupunkid
                        Case enAllocation.CLASSES
                            iValueId = objEmp._Classunkid
                    End Select
                End If
                objEmp = Nothing
            End If
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT owrfield1unkid AS Id, field_data AS Name FROM hrassess_owrfield1_master WHERE isvoid = 0 "

            If iPeriodId > -1 Then
                StrQ &= " AND periodunkid = '" & iPeriodId & "' "
            End If

            If iOwnerId > 0 Then
                StrQ &= " AND ownerunkid = '" & iOwnerId & "' "
            End If

            If iValueId > 0 Then
                StrQ &= " AND ownerunkid = '" & iValueId & "' "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtTab As DataTable = Nothing
            If iOnlyCommit = True Then
                Dim iLastStatusId As Integer = 0
                If iEmployeeId > 0 Then
                    iLastStatusId = GetLastStatus(iPeriodId, iValueId)
                Else
                    iLastStatusId = GetLastStatus(iPeriodId, iOwnerId)
                End If
                If iLastStatusId <> enObjective_Status.FINAL_COMMITTED Then
                    mdtTab = New DataView(dsList.Tables(0), "Id IN(0)", "", DataViewRowState.CurrentRows).ToTable
                    dsList.Tables.RemoveAt(0) : dsList.Tables.Add(mdtTab.Copy)
                End If
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
    End Function

    Public Function GetDisplayList(ByVal iOwnerId As Integer, ByVal iPeriodId As Integer, Optional ByVal iList As String = "") As DataTable
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn = Nothing
        Dim iCaptionName As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If iList.Trim.Length <= 0 Then iList = "List"
            mdtFinal = New DataTable(iList)

            'dCol = New DataColumn
            'dCol.ColumnName = "OwnerCategory"
            'dCol.Caption = Language.getMessage(mstrModuleName, 3, "Owner Type")
            'dCol.DataType = System.Type.GetType("System.String")
            'dCol.DefaultValue = ""
            'mdtFinal.Columns.Add(dCol)

            'dCol = New DataColumn
            'dCol.ColumnName = "Owner"
            'dCol.Caption = Language.getMessage(mstrModuleName, 4, "Owner")
            'dCol.DataType = System.Type.GetType("System.String")
            'dCol.DefaultValue = ""
            'mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CoyField1"
            Dim objFMaster As New clsAssess_Field_Master(True)
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "Company") & " " & objFMaster._Field1_Caption
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(objFMaster._Field1Unkid))
            mdtFinal.Columns.Add(dCol)
            dsList = objFMaster.Get_Field_Mapping(iList)

            Dim objMap As New clsAssess_Field_Mapping
            iCaptionName = objMap.Get_Map_FieldName(iPeriodId)
            objMap = Nothing

            Dim i As Integer = 1
            If dsList.Tables(iList).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(iList).Rows
                    dCol = New DataColumn
                    If i <= 5 Then
                        dCol.ColumnName = "Field" & i.ToString
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString
                    End If
                    dCol.Caption = dRow.Item("fieldcaption").ToString
                    dCol.DataType = System.Type.GetType("System.String")
                    dCol.DefaultValue = ""
                    dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(dRow.Item("fieldunkid")))
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    If i <= 5 Then
                        dCol.ColumnName = "Field" & i.ToString & "Id"
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString & "Id"
                    End If
                    dCol.DataType = System.Type.GetType("System.Int32")
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    mdtFinal.Columns.Add(dCol)
                    i += 1
                Next
            End If
            dCol = New DataColumn
            dCol.ColumnName = "St_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "Start Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.ST_DATE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Ed_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "End Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.ED_DATE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatus"
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "Status")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.STATUS))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "OPeriod"
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "Period")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatusId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ownerunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "periodunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Weight"
            'S.SANDEEP [ 16 JAN 2015 ] -- START
            'dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 11, "Weight")
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "Weight")
            'S.SANDEEP [ 16 JAN 2015 ] -- END
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.WEIGHT))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "pct_complete"
            dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 12, "% Completed")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "coyfield1unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "owrfield1unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "owrfield2unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "owrfield3unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "owrfield4unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "owrfield5unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "sdate"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "edate"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ownerrefid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "opstatusid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "OwnerIds"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CFieldTypeId"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [16 JUN 2015] -- START
            dCol = New DataColumn
            dCol.ColumnName = "vuRemark"
            dCol.Caption = Language.getMessage(mstrModuleName, 29, "Progress Remark")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "vuProgress"
            dCol.Caption = Language.getMessage(mstrModuleName, 24, "View/Update Progress")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = Language.getMessage(mstrModuleName, 25, "Update Progress")
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [16 JUN 2015] -- END

            StrQ = "DECLARE @LinkFieldId AS INT " & _
                   "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WHERE periodunkid = '" & iPeriodId & "' AND isactive = 1),0) " & _
                   "SELECT DISTINCT " & _
                   "  ISNULL(OwnerCategory,'') AS OwnerCategory " & _
                   " ,Owner AS Owner " & _
                   " ,CoyField1 AS CoyField1 " & _
                   " ,Field1 " & _
                   " ,Field2 " & _
                   " ,Field3 " & _
                   " ,Field4 " & _
                   " ,Field5 " & _
                   " ,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_SDate " & _
                   "			  WHEN @LinkFieldId = Field2Id THEN f2_SDate " & _
                   "			  WHEN @LinkFieldId = Field3Id THEN f3_SDate " & _
                   "			  WHEN @LinkFieldId = Field4Id THEN f4_SDate " & _
                   "			  WHEN @LinkFieldId = Field5Id THEN f5_SDate " & _
                   "		 END,'') AS St_Date " & _
                   " ,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_EDate " & _
                   " 			  WHEN @LinkFieldId = Field2Id THEN f2_EDate " & _
                   "			  WHEN @LinkFieldId = Field3Id THEN f3_EDate " & _
                   "			  WHEN @LinkFieldId = Field4Id THEN f4_EDate " & _
                   "			  WHEN @LinkFieldId = Field5Id THEN f5_EDate " & _
                   "		END,'') AS Ed_Date " & _
                   " ,ISNULL(CASE WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "							   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "			   				 END,0) = 1 THEN @ST_PENDING " & _
                   "			  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "					 		   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "							 END,0) = 2 THEN @ST_INPROGRESS " & _
                   "			  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "							   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "							END,0) = 3 THEN @ST_COMPLETE " & _
                   "			  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "							   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "							END,0) = 4 THEN @ST_CLOSED " & _
                   "			  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "							   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "							END,0) = 5 THEN @ST_ONTRACK " & _
                   "			  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "							   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "							END,0) = 6 THEN @ST_ATRISK " & _
                   "			  WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "							   WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "							   WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "							   WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "							   WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "							END,0) = 7 THEN @ST_NOTAPPLICABLE " & _
                   "			END,'') AS CStatus " & _
                   " ,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                   "			  WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                   "			  WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                   "			  WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                   "			  WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                   "			END,0) AS CStatusId " & _
                   " ,ownerrefid " & _
                   " ,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN CASE WHEN f1weight <=0 THEN '' ELSE CAST(f1weight AS NVARCHAR(MAX)) END " & _
                   "			  WHEN @LinkFieldId = Field2Id THEN CASE WHEN f2weight <=0 THEN '' ELSE CAST(f2weight AS NVARCHAR(MAX)) END " & _
                   "			  WHEN @LinkFieldId = Field3Id THEN CASE WHEN f3weight <=0 THEN '' ELSE CAST(f3weight AS NVARCHAR(MAX)) END " & _
                   "			  WHEN @LinkFieldId = Field4Id THEN CASE WHEN f4weight <=0 THEN '' ELSE CAST(f4weight AS NVARCHAR(MAX)) END " & _
                   "			  WHEN @LinkFieldId = Field5Id THEN CASE WHEN f5weight <=0 THEN '' ELSE CAST(f5weight AS NVARCHAR(MAX)) END " & _
                   "		 END,'') AS Weight " & _
                   ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN CASE WHEN f1pct <= 0 THEN '' ELSE CAST(f1pct AS NVARCHAR(MAX)) END " & _
                   "              WHEN @LinkFieldId = Field2Id THEN CASE WHEN f2pct <= 0 THEN '' ELSE CAST(f2pct AS NVARCHAR(MAX)) END " & _
                   "              WHEN @LinkFieldId = Field3Id THEN CASE WHEN f3pct <= 0 THEN '' ELSE CAST(f3pct AS NVARCHAR(MAX)) END " & _
                   "              WHEN @LinkFieldId = Field4Id THEN CASE WHEN f4pct <= 0 THEN '' ELSE CAST(f4pct AS NVARCHAR(MAX)) END " & _
                   "              WHEN @LinkFieldId = Field5Id THEN CASE WHEN f5pct <= 0 THEN '' ELSE CAST(f5pct AS NVARCHAR(MAX)) END " & _
                   "         END, '') AS pct_complete " & _
                   " ,ISNULL(owrfield1unkid,0) AS owrfield1unkid " & _
                   " ,ISNULL(owrfield2unkid,0) AS owrfield2unkid " & _
                   " ,ISNULL(owrfield3unkid,0) AS owrfield3unkid " & _
                   " ,ISNULL(owrfield4unkid,0) AS owrfield4unkid " & _
                   " ,ISNULL(owrfield5unkid,0) AS owrfield5unkid " & _
                   " ,Field1Id " & _
                   " ,Field2Id " & _
                   " ,Field3Id " & _
                   " ,Field4Id " & _
                   " ,Field5Id " & _
                   " ,yearunkid " & _
                   " ,periodunkid " & _
                   " ,OPeriod " & _
                   " ,coyfield1unkid " & _
                   " ,ownerunkid " & _
                   " ,ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN '" & enWeight_Types.WEIGHT_FIELD1 & "' " & _
                   "			  WHEN @LinkFieldId = Field2Id THEN '" & enWeight_Types.WEIGHT_FIELD2 & "' " & _
                   "			  WHEN @LinkFieldId = Field3Id THEN '" & enWeight_Types.WEIGHT_FIELD3 & "' " & _
                   "			  WHEN @LinkFieldId = Field4Id THEN '" & enWeight_Types.WEIGHT_FIELD4 & "' " & _
                   "			  WHEN @LinkFieldId = Field5Id THEN '" & enWeight_Types.WEIGHT_FIELD5 & "' " & _
                   "         END,0) AS CFieldTypeId " & _
                   ",'' AS OwnerIds " & _
                   "FROM ( " & _
                   "	SELECT " & _
                   "		 CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN @BRANCH " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN @DEPARTMENT_GROUP " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN @DEPARTMENT " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN @SECTION_GROUP " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN @SECTION " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN @UNIT_GROUP " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN @UNIT " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN @TEAM " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN @JOB_GROUP " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN @JOBS " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN @CLASS_GROUP " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN @CLASSES " & _
                   "		 ELSE '' END AS OwnerCategory " & _
                   "		,CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN ISNULL(hrstation_master.name,'') " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN ISNULL(hrdepartment_group_master.name,'') " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN ISNULL(hrdepartment_master.name,'')  " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN ISNULL(hrsectiongroup_master.name,'') " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN ISNULL(hrsection_master.name,'')   " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN ISNULL(hrunitgroup_master.name,'')   " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN ISNULL(hrunit_master.name,'')   " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN ISNULL(hrteam_master.name,'')   " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN ISNULL(hrjobgroup_master.name,'')  " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN ISNULL(hrjob_master.job_name,'')  " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN ISNULL(hrclassgroup_master.name,'')  " & _
                   "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN ISNULL(hrclasses_master.name,'')  " & _
                   "         ELSE '' END AS Owner " & _
                   "		,ISNULL(hrassess_coyfield1_master.field_data,'') AS CoyField1 " & _
                   "		,ISNULL(hrassess_owrfield1_master.field_data,'') AS Field1 " & _
                   "		,ISNULL(hrassess_owrfield2_master.field_data,'') AS Field2 " & _
                   "		,ISNULL(hrassess_owrfield3_master.field_data,'') AS Field3 " & _
                   "		,ISNULL(hrassess_owrfield4_master.field_data,'') AS Field4 " & _
                   "		,ISNULL(hrassess_owrfield5_master.field_data,'') AS Field5 " & _
                   "		,ISNULL(hrassess_owrfield1_master.fieldunkid,0) AS Field1Id " & _
                   "		,ISNULL(hrassess_owrfield2_master.fieldunkid,0) AS Field2Id " & _
                   "		,ISNULL(hrassess_owrfield3_master.fieldunkid,0) AS Field3Id " & _
                   "		,ISNULL(hrassess_owrfield4_master.fieldunkid,0) AS Field4Id " & _
                   "		,ISNULL(hrassess_owrfield5_master.fieldunkid,0) AS Field5Id " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.startdate,112),'') AS f1_SDate " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.enddate,112),'') AS f1_EDate " & _
                   "		,ISNULL(hrassess_owrfield1_master.statusunkid,0) AS f1StatId " & _
                   "		,ISNULL(hrassess_owrfield1_master.ownerrefid,0) AS ownerrefid " & _
                   "		,ISNULL(hrassess_owrfield1_master.weight,0) AS f1weight " & _
                   "        ,ISNULL(hrassess_owrfield1_master.pct_completed, 0) AS f1pct " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.startdate,112),'') AS f2_SDate " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.enddate,112),'') AS f2_EDate " & _
                   "		,ISNULL(hrassess_owrfield2_master.statusunkid,0) AS f2StatId " & _
                   "		,ISNULL(hrassess_owrfield2_master.weight,0) AS f2weight " & _
                   "        ,ISNULL(hrassess_owrfield2_master.pct_completed, 0) AS f2pct " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.startdate,112),'') AS f3_SDate " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.enddate,112),'') AS f3_EDate " & _
                   "		,ISNULL(hrassess_owrfield3_master.statusunkid,0) AS f3StatId " & _
                   "		,ISNULL(hrassess_owrfield3_master.weight,0) AS f3weight " & _
                   "        ,ISNULL(hrassess_owrfield3_master.pct_completed, 0) AS f3pct " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.startdate,112),'') AS f4_SDate " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.enddate,112),'') AS f4_EDate " & _
                   "		,ISNULL(hrassess_owrfield4_master.statusunkid,0) AS f4StatId " & _
                   "		,ISNULL(hrassess_owrfield4_master.weight,0) AS f4weight " & _
                   "        ,ISNULL(hrassess_owrfield4_master.pct_completed, 0) AS f4pct " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.startdate,112),'') AS f5_SDate " & _
                   "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.enddate,112),'') AS f5_EDate " & _
                   "		,ISNULL(hrassess_owrfield5_master.statusunkid,0) AS f5StatId " & _
                   "		,ISNULL(hrassess_owrfield5_master.weight,0) AS f5weight " & _
                   "        ,ISNULL(hrassess_owrfield5_master.pct_completed, 0) AS f5pct " & _
                   "		,hrassess_owrfield1_master.owrfield1unkid " & _
                   "		,hrassess_owrfield2_master.owrfield2unkid " & _
                   "		,hrassess_owrfield3_master.owrfield3unkid " & _
                   "		,hrassess_owrfield4_master.owrfield4unkid " & _
                   "		,hrassess_owrfield5_master.owrfield5unkid " & _
                   "		,ISNULL(hrassess_owrfield1_master.yearunkid,0) AS yearunkid " & _
                   "		,ISNULL(hrassess_owrfield1_master.periodunkid,0) AS periodunkid " & _
                   "		,ISNULL(period_name,'') AS OPeriod " & _
                   "		,ISNULL(hrassess_owrfield1_master.coyfield1unkid,0) AS coyfield1unkid " & _
                   "		,hrassess_owrfield1_master.ownerunkid " & _
                   "	FROM hrassess_owrfield1_master " & _
                   "        LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_owrfield1_master.ownerunkid	" & _
                   "        LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                   "        LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_owrfield1_master.ownerunkid " & _
                   "		LEFT JOIN hrassess_coyfield1_master ON hrassess_owrfield1_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid " & _
                   "		LEFT JOIN cfcommon_period_tran ON hrassess_owrfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "		LEFT JOIN hrassess_owrfield2_master ON hrassess_owrfield1_master.owrfield1unkid = hrassess_owrfield2_master.owrfield1unkid AND hrassess_owrfield2_master.isvoid = 0 " & _
                   "		LEFT JOIN hrassess_owrfield3_master ON hrassess_owrfield2_master.owrfield2unkid = hrassess_owrfield3_master.owrfield2unkid AND hrassess_owrfield3_master.isvoid = 0 " & _
                   "		LEFT JOIN hrassess_owrfield4_master ON hrassess_owrfield3_master.owrfield3unkid = hrassess_owrfield4_master.owrfield3unkid AND hrassess_owrfield4_master.isvoid = 0 " & _
                   "		LEFT JOIN hrassess_owrfield5_master ON hrassess_owrfield4_master.owrfield4unkid = hrassess_owrfield5_master.owrfield4unkid AND hrassess_owrfield5_master.isvoid = 0 " & _
                   "	WHERE hrassess_owrfield1_master.isvoid = 0 AND hrassess_owrfield1_master.ownerunkid = '" & iOwnerId & "' "
            If iPeriodId > 0 Then
                StrQ &= " AND hrassess_owrfield1_master.periodunkid = '" & iPeriodId & "' "
            End If
            StrQ &= "UNION " & _
                    "	SELECT " & _
                    "		  CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN @BRANCH " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN @DEPARTMENT_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN @DEPARTMENT " & _
                    " 			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN @SECTION_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN @SECTION " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN @UNIT_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN @UNIT " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN @TEAM " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN @JOB_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN @JOBS " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN @CLASS_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN @CLASSES " & _
                    "		 ELSE '' END AS OwnerCategory " & _
                    "		,CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN ISNULL(hrstation_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN ISNULL(hrdepartment_group_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN ISNULL(hrdepartment_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN ISNULL(hrsectiongroup_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN ISNULL(hrsection_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN ISNULL(hrunitgroup_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN ISNULL(hrunit_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN ISNULL(hrteam_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN ISNULL(hrjobgroup_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN ISNULL(hrjob_master.job_name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN ISNULL(hrclassgroup_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN ISNULL(hrclasses_master.name,'')  " & _
                    "         ELSE '' END AS Owner " & _
                    "		,ISNULL(hrassess_coyfield1_master.field_data,'') AS CoyField1 " & _
                    "		,ISNULL(hrassess_owrfield1_master.field_data,'') AS Field1 " & _
                    "		,ISNULL(hrassess_owrfield2_master.field_data,'') AS Field2 " & _
                    "		,ISNULL(hrassess_owrfield3_master.field_data,'') AS Field3 " & _
                    "		,ISNULL(hrassess_owrfield4_master.field_data,'') AS Field4 " & _
                    "		,ISNULL(hrassess_owrfield5_master.field_data,'') AS Field5 " & _
                    "		,ISNULL(hrassess_owrfield1_master.fieldunkid,0) AS Field1Id " & _
                    "		,ISNULL(hrassess_owrfield2_master.fieldunkid,0) AS Field2Id " & _
                    " 		,ISNULL(hrassess_owrfield3_master.fieldunkid,0) AS Field3Id " & _
                    "		,ISNULL(hrassess_owrfield4_master.fieldunkid,0) AS Field4Id " & _
                    "		,ISNULL(hrassess_owrfield5_master.fieldunkid,0) AS Field5Id " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.startdate,112),'') AS f1_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.enddate,112),'') AS f1_EDate " & _
                    "		,ISNULL(hrassess_owrfield1_master.statusunkid,0) AS f1StatId " & _
                    "		,ISNULL(hrassess_owrfield1_master.ownerrefid,0) AS ownerrefid " & _
                    "		,ISNULL(hrassess_owrfield1_master.weight,0) AS f1weight " & _
                    "       ,ISNULL(hrassess_owrfield1_master.pct_completed, 0) AS f1pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.startdate,112),'') AS f2_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.enddate,112),'') AS f2_EDate " & _
                    "		,ISNULL(hrassess_owrfield2_master.statusunkid,0) AS f2StatId " & _
                    "		,ISNULL(hrassess_owrfield2_master.weight,0) AS f2weight " & _
                    "       ,ISNULL(hrassess_owrfield2_master.pct_completed, 0) AS f2pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.startdate,112),'') AS f3_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.enddate,112),'') AS f3_EDate " & _
                    "		,ISNULL(hrassess_owrfield3_master.statusunkid,0) AS f3StatId " & _
                    "		,ISNULL(hrassess_owrfield3_master.weight,0) AS f3weight " & _
                    "       ,ISNULL(hrassess_owrfield3_master.pct_completed, 0) AS f3pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.startdate,112),'') AS f4_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.enddate,112),'') AS f4_EDate " & _
                    "		,ISNULL(hrassess_owrfield4_master.statusunkid,0) AS f4StatId " & _
                    "		,ISNULL(hrassess_owrfield4_master.weight,0) AS f4weight " & _
                    "       ,ISNULL(hrassess_owrfield4_master.pct_completed, 0) AS f4pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.startdate,112),'') AS f5_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.enddate,112),'') AS f5_EDate " & _
                    "		,ISNULL(hrassess_owrfield5_master.statusunkid,0) AS f5StatId " & _
                    "		,ISNULL(hrassess_owrfield5_master.weight,0) AS f5weight " & _
                    "       ,ISNULL(hrassess_owrfield5_master.pct_completed, 0) AS f5pct " & _
                    "		,hrassess_owrfield1_master.owrfield1unkid " & _
                    "		,hrassess_owrfield2_master.owrfield2unkid " & _
                    "		,hrassess_owrfield3_master.owrfield3unkid " & _
                    "		,hrassess_owrfield4_master.owrfield4unkid " & _
                    "		,hrassess_owrfield5_master.owrfield5unkid " & _
                    "		,ISNULL(hrassess_owrfield1_master.yearunkid,0) AS yearunkid " & _
                    "		,ISNULL(hrassess_owrfield1_master.periodunkid,0) AS periodunkid " & _
                    "		,ISNULL(period_name,'') AS OPeriod " & _
                    "		,ISNULL(hrassess_owrfield1_master.coyfield1unkid,0) AS coyfield1unkid " & _
                    "		,hrassess_owrfield1_master.ownerunkid " & _
                    "	FROM hrassess_owrfield2_master " & _
                    "		LEFT JOIN hrassess_owrfield1_master ON hrassess_owrfield2_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_coyfield1_master ON hrassess_owrfield1_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid " & _
                    "		LEFT JOIN cfcommon_period_tran ON hrassess_owrfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "       LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                    "       LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_owrfield1_master.ownerunkid " & _
                    "		LEFT JOIN hrassess_owrfield3_master ON hrassess_owrfield2_master.owrfield2unkid = hrassess_owrfield3_master.owrfield2unkid AND hrassess_owrfield3_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_owrfield4_master ON hrassess_owrfield3_master.owrfield3unkid = hrassess_owrfield4_master.owrfield3unkid AND hrassess_owrfield4_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_owrfield5_master ON hrassess_owrfield4_master.owrfield4unkid = hrassess_owrfield5_master.owrfield4unkid AND hrassess_owrfield5_master.isvoid = 0 " & _
                    "	WHERE hrassess_owrfield2_master.isvoid = 0 AND hrassess_owrfield1_master.ownerunkid = '" & iOwnerId & "' "
            If iPeriodId > 0 Then
                StrQ &= " AND hrassess_owrfield1_master.periodunkid = '" & iPeriodId & "' "
            End If
            StrQ &= "UNION " & _
                    "	SELECT " & _
                    "		  CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN @BRANCH " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN @DEPARTMENT_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN @DEPARTMENT " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN @SECTION_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN @SECTION " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN @UNIT_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN @UNIT " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN @TEAM " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN @JOB_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN @JOBS " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN @CLASS_GROUP " & _
                    "			   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN @CLASSES " & _
                    "		  ELSE '' END AS OwnerCategory " & _
                    "		,CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN ISNULL(hrstation_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN ISNULL(hrdepartment_group_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN ISNULL(hrdepartment_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN ISNULL(hrsectiongroup_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN ISNULL(hrsection_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN ISNULL(hrunitgroup_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN ISNULL(hrunit_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN ISNULL(hrteam_master.name,'')   " & _
                    " 			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN ISNULL(hrjobgroup_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN ISNULL(hrjob_master.job_name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN ISNULL(hrclassgroup_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN ISNULL(hrclasses_master.name,'')  " & _
                    "         ELSE '' END AS Owner " & _
                    "		,ISNULL(hrassess_coyfield1_master.field_data,'') AS CoyField1 " & _
                    "		,ISNULL(hrassess_owrfield1_master.field_data,'') AS Field1 " & _
                    "		,ISNULL(hrassess_owrfield2_master.field_data,'') AS Field2 " & _
                    "		,ISNULL(hrassess_owrfield3_master.field_data,'') AS Field3 " & _
                    "		,ISNULL(hrassess_owrfield4_master.field_data,'') AS Field4 " & _
                    "		,ISNULL(hrassess_owrfield5_master.field_data,'') AS Field5 " & _
                    "		,ISNULL(hrassess_owrfield1_master.fieldunkid,0) AS Field1Id " & _
                    "		,ISNULL(hrassess_owrfield2_master.fieldunkid,0) AS Field2Id " & _
                    "		,ISNULL(hrassess_owrfield3_master.fieldunkid,0) AS Field3Id " & _
                    "		,ISNULL(hrassess_owrfield4_master.fieldunkid,0) AS Field4Id " & _
                    "		,ISNULL(hrassess_owrfield5_master.fieldunkid,0) AS Field5Id " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.startdate,112),'') AS f1_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.enddate,112),'') AS f1_EDate " & _
                    "		,ISNULL(hrassess_owrfield1_master.statusunkid,0) AS f1StatId " & _
                    "		,ISNULL(hrassess_owrfield1_master.ownerrefid,0) AS ownerrefid " & _
                    "		,ISNULL(hrassess_owrfield1_master.weight,0) AS f1weight " & _
                    "       ,ISNULL(hrassess_owrfield1_master.pct_completed, 0) AS f1pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.startdate,112),'') AS f2_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.enddate,112),'') AS f2_EDate " & _
                    "		,ISNULL(hrassess_owrfield2_master.statusunkid,0) AS f2StatId " & _
                    "		,ISNULL(hrassess_owrfield2_master.weight,0) AS f2weight " & _
                    "       ,ISNULL(hrassess_owrfield2_master.pct_completed, 0) AS f2pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.startdate,112),'') AS f3_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.enddate,112),'') AS f3_EDate " & _
                    "		,ISNULL(hrassess_owrfield3_master.statusunkid,0) AS f3StatId " & _
                    "		,ISNULL(hrassess_owrfield3_master.weight,0) AS f3weight " & _
                    "       ,ISNULL(hrassess_owrfield3_master.pct_completed, 0) AS f3pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.startdate,112),'') AS f4_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.enddate,112),'') AS f4_EDate " & _
                    "		,ISNULL(hrassess_owrfield4_master.statusunkid,0) AS f4StatId " & _
                    "		,ISNULL(hrassess_owrfield4_master.weight,0) AS f4weight " & _
                    "       ,ISNULL(hrassess_owrfield4_master.pct_completed, 0) AS f4pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.startdate,112),'') AS f5_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.enddate,112),'') AS f5_EDate " & _
                    "		,ISNULL(hrassess_owrfield5_master.statusunkid,0) AS f5StatId " & _
                    "		,ISNULL(hrassess_owrfield5_master.weight,0) AS f5weight " & _
                    "       ,ISNULL(hrassess_owrfield5_master.pct_completed, 0) AS f5pct " & _
                    "		,hrassess_owrfield1_master.owrfield1unkid " & _
                    "		,hrassess_owrfield2_master.owrfield2unkid " & _
                    "		,hrassess_owrfield3_master.owrfield3unkid " & _
                    "		,hrassess_owrfield4_master.owrfield4unkid " & _
                    "		,hrassess_owrfield5_master.owrfield5unkid " & _
                    "		,ISNULL(hrassess_owrfield1_master.yearunkid,0) AS yearunkid " & _
                    "		,ISNULL(hrassess_owrfield1_master.periodunkid,0) AS periodunkid " & _
                    "		,ISNULL(period_name,'') AS OPeriod " & _
                    "		,ISNULL(hrassess_owrfield1_master.coyfield1unkid,0) AS coyfield1unkid " & _
                    "		,hrassess_owrfield1_master.ownerunkid " & _
                    "	FROM hrassess_owrfield3_master " & _
                    "		LEFT JOIN hrassess_owrfield2_master ON hrassess_owrfield3_master.owrfield2unkid = hrassess_owrfield2_master.owrfield2unkid AND hrassess_owrfield2_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_owrfield1_master ON hrassess_owrfield2_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_coyfield1_master ON hrassess_owrfield1_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid " & _
                    " 		LEFT JOIN cfcommon_period_tran ON hrassess_owrfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "       LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                    "       LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_owrfield1_master.ownerunkid " & _
                    "		LEFT JOIN hrassess_owrfield4_master ON hrassess_owrfield3_master.owrfield3unkid = hrassess_owrfield4_master.owrfield3unkid AND hrassess_owrfield4_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_owrfield5_master ON hrassess_owrfield4_master.owrfield4unkid = hrassess_owrfield5_master.owrfield4unkid AND hrassess_owrfield5_master.isvoid = 0 " & _
                    "	WHERE hrassess_owrfield3_master.isvoid = 0 AND hrassess_owrfield1_master.ownerunkid = '" & iOwnerId & "' "
            If iPeriodId > 0 Then
                StrQ &= " AND hrassess_owrfield1_master.periodunkid = '" & iPeriodId & "' "
            End If
            StrQ &= "UNION " & _
                    "	SELECT " & _
                    "		  CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN @BRANCH " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN @DEPARTMENT_GROUP " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN @DEPARTMENT " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN @SECTION_GROUP " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN @SECTION " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN @UNIT_GROUP " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN @UNIT " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN @TEAM " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN @JOB_GROUP " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN @JOBS " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN @CLASS_GROUP " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN @CLASSES " & _
                    "		 ELSE '' END AS OwnerCategory " & _
                    "		,CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN ISNULL(hrstation_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN ISNULL(hrdepartment_group_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN ISNULL(hrdepartment_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN ISNULL(hrsectiongroup_master.name,'') " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN ISNULL(hrsection_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN ISNULL(hrunitgroup_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN ISNULL(hrunit_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN ISNULL(hrteam_master.name,'')   " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN ISNULL(hrjobgroup_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN ISNULL(hrjob_master.job_name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN ISNULL(hrclassgroup_master.name,'')  " & _
                    "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN ISNULL(hrclasses_master.name,'')  " & _
                    "         ELSE '' END AS Owner " & _
                    "		,ISNULL(hrassess_coyfield1_master.field_data,'') AS CoyField1 " & _
                    "		,ISNULL(hrassess_owrfield1_master.field_data,'') AS Field1 " & _
                    "		,ISNULL(hrassess_owrfield2_master.field_data,'') AS Field2 " & _
                    "		,ISNULL(hrassess_owrfield3_master.field_data,'') AS Field3 " & _
                    "		,ISNULL(hrassess_owrfield4_master.field_data,'') AS Field4 " & _
                    "		,ISNULL(hrassess_owrfield5_master.field_data,'') AS Field5 " & _
                    "		,ISNULL(hrassess_owrfield1_master.fieldunkid,0) AS Field1Id " & _
                    "		,ISNULL(hrassess_owrfield2_master.fieldunkid,0) AS Field2Id " & _
                    "		,ISNULL(hrassess_owrfield3_master.fieldunkid,0) AS Field3Id " & _
                    "		,ISNULL(hrassess_owrfield4_master.fieldunkid,0) AS Field4Id " & _
                    "		,ISNULL(hrassess_owrfield5_master.fieldunkid,0) AS Field5Id " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.startdate,112),'') AS f1_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.enddate,112),'') AS f1_EDate " & _
                    "		,ISNULL(hrassess_owrfield1_master.statusunkid,0) AS f1StatId " & _
                    "		,ISNULL(hrassess_owrfield1_master.ownerrefid,0) AS ownerrefid " & _
                    "		,ISNULL(hrassess_owrfield1_master.weight,0) AS f1weight " & _
                    "       ,ISNULL(hrassess_owrfield1_master.pct_completed, 0) AS f1pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.startdate,112),'') AS f2_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.enddate,112),'') AS f2_EDate " & _
                    "		,ISNULL(hrassess_owrfield2_master.statusunkid,0) AS f2StatId " & _
                    "		,ISNULL(hrassess_owrfield2_master.weight,0) AS f2weight " & _
                    "       ,ISNULL(hrassess_owrfield2_master.pct_completed, 0) AS f2pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.startdate,112),'') AS f3_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.enddate,112),'') AS f3_EDate " & _
                    "		,ISNULL(hrassess_owrfield3_master.statusunkid,0) AS f3StatId " & _
                    "		,ISNULL(hrassess_owrfield3_master.weight,0) AS f3weight " & _
                    "       ,ISNULL(hrassess_owrfield3_master.pct_completed, 0) AS f3pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.startdate,112),'') AS f4_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.enddate,112),'') AS f4_EDate " & _
                    "		,ISNULL(hrassess_owrfield4_master.statusunkid,0) AS f4StatId " & _
                    "		,ISNULL(hrassess_owrfield4_master.weight,0) AS f4weight " & _
                    "       ,ISNULL(hrassess_owrfield4_master.pct_completed, 0) AS f4pct " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.startdate,112),'') AS f5_SDate " & _
                    "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.enddate,112),'') AS f5_EDate " & _
                    "		,ISNULL(hrassess_owrfield5_master.statusunkid,0) AS f5StatId " & _
                    "		,ISNULL(hrassess_owrfield5_master.weight,0) AS f5weight " & _
                    "       ,ISNULL(hrassess_owrfield5_master.pct_completed, 0) AS f5pct " & _
                    "		,hrassess_owrfield1_master.owrfield1unkid " & _
                    "		,hrassess_owrfield2_master.owrfield2unkid " & _
                    "		,hrassess_owrfield3_master.owrfield3unkid " & _
                    "		,hrassess_owrfield4_master.owrfield4unkid " & _
                    "		,hrassess_owrfield5_master.owrfield5unkid " & _
                    "		,ISNULL(hrassess_owrfield1_master.yearunkid,0) AS yearunkid " & _
                    "		,ISNULL(hrassess_owrfield1_master.periodunkid,0) AS periodunkid " & _
                    "		,ISNULL(period_name,'') AS OPeriod " & _
                    "		,ISNULL(hrassess_owrfield1_master.coyfield1unkid,0) AS coyfield1unkid " & _
                    "		,hrassess_owrfield1_master.ownerunkid " & _
                    "	FROM hrassess_owrfield4_master " & _
                    "		LEFT JOIN hrassess_owrfield3_master ON hrassess_owrfield4_master.owrfield3unkid = hrassess_owrfield3_master.owrfield3unkid AND hrassess_owrfield3_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_owrfield2_master ON hrassess_owrfield3_master.owrfield2unkid = hrassess_owrfield2_master.owrfield2unkid AND hrassess_owrfield2_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_owrfield1_master ON hrassess_owrfield2_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                    "		LEFT JOIN hrassess_coyfield1_master ON hrassess_owrfield1_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid " & _
                    "		LEFT JOIN cfcommon_period_tran ON hrassess_owrfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "       LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_owrfield1_master.ownerunkid	" & _
                    "       LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                    "       LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_owrfield1_master.ownerunkid " & _
                    "		LEFT JOIN hrassess_owrfield5_master ON hrassess_owrfield4_master.owrfield4unkid = hrassess_owrfield5_master.owrfield4unkid AND hrassess_owrfield5_master.isvoid = 0 " & _
                    "	WHERE hrassess_owrfield4_master.isvoid = 0 AND hrassess_owrfield1_master.ownerunkid = '" & iOwnerId & "' "
            If iPeriodId > 0 Then
                StrQ &= " AND hrassess_owrfield1_master.periodunkid = '" & iPeriodId & "' "
            End If
            StrQ &= "UNION " & _
                       "	SELECT " & _
                       "		  CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN @BRANCH " & _
                       "		  	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN @DEPARTMENT_GROUP " & _
                       "		  	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN @DEPARTMENT " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN @SECTION_GROUP " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN @SECTION " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN @UNIT_GROUP " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN @UNIT " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN @TEAM " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN @JOB_GROUP " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN @JOBS " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN @CLASS_GROUP " & _
                       "		 	   WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN @CLASSES " & _
                       "		  ELSE '' END AS OwnerCategory " & _
                       "		,CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 1 THEN ISNULL(hrstation_master.name,'') " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 2 THEN ISNULL(hrdepartment_group_master.name,'') " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 3 THEN ISNULL(hrdepartment_master.name,'')  " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 4 THEN ISNULL(hrsectiongroup_master.name,'') " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 5 THEN ISNULL(hrsection_master.name,'')   " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 6 THEN ISNULL(hrunitgroup_master.name,'')   " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 7 THEN ISNULL(hrunit_master.name,'')   " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 8 THEN ISNULL(hrteam_master.name,'')   " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 9 THEN ISNULL(hrjobgroup_master.name,'')  " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 10 THEN ISNULL(hrjob_master.job_name,'')  " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 13 THEN ISNULL(hrclassgroup_master.name,'')  " & _
                       "			  WHEN ISNULL(hrassess_owrfield1_master.ownerrefid,0) = 14 THEN ISNULL(hrclasses_master.name,'')  " & _
                       "         ELSE '' END AS Owner " & _
                       "		,ISNULL(hrassess_coyfield1_master.field_data,'') AS CoyField1 " & _
                       "		,ISNULL(hrassess_owrfield1_master.field_data,'') AS Field1 " & _
                       "		,ISNULL(hrassess_owrfield2_master.field_data,'') AS Field2 " & _
                       "		,ISNULL(hrassess_owrfield3_master.field_data,'') AS Field3 " & _
                       "		,ISNULL(hrassess_owrfield4_master.field_data,'') AS Field4 " & _
                       "		,ISNULL(hrassess_owrfield5_master.field_data,'') AS Field5 " & _
                       "		,ISNULL(hrassess_owrfield1_master.fieldunkid,0) AS Field1Id " & _
                       "		,ISNULL(hrassess_owrfield2_master.fieldunkid,0) AS Field2Id " & _
                       "		,ISNULL(hrassess_owrfield3_master.fieldunkid,0) AS Field3Id " & _
                       "		,ISNULL(hrassess_owrfield4_master.fieldunkid,0) AS Field4Id " & _
                       "		,ISNULL(hrassess_owrfield5_master.fieldunkid,0) AS Field5Id " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.startdate,112),'') AS f1_SDate " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield1_master.enddate,112),'') AS f1_EDate " & _
                       "		,ISNULL(hrassess_owrfield1_master.statusunkid,0) AS f1StatId " & _
                       "		,ISNULL(hrassess_owrfield1_master.ownerrefid,0) AS ownerrefid " & _
                       "		,ISNULL(hrassess_owrfield1_master.weight,0) AS f1weight " & _
                       "        ,ISNULL(hrassess_owrfield1_master.pct_completed, 0) AS f1pct " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.startdate,112),'') AS f2_SDate " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield2_master.enddate,112),'') AS f2_EDate " & _
                       "		,ISNULL(hrassess_owrfield2_master.statusunkid,0) AS f2StatId " & _
                       "		,ISNULL(hrassess_owrfield2_master.weight,0) AS f2weight " & _
                       "        ,ISNULL(hrassess_owrfield2_master.pct_completed, 0) AS f2pct " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.startdate,112),'') AS f3_SDate " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield3_master.enddate,112),'') AS f3_EDate " & _
                       "		,ISNULL(hrassess_owrfield3_master.statusunkid,0) AS f3StatId " & _
                       "		,ISNULL(hrassess_owrfield3_master.weight,0) AS f3weight " & _
                       "        ,ISNULL(hrassess_owrfield3_master.pct_completed, 0) AS f3pct " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.startdate,112),'') AS f4_SDate " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield4_master.enddate,112),'') AS f4_EDate " & _
                       "		,ISNULL(hrassess_owrfield4_master.statusunkid,0) AS f4StatId " & _
                       "		,ISNULL(hrassess_owrfield4_master.weight,0) AS f4weight " & _
                       "        ,ISNULL(hrassess_owrfield4_master.pct_completed, 0) AS f4pct " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.startdate,112),'') AS f5_SDate " & _
                       "		,ISNULL(CONVERT(CHAR(8),hrassess_owrfield5_master.enddate,112),'') AS f5_EDate " & _
                       "		,ISNULL(hrassess_owrfield5_master.statusunkid,0) AS f5StatId " & _
                       "		,ISNULL(hrassess_owrfield5_master.weight,0) AS f5weight " & _
                       "        ,ISNULL(hrassess_owrfield5_master.pct_completed, 0) AS f5pct " & _
                       "		,hrassess_owrfield1_master.owrfield1unkid " & _
                       "		,hrassess_owrfield2_master.owrfield2unkid " & _
                       "		,hrassess_owrfield3_master.owrfield3unkid " & _
                       "		,hrassess_owrfield4_master.owrfield4unkid " & _
                       "		,hrassess_owrfield5_master.owrfield5unkid " & _
                       "		,ISNULL(hrassess_owrfield1_master.yearunkid,0) AS yearunkid " & _
                       "		,ISNULL(hrassess_owrfield1_master.periodunkid,0) AS periodunkid " & _
                       "		,ISNULL(period_name,'') AS OPeriod " & _
                       "		,ISNULL(hrassess_owrfield1_master.coyfield1unkid,0) AS coyfield1unkid " & _
                       "		,hrassess_owrfield1_master.ownerunkid " & _
                       "	FROM hrassess_owrfield5_master " & _
                       "		LEFT JOIN hrassess_owrfield4_master ON hrassess_owrfield5_master.owrfield4unkid = hrassess_owrfield4_master.owrfield4unkid AND hrassess_owrfield4_master.isvoid = 0 " & _
                       "		LEFT JOIN hrassess_owrfield3_master ON hrassess_owrfield4_master.owrfield3unkid = hrassess_owrfield3_master.owrfield3unkid AND hrassess_owrfield3_master.isvoid = 0 " & _
                       "		LEFT JOIN hrassess_owrfield2_master ON hrassess_owrfield3_master.owrfield2unkid = hrassess_owrfield2_master.owrfield2unkid AND hrassess_owrfield2_master.isvoid = 0 " & _
                       "		LEFT JOIN hrassess_owrfield1_master ON hrassess_owrfield2_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                       "		LEFT JOIN hrassess_coyfield1_master ON hrassess_owrfield1_master.coyfield1unkid = hrassess_coyfield1_master.coyfield1unkid " & _
                       "		LEFT JOIN cfcommon_period_tran ON hrassess_owrfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                       "        LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_owrfield1_master.ownerunkid	" & _
                       "        LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                       "        LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_owrfield1_master.ownerunkid " & _
                       "	WHERE hrassess_owrfield5_master.isvoid = 0 AND hrassess_owrfield1_master.ownerunkid = '" & iOwnerId & "' "
            If iPeriodId > 0 Then
                StrQ &= " AND hrassess_owrfield1_master.periodunkid = '" & iPeriodId & "' "
            End If
            StrQ &= ") AS iLst " & _
                    "WHERE 1 = 1 ORDER BY Field1Id DESC, Field1 ASC "

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))
            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dsProgress As New DataSet
            Dim objProgress As New clsassess_owrupdate_tran
            dsProgress = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime.Date, iOwnerId, iPeriodId)
            objProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END


            Dim objInfoField As New clsassess_owrinfofield_tran
            Dim mdicFieldData As New Dictionary(Of Integer, String)
            For Each dRow As DataRow In dsList.Tables(iList).Rows
                mdicFieldData = New Dictionary(Of Integer, String)

                If dRow.Item("St_Date").ToString.Trim.Length > 0 Then
                    dRow.Item("St_Date") = eZeeDate.convertDate(dRow.Item("St_Date").ToString).ToShortDateString
                End If
                If dRow.Item("Ed_Date").ToString.Trim.Length > 0 Then
                    dRow.Item("Ed_Date") = eZeeDate.convertDate(dRow.Item("Ed_Date").ToString).ToShortDateString
                End If

                Dim objOwnerTran As New clsassess_owrowner_tran
                Select Case CInt(dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("owrfield1unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("owrfield1unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("owrfield2unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("owrfield2unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD3
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("owrfield3unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("owrfield3unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("owrfield4unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("owrfield4unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("owrfield5unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("owrfield5unkid"), dRow.Item("CFieldTypeId"))
                End Select

                mdtFinal.ImportRow(dRow)
                If dRow.Item("St_Date").ToString.Trim.Length > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("sdate") = eZeeDate.convertDate(CDate(dRow.Item("St_Date")))
                End If
                If dRow.Item("Ed_Date").ToString.Trim.Length > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("edate") = eZeeDate.convertDate(CDate(dRow.Item("Ed_Date")))
                End If

                Dim iLastStatusId As Integer = GetLastStatus(dRow.Item("periodunkid"), dRow.Item("ownerunkid"))

                If iLastStatusId > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("opstatusid") = iLastStatusId
                End If


                'S.SANDEEP [16 JUN 2015] -- START
                If dsProgress IsNot Nothing AndAlso dsProgress.Tables(0).Rows.Count > 0 Then
                    Dim intTableUnkid As Integer = 0
                    Select Case CInt(dRow.Item("CFieldTypeId"))
                        Case enWeight_Types.WEIGHT_FIELD1
                            intTableUnkid = dRow.Item("owrfield1unkid")
                        Case enWeight_Types.WEIGHT_FIELD2
                            intTableUnkid = dRow.Item("owrfield2unkid")
                        Case enWeight_Types.WEIGHT_FIELD3
                            intTableUnkid = dRow.Item("owrfield3unkid")
                        Case enWeight_Types.WEIGHT_FIELD4
                            intTableUnkid = dRow.Item("owrfield4unkid")
                        Case enWeight_Types.WEIGHT_FIELD5
                            intTableUnkid = dRow.Item("owrfield5unkid")
                    End Select

                    Dim pRow() As DataRow = dsProgress.Tables(0).Select("owrfieldtypeid = '" & dRow.Item("CFieldTypeId") & "' AND owrfieldunkid = '" & intTableUnkid & "'")
                    If pRow.Length > 0 Then
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("pct_complete") = pRow(0).Item("pct_completed")
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatusId") = pRow(0).Item("statusunkid")
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatus") = pRow(0).Item("dstatus")
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("vuRemark") = pRow(0).Item("remark")
					Else
						mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("pct_complete") = 0
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatusId") = 0
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatus") = ""
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("vuRemark") = ""
                    End If
                End If
                'S.SANDEEP [16 JUN 2015] -- END


                If mdicFieldData.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicFieldData.Keys
                        If mdtFinal.Columns.Contains("Field" & iKey.ToString) Then
                            mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("Field" & iKey.ToString) = mdicFieldData(iKey)
                            mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("Field" & iKey.ToString & "Id") = iKey
                        End If
                    Next
                End If
            Next
            objInfoField = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDisplayList", mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    Public Function Get_OwnerField1Unkid(ByVal iFieldData As String, ByVal iOwnerId As Integer, ByVal iPeriodId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                    "  owrfield1unkid " & _
                    "FROM hrassess_owrfield1_master " & _
                    "WHERE field_data = @field_data AND isvoid = 0 AND ownerunkid = @ownerunkid AND periodunkid = @periodunkid "

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, iFieldData)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwnerId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("owrfield1unkid")
            Else
                Return 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_OwnerField1Unkid", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'S.SANDEEP |08-DEC-2020| -- START
    'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
    Public Sub AssignGoalsToNewlyHiredEmployee(ByVal intEmployeeId As Integer, _
                                               ByVal iYearId As Integer, _
                                               ByVal iDataBaseName As String, _
                                               ByVal strUserAccessMode As String, _
                                               ByVal intCompanyId As Integer, _
                                               ByVal intUserId As Integer, _
                                               ByVal blnOnlyApproved As Boolean, _
                                               ByVal dtAppDate As Date)
        Dim objMst As New clsMasterData
        Dim intPeriodId As Integer = 0
        Dim objFMaster As New clsAssess_Field_Master(True)
        Dim objGrp As New clsGroup_Master
        Dim dtEffDate As Date = Now.Date
        Try
            If dtAppDate > dtEffDate Then dtEffDate = dtAppDate
            objGrp._Groupunkid = 1
            If objGrp._Groupname.ToUpper <> "NMB PLC" Then Exit Sub
            intPeriodId = objMst.getCurrentPeriodID(enModuleReference.Assessment, dtEffDate, iYearId, 1, , True)
            If intPeriodId <= 0 Then
                intPeriodId = objMst.getFirstPeriodID(enModuleReference.Assessment, iYearId, 1)
            End If
            If intPeriodId > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim strFollowEmployeeHierarchy As String = ""
                Dim objConfig As New clsConfigOptions
                strFollowEmployeeHierarchy = objConfig.GetKeyValue(intCompanyId, "FollowEmployeeHierarchy", Nothing)
                If strFollowEmployeeHierarchy Is Nothing Then strFollowEmployeeHierarchy = ""
                If strFollowEmployeeHierarchy.Trim.Length <= 0 Then strFollowEmployeeHierarchy = ConfigParameter._Object._FollowEmployeeHierarchy.ToString
                objConfig = Nothing

                Dim objMapping As New clsAssess_Field_Mapping
                Dim mintLinkedFieldId As Integer = 0
                mintLinkedFieldId = objMapping.Get_Map_FieldId(intPeriodId)
                objMapping = Nothing
                Dim dsFields As New DataSet

                dsFields = (New clsassess_empfield1_master).getComboList(intEmployeeId, intPeriodId, "List", False, False)
                If dsFields.Tables(0).Rows.Count > 0 Then Exit Sub

                dsFields = objFMaster.Get_Field_Mapping("List", , False)
                Dim iOwnerRefId As Integer = 0
                If dsFields.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = dsFields.Tables(0).Select("fieldunkid = '" & mintLinkedFieldId & "'")
                    If dtmp.Length > 0 Then
                        Select Case dtmp(0).Item("ExOrder")
                            Case 1
                                Dim objCoyField1 As New clsassess_coyfield1_master
                                iOwnerRefId = objCoyField1.GetOwnerRefId
                                objCoyField1 = Nothing
                            Case 2
                                Dim objCoyField2 As New clsassess_coyfield2_master
                                iOwnerRefId = objCoyField2.GetOwnerRefId
                                objCoyField2 = Nothing
                            Case 3
                                Dim objCoyField3 As New clsassess_coyfield3_master
                                iOwnerRefId = objCoyField3.GetOwnerRefId
                                objCoyField3 = Nothing
                            Case 4
                                Dim objCoyField4 As New clsassess_coyfield4_master
                                iOwnerRefId = objCoyField4.GetOwnerRefId
                                objCoyField4 = Nothing
                            Case 5
                                Dim objCoyField5 As New clsassess_coyfield5_master
                                iOwnerRefId = objCoyField5.GetOwnerRefId
                                objCoyField5 = Nothing
                        End Select
                    End If
                End If
                objEmp._Employeeunkid(dtEffDate) = intEmployeeId
                Dim intAllocationId As Integer = 0
                Select Case iOwnerRefId
                    Case enAllocation.BRANCH
                        intAllocationId = objEmp._Stationunkid
                    Case enAllocation.DEPARTMENT_GROUP
                        intAllocationId = objEmp._Deptgroupunkid
                    Case enAllocation.DEPARTMENT
                        intAllocationId = objEmp._Departmentunkid
                    Case enAllocation.SECTION_GROUP
                        intAllocationId = objEmp._Sectiongroupunkid
                    Case enAllocation.SECTION
                        intAllocationId = objEmp._Sectionunkid
                    Case enAllocation.UNIT_GROUP
                        intAllocationId = objEmp._Unitgroupunkid
                    Case enAllocation.UNIT
                        intAllocationId = objEmp._Unitunkid
                    Case enAllocation.TEAM
                        intAllocationId = objEmp._Teamunkid
                    Case enAllocation.JOB_GROUP
                        intAllocationId = objEmp._Jobgroupunkid
                    Case enAllocation.JOBS
                        intAllocationId = objEmp._Jobunkid
                    Case enAllocation.CLASS_GROUP
                        intAllocationId = objEmp._Classgroupunkid
                    Case enAllocation.CLASSES
                        intAllocationId = objEmp._Classunkid
                End Select
                Dim iLast_StatusId As Integer = 0
                iLast_StatusId = GetLastStatus(intPeriodId, intAllocationId)
                If iLast_StatusId = enObjective_Status.FINAL_COMMITTED Then
                    Transfer_Goals_ToEmployee(iYearId, iDataBaseName, intAllocationId, intPeriodId, strUserAccessMode, intCompanyId, intUserId, blnOnlyApproved, Nothing, strFollowEmployeeHierarchy, intEmployeeId)
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AssignGoalsToNewlyHiredEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |08-DEC-2020| -- END

    Public Function Transfer_Goals_ToEmployee(ByVal iYearId As Integer, ByVal iDataBaseName As String, _
                                              ByVal iOwnerId As Integer, ByVal iPeriodId As Integer, _
                                              ByVal strUserAccessMode As String, _
                                              ByVal intCompanyId As Integer, _
                                              ByVal intUserId As Integer, _
                                              ByVal blnOnlyApproved As Boolean, _
                                              Optional ByVal iWorker As System.ComponentModel.BackgroundWorker = Nothing, _
                                              Optional ByVal strFollowEmployeeHierarchy As String = "", _
                                              Optional ByVal intEmpId As Integer = -1) As Boolean 'S.SANDEEP |08-DEC-2020| -- START {intEmpId} -- END

        Dim mdTran As DataTable = Nothing
        Try
            mstrMessage = ""
            mdTran = GetDisplayList(iOwnerId, iPeriodId, "List")
            If iWorker IsNot Nothing Then iWorker.ReportProgress(0)
            Dim intCount As Integer = 0
            If mdTran.Rows.Count > 0 Then
                Dim dsEmp As New DataSet
                Dim objEmp = New clsEmployee_Master
                If strFollowEmployeeHierarchy.Trim.Length <= 0 Then strFollowEmployeeHierarchy = ConfigParameter._Object._FollowEmployeeHierarchy.ToString

                'S.SANDEEP |27-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                Dim iLinkedFld As Integer = 0 : Dim intGoalType As Integer = 0
                iLinkedFld = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)
                Dim objConfig As New clsConfigOptions : Dim strvalue As String = ""
                strvalue = objConfig.GetKeyValue(intCompanyId, "GoalTypeInclusion")
                objConfig = Nothing
                If strvalue Is Nothing Then strvalue = ""
                Integer.TryParse(strvalue, intGoalType)
                If intGoalType <= 0 Then intGoalType = enGoalType.GT_QUALITATIVE
                'S.SANDEEP |27-NOV-2020| -- END

                'S.SANDEEP |08-DEC-2020| -- START
                'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
                Dim strGrpCode As String = ""
                Dim objGroup As New clsGroup_Master
                objGroup._Groupunkid = 1
                strGrpCode = objGroup._Groupname
                objGroup = Nothing
                'S.SANDEEP |08-DEC-2020| -- END

                For Each iORow As DataRow In mdTran.Rows
                    If dsEmp.Tables.Count > 0 Then dsEmp.Tables(0).Rows.Clear()

                    If CBool(strFollowEmployeeHierarchy) = True Then
                        If iORow.Item("OwnerIds").ToString.Trim.Length > 0 Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'dsEmp = objEmp.Get_AllocationBasedEmployee(iORow.Item("ownerrefid"), iOwnerId, iPeriodId)
                            dsEmp = objEmp.Get_AllocationBasedEmployee(iORow.Item("ownerrefid"), iOwnerId, iPeriodId, iDataBaseName, strUserAccessMode, intCompanyId, intUserId, blnOnlyApproved, , , intEmpId) 'S.SANDEEP |08-DEC-2020| -- START {intEmpId} -- END
                            'Sohail (21 Aug 2015) -- End
                            dsEmp.Tables(0).Rows.Clear()
                            Dim iOwnr() As String = iORow.Item("OwnerIds").ToString.Split(",")
                            For iRow As Integer = 0 To iOwnr.Length - 1
                                Dim xRow As DataRow = dsEmp.Tables(0).NewRow
                                xRow.Item("employeecode") = ""
                                xRow.Item("employeename") = ""
                                xRow.Item("employeeunkid") = iOwnr(iRow)
                                dsEmp.Tables(0).Rows.Add(xRow)
                            Next
                        ElseIf iORow.Item("OwnerIds").ToString.Trim.Length <= 0 Then
                            'S.SANDEEP |08-DEC-2020| -- START
                            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
                            If strGrpCode = "NMB PLC" Then
                                dsEmp = objEmp.Get_AllocationBasedEmployee(iORow.Item("ownerrefid"), iOwnerId, iPeriodId, iDataBaseName, strUserAccessMode, intCompanyId, intUserId, blnOnlyApproved, , , intEmpId) 'S.SANDEEP |08-DEC-2020| -- START {intEmpId} -- END
                            End If
                            'S.SANDEEP |08-DEC-2020| -- END
                        End If
                    Else
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'dsEmp = objEmp.Get_AllocationBasedEmployee(iORow.Item("ownerrefid"), iOwnerId, iPeriodId)
                        dsEmp = objEmp.Get_AllocationBasedEmployee(iORow.Item("ownerrefid"), iOwnerId, iPeriodId, iDataBaseName, strUserAccessMode, intCompanyId, intUserId, blnOnlyApproved, , , intEmpId) 'S.SANDEEP |08-DEC-2020| -- START {intEmpId} -- END
                        'Sohail (21 Aug 2015) -- End
                    End If
                    If dsEmp.Tables.Count > 0 Then
                        If dsEmp.Tables(0).Rows.Count <= 0 Then
                            'mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, No Employee found for the selected period and owner.")
                            'Return False
                            Continue For
                        End If
                    Else
                        Continue For
                    End If
                    For Each iERow As DataRow In dsEmp.Tables(0).Rows
                        '************** SRART TRANSFERING FIELD DATA TO EMPLOYEE ************** START
                        Dim iEmpField1Unkid, iEmpField2Unkid, iEmpField3Unkid, iEmpField4Unkid, iEmpField5Unkid As Integer
                        iEmpField1Unkid = 0 : iEmpField2Unkid = 0 : iEmpField3Unkid = 0 : iEmpField4Unkid = 0 : iEmpField5Unkid = 0
                        Dim mDicFieldInfo As New Dictionary(Of Integer, String)
                        Dim objInfoField As New clsassess_owrinfofield_tran
                        _Owrfield1unkid = iORow.Item("owrfield1unkid")
                        If mstrField_Data.Trim.Length > 0 Then
                            Dim objEField1 As New clsassess_empfield1_master
                            iEmpField1Unkid = objEField1.Get_EmployeeField1Unkid(mstrField_Data, iERow.Item("employeeunkid"), iPeriodId)
                            If iEmpField1Unkid <= 0 Then
                                objEField1._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                                objEField1._Employeeunkid = iERow.Item("employeeunkid")
                                objEField1._Enddate = mdtEnddate
                                objEField1._Field_Data = mstrField_Data
                                objEField1._Fieldunkid = mintFieldunkid
                                objEField1._Isfinal = False
                                objEField1._Isvoid = False
                                objEField1._Owrfield1unkid = mintOwrfield1unkid
                                objEField1._Pct_Completed = mDecPct_Completed
                                objEField1._Periodunkid = iPeriodId
                                objEField1._Startdate = mdtStartdate
                                objEField1._Statusunkid = mintStatusunkid
                                objEField1._Userunkid = mintUserunkid
                                objEField1._Voiddatetime = mdtVoiddatetime
                                objEField1._Voidreason = mstrVoidreason
                                objEField1._Weight = mdblWeight
                                objEField1._Yearunkid = mintYearunkid
                                With objEField1
                                    ._FormName = mstrFormName
                                    ._Loginemployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(mintOwrfield1unkid, enWeight_Types.WEIGHT_FIELD1)
                                'S.SANDEEP |27-NOV-2020| -- START
                                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                If iLinkedFld = enWeight_Types.WEIGHT_FIELD1 Then
                                    objEField1._GoalTypeid = intGoalType
                                End If
                                'S.SANDEEP |27-NOV-2020| -- END                                
                                If objEField1.Insert(mDicFieldInfo) = True Then
                                    iEmpField1Unkid = objEField1._Empfield1unkid
                                End If
                            End If
                        End If

                        Dim objOField2 As New clsassess_owrfield2_master
                        objOField2._Owrfield2unkid = iORow.Item("owrfield2unkid")
                        If objOField2._Field_Data.Trim.Length > 0 Then
                            Dim objEField2 As New clsassess_empfield2_master
                            iEmpField2Unkid = objEField2.Get_EmployeeField2Unkid(objOField2._Field_Data, iERow.Item("employeeunkid"), iPeriodId)
                            If iEmpField2Unkid <= 0 Then
                                objEField2._Empfield1unkid = iEmpField1Unkid
                                objEField2._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                                objEField2._Employeeunkid = iERow.Item("employeeunkid")
                                objEField2._Enddate = objOField2._Enddate
                                objEField2._Field_Data = objOField2._Field_Data
                                objEField2._Fieldunkid = objOField2._Fieldunkid
                                objEField2._Isvoid = objOField2._Isvoid
                                objEField2._Pct_Completed = objOField2._Pct_Completed
                                objEField2._Periodunkid = iPeriodId
                                objEField2._Startdate = objOField2._Startdate
                                objEField2._Statusunkid = objOField2._Statusunkid
                                objEField2._Userunkid = objOField2._Userunkid
                                objEField2._Voiddatetime = objOField2._Voiddatetime
                                objEField2._Voidreason = objOField2._Voidreason
                                objEField2._Voiduserunkid = objOField2._Voiduserunkid
                                objEField2._Weight = objOField2._Weight
                                With objEField2
                                    ._FormName = mstrFormName
                                    ._Loginemployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objOField2._Owrfield2unkid, enWeight_Types.WEIGHT_FIELD2)
                                'S.SANDEEP |27-NOV-2020| -- START
                                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                If iLinkedFld = enWeight_Types.WEIGHT_FIELD2 Then
                                    objEField2._GoalTypeid = intGoalType
                                End If
                                'S.SANDEEP |27-NOV-2020| -- END                
                                If objEField2.Insert(mDicFieldInfo) = True Then
                                    iEmpField2Unkid = objEField2._Empfield2unkid
                                End If
                            End If
                        End If


                        Dim objOField3 As New clsassess_owrfield3_master
                        objOField3._Owrfield3unkid = iORow.Item("owrfield3unkid")
                        If objOField3._Field_Data.Trim.Length > 0 Then
                            Dim objEField3 As New clsassess_empfield3_master
                            iEmpField3Unkid = objEField3.Get_EmployeeField3Unkid(objOField3._Field_Data, iERow.Item("employeeunkid"), iPeriodId)
                            If iEmpField3Unkid <= 0 Then
                                objEField3._Empfield2unkid = iEmpField2Unkid
                                objEField3._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                                objEField3._Employeeunkid = iERow.Item("employeeunkid")
                                objEField3._Periodunkid = iPeriodId
                                objEField3._Enddate = objOField3._Enddate
                                objEField3._Field_Data = objOField3._Field_Data
                                objEField3._Fieldunkid = objOField3._Fieldunkid
                                objEField3._Isvoid = objOField3._Isvoid
                                objEField3._Pct_Completed = objOField3._Pct_Completed
                                objEField3._Startdate = objOField3._Startdate
                                objEField3._Statusunkid = objOField3._Statusunkid
                                objEField3._Userunkid = objOField3._Userunkid
                                objEField3._Voiddatetime = objOField3._Voiddatetime
                                objEField3._Voidreason = objOField3._Voidreason
                                objEField3._Voiduserunkid = objOField3._Voiduserunkid
                                objEField3._Weight = objOField3._Weight
                                With objEField3
                                    ._FormName = mstrFormName
                                    ._Loginemployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objOField3._Owrfield3unkid, enWeight_Types.WEIGHT_FIELD3)
                                'S.SANDEEP |27-NOV-2020| -- START
                                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                If iLinkedFld = enWeight_Types.WEIGHT_FIELD3 Then
                                    objEField3._GoalTypeid = intGoalType
                                End If
                                'S.SANDEEP |27-NOV-2020| -- END                
                                If objEField3.Insert(mDicFieldInfo) = True Then
                                    iEmpField3Unkid = objEField3._Empfield3unkid
                                End If
                            End If
                        End If


                        Dim objOField4 As New clsassess_owrfield4_master
                        objOField4._Owrfield4unkid = iORow.Item("owrfield4unkid")
                        If objOField4._Field_Data.Trim.Length > 0 Then
                            Dim objEField4 As New clsassess_empfield4_master
                            iEmpField4Unkid = objEField4.Get_EmployeeField4Unkid(objOField4._Field_Data, iERow.Item("employeeunkid"), iPeriodId)
                            If iEmpField4Unkid <= 0 Then
                                objEField4._Empfield3unkid = iEmpField3Unkid
                                objEField4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                                objEField4._Employeeunkid = iERow.Item("employeeunkid")
                                objEField4._Periodunkid = iPeriodId
                                objEField4._Enddate = objOField4._Enddate
                                objEField4._Field_Data = objOField4._Field_Data
                                objEField4._Fieldunkid = objOField4._Fieldunkid
                                objEField4._Isvoid = objOField4._Isvoid
                                objEField4._Pct_Completed = objOField4._Pct_Completed
                                objEField4._Startdate = objOField4._Startdate
                                objEField4._Statusunkid = objOField4._Statusunkid
                                objEField4._Userunkid = objOField4._Userunkid
                                objEField4._Voiddatetime = objOField4._Voiddatetime
                                objEField4._Voidreason = objOField4._Voidreason
                                objEField4._Voiduserunkid = objOField4._Voiduserunkid
                                objEField4._Weight = objOField4._Weight
                                With objEField4
                                    ._FormName = mstrFormName
                                    ._Loginemployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objOField4._Owrfield4unkid, enWeight_Types.WEIGHT_FIELD4)
                                'S.SANDEEP |27-NOV-2020| -- START
                                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                If iLinkedFld = enWeight_Types.WEIGHT_FIELD4 Then
                                    objEField4._GoalTypeid = intGoalType
                                End If
                                'S.SANDEEP |27-NOV-2020| -- END                
                                If objEField4.Insert(mDicFieldInfo) = True Then
                                    iEmpField4Unkid = objEField4._Empfield4unkid
                                End If
                            End If
                        End If


                        Dim objOField5 As New clsassess_owrfield5_master
                        objOField5._Owrfield5unkid = iORow.Item("owrfield5unkid")
                        If objOField5._Field_Data.Trim.Length > 0 Then
                            Dim objEField5 As New clsassess_empfield5_master
                            iEmpField5Unkid = objEField5.Get_EmployeeField5Unkid(objOField5._Field_Data, iERow.Item("employeeunkid"), iPeriodId)
                            If iEmpField5Unkid <= 0 Then
                                objEField5._Empfield4unkid = iEmpField4Unkid
                                objEField5._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD5
                                objEField5._Employeeunkid = iERow.Item("employeeunkid")
                                objEField5._Periodunkid = iPeriodId
                                objEField5._Enddate = objOField5._Enddate
                                objEField5._Field_Data = objOField5._Field_Data
                                objEField5._Fieldunkid = objOField5._Fieldunkid
                                objEField5._Isvoid = objOField5._Isvoid
                                objEField5._Pct_Completed = objOField5._Pct_Completed
                                objEField5._Startdate = objOField5._Startdate
                                objEField5._Statusunkid = objOField5._Statusunkid
                                objEField5._Userunkid = objOField5._Userunkid
                                objEField5._Voiddatetime = objOField5._Voiddatetime
                                objEField5._Voidreason = objOField5._Voidreason
                                objEField5._Voiduserunkid = objOField5._Voiduserunkid
                                objEField5._Weight = objOField5._Weight
                                With objEField5
                                    ._FormName = mstrFormName
                                    ._Loginemployeeunkid = mintLoginEmployeeunkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With
                                mDicFieldInfo = objInfoField.Get_Data(objOField5._Owrfield5unkid, enWeight_Types.WEIGHT_FIELD5)
                                'S.SANDEEP |27-NOV-2020| -- START
                                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                If iLinkedFld = enWeight_Types.WEIGHT_FIELD5 Then
                                    objEField5._GoalTypeid = intGoalType
                                End If
                                'S.SANDEEP |27-NOV-2020| -- END                
                                If objEField5.Insert(mDicFieldInfo) = True Then
                                    iEmpField5Unkid = objEField5._Empfield5unkid
                                End If
                            End If
                        End If
                        '************** SRART TRANSFERING FIELD DATA TO EMPLOYEE ************** END
                    Next
                    If iWorker IsNot Nothing Then
                        intCount = intCount + 1
                        iWorker.ReportProgress(intCount)
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Transfer_Goals_ToEmployee", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Update_Status(ByVal iStatusId As Integer, ByVal iComments As String, ByVal iUserId As Integer, ByVal iPeriodId As Integer, ByVal iOwnerId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ = "INSERT INTO hrassess_owrstatus_tran " & _
                   "( " & _
                       " periodunkid " & _
                       ",ownerunkid " & _
                       ",statustypeid " & _
                       ",status_date " & _
                       ",commtents " & _
                       ",userunkid " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                       " @periodunkid " & _
                       ",@ownerunkid " & _
                       ",@statustypeid " & _
                       ",@status_date " & _
                       ",@commtents " & _
                       ",@userunkid " & _
                   ") "

            objDataOperation.AddParameter("@statustypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iStatusId)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@commtents", SqlDbType.NVarChar, iComments.Length, iComments)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwnerId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_Status", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetLastStatus(ByVal iPeriodId As Integer, ByVal iOwnerId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim iStatusId As Integer = -1
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT TOP 1 statustypeid FROM hrassess_owrstatus_tran " & _
                   "WHERE periodunkid = '" & iPeriodId & "' AND ownerunkid = '" & iOwnerId & "' " & _
                   "ORDER BY status_date DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iStatusId = dsList.Tables(0).Rows(0).Item("statustypeid")
            Else
                iStatusId = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLastStatus", mstrModuleName)
        Finally
        End Try
        Return iStatusId
    End Function


    'S.SANDEEP [ 10 JAN 2015 ] -- START
    Public Function GetListForTansfer(ByVal xPeriodId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     owrfield1unkid " & _
                       "    ,coyfield1unkid " & _
                       "    ,ownerrefid " & _
                       "    ,ownerunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,yearunkid " & _
                       "    ,periodunkid " & _
                       "    ,weight " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,userunkid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "    ,pct_completed " & _
                       "FROM hrassess_owrfield1_master " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' ORDER BY owrfield1unkid "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetListForTansfer", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetOwrFieldUnkid(ByVal xFieldData As String, ByVal xPeriodId As Integer, ByVal xOwnerUnkid As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim xUnkid As Integer = -1
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT @xUnkid = owrfield1unkid FROM hrassess_owrfield1_master WHERE isvoid = 0 " & _
                       " AND periodunkid = @xPeriodId AND field_data = @xFieldData AND ownerunkid = @xOwnerUnkid "

                objDo.AddParameter("@xUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid, ParameterDirection.InputOutput)
                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFieldData", SqlDbType.NVarChar, xFieldData.Length, xFieldData)
                objDo.AddParameter("@xOwnerUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOwnerUnkid)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xUnkid = objDo.GetParameterValue("@xUnkid")

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOwrFieldUnkid", mstrModuleName)
        Finally
        End Try
        Return xUnkid
    End Function
    'S.SANDEEP [ 10 JAN 2015 ] -- END


    'Pinkal (19-Sep-2016) -- Start
    'Enhancement - Global Operation for all Organization.

    Public Function GetGlobalOwnerList(ByVal xPeriodID As Integer, ByVal xOperationType As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  " & _
                      " 0 AS ischeck " & _
                      ",  hrassess_owrfield1_master.ownerrefid " & _
                      ", hrassess_owrfield1_master.ownerunkid  " & _
                      ", CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 1 THEN @BRANCH " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 2 THEN @DEPARTMENT_GROUP " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 3 THEN @DEPARTMENT " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 4 THEN @SECTION_GROUP " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 5 THEN @SECTION " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 6 THEN @UNIT_GROUP " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 7 THEN @UNIT " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 8 THEN @TEAM " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 9 THEN @JOB_GROUP " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 10 THEN @JOBS " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 13 THEN @CLASS_GROUP " & _
                      "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 14 THEN @CLASSES " & _
                      " ELSE '' END AS OwnerCategory " & _
                     ", CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 1 THEN ISNULL(hrstation_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 2 THEN ISNULL(hrdepartment_group_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 3 THEN ISNULL(hrdepartment_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 4 THEN ISNULL(hrsectiongroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 5 THEN ISNULL(hrsection_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 6 THEN ISNULL(hrunitgroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 7 THEN ISNULL(hrunit_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 8 THEN ISNULL(hrteam_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 9 THEN ISNULL(hrjobgroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 10 THEN ISNULL(hrjob_master.job_name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 13 THEN ISNULL(hrclassgroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 14 THEN ISNULL(hrclasses_master.name, '') " & _
                     " ELSE '' END AS Owner  " & _
                     ", ISNULL(a.statustypeid, 3) AS statustypeid  " & _
                     ", SUM(ISNULL(hrassess_owrfield1_master.weight, 0.00)) AS Weight " & _
                     " FROM    hrassess_owrfield1_master " & _
                     " LEFT JOIN ( " & _
                     "                   SELECT  ROW_NUMBER() OVER (PARTITION BY hrassess_owrstatus_tran.ownerunkid ORDER BY hrassess_owrstatus_tran.status_date DESC ) AS Rno " & _
                     "                   , hrassess_owrstatus_tran.periodunkid  " & _
                     "                   , hrassess_owrstatus_tran.statustypeid  " & _
                     "                   , hrassess_owrstatus_tran.ownerunkid " & _
                     "                   FROM    hrassess_owrstatus_tran " & _
                     " ) AS A ON A.periodunkid = hrassess_owrfield1_master.periodunkid AND A.ownerunkid = hrassess_owrfield1_master.ownerunkid AND A.Rno = 1 " & _
                     " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrassess_owrfield1_master.ownerunkid " & _
                     " WHERE   hrassess_owrfield1_master.isvoid = 0 AND hrassess_owrfield1_master.periodunkid = @periodunkid " & _
                     " AND ISNULL(a.statustypeid,3) = @status " & _
                     " GROUP BY  " & _
                     "  CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 1 THEN @BRANCH " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 2 THEN @DEPARTMENT_GROUP " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 3 THEN @DEPARTMENT " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 4 THEN @SECTION_GROUP " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 5 THEN @SECTION " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 6 THEN @UNIT_GROUP " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 7 THEN @UNIT " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 8 THEN @TEAM " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 9 THEN @JOB_GROUP " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 10 THEN @JOBS " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 13 THEN @CLASS_GROUP " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 14 THEN @CLASSES " & _
                     " ELSE '' END  " & _
                     ", CASE WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 1 THEN ISNULL(hrstation_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 2 THEN ISNULL(hrdepartment_group_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 3 THEN ISNULL(hrdepartment_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 4 THEN ISNULL(hrsectiongroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 5 THEN ISNULL(hrsection_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 6 THEN ISNULL(hrunitgroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 7 THEN ISNULL(hrunit_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 8 THEN ISNULL(hrteam_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 9 THEN ISNULL(hrjobgroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 10 THEN ISNULL(hrjob_master.job_name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 13 THEN ISNULL(hrclassgroup_master.name, '') " & _
                     "            WHEN ISNULL(hrassess_owrfield1_master.ownerrefid, 0) = 14 THEN ISNULL(hrclasses_master.name, '') " & _
                     "  ELSE '' END   " & _
                     ", ISNULL(a.statustypeid, 3)  " & _
                     ", hrassess_owrfield1_master.ownerrefid " & _
                     ", hrassess_owrfield1_master.ownerunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodID)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, xOperationType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGlobalOwnerList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (19-Sep-2016) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data for the selected owner.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Owner Type")
            Language.setMessage(mstrModuleName, 4, "Owner")
            Language.setMessage(mstrModuleName, 5, "Company")
            Language.setMessage(mstrModuleName, 6, "Start Date")
            Language.setMessage(mstrModuleName, 7, "End Date")
            Language.setMessage(mstrModuleName, 8, "Status")
            Language.setMessage(mstrModuleName, 9, "Year")
            Language.setMessage(mstrModuleName, 10, "Period")
            Language.setMessage(mstrModuleName, 11, "Weight")
            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 422, "Job Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage("clsMasterData", 518, "Pending")
            Language.setMessage("clsMasterData", 519, "In progress")
            Language.setMessage("clsMasterData", 520, "Complete")
            Language.setMessage("clsMasterData", 521, "Closed")
            Language.setMessage("clsMasterData", 522, "On Track")
            Language.setMessage("clsMasterData", 523, "At Risk")
            Language.setMessage("clsMasterData", 524, "Not Applicable")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
