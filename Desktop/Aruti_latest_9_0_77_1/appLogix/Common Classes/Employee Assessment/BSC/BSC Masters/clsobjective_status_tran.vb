﻿'************************************************************************************************************************************
'Class Name : clsobjective_status_tran.vb
'Purpose    :
'Date       :11-Apr-2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsobjective_status_tran
    Private Const mstrModuleName = "clsobjective_status_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintObjectivestatustranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintStatustypeid As Integer
    Private mintAssessormasterunkid As Integer
    Private mintAssessoremployeeunkid As Integer
    Private mdtStatus_Date As Date
    Private mstrCommtents As String = String.Empty
    Private mblnIsunlock As Boolean = False
    Private mintUserunkid As Integer = 0

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set objectivestatustranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Objectivestatustranunkid() As Integer
        Get
            Return mintObjectivestatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintObjectivestatustranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statustypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statustypeid() As Integer
        Get
            Return mintStatustypeid
        End Get
        Set(ByVal value As Integer)
            mintStatustypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessoremployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assessoremployeeunkid() As Integer
        Get
            Return mintAssessoremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessoremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Status_Date() As Date
        Get
            Return mdtStatus_Date
        End Get
        Set(ByVal value As Date)
            mdtStatus_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set commtents
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Commtents() As String
        Get
            Return mstrCommtents
        End Get
        Set(ByVal value As String)
            mstrCommtents = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isunlock
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isunlock() As Boolean
        Get
            Return mblnIsunlock
        End Get
        Set(ByVal value As Boolean)
            mblnIsunlock = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  objectivestatustranunkid " & _
                   ", employeeunkid " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", commtents " & _
                   ", isunlock " & _
                   ", userunkid " & _
                   "FROM hrobjective_status_tran " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND periodunkid = @periodunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintobjectivestatustranunkid = CInt(dtRow.Item("objectivestatustranunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintstatustypeid = CInt(dtRow.Item("statustypeid"))
                mintassessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                mintassessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
                mdtstatus_date = dtRow.Item("status_date")
                mstrCommtents = dtRow.Item("commtents").ToString
                mblnIsunlock = CBool(dtRow.Item("isunlock"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrobjective_status_tran) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation, Optional ByVal iUserUnkid As Integer = 0, Optional ByVal iExcludeChecking As Boolean = False) As Boolean 'S.SANDEEP [ 13 JUL 2014 ] -- Start {Optional ByVal iExcludeChecking As Boolean = False} -- End
        'S.SANDEEP [ 13 JUL 2014 ] -- Start
        'If isExist(mintEmployeeunkid, mintYearunkid, mintPeriodunkid, mintStatustypeid, objDataOperation) Then
        '    Return True
        'End If
        If iExcludeChecking = False Then
        If isExist(mintEmployeeunkid, mintYearunkid, mintPeriodunkid, mintStatustypeid, objDataOperation) Then
            Return True
        End If
        End If
        'S.SANDEEP [ 13 JUL 2014 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@statustypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypeid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatus_Date.ToString)
            objDataOperation.AddParameter("@commtents", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrCommtents.ToString)
            objDataOperation.AddParameter("@isunlock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlock.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "INSERT INTO hrobjective_status_tran ( " & _
                      "  employeeunkid " & _
                      ", yearunkid " & _
                      ", periodunkid " & _
                      ", statustypeid " & _
                      ", assessormasterunkid " & _
                      ", assessoremployeeunkid " & _
                      ", status_date " & _
                      ", commtents" & _
                      ", isunlock " & _
                      ", userunkid" & _
                   ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @yearunkid " & _
                      ", @periodunkid " & _
                      ", @statustypeid " & _
                      ", @assessormasterunkid " & _
                      ", @assessoremployeeunkid " & _
                      ", @status_date " & _
                      ", @commtents" & _
                      ", @isunlock " & _
                      ", @userunkid" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintObjectivestatustranunkid = dsList.Tables(0).Rows(0).Item(0)

            If mintStatustypeid = enObjective_Status.FINAL_SAVE Then
                strQ = "UPDATE hrobjective_master SET isfinal = 1 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            ElseIf mintStatustypeid = enObjective_Status.OPEN_CHANGES Then
                strQ = "UPDATE hrobjective_master SET isfinal = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrobjective_status_tran", "objectivestatustranunkid", mintObjectivestatustranunkid, , iUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iEmpUnkid As Integer, _
                            ByVal iYearUnkid As Integer, _
                            ByVal iPeriodUnkid As Integer, _
                            ByVal iStatusId As Integer, _
                            ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Try
            'S.SANDEEP [ 13 JUL 2014 ] -- Start         
            'strQ = "SELECT TOP 1 " & _
            '       "  objectivestatustranunkid " & _
            '       ", employeeunkid " & _
            '       ", yearunkid " & _
            '       ", periodunkid " & _
            '       ", statustypeid " & _
            '       ", assessormasterunkid " & _
            '       ", assessoremployeeunkid " & _
            '       ", status_date " & _
            '       ", commtents " & _
            '       "FROM hrobjective_status_tran " & _
            '       "WHERE employeeunkid = @employeeunkid " & _
            '       " AND yearunkid = @yearunkid " & _
            '       " AND periodunkid = @periodunkid " & _
            '       " ORDER BY objectivestatustranunkid DESC "
            strQ = "SELECT TOP 1 " & _
                   "  objectivestatustranunkid " & _
                   ", employeeunkid " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", commtents " & _
                   "FROM hrobjective_status_tran " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND yearunkid = @yearunkid " & _
                   " AND periodunkid = @periodunkid " & _
                   " ORDER BY objectivestatustranunkid DESC "
            'S.SANDEEP [ 13 JUL 2014 ] -- End 

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("statustypeid") = iStatusId Then
                    blnFlag = True
                Else
                    blnFlag = False
                End If
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Overloaded Method </purpose>
    Public Function Insert(ByVal iUserUnkid As Integer, Optional ByVal iExcludeChecking As Boolean = False) As Boolean 'S.SANDEEP [ 13 JUL 2014 ] -- Start {Optional ByVal iExcludeChecking As Boolean = False} -- End 
        Dim objDataOperation As New clsDataOperation
        Try
            mstrMessage = ""
            objDataOperation.BindTransaction()

            If Insert(objDataOperation, iUserUnkid, iExcludeChecking) = False Then 'S.SANDEEP [ 13 JUL 2014 ] -- Start {Optional ByVal iExcludeChecking As Boolean = False} -- End 
                mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isStatusExists(ByVal iEmpUnkid As Integer, _
                                   ByVal iYearUnkid As Integer, _
                                   ByVal iPeriodUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim blnFlag As Boolean = False
        Try
            strQ = "SELECT TOP 1 " & _
                   "  objectivestatustranunkid " & _
                   ", employeeunkid " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", commtents " & _
                   "FROM hrobjective_status_tran " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND periodunkid = @periodunkid " & _
                   "ORDER BY objectivestatustranunkid DESC "


            'S.SANDEEP [ 17 JUL 2014 ] -- START
            '" AND yearunkid = @yearunkid " & _ -- REMOVED
            'S.SANDEEP [ 17 JUL 2014 ] -- END

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Select Case CInt(dsList.Tables(0).Rows(0).Item("statustypeid"))
                    Case enObjective_Status.SUBMIT_APPROVAL, enObjective_Status.FINAL_SAVE
                        blnFlag = True
                End Select
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isStatusExists", mstrModuleName)
            Return True
        End Try
    End Function

End Class
