﻿'************************************************************************************************************************************
'Class Name : clstarget_master.vb
'Purpose    :
'Date       :04/01/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clstarget_master
    Private Shared ReadOnly mstrModuleName As String = "clstarget_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTargetunkid As Integer
    Private mintObjectiveunkid As Integer
    Private mintKpiunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIsactive As Boolean = True


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mintEmployeeunkid As Integer
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecWeight As Decimal = 0
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set targetunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Targetunkid() As Integer
        Get
            Return mintTargetunkid
        End Get
        Set(ByVal value As Integer)
            mintTargetunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set objectiveunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Objectiveunkid() As Integer
        Get
            Return mintObjectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintObjectiveunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set kpiunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Kpiunkid() As Integer
        Get
            Return mintKpiunkid
        End Get
        Set(ByVal value As Integer)
            mintKpiunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Employeeunkid()
        Set(ByVal value)
            mintEmployeeunkid = value
        End Set
    End Property
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mdecWeight
        End Get
        Set(ByVal value As Decimal)
            mdecWeight = value
        End Set
    End Property
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  targetunkid " & _
              ", objectiveunkid " & _
              ", kpiunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive " & _
              ", ISNULL(weight,0) AS weight " & _
             "FROM hrtarget_master " & _
             "WHERE targetunkid = @targetunkid "

            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            objDataOperation.AddParameter("@targetunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintTargetUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                minttargetunkid = CInt(dtRow.Item("targetunkid"))
                mintobjectiveunkid = CInt(dtRow.Item("objectiveunkid"))
                mintkpiunkid = CInt(dtRow.Item("kpiunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrdescription = dtRow.Item("description").ToString
                mstrname1 = dtRow.Item("name1").ToString
                mstrname2 = dtRow.Item("name2").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mdecWeight = dtRow.Item("weight")
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal strIncludeInactiveEmployee As String = "", _
                            Optional ByVal strEmployeeAsOnDate As String = "", _
                            Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '          "  hrtarget_master.targetunkid " & _
            '          ", hrtarget_master.objectiveunkid " & _
            '          ", hrtarget_master.kpiunkid " & _
            '          ", hrtarget_master.code " & _
            '          ", hrtarget_master.name " & _
            '          ", hrtarget_master.description " & _
            '          ", hrtarget_master.name1 " & _
            '          ", hrtarget_master.name2 " & _
            '          ", hrtarget_master.isactive " & _
            '          ", ISNULL(hrobjective_master.name,'') as objective " & _
            '          ", ISNULL(hrkpi_master.name,'') as kpi "

            'If ConfigParameter._Object._IsBSC_ByEmployee Then

            '    strQ &= ", CASE WHEN emp.employeeunkid > 0 THEN " & _
            '                "    ISNULL(emp.employeeunkid, 0) " & _
            '                " Else " & _
            '                "     ISNULL(hremployee_master.employeeunkid, 0) " & _
            '                " END  employeeunkid " & _
            '                ", CASE WHEN emp.employeeunkid > 0 THEN " & _
            '                "     ISNULL(emp.employeecode, '') " & _
            '                " Else " & _
            '                "    ISNULL(hremployee_master.employeecode, '')  " & _
            '                " END  employeecode " & _
            '                ", CASE WHEN emp.employeeunkid > 0 THEN  " & _
            '                "     ISNULL(emp.firstname, '') + ' ' + ISNULL(emp.surname, '') " & _
            '                " Else " & _
            '                "     ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '')  " & _
            '                " END employee "
            'End If

            'strQ &= " FROM hrtarget_master " & _
            '            " LEFT JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid  AND hrobjective_master.isactive = 1 " & _
            '            " LEFT JOIN hrkpi_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid AND hrkpi_master.isactive = 1 "

            'If ConfigParameter._Object._IsBSC_ByEmployee Then
            '    strQ &= "  LEFT JOIN hrobjective_master om  ON om.objectiveunkid = hrkpi_master.objectiveunkid AND om.isactive = 1 " & _
            '                "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrobjective_master.employeeunkid " & _
            '                "  LEFT JOIN hremployee_master emp ON emp.employeeunkid = om.employeeunkid "
            'End If

            strQ = "SELECT " & _
                      "  hrtarget_master.targetunkid " & _
                      ", hrtarget_master.objectiveunkid " & _
                      ", hrtarget_master.kpiunkid " & _
                      ", hrtarget_master.code " & _
                      ", hrtarget_master.name " & _
                      ", hrtarget_master.description " & _
                      ", hrtarget_master.name1 " & _
                      ", hrtarget_master.name2 " & _
                      ", hrtarget_master.isactive " & _
                      ", ISNULL(hrobjective_master.name,'') as objective " & _
                    " ,ISNULL(hrobjective_master.employeeunkid,0) employeeunkid " & _
                    " ,CASE WHEN hrobjective_master.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ELSE '' END AS employee " & _
                    " ,ISNULL(hremployee_master.employeecode,'') employeecode " & _
                    " ,hrobjective_master.isfinal " & _
                   " ,ISNULL(hrkpi_master.name,'') AS KPI " & _
                  ", hrobjective_master.perspectiveunkid " & _
                  ", hrobjective_master.code as ocode " & _
                  ", ISNULL(hrkpi_master.code,'') as kcode " & _
                        ", ISNULL(hrtarget_master.weight,0) AS target_weight " & _
                        ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                        ", ISNULL(cfcommon_period_tran.statusid,2) AS statusid " & _
                         ", ISNULL(STypId,0) AS STypId " & _
                         ", hrobjective_master.periodunkid " & _
                    "FROM hrtarget_master " & _
                    " JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                    " LEFT JOIN cfcommon_period_tran ON hrobjective_master.periodunkid = cfcommon_period_tran.periodunkid  " & _
                    " LEFT JOIN hremployee_master ON hrobjective_master.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= " LEFT JOIN hrkpi_master ON hrtarget_master.kpiunkid = hrkpi_master.kpiunkid "
            'S.SANDEEP [ 12 JUNE 2012 ] -- END



            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            strQ &= "LEFT JOIN " & _
                    "( " & _
                         "SELECT EId,YId,PId,STypId FROM " & _
                         "( " & _
                              "SELECT " & _
                                   " employeeunkid AS EId " & _
                                   ",yearunkid AS YId " & _
                                   ",periodunkid AS PId " & _
                                   ",statustypeid AS STypId " & _
                                   ",CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid,yearunkid,periodunkid ORDER BY objectivestatustranunkid DESC) AS RNo " & _
                              "FROM hrobjective_status_tran " & _
                         ")AS A WHERE RNo = 1 " & _
                    ") AS Obj_Status ON Obj_Status.EId = hrobjective_master.employeeunkid AND Obj_Status.PId = hrobjective_master.periodunkid AND Obj_Status.YId = hrobjective_master.yearunkid "

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            End If

            If strUserAccessLevelFilterString = "" Then
                'Sohail (08 May 2015) -- Start
                'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                strQ &= UserAccessLevel._AccessLevelFilterString
                'strQ &= NewAccessLevelFilterString()
                'Sohail (08 May 2015) -- End
            Else
                strQ &= strUserAccessLevelFilterString
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            If blnOnlyActive Then
                strQ &= " WHERE hrtarget_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtarget_master) </purpose>
    Public Function Insert(Optional ByVal intUserUnkid As Integer = 0, _
                           Optional ByVal IsBSC_ByEmp As String = "") As Boolean 'S.SANDEEP [ 12 JUNE 2012 ] -- START -- END
        'Public Function Insert(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Insert() As Boolean

        'S.SANDEEP [ 12 JUNE 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
        '    Return False
        'End If

        'If isExist(, mstrName) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
        '    Return False
        'End If
        If isExist(mstrCode, , , mintObjectiveunkid, mintKpiunkid, IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, , mintObjectiveunkid, mintKpiunkid, IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If
        'S.SANDEEP [ 12 JUNE 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintobjectiveunkid.ToString)
            objDataOperation.AddParameter("@kpiunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintkpiunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight)
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            StrQ = "INSERT INTO hrtarget_master ( " & _
              "  objectiveunkid " & _
              ", kpiunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive" & _
              ", weight " & _
            ") VALUES (" & _
              "  @objectiveunkid " & _
              ", @kpiunkid " & _
              ", @code " & _
              ", @name " & _
              ", @description " & _
              ", @name1 " & _
              ", @name2 " & _
              ", @isactive" & _
              ", @weight " & _
            "); SELECT @@identity"
            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTargetunkid = dsList.Tables(0).Rows(0).Item(0)


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrtarget_master", "targetunkid", mintTargetunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrtarget_master", "targetunkid", mintTargetunkid, , intUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtarget_master) </purpose>
    Public Function Update(Optional ByVal intUserUnkid As Integer = 0, _
                           Optional ByVal IsBSC_ByEmp As String = "") As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Update(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Update() As Boolean 

        'S.SANDEEP [ 12 JUNE 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , mintTargetunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
        '    Return False
        'End If

        'If isExist(, mstrName, mintTargetunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
        '    Return False
        'End If

        If isExist(mstrCode, , mintTargetunkid, mintObjectiveunkid, mintKpiunkid, IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintTargetunkid, mintObjectiveunkid, mintKpiunkid, IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If
        'S.SANDEEP [ 12 JUNE 2012 ] -- END


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@targetunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttargetunkid.ToString)
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintobjectiveunkid.ToString)
            objDataOperation.AddParameter("@kpiunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintkpiunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight)
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            strQ = "UPDATE hrtarget_master SET " & _
              "  objectiveunkid = @objectiveunkid" & _
              ", kpiunkid = @kpiunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", description = @description" & _
              ", name1 = @name1" & _
              ", name2 = @name2" & _
              ", isactive = @isactive " & _
              ", weight = @weight " & _
            "WHERE targetunkid = @targetunkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrtarget_master", "targetunkid", mintTargetunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrtarget_master", "targetunkid", mintTargetunkid, , intUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtarget_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrtarget_master " & _
                      " SET isactive = 0 " & _
                      "WHERE targetunkid = @targetunkid "

            objDataOperation.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrtarget_master", "targetunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrtarget_master", "targetunkid", intUnkid, , intUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='targetunkid' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName") = "hrtarget_master" Then Continue For

                objDataOperation.ClearParameters()

                If dtRow.Item("TableName") = "hrbsc_analysis_tran" Then

                    strQ = "SELECT targetunkid FROM " & dtRow.Item("TableName").ToString & " WHERE targetunkid = @targetunkid  and isvoid = 0"
            objDataOperation.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                Else

                    strQ = "SELECT targetunkid FROM " & dtRow.Item("TableName").ToString & " WHERE targetunkid = @targetunkid  and isactive = 1 "
            objDataOperation.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", _
                            Optional ByVal strName As String = "", _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal intObjectiveId As Integer = -1, _
                            Optional ByVal intKPIUnkid As Integer = -1, _
                            Optional ByVal IsBSC_ByEmp As String = "") As Boolean 'S.SANDEEP [ 12 JUNE 2012 ] -- START -- END
        'Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrtarget_master.targetunkid " & _
              ", hrtarget_master.objectiveunkid " & _
              ", hrtarget_master.kpiunkid " & _
              ", hrtarget_master.code " & _
              ", hrtarget_master.name " & _
              ", hrtarget_master.description " & _
              ", hrtarget_master.name1 " & _
              ", hrtarget_master.name2 " & _
              ", hrtarget_master.isactive " & _
             "FROM hrtarget_master "


            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsBSC_ByEmployee Then
            '    strQ &= " LEFT JOIN hrobjective_master on hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid AND hrobjective_master.isactive = 1 " & _
            '                " LEFT JOIN hrkpi_master on hrkpi_master.objectiveunkid = hrtarget_master.objectiveunkid AND hrkpi_master.isactive = 1 " & _
            '                " JOIN hremployee_master on hremployee_master.employeeunkid = hrobjective_master.employeeunkid "
            '    If mintEmployeeunkid > 0 Then
            '        strQ &= " AND hrobjective_master.employeeunkid = @employeeunkid"
            '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            '    End If
            'End If
            If IsBSC_ByEmp.Trim.Length <= 0 Then
                IsBSC_ByEmp = ConfigParameter._Object._IsBSC_ByEmployee.ToString
            End If
            If CBool(IsBSC_ByEmp) = True Then
                strQ &= " LEFT JOIN hrobjective_master on hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid AND hrobjective_master.isactive = 1 " & _
                            " LEFT JOIN hrkpi_master on hrkpi_master.objectiveunkid = hrtarget_master.objectiveunkid AND hrkpi_master.isactive = 1 " & _
                            " JOIN hremployee_master on hremployee_master.employeeunkid = hrobjective_master.employeeunkid "
                If mintEmployeeunkid > 0 Then
                    strQ &= " AND hrobjective_master.employeeunkid = @employeeunkid"
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                End If
            End If
            'S.SANDEEP [ 12 JUNE 2012 ] -- END

            strQ &= " WHERE hrtarget_master.isactive = 1 "

            If strCode.Length > 0 Then
                strQ &= "AND hrtarget_master.code = @code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND hrtarget_master.name = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND hrtarget_master.targetunkid <> @targetunkid"
            End If


            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intObjectiveId > 0 Then
                strQ &= " AND hrtarget_master.objectiveunkid = @intObjectiveId "
                objDataOperation.AddParameter("@intObjectiveId", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveId)
            End If

            If intKPIUnkid > 0 Then
                strQ &= " AND hrtarget_master.kpiunkid = @intKPIUnkid "
                objDataOperation.AddParameter("@intKPIUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intKPIUnkid)
            End If
            'S.SANDEEP [ 12 JUNE 2012 ] -- END


            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intObjectiveunkid As Integer = -1, Optional ByVal intkpiunkid As Integer = -1, Optional ByVal intEmployeeunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As id , @ItemName As  name , '' as code UNION "
            End If
            strQ &= "SELECT targetunkid as id,hrtarget_master.name as name,hrtarget_master.code as code FROM hrtarget_master "

            If ConfigParameter._Object._IsBSC_ByEmployee = False Then
                strQ &= " LEFT JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid "

            Else

                strQ &= " LEFT JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                        " LEFT JOIN hrkpi_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid " & _
                        " LEFT JOIN hrobjective_master OM ON hrkpi_master.objectiveunkid = OM.objectiveunkid "

            End If

            If intObjectiveunkid > 0 Then
                If ConfigParameter._Object._IsBSC_ByEmployee Then
                    strQ &= " JOIN hremployee_master on hremployee_master.employeeunkid = hrobjective_master.employeeunkid "
                End If

                strQ &= " AND hrobjective_master.objectiveunkid = '" & intObjectiveunkid & "'"
            End If


            strQ &= " WHERE hrtarget_master.isactive =1 "

            If intkpiunkid > 0 Then
                strQ &= " AND hrtarget_master.kpiunkid ='" & intkpiunkid & "'"
            End If

            If intEmployeeunkid > 0 Then
                strQ &= "AND (hrobjective_master.employeeunkid = '" & intEmployeeunkid & "' OR OM.employeeunkid = '" & intEmployeeunkid & "')"
            End If

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetTargetUnkid(ByVal mstrTgtCode As String, _
                                   Optional ByVal mstrTarget As String = "", _
                                   Optional ByVal iObjectiveId As Integer = 0, _
                                   Optional ByVal iKpiId As Integer = 0) As Integer 'S.SANDEEP [ 20 JULY 2013 (mstrTgtCode,iObjectiveId,iKpiId) ] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = " SELECT " & _
            '       "     targetunkid " & _
            '       " FROM hrtarget_master " & _
            '       " WHERE name = @name "

            strQ = "SELECT targetunkid " & _
                      " FROM hrtarget_master " & _
                   "JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                   "LEFT JOIN hrkpi_master ON hrtarget_master.kpiunkid = hrkpi_master.kpiunkid " & _
                   "WHERE hrtarget_master.isactive = 1 "
            'S.SANDEEP [ 20 JULY 2013 ] -- END

            If iObjectiveId > 0 Then
                strQ &= " AND hrtarget_master.objectiveunkid = '" & iObjectiveId & "' "
            End If

            If iKpiId > 0 Then
                strQ &= " AND hrtarget_master.kpiunkid = '" & iKpiId & "' "
            End If

            If mstrTgtCode.Trim.Length > 0 Then
                If mstrTgtCode.Contains("'") Then
                    mstrTgtCode = mstrTgtCode.Replace("'", "''")
                End If
                strQ &= " AND hrtarget_master.code = '" & mstrTgtCode & "' "
            End If

            If mstrTarget.Trim.Length > 0 Then
                If mstrTarget.Contains("'") Then
                    mstrTarget = mstrTarget.Replace("'", "''")
                End If
                strQ &= " AND hrtarget_master.name = '" & mstrTarget & "' "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("targetunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTargetUnkid; Module Name: " & mstrModuleName)
        End Try
        Return -1

    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
			Language.setMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class