﻿'************************************************************************************************************************************
'Class Name : clsObjective_tran
'Purpose    :
'Date       :25/01/2012
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsObjective_tran

    Private Shared ReadOnly mstrModuleName As String = "clsObjective_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_group_tran) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation, ByVal intObjectiveId As Integer, ByVal strAllocationIds As String, ByVal intReferenceUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintObjectiveTranUnkId As Integer = -1
        Dim dsAllocated As New DataSet
        Dim blnFlagIsInserted As Boolean = False
        Try
            dsAllocated = GetAllocations(objDataOperation, intObjectiveId)

            If dsAllocated.Tables(0).Rows.Count > 0 Then
                Dim dtTemp() As DataRow = dsAllocated.Tables(0).Select("allocationunkid NOT IN(" & strAllocationIds & ")")
                If dtTemp.Length > 0 Then
                    For i As Integer = 0 To dtTemp.Length - 1
                        If isAllocationUsed(objDataOperation, dtTemp(i)("allocationunkid"), intReferenceUnkid) = False Then
                            Call Delete(objDataOperation, dtTemp(i)("objectivetranunkid"))
                            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrobjective_master", "objectiveunkid", intObjectiveId, "hrobjective_tran", "objectivetranunkid", dtTemp(i)("objectivetranunkid"), 2, 3) = False Then
                            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            '    Throw exForce
                            'End If
                        Else
                            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot unassign some allocation(s). Reason : They are already linked with some transaction(s).")
                        End If
                    Next
                End If
            End If

            For Each StrId As String In strAllocationIds.Split(",")

                If isExist(objDataOperation, intObjectiveId, StrId) = True Then Continue For


                objDataOperation.ClearParameters() : mintObjectiveTranUnkId = -1

                objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveId.ToString)
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, StrId)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)

                strQ = "INSERT INTO hrobjective_tran ( " & _
                        "  objectiveunkid " & _
                        ", allocationunkid " & _
                        ", isactive" & _
                        ") VALUES (" & _
                        "  @objectiveunkid " & _
                        ", @allocationunkid " & _
                        ", @isactive" & _
                        "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintObjectiveTranUnkId = dsList.Tables(0).Rows(0).Item(0)

                If blnFlagIsInserted = False Then blnFlagIsInserted = True

                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrobjective_master", "objectiveunkid", intObjectiveId, "hrobjective_tran", "objectivetranunkid", mintObjectiveTranUnkId, 2, 1) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

            Next

            If blnFlagIsInserted = False Then
                'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrobjective_master", intObjectiveId, "objectiveunkid", 2) Then
                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrobjective_master", "objectiveunkid", intObjectiveId, "hrobjective_tran", "objectivetranunkid", -1, 2, 0) = False Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrObjective_tran) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intObjectiveTranID As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "UPDATE hrobjective_tran " & _
                   " SET isactive = 0 " & _
                   "WHERE objectivetranunkid = @objectivetranunkid "

            objDataOperation.AddParameter("@objectivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveTranID)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose></purpose>
    Private Function isExist(ByVal objDataOperation As clsDataOperation, ByVal intObjectiveId As Integer, ByVal intAllocationId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        Try
            strQ = " SELECT * FROM hrobjective_tran WHERE objectiveunkid = '" & intObjectiveId & "' AND allocationunkid = '" & intAllocationId & "' AND isactive = 1 "


            If objDataOperation.RecordCount(strQ) <= 0 Then
                blnFlag = False
            Else
                blnFlag = True
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose></purpose>
    Public Function GetAllocations(ByVal objDataOperation As clsDataOperation, ByVal intObjectiveId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ &= "SELECT " & _
                    " objectivetranunkid " & _
                    ",objectiveunkid " & _
                    ",allocationunkid " & _
                    ",isactive " & _
                    "FROM hrobjective_tran " & _
                    "WHERE objectiveunkid = '" & intObjectiveId & "' AND isactive = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocations; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose></purpose>
    Public Function isAllocationUsed(ByVal objDataOperation As clsDataOperation, ByVal intAllocationId As Integer, ByVal intReferenceId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        Try
            strQ = "SELECT " & _
                   "	hrbsc_analysis_master.analysisunkid " & _
                   " FROM hrbsc_analysis_master " & _
                   "    JOIN hrbsc_analysis_tran on hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid  " & _
                   "	JOIN hrobjective_master ON hrbsc_analysis_tran.objectiveunkid = hrobjective_master.objectiveunkid " & _
                   "   JOIN hrobjective_tran ON hrobjective_tran.objectiveunkid = hrobjective_master.objectiveunkid " & _
                   "	LEFT JOIN hremployee_master AS Assess ON hrbsc_analysis_master.assessedemployeeunkid = Assess.employeeunkid " & _
                   "	LEFT JOIN hremployee_master AS Self ON hrbsc_analysis_master.selfemployeeunkid = Self.employeeunkid AND hrbsc_analysis_master.assessmodeid =  " & enAssessmentMode.SELF_ASSESSMENT & _
                   "WHERE hrobjective_tran.allocationunkid = '" & intAllocationId & "' " & _
                   "AND hrbsc_analysis_master.isvoid = 0 "
            Select Case intReferenceId
                Case enAllocation.BRANCH
                    strQ &= " AND (Self.stationunkid = '" & intAllocationId & "' OR Assess.stationunkid = '" & intAllocationId & "') "
                Case enAllocation.DEPARTMENT_GROUP
                    strQ &= " AND (Self.deptgroupunkid = '" & intAllocationId & "' OR Assess.deptgroupunkid = '" & intAllocationId & "') "
                Case enAllocation.DEPARTMENT
                    strQ &= " AND (Self.departmentunkid = '" & intAllocationId & "' OR Assess.departmentunkid = '" & intAllocationId & "') "
                Case enAllocation.SECTION_GROUP
                    strQ &= " AND (Self.sectiongroupunkid = '" & intAllocationId & "' OR Assess.sectiongroupunkid = '" & intAllocationId & "') "
                Case enAllocation.SECTION
                    strQ &= " AND (Self.sectionunkid = '" & intAllocationId & "' OR Assess.sectionunkid = '" & intAllocationId & "') "
                Case enAllocation.UNIT_GROUP
                    strQ &= " AND (Self.unitgroupunkid = '" & intAllocationId & "' OR Assess.unitgroupunkid = '" & intAllocationId & "') "
                Case enAllocation.UNIT
                    strQ &= " AND (Self.unitunkid = '" & intAllocationId & "' OR Assess.unitunkid = '" & intAllocationId & "') "
                Case enAllocation.TEAM
                    strQ &= " AND (Self.teamunkid = '" & intAllocationId & "' OR Assess.teamunkid = '" & intAllocationId & "') "
                Case enAllocation.JOB_GROUP
                    strQ &= " AND (Self.jobgroupunkid = '" & intAllocationId & "' OR Assess.jobgroupunkid = '" & intAllocationId & "') "
                Case enAllocation.JOBS
                    strQ &= " AND (Self.jobunkid = '" & intAllocationId & "' OR Assess.jobunkid = '" & intAllocationId & "') "
                Case enAllocation.EMPLOYEE
                    strQ &= " AND (Self.employeeunkid = '" & intAllocationId & "' OR Assess.employeeunkid = '" & intAllocationId & "') "
            End Select

            If objDataOperation.RecordCount(strQ) <= 0 Then
                blnFlag = False
            Else
                blnFlag = True
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isAllocationUsed; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrObjective_tran) </purpose>
    Public Function ObjectiveTranDelete(ByVal intObjectiveID As Integer, ByVal intObjectiveTranID As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrobjective_tran " & _
                   " SET isactive = 0 " & _
                   "WHERE objectivetranunkid = @objectivetranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@objectivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveTranID)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrobjective_master", "objectiveunkid", intObjectiveID, "hrobjective_tran", "objectivetranunkid", intObjectiveTranID, 2, 3) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ObjectiveTranDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, You cannot unassign some allocation(s). Reason : They are already linked with some transaction(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
