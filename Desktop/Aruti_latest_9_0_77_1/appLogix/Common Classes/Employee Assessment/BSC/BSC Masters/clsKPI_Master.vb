﻿'************************************************************************************************************************************
'Class Name : clsKPI_Master.vb
'Purpose    :
'Date       :04/01/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsKPI_Master
    Private Shared ReadOnly mstrModuleName As String = "clsKPI_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintKpiunkid As Integer
    Private mintObjectiveunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIsactive As Boolean = True


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mintEmployeeunkid As Integer = 0
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecWeight As Decimal = 0
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set kpiunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Kpiunkid() As Integer
        Get
            Return mintKpiunkid
        End Get
        Set(ByVal value As Integer)
            mintKpiunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set objectiveunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Objectiveunkid() As Integer
        Get
            Return mintObjectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintObjectiveunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property



    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Employeeunkid()
        Set(ByVal value)
            mintEmployeeunkid = value
        End Set
    End Property
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mdecWeight
        End Get
        Set(ByVal value As Decimal)
            mdecWeight = value
        End Set
    End Property
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  kpiunkid " & _
              ", objectiveunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive " & _
                    ", ISNULL(weight,0) AS weight " & _
             "FROM hrkpi_master " & _
             "WHERE kpiunkid = @kpiunkid "

            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintKpiunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintKpiunkid = CInt(dtRow.Item("kpiunkid"))
                mintObjectiveunkid = CInt(dtRow.Item("objectiveunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mdecWeight = dtRow.Item("weight")
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal strIncludeInactiveEmployee As String = "", _
                            Optional ByVal strEmployeeAsOnDate As String = "", _
                            Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
        'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '          "  hrkpi_master.kpiunkid " & _
            '          ", hrkpi_master.objectiveunkid " & _
            '          ", hrkpi_master.code " & _
            '          ", hrkpi_master.name " & _
            '          ", hrkpi_master.description " & _
            '          ", hrkpi_master.name1 " & _
            '          ", hrkpi_master.name2 " & _
            '          ", hrkpi_master.isactive " & _
            '          ", ISNULL(hrobjective_master.name,'') AS objective " & _
            '          ", ISNULL(hrobjective_master.employeeunkid,0) employeeunkid "

            'If ConfigParameter._Object._IsBSC_ByEmployee Then
            '    strQ &= ", ISNULL(hremployee_master.employeecode,'') employeecode " & _
            '          ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employee "
            'End If


            'strQ &= " FROM hrkpi_master " & _
            '      " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid "

            'If ConfigParameter._Object._IsBSC_ByEmployee Then
            '    strQ &= " JOIN hremployee_master ON hremployee_master.employeeunkid = hrobjective_master.employeeunkid "
            'End If
            strQ = "SELECT " & _
                      "  hrkpi_master.kpiunkid " & _
                      ", hrkpi_master.objectiveunkid " & _
                      ", hrkpi_master.code " & _
                      ", hrkpi_master.name " & _
                      ", hrkpi_master.description " & _
                      ", hrkpi_master.name1 " & _
                      ", hrkpi_master.name2 " & _
                      ", hrkpi_master.isactive " & _
                      ", ISNULL(hrobjective_master.name,'') AS objective " & _
                     ",ISNULL(hrobjective_master.employeeunkid,0) employeeunkid " & _
                     ",CASE WHEN hrobjective_master.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ELSE '' END AS employee " & _
                     ",ISNULL(hremployee_master.employeecode,'') employeecode " & _
                     ",hrobjective_master.isfinal " & _
                      ", hrobjective_master.perspectiveunkid " & _
                      ", hrobjective_master.code as ocode " & _
                      ", ISNULL(hrkpi_master.weight,0) AS kpi_weight " & _
                      ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                      ", ISNULL(cfcommon_period_tran.statusid,2) AS statusid " & _
                      ", ISNULL(STypId,0) AS STypId " & _
                      ", hrobjective_master.periodunkid " & _
                   "FROM hrkpi_master " & _
                    "JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                    " LEFT JOIN cfcommon_period_tran ON hrobjective_master.periodunkid = cfcommon_period_tran.periodunkid  " & _
                    "LEFT JOIN hremployee_master ON hrobjective_master.employeeunkid = hremployee_master.employeeunkid "

            strQ &= "LEFT JOIN " & _
                    "( " & _
                         "SELECT EId,YId,PId,STypId FROM " & _
                         "( " & _
                              "SELECT " & _
                                   " employeeunkid AS EId " & _
                                   ",yearunkid AS YId " & _
                                   ",periodunkid AS PId " & _
                                   ",statustypeid AS STypId " & _
                                   ",CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid,yearunkid,periodunkid ORDER BY objectivestatustranunkid DESC) AS RNo " & _
                              "FROM hrobjective_status_tran " & _
                         ")AS A WHERE RNo = 1 " & _
                    ") AS Obj_Status ON Obj_Status.EId = hrobjective_master.employeeunkid AND Obj_Status.PId = hrobjective_master.periodunkid AND Obj_Status.YId = hrobjective_master.yearunkid "

            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If
            If CBool(strIncludeInactiveEmployee) = False Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            End If

            
            If strUserAccessLevelFilterString = "" Then
                'Sohail (08 May 2015) -- Start
                'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                strQ &= UserAccessLevel._AccessLevelFilterString
                'strQ &= NewAccessLevelFilterString()
                'Sohail (08 May 2015) -- End
            Else
                strQ &= strUserAccessLevelFilterString
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            'S.SANDEEP [ 05 MARCH 2012 ] -- END


            If blnOnlyActive Then
                strQ &= " WHERE hrkpi_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrkpi_master) </purpose>
    Public Function Insert(Optional ByVal intUserUnkid As Integer = 0, _
                           Optional ByVal IsBSC_ByEmp As String = "") As Boolean 'S.SANDEEP [ 12 JUNE 2012 ] -- START-- END
        'Public Function Insert(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function Insert() As Boolean

        'S.SANDEEP [ 12 JUNE 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , mintObjectiveunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
        '    Return False
        'End If

        'If isExist(, mstrName, mintObjectiveunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
        '    Return False
        'End If

        If isExist(mstrCode, , mintObjectiveunkid, , IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintObjectiveunkid, , IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If
        'S.SANDEEP [ 12 JUNE 2012 ] -- END

        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintObjectiveunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight)
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            strQ = "INSERT INTO hrkpi_master ( " & _
              "  objectiveunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive" & _
              ", weight " & _
            ") VALUES (" & _
              "  @objectiveunkid " & _
              ", @code " & _
              ", @name " & _
              ", @description " & _
              ", @name1 " & _
              ", @name2 " & _
              ", @isactive" & _
              ", @weight " & _
            "); SELECT @@identity"
            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintKpiunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrkpi_master", "kpiunkid", mintKpiunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrkpi_master", "kpiunkid", mintKpiunkid, , intUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrkpi_master) </purpose>
    Public Function Update(Optional ByVal intUserUnkid As Integer = 0, _
                           Optional ByVal IsBSC_ByEmp As String = "") As Boolean 'S.SANDEEP [ 12 JUNE 2012 ] -- START-- END
        'Public Function Update(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function Update() As Boolean


        'S.SANDEEP [ 12 JUNE 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , mintObjectiveunkid, mintKpiunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
        '    Return False
        'End If

        'If isExist(, mstrName, mintObjectiveunkid, mintKpiunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
        '    Return False
        'End If

        If isExist(mstrCode, , mintObjectiveunkid, mintKpiunkid, IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintObjectiveunkid, mintKpiunkid, IsBSC_ByEmp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If
        'S.SANDEEP [ 12 JUNE 2012 ] -- END




        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintKpiunkid.ToString)
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintObjectiveunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight)
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            strQ = "UPDATE hrkpi_master SET " & _
              "  objectiveunkid = @objectiveunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", description = @description" & _
              ", name1 = @name1" & _
              ", name2 = @name2" & _
              ", isactive = @isactive " & _
                   ", weight = @weight " & _
            "WHERE kpiunkid = @kpiunkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- START {weight} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrkpi_master", "kpiunkid", mintKpiunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrkpi_master", "kpiunkid", mintKpiunkid, , intUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrkpi_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrkpi_master " & _
                      " SET isactive = 0 " & _
                      "WHERE kpiunkid = @kpiunkid "

            objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrkpi_master", "kpiunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrkpi_master", "kpiunkid", intUnkid, , intUserUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim blnIsUsed As Boolean = False
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                     "TABLE_NAME AS TableName " & _
                  "FROM INFORMATION_SCHEMA.COLUMNS " & _
                  "WHERE COLUMN_NAME='kpiunkid' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName") = "hrkpi_master" Then Continue For


                objDataOperation.ClearParameters()

                If dtRow.Item("TableName") = "hrbsc_analysis_tran" Then

                    strQ = "SELECT kpiunkid FROM " & dtRow.Item("TableName").ToString & " WHERE kpiunkid = @kpiunkid  and isvoid = 0"
                    objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                Else

                    strQ = "SELECT kpiunkid FROM " & dtRow.Item("TableName").ToString & " WHERE kpiunkid = @kpiunkid  and isactive = 1 "
            objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            Return blnIsUsed


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", _
                            Optional ByVal strName As String = "", _
                            Optional ByVal intObjectiveId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal IsBSC_ByEmp As String = "") As Boolean 'S.SANDEEP [ 12 JUNE 2012 ] -- START -- END
        'Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intObjectiveId As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'strQ = "SELECT " & _
            '  "  kpiunkid " & _
            '  ", objectiveunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", description " & _
            '  ", name1 " & _
            '  ", name2 " & _
            '  ", isactive " & _
            ' "FROM hrkpi_master " & _
            ' "WHERE isactive = 1 "


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            strQ = "SELECT " & _
              "  hrkpi_master.kpiunkid " & _
              ", hrkpi_master.objectiveunkid " & _
              ", hrkpi_master.code " & _
              ", hrkpi_master.name " & _
              ", hrkpi_master.description " & _
              ", hrkpi_master.name1 " & _
              ", hrkpi_master.name2 " & _
              ", hrkpi_master.isactive " & _
             "FROM hrkpi_master "


            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsBSC_ByEmployee Then
            '    strQ &= " JOIN hrobjective_master on hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid " & _
            '                " JOIN hremployee_master on hremployee_master.employeeunkid = hrobjective_master.employeeunkid "
            'End If

            If IsBSC_ByEmp.Trim.Length <= 0 Then
                IsBSC_ByEmp = ConfigParameter._Object._IsBSC_ByEmployee.ToString
            End If

            If CBool(IsBSC_ByEmp) = True Then
                strQ &= " JOIN hrobjective_master on hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid " & _
                            " JOIN hremployee_master on hremployee_master.employeeunkid = hrobjective_master.employeeunkid "
            End If
            'S.SANDEEP [ 12 JUNE 2012 ] -- END

            


            strQ &= "WHERE hrkpi_master.isactive = 1 "

            If strCode.Length > 0 Then
                strQ &= "AND hrkpi_master.code = @code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND hrkpi_master.name = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND hrkpi_master.kpiunkid <> @kpiunkid"
            End If


            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsBSC_ByEmployee Then
            '    If mintEmployeeunkid > 0 Then
            '        strQ &= " AND hrobjective_master.employeeunkid = @employeeunkid"
            '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            '    End If
            'Else
            '    If intObjectiveId > 0 Then
            '        strQ &= " AND hrkpi_master.objectiveunkid <> @objectiveunkid"
            '        objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveId)
            '    End If
            'End If
            If CBool(IsBSC_ByEmp) = True Then
                If mintEmployeeunkid > 0 Then
                    strQ &= " AND hrobjective_master.employeeunkid = @employeeunkid"
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                End If
            End If

                If intObjectiveId > 0 Then
                strQ &= " AND hrkpi_master.objectiveunkid = @objectiveunkid"
                    objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intObjectiveId)
                End If
            'S.SANDEEP [ 12 JUNE 2012 ] -- END



            'Pinkal (20-Jan-2012) -- End

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intObjectiveunkid As Integer = -1, Optional ByVal intEmployeeId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As id , @ItemName As  name , '' as code UNION "
            End If

            strQ &= "SELECT kpiunkid as id ,name as name ,code as code FROM hrkpi_master  WHERE 1=1 "

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If intEmployeeId >= 0 Then
                    strQ &= " AND hrkpi_master.objectiveunkid in (select objectiveunkid from hrobjective_master WHERE employeeunkid = @empId AND isactive = 1 AND referenceunkid =" & enAllocation.EMPLOYEE & " ) "
                End If
            End If

            If intObjectiveunkid > 0 Then
                strQ &= " AND objectiveunkid = '" & intObjectiveunkid & "'"
            End If

            strQ &= " AND hrkpi_master.isactive = 1 "

            objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetKPIUnkid(ByVal mstKCode As String, _
                                Optional ByVal mstKPIName As String = "", _
                                Optional ByVal iObjectiveId As Integer = 0) As Integer 'S.SANDEEP [ 20 JULY 2013 (mstKCode,iPeriodId,iEmpId) ] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = " SELECT " & _
            '          "     kpiunkid " & _
            '          " FROM hrkpi_master " & _
            '          " WHERE name = @name "

            strQ = "SELECT kpiunkid " & _
                      " FROM hrkpi_master " & _
                   "  JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                   "WHERE hrkpi_master.isactive = 1 "

            If iObjectiveId > 0 Then
                strQ &= " AND hrkpi_master.objectiveunkid = '" & iObjectiveId & "' "
            End If

            If mstKCode.Trim.Length > 0 Then
                If mstKCode.Contains("'") Then
                    mstKCode = mstKCode.Replace("'", "''")
                End If
                strQ &= " AND hrkpi_master.code = '" & mstKCode & "' "
            End If

            If mstKPIName.Trim.Length > 0 Then
                If mstKPIName.Contains("'") Then
                    mstKPIName = mstKPIName.Replace("'", "''")
                End If
                strQ &= " AND hrkpi_master.name = '" & mstKPIName & "' "
            End If
            'S.SANDEEP [ 20 JULY 2013 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("kpiunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetKPIUnkid; Module Name: " & mstrModuleName)
        End Try
        Return -1

    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
			Language.setMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class