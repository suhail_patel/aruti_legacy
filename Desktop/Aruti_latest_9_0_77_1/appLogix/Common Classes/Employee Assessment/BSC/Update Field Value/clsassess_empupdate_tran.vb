﻿'************************************************************************************************************************************
'Class Name : clsassess_empupdate_tran.vb
'Purpose    :
'Date       :17-Jun-2015
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsassess_empupdate_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_empupdate_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmpupdatetranunkid As Integer = 0
    Private mdtUpdatedate As Date = Nothing
    Private mintEmployeeunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintEmpfieldunkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mdecPct_Completed As Decimal = 0
    Private mintStatusunkid As Integer = 0
    Private mstrRemark As String = String.Empty
    Private mintEmpfieldtypeid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private xDataOpr As clsDataOperation = Nothing

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mintGoalAccomplishmentStatusid As Integer = 0
    Private mstrGoalAccomplishmentRemark As String = ""
    Private mstrWebFrmName As String = String.Empty
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Private mintChangeById As Integer = 0
    'S.SANDEEP |25-MAR-2019| -- START
    'Private mdblFinalValue As Double = 0
    Private mdblFinalValue As Decimal = 0
    'S.SANDEEP |25-MAR-2019| -- END

    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |24-APR-2020| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
    Private mintCalcModeId As Integer = 0
    'S.SANDEEP |24-APR-2020| -- END

#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empupdatetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Empupdatetranunkid() As Integer
        Get
            Return mintEmpupdatetranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpupdatetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set updatedate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Updatedate() As Date
        Get
            Return mdtUpdatedate
        End Get
        Set(ByVal value As Date)
            mdtUpdatedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empfieldunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Empfieldunkid() As Integer
        Get
            Return mintEmpfieldunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpfieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_completed
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Pct_Completed() As Decimal
        Get
            Return mdecPct_Completed
        End Get
        Set(ByVal value As Decimal)
            mdecPct_Completed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empfieldtypeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Empfieldtypeid() As Integer
        Get
            Return mintEmpfieldtypeid
        End Get
        Set(ByVal value As Integer)
            mintEmpfieldtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

    Public Property _GoalAccomplishmentStatusid() As Integer
        Get
            Return mintGoalAccomplishmentStatusid
        End Get
        Set(ByVal value As Integer)
            mintGoalAccomplishmentStatusid = value
        End Set
    End Property

    Public Property _GoalAccomplishmentRemark() As String
        Get
            Return mstrGoalAccomplishmentRemark
        End Get
        Set(ByVal value As String)
            mstrGoalAccomplishmentRemark = value
        End Set
    End Property

    'Public WriteOnly Property _WebFrmName() As String
    '    Set(ByVal value As String)
    '        mstrWebFrmName = value
    '    End Set
    'End Property

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Public Property _ChangeById() As Integer
        Get
            Return mintChangeById
        End Get
        Set(ByVal value As Integer)
            mintChangeById = value
        End Set
    End Property

    'S.SANDEEP |25-MAR-2019| -- START
    'Public Property _FinalValue() As Double
    '    Get
    '        Return mdblFinalValue
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblFinalValue = value
    '    End Set
    'End Property
    Public Property _FinalValue() As Decimal
        Get
            Return mdblFinalValue
        End Get
        Set(ByVal value As Decimal)
            mdblFinalValue = value
        End Set
    End Property
    'S.SANDEEP |25-MAR-2019| -- END

    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |24-APR-2020| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
    Public Property _CalcModeId() As Integer
        Get
            Return mintCalcModeId
        End Get
        Set(ByVal value As Integer)
            mintCalcModeId = value
        End Set
    End Property
    'S.SANDEEP |24-APR-2020| -- END
#End Region


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
#Region "Enum"

    Public Enum enGoalAccomplished_Status
        GA_Pending = 1
        GA_Approved = 2
        GA_Rejected = 3
    End Enum

    'S.SANDEEP |24-APR-2020| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
    Public Enum enCalcMode
        Increasing = 1
        Decreasing = 2
    End Enum
    'S.SANDEEP |24-APR-2020| -- END

#End Region
    'Shani (26-Sep-2016) -- End


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        Try
            strQ = "SELECT " & _
              "  empupdatetranunkid " & _
              ", updatedate " & _
              ", employeeunkid " & _
              ", periodunkid " & _
              ", empfieldunkid " & _
              ", fieldunkid " & _
              ", pct_completed " & _
              ", statusunkid " & _
              ", remark " & _
              ", empfieldtypeid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", approvalstatusunkid " & _
              ", approval_remark " & _
              ", approval_date " & _
              ", changebyid " & _
              ", finalvalue " & _
              ", ISNULL(calcmodeid,1) AS calcmodeid " & _
             "FROM hrassess_empupdate_tran " & _
             "WHERE empupdatetranunkid = @empupdatetranunkid "
            'S.SANDEEP |24-APR-2020| -- START {calcmodeid} -- END
            'Shani (26-Sep-2016) -- ADD [approvalstatusunkid,approval_remark,approval_date]


            objDataOperation.AddParameter("@empupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpupdatetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpupdatetranunkid = CInt(dtRow.Item("empupdatetranunkid"))
                mdtUpdatedate = dtRow.Item("updatedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmpfieldunkid = CInt(dtRow.Item("empfieldunkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mdecPct_Completed = dtRow.Item("pct_completed")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintEmpfieldtypeid = CInt(dtRow.Item("empfieldtypeid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                mintGoalAccomplishmentStatusid = CInt(dtRow.Item("approvalstatusunkid"))
                mstrGoalAccomplishmentRemark = dtRow.Item("approval_remark")
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                mintChangeById = CInt(dtRow.Item("changebyid"))
                'S.SANDEEP |25-MAR-2019| -- START
                'mdblFinalValue = CDbl(dtRow.Item("finalvalue"))
                mdblFinalValue = CDec(dtRow.Item("finalvalue"))
                'S.SANDEEP |25-MAR-2019| -- END

                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |24-APR-2020| -- START
                'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                mintCalcModeId = CInt(dtRow.Item("calcmodeid"))
                'S.SANDEEP |24-APR-2020| -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            ByVal iEmployeeId As Integer, _
                            ByVal iPeriodId As Integer, _
                            ByVal iEmpFieldTranId As Integer, _
                            Optional ByVal strFmtCurrency As String = "") As DataSet 'S.SANDEEP |25-FEB-2019| -- START {strFmtCurrency} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        Try
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'strQ = "SELECT " & _
            '       "  empupdatetranunkid " & _
            '       ", updatedate " & _
            '       ", employeeunkid " & _
            '       ", periodunkid " & _
            '       ", empfieldunkid " & _
            '       ", fieldunkid " & _
            '       ", CAST(pct_completed AS DECIMAL(10,2)) AS pct_completed " & _
            '       ", statusunkid " & _
            '       ", remark " & _
            '       ", empfieldtypeid " & _
            '       ", userunkid " & _
            '       ", isvoid " & _
            '       ", voiduserunkid " & _
            '       ", voiddatetime " & _
            '       ", voidreason " & _
            '       ", CASE WHEN statusunkid = '" & enCompGoalStatus.ST_PENDING & "' THEN @ST_PENDING " & _
            '       "       WHEN statusunkid = '" & enCompGoalStatus.ST_INPROGRESS & "' THEN @ST_INPROGRESS " & _
            '       "       WHEN statusunkid = '" & enCompGoalStatus.ST_COMPLETE & "' THEN @ST_COMPLETE " & _
            '       "       WHEN statusunkid = '" & enCompGoalStatus.ST_CLOSED & "' THEN @ST_CLOSED " & _
            '       "       WHEN statusunkid = '" & enCompGoalStatus.ST_ONTRACK & "' THEN @ST_ONTRACK " & _
            '       "       WHEN statusunkid = '" & enCompGoalStatus.ST_ATRISK & "' THEN @ST_ATRISK " & _
            '       "       WHEN statusunkid = '" & enCompGoalStatus.ST_NOTAPPLICABLE & "' THEN @ST_NOTAPPLICABLE " & _
            '       "  END AS dstatus " & _
            '       ", CONVERT(CHAR(8),updatedate,112) AS odate " & _
            '       ", '' AS ddate " & _
            '       "FROM hrassess_empupdate_tran " & _
            '       "WHERE isvoid = 0 "

            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)
            Dim strMappeedTableName As String = ""
            Dim strMappedFiledName As String = ""

            If strFmtCurrency.Trim.Length <= 0 Then strFmtCurrency = GUI.fmtCurrency

            Select Case intLinkedFld
                Case enWeight_Types.WEIGHT_FIELD1
                    strMappedFiledName = "empfield1unkid"
                    strMappeedTableName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD2
                    strMappedFiledName = "empfield2unkid"
                    strMappeedTableName = "hrassess_empfield2_master"
                Case enWeight_Types.WEIGHT_FIELD3
                    strMappedFiledName = "empfield3unkid"
                    strMappeedTableName = "hrassess_empfield3_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    strMappedFiledName = "empfield4unkid"
                    strMappeedTableName = "hrassess_empfield4_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    strMappedFiledName = "empfield5unkid"
                    strMappeedTableName = "hrassess_empfield5_master"
            End Select

            'S.SANDEEP |22-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
            '", CAST(CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(10,2)) AS NVARCHAR(MAX))+' '+ISNULL(cfcommon_master.name,'') AS pct_completed " & _
            '", CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(10,2)) AS pct_completed " & _
            'S.SANDEEP |22-MAR-2019| -- END

            strQ = "SELECT " & _
                   "  hrassess_empupdate_tran.empupdatetranunkid " & _
                   ", hrassess_empupdate_tran.updatedate " & _
                   ", hrassess_empupdate_tran.employeeunkid " & _
                   ", hrassess_empupdate_tran.periodunkid " & _
                   ", hrassess_empupdate_tran.empfieldunkid " & _
                   ", hrassess_empupdate_tran.fieldunkid " & _
                   ", CAST(CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS NVARCHAR(MAX))  AS pct_completed " & _
                   ", hrassess_empupdate_tran.statusunkid " & _
                   ", hrassess_empupdate_tran.remark " & _
                   ", hrassess_empupdate_tran.empfieldtypeid " & _
                   ", hrassess_empupdate_tran.userunkid " & _
                   ", hrassess_empupdate_tran.isvoid " & _
                   ", hrassess_empupdate_tran.voiduserunkid " & _
                   ", hrassess_empupdate_tran.voiddatetime " & _
                   ", hrassess_empupdate_tran.voidreason " & _
                   ", CASE WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_PENDING & "' THEN @ST_PENDING " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_INPROGRESS & "' THEN @ST_INPROGRESS " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_COMPLETE & "' THEN @ST_COMPLETE " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_CLOSED & "' THEN @ST_CLOSED " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_ONTRACK & "' THEN @ST_ONTRACK " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_ATRISK & "' THEN @ST_ATRISK " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_NOTAPPLICABLE & "' THEN @ST_NOTAPPLICABLE " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_POSTPONED & "' THEN @ST_POSTPONED " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_BEHIND_SCHEDULE & "' THEN @ST_BEHIND_SCHEDULE " & _
                   "       WHEN hrassess_empupdate_tran.statusunkid = '" & enCompGoalStatus.ST_AHEAD_SCHEDULE & "' THEN @ST_AHEAD_SCHEDULE " & _
                   "  END AS dstatus " & _
                   ", CONVERT(CHAR(8),hrassess_empupdate_tran.updatedate,112) AS odate " & _
                   ", '' AS ddate " & _
                   ", hrassess_empupdate_tran.approvalstatusunkid AS approvalstatusunkid " & _
                   ", hrassess_empupdate_tran.approval_remark AS approval_remark " & _
                   ", CONVERT(CHAR(8),hrassess_empupdate_tran.approval_date,112) AS approval_date " & _
                  ", hrassess_empupdate_tran.changebyid " & _
                  ", hrassess_empupdate_tran.finalvalue " & _
                   ", '' AS dfinalvalue " & _
                   ", ISNULL(cfcommon_master.name,'') AS uom " & _
                  ", CASE approvalstatusunkid WHEN '" & enGoalAccomplished_Status.GA_Pending & "' THEN @GA_Pending " & _
                  "                           WHEN '" & enGoalAccomplished_Status.GA_Approved & "' THEN @GA_Approved " & _
                  "                           WHEN '" & enGoalAccomplished_Status.GA_Rejected & "' THEN @GA_Rejected " & _
                  "                           ELSE '' " & _
                  " END AS GoalAccomplishmentStatus " & _
                   ", CASE WHEN hrassess_empupdate_tran.changebyid = 1 THEN @P " & _
                   "       WHEN hrassess_empupdate_tran.changebyid = 2 THEN @V " & _
                  "  END AS changeby " & _
                   ", CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36, 2)) AS pct_complete " & _
                   ", CASE WHEN goaltypeid = 2 AND changebyid = 1 THEN CAST(goalvalue*(finalvalue/100) AS DECIMAL(36,2)) ELSE finalvalue END AS ofinalvalue " & _
                   ", calcmodeid " & _
                   ", CASE WHEN calcmodeid = " & enCalcMode.Increasing & " THEN @Inc " & _
                   "       WHEN calcmodeid = " & enCalcMode.Decreasing & " THEN @Dec " & _
                   "  END AS calcmode " & _
                   ",ISNULL(CAST((SELECT " & _
                   "    SUM(b.finalvalue) " & _
                   "  FROM hrassess_empupdate_tran As b " & _
                   "    WHERE b.isvoid = 0 AND b.employeeunkid = hrassess_empupdate_tran.employeeunkid " & _
                   "    AND b.periodunkid = hrassess_empupdate_tran.periodunkid " & _
                   "    AND b.empfieldunkid= hrassess_empupdate_tran.empfieldunkid "
            If iEmpFieldTranId > 0 Then
                strQ &= " AND b.empfieldunkid = '" & iEmpFieldTranId & "' "
            End If

            If iEmployeeId > 0 Then
                strQ &= " AND b.employeeunkid = '" & iEmployeeId & "' "
            End If

            If iPeriodId > 0 Then
                strQ &= " AND b.periodunkid = '" & iPeriodId & "' "
            End If
            strQ &= " AND b.approvalstatusunkid IN (1,2) AND b.empupdatetranunkid <= hrassess_empupdate_tran.empupdatetranunkid " & _
                    ") AS NVARCHAR(MAX)),CAST(finalvalue AS NVARCHAR(MAX))) AS tp " & _
                   "FROM hrassess_empupdate_tran " & _
                   "    JOIN " & strMappeedTableName & " ON " & strMappeedTableName & "." & strMappedFiledName & " = hrassess_empupdate_tran.empfieldunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = " & strMappeedTableName & ".uomtypeid " & _
                   "WHERE hrassess_empupdate_tran.isvoid = 0 /*AND approvalstatusunkid IN (1,2)*/ "
            'S.SANDEEP |24-APR-2020| -- START {calcmodeid,tp} -- END
            'S.SANDEEP |05-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            '", CASE WHEN goaltypeid = 2 AND changebyid = 1 THEN goalvalue*(finalvalue/100) ELSE finalvalue END AS ofinalvalue " & _ ------ ADDED
            'S.SANDEEP |05-MAR-2019| -- END

            'S.SANDEEP |25-MAR-2019| -- START
            'REMOVED : DECIMAL(10, 2)
            'ADDED   : DECIMAL(36, 2)
            'S.SANDEEP |25-MAR-2019| -- END


            'Shani (26-Sep-2016) -- End
            'S.SANDEEP |18-JAN-2019| -- START {changebyid,finalvalue} -- END

            If iEmpFieldTranId > 0 Then
                strQ &= " AND hrassess_empupdate_tran.empfieldunkid = '" & iEmpFieldTranId & "' "
            End If

            If iEmployeeId > 0 Then
                strQ &= " AND hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' "
            End If

            If iPeriodId > 0 Then
                strQ &= " AND hrassess_empupdate_tran.periodunkid = '" & iPeriodId & "' "
            End If

            'S.SANDEEP |18-JAN-2019| -- START
            'strQ &= " ORDER BY updatedate DESC "
            strQ &= " ORDER BY updatedate DESC,empupdatetranunkid DESC "
            'S.SANDEEP |18-JAN-2019| -- END


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            'objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            'objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            'objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            'objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            'objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            'objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 579, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 580, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 581, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 582, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 583, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 584, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 585, "Not Applicable"))
            objDataOperation.AddParameter("@ST_POSTPONED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 748, "Postponed"))
            objDataOperation.AddParameter("@ST_BEHIND_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 749, "Behind the Schedule"))
            objDataOperation.AddParameter("@ST_AHEAD_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 750, "Ahead of Schedule"))

            objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Pending"))
            objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 760, "Approved"))
            objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 761, "Rejected"))
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |18-JAN-2019| -- START
            objDataOperation.AddParameter("@P", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 101, "Percentage"))
            objDataOperation.AddParameter("@V", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 102, "Value"))
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            objDataOperation.AddParameter("@Inc", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Increasing"))
            objDataOperation.AddParameter("@Dec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Decreasing"))
            'S.SANDEEP |24-APR-2020| -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each row As DataRow In dsList.Tables(0).Rows
                row.Item("ddate") = eZeeDate.convertDate(row.Item("odate").ToString).ToShortDateString
                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                row.Item("approval_date") = eZeeDate.convertDate(row.Item("approval_date").ToString).ToShortDateString
                'Shani (26-Sep-2016) -- End

'S.SANDEEP |25-FEB-2019| -- START
'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'row.Item("dfinalvalue") = Format(CDec(row("finalvalue")), strFmtCurrency) & " " & row("uom").ToString()
                row.Item("dfinalvalue") = Format(CDec(row("ofinalvalue")), strFmtCurrency) & " " & row("uom").ToString()
                'S.SANDEEP |05-MAR-2019| -- END

'S.SANDEEP |25-FEB-2019| -- END

                'S.SANDEEP |24-APR-2020| -- START
                'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                row.Item("tp") = Format(CDec(row("tp")), strFmtCurrency) & " " & row("uom").ToString()
                'S.SANDEEP |24-APR-2020| -- END

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_empupdate_tran) </purpose>
    Public Function Insert() As Boolean
        'S.SANDEEP |18-JAN-2019| -- START
        'If isExist(mdtUpdatedate, 0, 0, mintEmpfieldunkid, mintEmpfieldtypeid) Then
        '    If xDataOpr Is Nothing Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Progress is already defined for the selected date.")
        '    Else
        '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Progress is already defined for the current date.")
        '    End If
        '    Return False
        'End If
        'S.SANDEEP |18-JAN-2019| -- END

        If isExist(Nothing, mintStatusunkid, mdecPct_Completed, mintEmpfieldunkid, mintEmpfieldtypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Combination of status and percentage is already defined.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If

        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@updatedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUpdatedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfieldunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPct_Completed)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfieldtypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalAccomplishmentStatusid.ToString)
            objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, mstrGoalAccomplishmentRemark.Length, mstrGoalAccomplishmentRemark.ToString)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@changebyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangeById)

            'S.SANDEEP |25-MAR-2019| -- START
            'objDataOperation.AddParameter("@finalvalue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblFinalValue)
            objDataOperation.AddParameter("@finalvalue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblFinalValue)
            'S.SANDEEP |25-MAR-2019| -- END

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            objDataOperation.AddParameter("@calcmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalcModeId)
            'S.SANDEEP |24-APR-2020| -- END


            'S.SANDEEP [01-OCT-2018] -- END

            'strQ = "INSERT INTO hrassess_empupdate_tran ( " & _
            '           "  updatedate " & _
            '           ", employeeunkid " & _
            '           ", periodunkid " & _
            '           ", empfieldunkid " & _
            '           ", fieldunkid " & _
            '           ", pct_completed " & _
            '           ", statusunkid " & _
            '           ", remark " & _
            '           ", empfieldtypeid " & _
            '           ", userunkid " & _
            '           ", isvoid " & _
            '           ", voiduserunkid " & _
            '           ", voiddatetime " & _
            '           ", voidreason" & _
            '       ") VALUES (" & _
            '           "  @updatedate " & _
            '           ", @employeeunkid " & _
            '           ", @periodunkid " & _
            '           ", @empfieldunkid " & _
            '           ", @fieldunkid " & _
            '           ", @pct_completed " & _
            '           ", @statusunkid " & _
            '           ", @remark " & _
            '           ", @empfieldtypeid " & _
            '           ", @userunkid " & _
            '           ", @isvoid " & _
            '           ", @voiduserunkid " & _
            '           ", @voiddatetime " & _
            '           ", @voidreason" & _
            '       "); SELECT @@identity"

            strQ = "INSERT INTO hrassess_empupdate_tran ( " & _
                       "  updatedate " & _
                       ", employeeunkid " & _
                       ", periodunkid " & _
                       ", empfieldunkid " & _
                       ", fieldunkid " & _
                       ", pct_completed " & _
                       ", statusunkid " & _
                       ", remark " & _
                       ", empfieldtypeid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                       ", approvalstatusunkid" & _
                       ", approval_remark" & _
                       ", approval_date " & _
                       ", changebyid " & _
                       ", finalvalue " & _
                       ", calcmodeid " & _
                   ") VALUES (" & _
                       "  @updatedate " & _
                       ", @employeeunkid " & _
                       ", @periodunkid " & _
                       ", @empfieldunkid " & _
                       ", @fieldunkid " & _
                       ", @pct_completed " & _
                       ", @statusunkid " & _
                       ", @remark " & _
                       ", @empfieldtypeid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                       ", @approvalstatusunkid" & _
                       ", @approval_remark" & _
                       ", GETDATE() " & _
                       ", @changebyid " & _
                       ", @finalvalue " & _
                       ", @calcmodeid " & _
                   "); SELECT @@identity"
            'S.SANDEEP |24-APR-2020| -- START {calcmodeid} -- END
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585|ARUTI-} [changebyid,finalvalue] -- END

            'Shani (26-Sep-2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpupdatetranunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_empupdate_tran", "empupdatetranunkid", mintEmpupdatetranunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Private Function UpdateRowData(ByVal row As DataRow, ByVal strQ As String, ByVal strRemark As String, ByVal intAprSatusId As Integer, ByVal xOperation As clsDataOperation) As Boolean
        Try
            xOperation.ClearParameters()
            xOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, row("per_comp"))
            xOperation.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAprSatusId)
            xOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, strRemark.Length, strRemark)
            xOperation.AddParameter("@empupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("empupdatetranunkid"))

            Call xOperation.ExecNonQuery(strQ)

            If xOperation.ErrorMessage <> "" Then
                Throw New Exception(xOperation.ErrorNumber & ": " & xOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Update(ByVal xDTable As DataTable, ByVal strRemark As String, _
                           ByVal eStatus As enGoalAccomplished_Status, _
                           ByVal intUserId As Integer, _
                           ByVal strFormName As String, _
                           ByVal strHost As String, _
                           ByVal blnIsWeb As Boolean, _
                           ByVal strIPAddr As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objOperation As New clsDataOperation
        objOperation.BindTransaction()
        Try
            If xDTable IsNot Nothing AndAlso xDTable.Rows.Count > 0 Then
                Dim strFlds As String = String.Join(",", xDTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid").ToString).ToArray())
                If strFlds.Trim.Length > 0 Then

                    strQ = "UPDATE hrassess_empupdate_tran SET " & _
                           "     pct_completed = @pct_completed " & _
                           "    ,approvalstatusunkid = @approvalstatusunkid " & _
                           "    ,approval_remark = @approval_remark " & _
                           "    ,approval_date = GETDATE() " & _
                           "WHERE empupdatetranunkid = @empupdatetranunkid "

                    xDTable.AsEnumerable().ToList().ForEach(Function(x) UpdateRowData(x, strQ, strRemark, eStatus, objOperation))

                    strQ = "DECLARE @words VARCHAR (MAX) " & _
                           "   SET @words = '" & strFlds & "' " & _
                           "DECLARE @split TABLE(word INT) " & _
                           "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                           "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                           "WHILE @start < @stop begin " & _
                           "    SELECT " & _
                           "       @end   = CHARINDEX(',',@words,@start) " & _
                           "      ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                           "      ,@start = @end+1 " & _
                           "    INSERT @split VALUES (@word) " & _
                           "END "
                    strQ &= "INSERT INTO atcommon_log " & _
                            "( " & _
                            "     refunkid " & _
                            "    ,table_name " & _
                            "    ,audittype " & _
                            "    ,audituserunkid " & _
                            "    ,auditdatetime " & _
                            "    ,ip " & _
                            "    ,host " & _
                            "    ,value " & _
                            "    ,form_name " & _
                            "    ,module_name1 " & _
                            "    ,module_name2 " & _
                            "    ,module_name3 " & _
                            "    ,module_name4 " & _
                            "    ,module_name5 " & _
                            "    ,isweb " & _
                            "    ,loginemployeeunkid " & _
                            ") " & _
                            "SELECT " & _
                            "    [@split].word " & _
                            "   ,'hrassess_empupdate_tran' " & _
                            "   ,2 " & _
                            "   ," & intUserId & " " & _
                            "   ,GETDATE() " & _
                            "   ,'" & strIPAddr & "' " & _
                            "   ,'" & strHost & "' " & _
                            "   ,CAST((SELECT TOP 1 * FROM hrassess_empupdate_tran WHERE empupdatetranunkid = [@split].word FOR XML PATH(''),ROOT('hrassess_empupdate_tran'),ELEMENTS XSINIL ) AS XML) " & _
                            "   ,'frmAppRejGoalAccomplishmentList' " & _
                            "   ,'WEB' " & _
                            "   ,'mnuAssessment' " & _
                            "   ,'mnuSetups' " & _
                            "   ,'' " & _
                            "   ,'' " & _
                            "   ,1 " & _
                            "   ,-1 " & _
                            "FROM @split " & _
                            "JOIN hrassess_empupdate_tran ON hrassess_empupdate_tran.empupdatetranunkid = [@split].word "
                   
                    Call objOperation.ExecNonQuery(strQ)

                    If objOperation.ErrorMessage <> "" Then
                        Throw New Exception(objOperation.ErrorNumber & ": " & objOperation.ErrorMessage)
                    End If


                End If
            End If

            objOperation.ReleaseTransaction(True)
        Catch ex As Exception
            objOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_empupdate_tran) </purpose>
    Public Function Update() As Boolean
        'S.SANDEEP |05-MAR-2019| -- START
        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
        'If isExist(mdtUpdatedate, 0, 0, mintEmpfieldunkid, mintEmpfieldtypeid, mintEmpupdatetranunkid) Then
        '    If xDataOpr Is Nothing Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Progress is already defined for the selected date.")
        '    Else
        '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Progress is already defined for the current date.")
        '    End If
        '    Return False
        'End If
        'S.SANDEEP |05-MAR-2019| -- END
        

        If isExist(Nothing, mintStatusunkid, mdecPct_Completed, mintEmpfieldunkid, mintEmpfieldtypeid, mintEmpupdatetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Combination of status and percentage is already defined.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@empupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpupdatetranunkid.ToString)
            objDataOperation.AddParameter("@updatedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUpdatedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfieldunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPct_Completed)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            'S.SANDEEP |31-DEC-2021| -- START
            'ISSUE : DATA GETTING TRUNCATED
            'objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            'S.SANDEEP |31-DEC-2021| -- END        
            objDataOperation.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfieldtypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalAccomplishmentStatusid.ToString)
            objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, mstrGoalAccomplishmentRemark.Length, mstrGoalAccomplishmentRemark.ToString)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@changebyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangeById)

            'S.SANDEEP |25-MAR-2019| -- START
            'objDataOperation.AddParameter("@finalvalue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblFinalValue)
            objDataOperation.AddParameter("@finalvalue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblFinalValue)
            'S.SANDEEP |25-MAR-2019| -- END

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            objDataOperation.AddParameter("@calcmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalcModeId)
            'S.SANDEEP |24-APR-2020| -- END

            'S.SANDEEP [01-OCT-2018] -- END

            'strQ = "UPDATE hrassess_empupdate_tran SET " & _
            '       "  updatedate = @updatedate" & _
            '       ", employeeunkid = @employeeunkid" & _
            '       ", periodunkid = @periodunkid" & _
            '       ", empfieldunkid = @empfieldunkid" & _
            '       ", fieldunkid = @fieldunkid" & _
            '       ", pct_completed = @pct_completed" & _
            '       ", statusunkid = @statusunkid" & _
            '       ", remark = @remark" & _
            '       ", empfieldtypeid = @empfieldtypeid" & _
            '       ", userunkid = @userunkid" & _
            '       ", isvoid = @isvoid" & _
            '       ", voiduserunkid = @voiduserunkid" & _
            '       ", voiddatetime = @voiddatetime" & _
            '       ", voidreason = @voidreason " & _
            '       "WHERE empupdatetranunkid = @empupdatetranunkid "
            strQ = "UPDATE hrassess_empupdate_tran SET " & _
                   "  updatedate = @updatedate" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", periodunkid = @periodunkid" & _
                   ", empfieldunkid = @empfieldunkid" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", pct_completed = @pct_completed" & _
                   ", statusunkid = @statusunkid" & _
                   ", remark = @remark" & _
                   ", empfieldtypeid = @empfieldtypeid" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", approvalstatusunkid = @approvalstatusunkid " & _
                   ", approval_remark = @approval_remark " & _
                   ", approval_date = GETDATE() " & _
                   ", changebyid = @changebyid " & _
                   ", finalvalue = @finalvalue " & _
                   ", calcmodeid = @calcmodeid " & _
                   "WHERE empupdatetranunkid = @empupdatetranunkid "
            'S.SANDEEP |24-APR-2020| -- START {calcmodeid} -- END
            'Shani (26-Sep-2016) -- End
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585|ARUTI-} [changebyid,finalvalue] -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_empupdate_tran", mintEmpupdatetranunkid, "empupdatetranunkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_empupdate_tran", "empupdatetranunkid", mintEmpupdatetranunkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            'End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_empupdate_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hrassess_empupdate_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE empupdatetranunkid = @empupdatetranunkid "

            objDataOperation.AddParameter("@empupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_empupdate_tran", "empupdatetranunkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''S.SANDEEP |09-JUL-2019| -- START
    ''ISSUE/ENHANCEMENT : PA CHANGES
    'Public Function Delete(ByVal intFieldTranUnkid As Integer, _
    '                       ByVal eFieldTypeId As enWeight_Types, _
    '                       ByVal intEmpId As Integer, _
    '                       ByVal intPeriodId As Integer, _
    '                       ByVal xDataOper As clsDataOperation) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        xDataOper.ClearParameters()

    '        strQ = "SELECT " & _
    '               "    empupdatetranunkid " & _
    '               "FROM hrassess_empupdate_tran " & _
    '               "WHERE isvoid = 0 " & _
    '               " AND employeeunkid = @employeeunkid " & _
    '               " AND periodunkid = @periodunkid " & _
    '               " AND empfieldunkid = @empfieldunkid " & _
    '               " AND fieldunkid = @fieldunkid "

    '        xDataOper.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
    '        xDataOper.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
    '        xDataOper.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFieldTranUnkid)
    '        xDataOper.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, eFieldTypeId)

    '        dsList = xDataOper.ExecQuery(strQ, "List")

    '        If xDataOper.ErrorMessage <> "" Then
    '            exForce = New Exception(xDataOper.ErrorNumber & ": " & xDataOper.ErrorMessage)
    '            Throw exForce
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    ''S.SANDEEP |09-JUL-2019| -- END

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iDate As Date, ByVal iStatusId As Integer, ByVal iPercent As Decimal, _
                            ByVal iEmpfieldunkid As Integer, ByVal iEmpfieldtypeid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  empupdatetranunkid " & _
                   ", updatedate " & _
                   ", employeeunkid " & _
                   ", periodunkid " & _
                   ", empfieldunkid " & _
                   ", fieldunkid " & _
                   ", pct_completed " & _
                   ", statusunkid " & _
                   ", remark " & _
                   ", empfieldtypeid " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", changebyid " & _
                   ", finalvalue " & _
                   ", calcmodeid " & _
                   "FROM hrassess_empupdate_tran " & _
                   "WHERE isvoid = 0 "
            'S.SANDEEP |24-APR-2020| -- START {calcmodeid} -- END
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585|ARUTI-} [changebyid,finalvalue] -- END

            'S.SANDEEP |25-MAR-2019| -- START
            ''S.SANDEEP [29-NOV-2017] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 32
            'strQ &= " AND approvalstatusunkid <> '" & Convert.ToInt32(enGoalAccomplished_Status.GA_Rejected) & "' "
            ''S.SANDEEP [29-NOV-2017] -- END
            strQ &= " AND approvalstatusunkid <> @approvalstatusunkid "
            objDataOperation.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToInt32(enGoalAccomplished_Status.GA_Rejected))
            'S.SANDEEP |25-MAR-2019| -- END

            'S.SANDEEP |25-MAR-2019| -- START
            'If iDate <> Nothing Then
            '    strQ &= " AND CONVERT(CHAR(8),updatedate,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
            'End If

            'If iStatusId > 0 Then
            '    strQ &= " AND statusunkid = '" & iStatusId & "' "
            'End If

            'If iPercent > 0 Then
            '    strQ &= " AND pct_completed = '" & iPercent & "' "
            'End If

            'If iEmpfieldunkid > 0 Then
            '    strQ &= " AND empfieldunkid = '" & iEmpfieldunkid & "' "
            'End If

            'If iEmpfieldtypeid > 0 Then
            '    strQ &= " AND empfieldtypeid = '" & iEmpfieldtypeid & "' "
            'End If
            If iDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),updatedate,112) = @updatedate "
                objDataOperation.AddParameter("@updatedate", SqlDbType.NVarChar, eZeeDataType.DECIMAL_SIZE, eZeeDate.convertDate(iDate).ToString)
            End If

            If iStatusId > 0 Then
                strQ &= " AND statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iStatusId)
            End If

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            'If iPercent > 0 Then
            '    strQ &= " AND pct_completed = @pct_completed "
            '    objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iPercent)
            'End If
            'If iPercent > 0 Then
                strQ &= " AND pct_completed = @pct_completed "
                objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iPercent)
            'End If
            'S.SANDEEP |24-APR-2020| -- END


            If iEmpfieldunkid > 0 Then
                strQ &= " AND empfieldunkid = @empfieldunkid "
                objDataOperation.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpfieldunkid)
            End If

            If iEmpfieldtypeid > 0 Then
                strQ &= " AND empfieldtypeid = @empfieldtypeid "
                objDataOperation.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpfieldtypeid)
            End If
            'S.SANDEEP |25-MAR-2019| -- END

            If intUnkid > 0 Then
                strQ &= " AND empupdatetranunkid <> @empupdatetranunkid"
            End If

            objDataOperation.AddParameter("@empupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Public Function GetLatestProgress(ByVal iDate As Date, ByVal iEmployeeId As Integer, ByVal iPeriodId As Integer) As DataSet
    Public Function GetLatestProgress(ByVal iDate As Date, ByVal iEmployeeId As Integer, ByVal iPeriodId As Integer, Optional ByVal iIsAccomplishementScreen As Boolean = False) As DataSet
        'Shani (26-Sep-2016) -- End


        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "     A.pct_completed " & _
                       "    ,A.dstatus " & _
                       "    ,A.remark " & _
                       "    ,A.cdate " & _
                       "    ,A.empfieldunkid " & _
                       "    ,A.empfieldtypeid " & _
                       "    ,A.fieldunkid " & _
                       "    ,A.statusunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         CAST(pct_completed AS DECIMAL(36,2)) AS pct_completed " & _
                       "        ,CASE WHEN statusunkid = '" & enCompGoalStatus.ST_PENDING & "' THEN @ST_PENDING " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_INPROGRESS & "' THEN @ST_INPROGRESS " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_COMPLETE & "' THEN @ST_COMPLETE " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_CLOSED & "' THEN @ST_CLOSED " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_ONTRACK & "' THEN @ST_ONTRACK " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_ATRISK & "' THEN @ST_ATRISK " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_NOTAPPLICABLE & "' THEN @ST_NOTAPPLICABLE " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_POSTPONED & "' THEN @ST_POSTPONED " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_BEHIND_SCHEDULE & "' THEN @ST_BEHIND_SCHEDULE " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_AHEAD_SCHEDULE & "' THEN @ST_AHEAD_SCHEDULE " & _
                       "         END AS dstatus " & _
                       "        ,remark " & _
                       "        ,CONVERT(CHAR(8),updatedate,112) AS cdate " & _
                       "        ,statusunkid " & _
                       "        ,empfieldunkid " & _
                       "        ,fieldunkid " & _
                       "        ,empfieldtypeid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid,empfieldunkid ORDER BY updatedate DESC,empupdatetranunkid DESC) AS RN " & _
                       "        ,calcmodeid " & _
                       "        ,CASE WHEN calcmodeid = " & enCalcMode.Increasing & " THEN @Inc " & _
                       "              WHEN calcmodeid = " & enCalcMode.Decreasing & " THEN @Dec " & _
                       "         END AS calcmode " & _
                       "    FROM hrassess_empupdate_tran WITH (NOLOCK) WHERE isvoid = 0 "

                If iIsAccomplishementScreen = False Then
                    StrQ &= "AND approvalstatusunkid = '" & enGoalAccomplished_Status.GA_Approved & "' "
                End If

                StrQ &= "        AND employeeunkid = '" & iEmployeeId & "' AND periodunkid = '" & iPeriodId & "' AND CONVERT(CHAR(8),updatedate,112) <= '" & eZeeDate.convertDate(iDate).ToString & "' " & _
                       ") AS A WHERE A.RN = 1 "
                'S.SANDEEP |24-APR-2020| -- START {calcmodeid} -- END
                'S.SANDEEP |16-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA-ERROR OR PROGRESS UPDATE
                '--------------------- REMOVED
                'CAST(pct_completed AS DECIMAL(10,2)) AS pct_completed
                ',ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid,empfieldunkid ORDER BY updatedate DESC) AS RN
                '--------------------- ADDED
                'CAST(pct_completed AS DECIMAL(36,6)) AS pct_completed
                ',ROW_NUMBER()OVER(PARTITION BY employeeunkid,periodunkid,empfieldunkid ORDER BY updatedate DESC,empupdatetranunkid DESC) AS RN
                'S.SANDEEP |16-JUL-2019| -- END


                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                '--------------------- REMOVED
                'CAST(pct_completed AS DECIMAL(36,6)) AS pct_completed
                '--------------------- ADDED
                'CAST(pct_completed AS DECIMAL(36,2)) AS pct_completed
                'S.SANDEEP |21-AUG-2019| -- END


                'Shani (26-Sep-2016) -- []


                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'objDataOpr.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
                'objDataOpr.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
                'objDataOpr.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
                'objDataOpr.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
                'objDataOpr.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
                'objDataOpr.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
                'objDataOpr.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))
                objDataOpr.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 579, "Pending"))
                objDataOpr.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 580, "In progress"))
                objDataOpr.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 581, "Complete"))
                objDataOpr.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 582, "Closed"))
                objDataOpr.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 583, "On Track"))
                objDataOpr.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 584, "At Risk"))
                objDataOpr.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 585, "Not Applicable"))
                objDataOpr.AddParameter("@ST_POSTPONED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 748, "Postponed"))
                objDataOpr.AddParameter("@ST_BEHIND_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 749, "Behind the Schedule"))
                objDataOpr.AddParameter("@ST_AHEAD_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 750, "Ahead of Schedule"))
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP |24-APR-2020| -- START
                'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                objDataOpr.AddParameter("@Inc", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Increasing"))
                objDataOpr.AddParameter("@Dec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Decreasing"))
                'S.SANDEEP |24-APR-2020| -- END

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLatestProgress; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    'Public Sub Send_Notification_Assessor(ByVal iEmployeeId As Integer, _
    '                                      ByVal iPeriodId As Integer, _
    '                                      ByVal iYearId As Integer, _
    '                                      ByVal sYearName As String, _
    '                                      ByVal strDatabaseName As String, _
    '                                      ByVal intEmpUpdateTranUnkId As Integer, _
    '                                      Optional ByVal iCompanyId As Integer = 0, _
    '                                      Optional ByVal sArutiSSURL As String = "", _
    '                                      Optional ByVal iLoginTypeId As Integer = 0, _
    '                                      Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                      Optional ByVal iUserId As Integer = 0)

    '    Dim dsList As DataSet = Nothing
    '    Dim strLink As String = String.Empty
    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
    '        If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
    '        Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

    '        dsList = (New clsAssessor).GetEmailNotificationList(iEmployeeId, False, objDataOperation, "List")

    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail

    '        Dim objPeriod As New clscommom_period_Tran
    '        objPeriod._Periodunkid(strDatabaseName) = iPeriodId
    '        objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId


    '        _Empupdatetranunkid = intEmpUpdateTranUnkId

    '        Dim intFiledMapId As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(mintPeriodunkid)
    '        Dim strBSCItem As String = ""

    '        Select Case intFiledMapId
    '            Case enWeight_Types.WEIGHT_FIELD1
    '                Dim objFiled1 As New clsassess_empfield1_master
    '                objFiled1._Empfield1unkid = mintEmpfieldunkid
    '                strBSCItem = objFiled1._Field_Data
    '                objFiled1 = Nothing
    '            Case enWeight_Types.WEIGHT_FIELD2
    '                Dim objFiled2 As New clsassess_empfield2_master
    '                objFiled2._Empfield2unkid = mintEmpfieldunkid
    '                strBSCItem = objFiled2._Field_Data
    '                objFiled2 = Nothing
    '            Case enWeight_Types.WEIGHT_FIELD3
    '                Dim objFiled3 As New clsassess_empfield3_master
    '                objFiled3._Empfield3unkid = mintEmpfieldunkid
    '                strBSCItem = objFiled3._Field_Data
    '                objFiled3 = Nothing
    '            Case enWeight_Types.WEIGHT_FIELD4
    '                Dim objFiled4 As New clsassess_empfield4_master
    '                objFiled4._Empfield4unkid = mintEmpfieldunkid
    '                strBSCItem = objFiled4._Field_Data
    '                objFiled4 = Nothing
    '            Case enWeight_Types.WEIGHT_FIELD5
    '                Dim objFiled5 As New clsassess_empfield5_master
    '                objFiled5._Empfield5unkid = mintEmpfieldunkid
    '                strBSCItem = objFiled5._Field_Data
    '                objFiled5 = Nothing
    '        End Select



    '        For Each dtRow As DataRow In dsList.Tables("List").Rows
    '            If dtRow.Item("Email") = "" Then Continue For
    '            Dim strMessage As String = ""
    '            objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification for Approval/Rejection Goal Accomplishment.")
    '            strMessage = "<HTML> <BODY>"
    '            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & dtRow.Item("Ename").ToString() & ", <BR><BR>"
    '            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "The status of ") & strBSCItem
    '            strMessage &= Language.getMessage(mstrModuleName, 16, "has been updated as on ") & mdtUpdatedate.ToShortDateString()
    '            strMessage &= "<BR><BR>"
    '            strMessage &= Language.getMessage(mstrModuleName, 7, "Please click the link below to Approve/Reject the update.")
    '            strMessage &= "<BR><BR>"
    '            strLink = sArutiSSURL & "/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & iPeriodId.ToString & "|" & intEmpUpdateTranUnkId.ToString))
    '            strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '            strMessage &= "</BODY></HTML>"
    '            objMail._Message = strMessage
    '            objMail._ToEmail = dtRow.Item("Email")
    '            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '            If mstrWebFrmName.Trim.Length > 0 Then
    '                objMail._Form_Name = mstrWebFrmName
    '            End If
    '            objMail._LogEmployeeUnkid = iLoginEmployeeId
    '            objMail._OperationModeId = iLoginTypeId
    '            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
    '            objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '            objMail.SendMail()
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Send_Notification_Assessor", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Public Sub Send_Notification_Assessor(ByVal iEmployeeId As Integer, _
                                          ByVal iPeriodId As Integer, _
                                          ByVal iYearId As Integer, _
                                          ByVal sYearName As String, _
                                          ByVal strDatabaseName As String, _
                                          ByVal intEmpUpdateTranUnkId As Integer, _
                                          Optional ByVal iCompanyId As Integer = 0, _
                                          Optional ByVal sArutiSSURL As String = "", _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0)

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation

        Try
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
            If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            dsList = (New clsAssessor).GetEmailNotificationList(iEmployeeId, False, objDataOperation, "List")

            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId

            'Gajanan [26-Feb-2019] -- Start
            'Enhancement - Email Language Changes For NMB.
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
            'Gajanan [26-Feb-2019] -- End

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("Email") = "" Then Continue For
                Dim strMessage As String = ""
                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                'objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification for Approval/Rejection Goal Accomplishment.")
                objMail._Subject = Language.getMessage(mstrModuleName, 21, "Notification for Reviewing Progress check.")
                'Gajanan [26-Feb-2019] -- End

                strMessage = "<HTML> <BODY>"
                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                'strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & dtRow.Item("Ename").ToString() & ", <BR><BR>"
                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(dtRow.Item("Ename").ToString().ToLower()).ToString() & ", <BR><BR>"


                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 17, "This is to inform you that i have done " & _
                '                                                                           "changes in the progress update for the goals assigned to me for assessment period ")


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 22, "Please note that, I have submitted progress update for the goals assigned to me for assessment period ")
                'strMessage &= Language.getMessage(mstrModuleName, 22, "Please note that, I have submitted progress update for the goals assigned to me for assessment period ") & " "
                strMessage &= Language.getMessage(mstrModuleName, 30, "Please note that, I have submitted my objective progress updates for assessment period") & " "
                'Pinkal (22-Mar-2019) -- End


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "(<B>" & objPeriod._Period_Name & "</B>)"

                'S.SANDEEP |30-MAR-2019| -- START
                'strMessage &= "<B>(" & objPeriod._Period_Name & ")</B>"
                strMessage &= "<B>(" & objPeriod._Period_Name & ")</B>."
                'S.SANDEEP |30-MAR-2019| -- END
                'Pinkal (22-Mar-2019) -- End


                strMessage &= "<BR><BR>"

                'strMessage &= Language.getMessage(mstrModuleName, 7, "Please click the link below to Approve/Reject the update.")
                strMessage &= Language.getMessage(mstrModuleName, 23, "Please click the link below to review.")
                'Gajanan [26-Feb-2019] -- End

                strMessage &= "<BR><BR>"
                'S.SANDEEP |05-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'strLink = sArutiSSURL & "/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & iPeriodId.ToString))
                strLink = sArutiSSURL & "/Assessment New/Performance Goals/wPgUpdateGoalProgress.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & iPeriodId.ToString))
                'S.SANDEEP |05-MAR-2019| -- END


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                'Pinkal (22-Mar-2019) -- End


                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 35
                strMessage &= "<BR><BR>"


                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                'strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                strMessage &= Language.getMessage(mstrModuleName, 24, "Regards,")
                strMessage &= "<BR>" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString()
                'Gajanan [26-Feb-2019] -- End


                'S.SANDEEP [29-NOV-2017] -- END
                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"
                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("Email")
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                With objMail
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objMail.SendMail()
                objMail.SendMail(iCompanyId)
                'Sohail (30 Nov 2017) -- End
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Assessor", mstrModuleName)
        Finally
        End Try
    End Sub
    'Shani(24-JAN-2017) -- End

    'Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
    '                                      ByVal sAssessorName As String, _
    '                                      ByVal sComments As String, _
    '                                      ByVal isApproved As Boolean, _
    '                                      ByVal sPeriodName As String, _
    '                                      ByVal intCompanyUnkId As Integer, _
    '                                      Optional ByVal iLoginTypeId As Integer = 0, _
    '                                      Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                      Optional ByVal iUserId As Integer = 0)
    '    'Sohail (30 Nov 2017) - [intCompanyUnkId]

    '    'Shani(24-JAN-2017) --ADD-- [ByVal sPeriodName As String]
    '    'REMOVE [ByVal intEmpUpdateTranUnkId As Integer, _]

    '    Dim objDataOperation As New clsDataOperation
    '    Dim objEmp As New clsEmployee_Master
    '    Dim objMail As New clsSendMail
    '    Try

    '        objEmp._Employeeunkid(Now.Date) = iEmployeeId

    '        'Gajanan [26-Feb-2019] -- Start
    '        'Enhancement - Email Language Changes For NMB.
    '        Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
    '        'Gajanan [26-Feb-2019] -- End

    '        If objEmp._Email.Trim.Length > 0 Then
    '            Dim strMessage As String = ""


    '            'Shani(24-JAN-2017) -- Start
    '            'Enhancement : 
    '            '    _Empupdatetranunkid = intEmpUpdateTranUnkId
    '            '    Dim intFiledMapId As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(mintPeriodunkid)
    '            '    Dim strBSCItem As String = ""

    '            '    Select Case intFiledMapId
    '            '        Case enWeight_Types.WEIGHT_FIELD1
    '            '            Dim objFiled1 As New clsassess_empfield1_master
    '            '            objFiled1._Empfield1unkid = mintEmpfieldunkid
    '            '            strBSCItem = objFiled1._Field_Data
    '            '            objFiled1 = Nothing
    '            '        Case enWeight_Types.WEIGHT_FIELD2
    '            '            Dim objFiled2 As New clsassess_empfield2_master
    '            '            objFiled2._Empfield2unkid = mintEmpfieldunkid
    '            '            strBSCItem = objFiled2._Field_Data
    '            '            objFiled2 = Nothing
    '            '        Case enWeight_Types.WEIGHT_FIELD3
    '            '            Dim objFiled3 As New clsassess_empfield3_master
    '            '            objFiled3._Empfield3unkid = mintEmpfieldunkid
    '            '            strBSCItem = objFiled3._Field_Data
    '            '            objFiled3 = Nothing
    '            '        Case enWeight_Types.WEIGHT_FIELD4
    '            '            Dim objFiled4 As New clsassess_empfield4_master
    '            '            objFiled4._Empfield4unkid = mintEmpfieldunkid
    '            '            strBSCItem = objFiled4._Field_Data
    '            '            objFiled4 = Nothing
    '            '        Case enWeight_Types.WEIGHT_FIELD5
    '            '            Dim objFiled5 As New clsassess_empfield5_master
    '            '            objFiled5._Empfield5unkid = mintEmpfieldunkid
    '            '            strBSCItem = objFiled5._Field_Data
    '            '            objFiled5 = Nothing
    '            '    End Select
    '            '    If isApproved = True Then
    '            '        objMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification for Approval of Goal Accomplishment Progress")
    '            '    Else
    '            '        objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Rejection of Goal Accomplishment Progress")
    '            '    End If


    '            '    strMessage = "<HTML> <BODY>"
    '            '    strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & objEmp._Firstname & " " & objEmp._Surname & ", <BR><BR>"

    '            '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "The status of ") & strBSCItem

    '            '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "updated on ") & mdtUpdatedate.ToShortDateString()

    '            '    strMessage &= Language.getMessage(mstrModuleName, 12, " has been ") & _
    '            '                  IIf(isApproved, Language.getMessage(mstrModuleName, 13, "Approved"), Language.getMessage(mstrModuleName, 14, "Rejected")) & _
    '            '                  Language.getMessage(mstrModuleName, 15, ". Please find the comment below for necessary action if applicable.") & "<BR><BR>" & _
    '            '                  sComments

    '            '    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '            '    strMessage &= "</BODY></HTML>"
    '            '    objMail._ToEmail = objEmp._Email
    '            '    objMail._Message = strMessage
    '            '    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '            '    If mstrWebFrmName.Trim.Length > 0 Then
    '            '        objMail._Form_Name = mstrWebFrmName
    '            '    End If
    '            '    objMail._LogEmployeeUnkid = iLoginEmployeeId
    '            '    objMail._OperationModeId = iLoginTypeId
    '            '    objMail._UserUnkid = iUserId
    '            '    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '            '    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '            '    objMail.SendMail()
    '            'End If


                '    If isApproved = True Then

    '                'Gajanan [26-Feb-2019] -- Start
    '                'Enhancement - Email Language Changes For NMB.
    '                'objMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification for Approval of Goal Accomplishment Progress")
    '                objMail._Subject = Language.getMessage(mstrModuleName, 27, "Notification for Approval of Progress Update")
    '                'Gajanan [26-Feb-2019] -- End
                '    Else
    '                'Gajanan [26-Feb-2019] -- Start
    '                'Enhancement - Email Language Changes For NMB.
    '                'objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Rejection of Goal Accomplishment Progress")
    '                objMail._Subject = Language.getMessage(mstrModuleName, 25, "Notification for Rejection of Progress Update")
    '                'Gajanan [26-Feb-2019] -- End
                '    End If


                '    strMessage = "<HTML> <BODY>"
    '            'Gajanan [26-Feb-2019] -- Start
    '            'Enhancement - Email Language Changes For NMB.

    '            'strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & objEmp._Firstname & " " & objEmp._Surname & ", <BR><BR>"
    '            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & ", <BR><BR>"



    '            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 17, "This to inform you that progress update has been ")

    '            'Pinkal (22-Mar-2019) -- Start
    '            'Enhancement - Working on PA Changes FOR NMB.
    '            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 20, "This to inform you that progress update has been ") & " "
    '            strMessage &= Language.getMessage(mstrModuleName, 20, "This to inform you that progress update has been ") & " "
    '            'Pinkal (22-Mar-2019) -- End


    '            'Gajanan [26-Feb-2019] -- End

    '            strMessage &= IIf(isApproved, Language.getMessage(mstrModuleName, 13, "Approved"), Language.getMessage(mstrModuleName, 14, "Rejected"))


    '            'Pinkal (22-Mar-2019) -- Start
    '            'Enhancement - Working on PA Changes FOR NMB.
    '            'strMessage &= " " & Language.getMessage(mstrModuleName, 18, "for the period ") & " " & sPeriodName & ", "
    '            'S.SANDEEP |2-APRIL-2019| -- START
    '            'strMessage &= " " & Language.getMessage(mstrModuleName, 18, "for the period ") & " (<b>" & sPeriodName & "</b>) ,"
    '            strMessage &= " " & Language.getMessage(mstrModuleName, 18, "for the period ") & " (<b>" & sPeriodName & "</b>)"
    '            'S.SANDEEP |2-APRIL-2019| -- END
    '            'Pinkal (22-Mar-2019) -- End


    '            'S.SANDEEP [29-NOV-2017] -- START
    '            'ISSUE/ENHANCEMENT : REF-ID # 35

    '            'Gajanan [26-Feb-2019] -- Start
    '            'Enhancement - Email Language Changes For NMB.
    '            'strMessage &= " " & Language.getMessage(mstrModuleName, 19, "by ") & sAssessorName & ", "
    '            'Gajanan [26-Feb-2019] -- End

    '            'S.SANDEEP [29-NOV-2017] -- END


    '            'Gajanan [26-Feb-2019] -- Start
    '            'Enhancement - Email Language Changes For NMB.


    '            'Pinkal (22-Mar-2019) -- Start
    '            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '            strMessage &= "<BR></BR>" & Language.getMessage(mstrModuleName, 29, "Line Manager Comment :") & " "
    '            'Pinkal (22-Mar-2019) -- End


    '            'strMessage &= Language.getMessage(mstrModuleName, 15, "Please find the comment below for necessary action if applicable.") & "<BR><BR> "

    '            'Pinkal (22-Mar-2019) -- Start
    '            'Enhancement - Working on PA Changes FOR NMB.

    '            'If isApproved = True Then
    '            '    strMessage &= "<BR>" & Language.getMessage(mstrModuleName, 28, "Approved:")
    '            'Else
    '            '    strMessage &= "<BR>" & Language.getMessage(mstrModuleName, 26, "Rejected:")
    '            'End If

    '            If isApproved = True Then
    '                strMessage &= Language.getMessage(mstrModuleName, 28, "Approved:")
    '            Else
    '                strMessage &= Language.getMessage(mstrModuleName, 26, "Rejected:")
    '            End If

    '            'Pinkal (22-Mar-2019) -- End


    '            strMessage &= sComments

    '            strMessage &= "<BR></BR>" & Language.getMessage(mstrModuleName, 24, "Regards,") & "<BR>"
    '            'S.SANDEEP |05-MAR-2019| -- START
    '            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '            'strMessage &= sAssessorName & "(" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & ")"
    '            strMessage &= info1.ToTitleCase(sAssessorName.ToLower())
    '            'S.SANDEEP |05-MAR-2019| -- END

    '            'Gajanan [26-Feb-2019] -- End

                '    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                '    strMessage &= "</BODY></HTML>"
                '    objMail._ToEmail = objEmp._Email
                '    objMail._Message = strMessage
                '    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                '    If mstrWebFrmName.Trim.Length > 0 Then
                '        objMail._Form_Name = mstrWebFrmName
                '    End If
                '    objMail._LogEmployeeUnkid = iLoginEmployeeId
                '    objMail._OperationModeId = iLoginTypeId
                '    objMail._UserUnkid = iUserId
                '    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                '    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'objMail.SendMail()
    '            objMail.SendMail(intCompanyUnkId)
    '            'Sohail (30 Nov 2017) -- End
    '        End If
    '        'Shani(24-JAN-2017) -- End

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
    '    End Try
    'End Sub

    Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
                                          ByVal sAssessorName As String, _
                                          ByVal sComments As String, _
                                          ByVal isApproved As Boolean, _
                                          ByVal sPeriodName As String, _
                                          ByVal intCompanyUnkId As Integer, _
                                          ByVal iListRGoals As List(Of String), _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0)

        Dim objDataOperation As New clsDataOperation
        Dim objEmp As New clsEmployee_Master
        Dim objMail As New clsSendMail
        Try

            objEmp._Employeeunkid(Now.Date) = iEmployeeId

            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo

            If objEmp._Email.Trim.Length > 0 Then
                Dim strMessage As String = ""

                If isApproved = True Then
                    objMail._Subject = Language.getMessage(mstrModuleName, 27, "Notification for Approval of Progress Update")
                Else
                    objMail._Subject = Language.getMessage(mstrModuleName, 25, "Notification for Rejection of Progress Update")
                End If


                strMessage = "<HTML> <BODY>"
                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & ", <BR><BR>"
                strMessage &= Language.getMessage(mstrModuleName, 20, "This to inform you that progress update has been ") & " "
                strMessage &= IIf(isApproved, Language.getMessage(mstrModuleName, 13, "Approved"), Language.getMessage(mstrModuleName, 14, "Rejected"))
                strMessage &= " " & Language.getMessage(mstrModuleName, 18, "for the period ") & " (<b>" & sPeriodName & "</b>)"

                strMessage &= "<BR></BR>" & Language.getMessage(mstrModuleName, 29, "Line Manager Comment :") & " "
                If isApproved = True Then                    
                    strMessage &= Language.getMessage(mstrModuleName, 28, "Approved:")
                    strMessage &= sComments
                Else
                    strMessage &= "<BR></BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 26, "Rejected:")
                    strMessage &= sComments

                    strMessage &= "<BR></BR>"
                    strMessage &= "<TABLE style='margin:0px' BORDER=1 style='width: 60%;'>" & vbCrLf
                    strMessage &= "<TR style='width: 100%'>" & vbCrLf
                    strMessage &= "<TD BGCOLOR='Silver' style='width: 10%'><b>" & Language.getMessage(mstrModuleName, 200, "Sr.No") & "</b></TD>" & vbCrLf
                    strMessage &= "<TD BGCOLOR='Silver' style='width: 50%'><b>" & Language.getMessage(mstrModuleName, 201, "Goal(s) with Rejected Progress Update") & "</b></TD>" & vbCrLf
                    strMessage &= "</TR>" & vbCrLf
                    Dim iRow As Integer = 1
                    For Each iGoal As String In iListRGoals
                        strMessage &= "<TR style='width: 100%'>" & vbCrLf
                        strMessage &= "<TD style='width: 10%'>" & iRow.ToString() & "</TD>" & vbCrLf
                        strMessage &= "<TD style='width: 50%'>" & iGoal & "</TD>" & vbCrLf
                        strMessage &= "</TR>" & vbCrLf
                        iRow = iRow + 1
                    Next
                    strMessage &= "</TABLE>" & vbCrLf
                End If


                strMessage &= "<BR></BR>" & Language.getMessage(mstrModuleName, 24, "Regards,") & "<BR>"
                strMessage &= info1.ToTitleCase(sAssessorName.ToLower())
                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"
                objMail._ToEmail = objEmp._Email
                objMail._Message = strMessage
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = iUserId
                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                With objMail
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objMail.SendMail()
                objMail.SendMail(intCompanyUnkId)
                'Sohail (30 Nov 2017) -- End
            End If
            'Shani(24-JAN-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
        End Try
    End Sub

    'Shani (26-Sep-2016) -- End

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 136
    Public Function GetListAttachedAssociatedValue(ByVal intTransactionId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim objDataOperation As New clsDataOperation
        Dim StrQ As String = String.Empty
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "     hrassess_empupdate_tran.empupdatetranunkid as avalueid " & _
                   "    ,CASE WHEN hrassess_empupdate_tran.fieldunkid = 1 THEN hrassess_empfield1_master.field_data " & _
                   "          WHEN hrassess_empupdate_tran.fieldunkid = 2 THEN hrassess_empfield2_master.field_data " & _
                   "          WHEN hrassess_empupdate_tran.fieldunkid = 3 THEN hrassess_empfield3_master.field_data " & _
                   "          WHEN hrassess_empupdate_tran.fieldunkid = 4 THEN hrassess_empfield4_master.field_data " & _
                   "          WHEN hrassess_empupdate_tran.fieldunkid = 5 THEN hrassess_empfield5_master.field_data " & _
                   "     END AS avalue " & _
                   "FROM hrassess_empupdate_tran " & _
                   "    LEFT JOIN hrassess_empfield1_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empupdate_tran.empfieldunkid and hrassess_empfield1_master.isvoid = 0 " & _
                   "    LEFT JOIN hrassess_empfield2_master ON hrassess_empfield2_master.empfield2unkid = hrassess_empupdate_tran.empfieldunkid and hrassess_empfield2_master.isvoid = 0 " & _
                   "    LEFT JOIN hrassess_empfield3_master ON hrassess_empfield3_master.empfield3unkid = hrassess_empupdate_tran.empfieldunkid and hrassess_empfield3_master.isvoid = 0 " & _
                   "    LEFT JOIN hrassess_empfield4_master ON hrassess_empfield4_master.empfield4unkid = hrassess_empupdate_tran.empfieldunkid and hrassess_empfield4_master.isvoid = 0 " & _
                   "    LEFT JOIN hrassess_empfield5_master ON hrassess_empfield5_master.empfield5unkid = hrassess_empupdate_tran.empfieldunkid and hrassess_empfield5_master.isvoid = 0 " & _
                   "WHERE hrassess_empupdate_tran.isvoid = 0 and hrassess_empupdate_tran.empupdatetranunkid = @empupdatetranunkid "

            objDataOperation.AddParameter("@empupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListAttachedAssociatedValue; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP [29-NOV-2017] -- END

    'S.SANDEEP |24-APR-2020| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
    Public Function GetCalculationMode(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                If blnAddSelect Then
                    StrQ = "SELECT 0 AS Id, @Select AS Name UNION ALL"
                End If
                StrQ &= "SELECT " & enCalcMode.Increasing & " AS Id, @Inc AS Name " & _
                        "UNION ALL SELECT " & enCalcMode.Decreasing & " AS Id, @Dec AS Name "

                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, Language.getMessage(mstrModuleName, 33, "Select"))
                objDo.AddParameter("@Inc", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Increasing"))
                objDo.AddParameter("@Dec", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Decreasing"))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                End If
            End Using
            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |24-APR-2020| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 579, "Pending")
            Language.setMessage("clsMasterData", 580, "In progress")
            Language.setMessage("clsMasterData", 581, "Complete")
            Language.setMessage("clsMasterData", 582, "Closed")
            Language.setMessage("clsMasterData", 583, "On Track")
            Language.setMessage("clsMasterData", 584, "At Risk")
            Language.setMessage("clsMasterData", 585, "Not Applicable")
            Language.setMessage("clsMasterData", 748, "Postponed")
            Language.setMessage("clsMasterData", 749, "Behind the Schedule")
            Language.setMessage("clsMasterData", 750, "Ahead of Schedule")
            Language.setMessage("clsMasterData", 759, "Pending")
            Language.setMessage("clsMasterData", 760, "Approved")
            Language.setMessage("clsMasterData", 761, "Rejected")
            Language.setMessage("frmUpdateFieldValue", 101, "Percentage")
            Language.setMessage("frmUpdateFieldValue", 102, "Value")
			Language.setMessage(mstrModuleName, 3, "Sorry, Combination of status and percentage is already defined.")
			Language.setMessage(mstrModuleName, 5, "Dear")
			Language.setMessage(mstrModuleName, 13, "Approved")
			Language.setMessage(mstrModuleName, 14, "Rejected")
			Language.setMessage(mstrModuleName, 18, "for the period")
			Language.setMessage(mstrModuleName, 20, "This to inform you that progress update has been")
			Language.setMessage(mstrModuleName, 21, "Notification for Reviewing Progress check.")
			Language.setMessage(mstrModuleName, 23, "Please click the link below to review.")
			Language.setMessage(mstrModuleName, 24, "Regards,")
			Language.setMessage(mstrModuleName, 25, "Notification for Rejection of Progress Update")
			Language.setMessage(mstrModuleName, 26, "Rejected:")
			Language.setMessage(mstrModuleName, 27, "Notification for Approval of Progress Update")
			Language.setMessage(mstrModuleName, 28, "Approved:")
			Language.setMessage(mstrModuleName, 29, "Line Manager Comment :")
			Language.setMessage(mstrModuleName, 30, "Please note that, I have submitted my objective progress updates for assessment period")
            Language.setMessage(mstrModuleName, 31, "Increasing")
            Language.setMessage(mstrModuleName, 32, "Decreasing")
            Language.setMessage(mstrModuleName, 33, "Select")
            Language.setMessage(mstrModuleName, 200, "Sr.No")
            Language.setMessage(mstrModuleName, 201, "Goal(s) with Rejected Progress Update")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
