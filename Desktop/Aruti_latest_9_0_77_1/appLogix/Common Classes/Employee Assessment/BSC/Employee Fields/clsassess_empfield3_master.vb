﻿'************************************************************************************************************************************
'Class Name : clsassess_empfield3_master.vb
'Purpose    :
'Date       :17-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_empfield3_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_empfield3_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmpfield3unkid As Integer = 0
    Private mintEmpfield2unkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mdblWeight As Double = 0
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintEmpFieldTypeId As Integer = 0
    Private mDecPct_Completed As Decimal = 0
    Private mintPeriodunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mstrWebFrmName As String = String.Empty
    Private dtOldValue As DataTable = Nothing
    Private mintLoginemployeeunkid As Integer = 0
    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Private mintGoalTypeid As Integer = CInt(enGoalType.GT_QUALITATIVE)
    Private mdblGoalValue As Decimal = 0
    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mintUnitOfMeasure As Integer = 0
    'S.SANDEEP |12-FEB-2019| -- END

#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empfield3unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Empfield3unkid() As Integer
        Get
            Return mintEmpfield3unkid
        End Get
        Set(ByVal value As Integer)
            mintEmpfield3unkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empfield2unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Empfield2unkid() As Integer
        Get
            Return mintEmpfield2unkid
        End Get
        Set(ByVal value As Integer)
            mintEmpfield2unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_completed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Pct_Completed() As Decimal
        Get
            Return mDecPct_Completed
        End Get
        Set(ByVal value As Decimal)
            mDecPct_Completed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set EmpFieldTypeId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _EmpFieldTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmpFieldTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set WebFormName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set loginemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Loginemployeeunkid() As Integer
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Public Property _GoalTypeid() As Integer
        Get
            Return mintGoalTypeid
        End Get
        Set(ByVal value As Integer)
            mintGoalTypeid = value
        End Set
    End Property

    Public Property _GoalValue() As Decimal
        Get
            Return mdblGoalValue
        End Get
        Set(ByVal value As Decimal)
            mdblGoalValue = value
        End Set
    End Property
    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Public Property _UnitOfMeasure() As Integer
        Get
            Return mintUnitOfMeasure
        End Get
        Set(ByVal value As Integer)
            mintUnitOfMeasure = value
        End Set
    End Property
    'S.SANDEEP |12-FEB-2019| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal iDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If iDataOpr IsNot Nothing Then
            objDataOperation = iDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
              "  empfield3unkid " & _
              ", empfield2unkid " & _
              ", employeeunkid " & _
              ", periodunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", weight " & _
              ", pct_completed " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
              ", ISNULL(goalvalue,0) AS goalvalue " & _
              ", ISNULL(uomtypeid,0) AS uomtypeid " & _
             "FROM hrassess_empfield3_master " & _
             "WHERE empfield3unkid = @empfield3unkid "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfield3unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtOldValue Is Nothing Then
                dtOldValue = dsList.Tables(0).Copy
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpfield3unkid = CInt(dtRow.Item("empfield3unkid"))
                mintEmpfield2unkid = CInt(dtRow.Item("empfield2unkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mstrField_Data = dtRow.Item("field_data").ToString
                mdblWeight = CDbl(dtRow.Item("weight"))
                mDecPct_Completed = CDec(dtRow.Item("pct_completed"))
                If IsDBNull(dtRow.Item("startdate")) = False Then
                    mdtStartdate = dtRow.Item("startdate")
                Else
                    mdtStartdate = Nothing
                End If
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                mintGoalTypeid = CInt(dtRow("goaltypeid"))
                mdblGoalValue = CDec(dtRow("goalvalue"))
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                mintUnitOfMeasure = CInt(dtRow("uomtypeid"))
                'S.SANDEEP |12-FEB-2019| -- END

                Exit For
            Next

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dsUpdate As DataSet
            Dim objProgress As New clsassess_empupdate_tran
            objProgress._DataOperation = objDataOperation
            dsUpdate = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime, mintEmployeeunkid, mintPeriodunkid)
            If dsUpdate IsNot Nothing Then
                Dim pRow() As DataRow = dsUpdate.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "' AND empfieldunkid = '" & mintEmpfield3unkid & "'")
                If pRow.Length > 0 Then
                    mDecPct_Completed = pRow(0).Item("pct_completed")
                    mintStatusunkid = pRow(0).Item("statusunkid")
                End If
            End If
            objProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empfield3unkid " & _
              ", empfield2unkid " & _
              ", employeeunkid " & _
              ", periodunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", weight " & _
              ", pct_completed " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
              ", ISNULL(goalvalue,0) AS goalvalue " & _
              ", ISNULL(uomtypeid,0) AS uomtypeid " & _
             "FROM hrassess_empfield3_master "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_empfield3_master) </purpose>
    Public Function Insert(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, , mintEmpfield2unkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfield2unkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@goaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalTypeid.ToString)
            objDataOperation.AddParameter("@goalvalue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGoalValue)
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objDataOperation.AddParameter("@uomtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitOfMeasure)
            'S.SANDEEP |12-FEB-2019| -- END

            strQ = "INSERT INTO hrassess_empfield3_master ( " & _
                       "  empfield2unkid " & _
                       ", employeeunkid " & _
                       ", periodunkid " & _
                       ", fieldunkid " & _
                       ", field_data " & _
                       ", weight " & _
                       ", pct_completed " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", statusunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime " & _
                       ", goaltypeid " & _
                       ", goalvalue " & _
                       ", uomtypeid " & _
                   ") VALUES (" & _
                       "  @empfield2unkid " & _
                       ", @employeeunkid " & _
                       ", @periodunkid " & _
                       ", @fieldunkid " & _
                       ", @field_data " & _
                       ", @weight " & _
                       ", @pct_completed " & _
                       ", @startdate " & _
                       ", @enddate " & _
                       ", @statusunkid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                       ", @goaltypeid " & _
                       ", @goalvalue " & _
                       ", @uomtypeid " & _
                   "); SELECT @@identity"
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpfield3unkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_empfield3_master", "empfield3unkid", mintEmpfield3unkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_empowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintEmpfield3unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_empinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintEmpfield3unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_empfield3_master) </purpose>
    Public Function Update(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, mintEmpfield3unkid, mintEmpfield2unkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfield3unkid.ToString)
            objDataOperation.AddParameter("@empfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfield2unkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@goaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalTypeid.ToString)
            objDataOperation.AddParameter("@goalvalue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGoalValue)
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objDataOperation.AddParameter("@uomtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitOfMeasure)
            'S.SANDEEP |12-FEB-2019| -- END

            strQ = "UPDATE hrassess_empfield3_master SET " & _
                   "  empfield2unkid = @empfield2unkid" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", periodunkid = @periodunkid" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", field_data = @field_data" & _
                   ", weight = @weight" & _
                   ", pct_completed = @pct_completed" & _
                   ", startdate = @startdate" & _
                   ", enddate = @enddate" & _
                   ", statusunkid = @statusunkid" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   ", goaltypeid = @goaltypeid " & _
                   ", goalvalue = @goalvalue " & _
                   ", uomtypeid = @uomtypeid " & _
                   "WHERE empfield3unkid = @empfield3unkid "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_empfield3_master", mintEmpfield3unkid, "empfield3unkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_empfield3_master", "empfield3unkid", mintEmpfield3unkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'S.SANDEEP |11-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Object Reference Error}
            'If Periodic_Review(enAuditType.EDIT, objDataOperation, dtOldValue) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If dtOldValue IsNot Nothing Then
            If Periodic_Review(enAuditType.EDIT, objDataOperation, dtOldValue) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            End If
            'S.SANDEEP |11-MAY-2019| -- END
            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_empowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintEmpfield3unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_empinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintEmpfield3unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            '==================|START  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            Dim dsChild As New DataSet : Dim dtmp As DataRow()
            If objCommonATLog Is Nothing Then objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empupdate_tran", "empfieldunkid", mintEmpfield3unkid)
            dtmp = dsChild.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    Dim objProgress As New clsassess_empupdate_tran
                    objProgress._DataOperation = objDataOperation
                    objProgress._Isvoid = True
                    objProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objProgress._Voidreason = Language.getMessage("clsassess_empfield1_master", 999, "Voided due to update")
                    objProgress._Voiduserunkid = IIf(mintLoginemployeeunkid <= 0, mintUserunkid, mintLoginemployeeunkid)
                    If objProgress.Delete(dr.Item("empupdatetranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objProgress = Nothing
                Next
            End If
            '==================|ENDING  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            'S.SANDEEP |09-JUL-2019| -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_empfield3_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal iDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmpField4 As New clsassess_empfield4_master

        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = iDataOpr
        End If

        mstrMessage = "" : objDataOperation.ClearParameters()
        If isUsed(intUnkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Return False
        End If

        Try
            strQ = "UPDATE hrassess_empfield3_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE empfield3unkid = @empfield3unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_empfield3_master", "empfield3unkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Dim dsChild As New DataSet : Dim dtmp As DataRow() = Nothing
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empfield4_master", "empfield3unkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("isvoid = 0")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    objEmpField4._Isvoid = mblnIsvoid
                    objEmpField4._Voiddatetime = mdtVoiddatetime
                    objEmpField4._Voidreason = mstrVoidreason
                    objEmpField4._Voiduserunkid = mintVoiduserunkid

                    objEmpField4._FormName = mstrFormName
                    objEmpField4._Loginemployeeunkid = mintLoginemployeeunkid
                    objEmpField4._ClientIP = mstrClientIP
                    objEmpField4._HostName = mstrHostName
                    objEmpField4._FromWeb = mblnIsWeb
                    objEmpField4._AuditUserId = mintAuditUserId
objEmpField4._CompanyUnkid = mintCompanyUnkid
                    objEmpField4._AuditDate = mdtAuditDate

                    If objEmpField4.Delete(dr.Item("empfield4unkid"), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empinfofield_tran", "empfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_empinfofield_tran", "empinfofieldunkid", dr.Item("empinfofieldunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
                strQ = "DELETE FROM hrassess_empinfofield_tran WHERE empfieldunkid = '" & intUnkid & "' AND empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empupdate_tran", "empfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    Dim objProgress As New clsassess_empupdate_tran

                    objProgress._DataOperation = objDataOperation
                    objProgress._Isvoid = mblnIsvoid
                    objProgress._Voiddatetime = mdtVoiddatetime
                    objProgress._Voidreason = mstrVoidreason
                    objProgress._Voiduserunkid = mintVoiduserunkid

                    objProgress._FormName = mstrFormName
                    objProgress._LoginEmployeeunkid = mintLoginemployeeunkid
                    objProgress._ClientIP = mstrClientIP
                    objProgress._HostName = mstrHostName
                    objProgress._FromWeb = mblnIsWeb
                    objProgress._AuditUserId = mintAuditUserId
objProgress._CompanyUnkid = mintCompanyUnkid
                    objProgress._AuditDate = mdtAuditDate

                    If objProgress.Delete(dr.Item("empupdatetranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objProgress = Nothing
                Next
            End If
            '==================|ENDING  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            'S.SANDEEP [16 JUN 2015] -- END

            mintEmpfield3unkid = intUnkid
            Call GetData(objDataOperation)

            'S.SANDEEP |11-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Object Reference Error}
            'If Periodic_Review(enAuditType.DELETE, objDataOperation, dtOldValue) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If dtOldValue IsNot Nothing Then
            If Periodic_Review(enAuditType.DELETE, objDataOperation, dtOldValue) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If
            'S.SANDEEP |11-MAY-2019| -- END
            

            If iDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal objDataOpr As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOpr.ClearParameters()
        Try
            strQ = "SELECT 1 FROM hrgoals_analysis_tran WITH (NOLOCK) WHERE empfield3unkid = @empfield3unkid AND isvoid = 0"

            objDataOpr.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOpr.ExecQuery(strQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal iEmpField2Unkid As Integer = 0, Optional ByVal iPeriodId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_empfield3_master.empfield3unkid " & _
                   ", hrassess_empfield3_master.empfield2unkid " & _
                   ", hrassess_empfield3_master.fieldunkid " & _
                   ", hrassess_empfield3_master.field_data " & _
                   ", hrassess_empfield3_master.weight " & _
                   ", hrassess_empfield3_master.pct_completed " & _
                   ", hrassess_empfield3_master.startdate " & _
                   ", hrassess_empfield3_master.enddate " & _
                   ", hrassess_empfield3_master.statusunkid " & _
                   ", hrassess_empfield3_master.userunkid " & _
                   ", hrassess_empfield3_master.isvoid " & _
                   ", hrassess_empfield3_master.voiduserunkid " & _
                   ", hrassess_empfield3_master.voidreason " & _
                   ", hrassess_empfield3_master.voiddatetime " & _
                   ", ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
                   ", ISNULL(hrassess_empfield3_master.goalvalue,0) AS goalvalue " & _
                   "FROM hrassess_empfield3_master WITH (NOLOCK) " & _
                   " LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid  = hrassess_empfield2_master.empfield2unkid " & _
                   " LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid  = hrassess_empfield2_master.empfield1unkid "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END

            If iPeriodId > 0 Then
                strQ &= "   AND hrassess_empfield1_master.periodunkid = '" & iPeriodId & "' "
            End If

            strQ &= "WHERE hrassess_empfield3_master.field_data = @field_data AND hrassess_empfield3_master.isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND empfield3unkid <> @empfield3unkid"
            End If

            If iEmpField2Unkid > 0 Then
                strQ &= "   AND hrassess_empfield3_master.empfield2unkid = '" & iEmpField2Unkid & "' "
            End If


            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, strName.Length, strName)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get Data Set for Combo </purpose>sss
    Public Function getComboList(ByVal iEmpId As Integer, ByVal iPeriodId As Integer, Optional ByVal iParentId As Integer = 0, Optional ByVal iList As String = "List", _
                                 Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT empfield3unkid AS Id, field_data AS Name FROM hrassess_empfield3_master WHERE isvoid = 0 " & _
                    "AND employeeunkid = '" & iEmpId & "' AND periodunkid = '" & iPeriodId & "' "
            If iParentId > 0 Then
                StrQ &= "AND hrassess_empfield3_master.empfield2unkid = '" & iParentId & "' "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
    End Function

    Public Function Get_EmployeeField3Unkid(ByVal iFieldData As String, ByVal iEmployeeId As Integer, ByVal iPeriodId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  empfield3unkid " & _
                   "FROM hrassess_empfield3_master " & _
                   "    JOIN hrassess_empfield2_master ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid " & _
                   "    JOIN hrassess_empfield1_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid " & _
                   "WHERE hrassess_empfield3_master.field_data = @field_data AND hrassess_empfield3_master.isvoid = 0 " & _
                   "    AND hrassess_empfield1_master.employeeunkid = @employeeunkid AND hrassess_empfield1_master.periodunkid = @periodunkid "

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, iFieldData)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("empfield3unkid")
            Else
                Return 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_EmployeeField3Unkid", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function Periodic_Review(ByVal eAudit As enAuditType, ByVal objDataOperation As clsDataOperation, ByVal xDataTable As DataTable) As Boolean
        Try
            If xDataTable.Rows.Count <= 0 Then Return True

            Dim xLastStatusId As Integer = -1
            Dim objEStatusTran As New clsassess_empstatus_tran
            Dim xStatusTranId As Integer = -1
            xLastStatusId = objEStatusTran.Get_Last_StatusId(mintEmployeeunkid, mintPeriodunkid, xStatusTranId, objDataOperation)
            If xLastStatusId = enObjective_Status.PERIODIC_REVIEW Then
                Dim objPriodicReview As New clsassess_periodic_review
                If IsDBNull(xDataTable.Rows(0).Item("enddate")) = True Then
                    objPriodicReview._Enddate = Nothing
                Else
                    objPriodicReview._Enddate = xDataTable.Rows(0).Item("enddate")
                End If
                objPriodicReview._Field_Data = xDataTable.Rows(0).Item("field_data")
                objPriodicReview._Fieldtranunkid = xDataTable.Rows(0).Item("empfield3unkid")
                objPriodicReview._Fieldtypeid = enWeight_Types.WEIGHT_FIELD3
                objPriodicReview._Fieldunkid = xDataTable.Rows(0).Item("fieldunkid")
                objPriodicReview._Isfinal = False
                objPriodicReview._Loginemployeeunkid = mintLoginemployeeunkid
                objPriodicReview._Owrfield1unkid = 0
                objPriodicReview._Pct_Complete = xDataTable.Rows(0).Item("pct_completed")
                objPriodicReview._Periodunkid = xDataTable.Rows(0).Item("periodunkid")
                objPriodicReview._Perspectiveunkid = 0
                objPriodicReview._Reviewdate = ConfigParameter._Object._CurrentDateAndTime
                objPriodicReview._Reviewtypeid = eAudit
                If IsDBNull(xDataTable.Rows(0).Item("startdate")) = True Then
                    objPriodicReview._Startdate = Nothing
                Else
                    objPriodicReview._Startdate = xDataTable.Rows(0).Item("startdate")
                End If
                objPriodicReview._Statusunkid = xDataTable.Rows(0).Item("statusunkid")
                objPriodicReview._Userunkid = xDataTable.Rows(0).Item("userunkid")
                objPriodicReview._FormName = mstrFormName
                objPriodicReview._Loginemployeeunkid = mintLoginemployeeunkid
                objPriodicReview._ClientIP = mstrClientIP
                objPriodicReview._HostName = mstrHostName
                objPriodicReview._FromWeb = mblnIsWeb
                objPriodicReview._AuditUserId = mintAuditUserId
objPriodicReview._CompanyUnkid = mintCompanyUnkid
                objPriodicReview._AuditDate = mdtAuditDate
                objPriodicReview._Weight = xDataTable.Rows(0).Item("weight")
                objPriodicReview._Employeeunkid = xDataTable.Rows(0).Item("employeeunkid")
                objPriodicReview._Statustranunkid = xStatusTranId
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                objPriodicReview._GoalTypeid = xDataTable.Rows(0).Item("goaltypeid")
                objPriodicReview._GoalValue = xDataTable.Rows(0).Item("goalvalue")
                'S.SANDEEP [01-OCT-2018] -- END
                If objPriodicReview.Insert(objDataOperation) = False Then
                    Return False
                End If
                objPriodicReview = Nothing
            End If
            objEStatusTran = Nothing
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Periodic_Review", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 10 JAN 2015 ] -- START
    Public Function GetListForTansfer(ByVal xPeriodId As Integer, ByVal xEmployeeId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     empfield3unkid " & _
                       "    ,empfield2unkid " & _
                       "    ,employeeunkid " & _
                       "    ,periodunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,weight " & _
                       "    ,pct_completed " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,userunkid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "    ,ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
                       "    ,ISNULL(goalvalue,0) AS goalvalue " & _
                       "FROM hrassess_empfield3_master " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' AND employeeunkid = '" & xEmployeeId & "' ORDER BY empfield3unkid "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetListForTansfer", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetEmpFieldUnkid(ByVal xFieldData As String, ByVal xPeriodId As Integer, ByVal xEmployeeUnkid As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim xUnkid As Integer = -1
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT @xUnkid = empfield3unkid FROM hrassess_empfield3_master WHERE isvoid = 0 " & _
                       " AND periodunkid = @xPeriodId AND field_data = @xFieldData AND employeeunkid = @xEmployeeUnkid "

                objDo.AddParameter("@xUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid, ParameterDirection.InputOutput)
                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFieldData", SqlDbType.NVarChar, xFieldData.Length, xFieldData)
                objDo.AddParameter("@xEmployeeUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeUnkid)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xUnkid = objDo.GetParameterValue("@xUnkid")

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmpFieldUnkid", mstrModuleName)
        Finally
        End Try
        Return xUnkid
    End Function
    'S.SANDEEP [ 10 JAN 2015 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage("clsassess_empfield1_master", 999, "Voided due to update")
			Language.setMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

