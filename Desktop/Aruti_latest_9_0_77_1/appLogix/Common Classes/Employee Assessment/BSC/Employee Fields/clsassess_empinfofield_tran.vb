﻿'************************************************************************************************************************************
'Class Name :clsassess_empinfofield_tran.vb
'Purpose    :
'Date       :14-MAY-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_empinfofield_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsassess_empinfofield_tran"
    Private mstrMessage As String = ""
    Private mintEmpInfoFieldtranunkid As Integer
    Private mdicInfoField As Dictionary(Of Integer, String)

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _dicInfoField() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicInfoField = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function Get_Data(ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer) As Dictionary(Of Integer, String)
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdicInfoField As New Dictionary(Of Integer, String)
        Try
            StrQ = "SELECT " & _
                   "  empinfofieldunkid " & _
                   ", empfieldunkid " & _
                   ", fieldunkid " & _
                   ", field_data " & _
                   ", empfieldtypeid " & _
                   "FROM hrassess_empinfofield_tran WITH (NOLOCK) " & _
                   "WHERE empfieldunkid = @empfieldunkid AND empfieldtypeid = @empfieldtypeid "

            objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
            objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If mdicInfoField.ContainsKey(dRow.Item("fieldunkid")) = False Then
                        mdicInfoField.Add(dRow.Item("fieldunkid"), dRow.Item("field_data"))
                    End If
                Next
            End If

            Return mdicInfoField
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function InsertDelete_InfoField(ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer, ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer) As Boolean
        Dim StrQI, StrQU, StrQ As String
        Dim exForce As Exception
        Dim iEmpInfoFieldId As Integer = 0
        Try
            StrQI = "INSERT INTO hrassess_empinfofield_tran ( " & _
                        "  empfieldunkid " & _
                        ", fieldunkid " & _
                        ", field_data " & _
                        ", empfieldtypeid" & _
                    ") VALUES (" & _
                        "  @empfieldunkid " & _
                        ", @fieldunkid " & _
                        ", @field_data " & _
                        ", @empfieldtypeid" & _
                    "); SELECT @@identity"

            StrQU = "UPDATE hrassess_empinfofield_tran SET " & _
                        "  empfieldunkid = @empfieldunkid" & _
                        ", fieldunkid = @fieldunkid" & _
                        ", field_data = @field_data" & _
                        ", empfieldtypeid = @empfieldtypeid " & _
                    "WHERE empinfofieldunkid = @empinfofieldunkid "

            For Each iKey As Integer In mdicInfoField.Keys
                objDataOpr.ClearParameters()
                StrQ = "SELECT empinfofieldunkid FROM hrassess_empinfofield_tran WITH (NOLOCK) WHERE empfieldunkid = '" & iEmpFieldUnkid & "' AND empfieldtypeid = '" & iEmpFieldTypeId & "' AND fieldunkid = '" & iKey & "' "

                Dim dsList As New DataSet
                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iEmpInfoFieldId = dsList.Tables(0).Rows(0).Item("empinfofieldunkid")
                End If

                If iEmpInfoFieldId > 0 Then
                    objDataOpr.AddParameter("@empinfofieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpInfoFieldId.ToString)
                    objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempFieldTypeId.ToString)

                    Call objDataOpr.ExecNonQuery(StrQU)

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_empinfofield_tran", iEmpInfoFieldId, "empinfofieldunkid", 2, objDataOpr) Then
                        If objCommonATLog.Insert_AtLog(objDataOpr, 2, "hrassess_empinfofield_tran", "empinfofieldunkid", iEmpInfoFieldId, , iUserId) = False Then
                            exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Else
                    objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempFieldTypeId.ToString)

                    dsList = objDataOpr.ExecQuery(StrQI, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    mintEmpInfoFieldtranunkid = dsList.Tables(0).Rows(0).Item(0)

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOpr, 1, "hrassess_empinfofield_tran", "empinfofieldunkid", mintEmpInfoFieldtranunkid, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                End If
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDelete_InfoField", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class