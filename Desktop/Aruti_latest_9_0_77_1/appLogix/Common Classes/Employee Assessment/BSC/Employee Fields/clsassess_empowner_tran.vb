﻿'************************************************************************************************************************************
'Class Name : clsassess_empowner_tran.vb
'Purpose    :
'Date       :28-Jul-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_empowner_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsassess_empowner_tran"
    Private mstrMessage As String = ""
    Private mintEmpOwnertranunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _DatTable() As DataTable
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function Get_Data(ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "  ownertranunkid " & _
                   ", empfieldunkid " & _
                   ", employeeunkid " & _
                   ", empfieldtypeid " & _
                   ", '' AS AUD " & _
                   ", '' AS GUID " & _
                   "FROM hrassess_empowner_tran WITH (NOLOCK) " & _
                   "WHERE empfieldunkid = @empfieldunkid AND empfieldtypeid = @empfieldtypeid "

            objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
            objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function GlobalAssign_Owner(ByVal iUserId As Integer, ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer) As Boolean
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            If InsertDelete_Owners(objDataOperation, iUserId, iEmpFieldUnkid, iEmpFieldTypeId, True) = False Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GlobalAssign_Owner", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function InsertDelete_Owners(ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer, ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer, Optional ByVal blnCheckIsExist As Boolean = False) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            For i As Integer = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOpr.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                If blnCheckIsExist = True Then
                                    Dim iCnt As Integer = -1
                                    iCnt = objDataOpr.RecordCount("SELECT ownertranunkid FROM hrassess_empowner_tran WITH (NOLOCK) WHERE empfieldunkid = '" & iEmpFieldUnkid & "' " & _
                                                                  "AND employeeunkid = '" & .Item("employeeunkid") & "' AND empfieldtypeid = '" & iEmpFieldTypeId & "'")

                                    If objDataOpr.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If iCnt > 0 Then Continue For

                                End If

                                StrQ = "INSERT INTO hrassess_empowner_tran ( " & _
                                           "  empfieldunkid " & _
                                           ", employeeunkid " & _
                                           ", empfieldtypeid" & _
                                       ") VALUES (" & _
                                           "  @empfieldunkid " & _
                                           ", @employeeunkid " & _
                                           ", @empfieldtypeid" & _
                                       "); SELECT @@identity"

                                objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid)
                                objDataOpr.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldTypeId)

                                Dim dsList As New DataSet
                                dsList = objDataOpr.ExecQuery(StrQ, "List")

                                If objDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                mintEmpOwnertranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOpr, 1, "hrassess_empowner_tran", "ownertranunkid", mintEmpOwnertranunkid, , iUserId) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Case "D"
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOpr, 3, "hrassess_empowner_tran", "ownertranunkid", .Item("ownertranunkid"), , iUserId) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                                StrQ = "DELETE FROM hrassess_empowner_tran WHERE ownertranunkid = @ownertranunkid "

                                objDataOpr.AddParameter("@ownertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ownertranunkid"))

                                Call objDataOpr.ExecNonQuery(StrQ)

                                If objDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDelete_Owners", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetCSV_OwnerName(ByVal iEmpFieldId As Integer, ByVal iEmpFieldTypeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwners As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT ISNULL(STUFF((SELECT ',' + " & _
                   "    CAST((ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'')) AS NVARCHAR(MAX)) " & _
                   "FROM hrassess_empowner_tran WITH (NOLOCK) " & _
                   " JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassess_empowner_tran.employeeunkid " & _
                   "WHERE empfieldunkid = '" & iEmpFieldId & "' AND empfieldtypeid = '" & iEmpFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iOwners = dsList.Tables(0).Rows(0).Item("CSV")
            End If

            Return iOwners
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_OwnerName", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function GetCSV_OwnerIds(ByVal iEmpFieldId As Integer, ByVal iEmpFieldTypeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iOwnerIds As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try

            StrQ = "SELECT ISNULL(STUFF((SELECT ',' + " & _
                   "    CAST(employeeunkid AS NVARCHAR(MAX)) " & _
                   "FROM hrassess_empowner_tran WITH (NOLOCK) " & _
                   "WHERE empfieldunkid = '" & iEmpFieldId & "' AND empfieldtypeid = '" & iEmpFieldTypeId & "' FOR XML PATH('')),1,1,''),'') AS CSV "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iOwnerIds = dsList.Tables(0).Rows(0).Item("CSV")
            End If

            Return iOwnerIds
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_OwnerIds", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function Get_AllOwner(ByVal iEmpFieldTypeId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "  ownertranunkid " & _
                   ", empfieldunkid " & _
                   ", emplyoeeunkid " & _
                   ", empfieldtypeid " & _
                   "FROM hrassess_empowner_tran WITH (NOLOCK) " & _
                   "WHERE empfieldtypeid = @empfieldtypeid "

            objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_AllOwner", mstrModuleName)
            Return Nothing
        Finally
        End Try
        Return dsList
    End Function

#End Region

End Class
