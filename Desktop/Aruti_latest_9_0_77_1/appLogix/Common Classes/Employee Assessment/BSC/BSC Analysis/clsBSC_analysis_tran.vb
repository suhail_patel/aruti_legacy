﻿'************************************************************************************************************************************
'Class Name : clsassess_analysis_tran.vb
'Purpose    :
'Date       :13/01/2012
'Written By :Sandeep J. Sharma
'Modified   : ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsBSC_analysis_tran
    Private Shared ReadOnly mstrModuleName As String = "clsBSC_analysis_tran"
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintAnalysisTranId As Integer = -1
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mdicGroupTotal As New Dictionary(Of Integer, Decimal)
    Private mdicItemWeight As New Dictionary(Of Integer, Decimal)
    Private mintGrpCounter As Integer = 0

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private objDataOperation As clsDataOperation
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

#End Region

#Region " Properties "

    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
            Call GetAnalysisTran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _GrpWiseTotal() As Dictionary(Of Integer, Decimal)
        Get
            Return mdicGroupTotal
        End Get
        Set(ByVal value As Dictionary(Of Integer, Decimal))
            mdicGroupTotal = value
        End Set
    End Property

    Public Property _ItemWeight() As Dictionary(Of Integer, Decimal)
        Get
            Return mdicItemWeight
        End Get
        Set(ByVal value As Dictionary(Of Integer, Decimal))
            mdicItemWeight = value
        End Set
    End Property

    Public ReadOnly Property _Grp_Counter() As Integer
        Get
            Return mintGrpCounter
        End Get
    End Property

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("AnalysisTran")

            mdtTran.Columns.Add("analysistranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("objectiveunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("kpiunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("targetunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("initiativeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("resultunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("perspectiveunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("objective", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("kpi", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("target", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("initiative", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("result", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("group_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "

    Private Sub GetAnalysisTran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '            " hrbsc_analysis_tran.analysistranunkid " & _
            '            ",hrbsc_analysis_tran.analysisunkid " & _
            '            ",hrbsc_analysis_tran.objectiveunkid " & _
            '            ",hrbsc_analysis_tran.kpiunkid " & _
            '            ",hrbsc_analysis_tran.targetunkid " & _
            '            ",hrbsc_analysis_tran.initiativeunkid " & _
            '            ",hrbsc_analysis_tran.resultunkid " & _
            '            ",hrbsc_analysis_tran.remark " & _
            '            ",hrbsc_analysis_tran.isvoid " & _
            '            ",hrbsc_analysis_tran.voiduserunkid " & _
            '            ",hrbsc_analysis_tran.voiddatetime " & _
            '            ",hrbsc_analysis_tran.voidreason " & _
            '            ",'' AS AUD " & _
            '         ",CASE WHEN ISNULL(hrbsc_analysis_tran.kpiunkid,0)<=0 " & _
            '              "AND ISNULL(hrbsc_analysis_tran.targetunkid,0)<=0 " & _
            '              "AND ISNULL(hrbsc_analysis_tran.initiativeunkid,0)<=0 THEN ISNULL(hrobjective_master.name,'') ELSE '' END AS objective " & _
            '            ",ISNULL(hrkpi_master.name,'') AS kpi " & _
            '            ",ISNULL(hrtarget_master.name,'') AS TARGET " & _
            '            ",ISNULL(hrinitiative_master.name,'') AS initiative " & _
            '         ",CASE WHEN hrobjective_master.perspectiveunkid = 1 THEN @FINANCIAL " & _
            '                    "WHEN hrobjective_master.perspectiveunkid = 2 THEN @CUSTOMER " & _
            '                    "WHEN hrobjective_master.perspectiveunkid = 3 THEN @BUSINESS_PROCESS " & _
            '                    "WHEN hrobjective_master.perspectiveunkid = 4 THEN @ORGANIZATION_CAPACITY " & _
            '            " END AS group_name " & _
            '            ", ISNULL(hrobjective_master.weight,0) As weight " & _
            '         ",CASE WHEN ISNULL(hrbsc_analysis_tran.kpiunkid,0)<=0 " & _
            '              "AND ISNULL(hrbsc_analysis_tran.targetunkid,0)<=0 " & _
            '              "AND ISNULL(hrbsc_analysis_tran.initiativeunkid,0)<=0 THEN 1 ELSE 0 END AS isGrp " & _
            '         ",hrbsc_analysis_tran.perspectiveunkid AS GrpId " & _
            '         ",CASE WHEN ISNULL(hrbsc_analysis_tran.kpiunkid,0)<=0 " & _
            '              "AND ISNULL(hrbsc_analysis_tran.targetunkid,0)<=0 " & _
            '              "AND ISNULL(hrbsc_analysis_tran.initiativeunkid,0)<=0 THEN ISNULL(hrresult_master.resultname,'') ELSE '' END AS result " & _
            '         ",hrobjective_master.perspectiveunkid " & _
            '            "FROM hrbsc_analysis_tran " & _
            '            "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '            "LEFT JOIN hrinitiative_master ON hrbsc_analysis_tran.initiativeunkid = hrinitiative_master.initiativeunkid " & _
            '            "LEFT JOIN hrtarget_master ON hrbsc_analysis_tran.targetunkid = hrtarget_master.targetunkid " & _
            '            "LEFT JOIN hrkpi_master ON hrbsc_analysis_tran.kpiunkid = hrkpi_master.kpiunkid " & _
            '            "LEFT JOIN hrobjective_master ON hrbsc_analysis_tran.objectiveunkid = hrobjective_master.objectiveunkid " & _
            '            "JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
            '            "WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_tran.analysisunkid = @AnalysisUnkid "

            StrQ = "SELECT " & _
                        " hrbsc_analysis_tran.analysistranunkid " & _
                        ",hrbsc_analysis_tran.analysisunkid " & _
                        ",hrbsc_analysis_tran.objectiveunkid " & _
                        ",hrbsc_analysis_tran.kpiunkid " & _
                        ",hrbsc_analysis_tran.targetunkid " & _
                        ",hrbsc_analysis_tran.initiativeunkid " & _
                        ",hrbsc_analysis_tran.resultunkid " & _
                        ",hrbsc_analysis_tran.remark " & _
                        ",hrbsc_analysis_tran.isvoid " & _
                        ",hrbsc_analysis_tran.voiduserunkid " & _
                        ",hrbsc_analysis_tran.voiddatetime " & _
                        ",hrbsc_analysis_tran.voidreason " & _
                        ",'' AS AUD " & _
                     ",CASE WHEN ISNULL(hrbsc_analysis_tran.kpiunkid,0)<=0 " & _
                          "AND ISNULL(hrbsc_analysis_tran.targetunkid,0)<=0 " & _
                          "AND ISNULL(hrbsc_analysis_tran.initiativeunkid,0)<=0 THEN ISNULL(hrobjective_master.name,'') ELSE '' END AS objective " & _
                        ",ISNULL(hrkpi_master.name,'') AS kpi " & _
                        ",ISNULL(hrtarget_master.name,'') AS TARGET " & _
                        ",ISNULL(hrinitiative_master.name,'') AS initiative " & _
                     ",CASE WHEN hrobjective_master.perspectiveunkid = 1 THEN @FINANCIAL " & _
                                "WHEN hrobjective_master.perspectiveunkid = 2 THEN @CUSTOMER " & _
                                "WHEN hrobjective_master.perspectiveunkid = 3 THEN @BUSINESS_PROCESS " & _
                                "WHEN hrobjective_master.perspectiveunkid = 4 THEN @ORGANIZATION_CAPACITY " & _
                        " END AS group_name " & _
                        ", ISNULL(hrobjective_master.weight,0) As weight " & _
                     ",CASE WHEN ISNULL(hrbsc_analysis_tran.kpiunkid,0)<=0 " & _
                          "AND ISNULL(hrbsc_analysis_tran.targetunkid,0)<=0 " & _
                          "AND ISNULL(hrbsc_analysis_tran.initiativeunkid,0)<=0 THEN 1 ELSE 0 END AS isGrp " & _
                     ",hrbsc_analysis_tran.perspectiveunkid AS GrpId " & _
                     ",ISNULL(hrresult_master.resultname,0) AS result " & _
                     ",hrobjective_master.perspectiveunkid " & _
                     ",hrbsc_analysis_master.periodunkid " & _
                        "FROM hrbsc_analysis_tran " & _
                        "LEFT JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                        "LEFT JOIN hrinitiative_master ON hrbsc_analysis_tran.initiativeunkid = hrinitiative_master.initiativeunkid " & _
                        "LEFT JOIN hrtarget_master ON hrbsc_analysis_tran.targetunkid = hrtarget_master.targetunkid " & _
                        "LEFT JOIN hrkpi_master ON hrbsc_analysis_tran.kpiunkid = hrkpi_master.kpiunkid " & _
                        "LEFT JOIN hrobjective_master ON hrbsc_analysis_tran.objectiveunkid = hrobjective_master.objectiveunkid " & _
                        "JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid " & _
                        "WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_tran.analysisunkid = @AnalysisUnkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP [ 30 MAY 2014 ] -- START {hrbsc_analysis_master.periodunkid} -- END

            

            objDataOperation.AddParameter("@AnalysisUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
            objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear() : mdicGroupTotal = New Dictionary(Of Integer, Decimal) : mdicItemWeight = New Dictionary(Of Integer, Decimal)
            Dim iCnt As Integer = 0
            Dim strGrpItem As String = String.Empty

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'S.SANDEEP [ 30 MAY 2014 ] -- START
            'Dim objWSetting As New clsWeight_Setting(True)
            'S.SANDEEP [ 30 MAY 2014 ] -- END
            Dim StrName As String = String.Empty
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If strGrpItem <> dtRow.Item("group_name").ToString Then
                    iCnt += 1
                    mintGrpCounter = iCnt
                    strGrpItem = dtRow.Item("group_name").ToString
                End If
               
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If dtRow.Item("objective").ToString.Trim.Length > 0 Then
                '    dtRow.Item("objective") = dtRow.Item("objective") & " [ " & dtRow.Item("weight") & " ]"
                'End If

                'S.SANDEEP [ 30 MAY 2014 ] -- START
                Dim objWSetting As New clsWeight_Setting(CInt(dtRow.Item("periodunkid")))
                'S.SANDEEP [ 30 MAY 2014 ] -- END

                Select Case objWSetting._Weight_Optionid
                    Case enWeight_Options.WEIGHT_BASED_ON
                        Select Case objWSetting._Weight_Typeid
                            Case enWeight_Types.WEIGHT_FIELD1
                                If CBool(dtRow.Item("isGrp")) = False Then
                                    dtRow.Item("result") = ""
                                End If
                            Case enWeight_Types.WEIGHT_FIELD2
                                If CBool(dtRow.Item("isGrp")) = True Then
                                    dtRow.Item("result") = ""
                                End If
                                If dtRow.Item("kpi") = StrName Then
                                    dtRow.Item("result") = ""
                                End If
                                StrName = dtRow.Item("kpi")
                            Case enWeight_Types.WEIGHT_FIELD3
                                If CBool(dtRow.Item("isGrp")) = True Then
                                    dtRow.Item("result") = ""
                                End If
                                If dtRow.Item("Target") = StrName Then
                                    dtRow.Item("result") = ""
                                End If
                                StrName = dtRow.Item("Target")
                            Case enWeight_Types.WEIGHT_FIELD5
                                If CBool(dtRow.Item("isGrp")) = True Then
                                    dtRow.Item("result") = ""
                                End If
                                If dtRow.Item("initiative") = StrName Then
                                    dtRow.Item("result") = ""
                                End If
                                StrName = dtRow.Item("initiative")
                        End Select
                    Case enWeight_Options.WEIGHT_EACH_ITEM
                        If CBool(dtRow.Item("isGrp")) = True Then
                            dtRow.Item("result") = ""
                        End If
                End Select

                If mdicGroupTotal.ContainsKey(dtRow.Item("objectiveunkid")) = False Then
                    If IsNumeric(dtRow.Item("result")) Then
                        mdicGroupTotal.Add(dtRow.Item("objectiveunkid"), dtRow.Item("result"))
                    End If
                Else
                    If IsNumeric(dtRow.Item("result")) Then
                        mdicGroupTotal(dtRow.Item("objectiveunkid")) = mdicGroupTotal(dtRow.Item("objectiveunkid")) + dtRow.Item("result")
                    End If
                End If
                'S.SANDEEP [ 28 DEC 2012 ] -- END



                mdtTran.ImportRow(dtRow)

            Next

            'For Each dtRow As DataRow In dsList.Tables("List").Rows
            '    If strGrpItem <> dtRow.Item("assessitem") Then
            '        iCnt += 1
            '        dtRow.Item("group_name") = iCnt.ToString & ".0" & Space(5) & dtRow.Item("assessitem").ToString & Space(3) & "[" & dtRow.Item("weight").ToString & "]"
            '        strGrpItem = dtRow.Item("assessitem").ToString
            '        dtRow.AcceptChanges()
            '        mintGrpCounter = iCnt
            '    Else
            '        dtRow.Item("group_name") = iCnt.ToString & ".0" & Space(5) & dtRow.Item("assessitem").ToString & Space(3) & "[" & dtRow.Item("weight").ToString & "]"
            '    End If

            '    If mdicGroupTotal.ContainsKey(dtRow.Item("assessitemunkid")) = False Then
            '        If IsNumeric(dtRow.Item("result")) Then
            '            mdicGroupTotal.Add(dtRow.Item("assessitemunkid"), dtRow.Item("result"))
            '        End If
            '    Else
            '        If IsNumeric(dtRow.Item("result")) Then
            '            mdicGroupTotal(dtRow.Item("assessitemunkid")) = mdicGroupTotal(dtRow.Item("assessitemunkid")) + dtRow.Item("result")
            '        End If
            '    End If

            '    If mdicItemWeight.ContainsKey(dtRow.Item("assessitemunkid")) = False Then
            '        mdicItemWeight.Add(dtRow.Item("assessitemunkid"), dtRow.Item("weight"))
            '    End If

            '    mdtTran.ImportRow(dtRow)

            'Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAnalysisTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_AnalysisTran(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function InsertUpdateDelete_AnalysisTran() As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objDataOp As New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END
        Try
            Dim dGRow() As DataRow = mdtTran.Select("isGrp=true")

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    'For i = 0 To dGRow.Length - 1
                    'With dGRow(i)

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'objDataOp.ClearParameters()
                    objDataOperation.ClearParameters()
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrbsc_analysis_tran ( " & _
                                                            "  analysisunkid " & _
                                                            ", objectiveunkid " & _
                                                            ", kpiunkid " & _
                                                            ", targetunkid " & _
                                                            ", initiativeunkid " & _
                                                            ", resultunkid " & _
                                                            ", remark " & _
                                                            ", isvoid " & _
                                                            ", voiduserunkid " & _
                                                            ", voiddatetime " & _
                                                            ", voidreason" & _
                                                ", perspectiveunkid " & _
                                                     ") VALUES (" & _
                                                            "  @analysisunkid " & _
                                                            ", @objectiveunkid " & _
                                                            ", @kpiunkid " & _
                                                            ", @targetunkid " & _
                                                            ", @initiativeunkid " & _
                                                            ", @resultunkid " & _
                                                            ", @remark " & _
                                                            ", @isvoid " & _
                                                            ", @voiduserunkid " & _
                                                            ", @voiddatetime " & _
                                                            ", @voidreason" & _
                                                ", @perspectiveunkid " & _
                                                          "); SELECT @@identity"


                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                'objDataOp.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("objectiveunkid").ToString)
                                'objDataOp.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("kpiunkid").ToString)
                                'objDataOp.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("targetunkid").ToString)
                                'objDataOp.AddParameter("@initiativeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("initiativeunkid").ToString)
                                'objDataOp.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                'objDataOp.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                'objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                'objDataOp.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("perspectiveunkid").ToString)

                                'Dim dsList As DataSet = objDataOp.ExecQuery(StrQ, "List")

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("objectiveunkid").ToString)
                                objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("kpiunkid").ToString)
                                objDataOperation.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("targetunkid").ToString)
                                objDataOperation.AddParameter("@initiativeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("initiativeunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("perspectiveunkid").ToString)

                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END




                                mintAnalysisTranId = dsList.Tables(0).Rows(0).Item(0)

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("analysisunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrbsc_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrbsc_analysis_tran", "analysistranunkid", mintAnalysisTranId, 2, 1) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrbsc_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrbsc_analysis_tran", "analysistranunkid", mintAnalysisTranId, 1, 1) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                'If .Item("analysisunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbsc_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrbsc_analysis_tran", "analysistranunkid", mintAnalysisTranId, 2, 1, , intUserUnkid) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbsc_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrbsc_analysis_tran", "analysistranunkid", mintAnalysisTranId, 1, 1, , intUserUnkid) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                            Case "U"
                                StrQ = "UPDATE hrbsc_analysis_tran SET " & _
                                             "  analysisunkid = @analysisunkid" & _
                                             ", objectiveunkid = @objectiveunkid" & _
                                             ", kpiunkid = @kpiunkid" & _
                                             ", targetunkid = @targetunkid" & _
                                             ", initiativeunkid = @initiativeunkid" & _
                                             ", resultunkid = @resultunkid" & _
                                             ", remark = @remark" & _
                                             ", isvoid = @isvoid" & _
                                             ", voiduserunkid = @voiduserunkid" & _
                                             ", voiddatetime = @voiddatetime" & _
                                             ", voidreason = @voidreason " & _
                                         ", perspectiveunkid = @perspectiveunkid " & _
                                           "WHERE analysistranunkid = @analysistranunkid "

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                'objDataOp.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                'objDataOp.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("objectiveunkid").ToString)
                                'objDataOp.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("kpiunkid").ToString)
                                'objDataOp.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("targetunkid").ToString)
                                'objDataOp.AddParameter("@initiativeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("initiativeunkid").ToString)
                                'objDataOp.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                'objDataOp.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'If IsDBNull(.Item("voiddatetime")) Then
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'objDataOp.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("perspectiveunkid").ToString)

                                'Call objDataOp.ExecNonQuery(StrQ)

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("objectiveunkid").ToString)
                                objDataOperation.AddParameter("@kpiunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("kpiunkid").ToString)
                                objDataOperation.AddParameter("@targetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("targetunkid").ToString)
                                objDataOperation.AddParameter("@initiativeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("initiativeunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("perspectiveunkid").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END




                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrbsc_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrbsc_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2) = False Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbsc_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrbsc_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2, , intUserUnkid) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                            Case "D"
                                If .Item("analysistranunkid") > 0 Then
                                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrbsc_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrbsc_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3) = False Then
                                    '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                    '    Throw exForce
                                    'End If

                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbsc_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrbsc_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3, , intUserUnkid) = False Then
                                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    '    Throw exForce
                                    'End If
                                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                                End If
                                StrQ = "UPDATE hrbsc_analysis_tran SET " & _
                                        "  isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE analysistranunkid = @analysistranunkid "


                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'If IsDBNull(.Item("voiddatetime")) Then
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Call objDataOp.ExecNonQuery(StrQ)

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_AnalysisTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsMasterData", 315, "Financial")
			Language.setMessage("clsMasterData", 316, "Customer")
			Language.setMessage("clsMasterData", 317, "Business Process")
			Language.setMessage("clsMasterData", 318, "Organization Capacity")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
