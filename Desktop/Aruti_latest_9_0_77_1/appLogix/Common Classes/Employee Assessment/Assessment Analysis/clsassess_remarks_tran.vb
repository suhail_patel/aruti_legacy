﻿'************************************************************************************************************************************
'Class Name : clsassess_analysis_tran.vb
'Purpose    :
'Date       :09/01/2012
'Written By :Sandeep J. Sharma
'Modified   : ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsassess_remarks_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_remarks_tran"
    Dim mstrMessage As String = ""
    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim objDataOperation As New clsDataOperation
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

#Region " Private Variables "

    Private mintRemarkTranUnkid As Integer = -1
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable

#End Region

#Region " Properties "

    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
            Call GetRemarksTran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("RemarksTran")

            mdtTran.Columns.Add("remarkstranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("coursemasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("timeframe_date", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("major_area", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("activity", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("support_required", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("other_training", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isimporvement", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("course_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "

    Private Sub GetRemarksTran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim objDataOperation As New clsDataOperation
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            StrQ = "SELECT " & _
                   " hrassess_remarks_tran.remarkstranunkid " & _
                   ",hrassess_remarks_tran.analysisunkid " & _
                   ",hrassess_remarks_tran.coursemasterunkid " & _
                   ",hrassess_remarks_tran.timeframe_date " & _
                   ",hrassess_remarks_tran.major_area " & _
                   ",hrassess_remarks_tran.activity " & _
                   ",hrassess_remarks_tran.support_required " & _
                   ",hrassess_remarks_tran.other_training " & _
                   ",hrassess_remarks_tran.isimporvement " & _
                   ",hrassess_remarks_tran.isvoid " & _
                   ",hrassess_remarks_tran.voiduserunkid " & _
                   ",hrassess_remarks_tran.voiddatetime " & _
                   ",hrassess_remarks_tran.voidreason " & _
                   ",'' AS AUD " & _
                   ",ISNULL(cfcommon_master.name,'') AS course_name " & _
                   "FROM hrassess_remarks_tran " & _
                   " JOIN hrassess_analysis_master ON hrassess_remarks_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
                   " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_remarks_tran.coursemasterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "'  " & _
                   "WHERE hrassess_remarks_tran.analysisunkid = @AnalysisUnkid " & _
                   " AND hrassess_remarks_tran.isvoid = 0 "

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ClearParameters()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@AnalysisUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                mdtTran.ImportRow(dtRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAnalysisTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_RemarksTran(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function InsertUpdateDelete_RemarksTran() As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objDataOp As New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END
        Try
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOp.BindTransaction()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'objDataOp.ClearParameters()
                    objDataOperation.ClearParameters()
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassess_remarks_tran ( " & _
                                            "  analysisunkid " & _
                                            ", coursemasterunkid " & _
                                            ", timeframe_date " & _
                                            ", major_area " & _
                                            ", activity " & _
                                            ", support_required " & _
                                            ", other_training " & _
                                            ", isimporvement " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                          ") VALUES (" & _
                                            "  @analysisunkid " & _
                                            ", @coursemasterunkid " & _
                                            ", @timeframe_date " & _
                                            ", @major_area " & _
                                            ", @activity " & _
                                            ", @support_required " & _
                                            ", @other_training " & _
                                            ", @isimporvement " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"


                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                'objDataOp.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("coursemasterunkid").ToString)
                                'objDataOp.AddParameter("@timeframe_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("timeframe_date").ToString)
                                'objDataOp.AddParameter("@major_area", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("major_area").ToString)
                                'objDataOp.AddParameter("@activity", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("activity").ToString)
                                'objDataOp.AddParameter("@support_required", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("support_required").ToString)
                                'objDataOp.AddParameter("@other_training", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("other_training").ToString)
                                'objDataOp.AddParameter("@isimporvement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isimporvement").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                'objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                'Dim dsList As DataSet = objDataOp.ExecQuery(StrQ, "List")

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'mintRemarkTranUnkid = dsList.Tables(0).Rows(0).Item(0)

                                'If .Item("analysisunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_remarks_tran", "remarkstranunkid", mintRemarkTranUnkid, 2, 1) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrassess_remarks_tran", "remarkstranunkid", mintRemarkTranUnkid, 1, 1) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("coursemasterunkid").ToString)
                                objDataOperation.AddParameter("@timeframe_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("timeframe_date").ToString)
                                objDataOperation.AddParameter("@major_area", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("major_area").ToString)
                                objDataOperation.AddParameter("@activity", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("activity").ToString)
                                objDataOperation.AddParameter("@support_required", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("support_required").ToString)
                                objDataOperation.AddParameter("@other_training", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("other_training").ToString)
                                objDataOperation.AddParameter("@isimporvement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isimporvement").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintRemarkTranUnkid = dsList.Tables(0).Rows(0).Item(0)

                                'If .Item("analysisunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_remarks_tran", "remarkstranunkid", mintRemarkTranUnkid, 2, 1, , intUserUnkid) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrassess_remarks_tran", "remarkstranunkid", mintRemarkTranUnkid, 1, 1, , intUserUnkid) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                            Case "U"
                                StrQ = "UPDATE hrassess_remarks_tran SET " & _
                                        "  analysisunkid = @analysisunkid" & _
                                        ", coursemasterunkid = @coursemasterunkid" & _
                                        ", timeframe_date = @timeframe_date" & _
                                        ", major_area = @major_area" & _
                                        ", activity = @activity" & _
                                        ", support_required = @support_required" & _
                                        ", other_training = @other_training" & _
                                        ", isimporvement = @isimporvement" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE remarkstranunkid = @remarkstranunkid "

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@remarkstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("remarkstranunkid").ToString)
                                'objDataOp.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                'objDataOp.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("coursemasterunkid").ToString)
                                'objDataOp.AddParameter("@timeframe_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("timeframe_date").ToString)
                                'objDataOp.AddParameter("@major_area", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("major_area").ToString)
                                'objDataOp.AddParameter("@activity", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("activity").ToString)
                                'objDataOp.AddParameter("@support_required", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("support_required").ToString)
                                'objDataOp.AddParameter("@other_training", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("other_training").ToString)
                                'objDataOp.AddParameter("@isimporvement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isimporvement").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'If IsDBNull(.Item("voiddatetime")) Then
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'Call objDataOp.ExecNonQuery(StrQ)

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_remarks_tran", "remarkstranunkid", .Item("remarkstranunkid"), 2, 2) = False Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@remarkstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("remarkstranunkid").ToString)
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("coursemasterunkid").ToString)
                                objDataOperation.AddParameter("@timeframe_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("timeframe_date").ToString)
                                objDataOperation.AddParameter("@major_area", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("major_area").ToString)
                                objDataOperation.AddParameter("@activity", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("activity").ToString)
                                objDataOperation.AddParameter("@support_required", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("support_required").ToString)
                                objDataOperation.AddParameter("@other_training", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("other_training").ToString)
                                objDataOperation.AddParameter("@isimporvement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isimporvement").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_remarks_tran", "remarkstranunkid", .Item("remarkstranunkid"), 2, 2, , intUserUnkid) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                            Case "D"
                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("remarkstranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_remarks_tran", "remarkstranunkid", .Item("remarkstranunkid"), 2, 3) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                'If .Item("remarkstranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_remarks_tran", "remarkstranunkid", .Item("remarkstranunkid"), 2, 3, , intUserUnkid) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                                StrQ = "UPDATE hrassess_remarks_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE remarkstranunkid = @remarkstranunkid "


                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@remarkstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("remarkstranunkid").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'If IsDBNull(.Item("voiddatetime")) Then
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'Call objDataOp.ExecNonQuery(StrQ)

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@remarkstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("remarkstranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_RemarksTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
