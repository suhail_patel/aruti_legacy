﻿'************************************************************************************************************************************
'Class Name : clsassess_competence_assign_tran.vb
'Purpose    :
'Date       :13-Jun-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_competence_assign_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_competence_assign_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintAssigncompetencetranunkid As Integer
    Private mintAssigncompetenceunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assigncompetenceunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assigncompetenceunkid() As Integer
        Get
            Return mintAssigncompetenceunkid
        End Get
        Set(ByVal value As Integer)
            mintAssigncompetenceunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtTran
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("List") : Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "assigncompetencetranunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "assigncompetenceunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "competenciesunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "weight"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GrpId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    Private Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_competence_assign_tran.assigncompetencetranunkid " & _
                   ", hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   ", hrassess_competence_assign_tran.competenciesunkid " & _
                   ", hrassess_competence_assign_tran.weight " & _
                   ", hrassess_competence_assign_tran.isvoid " & _
                   ", hrassess_competence_assign_tran.voiduserunkid " & _
                   ", hrassess_competence_assign_tran.voidreason " & _
                   ", hrassess_competence_assign_tran.voiddatetime " & _
                   ", '' AS AUD " & _
                   ", competence_categoryunkid AS GrpId " & _
                  "FROM hrassess_competence_assign_tran " & _
                  " JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrassess_competence_assign_tran.competenciesunkid " & _
                  "WHERE assigncompetenceunkid = @assigncompetenceunkid AND hrassess_competence_assign_tran.isvoid = 0 "

            objDataOperation.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssigncompetenceunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            mdtTran.Rows.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dtRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_CompetenceAssignment(ByVal iDataOpr As clsDataOperation, ByVal intUserUnkId As Integer) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    iDataOpr.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                'S.SANDEEP [ 17 DEC 2014 ] -- START
                                'StrQ = "INSERT INTO hrassess_competence_assign_tran ( " & _
                                '           "  assigncompetenceunkid " & _
                                '           ", competenciesunkid " & _
                                '           ", weight " & _
                                '           ", isvoid " & _
                                '           ", voiduserunkid " & _
                                '           ", voidreason " & _
                                '           ", voiddatetime" & _
                                '       ") VALUES (" & _
                                '           "  @assigncompetenceunkid " & _
                                '           ", @competenciesunkid " & _
                                '           ", @weight " & _
                                '           ", @isvoid " & _
                                '           ", @voiduserunkid " & _
                                '           ", @voidreason " & _
                                '           ", @voiddatetime" & _
                                '       "); SELECT @@identity"

                                'iDataOpr.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssigncompetenceunkid.ToString)
                                'iDataOpr.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid"))
                                'iDataOpr.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("weight"))
                                'iDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                'iDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                'iDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'If IsDBNull(.Item("voiddatetime")) = False Then
                                '    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                'Else
                                '    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'End If


                                'Dim dsList As New DataSet
                                'dsList = iDataOpr.ExecQuery(StrQ, "List")

                                'If iDataOpr.ErrorMessage <> "" Then
                                '    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'mintAssigncompetencetranunkid = dsList.Tables(0).Rows(0).Item(0)

                                'If clsCommonATLog.Insert_TranAtLog(iDataOpr, "hrassess_competence_assign_master", "assigncompetenceunkid", mintAssigncompetenceunkid, "hrassess_competence_assign_tran", "assigncompetencetranunkid", mintAssigncompetencetranunkid, 1, 1, , intUserUnkId) = False Then
                                '    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'S.SANDEEP [23 FEB 2016] -- START
                                'If .Item("weight") <> 0 Then
                                'S.SANDEEP [23 FEB 2016] -- END


                                StrQ = "INSERT INTO hrassess_competence_assign_tran ( " & _
                                           "  assigncompetenceunkid " & _
                                           ", competenciesunkid " & _
                                           ", weight " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voidreason " & _
                                           ", voiddatetime" & _
                                       ") VALUES (" & _
                                           "  @assigncompetenceunkid " & _
                                           ", @competenciesunkid " & _
                                           ", @weight " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voidreason " & _
                                           ", @voiddatetime" & _
                                       "); SELECT @@identity"

                                iDataOpr.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssigncompetenceunkid.ToString)
                                iDataOpr.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid"))
                                iDataOpr.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("weight"))
                                iDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                iDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                iDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If


                                Dim dsList As New DataSet
                                dsList = iDataOpr.ExecQuery(StrQ, "List")

                                If iDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                mintAssigncompetencetranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(iDataOpr, "hrassess_competence_assign_master", "assigncompetenceunkid", mintAssigncompetenceunkid, "hrassess_competence_assign_tran", "assigncompetencetranunkid", mintAssigncompetencetranunkid, 1, 1, , intUserUnkId) = False Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                                'S.SANDEEP [ 17 DEC 2014 ] -- END
                            Case "U"

                                StrQ = "UPDATE hrassess_competence_assign_tran SET " & _
                                           "  assigncompetenceunkid = @assigncompetenceunkid" & _
                                           ", competenciesunkid = @competenciesunkid" & _
                                           ", weight = @weight" & _
                                           ", isvoid = @isvoid" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voidreason = @voidreason" & _
                                           ", voiddatetime = @voiddatetime " & _
                                       "WHERE assigncompetencetranunkid = @assigncompetencetranunkid "

                                iDataOpr.AddParameter("@assigncompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assigncompetencetranunkid"))
                                iDataOpr.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assigncompetenceunkid"))
                                iDataOpr.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid"))
                                iDataOpr.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("weight"))
                                iDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                iDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                iDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                Call iDataOpr.ExecNonQuery(StrQ)

                                If iDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(iDataOpr, "hrassess_competence_assign_master", "assigncompetenceunkid", .Item("assigncompetenceunkid"), "hrassess_competence_assign_tran", "assigncompetencetranunkid", .Item("assigncompetencetranunkid"), 2, 2, , intUserUnkId) = False Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Case "D"

                                StrQ = "UPDATE hrassess_competence_assign_tran SET " & _
                                           "  isvoid = @isvoid" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voidreason = @voidreason" & _
                                           ", voiddatetime = @voiddatetime " & _
                                       "WHERE assigncompetencetranunkid = @assigncompetencetranunkid "

                                iDataOpr.AddParameter("@assigncompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assigncompetencetranunkid"))
                                iDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                iDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                iDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                
                                Call iDataOpr.ExecNonQuery(StrQ)

                                If iDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(iDataOpr, "hrassess_competence_assign_master", "assigncompetenceunkid", .Item("assigncompetenceunkid"), "hrassess_competence_assign_tran", "assigncompetencetranunkid", .Item("assigncompetencetranunkid"), 2, 3, , intUserUnkId) = False Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_CompetenceAssignment", mstrModuleName)
        Finally
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_competence_assign_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal mblnIsvoid As Boolean, ByVal mintVoiduserunkid As Integer, ByVal mstrVoidreason As String, ByVal mdtVoiddatetime As DateTime, ByVal blnSelfAssignCompetencies As Boolean) As Boolean 'S.SANDEEP [22 Jan 2016] -- START -- END
        'Public Function Delete(ByVal intUnkid As Integer, ByVal mblnIsvoid As Boolean, ByVal mintVoiduserunkid As Integer, ByVal mstrVoidreason As String, ByVal mdtVoiddatetime As DateTime) As Boolean
        mstrMessage = ""
        'S.SANDEEP [22 Jan 2016] -- START
        'If isUsed(intUnkid) Then
        If isUsed(intUnkid, blnSelfAssignCompetencies) Then
            'S.SANDEEP [22 Jan 2016] -- END

            'S.SANDEEP [18 Jan 2016] -- START
            'mstrMessage = mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot void the selected competencies. Reason, They are linked with assessment evaluation.")
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot void the selected competencies. Reason, They are linked with assessment evaluation.")
            'S.SANDEEP [18 Jan 2016] -- END
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            'S.SANDEEP [29 JAN 2015] -- START
            strQ = "SELECT @MstId = assigncompetenceunkid FROM hrassess_competence_assign_tran WHERE assigncompetencetranunkid = '" & intUnkid & "' "

            objDataOperation.AddParameter("@MstId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssigncompetenceunkid, ParameterDirection.InputOutput)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssigncompetenceunkid = objDataOperation.GetParameterValue("@MstId")
            'S.SANDEEP [29 JAN 2015] -- END

            strQ = "UPDATE hrassess_competence_assign_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE assigncompetencetranunkid = @assigncompetencetranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@assigncompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [29 JAN 2015] -- START
            Dim xRowCnt As Integer = -1
            strQ = "SELECT @Cnt = COUNT(assigncompetencetranunkid) FROM hrassess_competence_assign_tran WHERE assigncompetenceunkid = '" & mintAssigncompetenceunkid & "' AND isvoid = 0 "

            objDataOperation.AddParameter("@Cnt", SqlDbType.Int, eZeeDataType.INT_SIZE, xRowCnt, ParameterDirection.InputOutput)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xRowCnt = objDataOperation.GetParameterValue("@Cnt")

            If xRowCnt = 0 Then
                strQ = "UPDATE hrassess_competence_assign_master SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidreason = @voidreason" & _
                       ", voiddatetime = @voiddatetime " & _
                       "WHERE assigncompetenceunkid = '" & mintAssigncompetenceunkid & "' "

                objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@assigncompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If

            If xRowCnt = 0 Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_competence_assign_master", "assigncompetenceunkid", mintAssigncompetenceunkid, "hrassess_competence_assign_tran", "assigncompetencetranunkid", intUnkid, 3, 3, , mintVoiduserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            Else
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_competence_assign_master", "assigncompetenceunkid", mintAssigncompetenceunkid, "hrassess_competence_assign_tran", "assigncompetencetranunkid", intUnkid, 2, 3, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END


            End If
            'S.SANDEEP [29 JAN 2015] -- END
            
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [29 JAN 2015] -- START
    Public Function isUsed(ByVal intUnkid As Integer, ByVal blnSelfAssignCompetencies As Boolean) As Boolean 'S.SANDEEP [22 Jan 2016] -- START -- END
        'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'S.SANDEEP [22 Jan 2016] -- START
            'strQ = "SELECT " & _
            '       "    1 " & _
            '       "FROM hrcompetency_analysis_tran " & _
            '       "    JOIN hrevaluation_analysis_master ON hrcompetency_analysis_tran.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
            '       "    JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '       "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND assigncompetencetranunkid = @assigncompetencetranunkid "

            strQ = "SELECT " & _
                   "    1 " & _
                   "FROM hrcompetency_analysis_tran " & _
                   "    JOIN hrevaluation_analysis_master ON hrcompetency_analysis_tran.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "    JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   "    JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND assigncompetencetranunkid = @assigncompetencetranunkid "

            If blnSelfAssignCompetencies = True Then
                strQ &= " AND (hrassess_competence_assign_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid OR hrassess_competence_assign_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid) "
            End If
            'S.SANDEEP [22 Jan 2016] -- END

            objDataOperation.AddParameter("@assigncompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP [29 JAN 2015] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, you cannot void the selected competencies. Reason, They are linked with assessment evaluation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
