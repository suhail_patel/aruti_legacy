﻿'************************************************************************************************************************************
'Class Name : clsassess_competence_assign_master.vb
'Purpose    :
'Date       :13-Jun-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_competence_assign_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_competence_assign_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintAssigncompetenceunkid As Integer = 0
    Private mintAssessgroupunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mdblWeight As Decimal = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    'S.SANDEEP [29 JAN 2015] -- START
    Private mintEmployeeunkid As Integer = 0
    Private mblnIsFinal As Boolean = False
    'S.SANDEEP [29 JAN 2015] -- END

    'S.SANDEEP [23 FEB 2016] -- START
    Private mblnIsZeroWeightAllowed As Boolean = False
    'S.SANDEEP [23 FEB 2016] -- END

#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assigncompetenceunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assigncompetenceunkid() As Integer
        Get
            Return mintAssigncompetenceunkid
        End Get
        Set(ByVal value As Integer)
            mintAssigncompetenceunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assessgroupunkid() As Integer
        Get
            Return mintAssessgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Decimal)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    'S.SANDEEP [29 JAN 2015] -- START
    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsFinal() As Boolean
        Get
            Return mblnIsFinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinal = value
        End Set
    End Property
    'S.SANDEEP [29 JAN 2015] -- END

    'S.SANDEEP [23 FEB 2016] -- START
    Public Property _IsZeroWeightAllowed() As Boolean
        Get
            Return mblnIsZeroWeightAllowed
        End Get
        Set(ByVal value As Boolean)
            mblnIsZeroWeightAllowed = value
        End Set
    End Property
    'S.SANDEEP [23 FEB 2016] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assigncompetenceunkid " & _
              ", assessgroupunkid " & _
              ", periodunkid " & _
              ", weight " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", employeeunkid " & _
              ", isfinal " & _
              ", iszeroweightallowed " & _
             "FROM hrassess_competence_assign_master " & _
             "WHERE assigncompetenceunkid = @assigncompetenceunkid " 'S.SANDEEP [29 JAN 2015] -- START {employeeunkid,isfinal} -- END
            'S.SANDEEP [23 FEB 2016] -- START {iszeroweightallowed} -- END


            objDataOperation.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssigncompetenceunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssigncompetenceunkid = CInt(dtRow.Item("assigncompetenceunkid"))
                mintAssessgroupunkid = CInt(dtRow.Item("assessgroupunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdblWeight = CDbl(dtRow.Item("weight"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                'S.SANDEEP [29 JAN 2015] -- START
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsFinal = CBool(dtRow.Item("isfinal"))
                'S.SANDEEP [29 JAN 2015] -- END

                'S.SANDEEP [23 FEB 2016] -- START
                mblnIsZeroWeightAllowed = CBool(dtRow.Item("iszeroweightallowed"))
                'S.SANDEEP [23 FEB 2016] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal dtEmployeeAsOnDate As DateTime, _
                            ByVal iGroupId As Integer, _
                            ByVal iPeriodId As Integer, _
                            Optional ByVal iEmployeeId As Integer = 0, _
                            Optional ByVal iSelfAssignment As Boolean = False) As DataSet 'S.SANDEEP [19 DEC 2016] -- START {REMOVED OPTIONAL FROM iGroupId,iPeriodId} -- END




        'Public Function GetList(ByVal strTableName As String, _
        '                    Optional ByVal iGroupId As Integer = 0, _
        '                    Optional ByVal iPeriodId As Integer = 0, _
        '                    Optional ByVal iEmployeeId As Integer = 0, _
        '                    Optional ByVal iSelfAssignment As Boolean = False) As DataSet 'S.SANDEEP [29 JAN 2015] -- START {iEmployeeId} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [28 MAR 2015] -- START
            'strQ = "SELECT " & _
            '           " hrassess_group_master.assessgroup_name AS group_name " & _
            '           ",cfcommon_master.name AS ccategory " & _
            '           ",ISNULL(hrassess_competencies_master.name,'') AS competencies " & _
            '           ",cfcommon_period_tran.period_name AS Period " & _
            '           ",hrassess_competence_assign_tran.weight AS assigned_weight " & _
            '           ",hrassess_competence_assign_master.assigncompetenceunkid " & _
            '           ",hrassess_competence_assign_master.assessgroupunkid " & _
            '           ",hrassess_competence_assign_master.periodunkid " & _
            '           ",hrassess_competence_assign_master.weight " & _
            '           ",hrassess_competence_assign_master.userunkid " & _
            '           ",hrassess_competence_assign_master.isvoid " & _
            '           ",hrassess_competence_assign_master.voiduserunkid " & _
            '           ",hrassess_competence_assign_master.voidreason " & _
            '           ",hrassess_competence_assign_master.voiddatetime " & _
            '           ",assigncompetencetranunkid " & _
            '           ",masterunkid " & _
            '           ",hrassess_competence_assign_master.employeeunkid " & _
            '           ",hrassess_competence_assign_master.isfinal " & _
            '           ",hrassess_competence_assign_tran.competenciesunkid " & _
            '           ",hremployee_master.employeecode+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS empname " & _
            '           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ename " & _
            '           ",cfcommon_period_tran.statusid "
            'If iSelfAssignment = True Then
            '    strQ &= ",ISNULL(STypId,0) AS opstatusid "
            '    'S.SANDEEP [12 FEB 2015] -- START
            'Else
            '    strQ &= ",0 AS opstatusid "
            '    'S.SANDEEP [12 FEB 2015] -- END
            'End If
            'strQ &= "FROM hrassess_competence_assign_master " & _
            '           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_competence_assign_master.employeeunkid " & _
            '           "JOIN cfcommon_period_tran ON hrassess_competence_assign_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '           "JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
            '           "JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '           "JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
            '           "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' "
            'If iSelfAssignment = True Then
            '    strQ &= "LEFT JOIN " & _
            '            "( " & _
            '            "   SELECT EId,PId,STypId FROM " & _
            '            "   ( " & _
            '            "       SELECT " & _
            '            "            employeeunkid AS EId " & _
            '            "           ,periodunkid AS PId " & _
            '            "           ,statustypeid AS STypId " & _
            '            "           ,CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
            '            "           ,ROW_NUMBER() OVER(PARTITION BY employeeunkid,periodunkid ORDER BY statustranunkid DESC) AS RNo " & _
            '            "       FROM hrassess_empstatus_tran " & _
            '            "   )AS A WHERE RNo = 1 " & _
            '            ") AS Obj_Status ON Obj_Status.EId = hremployee_master.employeeunkid AND Obj_Status.PId = hrassess_competence_assign_master.periodunkid "
            'End If
            'strQ &= "WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 "

            'If iGroupId > 0 Then
            '    strQ &= " AND hrassess_competence_assign_master.assessgroupunkid = '" & iGroupId & "' "
            'End If

            'If iPeriodId > 0 Then
            '    strQ &= " AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
            'End If

            ''S.SANDEEP [29 JAN 2015] -- START
            'If iEmployeeId > 0 Then
            '    strQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
            'End If
            ''S.SANDEEP [29 JAN 2015] -- END

            If iEmployeeId > 0 AndAlso iSelfAssignment = True Then
                strQ = "SELECT " & _
                           " hrassess_group_master.assessgroup_name AS group_name " & _
                           ",cfcommon_master.name AS ccategory " & _
                           ",ISNULL(hrassess_competencies_master.name,'') AS competencies " & _
                           ",cfcommon_period_tran.period_name AS Period " & _
                           ",hrassess_competence_assign_tran.weight AS assigned_weight " & _
                           ",hrassess_competence_assign_master.assigncompetenceunkid " & _
                           ",hrassess_competence_assign_master.assessgroupunkid " & _
                           ",hrassess_competence_assign_master.periodunkid " & _
                           ",hrassess_competence_assign_master.weight " & _
                           ",hrassess_competence_assign_master.userunkid " & _
                           ",hrassess_competence_assign_master.isvoid " & _
                           ",hrassess_competence_assign_master.voiduserunkid " & _
                           ",hrassess_competence_assign_master.voidreason " & _
                           ",hrassess_competence_assign_master.voiddatetime " & _
                           ",assigncompetencetranunkid " & _
                           ",masterunkid " & _
                           ",hrassess_competence_assign_master.employeeunkid " & _
                           ",hrassess_competence_assign_master.isfinal " & _
                           ",hrassess_competence_assign_tran.competenciesunkid " & _
                           ",hremployee_master.employeecode+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS empname " & _
                           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ename " & _
                           ",cfcommon_period_tran.statusid " & _
                           ",hrassess_competence_assign_master.iszeroweightallowed "
                'S.SANDEEP [23 FEB 2016] -- START {iszeroweightallowed} -- END

                If iSelfAssignment = True Then
                    strQ &= ",ISNULL(STypId,0) AS opstatusid "
                    'S.SANDEEP [12 FEB 2015] -- START
                Else
                    strQ &= ",0 AS opstatusid "
                    'S.SANDEEP [12 FEB 2015] -- END
                End If
                strQ &= "FROM hrassess_competence_assign_master " & _
                           "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_competence_assign_master.employeeunkid " & _
                           "JOIN cfcommon_period_tran ON hrassess_competence_assign_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                           "JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                           "JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                           "JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                           "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' "
                If iSelfAssignment = True Then
                    strQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT EId,PId,STypId FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            employeeunkid AS EId " & _
                            "           ,periodunkid AS PId " & _
                            "           ,statustypeid AS STypId " & _
                            "           ,CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
                            "           ,ROW_NUMBER() OVER(PARTITION BY employeeunkid,periodunkid ORDER BY statustranunkid DESC) AS RNo " & _
                            "       FROM hrassess_empstatus_tran " & _
                            "   )AS A WHERE RNo = 1 " & _
                            ") AS Obj_Status ON Obj_Status.EId = hremployee_master.employeeunkid AND Obj_Status.PId = hrassess_competence_assign_master.periodunkid "
                End If
                strQ &= "WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 "

                If iGroupId > 0 Then
                    strQ &= " AND hrassess_competence_assign_master.assessgroupunkid = '" & iGroupId & "' "
                End If

                If iPeriodId > 0 Then
                    strQ &= " AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
                End If

                If iEmployeeId > 0 Then
                    strQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
                End If
            Else

                Dim iStrGroupIds As String = String.Empty
                Dim objAssessGrp As New clsassess_group_master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(iEmployeeId)

                'S.SANDEEP [19 DEC 2016] -- START
                'iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(iEmployeeId, dtEmployeeAsOnDate)
                iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(iEmployeeId, dtEmployeeAsOnDate, iPeriodId)
                'S.SANDEEP [19 DEC 2016] -- END

                'S.SANDEEP [04 JUN 2015] -- END

                objAssessGrp = Nothing

                If iGroupId > 0 Then
                    iStrGroupIds = iGroupId.ToString
                End If


                'S.SANDEEP [14 APR 2015] -- START
                If iStrGroupIds.Trim.Length <= 0 Then Exit Try
                'S.SANDEEP [14 APR 2015] -- END


                strQ = "SELECT " & _
                       "     hrassess_group_master.assessgroup_name AS group_name " & _
                       "    ,cfcommon_master.name AS ccategory " & _
                       "    ,ISNULL(hrassess_competencies_master.name,'') AS competencies " & _
                       "    ,cfcommon_period_tran.period_name AS Period " & _
                       "    ,hrassess_competence_assign_tran.weight AS assigned_weight " & _
                       "    ,hrassess_competence_assign_master.assigncompetenceunkid " & _
                       "    ,hrassess_competence_assign_master.assessgroupunkid " & _
                       "    ,hrassess_competence_assign_master.periodunkid " & _
                       "    ,hrassess_competence_assign_master.weight " & _
                       "    ,hrassess_competence_assign_master.userunkid " & _
                       "    ,hrassess_competence_assign_master.isvoid " & _
                       "    ,hrassess_competence_assign_master.voiduserunkid " & _
                       "    ,hrassess_competence_assign_master.voidreason " & _
                       "    ,hrassess_competence_assign_master.voiddatetime " & _
                       "    ,assigncompetencetranunkid " & _
                       "    ,masterunkid " & _
                       "    ,hrassess_competence_assign_master.employeeunkid " & _
                       "    ,hrassess_competence_assign_master.isfinal " & _
                       "    ,hrassess_competence_assign_tran.competenciesunkid " & _
                       "    ,hremployee_master.employeecode+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS empname " & _
                       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ename " & _
                       "    ,cfcommon_period_tran.statusid " & _
                       "    ,0 AS opstatusid " & _
                       "    ,hrassess_competence_assign_master.iszeroweightallowed " & _
                       "FROM hrassess_competence_assign_master " & _
                       "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_competence_assign_master.employeeunkid " & _
                       "    JOIN cfcommon_period_tran ON hrassess_competence_assign_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                       "    JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                       "    JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                       "    JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                       "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
                       "WHERE hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid IN (" & iStrGroupIds & ") " & _
                       "AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "'  AND ISNULL(hrassess_competence_assign_master.employeeunkid,0) <= 0 "
                'S.SANDEEP [23 FEB 2016] -- START {iszeroweightallowed} -- END

            End If

            'S.SANDEEP [28 MAR 2015] -- END



            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_competence_assign_master) </purpose>
    Public Function Insert(ByVal iDataTable As DataTable) As Boolean
        If isExist(mintAssessgroupunkid, mintPeriodunkid, , mintEmployeeunkid) Then 'S.SANDEEP [29 JAN 2015] -- START {employeeunkid} -- END
            'S.SANDEEP [29 DEC 2015] -- START
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry this group is already defined for the selected period. Please define new group.")
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Competencies are already defined for this employee for the competence group and period selected, use Edit option if you wish to Add/Edit/Delete the competencies.")
            'S.SANDEEP [29 DEC 2015] -- END
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [29 JAN 2015] -- START
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal)
            'S.SANDEEP [29 JAN 2015] -- END

            'S.SANDEEP [23 FEB 2016] -- START
            objDataOperation.AddParameter("@iszeroweightallowed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsZeroWeightAllowed)
            'S.SANDEEP [23 FEB 2016] -- END


            strQ = "INSERT INTO hrassess_competence_assign_master ( " & _
                       "  assessgroupunkid " & _
                       ", periodunkid " & _
                       ", weight " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime" & _
                       ", employeeunkid " & _
                       ", isfinal " & _
                       ", iszeroweightallowed " & _
                   ") VALUES (" & _
                       "  @assessgroupunkid " & _
                       ", @periodunkid " & _
                       ", @weight " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                       ", @employeeunkid " & _
                       ", @isfinal " & _
                       ", @iszeroweightallowed " & _
                   "); SELECT @@identity" 'S.SANDEEP [29 JAN 2015] -- START {employeeunkid,isfinal} -- END
            'S.SANDEEP [23 FEB 2016] -- START {iszeroweightallowed} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssigncompetenceunkid = dsList.Tables(0).Rows(0).Item(0)

            If iDataTable IsNot Nothing Then
                Dim objAssignCompetenceTran As New clsassess_competence_assign_tran
                objAssignCompetenceTran._Assigncompetenceunkid = mintAssigncompetenceunkid
                objAssignCompetenceTran._DataTable = iDataTable.Copy
                With objAssignCompetenceTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssignCompetenceTran.InsertUpdateDelete_CompetenceAssignment(objDataOperation, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objAssignCompetenceTran = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_competence_assign_master) </purpose>
    Public Function Update(ByVal iDataTable As DataTable) As Boolean
        If isExist(mintAssessgroupunkid, mintPeriodunkid, mintAssigncompetenceunkid, mintEmployeeunkid) Then 'S.SANDEEP [29 JAN 2015] -- START {employeeunkid} -- END
            'S.SANDEEP [29 DEC 2015] -- START
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry this group is already defined for the selected period. Please define new group.")
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Competencies are already defined for this employee for the competence group and period selected, use Edit option if you wish to Add/Edit/Delete the competencies.")
            'S.SANDEEP [29 DEC 2015] -- END
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssigncompetenceunkid.ToString)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [29 JAN 2015] -- START
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal)
            'S.SANDEEP [29 JAN 2015] -- END

            'S.SANDEEP [23 FEB 2016] -- START
            objDataOperation.AddParameter("@iszeroweightallowed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsZeroWeightAllowed)
            'S.SANDEEP [23 FEB 2016] -- END

            strQ = "UPDATE hrassess_competence_assign_master SET " & _
                       "  assessgroupunkid = @assessgroupunkid" & _
                       ", periodunkid = @periodunkid" & _
                       ", weight = @weight" & _
                       ", userunkid = @userunkid" & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidreason = @voidreason" & _
                       ", voiddatetime = @voiddatetime " & _
                       ", employeeunkid = @employeeunkid " & _
                       ", isfinal = @isfinal " & _
                       ", iszeroweightallowed = @iszeroweightallowed " & _
                   "WHERE assigncompetenceunkid = @assigncompetenceunkid " 'S.SANDEEP [29 JAN 2015] -- START {employeeunkid,isfinal} -- END
            'S.SANDEEP [23 FEB 2016] -- START {iszeroweightallowed} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iDataTable IsNot Nothing Then
                Dim objAssignCompetenceTran As New clsassess_competence_assign_tran
                objAssignCompetenceTran._Assigncompetenceunkid = mintAssigncompetenceunkid
                objAssignCompetenceTran._DataTable = iDataTable.Copy
                With objAssignCompetenceTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssignCompetenceTran.InsertUpdateDelete_CompetenceAssignment(objDataOperation, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objAssignCompetenceTran = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iAssessGrpId As Integer, _
                            ByVal iPeriodId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal iEmployeeId As Integer = 0) As Boolean 'S.SANDEEP [29 JAN 2015] -- START {employeeunkid} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assigncompetenceunkid " & _
              ", assessgroupunkid " & _
              ", periodunkid " & _
              ", weight " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", employeeunkid " & _
              ", iszeroweightallowed " & _
             "FROM hrassess_competence_assign_master " & _
             "WHERE assessgroupunkid = @assessgroupunkid " & _
             "AND periodunkid = @periodunkid AND isvoid = 0 "
            'S.SANDEEP [23 FEB 2016] -- START {iszeroweightallowed} -- END

            If intUnkid > 0 Then
                strQ &= " AND assigncompetenceunkid <> @assigncompetenceunkid"
            End If

            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAssessGrpId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            objDataOperation.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'S.SANDEEP [29 JAN 2015] -- START
            If iEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @iEmployeeId "
                objDataOperation.AddParameter("@iEmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            End If
            'S.SANDEEP [29 JAN 2015] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_competence_assign_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot void the selected competencies. Reason, They are linked with assessment evaluation.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_competence_assign_master SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidreason = @voidreason" & _
                       ", voiddatetime = @voiddatetime " & _
                   "WHERE assigncompetenceunkid = @assigncompetenceunkid "


            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_competence_assign_master", "assigncompetenceunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Dim objAssignedTran As New clsassess_competence_assign_tran
            objCommonATLog = New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "hrassess_competence_assign_tran", "assigncompetenceunkid", intUnkid)
            objCommonATLog = Nothing
            strQ = "UPDATE hrassess_competence_assign_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE assigncompetencetranunkid = @assigncompetencetranunkid "

            For Each dRow As DataRow In dsList.Tables(0).Rows
                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@assigncompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("assigncompetencetranunkid"))

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_competence_assign_tran", "assigncompetencetranunkid", dRow.Item("assigncompetencetranunkid")) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            Next
            objAssignedTran = Nothing

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [29 JAN 2015] -- START
            strQ = "SELECT " & _
                   "    1 " & _
                   "FROM hrcompetency_analysis_tran " & _
                   "    JOIN hrevaluation_analysis_master ON hrcompetency_analysis_tran.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "    JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   "    JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   "        AND hrassess_competence_assign_master.periodunkid = hrevaluation_analysis_master.periodunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrassess_competence_assign_tran.assigncompetenceunkid = @assigncompetenceunkid "
            'S.SANDEEP [29 JAN 2015] -- END

            objDataOperation.AddParameter("@assigncompetenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry this group is already defined for the selected period. Please define new group.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot void the selected competencies. Reason, They are linked with assessment evaluation.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
