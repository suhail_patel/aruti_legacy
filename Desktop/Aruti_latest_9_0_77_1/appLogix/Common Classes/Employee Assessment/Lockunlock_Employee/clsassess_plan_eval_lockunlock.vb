﻿'************************************************************************************************************************************
'Class Name :clsassess_plan_eval_lockunlock.vb
'Purpose    :
'Date       :09-Jan-2019
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_plan_eval_lockunlock
    Private Shared ReadOnly mstrModuleName As String = "clsassess_plan_eval_lockunlock"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLockunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mdtLockunlockdatetime As Date = Nothing
    Private mintLockuserunkid As Integer = 0
    Private mintUnlockuserunkid As Integer = 0
    Private mblnIslock As Boolean = False
    Private mintLoginemployeeunkid As Integer = 0
    Private mblnIsunlocktenure As Boolean = False
    Private mintUnlockdays As Integer = 0
    Private mstrUnlockreason As String = String.Empty
    Private mdtNextlockdatetime As Date = Nothing
    Private mintLocktypeid As Integer = 0
    Private mintAssessmodeid As Integer = 0
    Private mintAudittype As Integer = 0
    Private mintAudituserunkid As Integer = 0
    Private mdtAuditdatetime As Date = Nothing
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean = False
    Private mintAssessorMasterunkid As Integer = 0

#End Region

#Region " Enum "

    Public Enum enAssementLockType
        PLANNING = 1
        ASSESSMENT = 2
    End Enum

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Lockunkid() As Object
        Get
            Return mintLockunkid
        End Get
        Set(ByVal value As Object)
            mintLockunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockunlockdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Lockunlockdatetime() As Date
        Get
            Return mdtLockunlockdatetime
        End Get
        Set(ByVal value As Date)
            mdtLockunlockdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Lockuserunkid() As Integer
        Get
            Return mintLockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintLockuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unlockuserunkid() As Integer
        Get
            Return mintUnlockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintUnlockuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set islock
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Islock() As Boolean
        Get
            Return mblnIslock
        End Get
        Set(ByVal value As Boolean)
            mblnIslock = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isunlocktenure
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isunlocktenure() As Boolean
        Get
            Return mblnIsunlocktenure
        End Get
        Set(ByVal value As Boolean)
            mblnIsunlocktenure = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockdays
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unlockdays() As Integer
        Get
            Return mintUnlockdays
        End Get
        Set(ByVal value As Integer)
            mintUnlockdays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unlockreason() As String
        Get
            Return mstrUnlockreason
        End Get
        Set(ByVal value As String)
            mstrUnlockreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nextlockdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Nextlockdatetime() As Date
        Get
            Return mdtNextlockdatetime
        End Get
        Set(ByVal value As Date)
            mdtNextlockdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set locktypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Locktypeid() As Integer
        Get
            Return mintLocktypeid
        End Get
        Set(ByVal value As Integer)
            mintLocktypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmodeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assessmodeid() As Integer
        Get
            Return mintAssessmodeid
        End Get
        Set(ByVal value As Integer)
            mintAssessmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _AssessorMasterunkid() As Integer
        Get
            Return mintAssessorMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessorMasterunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  lockunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", lockunlockdatetime " & _
              ", lockuserunkid " & _
              ", unlockuserunkid " & _
              ", islock " & _
              ", loginemployeeunkid " & _
              ", isunlocktenure " & _
              ", unlockdays " & _
              ", unlockreason " & _
              ", nextlockdatetime " & _
              ", locktypeid " & _
              ", assessmodeid " & _
              ", ISNULL(assessormasterunkid,0) AS assessormasterunkid " & _
             "FROM hrassess_plan_eval_lockunlock " & _
             "WHERE lockunkid = @lockunkid "

            objDataOperation.AddParameter("@lockunkid", SqlDbType.BigInt, eZeeDataType.INT_SIZE, mintLockunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLockunkid = dtRow.Item("lockunkid")
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtLockunlockdatetime = dtRow.Item("lockunlockdatetime")
                mintLockuserunkid = CInt(dtRow.Item("lockuserunkid"))
                mintUnlockuserunkid = CInt(dtRow.Item("unlockuserunkid"))
                mblnIslock = CBool(dtRow.Item("islock"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsunlocktenure = CBool(dtRow.Item("isunlocktenure"))
                mintUnlockdays = CInt(dtRow.Item("unlockdays"))
                mstrUnlockreason = dtRow.Item("unlockreason").ToString
                If IsDBNull(dtRow.Item("nextlockdatetime")) = False Then
                    mdtNextlockdatetime = dtRow.Item("nextlockdatetime")
                Else
                    mdtNextlockdatetime = Nothing
                End If
                mintLocktypeid = CInt(dtRow.Item("locktypeid"))
                mintAssessmodeid = CInt(dtRow.Item("assessmodeid"))
                mintAssessorMasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If blnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
                   "  hrassess_plan_eval_lockunlock.lockunkid " & _
                   ", hrassess_plan_eval_lockunlock.periodunkid " & _
                   ", hrassess_plan_eval_lockunlock.employeeunkid " & _
                   ", hrassess_plan_eval_lockunlock.lockunlockdatetime " & _
                   ", hrassess_plan_eval_lockunlock.lockuserunkid " & _
                   ", hrassess_plan_eval_lockunlock.unlockuserunkid " & _
                   ", hrassess_plan_eval_lockunlock.islock " & _
                   ", hrassess_plan_eval_lockunlock.loginemployeeunkid " & _
                   ", hrassess_plan_eval_lockunlock.isunlocktenure " & _
                   ", hrassess_plan_eval_lockunlock.unlockdays " & _
                   ", hrassess_plan_eval_lockunlock.unlockreason " & _
                   ", hrassess_plan_eval_lockunlock.nextlockdatetime " & _
                   ", hrassess_plan_eval_lockunlock.locktypeid " & _
                   ", hrassess_plan_eval_lockunlock.assessmodeid " & _
                   ", hremployee_master.employeecode " & _
                   ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                   ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS oName " & _
                   ", CASE WHEN hrassess_plan_eval_lockunlock.locktypeid = '" & enAssementLockType.PLANNING & "' THEN @PLANNING " & _
                   "       WHEN hrassess_plan_eval_lockunlock.locktypeid = '" & enAssementLockType.ASSESSMENT & "' THEN @ASSESSMENT " & _
                   "  END AS locktype " & _
                   ", CASE WHEN hrassess_plan_eval_lockunlock.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN @SELF_ASSESSMENT " & _
                   "       WHEN hrassess_plan_eval_lockunlock.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' THEN @APPRAISER_ASSESSMENT " & _
                   "       WHEN hrassess_plan_eval_lockunlock.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' THEN @REVIEWER_ASSESSMENT " & _
                   "  END AS assessmenttype " & _
                   ", CASE WHEN unlockdays <= 0 THEN '' ELSE CAST(unlockdays AS NVARCHAR(max)) END AS udays " & _
                   ", hrassess_plan_eval_lockunlock.assessormasterunkid " & _
                   ", hremployee_master.email AS emp_email " & _
                   "FROM hrassess_plan_eval_lockunlock " & _
                   "  JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_plan_eval_lockunlock.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            strQ &= " WHERE isunlocktenure = 0 "

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            objDataOperation.AddParameter("@PLANNING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Performance Planning"))
            objDataOperation.AddParameter("@ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Performance Assessment"))
            objDataOperation.AddParameter("@SELF_ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Self Assessment"))
            objDataOperation.AddParameter("@APPRAISER_ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Appraiser Assessment"))
            objDataOperation.AddParameter("@REVIEWER_ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Reviewer Assessment"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_plan_eval_lockunlock) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
            objDataOperation.AddParameter("@unlockdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockdays.ToString)
            objDataOperation.AddParameter("@unlockreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrUnlockreason.ToString)
            If mdtNextlockdatetime <> Nothing Then
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime)
            Else
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@locktypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLocktypeid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterunkid)

            strQ = "INSERT INTO hrassess_plan_eval_lockunlock ( " & _
                       "  periodunkid " & _
                       ", employeeunkid " & _
                       ", lockunlockdatetime " & _
                       ", lockuserunkid " & _
                       ", unlockuserunkid " & _
                       ", islock " & _
                       ", loginemployeeunkid " & _
                       ", isunlocktenure " & _
                       ", unlockdays " & _
                       ", unlockreason " & _
                       ", nextlockdatetime " & _
                       ", locktypeid " & _
                       ", assessmodeid" & _
                       ", assessormasterunkid" & _
                   ") VALUES (" & _
                       "  @periodunkid " & _
                       ", @employeeunkid " & _
                       ", @lockunlockdatetime " & _
                       ", @lockuserunkid " & _
                       ", @unlockuserunkid " & _
                       ", @islock " & _
                       ", @loginemployeeunkid " & _
                       ", @isunlocktenure " & _
                       ", @unlockdays " & _
                       ", @unlockreason " & _
                       ", @nextlockdatetime " & _
                       ", @locktypeid " & _
                       ", @assessmodeid" & _
                       ", @assessormasterunkid" & _
                   "); SELECT @@identity "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLockunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATEmployeePlan_Eval_LockUnlock(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_plan_eval_lockunlock) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@lockunkid", SqlDbType.BigInt, eZeeDataType.INT_SIZE, mintLockunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
            objDataOperation.AddParameter("@unlockdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockdays.ToString)
            objDataOperation.AddParameter("@unlockreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrUnlockreason.ToString)
            If mdtNextlockdatetime <> Nothing Then
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime)
            Else
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@locktypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLocktypeid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterunkid)

            strQ = "UPDATE hrassess_plan_eval_lockunlock SET " & _
                   "  periodunkid = @periodunkid" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", lockunlockdatetime = @lockunlockdatetime" & _
                   ", lockuserunkid = @lockuserunkid" & _
                   ", unlockuserunkid = @unlockuserunkid" & _
                   ", islock = @islock" & _
                   ", loginemployeeunkid = @loginemployeeunkid" & _
                   ", isunlocktenure = @isunlocktenure" & _
                   ", unlockdays = @unlockdays" & _
                   ", unlockreason = @unlockreason" & _
                   ", nextlockdatetime = @nextlockdatetime" & _
                   ", locktypeid = @locktypeid" & _
                   ", assessmodeid = @assessmodeid " & _
                   ", assessormasterunkid = @assessormasterunkid " & _
                   "WHERE lockunkid = @lockunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATEmployeePlan_Eval_LockUnlock(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_plan_eval_lockunlock) </purpose>
    Public Function GlobalEmployeeUnlock(ByVal dtData As DataTable) As Boolean
        Try
            For Each dr As DataRow In dtData.Rows

                mintLockunkid = dr("lockunkid")
                mintPeriodunkid = dr("periodunkid")
                mintEmployeeunkid = dr("employeeunkid")
                mdtLockunlockdatetime = dr("lockunlockdatetime")
                mintLockuserunkid = dr("lockuserunkid")
                mintUnlockuserunkid = dr("unlockuserunkid")
                mblnIslock = False
                mintLoginemployeeunkid = dr("loginemployeeunkid")
                mblnIsunlocktenure = dr("isunlocktenure")
                mintUnlockdays = dr("unlockdays")
                mstrUnlockreason = dr("unlockreason")
                mdtNextlockdatetime = dr("nextlockdatetime")
                mintLocktypeid = dr("locktypeid")
                mintAssessmodeid = dr("assessmodeid")
                mintAssessorMasterunkid = dr("assessormasterunkid")

                If Update() = False Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GlobalEmployeeUnlock; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function isValidDate(ByVal iEmpId As Integer, _
                                ByVal intPeriod As Integer, _
                                ByVal eALock As enAssementLockType, _
                                ByVal dtDate As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = True
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "     lockunkid " & _
                   "    ,periodunkid " & _
                   "    ,employeeunkid " & _
                   "    ,nextlockdatetime " & _
                   "FROM hrassess_plan_eval_lockunlock " & _
                   "WHERE employeeunkid = @employeeunkid AND isunlocktenure = 1 AND locktypeid = @locktypeid AND periodunkid = @periodunkid " & _
                   "ORDER BY lockunkid DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpId)
            objDataOperation.AddParameter("@locktypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eALock)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriod)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If IsDBNull(dsList.Tables(0).Rows(0)("nextlockdatetime")) = False Then
                    If dtDate.Date > CDate(dsList.Tables(0).Rows(0)("nextlockdatetime")).Date Then
                        blnFlag = False
                    End If
                End If
            Else
                blnFlag = False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isValidDate; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function isExist(ByVal iEmpId As Integer, _
                            ByVal intPeriod As Integer, _
                            ByVal eALock As enAssementLockType, _
                            ByVal strFilter As String, _
                            ByRef blnIsLock As Boolean, _
                            ByRef iLockId As Integer, _
                            ByRef dtNextDateTime As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  lockunkid " & _
                   ", periodunkid " & _
                   ", employeeunkid " & _
                   ", lockunlockdatetime " & _
                   ", lockuserunkid " & _
                   ", unlockuserunkid " & _
                   ", islock " & _
                   ", loginemployeeunkid " & _
                   ", isunlocktenure " & _
                   ", unlockdays " & _
                   ", unlockreason " & _
                   ", nextlockdatetime " & _
                   ", locktypeid " & _
                   ", assessmodeid " & _
                   "FROM hrassess_plan_eval_lockunlock " & _
                   "WHERE employeeunkid = @employeeunkid AND locktypeid = @locktypeid " & _
                   " AND periodunkid = @periodunkid "

            If strFilter.Trim.Length <= 0 Then
                strQ &= " AND isunlocktenure = 0 "
            Else
                strQ &= " AND " & strFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpId)
            objDataOperation.AddParameter("@locktypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eALock)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriod)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                iLockId = CInt(dsList.Tables(0).Rows(0)("lockunkid"))
                blnIsLock = CBool(dsList.Tables(0).Rows(0)("islock"))
                If IsDBNull(dsList.Tables(0).Rows(0)("nextlockdatetime")) = False Then
                    dtNextDateTime = CDate(dsList.Tables(0).Rows(0)("nextlockdatetime"))
                Else
                    dtNextDateTime = Nothing
                End If
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertATEmployeePlan_Eval_LockUnlock(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lockunkid", SqlDbType.BigInt, eZeeDataType.INT_SIZE, mintLockunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
            objDataOperation.AddParameter("@unlockdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockdays.ToString)
            objDataOperation.AddParameter("@unlockreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrUnlockreason.ToString)
            If mdtNextlockdatetime <> Nothing Then
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtNextlockdatetime.ToString)
            Else
                objDataOperation.AddParameter("@nextlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@locktypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLocktypeid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterunkid)

            strQ = "INSERT INTO athrassess_plan_eval_lockunlock ( " & _
                       "  lockunkid " & _
                       ", periodunkid " & _
                       ", employeeunkid " & _
                       ", lockunlockdatetime " & _
                       ", lockuserunkid " & _
                       ", unlockuserunkid " & _
                       ", islock " & _
                       ", loginemployeeunkid " & _
                       ", isunlocktenure " & _
                       ", unlockdays " & _
                       ", unlockreason " & _
                       ", nextlockdatetime " & _
                       ", locktypeid " & _
                       ", assessmodeid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", hostname " & _
                       ", form_name " & _
                       ", isweb" & _
                       ", assessormasterunkid" & _
                   ") VALUES (" & _
                       "  @lockunkid " & _
                       ", @periodunkid " & _
                       ", @employeeunkid " & _
                       ", @lockunlockdatetime " & _
                       ", @lockuserunkid " & _
                       ", @unlockuserunkid " & _
                       ", @islock " & _
                       ", @loginemployeeunkid " & _
                       ", @isunlocktenure " & _
                       ", @unlockdays " & _
                       ", @unlockreason " & _
                       ", @nextlockdatetime " & _
                       ", @locktypeid " & _
                       ", @assessmodeid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @hostname " & _
                       ", @form_name " & _
                       ", @isweb" & _
                       ", @assessormasterunkid" & _
                   ") "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATEmployeePlan_Eval_LockUnlock; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetLockTypeComboList(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            strQ &= "SELECT '" & enAssementLockType.PLANNING & "' AS Id, @PLANNING AS Name " & _
                    "UNION SELECT '" & enAssementLockType.ASSESSMENT & "' AS Id, @ASSESSMENT AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))
            objDataOperation.AddParameter("@PLANNING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Performance Planning"))
            objDataOperation.AddParameter("@ASSESSMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Performance Assessment"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLockTypeComboList; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    Public Sub SendUnlockNotification(ByVal strEmpName As String, _
                                      ByVal strEmpEmail As String, _
                                      ByVal strPeriod As String, _
                                      ByVal eALockType As enAssementLockType, _
                                      ByVal intCompanyId As Integer, _
                                      ByVal eLMode As enLogin_Mode, _
                                      ByVal strSenderName As String, _
                                      ByVal intUnlockDays As Integer, _
                                      ByVal dtNextLockDate As Date, _
                                      ByVal strFormName As String, _
                                      ByVal strIP As String, _
                                      ByVal intAuditUserId As Integer, _
                                      ByVal dtAuditDate As DateTime, _
                                      ByVal blnIsWeb As Boolean, _
                                      ByVal iLoginEmployeeId As Integer, _
                                      ByVal strHostName As String, _
                                      ByVal strSelfServiceURL As String)
        Dim objMail As New clsSendMail
        Try
            Dim strMessage As String = ""
            Dim strCommonMsg As String = String.Empty
            Select Case eALockType
                Case enAssementLockType.ASSESSMENT
                    objMail._Subject = Language.getMessage(mstrModuleName, 7, "Notification for") & " " & Language.getMessage(mstrModuleName, 2, "Performance Assessment")
                    strCommonMsg = Language.getMessage(mstrModuleName, 2, "Performance Assessment")
                Case enAssementLockType.PLANNING
                    objMail._Subject = Language.getMessage(mstrModuleName, 7, "Notification for") & " " & Language.getMessage(mstrModuleName, 1, "Performance Planning")
                    strCommonMsg = Language.getMessage(mstrModuleName, 1, "Performance Planning")
            End Select
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 8, "Dear") & " <B>" & strEmpName & "</B>, <BR><BR>"


            'Pinkal (22-Mar-2019) -- Start
            'Enhancement - Working on PA Changes FOR NMB.
            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 9, "This is to inform you that,You have been unlocked for the period of") & " "
            'strMessage &= "<B>" & strPeriod & "</B>&nbsp;" & Language.getMessage(mstrModuleName, 10, "for doing") & "&nbsp;" & strCommonMsg & ".&nbsp;"
            strMessage &= Language.getMessage(mstrModuleName, 9, "This is to inform you that,You have been unlocked for the period of") & " "
            strMessage &= "<B>(" & strPeriod & ")</B>&nbsp;" & Language.getMessage(mstrModuleName, 10, "for doing") & "&nbsp;" & strCommonMsg & ".&nbsp;"
            'Pinkal (22-Mar-2019) -- End

            strMessage &= Language.getMessage(mstrModuleName, 11, "Please login to aruti in order to complete ") & "&nbsp;" & strCommonMsg & ".&nbsp;"

            If dtNextLockDate <> Nothing Then
                strMessage &= "<BR><BR>"

                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT FONT COLOR = 'RED'>" & Language.getMessage(mstrModuleName, 12, "NOTE :") & "&nbsp;" & Language.getMessage(mstrModuleName, 13, "You have been provided with period of") & "&nbsp;" & intUnlockDays.ToString() & "&nbsp;" & _
                strMessage &= "<B><FONT FONT COLOR = 'RED'>" & Language.getMessage(mstrModuleName, 12, "NOTE :") & "&nbsp;" & Language.getMessage(mstrModuleName, 13, "You have been provided with period of") & "&nbsp;" & intUnlockDays.ToString() & "&nbsp;"
                'Pinkal (22-Mar-2019) -- End

                strMessage &= Language.getMessage(mstrModuleName, 14, "day(s), If you fail to do") & "&nbsp;" & strCommonMsg & "&nbsp;" & Language.getMessage(mstrModuleName, 15, "within the time period alloted to you. You will be locked again after") & _
                " " & dtNextLockDate.Date.ToShortDateString & "</FONT></B>."
            End If
            strMessage &= "<BR><BR>"
            strMessage &= "<B>" & strSenderName & "</B>"
            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"
            objMail._Message = strMessage
            objMail._ToEmail = strEmpEmail
            objMail._Form_Name = strFormName
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = eLMode
            objMail._UserUnkid = mintAudituserunkid
            objMail._SenderAddress = strSenderName
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
            objMail.SendMail(intCompanyId)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendUnlockNotification; Module Name: " & mstrModuleName)
        Finally
            objMail = Nothing
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Performance Planning")
            Language.setMessage(mstrModuleName, 2, "Performance Assessment")
            Language.setMessage(mstrModuleName, 3, "Self Assessment")
            Language.setMessage(mstrModuleName, 4, "Appraiser Assessment")
            Language.setMessage(mstrModuleName, 5, "Reviewer Assessment")
            Language.setMessage(mstrModuleName, 6, "Select")
            Language.setMessage(mstrModuleName, 7, "Notification for")
            Language.setMessage(mstrModuleName, 8, "Dear")
            Language.setMessage(mstrModuleName, 9, "This is to inform you that,You have been unlocked for the period of")
            Language.setMessage(mstrModuleName, 10, "for")
            Language.setMessage(mstrModuleName, 11, "Please login to aruti in order to")
            Language.setMessage(mstrModuleName, 12, "NOTE :")
            Language.setMessage(mstrModuleName, 13, "You have been provided with period of")
            Language.setMessage(mstrModuleName, 14, "day(s), If you fail to do")
            Language.setMessage(mstrModuleName, 15, "within the time period. you will be locked after")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
