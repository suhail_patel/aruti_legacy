﻿'************************************************************************************************************************************
'Class Name : clsassess_scalemapping_tran.vb
'Purpose    :
'Date       :30-Jul-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_scalemapping_tran
    Private Const mstrModuleName = "clsassess_scalemapping_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintAssignmenttranunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintMappingunkid As Integer = 0
    Private mintScalemasterunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mintPerspectiveunkid As Integer = 0

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assignmenttranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assignmenttranunkid() As Integer
        Get
            Return mintAssignmenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssignmenttranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set scalemasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Scalemasterunkid() As Integer
        Get
            Return mintScalemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintScalemasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perspectiveunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Perspectiveunkid() As Integer
        Get
            Return mintPerspectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintPerspectiveunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assignmenttranunkid " & _
              ", periodunkid " & _
              ", mappingunkid " & _
              ", perspectiveunkid " & _
              ", scalemasterunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrassess_scalemapping_tran " & _
             "WHERE periodunkid = @periodunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssignmenttranunkid = CInt(dtRow.Item("assignmenttranunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintScalemasterunkid = CInt(dtRow.Item("scalemasterunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintPerspectiveunkid = CInt(dtRow.Item("perspectiveunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "     hrassess_scalemapping_tran.assignmenttranunkid " & _
                   "    ,hrassess_scalemapping_tran.periodunkid " & _
                   "    ,hrassess_scalemapping_tran.mappingunkid " & _
                   "    ,hrassess_scalemapping_tran.scalemasterunkid " & _
                   "    ,hrassess_scalemapping_tran.isvoid " & _
                   "    ,hrassess_scalemapping_tran.voiduserunkid " & _
                   "    ,hrassess_scalemapping_tran.voiddatetime " & _
                   "    ,hrassess_scalemapping_tran.voidreason " & _
                   "    ,hrassess_scalemapping_tran.perspectiveunkid " & _
                   "    ,cfcommon_master.name AS Scale_Group " & _
                   "    ,ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                   "    ,cfcommon_period_tran.period_name AS Period " & _
                   "FROM hrassess_scalemapping_tran " & _
                   "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_scalemapping_tran.periodunkid " & _
                   "    LEFT JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_scalemapping_tran.perspectiveunkid " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_scalemapping_tran.scalemasterunkid " & _
                   "WHERE hrassess_scalemapping_tran.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_scalemapping_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScalemasterunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid)

            strQ = "INSERT INTO hrassess_scalemapping_tran ( " & _
                      "  periodunkid " & _
                      ", mappingunkid " & _
                      ", scalemasterunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", perspectiveunkid " & _
                   ") VALUES (" & _
                      "  @periodunkid " & _
                      ", @mappingunkid " & _
                      ", @scalemasterunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @perspectiveunkid " & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssignmenttranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_scalemapping_tran", "assignmenttranunkid", mintAssignmenttranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_scalemapping_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@assignmenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssignmenttranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScalemasterunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid)

            strQ = "UPDATE hrassess_scalemapping_tran SET " & _
                      "  periodunkid = @periodunkid" & _
                      ", mappingunkid = @mappingunkid" & _
                      ", scalemasterunkid = @scalemasterunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", perspectiveunkid = @perspectiveunkid " & _
                   "WHERE assignmenttranunkid = @assignmenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_scalemapping_tran", mintAssignmenttranunkid, "assignmenttranunkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_scalemapping_tran", "assignmenttranunkid", mintAssignmenttranunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xPeriodId As Integer, _
                            Optional ByVal xPerspectiveId As Integer = 0, _
                            Optional ByVal xScaleGrp As Integer = 0, _
                            Optional ByVal xUnkid As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT 1 FROM hrassess_scalemapping_tran WHERE isvoid = 0 "

            'S.SANDEEP [23 MAR 2015] -- START
            'If xPerspectiveId > 0 Then
            '    strQ &= " AND perspectiveunkid = 0 "    'CHECK IF ANY MAPPING IS THERE WITH OUT PERSPECTIVE (COMMON SCALE FOR ALL ITEMS)
            'ElseIf xScaleGrp > 0 Then
            '    strQ &= " AND perspectiveunkid > 0 "    'CHECK IF ANY MAPPING IS THERE WITH ANY OF PERSPECTIVE (SPECIFIC SCALE FOR ITEMS)
            'End If

            'If xPerspectiveId > 0 AndAlso xScaleGrp > 0 Then
            '    strQ &= " AND perspectiveunkid = '" & xPerspectiveId & "' AND scalemasterunkid = '" & xScaleGrp & "'"
            'End If

            If xPeriodId > 0 Then
                strQ &= " AND periodunkid = '" & xPeriodId & "' "
            End If

            If xPerspectiveId > 0 Then
                strQ &= " AND perspectiveunkid = '" & xPerspectiveId & "' "
            End If

            'S.SANDEEP [12 MAY 2015] -- START
            'If xScaleGrp > 0 Then
            '    strQ &= " AND scalemasterunkid > '" & xScaleGrp & "' "
            'End If
            If xScaleGrp > 0 Then
                strQ &= " AND scalemasterunkid = '" & xScaleGrp & "' "
            End If
            'S.SANDEEP [12 MAY 2015] -- END

            

            'S.SANDEEP [23 MAR 2015] -- END

            If xUnkid > 0 Then
                strQ &= " AND assignmenttranunkid <> '" & xUnkid & "' "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_scalemapping_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_scalemapping_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE assignmenttranunkid = @assignmenttranunkid "

            objDataOperation.AddParameter("@assignmenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_scalemapping_tran", "assignmenttranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class
