﻿'************************************************************************************************************************************
'Class Name : clsexternal_assessor_master.vb
'Purpose    :
'Date       :16/01/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsexternal_assessor_master
    Private Shared ReadOnly mstrModuleName As String = "clsexternal_assessor_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintExt_Assessorunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintRelationunkid As Integer
    Private mstrDisplayname As String = String.Empty
    Private mstrAssessorname As String = String.Empty
    Private mstrAddress As String = String.Empty
    Private mintCityunkid As Integer
    Private mintStateunkid As Integer
    Private mintCountryunkid As Integer
    Private mstrEmail As String = String.Empty
    Private mstrPhone As String = String.Empty
    Private mstrCompany As String = String.Empty
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ext_assessorunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Ext_Assessorunkid() As Integer
        Get
            Return mintExt_Assessorunkid
        End Get
        Set(ByVal value As Integer)
            mintExt_Assessorunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relationunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Relationunkid() As Integer
        Get
            Return mintRelationunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set displayname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Displayname() As String
        Get
            Return mstrDisplayname
        End Get
        Set(ByVal value As String)
            mstrDisplayname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessorname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Assessorname() As String
        Get
            Return mstrAssessorname
        End Get
        Set(ByVal value As String)
            mstrAssessorname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Phone() As String
        Get
            Return mstrPhone
        End Get
        Set(ByVal value As String)
            mstrPhone = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Company() As String
        Get
            Return mstrCompany
        End Get
        Set(ByVal value As String)
            mstrCompany = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  ext_assessorunkid " & _
              ", employeeunkid " & _
              ", relationunkid " & _
              ", displayname " & _
              ", assessorname " & _
              ", address " & _
              ", cityunkid " & _
              ", stateunkid " & _
              ", countryunkid " & _
              ", email " & _
              ", phone " & _
              ", company " & _
              ", isactive " & _
             "FROM hrexternal_assessor_master " & _
             "WHERE ext_assessorunkid = @ext_assessorunkid "

            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintExt_assessorUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintext_assessorunkid = CInt(dtRow.Item("ext_assessorunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintrelationunkid = CInt(dtRow.Item("relationunkid"))
                mstrdisplayname = dtRow.Item("displayname").ToString
                mstrassessorname = dtRow.Item("assessorname").ToString
                mstraddress = dtRow.Item("address").ToString
                mintcityunkid = CInt(dtRow.Item("cityunkid"))
                mintstateunkid = CInt(dtRow.Item("stateunkid"))
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mstremail = dtRow.Item("email").ToString
                mstrphone = dtRow.Item("phone").ToString
                mstrcompany = dtRow.Item("company").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrexternal_assessor_master.ext_assessorunkid " & _
              ", hrexternal_assessor_master.employeeunkid " & _
              ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') as employee " & _
              ", hrexternal_assessor_master.relationunkid " & _
              ", cfcommon_master. name as relation " & _
              ", hrexternal_assessor_master.displayname " & _
              ", hrexternal_assessor_master.assessorname " & _
              ", hrexternal_assessor_master.address " & _
              ", hrexternal_assessor_master.cityunkid " & _
              ", hrexternal_assessor_master.stateunkid " & _
              ", hrexternal_assessor_master.countryunkid " & _
              ", hrexternal_assessor_master.email " & _
              ", hrexternal_assessor_master.phone " & _
              ", hrexternal_assessor_master.company " & _
              ", hrexternal_assessor_master.isactive " & _
             " FROM hrexternal_assessor_master " & _
             " JOIN hremployee_master on hremployee_master.employeeunkid = hrexternal_assessor_master.employeeunkid " & _
             " JOIN cfcommon_master on cfcommon_master.masterunkid= hrexternal_assessor_master.relationunkid and mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS


            If blnOnlyActive Then
                strQ &= " WHERE hrexternal_assessor_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrexternal_assessor_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrDisplayname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Display Name is already defined. Please define new Display Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)
            objDataOperation.AddParameter("@displayname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisplayname.ToString)
            objDataOperation.AddParameter("@assessorname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssessorname.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone.ToString)
            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO hrexternal_assessor_master ( " & _
              "  employeeunkid " & _
              ", relationunkid " & _
              ", displayname " & _
              ", assessorname " & _
              ", address " & _
              ", cityunkid " & _
              ", stateunkid " & _
              ", countryunkid " & _
              ", email " & _
              ", phone " & _
              ", company " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @relationunkid " & _
              ", @displayname " & _
              ", @assessorname " & _
              ", @address " & _
              ", @cityunkid " & _
              ", @stateunkid " & _
              ", @countryunkid " & _
              ", @email " & _
              ", @phone " & _
              ", @company " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintExt_Assessorunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrexternal_assessor_master", "ext_assessorunkid", mintExt_Assessorunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrexternal_assessor_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrDisplayname, mintExt_Assessorunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Display Name is already defined. Please define new Display Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)
            objDataOperation.AddParameter("@displayname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisplayname.ToString)
            objDataOperation.AddParameter("@assessorname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssessorname.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone.ToString)
            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE hrexternal_assessor_master SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", relationunkid = @relationunkid" & _
                      ", displayname = @displayname" & _
                      ", assessorname = @assessorname" & _
                      ", address = @address" & _
                      ", cityunkid = @cityunkid" & _
                      ", stateunkid = @stateunkid" & _
                      ", countryunkid = @countryunkid" & _
                      ", email = @email" & _
                      ", phone = @phone" & _
                      ", company = @company" & _
                      ", isactive = @isactive " & _
                      "  WHERE ext_assessorunkid = @ext_assessorunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrexternal_assessor_master", mintExt_Assessorunkid, "ext_assessorunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrexternal_assessor_master", "ext_assessorunkid", mintExt_Assessorunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrexternal_assessor_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            strQ = " update hrexternal_assessor_master set isactive = 0 " & _
                       " WHERE ext_assessorunkid = @ext_assessorunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrexternal_assessor_master", "ext_assessorunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "TABLE_NAME AS TableName " & _
                "FROM INFORMATION_SCHEMA.COLUMNS " & _
                "WHERE COLUMN_NAME='ext_assessorunkid' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName") = "hrexternal_assessor_master" Then Continue For

                strQ = "SELECT ext_assessorunkid FROM " & dtRow.Item("TableName").ToString & " WHERE ext_assessorunkid = @ext_assessorunkid  and isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If

            Next

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strDisplayName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  ext_assessorunkid " & _
              ", employeeunkid " & _
              ", relationunkid " & _
              ", displayname " & _
              ", assessorname " & _
              ", address " & _
              ", cityunkid " & _
              ", stateunkid " & _
              ", countryunkid " & _
              ", email " & _
              ", phone " & _
              ", company " & _
              ", isactive " & _
             "FROM hrexternal_assessor_master " & _
             "WHERE DisplayName = @displayName AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND ext_assessorunkid <> @ext_assessorunkid"
            End If

            objDataOperation.AddParameter("@displayName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDisplayName)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetDisplayNameComboList(Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal Employeeunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If


            strQ &= "SELECT " & _
              "  ext_assessorunkid " & _
              ", displayname " & _
              "FROM hrexternal_assessor_master " & _
             "WHERE isactive = 1 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            If Employeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDisplayNameComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetExternalAssessorComboList(Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal Employeeunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If


            strQ &= "SELECT " & _
              "  ext_assessorunkid " & _
              ", assessorname " & _
              "FROM hrexternal_assessor_master " & _
             "WHERE isactive = 1 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            If Employeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalAssessorComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetEmpBasedOnExtAssessor(ByVal intExtAssessorId As Integer, Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id, @Select AS name UNION "
            End If

            strQ &= "SELECT " & _
                            " hremployee_master.employeeunkid AS Id " & _
                            ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS  name " & _
                      "FROM hrexternal_assessor_master " & _
                            "JOIN hremployee_master ON hrexternal_assessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                      "WHERE hrexternal_assessor_master.isactive = 1 AND ext_assessorunkid = '" & intExtAssessorId & "'"

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpBasedOnExtAssessor; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Display Name is already defined. Please define new Display Name.")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class