﻿'************************************************************************************************************************************
'Class Name : clshr_images_tran.vb
'Purpose    :
'Date       :01/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshr_images_tran
    Private Const mstrModuleName = "clshr_images_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintImagetranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrImagename As String = String.Empty
    Private mintTransactionid As Integer
    Private mintReferenceid As Integer
    Private mblnIsapplicant As Boolean

    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Private xDataOperation As clsDataOperation = Nothing
    'S.SANDEEP [19 OCT 2016] -- END

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set imagetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Imagetranunkid() As Integer
        Get
            Return mintImagetranunkid
        End Get
        Set(ByVal value As Integer)
            mintImagetranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set imagename
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Imagename() As String
        Get
            Return mstrImagename
        End Get
        Set(ByVal value As String)
            mstrImagename = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactionid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Transactionid() As Integer
        Get
            Return mintTransactionid
        End Get
        Set(ByVal value As Integer)
            mintTransactionid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referenceid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Referenceid() As Integer
        Get
            Return mintReferenceid
        End Get
        Set(ByVal value As Integer)
            mintReferenceid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapplicant
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isapplicant() As Boolean
        Get
            Return mblnIsapplicant
        End Get
        Set(ByVal value As Boolean)
            mblnIsapplicant = Value
        End Set
    End Property


    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Public WriteOnly Property _xDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOperation = value
        End Set
    End Property
    'S.SANDEEP [19 OCT 2016] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END

        Try
            strQ = "SELECT " & _
              "  imagetranunkid " & _
              ", employeeunkid " & _
              ", imagename " & _
              ", transactionid " & _
              ", referenceid " & _
              ", isapplicant " & _
             "FROM hr_images_tran " & _
             "WHERE employeeunkid = @imagetranunkid " & _
             "AND referenceid = @referenceid " & _
             "AND transactionid = @transactionid "


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If mintImagetranunkid > 0 Then
                strQ &= " AND imagetranunkid = @tranunkid "
                objDataOperation.AddParameter("@tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImagetranunkid.ToString)
            End If

            strQ &= "order by imagetranunkid desc"
            'Gajanan [22-Feb-2019] -- End


            objDataOperation.AddParameter("@imagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceid.ToString)
            objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrImagename = ""

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintimagetranunkid = CInt(dtRow.Item("imagetranunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrimagename = dtRow.Item("imagename").ToString
                minttransactionid = CInt(dtRow.Item("transactionid"))
                mintreferenceid = CInt(dtRow.Item("referenceid"))
                mblnisapplicant = CBool(dtRow.Item("isapplicant"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  imagetranunkid " & _
                      ", employeeunkid " & _
                      ", imagename " & _
                      ", transactionid " & _
                      ", referenceid " & _
                      ", isapplicant " & _
                     "FROM hr_images_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hr_images_tran) </purpose>
    Public Function Insert(ByVal xDataOperation As clsDataOperation, Optional ByVal isRemoveOldImage As Boolean = True) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        If isRemoveOldImage Then
        Call Delete(xDataOperation)
        End If
        'Gajanan [22-Feb-2019] -- End


        Try
            'S.SANDEEP [17 JUN 2016] -- START
            xDataOperation.ClearParameters()
            'S.SANDEEP [17 JUN 2016] -- END
            xDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            xDataOperation.AddParameter("@imagename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImagename.ToString)
            xDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionid.ToString)
            xDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceid.ToString)
            xDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)

            strQ = "INSERT INTO hr_images_tran ( " & _
                        "  employeeunkid " & _
                        ", imagename " & _
                        ", transactionid " & _
                        ", referenceid " & _
                        ", isapplicant" & _
                    ") VALUES (" & _
                        "  @employeeunkid " & _
                        ", @imagename " & _
                        ", @transactionid " & _
                        ", @referenceid " & _
                        ", @isapplicant" & _
                    "); SELECT @@identity"


            'S.SANDEEP [17 JUN 2016] -- START
            'dsList = objDataOperation.ExecQuery(strQ, "List")
            dsList = xDataOperation.ExecQuery(strQ, "List")
            'S.SANDEEP [17 JUN 2016] -- END




            If xDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(xDataOperation.ErrorNumber & ": " & xDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintImagetranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    'Public Function Insert() As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Call Delete()

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@imagename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImagename.ToString)
    '        objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionid.ToString)
    '        objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceid.ToString)
    '        objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)

    '        strQ = "INSERT INTO hr_images_tran ( " & _
    '                    "  employeeunkid " & _
    '                    ", imagename " & _
    '                    ", transactionid " & _
    '                    ", referenceid " & _
    '                    ", isapplicant" & _
    '                ") VALUES (" & _
    '                    "  @employeeunkid " & _
    '                    ", @imagename " & _
    '                    ", @transactionid " & _
    '                    ", @referenceid " & _
    '                    ", @isapplicant" & _
    '                "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintImagetranunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hr_images_tran) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'objDataOperation = New clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [22-Feb-2019] -- End

        Try
            objDataOperation.AddParameter("@imagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImagetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@imagename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImagename.ToString)
            objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionid.ToString)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceid.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapplicant.ToString)

            strQ = "UPDATE hr_images_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", imagename = @imagename" & _
                      ", transactionid = @transactionid" & _
                      ", referenceid = @referenceid" & _
                      ", isapplicant = @isapplicant " & _
                    "WHERE imagetranunkid = @imagetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End      
        End Try
    End Function

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.


    'Public Function Delete(ByVal xDataOpr As clsDataOperation) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END
    Public Function Delete(ByVal xDataOpr As clsDataOperation, Optional ByVal isfromapproval As Boolean = True) As Boolean
        'Gajanan [22-Feb-2019] -- End

        'Public Function Delete() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr IsNot Nothing Then
                xDataOpr = New clsDataOperation
            End If
            xDataOpr.ClearParameters()
            'Gajanan [22-Feb-2019] -- End

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ''Shani(24-Aug-2015) -- Start
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151104)
            ''xDataOpr.ClearParameters()
            ''Shani(24-Aug-2015) -- End


            strQ = "DELETE FROM hr_images_tran " & _
                    "WHERE employeeunkid = @employeeunkid " & _
                    " AND transactionid = @transactionid " & _
                    " AND referenceid = @referenceid "

            If isfromapproval = False Then
                strQ &= " AND imagetranunkid = @imagetranunkid "
                xDataOpr.AddParameter("@imagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImagetranunkid.ToString)
            End If


            xDataOpr.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            xDataOpr.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionid.ToString)
            xDataOpr.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceid.ToString)

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@imagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  imagetranunkid " & _
              ", employeeunkid " & _
              ", imagename " & _
              ", transactionid " & _
              ", referenceid " & _
              ", isapplicant " & _
             "FROM hr_images_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND imagetranunkid <> @imagetranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@imagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class