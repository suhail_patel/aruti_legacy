﻿'************************************************************************************************************************************
'Class Name : clsPdpgoals_update_Tran.vb
'Purpose    :
'Date       :30-01-2021
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsPdpgoals_update_Tran
    Private Const mstrModuleName = "clsPdpgoals_update_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintUpdatetranunkid As Integer
    Private mintPdpgoalsmstunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintStatusid As Integer = -1
    Private mdtUpdatedatetime As Date
    Private msinPct_Updated As Single
    Private msinLast_Pct_Update As Single
    Private mstrRemarks As String = String.Empty

    Private mintApprovalstatusid As Integer
    Private mdtApprovaldatetime As Date
    Private mstrApprovalcomments As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoidloginemployeeunkid As Integer
    Private mintUserunkid As Integer


    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set updatetranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Updatetranunkid() As Integer
        Get
            Return mintUpdatetranunkid
        End Get
        Set(ByVal value As Integer)
            mintUpdatetranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pdpgoalsmstunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Pdpgoalsmstunkid() As Integer
        Get
            Return mintPdpgoalsmstunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpgoalsmstunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    Public Property _StatusId() As Integer
        Get
            Return mintStatusid
        End Get
        Set(ByVal value As Integer)
            mintStatusid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set updatedatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Updatedatetime() As Date
        Get
            Return mdtUpdatedatetime
        End Get
        Set(ByVal value As Date)
            mdtUpdatedatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_updated
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Pct_Updated() As Single
        Get
            Return msinPct_Updated
        End Get
        Set(ByVal value As Single)
            msinPct_Updated = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set last_pct_update
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Last_Pct_Update() As Single
        Get
            Return msinLast_Pct_Update
        End Get
        Set(ByVal value As Single)
            msinLast_Pct_Update = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalstatusid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalstatusid() As Integer
        Get
            Return mintApprovalstatusid
        End Get
        Set(ByVal value As Integer)
            mintApprovalstatusid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvaldatetime() As Date
        Get
            Return mdtApprovaldatetime
        End Get
        Set(ByVal value As Date)
            mdtApprovaldatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalcomments
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalcomments() As String
        Get
            Return mstrApprovalcomments
        End Get
        Set(ByVal value As String)
            mstrApprovalcomments = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  updatetranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", employeeunkid " & _
              ", updatedatetime " & _
              ", pct_updated " & _
              ", last_pct_update " & _
              ", remarks " & _
              ", approvalstatusid " & _
              ", approvaldatetime " & _
              ", approvalcomments " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voidloginemployeeunkid " & _
              ", userunkid " & _
             "FROM pdpgoals_update_tran " & _
             "WHERE updatetranunkid = @updatetranunkid "

            objDataOperation.AddParameter("@updatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUpdatetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintUpdatetranunkid = CInt(dtRow.Item("updatetranunkid"))
                mintPdpgoalsmstunkid = CInt(dtRow.Item("pdpgoalsmstunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtUpdatedatetime = dtRow.Item("updatedatetime")
                msinPct_Updated = dtRow.Item("pct_updated")
                msinLast_Pct_Update = dtRow.Item("last_pct_update")
                mstrRemarks = dtRow.Item("remarks").ToString
                mintApprovalstatusid = CInt(dtRow.Item("approvalstatusid"))
                mdtApprovaldatetime = dtRow.Item("approvaldatetime")
                mstrApprovalcomments = dtRow.Item("approvalcomments").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        Try
            strQ = "SELECT " & _
              "  updatetranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", employeeunkid " & _
              ", updatedatetime " & _
              ", pct_updated " & _
              ", last_pct_update " & _
              ", remarks " & _
              ", approvalstatusid " & _
              ", approvaldatetime " & _
              ", approvalcomments " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voidloginemployeeunkid " & _
              ", userunkid " & _
              ", CASE WHEN statusid = " & enPDPActionPlanStatus.Pending & " THEN @Pending " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.Initiated & " THEN @Initiated " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.InProgress & " THEN @InProgress " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.Rotting & " THEN @Rotting " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.Stopped & " THEN @Stopped " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.OnEvaluation & " THEN @OnEvaluation " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.Completed & " THEN @Completed " & _
              "       WHEN statusid = " & enPDPActionPlanStatus.Achieved & " THEN @Achieved " & _
              "       ELSE - " & _
              "  END as Status" & _
              ", statusid " & _
             "FROM pdpgoals_update_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1094, "Pending"))
            objDataOperation.AddParameter("@Initiated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1095, "Initiated"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1096, "InProgress"))
            objDataOperation.AddParameter("@Rotting", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1097, "Rotting"))
            objDataOperation.AddParameter("@Stopped", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1098, "Stopped"))
            objDataOperation.AddParameter("@OnEvaluation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1099, "OnEvaluation"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1100, "Completed"))
            objDataOperation.AddParameter("@Achieved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1101, "Achieved"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusid.ToString)
            objDataOperation.AddParameter("@updatedatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUpdatedatetime.ToString)
            objDataOperation.AddParameter("@pct_updated", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinPct_Updated.ToString)
            objDataOperation.AddParameter("@last_pct_update", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinLast_Pct_Update.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@approvalstatusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalstatusid.ToString)
            objDataOperation.AddParameter("@approvaldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldatetime.ToString)
            objDataOperation.AddParameter("@approvalcomments", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalcomments.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "INSERT INTO pdpgoals_update_tran ( " & _
              "  pdpgoalsmstunkid " & _
              ", employeeunkid " & _
              ", statusid " & _
              ", updatedatetime " & _
              ", pct_updated " & _
              ", last_pct_update " & _
              ", remarks " & _
              ", approvalstatusid " & _
              ", approvaldatetime " & _
              ", approvalcomments " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voidloginemployeeunkid " & _
              ", userunkid" & _
            ") VALUES (" & _
              "  @pdpgoalsmstunkid " & _
              ", @employeeunkid " & _
              ", @statusid " & _
              ", @updatedatetime " & _
              ", @pct_updated " & _
              ", @last_pct_update " & _
              ", @remarks " & _
              ", @approvalstatusid " & _
              ", @approvaldatetime " & _
              ", @approvalcomments " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @voidloginemployeeunkid " & _
              ", @userunkid" & _
            "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintUpdatetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintUpdatetranunkid, -1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteAll(ByVal intPdpgoalsmstunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid)

            strQ = "UPDATE pdpgoals_update_tran SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, -1, intPdpgoalsmstunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function


    Public Function DeleteLastProgress(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@updatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            strQ = "UPDATE pdpgoals_update_tran SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE updatetranunkid = @updatetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intUnkid, -1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteLastProgress; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      ByVal intunkid As Integer, _
                                      ByVal intPdpgoalsmstunkid As Integer) As Boolean

        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpgoals_update_tran ( " & _
                    " tranguid " & _
                    ",updatetranunkid " & _
                    ",pdpgoalsmstunkid " & _
                    ",employeeunkid " & _
                    ",statusid " & _
                    ",updatedatetime " & _
                    ",pct_updated " & _
                    ",last_pct_update " & _
                    ",remarks " & _
                    ",approvalstatusid " & _
                    ",approvaldatetime " & _
                    ",approvalcomments " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ") select " & _
                    "  LOWER(NEWID()) " & _
                    ",updatetranunkid " & _
                    ",pdpgoalsmstunkid " & _
                    ",employeeunkid " & _
                    ",statusid " & _
                    ",updatedatetime " & _
                    ",pct_updated " & _
                    ",last_pct_update " & _
                    ",remarks " & _
                    ",approvalstatusid " & _
                    ",approvaldatetime " & _
                    ",approvalcomments " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb " & _
                    "  From " & mstrDatabaseName & "..pdpgoals_update_tran WHERE 1=1  "

            objDataOperation.ClearParameters()


            If intunkid > 0 Then
                StrQ &= " and updatetranunkid=@updatetranunkid "
                objDataOperation.AddParameter("@updatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUpdatetranunkid)
            End If

            If intPdpgoalsmstunkid > 0 Then
                StrQ &= " and pdpgoalsmstunkid=@pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid)
            End If


            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

End Class