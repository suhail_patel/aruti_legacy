﻿Imports eZeeCommonLib

Public Class clspdp_categoryitem_mapping
    Private Shared ReadOnly mstrModuleName As String = "clspdp_categoryitem_mapping"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintCategoryItemMappingid As Integer
    Private mintCategoryId As Integer = -1
    Private mintItemId As Integer = -1

    Private mblnIsVoid As Boolean = False
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property


    Public Property _CategoryItemMappingid() As Integer
        Get
            Return mintCategoryItemMappingid
        End Get
        Set(ByVal value As Integer)
            mintCategoryItemMappingid = value
            GetData()
        End Set
    End Property

    Public Property _CategoryId() As Integer
        Get
            Return mintCategoryId
        End Get
        Set(ByVal value As Integer)
            mintCategoryId = value
        End Set
    End Property

    Public Property _ItemId() As Integer
        Get
            Return mintItemId
        End Get
        Set(ByVal value As Integer)
            mintItemId = value
        End Set
    End Property

    
    Public Property _IsVoid() As Boolean
        Get
            Return mblnIsVoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsVoid = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  categoryitemmappingid " & _
              ", categoryid " & _
              ", itemid " & _
              ", isvoid " & _
             "FROM pdpcategory_item_mapping " & _
             "WHERE categoryitemmappingid = @categoryitemmappingid "

            objDataOperation.AddParameter("@categoryitemmappingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryItemMappingid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCategoryItemMappingid = CInt(dtRow.Item("categoryitemmappingid"))
                mintCategoryId = dtRow.Item("categoryid").ToString
                mintItemId = CInt(dtRow.Item("itemid"))
                mblnIsVoid = CBool(dtRow.Item("isvoid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  pdpcategory_item_mapping.*,isnull(pdpcategory_master.category,'') as  category,isnull(pdpitem_master.item,'') as item " & _
              " FROM " & mstrDatabaseName & "..pdpcategory_item_mapping " & _
             "left join pdpcategory_master on pdpcategory_item_mapping.categoryid = pdpcategory_master.categoryunkid and pdpcategory_master.isactive = 1 " & _
             "left join pdpitem_master on pdpcategory_item_mapping.itemid = pdpitem_master.itemunkid and pdpitem_master.isactive = 1 " & _
             "WHERE pdpcategory_item_mapping.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mintCategoryId, mintItemId, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This mapping is already defined. Please define new mapping.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryId)
            objDataOperation.AddParameter("@itemid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid.ToString)

            strQ = "INSERT INTO pdpcategory_item_mapping ( " & _
              " categoryid " & _
              ", itemid " & _
              ", isvoid " & _
            ") VALUES (" & _
              " @categoryid " & _
              ", @itemid " & _
              ", @isvoid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCategoryItemMappingid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mintCategoryId, mintItemId, mintCategoryItemMappingid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryitemmappingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryItemMappingid.ToString)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryId.ToString)
            objDataOperation.AddParameter("@itemid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemId.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsVoid.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..pdpcategory_item_mapping SET " & _
              "  categoryid = @categoryid " & _
              ", itemid = @itemid " & _
              ", isvoid = @isvoid " & _
            "WHERE categoryitemmappingid = @categoryitemmappingid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE pdpcategory_item_mapping SET " & _
                   " isvoid = 1 " & _
                   "WHERE categoryitemmappingid = @categoryitemmappingid "

            objDataOperation.AddParameter("@categoryitemmappingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function isUsed(ByVal intCategoryid As Integer, ByVal intItemid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pdpgoals_master"}
            For Each value As String In Tables
                Select Case value
                    Case "pdpgoals_master"
                        strQ = "SELECT " & _
                                     "pdpitemdatatran.categoryunkid,pdpitemdatatran.itemunkid " & _
                                "FROM " & value & " " & _
                                "LEFT JOIN pdpitemdatatran " & _
                                     "ON pdpgoals_master.itemunkid = pdpitemdatatran.itemtranunkid " & _
                                "WHERE pdpitemdatatran.itemunkid > 0 " & _
                                 "and pdpitemdatatran.categoryunkid = @categoryunkid " & _
                                 "and pdpitemdatatran.itemunkid=  @itemunkid "

                    Case Else
                        strQ = ""
                End Select
                objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryid)
                objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intCategoryId As Integer, _
                            ByVal intItemId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                    "  categoryitemmappingid " & _
                    ", categoryid " & _
                    ", itemid " & _
                    ", isvoid " & _
                    "FROM pdpcategory_item_mapping " & _
                    "WHERE isvoid = 0 " & _
                    " AND categoryid = @categoryid " & _
                    " AND itemid = @itemid "

            If intUnkid > 0 Then
                strQ &= " AND categoryitemmappingid <> @categoryitemmappingid "
            End If

            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryId)
            objDataOperation.AddParameter("@itemid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemId)
            objDataOperation.AddParameter("@categoryitemmappingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO atpdpcategory_item_mapping ( " & _
                    "  tranguid " & _
                    ", categoryitemmappingid " & _
                    ", categoryid " & _
                    ", itemid " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", categoryitemmappingid " & _
                    ", categoryid " & _
                    ", itemid " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    "  From pdpcategory_item_mapping WHERE 1=1 and categoryitemmappingid=@categoryitemmappingid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryitemmappingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryItemMappingid.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

End Class
