﻿Imports eZeeCommonLib

Public Class clspdpform_master

    Private Const mstrModuleName = "clsPdpformmaster"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mintPdpformunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintNextjobid As Integer
    Private mblnIsvoid As Boolean
    Private mintCategoryid As Integer
    Private mintItemunkid As Integer
    Private mstrFieldvalue As String
    Private mblnIscompetencyselection As Boolean = False
    Private mblnIsAddItemData As Boolean = False
    Private menViewType As enPDPCustomItemViewType = enPDPCustomItemViewType.List


    Private mintItemtranunkid As Integer = -1

    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    Private mstrItemgrpguid As String = ""

    Private mdtPersonalAnalysisGoalItem As DataTable = Nothing
    Private mintStatusunkid As Integer = 0

    Public Enum enPDPFormStatus
        Unlock = 0
        SubmitForReview = 1
        Lock = 2
    End Enum
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _DtPersonalAnalysisGoalItems() As DataTable
        Get
            Return mdtPersonalAnalysisGoalItem
        End Get
        Set(ByVal value As DataTable)
            mdtPersonalAnalysisGoalItem = value
        End Set
    End Property

    Public Property _ViewType() As enPDPCustomItemViewType
        Get
            Return menViewType
        End Get
        Set(ByVal value As enPDPCustomItemViewType)
            menViewType = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set pdpformunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Pdpformunkid() As Integer
        Get
            Return mintPdpformunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpformunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nextjobid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Nextjobid() As Integer
        Get
            Return mintNextjobid
        End Get
        Set(ByVal value As Integer)
            mintNextjobid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nextjobid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Categoryid() As Integer
        Get
            Return mintCategoryid
        End Get
        Set(ByVal value As Integer)
            mintCategoryid = value
        End Set
    End Property

    Public Property _Itemunkid() As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
        End Set
    End Property

    Public Property _Fieldvalue() As String
        Get
            Return mstrFieldvalue
        End Get
        Set(ByVal value As String)
            mstrFieldvalue = value
        End Set
    End Property

    Public Property _IsAddItemData() As Boolean
        Get
            Return mblnIsAddItemData
        End Get
        Set(ByVal value As Boolean)
            mblnIsAddItemData = value
        End Set
    End Property

    Public Property _Itemtranunkid() As Integer
        Get
            Return mintItemtranunkid
        End Get
        Set(ByVal value As Integer)
            mintItemtranunkid = value
        End Set
    End Property

    Public Property _IsCompetencySelection() As Boolean
        Get
            Return mblnIscompetencyselection
        End Get
        Set(ByVal value As Boolean)
            mblnIscompetencyselection = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Itemgrpguid() As String
        Set(ByVal value As String)
            mstrItemgrpguid = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

#End Region

#Region " Constuctor "
    Public Sub New()
        mdtPersonalAnalysisGoalItem = New DataTable()

        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("itemunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtPersonalAnalysisGoalItem.Columns.Add(dCol)

            dCol = New DataColumn("fieldvalue")
            dCol.DataType = System.Type.GetType("System.String")
            mdtPersonalAnalysisGoalItem.Columns.Add(dCol)

            dCol = New DataColumn("iscompetencyselection")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtPersonalAnalysisGoalItem.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  pdpformunkid " & _
              ", employeeunkid " & _
              ", nextjobid " & _
              ", isvoid " & _
              ", status " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM pdpformmaster " & _
             "WHERE pdpformunkid = @pdpformunkid "

            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPdpformunkid = CInt(dtRow.Item("pdpformunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintNextjobid = CInt(dtRow.Item("nextjobid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintStatusunkid = CInt(dtRow.Item("status"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intEmployeeid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  pdpformunkid " & _
              ", employeeunkid " & _
              ", nextjobid " & _
              ", isvoid " & _
              ", status " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM pdpformmaster WITH (NOLOCK) where 1=1 "

            If blnOnlyActive Then
                strQ &= " and isvoid = 0 "
            End If

            If intEmployeeid > 0 Then
                strQ &= " and employeeunkid=@employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SavePDPEmployeeData(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        Dim objpdpform_tran As New clspdpform_tran
        Dim objPdpgoals_master As New clsPdpgoals_master

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If isExist(mintPdpformunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                mstrItemgrpguid = Guid.NewGuid().ToString()
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnIsAddItemData Then


                If menViewType = enPDPCustomItemViewType.Table Then

                    If IsNothing(mdtPersonalAnalysisGoalItem) = False AndAlso mdtPersonalAnalysisGoalItem.Rows.Count > 0 Then
                objpdpform_tran._Itemtranunkid = mintItemtranunkid
                        objpdpform_tran._Itemgrpguid = mstrItemgrpguid
                        objpdpform_tran._Pdpformunkid = mintPdpformunkid
                        objpdpform_tran._Categoryunkid = mintCategoryid
                        objpdpform_tran._Isvoid = False
                        objpdpform_tran._AuditUserId = mintAuditUserId
                        objpdpform_tran._LoginEmployeeUnkid = mintloginemployeeunkid
                        objpdpform_tran._HostName = mstrHostName
                        objpdpform_tran._ClientIP = mstrClientIP
                        objpdpform_tran._FromWeb = mblnIsWeb
                        objpdpform_tran._DatabaseName = mstrDatabaseName
                        objpdpform_tran._FormName = mstrFormName

                        For Each drow As DataRow In mdtPersonalAnalysisGoalItem.Rows
                            objpdpform_tran._Itemunkid = drow("itemunkid").ToString()
                            objpdpform_tran._Fieldvalue = drow("fieldvalue").ToString()
                            objpdpform_tran._IsCompetencySelection = CBool(drow("iscompetencyselection").ToString())
                            If objpdpform_tran.SaveCustomItemData(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next
                    End If

                Else

                    objpdpform_tran._Itemtranunkid = mintItemtranunkid
                objpdpform_tran._Itemgrpguid = mstrItemgrpguid
                objpdpform_tran._Pdpformunkid = mintPdpformunkid
                objpdpform_tran._Categoryunkid = mintCategoryid
                objpdpform_tran._Isvoid = False
                objpdpform_tran._AuditUserId = mintAuditUserId
                    objpdpform_tran._LoginEmployeeUnkid = mintloginemployeeunkid
                objpdpform_tran._HostName = mstrHostName
                objpdpform_tran._ClientIP = mstrClientIP
                objpdpform_tran._FromWeb = mblnIsWeb
                objpdpform_tran._DatabaseName = mstrDatabaseName
                objpdpform_tran._FormName = mstrFormName
                objpdpform_tran._Itemunkid = mintItemunkid
                objpdpform_tran._Fieldvalue = mstrFieldvalue
                    objpdpform_tran._IsCompetencySelection = mblnIscompetencyselection

                    If objpdpform_tran.SaveCustomItemData(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

            End If

            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SavePDPEmployeeData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpformmaster) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@nextjobid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextjobid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enPDPFormStatus.Unlock))

            strQ = "INSERT INTO pdpformmaster ( " & _
              "  employeeunkid " & _
              ", nextjobid " & _
              ", isvoid " & _
              ", status " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @nextjobid " & _
              ", @isvoid " & _
              ", @status " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPdpformunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintPdpformunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpformmaster) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@nextjobid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextjobid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpformmaster SET " & _
              " nextjobid = @nextjobid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              "  WHERE pdpformunkid = @pdpformunkid and " & _
              "  employeeunkid = @employeeunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintPdpformunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpformmaster) </purpose>
    ''' 
    Public Function Delete(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objpdpform_tran As New clspdpform_tran


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpformmaster SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              "  WHERE pdpformunkid = @pdpformunkid and employeeunkid = @employeeunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objpdpform_tran._Itemgrpguid = mstrItemgrpguid
            objpdpform_tran._Pdpformunkid = mintPdpformunkid
            objpdpform_tran._Categoryunkid = mintCategoryid

            objpdpform_tran._Isvoid = True
            objpdpform_tran._Voiduserunkid = mintVoiduserunkid
            objpdpform_tran._LoginEmployeeUnkid = mintloginemployeeunkid
            objpdpform_tran._Voiddatetime = mdtVoiddatetime
            objpdpform_tran._Voidreason = mstrVoidreason


            'objpdpform_tran.Delete(objDataOperation)

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintPdpformunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  pdpformunkid " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM pdpformmaster " & _
             "WHERE employeeunkid = @employeeunkid and nextjobid=@nextjobid and isvoid = 0 "

            objDataOperation.ClearParameters()
            If intUnkid > 0 Then
                strQ &= " AND pdpformunkid = @pdpformunkid"
                objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@nextjobid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNextjobid)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal strMstUnkid As String) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpformmaster ( " & _
                    "  tranguid " & _
                    ", pdpformunkid " & _
                    ", employeeunkid " & _
                    ", nextjobid " & _
                    ", status " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb)" & _
                    " SELECT " & _
                    "  LOWER(NEWID()) " & _
                    ", pdpformunkid " & _
                    ", employeeunkid " & _
                    ", nextjobid " & _
                    ", status " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    " From " & mstrDatabaseName & "..pdpformmaster where pdpformunkid in ( " & strMstUnkid & " ) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_Custom_Items_List(ByVal iCategoryId As Integer, _
                                          ByVal iItemId As Integer, _
                                          ByVal iEmployeeId As Integer) As DataSet

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn
        Dim objDataOperation As New clsDataOperation
        Try

            StrQ = "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                              "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 " & _
                         "AND iscompetencyselection = 1 "

            StrQ &= "SELECT " & _
                         "pdpitemdatatran.itemunkid as customitemunkid " & _
                   ",CASE " & _
                                "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                          "ELSE pdpitemdatatran.fieldvalue " & _
                     "END AS custom_value " & _
                       ",pdpitem_master.itemtypeid " & _
                       ",pdpitem_master.selectionmodeid " & _
                       ",pdpitemdatatran.itemgrpguid " & _
                       ",pdpitemdatatran.pdpformunkid " & _
                   ",pdpitemdatatran.itemtranunkid " & _
                   ",pdpitemdatatran.categoryunkid " & _
                   ",ISNULL(pdpitemdatatran.iscompetencyselection, 0) AS iscompetencyselection " & _
                   ",CASE " & _
                     "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                          "ELSE -1 " & _
                     "END AS CompetencyValue " & _
                    "FROM pdpitemdatatran " & _
                    "JOIN atpdpitemdatatran " & _
                         "ON atpdpitemdatatran.itemtranunkid = pdpitemdatatran.itemtranunkid " & _
                              "AND atpdpitemdatatran.audittypeid = 1 " & _
                    "LEFT JOIN hrmsConfiguration..cfuser_master " & _
                         "ON atpdpitemdatatran.audtuserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    "JOIN pdpitem_master " & _
                     "ON pdpitem_master.itemunkid = pdpitemdatatran.itemunkid " & _
                          "AND pdpitem_master.isactive = 1 " & _
                    "JOIN pdpformmaster " & _
                         "ON pdpformmaster.pdpformunkid = pdpitemdatatran.pdpformunkid " & _
                "LEFT JOIN #competencyList " & _
                     "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                    "WHERE pdpitemdatatran.isvoid = 0 " & _
                    "AND pdpformmaster.employeeunkid = '" & iEmployeeId & "' " & _
                "AND pdpitem_master.itemunkid = '" & iItemId & "' " & _
                "AND pdpitem_master.categoryunkid = '" & iCategoryId & "' ORDER BY pdpitemdatatran.itemtranunkid "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dsList.Tables(0).Rows

                        Select Case CInt(dRow.Item("itemtypeid"))
                            Case clspdpitem_master.enPdpCustomType.SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    Select Case CInt(dRow.Item("selectionmodeid"))

                                        Case clspdpitem_master.enPdpSelectionMode.TRAINING_OBJECTIVE, clspdpitem_master.enPdpSelectionMode.JOB_CAPABILITIES_COURSES, clspdpitem_master.enPdpSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                        dRow.Item("custom_value") = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_COMPETENCIES
                                            Dim objCMaster As New clsassess_competencies_master
                                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                        dRow.Item("custom_value") = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_GOALS
                                            Dim objEmpField1 As New clsassess_empfield1_master
                                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                        dRow.Item("custom_value") = objEmpField1._Field_Data
                                            objEmpField1 = Nothing

                                        Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                        dRow.Item("custom_value") = objCMaster._Name
                                            objCMaster = Nothing
                                    End Select
                                End If
                            Case clspdpitem_master.enPdpCustomType.DATE_SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                dRow.Item("custom_value") = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                End If
                            Case clspdpitem_master.enPdpCustomType.NUMERIC_DATA
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dRow.Item("custom_value")) Then
                                    dRow.Item("custom_value") = CDbl(dRow.Item("custom_value")).ToString
                                    End If
                                End If
                        End Select

                    Next
                End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Custom_Items_List; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function Get_CListItems_ForAddEdit(ByVal iPeriodId As Integer, _
                                          ByVal iItemTranId As Integer) As DataTable

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                         "item " & _
                       ",ISNULL(CASE " & _
                              "WHEN itemtypeid IN (" & CInt(clsassess_custom_items.enCustomType.FREE_TEXT) & "," & CInt(clsassess_custom_items.enCustomType.DATE_SELECTION) & "," & CInt(clsassess_custom_items.enCustomType.NUMERIC_DATA) & ") THEN ISNULL(pdpitemdatatran.fieldvalue, '') " & _
                              "WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND " & _
                                   "selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE) & ", " & CInt(clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM) & ") THEN cfcommon_master.name " & _
                              "WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND " & _
                                   "selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES) & ") THEN hrassess_competencies_master.name " & _
                              "WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND " & _
                                   "selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS) & ") THEN '' " & _
                         "END, '') AS custom_value " & _
                       ",itemtypeid " & _
                       ",selectionmodeid " & _
                       ",CAST(NULL AS DATETIME) AS ddate " & _
                       ",0 AS selectedid " & _
                       ",ISNULL(pdpitemdatatran.itemunkid, pdpitem_master.itemunkid) AS customitemunkid " & _
                       ",pdpitem_master.categoryunkid " & _
                       ",ISNULL(pdpitemdatatran.fieldvalue, '') AS customvalueId " & _
                    "FROM pdpitem_master " & _
                    " " & _
                    "LEFT JOIN pdpitemdatatran " & _
                         "ON pdpitem_master.itemunkid = pdpitemdatatran.itemunkid " & _
                    "LEFT JOIN cfcommon_master " & _
                         "ON pdpitemdatatran.fieldvalue = CAST(masterunkid AS NVARCHAR(MAX)) " & _
                              "AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
                              "AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE) & ", " & CInt(clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM) & ") " & _
                              "AND cfcommon_master.isactive = 1 " & _
                    "LEFT JOIN hrassess_competencies_master " & _
                         "ON pdpitemdatatran.fieldvalue = CAST(hrassess_competencies_master.competenciesunkid AS NVARCHAR(MAX)) " & _
                              "AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
                              "AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES) & ") " & _
                              "AND hrassess_competencies_master.isactive = 1 " & _
                              "AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "' " & _
                    "LEFT JOIN hrassess_empfield1_master " & _
                         "ON pdpitemdatatran.fieldvalue = CAST(hrassess_empfield1_master.empfield1unkid AS NVARCHAR(MAX)) " & _
                              "AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
                              "AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS) & ") " & _
                              "AND hrassess_empfield1_master.isvoid = 0 " & _
                              "AND hrassess_empfield1_master.periodunkid = '" & iPeriodId & "' " & _
                    "WHERE pdpitem_master.isactive = 1 and pdpitemdatatran.itemtranunkid = '" & iItemTranId & "'  "


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For index As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dsList.Tables(0).Rows(index)("custom_value").ToString().Length > 0 Then
                        Dim dt As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).Date
                        dsList.Tables(0).Rows(index)("custom_value") = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).ToShortDateString()
                        dsList.Tables(0).Rows(index)("ddate") = dt
                    End If
                ElseIf dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.SELECTION Then
                    If Val(dsList.Tables(0).Rows(index)("customvalueId")) > 0 Then dsList.Tables(0).Rows(index)("selectedid") = CInt(Val(dsList.Tables(0).Rows(index)("customvalueId")))
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_CListItems_ForAddEdit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    Public Function Get_Custom_Items_Table(ByVal iHeaderId As Integer, _
                                           ByVal iEmployeeId As Integer, _
                                           ByVal strDBName As String) As DataTable 'S.SANDEEP |03-MAY-2021| -- START {strDBName} -- END

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn
        Dim objDataOperation As New clsDataOperation
        Try
            mdtFinal = New DataTable("CTList")

            Dim objCHeader As New clspdpcategory_master
            objCHeader._Categoryunkid = iHeaderId
            dCol = New DataColumn
            With dCol
                .ColumnName = "Header_Name"
                .Caption = Language.getMessage(mstrModuleName, 116, "Custom Header")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = objCHeader._Category
            End With
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Header_Id"
                .Caption = ""
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = objCHeader._Categoryunkid
            End With
            mdtFinal.Columns.Add(dCol)


            objCHeader = Nothing

            Dim objCustomItems As New clspdpitem_master
            dsList = objCustomItems.getComboList("List", False, iHeaderId)
            objCustomItems = Nothing

            If dsList.Tables(0).Rows.Count > 0 Then

                For Each drow As DataRow In dsList.Tables(0).Rows
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString
                        .Caption = drow.Item("Name").ToString
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                        .Caption = ""
                        .DataType = System.Type.GetType("System.Int32")
                        .DefaultValue = -1
                    End With
                    mdtFinal.Columns.Add(dCol)
                Next

                dCol = New DataColumn
                With dCol
                    .ColumnName = "itemtypeid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "selectionmodeid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "GUID"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "formunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)




                StrQ = "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                        "DROP TABLE #competencyList " & _
                              "CREATE TABLE #competencyList ( " & _
                                "itemtranunkid int " & _
                                ",fieldvalue  NVARCHAR(MAX) " & _
                                ",CompetencyValue int " & _
                              "); " & _
                        "INSERT INTO #competencyList " & _
                             "SELECT " & _
                                  "itemtranunkid " & _
                                ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                                ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                             "FROM pdpitemdatatran " & _
                             "LEFT JOIN hrassess_competencies_master " & _
                                  "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                             "WHERE isvoid = 0 " & _
                             "AND iscompetencyselection = 1 "


                StrQ &= "SELECT " & _
                         "pdpitemdatatran.itemunkid as customitemunkid " & _
                       ",CASE " & _
                                "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                            "ELSE pdpitemdatatran.fieldvalue " & _
                       "END AS custom_value" & _
                       ",pdpitem_master.itemtypeid " & _
                       ",pdpitem_master.selectionmodeid " & _
                       ",pdpitemdatatran.itemgrpguid " & _
                       ",pdpitemdatatran.pdpformunkid " & _
                       ",CASE " & _
                              "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                              "ELSE -1 " & _
                       "END as competencyVal " & _
                    ",ISNULL(pdpitemdatatran.iscompetencyselection, 0) AS iscompetencyselection " & _
                    "FROM pdpitemdatatran " & _
                    "JOIN pdpitem_master " & _
                         "ON pdpitem_master.itemunkid = pdpitemdatatran.itemunkid " & _
                    "JOIN pdpformmaster " & _
                         "ON pdpformmaster.pdpformunkid = pdpitemdatatran.pdpformunkid " & _
                    "LEFT JOIN #competencyList " & _
                         "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                    "WHERE pdpitemdatatran.isvoid = 0 " & _
                    "AND pdpformmaster.employeeunkid = '" & iEmployeeId & "' " & _
                    "AND pdpitem_master.categoryunkid = '" & iHeaderId & "' "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'If iAction <> enAction.EDIT_ONE Then
                '    dsList.Tables(0).Rows.Clear()
                'End If


                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                Dim blnIsPMCategory As Boolean = False
                Dim objCategory As New clspdpcategory_master
                objCategory._Categoryunkid = iHeaderId
                blnIsPMCategory = objCategory._Isincludeinpm
                objCategory = Nothing
                'S.SANDEEP |03-MAY-2021| -- END

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim dFRow As DataRow = Nothing
                    Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                    'S.SANDEEP |03-MAY-2021| -- START
                    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                    Dim iGuid As String = ""
                    'S.SANDEEP |03-MAY-2021| -- END
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                        iNewCustomId = dRow.Item("customitemunkid")

                        'S.SANDEEP |03-MAY-2021| -- START
                        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                        If blnIsPMCategory Then
                            If iGuid <> dRow.Item("itemgrpguid").ToString() Then
                                dFRow = mdtFinal.NewRow
                                mdtFinal.Rows.Add(dFRow)
                                iGuid = dRow.Item("itemgrpguid").ToString()
                            End If
                        Else
                        If iCustomId = iNewCustomId Then
                            dFRow = mdtFinal.NewRow
                            mdtFinal.Rows.Add(dFRow)
                        End If
                        End If
                        'S.SANDEEP |03-MAY-2021| -- END                        
                        Select Case CInt(dRow.Item("itemtypeid"))
                            Case clspdpitem_master.enPdpCustomType.FREE_TEXT
                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                            Case clspdpitem_master.enPdpCustomType.SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    Select Case CInt(dRow.Item("selectionmodeid"))

                                        Case clspdpitem_master.enPdpSelectionMode.TRAINING_OBJECTIVE, clspdpitem_master.enPdpSelectionMode.JOB_CAPABILITIES_COURSES, clspdpitem_master.enPdpSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_COMPETENCIES
                                            Dim objCMaster As New clsassess_competencies_master
                                            objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                        Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_GOALS
                                            Dim objEmpField1 As New clsassess_empfield1_master
                                            objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                            objEmpField1 = Nothing

                                        Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing

                                            'S.SANDEEP |03-MAY-2021| -- START
                                            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                                        Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_PERIOD
                                            Dim objPrd As New clscommom_period_Tran
                                            objPrd._Periodunkid(strDBName) = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objPrd._Period_Name
                                            objPrd = Nothing
                                            'S.SANDEEP |03-MAY-2021| -- END
                                    End Select
                                End If
                            Case clspdpitem_master.enPdpCustomType.DATE_SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                End If
                            Case clspdpitem_master.enPdpCustomType.NUMERIC_DATA
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dRow.Item("custom_value")) Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                    End If
                                End If
                        End Select
                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                        dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                        dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                        dFRow.Item("GUID") = dRow.Item("itemgrpguid")
                        dFRow.Item("formunkid") = dRow.Item("pdpformunkid")

                    Next
                Else
                    mdtFinal.Rows.Add(mdtFinal.NewRow)
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Custom_Items_Table; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    Public Function Get_CTableItems_ForAddEdit(ByVal iPeriodId As Integer, _
                                          ByVal iHeaderId As Integer, _
                                          Optional ByVal strEditGUID As String = "") As DataTable

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation = New clsDataOperation
        Try

            StrQ = "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                              "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 " & _
                         "AND iscompetencyselection = 1 "


            StrQ &= "SELECT " & _
                         "pdpitem_master.item " & _
                       ",ISNULL(CASE " & _
                              "WHEN itemtypeid IN (" & CInt(clsassess_custom_items.enCustomType.FREE_TEXT) & ") THEN " & _
                                 " CASE " & _
                                        "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                                        "ELSE pdpitemdatatran.fieldvalue " & _
                                 "END " & _
                              "WHEN itemtypeid IN (" & CInt(clsassess_custom_items.enCustomType.DATE_SELECTION) & "," & CInt(clsassess_custom_items.enCustomType.NUMERIC_DATA) & ") THEN ISNULL(pdpitemdatatran.fieldvalue, '') " & _
                              "WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND " & _
                                   "selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE) & ", " & CInt(clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM) & ") THEN cfcommon_master.name "

            '"WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND " & _
            '     "selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES) & ") THEN hrassess_competencies_master.name " & _

            StrQ &= "WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND " & _
                                   "selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS) & ") THEN '' " & _
                         "END, '') AS custom_value " & _
                       ",itemtypeid " & _
                       ",selectionmodeid " & _
                       ",CAST(NULL AS DATETIME) AS ddate " & _
                       ",0 AS selectedid " & _
                       ",ISNULL(pdpitemdatatran.itemunkid, pdpitem_master.itemunkid) AS customitemunkid " & _
                       ",pdpitem_master.categoryunkid " & _
                       ",ISNULL(pdpitemdatatran.fieldvalue, '') AS customvalueId " & _
                                   ",CASE " & _
                              "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                              "ELSE -1 " & _
                                   "END as competencyVal " & _
                                   ",iscompetencyselectionset " & _
                    "FROM pdpitem_master " & _
                    " " & _
                    "LEFT JOIN pdpitemdatatran " & _
                         "ON pdpitem_master.itemunkid = pdpitemdatatran.itemunkid " & _
                              "AND pdpitemdatatran.itemgrpguid = '" & strEditGUID & "' " & _
                    "LEFT JOIN cfcommon_master " & _
                         "ON pdpitemdatatran.fieldvalue = CAST(masterunkid AS NVARCHAR(MAX)) " & _
                              "AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
                              "AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE) & ", " & CInt(clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM) & ") " & _
                              "AND cfcommon_master.isactive = 1 " & _
                    "LEFT JOIN #competencyList " & _
                         "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                      "AND pdpitemdatatran.iscompetencyselection = 1 "

            '"LEFT JOIN hrassess_competencies_master " & _
            '     "ON pdpitemdatatran.fieldvalue = CAST(hrassess_competencies_master.competenciesunkid AS NVARCHAR(MAX)) " & _
            '          "AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
            '          "AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES) & ") " & _
            '          "AND hrassess_competencies_master.isactive = 1 " & _
            '          "AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "' " & _

            StrQ &= "LEFT JOIN hrassess_empfield1_master " & _
                         "ON pdpitemdatatran.fieldvalue = CAST(hrassess_empfield1_master.empfield1unkid AS NVARCHAR(MAX)) " & _
                              "AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
                              "AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS) & ") " & _
                              "AND hrassess_empfield1_master.isvoid = 0 " & _
                              "AND hrassess_empfield1_master.periodunkid = '" & iPeriodId & "' " & _
                    "WHERE pdpitem_master.categoryunkid = '" & iHeaderId & "' " & _
                    "AND pdpitem_master.isactive = 1 "


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For index As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dsList.Tables(0).Rows(index)("custom_value").ToString().Length > 0 Then
                        Dim dt As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).Date
                        dsList.Tables(0).Rows(index)("custom_value") = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).ToShortDateString()
                        dsList.Tables(0).Rows(index)("ddate") = dt
                    End If
                ElseIf dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.SELECTION Then
                    If Val(dsList.Tables(0).Rows(index)("customvalueId")) > 0 Then dsList.Tables(0).Rows(index)("selectedid") = CInt(Val(dsList.Tables(0).Rows(index)("customvalueId")))
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_CTableItems_ForAddEdit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    Public Function GetPastDevepmentTrainingList(ByVal strTableName As String, Optional ByVal intEmployeeid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "hrtraining_enrollment_tran.trainingschedulingunkid AS Id " & _
                   ",ISNULL(cfcommon_master.name, '') AS Name " & _
                   ",CONVERT(CHAR(8),hrtraining_enrollment_tran.enroll_date,112) AS EnrollDate " & _
                   "FROM hrtraining_enrollment_tran " & _
                   "LEFT JOIN hrtraining_scheduling " & _
                        "ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                   "JOIN cfcommon_master " & _
                        "ON cfcommon_master.masterunkid = hrtraining_scheduling.course_title " & _
                             "AND mastertype = '" & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "' " & _
                   "WHERE hrtraining_enrollment_tran.employeeunkid = @employeeunkid " & _
                   "AND hrtraining_enrollment_tran.isvoid = 0 " & _
                   "AND hrtraining_enrollment_tran.iscancel = 0 " & _
                   "AND hrtraining_enrollment_tran.status_id = 3 "
    
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
        Return dsList
    End Function

    Public Function UpdatePDPFormStatus(ByVal intPdpformunkid As Integer, ByVal enStatusId As enPDPFormStatus, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE pdpformmaster SET " & _
              " status = @status " & _
              " WHERE pdpformunkid = @pdpformunkid "

            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enStatusId))
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpformunkid)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, intPdpformunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePDPFormStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Public Function IsPDPFormExists(ByVal intEmployeeId As Integer, ByVal intNextJobId As Integer, Optional ByRef intFomId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim blnFlag As Boolean = False
        Try
            dsList = GetList("List", True, intEmployeeId)
            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
                Dim dr() As DataRow = Nothing
                If intNextJobId > 0 Then
                    dr = dsList.Tables("List").Select("nextjobid = '" & intNextJobId & "'")
                    If dr.Length <= 0 Then
                        blnFlag = False
                    Else
                        intFomId = dr(0)("pdpformunkid")
                    End If
                Else
                    intFomId = dsList.Tables("List").Rows(0)("pdpformunkid")
                End If
            Else
                blnFlag = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPDPFormExists; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP |03-MAY-2021| -- END

End Class
