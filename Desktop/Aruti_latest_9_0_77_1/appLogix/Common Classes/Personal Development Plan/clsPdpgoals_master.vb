﻿'************************************************************************************************************************************
'Class Name : clsPdpgoals_master.vb
'Purpose    :
'Date       :29-01-2021
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsPdpgoals_master
    Private Const mstrModuleName = "clsPdpgoals_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPdpgoalsmstunkid As Integer
    Private mintPdpformunkid As Integer
    Private mintActionplancategoryunkid As Integer
    Private mintItemunkid As Integer
    Private mintParentgoalunkid As Integer
    Private mstrGoal_Name As String = String.Empty
    Private mstrGoal_Description As String = String.Empty
    Private mstrGoal_Evaluators As String = String.Empty
    Private mintStatusunkid As Integer
    Private mdtDuedate As Date
    Private mintEmployeeunkid As Integer
    Private mintMentorEmployeeunkid As Integer
    Private mintCoachunkid As Integer = -1

    'Gajanan [15-Apr-2021] -- Start
    Private mblnIsCompetency As Boolean = False
    Private mintCompetencyCategoryId As Integer = -1
    Private mintTrainingAllocationid As Integer = -1
    Private mintTrainingDepartmentunkid As Integer = -1
    Private mintTrainingcategoryunkid As Integer = -1
    Private mblnIsAddRecommendedTraining As Boolean = False
    'Gajanan [15-Apr-2021] -- End



    Private mblnIsvoid As Boolean
    Private mintUserunkid As Integer = -1

    Private mintVoiduserunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mintAuditUserId As Integer = 0
    Private mdtAuditDate As DateTime
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""

    Private mstrDatabaseName As String = ""

    Private mdtEvalutorItems As DataTable = Nothing
    Private mstrTrainingItems As String = ""
    Private mstrTrainingVoidReason As String = ""

    Public Enum enEmailType
        LOCK_PDP = 1
        UNLOCK_PDP = 2
        SUBMITFORAPPROVAL = 3
    End Enum
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pdpgoalsmstunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Pdpgoalsmstunkid() As Integer
        Get
            Return mintPdpgoalsmstunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpgoalsmstunkid = value
            Call GetData()
        End Set
    End Property

    Public Property _Pdpformunkid() As Integer
        Get
            Return mintPdpformunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpformunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actionplancategoryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Actionplancategoryunkid() As Integer
        Get
            Return mintActionplancategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintActionplancategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Itemunkid() As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parentgoalunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Parentgoalunkid() As Integer
        Get
            Return mintParentgoalunkid
        End Get
        Set(ByVal value As Integer)
            mintParentgoalunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set goal_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Goal_Name() As String
        Get
            Return mstrGoal_Name
        End Get
        Set(ByVal value As String)
            mstrGoal_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set goal_description
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Goal_Description() As String
        Get
            Return mstrGoal_Description
        End Get
        Set(ByVal value As String)
            mstrGoal_Description = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set goal_evaluators
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Goal_Evaluators() As String
        Get
            Return mstrGoal_Evaluators
        End Get
        Set(ByVal value As String)
            mstrGoal_Evaluators = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set duedate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Duedate() As Date
        Get
            Return mdtDuedate
        End Get
        Set(ByVal value As Date)
            mdtDuedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _MentorEmployeeunkid() As Integer
        Get
            Return mintMentorEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintMentorEmployeeunkid = value
        End Set
    End Property

    Public Property _Coachunkid() As Integer
        Get
            Return mintCoachunkid
        End Get
        Set(ByVal value As Integer)
            mintCoachunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _IsAddRecommendedTraining() As Boolean
        Get
            Return mblnIsAddRecommendedTraining
        End Get
        Set(ByVal value As Boolean)
            mblnIsAddRecommendedTraining = value
        End Set
    End Property

    Public Property _CompetencyCategoryId() As Integer
        Get
            Return mintCompetencyCategoryId
        End Get
        Set(ByVal value As Integer)
            mintCompetencyCategoryId = value
        End Set
    End Property

    Public Property _TrainingAllocationid() As Integer
        Get
            Return mintTrainingAllocationid
        End Get
        Set(ByVal value As Integer)
            mintTrainingAllocationid = value
        End Set
    End Property

    Public Property _TrainingDepartmentunkid() As Integer
        Get
            Return mintTrainingDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingDepartmentunkid = value
        End Set
    End Property

    Public Property _Trainingcategoryunkid() As Integer
        Get
            Return mintTrainingcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingcategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _DtmdtEvalutorItems() As DataTable
        Get
            Return mdtEvalutorItems
        End Get
        Set(ByVal value As DataTable)
            mdtEvalutorItems = value
        End Set
    End Property

    Public Property _TrainingItems() As String
        Get
            Return mstrTrainingItems
        End Get
        Set(ByVal value As String)
            mstrTrainingItems = value
        End Set
    End Property

    Public WriteOnly Property _TrainingVoidReason() As String
        Set(ByVal value As String)
            mstrTrainingVoidReason = value
        End Set
    End Property
#End Region

#Region " Constuctor "
    Public Sub New()
        mdtEvalutorItems = New DataTable()

        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("evaluatortypeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEvalutorItems.Columns.Add(dCol)

            dCol = New DataColumn("evaluatorunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEvalutorItems.Columns.Add(dCol)

            dCol = New DataColumn("commentdatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtEvalutorItems.Columns.Add(dCol)

            dCol = New DataColumn("comments")
            dCol.DataType = System.Type.GetType("System.String")
            mdtEvalutorItems.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEvalutorItems.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try

    End Sub

#End Region


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  pdpgoalsmstunkid " & _
              ", pdpformunkid " & _
              ", actionplancategoryunkid " & _
              ", itemunkid " & _
              ", parentgoalunkid " & _
              ", goal_name " & _
              ", goal_description " & _
              ", goal_evaluators " & _
              ", statusunkid " & _
              ", duedate " & _
              ", employeeunkid " & _
              ", isvoid " & _
             "FROM pdpgoals_master " & _
             "WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid "

            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPdpgoalsmstunkid = CInt(dtRow.Item("pdpgoalsmstunkid"))
                mintPdpformunkid = CInt(dtRow.Item("pdpformunkid"))
                mintActionplancategoryunkid = CInt(dtRow.Item("actionplancategoryunkid"))
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mintParentgoalunkid = CInt(dtRow.Item("parentgoalunkid"))
                mstrGoal_Name = dtRow.Item("goal_name").ToString
                mstrGoal_Description = dtRow.Item("goal_description").ToString
                mstrGoal_Evaluators = dtRow.Item("goal_evaluators").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                If IsDBNull(dtRow.Item("duedate")) Then
                mdtDuedate = dtRow.Item("duedate")
                End If
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  pdpgoalsmstunkid " & _
              ", pdpformunkid " & _
              ", actionplancategoryunkid " & _
              ", itemunkid " & _
              ", parentgoalunkid " & _
              ", goal_name " & _
              ", goal_description " & _
              ", goal_evaluators " & _
              ", statusunkid " & _
              ", duedate " & _
              ", employeeunkid " & _
             "FROM pdpgoals_master where 1=1 "

            If blnOnlyActive Then
                strQ &= " and isvoid = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetParentComboList(ByVal strTableName As String, _
                            ByVal intpdpformunkid As Integer, _
                            Optional ByVal intParentgoalunkid As Integer = -1, _
                            Optional ByVal intPdpGoalsmstunkid As Integer = -1, _
                            Optional ByVal intActionplancategoryunkid As Integer = -1, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal intEmployeeid As Integer = -1, _
                            Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            If blnFlag = True Then
                strQ = "SELECT 0 AS pdpgoalsmstunkid,@Select AS goal_name,'' as goal_description " & _
                       "UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If


            strQ &= " SELECT " & _
              "  pdpgoals_master.pdpgoalsmstunkid " & _
              ",CASE " & _
              "     WHEN pdpgoals_master.itemunkid > 0 THEN pdpitemdatatran.fieldvalue " & _
              "     ELSE pdpgoals_master.goal_name " & _
              "END AS goal_name " & _
              ", pdpgoals_master.goal_description " & _
             "FROM pdpgoals_master " & _
             "LEFT JOIN pdpitemdatatran " & _
                         "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
             " Where 1=1 and pdpgoals_master.pdpformunkid = @pdpformunkid "

            If blnOnlyActive Then
                strQ &= " and pdpgoals_master.isvoid = 0 "
            End If

            If intEmployeeid > 0 Then
                strQ &= " and pdpgoals_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            End If


            If intActionplancategoryunkid > 0 Then
                strQ &= " and pdpgoals_master.actionplancategoryunkid = @actionplancategoryunkid "
                objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActionplancategoryunkid)
            End If


            If intParentgoalunkid > 0 Then
                strQ &= " and pdpgoals_master.parentgoalunkid = @parentgoalunkid "
                objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentgoalunkid)
            End If

            If intPdpGoalsmstunkid > 0 Then
                strQ &= " and pdpgoals_master.pdpgoalsmstunkid <> @pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpGoalsmstunkid)
            End If



            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpformunkid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Public Function GetGoalListForCombo(ByVal strTableName As String _
                                        , Optional ByVal blnAddSelect As Boolean = False _
                                        , Optional ByVal intEmployeeid As Integer = 0 _
                                        , Optional ByVal intActionplancategoryunkid As Integer = 0 _
                                        , Optional ByVal intParentgoalunkid As Integer = 0 _
                                        , Optional ByVal intPdpGoalsmstunkid As Integer = 0 _
                                        ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect = True Then
                strQ = "SELECT 0 AS pdpgoalsmstunkid, ' ' + @Select AS goal_name,'' as goal_description " & _
                       "UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If


            strQ &= " SELECT " & _
              "  pdpgoals_master.pdpgoalsmstunkid " & _
              ",CASE " & _
              "     WHEN pdpgoals_master.itemunkid > 0 THEN pdpitemdatatran.fieldvalue " & _
              "     ELSE pdpgoals_master.goal_name " & _
              "END AS goal_name " & _
              ", pdpgoals_master.goal_description " & _
             "FROM pdpgoals_master " & _
             "LEFT JOIN pdpitemdatatran " & _
                         "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
             " WHERE pdpgoals_master.isvoid = 0 "

            If intEmployeeid > 0 Then
                strQ &= " AND pdpgoals_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            End If


            If intActionplancategoryunkid > 0 Then
                strQ &= " AND pdpgoals_master.actionplancategoryunkid = @actionplancategoryunkid "
                objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActionplancategoryunkid)
            End If


            If intParentgoalunkid > 0 Then
                strQ &= " AND pdpgoals_master.parentgoalunkid = @parentgoalunkid "
                objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentgoalunkid)
            End If

            If intPdpGoalsmstunkid > 0 Then
                strQ &= " AND pdpgoals_master.pdpgoalsmstunkid <> @pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpGoalsmstunkid)
            End If

            strQ &= " ORDER BY goal_name "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGoalListForCombo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (01 Mar 2021) -- End

    Public Function SaveEmployeeGoal(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        Dim dsTrainingList As DataSet = Nothing

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If isExist(mintPdpformunkid, mintPdpgoalsmstunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            'Save Evalutor
            If IsNothing(mdtEvalutorItems) = False AndAlso mdtEvalutorItems.Rows.Count > 0 Then
                Dim objPdpevaluator_comments As New clsPdpevaluator_comments
                objPdpevaluator_comments._Pdpgoalsmstunkid = mintPdpgoalsmstunkid
                objPdpevaluator_comments._Employeeunkid = mintEmployeeunkid

                objPdpevaluator_comments._Isvoid = False
                objPdpevaluator_comments._AuditUserId = mintAuditUserId
                objPdpevaluator_comments._LoginEmployeeUnkid = mintloginemployeeunkid
                objPdpevaluator_comments._HostName = mstrHostName
                objPdpevaluator_comments._ClientIP = mstrClientIP
                objPdpevaluator_comments._FromWeb = mblnIsWeb
                objPdpevaluator_comments._DatabaseName = mstrDatabaseName
                objPdpevaluator_comments._FormName = mstrFormName

                For Each drow As DataRow In mdtEvalutorItems.Rows
                    objPdpevaluator_comments._Evaluatortypeid = CInt(drow("evaluatortypeid").ToString())
                    objPdpevaluator_comments._Evaluatorunkid = CInt(drow("evaluatorunkid").ToString())
                    objPdpevaluator_comments._Commentdatetime = CDate(drow("commentdatetime").ToString())
                    objPdpevaluator_comments._Comments = drow("comments").ToString()

                    If objPdpevaluator_comments.SaveEvalutorCommentData(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            'Save Mentor
            Dim objPdpgoals_mentors As New clsPdpgoals_mentors
            objPdpgoals_mentors._Pdpgoalsmstunkid = mintPdpgoalsmstunkid
            objPdpgoals_mentors._Employeeunkid = mintMentorEmployeeunkid

            objPdpgoals_mentors._Isvoid = False
            objPdpgoals_mentors._AuditUserId = mintAuditUserId
            objPdpgoals_mentors._LoginEmployeeUnkid = mintloginemployeeunkid
            objPdpgoals_mentors._HostName = mstrHostName
            objPdpgoals_mentors._ClientIP = mstrClientIP
            objPdpgoals_mentors._FromWeb = mblnIsWeb
            objPdpgoals_mentors._DatabaseName = mstrDatabaseName
            objPdpgoals_mentors._FormName = mstrFormName

            If objPdpgoals_mentors.SaveMentorData(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Save Training
            If mstrTrainingItems.Length > 0 Then
                Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
                Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master
                Dim objDepttrainingneed_employee_Tran As New clsDepttrainingneed_employee_Tran


                objPdpgoals_trainingneed_Tran = New clsPdpgoals_trainingneed_Tran
                objPdpgoals_trainingneed_Tran._Pdpgoalsmstunkid = mintPdpgoalsmstunkid
                objPdpgoals_trainingneed_Tran._Employeeunkid = mintEmployeeunkid

                objPdpgoals_trainingneed_Tran._Isvoid = False
                objPdpgoals_trainingneed_Tran._AuditUserId = mintAuditUserId
                objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = mintloginemployeeunkid
                objPdpgoals_trainingneed_Tran._HostName = mstrHostName
                objPdpgoals_trainingneed_Tran._ClientIP = mstrClientIP
                objPdpgoals_trainingneed_Tran._FromWeb = mblnIsWeb
                objPdpgoals_trainingneed_Tran._DatabaseName = mstrDatabaseName
                objPdpgoals_trainingneed_Tran._FormName = mstrFormName

                'Gajanan [13-Apr-2021] -- Start
                Dim intFirstOpen As Integer = 0
                Dim dsList As DataSet = Nothing

                Dim objTPeriod As New clsTraining_Calendar_Master
                dsList = objTPeriod.getListForCombo("List", False, 1)
                If dsList.Tables(0).Rows.Count > 0 Then
                    intFirstOpen = CInt(dsList.Tables(0).Rows(0).Item("calendarunkid"))
                End If
                dsList = Nothing


                'dsList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(mintPdpgoalsmstunkid, -1, CInt(enModuleReference.PDP), -1, -1, False, objDataOperation)
                'If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then

                '    For Each drrow As DataRow In dsList.Tables(0).Rows

                '        objDepartmentaltrainingneed_master._Departmentaltrainingneedunkid = CInt(drrow("departmentaltrainingneedunkid"))

                '        If mblnIsCompetency Then
                '            objDepartmentaltrainingneed_master._Competenceunkid = mintCompetencyCategoryId
                '            objDepartmentaltrainingneed_master._Other_competence = ""
                '        Else
                '            If mintItemunkid > 0 Then
                '                objDepartmentaltrainingneed_master._Competenceunkid = -1
                '                objDepartmentaltrainingneed_master._Other_competence = ""
                '            Else
                '                objDepartmentaltrainingneed_master._Competenceunkid = -1
                '                objDepartmentaltrainingneed_master._Other_competence = mstrGoal_Name
                '            End If
                '        End If

                '        objDepartmentaltrainingneed_master._Isvoid = False
                '        objDepartmentaltrainingneed_master._AuditUserId = mintAuditUserId
                '        objDepartmentaltrainingneed_master._AuditDate = mdtAuditDate
                '        objDepartmentaltrainingneed_master._Loginemployeeunkid = mintloginemployeeunkid
                '        objDepartmentaltrainingneed_master._HostName = mstrHostName
                '        objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                '        objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                '        objDepartmentaltrainingneed_master._FormName = mstrFormName

                '        If objDepartmentaltrainingneed_master.Update(objDataOperation) = False Then
                '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '            Throw exForce
                '        End If
                '    Next
                'End If
                'Gajanan [13-Apr-2021] -- End

                For Each trItem As String In mstrTrainingItems.Split(",")
                    objPdpgoals_trainingneed_Tran._Trainingcourseunkid = CInt(trItem.Split("|")(0))
                    objPdpgoals_trainingneed_Tran._IsFromCompetency = CBool(trItem.Split("|")(1))
                    objPdpgoals_trainingneed_Tran._IsRecommended = mblnIsAddRecommendedTraining

                    Dim isExists As Boolean = objPdpgoals_trainingneed_Tran.isExist(-1, objDataOperation)
                    If isExists Then
                        Continue For
                    End If

                    If objPdpgoals_trainingneed_Tran.SaveTrainingData(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'Gajanan [13-Apr-2021] -- Start
                    If mblnIsAddRecommendedTraining Then
                        objDepartmentaltrainingneed_master._ModuleId = CInt(enModuleReference.PDP)
                        objDepartmentaltrainingneed_master._ModuleTranUnkId = mintPdpgoalsmstunkid
                        objDepartmentaltrainingneed_master._Periodunkid = intFirstOpen
                        objDepartmentaltrainingneed_master._Startdate = Nothing
                        objDepartmentaltrainingneed_master._Enddate = Nothing

                        If objPdpgoals_trainingneed_Tran._IsFromCompetency Then
                            objDepartmentaltrainingneed_master._Competenceunkid = mintCompetencyCategoryId
                            objDepartmentaltrainingneed_master._Other_competence = ""
                        Else
                            If mintItemunkid > 0 Then
                                objDepartmentaltrainingneed_master._Competenceunkid = -1
                                objDepartmentaltrainingneed_master._Other_competence = ""
                            Else
                                objDepartmentaltrainingneed_master._Competenceunkid = -1
                                objDepartmentaltrainingneed_master._Other_competence = mstrGoal_Name
                            End If
                        End If
                        objDepartmentaltrainingneed_master._InsertFormId = 1
                        objDepartmentaltrainingneed_master._Trainingcategoryunkid = mintTrainingcategoryunkid
                        objDepartmentaltrainingneed_master._Trainingcourseunkid = CInt(trItem.Split("|")(0))
                        objDepartmentaltrainingneed_master._Targetedgroupunkid = 0
                        objDepartmentaltrainingneed_master._Departmentunkid = mintTrainingDepartmentunkid
                        objDepartmentaltrainingneed_master._AllocationId = mintTrainingAllocationid
                        objDepartmentaltrainingneed_master._Noofstaff = 1

                        Dim objTraining_Category_Master As New clsTraining_Category_Master
                        objTraining_Category_Master._Categoryunkid(objDataOperation) = mintTrainingcategoryunkid
                        objDepartmentaltrainingneed_master._Trainingpriority = objTraining_Category_Master._Priorityunkid
                        objTraining_Category_Master = Nothing

                        objDepartmentaltrainingneed_master._Isvoid = False
                        objDepartmentaltrainingneed_master._AuditUserId = mintAuditUserId
                        objDepartmentaltrainingneed_master._AuditDate = mdtAuditDate
                        objDepartmentaltrainingneed_master._Loginemployeeunkid = mintloginemployeeunkid
                        objDepartmentaltrainingneed_master._HostName = mstrHostName
                        objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                        objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                        objDepartmentaltrainingneed_master._FormName = mstrFormName

                        Dim mintDepartmentaltrainingneedunkid As Integer = -1
                        If objDepartmentaltrainingneed_master.Insert(objDataOperation, mintDepartmentaltrainingneedunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        objDepttrainingneed_employee_Tran._Isvoid = False
                        objDepttrainingneed_employee_Tran._AuditUserId = mintAuditUserId
                        objDepttrainingneed_employee_Tran._AuditDate = mdtAuditDate
                        objDepttrainingneed_employee_Tran._Loginemployeeunkid = mintloginemployeeunkid
                        objDepttrainingneed_employee_Tran._HostName = mstrHostName
                        objDepttrainingneed_employee_Tran._ClientIP = mstrClientIP
                        objDepttrainingneed_employee_Tran._Isweb = mblnIsWeb
                        objDepttrainingneed_employee_Tran._FormName = mstrFormName
                        objDepttrainingneed_employee_Tran._Employeeunkid = mintEmployeeunkid
                        objDepttrainingneed_employee_Tran._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid

                        If objDepttrainingneed_employee_Tran.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    'Gajanan [13-Apr-2021] -- End


                    Next
                End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SaveEmployeeGoal; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function ProcessReviewTraining(ByVal mdtReviewTraining As DataTable, ByVal intPdpFormUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        Dim dsTrainingList As DataSet = Nothing

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            'Save Training
            If IsNothing(mdtReviewTraining) = False AndAlso mdtReviewTraining.Rows.Count > 0 Then
                Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
                Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master
                Dim objDepttrainingneed_employee_Tran As New clsDepttrainingneed_employee_Tran
                Dim objpdpform_master As New clspdpform_master

                Dim intFirstOpen As Integer = 0
                Dim dsList As DataSet = Nothing
                Dim objTPeriod As New clsTraining_Calendar_Master
                dsList = objTPeriod.getListForCombo("List", False, 1)
                If dsList.Tables(0).Rows.Count > 0 Then
                    intFirstOpen = CInt(dsList.Tables(0).Rows(0).Item("calendarunkid"))
                    End If
                dsList = Nothing

                For Each drTraining As DataRow In mdtReviewTraining.Rows
                    objPdpgoals_trainingneed_Tran = New clsPdpgoals_trainingneed_Tran

                    objPdpgoals_trainingneed_Tran._AuditUserId = mintAuditUserId
                    objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = mintloginemployeeunkid
                    objPdpgoals_trainingneed_Tran._HostName = mstrHostName
                    objPdpgoals_trainingneed_Tran._ClientIP = mstrClientIP
                    objPdpgoals_trainingneed_Tran._FromWeb = mblnIsWeb
                    objPdpgoals_trainingneed_Tran._DatabaseName = mstrDatabaseName
                    objPdpgoals_trainingneed_Tran._FormName = mstrFormName

                    If objPdpgoals_trainingneed_Tran.UpdateTrainingRecommendedStatus(CInt(drTraining("Trainingneedtranunkid")), True, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    objDepartmentaltrainingneed_master._ModuleId = CInt(enModuleReference.PDP)
                    objDepartmentaltrainingneed_master._ModuleTranUnkId = CInt(drTraining("Pdpgoalsmstunkid"))
                    objDepartmentaltrainingneed_master._Periodunkid = intFirstOpen
                    objDepartmentaltrainingneed_master._Startdate = Nothing
                    objDepartmentaltrainingneed_master._Enddate = Nothing

                    If CBool(drTraining("IsFromCompetency")) Then
                        objDepartmentaltrainingneed_master._Competenceunkid = CInt(drTraining("CompetencyCategoryId"))
                        objDepartmentaltrainingneed_master._Other_competence = ""
                    Else
                        If CInt(drTraining("Itemunkid")) > 0 Then
                            objDepartmentaltrainingneed_master._Competenceunkid = -1
                            objDepartmentaltrainingneed_master._Other_competence = ""
                        Else
                            objDepartmentaltrainingneed_master._Competenceunkid = -1
                            objDepartmentaltrainingneed_master._Other_competence = CStr(drTraining("Goal_Name"))
                        End If
                    End If

                    objDepartmentaltrainingneed_master._InsertFormId = 1
                    objDepartmentaltrainingneed_master._Trainingcategoryunkid = mintTrainingcategoryunkid
                    objDepartmentaltrainingneed_master._Trainingcourseunkid = CInt(drTraining("Trainingcourseunkid"))
                    objDepartmentaltrainingneed_master._Targetedgroupunkid = 0
                    objDepartmentaltrainingneed_master._Departmentunkid = mintTrainingDepartmentunkid
                    objDepartmentaltrainingneed_master._AllocationId = mintTrainingAllocationid
                    objDepartmentaltrainingneed_master._Noofstaff = 1

                    Dim objTraining_Category_Master As New clsTraining_Category_Master
                    objTraining_Category_Master._Categoryunkid(objDataOperation) = mintTrainingcategoryunkid
                    objDepartmentaltrainingneed_master._Trainingpriority = objTraining_Category_Master._Priorityunkid
                    objTraining_Category_Master = Nothing

                    objDepartmentaltrainingneed_master._Isvoid = False
                    objDepartmentaltrainingneed_master._AuditUserId = mintAuditUserId
                    objDepartmentaltrainingneed_master._AuditDate = mdtAuditDate
                    objDepartmentaltrainingneed_master._Loginemployeeunkid = mintloginemployeeunkid
                    objDepartmentaltrainingneed_master._HostName = mstrHostName
                    objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                    objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                    objDepartmentaltrainingneed_master._FormName = mstrFormName

                    Dim mintDepartmentaltrainingneedunkid As Integer = -1
                    If objDepartmentaltrainingneed_master.Insert(objDataOperation, mintDepartmentaltrainingneedunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objDepttrainingneed_employee_Tran._Isvoid = False
                    objDepttrainingneed_employee_Tran._AuditUserId = mintAuditUserId
                    objDepttrainingneed_employee_Tran._AuditDate = mdtAuditDate
                    objDepttrainingneed_employee_Tran._Loginemployeeunkid = mintloginemployeeunkid
                    objDepttrainingneed_employee_Tran._HostName = mstrHostName
                    objDepttrainingneed_employee_Tran._ClientIP = mstrClientIP
                    objDepttrainingneed_employee_Tran._Isweb = mblnIsWeb
                    objDepttrainingneed_employee_Tran._FormName = mstrFormName
                    objDepttrainingneed_employee_Tran._Employeeunkid = mintEmployeeunkid
                    objDepttrainingneed_employee_Tran._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid

                    If objDepttrainingneed_employee_Tran.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

                If objpdpform_master.UpdatePDPFormStatus(intPdpFormUnkid, clspdpform_master.enPDPFormStatus.Lock, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
            End If

            End If



            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SaveEmployeeGoal; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function



    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionplancategoryunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParentgoalunkid.ToString)
            objDataOperation.AddParameter("@goal_name", SqlDbType.NVarChar, mstrGoal_Name.Length, mstrGoal_Name.ToString)
            objDataOperation.AddParameter("@goal_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrGoal_Description.ToString)
            objDataOperation.AddParameter("@goal_evaluators", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrGoal_Evaluators.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            If mdtDuedate = Nothing Then
                objDataOperation.AddParameter("@duedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@duedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDuedate.ToString)
            End If
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@coachunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachunkid.ToString)

            strQ = "INSERT INTO pdpgoals_master ( " & _
              "  actionplancategoryunkid " & _
              ", pdpformunkid " & _
              ", itemunkid " & _
              ", parentgoalunkid " & _
              ", goal_name " & _
              ", goal_description " & _
              ", goal_evaluators " & _
              ", statusunkid " & _
              ", duedate " & _
              ", employeeunkid " & _
              ", coachunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
              ", voidloginemployeeunkid" & _
            ") VALUES (" & _
              "  @actionplancategoryunkid " & _
              ", @pdpformunkid " & _
              ", @itemunkid " & _
              ", @parentgoalunkid " & _
              ", @goal_name " & _
              ", @goal_description " & _
              ", @goal_evaluators " & _
              ", @statusunkid " & _
              ", @duedate " & _
              ", @employeeunkid " & _
              ", @coachunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @userunkid " & _
              ", @voidloginemployeeunkid" & _
            "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPdpgoalsmstunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintPdpgoalsmstunkid, mintPdpformunkid, mintActionplancategoryunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionplancategoryunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParentgoalunkid.ToString)
            objDataOperation.AddParameter("@goal_name", SqlDbType.NVarChar, mstrGoal_Name.Length, mstrGoal_Name.ToString)
            objDataOperation.AddParameter("@goal_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrGoal_Description.ToString)
            objDataOperation.AddParameter("@goal_evaluators", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrGoal_Evaluators.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            If mdtDuedate = Nothing Then
                objDataOperation.AddParameter("@duedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@duedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDuedate.ToString)
            End If
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@coachunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachunkid.ToString)

            strQ = "UPDATE pdpgoals_master SET " & _
              "  actionplancategoryunkid = @actionplancategoryunkid" & _
              ", itemunkid = @itemunkid" & _
              ", pdpformunkid = @pdpformunkid " & _
              ", parentgoalunkid = @parentgoalunkid" & _
              ", goal_name = @goal_name" & _
              ", goal_description = @goal_description" & _
              ", goal_evaluators = @goal_evaluators" & _
              ", statusunkid = @statusunkid" & _
              ", duedate = @duedate" & _
              ", employeeunkid = @employeeunkid" & _
              ", coachunkid = @coachunkid " & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", userunkid = @userunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
            "WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid and actionplancategoryunkid = @actionplancategoryunkid and pdpformunkid = @pdpformunkid and employeeunkid = @employeeunkid "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintPdpgoalsmstunkid, mintPdpformunkid, mintActionplancategoryunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Delete(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objPdpgoals_update_Tran As New clsPdpgoals_update_Tran
        Dim objPdpevaluator_comments As New clsPdpevaluator_comments
        Dim objPdpgoals_trainingneed_Tran As New clsPdpgoals_trainingneed_Tran
        Dim objPdpgoals_mentors As New clsPdpgoals_mentors
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionplancategoryunkid.ToString)

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpgoals_master SET " & _
              "  isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid= @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              "  WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid  and actionplancategoryunkid =@actionplancategoryunkid and pdpformunkid = @pdpformunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objPdpgoals_update_Tran._Isvoid = True
            objPdpgoals_update_Tran._Voiduserunkid = mintVoiduserunkid
            objPdpgoals_update_Tran._Voidloginemployeeunkid = mintVoidloginemployeeunkid
            objPdpgoals_update_Tran._Voiddatetime = mdtVoiddatetime
            objPdpgoals_update_Tran._Voidreason = mstrVoidreason

            objPdpgoals_update_Tran._AuditUserId = mintAuditUserId
            objPdpgoals_update_Tran._LoginEmployeeUnkid = mintloginemployeeunkid
            objPdpgoals_update_Tran._HostName = mstrHostName
            objPdpgoals_update_Tran._ClientIP = mstrClientIP
            objPdpgoals_update_Tran._FromWeb = mblnIsWeb
            objPdpgoals_update_Tran._FormName = mstrFormName


            If objPdpgoals_update_Tran.DeleteAll(mintPdpgoalsmstunkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objPdpevaluator_comments._Isvoid = True
            objPdpevaluator_comments._Voiduserunkid = mintVoiduserunkid
            objPdpevaluator_comments._Voidloginemployeeunkid = mintVoidloginemployeeunkid
            objPdpevaluator_comments._Voiddatetime = mdtVoiddatetime
            objPdpevaluator_comments._Voidreason = mstrVoidreason

            objPdpevaluator_comments._AuditUserId = mintAuditUserId
            objPdpevaluator_comments._LoginEmployeeUnkid = mintloginemployeeunkid
            objPdpevaluator_comments._HostName = mstrHostName
            objPdpevaluator_comments._ClientIP = mstrClientIP
            objPdpevaluator_comments._FromWeb = mblnIsWeb
            objPdpevaluator_comments._FormName = mstrFormName

            If objPdpevaluator_comments.DeleteAll(mintPdpgoalsmstunkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objPdpgoals_trainingneed_Tran._Isvoid = True
            objPdpgoals_trainingneed_Tran._Voiduserunkid = mintVoiduserunkid
            objPdpgoals_trainingneed_Tran._Voidloginemployeeunkid = mintVoidloginemployeeunkid
            objPdpgoals_trainingneed_Tran._Voiddatetime = mdtVoiddatetime
            objPdpgoals_trainingneed_Tran._Voidreason = mstrVoidreason
            objPdpgoals_trainingneed_Tran._AuditUserId = mintAuditUserId
            objPdpgoals_trainingneed_Tran._LoginEmployeeUnkid = mintloginemployeeunkid
            objPdpgoals_trainingneed_Tran._HostName = mstrHostName
            objPdpgoals_trainingneed_Tran._ClientIP = mstrClientIP
            objPdpgoals_trainingneed_Tran._FromWeb = mblnIsWeb
            objPdpgoals_trainingneed_Tran._FormName = mstrFormName

            If objPdpgoals_trainingneed_Tran.DeleteAll(mintPdpgoalsmstunkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objPdpgoals_mentors._Isvoid = True
            objPdpgoals_mentors._Voiduserunkid = mintVoiduserunkid
            objPdpgoals_mentors._Voidloginemployeeunkid = mintVoidloginemployeeunkid
            objPdpgoals_mentors._Voiddatetime = mdtVoiddatetime
            objPdpgoals_mentors._Voidreason = mstrVoidreason
            objPdpgoals_mentors._AuditUserId = mintAuditUserId
            objPdpgoals_mentors._LoginEmployeeUnkid = mintloginemployeeunkid
            objPdpgoals_mentors._HostName = mstrHostName
            objPdpgoals_mentors._ClientIP = mstrClientIP
            objPdpgoals_mentors._FromWeb = mblnIsWeb
            objPdpgoals_mentors._FormName = mstrFormName

            If objPdpgoals_mentors.DeleteAll(mintPdpgoalsmstunkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            dsList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(mintPdpgoalsmstunkid, -1, CInt(enModuleReference.PDP), -1, -1, False, objDataOperation)
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each drrow As DataRow In dsList.Tables(0).Rows
                    objDepartmentaltrainingneed_master._Isvoid = True
                    objDepartmentaltrainingneed_master._Voiduserunkid = mintVoiduserunkid
                    objDepartmentaltrainingneed_master._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    objDepartmentaltrainingneed_master._Voiddatetime = mdtVoiddatetime
                    objDepartmentaltrainingneed_master._Voidreason = mstrVoidreason

                    objDepartmentaltrainingneed_master._AuditUserId = mintAuditUserId
                    objDepartmentaltrainingneed_master._AuditDate = mdtAuditDate
                    objDepartmentaltrainingneed_master._Loginemployeeunkid = mintloginemployeeunkid
                    objDepartmentaltrainingneed_master._HostName = mstrHostName
                    objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                    objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                    objDepartmentaltrainingneed_master._FormName = mstrFormName

                    If objDepartmentaltrainingneed_master.Void(CInt(drrow("departmentaltrainingneedunkid")), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintPdpgoalsmstunkid, mintPdpformunkid, mintActionplancategoryunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objPdpgoals_update_Tran = Nothing
            objPdpevaluator_comments = Nothing
            objPdpgoals_trainingneed_Tran = Nothing
            objPdpgoals_mentors = Nothing

        End Try
    End Function

    Public Function DeleteAllParentChildGoal(ByVal intParentUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsChildList As DataSet = Nothing


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            Delete(objDataOperation)

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintPdpgoalsmstunkid, mintPdpformunkid, mintActionplancategoryunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "SELECT * from pdpgoals_master " & _
                   "WHERE parentgoalunkid = @parentgoalunkid and isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentUnkid)

            dsChildList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each drow As DataRow In dsChildList.Tables("List").Rows
                mintPdpgoalsmstunkid = CInt(drow("pdpgoalsmstunkid"))
                Delete(objDataOperation)
            Next

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteAllParentChildGoal; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsChildList IsNot Nothing Then dsChildList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function isExist(ByVal intPdpFormUnkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
               "  pdpgoalsmstunkid " & _
               ", pdpformunkid " & _
               ", actionplancategoryunkid " & _
               ", itemunkid " & _
               ", parentgoalunkid " & _
               ", goal_name " & _
               ", goal_description " & _
               ", goal_evaluators " & _
               ", statusunkid " & _
               ", duedate " & _
               ", employeeunkid " & _
               ", isvoid " & _
               " FROM pdpgoals_master where pdpformunkid = @pdpformunkid " & _
               " and actionplancategoryunkid = @actionplancategoryunkid " & _
               " and employeeunkid = @employeeunkid and isvoid = 0 " & _
               " AND pdpgoalsmstunkid = @pdpgoalsmstunkid "

            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid)
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionplancategoryunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      ByVal strMstUnkid As String, _
                                      ByVal intPdpFormUnkid As Integer, _
                                      Optional ByVal intActionplancategoryunkid As Integer = -1) As Boolean

        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpgoals_master ( " & _
                    "  tranguid " & _
                    ", pdpgoalsmstunkid " & _
                    ", pdpformunkid " & _
                    ",actionplancategoryunkid " & _
                    ",itemunkid " & _
                    ",parentgoalunkid " & _
                    ",goal_name " & _
                    ",goal_description " & _
                    ",goal_evaluators " & _
                    ",statusunkid " & _
                    ",duedate " & _
                    ",employeeunkid " & _
                    ",coachunkid " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb)" & _
                    " SELECT " & _
                    "  LOWER(NEWID()) " & _
                    ",pdpgoalsmstunkid " & _
                    ",pdpformunkid " & _
                    ",actionplancategoryunkid " & _
                    ",itemunkid " & _
                    ",parentgoalunkid " & _
                    ",goal_name " & _
                    ",goal_description " & _
                    ",goal_evaluators " & _
                    ",statusunkid " & _
                    ",duedate " & _
                    ",employeeunkid " & _
                    ",coachunkid " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    " From " & mstrDatabaseName & "..pdpgoals_master where pdpformunkid = @pdpformunkid and pdpgoalsmstunkid in ( " & strMstUnkid & " ) "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid)
            If intActionplancategoryunkid > 0 Then
                StrQ &= " AND actionplancategoryunkid = @actionplancategoryunkid "
                objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActionplancategoryunkid)
            End If

            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetActionPlanListByCategory(ByVal strTableName As String, ByVal intpdpformunkid As Integer, ByVal intActionCategoryunkid As Integer, Optional ByVal intEmployeeid As Integer = -1, Optional ByVal intPdpGoalsmstunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            strQ &= "IF OBJECT_ID('tempdb..#parentscore') IS NOT NULL DROP TABLE #parentscore " & _
                     "CREATE TABLE #parentscore ( parentgoalunkid int, AvgScore Decimal(10,2) ); " & _
                    "INSERT INTO #parentscore " & _
                         "SELECT " & _
                              "A.parentgoalunkid " & _
                              ",SUM(A.pct_updated) / COUNT(A.pct_updated) AS AvgScore " & _
                         "FROM (SELECT " & _
                                   "pdpgoals_master.parentgoalunkid " & _
                                 ",ISNULL(pdpgoals_update_tran.pct_updated,0) as pct_updated " & _
                                 ",ROW_NUMBER() OVER (PARTITION BY pdpgoals_update_tran.pdpgoalsmstunkid ORDER BY pdpgoals_update_tran.updatedatetime DESC) AS xNo " & _
                              "FROM pdpgoals_master " & _
                              "LEFT JOIN pdpgoals_update_tran " & _
                                   "ON pdpgoals_master.pdpgoalsmstunkid = pdpgoals_update_tran.pdpgoalsmstunkid AND pdpgoals_update_tran.isvoid = 0 " & _
                              "WHERE parentgoalunkid > 0 " & _
                              "AND pdpgoals_master.isvoid = 0 " & _
                              " ) AS A " & _
                         "WHERE A.xNo = 1 " & _
                         "GROUP BY A.parentgoalunkid " & _
                    "IF OBJECT_ID('tempdb..#parentchild') IS NOT NULL DROP TABLE #parentchild " & _
                     "CREATE TABLE #parentchild ( parentgoalunkid int, childs int ); " & _
                    "INSERT INTO #parentchild " & _
                         "SELECT " & _
                              "A.parentGoal " & _
                            ",COUNT(A.childGoal) AS childs " & _
                         "FROM (SELECT " & _
                                   "pdpgoals_master.pdpgoalsmstunkid AS childGoal " & _
                                 ",pdpgoals_master.parentgoalunkid AS parentGoal " & _
                              "FROM pdpgoals_master " & _
                              "WHERE parentgoalunkid > 0 " & _
                              "AND pdpgoals_master.isvoid = 0) AS A " & _
                         "GROUP BY A.parentGoal " & _
                                   ",A.childGoal "

            strQ &= "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                              "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 " & _
                         "AND iscompetencyselection = 1 "


            strQ &= "SELECT " & _
                         "* " & _
                    "FROM (SELECT " & _
                              "p.* " & _
                         "FROM (SELECT " & _
                                   "pdpgoals_master.pdpgoalsmstunkid " & _
                                 ",pdpgoals_master.actionplancategoryunkid " & _
                                 ",pdpgoals_master.employeeunkid " & _
                                 ",CASE " & _
                                        "WHEN pdpgoals_master.itemunkid > 0 THEN CASE " & _
                                                  "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                                                  "ELSE pdpitemdatatran.fieldvalue " & _
                                             "END " & _
                                        "ELSE pdpgoals_master.goal_name " & _
                                   "END AS goal_name " & _
                                 ",pdpgoals_master.goal_description " & _
                                 ",ROW_NUMBER() OVER (PARTITION BY pdpgoals_master.pdpgoalsmstunkid ORDER BY pdpgoals_update_tran.updatedatetime DESC) AS xNo " & _
                                 ",isnull(pdpgoals_master.statusunkid,0) AS statusunkid " & _
                                 ",CASE " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Pending) & " THEN @Pending " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Initiated) & " THEN @Initiated " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.InProgress) & " THEN @InProgress " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Rotting) & " THEN @Rotting " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Stopped) & " THEN @Stopped " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.OnEvaluation) & " THEN @OnEvaluation " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Completed) & " THEN @Completed " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Achieved) & " THEN @Achieved " & _
                                        "ELSE '-' " & _
                                 "END AS Status " & _
                                 ",CONVERT(CHAR(8), pdpgoals_master.duedate, 112) AS DueDate " & _
                                 ",pdpgoals_master.itemunkid " & _
                                 ",pdpgoals_master.parentgoalunkid " & _
                                 ",pdpgoals_mentors.employeeunkid AS mentoremployeeunkid " & _
                                 ",pdpgoals_master.goal_evaluators " & _
                                 ",CASE " & _
                                        "WHEN #parentchild.childs > 0 THEN ISNULL(#parentscore.AvgScore, 0) " & _
                                        "ELSE ISNULL(pdpgoals_update_tran.pct_updated, 0) " & _
                                   "END AS point " & _
                                 ",ISNULL(pdpgoals_update_tran.last_pct_update, 0) AS lastupdatepoint " & _
                                  ",ISNULL(CONVERT(VARCHAR(8), pdpgoals_update_tran.updatedatetime, 112), '') as  updatedatetime " & _
                                  ", CASE WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Pending & " THEN @Pending " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Initiated & " THEN @Initiated " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.InProgress & " THEN @InProgress " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Rotting & " THEN @Rotting " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Stopped & " THEN @Stopped " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.OnEvaluation & " THEN @OnEvaluation " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Completed & " THEN @Completed " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Achieved & " THEN @Achieved " & _
                                  "       ELSE '-' " & _
                                  "  END as lastupdatedStatus" & _
                                 ",ISNULL(pdpgoals_update_tran.statusid,0) AS lastupdatedStatusId " & _
                                 ",ISNULL(pdpgoals_update_tran.remarks,'') as remarks " & _
                                 ",ISNULL(pdpgoals_update_tran.updatetranunkid,0) AS lastUpdatedProgressId " & _
                                 ",CASE " & _
                                        "WHEN ISNULL(pdpgoals_update_tran.pct_updated, 0) >= 100 THEN 1 " & _
                                        "ELSE 0 " & _
                                   "END AS isCompleted " & _
                                 ",ISNULL(#parentchild.childs, 0) AS childs " & _
                                 ",1 AS IsParent " & _
                                 ",pdpgoals_master.pdpgoalsmstunkid AS Primary_Sort_Order " & _
                                 ",0 AS Secondary_Sort_Order " & _
                                 ",CASE " & _
                                      "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                                      "ELSE -1 " & _
                                   "END as competencyVal " & _
                                 ",ISNULL(pdpitemdatatran.iscompetencyselection, 0) AS iscompetencyselection " & _
                              "FROM pdpgoals_master " & _
                              "LEFT JOIN pdpformmaster " & _
                                   "ON pdpformmaster.pdpformunkid = pdpgoals_master.pdpformunkid " & _
                                   "AND pdpformmaster.isvoid = 0 " & _
                              "LEFT JOIN pdpitemdatatran " & _
                                   "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
                                   "AND pdpitemdatatran.isvoid = 0 " & _
                              "LEFT JOIN #competencyList " & _
                                   "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                              "LEFT JOIN pdpgoals_mentors " & _
                                   "ON pdpgoals_mentors.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_mentors.isvoid = 0 " & _
                              "LEFT JOIN pdpgoals_trainingneed_tran " & _
                                   "ON pdpgoals_trainingneed_tran.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_trainingneed_tran.isvoid = 0 " & _
                              "LEFT JOIN pdpgoals_update_tran " & _
                                   "ON pdpgoals_update_tran.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_master.employeeunkid = pdpgoals_update_tran.employeeunkid " & _
                                   "AND pdpgoals_update_tran.isvoid = 0 " & _
                              "LEFT JOIN #parentscore " & _
                                   "ON #parentscore.parentgoalunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                              "LEFT JOIN #parentchild " & _
                                   "ON #parentchild.parentgoalunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                              "WHERE pdpgoals_master.actionplancategoryunkid = @actionplancategoryunkid " & _
                              "AND pdpgoals_master.pdpformunkid = @pdpformunkid " & _
                              "AND pdpgoals_master.isvoid = 0 "


            If intEmployeeid > 0 Then
                strQ &= "AND pdpgoals_master.employeeunkid = @employeeunkid "
            End If

            If intPdpGoalsmstunkid > 0 Then
                strQ &= "AND pdpgoals_master.pdpgoalsmstunkid = @pdpgoalsmstunkid "
            
            End If


            strQ &= ") AS p " & _
                         "WHERE p.xNo = 1 " & _
                         "AND p.childs > 0 " & _
                    " " & _
                    " " & _
                         "UNION " & _
                    " " & _
                    " " & _
                         "SELECT " & _
                              "c.* " & _
                         "FROM (SELECT " & _
                                   "pdpgoals_master.pdpgoalsmstunkid " & _
                                 ",pdpgoals_master.actionplancategoryunkid " & _
                                 ",pdpgoals_master.employeeunkid " & _
                                 ",CASE " & _
                                        "WHEN pdpgoals_master.itemunkid > 0 THEN CASE " & _
                                                  "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                                                  "ELSE pdpitemdatatran.fieldvalue " & _
                                             "END " & _
                                        "ELSE pdpgoals_master.goal_name " & _
                                   "END AS goal_name " & _
                                 ",pdpgoals_master.goal_description " & _
                                 ",ROW_NUMBER() OVER (PARTITION BY pdpgoals_master.pdpgoalsmstunkid ORDER BY pdpgoals_update_tran.updatedatetime DESC) AS xNo " & _
                                 ",ISNULL(pdpgoals_master.statusunkid,0) AS statusunkid " & _
                                 ",CASE " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Pending) & " THEN @Pending " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Initiated) & " THEN @Initiated " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.InProgress) & " THEN @InProgress " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Rotting) & " THEN @Rotting " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Stopped) & " THEN @Stopped " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.OnEvaluation) & " THEN @OnEvaluation " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Completed) & " THEN @Completed " & _
                                        "WHEN CASE WHEN ISNULL (pdpgoals_update_tran.statusid,0) > 0 THEN pdpgoals_update_tran.statusid ELSE ISNULL(pdpgoals_master.statusunkid,0) END = " & CInt(enPDPActionPlanStatus.Achieved) & " THEN @Achieved " & _
                                        "ELSE '-' " & _
                                 "END AS Status " & _
                                 ",CONVERT(CHAR(8), pdpgoals_master.duedate, 112) AS DueDate " & _
                                 ",pdpgoals_master.itemunkid " & _
                                 ",pdpgoals_master.parentgoalunkid " & _
                                 ",pdpgoals_mentors.employeeunkid AS mentoremployeeunkid " & _
                                 ",pdpgoals_master.goal_evaluators " & _
                                 ",CASE " & _
                                        "WHEN #parentchild.childs > 0 THEN ISNULL(#parentscore.AvgScore, 0) " & _
                                        "ELSE ISNULL(pdpgoals_update_tran.pct_updated, 0) " & _
                                   "END AS point " & _
                                 ",ISNULL(pdpgoals_update_tran.last_pct_update, 0) AS lastupdatepoint " & _
                                 ",ISNULL(CONVERT(VARCHAR(8), pdpgoals_update_tran.updatedatetime, 112), '') as  updatedatetime " & _
                                  ", CASE WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Pending & " THEN @Pending " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Initiated & " THEN @Initiated " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.InProgress & " THEN @InProgress " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Rotting & " THEN @Rotting " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Stopped & " THEN @Stopped " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.OnEvaluation & " THEN @OnEvaluation " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Completed & " THEN @Completed " & _
                                  "       WHEN ISNULL(pdpgoals_update_tran.statusid,0) = " & enPDPActionPlanStatus.Achieved & " THEN @Achieved " & _
                                  "       ELSE '-' " & _
                                  "  END as lastupdatedStatus" & _
                                  ", ISNULL(pdpgoals_update_tran.statusid,0) AS lastupdatedStatusId " & _
                                 ",ISNULL(pdpgoals_update_tran.remarks,'') as remarks " & _
                                 ",ISNULL(pdpgoals_update_tran.updatetranunkid,0) AS lastUpdatedProgressId " & _
                                 ",CASE " & _
                                        "WHEN ISNULL(pdpgoals_update_tran.pct_updated, 0) >= 100 THEN 1 " & _
                                        "ELSE 0 " & _
                                   "END AS isCompleted " & _
                                 ",ISNULL(#parentchild.childs, 0) AS childs " & _
                                 ",0 AS IsParent " & _
                                 ",a.parentgoalunkid AS Primary_Sort_Order " & _
                                 ",ISNULL(pdpgoals_master.pdpgoalsmstunkid, 0) AS Secondary_Sort_Order " & _
                                 ",CASE " & _
                                        "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                                        "ELSE -1 " & _
                                   "END as competencyVal " & _
                                 ",ISNULL(pdpitemdatatran.iscompetencyselection, 0) AS iscompetencyselection " & _
                              "FROM pdpgoals_master " & _
                              "LEFT JOIN pdpformmaster " & _
                                   "ON pdpformmaster.pdpformunkid = pdpgoals_master.pdpformunkid " & _
                                   "AND pdpformmaster.isvoid = 0 " & _
                              "LEFT JOIN pdpitemdatatran " & _
                                   "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
                                   "AND pdpitemdatatran.isvoid = 0 " & _
                              "LEFT JOIN #competencyList " & _
                                   "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                              "LEFT JOIN pdpgoals_mentors " & _
                                   "ON pdpgoals_mentors.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_mentors.isvoid = 0 " & _
                              "LEFT JOIN pdpgoals_trainingneed_tran " & _
                                   "ON pdpgoals_trainingneed_tran.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_trainingneed_tran.isvoid = 0 " & _
                              "LEFT JOIN pdpgoals_update_tran " & _
                                   "ON pdpgoals_update_tran.pdpgoalsmstunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                                   "AND pdpgoals_master.employeeunkid = pdpgoals_update_tran.employeeunkid " & _
                                   "AND pdpgoals_update_tran.isvoid = 0 " & _
                              "LEFT JOIN #parentscore " & _
                                   "ON #parentscore.parentgoalunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                              "LEFT JOIN #parentchild " & _
                                   "ON #parentchild.parentgoalunkid = pdpgoals_master.pdpgoalsmstunkid " & _
                              "JOIN pdpgoals_master AS a " & _
                                   "ON pdpgoals_master.parentgoalunkid = a.parentgoalunkid " & _
                              "WHERE pdpgoals_master.actionplancategoryunkid = @actionplancategoryunkid " & _
                              "AND pdpgoals_master.pdpformunkid = @pdpformunkid " & _
                              "AND pdpgoals_master.isvoid = 0 "


            If intEmployeeid > 0 Then
                strQ &= "AND pdpgoals_master.employeeunkid = @employeeunkid "
            End If

            If intPdpGoalsmstunkid > 0 Then
                strQ &= "AND pdpgoals_master.pdpgoalsmstunkid = @pdpgoalsmstunkid "
            End If

            strQ &= " ) AS c " & _
                         "WHERE c.xNo = 1 " & _
                         "AND c.childs = 0) AS x " & _
                    "ORDER BY Primary_Sort_Order ASC " & _
                    ", (CASE " & _
                         "WHEN Secondary_Sort_Order IS NULL THEN 0 " & _
                         "ELSE 1 " & _
                    "END) ASC " & _
                    ", Secondary_Sort_Order ASC "

            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpformunkid)
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActionCategoryunkid)

            If intEmployeeid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            End If

            If intPdpGoalsmstunkid > 0 Then
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpGoalsmstunkid)
            End If


            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1095, "Pending"))
            objDataOperation.AddParameter("@Initiated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1096, "Initiated"))
            objDataOperation.AddParameter("@InProgress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1097, "InProgress"))
            objDataOperation.AddParameter("@Rotting", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1098, "Rotting"))
            objDataOperation.AddParameter("@Stopped", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1099, "Stopped"))
            objDataOperation.AddParameter("@OnEvaluation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1100, "OnEvaluation"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1101, "Completed"))
            objDataOperation.AddParameter("@Achieved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1101, "Achieved"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActionPlanListByCategory; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetActionPlanGoalSelectionList(ByVal strTableName As String, ByVal intEmployeeid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                              "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 " & _
                         "AND iscompetencyselection = 1 "
            strQ &= "SELECT " & _
                         "pdpitemdatatran.itemtranunkid " & _
                       ",CASE " & _
                                "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                            "ELSE pdpitemdatatran.fieldvalue " & _
                       "END AS fieldvalue " & _
                       ",ISNULL(pdpitem_master.item, '') AS item " & _
                       ",ISNULL(pdpcategory_master.category,'') as category " & _
                       ",pdpitemdatatran.categoryunkid" & _
                       ",pdpitem_master.itemtypeid " & _
                       ",pdpitem_master.selectionmodeid " & _
                       ",pdpitem_master.itemunkid " & _
                       ",0 as isgrp " & _
                       ",ISNULL(pdpitem_master.iscompetencyselectionset, 0) AS iscompetencyselectionset " & _
                       ",CASE " & _
                              "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                              "ELSE -1 " & _
                         "END AS CompetencyValue " & _
                    "FROM pdpitemdatatran " & _
                    "JOIN pdpcategory_item_mapping " & _
                         "ON pdpcategory_item_mapping.itemid = pdpitemdatatran.itemunkid " & _
                              "AND pdpcategory_item_mapping.isvoid = 0 " & _
                    "LEFT JOIN pdpformmaster " & _
                         "ON pdpitemdatatran.pdpformunkid = pdpformmaster.pdpformunkid " & _
                    "LEFT JOIN pdpcategory_master " & _
                         "ON pdpcategory_master.categoryunkid = pdpitemdatatran.categoryunkid " & _
                    "LEFT JOIN pdpitem_master " & _
                         "ON pdpitem_master.itemunkid = pdpcategory_item_mapping.itemid " & _
                    "LEFT JOIN #competencyList " & _
                         "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                    "WHERE pdpformmaster.employeeunkid = @employeeunkid " & _
                    "AND pdpitemdatatran.isvoid = 0 " & _
                    "AND pdpformmaster.isvoid = 0 " & _
                    "and pdpcategory_master.isactive = 1 " & _
                    "AND pdpitem_master.isactive = 1 " & _
                    "AND pdpitemdatatran.itemtranunkid NOT IN (SELECT " & _
                     "pdpgoals_master.itemunkid " & _
                     "FROM pdpgoals_master " & _
                     "WHERE employeeunkid = @employeeunkid " & _
                     "AND isvoid = 0 " & _
                     "AND itemunkid > 0) " & _
                    "order by pdpitemdatatran.categoryunkid,pdpitem_master.itemunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If


            For Each dRow As DataRow In dsList.Tables(0).Rows
                Select Case CInt(dRow.Item("itemtypeid"))
                    Case clspdpitem_master.enPdpCustomType.FREE_TEXT
                        dRow.Item("fieldvalue") = dRow.Item("fieldvalue")
                    Case clspdpitem_master.enPdpCustomType.SELECTION
                        If dRow.Item("fieldvalue").ToString.Trim.Length > 0 Then
                            Select Case CInt(dRow.Item("selectionmodeid"))

                                Case clspdpitem_master.enPdpSelectionMode.TRAINING_OBJECTIVE, clspdpitem_master.enPdpSelectionMode.JOB_CAPABILITIES_COURSES, clspdpitem_master.enPdpSelectionMode.CAREER_DEVELOPMENT_COURSES
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dRow.Item("fieldvalue"))
                                    dRow.Item("fieldvalue") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_COMPETENCIES
                                    Dim objCMaster As New clsassess_competencies_master
                                    objCMaster._Competenciesunkid = CInt(dRow.Item("fieldvalue"))
                                    dRow.Item("fieldvalue") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_GOALS
                                    Dim objEmpField1 As New clsassess_empfield1_master
                                    objEmpField1._Empfield1unkid = CInt(dRow.Item("fieldvalue"))
                                    dRow.Item("fieldvalue") = objEmpField1._Field_Data
                                    objEmpField1 = Nothing

                                Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dRow.Item("fieldvalue"))
                                    dRow.Item("fieldvalue") = objCMaster._Name
                                    objCMaster = Nothing
                            End Select
                        End If
                    Case clspdpitem_master.enPdpCustomType.DATE_SELECTION
                        If dRow.Item("fieldvalue").ToString.Trim.Length > 0 Then
                            dRow.Item("fieldvalue") = eZeeDate.convertDate(dRow.Item("fieldvalue").ToString).ToShortDateString
                        End If
                    Case clspdpitem_master.enPdpCustomType.NUMERIC_DATA
                        If dRow.Item("fieldvalue").ToString.Trim.Length > 0 Then
                            If IsNumeric(dRow.Item("fieldvalue")) Then
                                dRow.Item("fieldvalue") = CDbl(dRow.Item("fieldvalue")).ToString
                            End If
                        End If
                End Select
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActionPlanGoalSelectionList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function IsParentExist(ByVal intParentUnkid As Integer, _
                                  Optional ByVal xDataOpr As clsDataOperation = Nothing) As Double

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT * from pdpgoals_master WHERE parentgoalunkid > 0 " & _
                   " and isvoid = 0 and  pdpgoalsmstunkid = @parentgoalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentUnkid)


            dsList = objDataOperation.ExecQuery(strQ, "List2")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List2").Rows.Count > 0 Then
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsParentExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function IsChildExist(ByVal intPdpgoalsmstunkid As Integer, _
                                 Optional ByVal xDataOpr As clsDataOperation = Nothing) As Double

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT * from pdpgoals_master " & _
                   "WHERE parentgoalunkid = @parentgoalunkid and isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsChildExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function IsChildUpdateProgressStarted(ByVal intParentUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Double

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                     "* " & _
                    "FROM pdpgoals_update_tran " & _
                    "WHERE pdpgoalsmstunkid IN (SELECT " & _
                              "pdpgoals_master.pdpgoalsmstunkid " & _
                         "FROM pdpgoals_master " & _
                         "WHERE parentgoalunkid = @parentgoalunkid " & _
                         "AND isvoid = 0) " & _
                    "AND pdpgoals_update_tran.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parentgoalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsChildUpdateProgressStarted; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function isGoalExist(ByVal intItemUnkid As Integer, _
                                Optional ByVal intUnkid As Integer = -1, _
                                Optional ByVal strGoalName As String = "", _
                                Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
               "  pdpgoalsmstunkid " & _
               ", pdpformunkid " & _
               ", actionplancategoryunkid " & _
               ", itemunkid " & _
               ", parentgoalunkid " & _
               ", goal_name " & _
               ", goal_description " & _
               ", goal_evaluators " & _
               ", statusunkid " & _
               ", duedate " & _
               ", employeeunkid " & _
               ", isvoid " & _
               " FROM pdpgoals_master where isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND pdpgoalsmstunkid <> @pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If intItemUnkid > 0 Then
                strQ &= " AND itemunkid=@itemunkid "
                objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemUnkid)
            End If

            If strGoalName.Length > 0 Then
                strQ &= " AND goal_name=@goal_name "
                objDataOperation.AddParameter("@goal_name", SqlDbType.NVarChar, strGoalName.Length, strGoalName)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Sub SendEmails(ByVal xEmailTypeId As Integer, _
                          ByVal strSelfServiceURL As String, _
                          ByVal intCompanyId As Integer, _
                          ByVal intUserId As Integer, _
                          ByVal strPeriodName As String, _
                          ByVal strFormName As String, _
                          ByVal eMode As enLogin_Mode, _
                          ByVal strSenderAddress As String, _
                          ByVal intEmployeeunkid As Integer, _
                          ByVal xPeriodStart As Date, _
                          ByVal intYearUnkid As Integer, _
                          ByVal xEmployeeAsOnDate As String, _
                          ByVal strUserAccessMode As String, _
                          ByVal strDatabaseName As String)
        Try
            Dim objUsr As New clsUserAddEdit : Dim StrMessage As New System.Text.StringBuilder
            Dim objScreener As New clstlscreener_master
            Dim dsUserList As New DataSet
            Dim dtScreenerList As New DataTable
            Dim dsReportToUser As DataSet
            Dim objEmp As New clsEmployee_Master
            Dim objEmpReportTo As New clsEmployee_Master
            Dim strSubject As String = ""
            Dim strToEmail As String = ""
            Dim blnSendReportingTo As Boolean = False

            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo

            Select Case CInt(xEmailTypeId)
                Case CInt(enEmailType.SUBMITFORAPPROVAL)
                    'dsUserList = objUsr.Get_UserBy_EmployeeAndPrivilegeId(enUserPriviledge.AllowToLockPDPForm, intEmployeeunkid, _
                    '                                                      intCompanyId, xEmployeeAsOnDate, strUserAccessMode, intYearUnkid)


                    Dim dsList As DataSet = objEmpReportTo.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, intEmployeeunkid, xPeriodStart.Date, xPeriodStart.Date, strDatabaseName)
                    If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        objEmpReportTo._Employeeunkid(xPeriodStart.Date) = CInt(dsList.Tables(0).Rows(0)("reporttoemployeeunkid"))
                    End If

                    objEmp._Employeeunkid(xPeriodStart) = intEmployeeunkid
                    strSubject = (objEmp._Firstname & " " & objEmp._Surname) + " " + Language.getMessage(mstrModuleName, 1, "PDP Approval")
                    'For Each drUser As DataRow In dsUserList.Tables(0).Rows
                    If objEmpReportTo._Email.ToString().Trim().Length > 0 Then
                        dsReportToUser = objUsr.GetList("ReportToUser", " hrmsConfiguration..cfuser_master.employeeunkid = " & CInt(dsList.Tables(0).Rows(0)("reporttoemployeeunkid")) & " ")
                        Dim isReprotToUserAvailable As Boolean = False

                        If IsNothing(dsReportToUser) = False AndAlso dsReportToUser.Tables(0).Rows.Count > 0 Then
                            isReprotToUserAvailable = True
                        End If





                        StrMessage = New System.Text.StringBuilder
                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)

                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")


                        'StrMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " " & "<b>" & getTitleCase(objEmpReportTo._Firstname & " " & objEmpReportTo._Surname) & "</b></span></p>")

                        StrMessage.Append(Language.getMessage(mstrModuleName, 4, "This is to notify you that") & " " & "<b>" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & "</b></span></p>" & " " & Language.getMessage(mstrModuleName, 5, "has submitted the PDP form for your review."))
                        StrMessage.Append("<BR>")
                        StrMessage.Append("<BR>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 6, "It is highly recommended that you review the PDP through a one on one discussion with the employee. Please lock it after you have successfully conducted a review with the employee."))
                        StrMessage.Append("<BR>")
                        StrMessage.Append("<BR>")



                        If isReprotToUserAvailable Then
                        StrMessage.Append(Language.getMessage(mstrModuleName, 7, "The form is available on the link below:"))
                        StrMessage.Append(vbCrLf)

                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
                        Dim strLink As String = ""
                        If strSelfServiceURL.Last() = "/" Then
                            strSelfServiceURL = strSelfServiceURL.Remove(strSelfServiceURL.Length - 1, 1)
                        End If
                            strLink = strSelfServiceURL & "/PDP/wPgEmployeePdp.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & CInt(dsReportToUser.Tables(0).Rows(0)("userunkid")) & "|" & CStr(True) & "|" & intEmployeeunkid))
                        StrMessage.Append(strLink)
                        End If



                        StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                        StrMessage.Append("</span></p>")
                        StrMessage.Append("<BR>" & Language.getMessage(mstrModuleName, 8, "Regards"))
                        StrMessage.Append("<BR> <BR>" & Language.getMessage(mstrModuleName, 9, "HR Department"))
                        StrMessage.Append("</BODY></HTML>")

                        Dim strEmailContent As String
                        strEmailContent = StrMessage.ToString()

                        Dim objSendMail As New clsSendMail
                        'objSendMail._ToEmail = drUser("Uemail").ToString().Trim()
                        objSendMail._ToEmail = objEmpReportTo._Email.ToString().Trim()
                        objSendMail._Subject = strSubject
                        objSendMail._Message = strEmailContent
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = intEmployeeunkid
                        objSendMail._OperationModeId = eMode
                        objSendMail._UserUnkid = intUserId
                        objSendMail._SenderAddress = strSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PDP_MGT
                        objSendMail.SendMail(intCompanyId)
                        objSendMail = Nothing
                    End If
                    'Next

                Case CInt(enEmailType.LOCK_PDP), CInt(enEmailType.UNLOCK_PDP)
                    objEmp._Employeeunkid(xPeriodStart) = intEmployeeunkid
                    If objEmp._Email.Length <= 0 Then Exit Sub

                    strToEmail = objEmp._Email.ToString.Trim()

                    If xEmailTypeId = enEmailType.LOCK_PDP Then
                        strSubject = Language.getMessage(mstrModuleName, 10, "Your PDP is ready for execution")
                    ElseIf xEmailTypeId = enEmailType.UNLOCK_PDP Then
                        strSubject = Language.getMessage(mstrModuleName, 11, "Notification for unlock pdp form")
                    End If
                    StrMessage = New System.Text.StringBuilder
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)

                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                    StrMessage.Append(Language.getMessage(mstrModuleName, 12, "Dear") & " " & "<b>" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & "</b></span></p>")

                    If xEmailTypeId = enEmailType.LOCK_PDP Then
                        StrMessage.Append(Language.getMessage(mstrModuleName, 13, "This is to notify you that your PDP has been reviewed/revised. It is time for you to execute the plans.") & " </span></p>")
                        StrMessage.Append("<BR>")
                        StrMessage.Append("<BR>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 14, "It is recommended that you visit your PDP at least once a week to update the progress you have made and receive feedback from your supervisor, coach, mentor and others.") & " </span></p>")
                    ElseIf xEmailTypeId = enEmailType.UNLOCK_PDP Then
                        StrMessage.Append(Language.getMessage(mstrModuleName, 15, "This is to notify that your personal development form is unlocked.") & " </span></p>")
                    End If
                    StrMessage.Append("<BR>")
                    StrMessage.Append("<BR>")

                    StrMessage.Append(Language.getMessage(mstrModuleName, 16, "Click the link below to access it:"))
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
                    Dim strLink As String = ""
                    If strSelfServiceURL.Last() = "/" Then
                        strSelfServiceURL = strSelfServiceURL.Remove(strSelfServiceURL.Length - 1, 1)
                    End If
                    strLink = strSelfServiceURL & "/PDP/wPgEmployeePdp.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & intEmployeeunkid & "|" & CStr(False) & "|" & intEmployeeunkid))
                    StrMessage.Append(strLink)


                    StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                    StrMessage.Append("</span></p>")
                    StrMessage.Append("<BR>" & Language.getMessage(mstrModuleName, 8, "Regards"))
                    StrMessage.Append("<BR> <BR>" & Language.getMessage(mstrModuleName, 9, "HR Department"))
                    StrMessage.Append("</BODY></HTML>")

                    Dim objSendMail As New clsSendMail
                    objSendMail._ToEmail = strToEmail
                    objSendMail._Subject = strSubject
                    objSendMail._Message = StrMessage.ToString()
                    objSendMail._Form_Name = strFormName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = eMode
                    objSendMail._UserUnkid = intUserId
                    objSendMail._SenderAddress = strSenderAddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PDP_MGT
                    objSendMail.SendMail(intCompanyId)
                    objSendMail = Nothing


            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmails; Module Name: " & mstrModuleName)
        End Try
    End Sub


End Class