﻿'************************************************************************************************************************************
'Class Name : clsHrjob_master.vb
'Purpose    :
'Date       :28/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsJobs
    Private Shared ReadOnly mstrModuleName As String = "clsJobs"
    Dim objDataOperation As clsDataOperation
    Private objJobSkill_tran As New clsJob_Skill_Tran
    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private objJobQualification As New clsJob_Qualification_Tran
    'S.SANDEEP [ 07 NOV 2011 ] -- END
    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
    Private objJobLanguage As New clsJob_Language_Tran
    'Sohail (18 Feb 2020) -- End

    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
    Private objKeyDuties As New clsJobKeyduties_Tran
    Dim objCompetencies As New clsJobCompetencies_Tran
    'Pinkal (03-Dec-2015) -- End


    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintJobunkid As Integer
    Private mintJobgroupunkid As Integer
    Private mintJobheadunkid As Integer
    Private mintJobunitunkid As Integer
    Private mintJobsectionunkid As Integer
    Private mintJobgradeunkid As Integer
    Private mstrJob_Code As String = String.Empty
    Private mstrJob_Name As String = String.Empty
    Private mintReport_Tounkid As Integer
    Private mdtCreate_Date As Date
    Private mintTotal_Position As Integer
    Private mdtTerminate_Date As Date
    Private mstrDesciription As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mstrJob_Name1 As String = String.Empty
    Private mstrJob_Name2 As String = String.Empty
    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mintJob_Level As Integer
    'Sandeep [ 21 Aug 2010 ] -- End 

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintTeamunkid As Integer
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
    Private mintInDirectReport_Tounkid As Integer = 0
    Private mintJobDepartmentunkid As Integer = 0
    Private mintJobClassGroupunkid As Integer = 0
    Private mintExperienceYear As Integer = 0
    Private mintExperienceMonth As Integer = 0
    Private mstrExperience_Comment As String = ""
    Private mstrWorkinghrs As String = ""
    'Pinkal (03-Dec-2015) -- End

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    Private mintJobBranchUnkid As Integer = 0
    Private mintDepartmentGrpUnkId As Integer = 0
    Private mintSectionGrpUnkId As Integer = 0
    Private mintUnitGrpUnkId As Integer = 0
    Private mintClassUnkid As Integer = 0
    Private mintGradeLevelUnkId As Integer = 0
    'Shani(18-JUN-2016) -- End

    'Gajanan [31-AUG-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Private mintCritical As Integer = 0
    'Gajanan [31-AUG-2019] -- End

'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Pick employment type for type of job on job master.
    Private mintJobtypeunkid As Integer = 0
    'Sohail (18 Feb 2020) -- End

    'Gajanan [29-Oct-2020] -- Start   
    'Enhancement:Worked On Succession Module
    Private mblnIskeyrole As Boolean = False
    'Gajanan [29-Oct-2020] -- End

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobgroupunkid() As Integer
        Get
            Return mintJobgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobheadunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobheadunkid() As Integer
        Get
            Return mintJobheadunkid
        End Get
        Set(ByVal value As Integer)
            mintJobheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunitunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobunitunkid() As Integer
        Get
            Return mintJobunitunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobsectionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobsectionunkid() As Integer
        Get
            Return mintJobsectionunkid
        End Get
        Set(ByVal value As Integer)
            mintJobsectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgradeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobgradeunkid() As Integer
        Get
            Return mintJobgradeunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set job_code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Job_Code() As String
        Get
            Return mstrJob_Code
        End Get
        Set(ByVal value As String)
            mstrJob_Code = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set job_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Job_Name() As String
        Get
            Return mstrJob_Name
        End Get
        Set(ByVal value As String)
            mstrJob_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set report_tounkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Report_Tounkid() As Integer
        Get
            Return mintReport_Tounkid
        End Get
        Set(ByVal value As Integer)
            mintReport_Tounkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set create_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Create_Date() As Date
        Get
            Return mdtCreate_Date
        End Get
        Set(ByVal value As Date)
            mdtCreate_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set total_position
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Total_Position() As Integer
        Get
            Return mintTotal_Position
        End Get
        Set(ByVal value As Integer)
            mintTotal_Position = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set terminate_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Terminate_Date() As Date
        Get
            Return mdtTerminate_Date
        End Get
        Set(ByVal value As Date)
            mdtTerminate_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set desciription
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Desciription() As String
        Get
            Return mstrDesciription
        End Get
        Set(ByVal value As String)
            mstrDesciription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set job_name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Job_Name1() As String
        Get
            Return mstrJob_Name1
        End Get
        Set(ByVal value As String)
            mstrJob_Name1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set job_name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Job_Name2() As String
        Get
            Return mstrJob_Name2
        End Get
        Set(ByVal value As String)
            mstrJob_Name2 = value
        End Set
    End Property


    'Sandeep [ 21 Aug 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set Job Level
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Job_Level() As Integer
        Get
            Return mintJob_Level
        End Get
        Set(ByVal value As Integer)
            mintJob_Level = value
        End Set
    End Property
    'Sandeep [ 21 Aug 2010 ] -- End 


    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set teamunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    ''' <summary>
    ''' Purpose: Get or Set JobDepartmentunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _JobDepartmentunkid() As Integer
        Get
            Return mintJobDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintJobDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set JobClassGroupunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _JobClassGroupunkid() As Integer
        Get
            Return mintJobClassGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobClassGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set InDirectReport_Tounkid 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _InDirectReport_Tounkid() As Integer
        Get
            Return mintInDirectReport_Tounkid
        End Get
        Set(ByVal value As Integer)
            mintInDirectReport_Tounkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Experience_Year 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Experience_Year() As Integer
        Get
            Return mintExperienceYear
        End Get
        Set(ByVal value As Integer)
            mintExperienceYear = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Experience_Month
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Experience_Month() As Integer
        Get
            Return mintExperienceMonth
        End Get
        Set(ByVal value As Integer)
            mintExperienceMonth = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Experience_Comments
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Experience_Comments() As String
        Get
            Return mstrExperience_Comment
        End Get
        Set(ByVal value As String)
            mstrExperience_Comment = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WorkingHrs
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _WorkingHrs() As String
        Get
            Return mstrWorkinghrs
        End Get
        Set(ByVal value As String)
            mstrWorkinghrs = value
        End Set
    End Property
    'Pinkal (03-Dec-2015) -- End

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]

    Public Property _JobBranchUnkid() As Integer
        Get
            Return mintJobBranchUnkid
        End Get
        Set(ByVal value As Integer)
            mintJobBranchUnkid = value
        End Set
    End Property

    Public Property _DepartmentGrpUnkId() As Integer
        Get
            Return mintDepartmentGrpUnkId
        End Get
        Set(ByVal value As Integer)
            mintDepartmentGrpUnkId = value
        End Set
    End Property

    Public Property _SectionGrpUnkId() As Integer
        Get
            Return mintSectionGrpUnkId
        End Get
        Set(ByVal value As Integer)
            mintSectionGrpUnkId = value
        End Set
    End Property

    Public Property _UnitGrpUnkId() As Integer
        Get
            Return mintUnitGrpUnkId
        End Get
        Set(ByVal value As Integer)
            mintUnitGrpUnkId = value
        End Set
    End Property

    Public Property _ClassUnkid() As Integer
        Get
            Return mintClassUnkid
        End Get
        Set(ByVal value As Integer)
            mintClassUnkid = value
        End Set
    End Property

    Public Property _GradeLevelUnkId() As Integer
        Get
            Return mintGradeLevelUnkId
        End Get
        Set(ByVal value As Integer)
            mintGradeLevelUnkId = value
        End Set
    End Property

    'Shani(18-JUN-2016) -- End

    'Gajanan [31-AUG-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Public Property _Critical() As Integer
        Get
            Return mintCritical
        End Get
        Set(ByVal value As Integer)
            mintCritical = value
        End Set
    End Property
    'Gajanan [31-AUG-2019] -- End

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Pick employment type for type of job on job master.
    Public Property _Jobtypeunkid() As Integer
        Get
            Return mintJobtypeunkid
        End Get
        Set(ByVal value As Integer)
            mintJobtypeunkid = value
        End Set
    End Property

    'Gajanan [29-Oct-2020] -- Start   
    'Enhancement:Worked On Succession Module
    Public Property _Iskeyrole() As Boolean
        Get
            Return mblnIskeyrole
        End Get
        Set(ByVal value As Boolean)
            mblnIskeyrole = value
        End Set
    End Property
    'Gajanan [29-Oct-2020] -- End

    'Sohail (18 Feb 2020) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '  "  jobunkid " & _
            '  ", jobgroupunkid " & _
            '  ", jobheadunkid " & _
            '  ", jobunitunkid " & _
            '  ", jobsectionunkid " & _
            '  ", jobgradeunkid " & _
            '  ", job_code " & _
            '  ", job_name " & _
            '  ", report_tounkid " & _
            '  ", create_date " & _
            '  ", total_position " & _
            '  ", terminate_date " & _
            '  ", desciription " & _
            '  ", userunkid " & _
            '  ", isactive " & _
            '  ", job_name1 " & _
            '  ", job_name2 " & _
            '  ", ISNULL(job_level,0) As job_level " & _
            ' "FROM hrjob_master " & _
            ' "WHERE jobunkid = @jobunkid "

            strQ = "SELECT " & _
              "  jobunkid " & _
              ", jobgroupunkid " & _
              ", jobheadunkid " & _
              ", jobunitunkid " & _
              ", jobsectionunkid " & _
              ", jobgradeunkid " & _
              ", job_code " & _
              ", job_name " & _
              ", report_tounkid " & _
              ", create_date " & _
              ", total_position " & _
              ", terminate_date " & _
              ", desciription " & _
              ", userunkid " & _
              ", isactive " & _
              ", job_name1 " & _
              ", job_name2 " & _
                          ", ISNULL(job_level,0) As job_level " & _
                     ",ISNULL(teamunkid,0) As teamunkid " & _
              ", ISNULL(indirectreport_tounkid,0) AS indirectreport_tounkid " & _
              ", ISNULL(jobdepartmentunkid,0) AS jobdepartmentunkid " & _
              ", ISNULL(jobclassgroupunkid,0) AS jobclassgroupunkid " & _
              ", ISNULL(experience_year,0) AS experience_year " & _
              ", ISNULL(experience_month,0) AS experience_month" & _
              ", ISNULL(experience_comment,'') AS experience_comment " & _
              ", ISNULL(working_hrs,'') AS working_hrs " & _
                   "  , ISNULL(jobbranchunkid,0) AS jobbranchunkid " & _
                   "  , ISNULL(jobdepartmentgrpunkid,0) AS jobdepartmentgrpunkid " & _
                   "  , ISNULL(jobsectiongrpunkid,0) AS jobsectiongrpunkid " & _
                   "  , ISNULL(jobunitgrpunkid,0) AS jobunitgrpunkid " & _
                   "  , ISNULL(jobclassunkid,0) AS jobclassunkid " & _
                   "  , ISNULL(jobgradelevelunkid,0) AS jobgradelevelunkid " & _
                   "  , ISNULL(critical,0) AS critical " & _
                   "  , ISNULL(jobtypeunkid,0) AS jobtypeunkid " & _
                   "  ,  ISNULL(iskeyrole,0) as iskeyrole " & _
             "FROM hrjob_master " & _
             "WHERE jobunkid = @jobunkid "
            'Sohail (18 Feb 2020) - [jobtypeunkid]
            'Shani(18-JUN-2016) -- ADD[jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]


            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'Pinkal (03-Dec-2015) -- End

            'Gajanan [31-AUG-2019] -- ADD [critical]

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintJobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintJobheadunkid = CInt(dtRow.Item("jobheadunkid"))
                mintJobunitunkid = CInt(dtRow.Item("jobunitunkid"))
                mintJobsectionunkid = CInt(dtRow.Item("jobsectionunkid"))
                mintJobgradeunkid = CInt(dtRow.Item("jobgradeunkid"))
                mstrJob_Code = dtRow.Item("job_code").ToString
                mstrJob_Name = dtRow.Item("job_name").ToString
                mintReport_Tounkid = CInt(dtRow.Item("report_tounkid"))
                If IsDBNull(dtRow.Item("create_date")) Then
                    mdtCreate_Date = Nothing
                Else
                    mdtCreate_Date = dtRow.Item("create_date")
                End If
                mintTotal_Position = CInt(dtRow.Item("total_position"))
                If IsDBNull(dtRow.Item("terminate_date")) Then
                    mdtTerminate_Date = Nothing
                Else
                    mdtTerminate_Date = dtRow.Item("terminate_date")
                End If
                mstrDesciription = dtRow.Item("desciription").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrJob_Name1 = dtRow.Item("job_name1").ToString
                mstrJob_Name2 = dtRow.Item("job_name2").ToString
                mintJob_Level = CInt(dtRow.Item("job_level"))
                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintTeamunkid = CInt(dtRow.Item("teamunkid"))
                'S.SANDEEP [ 07 NOV 2011 ] -- END


                'Pinkal (03-Dec-2015) -- Start
                'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
                mintInDirectReport_Tounkid = CInt(dtRow.Item("indirectreport_tounkid"))
                mintJobDepartmentunkid = CInt(dtRow.Item("jobdepartmentunkid"))
                mintJobClassGroupunkid = CInt(dtRow.Item("jobclassgroupunkid"))
                mintExperienceYear = CInt(dtRow.Item("experience_year"))
                mintExperienceMonth = CInt(dtRow.Item("experience_month"))
                mstrExperience_Comment = CStr(dtRow.Item("experience_comment"))
                mstrWorkinghrs = CStr(dtRow.Item("working_hrs"))
                'Pinkal (03-Dec-2015) -- End

                'Shani(18-JUN-2016) -- Start
                'Enhancement : Add Allocation Column in Job master[Rutta]
                mintJobBranchUnkid = CStr(dtRow.Item("jobbranchunkid"))
                mintDepartmentGrpUnkId = CStr(dtRow.Item("jobdepartmentgrpunkid"))
                mintSectionGrpUnkId = CStr(dtRow.Item("jobsectiongrpunkid"))
                mintUnitGrpUnkId = CStr(dtRow.Item("jobunitgrpunkid"))
                mintClassUnkid = CStr(dtRow.Item("jobclassunkid"))
                mintGradeLevelUnkId = CStr(dtRow.Item("jobgradelevelunkid"))
                'Shani(18-JUN-2016) -- End

                'Gajanan [31-AUG-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                mintCritical = CInt(dtRow.Item("critical"))
                'Gajanan [31-AUG-2019] -- End

                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : Pick employment type for type of job on job master.
                mintJobtypeunkid = CInt(dtRow.Item("jobtypeunkid"))
                'Sohail (18 Feb 2020) -- End

                'Gajanan [29-Oct-2020] -- Start   
                'Enhancement:Worked On Succession Module
                mblnIskeyrole = CBool(dtRow.Item("iskeyrole"))
                'Gajanan [29-Oct-2020] -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal IsForImport As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            'Pinkal (10-Mar-2011)  -- Start

            'strQ = "SELECT " & _
            '            "    CASE WHEN hrjob_master.jobgroupunkid > 0 THEN hrjobgroup_master.name " & _
            '            "             WHEN hrjob_master.jobunitunkid > 0 THEN hrunit_master.name " & _
            '            "             WHEN hrjob_master.jobsectionunkid > 0 THEN hrsection_master.name " & _
            '            "             WHEN hrjob_master.jobgradeunkid > 0 THEN hrgrade_master.name " & _
            '                    "	  END AS JobsBy " & _
            '                    "	 ,hrjob_master.job_code AS Code " & _
            '                    "	 ,hrjob_master.job_name AS JobName " & _
            '                    "	 ,ISNULL(CONVERT(CHAR(8),hrjob_master.create_date,112),'') AS Createdate " & _
            '                    "	 ,ISNULL(CONVERT(CHAR(8),hrjob_master.terminate_date,112),'') AS TerminateDate " & _
            '                    "	 ,ISNULL(hrjob_master.total_position,0) AS Positon " & _
            '                    "	 ,ISNULL(ReportTo.job_name,'') AS ReportTo " & _
            '                    "	 ,hrjob_master.jobgroupunkid " & _
            '                    "	 ,hrjob_master.jobheadunkid " & _
            '                    "    ,hrjob_master.jobunitunkid " & _
            '                    "	 ,hrjob_master.jobsectionunkid " & _
            '                    "	 ,hrjob_master.jobgradeunkid " & _
            '                    "	 ,ISNULL(ReportTo.jobunkid,0)AS Reportunkid " & _
            '                    "    ,hrjob_master.jobunkid " & _
            '            "   ,ISNULL(hrjob_master.job_level,0) As job_level " & _
            '            "   ,ISNULL(ReportTo.job_level,0) As RJob_Level " & _
            '            "FROM hrjob_master " & _
            '            "    LEFT JOIN hrgrade_master ON hrjob_master.jobgradeunkid = hrgrade_master.gradeunkid " & _
            '            "    LEFT JOIN hrsection_master ON hrjob_master.jobsectionunkid = hrsection_master.sectionunkid " & _
            '            "    LEFT JOIN hrunit_master ON hrjob_master.jobunitunkid = hrunit_master.unitunkid " & _
            '            "    LEFT JOIN hrjobgroup_master ON hrjob_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
            '            "    LEFT JOIN hrjob_master AS ReportTo ON hrjob_master.report_tounkid = ReportTo.jobunkid "




            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '            " CASE WHEN hrjob_master.jobgroupunkid > 0 THEN hrjobgroup_master.name " & _
            '            "      WHEN hrjob_master.jobunitunkid > 0 THEN hrunit_master.name " & _
            '            "      WHEN hrjob_master.jobsectionunkid > 0 THEN hrsection_master.name " & _
            '            "      WHEN hrjob_master.jobgradeunkid > 0 THEN hrgrade_master.name " & _
            '            " END AS JobsBy " & _
            '            ",hrjob_master.job_code AS Code " & _
            '            ",hrjob_master.job_name AS JobName " & _
            '            ",ISNULL(CONVERT(CHAR(8),hrjob_master.create_date,112),'') AS Createdate " & _
            '            ",ISNULL(CONVERT(CHAR(8),hrjob_master.terminate_date,112),'') AS TerminateDate " & _
            '            ",ISNULL(hrjob_master.total_position,0) AS Positon " & _
            '            ",ISNULL(ReportTo.job_name,'') AS ReportTo " & _
            '            ",hrjob_master.jobgroupunkid " & _
            '            ",hrjob_master.jobheadunkid " & _
            '            ",hrjob_master.jobunitunkid " & _
            '            ",hrjob_master.jobsectionunkid " & _
            '            ",hrjob_master.jobgradeunkid " & _
            '            ",ISNULL(ReportTo.jobunkid,0)AS Reportunkid " & _
            '            ",hrjob_master.jobunkid " & _
            '            ",ISNULL(hrjob_master.job_level,0) As job_level " & _
            '            ",ISNULL(ReportTo.job_level,0) As RJob_Level "

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            'strQ = "SELECT " & _
            '       "    CASE WHEN hrjob_master.jobgroupunkid > 0 THEN hrjobgroup_master.name " & _
            '       "             WHEN hrjob_master.jobunitunkid > 0 THEN hrunit_master.name " & _
            '       "             WHEN hrjob_master.jobsectionunkid > 0 THEN hrsection_master.name " & _
            '       "             WHEN hrjob_master.jobgradeunkid > 0 THEN hrgrade_master.name " & _
            '       "     WHEN ISNULL(hrteam_master.teamunkid,0) > 0 THEN ISNULL(hrteam_master.name,'') " & _
            '       "             WHEN ISNULL(hrjob_master.jobclassgroupunkid,0) > 0 THEN ISNULL(hrdepartment_master.name,'') " & _
            '       "             WHEN ISNULL(hrjob_master.jobclassgroupunkid,0) > 0 THEN ISNULL(hrclassgroup_master.name,'') " & _
            '       "	 END AS JobsBy " & _
            '       "	,hrjob_master.job_code AS Code " & _
            '       "	,hrjob_master.job_name AS JobName " & _
            '       "	,ISNULL(CONVERT(CHAR(8),hrjob_master.create_date,112),'') AS Createdate " & _
            '       "	,ISNULL(CONVERT(CHAR(8),hrjob_master.terminate_date,112),'') AS TerminateDate " & _
            '       "	,ISNULL(hrjob_master.total_position,0) AS Positon " & _
            '       "	,ISNULL(ReportTo.job_name,'') AS ReportTo " & _
            '       "	,hrjob_master.jobgroupunkid " & _
            '       "	,hrjob_master.jobheadunkid " & _
            '       "    ,hrjob_master.jobunitunkid " & _
            '       "	,hrjob_master.jobsectionunkid " & _
            '       "	,hrjob_master.jobgradeunkid " & _
            '       "	,ISNULL(ReportTo.jobunkid,0)AS Reportunkid " & _
            '       "    ,hrjob_master.jobunkid " & _
            '       "    ,ISNULL(hrjob_master.job_level,0) As job_level " & _
            '       "    ,ISNULL(ReportTo.job_level,0) As RJob_Level "
            ''S.SANDEEP [ 07 NOV 2011 ] -- END

            ''Pinkal (03-Dec-2015) -- Start
            ''Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            'strQ &= ",  ISNULL(IDReportTo.indirectreport_tounkid,0) AS indirectreport_tounkid " & _
            '        ", ISNULL(IDReportTo.job_name,'') AS InDirectReportTo " & _
            '        ", ISNULL(hrjob_master.jobdepartmentunkid,0) AS jobdepartmentunkid " & _
            '        ", ISNULL(hrjob_master.jobclassgroupunkid,0) AS jobclassgroupunkid " & _
            '        ", ISNULL(hrjob_master.experience_year,0) AS experience_year " & _
            '        ", ISNULL(hrjob_master.experience_month,0) AS experience_month" & _
            '        ", ISNULL(hrjob_master.experience_comment,'') AS experience_comment " & _
            '        ", ISNULL(hrjob_master.working_hrs,'') AS working_hrs "
            ''Pinkal (03-Dec-2015) -- End

            'If IsForImport Then
            '    'S.SANDEEP [ 07 NOV 2011 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'strQ &= ", hrjobgroup_master.name as jobgrpoup " & _
            '    '            ", hrunit_master.name as unit " & _
            '    '            ", hrsection_master.name as section " & _
            '    '            ", hrgrade_master.name as grade "

            '    strQ &= ", hrjobgroup_master.name as jobgrpoup " & _
            '            ", hrunit_master.name as unit " & _
            '            ", hrsection_master.name as section " & _
            '            ", hrgrade_master.name as grade " & _
            '            ", hrteam_master.name as teamname "
            '    'S.SANDEEP [ 07 NOV 2011 ] -- END

            '    'Pinkal (03-Dec-2015) -- Start
            '    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            '    strQ &= ", hrdepartment_master.name as department " & _
            '            ", hrclassgroup_master.name as classgroup "
            '    'Pinkal (03-Dec-2015) -- End
            'End If
            ''S.SANDEEP [ 07 NOV 2011 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''strQ &= ",hrjob_master.job_name1,hrjob_master.job_name2,hrjob_master.isactive " & _
            ''                    "FROM hrjob_master " & _
            ''                    " LEFT JOIN hrgrade_master ON hrjob_master.jobgradeunkid = hrgrade_master.gradeunkid " & _
            ''                    " LEFT JOIN hrsection_master ON hrjob_master.jobsectionunkid = hrsection_master.sectionunkid " & _
            ''                    " LEFT JOIN hrunit_master ON hrjob_master.jobunitunkid = hrunit_master.unitunkid " & _
            ''                    " LEFT JOIN hrjobgroup_master ON hrjob_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
            ''                    " LEFT JOIN hrjob_master AS ReportTo ON hrjob_master.report_tounkid = ReportTo.jobunkid "
            'strQ &= "   ,hrjob_master.job_name1,hrjob_master.job_name2,hrjob_master.isactive,ISNULL(hrteam_master.teamunkid,0) as teamunkid " & _
            '        "FROM hrjob_master " & _
            '        "   LEFT JOIN hrgrade_master ON hrjob_master.jobgradeunkid = hrgrade_master.gradeunkid " & _
            '        "   LEFT JOIN hrsection_master ON hrjob_master.jobsectionunkid = hrsection_master.sectionunkid " & _
            '        "   LEFT JOIN hrunit_master ON hrjob_master.jobunitunkid = hrunit_master.unitunkid " & _
            '        "   LEFT JOIN hrjobgroup_master ON hrjob_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
            '        "   LEFT JOIN hrjob_master AS ReportTo ON hrjob_master.report_tounkid = ReportTo.jobunkid " & _
            '        "   LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrjob_master.teamunkid "
            ''S.SANDEEP [ 07 NOV 2011 ] -- END


            ''Pinkal (03-Dec-2015) -- Start
            ''Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            'strQ &= "   LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrjob_master.jobdepartmentunkid " & _
            '        "   LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrjob_master.jobclassgroupunkid " & _
            '        "   LEFT JOIN hrjob_master AS IDReportTo ON hrjob_master.indirectreport_tounkid = IDReportTo.jobunkid "

            ''Pinkal (03-Dec-2015) -- End

            'If blnOnlyActive = True Then
            '    strQ &= " Where hrjob_master.isactive = 1 "
            'End If

            ''S.SANDEEP [ 04 FEB 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    strQ &= " AND hrjob_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''End If

            ''S.SANDEEP [ 01 JUNE 2012 ] -- START
            ''ENHANCEMENT : TRA DISCIPLINE CHANGES
            ''Select Case ConfigParameter._Object._UserAccessModeSetting
            ''    Case enAllocation.JOBS
            ''        If UserAccessLevel._AccessLevel.Length > 0 Then
            ''            strQ &= " AND hrjob_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            ''        End If
            ''End Select

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''Dim arrID As String() = ConfigParameter._Object._UserAccessModeSetting.Split(",")
            ''For i = 0 To arrID.Length - 1
            ''    If CInt(arrID(i)) = enAllocation.JOBS Then
            ''        Dim objMstr As New clsMasterData
            ''        Dim iStrData As String = objMstr.GetUserAccessLevel(, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, enAllocation.JOBS)
            ''        If iStrData.Trim.Length > 0 Then
            ''            strQ &= " AND hrjob_master.jobunkid IN (" & iStrData & ") "
            ''        End If
            ''        objMstr = Nothing
            ''        Exit For
            ''    End If
            ''Next
            ''S.SANDEEP [04 JUN 2015] -- END
            ''S.SANDEEP [ 01 JUNE 2012 ] -- END
            ''S.SANDEEP [ 04 FEB 2012 ] -- END
            ''Pinkal (10-Mar-2011)  -- End
            ''Pinkal (03-Dec-2015) -- End

            strQ = "SELECT " & _
                        "    CASE WHEN hrjob_master.jobgroupunkid > 0 THEN hrjobgroup_master.name " & _
                        "             WHEN hrjob_master.jobunitunkid > 0 THEN hrunit_master.name " & _
                        "             WHEN hrjob_master.jobsectionunkid > 0 THEN hrsection_master.name " & _
                        "             WHEN hrjob_master.jobgradeunkid > 0 THEN hrgrade_master.name " & _
                   "      WHEN ISNULL(hrteam_master.teamunkid,0) > 0 THEN ISNULL(hrteam_master.name,'') " & _
                        "             WHEN ISNULL(hrjob_master.jobclassgroupunkid,0) > 0 THEN ISNULL(hrdepartment_master.name,'') " & _
                        "             WHEN ISNULL(hrjob_master.jobclassgroupunkid,0) > 0 THEN ISNULL(hrclassgroup_master.name,'') " & _
                                "	  END AS JobsBy " & _
                                "	 ,hrjob_master.job_code AS Code " & _
                                "	 ,hrjob_master.job_name AS JobName " & _
                                "	 ,ISNULL(CONVERT(CHAR(8),hrjob_master.create_date,112),'') AS Createdate " & _
                                "	 ,ISNULL(CONVERT(CHAR(8),hrjob_master.terminate_date,112),'') AS TerminateDate " & _
                                "	 ,ISNULL(hrjob_master.total_position,0) AS Positon " & _
                                "	 ,ISNULL(ReportTo.job_name,'') AS ReportTo " & _
                                "	 ,hrjob_master.jobgroupunkid " & _
                                "	 ,hrjob_master.jobheadunkid " & _
                                "    ,hrjob_master.jobunitunkid " & _
                                "	 ,hrjob_master.jobsectionunkid " & _
                                "	 ,hrjob_master.jobgradeunkid " & _
                                "	 ,ISNULL(ReportTo.jobunkid,0)AS Reportunkid " & _
                                "    ,hrjob_master.jobunkid " & _
                        "   ,ISNULL(hrjob_master.job_level,0) As job_level " & _
                   "    ,ISNULL(ReportTo.job_level,0) As RJob_Level " & _
                   "    ,ISNULL(IDReportTo.indirectreport_tounkid,0) AS indirectreport_tounkid " & _
                         ", ISNULL(IDReportTo.job_name,'') AS InDirectReportTo " & _
                         ", ISNULL(hrjob_master.jobdepartmentunkid,0) AS jobdepartmentunkid " & _
                         ", ISNULL(hrjob_master.jobclassgroupunkid,0) AS jobclassgroupunkid " & _
                         ", ISNULL(hrjob_master.experience_year,0) AS experience_year " & _
                         ", ISNULL(hrjob_master.experience_month,0) AS experience_month" & _
                         ", ISNULL(hrjob_master.experience_comment,'') AS experience_comment " & _
                   "    ,ISNULL(hrjob_master.working_hrs,'') AS working_hrs " & _
                   "    ,hrjob_master.jobbranchunkid " & _
                   "    ,hrjob_master.jobdepartmentgrpunkid " & _
                   "    ,hrjob_master.jobsectiongrpunkid " & _
                   "    ,hrjob_master.jobunitgrpunkid " & _
                   "    ,hrjob_master.jobclassunkid " & _
                   "    ,hrjob_master.jobgradelevelunkid " & _
                   "    ,ISNULL(hrjob_master.jobtypeunkid, 0) AS jobtypeunkid "
            'Sohail (18 Feb 2020) - [jobtypeunkid]

            If IsForImport Then
                strQ &= ", hrjobgroup_master.name as jobgrpoup " & _
                            ", hrunit_master.name as unit " & _
                            ", hrsection_master.name as section " & _
                            ", hrgrade_master.name as grade " & _
                        ", hrteam_master.name as teamname " & _
                        ", hrdepartment_master.name as department " & _
                        ", hrclassgroup_master.name as classgroup " & _
                        ", ISNULL(hrstation_master.name,'') AS branch " & _
                        ", ISNULL(hrdepartment_group_master.name,'') AS department_grp " & _
                        ", ISNULL(hrsectiongroup_master.name,'') AS section_grp " & _
                        ", ISNULL(hrunitgroup_master.name,'') AS unit_grp " & _
                        ", ISNULL(hrclasses_master.name,'') AS class " & _
                        ", ISNULL(hrgradelevel_master.name,'') AS grade_level "
            End If

            strQ &= ",hrjob_master.job_name1,hrjob_master.job_name2,hrjob_master.isactive,ISNULL(hrteam_master.teamunkid,0) as teamunkid " & _
                        "FROM hrjob_master " & _
                        "    LEFT JOIN hrgrade_master ON hrjob_master.jobgradeunkid = hrgrade_master.gradeunkid " & _
                        "    LEFT JOIN hrsection_master ON hrjob_master.jobsectionunkid = hrsection_master.sectionunkid " & _
                        "    LEFT JOIN hrunit_master ON hrjob_master.jobunitunkid = hrunit_master.unitunkid " & _
                        "    LEFT JOIN hrjobgroup_master ON hrjob_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                                " LEFT JOIN hrjob_master AS ReportTo ON hrjob_master.report_tounkid = ReportTo.jobunkid " & _
                    "   LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrjob_master.teamunkid " & _
                    "   LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrjob_master.jobdepartmentunkid " & _
                        " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = hrjob_master.jobclassgroupunkid " & _
                    "   LEFT JOIN hrjob_master AS IDReportTo ON hrjob_master.indirectreport_tounkid = IDReportTo.jobunkid " & _
                    "   LEFT JOIN hrstation_master ON hrstation_master.stationunkid = hrjob_master.jobbranchunkid " & _
                    "   LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = hrjob_master.jobdepartmentgrpunkid " & _
                    "   LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrjob_master.jobsectiongrpunkid " & _
                    "   LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrjob_master.jobunitgrpunkid " & _
                    "   LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = hrjob_master.jobclassunkid " & _
                    "   LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hrjob_master.jobgradelevelunkid "
            If blnOnlyActive = True Then
                strQ &= " Where hrjob_master.isactive = 1 "
            End If


            'Shani(18-JUN-2016) -- End
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function



    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrjob_master) </purpose>
    '''Public Function Insert(Optional ByVal mdtTable As DataTable = Nothing) As Boolean
    Public Function Insert(Optional ByVal mdtTable As DataTable = Nothing, Optional ByVal mdtQualification As DataTable = Nothing _
                                    , Optional ByVal mdtKeyDuties As DataTable = Nothing, Optional ByVal mdtCompetencies As DataTable = Nothing _
                                    , Optional ByVal mdtLangTable As DataTable = Nothing _
                                    ) As Boolean
        'Sohail (18 Feb 2020) - [mdtLangTable]
        'S.SANDEEP [ 07 NOV 2011 ] -- END

        If isExist(mstrJob_Code) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Job Code is already defined. Please define new Job Code.")
            Return False
        End If

        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(, mstrJob_Name, mintJob_Level) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Job is already defined. Please define new Job.")
        '    Return False
        'End If
        If isExist(, mstrJob_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Job is already defined. Please define new Job.")
            Return False
        End If
        'S.SANDEEP [ 07 NOV 2011 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobheadunkid.ToString)
            objDataOperation.AddParameter("@jobunitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunitunkid.ToString)
            objDataOperation.AddParameter("@jobsectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobsectionunkid.ToString)
            objDataOperation.AddParameter("@jobgradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgradeunkid.ToString)
            objDataOperation.AddParameter("@job_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Code.ToString)
            objDataOperation.AddParameter("@job_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Name.ToString)
            objDataOperation.AddParameter("@report_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReport_Tounkid.ToString)
            If mdtCreate_Date = Nothing Then
                objDataOperation.AddParameter("@create_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@create_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreate_Date)
            End If
            objDataOperation.AddParameter("@total_position", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Position.ToString)
            If mdtTerminate_Date = Nothing Then
                objDataOperation.AddParameter("@terminate_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@terminate_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTerminate_Date)
            End If
            objDataOperation.AddParameter("@desciription", SqlDbType.VarChar, 8000, mstrDesciription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@job_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Name1.ToString)
            objDataOperation.AddParameter("@job_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Name2.ToString)
            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@job_level", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJob_Level.ToString)

            'strQ = "INSERT INTO hrjob_master ( " & _
            '  "  jobgroupunkid " & _
            '  ", jobheadunkid " & _
            '  ", jobunitunkid " & _
            '  ", jobsectionunkid " & _
            '  ", jobgradeunkid " & _
            '  ", job_code " & _
            '  ", job_name " & _
            '  ", report_tounkid " & _
            '  ", create_date " & _
            '  ", total_position " & _
            '  ", terminate_date " & _
            '  ", desciription " & _
            '  ", userunkid " & _
            '  ", isactive " & _
            '  ", job_name1 " & _
            '  ", job_name2" & _
            '") VALUES (" & _
            '  "  @jobgroupunkid " & _
            '  ", @jobheadunkid " & _
            '  ", @jobunitunkid " & _
            '  ", @jobsectionunkid " & _
            '  ", @jobgradeunkid " & _
            '  ", @job_code " & _
            '  ", @job_name " & _
            '  ", @report_tounkid " & _
            '  ", @create_date " & _
            '  ", @total_position " & _
            '  ", @terminate_date " & _
            '  ", @desciription " & _
            '  ", @userunkid " & _
            '  ", @isactive " & _
            '  ", @job_name1 " & _
            '  ", @job_name2" & _
            '"); SELECT @@identity"

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)

            'strQ = "INSERT INTO hrjob_master ( " & _
            '  "  jobgroupunkid " & _
            '  ", jobheadunkid " & _
            '  ", jobunitunkid " & _
            '  ", jobsectionunkid " & _
            '  ", jobgradeunkid " & _
            '  ", job_code " & _
            '  ", job_name " & _
            '  ", report_tounkid " & _
            '  ", create_date " & _
            '  ", total_position " & _
            '  ", terminate_date " & _
            '  ", desciription " & _
            '  ", userunkid " & _
            '  ", isactive " & _
            '  ", job_name1 " & _
            '  ", job_name2" & _
            '  ", job_level" & _
            '") VALUES (" & _
            '  "  @jobgroupunkid " & _
            '  ", @jobheadunkid " & _
            '  ", @jobunitunkid " & _
            '  ", @jobsectionunkid " & _
            '  ", @jobgradeunkid " & _
            '  ", @job_code " & _
            '  ", @job_name " & _
            '  ", @report_tounkid " & _
            '  ", @create_date " & _
            '  ", @total_position " & _
            '  ", @terminate_date " & _
            '  ", @desciription " & _
            '  ", @userunkid " & _
            '  ", @isactive " & _
            '  ", @job_name1 " & _
            '  ", @job_name2" & _
            '  ", @job_level" & _
            '"); SELECT @@identity"



            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            objDataOperation.AddParameter("@indirectreport_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInDirectReport_Tounkid.ToString)
            objDataOperation.AddParameter("@jobdepartmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobDepartmentunkid.ToString)
            objDataOperation.AddParameter("@jobclassgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobClassGroupunkid.ToString)
            objDataOperation.AddParameter("@experienceyear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperienceYear.ToString)
            objDataOperation.AddParameter("@experiencemonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperienceMonth.ToString)
            objDataOperation.AddParameter("@experience_comment", SqlDbType.NVarChar, mstrExperience_Comment.Trim.Length, mstrExperience_Comment.ToString)
            objDataOperation.AddParameter("@workinghrs", SqlDbType.NVarChar, mstrWorkinghrs.Trim.Length, mstrWorkinghrs.ToString)


            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            objDataOperation.AddParameter("@jobbranchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobBranchUnkid.ToString)
            objDataOperation.AddParameter("@jobdepartmentgrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentGrpUnkId.ToString)
            objDataOperation.AddParameter("@jobsectiongrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionGrpUnkId.ToString)
            objDataOperation.AddParameter("@jobunitgrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitGrpUnkId.ToString)
            objDataOperation.AddParameter("@jobclassunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassUnkid.ToString)
            objDataOperation.AddParameter("@jobgradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelUnkId.ToString)
            'Shani(18-JUN-2016) -- End


            'Gajanan [31-AUG-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            objDataOperation.AddParameter("@critical", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCritical.ToString)
            'Gajanan [31-AUG-2019] -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Pick employment type for type of job on job master.
            objDataOperation.AddParameter("@jobtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobtypeunkid.ToString)
            'Sohail (18 Feb 2020) -- End


            'Gajanan [29-Oct-2020] -- Start   
            'Enhancement:Worked On Succession Module
            objDataOperation.AddParameter("@iskeyrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIskeyrole)
            'Gajanan [29-Oct-2020] -- End


            'Gajanan [29-Oct-2020] -- Add [iskeyrole]


            strQ = "INSERT INTO hrjob_master ( " & _
              "  jobgroupunkid " & _
              ", jobheadunkid " & _
              ", jobunitunkid " & _
              ", jobsectionunkid " & _
              ", jobgradeunkid " & _
              ", job_code " & _
              ", job_name " & _
              ", report_tounkid " & _
              ", create_date " & _
              ", total_position " & _
              ", terminate_date " & _
              ", desciription " & _
              ", userunkid " & _
              ", isactive " & _
              ", job_name1 " & _
              ", job_name2" & _
                              ", job_level" & _
                        ", teamunkid" & _
              ",indirectreport_tounkid " & _
              ",jobdepartmentunkid" & _
              ",jobclassgroupunkid" & _
              ",experience_year" & _
              ",experience_month" & _
              ",experience_comment" & _
             ",working_hrs" & _
              ", jobbranchunkid " & _
              ", jobdepartmentgrpunkid " & _
              ", jobsectiongrpunkid " & _
              ", jobunitgrpunkid " & _
              ", jobclassunkid " & _
              ", jobgradelevelunkid " & _
              ", critical " & _
              ", jobtypeunkid " & _
              ", Syncdatetime " & _
              ", iskeyrole " & _
            ") VALUES (" & _
              "  @jobgroupunkid " & _
              ", @jobheadunkid " & _
              ", @jobunitunkid " & _
              ", @jobsectionunkid " & _
              ", @jobgradeunkid " & _
              ", @job_code " & _
              ", @job_name " & _
              ", @report_tounkid " & _
              ", @create_date " & _
              ", @total_position " & _
              ", @terminate_date " & _
              ", @desciription " & _
              ", @userunkid " & _
              ", @isactive " & _
              ", @job_name1 " & _
              ", @job_name2" & _
                              ", @job_level" & _
                        ", @teamunkid" & _
              ", @indirectreport_tounkid " & _
              ", @jobdepartmentunkid" & _
              ", @jobclassgroupunkid" & _
              ", @experienceyear" & _
              ", @experiencemonth" & _
              ", @experience_comment" & _
             ",  @workinghrs" & _
              ", @jobbranchunkid " & _
              ", @jobdepartmentgrpunkid " & _
              ", @jobsectiongrpunkid " & _
              ", @jobunitgrpunkid " & _
              ", @jobclassunkid " & _
              ", @jobgradelevelunkid " & _
              ", @critical " & _
              ", @jobtypeunkid " & _
              ", NULL " & _
              ", @iskeyrole " & _
            "); SELECT @@identity"
            'Gajanan [14-July-2020] - [Syncdatetime]
            'Sohail (18 Feb 2020) - [jobtypeunkid]
            'Gajanan [31-AUG-2019] -- ADD [critical]    

            'Shani(18-JUN-2016) -- ADD -->[jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]


            'S.SANDEEP [ 07 NOV 2011 ] -- END


            'Pinkal (03-Dec-2015) -- End




            'Sandeep [ 21 Aug 2010 ] -- End 
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintJobunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (28-oct-2010) -- START



            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim objUserAddEdit As New clsUserAddEdit
            'If objUserAddEdit.InsertAccess(1, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, mintJobunkid, mintJob_Level, enAllocation.JOBS) = True Then
            '    If User._Object._Userunkid = 1 Then
            '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
            '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
            '        'If ConfigParameter._Object._UserAccessModeSetting = enAllocation.JOBS Then
            '        '    If UserAccessLevel._AccessLevel.Trim.Length > 0 Then
            '        '        UserAccessLevel._AccessLevel = UserAccessLevel._AccessLevel & "," & mintJobunkid.ToString
            '        '    End If
            '        'End If
            '        Dim arrId() As String = ConfigParameter._Object._UserAccessModeSetting.Split(",")
            '        Dim objMaster As New clsMasterData
            '        For i As Integer = 0 To arrId.Length - 1
            '            If CInt(arrId(i)) = enAllocation.JOBS Then
            '                objMaster.GetUserAccessLevel()
            '            End If
            '        Next
            '        'S.SANDEEP [ 01 JUNE 2012 ] -- END
            '    End If
            'End If
            'objUserAddEdit = Nothing
            'S.SANDEEP [04 JUN 2015] -- END

            
            'strQ = "INSERT INTO hrmsConfiguration..cfuseraccess_privilege " & _
            '                   "(userunkid " & _
            '                   ",jobunkid " & _
            '                   ",job_level " & _
            '                   ",companyunkid " & _
            '                   ",yearunkid )" & _
            '                   "values " & _
            '                   "(@userunkid " & _
            '                   ",@jobunkid " & _
            '                   ",@job_level " & _
            '                   ",@companyunkid " & _
            '                   ",@yearunkid) "

            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 1) 'Anjan (20 Aug 2011)-Start Please never change this , 1 id is to be passed here.

            'objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)
            'objDataOperation.AddParameter("@job_level", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJob_Level)
            'objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Company._Object._Companyunkid)
            'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
            'objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            ''Sandeep | 04 JAN 2010 | -- Start
            'If User._Object._Userunkid = 1 Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        UserAccessLevel._AccessLevel = UserAccessLevel._AccessLevel & "," & mintJobunkid
            '    End If
            'End If
            ''Sandeep | 04 JAN 2010 | -- END
            'S.SANDEEP [ 04 FEB 2012 ] -- END




            'Pinkal (28-oct-2010) -- END


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            If mdtTable IsNot Nothing Then
                If mdtTable.Rows.Count > 0 Then
                    objJobSkill_tran._JobUnkid = mintJobunkid
                    objJobSkill_tran._DataTable = mdtTable
                    With objJobSkill_tran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objJobSkill_tran.InsertUpdateDelete_JobSkills() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrjob_master", mintJobunkid, "jobunkid", 2) Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjob_skill_tran", "jobskilltranunkid", -1, 1, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtQualification IsNot Nothing Then
                If mdtQualification.Rows.Count > 0 Then
                    objJobQualification._JobUnkid = mintJobunkid
                    objJobQualification._DataTable = mdtQualification
                    With objJobQualification
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objJobQualification.InsertUpdateDelete_JobQualification = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrjob_master", mintJobunkid, "jobunkid", 2) Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjob_skill_tran", "jobskilltranunkid", -1, 1, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If
            End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END



            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            If mdtKeyDuties IsNot Nothing Then
                If mdtKeyDuties.Rows.Count > 0 Then
                    objKeyDuties._Jobunkid = mintJobunkid
                    objKeyDuties._mdtKeyDuties = mdtKeyDuties
                    objKeyDuties._Userunkid = mintUserunkid
                    With objKeyDuties
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objKeyDuties.InsertUpdateDelete_JobKeyDuties(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If mdtCompetencies IsNot Nothing Then
                If mdtCompetencies.Rows.Count > 0 Then
                    objCompetencies._Jobunkid = mintJobunkid
                    objCompetencies._mdtCompetenciesTran = mdtCompetencies
                    objCompetencies._Userunkid = mintUserunkid
                    With objCompetencies
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objCompetencies.InsertUpdateDelete_JobCompetencies(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (03-Dec-2015) -- End

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            If ConfigParameter._Object._AllowToSyncJobWithVacancyMaster Then
                Dim objCommon_Master As New clsCommon_Master
                objCommon_Master._JobMstunkid = mintJobunkid
                objCommon_Master._Name = mstrJob_Name
                objCommon_Master._Code = mstrJob_Code
                objCommon_Master._Mastertype = clsCommon_Master.enCommonMaster.VACANCY_MASTER
                objCommon_Master._Userunkid = User._Object._Userunkid

                objCommon_Master._FormName = mstrModuleName
                objCommon_Master._ClientIP = getIP()
                objCommon_Master._HostName = getHostName()
                objCommon_Master._FromWeb = False
                objCommon_Master._AuditUserId = User._Object._Userunkid
                objCommon_Master._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                objCommon_Master._CompanyUnkid = Company._Object._Companyunkid

                If objCommon_Master.Insert(Nothing, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'Gajanan [13-Nov-2020] -- END

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            If mdtLangTable IsNot Nothing Then
                If mdtLangTable.Rows.Count > 0 Then
                    objJobLanguage._JobUnkid = mintJobunkid
                    objJobLanguage._DataTable = mdtLangTable
                    objJobLanguage._xDataOp = objDataOperation
                    If objJobLanguage.InsertUpdateDelete_JobLanguages = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END
                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrjob_master", mintJobunkid, "jobunkid", 2) Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjob_language_tran", "joblanguagetranunkid", -1, 1, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    objCommonATLog = Nothing
                End If
            End If
            'Sohail (18 Feb 2020) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrjob_master) </purpose>
    '''Public Function Update(Optional ByVal mdtTable As DataTable = Nothing) As Boolean
    Public Function Update(Optional ByVal mdtTable As DataTable = Nothing, Optional ByVal mdtQualification As DataTable = Nothing _
                                    , Optional ByVal mdtKeyDuties As DataTable = Nothing, Optional ByVal mdtCompetencies As DataTable = Nothing _
                                    , Optional ByVal mdtLangTable As DataTable = Nothing _
                                    ) As Boolean
        'Sohail (18 Feb 2020) - [mdtLangTable]
        'S.SANDEEP [ 07 NOV 2011 ] -- END
        If isExist(mstrJob_Code, , , mintJobunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Job Code is already defined. Please define new Job Code.")
            Return False
        End If

        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(, mstrJob_Name, mintJob_Level, mintJobunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Job is already defined. Please define new Job.")
        '    Return False
        'End If
        If isExist(, mstrJob_Name, , mintJobunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Job is already defined. Please define new Job.")
            Return False
        End If
        'S.SANDEEP [ 07 NOV 2011 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobheadunkid.ToString)
            objDataOperation.AddParameter("@jobunitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunitunkid.ToString)
            objDataOperation.AddParameter("@jobsectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobsectionunkid.ToString)
            objDataOperation.AddParameter("@jobgradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgradeunkid.ToString)
            objDataOperation.AddParameter("@job_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Code.ToString)
            objDataOperation.AddParameter("@job_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Name.ToString)
            objDataOperation.AddParameter("@report_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReport_Tounkid.ToString)
            If mdtCreate_Date = Nothing Then
                objDataOperation.AddParameter("@create_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@create_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreate_Date)
            End If
            objDataOperation.AddParameter("@total_position", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Position.ToString)
            If mdtTerminate_Date = Nothing Then
                objDataOperation.AddParameter("@terminate_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@terminate_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTerminate_Date)
            End If
            objDataOperation.AddParameter("@desciription", SqlDbType.VarChar, 8000, mstrDesciription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@job_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Name1.ToString)
            objDataOperation.AddParameter("@job_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob_Name2.ToString)
            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@job_level", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJob_Level.ToString)

            'strQ = "UPDATE hrjob_master SET " & _
            '          "  jobgroupunkid = @jobgroupunkid" & _
            '          ", jobheadunkid = @jobheadunkid" & _
            '          ", jobunitunkid = @jobunitunkid" & _
            '          ", jobsectionunkid = @jobsectionunkid" & _
            '          ", jobgradeunkid = @jobgradeunkid" & _
            '          ", job_code = @job_code" & _
            '          ", job_name = @job_name" & _
            '          ", report_tounkid = @report_tounkid" & _
            '          ", create_date = @create_date" & _
            '          ", total_position = @total_position" & _
            '          ", terminate_date = @terminate_date" & _
            '          ", desciription = @desciription" & _
            '          ", userunkid = @userunkid" & _
            '          ", isactive = @isactive" & _
            '          ", job_name1 = @job_name1" & _
            '          ", job_name2 = @job_name2 " & _
            '        "WHERE jobunkid = @jobunkid "

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)

            'strQ = "UPDATE hrjob_master SET " & _
            '          "  jobgroupunkid = @jobgroupunkid" & _
            '          ", jobheadunkid = @jobheadunkid" & _
            '          ", jobunitunkid = @jobunitunkid" & _
            '          ", jobsectionunkid = @jobsectionunkid" & _
            '          ", jobgradeunkid = @jobgradeunkid" & _
            '          ", job_code = @job_code" & _
            '          ", job_name = @job_name" & _
            '          ", report_tounkid = @report_tounkid" & _
            '          ", create_date = @create_date" & _
            '          ", total_position = @total_position" & _
            '          ", terminate_date = @terminate_date" & _
            '          ", desciription = @desciription" & _
            '          ", userunkid = @userunkid" & _
            '          ", isactive = @isactive" & _
            '          ", job_name1 = @job_name1" & _
            '          ", job_name2 = @job_name2 " & _
            '              ", job_level = @job_level " & _
            '        "WHERE jobunkid = @jobunkid "

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            objDataOperation.AddParameter("@indirectreport_tounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInDirectReport_Tounkid.ToString)
            objDataOperation.AddParameter("@jobdepartmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobDepartmentunkid.ToString)
            objDataOperation.AddParameter("@jobclassgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobClassGroupunkid.ToString)
            objDataOperation.AddParameter("@experienceyear", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperienceYear.ToString)
            objDataOperation.AddParameter("@experiencemonth", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperienceMonth.ToString)
            objDataOperation.AddParameter("@experience_comment", SqlDbType.NVarChar, mstrExperience_Comment.Trim.Length, mstrExperience_Comment.ToString)
            objDataOperation.AddParameter("@workinghrs", SqlDbType.NVarChar, mstrWorkinghrs.Trim.Length, mstrWorkinghrs.ToString)
            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            objDataOperation.AddParameter("@jobbranchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobBranchUnkid.ToString)
            objDataOperation.AddParameter("@jobdepartmentgrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentGrpUnkId.ToString)
            objDataOperation.AddParameter("@jobsectiongrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionGrpUnkId.ToString)
            objDataOperation.AddParameter("@jobunitgrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitGrpUnkId.ToString)
            objDataOperation.AddParameter("@jobclassunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassUnkid.ToString)
            objDataOperation.AddParameter("@jobgradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelUnkId.ToString)
            'Shani(18-JUN-2016) -- End

            'Gajanan [31-AUG-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            objDataOperation.AddParameter("@critical", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCritical.ToString)
            'Gajanan [31-AUG-2019] -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Pick employment type for type of job on job master.
            objDataOperation.AddParameter("@jobtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobtypeunkid.ToString)
            'Sohail (18 Feb 2020) -- End

            'Gajanan [29-Oct-2020] -- Start   
            'Enhancement:Worked On Succession Module
            objDataOperation.AddParameter("@iskeyrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIskeyrole)
            'Gajanan [29-Oct-2020] -- End


            strQ = "UPDATE hrjob_master SET " & _
                      "  jobgroupunkid = @jobgroupunkid" & _
                      ", jobheadunkid = @jobheadunkid" & _
                      ", jobunitunkid = @jobunitunkid" & _
                      ", jobsectionunkid = @jobsectionunkid" & _
                      ", jobgradeunkid = @jobgradeunkid" & _
                      ", job_code = @job_code" & _
                      ", job_name = @job_name" & _
                      ", report_tounkid = @report_tounkid" & _
                      ", create_date = @create_date" & _
                      ", total_position = @total_position" & _
                      ", terminate_date = @terminate_date" & _
                      ", desciription = @desciription" & _
                      ", userunkid = @userunkid" & _
                      ", isactive = @isactive" & _
                      ", job_name1 = @job_name1" & _
                      ", job_name2 = @job_name2 " & _
                          ", job_level = @job_level " & _
                        ", teamunkid = @teamunkid " & _
                      ", indirectreport_tounkid = @indirectreport_tounkid " & _
                      ", jobdepartmentunkid = @jobdepartmentunkid " & _
                      ", jobclassgroupunkid = @jobclassgroupunkid " & _
                      ", experience_year = @experienceyear " & _
                      ", experience_month = @experiencemonth " & _
                      ", experience_comment = @experience_comment " & _
                      ", working_hrs = @workinghrs " & _
                      ", jobbranchunkid = @jobbranchunkid " & _
                      ", jobdepartmentgrpunkid = @jobdepartmentgrpunkid " & _
                      ", jobsectiongrpunkid = @jobsectiongrpunkid " & _
                      ", jobunitgrpunkid = @jobunitgrpunkid " & _
                      ", jobclassunkid = @jobclassunkid " & _
                      ", jobgradelevelunkid = @jobgradelevelunkid " & _
                      ", critical = @critical " & _
                      ", jobtypeunkid = @jobtypeunkid " & _
                      ", Syncdatetime  = NULL " & _
                      ", iskeyrole  = @iskeyrole " & _
                    "WHERE jobunkid = @jobunkid "
            'Gajanan [14-July-2020] - [Syncdatetime]
            'Sohail (18 Feb 2020) - [jobtypeunkid]
            'Gajanan [31-AUG-2019] -- Add [critical]

            'Shani(18-JUN-2016) --ADD-->[jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]

            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            'Pinkal (03-Dec-2015) -- End



            'Sandeep [ 21 Aug 2010 ] -- End 
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtTable IsNot Nothing Then

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                Dim dt() As DataRow = mdtTable.Select("AUD=''")
                If dt.Length = mdtTable.Rows.Count Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrjob_master", mintJobunkid, "jobunkid", 2) Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjob_skill_tran", "jobskilltranunkid", -1, 2, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Else
                    'Sohail (12 Jan 2019) -- Start
                    'AT Testing - 76.1 - Fixing AT Issues.
                    With objJobSkill_tran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    'Sohail (12 Jan 2019) -- End
                    objJobSkill_tran._JobUnkid = mintJobunkid
                    objJobSkill_tran._DataTable = mdtTable
                    If objJobSkill_tran.InsertUpdateDelete_JobSkills() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
            End If

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtQualification IsNot Nothing Then
                Dim dt() As DataRow = mdtQualification.Select("AUD=''")
                If dt.Length = mdtQualification.Rows.Count Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrjob_master", mintJobunkid, "jobunkid", 2) Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjob_skill_tran", "jobskilltranunkid", -1, 2, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Else
                    objJobQualification._JobUnkid = mintJobunkid
                    objJobQualification._DataTable = mdtQualification
                    With objJobQualification
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objJobQualification.InsertUpdateDelete_JobQualification() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            If mdtKeyDuties IsNot Nothing Then
                If mdtKeyDuties.Rows.Count > 0 Then
                    objKeyDuties._Jobunkid = mintJobunkid
                    objKeyDuties._mdtKeyDuties = mdtKeyDuties
                    objKeyDuties._Userunkid = mintUserunkid
                    With objKeyDuties
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objKeyDuties.InsertUpdateDelete_JobKeyDuties(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If mdtCompetencies IsNot Nothing Then
                If mdtCompetencies.Rows.Count > 0 Then
                    objCompetencies._Jobunkid = mintJobunkid
                    objCompetencies._mdtCompetenciesTran = mdtCompetencies
                    objCompetencies._Userunkid = mintUserunkid
                    With objCompetencies
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objCompetencies.InsertUpdateDelete_JobCompetencies(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (03-Dec-2015) -- End

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            If mdtLangTable IsNot Nothing Then
                Dim dt() As DataRow = mdtLangTable.Select("AUD=''")                
                If dt.Length = mdtLangTable.Rows.Count Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END
                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrjob_master", mintJobunkid, "jobunkid", 2) Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjob_language_tran", "joblanguagetranunkid", -1, 2, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    objCommonATLog = Nothing
                Else
                    objJobLanguage._JobUnkid = mintJobunkid
                    objJobLanguage._DataTable = mdtLangTable
                    objJobLanguage._xDataOp = objDataOperation
                    If objJobLanguage.InsertUpdateDelete_JobLanguages() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If
            'Sohail (18 Feb 2020) -- End


            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            If ConfigParameter._Object._AllowToSyncJobWithVacancyMaster Then
                Dim objCommon_Master As New clsCommon_Master
                objCommon_Master._JobMstunkid = mintJobunkid
                objCommon_Master._Name = mstrJob_Name
                objCommon_Master._Code = mstrJob_Code
                objCommon_Master._Mastertype = clsCommon_Master.enCommonMaster.VACANCY_MASTER
                objCommon_Master._Userunkid = User._Object._Userunkid

                objCommon_Master._FormName = mstrModuleName
                objCommon_Master._ClientIP = getIP()
                objCommon_Master._HostName = getHostName()
                objCommon_Master._FromWeb = False
                objCommon_Master._AuditUserId = User._Object._Userunkid
                objCommon_Master._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                objCommon_Master._CompanyUnkid = Company._Object._Companyunkid

                If objCommon_Master.isSyncJobExist(CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER), mintJobunkid, Nothing) Then
                    If objCommon_Master.Update(Nothing, mintJobunkid, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If objCommon_Master.Insert(Nothing, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If


            End If
            'Gajanan [13-Nov-2020] -- END



            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (03-Dec-2015) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrjob_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this job. Reason : This job is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            strQ = "UPDATE hrjob_master SET " & _
                   " isactive = 0 " & _
                   ", Syncdatetime  = NULL " & _
            "WHERE jobunkid = @jobunkid "
            'Gajanan [14-July-2020] - [Syncdatetime]

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "hrjob_skill_tran", "jobunkid", intUnkid)
            objCommonATLog = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", intUnkid, "hrjob_skill_tran", "jobskilltranunkid", dtRow.Item("jobskilltranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
            Else
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", intUnkid, "hrjob_skill_tran", "jobskilltranunkid", -1, 3, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            strQ = "DELETE FROM hrjob_skill_tran WHERE jobunkid = @jobunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objCommonATLog = New clsCommonATLog
            dsList = objCommonATLog.GetChildList(objDataOperation, "hrjob_qualification_tran", "jobunkid", intUnkid)
            objCommonATLog = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", intUnkid, "hrjob_qualification_tran", "jobqualificationtranunkid", dtRow.Item("jobqualificationtranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
            Else
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", intUnkid, "hrjob_qualification_tran", "jobqualificationtranunkid", -1, 3, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            strQ = "DELETE FROM hrjob_qualification_tran WHERE jobunkid = @jobunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objKeyDuties._Userunkid = mintUserunkid
            objKeyDuties._FormName = mstrFormName
            objKeyDuties._LoginEmployeeunkid = mintLoginEmployeeunkid
            objKeyDuties._ClientIP = mstrClientIP
            objKeyDuties._HostName = mstrHostName
            objKeyDuties._FromWeb = mblnIsWeb
            objKeyDuties._AuditUserId = mintAuditUserId
objKeyDuties._CompanyUnkid = mintCompanyUnkid
            objKeyDuties._AuditDate = mdtAuditDate

            If objKeyDuties.DeleteKeyDuties_tran(objDataOperation, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objCompetencies._Userunkid = mintUserunkid
            objCompetencies._FormName = mstrFormName
            objCompetencies._LoginEmployeeunkid = mintLoginEmployeeunkid
            objCompetencies._ClientIP = mstrClientIP
            objCompetencies._HostName = mstrHostName
            objCompetencies._FromWeb = mblnIsWeb
            objCompetencies._AuditUserId = mintAuditUserId
objCompetencies._CompanyUnkid = mintCompanyUnkid
            objCompetencies._AuditDate = mdtAuditDate

            If objCompetencies.DeleteJobCompetencies_tran(objDataOperation, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (03-Dec-2015) -- End

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            objJobLanguage._xDataOp = objDataOperation
            If objJobLanguage.Delete_JobLanguages(intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (18 Feb 2020) -- End

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            If ConfigParameter._Object._AllowToSyncJobWithVacancyMaster Then
                Dim objCommon_Master As New clsCommon_Master
                objCommon_Master._Name = mstrJob_Name
                objCommon_Master._Code = mstrJob_Code
                objCommon_Master._Mastertype = clsCommon_Master.enCommonMaster.VACANCY_MASTER
                objCommon_Master._Userunkid = User._Object._Userunkid

                objCommon_Master._FormName = mstrModuleName
                objCommon_Master._ClientIP = getIP()
                objCommon_Master._HostName = getHostName()
                objCommon_Master._FromWeb = False
                objCommon_Master._AuditUserId = User._Object._Userunkid
                objCommon_Master._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                objCommon_Master._CompanyUnkid = Company._Object._Companyunkid

                If objCommon_Master.Delete(-1, CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER), intUnkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'Gajanan [13-Nov-2020] -- END



            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 18 Aug 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 18 Aug 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " TABLE_NAME AS TableName " & _
                   " FROM INFORMATION_SCHEMA.COLUMNS " & _
                   " WHERE COLUMN_NAME='jobunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                'hrjob_skill_tran


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'If dtRow.Item("TableName") = "hrjob_master" Then Continue For


                'Pinkal (03-Dec-2015) -- Start
                'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
                'If dtRow.Item("TableName").ToString.Trim = "hrjob_master" Or dtRow.Item("TableName").ToString.Trim = "hrjob_skill_tran" Then Continue For
                If dtRow.Item("TableName").ToString.Trim = "hrjob_master" Or dtRow.Item("TableName").ToString.Trim = "hrjob_skill_tran" _
                  Or dtRow.Item("TableName") = "hrjob_qualification_tran" Or dtRow.Item("TableName") = "hrjobkeyduties_tran" _
                  Or dtRow.Item("TableName") = "hrjobcompetencies_tran" Then Continue For
                'Pinkal (03-Dec-2015) -- End





                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                strQ = "SELECT jobunkid FROM " & dtRow.Item("TableName").ToString & " WHERE jobunkid = @jobunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
                If dsList.Tables("Used").Rows.Count > 0 AndAlso dtRow.Item("TableName").ToString().ToUpper() <> "CFCOMMON_MASTER" Then
            'Gajanan [13-Nov-2020] -- End
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intLevel As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        ' 'Changes done by Pinkal 20-Aug-2010 AND isactive = 1

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        "  jobunkid " & _
                        ", jobgroupunkid " & _
                        ", jobheadunkid " & _
                        ", jobunitunkid " & _
                        ", jobsectionunkid " & _
                        ", jobgradeunkid " & _
                        ", job_code " & _
                        ", job_name " & _
                        ", report_tounkid " & _
                        ", create_date " & _
                        ", total_position " & _
                        ", terminate_date " & _
                        ", desciription " & _
                        ", userunkid " & _
                        ", isactive " & _
                        ", job_name1 " & _
                        ", job_name2 " & _
                        ", job_level " & _
                        ", critical " & _
                        ", iskeyrole " & _
                    "FROM hrjob_master " & _
                    "WHERE 1=1 AND isactive = 1"

            'Gajanan [31-AUG-2019] -- Add [critical]

            If strName.Length > 0 Then
                strQ &= "And job_name = @job_name "
                objDataOperation.AddParameter("@job_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND job_code = @job_code "
                objDataOperation.AddParameter("@job_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If intUnkid > 0 Then
                strQ &= " AND jobunkid <> @jobunkid"
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If intLevel > -1 Then
                strQ &= " AND job_level = @job_level"
                objDataOperation.AddParameter("@job_level", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", _
                                 Optional ByVal mblnFlag As Boolean = False, _
                                 Optional ByVal intJobgroupid As Integer = -1, _
                                 Optional ByVal intUnitId As Integer = -1, _
                                 Optional ByVal intSectionId As Integer = -1, _
                                 Optional ByVal intGradeId As Integer = -1, _
                                 Optional ByVal intTeamUnkid As Integer = -1, _
                                 Optional ByVal mblnWithLevel As Boolean = False, _
                                 Optional ByVal strUserAccessModeSetting As String = "", _
                                 Optional ByVal intDepartmentUnkid As Integer = -1, _
                                 Optional ByVal intClassGroupUnkid As Integer = -1, _
                                 Optional ByVal intBrachUnkID As Integer = -1, _
                                 Optional ByVal intDepartmentGrpUnkID As Integer = -1, _
                                 Optional ByVal intSectionGrpUnkID As Integer = -1, _
                                 Optional ByVal intUnitGrpUnkID As Integer = -1, _
                                 Optional ByVal intClassUnkID As Integer = -1, _
                                 Optional ByVal intGradeLevelUnkID As Integer = -1, _
                                 Optional ByVal mblnIskeyrole As Boolean = False) As DataSet

        'Shani(18-JUN-2016) --ADD--> [jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]


        '                        'Sohail (18 May 2013) - [strUserAccessModeSetting]


        'Pinkal (03-Dec-2015) -- Start
        'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga. [ptional ByVal intDepartmentUnkid As Integer = -1,  Optional ByVal intClassGroupUnkid As Integer = -1]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            If strUserAccessModeSetting.Trim = "" Then strUserAccessModeSetting = ConfigParameter._Object._UserAccessModeSetting
            'Sohail (18 May 2013) -- End

            strQ = "SELECT " & _
                   " jobunkid As jobunkid " & _
                   ",name As name " & _
                   ",job_level AS job_level " & _
                   " FROM ( "
            If mblnFlag = True Then
                strQ &= "SELECT 0 As jobunkid , @ItemName As  name , -1 As job_level  UNION "
            End If

            If mblnWithLevel = True Then
                strQ &= "SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As name FROM hrjob_master WHERE isactive =1 "
            Else
                If mblnIskeyrole = False Then
                strQ &= "SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 "
            End If
            End If


            If intJobgroupid > 0 Or intUnitId > 0 Or intGradeId > 0 Or intSectionId > 0 Then
                strQ &= " AND jobgroupunkid<=0 AND jobunitunkid<=0 AND jobsectionunkid<=0 AND jobgradeunkid<=0 "
            End If


            If intJobgroupid > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobgroupunkid = @jobgroupunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobgroupunkid = @jobgroupunkid"
                End If
                objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobgroupid)
            End If

            If intUnitId > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobunitunkid = @UnitId"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobunitunkid = @UnitId"
                End If
                objDataOperation.AddParameter("@UnitId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitId)
            End If

            If intSectionId > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobsectionunkid = @SectionId"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobsectionunkid = @SectionId"
                End If
                objDataOperation.AddParameter("@SectionId", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionId)
            End If

            If intGradeId > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobgradeunkid = @GradeId"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobgradeunkid = @GradeId"
                End If
                objDataOperation.AddParameter("@GradeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId)
            End If

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intTeamUnkid > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND teamunkid = @TeamId"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND teamunkid = @TeamId"
                End If
                objDataOperation.AddParameter("@TeamId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTeamUnkid)
            End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END



            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            If intDepartmentUnkid > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobdepartmentunkid = @jobdepartmentunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobdepartmentunkid = @jobdepartmentunkid"
                End If
                objDataOperation.AddParameter("@jobdepartmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentUnkid)
            End If

            If intClassGroupUnkid > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobclassgroupunkid = @jobclassgroupunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobclassgroupunkid = @jobclassgroupunkid"
                End If
                objDataOperation.AddParameter("@jobclassgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassGroupUnkid)
            End If

            'Pinkal (03-Dec-2015) -- End


            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            If intBrachUnkID > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobbranchunkid = @jobbranchunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobbranchunkid = @jobbranchunkid"
                End If
                objDataOperation.AddParameter("@jobbranchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBrachUnkID)
            End If

            If intDepartmentGrpUnkID > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobdepartmentgrpunkid = @jobdepartmentgrpunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobdepartmentgrpunkid = @jobdepartmentgrpunkid"
                End If
                objDataOperation.AddParameter("@jobdepartmentgrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentGrpUnkID)
            End If
            If intSectionGrpUnkID > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobsectiongrpunkid = @jobsectiongrpunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobsectiongrpunkid = @jobsectiongrpunkid"
                End If
                objDataOperation.AddParameter("@jobsectiongrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionGrpUnkID)
            End If
            If intUnitGrpUnkID > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobunitgrpunkid = @jobunitgrpunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobunitgrpunkid = @jobunitgrpunkid"
                End If
                objDataOperation.AddParameter("@jobunitgrpunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitGrpUnkID)
            End If
            If intClassUnkID > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobclassunkid = @jobclassunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobclassunkid = @jobclassunkid"
                End If
                objDataOperation.AddParameter("@jobclassunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassUnkID)
            End If
            If intGradeLevelUnkID > 0 Then
                If mblnWithLevel = True Then
                    strQ &= " UNION SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND jobgradelevelunkid = @jobgradelevelunkid"
                Else
                    strQ &= " UNION SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobgradelevelunkid = @jobgradelevelunkid"
                End If
                objDataOperation.AddParameter("@jobgradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeLevelUnkID)
            End If
            'Shani(18-JUN-2016) -- End

            'Gajanan [29-Oct-2020] -- Start   
            'Enhancement:Worked On Succession Module
            If mblnIskeyrole = True Then
                If mblnWithLevel = True Then
                    strQ &= " SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 AND iskeyrole = 1 "
                Else
                    strQ &= " SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND iskeyrole = 1 "
                End If
            End If
            'Gajanan [29-Oct-2020] -- End



            strQ &= ") As Job Where 1 = 1 "




            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= " AND Job.jobunkid IN (0," & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  Job.jobunkid IN (0, " & UserAccessLevel._AccessLevel & ") "
            '        End If
            'End Select
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'Dim arrID As String() = ConfigParameter._Object._UserAccessModeSetting.Split(",")

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Dim arrID As String() = strUserAccessModeSetting.Split(",")
            ''Sohail (18 May 2013) -- End
            'For i = 0 To arrID.Length - 1
            '    If CInt(arrID(i)) = enAllocation.JOBS Then
            '        Dim objMstr As New clsMasterData
            '        Dim iStrData As String = objMstr.GetUserAccessLevel(, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, enAllocation.JOBS)
            '        If iStrData.Trim.Length > 0 Then
            '            strQ &= " AND  Job.jobunkid IN (0, " & iStrData & ") "
            '        End If
            '        objMstr = Nothing
            '    End If
            '    Exit For
            'Next

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END


            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Level"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function
    'Sandeep [ 15 DEC 2010 ] -- End 


    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetJobUnkId(ByVal mstrName As String, Optional ByVal intLevelId As Integer = -1) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " jobunkid " & _
                      "  FROM hrjob_master " & _
                      " WHERE job_name = @name "


            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            If intLevelId > -1 Then
                strQ &= " AND job_level = @LevelId "
                objDataOperation.AddParameter("@LevelId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelId)
            End If

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("jobunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSectionUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End


    'Sohail (28 May 2014) -- Start
    'Enhancement - Staff Requisition.
    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
    'Public Function getHeadCount(Optional ByVal strListName As String = "List", _
    '                            Optional ByVal intJobGroupId As Integer = 0, _
    '                            Optional ByVal intJobId As Integer = 0, _
    '                            Optional ByVal strFilter As String = "", _
    '                            Optional ByVal mstrUserAccessFilter As String = "" _
    '                            ) As DataSet
    Public Function getHeadCount(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , Optional ByVal strListName As String = "List" _
                                    , Optional ByVal intJobGroupId As Integer = 0 _
                                    , Optional ByVal intJobId As Integer = 0 _
                                    , Optional ByVal strFilter As String = "" _
                                    , Optional ByVal mstrUserAccessFilter As String = "" _
                                ) As DataSet
        'Sohail (12 Oct 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If mstrUserAccessFilter.Trim = "" Then mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'strQ = "SELECT  hrjobgroup_master.name AS GROUP_NAME " & _
            '              ", hrjob_master.job_name AS JOB_NAME " & _
            '              ", total_position AS PLANNED " & _
            '              ", COUNT(employeeunkid) AS AVAILABLE " & _
            '              ", COUNT(employeeunkid) - ISNULL(total_position, 0) AS VARIATION " & _
            '              ", hrjobgroup_master.jobgroupunkid " & _
            '              ", hrjob_master.jobunkid " & _
            '        "FROM    hrjobgroup_master " & _
            '                "JOIN hrjob_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _
            '                "JOIN hremployee_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
            '        "WHERE   ISNULL(hrjobgroup_master.isactive, 1) = 1 " & _
            '                "AND ISNULL(hrjob_master.isactive, 1) = 1 "
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT  A.jobunkid  " & _
                          ", JOB_CODE " & _
                          ", JOB_NAME " & _
                          ", GROUP_NAME " & _
                          ", jobgroupunkid " & _
                          ", desciription " & _
                          ", jobgradeunkid " & _
                          ", PLANNED " & _
                          ", AVAILABLE " & _
                          ", ISNULL(PLANNED, 0) - ISNULL(AVAILABLE, 0) AS VARIATION " & _
                    "FROM    ( SELECT    hrjob_master.job_name AS JOB_NAME " & _
                                      ", hrjob_master.job_code AS JOB_CODE " & _
                          ", total_position AS PLANNED " & _
                                      ", ISNULL(AA.AVAILABLE, 0) AS AVAILABLE " & _
                                      ", hrjob_master.jobunkid " & _
                                      ", hrjob_master.desciription " & _
                                      ", hrjob_master.jobgradeunkid " & _
                                      ", ISNULL(hrjobgroup_master.name, '') AS GROUP_NAME " & _
                          ", ISNULL(hrjobgroup_master.jobgroupunkid, 0) AS jobgroupunkid " & _
                              "FROM      hrjob_master " & _
                              " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _
                               "  AND hrjobgroup_master.isactive = 1  " & _
                              "LEFT JOIN ( SELECT  SUM(B.AVAILABLE) AS AVAILABLE  " & _
                                                ", B.jobunkid " & _
                                          "FROM    ( SELECT    COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                          ", hrjob_master.jobunkid " & _
                                                    "FROM      hrjob_master " & _
                                                    " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _
                                                    "  AND hrjobgroup_master.isactive = 1  " & _
                                                    "LEFT JOIN ( SELECT  jobunkid  " & _
                                                                      ", employeeunkid " & _
                                                                      ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
                                                                "FROM    hremployee_categorization_tran " & _
                                                                "WHERE   isvoid = 0 " & _
                                                                        "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                                    ") AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid " & _
                                                            "AND Jobs.rno = 1 " & _
                                                    "LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "

            'Hemant (01 Aug 2019) -- [hrjobgroup_master.jobgroupunkid = ISNULL(hrjobgroup_master.jobgroupunkid, 0) AS jobgradeunkid, hrjobgroup_master.name AS GROUP_NAME = ISNULL(hrjobgroup_master.name, '') AS GROUP_NAME, JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid = LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid, AND hrjobgroup_master.isactive = 1, "JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _ = " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _]
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE hrjob_master.isactive = 1 " & _
                    " /* AND hrjobgroup_master.isactive = 1 */ "
            'Hemant (01 Aug 2019) -- ["AND hrjobgroup_master.isactive = 1 " = " /* AND hrjobgroup_master.isactive = 1 */ " ]
            'Sohail (12 Oct 2018) - [AND hrjobgroup_master.isactive = 1]

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If
            'Sohail (12 Oct 2018) -- End

            If intJobGroupId > 0 Then
                strQ &= " AND hrjobgroup_master.jobgroupunkid = @jobgroupunkid "
            End If

            If intJobId > 0 Then
                strQ &= " AND hrjob_master.jobunkid = @jobunkid "
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            'Sohail (12 Oct 2018) -- End

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'strQ &= "GROUP BY hrjobgroup_master.name " & _
            '              ", hrjobgroup_master.jobgroupunkid " & _
            '              ", hrjob_master.job_name " & _
            '              ", hrjob_master.jobunkid " & _
            '              ", total_position "
            strQ &= "       GROUP BY  hrjob_master.job_name " & _
                                    ", ISNULL(hrjobgroup_master.name, '') " & _
                          ", ISNULL(hrjobgroup_master.jobgroupunkid, 0) " & _
                                     ", total_position " & _
                                     ", hrjob_master.jobunkid "
            'Hemant (01 Aug 2019) -- [", hrjobgroup_master.name " & _ --> ", ISNULL(hrjobgroup_master.name, '') " & _ , ", hrjobgroup_master.jobgroupunkid " & _ -->  ", ISNULL(hrjobgroup_master.jobgroupunkid, 0) " & _ ]

            strQ &= "   ) B " & _
                         "GROUP BY B.jobunkid " & _
                               ") AS AA ON AA.jobunkid = hrjob_master.jobunkid " & _
                               "WHERE hrjob_master.isactive = 1 " & _
                               " /* AND hrjobgroup_master.isactive = 1 */ " & _
                               ") AS A " & _
                       "WHERE   1 = 1 "
            'Hemant (01 Aug 2019) -- [" AND hrjobgroup_master.isactive = 1 " & _   =   " /* AND hrjobgroup_master.isactive = 1 */ " & _]
            If intJobGroupId > 0 Then
                strQ &= " AND A.jobgroupunkid = @jobgroupunkid "
                objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobGroupId)
            End If

            If intJobId > 0 Then
                strQ &= " AND A.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (12 Oct 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getHeadCount; Module Name: " & mstrModuleName)
        End Try
    End Function
    'Sohail (28 May 2014) -- End


    'Pinkal (01-Apr-2015) -- Start
    'Enhancement - IMPLEMENT EMPLOYEE RE-CATEGORIZATION FOR TANAPA

    Public Function IsJobsByGrades() As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = " SELECT 1 FROM hrjob_master WHERE jobgradeunkid > 0 AND isactive = 1 "
            Return objDataOperation.RecordCount(strQ) > 0

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsJobsByGrades; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetJobsByGrades(Optional ByVal iJobUnkid As Integer = 0, Optional ByVal iGradeUnkid As Integer = 0) As DataSet
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = " SELECT  " & _
                      " hrjob_master.jobunkid " & _
                      ", hrjob_master.jobgroupunkid " & _
                      ", hrjob_master.jobheadunkid " & _
                      ", hrjob_master.jobunitunkid" & _
                      ", hrjob_master.jobsectionunkid " & _
                      ", hrjob_master.jobgradeunkid" & _
                      ", hrjob_master.job_code" & _
                      ", hrjob_master.job_name" & _
                      ", hrjob_master.report_tounkid " & _
                      ", hrjob_master.create_date " & _
                      ", hrjob_master.total_position" & _
                      ", hrjob_master.terminate_date" & _
                      ", hrjob_master.desciription" & _
                      ", hrjob_master.userunkid" & _
                      ", hrjob_master.isactive" & _
                      ", hrjob_master.job_name1" & _
                      ", hrjob_master.job_name2" & _
                      ", hrjob_master.job_level" & _
                      ", hrjob_master.teamunkid" & _
                      ", hrgrade_master.name AS Grade" & _
                      "  FROM  hrjob_master " & _
                      " JOIN hrgrade_master ON hrgrade_master.gradeunkid = hrjob_master.jobgradeunkid " & _
                      "  WHERE hrjob_master.jobgradeunkid > 0 AND hrjob_master.isactive = 1 "

            If iJobUnkid > 0 Then
                strQ &= " AND hrjob_master.jobunkid =" & iJobUnkid
            End If

            If iGradeUnkid > 0 Then
                strQ &= " AND hrjob_master.jobgradeunkid =" & iGradeUnkid
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsJobsByGrades; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (01-Apr-2015) -- End


    'Gajanan [31-AUG-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Public Sub SendNotification(ByVal strUserlist As String, _
                                ByVal intCompanyId As Integer, _
                                ByVal strFormName As String, _
                                ByVal eMode As enLogin_Mode, _
                                ByVal intUserId As Integer, _
                                ByVal strSenderAddress As String, _
                                ByVal Message As String)

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Dim dtAppr As DataTable = Nothing
        Dim objUserAddEdit As New clsUserAddEdit

        Try
            If strUserlist.Length > 0 Then
                Dim dsUserlist As DataSet = objUserAddEdit.GetList("List", "hrmsConfiguration..cfuser_master.userunkid in (" & strUserlist & ")")
                Dim objSendMail As New clsSendMail


                If IsNothing(dsUserlist.Tables(0)) = False AndAlso dsUserlist.Tables(0).Rows.Count > 0 Then


                    For Each iRow As DataRow In dsUserlist.Tables(0).Rows
                        Dim StrMessage As New System.Text.StringBuilder

                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                        Dim mstrUsername As String = iRow("firstname").ToString() + " " + iRow("lastname").ToString()
                        If mstrUsername.Trim().Length <= 0 Then
                            mstrUsername = iRow("username").ToString()
                        End If
                        If iRow("email").ToString().Length <= 0 Then Continue For

                        StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Dear") & " " & "<b>" & getTitleCase(mstrUsername) & "</b></span></p>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 8, "This is to notify you that a new job has been created with the below details.") & " </span></p>")
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                        StrMessage.Append("<TABLE border = '1' >")
                        StrMessage.Append("<TR bgcolor= 'SteelBlue'>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 9, "Date") & "</span></b></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 10, "Job") & "</span></b></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 11, "Direct Reporting To") & "</span></b></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 12, "Grade") & "</span></b></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 13, "Job Group") & "</span></b></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 14, "Job Level") & "</span></b></TD>")
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 15, "Vacancies") & "</span></b></TD>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("</TR>")
                        StrMessage.Append(Message)
                        StrMessage.Append("</TR>" & vbCrLf)
                        StrMessage.Append("</TABLE>")

                        objSendMail._ToEmail = iRow("email").ToString().Trim()
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 6, "Notification For Newly Added Job")
                        objSendMail._Message = StrMessage.ToString()
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = eMode
                        objSendMail._UserUnkid = intUserId
                        objSendMail._SenderAddress = strSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        objSendMail.SendMail(intCompanyId)
                    Next
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendNotification", mstrModuleName)
        Finally
        End Try
    End Sub


    'Gajanan [29-Oct-2020] -- Start   
    'Enhancement:Worked On Succession Module
    Public Function IsJobAssignAsKeyRoleToEmployee(ByVal intJobunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal xDateAsOn As Date, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            If xDateAsOn = Nothing Then xDateAsOn = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

            strQ = "SELECT * " & _
                    "FROM (SELECT " & _
                              "hremployee_categorization_tran.jobgroupunkid " & _
                            ",hremployee_categorization_tran.jobunkid " & _
                            ",employeeunkid " & _
                            ",isnull(hrjob_master.iskeyrole,0) as keyrole " & _
                            ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                         "FROM hremployee_categorization_tran " & _
                         "left JOIN hrjob_master on hremployee_categorization_tran.jobunkid  = hrjob_master.jobunkid " & _
                         "WHERE hremployee_categorization_tran.isvoid = 0 and hrjob_master.isactive = 1 " & _
                         "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xDateAsOn).ToString & "') AS a " & _
                    "WHERE a.Rno = 1 " & _
                    "and a.employeeunkid != @employeeunkid " & _
                    "and a.jobunkid = @jobunkid " & _
                    "and a.keyrole = 1 "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsJobAssignAsKeyRole; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function IsJobAsKeyRoleToIntoApprovalFlow(ByVal intJobunkid As Integer, _
                                                     ByVal intEmployeeunkid As Integer, _
                                                     Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT * " & _
                   "FROM (SELECT " & _
                            "hremployee_categorization_approval_tran.jobgroupunkid " & _
                            ",hremployee_categorization_approval_tran.jobunkid " & _
                            ",employeeunkid " & _
                            ",isnull(hrjob_master.iskeyrole,0) as keyrole " & _
                         "FROM hremployee_categorization_approval_tran " & _
                         "LEFT JOIN hrjob_master on hremployee_categorization_approval_tran.jobunkid  = hrjob_master.jobunkid " & _
                         "WHERE hremployee_categorization_approval_tran.isvoid = 0 and hremployee_categorization_approval_tran.isprocessed = 0 and hrjob_master.isactive = 1) AS a " & _
                    "WHERE a.employeeunkid != @employeeunkid and a.jobunkid = @jobunkid " & _
                    "and a.keyrole = 1 "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsJobAsKeyRoleToIntoApprovalFlow; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [29-Oct-2020] -- End


    'Gajanan [31-AUG-2019] -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Job Code is already defined. Please define new Job Code.")
            Language.setMessage(mstrModuleName, 2, "This Job is already defined. Please define new Job.")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this job. Reason : This job is already linked with some transaction.")
            Language.setMessage(mstrModuleName, 5, "Level")
			Language.setMessage(mstrModuleName, 6, "Notification For Newly Added Job")
			Language.setMessage(mstrModuleName, 7, "Dear")
			Language.setMessage(mstrModuleName, 8, "This is to notify you that a new job has been created with the below details.")
			Language.setMessage(mstrModuleName, 9, "Date")
			Language.setMessage(mstrModuleName, 10, "Job")
			Language.setMessage(mstrModuleName, 11, "Direct Reporting To")
			Language.setMessage(mstrModuleName, 12, "Grade")
			Language.setMessage(mstrModuleName, 13, "Job Group")
			Language.setMessage(mstrModuleName, 14, "Job Level")
			Language.setMessage(mstrModuleName, 15, "Vacancies")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Sandeep [ 15 DEC 2010 ] -- Start
'Issue : Mr. Rutta's Comment
'''' <summary>
'''' Modify By: Sandeep J. Sharma
'''' </summary>
'''' <purpose> Assign all Property variable </purpose>
'Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intJobgroupid As Integer = -1) As DataSet
'    Dim dsList As DataSet = Nothing
'    Dim strQ As String = ""
'    Dim exForce As Exception

'    objDataOperation = New clsDataOperation

'    Try
'        If mblnFlag = True Then
'            strQ = "SELECT 0 As jobunkid , @ItemName As  name , -1 As job_level UNION "
'        End If
'        strQ &= "SELECT jobunkid, job_name +' '+@Level+' '+CAST(ISNULL(job_level,'') AS NVARCHAR(20)), job_level As  name FROM hrjob_master WHERE isactive =1 "

'        If UserAccessLevel._AccessLevel.Length > 0 Then
'            strQ &= " AND jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'        End If

'        If intJobgroupid > 0 Then
'            strQ &= " AND jobgroupunkid = @jobgroupunkid"
'            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobgroupid)
'        End If

'        objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
'        objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Level"))

'        dsList = objDataOperation.ExecQuery(strQ, strListName)

'        If objDataOperation.ErrorMessage <> "" Then
'            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'            Throw exForce
'        End If

'        Return dsList

'    Catch ex As Exception
'        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'    End Try
'End Function
