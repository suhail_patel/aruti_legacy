﻿'************************************************************************************************************************************
'Class Name : clsJob_Language_Tran.vb
'Purpose    :
'Date       :18/02/2020
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsJob_Language_Tran

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsJob_Language_Tran"
    Dim objDataOperation As clsDataOperation
    Private xDataOp As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintJobUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintJobLanguageTranId As Integer = 0
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    Public Property _JobUnkid() As Integer
        Get
            Return mintJobUnkid
        End Get
        Set(ByVal value As Integer)
            mintJobUnkid = value
            Call Get_Job_Language()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("JobLanguage")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("joblanguagetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("jobunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("masterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("languagename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("isactive", GetType(System.Boolean)).DefaultValue = True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "
    Private Sub Get_Job_Language()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowJS_Tran As DataRow
        Dim exForce As Exception
        Try
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                     "  hrjob_language_tran.joblanguagetranunkid " & _
                     ", hrjob_language_tran.jobunkid " & _
                     ", hrjob_language_tran.masterunkid " & _
                     ", ISNULL(cfcommon_master.name, '') AS languagename " & _
                     ", hrjob_language_tran.isactive " & _
                     ", '' As AUD " & _
                   "FROM hrjob_language_tran " & _
                   "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjob_language_tran.masterunkid " & _
                        "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.LANGUAGES & " " & _
                   "WHERE hrjob_language_tran.isactive = 1 " & _
                        "AND cfcommon_master.isactive = 1 " & _
                        "AND hrjob_language_tran.jobunkid = @jobunkid "

            objDataOperation.AddParameter("@jobunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowJS_Tran = mdtTran.NewRow()

                    dRowJS_Tran.Item("joblanguagetranunkid") = .Item("joblanguagetranunkid")
                    dRowJS_Tran.Item("jobunkid") = .Item("jobunkid")
                    dRowJS_Tran.Item("masterunkid") = .Item("masterunkid")
                    dRowJS_Tran.Item("languagename") = .Item("languagename")
                    dRowJS_Tran.Item("AUD") = .Item("AUD")
                    dRowJS_Tran.Item("isactive") = .Item("isactive")

                    mdtTran.Rows.Add(dRowJS_Tran)
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Job_Language; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_JobLanguages() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrjob_language_tran ( " & _
                                            "  jobunkid " & _
                                            ", masterunkid " & _
                                            ", isactive " & _
                                       ") VALUES (" & _
                                            "  @jobunkid " & _
                                            ", @masterunkid " & _
                                            ", @isactive " & _
                                       "); SELECT @@identity"

                                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkid.ToString)
                                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("masterunkid").ToString)
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintJobLanguageTranId = dsList.Tables(0).Rows(0)(0)
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END
                                If .Item("jobunkid") > 0 Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_language_tran", "joblanguagetranunkid", mintJobLanguageTranId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobUnkid, "hrjob_language_tran", "joblanguagetranunkid", mintJobLanguageTranId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                objCommonATLog = Nothing
                            Case "U"
                                strQ = "UPDATE hrjob_language_tran SET " & _
                                         "  jobunkid = @jobunkid" & _
                                         ", masterunkid = @masterunkid " & _
                                         ", Syncdatetime = NULL " & _
                                         ", isactive = @isactive " & _
                                       "WHERE joblanguagetranunkid = @joblanguagetranunkid "

                                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobunkid").ToString)
                                objDataOperation.AddParameter("@joblanguagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("joblanguagetranunkid").ToString)
                                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("masterunkid").ToString)
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_language_tran", "joblanguagetranunkid", .Item("joblanguagetranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                objCommonATLog = Nothing
                            Case "D"

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END
                                If .Item("joblanguagetranunkid") > 0 Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_language_tran", "joblanguagetranunkid", .Item("joblanguagetranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                objCommonATLog = Nothing
                                strQ = "UPDATE hrjob_language_tran SET " & _
                                        "  isactive = @isactive " & _
                                        ",Syncdatetime = NULL " & _
                                        "WHERE joblanguagetranunkid = @joblanguagetranunkid "

                                objDataOperation.AddParameter("@joblanguagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("joblanguagetranunkid").ToString)
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_JobLanguages; Module Name: " & mstrModuleName)
            Return False
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete_JobLanguages(ByVal intJobID As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()

            strQ = " SELECT ISNULL(joblanguagetranunkid,0) AS  joblanguagetranunkid FROM hrjob_language_tran WHERE  isactive = 1 AND jobunkid = @jobunkid "

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")


            strQ = "UPDATE hrjob_language_tran SET " & _
                      " isactive = 0 " & _
                      " , Syncdatetime = NULL " & _
                      " WHERE jobunkid = @jobunkid AND isactive = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsList.Tables(0).Rows
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END
                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", intJobID, "hrjob_language_tran", "joblanguagetranunkid", CInt(dr("joblanguagetranunkid")), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objCommonATLog = Nothing
                Next
            End If
            dsList = Nothing
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete_JobLanguages; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

#End Region

End Class
