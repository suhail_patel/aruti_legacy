﻿'************************************************************************************************************************************
'Class Name :clsAllocationMapping.vb
'Purpose    :
'Date       :22/09/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsAllocationMapping
    Private Shared ReadOnly mstrModuleName As String = "clsAllocationMapping"
    Dim objDataOperation As clsDataOperation
    Private mintMappingUnkid As Integer = 0
    Private mdtTran As DataTable

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public Property _MappingUnkid() As Integer
        Get
            Return mintMappingUnkid
        End Get
        Set(ByVal value As Integer)
            mintMappingUnkid = value
        End Set
    End Property

    Public Property _MappedData() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("MData")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "mappingunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "p_allocationid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "c_allocationid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "p_referenceunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "userunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DefaultValue = DBNull.Value
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "c_referenceunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "mappingtranunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Allocation"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "DisplayAllocation"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Function(s) & Procedure(s) "

    Public Function InsertUpdateDelete_MappedAllocation() As Boolean
        Dim StrQ1 As String = ""
        Dim StrQ2 As String = ""
        Dim dsList As New DataSet
        Dim mintMappingTranId As Integer = -1
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ1 = "INSERT INTO hralloc_mapping_master ( " & _
                        "  p_allocationid " & _
                        ", c_allocationid " & _
                        ", p_referenceunkid " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason" & _
                    ") VALUES (" & _
                        "  @p_allocationid " & _
                        ", @c_allocationid " & _
                        ", @p_referenceunkid " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                    "); SELECT @@identity"

            StrQ2 = "INSERT INTO hralloc_mapping_tran ( " & _
                        "  mappingunkid " & _
                        ", c_referenceunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason" & _
                    ") VALUES (" & _
                        "  @mappingunkid " & _
                        ", @c_referenceunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                    "); SELECT @@identity"

            Dim drTemp() As DataRow
            drTemp = mdtTran.Select("c_referenceunkid = -1 AND AUD <> 'D'")
            If drTemp.Length > 0 Then
                objDataOperation.BindTransaction()
                For i As Integer = 0 To drTemp.Length - 1

                    If isExists(objDataOperation, _
                                drTemp(i)("p_allocationid"), _
                                drTemp(i)("c_allocationid"), _
                                drTemp(i)("p_referenceunkid"), _
                                drTemp(i)("c_referenceunkid"), _
                                mintMappingUnkid, _
                                mintMappingTranId) = True Then GoTo 111

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@p_allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, drTemp(i)("p_allocationid").ToString)
                    objDataOperation.AddParameter("@c_allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, drTemp(i)("c_allocationid").ToString)
                    objDataOperation.AddParameter("@p_referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drTemp(i)("p_referenceunkid").ToString)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drTemp(i)("userunkid").ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, drTemp(i)("isvoid").ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drTemp(i)("voiduserunkid").ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, drTemp(i)("voiddatetime"))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, drTemp(i)("voidreason").ToString)

                    dsList = objDataOperation.ExecQuery(StrQ1, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintMappingUnkid = dsList.Tables(0).Rows(0)(0)

111:                Dim dTemp() As DataRow = mdtTran.Select("p_referenceunkid = '" & CInt(drTemp(i)("p_referenceunkid")) & _
                                                            "' AND c_referenceunkid > 0 AND AUD <> 'D' AND p_allocationid ='" & drTemp(i)("p_allocationid") & "'")
                    If dTemp.Length > 0 Then
                        For j As Integer = 0 To dTemp.Length - 1

                            If isExists(objDataOperation, _
                                drTemp(i)("p_allocationid"), _
                                drTemp(i)("c_allocationid"), _
                                drTemp(i)("p_referenceunkid"), _
                                dTemp(j)("c_referenceunkid"), _
                                mintMappingUnkid, _
                                mintMappingTranId) = True Then Continue For

                            objDataOperation.ClearParameters()

                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingUnkid.ToString)
                            objDataOperation.AddParameter("@c_referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dTemp(j)("c_referenceunkid").ToString)
                            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dTemp(j)("isvoid").ToString)
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dTemp(j)("voiduserunkid").ToString)
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dTemp(j)("voiddatetime"))
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dTemp(j)("voidreason").ToString)

                            dsList = objDataOperation.ExecQuery(StrQ2, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintMappingTranId = dsList.Tables(0).Rows(0)(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hralloc_mapping_master", "mappingunkid", mintMappingUnkid, "hralloc_mapping_tran", "mappingtranunkid", mintMappingTranId, 1, 1) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                        Next
                    End If
                Next
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Call DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_MappedAllocation", mstrModuleName)
            Return False
        End Try
    End Function

    Public Function Get_Mapped_List(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                     " hralloc_mapping_master.mappingunkid " & _
                     ",hralloc_mapping_master.p_allocationid " & _
                     ",hralloc_mapping_master.c_allocationid " & _
                     ",hralloc_mapping_master.p_referenceunkid " & _
                     ",hralloc_mapping_master.userunkid " & _
                     ",hralloc_mapping_master.isvoid " & _
                     ",hralloc_mapping_master.voiduserunkid " & _
                     ",hralloc_mapping_master.voiddatetime " & _
                     ",hralloc_mapping_master.voidreason " & _
                     ",hralloc_mapping_tran.c_referenceunkid " & _
                     ",hralloc_mapping_tran.mappingtranunkid " & _
                     ",CASE WHEN p_allocationid = 1 THEN ISNULL(sp.name,'') " & _
                           "WHEN p_allocationid = 2 THEN ISNULL(DGP.name,'') " & _
                           "WHEN p_allocationid = 3 THEN ISNULL(DP.name,'') " & _
                           "WHEN p_allocationid = 4 THEN ISNULL(SGP.name,'') " & _
                           "WHEN p_allocationid = 5 THEN ISNULL(SCP.name,'') " & _
                           "WHEN p_allocationid = 6 THEN ISNULL(UGP.name,'') " & _
                           "WHEN p_allocationid = 7 THEN ISNULL(UP.name,'') " & _
                           "WHEN p_allocationid = 8 THEN ISNULL(TP.name,'') " & _
                     "END + '  [ ' + " & _
                         "CASE WHEN p_allocationid = 1 THEN @BRANCH " & _
                              "WHEN p_allocationid = 2 THEN @DEPARTMENT_GROUP " & _
                              "WHEN p_allocationid = 3 THEN @DEPARTMENT " & _
                              "WHEN p_allocationid = 4 THEN @SECTION_GROUP " & _
                              "WHEN p_allocationid = 5 THEN @SECTION " & _
                              "WHEN p_allocationid = 6 THEN @UNIT_GROUP " & _
                              "WHEN p_allocationid = 7 THEN @UNIT " & _
                              "WHEN p_allocationid = 8 THEN @TEAM " & _
                         "END + ' -> ' + " & _
                         "CASE WHEN c_allocationid = 1 THEN @BRANCH " & _
                              "WHEN c_allocationid = 2 THEN @DEPARTMENT_GROUP " & _
                              "WHEN c_allocationid = 3 THEN @DEPARTMENT " & _
                              "WHEN c_allocationid = 4 THEN @SECTION_GROUP " & _
                              "WHEN c_allocationid = 5 THEN @SECTION " & _
                              "WHEN c_allocationid = 6 THEN @UNIT_GROUP " & _
                              "WHEN c_allocationid = 7 THEN @UNIT " & _
                              "WHEN c_allocationid = 8 THEN @TEAM " & _
                         "END + ' ]' AS Allocation " & _
                    ",CASE WHEN c_allocationid = 1 THEN ISNULL(CSP.name,'') " & _
                          "WHEN c_allocationid = 2 THEN ISNULL(CDGP.name,'') " & _
                          "WHEN c_allocationid = 3 THEN ISNULL(CDP.name,'') " & _
                          "WHEN c_allocationid = 4 THEN ISNULL(CSGP.name,'') " & _
                          "WHEN c_allocationid = 5 THEN ISNULL(CSCP.name,'') " & _
                          "WHEN c_allocationid = 6 THEN ISNULL(CUGP.name,'') " & _
                          "WHEN c_allocationid = 7 THEN ISNULL(CUP.name,'') " & _
                          "WHEN c_allocationid = 8 THEN ISNULL(CTP.name,'') " & _
                    " END AS DisplayAllocation " & _
                    "FROM hralloc_mapping_tran " & _
                        "JOIN hralloc_mapping_master ON hralloc_mapping_tran.mappingunkid = hralloc_mapping_master.mappingunkid " & _
                        "LEFT JOIN hrteam_master AS TP ON hralloc_mapping_master.p_referenceunkid = TP.teamunkid AND p_allocationid = " & enAllocation.TEAM & " " & _
                        "LEFT JOIN hrunit_master AS UP ON hralloc_mapping_master.p_referenceunkid = UP.unitunkid AND p_allocationid = " & enAllocation.UNIT & " " & _
                        "LEFT JOIN hrunitgroup_master AS UGP ON hralloc_mapping_master.p_referenceunkid = UGP.unitgroupunkid AND p_allocationid = " & enAllocation.UNIT_GROUP & " " & _
                        "LEFT JOIN hrsection_master AS SCP ON hralloc_mapping_master.p_referenceunkid = SCP.sectionunkid AND p_allocationid = " & enAllocation.SECTION & " " & _
                        "LEFT JOIN hrsectiongroup_master AS SGP ON hralloc_mapping_master.p_referenceunkid = SGP.sectiongroupunkid AND p_allocationid = " & enAllocation.SECTION_GROUP & " " & _
                        "LEFT JOIN hrdepartment_master AS DP ON hralloc_mapping_master.p_referenceunkid = DP.departmentunkid AND p_allocationid = " & enAllocation.DEPARTMENT & " " & _
                        "LEFT JOIN hrdepartment_group_master AS DGP ON hralloc_mapping_master.p_referenceunkid = DGP.deptgroupunkid AND p_allocationid = " & enAllocation.DEPARTMENT_GROUP & " " & _
                        "LEFT JOIN hrstation_master AS SP ON hralloc_mapping_master.p_referenceunkid = SP.stationunkid AND p_allocationid =" & enAllocation.BRANCH & " " & _
                        "LEFT JOIN hrteam_master AS CTP ON hralloc_mapping_tran.c_referenceunkid = CTP.teamunkid AND c_allocationid = " & enAllocation.TEAM & " " & _
                        "LEFT JOIN hrunit_master AS CUP ON hralloc_mapping_tran.c_referenceunkid = CUP.unitunkid AND c_allocationid = " & enAllocation.UNIT & " " & _
                        "LEFT JOIN hrunitgroup_master AS CUGP ON hralloc_mapping_tran.c_referenceunkid = CUGP.unitgroupunkid AND c_allocationid = " & enAllocation.UNIT_GROUP & " " & _
                        "LEFT JOIN hrsection_master AS CSCP ON hralloc_mapping_tran.c_referenceunkid = CSCP.sectionunkid AND c_allocationid = " & enAllocation.SECTION & " " & _
                        "LEFT JOIN hrsectiongroup_master AS CSGP ON hralloc_mapping_tran.c_referenceunkid = CSGP.sectiongroupunkid AND c_allocationid = " & enAllocation.SECTION_GROUP & " " & _
                        "LEFT JOIN hrdepartment_master AS CDP ON hralloc_mapping_tran.c_referenceunkid = CDP.departmentunkid AND c_allocationid = " & enAllocation.DEPARTMENT & " " & _
                        "LEFT JOIN hrdepartment_group_master AS CDGP ON hralloc_mapping_tran.c_referenceunkid = CDGP.deptgroupunkid AND c_allocationid = " & enAllocation.DEPARTMENT_GROUP & " " & _
                        "LEFT JOIN hrstation_master AS CSP ON hralloc_mapping_tran.c_referenceunkid = CSP.stationunkid AND c_allocationid = " & enAllocation.BRANCH & " " & _
                    "WHERE ISNULL(hralloc_mapping_master.isvoid,0) = 0 AND ISNULL(hralloc_mapping_tran.isvoid,0) = 0 "

            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_Mapped_List", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Void_Mapped_Allocation(ByVal intTranUnkid As Integer, ByVal intMastUnkid As Integer, _
                         ByVal dtVoid_Date As DateTime, ByVal strVoid_Reason As String, ByVal intVoidUser As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exFore As Exception
        Dim iCnt As Integer = -1
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            StrQ = "UPDATE hralloc_mapping_tran SET " & _
                    "  isvoid = 1 " & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                   "WHERE mappingtranunkid = @mappingtranunkid "

            objDataOperation.AddParameter("@mappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranUnkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoid_Date)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoid_Reason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUser.ToString)


            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exFore = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exFore
            End If

            StrQ = "SELECT * FROM hralloc_mapping_tran WHERE mappingunkid = @mappingunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMastUnkid.ToString)

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exFore = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exFore
            End If

            If iCnt > 0 Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hralloc_mapping_master", "mappingunkid", intMastUnkid, "hralloc_mapping_tran", "mappingtranunkid", intTranUnkid, 2, 3) = False Then
                    exFore = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exFore
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            Else
                StrQ = "UPDATE hralloc_mapping_master SET " & _
                        "  isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason " & _
                       "WHERE mappingunkid = @mappingunkid "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exFore = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exFore
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hralloc_mapping_master", "mappingunkid", intMastUnkid, "hralloc_mapping_tran", "mappingtranunkid", intTranUnkid, 3, 3) = False Then
                    exFore = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exFore
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Call DisplayError.Show("-1", ex.Message, "Void_Mapped_Allocation", mstrModuleName)
        End Try
    End Function

    Private Function isExists(ByVal objDataOp As clsDataOperation, _
                              ByVal intP_AllocationId As Integer, _
                              ByVal intC_AllocationId As Integer, _
                              ByVal intP_ReferenceId As Integer, _
                              ByVal intC_ReferenceId As Integer, _
                              ByRef intMstMappId As Integer, _
                              ByRef intTrnMappId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Dim dsList As New DataSet
        Try
            If intC_ReferenceId <= 0 Then
                StrQ = "SELECT * FROM hralloc_mapping_master " & _
                       "WHERE p_allocationid = '" & intP_AllocationId & "' AND c_allocationid = '" & intC_AllocationId & "' AND p_referenceunkid = '" & intP_ReferenceId & "' AND isvoid = 0 "
            Else
                StrQ = "SELECT * FROM hralloc_mapping_tran " & _
                       "JOIN dbo.hralloc_mapping_master ON hralloc_mapping_tran.mappingunkid = hralloc_mapping_master.mappingunkid " & _
                       "WHERE p_allocationid = '" & intP_AllocationId & "' AND c_allocationid = '" & intC_AllocationId & "' AND p_referenceunkid = '" & intP_ReferenceId & "' AND hralloc_mapping_master.isvoid = 0 " & _
                       " AND c_referenceunkid = '" & intC_ReferenceId & "' AND hralloc_mapping_tran.isvoid = 0 "
            End If

            dsList = objDataOp.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                If intC_ReferenceId <= 0 Then
                    intMstMappId = dsList.Tables(0).Rows(0)("mappingunkid")
                Else
                    intTrnMappId = dsList.Tables(0).Rows(0)("mappingtranunkid")
                End If
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "isExists", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function isUsed(ByVal intMasterId As Integer, ByVal intTranId As Integer) As Boolean
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim iCnt As Integer = -1
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   " p_allocationid " & _
                   ",c_allocationid " & _
                   ",p_referenceunkid " & _
                   ",c_referenceunkid " & _
                   ",CASE WHEN p_allocationid = 1 AND c_allocationid = 2 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE stationunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND deptgroupunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                         "WHEN p_allocationid = 2 AND c_allocationid = 3 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE deptgroupunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND departmentunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                         "WHEN p_allocationid = 3 AND c_allocationid = 4 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE departmentunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND sectiongroupunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                         "WHEN p_allocationid = 4 AND c_allocationid = 5 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE sectiongroupunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND sectionunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                         "WHEN p_allocationid = 5 AND c_allocationid = 6 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE sectionunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND unitgroupunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                         "WHEN p_allocationid = 6 AND c_allocationid = 7 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE unitgroupunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND unitunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                         "WHEN p_allocationid = 7 AND c_allocationid = 8 THEN " & _
                                 "'SELECT employeeunkid FROM hremployee_master WHERE unitunkid = '+ CAST(p_referenceunkid AS NVARCHAR(10)) +' AND teamunkid = '+ CAST(c_referenceunkid AS NVARCHAR(10)) " & _
                    "END AS Qry " & _
                    "FROM hralloc_mapping_master " & _
                    "JOIN hralloc_mapping_tran ON hralloc_mapping_master.mappingunkid = hralloc_mapping_tran.mappingunkid " & _
                    "WHERE hralloc_mapping_master.mappingunkid = '" & intMasterId & "' AND mappingtranunkid = '" & intTranId & "' " & _
                    "AND ISNULL(hralloc_mapping_master.isvoid,0) = 0 AND ISNULL(hralloc_mapping_tran.isvoid,0) = 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                iCnt = objDataOperation.RecordCount(dRow.Item("Qry"))

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If iCnt > 0 Then
                    blnFlag = True
                    Exit For
                End If
            Next
            Return blnFlag
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "isUsed", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsMasterData", 423, "Team")
			Language.setMessage("clsMasterData", 424, "Unit")
			Language.setMessage("clsMasterData", 425, "Unit Group")
			Language.setMessage("clsMasterData", 426, "Section")
			Language.setMessage("clsMasterData", 427, "Section Group")
			Language.setMessage("clsMasterData", 428, "Department")
			Language.setMessage("clsMasterData", 429, "Department Group")
			Language.setMessage("clsMasterData", 430, "Branch")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
