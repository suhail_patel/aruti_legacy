﻿'************************************************************************************************************************************
'Class Name : clsGrade.vb
'Purpose    :
'Date       :27/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsGrade
    Private Shared ReadOnly mstrModuleName As String = "clsGrade"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintGradeunkid As Integer
    Private mintGradegroupunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mintPriority As Integer = 0 'Sohail (27 Apr 2016)
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property
    'Sohail (27 Apr 2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  gradeunkid " & _
              ", gradegroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
              ", ISNULL(priority, 0) AS priority " & _
             "FROM hrgrade_master " & _
             "WHERE gradeunkid = @gradeunkid "
            'Sohail (27 Apr 2016) - [priority]

            objDataOperation.AddParameter("@gradeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintGradeUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintgradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintgradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrname1 = dtRow.Item("name1").ToString
                mstrname2 = dtRow.Item("name2").ToString
                mintPriority = CInt(dtRow.Item("priority")) 'Sohail (27 Apr 2016)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (27 Apr 2016) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                             " hrgrade_master.gradeunkid " & _
                             ",hrgrade_master.gradegroupunkid " & _
                             ",hrgrade_master.code " & _
                             ",hrgrade_master.name " & _
                             ",hrgrade_master.description " & _
                             ",hrgrade_master.isactive " & _
                             ",hrgrade_master.name1 " & _
                             ",hrgrade_master.name2 " & _
                             ",hrgradegroup_master.name AS GradeGroup " & _
                             ", ISNULL(hrgrade_master.priority, 0) AS priority " & _
                        "FROM hrgrade_master " & _
                             "LEFT JOIN hrgradegroup_master ON hrgrade_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid "
            'Sohail (27 Apr 2016) - [priority]

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'If blnOnlyActive Then
            '    strQ &= " WHERE hrgrade_master.isactive = 1 "
            'End If
            strQ &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND hrgrade_master.isactive = 1 "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (27 Apr 2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrgrade_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grade Code is already defined. Please define new Grade Code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Grade Name is already defined. Please define new Grade Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradegroupunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority) 'Sohail (27 Apr 2016)

            strQ = "INSERT INTO hrgrade_master ( " & _
              "  gradegroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2" & _
              ", priority" & _
            ") VALUES (" & _
              "  @gradegroupunkid " & _
              ", @code " & _
              ", @name " & _
              ", @description " & _
              ", @isactive " & _
              ", @name1 " & _
              ", @name2" & _
              ", @priority" & _
            "); SELECT @@identity"
            'Sohail (27 Apr 2016) - [priority]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGradeunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrgrade_master", "gradeunkid", mintGradeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrgrade_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintGradeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grade Code is already defined. Please define new Grade Code.")
            Return False
        End If

        If isExist(, mstrName, mintGradeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Grade Name is already defined. Please define new Grade Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END

        Try
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradeunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradegroupunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority) 'Sohail (27 Apr 2016)

            strQ = "UPDATE hrgrade_master SET " & _
              "  gradegroupunkid = @gradegroupunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", name1 = @name1" & _
              ", name2 = @name2 " & _
              ", priority = @priority " & _
            "WHERE gradeunkid = @gradeunkid "
            'Sohail (27 Apr 2016) - [priority]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrgrade_master", mintGradeunkid, "gradeunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrgrade_master", "gradeunkid", mintGradeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Function UpdatePriority(ByVal dtTable As DataTable) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        objDataOperation.BindTransaction()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("gradeunkid")))
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("priority")))

                strQ = "UPDATE hrgrade_master SET " & _
                            " priority = @priority " & _
                        "WHERE gradeunkid = @gradeunkid "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrgrade_master", CInt(dtRow.Item("gradeunkid")), "gradeunkid", 2) Then
                    If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrgrade_master", "gradeunkid", CInt(dtRow.Item("gradeunkid"))) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePriority; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (27 Apr 2016) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrgrade_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Grade. Reason : This Grade is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            strQ = "UPDATE hrgrade_master SET " & _
                   " isactive = 0 " & _
            "WHERE gradeunkid = @gradeunkid "

            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrgrade_master", "gradeunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 18 Aug 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 18 Aug 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " TABLE_NAME AS TableName " & _
                   " FROM INFORMATION_SCHEMA.COLUMNS " & _
                   " WHERE COLUMN_NAME='gradeunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "hrgrade_master" Then Continue For
                strQ = "SELECT gradeunkid FROM " & dtRow.Item("TableName").ToString & " WHERE gradeunkid = @gradeunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef intRetUnkIdIfExist As Integer = 0) As Boolean
        'Sohail (10 Nov 2014) - [intRetUnkIdIfExist]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        intRetUnkIdIfExist = 0 'Sohail (10 Nov 2014)

        Try
            strQ = "SELECT " & _
              "  gradeunkid " & _
              ", gradegroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrgrade_master " & _
             "WHERE 1 = 1 "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strName.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If


            If intUnkid > 0 Then
                strQ &= " AND gradeunkid <> @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (10 Nov 2014) -- Start
            'Enhancement - Change Grade option on Import Salary Change.
            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkIdIfExist = CInt(dsList.Tables(0).Rows(0).Item("gradeunkid"))
            End If
            'Sohail (10 Nov 2014) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intGroupId As Integer = -1, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (27 Apr 2016) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As gradeunkid , @ItemName As  name  UNION "
            End If
            strQ &= "SELECT gradeunkid,name FROM hrgrade_master WHERE isactive =1 "

            If intGroupId > 0 Then
                strQ &= " AND hrgrade_master.gradegroupunkid = @GradeGroupId "
                'gradegroupunkid
            End If

            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (27 Apr 2016) -- End

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@GradeGroupId", SqlDbType.Int, eZeeDataType.INT_SIZE, intGroupId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetGradeUnkId(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " gradeunkid " & _
                      "  FROM hrgrade_master " & _
                      " WHERE name = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("gradeunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetGradeUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Grade Code is already defined. Please define new Grade Code.")
			Language.setMessage(mstrModuleName, 2, "This Grade Name is already defined. Please define new Grade Name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Grade. Reason : This Grade is already linked with some transaction.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
