﻿'************************************************************************************************************************************
'Class Name : clsSectionGroup.vb
'Purpose    :
'Date       :15/11/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsUnitGroup
    Private Shared ReadOnly mstrModuleName As String = "clsUnitGroup"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintUnitgroupunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mintSectionunkid As Integer

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Unitgroupunkid() As Integer
        Get
            Return mintUnitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitgroupunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  unitgroupunkid " & _
                      ", code " & _
                      ", name " & _
                      ", description " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", sectionunkid " & _
                   "FROM hrunitgroup_master " & _
                   "WHERE unitgroupunkid = @unitgroupunkid "

            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintUnitgroupunkid = CInt(dtRow.Item("unitgroupunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  hrunitgroup_master.unitgroupunkid " & _
                    ", hrunitgroup_master.code " & _
                    ", hrunitgroup_master.name " & _
                    ", hrunitgroup_master.description " & _
                    ", hrunitgroup_master.isactive " & _
                    ", hrunitgroup_master.name1 " & _
                    ", hrunitgroup_master.name2 " & _
                    ", hrunitgroup_master.sectionunkid " & _
                    ", ISNULL(hrsection_master.name,'') AS Section " & _
                   "FROM hrunitgroup_master " & _
                   " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrunitgroup_master.sectionunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrunitgroup_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrsectiongroup_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Unit Group Code is already defined. Please define new Unit Group Code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Unit Group Name is already defined. Please define new Unit Group Name.")
            Return False
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)

            strQ = "INSERT INTO hrunitgroup_master ( " & _
                      "  sectionunkid " & _
                      ", code " & _
                      ", name " & _
                      ", description " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2" & _
                   ") VALUES (" & _
                      "  @sectionunkid " & _
                      ", @code " & _
                      ", @name " & _
                      ", @description " & _
                      ", @isactive " & _
                      ", @name1 " & _
                      ", @name2" & _
                   "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintUnitgroupunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrunitgroup_master", "unitgroupunkid", mintUnitgroupunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrsectiongroup_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintUnitgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Unit Group Code is already defined. Please define new Unit Group Code.")
            Return False
        End If

        If isExist(, mstrName, mintUnitgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Unit Group Name is already defined. Please define new Unit Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)

            strQ = "UPDATE hrunitgroup_master SET " & _
                        "  sectionunkid = @sectionunkid" & _
                        ", code = @code" & _
                        ", name = @name" & _
                        ", description = @description" & _
                        ", isactive = @isactive" & _
                        ", name1 = @name1" & _
                        ", name2 = @name2 " & _
                    "WHERE unitgroupunkid = @unitgroupunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrunitgroup_master", mintUnitgroupunkid, "unitgroupunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrunitgroup_master", "unitgroupunkid", mintUnitgroupunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrsectiongroup_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Section Group. Reason : This Section Group is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            strQ = "UPDATE hrunitgroup_master SET " & _
                     " isactive = 0 " & _
            "WHERE unitgroupunkid = @unitgroupunkid "

            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrunitgroup_master", "unitgroupunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='unitgroupunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "hrunitgroup_master" Then Continue For
                strQ = "SELECT unitgroupunkid FROM " & dtRow.Item("TableName").ToString & " WHERE unitgroupunkid = @unitgroupunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  unitgroupunkid " & _
                    ", code " & _
                    ", name " & _
                    ", description " & _
                    ", isactive " & _
                    ", name1 " & _
                    ", name2 " & _
                   "FROM hrunitgroup_master " & _
                   "WHERE 1 = 1 AND isactive = 1 "


            If strName.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If


            If intUnkid > 0 Then
                strQ &= " AND unitgroupunkid <> @unitgroupunkid"
            End If

            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intSectionId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As unitgroupunkid , @ItemName As  name  UNION "
            End If

            'S.SANDEEP [ 21 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ &= "SELECT unitgroupunkid,name FROM hrunitgroup_master WHERE isactive =1 "

            'If intSectionId > 0 Then
            '    strQ &= " AND sectionunkid = @sectionunkid "
            '    objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionId)
            'End If
            If intSectionId <= 0 Then
                strQ &= "SELECT unitgroupunkid,name FROM hrunitgroup_master WHERE isactive =1 "
            Else
                strQ &= "SELECT unitgroupunkid,name FROM hrunitgroup_master " & _
                        " JOIN hralloc_mapping_tran ON hralloc_mapping_tran.c_referenceunkid =  hrunitgroup_master.unitgroupunkid " & _
                        " JOIN hralloc_mapping_master ON hralloc_mapping_master.mappingunkid =  hralloc_mapping_tran.mappingunkid " & _
                        " WHERE isactive = 1 " & _
                        " AND hralloc_mapping_tran.isvoid = 0 AND hralloc_mapping_master.isvoid = 0 AND hralloc_mapping_master.p_referenceunkid = '" & intSectionId & "' AND p_allocationid = " & enAllocation.SECTION & _
                        " AND c_allocationid = " & enAllocation.UNIT_GROUP
            End If
            'S.SANDEEP [ 21 SEP 2012 ] -- END



            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUnitGrpUnkId(ByVal mstrState As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " unitgroupunkid " & _
                      "  FROM hrunitgroup_master " & _
                      " WHERE name = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrState)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("unitgroupunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUnitGrpUnkId", mstrModuleName)
        End Try
        Return -1

    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Unit Group Code is already defined. Please define new Unit Group Code.")
            Language.setMessage(mstrModuleName, 2, "This Unit Group Name is already defined. Please define new Unit Group Name.")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Section Group. Reason : This Section Group is already linked with some transaction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class