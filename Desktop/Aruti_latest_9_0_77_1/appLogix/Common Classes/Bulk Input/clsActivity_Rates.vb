﻿'************************************************************************************************************************************
'Class Name : clsActivity_Rates.vb
'Purpose      :
'Date          :15-Jun-2013
'Written By  :Sandeep Sharma
'Modified    :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsActivity_Rates
    Private Shared ReadOnly mstrModuleName As String = "clsActivity_Rates"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "



    'Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty

#End Region
    
#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

#Region " Private Methods & Function "

    Public Function Generate_Table(ByVal sName As String, Optional ByVal blnIsFB As Boolean = False) As DataTable
        Dim mdtTran As DataTable
        Try
            mdtTran = New DataTable(sName)
            Dim dCol As DataColumn

            dCol = New DataColumn("period")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "Period")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            For i As Integer = 1 To 31
                dCol = New DataColumn("column" & i.ToString)
                If blnIsFB = True Then
                    dCol.DataType = System.Type.GetType("System.Int32")
                    dCol.DefaultValue = 0
                Else
                    dCol.DataType = System.Type.GetType("System.String")
                    dCol.DefaultValue = ""
                End If
                dCol.Caption = i.ToString
                mdtTran.Columns.Add(dCol)
            Next

            dCol = New DataColumn("periodid")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("fmonth")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("fyear")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("statusid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 2
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ischange")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("eday")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("imonth")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            dCol = New DataColumn("modeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)
            'Pinkal (4-Sep-2014) -- End


            'Pinkal (18-May-2015) -- Start
            'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.

            dCol = New DataColumn("PStartDate")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("PEndDate")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            'Pinkal (18-May-2015) -- End

            Return mdtTran
        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : Generate_Table; " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Fill_Data(ByVal sName As String, ByVal blnIsFBased As Boolean, _
                              ByVal intModeid As Integer, _
                              Optional ByVal iActivityId As Integer = 0, _
                              Optional ByVal sfmtCurrency As String = "", _
                              Optional ByVal iFinancialStDate As DateTime = Nothing, _
                              Optional ByVal iFinancialEtDate As DateTime = Nothing, _
                              Optional ByVal iPeriod As Integer = 0) As DataTable
        Dim mdtFinal As DataTable
        Try
            If iFinancialStDate = Nothing Then iFinancialStDate = FinancialYear._Object._Database_Start_Date
            If iFinancialEtDate = Nothing Then iFinancialEtDate = FinancialYear._Object._Database_End_Date

            mdtFinal = Generate_Table(sName, blnIsFBased)
            Dim objPeriod As New clscommom_period_Tran
            Dim dsList As New DataSet
            Dim StrQ As String = String.Empty
            Using objDataOperation As New clsDataOperation

                'S.SANDEEP [ 20 JULY 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'StrQ = "WITH MONTHS AS " & _
                '         "(" & _
                '         "		SELECT " & _
                '         "			CAST('" & eZeeDate.convertDate(iFinancialStDate) & "' AS DATETIME) DateValue " & _
                '         "	UNION ALL " & _
                '         "		SELECT " & _
                '         "			DATEADD(MONTH,1,DateValue) AS date " & _
                '         "		FROM MONTHS " & _
                '         "		WHERE DATEADD(MONTH,1,DateValue)<= '" & eZeeDate.convertDate(iFinancialEtDate) & "' " & _
                '         ") " & _
                '         "SELECT DISTINCT " & _
                '         "  DateValue AS stDay " & _
                '         " ,DATEADD(day,-1,DATEADD(month,MONTH(DateValue),DATEADD(year,YEAR(DateValue)-1900,0))) AS etDay " & _
                '         " ,DATENAME(MM,DateValue) AS Months " & _
                '         " ,DAY(DATEADD(DAY,-DAY(DATEADD(MONTH,1,DateValue)),DATEADD(MONTH,1,DateValue))) AS eday " & _
                '         " ,CASE WHEN LEN(MONTH(DateValue)) < 2 THEN '0' + CAST(MONTH(DateValue) AS NVARCHAR(MAX)) ELSE CAST(MONTH(DateValue) AS NVARCHAR(MAX)) END AS fmonth " & _
                '         " ,YEAR(DateValue) AS fyear " & _
                '         " ,MONTH(DateValue) AS imonth " & _
                '         " ,ISNULL(periodunkid,0) AS periodunkid " & _
                '         " ,ISNULL(statusid,2) AS statusid " & _
                '         " ,CONVERT(CHAR(8),DateValue,112) AS estDay " & _
                '         " ,CONVERT(CHAR(8),DATEADD(day,-1,DATEADD(month,MONTH(DateValue),DATEADD(year,YEAR(DateValue)-1900,0))),112) eetDay " & _
                '         "FROM MONTHS " & _
                '         " LEFT JOIN cfcommon_period_tran ON DateValue BETWEEN start_date AND end_date AND isactive = 1 " & _
                '         "OPTION (MAXRECURSION 0) "

                'Pinkal (18-May-2015) -- Start
                'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.

                StrQ = "WITH MONTHS AS " & _
                       "(" & _
                       "		SELECT " & _
                       "			CAST('" & eZeeDate.convertDate(iFinancialStDate) & "' AS DATETIME) DateValue " & _
                       "	UNION ALL " & _
                       "		SELECT " & _
                       "			DATEADD(MONTH,1,DateValue) AS date " & _
                       "		FROM MONTHS " & _
                       "		WHERE DATEADD(MONTH,1,DateValue)<= '" & eZeeDate.convertDate(iFinancialEtDate) & "' " & _
                       ") " & _
                        " SELECT stDay, etDay, Months, eday, fmonth, fyear, imonth, periodunkid, statusid, estDay, eetDay, Rno, " & intModeid & " as modeid,B.PStartDate, B.PEndDate " & _
                        " FROM " & _
                        " ( " & _
                       "SELECT DISTINCT " & _
                       "  DateValue AS stDay " & _
                       " ,DATEADD(day,-1,DATEADD(month,MONTH(DateValue),DATEADD(year,YEAR(DateValue)-1900,0))) AS etDay " & _
                       " ,DATENAME(MM,DateValue) AS Months " & _
                       " ,DAY(DATEADD(DAY,-DAY(DATEADD(MONTH,1,DateValue)),DATEADD(MONTH,1,DateValue))) AS eday " & _
                       " ,CASE WHEN LEN(MONTH(DateValue)) < 2 THEN '0' + CAST(MONTH(DateValue) AS NVARCHAR(MAX)) ELSE CAST(MONTH(DateValue) AS NVARCHAR(MAX)) END AS fmonth " & _
                       " ,YEAR(DateValue) AS fyear " & _
                       " ,MONTH(DateValue) AS imonth " & _
                        "      ,CASE WHEN cfcommon_period_tran.isactive = 0 THEN 0 ELSE ISNULL(periodunkid,0) END AS periodunkid " & _
                       " ,ISNULL(statusid,2) AS statusid " & _
                       " ,CONVERT(CHAR(8),DateValue,112) AS estDay " & _
                       " ,CONVERT(CHAR(8),DATEADD(day,-1,DATEADD(month,MONTH(DateValue),DATEADD(year,YEAR(DateValue)-1900,0))),112) eetDay " & _
                        ",ROW_NUMBER()OVER(PARTITION BY MONTH(DateValue) ORDER BY MONTH(DateValue)) AS Rno " & _
                        ", Convert(char(8),cfcommon_period_tran.start_date,112) AS PStartDate " & _
                        ", Convert(char(8),cfcommon_period_tran.end_date,112) AS PEndDate   " & _
                       "FROM MONTHS " & _
                        "    LEFT JOIN cfcommon_period_tran ON (MONTH(DateValue) = MONTH(start_date) AND YEAR(DateValue) = YEAR(start_date) " & _
                        "    OR MONTH(DateValue) = MONTH(end_date) AND YEAR(DateValue) = YEAR(end_date)) AND isactive  = 1  " & _
                        "  AND modulerefid = '" & enModuleReference.Payroll & "' " & _
                        " ) AS B WHERE Rno = 1 ORDER BY stDay " & _
                       "OPTION (MAXRECURSION 0) "
                'S.SANDEEP [ 20 JULY 2013 ] -- END

                'Pinkal (18-May-2015) -- End

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If
            End Using

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dtRow As DataRow = mdtFinal.NewRow

                    dtRow.Item("period") = dRow.Item("Months")
                    dtRow.Item("periodid") = dRow.Item("periodunkid")
                    dtRow.Item("fmonth") = dRow.Item("fmonth")
                    dtRow.Item("fyear") = dRow.Item("fyear")
                    dtRow.Item("statusid") = dRow.Item("statusid")
                    dtRow.Item("eday") = dRow.Item("eday")
                    dtRow.Item("imonth") = dRow.Item("imonth")

                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA
                    dtRow.Item("modeid") = dRow.Item("modeid")
                    'Pinkal (4-Sep-2014) -- End

                    'Pinkal (18-May-2015) -- Start
                    'Enhancement - PROBLEM IN KVTC WHEN PERIOD IS CREATED ON IN BETWEEN MONTHS.
                    dtRow.Item("PStartDate") = dRow.Item("PStartDate")
                    dtRow.Item("PEndDate") = dRow.Item("PEndDate")
                    'Pinkal (18-May-2015) -- End


                    mdtFinal.Rows.Add(dtRow)
                Next
            End If

                    If iActivityId > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                        If sfmtCurrency = "" Then sfmtCurrency = GUI.fmtCurrency
                        Dim dLst As New DataSet

                    'Pinkal (4-Sep-2014) -- Start
                    'Enhancement - PAY_A CHANGES IN PPA
                    'dLst = GetList("sRate", iActivityId, CInt(dRow.Item("periodunkid")))
                    dLst = GetList("sRate", intModeid, iActivityId, CInt(dRow.Item("periodunkid")))
                    'Pinkal (4-Sep-2014) -- End

                    If iPeriod > 0 Then
                        If iPeriod <> CInt(dRow.Item("periodunkid")) Then
                            dLst.Tables(0).Rows.Clear()
                        End If
                    End If
                        If blnIsFBased = True Then
                            For Each dARow As DataRow In dLst.Tables("sRate").Rows
                            Dim dtmp() As DataRow = Nothing
                            dtmp = mdtFinal.Select("imonth = '" & dARow.Item("iMonths") & "'")
                            dtmp(0).Item("column" & dARow.Item("iDay").ToString) = CInt(dARow.Item("iRate"))
                            Next
                        Else
                            For Each dARow As DataRow In dLst.Tables("sRate").Rows
                            Dim dtmp() As DataRow = Nothing
                            dtmp = mdtFinal.Select("imonth = '" & dARow.Item("iMonths") & "'")
                            dtmp(0).Item("column" & dARow.Item("iDay").ToString) = Format(CDec(dARow.Item("iRate")), sfmtCurrency)
                            Next
                        End If
                Next
                    End If

            objPeriod = Nothing

            Return mdtFinal
        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : Fill_Data; " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Get_CSV_PeriodIds(ByVal sLst As String, ByVal dtDate1 As Date, ByVal dtDate2 As Date, ByVal iYearId As Integer) As String
        Dim sPeriodIds As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet : Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'StrQ = "SELECT STUFF((SELECT ',' + CAST(periodunkid AS NVARCHAR(MAX)) FROM cfcommon_period_tran WHERE MONTH(start_date) >= MONTH(@Date1) AND MONTH(end_date) <= MONTH(@Date2) AND yearunkid = '" & iYearId & "' AND isactive = 1 ORDER BY end_date FOR XML PATH('')),1,1,'') AS PeriodIds "

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT STUFF((SELECT ',' + CAST(periodunkid AS NVARCHAR(MAX)) FROM cfcommon_period_tran WHERE MONTH(@Date) BETWEEN MONTH(start_date) AND MONTH(end_date) AND yearunkid = '" & iYearId & "' AND isactive = 1 ORDER BY end_date FOR XML PATH('')),1,1,'') AS PeriodIds "
            StrQ = "SELECT STUFF((SELECT ',' + CAST(periodunkid AS NVARCHAR(MAX)) FROM cfcommon_period_tran WHERE MONTH(@Date) BETWEEN MONTH(start_date) AND MONTH(end_date) AND yearunkid = '" & iYearId & "' AND isactive = 1 AND modulerefid = '" & enModuleReference.Payroll & "' AND statusid = 1  ORDER BY end_date FOR XML PATH('')),1,1,'') AS PeriodIds "
            'S.SANDEEP [ 20 JULY 2013 ] -- END

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate1).ToString)
            'objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate2).ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                sPeriodIds = dsList.Tables(0).Rows(0)("PeriodIds")
            End If

            Return sPeriodIds
        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : Get_CSV_PeriodIds; " & mstrModuleName)
            Return ""
        Finally
        End Try
    End Function


    Public Function Get_Date_Range(ByVal dtDate1 As String, ByVal dtDate2 As String, Optional ByVal mblnClosePeriod As Boolean = True) As DataTable

        'Pinkal (21-Jun-2016) -- 'Enhancement - Adding Close Period Boolean for Pay Per Activity Report.[Optional ByVal mblnClosePeriod As Boolean = True]

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet : Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "WITH MYCTE AS " & _
                   "(   SELECT CAST('" & dtDate1 & "' AS DATETIME) DateValue " & _
                   " UNION ALL " & _
                   "    SELECT  DateValue + 1 FROM MYCTE WHERE   DateValue + 1 <= '" & dtDate2 & "' " & _
                   ")SELECT " & _
                   "     DAY(DateValue) AS iDay " & _
                   "    ,CONVERT(CHAR(8),DateValue,112) AS iDate " & _
                   "    ,MONTH(DateValue) AS iMonth " & _
                   "    ,YEAR(DateValue) AS iYear " & _
                   "    ,DateValue AS DateValue " & _
                   "    ,SUBSTRING(DATENAME(dw,CONVERT(CHAR(8),DateValue,112)),0,4) AS iName " & _
                   "    ,periodunkid AS periodunkid " & _
                   "FROM MYCTE " & _
                   "    JOIN cfcommon_period_tran ON (MONTH(MYCTE.DateValue) = MONTH(start_date) AND YEAR(MYCTE.DateValue) = YEAR(start_date) " & _
                   "    OR MONTH(MYCTE.DateValue) = MONTH(end_date) AND YEAR(MYCTE.DateValue) = YEAR(end_date)) AND DateValue BETWEEN start_date AND end_date "

            If mblnClosePeriod Then
                StrQ &= "  AND statusid = 1 "
            End If

            StrQ &= " WHERE isactive = 1 AND modulerefid = '" & enModuleReference.Payroll & "' " & _
                   "    ORDER BY DateValue " & _
                   "OPTION (MAXRECURSION 0) "


            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            '" WHERE isactive = 1 AND modulerefid = '" & enModuleReference.Payroll & "' " -- Added
            'OR MONTH(MYCTE.DateValue) = MONTH(end_date) AND YEAR(MYCTE.DateValue) = YEAR(end_date) -- Added
            ' AND DateValue BETWEEN start_date AND end_date  -- Added
            'S.SANDEEP [ 20 JULY 2013 ] -- END



            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : Get_CSV_PeriodIds; " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Insert/Update/Delete in Table practivityrate_master
    ''' </summary>
    ''' <param name="mdtTran">Datatable for Insert/Update/Delete</param>
    ''' <param name="blnIsFB">To Determin Save Type (Flat Rate/Formula Based)</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function Insert_Update_Delete(ByVal mdtTran As DataTable, ByVal iUserId As Integer, ByVal sActivityIds As String, ByVal blnIsFB As Boolean, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) - [dtCurrentDateAndTime]
        Dim iStrQ, uStrQ, dStrQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim iTransactionId As Integer = 0
        Dim iRefValue As Decimal = 0
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim dtRow() As DataRow = mdtTran.Select("ischange = True")
            iStrQ = "" : uStrQ = "" : dStrQ = ""

            iStrQ = "INSERT INTO practivityrate_master ( " & _
                        "  activityunkid " & _
                        ", periodunkid " & _
                        ", rate_date " & _
                        ", isformula_based " & _
                        ", activity_rate " & _
                        ", tranheadunkid " & _
                        ", userunkid " & _
                        ", isactive" & _
                        ", modeid " & _
                    ") VALUES (" & _
                        "  @activityunkid " & _
                        ", @periodunkid " & _
                        ", @rate_date " & _
                        ", @isformula_based " & _
                        ", @activity_rate " & _
                        ", @tranheadunkid " & _
                        ", @userunkid " & _
                        ", @isactive" & _
                        ", @modeid " & _
                    "); SELECT @@identity"

            uStrQ = "UPDATE practivityrate_master SET " & _
                        "  activityunkid = @activityunkid" & _
                        ", periodunkid = @periodunkid" & _
                        ", rate_date = @rate_date" & _
                        ", isformula_based = @isformula_based" & _
                        ", activity_rate = @activity_rate" & _
                        ", tranheadunkid = @tranheadunkid" & _
                        ", userunkid = @userunkid" & _
                        ", isactive = @isactive " & _
                        ", modeid = @modeid " & _
                    "WHERE activityrateunkid = @activityrateunkid "

            dStrQ = "UPDATE practivityrate_master SET " & _
                        " isactive = 0 " & _
                    "WHERE activityrateunkid = @activityrateunkid "

            If dtRow.Length > 0 Then
                For Each iKey As String In sActivityIds.Split(",")
                    For iRow As Integer = 0 To dtRow.Length - 1
                        For iItem As Integer = 1 To dtRow(iRow).ItemArray.Length - 6
                            If CInt(iItem) > CInt(dtRow(iRow).Item("eday")) Then Continue For
                            Dim dtDate As Date = eZeeDate.convertDate(dtRow(iRow).Item("fyear").ToString & dtRow(iRow).Item("fmonth").ToString & iItem.ToString("0#")).Date
                            iTransactionId = IsExist(CInt(iKey), dtDate, dtRow(iRow).Item("periodid"), objDataOperation, iRefValue, CInt(dtRow(iRow).Item("modeid")))
                            If iTransactionId <= 0 Then
                                If dtRow(iRow).Item(iItem).ToString = "0" Or dtRow(iRow).Item(iItem).ToString = "" Then Continue For

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow(iRow).Item("periodid"))
                                objDataOperation.AddParameter("@rate_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtDate)
                                objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFB.ToString)
                                If blnIsFB = True Then
                                    objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, 0)
                                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow(iRow).Item(iItem).ToString)
                                Else
                                    objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dtRow(iRow).Item(iItem).ToString))
                                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                End If
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)


                                'Pinkal (4-Sep-2014) -- Start
                                'Enhancement - PAY_A CHANGES IN PPA
                                objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow(iRow).Item("modeid"))
                                'Pinkal (4-Sep-2014) -- End


                                dsList = objDataOperation.ExecQuery(iStrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                iTransactionId = dsList.Tables(0).Rows(0).Item(0)
                                objDataOperation.AddParameter("@activityrateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iTransactionId)

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If Insert_Rates_AT_Logs(objDataOperation, 1, iUserId) = False Then
                                If Insert_Rates_AT_Logs(objDataOperation, 1, iUserId, dtCurrentDateAndTime) = False Then
                                    'Shani(24-Aug-2015) -- End

                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            ElseIf iTransactionId > 0 Then

                                If CDec(IIf(IsDBNull(dtRow(iRow).Item(iItem)), 0, dtRow(iRow).Item(iItem))) <> iRefValue Then
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@activityrateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iTransactionId)
                                    objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow(iRow).Item("periodid"))
                                    objDataOperation.AddParameter("@rate_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtDate)
                                    objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFB.ToString)
                                    If blnIsFB = True Then
                                        objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, 0)
                                        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow(iRow).Item(iItem).ToString)
                                    Else
                                        objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(IIf(IsDBNull(dtRow(iRow).Item(iItem)), 0, dtRow(iRow).Item(iItem))))
                                        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                    End If
                                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
                                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)

                                    'Pinkal (4-Sep-2014) -- Start
                                    'Enhancement - PAY_A CHANGES IN PPA
                                    objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow(iRow).Item("modeid"))
                                    'Pinkal (4-Sep-2014) -- End

                                    If IIf(IsDBNull(dtRow(iRow).Item(iItem)), 0, dtRow(iRow).Item(iItem)) = "0" Then
                                        Call objDataOperation.ExecNonQuery(dStrQ)
                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        'Shani(24-Aug-2015) -- Start
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'If Insert_Rates_AT_Logs(objDataOperation, 3, iUserId) = False Then
                                        If Insert_Rates_AT_Logs(objDataOperation, 3, iUserId, dtCurrentDateAndTime) = False Then
                                            'Shani(24-Aug-2015) -- End

                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    Else
                                        Call objDataOperation.ExecNonQuery(uStrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If


                                        'Shani(24-Aug-2015) -- Start
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'If Insert_Rates_AT_Logs(objDataOperation, 2, iUserId) = False Then
                                        If Insert_Rates_AT_Logs(objDataOperation, 2, iUserId, dtCurrentDateAndTime) = False Then
                                            'Shani(24-Aug-2015) -- End

                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If


                                        'Shani(24-Aug-2015) -- Start
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'If Update_Activity_Tran(CInt(iKey), dtDate, CDec(dtRow(iRow).Item(iItem)), blnIsFB, objDataOperation, iUserId, CInt(dtRow(iRow).Item("modeid"))) = False Then
                                        If Update_Activity_Tran(CInt(iKey), dtDate, CDec(dtRow(iRow).Item(iItem)), blnIsFB, objDataOperation, iUserId, CInt(dtRow(iRow).Item("modeid")), dtCurrentDateAndTime) = False Then
                                            'Shani(24-Aug-2015) -- End

                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End If
                                End If
                            End If
                        Next
                    Next
                Next
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception("-1;" & ex.Message & "; Procedure : Insert_Update_Delete; " & mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    Public Function IsExist(ByVal iActivityId As Integer, ByVal iDate As Date, ByVal iPeriodId As Integer, ByVal objDataOperation As clsDataOperation, ByRef iValue As Decimal, ByVal iModeId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim iRateTranUnkid As Integer = 0
        Try
            StrQ = "SELECT activityrateunkid " & _
                   " ,CASE WHEN isformula_based = 0 THEN activity_rate " & _
                   "	   WHEN isformula_based = 1 THEN tranheadunkid END AS iRate " & _
                   "FROM practivityrate_master " & _
                   "WHERE isactive = 1 AND activityunkid = '" & iActivityId & "' AND CONVERT(CHAR(8),rate_date,112)= '" & eZeeDate.convertDate(iDate).ToString & "' " & _
                   " AND periodunkid = '" & iPeriodId & "'  AND modeid = " & iModeId

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iRateTranUnkid = dsList.Tables(0).Rows(0)("activityrateunkid")
                iValue = dsList.Tables(0).Rows(0)("iRate")
            End If

            Return iRateTranUnkid

        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : IsExist; " & mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (4-Sep-2014) -- End


    Public Function GetList(ByVal sTabName As String, ByVal intModeId As Integer, Optional ByVal iActivityId As Integer = 0, Optional ByVal iPeriodId As Integer = 0) As DataSet 'Pinkal (4-Sep-2014)  'Enhancement - PAY_A CHANGES IN PPA    ['ByVal intModeId As Integer]
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "  periodunkid " & _
                   " ,rate_date " & _
                   " ,CONVERT(CHAR(8),rate_date,112)  AS R_Date " & _
                   " ,DATEPART(dd,rate_date) AS iDay " & _
                   " ,CASE WHEN isformula_based = 0 THEN activity_rate " & _
                   "	   WHEN isformula_based = 1 THEN tranheadunkid END AS iRate " & _
                   " ,activityrateunkid " & _
                   " ,activityunkid " & _
                   " ,isformula_based " & _
                   " ,tranheadunkid " & _
                   " ,isactive " & _
                   " ,MONTH(rate_date) as iMonths " & _
                   " , ISNULL(modeid,1) AS modeid " & _
                   "FROM practivityrate_master " & _
                   "WHERE isactive = 1 "

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ' " MONTH(rate_date) as iMonths "  -- Added
            'S.SANDEEP [ 20 JULY 2013 ] -- END


            If iActivityId > 0 Then
                StrQ &= " AND activityunkid = '" & iActivityId & "' "
            End If

            'S.SANDEEP [ 10 MAR 2014 ] -- START
            'If iPeriodId > 0 Then
            '    StrQ &= " AND periodunkid = '" & iPeriodId & "' "
            'End If
                StrQ &= " AND periodunkid = '" & iPeriodId & "' "
            'S.SANDEEP [ 10 MAR 2014 ] -- END


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            StrQ &= " AND modeid = " & intModeId
            'Pinkal (4-Sep-2014) -- End


            dsList = objDataOperation.ExecQuery(StrQ, sTabName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : GetList; " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Update_Activity_Tran(ByVal iActivityId As Integer, ByVal iDate As Date, ByVal iValue As Decimal, ByVal IsFB As Boolean, _
                                                            ByVal objDataOperation As clsDataOperation, ByVal iUserId As Integer, ByVal intModeId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) - [dtCurrentDateAndTime]
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT payactivitytranunkid " & _
                   "FROM prpayactivity_tran " & _
                   "WHERE activityunkid = '" & iActivityId & "' AND CONVERT(CHAR(8),activity_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' " & _
                   "AND isvoid = 0  "
            'Sohail (16 Aug 2014) - [AND isposted = 0] To update rate in already posted activities before payroll is processed for last date of selected period.

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA

            'If IsFB = False Then
            '    StrQ = "UPDATE prpayactivity_tran " & _
            '           " SET activity_rate = @Value " & _
            '           "  ,normal_amount = CASE WHEN normal_amount > 0 THEN (CASE WHEN normal_hrs > activity_value THEN activity_value ELSE normal_hrs END) * @Value ELSE 0 END " & _
            '           "  ,ot_amount = CASE WHEN ot_amount > 0 THEN ot_hrs * (CASE WHEN normal_factor > 0 THEN normal_factor ELSE ph_factor END) * @Value ELSE 0 END " & _
            '           "  ,amount = CASE WHEN normal_amount > 0 THEN (CASE WHEN normal_hrs > activity_value THEN activity_value ELSE normal_hrs END) * @Value ELSE 0 END + CASE WHEN ot_amount > 0 THEN ot_hrs * (CASE WHEN normal_factor > 0 THEN normal_factor ELSE ph_factor END) * @Value ELSE 0 END " & _
            '           "WHERE payactivitytranunkid = @payactivitytranunkid "
            '    'Sohail (16 Aug 2014) - [AND isposted = 0] To update rate in already posted activities before payroll is processed for last date of selected period.
            'Else
            '    StrQ = "UPDATE prpayactivity_tran " & _
            '           "    SET activity_rate = 0,amount = 0,tranheadunkid = @Value " & _
            '           "WHERE payactivitytranunkid = @payactivitytranunkid "
            '    'Sohail (16 Aug 2014) - [AND isposted = 0] To update rate in already posted activities before payroll is processed for last date of selected period.
            'End If


            Dim days() As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            Dim mDicWeekName As New Dictionary(Of String, Integer)
            For i As Integer = 0 To 6
                mDicWeekName.Add(days(i).ToString, i)
            Next

            For Each dRow As DataRow In dsList.Tables(0).Rows
                'objDataOperation.ClearParameters()
                'If IsFB = False Then
                '    objDataOperation.AddParameter("@Value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iValue)
                'Else
                '    objDataOperation.AddParameter("@Value", SqlDbType.Int, eZeeDataType.INT_SIZE, iValue)
                'End If
                'objDataOperation.AddParameter("@payactivitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("payactivitytranunkid"))

                'objDataOperation.ExecNonQuery(StrQ)

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                Dim objEmpShift As New clsEmployee_Shift_Tran
                Dim objAct_Tran As New clsPayActivity_Tran
                Dim mblnIsWeekEnd As Boolean = False
                Dim mblnIsHoliday As Boolean = False
                Dim mintShiftUnkid As Integer = 0
                Dim mDblSftHrs As Decimal = 0
                objAct_Tran._ObjData = objDataOperation
                objAct_Tran._Payactivitytranunkid = dRow.Item("payactivitytranunkid")

                mintShiftUnkid = objEmpShift.GetEmployee_Current_ShiftId(objAct_Tran._Activity_Date.Date, objAct_Tran._Employeeunkid)
                mDblSftHrs = objAct_Tran.Get_Sft_WorkHrs(mDicWeekName, mintShiftUnkid, objAct_Tran._Activity_Date.Date, mblnIsWeekEnd)

                Dim objEHoliday As New clsemployee_holiday
                If CType(objEHoliday.GetEmployeeHoliday(objAct_Tran._Employeeunkid, objAct_Tran._Activity_Date.Date), DataTable).Rows.Count > 0 Then
                    mblnIsHoliday = True
                Else
                    mblnIsHoliday = False
                End If


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                Dim objActMst As New clsActivity_Master
                objActMst._Activityunkid = iActivityId
                'Pinkal (23-Sep-2014) -- End


                If objAct_Tran._Isformula_Based = False Then
                    Select Case intModeId
                        Case enPPAMode.RATE
                            objAct_Tran._Activity_Rate = iValue
                        Case enPPAMode.OT
                            objAct_Tran._Normal_Factor = iValue
                        Case enPPAMode.PH
                            objAct_Tran._PH_Factor = iValue
                    End Select


                    If objAct_Tran._Activity_Rate > 0 Then
                        If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                            If objActMst._Is_Ot_Activity Then
                            objAct_Tran._Ot_Hrs = objAct_Tran._Activity_Value
                            objAct_Tran._Amount = ((objAct_Tran._Activity_Rate * objAct_Tran._PH_Factor) * objAct_Tran._Activity_Value)
                            objAct_Tran._Ot_Amount = objAct_Tran._Amount
                            objAct_Tran._Normal_Amount = 0
                        Else
                                objAct_Tran._Normal_Hrs = CDec(mDblSftHrs)
                                objAct_Tran._Normal_Amount = 0
                                objAct_Tran._Ot_Amount = 0
                                objAct_Tran._PH_Factor = 0
                                objAct_Tran._Normal_Factor = 0
                                objAct_Tran._Ot_Hrs = 0
                                objAct_Tran._Amount = objAct_Tran._Activity_Rate * objAct_Tran._Activity_Value
                            End If
                        Else
                            If mDblSftHrs > 0 Then
                                If objActMst._Is_Ot_Activity Then
                                objAct_Tran._Ot_Hrs = objAct_Tran._Activity_Value - mDblSftHrs
                                If objAct_Tran._Ot_Hrs <= 0 Then
                                    objAct_Tran._Amount = objAct_Tran._Activity_Rate * objAct_Tran._Activity_Value
                                    objAct_Tran._Normal_Amount = objAct_Tran._Amount
                                    objAct_Tran._Ot_Amount = 0
                                    objAct_Tran._Ot_Hrs = 0
                                Else
                                        objAct_Tran._Normal_Hrs = CDec(mDblSftHrs)
                                    objAct_Tran._Normal_Amount = (CDec(mDblSftHrs * objAct_Tran._Activity_Rate))
                                    objAct_Tran._Ot_Amount = CDec(objAct_Tran._Ot_Hrs * (objAct_Tran._Activity_Rate * objAct_Tran._Normal_Factor))
                                    objAct_Tran._Amount = objAct_Tran._Normal_Amount + objAct_Tran._Ot_Amount
                                End If
            Else
                                    objAct_Tran._Normal_Hrs = CDec(mDblSftHrs)
                                    objAct_Tran._Normal_Amount = 0
                                    objAct_Tran._Ot_Amount = 0
                                    objAct_Tran._PH_Factor = 0
                                    objAct_Tran._Normal_Factor = 0
                                    objAct_Tran._Ot_Hrs = 0
                                    objAct_Tran._Amount = objAct_Tran._Activity_Rate * objAct_Tran._Activity_Value
                                End If
                            Else
                                objAct_Tran._Amount = objAct_Tran._Activity_Rate * objAct_Tran._Activity_Value
                            End If
                        End If
            End If

                Else
                    objAct_Tran._Activity_Rate = 0
                    objAct_Tran._Amount = 0
                    objAct_Tran._Ot_Amount = 0
                    objAct_Tran._Normal_Amount = 0
                    Select Case intModeId
                        Case enPPAMode.RATE
                            objAct_Tran._Tranheadunkid = iValue
                        Case enPPAMode.OT
                            objAct_Tran._OT_Tranheadunkid = iValue
                        Case enPPAMode.PH
                            objAct_Tran._PH_Tranheadunkid = iValue
                    End Select

                    If objActMst._Is_Ot_Activity Then
                    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                        objAct_Tran._Ot_Hrs = objAct_Tran._Activity_Value
                    Else
                        If mDblSftHrs > 0 Then
                            objAct_Tran._Ot_Hrs = objAct_Tran._Activity_Value - mDblSftHrs
                            If objAct_Tran._Ot_Hrs <= 0 Then
                                objAct_Tran._Ot_Hrs = 0
                                objAct_Tran._Normal_Hrs = mDblSftHrs
                                    objAct_Tran._OT_Tranheadunkid = 0
                                    objAct_Tran._PH_Tranheadunkid = 0
                            Else
                                objAct_Tran._Normal_Hrs = mDblSftHrs
                            End If
                        Else
                            objAct_Tran._Normal_Hrs = mDblSftHrs
                        End If
                End If
                    Else
                        objAct_Tran._PH_Tranheadunkid = 0
                        objAct_Tran._OT_Tranheadunkid = 0
                        objAct_Tran._Ot_Hrs = 0
                        objAct_Tran._PH_Factor = 0
                        objAct_Tran._Normal_Factor = 0
                    End If
                End If

                With objAct_Tran
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAct_Tran.Update(objDataOperation, iUserId) = False Then
                If objAct_Tran.Update(dtCurrentDateAndTime, objDataOperation, iUserId) = False Then
                    'Shani(24-Aug-2015) -- End

                    objAct_Tran = Nothing
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objAct_Tran = Nothing
            Next

            'Pinkal (4-Sep-2014) -- End

            Return True
        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : Update_Activity_Tran; " & mstrModuleName)
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Insert_Rates_AT_Logs(ByVal objDataOperation As clsDataOperation, ByVal iAuditTyp As Integer, ByVal iUserId As Integer) As Boolean
    Private Function Insert_Rates_AT_Logs(ByVal objDataOperation As clsDataOperation, ByVal iAuditTyp As Integer, ByVal iUserId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA

            StrQ = "INSERT INTO atpractivityrate_master ( " & _
                       "  activityrateunkid " & _
                       ", activityunkid " & _
                       ", periodunkid " & _
                       ", rate_date " & _
                       ", isformula_based " & _
                       ", activity_rate " & _
                       ", tranheadunkid " & _
                       ", userunkid " & _
                       ", isactive " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", isweb " & _
                       ", loginemployeeunkid" & _
                       ", modeid " & _
                   ") VALUES (" & _
                       "  @activityrateunkid " & _
                       ", @activityunkid " & _
                       ", @periodunkid " & _
                       ", @rate_date " & _
                       ", @isformula_based " & _
                       ", @activity_rate " & _
                       ", @tranheadunkid " & _
                       ", @userunkid " & _
                       ", @isactive " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       " " & _
                       ", @isweb " & _
                       ", @loginemployeeunkid" & _
                       ", @modeid " & _
                   "); SELECT @@identity"

            'Pinkal (4-Sep-2014) -- End

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, iAuditTyp.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : Insert_Rates_AT_Logs; " & mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    Public Function GetComboForMode(ByVal StrLst As String, Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            If blnFlag = True Then
                StrQ = "SELECT  @Select AS name, 0 AS modeid UNION "
            End If

            StrQ &= " SELECT @Rate AS name, 1 As modeid  UNION " & _
                          " SELECT @OT AS name, 2 As modeid  UNION " & _
                          " SELECT @PH AS name, 3 As modeid  ORDER BY  modeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@Rate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Rate"))
            objDataOperation.AddParameter("@OT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "OT"))
            objDataOperation.AddParameter("@PH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "PH"))
            dsList = objDataOperation.ExecQuery(StrQ, StrLst)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & "; Procedure : GetComboForMode; " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    'Pinkal (4-Sep-2014) -- End


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period")
            Language.setMessage(mstrModuleName, 2, "WEB")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "Rate")
            Language.setMessage(mstrModuleName, 5, "OT")
            Language.setMessage(mstrModuleName, 6, "PH")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
