﻿'************************************************************************************************************************************
'Class Name : clsActivity_Master.vb
'Purpose      :
'Date          :11-Jun-2013
'Written By  :Sandeep Sharma
'Modified    :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsActivity_Master
    Private Shared ReadOnly mstrModuleName As String = "clsActivity_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintActivityunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mintMeasureunkid As Integer
    Private mintTranheadTypeId As Integer
    Private mstrDescription As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIsactive As Boolean = True
    'Hemant (26-07-2018)  -- Start
    'Enhancement : To Stop Generate & Insert Junk Character of blank value in variable mchrIsdeleted into "parent_xmlvalue" & Child_xmlvalue Columns In Audit & Trail Database Table 
    'Private mchrIsdeleted As Char
    Private mchrIsdeleted As Char = "0"
    'Hemant (26-07-2018)  -- End
    Private mblnIs_Ot_Activity As Boolean
    Private msinNormal_Otfactor As Decimal
    Private msinPh_Otfactor As Decimal



    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
    Private mblnIsappearonpayslip As Boolean = True
    Private mblnIstaxable As Boolean

    Private mblnIsappearOTonpayslip As Boolean = True
    Private mblnIstaxableOT As Boolean

    Private mblnIsappearPHonpayslip As Boolean = True
    Private mblnIstaxablePH As Boolean
    'Sohail (17 Sep 2014) -- End

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activityunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Activityunkid() As Integer
        Get
            Return mintActivityunkid
        End Get
        Set(ByVal value As Integer)
            mintActivityunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set measureunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Measureunkid() As Integer
        Get
            Return mintMeasureunkid
        End Get
        Set(ByVal value As Integer)
            mintMeasureunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _TranheadTypeId() As Integer
        Get
            Return mintTranheadTypeId
        End Get
        Set(ByVal value As Integer)
            mintTranheadTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdeleted
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isdeleted() As Char
        Get
            Return mchrIsdeleted
        End Get
        Set(ByVal value As Char)
            mchrIsdeleted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_ot_activity
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Is_Ot_Activity() As Boolean
        Get
            Return mblnIs_Ot_Activity
        End Get
        Set(ByVal value As Boolean)
            mblnIs_Ot_Activity = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set normal_otfactor
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Normal_Otfactor() As Decimal
        Get
            Return msinNormal_Otfactor
        End Get
        Set(ByVal value As Decimal)
            msinNormal_Otfactor = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ph_otfactor
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ph_Otfactor() As Decimal
        Get
            Return msinPh_Otfactor
        End Get
        Set(ByVal value As Decimal)
            msinPh_Otfactor = value
        End Set
    End Property


#End Region

    'Sohail (17 Sep 2014) -- Start
    'Enhancement - System generated Base, OT and PH heads for PPA Activity.
#Region " Other Properties "
    Public Property _Isappearonpayslip() As Boolean
        Get
            Return mblnIsappearonpayslip
        End Get
        Set(ByVal value As Boolean)
            mblnIsappearonpayslip = value
        End Set
    End Property

    Public Property _Istaxable() As Boolean
        Get
            Return mblnIstaxable
        End Get
        Set(ByVal value As Boolean)
            mblnIstaxable = Value
        End Set
    End Property


    Public Property _IsappearOTonpayslip() As Boolean
        Get
            Return mblnIsappearOTonpayslip
        End Get
        Set(ByVal value As Boolean)
            mblnIsappearOTonpayslip = value
        End Set
    End Property

    Public Property _IstaxableOT() As Boolean
        Get
            Return mblnIstaxableOT
        End Get
        Set(ByVal value As Boolean)
            mblnIstaxableOT = value
        End Set
    End Property


    Public Property _IsappearPHonpayslip() As Boolean
        Get
            Return mblnIsappearPHonpayslip
        End Get
        Set(ByVal value As Boolean)
            mblnIsappearPHonpayslip = value
        End Set
    End Property

    Public Property _IstaxablePH() As Boolean
        Get
            Return mblnIstaxablePH
        End Get
        Set(ByVal value As Boolean)
            mblnIstaxablePH = value
        End Set
    End Property
#End Region
    'Sohail (17 Sep 2014) -- End

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "  activityunkid " & _
                     ", code " & _
                     ", name " & _
                     ", measureunkid " & _
                     ", trnheadtype_id " & _
                     ", description " & _
                     ", name1 " & _
                     ", name2 " & _
                     ", is_ot_activity " & _
                     ", normal_otfactor " & _
                     ", ph_otfactor " & _
                     ", isactive " & _
                     ", isdeleted " & _
                     "FROM practivity_master " & _
                     "WHERE activityunkid = @activityunkid "

            objDataOperation.AddParameter("@activityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintActivityUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintactivityunkid = CInt(dtRow.Item("activityunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mintmeasureunkid = CInt(dtRow.Item("measureunkid"))
                mintTranheadTypeId = CInt(dtRow.Item("trnheadtype_id"))
                mstrdescription = dtRow.Item("description").ToString
                mstrname1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mblnIs_Ot_Activity = CBool(dtRow.Item("is_ot_activity"))
                msinNormal_Otfactor = dtRow.Item("normal_otfactor")
                msinPh_Otfactor = dtRow.Item("ph_otfactor")
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mchrisdeleted = dtRow.Item("isdeleted")
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     " practivity_master.activityunkid " & _
                     ",practivity_master.code " & _
                     ",practivity_master.name " & _
                     ",practivity_master.measureunkid " & _
                     ",practivity_master.trnheadtype_id " & _
                     ",practivity_master.description " & _
                     ",practivity_master.name1 " & _
                     ",practivity_master.name2 " & _
                     ",practivity_master.is_ot_activity " & _
                     ",practivity_master.normal_otfactor " & _
                     ",practivity_master.ph_otfactor " & _
                     ",practivity_master.isactive " & _
                     ",practivity_master.isdeleted " & _
                     ",prunitmeasure_master.name AS measure " & _
                     ",CASE WHEN trnheadtype_id = 1 THEN @EarningforEmployee " & _
                     "      WHEN trnheadtype_id = 2 THEN @DeductionFromEmployee " & _
                     "      WHEN trnheadtype_id = 5 THEN @Informational " & _
                     " END AS headtype " & _
                     "FROM practivity_master " & _
                     " JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid AND prunitmeasure_master.isactive = 1 "
            'Sohail (19 Aug 2014) - [@Informational ]

            If blnOnlyActive Then
                strQ &= " WHERE practivity_master.isactive = 1 "
            End If

            objDataOperation.AddParameter("@EarningforEmployee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 2, "Earning for Employee"))
            objDataOperation.AddParameter("@DeductionFromEmployee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 3, "Deduction From Employee"))
            objDataOperation.AddParameter("@Informational", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 6, "Informational")) 'Sohail (19 Aug 2014)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (practivity_master) </purpose>
    Public Function Insert(ByVal strDatabaseName As String, ByVal dtCurrentDateAndTime As Date, ByVal intUserunkid As Integer) As Boolean
        'Shani(24-Aug-2015) - [intUserunkid]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]
        If isExist("", mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This activity name is already defined. Please define new activity name.")
            Return False
        End If

        If isExist(mstrCode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This activity code is already defined. Please define new activity code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@measureunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmeasureunkid.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadTypeId.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@isdeleted", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIsdeleted.ToString)
            objDataOperation.AddParameter("@is_ot_activity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Ot_Activity.ToString)
            objDataOperation.AddParameter("@normal_otfactor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinNormal_Otfactor)
            objDataOperation.AddParameter("@ph_otfactor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinPh_Otfactor)


            strQ = "INSERT INTO practivity_master ( " & _
                      "  code " & _
                      ", name " & _
                      ", measureunkid " & _
                      ", trnheadtype_id " & _
                      ", description " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", is_ot_activity " & _
                      ", normal_otfactor " & _
                      ", ph_otfactor " & _
                      ", isactive " & _
                      ", isdeleted" & _
                   ") VALUES (" & _
                      "  @code " & _
                      ", @name " & _
                      ", @measureunkid " & _
                      ", @trnheadtype_id " & _
                      ", @description " & _
                      ", @name1 " & _
                      ", @name2 " & _
                      ", @is_ot_activity " & _
                      ", @normal_otfactor " & _
                      ", @ph_otfactor " & _
                      ", @isactive " & _
                      ", @isdeleted" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintActivityunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "practivity_master", "activityunkid", mintActivityunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            Dim objTransactionHead As New clsTransactionHead
            Dim intHeadID As Integer = 0


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'NORMAL AMOUNT HEAD
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, mintActivityunkid, intHeadID) = True Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, mintActivityunkid, intHeadID, objDataOperation) = True Then
                'Sohail (03 May 2018) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = intHeadID
                objTransactionHead._Tranheadunkid(strDatabaseName) = intHeadID
                'Sohail (21 Aug 2015) -- End
            End If
            objTransactionHead._Trnheadcode = mstrCode
            objTransactionHead._Trnheadname = mstrName
            objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
            objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY
            objTransactionHead._Calctype_Id = enCalcType.PAY_PER_ACTIVITY
            objTransactionHead._Isrecurrent = True
            objTransactionHead._Isappearonpayslip = mblnIsappearonpayslip
            objTransactionHead._Computeon_Id = 0
            objTransactionHead._Formula = ""
            objTransactionHead._Formulaid = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTransactionHead._Userunkid = User._Object._Userunkid
            objTransactionHead._Userunkid = intUserunkid
            'Shani(24-Aug-2015) -- End

            objTransactionHead._Istaxable = mblnIstaxable
            objTransactionHead._Istaxrelief = False
            objTransactionHead._Iscumulative = False
            objTransactionHead._Isnoncashbenefit = False
            objTransactionHead._Ismonetary = True
            objTransactionHead._Isactive = True
            objTransactionHead._Activityunkid = mintActivityunkid
            objTransactionHead._Isbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
            'Hemant (23 Oct 2020) -- Start
            objTransactionHead._CostCenterUnkId = 0
            'Hemant (23 Oct 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            objTransactionHead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            objTransactionHead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
            'Sohail (11 Dec 2020) -- End

            If intHeadID > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'If objTransactionHead.Update(enCalcType.PAY_PER_ACTIVITY) = False Then
                If objTransactionHead.Update(strDatabaseName, enCalcType.PAY_PER_ACTIVITY, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            Else

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY) = False Then
                If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            End If

            'OT AMOUNT HEAD
            intHeadID = 0
            objTransactionHead = New clsTransactionHead


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, mintActivityunkid, intHeadID) = True Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, mintActivityunkid, intHeadID, objDataOperation) = True Then
                'Sohail (03 May 2018) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = intHeadID
                objTransactionHead._Tranheadunkid(strDatabaseName) = intHeadID
                'Sohail (21 Aug 2015) -- End
            End If
            objTransactionHead._Trnheadcode = mstrCode & " OT"
            objTransactionHead._Trnheadname = mstrName & " OT"
            objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
            objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY
            objTransactionHead._Calctype_Id = enCalcType.PAY_PER_ACTIVITY_OT
            objTransactionHead._Isrecurrent = True
            objTransactionHead._Isappearonpayslip = mblnIsappearOTonpayslip
            objTransactionHead._Computeon_Id = 0
            objTransactionHead._Formula = ""
            objTransactionHead._Formulaid = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTransactionHead._Userunkid = User._Object._Userunkid
            objTransactionHead._Userunkid = intUserunkid
            'Shani(24-Aug-2015) -- End

            objTransactionHead._Istaxable = mblnIstaxableOT
            objTransactionHead._Istaxrelief = False
            objTransactionHead._Iscumulative = False
            objTransactionHead._Isnoncashbenefit = False
            objTransactionHead._Ismonetary = True
            objTransactionHead._Isactive = True
            objTransactionHead._Activityunkid = mintActivityunkid
            objTransactionHead._Isbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
            'Hemant (23 Oct 2020) -- Start
            objTransactionHead._CostCenterUnkId = 0
            'Hemant (23 Oct 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            objTransactionHead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            objTransactionHead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
            'Sohail (11 Dec 2020) -- End
            If intHeadID > 0 Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Update(enCalcType.PAY_PER_ACTIVITY_OT) = False Then
                If objTransactionHead.Update(strDatabaseName, enCalcType.PAY_PER_ACTIVITY_OT, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            Else

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_OT) = False Then
                If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_OT, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            End If

            'PH AMOUNT HEAD
            intHeadID = 0
            objTransactionHead = New clsTransactionHead

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, mintActivityunkid, intHeadID) = True Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, mintActivityunkid, intHeadID, objDataOperation) = True Then
                'Sohail (03 May 2018) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = intHeadID
                objTransactionHead._Tranheadunkid(strDatabaseName) = intHeadID
                'Sohail (21 Aug 2015) -- End
            End If
            objTransactionHead._Trnheadcode = mstrCode & " PH"
            'Hemant (27 July 2018) -- Start
            'Enhancement : To Stop Insert Trnheadcode value into Trnheadname Column
            'objTransactionHead._Trnheadname = mstrCode & " PH"
            objTransactionHead._Trnheadname = mstrName & " PH"
            'Hemant (27 July 2018) -- End
            objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
            objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY
            objTransactionHead._Calctype_Id = enCalcType.PAY_PER_ACTIVITY_PH
            objTransactionHead._Isrecurrent = True
            objTransactionHead._Isappearonpayslip = mblnIsappearPHonpayslip
            objTransactionHead._Computeon_Id = 0
            objTransactionHead._Formula = ""
            objTransactionHead._Formulaid = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTransactionHead._Userunkid = User._Object._Userunkid
            objTransactionHead._Userunkid = intUserunkid
            'Shani(24-Aug-2015) -- End

            objTransactionHead._Istaxable = mblnIstaxablePH
            objTransactionHead._Istaxrelief = False
            objTransactionHead._Iscumulative = False
            objTransactionHead._Isnoncashbenefit = False
            objTransactionHead._Ismonetary = True
            objTransactionHead._Isactive = True
            objTransactionHead._Activityunkid = mintActivityunkid
            objTransactionHead._Isbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
            'Hemant (23 Oct 2020) -- Start
            objTransactionHead._CostCenterUnkId = 0
            'Hemant (23 Oct 2020) -- End
            'Sohail (03 Dec 2020) -- Start
            'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
            objTransactionHead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
            'Sohail (03 Dec 2020) -- End
            'Sohail (11 Dec 2020) -- Start
            'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
            objTransactionHead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
            'Sohail (11 Dec 2020) -- End
            If intHeadID > 0 Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Update(enCalcType.PAY_PER_ACTIVITY_PH) = False Then
                If objTransactionHead.Update(strDatabaseName, enCalcType.PAY_PER_ACTIVITY_PH, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            Else

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_PH) = False Then
                If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_PH, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            End If
            'Sohail (17 Sep 2014) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (practivity_master) </purpose>
    Public Function Update(ByVal strDatabaseName As String, ByVal dtCurrentDateAndTime As Date, ByVal intUserunkid As Integer) As Boolean
        'Shani(24-Aug-2015) - [intUserunkid]
        'Sohail (21 Aug 2015) - [strDatabaseName, dtCurrentDateAndTime]

        If isExist("", mstrName, mintActivityunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This activity name is already defined. Please define new activity name.")
            Return False
        End If

        If isExist(mstrCode, "", mintActivityunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This activity code is already defined. Please define new activity code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@activityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintactivityunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@measureunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmeasureunkid.ToString)
            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadTypeId.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@isdeleted", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIsdeleted.ToString)
            objDataOperation.AddParameter("@is_ot_activity", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Ot_Activity.ToString)
            objDataOperation.AddParameter("@normal_otfactor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinNormal_Otfactor)
            objDataOperation.AddParameter("@ph_otfactor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinPh_Otfactor)

            strQ = "UPDATE practivity_master SET " & _
                    "  code = @code" & _
                    ", name = @name" & _
                    ", measureunkid = @measureunkid" & _
                    ", trnheadtype_id = @trnheadtype_id" & _
                    ", description = @description" & _
                    ", name1 = @name1" & _
                    ", name2 = @name2" & _
                    ", is_ot_activity = @is_ot_activity" & _
                    ", normal_otfactor = @normal_otfactor" & _
                    ", ph_otfactor = @ph_otfactor" & _
                    ", isactive = @isactive" & _
                    ", isdeleted = @isdeleted " & _
                   "WHERE activityunkid = @activityunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "practivity_master", mintActivityunkid, "activityunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "practivity_master", "activityunkid", mintActivityunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (22 Aug 2014) -- Start
            'Enhancement - Update OT Rates and PH Rates to UnPosted Activities.
            'TO DO : Please comment this block when OT Rate and PH Rate are removed/disabled on Activity Master Add/Edit Screen.
            'Dim objPPA As New clsPayActivity_Tran
            'Dim ds As DataSet = objPPA.GetList("List", , , mintActivityunkid.ToString, , , False, False)
            'Dim blnUpdate As Boolean = False

            'For Each dsRow As DataRow In ds.Tables(0).Rows
            '    objPPA = New clsPayActivity_Tran
            '    objPPA._Payactivitytranunkid = CInt(dsRow.Item("payactivitytranunkid"))
            '    If objPPA._Normal_Factor <> 0 Then
            '        If objPPA._Normal_Factor <> msinNormal_Otfactor Then
            '            blnUpdate = True
            '        Else
            '            blnUpdate = False
            '        End If
            '        objPPA._Normal_Factor = msinNormal_Otfactor
            '    End If
            '    If objPPA._PH_Factor <> 0 Then
            '        If objPPA._Normal_Factor <> msinNormal_Otfactor Then
            '            blnUpdate = True
            '        Else
            '            blnUpdate = False
            '        End If
            '        objPPA._PH_Factor = msinPh_Otfactor
            '    End If

            '    If blnUpdate = True Then
            '        If objPPA.Update(objDataOperation) = False Then
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '        End If
            '    End If
            'Next
            'Sohail (22 Aug 2014) -- End

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            Dim objTransactionHead As New clsTransactionHead
            Dim intHeadID As Integer = 0


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'NORMAL AMOUNT HEAD
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, mintActivityunkid, intHeadID) = False Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, mintActivityunkid, intHeadID, objDataOperation) = False Then
                'Sohail (03 May 2018) -- End
                objTransactionHead._Trnheadcode = mstrCode
                objTransactionHead._Trnheadname = mstrName
                objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
                objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY
                objTransactionHead._Calctype_Id = enCalcType.PAY_PER_ACTIVITY
                objTransactionHead._Isrecurrent = True
                objTransactionHead._Isappearonpayslip = mblnIsappearonpayslip
                objTransactionHead._Computeon_Id = 0
                objTransactionHead._Formula = ""
                objTransactionHead._Formulaid = ""

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTransactionHead._Userunkid = User._Object._Userunkid
                objTransactionHead._Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End

                objTransactionHead._Istaxable = mblnIstaxable
                objTransactionHead._Istaxrelief = False
                objTransactionHead._Iscumulative = False
                objTransactionHead._Isnoncashbenefit = False
                objTransactionHead._Ismonetary = True
                objTransactionHead._Isactive = True
                objTransactionHead._Activityunkid = mintActivityunkid
                objTransactionHead._Isbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
                'Hemant (23 Oct 2020) -- Start
                objTransactionHead._CostCenterUnkId = 0
                'Hemant (23 Oct 2020) -- End
                'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                objTransactionHead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
                'Sohail (03 Dec 2020) -- End
 'Sohail (11 Dec 2020) -- Start
                'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                objTransactionHead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
                'Sohail (11 Dec 2020) -- End
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY) = False Then
                If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = intHeadID
                objTransactionHead._Tranheadunkid(strDatabaseName) = intHeadID
                'Sohail (21 Aug 2015) -- End
                objTransactionHead._Trnheadcode = mstrCode
                objTransactionHead._Trnheadname = mstrName
                objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
                objTransactionHead._Isappearonpayslip = mblnIsappearonpayslip
                objTransactionHead._Istaxable = mblnIstaxable

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTransactionHead._Userunkid = User._Object._Userunkid
                objTransactionHead._Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End



                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Update(enCalcType.PAY_PER_ACTIVITY) = False Then
                If objTransactionHead.Update(strDatabaseName, enCalcType.PAY_PER_ACTIVITY, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            End If

            'OT AMOUNT HEAD
            intHeadID = 0
            objTransactionHead = New clsTransactionHead

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, mintActivityunkid, intHeadID) = False Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, mintActivityunkid, intHeadID, objDataOperation) = False Then
                'Sohail (03 May 2018) -- End
                objTransactionHead._Trnheadcode = mstrCode & " OT"
                objTransactionHead._Trnheadname = mstrName & " OT"
                objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
                objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY
                objTransactionHead._Calctype_Id = enCalcType.PAY_PER_ACTIVITY_OT
                objTransactionHead._Isrecurrent = True
                objTransactionHead._Isappearonpayslip = mblnIsappearOTonpayslip
                objTransactionHead._Computeon_Id = 0
                objTransactionHead._Formula = ""
                objTransactionHead._Formulaid = ""

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTransactionHead._Userunkid = User._Object._Userunkid
                objTransactionHead._Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End

                objTransactionHead._Istaxable = mblnIstaxableOT
                objTransactionHead._Istaxrelief = False
                objTransactionHead._Iscumulative = False
                objTransactionHead._Isnoncashbenefit = False
                objTransactionHead._Ismonetary = True
                objTransactionHead._Isactive = True
                objTransactionHead._Activityunkid = mintActivityunkid
                objTransactionHead._Isbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
                'Hemant (23 Oct 2020) -- Start
                objTransactionHead._CostCenterUnkId = 0
                'Hemant (23 Oct 2020) -- End

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END
'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                objTransactionHead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
                'Sohail (03 Dec 2020) -- End
                'Sohail (11 Dec 2020) -- Start
                'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                objTransactionHead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
                'Sohail (11 Dec 2020) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_OT) = False Then
                If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_OT, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = intHeadID
                objTransactionHead._Tranheadunkid(strDatabaseName) = intHeadID
                'Sohail (21 Aug 2015) -- End
                objTransactionHead._Trnheadcode = mstrCode & " OT"
                objTransactionHead._Trnheadname = mstrName & " OT"
                objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
                objTransactionHead._Isappearonpayslip = mblnIsappearOTonpayslip
                objTransactionHead._Istaxable = mblnIstaxableOT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTransactionHead._Userunkid = User._Object._Userunkid
                objTransactionHead._Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End



                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Update(enCalcType.PAY_PER_ACTIVITY) = False Then
                If objTransactionHead.Update(strDatabaseName, enCalcType.PAY_PER_ACTIVITY, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            End If

            'PH AMOUNT HEAD
            intHeadID = 0
            objTransactionHead = New clsTransactionHead

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, mintActivityunkid, intHeadID) = False Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, mintActivityunkid, intHeadID, objDataOperation) = False Then
                'Sohail (03 May 2018) -- End
                objTransactionHead._Trnheadcode = mstrCode & " PH"
                'Hemant (27 July 2018) -- Start
                'Enhancement : To Stop Insert Trnheadcode value into Trnheadname Column
                'objTransactionHead._Trnheadname = mstrCode & " PH"
                objTransactionHead._Trnheadname = mstrName & " PH"
                'Hemant (27 July 2018) -- End
                objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
                objTransactionHead._Typeof_id = enTypeOf.PAY_PER_ACTIVITY
                objTransactionHead._Calctype_Id = enCalcType.PAY_PER_ACTIVITY_PH
                objTransactionHead._Isrecurrent = True
                objTransactionHead._Isappearonpayslip = mblnIsappearPHonpayslip
                objTransactionHead._Computeon_Id = 0
                objTransactionHead._Formula = ""
                objTransactionHead._Formulaid = ""

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTransactionHead._Userunkid = User._Object._Userunkid
                objTransactionHead._Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End

                objTransactionHead._Istaxable = mblnIstaxablePH
                objTransactionHead._Istaxrelief = False
                objTransactionHead._Iscumulative = False
                objTransactionHead._Isnoncashbenefit = False
                objTransactionHead._Ismonetary = True
                objTransactionHead._Isactive = True
                objTransactionHead._Activityunkid = mintActivityunkid
                objTransactionHead._Isbasicsalaryasotherearning = False 'Sohail (09 Sep 2017)
                'Hemant (23 Oct 2020) -- Start
                objTransactionHead._CostCenterUnkId = 0
                'Hemant (23 Oct 2020) -- End
'Sohail (03 Dec 2020) -- Start
                'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                objTransactionHead._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
                'Sohail (03 Dec 2020) -- End
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (11 Dec 2020) -- Start
                'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                objTransactionHead._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
                'Sohail (11 Dec 2020) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_PH) = False Then
                If objTransactionHead.Insert(enCalcType.PAY_PER_ACTIVITY_PH, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTransactionHead._Tranheadunkid = intHeadID
                objTransactionHead._Tranheadunkid(strDatabaseName) = intHeadID
                'Sohail (21 Aug 2015) -- End
                objTransactionHead._Trnheadcode = mstrCode & " PH"
                'Hemant (27 July 2018) -- Start
                'Enhancement : To Stop Insert Trnheadcode value into Trnheadname Column
                'objTransactionHead._Trnheadname = mstrCode & " PH"
                objTransactionHead._Trnheadname = mstrName & " PH"
                'Hemant (27 July 2018) -- End
                objTransactionHead._Trnheadtype_Id = mintTranheadTypeId
                objTransactionHead._Isappearonpayslip = mblnIsappearPHonpayslip
                objTransactionHead._Istaxable = mblnIstaxablePH

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTransactionHead._Userunkid = User._Object._Userunkid
                objTransactionHead._Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End



                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Update(enCalcType.PAY_PER_ACTIVITY) = False Then
                If objTransactionHead.Update(strDatabaseName, enCalcType.PAY_PER_ACTIVITY, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    If objTransactionHead._Message <> "" Then
                        eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                    End If
                    Return False
                End If
            End If
            'Sohail (17 Sep 2014) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (practivity_master) </purpose>
    Public Function Delete(ByVal strDatabaseName As String, ByVal intUnkid As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) - [intUserunkid,dtCurrentDateAndTime]
        'Sohail (21 Aug 2015) - [strDatabaseName]
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, You cannot deactivate this activity. Reason : This activity is already linked with some transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE practivity_master SET isactive = 0 " & _
                     "WHERE activityunkid = @activityunkid "

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "practivity_master", "activityunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            Dim objTransactionHead As New clsTransactionHead
            Dim intHeadID As Integer = 0


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'NORMAL AMOUNT HEAD
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, intUnkid, intHeadID) = True Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY, intUnkid, intHeadID, objDataOperation) = True Then
                'Sohail (03 May 2018) -- End

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END
          
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Void(intHeadID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objTransactionHead.Void(strDatabaseName, intHeadID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then
                If objTransactionHead.Void(strDatabaseName, intHeadID, intUserunkid, dtCurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then
                    'Shani(24-Aug-2015) -- End

                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                End If
            End If

            'OT AMOUNT HEAD
            intHeadID = 0
            objTransactionHead = New clsTransactionHead
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, intUnkid, intHeadID) = True Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_OT, intUnkid, intHeadID, objDataOperation) = True Then
                'Sohail (03 May 2018) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Void(intHeadID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objTransactionHead.Void(strDatabaseName, intHeadID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTransactionHead._FormName = mstrFormName
            objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
            objTransactionHead._ClientIP = mstrClientIP
            objTransactionHead._HostName = mstrHostName
            objTransactionHead._FromWeb = mblnIsWeb
            objTransactionHead._AuditUserId = mintAuditUserId
            objTransactionHead._CompanyUnkid = mintCompanyUnkid
            objTransactionHead._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

                If objTransactionHead.Void(strDatabaseName, intHeadID, intUserunkid, dtCurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then
                    'Shani(24-Aug-2015) -- End

                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                End If
            End If

            'PH AMOUNT HEAD
            intHeadID = 0
            objTransactionHead = New clsTransactionHead
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, intUnkid, intHeadID) = True Then
            If objTransactionHead.isExistActivityHead(enCalcType.PAY_PER_ACTIVITY_PH, intUnkid, intHeadID, objDataOperation) = True Then
                'Sohail (03 May 2018) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTransactionHead.Void(intHeadID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTransactionHead._FormName = mstrFormName
                objTransactionHead._LoginEmployeeunkid = mintLoginEmployeeunkid
                objTransactionHead._ClientIP = mstrClientIP
                objTransactionHead._HostName = mstrHostName
                objTransactionHead._FromWeb = mblnIsWeb
                objTransactionHead._AuditUserId = mintAuditUserId
                objTransactionHead._CompanyUnkid = mintCompanyUnkid
                objTransactionHead._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objTransactionHead.Void(strDatabaseName, intHeadID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then
                If objTransactionHead.Void(strDatabaseName, intHeadID, intUserunkid, dtCurrentDateAndTime, Language.getMessage(mstrModuleName, 111, "PPA Deleted")) = False Then
                    'Shani(24-Aug-2015) -- End

                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                End If
            End If
            'Sohail (17 Sep 2014) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'activityunkid' AND TABLE_NAME <> 'practivity_master' AND TABLE_NAME NOT LIKE 'at%' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim iRowCnt As Integer = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If dRow.Item("TABLE_NAME").ToString.ToUpper = "PRPAYACTIVITY_TRAN" Then
                        strQ = "SELECT * FROM " & dRow.Item("TABLE_NAME") & " WHERE " & dRow.Item("COLUMN_NAME") & " = '" & intUnkid & "' AND isvoid = 0 "
                    ElseIf dRow.Item("TABLE_NAME").ToString.ToUpper = "PRPAYACTIVITY_SUMMARY" Then
                        strQ = "SELECT * FROM " & dRow.Item("TABLE_NAME") & " WHERE " & dRow.Item("COLUMN_NAME") & " = '" & intUnkid & "' "
                        'Sohail (17 Sep 2014) -- Start
                        'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                    ElseIf dRow.Item("TABLE_NAME").ToString.ToUpper = "PRACTIVITYRATE_MASTER" Then
                        strQ = "SELECT * FROM " & dRow.Item("TABLE_NAME") & " WHERE " & dRow.Item("COLUMN_NAME") & " = '" & intUnkid & "' AND isactive = 1 "
                    ElseIf dRow.Item("TABLE_NAME").ToString.ToUpper = "PRTRANHEAD_MASTER" Then
                        Continue For 'Do Nothing as it is System Generated Head for PPA
                        'Sohail (17 Sep 2014) -- End
                        'Sohail (30 Jun 2017) -- Start
                        'Issue - 68.1 - Not able to delete PPA activity if that same id is used in employee budget timesheet activity.
                    ElseIf dRow.Item("TABLE_NAME").ToString.ToUpper = "LTBEMPLOYEE_TIMESHEET" OrElse dRow.Item("TABLE_NAME").ToString.ToUpper = "TSEMPTIMESHEET_APPROVAL" Then
                        Continue For 'Do Nothing as it is budget activity unkid
                        'Sohail (30 Jun 2017) -- End
                    Else
                        strQ = "SELECT * FROM " & dRow.Item("TABLE_NAME") & " WHERE " & dRow.Item("COLUMN_NAME") & " = '" & intUnkid & "' "
                    End If

                    iRowCnt = objDataOperation.RecordCount(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If iRowCnt > 0 Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Procedure Name: isUsed; Module Name: ", mstrModuleName)
            Return True
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                         "  activityunkid " & _
                         ", code " & _
                         ", name " & _
                         ", measureunkid " & _
                         ", trnheadtype_id " & _
                         ", description " & _
                         ", name1 " & _
                         ", name2 " & _
                         ", isactive " & _
                         ", isdeleted " & _
                     "FROM practivity_master " & _
                     "WHERE isactive = 1 "
            If strCode.Trim.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Trim.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND activityunkid <> @activityunkid"
                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose>Get List Of Data For Combo</purpose>
    Public Function getComboList(ByVal StrLst As String, Optional ByVal blnFlag As Boolean = False, Optional ByVal iMeasureId As Integer = 0, Optional ByVal iTranHeadTypeId As Integer = 0, _
                                 Optional ByVal strOrderByQuery As String = "") As DataSet
        'Hemant (23 May 2019) -- [strOrderByQuery] 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If blnFlag = True Then
                strQ = "SELECT ' ' AS code, @Select AS name, 0 AS activityunkid UNION "
            End If
            strQ &= "SELECT code, name, activityunkid FROM practivity_master WHERE isactive = 1 "

            If iMeasureId > 0 Then
                strQ &= " AND measureunkid = @measureunkid "
                objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iMeasureId)
            End If

            If iTranHeadTypeId > 0 Then
                strQ &= " AND trnheadtype_id = @trnheadtype_id "
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, iTranHeadTypeId)
            End If

            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            If strOrderByQuery.Trim <> "" Then
                strQ &= strOrderByQuery
            End If
            'Hemant (23 May 2019) -- End

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, StrLst)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <param name="StrLst">Table Name</param>
    ''' <param name="iMeasureId">Get Result of Particular Measure</param>
    ''' <param name="iUoMTypeId">0 = All Measure, 1 = Non Formula Based, 2 = Formula Based</param>
    ''' <purpose>Get List Of Data For Grid</purpose>
    Public Function getGridList(ByVal StrLst As String, Optional ByVal iMeasureId As Integer = 0, Optional ByVal iUoMTypeId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ &= "SELECT CAST(0 AS BIT) AS ischeck, practivity_master.code, practivity_master.name, practivity_master.activityunkid,is_ot_activity FROM practivity_master " & _
                        " JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
                        "WHERE practivity_master.isactive = 1 "

            Select Case iUoMTypeId
                Case 1 'Non Formula Based
                    strQ &= " AND prunitmeasure_master.isformula_based = 0 "
                Case 2 'Formula Based
                    strQ &= " AND prunitmeasure_master.isformula_based = 1 "
            End Select

            If iMeasureId > 0 Then
                strQ &= " AND practivity_master.measureunkid = @measureunkid "
                objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iMeasureId)
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, StrLst)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>    
    ''' <purpose>Check If Selected Head is  already mapped with other activity</purpose>
    Public Function Is_HeadMapped(ByVal iTranHeadId As Integer, ByVal iTranId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim sMessage As String = String.Empty

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT code,name FROM practivity_master WHERE tranheadunkid = '" & iTranHeadId & "' AND isactive = 1 " & _
                     " AND activityunkid <> '" & iTranId & "' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                sMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot map selected transaction head. Reason selected transaction head is already mapped with [ ") & dsList.Tables(0).Rows(0).Item("name") & _
                Language.getMessage(mstrModuleName, 5, " ] activity. Please select another transaction head.")
            End If

            Return sMessage
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose>Get Date and Day Name For the Given Month  and Year</purpose>
    Public Function Get_Month_Days(ByVal iMonthId As Integer, ByVal iYearId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                        " CONVERT(CHAR(8),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number,112) AS MDate " & _
                        " ,SUBSTRING(DATENAME(dw, CONVERT(CHAR(8),CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number,112)),0,4) AS DName " & _
                        "FROM master..spt_values WHERE type = 'P' " & _
                        "AND (CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) + Number ) < DATEADD(mm,1,CAST(CAST(@year AS VARCHAR) + '-' + CAST(@Month AS VARCHAR) + '-01' AS DATETIME) ) "

            objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, iMonthId)
            objDataOperation.AddParameter("@year", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose>Get Activity Id By Passing Activity Name</purpose>
    Public Function Get_Activity_Id(ByVal iActivityName As String, ByVal iActivityCode As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Pinkal (16-Jun-2015) -- Start
            'Enhancement - WORKING ON PPA Enhancement Given By Anjan sir.

            strQ = "SELECT " & _
                   " activityunkid " & _
                   "FROM practivity_master " & _
                   "WHERE  isactive = 1 "

            If iActivityName.Trim.Length > 0 Then
                strQ &= " AND name = @name"
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iActivityName)
            End If

            If iActivityCode.Trim.Length > 0 Then
                strQ &= " AND code = @code"
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iActivityCode)
            End If

            'Pinkal (16-Jun-2015) -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("activityunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Activity_Id", mstrModuleName)
        Finally
        End Try
    End Function

    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function Is_FormulaBased(ByVal intActivityUnkId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "SELECT  ISNULL(isformula_based, 0) AS isformula_based " & _
                    "FROM    practivity_master " & _
                            "LEFT JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
                    "WHERE   practivity_master.activityunkid = @activityunkid "

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CBool(dsList.Tables(0).Rows(0).Item("isformula_based"))
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_FormulaBased; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function

    Public Function GetActivityIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.PAY_PER_ACTIVITY)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActivityIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function


    Public Function GetActivityIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.PAY_PER_ACTIVITY)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActivityIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (21 Jun 2013) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This activity name is already defined. Please define new activity name.")
			Language.setMessage("clsMasterData", 2, "Earning for Employee")
			Language.setMessage("clsMasterData", 3, "Deduction From Employee")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot map selected transaction head. Reason selected transaction head is already mapped with [")
			Language.setMessage(mstrModuleName, 5, " ] activity. Please select another transaction head.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot deactivate this activity. Reason : This activity is already linked with some transactions.")
			Language.setMessage(mstrModuleName, 2, "This activity code is already defined. Please define new activity code.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class