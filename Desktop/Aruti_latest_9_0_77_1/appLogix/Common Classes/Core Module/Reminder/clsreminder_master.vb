﻿'************************************************************************************************************************************
'Class Name : clsreminder_master.vb
'Purpose    : All Reminder Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :29/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 2
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 

Public Enum PriorityType
    Low = 1
    Medium = 2
    High = 3
End Enum

Public Enum IntervalType
    Daily = 1
    Weekly = 2
    Monthly = 3
    Quarter = 4
    Yearly = 5
End Enum

Public Class clsreminder_master
    Private Shared ReadOnly mstrModuleName As String = "clsreminder_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintReminderunkid As Integer
    Private mdtStart_Date As Date
    Private mstrTitle As String = String.Empty
    Private mstrReminderMessage As String = String.Empty
    'Sandeep [ 21 Aug 2010 ] -- Start
    'Private mstrReminder_Type As String = String.Empty
    Private mintReminder_Type As Integer
    Private mstrUserUnkIDs As String = "0"
    'Sandeep [ 21 Aug 2010 ] -- End 
    Private mintPriority As Integer
    Private mblnIscomplete As Boolean
    Private mintInterval_Type As Integer
    Private mintFrequency As Integer
    Private mdtStopreminder As Nullable(Of Date)
    Private mintSnooz As Integer
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reminderunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Reminderunkid() As Integer
        Get
            Return mintReminderunkid
        End Get
        Set(ByVal value As Integer)
            mintReminderunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set title
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Title() As String
        Get
            Return mstrTitle
        End Get
        Set(ByVal value As String)
            mstrTitle = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set message
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ReminderMessage() As String
        Get
            Return mstrReminderMessage
        End Get
        Set(ByVal value As String)
            mstrReminderMessage = value
        End Set
    End Property


    'Sandeep [ 21 Aug 2010 ] -- Start
    '''' <summary>
    '''' Purpose: Get or Set reminder_type
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Reminder_Type() As String
    '    Get
    '        Return mstrReminder_Type
    '    End Get
    '    Set(ByVal value As String)
    '        mstrReminder_Type = Value
    '    End Set
    'End Property
    ''' <summary>
    ''' Purpose: Get or Set reminder_type
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Reminder_Type() As Integer
        Get
            Return mintReminder_Type
        End Get
        Set(ByVal value As Integer)
            mintReminder_Type = value
        End Set
    End Property

    Public Property _UserUnkIds() As String
        Get
            Return mstrUserUnkIDs
        End Get
        Set(ByVal value As String)
            mstrUserUnkIDs = value
        End Set
    End Property
    'Sandeep [ 21 Aug 2010 ] -- End

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscomplete
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Iscomplete() As Boolean
        Get
            Return mblnIscomplete
        End Get
        Set(ByVal value As Boolean)
            mblnIscomplete = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interval_type
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Interval_Type() As Integer
        Get
            Return mintInterval_Type
        End Get
        Set(ByVal value As Integer)
            mintInterval_Type = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set frequency
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Frequency() As Integer
        Get
            Return mintFrequency
        End Get
        Set(ByVal value As Integer)
            mintFrequency = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stopreminder
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stopreminder() As Nullable(Of Date)
        Get
            Return mdtStopreminder
        End Get
        Set(ByVal value As Nullable(Of Date))
            mdtStopreminder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set snooz
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Snooz() As Integer
        Get
            Return mintSnooz
        End Get
        Set(ByVal value As Integer)
            mintSnooz = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  reminderunkid " & _
              ", start_date " & _
              ", title " & _
              ", message " & _
              ", reminder_type " & _
              ", priority " & _
              ", iscomplete " & _
              ", interval_type " & _
              ", frequency " & _
              ", stopreminder " & _
              ", snooz " & _
              ", userunkid " & _
              ", isactive " & _
             "FROM crreminder_master " & _
             "WHERE reminderunkid = @reminderunkid "

            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminderunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintReminderunkid = CInt(dtRow.Item("reminderunkid"))
                mdtStart_Date = dtRow.Item("start_date")
                mstrTitle = dtRow.Item("title").ToString
                mstrReminderMessage = dtRow.Item("message").ToString
                'Sandeep [ 21 Aug 2010 ] -- Start
                'mstrReminder_Type = dtRow.Item("reminder_type").ToString
                mintReminder_Type = CInt(dtRow.Item("reminder_type"))
                'Sandeep [ 21 Aug 2010 ] -- End 
                mintPriority = CInt(dtRow.Item("priority"))
                mblnIscomplete = CBool(dtRow.Item("iscomplete"))
                mintInterval_Type = CInt(dtRow.Item("interval_type"))
                mintFrequency = CInt(dtRow.Item("frequency"))
                If Not dtRow.Item("stopreminder") Is DBNull.Value And Not (dtRow.Item("stopreminder") Is Nothing) Then
                    mdtStopreminder = dtRow.Item("stopreminder")
                End If
                mintSnooz = CInt(dtRow.Item("snooz"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next


            'Sandeep [ 21 Aug 2010 ] -- Start
            mstrUserUnkIDs = "0"
            strQ = "SELECT destination_userunkid " & _
                        "FROM crreminder_tran " & _
                    "WHERE reminderunkid = @reminderunkid "

            dsList = objDataOperation.ExecQuery(strQ, "UserList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("UserList").Rows
                mstrUserUnkIDs &= ", " & dtRow.Item("destination_userunkid")
            Next
            'Sandeep [ 21 Aug 2010 ] -- End 


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sandeep [ 23 Oct 2010 ] -- Start
            'strQ = "SELECT " & _
            '            "	 crreminder_master.reminderunkid " & _
            '            "	,crreminder_master.start_date " & _
            '            "	,crreminder_master.title " & _
            '            "	,crreminder_master.message " & _
            '            "	,ISNULL(cfcommon_master.name,'')AS reminder_type " & _
            '            "	,crreminder_master.reminder_type AS reminder_typeid " & _
            '            "	,crreminder_master.priority " & _
            '  ", case when isnull(iscomplete,0) = 1 then 'True' else 'False' end as iscomplete " & _
            '            "	,crreminder_master.interval_type " & _
            '            "	,crreminder_master.frequency " & _
            '            "	,crreminder_master.stopreminder " & _
            '            "	,crreminder_master.snooz " & _
            '            "	,crreminder_master.userunkid " & _
            '            "	,crreminder_master.isactive " & _
            '            "FROM crreminder_master " & _
            '            "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = crreminder_master.reminder_type AND mastertype =  " & enCommonMaster.REMINDER_TYPE

            strQ = "SELECT " & _
                   "	 crreminder_master.reminderunkid " & _
                   "	,crreminder_master.start_date " & _
                   "	,crreminder_master.title " & _
                   "	,crreminder_master.message " & _
                       ",ISNULL(hrmsConfiguration..cfremindertype_master.name,'')AS reminder_type " & _
                   "	,crreminder_master.reminder_type AS reminder_typeid " & _
                   "	,crreminder_master.priority " & _
                   ", case when isnull(iscomplete,0) = 1 then 'True' else 'False' end as iscomplete " & _
                   "	,crreminder_master.interval_type " & _
                   "	,crreminder_master.frequency " & _
                   "	,crreminder_master.stopreminder " & _
                   "	,crreminder_master.snooz " & _
                   "	,crreminder_master.userunkid " & _
                   "	,crreminder_master.isactive " & _
                   "FROM crreminder_master " & _
                       "LEFT JOIN hrmsConfiguration..cfremindertype_master ON hrmsConfiguration..cfremindertype_master.remindertypeunkid = crreminder_master.reminder_type "

            'Sandeep [ 23 Oct 2010 ] -- End 
            If blnOnlyActive Then
                strQ &= " WHERE crreminder_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (crreminder_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrTitle) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Reminder Name is already defined. Please define new Reminder Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Sandeep [ 21 Aug 2010 ] -- Start
        objDataOperation.BindTransaction()
        'Sandeep [ 21 Aug 2010 ] -- End 


        Try
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            objDataOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTitle.ToString)
            objDataOperation.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReminderMessage.ToString)
            'Sandeep [ 21 Aug 2010 ] -- Start
            'objDataOperation.AddParameter("@reminder_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReminder_Type.ToString)
            objDataOperation.AddParameter("@reminder_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminder_Type.ToString)
            'Sandeep [ 21 Aug 2010 ] -- End 
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)
            objDataOperation.AddParameter("@interval_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterval_Type.ToString)
            objDataOperation.AddParameter("@frequency", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFrequency.ToString)
            objDataOperation.AddParameter("@stopreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStopreminder.HasValue And Not (mdtStopreminder Is Nothing), mdtStopreminder, DBNull.Value))
            objDataOperation.AddParameter("@snooz", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSnooz.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO crreminder_master ( " & _
              "  start_date " & _
              ", title " & _
              ", message " & _
              ", reminder_type " & _
              ", priority " & _
              ", iscomplete " & _
              ", interval_type " & _
              ", frequency " & _
              ", stopreminder " & _
              ", snooz " & _
              ", userunkid " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @start_date " & _
              ", @title " & _
              ", @message " & _
              ", @reminder_type " & _
              ", @priority " & _
              ", @iscomplete " & _
              ", @interval_type " & _
              ", @frequency " & _
              ", @stopreminder " & _
              ", @snooz " & _
              ", @userunkid " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintReminderunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sandeep [ 21 Aug 2010 ] -- Start


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'strQ = "INSERT INTO crreminder_tran ( " & _
            '                       "  reminderunkid " & _
            '                       ", due_datetime " & _
            '                       ", destination_userunkid " & _
            '                       ", isread " & _
            '                   ") VALUES ( " & _
            '                       "  @reminderunkid " & _
            '                       ", @start_date " & _
            '                       ", @destination_userunkid " & _
            '                       ", @isread " & _
            '                   ")"
            'objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminderunkid)

            strQ = "INSERT INTO crreminder_tran ( " & _
                                   "  reminderunkid " & _
                                   ", due_datetime " & _
                                   ", destination_userunkid " & _
                                   ", isread " & _
                               ") VALUES ( " & _
                                   "  @reminderunkid " & _
                                   ", @start_date " & _
                                   ", @destination_userunkid " & _
                                   ", @isread " & _
                   ") ; SELECT @@identity "
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminderunkid)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            If mstrUserUnkIDs.Trim.Length > 0 Then
            For Each str As String In mstrUserUnkIDs.Split(",")
                If Val(str) <= 0 Then Continue For

                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminderunkid)
                    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 
                objDataOperation.RemoveParameters("@destination_userunkid")
                objDataOperation.RemoveParameters("@isread")
                objDataOperation.AddParameter("@destination_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, str)
                objDataOperation.AddParameter("@isread", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)


                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    'objDataOperation.ExecNonQuery(strQ)
                    Dim dsTran As DataSet = objDataOperation.ExecQuery(strQ, "List")
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "crreminder_master", "reminderunkid", mintReminderunkid, "crreminder_tran", "remindertranunkid", dsTran.Tables(0).Rows(0)(0), 1, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

            Next
            Else
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "crreminder_master", "reminderunkid", mintReminderunkid, "crreminder_tran", "remindertranunkid", -1, 1, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.ReleaseTransaction(True)
            'Sandeep [ 21 Aug 2010 ] -- End 

            Return True

        Catch ex As Exception
            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 21 Aug 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (crreminder_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrTitle, mintReminderunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Reminder Name is already defined. Please define new Reminder Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Sandeep [ 21 Aug 2010 ] -- Start
        objDataOperation.BindTransaction()
        'Sandeep [ 21 Aug 2010 ] -- End 


        Try
            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminderunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTitle.ToString)
            objDataOperation.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReminderMessage.ToString)
            'Sandeep [ 21 Aug 2010 ] -- Start
            'objDataOperation.AddParameter("@reminder_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReminder_Type.ToString)
            objDataOperation.AddParameter("@reminder_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminder_Type.ToString)
            'Sandeep [ 21 Aug 2010 ] -- End 
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)
            objDataOperation.AddParameter("@interval_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterval_Type.ToString)
            objDataOperation.AddParameter("@frequency", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFrequency.ToString)
            objDataOperation.AddParameter("@stopreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStopreminder.HasValue And Not (mdtStopreminder Is Nothing), mdtStopreminder, DBNull.Value))
            objDataOperation.AddParameter("@snooz", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSnooz.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE crreminder_master SET " & _
              "  start_date = @start_date" & _
              ", title = @title" & _
              ", message = @message" & _
              ", reminder_type = @reminder_type" & _
              ", priority = @priority" & _
              ", iscomplete = @iscomplete" & _
              ", interval_type = @interval_type" & _
              ", frequency = @frequency" & _
              ", stopreminder = @stopreminder" & _
              ", snooz = @snooz" & _
              ", userunkid = @userunkid" & _
              ", isactive = @isactive " & _
            "WHERE reminderunkid = @reminderunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Sandeep [ 21 Aug 2010 ] -- Start


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'strQ = "DELETE FROM crreminder_tran " & _
            '          "WHERE reminderunkid = @reminderunkid "

            'Call objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'strQ = "INSERT INTO crreminder_tran ( " & _
            '           "  reminderunkid " & _
            '           ", due_datetime " & _
            '           ", destination_userunkid " & _
            '           ", isread " & _
            '       ") VALUES ( " & _
            '           "  @reminderunkid " & _
            '           ", @start_date " & _
            '           ", @destination_userunkid " & _
            '           ", @isread " & _
            '       ") "

            'For Each str As String In mstrUserUnkIDs.Split(",")

            '    If Val(str) <= 0 Then Continue For

            '    objDataOperation.RemoveParameters("@destination_userunkid")
            '    objDataOperation.RemoveParameters("@isread")
            '    objDataOperation.AddParameter("@destination_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, str)
            '    objDataOperation.AddParameter("@isread", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

            '    objDataOperation.ExecNonQuery(strQ)
            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'Next

            Dim dtTemp() As DataRow
            Dim objCommonATLog = New clsCommonATLog
            Dim dsTemp As DataSet = objCommonATLog.GetChildList(objDataOperation, "crreminder_tran", "reminderunkid", mintReminderunkid)
            objCommonATLog = Nothing
            If mstrUserUnkIDs.Trim.Length > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    dtTemp = dsTemp.Tables(0).Select("destination_userunkid NOT IN(" & mstrUserUnkIDs & ")")
                    If dtTemp.Length > 0 Then
                        For i As Integer = 0 To dtTemp.Length - 1
                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "crreminder_master", "reminderunkid", mintReminderunkid, "crreminder_tran", "remindertranunkid", dtTemp(i)("remindertranunkid"), 2, 3) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                            strQ = "DELETE FROM crreminder_tran WHERE remindertranunkid = '" & dtTemp(i)("remindertranunkid") & "'"
                            Call objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            dsTemp.Tables(0).Rows.Remove(dtTemp(i))
                        Next
                    End If
                    dsTemp.Tables(0).AcceptChanges()
                End If
            Else
                For Each dtRow As DataRow In dsTemp.Tables(0).Rows
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "crreminder_master", "reminderunkid", mintReminderunkid, "crreminder_tran", "remindertranunkid", dtRow.Item("remindertranunkid"), 2, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
            strQ = "DELETE FROM crreminder_tran " & _
                      "WHERE reminderunkid = '" & mintReminderunkid & "'"

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            End If

            



         

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'strQ = "INSERT INTO crreminder_tran ( " & _
            '                       "  reminderunkid " & _
            '                       ", due_datetime " & _
            '                       ", destination_userunkid " & _
            '                       ", isread " & _
            '                   ") VALUES ( " & _
            '                       "  @reminderunkid " & _
            '                       ", @start_date " & _
            '                       ", @destination_userunkid " & _
            '                       ", @isread " & _
            '                   ")"

            strQ = "INSERT INTO crreminder_tran ( " & _
                       "  reminderunkid " & _
                       ", due_datetime " & _
                       ", destination_userunkid " & _
                       ", isread " & _
                   ") VALUES ( " & _
                       "  @reminderunkid " & _
                       ", @start_date " & _
                       ", @destination_userunkid " & _
                       ", @isread " & _
                   ") ; SELECT @@identity "
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            If mstrUserUnkIDs.Trim.Length > 0 Then
            For Each str As String In mstrUserUnkIDs.Split(",")
                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    If dsTemp.Tables(0).Rows.Count > 0 Then
                        dtTemp = dsTemp.Tables(0).Select("destination_userunkid = '" & str & "'")
                        If dtTemp.Length > 0 Then Continue For
                    End If
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReminderunkid)
                    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 
                objDataOperation.RemoveParameters("@destination_userunkid")
                objDataOperation.RemoveParameters("@isread")
                objDataOperation.AddParameter("@destination_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, str)
                objDataOperation.AddParameter("@isread", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    'objDataOperation.ExecNonQuery(strQ)
                    Dim dsTran As DataSet = objDataOperation.ExecQuery(strQ, "List")
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 


                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "crreminder_master", "reminderunkid", mintReminderunkid, "crreminder_tran", "remindertranunkid", dsTran.Tables(0).Rows(0)(0), 2, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


            Next
            End If
            
            'S.SANDEEP [ 12 OCT 2011 ] -- END 



            objDataOperation.ReleaseTransaction(True)
            'Sandeep [ 21 Aug 2010 ] -- End 

            Return True
        Catch ex As Exception
            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 21 Aug 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (crreminder_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete selected Reminder. Reason: This Reminder is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim objCommonATLog As New clsCommonATLog
            Dim dsTemp As DataSet = objCommonATLog.GetChildList(objDataOperation, "crreminder_tran", "reminderunkid", intUnkid)
            objCommonATLog = Nothing

            For Each dtRow As DataRow In dsTemp.Tables(0).Rows
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "crreminder_master", "reminderunkid", intUnkid, "crreminder_tran", "remindertranunkid", dtRow.Item("remindertranunkid"), 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            Next

            strQ = "DELETE FROM crreminder_master " & _
            "WHERE reminderunkid = @reminderunkid "

            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  reminderunkid " & _
              ", start_date " & _
              ", title " & _
              ", message " & _
              ", reminder_type " & _
              ", priority " & _
              ", iscomplete " & _
              ", interval_type " & _
              ", frequency " & _
              ", stopreminder " & _
              ", snooz " & _
              ", userunkid " & _
              ", isactive " & _
             "FROM crreminder_master " & _
             "WHERE title = @title "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If intUnkid > 0 Then
                strQ &= " AND reminderunkid <> @reminderunkid"
            End If

            objDataOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sandeep [ 21 Aug 2010 ] -- Start
    Public Sub SnoozRemainder(ByVal intReminderUnkId As Integer, _
                                ByVal intDestination_UserUnkId As Integer, _
                                ByVal dtDueDateTime As Date)

        Dim strQ As String
        Dim exForce As Exception
        Dim intUnkId As Integer = 0
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "UPDATE crreminder_tran " & _
                        "SET due_datetime = @due_datetime " & _
                    "WHERE reminderunkid = @reminderunkid " & _
                    "AND destination_userunkid = @destination_userunkid "

            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReminderUnkId)
            objDataOperation.AddParameter("@destination_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDestination_UserUnkId)
            objDataOperation.AddParameter("@due_datetime", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDueDateTime) & " " & dtDueDateTime.ToString("HH:mm:ss"))

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SnoozRemainder", mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

    Public Sub MarkAsRead(ByVal intReminderUnkId As Integer, _
                                ByVal intDestination_UserUnkId As Integer)

        Dim strQ As String
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            objDataOperation.AddParameter("@reminderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReminderUnkId)
            objDataOperation.AddParameter("@destination_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDestination_UserUnkId)

            strQ = "UPDATE crreminder_tran " & _
                        "SET isread = 1 " & _
                    "WHERE reminderunkid = @reminderunkid " & _
                    "AND destination_userunkid = @destination_userunkid "

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE crreminder_master " & _
                       "SET iscomplete = 1 " & _
                   "WHERE reminderunkid NOT IN (SELECT reminderunkid FROM crreminder_tran " & _
                                                "WHERE isread = 0 " & _
                                                "AND reminderunkid = @reminderunkid ) " & _
                   "AND reminderunkid = @reminderunkid "

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "MarkAsRead", mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function getTodayReminder(ByVal intUserUnkId As Integer, _
                                    Optional ByVal strListName As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim strQ As String
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT crreminder_master.reminderunkid " & _
                   ", crreminder_tran.due_datetime " & _
                   ", crreminder_master.title " & _
                   ", crreminder_master.message " & _
                   ", crreminder_master.priority " & _
                   ", crreminder_tran.isread " & _
                   ", crreminder_master.frequency " & _
                   ", crreminder_master.interval_type " & _
                   ", crreminder_master.stopreminder " & _
                   "FROM crreminder_master , crreminder_tran " & _
                   "WHERE crreminder_master.reminderunkid = crreminder_tran.reminderunkid " & _
                   "AND crreminder_tran.destination_userunkid = @userunkid " & _
                   "AND CONVERT(CHAR(8), crreminder_tran.due_datetime, 112)  = @due_date " & _
                   "AND CONVERT(CHAR(8), crreminder_tran.due_datetime, 108) <= @due_time " & _
                   "AND crreminder_tran.isread = 0 "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId.ToString)
            objDataOperation.AddParameter("@due_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(Now))
            objDataOperation.AddParameter("@due_time", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, Format(Now, "HH:mm:ss"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "getTodayReminder", mstrModuleName)
            Return dsList
        Finally
            objDataOperation = Nothing
            dsList = Nothing
        End Try
    End Function
    'Sandeep [ 21 Aug 2010 ] -- End 


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Reminder Name is already defined. Please define new Reminder Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class