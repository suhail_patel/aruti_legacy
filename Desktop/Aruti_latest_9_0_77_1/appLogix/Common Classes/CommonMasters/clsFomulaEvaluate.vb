﻿Public Class clsFomulaEvaluate
    'It is not really possible to provide an EVAL statement under the .NET framework as by the time the program comes to run, the names of variables used in the string have been forgotten. I needed the same function for a graphing program so here is my solution. As well as standard expressions it will cope with a number of useful (non standard BASIC) ones such as

    '        2x² -3(xy - a) +c    and    x sin x°   (using chrs alt-0178 and alt-0176)

    'Currently it accepts the variables a, b, c, x and y and about a dozen operators and functions. I am sure you could adapt it to your own needs easily.



    Dim x As Double = 2

    Dim y As Double = 3

    Dim a As Double = 4

    Dim b As Double = 5

    Dim c As Double = 6

    Dim ops As New Stack

    Dim var As New Stack


    Public Function Eval(ByVal expr As String) As Decimal

        Dim varexpected As Boolean = True

        Dim eptr, oldeptr As Integer

        Dim type As String = ""

        Dim value As Decimal = 0 'Sohail (11 May 2011)

        Dim op As String = ""

        ops.Clear()

        var.Clear()

        eptr = 1

        Do Until eptr > Len(expr)

            oldeptr = eptr

            Parse(expr, eptr, type, op, value, varexpected)

            If type = "Invalid character" Then DoError(type, oldeptr) : Exit Function

            If varexpected And type = "Operator" Then DoError("Variable expected", oldeptr) : Exit Function

            'insert implied multiply

            If Not varexpected And type = "Open bracket" Then op = "*" : type = "Operator" : eptr -= 1

            If Not varexpected And type <> "Operator" And type <> "Close bracket" Then DoError("Operator expected", oldeptr) : Exit Function

            Select Case type

                Case "Numeric" : var.Push(value) : varexpected = False

                Case "Operator"

                    'check if operation already on stack has higher or equal priority

                    If ops.Count > 0 AndAlso Prior(ops.Peek, op) Then

                        dooperation()

                    End If

                    ops.Push(op)

                    If op = "²" Or op = "°" Then

                        'push a dummy variable onto the stack for unary operators

                        var.Push(0)

                    Else

                        varexpected = True

                    End If

                Case "Function"

                    'push a dummy variable onto the stack for unary operators

                    ops.Push(op) : var.Push(0)

                Case "Open bracket"

                    ops.Push("(")

                Case "Close bracket"

                    'do all operations up to the last leftbracket

                    Do Until ops.Count = 0 OrElse ops.Peek = "("

                        dooperation()

                    Loop

                    If ops.Count = 0 Then MsgBox("Invalid expression") : Exit Function

                    op = ops.Pop

            End Select

        Loop

        While ops.Count > 0 And var.Count > 1

            dooperation()

        End While

        If ops.Count > 0 Or var.Count <> 1 Then MsgBox("Invalid expression") : Exit Function

        Return var.Pop

    End Function

    Private Function Prior(ByVal op1, ByVal op2) As Boolean

        Return Priority(op1) >= Priority(op2)

    End Function

    Private Function Priority(ByVal op)

        Select Case op

            Case "(" : Return 0

            Case "+", "-" : Return 1

            Case "*", "/", "Mod" : Return 2
                'Hemant (04 Mar 2019) -- ["Mod"]
            Case "sin", "cos", "tan", "log", "ln", "exp", "sqr" : Return 3

            Case "^", "²", "°" : Return 4

            Case "_" : Return 5

        End Select

        Return 0

    End Function

    Private Function dooperation()

        'apply operation at the top of the ops stack to the numbers at the top of the var stack

        'and put the answer on the var stack

        Dim op As String

        Dim var1, var2 As Decimal 'Sohail (11 May 2011)

        op = ops.Pop

        var1 = var.Pop

        var2 = var.Pop

        Select Case op

            Case "+" : var1 = var2 + var1

            Case "-" : var1 = var2 - var1

            Case "*" : var1 = var2 * var1

                'Sohail (11 Sep 2010) -- Start
                'Case "/" : var1 = var2 / var1
            Case "/"
                'Sohail (08 Nov 2011) -- Start
                'var1 = IIf(var1 = 0, 0, var2 / var1)
                If var1 = 0 Then
                    var1 = 0
                Else
                    var1 = var2 / var1
                End If
                'Sohail (08 Nov 2011) -- End
                'Sohail (11 Sep 2010) -- End
                'Hemant (04 Mar 2019) -- Start
                'Internal # 32 : Provide "Modulus" function to finds the remainder after division of one number in transaction head formula.
            Case "Mod"
                If var1 = 0 Then
                    var1 = 0
                Else
                    var1 = var2 Mod var1
                End If
                'Hemant (04 Mar 2019) -- End
            Case "^" : var1 = var2 ^ var1

            Case "²" : var1 = var2 * var2

            Case "°" : var1 = var2 * Math.PI / 180

            Case "sin" : var1 = Math.Sin(var1)

            Case "cos" : var1 = Math.Cos(var1)

            Case "tan" : var1 = Math.Tan(var1)

            Case "log" : var1 = Math.Log10(var1)

            Case "ln" : var1 = Math.Log(var1)

            Case "exp" : var1 = Math.Exp(var1)

            Case "sqr" : var1 = Math.Sqrt(var1)

            Case "_" : var1 = -var1

        End Select

        var.Push(var1)

        Return var1

    End Function

    Private Sub Parse(ByVal expr As Object, ByRef eptr As Object, ByRef type As Object, ByRef op As Object, ByRef value As Decimal, ByVal varexpected As Object)

        Dim finished As Boolean = False

        Dim first As Boolean = True

        Dim DP As Boolean = False

        'Sohail (24 Jun 2011) -- Start
        'Issue : Overflow error
        'Dim divider As Long = 1
        Dim divider As Decimal = 1
        'Sohail (24 Jun 2011) -- End


        'skip spaces

        Do While Mid(expr, eptr, 1) = " "

            eptr += 1

        Loop

        type = "" : op = "" : value = 0

        If varexpected And Mid(expr, eptr, 1) = "-" Then Mid(expr, eptr, 1) = "_" 'replace unary minus with _

        Select Case Mid(expr, eptr, 1)

            Case "("

                type = "Open bracket" : eptr += 1

            Case ")"

                type = "Close bracket" : eptr += 1

            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "x", "y"

                type = "Numeric"

                Do

                    Select Case Mid(expr, eptr, 1)

                        Case "a"

                            If first Then value = a Else value = value * a

                        Case "b"

                            If first Then value = b Else value = value * b

                        Case "c"

                            If first Then value = c Else value = value * c

                        Case "x"

                            If first Then value = x Else value = value * x

                            'rem treat x² as a special case

                            If Mid(expr, eptr + 1, 1) = "²" Then value = value * x : eptr += 1

                        Case "y"

                            If first Then value = y Else value = value * y

                            If Mid(expr, eptr + 1, 1) = "²" Then value = value * y : eptr += 1

                        Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"

                            value = 10 * value + CDec(Mid(expr, eptr, 1))

                            If DP Then divider = 10 * divider

                        Case "."

                            DP = True

                        Case Else

                            eptr -= 1

                            finished = True

                    End Select

                    first = False

                    eptr += 1

                Loop Until finished

                value = value / divider

            Case "+", "-", "*", "/", "^", "²", "°"

                type = "Operator"

                op = Mid(expr, eptr, 1) : eptr += 1

            Case Else

                type = "Function"

                Select Case Mid(expr, eptr, 1)

                    Case "_"

                        op = Mid(expr, eptr, 1) : eptr += 1

                    Case Else

                        Select Case Mid(expr, eptr, 2)

                            Case "ln"

                                op = Mid(expr, eptr, 2) : eptr += 2

                            Case Else

                                Select Case Mid(expr, eptr, 3)

                                    Case "sin", "cos", "tan", "log", "exp", "sqr"

                                        op = Mid(expr, eptr, 3) : eptr += 3
                                        'Hemant (04 Mar 2019) -- Start
                                        'Internal # 32 : Provide "Modulus" function to finds the remainder after division of one number in transaction head formula.
                                    Case "Mod"
                                        op = Mid(expr, eptr, 3) : eptr += 3
                                        type = "Operator"
                                        'Hemant (04 Mar 2019) -- End
                                    Case Else

                                        type = "Invalid character"

                                End Select

                        End Select

                End Select

        End Select

    End Sub



    Private Sub DoError(ByVal message, ByVal ptr)
        Dim exForce As Exception

        exForce = New Exception(message)
        Throw exForce
    End Sub


End Class
