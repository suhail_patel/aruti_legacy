﻿'************************************************************************************************************************************
'Class Name : clsCommonmaster_allocation.vb
'Purpose    :
'Date       :15-11-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsCommonmaster_allocation
    Private Const mstrModuleName = "clsommonmaster_allocation_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private xDataOp As clsDataOperation
    Private mintMasterallocationtranunkid As Integer
    Private mintMasterunkid As Integer
    Private mintAdvancefilterallocationid As Integer
    Private mintAdvancefilterallocationtranunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set masterallocationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Masterallocationtranunkid() As Integer
        Get
            Return mintMasterallocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintMasterallocationtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set masterunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Masterunkid() As Integer
        Get
            Return mintMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintMasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set advancefilterallocationid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Advancefilterallocationid() As Integer
        Get
            Return mintAdvancefilterallocationid
        End Get
        Set(ByVal value As Integer)
            mintAdvancefilterallocationid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set advancefilterallocationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Advancefilterallocationtranunkid() As Integer
        Get
            Return mintAdvancefilterallocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAdvancefilterallocationtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property



    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "SELECT " & _
              "  masterallocationtranunkid " & _
              ", masterunkid " & _
              ", advancefilterallocationid " & _
              ", advancefilterallocationtranunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM cfcommonmaster_allocation_tran " & _
             "WHERE masterallocationtranunkid = @masterallocationtranunkid "

            objDataOperation.AddParameter("@masterallocationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintMasterallocationTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintmasterallocationtranunkid = CInt(dtRow.Item("masterallocationtranunkid"))
                mintmasterunkid = CInt(dtRow.Item("masterunkid"))
                mintadvancefilterallocationid = CInt(dtRow.Item("advancefilterallocationid"))
                mintadvancefilterallocationtranunkid = CInt(dtRow.Item("advancefilterallocationtranunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intMasterunkid As Integer, ByVal blnAddAllocationNameColumns As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            'Dim dicAllocation As Dictionary(Of Integer, String) = Nothing
            Dim objMaster As New clsMasterData
            Dim dsAdvAlloc As DataSet = Nothing
            Dim dsGender As DataSet = Nothing
            Dim dsCountry As DataSet = Nothing
            If blnAddAllocationNameColumns = True Then
                dsAdvAlloc = objMaster.GetAdvanceFilterAllocation("List", False, "", objDataOperation)
                dsGender = objMaster.getGenderList("List", False, objDataOperation)
                dsCountry = objMaster.getCountryList("List", False, , objDataOperation)
            End If

            strQ = "SELECT " & _
              "  cfcommonmaster_allocation_tran.masterallocationtranunkid " & _
              ", cfcommonmaster_allocation_tran.masterunkid " & _
              ", cfcommonmaster_allocation_tran.advancefilterallocationid " & _
              ", cfcommonmaster_allocation_tran.advancefilterallocationtranunkid " & _
              ", cfcommonmaster_allocation_tran.userunkid " & _
              ", cfcommonmaster_allocation_tran.loginemployeeunkid " & _
              ", cfcommonmaster_allocation_tran.isweb " & _
              ", cfcommonmaster_allocation_tran.isvoid " & _
              ", cfcommonmaster_allocation_tran.voiduserunkid " & _
              ", cfcommonmaster_allocation_tran.voidloginemployeeunkid " & _
              ", cfcommonmaster_allocation_tran.voiddatetime " & _
              ", cfcommonmaster_allocation_tran.voidreason " & _
              ", CAST(0 AS BIT) AS IsGroup " & _
              ", '' AS AUD "

            If blnAddAllocationNameColumns = True Then
                strQ &= ", CASE cfcommonmaster_allocation_tran.advancefilterallocationid "
                For Each dr As DataRow In dsAdvAlloc.Tables(0).Rows
                    strQ &= " WHEN " & CInt(dr.Item("Id")) & "  THEN '" & dr.Item("Name").ToString.Replace("'", "''") & "' "
                Next
                strQ &= " ELSE '' END AS advancefilterallocationname "

                strQ &= ", CASE cfcommonmaster_allocation_tran.advancefilterallocationid "
                For Each dr As DataRow In dsAdvAlloc.Tables(0).Rows
                    If CInt(dr.Item("Id")) = enAdvanceFilterAllocation.GENDER Then
                        strQ &= " WHEN " & CInt(dr.Item("Id")) & "  THEN  "

                        strQ &= " CASE cfcommonmaster_allocation_tran.advancefilterallocationtranunkid "
                        For Each dr1 As DataRow In dsGender.Tables(0).Rows
                            strQ &= " WHEN " & CInt(dr1.Item("Id")) & "  THEN '" & dr1.Item("Name").ToString.Replace("'", "''") & "' "
                        Next
                        strQ &= " ELSE '' END  "
                    ElseIf CInt(dr.Item("Id")) = enAdvanceFilterAllocation.NATIONALITY Then
                        strQ &= " WHEN " & CInt(dr.Item("Id")) & "  THEN  "

                        strQ &= " CASE cfcommonmaster_allocation_tran.advancefilterallocationtranunkid "
                        For Each dr2 As DataRow In dsCountry.Tables(0).Rows
                            strQ &= " WHEN " & CInt(dr2.Item("countryunkid")) & "  THEN '" & dr2.Item("country_name").ToString.Replace("'", "''") & "' "
                        Next
                        strQ &= " ELSE '' END  "
                    Else
                        strQ &= " WHEN " & CInt(dr.Item("Id")) & "  THEN " & dr.Item("AllocationName").ToString.Replace("'", "''") & " "
                    End If
                Next
                strQ &= " ELSE '' END AS advancefilterallocationtranname "

            End If

            strQ &= "FROM cfcommonmaster_allocation_tran "

            If blnAddAllocationNameColumns = True Then
                strQ &= "LEFT JOIN hrstation_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrstation_master.stationunkid  AND hrstation_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.BRANCH & " " & _
                        "LEFT JOIN hrdepartment_group_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrdepartment_group_master.deptgroupunkid  AND hrdepartment_group_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.DEPARTMENT_GROUP & " " & _
                        "LEFT JOIN hrdepartment_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrdepartment_master.departmentunkid  AND hrdepartment_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.DEPARTMENT & " " & _
                        "LEFT JOIN hrsectiongroup_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrsectiongroup_master.sectiongroupunkid  AND hrsectiongroup_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.SECTION_GROUP & " " & _
                        "LEFT JOIN hrsection_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrsection_master.sectionunkid  AND hrsection_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.SECTION & " " & _
                        "LEFT JOIN hrunitgroup_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrunitgroup_master.unitgroupunkid  AND hrunitgroup_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.UNIT_GROUP & " " & _
                        "LEFT JOIN hrunit_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrunit_master.unitunkid  AND hrunit_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.UNIT & " " & _
                        "LEFT JOIN hrteam_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrteam_master.teamunkid  AND hrteam_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.TEAM & " " & _
                        "LEFT JOIN hrjobgroup_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrjobgroup_master.jobgroupunkid  AND hrjobgroup_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.JOB_GROUP & " " & _
                        "LEFT JOIN hrjob_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrjob_master.jobunkid  AND hrjob_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.JOBS & " " & _
                        "LEFT JOIN hrclassgroup_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrclassgroup_master.classgroupunkid  AND hrclassgroup_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.CLASS_GROUP & " " & _
                        "LEFT JOIN hrclasses_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrclasses_master.classesunkid  AND hrclasses_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.CLASSES & " " & _
                        "LEFT JOIN hrgradegroup_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrgradegroup_master.gradegroupunkid  AND hrgradegroup_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.GRADE_GROUP & " " & _
                        "LEFT JOIN hrgrade_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrgrade_master.gradeunkid  AND hrgrade_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.GRADE & " " & _
                        "LEFT JOIN hrgradelevel_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrgradelevel_master.gradelevelunkid  AND hrgradelevel_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.GRADE_LEVEL & " " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = cfpayrollgroup_master.groupmasterunkid  AND cfpayrollgroup_master.isactive = 1 AND grouptype_id = " & enPayrollGroupType.CostCenter & " AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.COST_CENTER_GROUP & " " & _
                        "LEFT JOIN prcostcenter_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = prcostcenter_master.costcenterunkid  AND prcostcenter_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.COST_CENTER & " " & _
                        "LEFT JOIN cfcommon_master AS EmplType ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = EmplType.masterunkid  AND EmplType.isactive = 1 AND EmplType.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.EMPLOYEMENT_TYPE & " " & _
                        "LEFT JOIN cfcommon_master AS Religion ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = Religion.masterunkid  AND Religion.isactive = 1 AND Religion.mastertype = " & clsCommon_Master.enCommonMaster.RELIGION & " AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.RELIGION & " " & _
                        "LEFT JOIN cfcommon_master AS Relation ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = Relation.masterunkid  AND Relation.isactive = 1 AND Relation.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS & " " & _
                        "LEFT JOIN cfcommon_master AS PayType ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = PayType.masterunkid  AND PayType.isactive = 1 AND PayType.mastertype = " & clsCommon_Master.enCommonMaster.PAY_TYPE & " AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.PAY_TYPE & " " & _
                        "LEFT JOIN prpaypoint_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = prpaypoint_master.paypointunkid  AND prpaypoint_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.PAY_POINT & " " & _
                        "LEFT JOIN cfcommon_master AS Marital ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = Marital.masterunkid  AND Marital.isactive = 1 AND Marital.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.MARITAL_STATUS & " " & _
                        "LEFT JOIN hrskill_master ON cfcommonmaster_allocation_tran.advancefilterallocationtranunkid = hrskill_master.skillunkid  AND hrskill_master.isactive = 1 AND cfcommonmaster_allocation_tran.advancefilterallocationid = " & enAdvanceFilterAllocation.SKILL & " "
            End If

            strQ &= "WHERE cfcommonmaster_allocation_tran.isvoid = 0 "

            If intMasterunkid > 0 Then
                strQ &= " AND cfcommonmaster_allocation_tran.masterunkid = @masterunkid "
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfcommonmaster_allocation_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@masterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmasterunkid.ToString)
            objDataOperation.AddParameter("@advancefilterallocationid", SqlDbType.int, eZeeDataType.INT_SIZE, mintadvancefilterallocationid.ToString)
            objDataOperation.AddParameter("@advancefilterallocationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintadvancefilterallocationtranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "INSERT INTO cfcommonmaster_allocation_tran ( " & _
              "  masterunkid " & _
              ", advancefilterallocationid " & _
              ", advancefilterallocationtranunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @masterunkid " & _
              ", @advancefilterallocationid " & _
              ", @advancefilterallocationtranunkid " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMasterallocationTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll(ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                If Not (dtRow.Item("AUD").ToString = "A" OrElse dtRow.Item("AUD").ToString = "D") Then Continue For

                If IsDBNull(dtRow.Item("masterallocationtranunkid")) = True Then
                    mintMasterallocationtranunkid = 0
                Else
                    mintMasterallocationtranunkid = CInt(dtRow.Item("masterallocationtranunkid"))
                End If
                mintAdvancefilterallocationid = CInt(dtRow.Item("advancefilterallocationid"))
                mintAdvancefilterallocationtranunkid = CInt(dtRow.Item("advancefilterallocationtranunkid"))

                Me._xDataOp = objDataOperation

                If dtRow.Item("AUD").ToString = "A" Then
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintVoidloginemployeeunkid = -1

                    If Insert() = False Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                ElseIf dtRow.Item("AUD").ToString = "D" Then
                    mblnIsvoid = True
                    mintVoiduserunkid = mintUserunkid
                    mdtVoiddatetime = mdtAuditDate
                    mstrVoidreason = Language.getMessage(mstrModuleName, 1, "Allocation Removed")
                    mintVoidloginemployeeunkid = mintLoginemployeeunkid
                    Dim intOldUserUnkId As Integer = mintUserunkid
                    Dim intOldLoginemployeeUnkId As Integer = mintLoginemployeeunkid

                    If Delete() = False Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    mintUserunkid = intOldUserUnkId
                    mintLoginemployeeunkid = intOldLoginemployeeUnkId
                End If


            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfcommonmaster_allocation_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintMasterallocationtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@masterallocationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmasterallocationtranunkid.ToString)
            objDataOperation.AddParameter("@masterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmasterunkid.ToString)
            objDataOperation.AddParameter("@advancefilterallocationid", SqlDbType.int, eZeeDataType.INT_SIZE, mintadvancefilterallocationid.ToString)
            objDataOperation.AddParameter("@advancefilterallocationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintadvancefilterallocationtranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "UPDATE cfcommonmaster_allocation_tran SET " & _
              "  masterunkid = @masterunkid" & _
              ", advancefilterallocationid = @advancefilterallocationid" & _
              ", advancefilterallocationtranunkid = @advancefilterallocationtranunkid" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE masterallocationtranunkid = @masterallocationtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfcommonmaster_allocation_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE cfcommonmaster_allocation_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE masterallocationtranunkid = @masterallocationtranunkid "

            objDataOperation.AddParameter("@masterallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMasterallocationtranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Masterallocationtranunkid = mintMasterallocationtranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@masterallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  masterallocationtranunkid " & _
              ", masterunkid " & _
              ", advancefilterallocationid " & _
              ", advancefilterallocationtranunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM cfcommonmaster_allocation_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND masterallocationtranunkid <> @masterallocationtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@masterallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMasterallocationtranunkid.ToString)
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMasterunkid.ToString)
            objDataOperation.AddParameter("@advancefilterallocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdvancefilterallocationid.ToString)
            objDataOperation.AddParameter("@advancefilterallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdvancefilterallocationtranunkid.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO atcfcommonmaster_allocation_tran ( " & _
              "  masterallocationtranunkid " & _
              ", masterunkid " & _
              ", advancefilterallocationid " & _
              ", advancefilterallocationtranunkid " & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", audittype " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
            ") VALUES (" & _
              "  @masterallocationtranunkid " & _
              ", @masterunkid " & _
              ", @advancefilterallocationid " & _
              ", @advancefilterallocationtranunkid " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @audittype " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class
