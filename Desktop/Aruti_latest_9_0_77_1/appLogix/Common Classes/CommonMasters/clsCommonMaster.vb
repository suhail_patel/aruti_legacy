﻿'************************************************************************************************************************************
'Class Name : clsCfcommon_master.vb
'Purpose    :
'Date       :27/06/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsCommon_Master
    Private Shared ReadOnly mstrModuleName As String = "clsCommon_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    Public Enum enCommonMaster
        ADVERTISE_CATEGORY = 1
        ALLERGIES = 2
        ASSET_CONDITION = 3
        BENEFIT_GROUP = 4
        BLOOD_GROUP = 5
        COMPLEXION = 6
        DISABILITIES = 7
        EMPLOYEMENT_TYPE = 8
        ETHNICITY = 9
        EYES_COLOR = 10
        HAIR_COLOR = 11
        IDENTITY_TYPES = 12
        INTERVIEW_TYPE = 13
        LANGUAGES = 14
        MARRIED_STATUS = 15
        MEMBERSHIP_CATEGORY = 16
        PAY_TYPE = 17
        QUALIFICATION_COURSE_GROUP = 18
        RELATIONS = 19
        RELIGION = 20
        RESULT_GROUP = 21
        SHIFT_TYPE = 22
        SKILL_CATEGORY = 23
        TITLE = 24
        TRAINING_RESOURCES = 25
        ASSET_STATUS = 26
        ASSETS = 27
        'Sandeep [ 21 Aug 2010 ] -- Start
        LETTER_TYPE = 28
        'Sandeep [ 21 Aug 2010 ] -- End 
        SALINC_REASON = 29 'Sohail (08 Nov 2011)

        'S.SANDEEP [ 29 DEC 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES 
        'TYPE : EMPLOYEMENT CONTRACT PROCESS
        TRAINING_COURSEMASTER = 30
        STRATEGIC_GOAL = 31
        'S.SANDEEP [ 29 DEC 2011 ] -- END

        'S.SANDEEP [ 24 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES [<TRAINING>]
        SOURCES_FUNDINGS = 32
        'S.SANDEEP [ 24 FEB 2012 ] -- END
 'S.SANDEEP [ 04 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        ATTACHMENT_TYPES = 33
        'S.SANDEEP [ 04 FEB 2012 ] -- END

        'S.SANDEEP [ 28 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        VACANCY_MASTER = 34
        'S.SANDEEP [ 28 FEB 2012 ] -- END

        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        DISCIPLINE_COMMITTEE = 35
        'Anjan (20 Mar 2012)-End 

        'S.SANDEEP [ 20 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
        OFFENCE_CATEGORY = 36
        'S.SANDEEP [ 20 MARCH 2012 ] -- END

        'S.SANDEEP [ 19 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        COMMITTEE_MEMBERS_CATEGORY = 37
        'S.SANDEEP [ 19 DEC 2012 ] -- END

        'S.SANDEEP [ 14 AUG 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        APPRAISAL_ACTIONS = 38
        'S.SANDEEP [ 14 AUG 2013 ] -- END
        SECTOR_ROUTE = 39

       'S.SANDEEP [ 05 NOV 2014 ] -- START
        COMPETENCE_CATEGORIES = 40
        ASSESSMENT_SCALE_GROUP = 41
        'S.SANDEEP [ 05 NOV 2014 ] -- END


        'S.SANDEEP [14 MAR 2015] -- START
        TRANSFERS = 42
        PROMOTIONS = 43
        RECATEGORIZE = 44
        PROBATION = 45
        CONFIRMATION = 46
        SUSPENSION = 47
        TERMINATION = 48
        RE_HIRE = 49
        WORK_PERMIT = 50
        RETIREMENTS = 51
        COST_CENTER = 52
        'S.SANDEEP [14 MAR 2015] -- END

        'S.SANDEEP [21 NOV 2015] -- START
        COMPETENCE_GROUP = 53
        'S.SANDEEP [21 NOV 2015] -- END

        'Nilay (21-Mar-2016) -- Start
        'ENCHACEMENT for Sierra Leone
        PROV_REGION_1 = 54
        ROAD_STREET_1 = 55
        CHIEFDOM = 56
        VILLAGE = 57
        TOWN_1 = 58
        'Nilay (21-Mar-2016) -- End

        'S.SANDEEP [24 MAY 2016] -- START
        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
        DISC_REPONSE_TYPE = 59
        'S.SANDEEP [24 MAY 2016] -- END

        'Shani (26-Sep-2016) -- Start
        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
        GE_APPRAISAL_ACTIONS = 60
        BSC_APPRAISAL_ACTIONS = 61
        'Shani (26-Sep-2016) -- End
        'S.SANDEEP [04-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 120
        RESIDENT_PERMIT = 62
        'S.SANDEEP [04-Jan-2018] -- END

        'Hemant (25 Aug 2018) -- Start
        'Enhancement : New Modules for Grievance
        GRIEVANCE_TYPE = 63
        'Hemant (25 Aug 2018)) -- End   

        'Hemant (06 Oct 2018) -- Start
        'Enhancement : Implementing New Module of Asset Declaration Template 2
        ASSET_SECTOR = 64
        ASSET_SECURITIES = 65
        'Hemant (06 Oct 2018) -- End

        'S.SANDEEP [09-OCT-2018] -- START
        TRAINING_MODE = 66
        'S.SANDEEP [09-OCT-2018] -- END

        'Hemant (15 Nov 2018) -- Start
        'Enhancement : Changes for NMB Requirement
        LIABILITY_TYPE_ASSET_DECLARATION_TEMPLATE2 = 67
        'Hemant (15 Nov 2018) -- End

        'S.SANDEEP |12-FEB-2019| -- START
        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
        GOAL_UNIT_OF_MEASURE = 68
        'S.SANDEEP |12-FEB-2019| -- END
        'Sohail (18 May 2019) -- Start
        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
        DEPENDANT_ACTIVE_INACTIVE_REASON = 69
        'Sohail (18 May 2019) -- End


        'S.SANDEEP |16-AUG-2019| -- START
        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
        PERFORMANCE_CUSTOM_ITEM = 70
        'S.SANDEEP |16-AUG-2019| -- END
        'Sohail (27 Sep 2019) -- Start
        'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
        VACANCY_SOURCE = 71
        'Sohail (27 Sep 2019) -- End
        'Sohail (21 Oct 2019) -- Start
        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
        EXEMPTION = 72
        'Sohail (21 Oct 2019) -- End
        'Sohail (14 Nov 2019) -- Start
        'NMB UAT Enhancement # : New Screen Training Need Form.
        TRAINING_REQUEST_COST_ITEM = 73
        'Sohail (14 Nov 2019) -- End

        'S.SANDEEP |01-MAY-2020| -- START
        'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
        DISCIPLINE_CASE_OPEN_REASON = 74
        'S.SANDEEP |01-MAY-2020| -- END

        'S.SANDEEP |18-DEC-2020| -- START
        'ISSUE/ENHANCEMENT : DISCIPLINE CHANGES
        DISCIPLINE_CONTRARY_TO = 75
        'S.SANDEEP |18-DEC-2020| -- END

    End Enum

#Region " Private variables "
    Private mintMasterunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrAlias As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mintMastertype As Integer
    Private mblnIsactive As Boolean = True


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Private mintQualificationGrp_Level As Integer
    'Pinkal (12-Oct-2011) -- End

  'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintDependant_Limit As Integer = 0
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    Private mintCourseTypeId As Integer = 0
    Private mblnIsRefreeAllowed As Boolean = False
    'Pinkal (06-Feb-2012) -- End

    'Sohail (22 Apr 2015) -- Start
    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    Private mblnIsSyncWithrecruitment As Boolean = False
    'Sohail (22 Apr 2015) -- End


    'Pinkal (06-Mar-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnMedicalBenefit As Boolean = False
    'Pinkal (06-Mar-2013) -- End

    'S.SANDEEP [21 NOV 2015] -- START
    Private mstrDescription As String = ""
    'S.SANDEEP [21 NOV 2015] -- END

    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Private mblnIsMandatoryForNewEmp As Boolean = False
    'Nilay (03-Dec-2015) -- End

    'Hemant (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2
    Private mblnIsUseInAssetDeclaration As Boolean = False
    'Hemant (06 Oct 2018) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    Private mblnIsForTrainingRequisition As Boolean = False
    'S.SANDEEP [09-OCT-2018] -- END


    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsCRBenefit As Boolean = False
    Private mintMaxCountForCRBenefit As Integer = 0
    Private mintMinAgeLimitForCRBenefit As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private mintUserunkid As Integer = 0
    'Sohail (14 Nov 2019) -- End

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    Private mintJobMstunkid As Integer = 0
    'Gajanan [13-Nov-2020] -- End



#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
    Private mintVoidlogingemployeeunkid As Integer = 0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set masterunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Masterunkid() As Integer
        Get
            Return mintMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintMasterunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set alias
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Alias() As String
        Get
            Return mstrAlias
        End Get
        Set(ByVal value As String)
            mstrAlias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mastertype
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Mastertype() As Integer
        Get
            Return mintMastertype
        End Get
        Set(ByVal value As Integer)
            mintMastertype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set Qualification Group Level
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _QualificationGrp_Level() As Integer
        Get
            Return mintQualificationGrp_Level
        End Get
        Set(ByVal value As Integer)
            mintQualificationGrp_Level = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End

 'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set dependant_limit
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Dependant_Limit() As Integer
        Get
            Return mintDependant_Limit
        End Get
        Set(ByVal value As Integer)
            mintDependant_Limit = Value
        End Set
    End Property
    'S.SANDEEP [ 07 NOV 2011 ] -- END



    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes

    Public Property _CouseTypeId() As Integer
        Get
            Return mintCourseTypeId
        End Get
        Set(ByVal value As Integer)
            mintCourseTypeId = value
        End Set
    End Property

    Public Property _IsRefreeAllowed() As Boolean
        Get
            Return mblnIsRefreeAllowed
        End Get
        Set(ByVal value As Boolean)
            mblnIsRefreeAllowed = value
        End Set
    End Property

    'Pinkal (06-Feb-2012) -- End


    'Pinkal (06-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _IsMedicalBenefit()
        Get
            Return mblnMedicalBenefit
        End Get
        Set(ByVal value)
            mblnMedicalBenefit = value
        End Set
    End Property

    'Pinkal (06-Mar-2013) -- End

    'Sohail (22 Apr 2015) -- Start
    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    Public Property _IsSyncWithrecruitment() As Boolean
        Get
            Return mblnIsSyncWithrecruitment
        End Get
        Set(ByVal value As Boolean)
            mblnIsSyncWithrecruitment = value
        End Set
    End Property
    'Sohail (22 Apr 2015) -- End


    'S.SANDEEP [21 NOV 2015] -- START
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property
    'S.SANDEEP [21 NOV 2015] -- END

    'Nilay (03-Dec-2015) -- Start
    'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
    Public Property _IsMandatoryForNewEmp() As Boolean
        Get
            Return mblnIsMandatoryForNewEmp
        End Get
        Set(ByVal value As Boolean)
            mblnIsMandatoryForNewEmp = value
        End Set
    End Property
    'Nilay (03-Dec-2015) -- End

    'Hemant (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2
    Public Property _IsUseInAssetDeclaration() As Boolean
        Get
            Return mblnIsUseInAssetDeclaration
        End Get
        Set(ByVal value As Boolean)
            mblnIsUseInAssetDeclaration = value
        End Set
    End Property
    'Hemant (06 Oct 2018) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    Public Property _IsForTrainingRequisition() As Boolean
        Get
            Return mblnIsForTrainingRequisition
        End Get
        Set(ByVal value As Boolean)
            mblnIsForTrainingRequisition = value
        End Set
    End Property
    'S.SANDEEP [09-OCT-2018] -- END


    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    ''' <summary>
    ''' Purpose: Get or Set IsCRBenefit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsCRBenefit() As Boolean
        Get
            Return mblnIsCRBenefit
        End Get
        Set(ByVal value As Boolean)
            mblnIsCRBenefit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MaxCountForCRBenefit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _MaxCountForCRBenefit() As Integer
        Get
            Return mintMaxCountForCRBenefit
        End Get
        Set(ByVal value As Integer)
            mintMaxCountForCRBenefit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MinAgeLimitForCRBenefit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _MinAgeLimitForCRBenefit() As Integer
        Get
            Return mintMinAgeLimitForCRBenefit
        End Get
        Set(ByVal value As Integer)
            mintMinAgeLimitForCRBenefit = value
        End Set
    End Property

    'Pinkal (25-Oct-2018) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property
    'Sohail (14 Nov 2019) -- End

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    Public Property _JobMstunkid() As Integer
        Get
            Return mintJobMstunkid
        End Get
        Set(ByVal value As Integer)
            mintJobMstunkid = value
        End Set
    End Property
    'Gajanan [13-Nov-2020] -- End


#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                    "  masterunkid " & _
                    ", code " & _
                    ", alias " & _
                    ", name " & _
                    ", name1 " & _
                    ", name2 " & _
                    ", mastertype " & _
                  ", ISNULL(qlevel,0) qlevel " & _
                    ", isactive " & _
                    ", ISNULL(dependant_limit,0) AS dependant_limit " & _
                ", ISNULL(coursetypeid,0) as  coursetypeid " & _
                    ", ISNULL(isrefreeallowed,0) as isrefreeallowed " & _
                    ", ISNULL(ismedicalbenefit,0) as ismedicalbenefit " & _
                    ", ISNULL(issyncwithrecruitment,0) as issyncwithrecruitment " & _
                    ", ISNULL(description,'') AS description " & _
                        ", ISNULL(ismandatoryfornewemp, 0) AS ismandatoryfornewemp " & _
                        ", ISNULL(isuseinassetdeclaration,0) as isuseinassetdeclaration " & _
                    ", ISNULL(istrainingrequisition, 0) AS istrainingrequisition " & _
                    ", ISNULL(iscrbenefit, 0) AS iscrbenefit " & _
                    ", ISNULL(minagelimit_crdependants, 0) AS minagelimit_crdependants " & _
                    ", ISNULL(maxcnt_crdependants, 0) AS maxcnt_crdependants " & _
                    ", ISNULL(jobmstunkid,0) as jobmstunkid " & _
                    " FROM cfcommon_master " & _
                    " WHERE masterunkid = @masterunkid "


            'Pinkal (25-Oct-2018) --   'Enhancement - Implementing Claim & Request changes For NMB .[ ", ISNULL(iscrbenefit, 0) AS iscrbenefit " & _ ", ISNULL(minagelimit_crdependants, 0) AS minagelimit_crdependants " & _", ISNULL(maxcnt_crdependants, 0) AS maxcnt_crdependants " & _]

            'S.SANDEEP [09-OCT-2018] -- START {istrainingrequisition} -- END

            'Hemant (06 Oct 2018) -- [isuseinassetdeclaration]
            'Nilay (03-Dec-2015) -- [ismandatoryfornewemp]
            'Sohail (22 Apr 2015) - [issyncwithrecruitment]
            'S.SANDEEP [21 NOV 2015] -- START {description} -- END


            'strQ = "SELECT " & _
            '        "  masterunkid " & _
            '        ", code " & _
            '        ", alias " & _
            '        ", name " & _
            '        ", name1 " & _
            '        ", name2 " & _
            '        ", mastertype " & _
            '        ", ISNULL(qlevel,0) qlevel " & _
            '        ", isactive " & _
            '        ", ISNULL(dependant_limit,0) AS dependant_limit " & _
            '        ", ISNULL(coursetypeid,0) as  coursetypeid " & _
            '         ", ISNULL(isrefreeallowed,0) as isrefreeallowed " & _
            '     "FROM cfcommon_master " & _
            '     "WHERE masterunkid = @masterunkid "

            'Pinkal (06-Mar-2013) -- End


            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMasterunkid = CInt(dtRow.Item("masterunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrAlias = dtRow.Item("alias").ToString
                mstrName = dtRow.Item("name").ToString
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mintMastertype = CInt(dtRow.Item("mastertype"))
                mblnIsactive = CBool(dtRow.Item("isactive"))

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                If Not IsDBNull(dtRow.Item("qlevel")) Then
                    mintQualificationGrp_Level = CInt(dtRow.Item("qlevel"))
                End If
                'Pinkal (12-Oct-2011) -- End
                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintDependant_Limit = CInt(dtRow.Item("dependant_limit"))
                'S.SANDEEP [ 07 NOV 2011 ] -- END



                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                mintCourseTypeId = CInt(dtRow.Item("coursetypeid"))
                mblnIsRefreeAllowed = CBool(dtRow.Item("isrefreeallowed"))
                'Pinkal (06-Feb-2012) -- End

                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                mblnIsSyncWithrecruitment = CBool(dtRow.Item("issyncwithrecruitment"))
                'Sohail (22 Apr 2015) -- End

                'S.SANDEEP [21 NOV 2015] -- START
                mstrDescription = CStr(dtRow.Item("description"))
                'S.SANDEEP [21 NOV 2015] -- END

                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                mblnIsMandatoryForNewEmp = CBool(dtRow.Item("ismandatoryfornewemp"))
                'Nilay (03-Dec-2015) -- End

                'Hemant (06 Oct 2018) -- Start
                'Enhancement : Implementing New Module of Asset Declaration Template 2
                mblnIsUseInAssetDeclaration = CBool(dtRow.Item("isuseinassetdeclaration"))
                'Hemant (06 Oct 2018) -- End

                'S.SANDEEP [09-OCT-2018] -- START
                mblnIsForTrainingRequisition = CBool(dtRow.Item("istrainingrequisition"))
                'S.SANDEEP [09-OCT-2018] -- END


                'Pinkal (25-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                mblnIsCRBenefit = CBool(dtRow.Item("iscrbenefit"))
                mintMinAgeLimitForCRBenefit = CInt(dtRow.Item("minagelimit_crdependants"))
                mintMaxCountForCRBenefit = CInt(dtRow.Item("maxcnt_crdependants"))
                'Pinkal (25-Oct-2018) -- End

                'Gajanan [13-Nov-2020] -- Start
                'Final Recruitment UAT v1 Changes.
                mintJobMstunkid = CInt(dtRow.Item("jobmstunkid"))
                'Gajanan [13-Nov-2020] -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal intMasterTypeID As Integer, Optional ByVal strTableName As String = "List", Optional ByVal intLanguageID As Integer = -1, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (06-Mar-2013) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '        "  masterunkid " & _
            '        ", code " & _
            '        ", alias " & _
            '        ", CASE @Language WHEN 1 THEN name1 " & _
            '                     "WHEN 2 THEN name2 " & _
            '                     "ELSE name END AS name " & _
            '        ", mastertype " & _
            '      ", ISNULL(qlevel,0) qlevel " & _
            '        ", isactive " & _
            '        ", ISNULL(dependant_limit,0) AS dependant_limit " & _
            '        ", ISNULL(coursetypeid,0) AS coursetypeid " & _
            '        ", ISNULL(isrefreeallowed,0) AS isrefreeallowed " & _
            '       "FROM cfcommon_master " & _
            '       "WHERE mastertype = @MasterTypeId "

            'Gajanan [13-Nov-2020] -- Add [jobunkid]

            strQ = "SELECT " & _
                    "  masterunkid " & _
                    ", code " & _
                    ", alias " & _
                    ", CASE @Language WHEN 1 THEN name1 " & _
                                 "WHEN 2 THEN name2 " & _
                        "                 ELSE name " & _
                        "  END AS name " & _
                    ", mastertype " & _
                  ", ISNULL(qlevel,0) qlevel " & _
                    ", isactive " & _
                    ", ISNULL(dependant_limit,0) AS dependant_limit " & _
                 ", ISNULL(coursetypeid,0) AS coursetypeid " & _
                    ", ISNULL(isrefreeallowed,0) AS isrefreeallowed " & _
                ", ISNULL(ismedicalbenefit,0) AS ismedicalbenefit " & _
                ", ISNULL(issyncwithrecruitment,0) AS issyncwithrecruitment " & _
                   ", ISNULL(description,'') AS description " & _
                        ", ISNULL(ismandatoryfornewemp, 0) AS ismandatoryfornewemp " & _
                        ", ISNULL(isuseinassetdeclaration ,0) AS isuseinassetdeclaration  " & _
                    ", ISNULL(istrainingrequisition, 0) AS istrainingrequisition " & _
                    ", ISNULL(iscrbenefit, 0) AS iscrbenefit" & _
                    ", ISNULL(minagelimit_crdependants, 0) AS minagelimit_crdependants " & _
                    ", ISNULL(maxcnt_crdependants, 0) AS maxcnt_crdependants " & _
                    ", ISNULL(jobmstunkid, 0) AS jobmstunkid " & _
                    " FROM cfcommon_master " & _
                    " WHERE mastertype = @MasterTypeId "

            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[    ", ISNULL(iscrbenefit, 0) AS iscrbenefit" & _, ISNULL(minagelimit_crdependants, 0) AS minagelimit_crdependants " & _        ", ISNULL(maxcnt_crdependants, 0) AS maxcnt_crdependants " & _]

            'S.SANDEEP [09-OCT-2018] -- START {istrainingrequisition} -- END

            'Hemant (06 Oct 2018) -- [isuseinassetdeclaration]
            'Nilay (03-Dec-2015) -- [ismandatoryfornewemp]
            'Sohail (22 Apr 2015) - [issyncwithrecruitment]
            'Pinkal (06-Mar-2013) -- End
            'S.SANDEEP [21 NOV 2015] -- START {description} -- END



            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            'Gajanan [30-JAN-2021] -- Start   
            'Enhancement:Worked On PDP Module
            If strFilter.Length > 0 Then
                strQ &= strFilter
            End If
            'Gajanan [30-JAN-2021] -- End 
            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@Language", SqlDbType.Int, eZeeDataType.INT_SIZE, intLanguageID)
            objDataOperation.AddParameter("@MasterTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeID)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfcommon_master) </purpose>
    ''' 

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    'Public Function Insert(Optional ByVal mdtAllocation As DataTable = Nothing) As Boolean
    Public Function Insert(Optional ByVal mdtAllocation As DataTable = Nothing, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [13-Nov-2020] -- End

        'Sohail (14 Nov 2019) - [mdtAllocation]
        
        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'If isExist(mintMastertype, mstrCode) Then
        If isExist(mintMastertype, mstrCode, , , , -1, xDataOpr) Then
            'Gajanan [13-Nov-2020] -- END
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'If isExist(mintMastertype, , mstrName) Then
        If isExist(mintMastertype, , mstrName, , , -1, xDataOpr) Then
            'Gajanan [13-Nov-2020] -- END
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.

        'objDataOperation = New clsDataOperation
        ''Anjan (12 Oct 2011)-Start
        ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        'objDataOperation.BindTransaction()
        ''Anjan (12 Oct 2011)-End 

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        'Gajanan [13-Nov-2020] -- Start

        Try
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@alias", SqlDbType.NVarChar, eZeeDataType.ALIAS_SIZE, mstrAlias.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMastertype.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGrp_Level.ToString)
 objDataOperation.AddParameter("@dependant_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependant_Limit.ToString)
            objDataOperation.AddParameter("@coursetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseTypeId.ToString)
            objDataOperation.AddParameter("@isrefreeallowed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsRefreeAllowed.ToString())

            'Pinkal (06-Mar-2013) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@ismedicalbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnMedicalBenefit.ToString())
            objDataOperation.AddParameter("@issyncwithrecruitment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSyncWithrecruitment.ToString()) 'Sohail (22 Apr 2015)

            'S.SANDEEP [21 NOV 2015] -- START
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, 2000, mstrDescription)
            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            objDataOperation.AddParameter("@ismandatoryfornewemp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsMandatoryForNewEmp.ToString())
            'Nilay (03-Dec-2015) -- End

            'S.SANDEEP [21 NOV 2015] -- END

            'Hemant (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            objDataOperation.AddParameter("@isuseinassetdeclaration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsUseInAssetDeclaration.ToString())
            'Hemant (06 Oct 2018) -- End

            'S.SANDEEP [09-OCT-2018] -- START
            objDataOperation.AddParameter("@istrainingrequisition", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForTrainingRequisition.ToString())
            'S.SANDEEP [09-OCT-2018] -- END


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            objDataOperation.AddParameter("@iscrbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCRBenefit.ToString())
            objDataOperation.AddParameter("@minagelimit_crdependants", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinAgeLimitForCRBenefit.ToString())
            objDataOperation.AddParameter("@maxcnt_crdependants", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxCountForCRBenefit.ToString())
            'Pinkal (25-Oct-2018) -- End

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            objDataOperation.AddParameter("@jobmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobMstunkid)
            'Gajanan [13-Nov-2020] -- END


            'strQ = "INSERT INTO cfcommon_master ( " & _
            '          "  code " & _
            '          ", alias " & _
            '          ", name " & _
            '          ", name1 " & _
            '          ", name2 " & _
            '          ", mastertype " & _
            '        ", qlevel " & _
            '          ", isactive" & _
            '          ", dependant_limit" & _
            '        ", coursetypeid " & _
            '          ", isrefreeallowed " & _
            '        ") VALUES (" & _
            '          "  @code " & _
            '          ", @alias " & _
            '          ", @name " & _
            '          ", @name1 " & _
            '          ", @name2 " & _
            '          ", @mastertype " & _
            '        ", @qlevel " & _
            '          ", @isactive" & _
            '          ", @dependant_limit" & _
            '        ", @coursetypeid " & _
            '          ", @isrefreeallowed " & _
            '        "); SELECT @@identity"


            strQ = "INSERT INTO cfcommon_master ( " & _
                      "  code " & _
                      ", alias " & _
                      ", name " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", mastertype " & _
                    ", qlevel " & _
                      ", isactive" & _
                      ", dependant_limit" & _
                    ", coursetypeid " & _
                      ", isrefreeallowed " & _
                     ", ismedicalbenefit " & _
                     ", issyncwithrecruitment " & _
                      ", description " & _
                        ", ismandatoryfornewemp " & _
                        ", isuseinassetdeclaration " & _
                        ", istrainingrequisition " & _
                      ", iscrbenefit " & _
                      ", minagelimit_crdependants " & _
                      ", maxcnt_crdependants " & _
                      ", jobmstunkid " & _
                      " ) VALUES (" & _
                      "  @code " & _
                      ", @alias " & _
                      ", @name " & _
                      ", @name1 " & _
                      ", @name2 " & _
                      ", @mastertype " & _
                    ", @qlevel " & _
                      ", @isactive" & _
                      ", @dependant_limit" & _
                    ", @coursetypeid " & _
                      ", @isrefreeallowed " & _
                     ", @ismedicalbenefit " & _
                     ", @issyncwithrecruitment " & _
                      ", @description " & _
                        ", @ismandatoryfornewemp " & _
                        ", @isuseinassetdeclaration " & _
                        ", @istrainingrequisition " & _
                      ", @iscrbenefit " & _
                      ", @minagelimit_crdependants " & _
                      ", @maxcnt_crdependants " & _
                      ", @jobmstunkid " & _
                      " ); SELECT @@identity"


            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[", iscrbenefit " & _", minagelimit_crdependants " & _", maxcnt_crdependants " & _]

            'S.SANDEEP [09-OCT-2018] -- START {istrainingrequisition} -- END

            'Hemant (06 Oct 2018) -- [isuseinassetdeclaration]
            'Nilay (03-Dec-2015) -- [ismandatoryfornewemp]
            'Sohail (22 Apr 2015) - [issyncwithrecruitment]
      'Pinkal (06-Mar-2013) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMasterunkid = dsList.Tables(0).Rows(0).Item(0)


'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cfcommon_master", "masterunkid", mintMasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            If mdtAllocation IsNot Nothing Then
                Dim objCommonAllocation As New clsCommonmaster_allocation
                objCommonAllocation._Masterunkid = mintMasterunkid
                objCommonAllocation._Userunkid = mintUserunkid
                objCommonAllocation._Isvoid = False
                objCommonAllocation._Voiduserunkid = -1
                objCommonAllocation._Voiddatetime = Nothing
                objCommonAllocation._Voidreason = ""

                objCommonAllocation._FormName = mstrFormName
                objCommonAllocation._Loginemployeeunkid = mintLoginEmployeeunkid
                objCommonAllocation._ClientIP = mstrClientIP
                objCommonAllocation._HostName = mstrHostName
                objCommonAllocation._Isweb = mblnIsWeb
                objCommonAllocation._AuditUserId = mintUserunkid
                objCommonAllocation._CompanyUnkid = mintCompanyUnkid
                objCommonAllocation._AuditDate = mdtAuditDate

                objCommonAllocation._xDataOp = objDataOperation
                objCommonAllocation.InsertAll(mdtAllocation)
            End If
            'Sohail (14 Nov 2019) -- End

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan [13-Nov-2020] -- End

            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.

            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [13-Nov-2020] -- End
            'Anjan (12 Oct 2011)-End 


            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [13-Nov-2020] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfcommon_master) </purpose>
    ''' 

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    'Public Function Update(Optional ByVal mdtAllocation As DataTable = Nothing) As Boolean
    Public Function Update(Optional ByVal mdtAllocation As DataTable = Nothing, Optional ByVal intJobunkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [13-Nov-2020] -- End

        'Sohail (14 Nov 2019) - [mdtAllocation]
        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'If isExist(mintMastertype, , mstrName, mintMasterunkid) Then
        If isExist(mintMastertype, , mstrName, mintMasterunkid, , intJobunkid, xDataOpr) Then
            'Gajanan [13-Nov-2020] -- End
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'If isExist(mintMastertype, mstrCode, , mintMasterunkid) Then
        If isExist(mintMastertype, mstrCode, , mintMasterunkid, , intJobunkid, xDataOpr) Then
            'Gajanan [13-Nov-2020] -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'objDataOperation = New clsDataOperation
        ''Anjan (12 Oct 2011)-Start
        ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        'objDataOperation.BindTransaction()
        ''Anjan (12 Oct 2011)-End 

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Gajanan [13-Nov-2020] -- End


        Try
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMasterunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@alias", SqlDbType.NVarChar, eZeeDataType.ALIAS_SIZE, mstrAlias.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMastertype.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGrp_Level.ToString)
            objDataOperation.AddParameter("@dependant_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependant_Limit.ToString)
            objDataOperation.AddParameter("@coursetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseTypeId.ToString)
            objDataOperation.AddParameter("@isrefreeallowed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsRefreeAllowed.ToString())
            objDataOperation.AddParameter("@issyncwithrecruitment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSyncWithrecruitment.ToString()) 'Sohail (22 Apr 2015)

            'Pinkal (06-Mar-2013) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@ismedicalbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnMedicalBenefit.ToString())

            'S.SANDEEP [21 NOV 2015] -- START
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, 2000, mstrDescription)
            'S.SANDEEP [21 NOV 2015] -- END

            'Nilay (03-Dec-2015) -- Start
            'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            objDataOperation.AddParameter("@ismandatoryfornewemp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsMandatoryForNewEmp.ToString())
            'Nilay (03-Dec-2015) -- End

            'Hemant (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            objDataOperation.AddParameter("@isuseinassetdeclaration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsUseInAssetDeclaration.ToString())
            'Hemant (06 Oct 2018) -- End

            'S.SANDEEP [09-OCT-2018] -- START
            objDataOperation.AddParameter("@istrainingrequisition", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForTrainingRequisition.ToString())
            'S.SANDEEP [09-OCT-2018] -- END


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            objDataOperation.AddParameter("@iscrbenefit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCRBenefit.ToString())
            objDataOperation.AddParameter("@minagelimit_crdependants", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinAgeLimitForCRBenefit.ToString())
            objDataOperation.AddParameter("@maxcnt_crdependants", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxCountForCRBenefit.ToString())
            'Pinkal (25-Oct-2018) -- End

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            objDataOperation.AddParameter("@jobmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid.ToString())
            'Gajanan [13-Nov-2020] -- END


            'strQ = "UPDATE cfcommon_master SET " & _
            '          "  code = @code" & _
            '          ", alias = @alias" & _
            '          ", name = @name" & _
            '          ", name1 = @name1" & _
            '          ", name2 = @name2" & _
            '          ", mastertype = @mastertype" & _
            '        ", qlevel = @qlevel " & _
            '          ", isactive = @isactive " & _
            '          ", dependant_limit = @dependant_limit " & _
            '          ", coursetypeid = @coursetypeid " & _
            '          ", isrefreeallowed = @isrefreeallowed " & _
            '          ", Syncdatetime  = NULL " & _
            '        "WHERE masterunkid = @masterunkid "

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.

            strQ = "UPDATE cfcommon_master SET " & _
                      "  code = @code" & _
                      ", alias = @alias" & _
                      ", name = @name" & _
                      ", name1 = @name1" & _
                      ", name2 = @name2" & _
                      ", mastertype = @mastertype" & _
                    ", qlevel = @qlevel " & _
                      ", isactive = @isactive " & _
                      ", dependant_limit = @dependant_limit " & _
                     ", coursetypeid = @coursetypeid " & _
                      ", isrefreeallowed = @isrefreeallowed " & _
                      ", Syncdatetime  = NULL " & _
                    ", ismedicalbenefit  = @ismedicalbenefit " & _
                    ", issyncwithrecruitment  = @issyncwithrecruitment " & _
                      ", description = @description " & _
                        ", ismandatoryfornewemp = @ismandatoryfornewemp " & _
                        ", istrainingrequisition = @istrainingrequisition " & _
                        ", isuseinassetdeclaration = @isuseinassetdeclaration " & _
                      ", iscrbenefit = @iscrbenefit " & _
                      ", minagelimit_crdependants = @minagelimit_crdependants " & _
                      ", maxcnt_crdependants = @maxcnt_crdependants " & _
                      ", jobmstunkid = @jobmstunkid " & _
                      " WHERE 1=1 "
         
            If intJobunkid > 0 Then
                strQ &= "AND jobmstunkid = @jobmstunkid and mastertype = @mastertype "
            Else
                strQ &= "AND masterunkid = @masterunkid "
            End If
            'Gajanan [13-Nov-2020] -- END


            'Pinkal (25-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[", iscrbenefit = @iscrbenefit " & _ ",    ", minagelimit_crdependants = @minagelimit_crdependants " & _maxcnt_crdependants = @maxcnt_crdependants " & _]

            'S.SANDEEP [09-OCT-2018] -- START {istrainingrequisition} -- END

            'Hemant (06 Oct 2018) -- [isuseinassetdeclaration]
            'Nilay (03-Dec-2015) -- ['Nilay (03-Dec-2015)]
            'Pinkal (06-Mar-2013) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "cfcommon_master", mintMasterunkid, "masterunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cfcommon_master", "masterunkid", mintMasterunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            If mdtAllocation IsNot Nothing Then
                Dim objCommonAllocation As New clsCommonmaster_allocation
                objCommonAllocation._Masterunkid = mintMasterunkid
                objCommonAllocation._Userunkid = mintUserunkid
                objCommonAllocation._Isvoid = False
                objCommonAllocation._Voiduserunkid = -1
                objCommonAllocation._Voiddatetime = Nothing
                objCommonAllocation._Voidreason = ""

                objCommonAllocation._FormName = mstrFormName
                objCommonAllocation._Loginemployeeunkid = mintLoginEmployeeunkid
                objCommonAllocation._ClientIP = mstrClientIP
                objCommonAllocation._HostName = mstrHostName
                objCommonAllocation._Isweb = mblnIsWeb
                objCommonAllocation._AuditUserId = mintUserunkid
                objCommonAllocation._CompanyUnkid = mintCompanyUnkid
                objCommonAllocation._AuditDate = mdtAuditDate

                objCommonAllocation._xDataOp = objDataOperation
                If objCommonAllocation.InsertAll(mdtAllocation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'Sohail (14 Nov 2019) -- End

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan [13-Nov-2020] -- End


            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [13-Nov-2020] -- End

            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [13-Nov-2020] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfcommon_master) </purpose>
    ''' 

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal intMasterTypeid As Integer = -1, Optional ByVal intJobunkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [13-Nov-2020] -- End
        '<To Do> when transaction coding are done.

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'If isUsed(intUnkid) Then
        If isUsed(intUnkid, xDataOpr) Then
            'Gajanan [13-Nov-2020] -- End
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this master entry. Reason: This master entry is in use.")
            Return False
        End If

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        If ConfigParameter._Object._AllowToSyncJobWithVacancyMaster AndAlso intMasterTypeid = CInt(enCommonMaster.VACANCY_MASTER) Then
            If isVancancyInUsed(intJobunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete this vacancy. Reason: This vacancy is in use.")
                Return False
            End If
        End If
        'Gajanan [13-Nov-2020] -- end


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.


        'objDataOperation = New clsDataOperation
        ''Anjan (12 Oct 2011)-Start
        ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        'objDataOperation.BindTransaction()
        ''Anjan (12 Oct 2011)-End 

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Gajanan [13-Nov-2020] -- End


        Try
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            strQ = " UPDATE cfcommon_master SET " & _
                        "  isactive = 0 " & _
                    "WHERE  "

            If intJobunkid > 0 Then
                strQ &= " jobmstunkid = @jobmstunkid "
                objDataOperation.AddParameter("@jobmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)
            Else
                strQ &= "masterunkid = @masterunkid "
            End If
            'Gajanan [13-Nov-2020] -- END



            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cfcommon_master", "masterunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan [13-Nov-2020] -- End
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [13-Nov-2020] -- End

            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [13-Nov-2020] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [13-Nov-2020] -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTable As DataSet = Nothing
        Dim blnIsUsed As Boolean = False

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Gajanan [13-Nov-2020] -- End

        Try



            If intUnkid > 0 Then


            strQ = "SELECT  TABLE_NAME AS TableName " & _
                            ",COLUMN_NAME " & _
                    "FROM    INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE   COLUMN_NAME IN ( 'advertisecategoryunkid' ,'allergiesunkid' , " & _
                                     "'conditionunkid' ,'benefitgroupunkid' , " & _
                                     "'bloodgroupunkid' ,'complexionunkid' , " & _
                                     "'disabilitiesunkid' , " & _
                                     "'employmenttypeunkid' , " & _
                                     "'employeementtypeunkid' ,'ethnicityunkid' , " & _
                                     "'eyecolorunkid' ,'hairunkid' , " & _
                                     "'idtypeunkid' ,'interviewtypeunkid' , " & _
                                     "'language1unkid' ,'language2unkid' , " & _
                                     "'language3unkid' ,'language4unkid' , " & _
                                     "'marital_statusunkid' , " & _
                                     "'maritalstatusunkid' , " & _
                                     "'membership_categoryunkid' , " & _
                                     "'paytypeunkid' ,'qualificationgroupunkid' , " & _
                                     "'relationunkid' ,'religionunkid' , " & _
                                     "'resultcodeunkid' ,'resultgroupunkid' , " & _
                                     "'shifttypeunkid' ,'skillcategoryunkid' , " & _
                                     "'titleunkid' ,'resourceunkid' , " & _
                                     "'asset_statusunkid' ,'assetunkid' , " & _
                                     "'lettermasterunkid','trainingmodeunkid','resourcemasterunkid' ) AND TABLE_NAME NOT LIKE 'at%' "
            'S.SANDEEP [09-OCT-2018] -- START {'trainingmodeunkid','resourcemasterunkid'} -- END

            dsTable = objDataOperation.ExecQuery(strQ, "Table")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each drRow As DataRow In dsTable.Tables(0).Rows
                strQ = "SELECT " & drRow.Item("COLUMN_NAME") & " FROM " & drRow.Item("TableName") & " WHERE  " & drRow.Item("COLUMN_NAME") & " = @masterunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables(0).Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            End If
            mstrMessage = ""
            Return blnIsUsed


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [13-Nov-2020] -- End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>


    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    'Public Function isExist(Optional ByVal intMasterTypeID As Integer = -1, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal intLanguageID As Integer = -1) As Boolean
    Public Function isExist(Optional ByVal intMasterTypeID As Integer = -1, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal intLanguageID As Integer = -1, Optional ByVal intJobunkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [13-Nov-2020] -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Gajanan [13-Nov-2020] -- End

        Try
            'Pinkal (06-Mar-2013) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '          "  masterunkid " & _
            '          ", code " & _
            '          ", alias " & _
            '          ", name " & _
            '          ", name1 " & _
            '          ", name2 " & _
            '          ", mastertype " & _
            '         ", qlevel " & _
            '          ", isactive " & _
            '          ", dependant_limit " & _
            '          ", coursetypeid " & _
            '          ", isrefreeallowed " & _
            '        "FROM cfcommon_master " & _
            '        "WHERE isactive = 1 "

            strQ = "SELECT " & _
                      "  masterunkid " & _
                      ", code " & _
                      ", alias " & _
                      ", name " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", mastertype " & _
                     ", qlevel " & _
                      ", isactive " & _
                     ", dependant_limit " & _
                     ", coursetypeid " & _
                     ", isrefreeallowed " & _
                    ", ISNULL(ismedicalbenefit,0) AS ismedicalbenefit " & _
                    ", ISNULL(istrainingrequisition,0) AS istrainingrequisition " & _
                      ", ISNULL(iscrbenefit,0) AS iscrbenefit " & _
                      ", ISNULL(minagelimit_crdependants,0) AS minagelimit_crdependants " & _
                      ", ISNULL(maxcnt_crdependants,0) AS maxcnt_crdependants " & _
                      " FROM cfcommon_master " & _
                      " WHERE isactive = 1 "

            'Pinkal (25-Oct-2018) --  'Enhancement - Implementing Claim & Request changes For NMB .[", ISNULL(iscrbenefit,0) AS iscrbenefit " & _   ", ISNULL(minagelimit_crdependants,0) AS minagelimit_crdependants " & _", ISNULL(maxcnt_crdependants,0) AS maxcnt_crdependants " & _]

            'S.SANDEEP [09-OCT-2018] -- START {istrainingrequisition} -- END

            'Pinkal (06-Mar-2013) -- End

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
            End If
            If strName.Length > 0 Then
                strQ &= "AND name = @name"
            End If

            If intMasterTypeID > 0 Then
                strQ &= " AND mastertype = @mastertypeID "
                objDataOperation.AddParameter("@mastertypeID", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeID)
            End If

            If intUnkid > 0 Then
                strQ &= " AND masterunkid <> @masterunkid"
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If intJobunkid > 0 Then
                strQ &= " AND jobmstunkid <> @jobmstunkid "
                objDataOperation.AddParameter("@jobmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@Language", SqlDbType.Int, eZeeDataType.INT_SIZE, intLanguageID)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [13-Nov-2020] -- End
        End Try
    End Function

    ''' <summary>
    ''' Purpose  : Get combo List for particular master
    ''' Modify By: Anjan
    ''' </summary>
    ''' <param name="strListName"> </param>
    ''' <returns>DataSet</returns>
    ''' <purpose> </purpose>
    Public Function getComboList(ByVal intMasterTypeID As enCommonMaster, _
                                 Optional ByVal blnNA As Boolean = False, _
                                    Optional ByVal strListName As String = "List", _
                                 Optional ByVal intLanguageID As Integer = -1, _
                                 Optional ByVal IsRefreeAllowed As Boolean = False, _
                                 Optional ByVal blnIncludeAllRelation As Boolean = False, _
                                Optional ByVal iCompanyId As Integer = 0, _
Optional ByVal blnOnlyUsedInAssetDeclaration As Boolean = False, _
                                 Optional ByVal blnOnlyForTrainingRequsition As Boolean = False) As DataSet 'S.SANDEEP [10-MAY-2017] -- START {iCompanyId} -- END
        'S.SANDEEP [09-OCT-2018] -- START {blnOnlyForTrainingRequsition} -- END

        'Shani(24-Aug-2015) -- [Optional ByVal blnIncludeAllRelation As Boolean = False]

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try
            '<To Do> uncomment when language is ready
            'If Not intLanguageID > 0 Then
            '    intLanguageID = User._Object._LanguageId
            'End If


            'S.SANDEEP [10-MAY-2017] -- START
            Dim sDataBase As String = String.Empty
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
            If iCompanyId > 0 Then
                strQ = "SELECT database_name FROM cffinancial_year_tran WHERE companyunkid = @companyunkid AND isclosed = 0 "
                Dim dData As New DataSet
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompanyId)

                dData = objDataOperation.ExecQuery(strQ, "List")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dData.Tables(0).Rows.Count > 0 Then sDataBase = dData.Tables(0).Rows(0).Item("database_name")
            End If
            If sDataBase = "" Then sDataBase = FinancialYear._Object._DatabaseName
            strQ = "" : objDataOperation.ClearParameters()
            'S.SANDEEP [10-MAY-2017] -- END




            If blnNA Then

                'S.SANDEEP [21 NOV 2015] -- START
                'strQ = "SELECT 0 AS masterunkid " & _
                '          ",  ' ' + @Select AS name " & _
                '       "UNION "

                strQ = "SELECT 0 AS masterunkid " & _
                          ",  ' ' + @Select AS name " & _
                       ",'' AS description " & _
                       ",99 AS coursetypeid " & _
                       "UNION "
                'S.SANDEEP [04 OCT 2016] -- START {coursetypeid 99 -- Filter Purpose} -- END

                'S.SANDEEP [21 NOV 2015] -- END
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            End If


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'strQ &= "SELECT masterunkid " & _
            '            ", CASE @Language WHEN 1 THEN name1 " & _
            '                        "WHEN 2 THEN name2 " & _
            '                        "ELSE name END AS name " & _
            '         "FROM cfcommon_master " & _
            '         "WHERE mastertype = @mastertypeId  " & _
            '         "AND isactive = 1 " & _
            '         "ORDER BY  name "

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
            'strQ &= "SELECT masterunkid " & _
            '            ", CASE @Language WHEN 1 THEN name1 " & _
            '                        "WHEN 2 THEN name2 " & _
            '                        "ELSE name END AS name " & _
            '        ", ISNULL(description,'') AS description " & _
            '        ", ISNULL(coursetypeid,0) AS coursetypeid " & _
            '         "FROM cfcommon_master " & _
            '         "WHERE mastertype = @mastertypeId  " & _
            '         "AND isactive = 1 "

            strQ &= "SELECT masterunkid " & _
                        ", CASE @Language WHEN 1 THEN name1 " & _
                                    "WHEN 2 THEN name2 " & _
                                    "ELSE name END AS name " & _
                    ", ISNULL(description,'') AS description " & _
                    ", ISNULL(coursetypeid,0) AS coursetypeid " & _
                     "FROM " & sDataBase & "..cfcommon_master " & _
                     "WHERE mastertype = @mastertypeId  " & _
                     "AND isactive = 1 "
            'S.SANDEEP [10-MAY-2017] -- END


            
            'S.SANDEEP [04 OCT 2016] -- START {coursetypeid} -- END

            If intMasterTypeID = enCommonMaster.RELATIONS AndAlso blnIncludeAllRelation = False Then
                strQ &= " AND ISNULL(isrefreeallowed,0) = @isrefreeallowed "
                objDataOperation.AddParameter("@isrefreeallowed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IsRefreeAllowed)
            End If

            'Hemant (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            If intMasterTypeID = enCommonMaster.RELATIONS AndAlso blnOnlyUsedInAssetDeclaration = True Then
                strQ &= " AND ISNULL(isuseinassetdeclaration,0) = @isuseinassetdeclaration "
                objDataOperation.AddParameter("@isuseinassetdeclaration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnOnlyUsedInAssetDeclaration)
            End If
            'Hemant (06 Oct 2018) -- End

            'S.SANDEEP [09-OCT-2018] -- START
            If intMasterTypeID = enCommonMaster.TRAINING_RESOURCES AndAlso blnOnlyForTrainingRequsition = True Then
                strQ &= " AND ISNULL(istrainingrequisition,0) = @istrainingrequisition "
                objDataOperation.AddParameter("@istrainingrequisition", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnOnlyForTrainingRequsition)
            End If
            'S.SANDEEP [09-OCT-2018] -- END

            strQ &= "ORDER BY  name "



            'Pinkal (06-Feb-2012) -- End



            objDataOperation.AddParameter("@Language", SqlDbType.Int, eZeeDataType.INT_SIZE, intLanguageID)
            objDataOperation.AddParameter("@mastertypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeID)


            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    Public Function isSyncJobExist(Optional ByVal intMasterTypeID As Integer = -1, Optional ByVal intJobunkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  masterunkid " & _
                      ", code " & _
                      ", alias " & _
                      ", name " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", mastertype " & _
                     ", qlevel " & _
                      ", isactive " & _
                     ", dependant_limit " & _
                     ", coursetypeid " & _
                     ", isrefreeallowed " & _
                    ", ISNULL(ismedicalbenefit,0) AS ismedicalbenefit " & _
                    ", ISNULL(istrainingrequisition,0) AS istrainingrequisition " & _
                      ", ISNULL(iscrbenefit,0) AS iscrbenefit " & _
                      ", ISNULL(minagelimit_crdependants,0) AS minagelimit_crdependants " & _
                      ", ISNULL(maxcnt_crdependants,0) AS maxcnt_crdependants " & _
                      " FROM cfcommon_master " & _
                      " WHERE isactive = 1 "

            If intMasterTypeID > 0 Then
                strQ &= " AND mastertype = @mastertypeID "
                objDataOperation.AddParameter("@mastertypeID", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeID)
            End If


            If intJobunkid > 0 Then
                strQ &= " AND jobmstunkid = @jobmstunkid "
                objDataOperation.AddParameter("@jobmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSyncJobExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function isVancancyInUsed(Optional ByVal intJobunkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [13-Nov-2020] -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [13-Nov-2020] -- Start
        'Final Recruitment UAT v1 Changes.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Gajanan [13-Nov-2020] -- End

        Try
            strQ = "SELECT cfcommon_master.jobmstunkid from cfcommon_master " & _
                  "join hrjob_master on cfcommon_master.jobmstunkid = hrjob_master.jobunkid and cfcommon_master.mastertype = " & enCommonMaster.VACANCY_MASTER & " and cfcommon_master.isactive = 1 " & _
                  "join rcvacancy_master on rcvacancy_master.vacancytitle = cfcommon_master.masterunkid and rcvacancy_master.isvoid = 0 " & _
                  "WHERE 1=1 "


            If intJobunkid > 0 Then
                strQ &= " AND hrjob_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [13-Nov-2020] -- Start
            'Final Recruitment UAT v1 Changes.
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [13-Nov-2020] -- End
        End Try
    End Function
    'Gajanan [13-Nov-2020] -- END


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Public Function GetQualificationGrpFromLevel(ByVal mintLevel As Integer, Optional ByVal mblnFlag As Boolean = False, _
                                                 Optional ByVal mstrFilter As String = "" _
                                                 ) As DataSet
        'Hemant (07 Oct 2019) -- [mstrFilter]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            If mblnFlag Then
                strQ = " SELECT 0 masterunkid,'' code,@Select as name  " & _
                       " , -1 qlevel " & _
                          " UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            End If

            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 6 : On shortlisting screen, remove the qualification group levels and allow the user to select the qualification group from the dropdown freely without selecting the qualification group level.)
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            'If mintLevel = -1 Then
            '    strQ &= " SELECT " & _
            '             " masterunkid " & _
            '             ",code,[name] " & _
            '             " FROM dbo.cfcommon_master " & _
            '             " WHERE  isactive =1 " & _
            '             " AND mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP
            'Else
            '    'Hemant (27 Sep 2019) -- End
            '    strQ &= "SELECT masterunkid,code,[name] FROM dbo.cfcommon_master WHERE qlevel = @qlevel AND  isactive =1 AND mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP
            'End If 'Hemant (27 Sep 2019)
                strQ &= " SELECT " & _
                         " masterunkid " & _
                         ",code,[name] " & _
                         ",qlevel " & _
                         " FROM dbo.cfcommon_master " & _
                         " WHERE  isactive =1 " & _
                         " AND mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP
            'Hemant (30 Oct 2019) -- [qlevel]

            If mintLevel > -1 Then
                strQ &= " AND qlevel = @qlevel "
            End If

            If mstrFilter.Trim <> "" Then
                strQ &= mstrFilter
            End If
            'Hemant (07 Oct 2019) -- End

            objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevel)
            dsList = objDataOperation.ExecQuery(strQ, "List")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQualificationGrpFromLevel", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (12-Oct-2011) -- End



    'Pinkal (10-Mar-2011) -- Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetCommonMasterUnkId(ByVal intMasterTypeId As Integer, ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " masterunkid " & _
                      "  FROM cfcommon_master " & _
                      " WHERE mastertype =  @mastertype and name = @name "
            'S.SANDEEP [16 Jan 2016] -- START
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [16 Jan 2016] -- END

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("masterunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommonMasterUnkId", mstrModuleName)
        End Try
        Return -1
    End Function
    'Pinkal (10-Mar-2011) -- End

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetDependantsAgeLimit(Optional ByVal StrList As String = "List", Optional ByVal intRelationUnkid As Integer = -1) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                     " masterunkid AS Id " & _
                     ",ISNULL(name,'') AS RName " & _
                     ",ISNULL(dependant_limit,0) AS Age " & _
                    ",ISNULL(ismedicalbenefit,0) AS ismedicalbenefit " & _
                     ",ISNULL(iscrbenefit,0) AS iscrbenefit " & _
                     ",ISNULL(minagelimit_crdependants,0) AS minagelimit_crdependants " & _
                     ",ISNULL(maxcnt_crdependants,0) AS maxcnt_crdependants " & _
                     " FROM cfcommon_master " & _
                     " WHERE isactive = 1 AND mastertype = " & enCommonMaster.RELATIONS

            'Pinkal (25-Oct-2018) --  'Enhancement - Implementing Claim & Request changes For NMB .[ ",ISNULL(minagelimit_crdependants,0) AS minagelimit_crdependants " & _ ",ISNULL(iscrbenefit,0) AS iscrbenefit " & _   ",ISNULL(maxcnt_crdependants,0) AS maxcnt_crdependants " & _]

            If intRelationUnkid > 0 Then
                StrQ &= " AND masterunkid = '" & intRelationUnkid & "'"
            End If

            If StrList.Trim.Length <= 0 Then StrList = "List"

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDependantsAgeLimit", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'Pinkal (10-Mar-2011) -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
    Public Function GetCommaSeparateCommonMasterData(ByVal strColumnName As String, ByVal strMasterUnkId As String, ByVal intMasterTypeId As Integer) As String
        Dim strQ As String = ""
        Dim mstrCommonMasterData As String = ""
        Dim dsList As DataSet = Nothing
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = "SELECT STUFF(( SELECT  ',' + CAST( " & strColumnName & " AS VARCHAR(MAX))  " & _
                        " FROM cfcommon_master " & _
                        " WHERE isactive = 1 " & _
                        " AND mastertype = " & CInt(intMasterTypeId) & " " & _
                        " AND masterunkid In (" & strMasterUnkId & ")" & _
                        " ORDER BY masterunkid " & _
                        " FOR  XML PATH('')), 1, 1, '')  AS items "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrCommonMasterData = dsList.Tables(0).Rows(0)("items")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommaSeparateCommonMasterData", mstrModuleName)
        End Try
        Return mstrCommonMasterData
    End Function
    'Hemant (07 Oct 2019) -- End


    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Public Function getDesciplineRefNoComboList(ByVal intEmployeeunkid As Integer, _
                                                Optional ByVal strListName As String = "List" _
                                                ) As DataSet

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try
            Dim sDataBase As String = String.Empty
            strQ = "SELECT reference_no,disciplinefileunkid from hrdiscipline_file_master WHERE isvoid = 0 and involved_employeeunkid = @employeeunkid "
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getDesciplineRefNoComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function
    'Gajanan [18-May-2020] -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this master entry. Reason: This master entry is in use.")
			Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot delete this vacancy. Reason: This vacancy is in use.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
