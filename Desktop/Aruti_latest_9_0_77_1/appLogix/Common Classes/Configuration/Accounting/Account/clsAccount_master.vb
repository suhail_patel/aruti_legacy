﻿'************************************************************************************************************************************
'Class Name : clsAccount_master.vb
'Purpose    :
'Date       :19/08/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsAccount_master
    Private Shared ReadOnly mstrModuleName As String = "clsAccount_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAccountunkid As Integer
    Private mstrAccount_Code As String = String.Empty
    Private mstrAccount_Name As String = String.Empty
    Private mintAccountgroup_Id As Integer
    Private mblnIsactive As Boolean = True
    Private mstrAccount_Name1 As String = String.Empty
    Private mstrAccount_Name2 As String = String.Empty
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Accountunkid() As Integer
        Get
            Return mintAccountunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set account_code
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Account_Code() As String
        Get
            Return mstrAccount_Code
        End Get
        Set(ByVal value As String)
            mstrAccount_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set account_name
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Account_Name() As String
        Get
            Return mstrAccount_Name
        End Get
        Set(ByVal value As String)
            mstrAccount_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountgroup_id
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Accountgroup_Id() As Integer
        Get
            Return mintAccountgroup_Id
        End Get
        Set(ByVal value As Integer)
            mintAccountgroup_Id = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set account_name1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Account_Name1() As String
        Get
            Return mstrAccount_Name1
        End Get
        Set(ByVal value As String)
            mstrAccount_Name1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set account_name2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Account_Name2() As String
        Get
            Return mstrAccount_Name2
        End Get
        Set(ByVal value As String)
            mstrAccount_Name2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  accountunkid " & _
              ", account_code " & _
              ", account_name " & _
              ", accountgroup_id " & _
              ", isactive " & _
              ", account_name1 " & _
              ", account_name2 " & _
             "FROM praccount_master " & _
             "WHERE accountunkid = @accountunkid "

            objDataOperation.AddParameter("@accountunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAccountUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintaccountunkid = CInt(dtRow.Item("accountunkid"))
                mstraccount_code = dtRow.Item("account_code").ToString
                mstraccount_name = dtRow.Item("account_name").ToString
                mintaccountgroup_id = CInt(dtRow.Item("accountgroup_id"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstraccount_name1 = dtRow.Item("account_name1").ToString
                mstraccount_name2 = dtRow.Item("account_name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intAccountID As Integer = 0, Optional ByVal intAccGroupID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  accountunkid " & _
                      ", account_code " & _
                      ", account_name " & _
                      ", accountgroup_id " & _
                      ", isactive " & _
                      ", account_name1 " & _
                      ", account_name2 " & _
                "FROM praccount_master " & _
                "WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If
            If intAccountID > 0 Then
                strQ &= " AND accountunkid = @accountunkid "
                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccountID)
            End If
            If intAccGroupID > 0 Then
                strQ &= " AND accountgroup_id = @accountgroup_id "
                objDataOperation.AddParameter("@accountgroup_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccGroupID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (praccount_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrAccount_Code, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Account Code is already defined. Please define new Account Code.")
            Return False
        End If
        If isExist("", mstrAccount_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Account Name is already defined. Please define new Account Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@account_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_code.ToString)
            objDataOperation.AddParameter("@account_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_name.ToString)
            objDataOperation.AddParameter("@accountgroup_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccountgroup_id.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@account_name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_name1.ToString)
            objDataOperation.AddParameter("@account_name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_name2.ToString)

            strQ = "INSERT  INTO praccount_master " & _
                            "( account_code , " & _
                              "account_name , " & _
                              "accountgroup_id , " & _
                              "isactive , " & _
                              "account_name1 , " & _
                              "account_name2 " & _
                            ") " & _
                    "VALUES  ( @account_code , " & _
                              "@account_name , " & _
                              "@accountgroup_id , " & _
                              "@isactive , " & _
                              "@account_name1 , " & _
                              "@account_name2 " & _
                            ") ; " & _
                    "SELECT  @@identity "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAccountUnkId = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_master", "accountunkid", mintAccountunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (praccount_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrAccount_Code, "", mintAccountunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Account Code is already defined. Please define new Account Code.")
            Return False
        End If
        If isExist("", mstrAccount_Name, mintAccountunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Account Name is already defined. Please define new Account Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@accountunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccountunkid.ToString)
            objDataOperation.AddParameter("@account_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_code.ToString)
            objDataOperation.AddParameter("@account_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_name.ToString)
            objDataOperation.AddParameter("@accountgroup_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccountgroup_id.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@account_name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_name1.ToString)
            objDataOperation.AddParameter("@account_name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraccount_name2.ToString)

            StrQ = "UPDATE praccount_master SET " & _
              "  account_code = @account_code" & _
              ", account_name = @account_name" & _
              ", accountgroup_id = @accountgroup_id" & _
              ", isactive = @isactive" & _
              ", account_name1 = @account_name1" & _
              ", account_name2 = @account_name2 " & _
            "WHERE accountunkid = @accountunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_master", mintAccountunkid, "accountunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso objCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_master", "accountunkid", mintAccountunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (praccount_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "DELETE FROM praccount_master " & _
            '"WHERE accountunkid = @accountunkid "
            strQ = "UPDATE praccount_master SET " & _
                     " isactive = 0 " & _
            "WHERE accountunkid = @accountunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_master", "accountunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "Select accountunkid " & _
                    "FROM praccount_configuration " & _
                        "WHERE accountunkid = @accountunkid " & _
                        "AND isactive = 1 " & _
                    "UNION " & _
                    "Select accountunkid " & _
                        "FROM praccount_configuration_employee " & _
                        "WHERE accountunkid = @accountunkid " & _
                        "AND isactive = 1 " & _
                    "UNION " & _
                    "Select accountunkid " & _
                        "FROM praccount_configuration_costcenter " & _
                        "WHERE accountunkid = @accountunkid " & _
                        "AND isactive = 1 " 'Sohail (02 Aug 2011)

            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByRef intId As Integer = 0) As Boolean
        'Sohail (02 Aug 2017) - [intId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            intId = 0 'Sohail (02 Aug 2017)

            'Anjan (05 Oct 2011)-Start
            'Issue : Rutta wants to use deleted code and name again.
            'strQ = "SELECT " & _
            '  "  accountunkid " & _
            '  ", account_code " & _
            '  ", account_name " & _
            '  ", accountgroup_id " & _
            '  ", isactive " & _
            '  ", account_name1 " & _
            '  ", account_name2 " & _
            ' "FROM praccount_master " & _
            ' "WHERE 1 = 1 "

            strQ = "SELECT " & _
              "  accountunkid " & _
              ", account_code " & _
              ", account_name " & _
              ", accountgroup_id " & _
              ", isactive " & _
              ", account_name1 " & _
              ", account_name2 " & _
             "FROM praccount_master " & _
             "WHERE isactive = 1 "
            'Anjan (05 Oct 2011)-End

            If strCode.Length > 0 Then
                strQ &= " AND account_code = @account_code"
                objDataOperation.AddParameter("@account_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Length > 0 Then
                strQ &= " AND account_name = @account_name"
                objDataOperation.AddParameter("@account_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND accountunkid <> @accountunkid"
                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("accountunkid"))
            End If
            'Sohail (02 Aug 2017) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <param name="strListName"></param>
    ''' <param name="mblnFlag"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = True, Optional ByVal intAccountGrpID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            'If mblnFlag = True Then
            '    strQ = "SELECT 0 As accountunkid , '' AS account_code, @ItemName As  name  UNION "
            'End If
            'strQ &= "SELECT accountunkid, account_code, account_name AS name FROM praccount_master WHERE isactive =1 "

            If mblnFlag = True Then
                strQ = "SELECT 0 As accountunkid , '' AS account_code, @ItemName As  name, @ItemName AS accname  UNION "
            End If
            strQ &= "SELECT accountunkid, account_code, account_name AS name, account_code + ' - ' +  account_name as accname FROM praccount_master WHERE isactive =1 "

            'Pinkal (20-Nov-2018) -- End

            If intAccountGrpID > 0 Then
                strQ &= "AND accountgroup_id = @accountgroup_id "
                objDataOperation.AddParameter("@accountgroup_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccountGrpID.ToString)
            End If

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        End Try
    End Function

#Region " Message "
    '1, "This Account Code is already defined. Please define new Account Code."
    '2, "This Account Name is already defined. Please define new Account Name."
    '3, "Select"
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Account Code is already defined. Please define new Account Code.")
			Language.setMessage(mstrModuleName, 2, "This Account Name is already defined. Please define new Account Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class