﻿'************************************************************************************************************************************
'Class Name : clsCfUser_tracking_Tran.vb
'Purpose    :
'Date       :11/06/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsUser_tracking_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsUser_tracking_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTrackingunkid As Integer
    Private mintCompanyunkid As Integer
    Private mintYearunkid As Integer
    Private mintUserunkid As Integer
    Private mdtLogindate As Date
    Private mblnIsconfiguration As Boolean
    Private mstrIp As String = String.Empty
    Private mstrMachine As String = String.Empty
    Private mintModeid As Integer
    'S.SANDEEP [ 21 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private mintLogin_Modeid As Integer
    Private mstrDatabase_Name As String = String.Empty
    Private mdtLogout_Date As Date
    'S.SANDEEP [ 21 MAY 2012 ] -- END

    'Sohail (04 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnIsforcelogout As Boolean
    Private mstrLogout_Reason As String = String.Empty
    Private mintForce_Userunkid As Integer
    Private mdtRelogin_Date As Date
    'Sohail (04 Jul 2012) -- End
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trackingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Trackingunkid() As Integer
        Get
            Return mintTrackingunkid
        End Get
        Set(ByVal value As Integer)
            mintTrackingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set logindate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Logindate() As Date
        Get
            Return mdtLogindate
        End Get
        Set(ByVal value As Date)
            mdtLogindate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isConfiguration
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isconfiguration() As Boolean
        Get
            Return mblnIsconfiguration
        End Get
        Set(ByVal value As Boolean)
            mblnIsconfiguration = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Machine() As String
        Get
            Return mstrMachine
        End Get
        Set(ByVal value As String)
            mstrMachine = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ModeId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Modeid() As Integer
        Get
            Return mintModeid
        End Get
        Set(ByVal value As Integer)
            mintModeid = value
        End Set
    End Property

    'S.SANDEEP [ 21 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    ''' <summary>
    ''' Purpose: Get or Set login_modeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Login_Modeid() As Integer
        Get
            Return mintLogin_Modeid
        End Get
        Set(ByVal value As Integer)
            mintLogin_Modeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set database_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Database_Name() As String
        Get
            Return mstrDatabase_Name
        End Get
        Set(ByVal value As String)
            mstrDatabase_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set logout_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Logout_Date() As Date
        Get
            Return mdtLogout_Date
        End Get
        Set(ByVal value As Date)
            mdtLogout_Date = Value
        End Set
    End Property
    'S.SANDEEP [ 21 MAY 2012 ] -- END

    'Sohail (04 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set isforcelogout
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isforcelogout() As Boolean
        Get
            Return mblnIsforcelogout
        End Get
        Set(ByVal value As Boolean)
            mblnIsforcelogout = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set logout_reason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Logout_Reason() As String
        Get
            Return mstrLogout_Reason
        End Get
        Set(ByVal value As String)
            mstrLogout_Reason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set force_userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Force_Userunkid() As Integer
        Get
            Return mintForce_Userunkid
        End Get
        Set(ByVal value As Integer)
            mintForce_Userunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relogin_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Relogin_Date() As Date
        Get
            Return mdtRelogin_Date
        End Get
        Set(ByVal value As Date)
            mdtRelogin_Date = Value
        End Set
    End Property
    'Sohail (04 Jul 2012) -- End
#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'strQ = "SELECT " & _
            '       "  trackingunkid " & _
            '       ", companyunkid " & _
            '       ", yearunkid " & _
            '       ", userunkid " & _
            '       ", logindate " & _
            '       ", isConfiguration " & _
            '       ", ip " & _
            '       ", machine " & _
            '       ", ModeId " & _
            '       "FROM hrmsconfiguration..cfuser_tracking_tran " & _
            '       "WHERE trackingunkid = @trackingunkid "
            strQ = "SELECT " & _
              "  trackingunkid " & _
              ", companyunkid " & _
              ", yearunkid " & _
              ", userunkid " & _
              ", logindate " & _
              ", isConfiguration " & _
              ", ip " & _
              ", machine " & _
              ", ModeId " & _
                   ", login_modeid " & _
                   ", database_name " & _
                   ", logout_date " & _
                        ", ISNULL(isforcelogout, 0) AS isforcelogout " & _
                        ", ISNULL(logout_reason, '') AS logout_reason " & _
                        ", ISNULL(force_userunkid, 0) AS force_userunkid " & _
                        ", relogin_date " & _
             "FROM hrmsconfiguration..cfuser_tracking_tran " & _
                 "WHERE trackingunkid = @trackingunkid " 'Sohail (04 Jul 2012) - [isforcelogout, logout_reason, force_userunkid, relogin_date]
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            objDataOperation.AddParameter("@trackingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrackingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrackingunkid = CInt(dtRow.Item("trackingunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mdtLogindate = dtRow.Item("logindate")
                mblnIsconfiguration = CBool(dtRow.Item("isConfiguration"))
                mstrIp = dtRow.Item("ip").ToString
                mstrMachine = dtRow.Item("machine").ToString
                mintModeid = CInt(dtRow.Item("ModeId"))
                'S.SANDEEP [ 21 MAY 2012 ] -- START
                'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                mintLogin_Modeid = CInt(dtRow.Item("login_modeid"))
                mstrDatabase_Name = dtRow.Item("database_name").ToString
                If IsDBNull(dtRow.Item("logout_date")) = False Then
                    mdtLogout_Date = dtRow.Item("logout_date")
                Else
                    mdtLogout_Date = Nothing
                End If
                'S.SANDEEP [ 21 MAY 2012 ] -- END

                'Sohail (04 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                mblnIsforcelogout = CBool(dtRow.Item("isforcelogout"))
                mstrLogout_Reason = dtRow.Item("logout_reason").ToString
                mintForce_Userunkid = CInt(dtRow.Item("force_userunkid"))
                If IsDBNull(dtRow.Item("relogin_date")) = True Then
                    mdtRelogin_Date = Nothing
                Else
                    mdtRelogin_Date = dtRow.Item("relogin_date")
                End If
                'Sohail (04 Jul 2012) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intUserUnkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'strQ = "SELECT " & _
            '  "  trackingunkid " & _
            '  ", companyunkid " & _
            '  ", yearunkid " & _
            '  ", cfuser_tracking_tran.userunkid " & _
            '  ", logindate " & _
            '  ", isConfiguration " & _
            '  ", ip " & _
            '  ", machine " & _
            '  ", ModeId " & _
            ' "FROM hrmsconfiguration..cfuser_tracking_tran "

            strQ = "SELECT " & _
              "  trackingunkid " & _
              ", companyunkid " & _
              ", yearunkid " & _
              ", cfuser_tracking_tran.userunkid " & _
              ", logindate " & _
              ", isConfiguration " & _
              ", ip " & _
              ", machine " & _
              ", ModeId " & _
                  ", login_modeid " & _
                  ", database_name " & _
                  ", logout_date " & _
                        ", ISNULL(isforcelogout, 0) AS isforcelogout " & _
                        ", ISNULL(logout_reason, '') AS logout_reason " & _
                        ", ISNULL(force_userunkid, 0) AS force_userunkid " & _
                        ", relogin_date " & _
             "FROM hrmsconfiguration..cfuser_tracking_tran " 'Sohail (04 Jul 2012) - [isforcelogout, logout_reason, force_userunkid, relogin_date]
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            If intUserUnkid > 0 Then
                strQ &= " WHERE cfuser_tracking_tran.userunkid  = @userunkid"
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfuser_tracking_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            objDataOperation.AddParameter("@isConfiguration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsconfiguration.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine.ToString)
            objDataOperation.AddParameter("@ModeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            objDataOperation.AddParameter("@login_modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogin_Modeid.ToString)
            objDataOperation.AddParameter("@database_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatabase_Name.ToString)
            If mdtLogout_Date <> Nothing Then
                objDataOperation.AddParameter("@logout_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogout_Date)
            Else
                objDataOperation.AddParameter("@logout_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@isforcelogout", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsforcelogout.ToString)
            objDataOperation.AddParameter("@logout_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrLogout_Reason.ToString)
            objDataOperation.AddParameter("@force_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintForce_Userunkid.ToString)
            If mdtRelogin_Date <> Nothing Then
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRelogin_Date)
            Else
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (04 Jul 2012) -- End

            'strQ = "INSERT INTO hrmsconfiguration..cfuser_tracking_tran ( " & _
            '      "  companyunkid " & _
            '      ", yearunkid " & _
            '      ", userunkid " & _
            '      ", logindate " & _
            '      ", isConfiguration " & _
            '      ", ip " & _
            '      ", machine " & _
            '      ", ModeId" & _
            '    ") VALUES (" & _
            '      "  @companyunkid " & _
            '      ", @yearunkid " & _
            '      ", @userunkid " & _
            '      ", @logindate " & _
            '      ", @isConfiguration " & _
            '      ", @ip " & _
            '      ", @machine " & _
            '      ", @ModeId" & _
            '    "); SELECT @@identity"

            strQ = "INSERT INTO hrmsconfiguration..cfuser_tracking_tran ( " & _
              "  companyunkid " & _
              ", yearunkid " & _
              ", userunkid " & _
              ", logindate " & _
              ", isConfiguration " & _
              ", ip " & _
              ", machine " & _
              ", ModeId" & _
                       ", login_modeid " & _
                       ", database_name " & _
                       ", logout_date " & _
                        ", isforcelogout " & _
                        ", logout_reason " & _
                        ", force_userunkid " & _
                        ", relogin_date" & _
            ") VALUES (" & _
              "  @companyunkid " & _
              ", @yearunkid " & _
              ", @userunkid " & _
              ", @logindate " & _
              ", @isConfiguration " & _
              ", @ip " & _
              ", @machine " & _
              ", @ModeId" & _
                       ", @login_modeid " & _
                       ", @database_name " & _
                       ", @logout_date" & _
                        ", @isforcelogout " & _
                        ", @logout_reason " & _
                        ", @force_userunkid " & _
                        ", @relogin_date" & _
            "); SELECT @@identity" 'Sohail (04 Jul 2012) - [isforcelogout, logout_reason, force_userunkid, relogin_date]
            'S.SANDEEP [ 21 MAY 2012 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrackingunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfuser_tracking_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintTrackingunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@trackingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrackingunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            objDataOperation.AddParameter("@isConfiguration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsconfiguration.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine.ToString)
            objDataOperation.AddParameter("@ModeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            objDataOperation.AddParameter("@login_modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogin_Modeid.ToString)
            objDataOperation.AddParameter("@database_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatabase_Name.ToString)
            If mdtLogout_Date <> Nothing Then
                objDataOperation.AddParameter("@logout_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogout_Date)
            Else
                objDataOperation.AddParameter("@logout_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@isforcelogout", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsforcelogout.ToString)
            objDataOperation.AddParameter("@logout_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrLogout_Reason.ToString)
            objDataOperation.AddParameter("@force_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintForce_Userunkid.ToString)
            If mdtRelogin_Date <> Nothing Then
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRelogin_Date)
            Else
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (04 Jul 2012) -- End

            'strQ = "UPDATE hrmsconfiguration..cfuser_tracking_tran SET " & _
            '       "  companyunkid = @companyunkid" & _
            '       ", yearunkid = @yearunkid" & _
            '       ", userunkid = @userunkid" & _
            '       ", logindate = @logindate" & _
            '       ", isConfiguration = @isConfiguration" & _
            '       ", ip = @ip" & _
            '       ", machine = @machine" & _
            '       ", ModeId = @ModeId " & _
            '       "WHERE trackingunkid = @trackingunkid "

            strQ = "UPDATE hrmsconfiguration..cfuser_tracking_tran SET " & _
              "  companyunkid = @companyunkid" & _
              ", yearunkid = @yearunkid" & _
              ", userunkid = @userunkid" & _
              ", logindate = @logindate" & _
              ", isConfiguration = @isConfiguration" & _
              ", ip = @ip" & _
              ", machine = @machine" & _
              ", ModeId = @ModeId " & _
                   ", login_modeid = @login_modeid" & _
                   ", database_name = @database_name " & _
                   ", logout_date = @logout_date " & _
                            ", isforcelogout = @isforcelogout" & _
                            ", logout_reason = @logout_reason" & _
                            ", force_userunkid = @force_userunkid" & _
                            ", relogin_date = @relogin_date " & _
            "WHERE trackingunkid = @trackingunkid " 'Sohail (04 Jul 2012) - [isforcelogout, logout_reason, force_userunkid, relogin_date]

            'S.SANDEEP [ 21 MAY 2012 ] -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfuser_tracking_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM hrmsconfiguration..cfuser_tracking_tran " & _
            "WHERE trackingunkid = @trackingunkid "

            objDataOperation.AddParameter("@trackingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@trackingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''

    Public Function isUserLoginExist(ByVal mintuserunkid As Integer, _
                                     ByVal mstrIPAddress As String, _
                                     ByVal eLoginMode As enLogin_Mode, _
                                     ByVal intCompanyId As Integer, _
                                     ByVal intYearId As Integer, _
                                     Optional ByVal dtLoingDate As Date = Nothing) As Integer 'S.SANDEEP [ 10 JULY 2012 ] -- START -- END
        'Public Function isUserLoginExist(ByVal mintuserunkid As Integer, _
        '                                 ByVal mstrIPAddress As String, _
        '                                 ByVal eLoginMode As enLogin_Mode, _
        '                                 Optional ByVal dtLoingDate As Date = Nothing) As Integer 'S.SANDEEP [ 04 JULY 2012 ] -- START -- END
        'Public Function isUserLoginExist(ByVal mintuserunkid As Integer, _
        '                                 ByVal mstrIPAddress As String, _
        '                                 ByVal eLoginMode As enLogin_Mode) As Integer 'S.SANDEEP [ 21 MAY 2012 ] -- START -- END
        'Public Function isUserLoginExist(ByVal mintuserunkid As Integer, ByVal blnisconfiguration As Boolean, ByVal ModeID As Integer, ByVal mdtdate As Date) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trackingunkid " & _
              ", companyunkid " & _
              ", yearunkid " & _
              ", userunkid " & _
              ", logindate " & _
              ", isConfiguration " & _
              ", ip " & _
              ", machine " & _
              ", ModeId " & _
             "FROM hrmsconfiguration..cfuser_tracking_tran " & _
             "WHERE userunkid = @userunkid " & _
                   " AND ip LIKE @IP " & _
                   " AND logout_date IS NULL " & _
             " AND login_modeid = @login_modeid " & _
             " AND companyunkid = @CompanyId " & _
             " AND yearunkid = @YearId "

            If dtLoingDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(dtLoingDate).ToString & "'"
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid)
            objDataOperation.AddParameter("@IP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIPAddress)
            objDataOperation.AddParameter("@login_modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eLoginMode)
            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intCompanyId <= 0, -1, intCompanyId))
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intYearId <= 0, -1, intYearId))

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'If mintYearunkid > 0 And mintCompanyunkid > 0 Then
            '    strQ &= " AND yearunkid = @yearunkid " & _
            '                " AND companyunkid = @companyunkid "

            '    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid)
            '    objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid)

            'End If
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("trackingunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return -1
    End Function

    'Pinkal (23-Jun-2011) -- Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUserLog(ByVal strTableName As String, ByVal intUserUnkid As Integer, ByVal gApplicationType As enArutiApplicatinType) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'strQ = "SELECT " & _
            '  "  trackingunkid " & _
            '  ", hrmsconfiguration..cfuser_tracking_tran.companyunkid " & _
            '  ", ISNULL(hrmsconfiguration..cfcompany_master.name,'') companyname " & _
            '  ", hrmsconfiguration..cfuser_tracking_tran.yearunkid " & _
            '  ", ISNULL(hrmsconfiguration..cffinancial_year_tran.database_name,'') yearName " & _
            '  ", hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
            '  ", hrmsconfiguration..cfuser_master.username " & _
            '  ", logindate " & _
            '  ", isConfiguration " & _
            '  ", ip " & _
            '  ", machine " & _
            '  ", modeid " & _
            '  ", CASE WHEN modeid = 1 THEN @Login " & _
            '  "       WHEN modeid = 2 THEN @PwdChange" & _
            '  " End as remark " & _
            ' " FROM hrmsconfiguration..cfuser_tracking_tran " & _
            ' " LEFT JOIN hrmsconfiguration..cfcompany_master ON hrmsconfiguration..cfuser_tracking_tran.companyunkid = hrmsconfiguration..cfcompany_master.companyunkid " & _
            ' " LEFT JOIN hrmsconfiguration..cffinancial_year_tran ON hrmsconfiguration..cfuser_tracking_tran.yearunkid = hrmsconfiguration..cffinancial_year_tran.yearunkid " & _
            ' " JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = hrmsconfiguration..cfuser_tracking_tran.userunkid "

            strQ = "SELECT " & _
              "  trackingunkid " & _
              ", hrmsconfiguration..cfuser_tracking_tran.companyunkid " & _
                   " ,ISNULL(database_name,'') AS database_name " & _
              ", hrmsconfiguration..cfuser_tracking_tran.yearunkid " & _
                   " ,hrmsconfiguration..cfuser_tracking_tran.userunkid "
            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= " ,CASE WHEN login_modeid = " & enLogin_Mode.EMP_SELF_SERVICE & " THEN " & _
                                            FinancialYear._Object._DatabaseName & "..hremployee_master.firstname + ' ' +" & _
                                            FinancialYear._Object._DatabaseName & "..hremployee_master.surname " & _
                            "       WHEN login_modeid <> " & enLogin_Mode.EMP_SELF_SERVICE & " THEN " & _
                            "               hrmsconfiguration..cfuser_master.username " & _
                            "END AS username "
                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= " ,hrmsconfiguration..cfuser_master.username "
            End Select
            strQ &= " ,logindate " & _
                    " ,logout_date " & _
                    " ,CONVERT(CHAR(8),logindate,112) AS logindt " & _
                    " ,CONVERT(CHAR(8),logout_date,112) AS logoutdt " & _
              ", isConfiguration " & _
              ", ip " & _
              ", machine " & _
              ", modeid " & _
                    " ,login_modeid " & _
                    " ,hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
                    " ,CASE WHEN login_modeid = 1 THEN @Desktop " & _
                    "	    WHEN login_modeid = 2 THEN @ESS " & _
                    "	    WHEN login_modeid = 3 THEN @MSS END AS LOGIN_FROM " & _
                    " ,CASE WHEN (modeid = 1 AND logout_date IS NOT NULL) THEN @Logout " & _
                    "	    WHEN (modeid = 1 AND logout_date IS NULL) THEN @Login " & _
                    "	    WHEN (modeid = 2 AND logout_date IS NOT NULL) THEN @PwdChange + '/' +@Logout " & _
                    "	    WHEN (modeid = 2 AND logout_date IS NULL) THEN @PwdChange " & _
                    "  End AS REMARK " & _
             " FROM hrmsconfiguration..cfuser_tracking_tran " & _
                    "	LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
                    "		AND login_modeid IN (" & enLogin_Mode.DESKTOP & "," & enLogin_Mode.MGR_SELF_SERVICE & ") "
            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= "   LEFT JOIN " & FinancialYear._Object._DatabaseName & "..hremployee_master ON " & FinancialYear._Object._DatabaseName & "..hremployee_master.employeeunkid = hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
                            "   AND login_modeid IN (" & enLogin_Mode.EMP_SELF_SERVICE & ") "
            End Select
            strQ &= "WHERE 1 = 1 "
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            'If intUserUnkid > 1 Then
            '    strQ &= " AND hrmsconfiguration..cfuser_tracking_tran.userunkid  = @userunkid"
            '    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)
            'End If

            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= " AND isConfiguration = 1"
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= " AND isConfiguration = 0"
            End Select

            'strQ &= " Order by hrmsconfiguration..cfuser_tracking_tran.userunkid,logindate "

            objDataOperation.AddParameter("@Login", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Login"))
            objDataOperation.AddParameter("@PwdChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Password Change"))
            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            objDataOperation.AddParameter("@Logout", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Logged Out"))
            objDataOperation.AddParameter("@Desktop", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Desktop"))
            objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Employee Self Sevice"))
            objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Manager Self Service"))
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (23-Jun-2011) -- End
'Sohail (04 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetUserLog(ByVal strTableName As String _
                               , Optional ByVal gApplicationType As enArutiApplicatinType = enArutiApplicatinType.Aruti_Configuration _
                               , Optional ByVal intCompanyId As Integer = 0 _
                               , Optional ByVal strTranDatabaseName As String = "" _
                               , Optional ByVal strLoginModeIDs As String = "" _
                               , Optional ByVal intUserUnkid As Integer = 0 _
                               , Optional ByVal strFilter As String = "" _
                               ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try

            If strTranDatabaseName = "" Then strTranDatabaseName = FinancialYear._Object._DatabaseName

            strQ = "SELECT  trackingunkid " & _
                          ", hrmsconfiguration..cfuser_tracking_tran.companyunkid " & _
                          ", ISNULL(database_name, '') AS database_name " & _
                          ", hrmsconfiguration..cfuser_tracking_tran.yearunkid " & _
                          ", hrmsconfiguration..cfuser_tracking_tran.userunkid "

            Select Case gApplicationType

                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= ", CASE WHEN login_modeid =  " & enLogin_Mode.EMP_SELF_SERVICE & " " & _
                               "THEN " & strTranDatabaseName & "..hremployee_master.firstname + ' ' " & _
                                    "+ " & strTranDatabaseName & "..hremployee_master.surname " & _
                               "WHEN login_modeid <> " & enLogin_Mode.EMP_SELF_SERVICE & " " & _
                               "THEN hrmsconfiguration..cfuser_master.username " & _
                            "END AS username " & _
                            ", CASE WHEN login_modeid =  " & enLogin_Mode.EMP_SELF_SERVICE & " " & _
                            "THEN " & strTranDatabaseName & "..hremployee_master.employeecode  " & _
                            "WHEN login_modeid <> " & enLogin_Mode.EMP_SELF_SERVICE & " " & _
                               "THEN '' " & _
                            "END AS employeecode "

                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= " ,hrmsconfiguration..cfuser_master.username " & _
                            " ,'' AS employeecode "

            End Select

            strQ &= "      , logindate " & _
                          ", logout_date " & _
                          ", CONVERT(CHAR(8), logindate, 112) AS logindt " & _
                          ", CONVERT(CHAR(8), logout_date, 112) AS logoutdt " & _
                          ", isConfiguration " & _
                          ", ip " & _
                          ", machine " & _
                          ", modeid " & _
                          ", login_modeid " & _
                          ", CASE WHEN login_modeid = 1 THEN @Desktop " & _
                                 "WHEN login_modeid = 2 THEN @ESS " & _
                                 "WHEN login_modeid = 3 THEN @MSS " & _
                            "END AS LOGIN_FROM " & _
                          ", CASE WHEN ( modeid = 1 " & _
                                        "AND logout_date IS NOT NULL " & _
                                      ") THEN @Logout " & _
                                 "WHEN ( modeid = 1 " & _
                                        "AND logout_date IS NULL " & _
                                      ") THEN @Login " & _
                                 "WHEN ( modeid = 2 " & _
                                        "AND logout_date IS NOT NULL " & _
                                      ") THEN @PwdChange + '/' + @Logout " & _
                                 "WHEN ( modeid = 2 " & _
                                        "AND logout_date IS NULL " & _
                                      ") THEN @PwdChange " & _
                            "END AS REMARK " & _
                          ", hrmsconfiguration..cfuser_tracking_tran.relogin_date " & _
                          ", CONVERT(CHAR(8), ISNULL(hrmsconfiguration..cfuser_tracking_tran.relogin_date, '19000101'), 112) AS relogindt " & _
                    "FROM    hrmsconfiguration..cfuser_tracking_tran " & _
                            "LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
                                                                          "AND login_modeid IN (" & enLogin_Mode.DESKTOP & "," & enLogin_Mode.MGR_SELF_SERVICE & ") "

            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= "LEFT JOIN " & strTranDatabaseName & "..hremployee_master ON " & strTranDatabaseName & "..hremployee_master.employeeunkid = hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
                                                                          "AND login_modeid IN (" & enLogin_Mode.EMP_SELF_SERVICE & ") "
            End Select

            strQ &= "WHERE   1 = 1 "

            Select Case gApplicationType

                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= " AND isConfiguration = 1 "

                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= " AND isConfiguration = 0 "
            End Select

            If intCompanyId > 0 Then
                strQ &= " AND hrmsconfiguration..cfuser_tracking_tran.companyunkid = @companyunkid "
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            End If

            If strLoginModeIDs.Trim <> "" Then
                strQ &= " AND login_modeid IN (" & strLoginModeIDs.Trim & ") "
            End If

            If intUserUnkid > 0 Then
                strQ &= " AND hrmsconfiguration..cfuser_tracking_tran.userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            objDataOperation.AddParameter("@Login", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Login"))
            objDataOperation.AddParameter("@PwdChange", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Password Change"))
            objDataOperation.AddParameter("@Logout", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Logged Out"))
            objDataOperation.AddParameter("@Desktop", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Desktop"))
            objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Employee Self Sevice"))
            objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Manager Self Service"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function UpdateForceLogout(ByVal menLoginMode As enLogin_Mode _
                                      , ByVal dicTrackingTran As Dictionary(Of Integer, ArrayList) _
                                      , ByVal blnIsForceLogout As Boolean _
                                      , ByVal strLogoutReason As String _
                                      , ByVal intForceUserId As Integer _
                                      ) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intTrackId As Integer
        Dim intUerId As Integer
        Dim strEmpCode As String
        Dim dtReloginDate As DateTime
        Dim intRowsAffected As Integer

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            Dim arr As ArrayList

            For Each pair In dicTrackingTran
                intTrackId = pair.Key
                arr = dicTrackingTran.Item(intTrackId)
                intUerId = arr.Item(0)
                strEmpCode = arr.Item(1)
                If IsDBNull(arr.Item(2)) = True Then
                    dtReloginDate = Nothing
                Else
                    dtReloginDate = arr.Item(2)
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@trackingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrackId.ToString)
                objDataOperation.AddParameter("@isforcelogout", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsForceLogout.ToString)
                objDataOperation.AddParameter("@logout_reason", SqlDbType.NText, eZeeDataType.NAME_SIZE, strLogoutReason.ToString)
                objDataOperation.AddParameter("@force_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intForceUserId.ToString)
                If dtReloginDate <> Nothing Then
                    objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtReloginDate)
                Else
                    objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUerId.ToString)
                objDataOperation.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmpCode.ToString)
                'S.SANDEEP [ 23 MAR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ = "UPDATE hrmsconfiguration..cfuser_tracking_tran SET " & _
                '       "  isforcelogout = @isforcelogout" & _
                '       ", logout_reason = @logout_reason" & _
                '       ", force_userunkid = @force_userunkid" & _
                '       ", relogin_date = @relogin_date " & _
                '       "WHERE trackingunkid = @trackingunkid " & _
                '       " AND logout_date IS NULL "

                objDataOperation.AddParameter("@logout_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

                strQ = "UPDATE hrmsconfiguration..cfuser_tracking_tran SET " & _
                           "  isforcelogout = @isforcelogout" & _
                           ", logout_reason = @logout_reason" & _
                           ", force_userunkid = @force_userunkid" & _
                           ", relogin_date = @relogin_date " & _
                       ", logout_date = @logout_date " & _
                    "WHERE trackingunkid = @trackingunkid " & _
                    "AND logout_date IS NULL "

                'S.SANDEEP [ 23 MAR 2013 ] -- END

                intRowsAffected = objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                If dtReloginDate <> Nothing AndAlso intRowsAffected > 0 Then

                    Select Case menLoginMode
                        Case enLogin_Mode.DESKTOP, enLogin_Mode.MGR_SELF_SERVICE
                            strQ = "UPDATE hrmsconfiguration..cfuser_master SET " & _
                                            " relogin_date = @relogin_date " & _
                                    "WHERE userunkid = @userunkid "

                        Case enLogin_Mode.EMP_SELF_SERVICE
                            strQ = "UPDATE hrmsconfiguration..cfuser_master SET " & _
                                          " relogin_date = @relogin_date " & _
                                  "WHERE employeecode = @employeecode "

                    End Select


                    intRowsAffected = objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateForceLogout; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetTrackTranUnkId(ByVal intUserunkid As Integer _
                                      , ByVal strIPAddress As String _
                                      , ByVal eLoginMode As enLogin_Mode _
                                      , ByVal blnIsConfiguration As Boolean _
                                      ) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  trackingunkid " & _
                    "FROM    hrmsconfiguration..cfuser_tracking_tran " & _
                    "WHERE   userunkid = @userunkid " & _
                            "AND ip LIKE @IP " & _
                            "AND login_modeid = @login_modeid " & _
                            "AND isConfiguration = @isConfiguration " & _
                            "AND isforcelogout = 1 " & _
                            "AND logout_date IS NULL "




            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            objDataOperation.AddParameter("@IP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIPAddress)
            objDataOperation.AddParameter("@login_modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eLoginMode)
            objDataOperation.AddParameter("@isConfiguration", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsConfiguration)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("trackingunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return -1
    End Function
    'Sohail (04 Jul 2012) -- End

    'S.SANDEEP [ 04 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function isUserLoginExist(ByVal mintuserunkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trackingunkid " & _
              "FROM hrmsconfiguration..cfuser_tracking_tran " & _
              "WHERE userunkid = @userunkid " & _
              " AND logout_date IS NULL " & _
              " AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime).ToString & "'"

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid)
            
            
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("trackingunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUserLoginExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return -1
    End Function
    'S.SANDEEP [ 04 JULY 2012 ] -- END

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Public Function GetUserAttempts(ByVal gApplicationType As enArutiApplicatinType) As DataSet
        ', Optional ByVal intAttemptType As Integer = 0, Optional ByVal intModeId As Integer = 0, Optional ByVal intUserId As Integer = 0
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            'If intAttemptType = 1 Or intAttemptType = 0 Then


            strQ = " SELECT "

            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= " CASE WHEN login_modeid = " & enLogin_Mode.EMP_SELF_SERVICE & " THEN " & _
                                                      FinancialYear._Object._DatabaseName & "..hremployee_master.firstname + ' ' +" & _
                                                      FinancialYear._Object._DatabaseName & "..hremployee_master.surname " & _
                                 "         WHEN login_modeid <> " & enLogin_Mode.EMP_SELF_SERVICE & " THEN " & _
                                          "          ISNULL(hrmsConfiguration..cfuser_master.username,'') " & _
                                " END AS [User] "
                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= "  ISNULL(hrmsConfiguration..cfuser_master.username,'') AS [User] "
            End Select

            'SANDEEP [ 24 Jan 2013 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            'strQ &= ",ip AS IP " & _
            '        ",machine AS Machine " & _
            '        ",CASE WHEN login_modeid = 1 THEN @Desktop" & _
            '        "      WHEN login_modeid = 2 THEN @ESS " & _
            '        "      WHEN login_modeid = 3 THEN @MSS " & _
            '        "  END AS Mode " & _
            '        ",COUNT(hrmsConfiguration..cfuser_tracking_tran.trackingunkid) AS Attempts " & _
            '        ",@Successful as Attempt_Type " & _
            '        ",ISNULL(login_modeid,0) AS login_modeid " & _
            '        ",CASE WHEN login_modeid = 2 THEN " & FinancialYear._Object._DatabaseName & "..hremployee_master.employeeunkid " & _
            '        "      WHEN login_modeid <> 2 THEN hrmsConfiguration..cfuser_master.userunkid ELSE 0 END AS userunkid " & _
            '        ",1 AS Attempt_TypeId " & _
            '        " FROM hrmsConfiguration..cfuser_tracking_tran " & _
            '        " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_tracking_tran.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '        " AND login_modeid IN (" & enLogin_Mode.DESKTOP & ", " & enLogin_Mode.MGR_SELF_SERVICE & ") "
            strQ &= ",ip AS IP " & _
                    ",machine AS Machine " & _
                    ",CASE WHEN login_modeid = 1 THEN @Desktop" & _
                    "      WHEN login_modeid = 2 THEN @ESS " & _
                    "      WHEN login_modeid = 3 THEN @MSS " & _
                    "  END AS Mode " & _
                    ",COUNT(hrmsConfiguration..cfuser_tracking_tran.trackingunkid) AS Attempts " & _
                    ",@Successful as Attempt_Type " & _
                    ",ISNULL(login_modeid,0) AS login_modeid " & _
                    ",CONVERT(CHAR(8),logindate,112) AS attempt_date "
            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= ",CASE WHEN login_modeid = 2 THEN " & FinancialYear._Object._DatabaseName & "..hremployee_master.employeeunkid " & _
                            "      WHEN login_modeid <> 2 THEN hrmsConfiguration..cfuser_master.userunkid ELSE 0 END AS userunkid "
                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= ",CASE WHEN login_modeid <> 2 THEN hrmsConfiguration..cfuser_master.userunkid ELSE 0 END AS userunkid "
            End Select
            strQ &= ",1 AS Attempt_TypeId " & _
                    " FROM hrmsConfiguration..cfuser_tracking_tran " & _
                    " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_tracking_tran.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    " AND login_modeid IN (" & enLogin_Mode.DESKTOP & ", " & enLogin_Mode.MGR_SELF_SERVICE & ") "
            'SANDEEP [ 24 Jan 2013 ] -- End


            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= "   LEFT JOIN " & FinancialYear._Object._DatabaseName & "..hremployee_master ON " & FinancialYear._Object._DatabaseName & "..hremployee_master.employeeunkid = hrmsconfiguration..cfuser_tracking_tran.userunkid " & _
                            "   AND login_modeid IN (" & enLogin_Mode.EMP_SELF_SERVICE & ") "
            End Select

            strQ &= " WHERE 1 = 1 "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END

            'If intModeId > 0 Then
            '    strQ &= " AND login_modeid = " & intModeId
            'End If

            'If intUserId > 0 Then
            '    strQ &= " AND hrmsConfiguration..cfuser_tracking_tran.userunkid = " & intUserId
            'End If


            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Configuration
                    strQ &= " AND isConfiguration = 1"
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= " AND isConfiguration = 0"
            End Select

            strQ &= " GROUP BY  ISNULL(hrmsConfiguration..cfuser_master.username,''),ip,machine,ModeId,login_modeid,hrmsConfiguration..cfuser_master.userunkid,logindate "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END

            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= ", " & FinancialYear._Object._DatabaseName & "..hremployee_master.firstname , " & FinancialYear._Object._DatabaseName & "..hremployee_master.surname " & _
                            ", " & FinancialYear._Object._DatabaseName & "..hremployee_master.employeeunkid "
            End Select

            'End If

            'If intAttemptType = 0 Then
            strQ &= " UNION "
            'End If

            'If intAttemptType = 2 Or intAttemptType = 0 Then

            strQ &= " SELECT  ISNULL(hrmsConfiguration..cfuser_master.username,'') AS [User] " & _
                        ",ip AS IP " & _
                        ",machine_name AS Machine " & _
                        ",@Desktop AS Mode " & _
                        ",SUM(ISNULL(hrmsConfiguration..cfuser_acc_lock.attempts,0)) AS Attempts " & _
                        ",@Unsuccessful as Attempt_Type " & _
                        ",1 AS login_modeid " & _
                    ",CONVERT(CHAR(8),attempt_date,112) AS attempt_date " & _
                        ",hrmsConfiguration..cfuser_master.userunkid " & _
                        ",2 AS Attempt_TypeId " & _
                        " FROM hrmsConfiguration..cfuser_acc_lock " & _
                        " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_acc_lock.userunkid = hrmsConfiguration..cfuser_master.userunkid "

            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= "   LEFT JOIN " & FinancialYear._Object._DatabaseName & "..hremployee_master ON " & FinancialYear._Object._DatabaseName & "..hremployee_master.employeeunkid = hrmsconfiguration..cfuser_acc_lock.userunkid "
            End Select

            strQ &= " WHERE isuser = 1 "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END

            'If intUserId > 0 Then
            'strQ &= " AND hrmsConfiguration..cfuser_acc_lock.userunkid = " & intUserId
            'End If

            strQ &= " GROUP BY  ISNULL(hrmsConfiguration..cfuser_master.username,''),ip,machine_name,hrmsConfiguration..cfuser_master.userunkid,attempt_date "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END

            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    strQ &= ", " & FinancialYear._Object._DatabaseName & "..hremployee_master.firstname ," & FinancialYear._Object._DatabaseName & "..hremployee_master.surname "
            End Select

            'End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Desktop", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Desktop"))
            objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Employee Self Sevice"))
            objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Manager Self Service"))
            objDataOperation.AddParameter("@Successful", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Successful"))
            objDataOperation.AddParameter("@Unsuccessful", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Unsuccessful"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUserAttempts", mstrModuleName)
        End Try
        Return dsList
    End Function
    'S.SANDEEP [ 19 JULY 2012 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Login")
			Language.setMessage(mstrModuleName, 2, "Password Change")
			Language.setMessage(mstrModuleName, 3, "Logged Out")
			Language.setMessage(mstrModuleName, 4, "Desktop")
			Language.setMessage(mstrModuleName, 5, "Employee Self Sevice")
			Language.setMessage(mstrModuleName, 6, "Manager Self Service")
			Language.setMessage(mstrModuleName, 7, "Successful")
			Language.setMessage(mstrModuleName, 8, "Unsuccessful")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class