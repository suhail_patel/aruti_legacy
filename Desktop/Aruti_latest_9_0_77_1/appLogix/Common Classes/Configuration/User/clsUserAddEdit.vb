﻿'************************************************************************************************************************************
'Class Name : clsUserAddEdit.vb
'Purpose    :
'Date       :29/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports ExcelWriter

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsUserAddEdit
    Private Shared ReadOnly mstrModuleName As String = "clsUserAddEdit"
    Dim mstrMessage As String = ""

#Region " ENUM "

    Public Enum enViewUserType
        SHOW_ACTIVE_USER = 1
        SHOW_INACTIVE_USER = 2
        SHOW_ALL_USER = 3
    End Enum

#End Region

#Region " Private variables "

    Private mintUserunkid As Integer
    Private mstrUsername As String = String.Empty
    Private mstrPassword As String = String.Empty
    Private mblnIspasswordexpire As Boolean
    Private mintRoleunkid As Integer
    Private mblnIsrighttoleft As Boolean
    Private mintExp_Days As Integer
    Private mintLanguageunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mstrAssignPrivilegeIDs As String = ""
    Private mstrAssignCompanyPrivilegeIDs As String = ""
    Private mstrAssignAccessPrivilegeIDs As String = ""
    Private mstrUserAccessLevels As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintYearId As Integer = 0
    Public Privilege As New clsUserPrivilege
    Private mstrReportCompanyIds As String = String.Empty
    Private mstrReportIDs As String = String.Empty
    Private mdtCreationDate As Date = Nothing
    Private mblnIsLocked As Boolean = False
    Private mdtLockedTime As DateTime
    Private mdtLockedDuration As DateTime
    Public PWDOptions As clsPassowdOptions
    Private mintCreatedById As Integer = 0
    Private mstrFirstname As String = String.Empty
    Private mstrLastname As String = String.Empty
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mstrPhone As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mblnIsADuser As Boolean = False
    Private mblnIsManager As Boolean = False
    Private mstrUserdomain As String = String.Empty
    Private mdtRelogin_Date As Date
    Dim mDicRolePrivilege As New Dictionary(Of Integer, DataTable)
    Private blnShowChangePasswdfrm As Boolean = False
    Private intRetUserId As Integer = 0
    Private mintEmployeeUnkid As Integer = 0
    Private mintEmployeeCompanyUnkid As Integer = 0
    Private mdtSuspended_From_Date As Date = Nothing
    Private mdtSuspended_To_Date As Date = Nothing
    Private mdtProbation_From_Date As Date = Nothing
    Private mdtProbation_To_Date As Date = Nothing
    Private mdtEmpl_Enddate As Date = Nothing
    Private mdtTermination_From_Date As Date = Nothing
    Private mdtTermination_To_Date As Date = Nothing
    Private mdtAppointeddate As Date = Nothing
    Private mdtReinstatement_Date As Date = Nothing
    Private iObjDataOpr As clsDataOperation
    'Sohail (24 Nov 2016) -- Start
    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
    Private mintTheme_Id As Integer = enSelfServiceTheme.Blue
    Private mintLastView_Id As Integer = enSelfServiceLastView.MainView
    'Sohail (24 Nov 2016) -- End
    'Hemant (25 May 2021) -- Start
    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
    Private mstrOldPassword As String = String.Empty
    Private mintChangedUserunkid As Integer
    'Private mstrIp As String = String.Empty
    'Private mstrHostname As String = String.Empty
    'Private mblnIsweb As Boolean
    Private mblnIsSaveAsEmployee As Boolean = False
    'Hemant (25 May 2021) -- End

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property


    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set username
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Username() As String
        Get
            Return mstrUsername
        End Get
        Set(ByVal value As String)
            mstrUsername = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set password
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Password() As String
        Get
            Return mstrPassword
        End Get
        Set(ByVal value As String)
            mstrPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispasswordexpire
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ispasswordexpire() As Boolean
        Get
            Return mblnIspasswordexpire
        End Get
        Set(ByVal value As Boolean)
            mblnIspasswordexpire = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isrighttoleft
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isrighttoleft() As Boolean
        Get
            Return mblnIsrighttoleft
        End Get
        Set(ByVal value As Boolean)
            mblnIsrighttoleft = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exp_days
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Exp_Days() As Integer
        Get
            Return mintExp_Days
        End Get
        Set(ByVal value As Integer)
            mintExp_Days = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set languageunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Languageunkid() As Integer
        Get
            Return mintLanguageunkid
        End Get
        Set(ByVal value As Integer)
            mintLanguageunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public WriteOnly Property _AssignPrivilegeIDs() As String
        Set(ByVal value As String)
            mstrAssignPrivilegeIDs = value
        End Set
    End Property

    Public WriteOnly Property _AssignCompanyPrivilegeIDs() As String
        Set(ByVal value As String)
            mstrAssignCompanyPrivilegeIDs = value
        End Set
    End Property

    Public WriteOnly Property _AssignAccessPrivilegeIDs() As String
        Set(ByVal value As String)
            mstrAssignAccessPrivilegeIDs = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessLevels() As String
        Set(ByVal value As String)
            mstrUserAccessLevels = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public Property _ReportCompanyIds() As String
        Get
            Return mstrReportCompanyIds
        End Get
        Set(ByVal value As String)
            mstrReportCompanyIds = value
        End Set
    End Property

    Public WriteOnly Property _ReportIDs() As String
        Set(ByVal value As String)
            mstrReportIDs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Creation Date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Creation_Date() As Date
        Get
            Return mdtCreationDate
        End Get
        Set(ByVal value As Date)
            mdtCreationDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Account Locking
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _IsAcc_Locked() As Boolean
        Get
            Return mblnIsLocked
        End Get
        Set(ByVal value As Boolean)
            mblnIsLocked = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Time User Account Locked
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Locked_Time() As DateTime
        Get
            Return mdtLockedTime
        End Get
        Set(ByVal value As DateTime)
            mdtLockedTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Time Duration User Account Locked
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LockedDuration() As DateTime
        Get
            Return mdtLockedDuration
        End Get
        Set(ByVal value As DateTime)
            mdtLockedDuration = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Created By User
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _CreatedById() As Integer
        Get
            Return mintCreatedById
        End Get
        Set(ByVal value As Integer)
            mintCreatedById = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set firstname
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Firstname() As String
        Get
            Return mstrFirstname
        End Get
        Set(ByVal value As String)
            mstrFirstname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastname
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Lastname() As String
        Get
            Return mstrLastname
        End Get
        Set(ByVal value As String)
            mstrLastname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Phone() As String
        Get
            Return mstrPhone
        End Get
        Set(ByVal value As String)
            mstrPhone = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isaduser
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _IsAduser() As Boolean
        Get
            Return mblnIsADuser
        End Get
        Set(ByVal value As Boolean)
            mblnIsADuser = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isManager
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _IsManager() As Boolean
        Get
            Return mblnIsManager
        End Get
        Set(ByVal value As Boolean)
            mblnIsManager = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set UserDomain
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _UserDomain() As String
        Get
            Return mstrUserdomain
        End Get
        Set(ByVal value As String)
            mstrUserdomain = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relogin_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Relogin_Date() As Date
        Get
            Return mdtRelogin_Date
        End Get
        Set(ByVal value As Date)
            mdtRelogin_Date = value
        End Set
    End Property

    Public ReadOnly Property _ShowChangePasswdfrm() As Boolean
        Get
            Return blnShowChangePasswdfrm
        End Get
    End Property

    Public ReadOnly Property _RetUserId() As Integer
        Get
            Return intRetUserId
        End Get
    End Property

    Public Property _EmployeeUnkid() As Integer
        Get
            Return mintEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public Property _EmployeeCompanyUnkid() As Integer
        Get
            Return mintEmployeeCompanyUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set suspended_from_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Suspended_From_Date() As Date
        Get
            Return mdtSuspended_From_Date
        End Get
        Set(ByVal value As Date)
            mdtSuspended_From_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set suspended_to_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Suspended_To_Date() As Date
        Get
            Return mdtSuspended_To_Date
        End Get
        Set(ByVal value As Date)
            mdtSuspended_To_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set probation_from_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Probation_From_Date() As Date
        Get
            Return mdtProbation_From_Date
        End Get
        Set(ByVal value As Date)
            mdtProbation_From_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set probation_to_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Probation_To_Date() As Date
        Get
            Return mdtProbation_To_Date
        End Get
        Set(ByVal value As Date)
            mdtProbation_To_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empl_enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Empl_Enddate() As Date
        Get
            Return mdtEmpl_Enddate
        End Get
        Set(ByVal value As Date)
            mdtEmpl_Enddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set termination_from_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Termination_Date() As Date
        Get
            Return mdtTermination_From_Date
        End Get
        Set(ByVal value As Date)
            mdtTermination_From_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set termination_to_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Retirement_Date() As Date
        Get
            Return mdtTermination_To_Date
        End Get
        Set(ByVal value As Date)
            mdtTermination_To_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appointeddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Appointeddate() As Date
        Get
            Return mdtAppointeddate
        End Get
        Set(ByVal value As Date)
            mdtAppointeddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reinstatement_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reinstatement_Date() As Date
        Get
            Return mdtReinstatement_Date
        End Get
        Set(ByVal value As Date)
            mdtReinstatement_Date = value
        End Set
    End Property

    Public WriteOnly Property _DataOpr() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            iObjDataOpr = value
        End Set
    End Property

    'Sohail (24 Nov 2016) -- Start
    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
    ''' <summary>
    ''' Purpose: Get or Set Theme_Id
    ''' Modify By: Sohail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _Theme_Id() As Integer
        Get
            Return mintTheme_Id
        End Get
        Set(ByVal value As Integer)
            mintTheme_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LastView_Id
    ''' Modify By: Sohail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _LastView_Id() As Integer
        Get
            Return mintLastView_Id
        End Get
        Set(ByVal value As Integer)
            mintLastView_Id = value
        End Set
    End Property
    'Sohail (24 Nov 2016) -- End

    'Hemant (25 May 2021) -- Start
    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
    ''' <summary>
    ''' Purpose: Get or Set oldpassword
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _OldPassword() As String
        Get
            Return mstrOldPassword
        End Get
        Set(ByVal value As String)
            mstrOldPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changed_userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ChangedUserunkid() As Integer
        Get
            Return mintChangedUserunkid
        End Get
        Set(ByVal value As Integer)
            mintChangedUserunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set ip
    '''' Modify By: Hemant
    '''' </summary>
    'Public Property _Ip() As String
    '    Get
    '        Return mstrIp
    '    End Get
    '    Set(ByVal value As String)
    '        mstrIp = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set hostname
    '''' Modify By: Hemant
    '''' </summary>
    'Public Property _Hostname() As String
    '    Get
    '        Return mstrHostname
    '    End Get
    '    Set(ByVal value As String)
    '        mstrHostname = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set isweb
    '''' Modify By: Hemant
    '''' </summary>
    'Public Property _Isweb() As Boolean
    '    Get
    '        Return mblnIsweb
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsweb = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set issaveasemployee
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsSaveAsEmployee() As Boolean
        Get
            Return mblnIsSaveAsEmployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsSaveAsEmployee = value
        End Set
    End Property
    'Hemant (25 May 2021) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = iObjDataOpr
            objDataOperation.ClearParameters()
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            strQ = "SELECT " & _
                   "  userunkid " & _
                   ", username " & _
                   ", password " & _
                   ", ispasswordexpire " & _
                   ", roleunkid " & _
                   ", isrighttoleft " & _
                   ", exp_days " & _
                   ", languageunkid " & _
                   ", isactive " & _
                   ", ISNULL(creationdate,'') AS creationdate " & _
                   ", ISNULL(islocked,0) AS islocked " & _
                   ", lockedtime " & _
                   ", lockedduration " & _
                   ", ISNULL(createdbyunkid,0) AS createdbyunkid " & _
                   ", ISNULL(firstname,'') AS firstname " & _
                   ", ISNULL(lastname,'') AS lastname " & _
                   ", ISNULL(address1,'') AS address1 " & _
                   ", ISNULL(address2,'') AS address2 " & _
                   ", ISNULL(phone,'') AS phone " & _
                   ", ISNULL(email,'') AS email " & _
                   ", ISNULL(isaduser,0) as  isaduser " & _
                   ", ISNULL(ismanager,0) as  ismanager " & _
                   ", ISNULL(userdomain,'') as  userdomain " & _
                   ", relogin_date " & _
                   ", employeeunkid " & _
                   ", companyunkid " & _
                   ", suspended_from_date " & _
                   ", suspended_to_date " & _
                   ", probation_from_date " & _
                   ", probation_to_date " & _
                   ", empl_enddate " & _
                   ", termination_from_date " & _
                   ", termination_to_date " & _
                   ", appointeddate " & _
                   ", reinstatement_date " & _
                   ", ISNULL(theme_id, 1) AS theme_id " & _
                   ", ISNULL(lastview_id, 0) AS lastview_id " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "WHERE userunkid = @userunkid "
            'Sohail (24 Nov 2016) - [theme_id, lastview_id]

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mstrUsername = dtRow.Item("username").ToString
                mstrPassword = clsSecurity.Decrypt(dtRow.Item("password"), "ezee").ToString
                mblnIspasswordexpire = CBool(dtRow.Item("ispasswordexpire"))
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                mblnIsrighttoleft = CBool(dtRow.Item("isrighttoleft"))
                mintExp_Days = CInt(dtRow.Item("exp_days"))
                mintLanguageunkid = CInt(dtRow.Item("languageunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsLocked = CBool(dtRow.Item("islocked"))
                If dtRow.Item("creationdate").ToString.Trim.Length > 0 Then
                    mdtCreationDate = dtRow.Item("creationdate")
                End If
                If dtRow.Item("lockedtime").ToString.Trim.Length > 0 Then
                    mdtLockedTime = dtRow.Item("lockedtime")
                End If
                If dtRow.Item("lockedduration").ToString.Trim.Length > 0 Then
                    mdtLockedDuration = dtRow.Item("lockedduration")
                End If
                mintCreatedById = CInt(dtRow.Item("createdbyunkid"))
                mstrFirstname = dtRow.Item("firstname").ToString
                mstrLastname = dtRow.Item("lastname").ToString
                mstrAddress1 = dtRow.Item("address1").ToString
                mstrAddress2 = dtRow.Item("address2").ToString
                mstrPhone = dtRow.Item("phone").ToString
                mstrEmail = dtRow.Item("email").ToString
                mblnIsADuser = CBool(dtRow.Item("isaduser"))
                mblnIsManager = CBool(dtRow.Item("ismanager"))
                mstrUserdomain = dtRow.Item("userdomain").ToString

                If IsDBNull(dtRow.Item("relogin_date")) = True Then
                    mdtRelogin_Date = Nothing
                Else
                    mdtRelogin_Date = dtRow.Item("relogin_date")
                End If

                mintEmployeeUnkid = CInt(dtRow.Item("employeeunkid"))
                mintEmployeeCompanyUnkid = CInt(dtRow.Item("companyunkid"))
                If IsDBNull(dtRow.Item("suspended_from_date")) = False Then
                    mdtSuspended_From_Date = dtRow.Item("suspended_from_date")
                Else
                    mdtSuspended_From_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("suspended_to_date")) = False Then
                    mdtSuspended_To_Date = dtRow.Item("suspended_to_date")
                Else
                    mdtSuspended_To_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("probation_from_date")) = False Then
                    mdtProbation_From_Date = dtRow.Item("probation_from_date")
                Else
                    mdtProbation_From_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("probation_to_date")) = False Then
                    mdtProbation_To_Date = dtRow.Item("probation_to_date")
                Else
                    mdtProbation_To_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
                    mdtEmpl_Enddate = dtRow.Item("empl_enddate")
                Else
                    mdtEmpl_Enddate = Nothing
                End If
                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
                    mdtTermination_From_Date = dtRow.Item("termination_from_date")
                Else
                    mdtTermination_From_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
                    mdtTermination_To_Date = dtRow.Item("termination_to_date")
                Else
                    mdtTermination_To_Date = Nothing
                End If
                If IsDBNull(dtRow.Item("appointeddate")) = False Then
                    mdtAppointeddate = dtRow.Item("appointeddate")
                Else
                    mdtAppointeddate = Nothing
                End If
                If IsDBNull(dtRow.Item("reinstatement_date")) = False Then
                    mdtReinstatement_Date = dtRow.Item("reinstatement_date")
                Else
                    mdtReinstatement_Date = Nothing
                End If
                'Sohail (24 Nov 2016) -- Start
                'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                mintTheme_Id = CInt(dtRow.Item("theme_id"))
                mintLastView_Id = CInt(dtRow.Item("lastview_id"))
                'Sohail (24 Nov 2016) -- End
                Exit For
            Next

            Call Privilege.setUserPrivilege(mintUserunkid)

            PWDOptions = New clsPassowdOptions

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnAdUser As Boolean = False) As DataSet
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilterString As String = "") As DataSet 'S.SANDEEP [24 SEP 2015] -- END
        'Public Function GetList(ByVal strTableName As String, _
        '                            Optional ByVal blnOnlyActive As Boolean = True, _
        '                            Optional ByVal blnAdUser As Boolean = False, _
        '                            Optional ByVal blnOnlyEmp As Boolean = False, _
        '                            Optional ByVal strFilterString As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try

            strQ = "SELECT " & _
                   "  hrmsConfiguration..cfuser_master.userunkid " & _
                   ", hrmsConfiguration..cfuser_master.username " & _
                   ", hrmsConfiguration..cfuser_master.password " & _
                   ", hrmsConfiguration..cfuser_master.ispasswordexpire " & _
                   ", hrmsConfiguration..cfuser_master.roleunkid " & _
                   ", hrmsConfiguration..cfuser_master.isrighttoleft " & _
                   ", hrmsConfiguration..cfuser_master.exp_days " & _
                   ", hrmsConfiguration..cfuser_master.languageunkid " & _
                   ", hrmsConfiguration..cfuser_master.isactive " & _
                   ", hrmsConfiguration..cfrole_master.name AS rolename " & _
                   ", hrmsConfiguration..cfuser_master.roleunkid " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') AS firstname " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS lastname " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.address1,'') AS address1 " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.address2,'') AS address2 " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.phone,'') AS phone " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.email,'') AS email " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) AS isaduser " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.ismanager,0) AS ismanager " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.userdomain,'') AS userdomain " & _
                   ", hrmsConfiguration..cfuser_master.relogin_date " & _
                   ", hrmsConfiguration..cfuser_master.employeeunkid " & _
                   ", hrmsConfiguration..cfuser_master.companyunkid " & _
                   ", hrmsConfiguration..cfuser_master.suspended_from_date " & _
                   ", hrmsConfiguration..cfuser_master.suspended_to_date " & _
                   ", hrmsConfiguration..cfuser_master.probation_from_date " & _
                   ", hrmsConfiguration..cfuser_master.probation_to_date " & _
                   ", hrmsConfiguration..cfuser_master.empl_enddate " & _
                   ", hrmsConfiguration..cfuser_master.termination_from_date " & _
                   ", hrmsConfiguration..cfuser_master.termination_to_date " & _
                   ", hrmsConfiguration..cfuser_master.appointeddate " & _
                   ", hrmsConfiguration..cfuser_master.reinstatement_date " & _
                   ", hrmsConfiguration..cfuser_master.lockedduration " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.theme_id, 1) AS theme_id " & _
                   ", ISNULL(hrmsConfiguration..cfuser_master.lastview_id, 0) AS lastview_id " & _
                   " FROM hrmsConfiguration..cfuser_master " & _
                   "    LEFT JOIN hrmsConfiguration..cfrole_master on hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid " & _
                   " WHERE  1 = 1 "
            'Sohail (24 Nov 2016) - [theme_id, lastview_id]

            'S.SANDEEP [24 SEP 2015] -- START
            'If blnOnlyActive Then
            '    strQ &= " And  hrmsConfiguration..cfuser_master.isactive = 1 "
            'End If

            'If blnAdUser Then
            '    strQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or userunkid = 1) "
            'Else
            '    strQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or userunkid = 1) "
            'End If

            'If blnOnlyEmp Then
            '    strQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR userunkid = 1) "
            'Else
            '    strQ &= " AND ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0 "
            'End If
            strQ &= " AND hrmsConfiguration..cfuser_master.isactive = 1 "

            Dim objOption As New clsPassowdOptions
            If objOption._EnableAllUser = False Then
                Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                If drOption.Length > 0 Then
                    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                        If objOption._IsEmployeeAsUser Then
                            strQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
            Else
                            strQ &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0  OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                        End If
                        strQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                        strQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    End If
                End If
            End If
            objOption = Nothing
            'S.SANDEEP [24 SEP 2015] -- END


            'S.SANDEEP [06-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Training Module Notification
            If strFilterString.Trim.Length > 0 Then
                strQ &= " AND " & strFilterString
            End If
            'S.SANDEEP [06-MAR-2017] -- END



            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfuser_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrUsername) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This User is already defined. Please define new User.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = iObjDataOpr
            objDataOperation.ClearParameters()
        End If
        Try
            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrPassword, "ezee").ToString)
            objDataOperation.AddParameter("@ispasswordexpire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspasswordexpire.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@isrighttoleft", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsrighttoleft.ToString)
            objDataOperation.AddParameter("@exp_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExp_Days.ToString)
            objDataOperation.AddParameter("@languageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@creationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreationDate)
            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
            If mdtLockedTime = Nothing Then
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedTime)
            End If

            If mdtLockedDuration = Nothing Then
                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedDuration)
            End If
            objDataOperation.AddParameter("@createdbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreatedById)
            objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
            objDataOperation.AddParameter("@lastname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLastname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@isaduser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsADuser.ToString)
            objDataOperation.AddParameter("@ismanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsManager.ToString)
            objDataOperation.AddParameter("@userdomain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUserdomain.ToString())
            If mdtRelogin_Date <> Nothing Then
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRelogin_Date)
            Else
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeCompanyUnkid.ToString)
            If mdtSuspended_From_Date <> Nothing Then
                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_From_Date)
            Else
                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtSuspended_To_Date <> Nothing Then
                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_To_Date)
            Else
                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtProbation_From_Date <> Nothing Then
                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_From_Date)
            Else
                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtProbation_To_Date <> Nothing Then
                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_To_Date)
            Else
                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEmpl_Enddate <> Nothing Then
                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmpl_Enddate)
            Else
                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtTermination_From_Date <> Nothing Then
                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_From_Date)
            Else
                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtTermination_To_Date <> Nothing Then
                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_To_Date)
            Else
                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtAppointeddate <> Nothing Then
                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppointeddate)
            Else
                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtReinstatement_Date <> Nothing Then
                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatement_Date)
            Else
                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            objDataOperation.AddParameter("@theme_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTheme_Id)
            objDataOperation.AddParameter("@lastview_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLastView_Id)
            'Sohail (24 Nov 2016) -- End

            strQ = "INSERT INTO hrmsConfiguration..cfuser_master ( " & _
                        "  username " & _
                        ", password " & _
                        ", ispasswordexpire " & _
                        ", roleunkid " & _
                        ", isrighttoleft " & _
                        ", exp_days " & _
                        ", languageunkid " & _
                        ", isactive" & _
                        ", creationdate " & _
                        ", islocked " & _
                        ", lockedtime " & _
                        ", lockedduration " & _
                        ", createdbyunkid " & _
                        ", firstname " & _
                        ", lastname " & _
                        ", address1 " & _
                        ", address2 " & _
                        ", phone " & _
                        ", email" & _
                        ", isaduser " & _
                        ", ismanager " & _
                        ", userdomain " & _
                        ", relogin_date" & _
                        ", employeeunkid " & _
                        ", companyunkid " & _
                        ", suspended_from_date " & _
                        ", suspended_to_date " & _
                        ", probation_from_date " & _
                        ", probation_to_date " & _
                        ", empl_enddate " & _
                        ", termination_from_date " & _
                        ", termination_to_date " & _
                        ", appointeddate " & _
                        ", reinstatement_date" & _
                        ", theme_id " & _
                        ", lastview_id " & _
                 ") VALUES (" & _
                        "  @username " & _
                        ", @password " & _
                        ", @ispasswordexpire " & _
                        ", @roleunkid " & _
                        ", @isrighttoleft " & _
                        ", @exp_days " & _
                        ", @languageunkid " & _
                        ", @isactive" & _
                        ", @creationdate " & _
                        ", @islocked " & _
                        ", @lockedtime " & _
                        ", @lockedduration " & _
                        ", @createdbyunkid " & _
                        ", @firstname " & _
                        ", @lastname " & _
                        ", @address1 " & _
                        ", @address2 " & _
                        ", @phone " & _
                        ", @email" & _
                        ", @isaduser " & _
                        ", @ismanager " & _
                        ", @userdomain " & _
                        ", @relogin_date" & _
                        ", @employeeunkid " & _
                        ", @companyunkid " & _
                        ", @suspended_from_date " & _
                        ", @suspended_to_date " & _
                        ", @probation_from_date " & _
                        ", @probation_to_date " & _
                        ", @empl_enddate " & _
                        ", @termination_from_date " & _
                        ", @termination_to_date " & _
                        ", @appointeddate " & _
                        ", @reinstatement_date" & _
                        ", @theme_id " & _
                        ", @lastview_id " & _
                        "); SELECT @@identity"
            'Sohail (24 Nov 2016) - [theme_id, lastview_id]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintUserunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim blnIsInserted As Boolean = False

            If mstrAssignPrivilegeIDs <> "" Then
                Dim strPrivilegeUnkid() As String = Split(mstrAssignPrivilegeIDs, ",")
                Dim dsTrans As New DataSet
                For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
                    objDataOperation.ClearParameters()
                    If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = '" & mintUserunkid & "' AND privilegeunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then
                        strQ = "INSERT INTO hrmsconfiguration..cfuser_privilege(userunkid, privilegeunkid) " & _
                                   " VALUES(@userunkid, @privilegeunkid); SELECT @@identity "

                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

                        dsTrans = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        Dim objCommonATLog As New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfuser_privilege", "userprivilegeunkid", dsTrans.Tables(0).Rows(0)(0), 1, 1, True) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        Else
                            blnIsInserted = True
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END


                    End If
                Next
            End If
            If mstrAssignCompanyPrivilegeIDs <> "" Then
                Dim strPrivilegeUnkid() As String = Split(mstrAssignCompanyPrivilegeIDs, ",")
                Dim dsTrans As New DataSet
                For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
                    objDataOperation.ClearParameters()
                    If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = '" & mintUserunkid & "' AND yearunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then
                        strQ = "INSERT INTO hrmsconfiguration..cfcompanyaccess_privilege (userunkid,yearunkid) " & _
                                   "VALUES(@userunkid,@yearunkid); SELECT @@identity "

                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

                        dsTrans = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        Dim objCommonATLog As New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfcompanyaccess_privilege", "companyacessunkid", dsTrans.Tables(0).Rows(0)(0), 1, 1, True) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        Else
                            blnIsInserted = True
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END


                    End If
                Next
            End If

            'S.SANDEEP |05-APR-2019| -- START
            If AssignReportPrivilege(mintUserunkid, mintRoleunkid, objDataOperation, False) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |05-APR-2019| -- END

'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            Dim objPassOpt As New clsPassowdOptions
            If objPassOpt._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPassOpt._IsPasswordHistory AndAlso _
               (objPassOpt._PasswdLastUsedNumber > 0 OrElse objPassOpt._PasswdLastUsedDays > 0) AndAlso mstrPassword <> mstrOldPassword Then
                Dim objPasswordHistory As New clsPasswordHistory
                With objPasswordHistory
                    ._Tranguid(objDataOperation) = Guid.NewGuid().ToString
                    If mblnIsSaveAsEmployee = True Then
                        ._Userunkid = -1
                        ._Employeeunkid = mintEmployeeUnkid
                    Else
                        ._Userunkid = mintUserunkid
                        ._Employeeunkid = -1
                    End If
                    ._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
                    ._NewPassword = mstrPassword
                    ._OldPassword = mstrOldPassword
                    ._Companyunkid = mintCompanyUnkid
                    ._ChangedUserunkid = mintChangedUserunkid
                    ._Ip = mstrClientIP
                    ._Hostname = mstrHostname
                    ._Isweb = mblnIsweb
                    .Insert(objDataOperation)
                    If objPasswordHistory._Message <> "" Then
                        mstrMessage = objPasswordHistory._Message
                        Return False
                    End If
                End With
                objPasswordHistory = Nothing
            End If
objPassOpt = Nothing
            'Hemant (25 May 2021) -- End

            If blnIsInserted = False Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "", "", -1, 1, 0, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfuser_master) </purpose>
    Public Function Update(Optional ByVal blnIsFromUnlock As Boolean = False) As Boolean
        If isExist(mstrUsername, mintUserunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This User is already defined. Please define new User.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If iObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = iObjDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrPassword, "ezee").ToString)
            objDataOperation.AddParameter("@ispasswordexpire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspasswordexpire.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@isrighttoleft", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsrighttoleft.ToString)
            objDataOperation.AddParameter("@exp_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExp_Days.ToString)
            objDataOperation.AddParameter("@languageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            If mdtCreationDate <> Nothing Then
                objDataOperation.AddParameter("@creationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreationDate)
            Else
                objDataOperation.AddParameter("@creationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Now.Date)
            End If
            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
            If mdtLockedTime = Nothing Then
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedTime)
            End If
            If mdtLockedDuration = Nothing Then
                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedDuration)
            End If
            objDataOperation.AddParameter("@createdbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreatedById)
            objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
            objDataOperation.AddParameter("@lastname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLastname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@isaduser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsADuser.ToString)
            objDataOperation.AddParameter("@ismanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsManager.ToString)
            objDataOperation.AddParameter("@userdomain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUserdomain.ToString)
            If mdtRelogin_Date <> Nothing Then
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRelogin_Date)
            Else
                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeCompanyUnkid.ToString)
            If mdtSuspended_From_Date <> Nothing Then
                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_From_Date)
            Else
                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtSuspended_To_Date <> Nothing Then
                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_To_Date)
            Else
                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtProbation_From_Date <> Nothing Then
                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_From_Date)
            Else
                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtProbation_To_Date <> Nothing Then
                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_To_Date)
            Else
                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEmpl_Enddate <> Nothing Then
                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmpl_Enddate)
            Else
                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtTermination_From_Date <> Nothing Then
                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_From_Date)
            Else
                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtTermination_To_Date <> Nothing Then
                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_To_Date)
            Else
                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtAppointeddate <> Nothing Then
                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppointeddate)
            Else
                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtReinstatement_Date <> Nothing Then
                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatement_Date)
            Else
                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            objDataOperation.AddParameter("@theme_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTheme_Id)
            objDataOperation.AddParameter("@lastview_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLastView_Id)
            'Sohail (24 Nov 2016) -- End

            strQ = "UPDATE hrmsconfiguration..cfuser_master SET " & _
                    "  username = @username" & _
                    ", password = @password" & _
                    ", ispasswordexpire = @ispasswordexpire" & _
                    ", roleunkid = @roleunkid" & _
                    ", isrighttoleft = @isrighttoleft" & _
                    ", exp_days = @exp_days" & _
                    ", languageunkid = @languageunkid" & _
                    ", isactive = @isactive " & _
                    ", creationdate = @creationdate" & _
                    ", islocked = @islocked" & _
                    ", lockedtime = @lockedtime" & _
                    ", lockedduration = @lockedduration " & _
                    ", createdbyunkid = @createdbyunkid " & _
                    ", firstname = @firstname" & _
                    ", lastname = @lastname" & _
                    ", address1 = @address1" & _
                    ", address2 = @address2" & _
                    ", phone = @phone" & _
                    ", email = @email " & _
                    ", isaduser = @isaduser " & _
                    ", ismanager = @ismanager " & _
                    ", userdomain = @userdomain " & _
                    ", relogin_date = @relogin_date " & _
                    ", employeeunkid = @employeeunkid" & _
                    ", companyunkid = @companyunkid " & _
                    ", suspended_from_date = @suspended_from_date " & _
                    ", suspended_to_date = @suspended_to_date " & _
                    ", probation_from_date = @probation_from_date " & _
                    ", probation_to_date = @probation_to_date " & _
                    ", empl_enddate = @empl_enddate " & _
                    ", termination_from_date = @termination_from_date " & _
                    ", termination_to_date = @termination_to_date " & _
                    ", appointeddate = @appointeddate" & _
                    ", reinstatement_date = @reinstatement_date " & _
                    ", theme_id = @theme_id " & _
                    ", lastview_id = @lastview_id " & _
                    "WHERE userunkid = @userunkid "
            'Sohail (24 Nov 2016) - [theme_id, lastview_id]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnIsInserted As Boolean = False
            If blnIsFromUnlock = False Then

                If mstrAssignPrivilegeIDs.Length > 0 Then

                    Dim dsTran As New DataSet

                    strQ = "SELECT userprivilegeunkid,userunkid,privilegeunkid FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = " & mintUserunkid

                    dsTran = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsTran.Tables(0).Rows.Count > 0 Then
                        Dim dRemPvg() As DataRow = dsTran.Tables(0).Select("privilegeunkid NOT IN (" & IIf(mstrAssignPrivilegeIDs = "", "0", mstrAssignPrivilegeIDs) & ")")
                        If dRemPvg.Length > 0 Then
                            For iCnt As Integer = 0 To dRemPvg.Length - 1
                                If dRemPvg(iCnt).Item("userprivilegeunkid").ToString.Trim.Length > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfuser_privilege", "userprivilegeunkid", dRemPvg(iCnt)("userprivilegeunkid"), 2, 3, True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    Else
                                        blnIsInserted = True
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END


                                    strQ = "DELETE FROM hrmsconfiguration..cfuser_privilege WHERE userprivilegeunkid = '" & dRemPvg(iCnt)("userprivilegeunkid") & "' "

                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If
                            Next
                        End If
                    End If

                    Dim strPrivilegeUnkid() As String = Split(mstrAssignPrivilegeIDs, ",")
                    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
                        objDataOperation.ClearParameters()
                        If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = '" & mintUserunkid & "' AND privilegeunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then

                            strQ = "INSERT INTO hrmsconfiguration..cfuser_privilege(userunkid, privilegeunkid) " & _
                                        "VALUES(@userunkid, @privilegeunkid); SELECT @@identity "

                            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                            objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

                            dsTran = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
                            objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfuser_privilege", "userprivilegeunkid", dsTran.Tables(0).Rows(0)(0), 2, 1, True) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            Else
                                blnIsInserted = True
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                        End If
                    Next
                End If

                If mstrAssignCompanyPrivilegeIDs.Length > 0 Then

                    Dim dsTran As New DataSet

                    strQ = "SELECT companyacessunkid,userunkid,yearunkid FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = " & mintUserunkid

                    dsTran = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsTran.Tables(0).Rows.Count > 0 Then
                        Dim dRemPvg() As DataRow = dsTran.Tables(0).Select("yearunkid NOT IN (" & IIf(mstrAssignCompanyPrivilegeIDs = "", "0", mstrAssignCompanyPrivilegeIDs) & ")")
                        If dRemPvg.Length > 0 Then
                            For iCnt As Integer = 0 To dRemPvg.Length - 1
                                If dRemPvg(iCnt).Item("yearunkid").ToString.Trim.Length > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfcompanyaccess_privilege", "companyacessunkid", dRemPvg(iCnt)("companyacessunkid"), 2, 3, True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    Else
                                        blnIsInserted = True
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END


                                    strQ = "DELETE FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE companyacessunkid = '" & dRemPvg(iCnt)("companyacessunkid") & "' "

                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If
                            Next
                        End If
                    End If


                    Dim strPrivilegeUnkid() As String = Split(mstrAssignCompanyPrivilegeIDs, ",")
                    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
                        objDataOperation.ClearParameters()
                        If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = '" & mintUserunkid & "' AND yearunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then

                            strQ = "INSERT INTO hrmsconfiguration..cfcompanyaccess_privilege (userunkid,yearunkid) " & _
                                            "VALUES(@userunkid,@yearunkid); SELECT @@identity "

                            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

                            dsTran = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
                            objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfcompanyaccess_privilege", "companyacessunkid", dsTran.Tables(0).Rows(0)(0), 2, 1, True) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            Else
                                blnIsInserted = True
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END


                        End If
                    Next
                End If
            End If


            'S.SANDEEP |05-APR-2019| -- START

            'Pinkal (13-Jun-2019) --  'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            '[Change Position of this function because In Self service when Manager Switch to MSS/ESS or vice versa report privilege deleted rather than role if any extra report privilege given to user]
            '[Talk to Sandeep for this and see this problem and suggest to move this function to If blnIsFromUnlock = False Then]
            If AssignReportPrivilege(mintUserunkid, mintRoleunkid, objDataOperation, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'End If

            'S.SANDEEP |05-APR-2019| -- START

            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            Dim objPassOpt As New clsPassowdOptions
            If objPassOpt._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPassOpt._IsPasswordHistory AndAlso _
               (objPassOpt._PasswdLastUsedNumber > 0 OrElse objPassOpt._PasswdLastUsedDays > 0) AndAlso mstrPassword <> mstrOldPassword Then
                Dim objPasswordHistory As New clsPasswordHistory
                With objPasswordHistory
                    ._Tranguid(objDataOperation) = Guid.NewGuid().ToString
                    If mblnIsSaveAsEmployee = True Then
                        ._Userunkid = -1
                        ._Employeeunkid = mintEmployeeUnkid
                    Else
                        ._Userunkid = mintUserunkid
                        ._Employeeunkid = -1
                    End If
                    ._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
                    ._NewPassword = mstrPassword
                    ._OldPassword = mstrOldPassword
                    ._Companyunkid = mintEmployeeCompanyUnkid
                    ._ChangedUserunkid = mintChangedUserunkid
                    ._Ip = mstrClientIP
                    ._Hostname = mstrHostName
                    ._Isweb = mblnIsWeb
                    .Insert(objDataOperation)
                    If objPasswordHistory._Message <> "" Then
                        mstrMessage = objPasswordHistory._Message
                        Return False
                    End If
                End With
                objPasswordHistory = Nothing
            End If
            objPassOpt = Nothing
            'Hemant (25 May 2021) -- End           

            'S.SANDEEP |05-APR-2019| -- END
            'End If
            If blnIsInserted = False Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "", "", -1, 2, 0, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfuser_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            strQ = "UPDATE hrmsConfiguration..cfuser_master SET isactive = 0 " & _
                   " WHERE userunkid = @userunkid "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", intUnkid, "", "", -1, 3, 0, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END


        Try

            strQ = "SELECT " & _
                   "  userunkid " & _
                   ", username " & _
                   ", password " & _
                   ", ispasswordexpire " & _
                   ", roleunkid " & _
                   ", isrighttoleft " & _
                   ", exp_days " & _
                   ", languageunkid " & _
                   ", isactive " & _
                   ", creationdate " & _
                   ", islocked " & _
                   ", lockedtime " & _
                   ", lockedduration " & _
                   ", createdbyunkid " & _
                   ", firstname " & _
                   ", lastname " & _
                   ", address1 " & _
                   ", address2 " & _
                   ", phone " & _
                   ", email" & _
                   ", isaduser " & _
                   ", ismanager " & _
                   ", userdomain " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "WHERE username = @name "

            strQ &= " AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND userunkid <> @userunkid"
            End If

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'S.SANDEEP [22-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : {#0001898}
    Public Function UpdateEmployeePassword(ByVal intUserId As Integer, ByVal strPassword As String) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Try
            Dim intCompanyId, intEmployeeId As Integer
            Dim strDBName As String = ""
            intCompanyId = 0 : intEmployeeId = 0
            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     @C = U.companyunkid " & _
                       "    ,@E = U.employeeunkid " & _
                       "    ,@D = ISNULL(F.database_name,'') " & _
                       "FROM hrmsConfiguration..cfuser_master AS U " & _
                       "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran AS F ON U.companyunkid = F.companyunkid " & _
                       "WHERE U.userunkid = @U And U.isactive = 1 "

                objDo.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId, ParameterDirection.InputOutput)
                objDo.AddParameter("@E", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId, ParameterDirection.InputOutput)
                objDo.AddParameter("@D", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDBName, ParameterDirection.InputOutput)
                objDo.AddParameter("@U", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                intCompanyId = Convert.ToInt32(objDo.GetParameterValue("@C"))
                intEmployeeId = Convert.ToInt32(objDo.GetParameterValue("@E"))
                strDBName = objDo.GetParameterValue("@D").ToString()

                objDo.ClearParameters()

                If intCompanyId > 0 AndAlso intEmployeeId > 0 AndAlso strDBName.Trim.Length > 0 Then

                    StrQ = "UPDATE " & strDBName & "..hremployee_master " & _
                           "SET password = @P " & _
                           "WHERE companyunkid = @C AND employeeunkid = @E "

                    objDo.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                    objDo.AddParameter("@E", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                    objDo.AddParameter("@P", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, clsSecurity.Encrypt(strPassword, "ezee"))

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                End If

            End Using
            blnFlag = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEmployeePassword; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP [22-Jan-2018] -- END

    'S.SANDEEP [20-Feb-2018] -- START
    'ISSUE/ENHANCEMENT : {#0001984}
    Public Function UpdateGlobalPasswordExpiryDate(ByVal intDays As Integer, ByVal strDate As DateTime, ByVal intUserId As Integer, ByVal strIP As String, ByVal strHost As String) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation
                StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results " & _
                       "CREATE TABLE #Results (userid int,exp_days int,username nvarchar(max)) "
                Select Case intDays
                    Case -1
                        StrQ &= "UPDATE hrmsConfiguration..cfuser_master SET exp_days = 0 "
                    Case Else
                        StrQ &= "UPDATE hrmsConfiguration..cfuser_master SET " & _
                               " exp_days = CASE WHEN ISNULL(exp_days,0) <= 0 THEN CAST(CONVERT(NVARCHAR(8),DATEADD(DAY,@day,GETDATE()),112) AS INT) " & _
                               "                 WHEN ISNULL(exp_days,0) > 0 THEN CAST(CONVERT(NVARCHAR(8),DATEADD(DAY,@day,CAST(CAST(exp_days AS NVARCHAR(8)) AS DATETIME)),112) AS INT) END "                              
                End Select
                StrQ &= "OUTPUT INSERTED.userunkid,INSERTED.exp_days,INSERTED.username INTO #Results " & _
                        "WHERE userunkid <> 1 " & _
                        "INSERT INTO hrmsConfiguration..atconfigcommon_tranlog(parentunkid,childunkid,parenttable_name,childtable_name,parentaudittype,childaudittype,audituserunkid,auditdatetime,ip,host,parent_xmlvalue,child_xmlvalue,form_name) " & _
                        "SELECT userid,0,'cfuser_master','',2,0,@audituserunkid,@auditdatetime,@ip,@host,F.avalue,NULL,@form_name " & _
                        "FROM #Results " & _
                        "JOIN " & _
                        "( " & _
                        "SELECT " & _
                         "usr.userunkid " & _
                        ",CAST(( " & _
                             "SELECT " & _
                             "* " & _
                             "FROM hrmsConfiguration..cfuser_master AS cu " & _
                             "WHERE usr.userunkid = cu.userunkid " & _
                             "FOR XML PATH(''),ROOT('cfuser_master'),ELEMENTS XSINIL) AS XML) as avalue " & _
                        "FROM hrmsConfiguration..cfuser_master AS usr " & _
                        "WHERE userunkid <> 1 " & _
                        "GROUP BY usr.userunkid " & _
                        ") AS F ON F.userunkid = #Results.userid "

                objDo.AddParameter("@day", SqlDbType.Int, eZeeDataType.INT_SIZE, intDays)
                objDo.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objDo.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strDate)
                objDo.AddParameter("@form_name", SqlDbType.NVarChar, 500, eZee.Common.eZeeForm.mstrForm_Name)
                objDo.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDo.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIP)
                objDo.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strHost)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results "

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateGlobalPasswordExpiryDate; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [20-Feb-2018] -- END

    'S.SANDEEP |05-APR-2019| -- START
    Private Function AssignReportPrivilege(ByVal intUserId As Integer, _
                                           ByVal intRoleId As Integer, _
                                           ByVal objDataOpr As clsDataOperation, _
                                           ByVal blnfromUpdate As Boolean) As Boolean
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim StrQ As String = String.Empty

        objDataOpr.ClearParameters()

        Try
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END
            If blnfromUpdate = True Then

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {ARUTI-725|#0003820}
                Dim iCount As Integer = -1
                StrQ = "SELECT 1 FROM hrmsConfiguration..cfreport_role_mapping_tran AS RMT WHERE RMT.roleunkid = @Rid "
                objDataOpr.AddParameter("@Rid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleId)
                iCount = objDataOpr.RecordCount(StrQ)
                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If
                If iCount <= 0 Then Return True
                objDataOpr.ClearParameters()
                'S.SANDEEP |16-MAY-2019| -- END

                StrQ = "SELECT reportprivilegeunkid FROM hrmsConfiguration..cfuser_reportprivilege WHERE userunkid = @Uid "

                objDataOpr.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                Dim row = dsList.Tables(0).AsEnumerable().Select(Function(x) x).ToList()
              
                For Each ro In row
                    If objCommonATLog.Insert_TranAtLog(objDataOpr, "cfuser_master", "userunkid", intUserId, "cfuser_reportprivilege", "reportprivilegeunkid", CInt(ro("reportprivilegeunkid")), 2, 3, True) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM hrmsConfiguration..cfuser_reportprivilege WHERE reportprivilegeunkid = @reportprivilegeunkid "

                    objDataOpr.AddParameter("@reportprivilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(ro("reportprivilegeunkid")))
                    objDataOpr.ExecNonQuery(StrQ)

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If
            objDataOpr.ClearParameters()
            StrQ = "SELECT " & _
                   "    fn.companyunkid " & _
                   "FROM hrmsConfiguration..cfcompanyaccess_privilege AS cp " & _
                   "    JOIN hrmsConfiguration..cffinancial_year_tran AS fn ON cp.yearunkid = fn.yearunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master AS ur ON cp.userunkid = ur.userunkid " & _
                   "WHERE ur.ismanager = 1 AND ur.userunkid = @Uid "

            objDataOpr.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Dim strCompanyIds As String = String.Empty
            If dsList.Tables("List").Rows.Count > 0 Then

                strCompanyIds = String.Join("','", dsList.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("companyunkid").ToString()).ToArray())

                If strCompanyIds.Trim.Length > 0 Then strCompanyIds = "'" & strCompanyIds & "'"

                objDataOpr.ClearParameters()

                StrQ = "SELECT DISTINCT " & _
                       "     reportunkid " & _
                       "    ,companyunkid " & _
                       "FROM hrmsConfiguration..cfreport_role_mapping_tran " & _
                       "WHERE roleunkid = @Rid AND companyunkid IN(" & strCompanyIds & ") "

                objDataOpr.AddParameter("@Rid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleId)

                Dim dstran As New DataSet : dstran = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                Dim dr = dstran.Tables(0).AsEnumerable().Select(Function(x) x).ToList()
                dsList = New DataSet
                For Each r In dr
                    objDataOpr.ClearParameters()
                    StrQ = "INSERT INTO hrmsConfiguration..cfuser_reportprivilege(userunkid,reportunkid,companyunkid,isfavorite) " & _
                           "VALUES(@userunkid,@reportunkid,@companyunkid,@isfavorite);SELECT @@identity; "

                    objDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                    objDataOpr.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(r("reportunkid")))
                    objDataOpr.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(r("companyunkid")))
                    objDataOpr.AddParameter("@isfavorite", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                    dsList = objDataOpr.ExecQuery(StrQ, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    If objCommonATLog.Insert_TranAtLog(objDataOpr, "cfuser_master", "userunkid", intUserId, "cfuser_reportprivilege", "reportprivilegeunkid", dsList.Tables(0).Rows(0)(0), 1, 1, True) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AssignReportPrivilege; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |05-APR-2019| -- END

#Region " Public Methods "

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    
    Public Function getComboList(Optional ByVal strListName As String = "List", _
                                 Optional ByVal mblnFlag As Boolean = False, _
                                 Optional ByVal blnAdUser As Boolean = False, _
                                 Optional ByVal blnOnlyEmp As Boolean = False, _
                                 Optional ByVal intCompanyId As Integer = 0, _
                                 Optional ByVal intPrivilegeId As Integer = 0, _
                                 Optional ByVal intYearid As Integer = 0) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If


        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END


        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As userunkid , @ItemName As  name , '' As Display UNION "
            End If
            strQ &= "SELECT hrmsConfiguration..cfuser_master.userunkid,hrmsConfiguration..cfuser_master.username  As name,ISNULL(firstname,'') + ' ' + ISNULL(lastname,'') AS Display FROM hrmsConfiguration..cfuser_master "

            If intCompanyId > 0 Then
                strQ &= " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                        "   AND hrmsConfiguration..cfuser_privilege.privilegeunkid = " & intPrivilegeId & " " & _
                        " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON hrmsConfiguration..cfuser_master.userunkid = hrmsConfiguration..cfcompanyaccess_privilege.userunkid  " & _
                        " JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cffinancial_year_tran.yearunkid =  hrmsConfiguration..cfcompanyaccess_privilege.yearunkid "

                If intYearid <= 0 Then intYearid = FinancialYear._Object._YearUnkid
                If intYearid > 0 Then
                    strQ &= " AND hrmsConfiguration..cffinancial_year_tran.yearunkid = '" & intYearid & "' "
                End If
            End If

            strQ &= " WHERE isactive =1 "


            If blnAdUser Then
                strQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
            Else
                strQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "
            End If

            If blnOnlyEmp Then
                strQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "

                If intCompanyId > 0 Then
                    strQ &= " AND (hrmsConfiguration..cfuser_master.companyunkid = " & intCompanyId & " OR hrmsConfiguration..cfuser_master.companyunkid <=0) "
                End If
            Else
                strQ &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0  OR hrmsConfiguration..cfuser_master.userunkid = 1) "
            End If

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>  
    Public Function getNewComboList(ByVal strListName As String, _
                                    Optional ByVal intUserId As Integer = 0, _
                                    Optional ByVal mblnFlag As Boolean = False, _
                                    Optional ByVal intCompanyId As Integer = -1, _
                                    Optional ByVal strCSVPrivilegeIds As String = "", _
                                    Optional ByVal intYearid As Integer = 0, _
                                    Optional ByVal blnIsIncludeCompany As Boolean = True) As DataSet
        'Nilay (01-Mar-2016) -- [blnIsIncludeCompany]

        'S.SANDEEP [20-JUN-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#244}
        'Optional ByVal intPrivilegeId As Integer = 0, _            -> REMOVED
        'Optional ByVal strCSVPrivilegeIds As String = "", _            -> ADDED
        'S.SANDEEP [20-JUN-2018] -- END

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try

            If mblnFlag = True Then
                StrQ = "SELECT 0 As userunkid , @ItemName As  name , '' As Display,0 AS companyunkid UNION "
            End If
            StrQ &= "SELECT hrmsConfiguration..cfuser_master.userunkid,hrmsConfiguration..cfuser_master.username  As name,ISNULL(firstname,'') + ' ' + ISNULL(lastname,'') AS Display,cfuser_master.companyunkid FROM hrmsConfiguration..cfuser_master "

            If intCompanyId > 0 Then
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If intPrivilegeId > 0 Then
                '    StrQ &= " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                '            "   AND hrmsConfiguration..cfuser_privilege.privilegeunkid = " & intPrivilegeId & " " & _
                '            " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON hrmsConfiguration..cfuser_master.userunkid = hrmsConfiguration..cfcompanyaccess_privilege.userunkid  " & _
                '            " JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cffinancial_year_tran.yearunkid =  hrmsConfiguration..cfcompanyaccess_privilege.yearunkid "
                '    If intYearid <= 0 Then intYearid = FinancialYear._Object._YearUnkid
                '    If intYearid > 0 Then
                '        StrQ &= " AND hrmsConfiguration..cffinancial_year_tran.yearunkid = '" & intYearid & "' "
                '    End If
                'End If

                If strCSVPrivilegeIds.Trim.Length > 0 Then
                    StrQ &= " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                            "   AND hrmsConfiguration..cfuser_privilege.privilegeunkid IN (" & strCSVPrivilegeIds & ") " & _
                            " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON hrmsConfiguration..cfuser_master.userunkid = hrmsConfiguration..cfcompanyaccess_privilege.userunkid  " & _
                            " JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cffinancial_year_tran.yearunkid =  hrmsConfiguration..cfcompanyaccess_privilege.yearunkid "
                    If intYearid <= 0 Then intYearid = FinancialYear._Object._YearUnkid
                    If intYearid > 0 Then
                        StrQ &= " AND hrmsConfiguration..cffinancial_year_tran.yearunkid = '" & intYearid & "' "
                    End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END
            End If

            StrQ &= " WHERE hrmsConfiguration..cfuser_master.isactive =1 "


            Dim objOption As New clsPassowdOptions
            If objOption._EnableAllUser = False Then
                Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                If drOption.Length > 0 Then
                    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                        If objOption._IsEmployeeAsUser Then
                            StrQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "

                            'Nilay (01-Mar-2016) -- Start
                            'ENHANCEMENT - Implementing External Approval changes 
                            'If intCompanyId > 0 Then
                            '    StrQ &= " AND (hrmsConfiguration..cfuser_master.companyunkid = " & intCompanyId & " OR hrmsConfiguration..cfuser_master.companyunkid <=0) "
                            'End If
                            If blnIsIncludeCompany = True Then
                            If intCompanyId > 0 Then
                                StrQ &= " AND (hrmsConfiguration..cfuser_master.companyunkid = " & intCompanyId & " OR hrmsConfiguration..cfuser_master.companyunkid <=0) "
                            End If
                            End If
                            'Nilay (01-Mar-2016) -- End
                        Else
                            StrQ &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0  OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                        End If
                        StrQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                        StrQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    End If
                End If
            End If
            objOption = Nothing

            'S.SANDEEP [01 DEC 2015] -- START
            'If intCompanyId <> -1 Then
            '    StrQ &= " AND cfuser_master.companyunkid = '" & intCompanyId & "' "
            'End If
            'S.SANDEEP [01 DEC 2015] -- END

            

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getNewComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get the List of Privileges </purpose>
    Public Function getPrivilegeList(ByVal intRoleId As Integer, ByVal intUserId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsAbilityPrivilege As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try

            StrQ = "SELECT ISNULL(hrmsConfiguration..cfrole_master.privilegeunkid,'-1') As privilegeunkid FROM hrmsConfiguration..cfrole_master WHERE roleunkid = @RoleId"

            objDataOperation.AddParameter("@RoleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleId)

            dsAbilityPrivilege = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsAbilityPrivilege.Tables(0).Rows.Count > 0 Then

                If dsAbilityPrivilege.Tables(0).Rows.Count >= 1 And dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "-1" Then

                    StrQ = "SELECT " & _
                               "    hrmsconfiguration..cfuserprivilege_master.privilegeunkid " & _
                               "   ,privilege_name " & _
                               "   ,hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid " & _
                               "   ,hrmsconfiguration..cfuserprivilegegroup_master.name AS group_name " & _
                               "   ,(CASE WHEN hrmsconfiguration..cfuser_privilege.privilegeunkid IS NULL THEN 0 ELSE 1 END ) AS assign " & _
                               "   ,(CASE WHEN hrmsconfiguration..cfuser_privilege.privilegeunkid IS NULL THEN 'False' ELSE 'True' END ) AS iassign " & _
                               "FROM hrmsconfiguration..cfuserprivilege_master " & _
                               "    LEFT OUTER JOIN hrmsconfiguration..cfuser_privilege ON hrmsconfiguration..cfuserprivilege_master.privilegeunkid = hrmsconfiguration..cfuser_privilege.privilegeunkid " & _
                               "    AND hrmsconfiguration..cfuser_privilege.userunkid = @userunkid " & _
                               "   ,hrmsconfiguration..cfuserprivilegegroup_master " & _
                               "WHERE hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsconfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
                               "    AND hrmsconfiguration..cfuserprivilege_master.privilegeunkid IN (SELECT Distinct hrmsconfiguration..cfuser_privilege.privilegeunkid " & _
                               "FROM hrmsconfiguration..cfuser_privilege, hrmsconfiguration..cfuserprivilege_master " & _
                               "WHERE userunkid = @mainuserunkid) " & _
                               "    AND isdeleted = 'N' "
                    If intUserId <= 0 Then
                        If dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "" And dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "-1" Then
                            StrQ &= "AND cfuserprivilege_master.privilegeunkid NOT IN (" & dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") & ") " & _
                            "UNION " & _
                                "SELECT " & _
                                   " hrmsconfiguration..cfuserprivilege_master.privilegeunkid " & _
                                   ",privilege_name " & _
                                   ",hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid " & _
                                   ",hrmsconfiguration..cfuserprivilegegroup_master.name AS group_name " & _
                                   ",1 AS assign " & _
                                   ",'True' AS iassign " & _
                                "FROM hrmsconfiguration..cfuserprivilege_master,hrmsconfiguration..cfuserprivilegegroup_master " & _
                                "WHERE hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsconfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
                                "AND privilegeunkid IN (" & dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") & ") " & _
                               " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
                        Else
                            StrQ &= " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
                        End If
                    Else
                        StrQ &= " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
                    End If
                    'S.SANDEEP [21-SEP-2018] -- START {iassign} -- END

                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
                    objDataOperation.AddParameter("@mainuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 1)

                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getPrivilegeList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList.Dispose()
            dsList = Nothing
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get the List of Company Access Privileges </purpose>
    Public Function getCompanyPrivilegeList(ByVal intUserId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            StrQ = "SELECT " & _
                       "	 ISNULL(hrmsConfiguration..cfcompany_master.name,'')AS group_name " & _
                       "	,ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name,'') AS privilege_name " & _
                       "    ,hrmsConfiguration..cfcompany_master.companyunkid " & _
                       "	,CASE WHEN hrmsconfiguration..cfcompanyaccess_privilege.yearunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                       "	,hrmsConfiguration..cfcompany_master.companyunkid As privilegegroupunkid " & _
                       "	," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid As privilegeunkid " & _
                       "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
                       "	LEFT OUTER JOIN hrmsconfiguration..cfcompanyaccess_privilege ON " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = hrmsconfiguration..cfcompanyaccess_privilege.yearunkid " & _
                       "	AND hrmsconfiguration..cfcompanyaccess_privilege.userunkid = @userunkid " & _
                       ",hrmsConfiguration..cfcompany_master " & _
                       "WHERE hrmsConfiguration..cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                       "	AND ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 " & _
                       "ORDER BY hrmsConfiguration..cfcompany_master.companyunkid "


            'Gajanan [19-OCT-2019] -- ADD [companyunkid]    

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getCompanyPrivilegeList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList.Dispose()
            dsList = Nothing
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get the List of Roles With Privileges </purpose>
    Public Function GetAbilityGroupList() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            StrQ = "SELECT " & _
                   "   hrmsConfiguration..cfrole_master.roleunkid " & _
                   "  ,hrmsConfiguration..cfrole_master.name " & _
                   "  ,ISNULL(hrmsConfiguration..cfrole_master.privilegeunkid ,'') As privilegeunkid " & _
                   "FROM hrmsConfiguration..cfrole_master " & _
                   "WHERE hrmsConfiguration..cfrole_master.isactive = 1 " & _
                   "ORDER BY hrmsConfiguration..cfrole_master.roleunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "list")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAbilityGroupList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList.Dispose()
            dsList = Nothing
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get the List of Groups With Privileges </purpose>
    Public Function GetPrivilegeGroupList() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try

            StrQ = "SELECT " & _
                   "    privilegeunkid " & _
                   "   ,privilege_name " & _
                   "   ,hrmsConfiguration..cfuserprivilege_master.privilegegroupunkid " & _
                   "   ,hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
                   "   ,hrmsConfiguration..cfuserprivilegegroup_master.name " & _
                   "FROM hrmsConfiguration..cfuserprivilege_master,hrmsConfiguration..cfuserprivilegegroup_master " & _
                   "WHERE hrmsConfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
                   "ORDER BY hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPrivilegeGroupList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList.Dispose()
            dsList = Nothing
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function


    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get the List of Ability With Privileges </purpose>
    Public Function GetPrivilegeAbilityList(ByVal StrDataBaseName As String) As DataTable
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable = Nothing

        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            Dim dCol As DataColumn

            'S.SANDEEP [21-SEP-2018] -- START
            'StrQ = "SELECT " & _
            '       "	 Privilege " & _
            '       "	,isprivilegegroup AS objisprivilegegroup" & _
            '       "	,privilegeGroupunkidunkid AS objprivilegeGroupunkidunkid " & _
            '       "	,privilegeunkid AS objprivilegeunkid " & _
            '       "	,'' AS objAUD " & _
            '       "    ,CASE WHEN isprivilegegroup = 1 THEN SUM(1) OVER(PARTITION BY privilegeGroupunkidunkid) -1 ELSE 0 END AS CNT " & _
            '       "    ," & _
            '       "    ," & _
            '       "FROM " & _
            '       "( " & _
            '       "	SELECT " & _
            '       "		 name AS Privilege " & _
            '       "		,CAST(1 AS BIT) AS isprivilegegroup " & _
            '       "		,privilegegroupunkid AS privilegeGroupunkidunkid " & _
            '       "		,0 AS  privilegeunkid " & _
            '       "        ," & _
            '       "        ," & _
            '       "	FROM " & StrDataBaseName & "..cfuserprivilegegroup_master " & _
            '       "	WHERE isactive = 1 " & _
            '       "UNION " & _
            '       "	SELECT " & _
            '       "		 SPACE(5) + privilege_name AS Privilege " & _
            '       "		,CAST(0 AS BIT) AS isprivilegegroup " & _
            '       "		,privilegegroupunkid AS privilegeGroupunkidunkid " & _
            '       "		,privilegeunkid AS  privilegeunkid " & _
            '       "	FROM " & StrDataBaseName & "..cfuserprivilege_master " & _
            '       "	WHERE isactive = 1 " & _
            '       ") AS Grp ORDER BY Grp.privilegeGroupunkidunkid,Grp.isprivilegegroup DESC "


            StrQ = "SELECT " & _
                   "	 Privilege " & _
                   "	,isprivilegegroup AS objisprivilegegroup" & _
                   "	,privilegeGroupunkidunkid AS objprivilegeGroupunkidunkid " & _
                   "	,privilegeunkid AS objprivilegeunkid " & _
                   "	,'' AS objAUD " & _
                   "    ,CASE WHEN isprivilegegroup = 1 THEN SUM(1) OVER(PARTITION BY privilegeGroupunkidunkid) -1 ELSE 0 END AS CNT " & _
                   "    ,Grp.objPrivilegeName " & _
                   "    ,name AS objPrivilegeGrp " & _
                   "FROM " & _
                   "( " & _
                   "	SELECT " & _
                   "		 name AS Privilege " & _
                   "		,CAST(1 AS BIT) AS isprivilegegroup " & _
                   "		,privilegegroupunkid AS privilegeGroupunkidunkid " & _
                   "		,0 AS  privilegeunkid " & _
                   "        ,name as objPrivilegeGrp " & _
                   "        ,'' AS objPrivilegeName " & _
                   "	FROM " & StrDataBaseName & "..cfuserprivilegegroup_master " & _
                   "	WHERE isactive = 1 " & _
                   "UNION " & _
                   "	SELECT " & _
                   "		 SPACE(5) + privilege_name AS Privilege " & _
                   "		,CAST(0 AS BIT) AS isprivilegegroup " & _
                   "		,privilegegroupunkid AS privilegeGroupunkidunkid " & _
                   "		,privilegeunkid AS  privilegeunkid " & _
                   "        ,'' as objPrivilegeGrp " & _
                   "        ,privilege_name AS objPrivilegeName " & _
                   "	FROM " & StrDataBaseName & "..cfuserprivilege_master " & _
                   "	WHERE isactive = 1 " & _
                   ") AS Grp " & _
                   "LEFT JOIN hrmsConfiguration..cfuserprivilegegroup_master ON Grp.privilegeGroupunkidunkid = privilegegroupunkid " & _
                   "ORDER BY Grp.privilegeGroupunkidunkid,Grp.isprivilegegroup DESC "
            'S.SANDEEP [21-SEP-2018] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "AbilityLevel")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0).Copy

            StrQ = "SELECT " & _
                   "	 roleunkid " & _
                   "	,name " & _
                   "	,ISNULL(privilegeunkid,'') AS privilegeunkid " & _
                   "FROM " & StrDataBaseName & "..cfrole_master " & _
                   "WHERE " & StrDataBaseName & "..cfrole_master.isactive = 1 " & _
                   "ORDER BY " & StrDataBaseName & "..cfrole_master.roleunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "Roles")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mDictionary As New Dictionary(Of Integer, DataSet)

            For Each row As DataRow In dsList.Tables("Roles").Rows
                dCol = New DataColumn
                dCol.ColumnName = row.Item("name")
                dCol.ExtendedProperties.Add(row.Item("name"), row.Item("roleunkid"))
                dCol.DataType = System.Type.GetType("System.Boolean")
                dCol.DefaultValue = False
                dtTable.Columns.Add(dCol)

                If row.Item("privilegeunkid").ToString.Trim.Length > 0 Then
                    If mDictionary.ContainsKey(row.Item("roleunkid")) = False Then
                        mDictionary.Add(row.Item("roleunkid"), GetCSVPrivilegeAsDataSet(row.Item("privilegeunkid")))
                    End If
                End If

                dCol = New DataColumn
                dCol.ColumnName = "objAUD" & row.Item("name").ToString
                dCol.ExtendedProperties.Add(dCol.ColumnName, row.Item("roleunkid"))
                dCol.DataType = System.Type.GetType("System.String")
                dCol.DefaultValue = ""
                dtTable.Columns.Add(dCol)
            Next

            For Each xCol As DataColumn In dtTable.Columns
                If xCol.ExtendedProperties.Count > 0 Then
                    Dim StrColName As String = ""
                    StrColName = xCol.ColumnName

                    If StrColName.StartsWith("objAUD") Then Continue For

                    If mDictionary.ContainsKey(xCol.ExtendedProperties(StrColName)) Then
                        Dim rows = (From A In dtTable.AsEnumerable() Join B In mDictionary(xCol.ExtendedProperties(StrColName)).Tables(0).AsEnumerable() _
                                    On A.Item("objprivilegeunkid") Equals B.Item("privilegeunkid") Select (A))

                        For Each ro As DataRow In rows
                            ro.Item(StrColName) = True
                        Next

                    End If

                    Dim grprows = (From T In dtTable.AsEnumerable().Where(Function(y) y.Field(Of Boolean)("objisprivilegegroup") = True) Select (T))
                    For Each ro As DataRow In grprows
                        Dim iGrpUnkid As Integer = 0
                        iGrpUnkid = ro.Item("objprivilegeGroupunkidunkid")
                        Dim xCount = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(StrColName) = True And x.Field(Of Integer)("objprivilegeGroupunkidunkid") = iGrpUnkid).Count()
                        If xCount > 0 Then
                            ro.Item(StrColName) = True
                        End If
                    Next

                End If
            Next
            dtTable.Columns.Remove("CNT")
            dtTable.AcceptChanges()

            dtTable.Columns("objisprivilegegroup").SetOrdinal(dtTable.Columns(dtTable.Columns.Count - 1).Ordinal)
            dtTable.Columns("objprivilegeGroupunkidunkid").SetOrdinal(dtTable.Columns("objisprivilegegroup").Ordinal)
            dtTable.Columns("objprivilegeunkid").SetOrdinal(dtTable.Columns("objprivilegeGroupunkidunkid").Ordinal)
            dtTable.Columns("objAUD").SetOrdinal(dtTable.Columns("objprivilegeunkid").Ordinal)

            'S.SANDEEP [21-SEP-2018] -- START
            dtTable.Columns("objPrivilegeName").SetOrdinal(dtTable.Columns(dtTable.Columns.Count - 1).Ordinal)
            dtTable.Columns("objPrivilegeGrp").SetOrdinal(dtTable.Columns(dtTable.Columns.Count - 1).Ordinal)
            'S.SANDEEP [21-SEP-2018] -- END

            dtTable.AcceptChanges()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPrivilegeAbilityList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    Private Function GetCSVPrivilegeAsDataSet(ByVal strCSVPrivilegeIds As String) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            StrQ = "DECLARE @words VARCHAR (MAX) " & _
                   "   SET @words = '" & strCSVPrivilegeIds & "' " & _
                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                   "WHILE @start < @stop begin " & _
                   "    SELECT " & _
                   "       @end   = CHARINDEX(',',@words,@start) " & _
                   "      ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                   "      ,@start = @end+1 " & _
                   "    INSERT @split VALUES (@word) " & _
                   "END " & _
                   "SELECT CAST(word AS INT) AS privilegeunkid FROM @split ORDER BY CAST(word AS INT) "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCSVPrivilegeAsDataSet; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Set the List of Ability With Privileges </purpose>
    Public Function SaveAbilityLevelPrivilege(ByVal intAbilityLevelUnkid As Integer, ByVal mPrivilegeIdsDictionary As Dictionary(Of Integer, String), ByVal strPrivilegeUnkid As String) As Boolean
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = ""
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            If mPrivilegeIdsDictionary.Keys.Count > 0 Then
                Dim dsUsr As New DataSet

                StrQ = "SELECT userunkid,roleunkid FROM hrmsConfiguration..cfuser_master WHERE isactive = 1 AND roleunkid = '" & intAbilityLevelUnkid & "' AND userunkid <> 1"

                dsUsr = objDataOperation.ExecQuery(StrQ, "List")

                Dim dUserList = From usr In dsUsr.Tables(0).AsEnumerable() Select usr

                'Dim strPrivilegeUnkid As String = String.Join(",", mPrivilegeIdsDictionary.Where(Function(x) x.Value <> "D").Select(Function(ikey) CStr(ikey.Key)).ToArray())

                For Each iUsr In dUserList

                    For Each iPrivilegeId As Integer In mPrivilegeIdsDictionary.Keys

                        Select Case mPrivilegeIdsDictionary(iPrivilegeId)

                            Case "A"

                                If objDataOperation.RecordCount("SELECT userunkid,privilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & iUsr.Item("userunkid") & "' AND privilegeunkid = '" & iPrivilegeId & "'") <= 0 Then

                                    StrQ = "INSERT INTO hrmsConfiguration..cfuser_privilege (userunkid,privilegeunkid) " & _
                                           "SELECT TOP 1 '" & iUsr.Item("userunkid") & "','" & iPrivilegeId & "'; SELECT @@identity "

                                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If dsList.Tables("List").Rows(0)(0).ToString.Trim.Length > 0 Then
                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        Dim objCommonATLog As New clsCommonATLog
                                        objCommonATLog._FormName = mstrFormName
                                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                        objCommonATLog._ClientIP = mstrClientIP
                                        objCommonATLog._HostName = mstrHostName
                                        objCommonATLog._FromWeb = mblnIsWeb
                                        objCommonATLog._AuditUserId = mintAuditUserId
                                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                        objCommonATLog._AuditDate = mdtAuditDate
                                        'S.SANDEEP [28-May-2018] -- END

                                        If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cfuser_privilege", "userprivilegeunkid", dsList.Tables("List").Rows(0)(0), True) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        objCommonATLog = Nothing
                                        'S.SANDEEP [28-May-2018] -- END

                                    End If

                                End If


                            Case "D"


                                StrQ = "SELECT userprivilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & iUsr.Item("userunkid") & "' AND privilegeunkid = '" & iPrivilegeId & "'"

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If dsList.Tables(0).Rows.Count > 0 Then

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cfuser_privilege", "userprivilegeunkid", dsList.Tables(0).Rows(0)("userprivilegeunkid"), True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END


                                    StrQ = "DELETE FROM hrmsConfiguration..cfuser_privilege WHERE userprivilegeunkid = '" & dsList.Tables(0).Rows(0)("userprivilegeunkid") & "' "

                                    objDataOperation.ExecNonQuery(StrQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                        End Select

                    Next

                Next

                StrQ = "UPDATE hrmsConfiguration..cfrole_master " & _
                   "SET hrmsConfiguration..cfrole_master.privilegeunkid = @privilegeunkid " & _
                   "WHERE hrmsConfiguration..cfrole_master.roleunkid = @RoleId "

                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@privilegeunkid", SqlDbType.VarChar, 8000, strPrivilegeUnkid)
                objDataOperation.AddParameter("@RoleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAbilityLevelUnkid.ToString)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [18 Jan 2016] -- START
                'If iObjDataOpr Is Nothing Then
                '    objDataOperation.ReleaseTransaction(True)
                'End If
                'S.SANDEEP [18 Jan 2016] -- END

            End If

            'S.SANDEEP [18 Jan 2016] -- START
                If iObjDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            'S.SANDEEP [18 Jan 2016] -- END

        Catch ex As Exception
            'S.SANDEEP [18 Jan 2016] -- START
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [18 Jan 2016] -- END
            Throw New Exception(ex.Message & "; Procedure Name: SaveAbilityLevelPrivilege; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [10 AUG 2015] -- END

    Public Function GetUserAccessList(ByVal StrAllocation As String, Optional ByVal intUserId As Integer = -1, Optional ByVal mintCompanyId As Integer = -1) As DataTable
        Dim dtUAccess As New DataTable("Access")
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dsTranList As New DataSet
        Dim StrDBName As String = String.Empty
        Dim intComId As Integer = 0
        Dim intYearId As Integer = 0
        Dim intRowCnt As Integer = 0
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            dtUAccess.Columns.Add("IsAssign", System.Type.GetType("System.Boolean")).DefaultValue = 0
            dtUAccess.Columns.Add("UserAccess", System.Type.GetType("System.String")).DefaultValue = ""
            dtUAccess.Columns.Add("CompanyId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtUAccess.Columns.Add("YearId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtUAccess.Columns.Add("AllocationId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtUAccess.Columns.Add("AllocationLevel", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtUAccess.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = 0
            dtUAccess.Columns.Add("TotAllocation", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtUAccess.Columns.Add("AllocationRefId", System.Type.GetType("System.Int32")).DefaultValue = 0

            StrQ = "SELECT " & _
                   "	 ISNULL(hrmsConfiguration..cfcompany_master.name,'')+' - ( '+ ISNULL(hrmsConfiguration..cffinancial_year_tran.database_name,'')+' )' AS GrpName " & _
                   "	,hrmsConfiguration..cfcompany_master.companyunkid AS CompId " & _
                   "	,hrmsConfiguration..cffinancial_year_tran.yearunkid AS YearId " & _
                   "	,1 AS IsGrp " & _
                   "	,hrmsConfiguration..cffinancial_year_tran.database_name AS DBName " & _
                   "FROM hrmsConfiguration..cfcompany_master " & _
                   "	JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cfcompany_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
                   "    JOIN sys.databases ON sys.databases.name = hrmsConfiguration..cffinancial_year_tran.database_name " & _
                   "WHERE hrmsConfiguration..cfcompany_master.isactive = 1 "

            If mintCompanyId > 0 Then
                StrQ &= " AND hrmsConfiguration..cfcompany_master.companyunkid = '" & mintCompanyId & "' "
            End If

            StrQ &= "ORDER BY hrmsConfiguration..cfcompany_master.companyunkid,hrmsConfiguration..cffinancial_year_tran.yearunkid DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim dsalloc As New DataSet
            dsalloc = (New clsMasterData).GetEAllocation_Notification("List", "", False)
            'S.SANDEEP [20-JUN-2018] -- END

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                StrDBName = "" : intComId = 0 : intYearId = 0
                StrDBName = dtRow.Item("DBName").ToString.Trim
                intComId = dtRow.Item("CompId")
                intYearId = dtRow.Item("YearId")

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                For Each IntKey As Integer In StrAllocation.Split(",")
                    If intUserId > 0 Then
                        Select Case IntKey
                            Case enAllocation.BRANCH
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrstation_master.stationunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrstation_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrstation_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrstation_master.stationunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrstation_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrstation_master.stationunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrstation_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrstation_master.stationunkid = A.stationunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrstation_master.isactive, 0) = 1 "

                            Case enAllocation.DEPARTMENT_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrdepartment_group_master.deptgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrdepartment_group_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrdepartment_group_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrdepartment_group_master.deptgroupunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "     ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrdepartment_group_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrdepartment_group_master.deptgroupunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "'  " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrdepartment_group_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrdepartment_group_master.deptgroupunkid = A.deptgroupunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_group_master.isactive, 0) = 1 "


                            Case enAllocation.DEPARTMENT
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrdepartment_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrdepartment_master.departmentunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrdepartment_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrdepartment_master.departmentunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrdepartment_master.departmentunkid = A.departmentunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "

                            Case enAllocation.SECTION_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrsectiongroup_master.sectiongroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrsectiongroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrsectiongroup_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrsectiongroup_master.sectiongroupunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrsectiongroup_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrsectiongroup_master.sectiongroupunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrsectiongroup_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrsectiongroup_master.sectiongroupunkid = A.sectiongroupunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrsectiongroup_master.isactive, 0) = 1 "

                            Case enAllocation.SECTION
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrsection_master.sectionunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrsection_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrsection_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrsection_master.sectionunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrsection_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrsection_master.sectionunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrsection_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrsection_master.sectionunkid = A.sectionunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrsection_master.isactive, 0) = 1 "

                            Case enAllocation.UNIT_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrunitgroup_master.unitgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrunitgroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrunitgroup_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrunitgroup_master.unitgroupunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrunitgroup_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrunitgroup_master.unitgroupunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrunitgroup_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrunitgroup_master.unitgroupunkid = A.unitgroupunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrunitgroup_master.isactive, 0) = 1 "

                            Case enAllocation.UNIT
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrunit_master.unitunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrunit_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrunit_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrunit_master.unitunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrunit_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrunit_master.unitunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrunit_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrunit_master.unitunkid = A.unitunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrunit_master.isactive, 0) = 1 "

                            Case enAllocation.TEAM
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrteam_master.teamunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrteam_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrteam_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrteam_master.teamunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrteam_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrteam_master.teamunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrteam_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrteam_master.teamunkid = A.teamunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrteam_master.isactive, 0) = 1 "

                            Case enAllocation.JOB_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrjobgroup_master.jobgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrjobgroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrjobgroup_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrjobgroup_master.jobgroupunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrjobgroup_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrjobgroup_master.jobgroupunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrjobgroup_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrjobgroup_master.jobgroupunkid = A.jobgroupunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrjobgroup_master.isactive, 0) = 1 "

                            Case enAllocation.JOBS
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
                                       "	,ISNULL(" & StrDBName & "..hrjob_master.job_level, 0) AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrjob_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	 " & StrDBName & "..hrjob_master.jobunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "     ,0 AS IsGrp " & _
                                       "    FROM " & StrDBName & "..hrjob_master " & _
                                       "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrjob_master.jobunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "'  " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrjob_master.jobunkid = A.jobunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "

                            Case enAllocation.CLASS_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrclassgroup_master.classgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrclassgroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrclassgroup_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrclassgroup_master.classgroupunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrclassgroup_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrclassgroup_master.classgroupunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrclassgroup_master.classgroupunkid = A.classgroupunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 "

                            Case enAllocation.CLASSES
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrclasses_master.classesunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrclasses_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,ISNULL(assign,0) AS assign " & _
                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                                       "	,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrclasses_master " & _
                                       "LEFT JOIN " & _
                                       "( " & _
                                       "	SELECT " & _
                                       "	  " & StrDBName & "..hrclasses_master.classesunkid " & _
                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                                       "	 ,0 AS IsGrp " & _
                                       "	FROM " & StrDBName & "..hrclasses_master " & _
                                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrclasses_master.classesunkid " & _
                                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                                       "            AND referenceunkid = '" & IntKey & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                                       "	WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 " & _
                                       ") AS A ON " & StrDBName & "..hrclasses_master.classesunkid = A.classesunkid " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 "
                        End Select
                    Else
                        Select Case IntKey
                            Case enAllocation.BRANCH
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrstation_master.stationunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrstation_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrstation_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrstation_master.isactive, 0) = 1 "

                            Case enAllocation.DEPARTMENT_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrdepartment_group_master.deptgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrdepartment_group_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "    ,0 AS assign " & _
                                       "    ," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrdepartment_group_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_group_master.isactive, 0) = 1 "

                            Case enAllocation.DEPARTMENT
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrdepartment_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "

                            Case enAllocation.SECTION_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrsectiongroup_master.sectiongroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrsectiongroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrsectiongroup_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrsectiongroup_master.isactive, 0) = 1 "

                            Case enAllocation.SECTION
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrsection_master.sectionunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrsection_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrsection_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrsection_master.isactive, 0) = 1 "

                            Case enAllocation.UNIT_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrunitgroup_master.unitgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrunitgroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrunitgroup_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrunitgroup_master.isactive, 0) = 1 "

                            Case enAllocation.UNIT
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrunit_master.unitunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrunit_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrunit_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrunit_master.isactive, 0) = 1 "

                            Case enAllocation.TEAM
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrteam_master.teamunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrteam_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrteam_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrteam_master.isactive, 0) = 1 "

                            Case enAllocation.JOB_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrjobgroup_master.jobgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrjobgroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrjobgroup_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrjobgroup_master.isactive, 0) = 1 "

                            Case enAllocation.JOBS
                                StrQ = "SELECT " & _
                                       "     " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
                                       "    ," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
                                       "    ,ISNULL(" & StrDBName & "..hrjob_master.job_level,0) AS AllocationLevel " & _
                                       "    ,0 AS assign " & _
                                       "    ," & intComId & " AS CompId " & _
                                       "    ," & intYearId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrjob_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "

                            Case enAllocation.CLASS_GROUP
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrclassgroup_master.classgroupunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrclassgroup_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrclassgroup_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 "

                            Case enAllocation.CLASSES
                                StrQ = "SELECT " & _
                                       "	 " & StrDBName & "..hrclasses_master.classesunkid AS AllocationId " & _
                                       "	," & StrDBName & "..hrclasses_master.name AS Allocation " & _
                                       "	,0 AS AllocationLevel " & _
                                       "	,0 AS assign " & _
                                       "	," & intComId & " AS CompId " & _
                                       "	," & intComId & " AS YearId " & _
                                       "    ,0 AS IsGrp " & _
                                       "FROM " & StrDBName & "..hrclasses_master " & _
                                       "WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 "
                        End Select
                    End If

                    dsTranList = objDataOperation.ExecQuery(StrQ, "TranList")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim StrGrp As String = ""
                    ' StrGrp = "[ " & enAllocation.JOBS.ToString & " ]"
                    Dim ival = IntKey
                    StrGrp = "[ " & dsalloc.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("Id") = ival).Select(Function(y) y.Field(Of String)("Name")).FirstOrDefault() & " ]"

                    Dim dtURow As DataRow = dtUAccess.NewRow

                    dtURow.Item("UserAccess") = dtRow.Item("GrpName") & Space(5) & StrGrp
                    dtURow.Item("CompanyId") = dtRow.Item("CompId")
                    dtURow.Item("YearId") = dtRow.Item("YearId")
                    dtURow.Item("IsGrp") = dtRow.Item("IsGrp")
                    dtURow.Item("TotAllocation") = intRowCnt
                    dtURow.Item("AllocationRefId") = IntKey

                    dtUAccess.Rows.Add(dtURow)

                    For Each dtTRow As DataRow In dsTranList.Tables("TranList").Rows
                        dtURow = dtUAccess.NewRow
                        dtURow.Item("UserAccess") = Space(5) & dtTRow.Item("Allocation").ToString
                        dtURow.Item("CompanyId") = dtRow.Item("CompId")
                        dtURow.Item("YearId") = dtRow.Item("YearId")
                        dtURow.Item("IsGrp") = dtTRow.Item("IsGrp")
                        dtURow.Item("IsAssign") = dtTRow.Item("assign")
                        dtURow.Item("AllocationId") = dtTRow.Item("AllocationId")
                        dtURow.Item("AllocationLevel") = dtTRow.Item("AllocationLevel")
                        dtURow.Item("TotAllocation") = intRowCnt
                        dtURow.Item("AllocationRefId") = IntKey
                        dtUAccess.Rows.Add(dtURow)
                    Next
                Next


                'For Each IntKey As Integer In StrAllocation.Split(",")
                '    If intUserId > 0 Then
                '        Select Case IntKey
                '            Case enAllocation.JOBS
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
                '                       "	,ISNULL(" & StrDBName & "..hrjob_master.job_level, 0) AS AllocationLevel " & _
                '                       "	,ISNULL(assign,0) AS assign " & _
                '                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                '                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                '                       "	,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrjob_master " & _
                '                       "LEFT JOIN " & _
                '                       "( " & _
                '                       "	SELECT " & _
                '                       "	 " & StrDBName & "..hrjob_master.jobunkid " & _
                '                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                '                       "     ,0 AS IsGrp " & _
                '                       "    FROM " & StrDBName & "..hrjob_master " & _
                '                       "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrjob_master.jobunkid " & _
                '                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid AND referenceunkid = '" & IntKey & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "'  " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                '                       "	WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive, 0) = 1 " & _
                '                       ") AS A ON " & StrDBName & "..hrjob_master.jobunkid = A.jobunkid " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
                '            Case enAllocation.DEPARTMENT
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
                '                       "	,0 AS AllocationLevel " & _
                '                       "	,ISNULL(assign,0) AS assign " & _
                '                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                '                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                '                       "	,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrdepartment_master " & _
                '                       "LEFT JOIN " & _
                '                       "( " & _
                '                       "	SELECT " & _
                '                       "	  " & StrDBName & "..hrdepartment_master.departmentunkid " & _
                '                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                '                       "	 ,0 AS IsGrp " & _
                '                       "	FROM " & StrDBName & "..hrdepartment_master " & _
                '                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrdepartment_master.departmentunkid " & _
                '                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                '                       "            AND referenceunkid = '" & IntKey & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                '                       "	WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 " & _
                '                       ") AS A ON " & StrDBName & "..hrdepartment_master.departmentunkid = A.departmentunkid " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "
                '            Case enAllocation.CLASS_GROUP
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrclassgroup_master.classgroupunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrclassgroup_master.name AS Allocation " & _
                '                       "	,0 AS AllocationLevel " & _
                '                       "	,ISNULL(assign,0) AS assign " & _
                '                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                '                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                '                       "	,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrclassgroup_master " & _
                '                       "LEFT JOIN " & _
                '                       "( " & _
                '                       "	SELECT " & _
                '                       "	  " & StrDBName & "..hrclassgroup_master.classgroupunkid " & _
                '                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                '                       "	 ,0 AS IsGrp " & _
                '                       "	FROM " & StrDBName & "..hrclassgroup_master " & _
                '                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrclassgroup_master.classgroupunkid " & _
                '                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                '                       "            AND referenceunkid = '" & IntKey & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                '                       "	WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 " & _
                '                       ") AS A ON " & StrDBName & "..hrclassgroup_master.classgroupunkid = A.classgroupunkid " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 "
                '            Case enAllocation.CLASSES
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrclasses_master.classesunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrclasses_master.name AS Allocation " & _
                '                       "	,0 AS AllocationLevel " & _
                '                       "	,ISNULL(assign,0) AS assign " & _
                '                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
                '                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
                '                       "	,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrclasses_master " & _
                '                       "LEFT JOIN " & _
                '                       "( " & _
                '                       "	SELECT " & _
                '                       "	  " & StrDBName & "..hrclasses_master.classesunkid " & _
                '                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
                '                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
                '                       "	 ,0 AS IsGrp " & _
                '                       "	FROM " & StrDBName & "..hrclasses_master " & _
                '                       "	    LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrclasses_master.classesunkid " & _
                '                       "	    JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                '                       "            AND referenceunkid = '" & IntKey & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
                '                       "		    AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
                '                       "	WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 " & _
                '                       ") AS A ON " & StrDBName & "..hrclasses_master.classesunkid = A.classesunkid " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 "
                '        End Select
                '    Else
                '        Select Case IntKey
                '            Case enAllocation.JOBS
                '                StrQ = "SELECT " & _
                '                       "     " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
                '                       "    ," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
                '                       "    ,ISNULL(" & StrDBName & "..hrjob_master.job_level,0) AS AllocationLevel " & _
                '                       "    ,0 AS assign " & _
                '                       "    ," & intComId & " AS CompId " & _
                '                       "    ," & intYearId & " AS YearId " & _
                '                       "    ,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrjob_master " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
                '            Case enAllocation.DEPARTMENT
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
                '                       "	,0 AS AllocationLevel " & _
                '                       "	,0 AS assign " & _
                '                       "	," & intComId & " AS CompId " & _
                '                       "	," & intComId & " AS YearId " & _
                '                       "    ,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrdepartment_master " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "
                '            Case enAllocation.CLASS_GROUP
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrclassgroup_master.classgroupunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrclassgroup_master.name AS Allocation " & _
                '                       "	,0 AS AllocationLevel " & _
                '                       "	,0 AS assign " & _
                '                       "	," & intComId & " AS CompId " & _
                '                       "	," & intComId & " AS YearId " & _
                '                       "    ,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrclassgroup_master " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 "
                '            Case enAllocation.CLASSES
                '                StrQ = "SELECT " & _
                '                       "	 " & StrDBName & "..hrclasses_master.classesunkid AS AllocationId " & _
                '                       "	," & StrDBName & "..hrclasses_master.name AS Allocation " & _
                '                       "	,0 AS AllocationLevel " & _
                '                       "	,0 AS assign " & _
                '                       "	," & intComId & " AS CompId " & _
                '                       "	," & intComId & " AS YearId " & _
                '                       "    ,0 AS IsGrp " & _
                '                       "FROM " & StrDBName & "..hrclasses_master " & _
                '                       "WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 "
                '        End Select
                '    End If

                '    dsTranList = objDataOperation.ExecQuery(StrQ, "TranList")

                '    If objDataOperation.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If

                '    Dim StrGrp As String = ""
                '    Select Case IntKey
                '        Case enAllocation.JOBS
                '            StrGrp = "[ " & enAllocation.JOBS.ToString & " ]"dsalloc.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("Id") = IntKey
                '        Case enAllocation.DEPARTMENT
                '            StrGrp = "[ " & enAllocation.DEPARTMENT.ToString & " ]"
                '        Case enAllocation.CLASS_GROUP
                '            StrGrp = "[ " & enAllocation.CLASS_GROUP.ToString.Replace("_", " ") & " ]"
                '        Case enAllocation.CLASSES
                '            StrGrp = "[ " & enAllocation.CLASSES.ToString & " ]"
                '    End Select

                '    Dim dtURow As DataRow = dtUAccess.NewRow

                '    dtURow.Item("UserAccess") = dtRow.Item("GrpName") & Space(5) & StrGrp
                '    dtURow.Item("CompanyId") = dtRow.Item("CompId")
                '    dtURow.Item("YearId") = dtRow.Item("YearId")
                '    dtURow.Item("IsGrp") = dtRow.Item("IsGrp")
                '    dtURow.Item("TotAllocation") = intRowCnt
                '    dtURow.Item("AllocationRefId") = IntKey

                '    dtUAccess.Rows.Add(dtURow)

                '    For Each dtTRow As DataRow In dsTranList.Tables("TranList").Rows
                '        dtURow = dtUAccess.NewRow
                '        dtURow.Item("UserAccess") = Space(5) & dtTRow.Item("Allocation").ToString
                '        dtURow.Item("CompanyId") = dtRow.Item("CompId")
                '        dtURow.Item("YearId") = dtRow.Item("YearId")
                '        dtURow.Item("IsGrp") = dtTRow.Item("IsGrp")
                '        dtURow.Item("IsAssign") = dtTRow.Item("assign")
                '        dtURow.Item("AllocationId") = dtTRow.Item("AllocationId")
                '        dtURow.Item("AllocationLevel") = dtTRow.Item("AllocationLevel")
                '        dtURow.Item("TotAllocation") = intRowCnt
                '        dtURow.Item("AllocationRefId") = IntKey
                '        dtUAccess.Rows.Add(dtURow)
                '    Next
                'Next
                'S.SANDEEP [20-JUN-2018] -- END

            Next

            Return dtUAccess

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUserAccessList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dtUAccess.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function IsAccessExist(ByVal intUserId As Integer, _
                                  ByVal intCompId As Integer, _
                                  ByVal intYearId As Integer, _
                                  ByVal intAllocation As Integer, _
                                  ByVal intRefId As Integer, _
                                  Optional ByVal intLevel As Integer = -1) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            StrQ = "SELECT " & _
                   "    hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
                   "FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                   "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                   "WHERE userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND allocationunkid = @AllocationId AND referenceunkid = '" & intRefId & "' "

            If intLevel > 0 Then
                StrQ &= " AND allocationlevel = @ALevel "
                objDataOperation.AddParameter("@ALevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)
            End If

            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@AllocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocation)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("useraccessprivilegeunkid")
            Else
                Return 0
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsAccessExist; Module Name: " & mstrModuleName)
        Finally
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAccess(ByVal intUserId As Integer, _
                                 ByVal intCompId As Integer, _
                                 ByVal intYearId As Integer, _
                                 ByVal intAllocationId As Integer, _
                                 ByVal intAllocationLevel As Integer, _
                                 ByVal intRefId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intAccessUnkid As Integer = -1
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END
        Dim objCommonATLog As New clsCommonATLog
        Try
            StrQ = " SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE " & _
                   " userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND referenceunkid = @RefId "

            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@RefId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intAccessUnkid = dsList.Tables("List").Rows(0)(0)
            End If

            If intAccessUnkid <= 0 Then
                StrQ = "INSERT INTO hrmsConfiguration..cfuseraccess_privilege_master (userunkid,companyunkid,yearunkid,referenceunkid) " & _
                                   "VALUES (@UserId,@CompId,@YearId,@RefId); SELECT @@identity "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
                objDataOperation.AddParameter("@RefId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                intAccessUnkid = dsList.Tables("List").Rows(0)(0)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuseraccess_privilege_master", "useraccessprivilegeunkid", intAccessUnkid, "", "", -1, 1, 0, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            StrQ = "INSERT INTO hrmsConfiguration..cfuseraccess_privilege_tran (useraccessprivilegeunkid,allocationunkid,allocationlevel) " & _
                   "VALUES (" & intAccessUnkid & ",@AllocationId,@AllocationLevel); SELECT @@identity "

            'Sohail (24 Nov 2016) - [theme_id, lastview_id]


            objDataOperation.AddParameter("@AllocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationId)
            objDataOperation.AddParameter("@AllocationLevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationLevel)


            'Gajanan [23-June-2020] -- Start
            'Enhancement NMB Missing AT For User Access Changes.
            'objDataOperation.ExecNonQuery(StrQ)
            Dim dsTrans As New DataSet
            dsTrans = objDataOperation.ExecQuery(StrQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
'Gajanan [23-June-2020] -- End


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            Dim iTranId As Integer = 0
            If dsList.Tables("List") IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                iTranId = CInt(dsList.Tables("List").Rows(0)(0))
            End If
            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuseraccess_privilege_master", "useraccessprivilegeunkid", intAccessUnkid, "cfuseraccess_privilege_tran", "useraccessprivilegetranunkid", iTranId, 1, 1, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertAccess; Module Name: " & mstrModuleName)
            Return False
        Finally
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function DeleteAccess(ByVal intUserId As Integer, ByVal intRefId As Integer, ByVal intCompanyId As Integer, ByVal intYearId As Integer) As Boolean
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            StrQ = "SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = '" & intUserId & "' " & _
                   " AND referenceunkid = '" & intRefId & "' AND companyunkid = '" & intCompanyId & "' AND yearunkid = '" & intYearId & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                Dim dsChildData As New DataSet
                dsChildData = objCommonATLog.GetChildList(objDataOperation, "hrmsConfiguration..cfuseraccess_privilege_tran", "useraccessprivilegeunkid", dRow.Item("useraccessprivilegeunkid"))
                For Each row As DataRow In dsChildData.Tables(0).Rows
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfuseraccess_privilege_master", "useraccessprivilegeunkid", dRow.Item("useraccessprivilegeunkid"), "cfuseraccess_privilege_tran", "useraccessprivilegetranunkid", row("useraccessprivilegetranunkid"), 3, 3, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

                StrQ = "DELETE FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE useraccessprivilegeunkid = '" & dRow.Item("useraccessprivilegeunkid") & "'; " & _
                       " DELETE FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid = '" & dRow.Item("useraccessprivilegeunkid") & "' "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: DeleteAccess; Module Name: " & mstrModuleName)
            Return False
        Finally
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function IsValidLogin(ByVal StrLoginName As String, ByVal StrPassword As String, ByVal IntLoginModId As Integer, ByVal StrDataBase As String, ByVal mblnAllowLoginOnActualEOCReitreDate As Boolean _
                                          , Optional ByVal IsADUserAlllow As Boolean = False) As Integer

        'Pinkal (27-Nov-2020) -- Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.[ByVal mblnAllowLoginOnActualEOCReitreDate As Boolean]

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iRetValue As Integer = -1
        Dim iUserId As Integer = -1
        Dim dsList As New DataSet
        Dim StrMessage As String = String.Empty
        Dim objUAL As New clsUser_Acc_Lock
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            mstrMessage = ""
            iUserId = Return_UserId(StrLoginName, StrDataBase, IntLoginModId)
            intRetUserId = iUserId

            Select Case IntLoginModId
                Case enLoginMode.USER
                    StrQ = "SELECT " & _
                           " " & StrDataBase & "..cfuser_master.userunkid AS LId " & _
                           "FROM " & StrDataBase & "..cfuser_master " & _
                           "WHERE " & StrDataBase & "..cfuser_master.username = @LName " & _
                           "    AND " & StrDataBase & "..cfuser_master.password = @LPass " & _
                           "    AND " & StrDataBase & "..cfuser_master.isactive = 1 "


                    'Pinkal (12-Oct-2011) -- Start
                    'Enhancement : TRA Changes
                    If iUserId <> 1 Then
                        StrQ &= " AND isnull(isaduser,0) = @IsADUserAlllow"
                    End If
                    'Pinkal (12-Oct-2011) -- End


                Case enLoginMode.EMPLOYEE
                    StrQ = ""
            End Select
            objDataOperation.AddParameter("@LName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrLoginName.ToString)
            objDataOperation.AddParameter("@LPass", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeCommonLib.clsSecurity.Encrypt(StrPassword, "ezee"))
            objDataOperation.AddParameter("@IsADUserAlllow", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IsADUserAlllow.ToString())

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iUserId > 0 And iUserId <> 1 Then

                'Shani(01-MAR-2016) -- Start
                'Enhancement:PA External Approver Flow
                '_Userunkid = iUserId
                'If _EmployeeUnkid > 0 AndAlso _IsManager = False Then
                '    mstrMessage = "Sorry, you cannot login. Reason : You are not authorized to login as manager. Please contact Administrator."
                '    iRetValue = -1
                '    GoTo 10
                'End If

                'If _EmployeeUnkid > 0 AndAlso _IsManager = True Then
                '    If _Reinstatement_Date = Nothing Then
                '        'CHECKING FOR SUSPENSION DATES >> START
                '        If _Suspended_To_Date <> Nothing AndAlso _Suspended_From_Date <> Nothing Then
                '            If ConfigParameter._Object._CurrentDateAndTime.Date <= _Suspended_To_Date.Date And _
                '               ConfigParameter._Object._CurrentDateAndTime.Date >= _Suspended_From_Date.Date Then
                '                mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
                '                iRetValue = -1
                '                GoTo 10
                '            End If
                '        End If
                '        'CHECKING FOR SUSPENSION DATES >> END

                '        'CHECKING FOR END OF CONTRACT DATES >> START
                '        If _Empl_Enddate <> Nothing Then
                '            If ConfigParameter._Object._CurrentDateAndTime.Date >= _Empl_Enddate.Date Then
                '                mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
                '                iRetValue = -1
                '                GoTo 10
                '            End If
                '        End If
                '        'CHECKING FOR END OF CONTRACT DATES >> END

                '        'CHECKING FOR TERMINATION DATE >> START
                '        If _Termination_Date <> Nothing Then
                '            If ConfigParameter._Object._CurrentDateAndTime.Date >= _Termination_Date.Date Then
                '                mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
                '                iRetValue = -1
                '                GoTo 10
                '            End If
                '        End If
                '        'CHECKING FOR TERMINATION DATE >> END

                '        'CHECKING FOR RETIREMENT DATE >> START
                '        If _Retirement_Date <> Nothing Then
                '            If ConfigParameter._Object._CurrentDateAndTime.Date >= _Retirement_Date.Date Then
                '                mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
                '                iRetValue = -1
                '                GoTo 10
                '            End If
                '        End If
                '        'CHECKING FOR RETIREMENT DATE >> END
                '    Else
                '        If ConfigParameter._Object._CurrentDateAndTime.Date < _Reinstatement_Date.Date Then
                '            mstrMessage = "Sorry, you cannot login. Reason : Your Reinstatement date is less than the today date. Please contact Administrator."
                '            iRetValue = -1
                '            GoTo 10
                '        End If
                '    End If
                'End If

                _Userunkid = iUserId
                If _EmployeeUnkid > 0 AndAlso _IsManager = False Then
                    mstrMessage = "Sorry, you cannot login. Reason : You are not authorized to login as manager. Please contact Administrator."
                    iRetValue = -1
                    GoTo 10
                End If

                'Shani(01-MAR-2016) -- Start
                'Enhancement:PA External Approver Flow
                If _EmployeeUnkid > 0 AndAlso _IsManager = True Then

                    Dim strEmplDate As String = ""
                    Dim strDbName As String = ""
                    StrQ = "SELECT " & _
                           "    @EmplDate = ISNULL(cfconfiguration.key_value, + " & _
                           "                CONVERT(NVARCHAR(8),CASE WHEN CONVERT(NVARCHAR(8),cffinancial_year_tran.end_date,112) < CONVERT(NVARCHAR(8),GETDATE(),112) " & _
                           "                                    THEN cffinancial_year_tran.end_date ELSE GETDATE() END  ,112)) " & _
                           "    ,@DBName = cffinancial_year_tran.database_name " & _
                           "FROM hrmsConfiguration..cffinancial_year_tran " & _
                           "    LEFT JOIN hrmsConfiguration..cfconfiguration ON cfconfiguration.companyunkid = cffinancial_year_tran.companyunkid AND UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                           "WHERE cffinancial_year_tran.isclosed = 0 AND cffinancial_year_tran.companyunkid = '" & _EmployeeCompanyUnkid & "' "

                    objDataOperation.AddParameter("@EmplDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmplDate, ParameterDirection.InputOutput)
                    objDataOperation.AddParameter("@DBName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDbName, ParameterDirection.InputOutput)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strEmplDate = objDataOperation.GetParameterValue("@EmplDate")
                    strDbName = objDataOperation.GetParameterValue("@DBName")
                    'Shani(19-MAR-2016) -- Start


                    '   objEmp._DataOperation = objDataOperation
                    '   objEmp._Employeeunkid(eZeeDate.convertDate(strEmplDate)) = _EmployeeUnkid

                    '    If objEmp._Reinstatementdate = Nothing Then
                    '        'CHECKING FOR SUSPENSION DATES >> START
                    '        If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
                    '            If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
                    '               ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
                    '                mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
                    '                iRetValue = -1
                    '                GoTo 10
                    '            End If
                    '        End If
                    '        'CHECKING FOR SUSPENSION DATES >> END

                    '        'CHECKING FOR END OF CONTRACT DATES >> START
                    '        If objEmp._Empl_Enddate <> Nothing Then
                    '            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then
                    '                mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
                    '                iRetValue = -1
                    '                GoTo 10
                    '            End If
                    '        End If
                    '        'CHECKING FOR END OF CONTRACT DATES >> END

                    '        'CHECKING FOR TERMINATION DATE >> START
                    '        If objEmp._Termination_From_Date <> Nothing Then
                    '            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then
                    '                mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
                    '                iRetValue = -1
                    '                GoTo 10
                    '            End If
                    '        End If
                    '        'CHECKING FOR TERMINATION DATE >> END

                    '        'CHECKING FOR RETIREMENT DATE >> START
                    '        If objEmp._Termination_To_Date <> Nothing Then
                    '            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then
                    '                mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
                    '                iRetValue = -1
                    '                GoTo 10
                    '            End If
                    '        End If
                    '        'CHECKING FOR RETIREMENT DATE >> END
                    '    Else
                    '        If ConfigParameter._Object._CurrentDateAndTime.Date < objEmp._Reinstatementdate.Date Then
                    '            mstrMessage = "Sorry, you cannot login. Reason : Your Reinstatement date is less than the today date. Please contact Administrator."
                    '            iRetValue = -1
                    '            GoTo 10
                    '        End If
                    '    End If
                    strDbName = strDbName & ".."

                    'S.SANDEEP [15 NOV 2016] -- START
                    'ENHANCEMENT : ISSUE FOR LOGIN IN KBC DATE WAS IN YYYYMMDD & CONVERT DATE WAS USED
                    'StrQ = "SELECT " & _
                    '       "     employeeunkid " & _
                    '       "    ,employeecode " & _
                    '       "    ,firstname " & _
                    '       "    ,surname " & _
                    '       "    ,othername " & _
                    '       "    ,appointeddate " & _
                    '       "    ,ISNULL(ESUSP.suspended_from_date,hremployee_master.suspended_from_date) AS suspended_from_date " & _
                    '       "    ,ISNULL(ESUSP.suspended_to_date,hremployee_master.suspended_to_date) AS suspended_to_date " & _
                    '       "    ,ISNULL(EPROB.probation_from_date,hremployee_master.probation_from_date) AS probation_from_date " & _
                    '       "    ,ISNULL(EPROB.probation_to_date,hremployee_master.probation_to_date) AS probation_to_date " & _
                    '       "    ,ISNULL(ETERM.termination_from_date,hremployee_master.termination_from_date) AS termination_from_date " & _
                    '       "    ,ISNULL(ERET.termination_to_date,hremployee_master.termination_to_date) AS termination_to_date " & _
                    '       "    ,ISNULL(ECNF.confirmation_date,hremployee_master.confirmation_date) AS confirmation_date " & _
                    '       "    ,ISNULL(ETERM.empl_enddate,hremployee_master.empl_enddate) AS empl_enddate " & _
                    '       "    ,ISNULL(ERH.reinstatment_date,hremployee_master.reinstatement_date) AS reinstatement_date " & _
                    '       "FROM " & strDbName & "hremployee_master " & _
                    '       "    LEFT JOIN " & _
                    '       "        ( " & _
                    '       "            SELECT " & _
                    '       "                 SUSP.SDEmpId " & _
                    '       "                ,SUSP.suspended_from_date " & _
                    '       "                ,SUSP.suspended_to_date " & _
                    '       "            FROM " & _
                    '       "            ( " & _
                    '       "                SELECT " & _
                    '       "                     SDT.employeeunkid AS SDEmpId " & _
                    '       "                    ,SDT.date1 AS suspended_from_date " & _
                    '       "                    ,SDT.date2 AS suspended_to_date " & _
                    '       "                    ,ROW_NUMBER()OVER(PARTITION BY SDT.employeeunkid ORDER BY SDT.effectivedate DESC) AS Rno " & _
                    '       "                FROM " & strDbName & "hremployee_dates_tran AS SDT " & _
                    '       "                WHERE isvoid = 0 AND SDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),SDT.effectivedate,112) <= '" & eZeeDate.convertDate(strEmplDate) & "' " & _
                    '       "            ) AS SUSP WHERE SUSP.Rno = 1 " & _
                    '       "        ) AS ESUSP ON ESUSP.SDEmpId = hremployee_master.employeeunkid " & _
                    '       "    LEFT JOIN " & _
                    '       "        ( " & _
                    '       "            SELECT " & _
                    '       "                 PROB.PDEmpId " & _
                    '       "                ,PROB.probation_from_date " & _
                    '       "                ,PROB.probation_to_date " & _
                    '       "            FROM " & _
                    '       "            ( " & _
                    '       "                SELECT " & _
                    '       "                     PDT.employeeunkid AS PDEmpId " & _
                    '       "                    ,PDT.date1 AS probation_from_date " & _
                    '       "                    ,PDT.date2 AS probation_to_date " & _
                    '       "                    ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                    '       "                FROM " & strDbName & "hremployee_dates_tran AS PDT " & _
                    '       "                WHERE isvoid = 0 AND PDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & eZeeDate.convertDate(strEmplDate) & "' " & _
                    '       "            ) AS PROB WHERE PROB.Rno = 1 " & _
                    '       "        ) AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid " & _
                    '       "    LEFT JOIN " & _
                    '       "        ( " & _
                    '       "            SELECT " & _
                    '       "                 TERM.TEEmpId " & _
                    '       "                ,TERM.empl_enddate " & _
                    '       "                ,TERM.termination_from_date " & _
                    '       "                ,TERM.isexclude_payroll " & _
                    '       "            FROM " & _
                    '       "            ( " & _
                    '       "                SELECT " & _
                    '       "                     TRM.employeeunkid AS TEEmpId " & _
                    '       "                    ,TRM.date1 AS empl_enddate " & _
                    '       "                    ,TRM.date2 AS termination_from_date " & _
                    '       "                    ,TRM.isexclude_payroll " & _
                    '       "                    ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    '       "                FROM " & strDbName & "hremployee_dates_tran AS TRM " & _
                    '       "                WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(strEmplDate) & "' " & _
                    '       "            ) AS TERM WHERE TERM.Rno = 1 " & _
                    '       "        ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                    '       "    LEFT JOIN " & _
                    '       "        ( " & _
                    '       "            SELECT " & _
                    '       "                 RET.REmpId " & _
                    '       "                ,RET.termination_to_date " & _
                    '       "            FROM " & _
                    '       "            ( " & _
                    '       "                SELECT " & _
                    '       "                     RTD.employeeunkid AS REmpId " & _
                    '       "                    ,RTD.date1 AS termination_to_date " & _
                    '       "                    ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    '       "                FROM " & strDbName & "hremployee_dates_tran AS RTD " & _
                    '       "                WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(strEmplDate) & "' " & _
                    '       "            ) AS RET WHERE RET.Rno = 1 " & _
                    '       "        ) AS ERET ON ERET.REmpId = hremployee_master.employeeunkid " & _
                    '       "    LEFT JOIN " & _
                    '       "        ( " & _
                    '       "            SELECT " & _
                    '       "                 CNF.CEmpId " & _
                    '       "                ,CNF.confirmation_date " & _
                    '       "            FROM " & _
                    '       "            ( " & _
                    '       "                SELECT " & _
                    '       "                     CNF.employeeunkid AS CEmpId " & _
                    '       "                    ,CNF.date1 AS confirmation_date " & _
                    '       "                    ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                    '       "                FROM " & strDbName & "hremployee_dates_tran AS CNF " & _
                    '       "                WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & eZeeDate.convertDate(strEmplDate) & "' " & _
                    '       "            ) AS CNF WHERE CNF.Rno = 1 " & _
                    '       "        ) AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid " & _
                    '       "        LEFT JOIN " & _
                    '       "            ( " & _
                    '       "                SELECT " & _
                    '       "                    RH.reinstatment_date " & _
                    '       "                    ,RH.RHEmpId " & _
                    '       "                FROM " & _
                    '       "                ( " & _
                    '       "                    SELECT " & _
                    '       "                         ERT.employeeunkid AS RHEmpId " & _
                    '       "                        ,ERT.reinstatment_date AS reinstatment_date " & _
                    '       "                        ,CONVERT(CHAR(8),ERT.effectivedate,112) AS RHEfDt " & _
                    '       "                        ,ROW_NUMBER()OVER(PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " & _
                    '       "                    FROM " & strDbName & "hremployee_rehire_tran AS ERT " & _
                    '       "                    WHERE isvoid = 0 AND CONVERT(CHAR(8),ERT.effectivedate,112) <= '" & eZeeDate.convertDate(strEmplDate) & "' " & _
                    '       "                ) AS RH WHERE RH.Rno = 1 " & _
                    '       "            )AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid " & _
                    '       "WHERE employeeunkid =  " & _EmployeeUnkid

                    StrQ = "SELECT " & _
                           "     employeeunkid " & _
                           "    ,employeecode " & _
                           "    ,firstname " & _
                           "    ,surname " & _
                           "    ,othername " & _
                           "    ,appointeddate " & _
                           "    ,ISNULL(ESUSP.suspended_from_date,hremployee_master.suspended_from_date) AS suspended_from_date " & _
                           "    ,ISNULL(ESUSP.suspended_to_date,hremployee_master.suspended_to_date) AS suspended_to_date " & _
                           "    ,ISNULL(EPROB.probation_from_date,hremployee_master.probation_from_date) AS probation_from_date " & _
                           "    ,ISNULL(EPROB.probation_to_date,hremployee_master.probation_to_date) AS probation_to_date " & _
                           "    ,ISNULL(ETERM.termination_from_date,hremployee_master.termination_from_date) AS termination_from_date " & _
                           "    ,ISNULL(ERET.termination_to_date,hremployee_master.termination_to_date) AS termination_to_date " & _
                           "    ,ISNULL(ECNF.confirmation_date,hremployee_master.confirmation_date) AS confirmation_date " & _
                           "    ,ISNULL(ETERM.empl_enddate,hremployee_master.empl_enddate) AS empl_enddate " & _
                           "    ,ISNULL(ERH.reinstatment_date,hremployee_master.reinstatement_date) AS reinstatement_date " & _
                           "FROM " & strDbName & "hremployee_master " & _
                           "    LEFT JOIN " & _
                           "        ( " & _
                           "            SELECT " & _
                           "                 SUSP.SDEmpId " & _
                           "                ,SUSP.suspended_from_date " & _
                           "                ,SUSP.suspended_to_date " & _
                           "            FROM " & _
                           "            ( " & _
                           "                SELECT " & _
                           "                     SDT.employeeunkid AS SDEmpId " & _
                           "                    ,SDT.date1 AS suspended_from_date " & _
                           "                    ,SDT.date2 AS suspended_to_date " & _
                           "                    ,ROW_NUMBER()OVER(PARTITION BY SDT.employeeunkid ORDER BY SDT.effectivedate DESC) AS Rno " & _
                           "                FROM " & strDbName & "hremployee_dates_tran AS SDT " & _
                           "                WHERE isvoid = 0 AND SDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),SDT.effectivedate,112) <= '" & strEmplDate & "' " & _
                           "            ) AS SUSP WHERE SUSP.Rno = 1 " & _
                           "        ) AS ESUSP ON ESUSP.SDEmpId = hremployee_master.employeeunkid " & _
                           "    LEFT JOIN " & _
                           "        ( " & _
                           "            SELECT " & _
                           "                 PROB.PDEmpId " & _
                           "                ,PROB.probation_from_date " & _
                           "                ,PROB.probation_to_date " & _
                           "            FROM " & _
                           "            ( " & _
                           "                SELECT " & _
                           "                     PDT.employeeunkid AS PDEmpId " & _
                           "                    ,PDT.date1 AS probation_from_date " & _
                           "                    ,PDT.date2 AS probation_to_date " & _
                           "                    ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                           "                FROM " & strDbName & "hremployee_dates_tran AS PDT " & _
                           "                WHERE isvoid = 0 AND PDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & strEmplDate & "' " & _
                           "            ) AS PROB WHERE PROB.Rno = 1 " & _
                           "        ) AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid " & _
                           "    LEFT JOIN " & _
                           "        ( " & _
                           "            SELECT " & _
                           "                 TERM.TEEmpId " & _
                           "                ,TERM.empl_enddate " & _
                           "                ,TERM.termination_from_date " & _
                           "                ,TERM.isexclude_payroll " & _
                           "            FROM " & _
                           "            ( " & _
                           "                SELECT " & _
                           "                     TRM.employeeunkid AS TEEmpId " & _
                           "                    ,TRM.date1 AS empl_enddate " & _
                           "                    ,TRM.date2 AS termination_from_date " & _
                           "                    ,TRM.isexclude_payroll " & _
                           "                    ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                           "                FROM " & strDbName & "hremployee_dates_tran AS TRM " & _
                           "                WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & strEmplDate & "' " & _
                           "            ) AS TERM WHERE TERM.Rno = 1 " & _
                           "        ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                           "    LEFT JOIN " & _
                           "        ( " & _
                           "            SELECT " & _
                           "                 RET.REmpId " & _
                           "                ,RET.termination_to_date " & _
                           "            FROM " & _
                           "            ( " & _
                           "                SELECT " & _
                           "                     RTD.employeeunkid AS REmpId " & _
                           "                    ,RTD.date1 AS termination_to_date " & _
                           "                    ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                           "                FROM " & strDbName & "hremployee_dates_tran AS RTD " & _
                           "                WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & strEmplDate & "' " & _
                           "            ) AS RET WHERE RET.Rno = 1 " & _
                           "        ) AS ERET ON ERET.REmpId = hremployee_master.employeeunkid " & _
                           "    LEFT JOIN " & _
                           "        ( " & _
                           "            SELECT " & _
                           "                 CNF.CEmpId " & _
                           "                ,CNF.confirmation_date " & _
                           "            FROM " & _
                           "            ( " & _
                           "                SELECT " & _
                           "                     CNF.employeeunkid AS CEmpId " & _
                           "                    ,CNF.date1 AS confirmation_date " & _
                           "                    ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                           "                FROM " & strDbName & "hremployee_dates_tran AS CNF " & _
                           "                WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & strEmplDate & "' " & _
                           "            ) AS CNF WHERE CNF.Rno = 1 " & _
                           "        ) AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid " & _
                           "        LEFT JOIN " & _
                           "            ( " & _
                           "                SELECT " & _
                           "                    RH.reinstatment_date " & _
                           "                    ,RH.RHEmpId " & _
                           "                FROM " & _
                           "                ( " & _
                           "                    SELECT " & _
                           "                         ERT.employeeunkid AS RHEmpId " & _
                           "                        ,ERT.reinstatment_date AS reinstatment_date " & _
                           "                        ,CONVERT(CHAR(8),ERT.effectivedate,112) AS RHEfDt " & _
                           "                        ,ROW_NUMBER()OVER(PARTITION BY ERT.employeeunkid ORDER BY ERT.effectivedate DESC) AS Rno " & _
                           "                    FROM " & strDbName & "hremployee_rehire_tran AS ERT " & _
                           "                    WHERE isvoid = 0 AND CONVERT(CHAR(8),ERT.effectivedate,112) <= '" & strEmplDate & "' " & _
                           "                ) AS RH WHERE RH.Rno = 1 " & _
                           "            )AS ERH ON ERH.RHEmpId = hremployee_master.employeeunkid " & _
                           "WHERE employeeunkid =  " & _EmployeeUnkid
                    'S.SANDEEP [15 NOV 2016] -- END


                    

                    Dim dsEmp As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsEmp.Tables(0).Rows.Count > 0 Then
                        Dim dRow As DataRow = dsEmp.Tables(0).Rows(0)
                        If IsDBNull(dRow("reinstatement_date")) = True Then
                        'CHECKING FOR SUSPENSION DATES >> START
                            If IsDBNull(dRow("suspended_to_date")) <> True AndAlso IsDBNull(dRow("suspended_from_date")) <> True Then
                                If ConfigParameter._Object._CurrentDateAndTime.Date <= CDate(dRow("suspended_to_date")).Date And _
                                   ConfigParameter._Object._CurrentDateAndTime.Date >= CDate(dRow("suspended_from_date")).Date Then
                                mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
                                iRetValue = -1
                                GoTo 10
                            End If
                        End If
                        'CHECKING FOR SUSPENSION DATES >> END

                        'CHECKING FOR END OF CONTRACT DATES >> START
                            If IsDBNull(dRow("empl_enddate")) <> True Then
                                'Pinkal (27-Nov-2020) -- Start
                                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                                Dim mblnFlag As Boolean = False
                                If mblnAllowLoginOnActualEOCReitreDate Then
                                    If ConfigParameter._Object._CurrentDateAndTime.Date > CDate(dRow("empl_enddate")).Date Then mblnFlag = True
                                Else
                                    If ConfigParameter._Object._CurrentDateAndTime.Date >= CDate(dRow("empl_enddate")).Date Then mblnFlag = True
                                End If

                                If mblnFlag Then
                                mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
                                iRetValue = -1
                                GoTo 10
                            End If
                                'Pinkal (27-Nov-2020) -- End

                        End If
                        'CHECKING FOR END OF CONTRACT DATES >> END

                        'CHECKING FOR TERMINATION DATE >> START
                            If IsDBNull(dRow("termination_from_date")) <> True Then 'termination_from_date
                                'Pinkal (27-Nov-2020) -- Start
                                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                                Dim mblnFlag As Boolean = False
                                If mblnAllowLoginOnActualEOCReitreDate Then
                                    If ConfigParameter._Object._CurrentDateAndTime.Date > CDate(dRow("termination_from_date")).Date Then mblnFlag = True 'termination_from_date
                                Else
                                    If ConfigParameter._Object._CurrentDateAndTime.Date >= CDate(dRow("termination_from_date")).Date Then mblnFlag = True 'termination_from_date
                                End If
                                If mblnFlag Then
                                mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
                                iRetValue = -1
                                GoTo 10
                            End If
                                'Pinkal (27-Nov-2020) -- End
                        End If
                        'CHECKING FOR TERMINATION DATE >> END

                        'CHECKING FOR RETIREMENT DATE >> START
                            If IsDBNull(dRow("termination_to_date")) <> True Then 'termination_to_date
                                'Pinkal (27-Nov-2020) -- Start
                                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                                Dim mblnFlag As Boolean = False
                                If mblnAllowLoginOnActualEOCReitreDate Then
                                    If ConfigParameter._Object._CurrentDateAndTime.Date > CDate(dRow("termination_to_date")).Date Then mblnFlag = True 'termination_to_date
                                Else
                                    If ConfigParameter._Object._CurrentDateAndTime.Date >= CDate(dRow("termination_to_date")).Date Then mblnFlag = True 'termination_to_date
                                End If

                                If mblnFlag Then
                                mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
                                iRetValue = -1
                                GoTo 10
                            End If
                                'Pinkal (27-Nov-2020) -- End 
                        End If
                        'CHECKING FOR RETIREMENT DATE >> END
                    Else
                            If ConfigParameter._Object._CurrentDateAndTime.Date < CDate(dRow("reinstatement_date")).Date Then 'reinstatement_date
                            mstrMessage = "Sorry, you cannot login. Reason : Your Reinstatement date is less than the today date. Please contact Administrator."
                            iRetValue = -1
                            GoTo 10
                        End If
                    End If
                End If
                End If
                
                'Shani(19-MAR-2016) -- End


                'Shani(01-MAR-2016) -- End



                Dim intDaysDiff As Integer = 0
                If _Exp_Days.ToString.Length = 8 Then
                    Dim dtDate As Date = CDate(eZeeDate.convertDate(_Exp_Days.ToString).ToShortDateString)
                    intDaysDiff = CInt(DateDiff(DateInterval.Day, dtDate, ConfigParameter._Object._CurrentDateAndTime.AddDays(-1)))
                    If intDaysDiff >= 0 Then
                        mstrMessage = "Sorry, you cannot login. Reason : Your Password is expired. Please contact Administrator."
                        blnShowChangePasswdfrm = True
                        iRetValue = -1
                        GoTo 10
                    End If
                End If

                If _Relogin_Date.Date <> Nothing AndAlso _Relogin_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    mstrMessage = "Sorry, You can not relogin before " & _Relogin_Date.ToShortDateString & "."
                    iRetValue = -1
                    GoTo 10
                ElseIf _Relogin_Date.Date <> Nothing AndAlso _Relogin_Date.Date = ConfigParameter._Object._CurrentDateAndTime.Date Then
                    If _Relogin_Date.ToString("HH:mm") > ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm") Then
                        mstrMessage = "Sorry, You can relogin today after " & _Relogin_Date.ToShortTimeString & "."
                        iRetValue = -1
                        GoTo 10
                    End If
                ElseIf _Relogin_Date.Date <> Nothing Then
                    _Relogin_Date = Nothing
                    _FormName = mstrFormName
                    _LoginEmployeeunkid = mintLoginEmployeeunkid
                    _ClientIP = mstrClientIP
                    _HostName = mstrHostName
                    _FromWeb = mblnIsWeb
                    _AuditUserId = mintAuditUserId
                    _AuditDate = mdtAuditDate
                    If Update(True) = False Then
                        iRetValue = -1
                        GoTo 10
                    End If
                    _Userunkid = iUserId
                End If
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                If _IsAcc_Locked = True Then
                    Dim StrMsg As String = ""
                    'S.SANDEEP [11-AUG-2017] -- START
                    'StrMsg = Language.getMessage(mstrModuleName, 5, "You can login after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
                    'StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
                    If PWDOptions._IsUnlockedByAdmin = False Then
                    StrMsg = Language.getMessage(mstrModuleName, 5, "You can login after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
                    StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
                    Else
                        StrMsg &= Language.getMessage(mstrModuleName, 11, "Please contact Administrator.")
                    End If
                    'S.SANDEEP [11-AUG-2017] -- END
                    mstrMessage = "Sorry, You cannot login, Your account is locked." & " " & StrMsg
                    iRetValue = -1
                    GoTo 10
                End If
                iRetValue = dsList.Tables("List").Rows(0).Item("LId")
            Else
                Dim intTry As Integer = 0
                If iUserId > 0 And iUserId <> 1 And iRetValue <= 0 Then
                    IIf(_Userunkid <= 0, _Userunkid = iUserId, _Userunkid)
                    If _IsAcc_Locked = True Then
                        Dim StrMsg As String = ""
                        If PWDOptions._IsUnlockedByAdmin = True Then
                            StrMsg = Language.getMessage(mstrModuleName, 11, "Please contact Administrator.")
                        Else
                            StrMsg = Language.getMessage(mstrModuleName, 5, "You can login after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
                            StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
                        End If
                        mstrMessage = "Sorry, You cannot login, Your account is locked." & " " & StrMsg
                        GoTo 10
                    End If

                    'Pinkal (26-Apr-2017) -- Start
                    'Enhancement - Working On Active directory Changes for PACRA.
                    If IsADUserAlllow Then Return iUserId
                    'Pinkal (26-Apr-2017) -- End

                    If PWDOptions._IsAccLockSet = True Then
                        intTry = PWDOptions._LockUserAfter
                        objUAL._Attempt_Date = ConfigParameter._Object._CurrentDateAndTime
                        objUAL._Userunkid = iUserId
                        If PWDOptions._LockUserAfter = objUAL._Attempts Then
                            Dim StrMsg As String = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot login, your account is locked.")
                            'S.SANDEEP [11-AUG-2017] -- START
                            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                            'If PWDOptions._IsUnlockedByAdmin = False Then
                            '    StrMsg &= Language.getMessage(mstrModuleName, 5, "You can login after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
                            'End If
                            'StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
                            If PWDOptions._IsUnlockedByAdmin = False Then
                                StrMsg &= Language.getMessage(mstrModuleName, 5, "You can login after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
                                StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
                            Else
                                StrMsg &= Language.getMessage(mstrModuleName, 11, "Please contact Administrator.")
                            End If
                            'S.SANDEEP [11-AUG-2017] -- END
                            mstrMessage = StrMsg
                        End If
                        If objUAL._Attempts <> 0 Then
                            intTry = PWDOptions._LockUserAfter - objUAL._Attempts
                        End If
                        StrMessage = Language.getMessage(mstrModuleName, 8, " As per password policy settings your account will be locked after ") & PWDOptions._LockUserAfter & Language.getMessage(mstrModuleName, 9, " attempts. And you have ") & intTry - 1 & Language.getMessage(mstrModuleName, 10, " attempts left.")
                    End If

                    If StrMessage.Trim.Length > 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.") & " " & StrMessage
                        StrMessage = String.Empty
                        objUAL._Userunkid = iUserId
                        objUAL._Ip = getIP()
                        objUAL._Machine_Name = getHostName()
                        objUAL._Unlockuserunkid = 0
                        objUAL._Isunlocked = objUAL._Isunlocked
                        Dim dtTime As DateTime = ConfigParameter._Object._CurrentDateAndTime
                        If objUAL._Attempts <> 0 Then
                            objUAL._Attempts = objUAL._Attempts + 1
                            If objUAL._Attempts = PWDOptions._LockUserAfter Then
                                _IsAcc_Locked = True
                                _Locked_Time = ConfigParameter._Object._CurrentDateAndTime
                                If PWDOptions._IsUnlockedByAdmin = False Then
                                    _LockedDuration = dtTime.AddMinutes(PWDOptions._AlloUserRetry)
                                Else
                                    _LockedDuration = Nothing
                                End If

                                _FormName = mstrFormName
                                _LoginEmployeeunkid = mintLoginEmployeeunkid
                                _ClientIP = mstrClientIP
                                _HostName = mstrHostName
                                _FromWeb = mblnIsWeb
                                _AuditUserId = mintAuditUserId
                                _AuditDate = mdtAuditDate

                                Update(True)
                                objUAL._Lockedtime = ConfigParameter._Object._CurrentDateAndTime
                            End If
                            With objUAL
                                ._FormName = mstrFormName
                                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
                                ._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With
                            Call objUAL.Update()
                        Else
                            objUAL._Attempts = 1
                            objUAL._Lockedtime = Nothing
                            With objUAL
                                ._FormName = mstrFormName
                                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
                                ._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With
                            Call objUAL.Insert()
                        End If
                    Else
                        iRetValue = -1
                        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.")
                    End If
                Else
                    iRetValue = -1
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.")
                End If
            End If

10:         Return iRetValue

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidLogin; Module Name: " & mstrModuleName)
        Finally
            StrQ = String.Empty
            dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function Return_UserId(ByVal StrUserName As String, ByVal StrDBName As String, ByVal IntLModeId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            Select Case IntLModeId
                Case enLoginMode.USER
                    StrQ = "SELECT " & _
                           " " & StrDBName & "..cfuser_master.userunkid AS LId " & _
                           "FROM " & StrDBName & "..cfuser_master " & _
                           "WHERE " & StrDBName & "..cfuser_master.username = @UName " & _
                           "   AND " & StrDBName & "..cfuser_master.isactive = 1 "

                    Dim objPwd As New clsPassowdOptions

                    If objPwd._EnableAllUser = False Then
                        Dim drOption As DataRow() = objPwd._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

                        If drOption.Length > 0 Then

                            If objPwd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                                If objPwd._IsEmployeeAsUser = False Then
                                    StrQ &= "   AND (ISNULL(" & StrDBName & "..cfuser_master.employeeunkid,0) < = 0 OR userunkid =1)"
                                Else
                                    StrQ &= "   AND (ISNULL(" & StrDBName & "..cfuser_master.employeeunkid,0) > 0 OR userunkid =1)"
                                End If
                            ElseIf objPwd._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objPwd._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                                StrQ &= " AND (isnull(isaduser,0) = 1 OR userunkid =1) "

                            End If

                        End If
                    End If

                Case enLoginMode.EMPLOYEE

            End Select


            objDataOperation.AddParameter("@UName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrUserName.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("LId")
            Else
                Return -1
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Return_UserId; Module Name: " & mstrModuleName)
            Return -1
        Finally
            StrQ = Nothing : dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function ImportADUser(ByVal dtUser As DataTable) As Boolean
        Dim blnFlag As Boolean = False
        Try
            If dtUser.Rows.Count <= 0 Then Return True


            'Varsha (10 Nov 2017) -- Start
            'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
            Dim objMaster As New clsMasterData
            Dim intThemeId As Integer = objMaster.GetDefaultThemeId(0, Nothing)
            If intThemeId < 0 Then
                intThemeId = 1
            End If
            'Varsha (10 Nov 2017) -- End


            For i As Integer = 0 To dtUser.Rows.Count - 1

                '******************** ASSIGN VALUE TO VARIABLES --- START
                mstrUsername = "" : mstrPassword = "" : mblnIspasswordexpire = False : mintRoleunkid = 0
                mblnIsrighttoleft = False : mintExp_Days = 0 : mintLanguageunkid = 0 : mblnIsactive = True
                mdtCreationDate = Now : mblnIsLocked = False : mdtLockedDuration = Nothing : mdtLockedTime = Nothing
                mstrFirstname = "" : mstrLastname = "" : mstrAddress1 = "" : mstrAddress2 = ""
                mstrPhone = "" : mstrEmail = "" : mblnIsADuser = False : mstrUserdomain = "" : mintTheme_Id = 1 'Varsha (10 Nov 2017) -- RefNo: 55 [mintTheme_Id = 1]


                'Pinkal (16-May-2019) -- Start
                'Enhancement [0003816]- EXTERNAL USER CREATION FROM AD Ability to create an external user when Integrate users from ACTIVE DIRECTORY option is enabled. 
                mintCompanyUnkid = 0
                'Pinkal (16-May-2019) -- End



                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                'mintCreatedById = 0 
                mintUserunkid = 0
                mintUserunkid = Return_UserId(dtUser.Rows(i)("username").ToString().Trim, "hrmsConfiguration", enLoginMode.USER)
                'Pinkal (03-Apr-2017) -- End

                mstrUsername = dtUser.Rows(i)("username")
                mstrPassword = dtUser.Rows(i)("password")
                mblnIspasswordexpire = dtUser.Rows(i)("ispasswordexpire")
                mintRoleunkid = dtUser.Rows(i)("roleunkid")
                mblnIsrighttoleft = dtUser.Rows(i)("isrighttoleft")
                mintExp_Days = dtUser.Rows(i)("exp_days")
                mintLanguageunkid = dtUser.Rows(i)("languageunkid")
                mblnIsactive = dtUser.Rows(i)("isactive")
                mdtCreationDate = Now
                mblnIsLocked = False
                mdtLockedDuration = Nothing
                mdtLockedTime = Nothing
                mintCreatedById = mintCreatedById
                mstrFirstname = dtUser.Rows(i)("firstname")
                mstrLastname = dtUser.Rows(i)("lastname")
                mstrAddress1 = dtUser.Rows(i)("address1")
                mstrAddress2 = dtUser.Rows(i)("address2")
                mstrPhone = dtUser.Rows(i)("phone")
                mstrEmail = dtUser.Rows(i)("email")
                mblnIsADuser = dtUser.Rows(i)("isaduser")
                mblnIsManager = mblnIsManager
                mstrUserdomain = dtUser.Rows(i)("userdomain")
                
                mintEmployeeUnkid = dtUser.Rows(i)("employeeunkid")

                'Pinkal (16-May-2019) -- Start
                'Enhancement [0003816]- EXTERNAL USER CREATION FROM AD Ability to create an external user when Integrate users from ACTIVE DIRECTORY option is enabled. 
                mintEmployeeCompanyUnkid = CInt(dtUser.Rows(i)("companyunkid"))
                mintTheme_Id = intThemeId
                'Pinkal (16-May-2019) -- End

                If dtUser.Rows(i)("password").ToString().Length > 0 Then
                    mstrPassword = dtUser.Rows(i)("password").ToString()
                End If

                '******************** ASSIGN VALUE TO VARIABLES --- END


                _FormName = mstrFormName
                _LoginEmployeeunkid = mintLoginEmployeeunkid
                _ClientIP = mstrClientIP
                _HostName = mstrHostName
                _FromWeb = mblnIsWeb
                _AuditUserId = mintAuditUserId
                _AuditDate = mdtAuditDate

                If mintUserunkid <= 0 Then
                blnFlag = Insert()
                Else
                    blnFlag = Update(True)
                End If

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ImportADUser; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return blnFlag
    End Function

    Public Function Get_UserBy_PrivilegeId(ByVal intPrivilegeUnkid As Integer, Optional ByVal xYearId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            If xYearId <= 0 Then xYearId = FinancialYear._Object._YearUnkid

            'S.SANDEEP [24 MAY 2016] -- Start
            'Email Notification
            'StrQ = "SELECT " & _
            '       "     ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS UName " & _
            '       "    ,ISNULL(hrmsConfiguration..cfuser_master.email,'') AS UEmail " & _
            '       ", ISNULL(hrmsConfiguration..cfuser_privilege.userunkid, 0) AS UId " & _
            '       "FROM hrmsConfiguration..cfuser_privilege " & _
            '       " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfuser_privilege.userunkid = cfcompanyaccess_privilege.userunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '       "WHERE privilegeunkid = @PvgId AND yearunkid = '" & xYearId & "' " & _
            '       "    AND ISNULL(hrmsConfiguration..cfuser_master.email,'') <> '' "

            'S.SANDEEP [22-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002123}
            'StrQ = "SELECT " & _
            '       "     CASE WHEN ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') = '' THEN hrmsConfiguration..cfuser_master.username " & _
            '       "          ELSE ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') " & _
            '       "     END AS UName " & _
            '       "    ,ISNULL(hrmsConfiguration..cfuser_master.email,'') AS UEmail " & _
            '       ", ISNULL(hrmsConfiguration..cfuser_privilege.userunkid, 0) AS UId " & _
            '       "FROM hrmsConfiguration..cfuser_privilege " & _
            '       " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfuser_privilege.userunkid = cfcompanyaccess_privilege.userunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '       "WHERE privilegeunkid = @PvgId AND yearunkid = '" & xYearId & "' " & _

            ''S.SANDEEP [24 MAY 2016] -- End

            'objDataOperation.AddParameter("@PvgId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeUnkid.ToString)

            'dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            '       "    AND ISNULL(hrmsConfiguration..cfuser_master.email,'') <> '' "

            Dim StrQFQry As String = "" : Dim StrQCond As String = ""
            StrQ = "SELECT " & _
                   "     CASE WHEN ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') = '' THEN hrmsConfiguration..cfuser_master.username " & _
                   "          ELSE ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') " & _
                   "     END AS UName " & _
                   "    ,ISNULL(hrmsConfiguration..cfuser_master.email,'') AS UEmail " & _
                   "    ,ISNULL(hrmsConfiguration..cfuser_privilege.userunkid, 0) AS UId " & _
                   "    ,#ACT# " & _
                   "FROM hrmsConfiguration..cfuser_privilege " & _
                   " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfuser_privilege.userunkid = cfcompanyaccess_privilege.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid "

            StrQCond = "WHERE privilegeunkid = @PvgId AND yearunkid = '" & xYearId & "' " & _
                       " AND ISNULL(hrmsConfiguration..cfuser_master.email,'') <> '' AND hrmsConfiguration..cfuser_master.isactive = 1 "

            objDataOperation.AddParameter("@PvgId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeUnkid.ToString)

            StrQFQry = StrQ
            StrQ = StrQ.Replace("#ACT#", "1 AS Id ")
            StrQ &= StrQCond & " AND companyunkid <= 0  "
            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim iCompanyId As Integer = 0

            StrQ = "SELECT @companyunkid = companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = '" & xYearId & "' "

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompanyId, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            iCompanyId = objDataOperation.GetParameterValue("@companyunkid")

            If iCompanyId > 0 Then
                Dim strJoin, strFilter, strActiveStatus As String
                strJoin = "" : strFilter = "" : strActiveStatus = " CAST(1 AS BIT) AS activestatus "
                GetActiveFilterCondition(iCompanyId, objDataOperation, strJoin, strFilter)
                StrQFQry &= strJoin
                StrQFQry = StrQFQry.Replace("#ACT#", strActiveStatus)
                StrQFQry &= StrQCond
                Dim ds As New DataSet
                ds = objDataOperation.ExecQuery(StrQFQry, "Lst")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dt() As DataRow = ds.Tables(0).Select("activestatus = 1")
                    If dt.Length > 0 Then
                        For Each drow In dt
                            dsList.Tables(0).ImportRow(drow)
                        Next
                    End If
                End If
            End If
            'S.SANDEEP [22-Mar-2018] -- END

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_UserBy_PrivilegeId; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            StrQ = Nothing : dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function Get_UserBy_EmployeeAndPrivilegeId(ByVal intPrivilegeUnkid As Integer, _
                                                      ByVal intEmployeeunkid As Integer, _
                                                      ByVal intCompanyId As Integer, _
                                                      ByVal xEmployeeAsOnDate As String, _
                                                      ByVal strUserAccessMode As String, _
                                                      Optional ByVal xYearId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()

        Try
            If xYearId <= 0 Then xYearId = FinancialYear._Object._YearUnkid


            Dim StrQFQry As String = "" : Dim StrQCond As String = ""

            StrQFQry = "DECLARE @employeeunkid AS INT; " & _
                   "SET @employeeunkid = @empId "

            StrQFQry &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                  "DROP TABLE #USR "
            StrQFQry &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQFQry &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT DISTINCT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM hremployee_master AS AEM " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        "   WHERE AEM.employeeunkid = @employeeunkid " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & intCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    StrQFQry &= " INTERSECT "
                End If
            Next

            StrQFQry &= ") AS Fl "




            StrQ = "SELECT " & _
                   "     CASE WHEN ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') = '' THEN hrmsConfiguration..cfuser_master.username " & _
                   "          ELSE ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') " & _
                   "     END AS UName " & _
                   "    ,ISNULL(hrmsConfiguration..cfuser_master.email,'') AS UEmail " & _
                   "    ,ISNULL(hrmsConfiguration..cfuser_privilege.userunkid, 0) AS UId " & _
                   "    ,#ACT# " & _
                   "FROM hrmsConfiguration..cfuser_privilege " & _
                   " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfuser_privilege.userunkid = cfcompanyaccess_privilege.userunkid " & _
                   " JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid "


            StrQCond = "WHERE privilegeunkid = @PvgId AND yearunkid = '" & xYearId & "' " & _
                       " AND ISNULL(hrmsConfiguration..cfuser_master.email,'') <> '' AND hrmsConfiguration..cfuser_master.isactive = 1 "

            objDataOperation.AddParameter("@PvgId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeUnkid.ToString)
        

            StrQFQry &= StrQ
            StrQFQry &= " JOIN #USR on hrmsConfiguration..cfuser_master.userunkid = #USR.userunkid "


            StrQ = StrQ.Replace("#ACT#", "1 AS Id ")
            StrQ &= StrQCond & " AND companyunkid <= 0  "
            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim iCompanyId As Integer = 0

            StrQ = "SELECT @companyunkid = companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = '" & xYearId & "' "

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompanyId, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            iCompanyId = objDataOperation.GetParameterValue("@companyunkid")

            If iCompanyId > 0 Then
                Dim strJoin, strFilter, strActiveStatus As String
                strJoin = "" : strFilter = "" : strActiveStatus = " CAST(1 AS BIT) AS activestatus "
                GetActiveFilterCondition(iCompanyId, objDataOperation, strJoin, strFilter)
                StrQFQry &= strJoin
                StrQFQry = StrQFQry.Replace("#ACT#", strActiveStatus)
                StrQFQry &= StrQCond
                Dim ds As New DataSet

                objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)

                ds = objDataOperation.ExecQuery(StrQFQry, "Lst")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dt() As DataRow = ds.Tables(0).Select("activestatus = 1")
                    If dt.Length > 0 Then
                        For Each drow In dt
                            dsList.Tables(0).ImportRow(drow)
                        Next
                    End If
                End If
            End If
            'S.SANDEEP [22-Mar-2018] -- END

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_UserBy_PrivilegeId; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            StrQ = Nothing : dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function


    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    'Public Function GetPrivilegeAbilityList() As DataSet
    '    Dim dsList As New DataSet
    '    Dim i As Integer
    '    Dim j As Integer
    '    Dim exForce As Exception
    '    Dim intGroupId As Integer = 0
    '    Dim objDataOperation As clsDataOperation

    '    If iObjDataOpr IsNot Nothing Then
    '        objDataOperation = iObjDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If

    '    Try
    '        Dim dtTemp As New DataTable
    '        Dim dtcol As DataColumn

    '        dtcol = New DataColumn("Privilege")
    '        dtcol.DataType = System.Type.GetType("System.String")
    '        dtTemp.Columns.Add(dtcol)

    '        Dim dsAbilityList As New DataSet
    '        dsAbilityList = GetAbilityGroupList()
    '        For i = 0 To dsAbilityList.Tables(0).Rows.Count - 1
    '            dtcol = New DataColumn()
    '            dtcol.ColumnName = dsAbilityList.Tables(0).Rows(i).Item("roleunkid").ToString
    '            dtcol.Caption = dsAbilityList.Tables(0).Rows(i).Item("name")
    '            dtcol.DataType = System.Type.GetType("System.Boolean")
    '            dtTemp.Columns.Add(dtcol)
    '        Next

    '        dtcol = New DataColumn("isprivilegegroup")
    '        dtcol.DataType = System.Type.GetType("System.Boolean")
    '        dtTemp.Columns.Add(dtcol)

    '        dtcol = New DataColumn("privilegeunkid")
    '        dtcol.DataType = System.Type.GetType("System.Int32")
    '        dtTemp.Columns.Add(dtcol)

    '        dtcol = New DataColumn("privilegeGroupunkidunkid")
    '        dtcol.DataType = System.Type.GetType("System.Int32")
    '        dtTemp.Columns.Add(dtcol)

    '        Dim drTemp As DataRow
    '        Dim dsPrivilege As New DataSet
    '        dsPrivilege = GetPrivilegeGroupList()
    '        Dim intParentRowIndex As Integer = 0
    '        Dim intTemp As Integer = 0
    '        For i = 0 To dsPrivilege.Tables(0).Rows.Count - 1
    '            If intGroupId <> dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid") Then
    '                drTemp = dtTemp.NewRow()
    '                drTemp.Item("privilege") = dsPrivilege.Tables(0).Rows(i).Item("name")
    '                For j = 0 To dsAbilityList.Tables(0).Rows.Count - 1
    '                    drTemp.Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = False
    '                Next
    '                drTemp.Item("isprivilegegroup") = True
    '                drTemp.Item("privilegeunkid") = -1
    '                drTemp.Item("privilegeGroupunkidunkid") = dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid")
    '                dtTemp.Rows.Add(drTemp)
    '                intGroupId = dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid")
    '                intParentRowIndex += 1
    '                intTemp = 0
    '            End If
    '            drTemp = dtTemp.NewRow()
    '            drTemp.Item("isprivilegegroup") = False
    '            For j = 0 To dsAbilityList.Tables(0).Rows.Count - 1
    '                Dim strPrivilegeUnkid As String() = Split(Replace(dsAbilityList.Tables(0).Rows(j)("privilegeunkid").ToString, " ", ""), ",")
    '                Dim index As Integer = Array.IndexOf(strPrivilegeUnkid, dsPrivilege.Tables(0).Rows(i).Item("privilegeunkid").ToString)
    '                If index >= 0 Then
    '                    drTemp.Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = True
    '                    dtTemp.Rows(intParentRowIndex - intTemp - 1).Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = True
    '                Else
    '                    drTemp.Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = False
    '                End If
    '            Next
    '            drTemp.Item("privilegeunkid") = dsPrivilege.Tables(0).Rows(i).Item("privilegeunkid")
    '            drTemp.Item("privilegeGroupunkidunkid") = dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid")
    '            drTemp.Item("privilege") = Space(5) & dsPrivilege.Tables(0).Rows(i).Item("privilege_name")
    '            dtTemp.Rows.Add(drTemp)
    '            intParentRowIndex += 1
    '            intTemp += 1
    '        Next

    '        dsList.Tables.Clear()

    '        dsList.Tables.Add(dtTemp)

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetPrivilegeAbilityList; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '        If iObjDataOpr Is Nothing Then
    '            objDataOperation = Nothing
    '        End If
    '    End Try
    'End Function
    'Public Function SaveAbilityLevelPrivilege(ByVal intAbilityLevelUnkid As Integer, ByVal strPrivilegeUnkid As String) As Boolean
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim StrQ As String = ""
    '    Dim objDataOperation As clsDataOperation

    '    If iObjDataOpr IsNot Nothing Then
    '        objDataOperation = iObjDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()
    '    End If

    '    Try
    '        If mDicRolePrivilege.Keys.Count <= 0 Then
    '            Call GetRole_Privilege()
    '        End If

    '        If mDicRolePrivilege.Keys.Count > 0 Then
    '            Dim dsUsr As New DataSet
    '            StrQ = "SELECT userunkid,roleunkid FROM hrmsConfiguration..cfuser_master WHERE isactive = 1 AND roleunkid = '" & intAbilityLevelUnkid & "' AND userunkid <> 1"
    '            dsUsr = objDataOperation.ExecQuery(StrQ, "List")
    '            Dim query = From usr In dsUsr.Tables(0).AsEnumerable() Select usr

    '            If mDicRolePrivilege.ContainsKey(intAbilityLevelUnkid) = True Then
    '                'S.SANDEEP [10 AUG 2015] -- START
    '                'ENHANCEMENT : Aruti SaaS Changes
    '                Dim CurrPrivileges As List(Of Integer) = strPrivilegeUnkid.Split(","c).[Select](Function(t) Integer.Parse(t)).ToList
    '                Dim OldPrivileges As List(Of Integer) = Nothing
    '                Dim xFinalList As List(Of Integer) = Nothing
    '                If mDicRolePrivilege(intAbilityLevelUnkid).Rows(0).Item("word") <> "" Then
    '                    OldPrivileges = mDicRolePrivilege(intAbilityLevelUnkid).AsEnumerable().[Select](Function(row) Convert.ToInt32(row.Field(Of String)("word"))).ToList()
    '                    xFinalList = OldPrivileges.Except(CurrPrivileges).ToList()
    '                End If


    '                If xFinalList IsNot Nothing AndAlso xFinalList.Count > 0 Then
    '                    For Each p In query
    '                        'THIS IS FOR REMOVING THE PRIVILEGE FROM " CFUSER_PRIVILEGE " WHEN UNASSIGNED FROM ABILITY LEVEL -- START
    '                        For Each num As Integer In xFinalList
    '                            StrQ = "SELECT userprivilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & p.Item("userunkid") & "' AND privilegeunkid = '" & num & "'"
    '                            dsList = objDataOperation.ExecQuery(StrQ, "List")
    '                            If dsList.Tables(0).Rows.Count > 0 Then
    '                                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cfuser_privilege", "userprivilegeunkid", dsList.Tables(0).Rows(0)("userprivilegeunkid"), True) = False Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                                StrQ = "DELETE FROM hrmsConfiguration..cfuser_privilege WHERE userprivilegeunkid = '" & dsList.Tables(0).Rows(0)("userprivilegeunkid") & "' "
    '                                objDataOperation.ExecNonQuery(StrQ)
    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                            End If
    '                        Next
    '                        'THIS IS FOR REMOVING THE PRIVILEGE FROM " CFUSER_PRIVILEGE " WHEN UNASSIGNED FROM ABILITY LEVEL -- END 
    '                    Next
    '                End If

    '                For Each p In query
    '                    If strPrivilegeUnkid.Trim.Length > 0 Then
    '                        'THIS IS FOR INSERT THE PRIVILEGE TO " CFUSER_PRIVILEGE " WHEN ASSIGNED FROM ABILITY LEVEL -- START
    '                        CurrPrivileges = strPrivilegeUnkid.Split(","c).[Select](Function(t) Integer.Parse(t)).ToList()
    '                        For Each num As Integer In CurrPrivileges
    '                            If objDataOperation.RecordCount("SELECT userunkid,privilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & p.Item("userunkid") & "' AND privilegeunkid = '" & num & "'") <= 0 Then
    '                                StrQ = "INSERT INTO hrmsConfiguration..cfuser_privilege (userunkid,privilegeunkid) " & _
    '                                       "SELECT TOP 1 '" & p.Item("userunkid") & "','" & num & "'; SELECT @@identity "

    '                                dsList = objDataOperation.ExecQuery(StrQ, "List")

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If

    '                                If dsList.Tables("List").Rows(0)(0).ToString.Trim.Length > 0 Then
    '                                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "cfuser_privilege", "userprivilegeunkid", dsList.Tables("List").Rows(0)(0), True) = False Then
    '                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                        Throw exForce
    '                                    End If
    '                                End If

    '                            End If
    '                        Next
    '                    End If
    '                    'THIS IS FOR INSERT THE PRIVILEGE TO " CFUSER_PRIVILEGE " WHEN ASSIGNED FROM ABILITY LEVEL -- END
    '                Next

    '                'Dim dRemPvg() As DataRow = mDicRolePrivilege(intAbilityLevelUnkid).Select("word NOT IN(" & IIf(strPrivilegeUnkid = "", "0", strPrivilegeUnkid) & ")")
    '                'If dRemPvg.Length > 0 Then
    '                '    If dsUsr.Tables(0).Rows.Count > 0 Then
    '                '        For Each dURow As DataRow In dsUsr.Tables(0).Rows
    '                '            'THIS IS FOR REMOVING THE PRIVILEGE FROM " CFUSER_PRIVILEGE " WHEN UNASSIGNED FROM ABILITY LEVEL -- START
    '                '            For i As Integer = 0 To dRemPvg.Length - 1
    '                '                If dRemPvg(i)("word").ToString.Trim.Length > 0 Then
    '                '                    StrQ = "SELECT userprivilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & dURow.Item("userunkid") & "' AND privilegeunkid = '" & dRemPvg(i)("word") & "'"
    '                '                    dsList = objDataOperation.ExecQuery(StrQ, "List")
    '                '                    If dsList.Tables(0).Rows.Count > 0 Then
    '                '                        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cfuser_privilege", "userprivilegeunkid", dsList.Tables(0).Rows(0)("userprivilegeunkid"), True) = False Then
    '                '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                '                            Throw exForce
    '                '                        End If
    '                '                        StrQ = "DELETE FROM hrmsConfiguration..cfuser_privilege WHERE userprivilegeunkid = '" & dsList.Tables(0).Rows(0)("userprivilegeunkid") & "' "
    '                '                        objDataOperation.ExecNonQuery(StrQ)
    '                '                        If objDataOperation.ErrorMessage <> "" Then
    '                '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                '                            Throw exForce
    '                '                        End If
    '                '                    End If
    '                '                End If
    '                '            Next
    '                '            'THIS IS FOR REMOVING THE PRIVILEGE FROM " CFUSER_PRIVILEGE " WHEN UNASSIGNED FROM ABILITY LEVEL -- END 
    '                '        Next
    '                '    End If
    '                'End If

    '                'For Each dURow As DataRow In dsUsr.Tables(0).Rows
    '                '    'THIS IS FOR INSERT THE PRIVILEGE TO " CFUSER_PRIVILEGE " WHEN ASSIGNED FROM ABILITY LEVEL -- START
    '                '    If strPrivilegeUnkid.Trim.Length > 0 Then
    '                '        For Each StrPrvgId As String In strPrivilegeUnkid.Trim.Split(",")
    '                '            If objDataOperation.RecordCount("SELECT userunkid,privilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & dURow.Item("userunkid") & "' AND privilegeunkid = '" & StrPrvgId & "'") <= 0 Then
    '                '                StrQ = "INSERT INTO hrmsConfiguration..cfuser_privilege (userunkid,privilegeunkid) " & _
    '                '                       "SELECT TOP 1 '" & dURow.Item("userunkid") & "','" & StrPrvgId & "'; SELECT @@identity "

    '                '                dsList = objDataOperation.ExecQuery(StrQ, "List")

    '                '                If objDataOperation.ErrorMessage <> "" Then
    '                '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                '                    Throw exForce
    '                '                End If
    '                '                If dsList.Tables("List").Rows(0)(0).ToString.Trim.Length > 0 Then
    '                '                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "cfuser_privilege", "userprivilegeunkid", dsList.Tables("List").Rows(0)(0), True) = False Then
    '                '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                '                        Throw exForce
    '                '                    End If
    '                '                End If
    '                '            End If
    '                '        Next
    '                '    End If
    '                '    'THIS IS FOR INSERT THE PRIVILEGE TO " CFUSER_PRIVILEGE " WHEN ASSIGNED FROM ABILITY LEVEL -- START
    '                'Next
    '                'S.SANDEEP [10 AUG 2015] -- END
    '            End If
    '        End If

    '        StrQ = "UPDATE hrmsConfiguration..cfrole_master " & _
    '               "SET hrmsConfiguration..cfrole_master.privilegeunkid = @privilegeunkid " & _
    '               "WHERE hrmsConfiguration..cfrole_master.roleunkid = @RoleId "

    '        objDataOperation.ClearParameters()

    '        objDataOperation.AddParameter("@privilegeunkid", SqlDbType.VarChar, 8000, strPrivilegeUnkid)
    '        objDataOperation.AddParameter("@RoleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAbilityLevelUnkid.ToString)

    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If iObjDataOpr Is Nothing Then
    '            objDataOperation.ReleaseTransaction(True)
    '        End If


    '        Return True

    '    Catch ex As Exception
    '        If iObjDataOpr Is Nothing Then
    '            objDataOperation.ReleaseTransaction(False)
    '        End If
    '        Throw New Exception(ex.Message & "; Procedure Name: SaveAbilityLevelPrivilege; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '        If iObjDataOpr Is Nothing Then
    '            objDataOperation = Nothing
    '        End If
    '    End Try
    'End Function
    'Private Sub GetRole_Privilege()
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim dsTran As New DataSet
    '    Dim exForce As Exception
    '    Dim objDataOperation As clsDataOperation

    '    If iObjDataOpr IsNot Nothing Then
    '        objDataOperation = iObjDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If

    '    Try
    '        dsTran = GetAbilityGroupList()

    '        If dsTran.Tables(0).Rows.Count > 0 Then
    '            For Each dRow As DataRow In dsTran.Tables(0).Rows

    '                StrQ = "DECLARE @words VARCHAR (MAX)  " & _
    '                       " SET @words = '" & dRow.Item("privilegeunkid").ToString & "' " & _
    '                       "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
    '                       "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
    '                       "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
    '                       "WHILE @start < @stop begin " & _
    '                       "    SELECT " & _
    '                       "        @end   = CHARINDEX(',',@words,@start) " & _
    '                       "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
    '                       "      , @start = @end+1 " & _
    '                       "    INSERT @split VALUES (@word) " & _
    '                       "END " & _
    '                       "SELECT * FROM @split ORDER BY word"

    '                dsList = objDataOperation.ExecQuery(StrQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                mDicRolePrivilege.Add(dRow.Item("roleunkid"), dsList.Tables(0))
    '            Next
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetRole_Privilege; Module Name: " & mstrModuleName)
    '    Finally
    '        If iObjDataOpr Is Nothing Then
    '            objDataOperation = Nothing
    '        End If
    '    End Try
    'End Sub
    'S.SANDEEP [10 AUG 2015] -- END
    
    Public Function Return_UserId(ByVal intEmpUnkid As Integer, _
                                  ByVal intCompanyUnkid As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try


            StrQ = "SELECT " & _
                   "  userunkid AS UsrId " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "WHERE employeeunkid = @EmpId " & _
                   "  AND companyunkid = @CompnayId AND isactive = 1 "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
            objDataOperation.AddParameter("@CompnayId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)

            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("UsrId")
            Else
                Return -1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Return_UserId; Module Name: " & mstrModuleName)
            Return -1
        Finally
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function isUExist(ByVal iEmpId As Integer, ByVal iCmpId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iUsrid As Integer = 0
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try

            strQ = "SELECT " & _
                   "  userunkid " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "WHERE employeeunkid = '" & iEmpId & "' " & _
                   " AND companyunkid = '" & iCmpId & "' " & _
                   " AND isactive = 1 "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iUsrid = dsList.Tables(0).Rows(0).Item("userunkid")
            End If

            Return iUsrid

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function GetUserAccessFromUser(ByVal iUserId As Integer, ByVal iCompanyId As Integer, ByVal iYearId As Integer, ByVal StrAllocation As String) As DataTable
        Dim dtUserAccess As DataTable = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        'S.SANDEEP [05 DEC 2015] -- START
        objDataOperation.ClearParameters()
        'S.SANDEEP [05 DEC 2015] -- END

        Try
            strQ = " SELECT userunkid,companyunkid,yearunkid,referenceunkid,allocationunkid,allocationlevel " & _
                      " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
                      " JOIN hrmsConfiguration..cfuseraccess_privilege_tran on cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
                      " WHERE userunkid = @userId AND companyunkid =@companyId AND yearunkid = @yearId AND referenceunkid in (" & StrAllocation & ")"

            objDataOperation.AddParameter("@userId", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
            objDataOperation.AddParameter("@companyId", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompanyId)
            objDataOperation.AddParameter("@yearId", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dtUserAccess = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUserAccessFromUser; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dtUserAccess
    End Function

    'S.SANDEEP [16 MAR 2016] -- START
    'Public Function GetGroupedUserList(ByVal strListName As String, _
    '                                   ByVal eUsrViewType As enViewUserType, _
    '                                   ByVal dtStDate As Date, _
    '                                   ByVal dtEdDate As Date, _
    '                                   Optional ByVal strFilterString As String = "", _
    '                                   Optional ByVal intCompanyUnkid As Integer = -1, _
    '                                   Optional ByVal IncludeManuallyAddedUser As Boolean = True, _
    '                                   Optional ByVal blnShowCount As Boolean = False) As DataTable

    '    Dim StrQ As String = String.Empty
    '    Dim objDataOperation As clsDataOperation
    '    Dim dtTable As DataTable = Nothing
    '    Dim exForce As Exception

    '    If iObjDataOpr IsNot Nothing Then
    '        objDataOperation = iObjDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If

    '    'S.SANDEEP [05 DEC 2015] -- START
    '    objDataOperation.ClearParameters()
    '    'S.SANDEEP [05 DEC 2015] -- END

    '    Try
    '        Dim StrActiveUserFilterCondition As String = ""

    '        StrActiveUserFilterCondition = " ,CASE WHEN (CASE WHEN CONVERT(CHAR(8),cfuser_master.appointeddate,112) IS NULL THEN '" & eZeeDate.convertDate(dtEdDate).ToString & "' ELSE CONVERT(CHAR(8),cfuser_master.appointeddate,112) END <= '" & eZeeDate.convertDate(dtEdDate).ToString & "') " & _
    '                                       "        AND (CASE WHEN CONVERT(CHAR(8),cfuser_master.termination_from_date,112) IS NULL THEN '" & eZeeDate.convertDate(dtEdDate).ToString & "' ELSE CONVERT(CHAR(8),cfuser_master.termination_from_date,112) END >= '" & eZeeDate.convertDate(dtEdDate).ToString & "') " & _
    '                                       "        AND (CASE WHEN CONVERT(CHAR(8),cfuser_master.empl_enddate,112) IS NULL THEN '" & eZeeDate.convertDate(dtEdDate).ToString & "' ELSE CONVERT(CHAR(8),cfuser_master.empl_enddate,112) END >= '" & eZeeDate.convertDate(dtEdDate).ToString & "') " & _
    '                                       "        AND (CASE WHEN CONVERT(CHAR(8),cfuser_master.termination_to_date,112) IS NULL THEN '" & eZeeDate.convertDate(dtEdDate).ToString & "' ELSE CONVERT(CHAR(8),cfuser_master.termination_to_date,112) END >= '" & eZeeDate.convertDate(dtEdDate).ToString & "') " & _
    '                                       "        AND (CASE WHEN CONVERT(CHAR(8),cfuser_master.reinstatement_date,112) IS NULL THEN '" & eZeeDate.convertDate(dtEdDate).ToString & "' ELSE CONVERT(CHAR(8),cfuser_master.reinstatement_date,112) END <= '" & eZeeDate.convertDate(dtEdDate).ToString & "') THEN 1 ELSE 0 END AS activestatus "

    '        StrQ = "SELECT "
    '        If blnShowCount Then
    '            StrQ &= "	 CASE WHEN A.IsGrp = 1 THEN Username + ' [' + CAST(SUM(1)OVER(PARTITION BY A.GrpId)-1 AS NVARCHAR(MAX)) +']' ELSE A.Username END AS Username "
    '        Else
    '            StrQ &= "	 CASE WHEN companyunkid <= 0 THEN REPLACE(Username,':','') ELSE Username END AS Username "
    '        End If
    '        StrQ &= "	,Firstname " & _
    '                "	,Lastname " & _
    '                "	,Email " & _
    '                " 	,URole " & _
    '                "	,UserUnkid " & _
    '                "	,RoleUnkid " & _
    '                "	,EmployeeUnkid " & _
    '                "	,companyunkid " & _
    '                "	,IsGrp " & _
    '                "	,GrpId " & _
    '                "   ,activestatus " & _
    '                "   ,isactive " & _
    '                "   ,Manager " & _
    '                "FROM " & _
    '                "( " & _
    '                "	SELECT DISTINCT " & _
    '                "		 CASE WHEN cfuser_master.companyunkid <= 0 THEN @Caption1 ELSE @Caption2 END + ' : ' + ISNULL(name,'') AS Username " & _
    '                "		,'' AS Firstname " & _
    '                "		,'' AS Lastname " & _
    '                "		,'' AS Email " & _
    '                "		,'' AS URole " & _
    '                "		,0 AS UserUnkid " & _
    '                "		,0 AS RoleUnkid " & _
    '                "		,0 AS EmployeeUnkid " & _
    '                "		,cfuser_master.companyunkid " & _
    '                "		,1 AS IsGrp " & _
    '                "        ,'' AS Manager " & _
    '                "		,cfuser_master.companyunkid AS GrpId " & _
    '                "        ,1 AS isactive "
    '        Select Case eUsrViewType
    '            Case enViewUserType.SHOW_ACTIVE_USER, enViewUserType.SHOW_ALL_USER
    '                StrQ &= " ,1 AS activestatus "
    '            Case enViewUserType.SHOW_INACTIVE_USER
    '                StrQ &= StrActiveUserFilterCondition
    '        End Select
    '        StrQ &= "	FROM cfuser_master " & _
    '                "		LEFT JOIN cfcompany_master ON cfcompany_master.companyunkid = cfuser_master.companyunkid " & _
    '                "	WHERE 1 = 1 "

    '        If strFilterString.Trim.Length > 0 Then
    '            StrQ &= " AND " & strFilterString
    '        End If

    '        If intCompanyUnkid <> -1 Then
    '            StrQ &= " AND cfuser_master.companyunkid = '" & intCompanyUnkid & "' "
    '        End If

    '        If IncludeManuallyAddedUser = False Then
    '            StrQ &= " AND cfuser_master.companyunkid > 0 "
    '        End If

    '        StrQ &= "UNION " & _
    '                "	SELECT " & _
    '                "		 SPACE(5)+' '+ username " & _
    '                "		,firstname " & _
    '                "		,lastname " & _
    '                "		,email " & _
    '                "		,ISNULL(hrmsConfiguration..cfrole_master.name,'') " & _
    '                "		,userunkid " & _
    '                "		,cfrole_master.roleunkid " & _
    '                "		,employeeunkid " & _
    '                "		,companyunkid " & _
    '                " 		,0 AS IsGrp " & _
    '                "       ,CASE WHEN ismanager = 0 THEN @No ELSE @Yes END AS Manager " & _
    '                "		,companyunkid AS GrpId " & _
    '                "       ,cfuser_master.isactive "
    '        StrQ &= StrActiveUserFilterCondition
    '        StrQ &= "	FROM cfuser_master " & _
    '                "		LEFT JOIN hrmsConfiguration..cfrole_master on hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid " & _
    '                "	WHERE 1 = 1 "

    '        Dim objOption As New clsPassowdOptions
    '        If objOption._EnableAllUser = False Then
    '            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
    '            If drOption.Length > 0 Then
    '                If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
    '                    If objOption._IsEmployeeAsUser Then
    '                        StrQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR userunkid = 1) "
    '                    Else
    '                        StrQ &= " AND ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0 "
    '                    End If
    '                    StrQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or userunkid = 1) "
    '                ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
    '                    StrQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or userunkid = 1) "
    '                End If
    '            End If
    '        End If


    '        If intCompanyUnkid <> -1 Then
    '            StrQ &= " AND cfuser_master.companyunkid = '" & intCompanyUnkid & "' "
    '        End If

    '        If strFilterString.Trim.Length > 0 Then
    '            StrQ &= " AND " & strFilterString
    '        End If

    '        If IncludeManuallyAddedUser = False Then
    '            StrQ &= " AND cfuser_master.companyunkid > 0 "
    '        End If

    '        StrQ &= ") AS A WHERE 1 = 1 "


    '        If objOption._EnableAllUser = False Then
    '            Select Case eUsrViewType
    '                Case enViewUserType.SHOW_ACTIVE_USER
    '                    StrQ &= " AND isactive = 1 "
    '                Case enViewUserType.SHOW_INACTIVE_USER
    '                    StrQ &= " AND isactive = 0 "
    '            End Select
    '        Else
    '            Select Case eUsrViewType
    '                Case enViewUserType.SHOW_ACTIVE_USER
    '                    StrQ &= " AND activestatus = 1 AND isactive = 1 "
    '                Case enViewUserType.SHOW_INACTIVE_USER
    '                    StrQ &= " AND (activestatus = 0 OR isactive = 0) "
    '            End Select
    '        End If


    '        objOption = Nothing

    '        StrQ &= " ORDER BY A.GrpId,A.IsGrp DESC "

    '        objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "User Created Manually"))
    '        objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "User(s) Imported From"))
    '        objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "No"))
    '        objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Yes"))

    '        If strListName.Trim.Length <= 0 Then strListName = "List"

    '        dtTable = objDataOperation.ExecQuery(StrQ, strListName).Tables(0)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetGroupedUserList; Module Name: " & mstrModuleName)
    '    Finally
    '        If iObjDataOpr Is Nothing Then
    '            objDataOperation = Nothing
    '        End If
    '    End Try
    '    Return dtTable
    'End Function

    Public Function GetGroupedUserList(ByVal strListName As String, _
                                       ByVal eUsrViewType As enViewUserType, _
                                       ByVal dtStDate As Date, _
                                       ByVal dtEdDate As Date, _
                                       Optional ByVal strFilterString As String = "", _
                                       Optional ByVal intCompanyUnkid As Integer = -1, _
                                       Optional ByVal IncludeManuallyAddedUser As Boolean = True, _
                                       Optional ByVal blnShowCount As Boolean = False) As DataTable

        Dim StrQ As String = String.Empty
        Dim strQCondition As String = String.Empty

        Dim objDataOperation As clsDataOperation
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()

        Try
            Dim dsCompany As New DataSet

            StrQ = "SELECT DISTINCT cfuser_master.companyunkid " & _
                   "FROM cfuser_master " & _
                   "WHERE cfuser_master.isactive = 1 "

            If intCompanyUnkid > 0 Then
                StrQ &= " AND cfuser_master.companyunkid = " & intCompanyUnkid
            End If

            StrQ &= "ORDER BY cfuser_master.companyunkid "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT DISTINCT " & _
                    "		 CASE WHEN cfuser_master.companyunkid <= 0 THEN @Caption1 ELSE @Caption2 END + ' : ' + ISNULL(name,'') AS Username " & _
                    "		,'' AS Firstname " & _
                    "		,'' AS Lastname " & _
                    "		,'' AS Email " & _
                    "		,'' AS URole " & _
                    "		,0 AS UserUnkid " & _
                    "		,0 AS RoleUnkid " & _
                    "		,0 AS EmployeeUnkid " & _
                    "		,cfuser_master.companyunkid " & _
                   "    ,CAST(1 AS BIT) AS IsGrp " & _
                    "        ,'' AS Manager " & _
                    "		,cfuser_master.companyunkid AS GrpId " & _
                   "    ,CAST(1 AS BIT) AS isactive "
            Select Case eUsrViewType
                Case enViewUserType.SHOW_ACTIVE_USER, enViewUserType.SHOW_ALL_USER
                    StrQ &= " ,CAST(1 AS BIT) AS activestatus "
                Case enViewUserType.SHOW_INACTIVE_USER
                    StrQ &= " ,CAST(0 AS BIT) AS activestatus "
            End Select
            StrQ &= "	FROM cfuser_master " & _
                    "		LEFT JOIN cfcompany_master ON cfcompany_master.companyunkid = cfuser_master.companyunkid " & _
                    "	WHERE 1 = 1 "

            If strFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & strFilterString
            End If

            If intCompanyUnkid <> -1 Then
                StrQ &= " AND cfuser_master.companyunkid = '" & intCompanyUnkid & "' "
            End If

            If IncludeManuallyAddedUser = False Then
                StrQ &= " AND cfuser_master.companyunkid > 0 "
            End If

            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "User Created Manually"))
            objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "User(s) Imported From"))

            If strListName.Trim.Length <= 0 Then strListName = "List"

            dtTable = objDataOperation.ExecQuery(StrQ, strListName).Tables(0)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT " & _
                   "     SPACE(5)+' '+ cfuser_master.username AS Username " & _
                   "    ,cfuser_master.firstname AS Firstname " & _
                   "    ,cfuser_master.lastname AS Lastname " & _
                   "    ,cfuser_master.email AS Email " & _
                   "    ,ISNULL(hrmsConfiguration..cfrole_master.name,'') AS URole " & _
                   "    ,cfuser_master.userunkid AS UserUnkid " & _
                   "    ,cfrole_master.roleunkid AS RoleUnkid " & _
                   "    ,cfuser_master.employeeunkid AS EmployeeUnkid " & _
                   "    ,cfuser_master.companyunkid " & _
                   "    ,CAST(0 AS BIT) AS IsGrp " & _
                    "       ,CASE WHEN ismanager = 0 THEN @No ELSE @Yes END AS Manager " & _
                   "    ,cfuser_master.companyunkid AS GrpId " & _
                   "    ,cfuser_master.isactive " & _
                   "    ,'#ACT#' " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "    LEFT JOIN hrmsConfiguration..cfrole_master on hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid "

            strQCondition = "	WHERE 1 = 1 "

            Dim objOption As New clsPassowdOptions
            If objOption._EnableAllUser = False Then
                Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                If drOption.Length > 0 Then
                    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                        If objOption._IsEmployeeAsUser Then
                            strQCondition &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR userunkid = 1) "
                        Else
                            strQCondition &= " AND ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0 "
                        End If
                        strQCondition &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or userunkid = 1) "
                    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                        strQCondition &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or userunkid = 1) "
                    End If
                End If
            End If

            Dim strRowFilter As String = String.Empty

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ALL USERS ARE COMING WHETHER ACTIVE OR INACTIVE
            'If objOption._EnableAllUser = False Then
            '    Select Case eUsrViewType
            '        Case enViewUserType.SHOW_ACTIVE_USER
            '            strRowFilter &= " AND activestatus = 1 AND isactive = 1 "
            '        Case enViewUserType.SHOW_INACTIVE_USER
            '            strRowFilter &= " AND (activestatus = 0 OR isactive = 0) "
            '    End Select
            'End If

                Select Case eUsrViewType
                    Case enViewUserType.SHOW_ACTIVE_USER
                        strRowFilter &= " AND activestatus = 1 AND isactive = 1 "
                    Case enViewUserType.SHOW_INACTIVE_USER
                        strRowFilter &= " AND (activestatus = 0 OR isactive = 0) "
                End Select
            'S.SANDEEP [04-AUG-2017] -- END




            objOption = Nothing

            If intCompanyUnkid <> -1 Then
                strQCondition &= " AND cfuser_master.companyunkid = '" & intCompanyUnkid & "' "
            End If

            If strFilterString.Trim.Length > 0 Then
                strQCondition &= " AND " & strFilterString
            End If

            If IncludeManuallyAddedUser = False Then
                strQCondition &= " AND cfuser_master.companyunkid > 0 "
            End If

            If dsCompany.Tables(0).Rows.Count > 0 Then
                Dim dsList As New DataSet
                Dim StrExQry As String = String.Empty
                For Each dr In dsCompany.Tables(0).Rows
                    StrExQry = StrQ
                    Dim strJoin, strFilter, strActiveSatusCol As String : strJoin = "" : strFilter = "" : strActiveSatusCol = " CAST(1 AS BIT) AS activestatus "
                    If dr("companyunkid") > 0 Then
                        GetActiveFilterCondition(dr("companyunkid"), objDataOperation, strJoin, strFilter, dtEdDate, strActiveSatusCol)
                    End If

                    StrExQry &= strJoin

                    StrExQry &= strQCondition
                    StrExQry &= " AND hrmsConfiguration..cfuser_master.companyunkid = '" & dr("companyunkid") & "' "

                    StrExQry = StrExQry.Replace("'#ACT#'", strActiveSatusCol)


                    objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "No"))
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Yes"))

                    dsList = objDataOperation.ExecQuery(StrExQry, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                    dtTable.Merge(dsList.Tables(0).Copy, True)
                Next
            End If

            If strRowFilter.Trim.Length > 0 Then
                dtTable = New DataView(dtTable, strRowFilter.Substring(4), "GrpId,IsGrp DESC", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dtTable, "", "GrpId,IsGrp DESC", DataViewRowState.CurrentRows).ToTable
            End If

            Dim dtmp() As DataRow = Nothing
            Dim strHideCompanyIds As String = ""
            If blnShowCount Then
                dtmp = dtTable.Select("IsGrp=True")
                Dim intCnt As Integer
                For rowindex As Integer = 0 To dtmp.Length - 1
                    intCnt = 0 : intCnt = dtTable.Compute("COUNT(companyunkid)", "companyunkid = " & dtmp(rowindex)("companyunkid")) - 1
                    dtmp(rowindex)("Username") = dtmp(rowindex)("Username") & " (" & intCnt.ToString() & ") "
                    If intCnt <= 0 Then strHideCompanyIds &= "," & dtmp(rowindex)("companyunkid")
                Next
            Else
                dtmp = dtTable.Select("IsGrp=True AND companyunkid <= 0 ")

                'Pinkal (12-Apr-2016) -- Start
                'Enhancement - WORKED ON Edit Report Priviledge For User.
                If dtmp.Length > 0 Then dtmp(0)("Username") = dtmp(0)("Username").ToString.Replace(":", "")
                'Pinkal (12-Apr-2016) -- End

            End If

            If strHideCompanyIds.Trim.Length > 0 Then
                strHideCompanyIds = Mid(strHideCompanyIds, 2)
                strHideCompanyIds = "GrpId NOT IN(" & strHideCompanyIds & ") "
                dtTable = New DataView(dtTable, strHideCompanyIds, "GrpId,IsGrp DESC", DataViewRowState.CurrentRows).ToTable
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGroupedUserList; Module Name: " & mstrModuleName)
        Finally
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dtTable
    End Function

    'S.SANDEEP [14-APR-2017] -- START
    'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
    'Private Sub GetActiveFilterCondition(ByVal intCompanyId As Integer, _
    '                                     ByVal objDataOpr As clsDataOperation, _
    '                                     ByRef strJoinCondition As String, _
    '                                     ByRef strFilterString As String, _
    '                                     Optional ByVal dtEffectiveDate As Date = Nothing, _
    '                                     Optional ByRef strActiveStatusAsCol As String = "")

    Friend Sub GetActiveFilterCondition(ByVal intCompanyId As Integer, _
                                         ByVal objDataOpr As clsDataOperation, _
                                         ByRef strJoinCondition As String, _
                                         ByRef strFilterString As String, _
                                         Optional ByVal dtEffectiveDate As Date = Nothing, _
                                         Optional ByRef strActiveStatusAsCol As String = "")
        'S.SANDEEP [14-APR-2017] -- END

        Dim StrQ As String = ""
        Dim ds As New DataSet
        Try
            StrQ = "SELECT " & _
                   "     cffinancial_year_tran.yearunkid " & _
                   "    ,cffinancial_year_tran.database_name " & _
                   "    ,CONVERT(NVARCHAR(8),cffinancial_year_tran.start_date,112) AS start_date " & _
                   "    ,CONVERT(NVARCHAR(8),cffinancial_year_tran.end_date,112) AS end_date " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE cffinancial_year_tran.companyunkid = '" & intCompanyId & "' AND cffinancial_year_tran.isclosed = 0 "

            ds = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP [01-AUG-2017] -- START
                If eZeeDate.convertDate(ds.Tables("List").Rows(0)("end_date").ToString()).Date < dtEffectiveDate.Date Then
                    dtEffectiveDate = eZeeDate.convertDate(ds.Tables("List").Rows(0)("end_date").ToString()).Date
                End If
                'S.SANDEEP [01-AUG-2017] -- END
                StrQ = ""
                strJoinCondition = " JOIN " & ds.Tables(0).Rows(0)("database_name") & "..hremployee_master ON hremployee_master.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "
                GetDatesFilterString(StrQ, _
                                     strFilterString, _
                                     dtEffectiveDate, dtEffectiveDate, _
                                     True, False, ds.Tables(0).Rows(0)("database_name"))

                strJoinCondition &= StrQ

                strActiveStatusAsCol = "CASE WHEN " & strFilterString.Substring(4) & " THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS activestatus "

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActiveFilterCondition; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [16 MAR 2016] -- END
    'Shani(16-MAR-2016) -- Start
    Public Function GetExternalApproverList(ByVal strListName As String, _
                                            ByVal intCompanyId As Integer, _
                                            ByVal intYearId As Integer, _
                                            ByVal dtStDate As Date, _
                                            ByVal dtEdDate As Date, _
                                            ByVal strPrivilegUnkids As String, _
                                            Optional ByVal blnOnlyManager As Boolean = True, _
                                            Optional ByVal strFilterString As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation

        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            Dim StrQCondition, strFinalQuery As String

            objDataOperation.ClearParameters()

            Dim dsCompany As New DataSet

            StrQ = "SELECT " & _
                   "    DISTINCT cfuser_master.companyunkid " & _
                   "    ,ISNULL(cfconfiguration.key_value,'" & eZeeDate.convertDate(Now) & "') AS Edate " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "    LEFT JOIN hrmsConfiguration..cfconfiguration ON cfconfiguration.companyunkid = cfuser_master.companyunkid AND UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                   "WHERE cfuser_master.isactive = 1 AND cfuser_master.companyunkid <> " & intCompanyId & " " & _
                   "ORDER BY cfuser_master.companyunkid "

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = ""

            StrQ = "SELECT " & _
                   "     cfuser_master.userunkid " & _
                   "    ,cfuser_master.username " & _
                   "    ,CASE WHEN ISNULL(cfuser_master.firstname,'') + ' ' + ISNULL(cfuser_master.lastname,'') = ' ' THEN cfuser_master.username ELSE ISNULL(cfuser_master.firstname,'') + ' ' + ISNULL(cfuser_master.lastname,'')  END AS Name " & _
                   "    ,cfuser_master.ismanager " & _
                   "    ,cfuser_master.employeeunkid " & _
                   "    ,cfuser_master.companyunkid " & _
                   "FROM hrmsConfiguration..cfuser_master " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfcompanyaccess_privilege.userunkid " & _
                   "    #JOIN_QUERY# "

            StrQCondition = " WHERE cfuser_privilege.privilegeunkid IN (" & strPrivilegUnkids & ") AND cfcompanyaccess_privilege.yearunkid = '" & intYearId & "' " & _
                            "   AND cfuser_master.companyunkid = #CoyId# "

            If blnOnlyManager Then
                StrQCondition &= " AND cfuser_master.ismanager = 1 "
            End If

            If strFilterString.Trim.Length > 0 Then
                StrQCondition &= " AND " & strFilterString
            End If

            Dim objOption As New clsPassowdOptions

            If objOption._EnableAllUser = False Then
                Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                If drOption.Length > 0 Then
                    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                        If objOption._IsEmployeeAsUser Then
                            'If objOption._EnableAllUser = False Then
                            '    StrQCondition &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                            'End If
                            StrQCondition &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1 ) "
                        Else
                            StrQCondition &= " AND ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0 "
                        End If
                        StrQCondition &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                        StrQCondition &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    End If
                End If
            End If

            objOption = Nothing

            If strListName.Trim.Length <= 0 Then strListName = "List"

            If dsCompany.Tables(0).Rows.Count > 0 Then
                Dim dstmp As DataSet
                For Each dr In dsCompany.Tables(0).Rows
                    dstmp = New DataSet
                    strFinalQuery = StrQ
                    Dim strJoin, strFilter As String : strJoin = "" : strFilter = ""
                    If dr("companyunkid") > 0 Then
                        GetActiveFilterCondition(dr("companyunkid"), objDataOperation, strJoin, strFilter, CDate(eZeeDate.convertDate(dr("Edate").ToString)))
                        strFinalQuery = strFinalQuery.Replace("#JOIN_QUERY#", strJoin)
                    Else
                        strFinalQuery = strFinalQuery.Replace("#JOIN_QUERY#", "")
                    End If

                    strFinalQuery &= StrQCondition
                    strFinalQuery = strFinalQuery.Replace("#CoyId#", dr("companyunkid"))
                    strFinalQuery &= strFilter

                    dstmp = objDataOperation.ExecQuery(strFinalQuery, strListName)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables.Count <= 0 Then
                        dsList.Tables.Add(dstmp.Tables(0).Copy)
                    Else
                        dsList.Tables(0).Merge(dstmp.Tables(0), True)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetExternalApproverList ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'Shani(16-MAR-2016) -- End
    'S.SANDEEP [30 JAN 2016] -- END

    ''' <summary>
    ''' Developed By: Nilay Mistry
    ''' 11-Aug-2015
    ''' </summary>
    ''' <purpose> Export User List </purpose>
    Public Function ExportUserList(ByVal dsUserList As DataTable, ByVal strCaption As String, ByVal strFilterCriteria As String) As System.Text.StringBuilder
        Dim strBuilder As New System.Text.StringBuilder
        Try
            strBuilder.Append("<HTML>" & vbCrLf)
            strBuilder.Append("<BODY><FONT FACE = VERDANA FONT SIZE=2>" & vbCrLf)
            strBuilder.Append("<TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH='90%'>" & vbCrLf)
            strBuilder.Append("<TR ALIGN='LEFT' WIDTH='90%'></TR>" & vbCrLf)
            strBuilder.Append("<TR ALIGN='LEFT' WIDTH='90%'></TR>" & vbCrLf)
            strBuilder.Append("<TR ALIGN='LEFT' WIDTH='90%'>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='CENTER' COLSPAN=6 BORDER=1 BORDERCOLOR=BLACK WIDTH='90%'><FONT SIZE=3><B> " & Language.getMessage(mstrModuleName, 18, "USER LIST") & " </B></FONT> " & "(" & strCaption.ToString & ")" & " </TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("<TR ALIGN='LEFT'><TD BORDER=1 COLSPAN=6 BORDERCOLOR=BLACK><FONT SIZE=3>" & strFilterCriteria & "</FONT></TD></TR>" & vbCrLf)
            strBuilder.Append("<TR ALIGN='LEFT'>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='LEFT'  BORDER=1 BORDERCOLOR=BLACK WIDTH='15%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & Language.getMessage(mstrModuleName, 20, "User Name") & " </B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='LEFT'  BORDER=1 BORDERCOLOR=BLACK WIDTH='15%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & Language.getMessage(mstrModuleName, 14, "First Name") & " </B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='LEFT'  BORDER=1 BORDERCOLOR=BLACK WIDTH='15%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & Language.getMessage(mstrModuleName, 15, "Last Name") & " </B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='LEFT'  BORDER=1 BORDERCOLOR=BLACK WIDTH='15%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & Language.getMessage(mstrModuleName, 16, "Email") & " </B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='LEFT'  BORDER=1 BORDERCOLOR=BLACK WIDTH='15%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & Language.getMessage(mstrModuleName, 19, "User Role") & " </B></FONT></TD>" & vbCrLf)
            strBuilder.Append("<TD ALIGN='LEFT'  BORDER=1 BORDERCOLOR=BLACK WIDTH='15%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & Language.getMessage(mstrModuleName, 23, "Manager/Desktop") & " </B></FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("<TR ALIGN='LEFT'><TD BORDER=1 COLSPAN=6 BORDERCOLOR=BLACK></TD></TR>" & vbCrLf)


            For i As Integer = 0 To dsUserList.Rows.Count - 1
                If CBool(dsUserList.Rows(i).Item("IsGrp")) Then
                    strBuilder.Append("<TR ALIGN='LEFT' WIDTH='90%'>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' COLSPAN=6 BORDER=1 BORDERCOLOR=BLACK WIDTH='90%' BGCOLOR = 'LightGreen'><FONT SIZE=2 COLOR='White'><B> " & dsUserList.Rows(i).Item("Username").ToString & " </B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                Else
                    strBuilder.Append("<TR ALIGN='LEFT' WIDTH='90%'>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' BORDER=1 BORDERCOLOR=BLACK WIDTH='15%'><FONT SIZE=2>&nbsp;&nbsp;&nbsp;&nbsp;" & dsUserList.Rows(i).Item("Username").ToString.Trim & " </FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' BORDER=1 BORDERCOLOR=BLACK WIDTH='15%'><FONT SIZE=2>" & dsUserList.Rows(i).Item("Firstname").ToString.Trim & " </FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' BORDER=1 BORDERCOLOR=BLACK WIDTH='15%'><FONT SIZE=2>" & dsUserList.Rows(i).Item("Lastname").ToString.Trim & " </FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' BORDER=1 BORDERCOLOR=BLACK WIDTH='15%'><FONT SIZE=2>" & dsUserList.Rows(i).Item("Email").ToString.Trim & " </FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' BORDER=1 BORDERCOLOR=BLACK WIDTH='15%'><FONT SIZE=2>" & dsUserList.Rows(i).Item("URole").ToString.Trim & " </FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD ALIGN='LEFT' BORDER=1 BORDERCOLOR=BLACK WIDTH='15%'><FONT SIZE=2>" & dsUserList.Rows(i).Item("Manager").ToString.Trim & " </FONT></TD>" & vbCrLf)
                    strBuilder.Append("</TR>" & vbCrLf)
                End If
            Next

            strBuilder.Append("</TABLE>" & vbCrLf)
            strBuilder.Append("</BODY>" & vbCrLf)
            strBuilder.Append("</HTML>" & vbCrLf)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportUserList; Module Name: " & mstrModuleName)
        End Try
        Return strBuilder
    End Function


    'S.SANDEEP [14-APR-2017] -- START
    'ISSUE/ENHANCEMENT : GET USER BASED ON COMPANY ACCESS PRIVILEGE IN OPTION SCREEN
    Public Function GetCompanyBasedUserList(ByVal strTableName As String, ByVal intCompanyId As Integer, Optional ByVal strFilterString As String = "", Optional ByVal strPrivilegIds As String = "", Optional ByVal blnIsManage As Boolean = False, Optional ByVal eCondition As enApplyPrivilegeCondition = enApplyPrivilegeCondition.APPLY_IN) As DataSet 'S.SANDEEP [20-JUN-2018] -- START {Ref#244} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If iObjDataOpr IsNot Nothing Then
            objDataOperation = iObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()

        Try
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'strQ = "SELECT " & _
            '       "     cfuser_master.username " & _
            '       "    ,cfuser_master.userunkid " & _
            '       "    ,cfuser_master.email " & _
            '       "FROM hrmsConfiguration..cffinancial_year_tran AS cYear " & _
            '       "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cYear.yearunkid = cfcompanyaccess_privilege.yearunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_master ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
            '       "WHERE cYear.companyunkid = @CompanyId AND cYear.isclosed = 0 "

            'S.SANDEEP [07-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002513|ARUTI-325}
            'strQ = "SELECT " & _
            '       "     cfuser_master.username " & _
            '       "    ,cfuser_master.userunkid " & _
            '       "    ,cfuser_master.email " & _
            '       "FROM hrmsConfiguration..cffinancial_year_tran AS cYear " & _
            '       "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cYear.yearunkid = cfcompanyaccess_privilege.yearunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_master ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid "
            strQ = "SELECT DISTINCT " & _
                   "     cfuser_master.username " & _
                   "    ,cfuser_master.userunkid " & _
                   "    ,cfuser_master.email " & _
                   "FROM hrmsConfiguration..cffinancial_year_tran AS cYear " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cYear.yearunkid = cfcompanyaccess_privilege.yearunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid "
            'S.SANDEEP [07-SEP-2018] -- END
            If strPrivilegIds.Trim.Length > 0 Then
                strQ &= " JOIN hrmsConfiguration..cfuser_privilege ON cfuser_privilege.userunkid = cfuser_master.userunkid "
                Select Case eCondition
                    Case enApplyPrivilegeCondition.APPLY_IN
                        strQ &= " AND privilegeunkid IN (" & strPrivilegIds & ") "
                    Case enApplyPrivilegeCondition.APPLY_NOT_IN
                        strQ &= " AND privilegeunkid NOT IN (" & strPrivilegIds & ") "
                End Select
            End If
            strQ &= "WHERE cYear.companyunkid = @CompanyId AND cYear.isclosed = 0 "
            If blnIsManage = True Then
                strQ &= " AND ismanager = 1 "
            End If
            'S.SANDEEP [20-JUN-2018] -- END

            
            strQ &= " AND hrmsConfiguration..cfuser_master.isactive = 1 "

            Dim objOption As New clsPassowdOptions
            If objOption._EnableAllUser = False Then
                Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                If drOption.Length > 0 Then
                    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                        If objOption._IsEmployeeAsUser Then
                            strQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                        Else
                            strQ &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0  OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                        End If
                        strQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                        strQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
                    End If
                End If
            End If
            objOption = Nothing

            If strFilterString.Trim.Length > 0 Then
                strQ &= " AND " & strFilterString
            End If

            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function
    'S.SANDEEP [14-APR-2017] -- END



    'Pinkal (02-May-2017) -- Start
    'Enhancement - Working on Leave Enhancement for HJF.

    Public Function GetUserAccessIDsAllocationWise(ByVal xUserId As Integer, ByVal xCompanyID As Integer, ByVal xYearId As Integer, ByVal xAllocationID As Integer) As String
        Dim mstrAccessIDs As String = ""
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                           "    STUFF((SELECT ',' + CAST(allocationunkid AS NVARCHAR(50)) " & _
                           " FROM hrmsConfiguration..cfuseraccess_privilege_tran " & _
                           " JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
                           " WHERE userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND referenceunkid = @AllocationID" & _
                           " ORDER BY hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid FOR XML PATH('')),1,1,'') AS CSV "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyID)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)
            objDataOperation.AddParameter("@AllocationID", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                If dsList.Tables("List").Rows(0)("CSV").ToString.Trim.Length > 0 Then
                    mstrAccessIDs = dsList.Tables("List").Rows(0)("CSV").ToString
                Else
                    mstrAccessIDs = "0"
                End If
            Else
                mstrAccessIDs = "0"
            End If
            objDataOperation = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUserAccessIDsAllocationWise; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return mstrAccessIDs
    End Function

    'Pinkal (02-May-2017) -- End


    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.

    Public Function EnableDisableADUser(ByVal mblnEnable As Boolean, ByVal xUserId As Integer, ByVal xOldUserAccountControl As Integer, ByVal xNewUserAccountControl As Integer) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            If mblnEnable Then

                If xOldUserAccountControl <> xNewUserAccountControl Then  'CHECK WHETHER DISABLE THEN ONLY INSERT TO THIS TABLE

                Dim dtDate As New DateTime(1970, 1, 1)
                Dim xTimeStamp As Integer = (ConfigParameter._Object._CurrentDateAndTime() - dtDate).TotalMinutes()

                strQ = " INSERT INTO hrmsConfiguration..cfenabledisable_aduser " & _
                          " ( " & _
                          "   userunkid " & _
                          "  ,timestamp " & _
                          " ) VALUES (" & _
                          "   @userunkid " & _
                          "  ,@timestamp " & _
                          "); SELECT @@identity "

                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                objDataOperation.AddParameter("@timestamp", SqlDbType.Int, eZeeDataType.INT_SIZE, xTimeStamp)
                objDataOperation.ExecQuery(strQ, "ADUser")

                End If

            Else

                If xOldUserAccountControl <> xNewUserAccountControl Then  'CHECK WHETHER ENABLE THEN ONLY DELETE TO THIS TABLE
                strQ = "Delete From hrmsConfiguration..cfenabledisable_aduser WHERE userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                objDataOperation.ExecNonQuery(strQ)
                End If

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mblnFlag = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EnableDisableADUser; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return mblnFlag
    End Function

    'Pinkal (23-AUG-2017) -- End


    'Varsha (10 Nov 2017) -- Start
    'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
    Public Function UpdateSelfServiceTheme(ByVal strKey_Name As String, ByVal intCompanyunkid As Integer, ByVal strCurrDBName As String, ByVal intThemeId As Integer)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If iObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = iObjDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            objDataOperation.AddParameter("@key_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strKey_Name.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyunkid)
            objDataOperation.AddParameter("@theme_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intThemeId)
            strQ = "UPDATE cfuser_master SET " & _
                    " theme_id = ISNULL(A.key_value, 1) " & _
                    " FROM (SELECT " & _
                           " C.companyunkid, " & _
                           " ISNULL(B.key_value, 1) AS key_value " & _
                           " FROM (SELECT TOP 1 " & _
                                     " companyunkid " & _
                                     " FROM cfcompany_master " & _
                                     " WHERE isactive = 1) AS C " & _
                                     " LEFT JOIN (SELECT " & _
                                                " key_value, " & _
                                                " companyunkid " & _
                                                " FROM cfconfiguration " & _
                                                " WHERE key_name = @key_name) AS B " & _
                                                " ON C.companyunkid = B.companyunkid) AS A " & _
                                                " WHERE cfuser_master.companyunkid = 0 " & _
                                                " AND theme_id <> ISNULL(A.key_value, 1) "
            strQ += "UPDATE cfuser_master SET " & _
                            " theme_id = ISNULL(A.key_value, 1) " & _
                            " FROM (SELECT " & _
                                                " key_value, " & _
                                                " companyunkid " & _
                                                " FROM cfconfiguration " & _
                                                " WHERE key_name = @key_name AND cfconfiguration.companyunkid = @companyunkid ) AS A " & _
                                                " WHERE cfuser_master.companyunkid = A.companyunkid " & _
                                                " AND theme_id <> ISNULL(A.key_value, 1) "


            strQ += "UPDATE " + strCurrDBName + "..hremployee_master SET hremployee_master.theme_id = @theme_id"
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iObjDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: UpdateSelfServiceTheme; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iObjDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function
    'Varsha (10 Nov 2017) -- End



    'Gajanan [19-OCT-2019] -- Start    
    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.

    Private Function CreateTableFromList(ByVal isCompnayPrivilege As Boolean, ByVal csvList As String) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Dim MaxCol As Integer = 5

        Try

            If isCompnayPrivilege Then
                strQ = "select name from cfcompany_master where companyunkid in(" & csvList & ")"
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                strQ = "select privilege.privilege_name,pgrp.name,pgrp.privilegegroupunkid from hrmsConfiguration..cfuserprivilege_master as privilege " & _
                       " left join hrmsConfiguration..cfuserprivilegegroup_master as pgrp " & _
                       " on privilege.privilegegroupunkid = pgrp.privilegegroupunkid where privilege.privilegeunkid in( " & csvList & ") order by pgrp.name, pgrp.privilegegroupunkid, privilege.privilege_name "
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            Dim StrMessage As New System.Text.StringBuilder
            If IsNothing(dsList.Tables("List")) = False AndAlso dsList.Tables("List").Rows.Count > 0 Then
                Dim icount As Integer = 0
                Dim OldGrp As String = String.Empty

                StrMessage.Append("<TABLE border = '1' >")
                For Each drow As DataRow In dsList.Tables(0).Rows

                    If isCompnayPrivilege = False Then
                        If OldGrp <> drow("name").ToString() Then
                            OldGrp = drow("name").ToString()

                            StrMessage.Append("<TR bgcolor= 'SteelBlue'>")
                            StrMessage.Append("<TD align = 'LEFT' colspan='" & MaxCol + 1 & "'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#ffffff; '>" & drow("name").ToString() & "</span></TD>")
                            StrMessage.Append("</TR>")
                            icount = 0
                        End If
                    End If

                    If icount = 0 Then
                        StrMessage.Append("<TR>")
                    End If

                    If isCompnayPrivilege Then
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & drow("name").ToString & "</span></TD>")
                    Else
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & drow("privilege_name").ToString & "</span></TD>")
                    End If

                    icount = icount + 1
                    If icount > MaxCol Then
                        StrMessage.Append("</TR>")
                        icount = 0
                    End If
                Next
                StrMessage.Append("</TABLE>")

            End If
            Return StrMessage.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateTableFromList; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    Public Sub SendNotification(ByVal xstrUserlist As String, _
                                ByVal xintCompanyId As Integer, _
                                ByVal xstrCompanyName As String, _
                                ByVal xstrUserName As String, _
                                ByVal xstrIp As String, _
                                ByVal xstrMachine As String, _
                                ByVal xstrFormName As String, _
                                ByVal xeMode As enLogin_Mode, _
                                ByVal xintUserId As Integer, _
                                ByVal xstrSenderAddress As String, _
                                ByVal xstrAssignedApproever As String, _
                                Optional ByVal xIsUserNewlyCreated As Boolean = False, _
                                Optional ByVal xNewlyAddedPrivilegeIds As String = "", _
                                Optional ByVal xDeletedPrivilegeIds As String = "", _
                                Optional ByVal xNewlyAddedCompanyPrivilegeIds As String = "", _
                                Optional ByVal xDeletedCompanyPrivilegeIds As String = "", _
                                Optional ByVal xNewAssignRole As String = "", _
                                Optional ByVal strOldRole As String = "", _
                                Optional ByVal dtCurrentDateTime As Date = Nothing _
                                )
        'Sohail (31 Oct 2019) - [strOldRole]

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty

        Dim objDataOperation As New clsDataOperation
        Dim dtAppr As DataTable = Nothing
        Dim objUserAddEdit As New clsUserAddEdit

        Try
            If xstrUserlist.Length > 0 Then
                Dim dsUserlist As DataSet = objUserAddEdit.GetList("List", "hrmsConfiguration..cfuser_master.userunkid in (" & xstrUserlist & ")")
                Dim objSendMail As New clsSendMail


                If IsNothing(dsUserlist.Tables(0)) = False AndAlso dsUserlist.Tables(0).Rows.Count > 0 Then

                    For Each iRow As DataRow In dsUserlist.Tables(0).Rows
                        Dim StrMessage As New System.Text.StringBuilder

                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                        Dim mstrUsername As String = iRow("firstname").ToString() + " " + iRow("lastname").ToString()
                        If mstrUsername.Trim().Length <= 0 Then
                            mstrUsername = iRow("username").ToString()
                        End If
                        If iRow("email").ToString().Length <= 0 Then Continue For

                        StrMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " " & "<b>" & getTitleCase(mstrUsername) & "</b></span></p>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                        If xIsUserNewlyCreated Then
                            StrMessage.Append(Language.getMessage(mstrModuleName, 101, "This is to notify you that a new user"))
                            'Sohail (01 Nov 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
                            'StrMessage.Append(" <b>" & xstrAssignedApproever & "</b>.")
                            'StrMessage.Append(Language.getMessage(mstrModuleName, 117, " has been created.") & " </span></p>")
                            StrMessage.Append(" <b>" & xstrAssignedApproever & "</b> ")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 117, "has been created."))
                            'Sohail (01 Nov 2019) -- End

                        ElseIf xNewAssignRole.Length > 0 Then
                            'Sohail (31 Oct 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                            'StrMessage.Append(Language.getMessage(mstrModuleName, 102, "This is to notify you that a new role has been"))
                            StrMessage.Append(Language.getMessage(mstrModuleName, 102, "This is to notify you that a role has been"))
                            'Sohail (31 Oct 2019) -- End

                        ElseIf (xDeletedPrivilegeIds.Length > 0 OrElse xNewlyAddedPrivilegeIds.Length > 0) AndAlso (xDeletedCompanyPrivilegeIds.Length > 0 OrElse xNewlyAddedCompanyPrivilegeIds.Length > 0) Then

                            'Sohail (31 Oct 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                            'StrMessage.Append(Language.getMessage(mstrModuleName, 103, "This is to notify you that a company access and operational privileges has been"))
                            StrMessage.Append(Language.getMessage(mstrModuleName, 103, "This is to notify you that changes have been made in company access and privileges by"))
                            'Sohail (31 Oct 2019) -- End

                        ElseIf (xDeletedCompanyPrivilegeIds.Length > 0 OrElse xNewlyAddedCompanyPrivilegeIds.Length > 0) Then

                            'Sohail (31 Oct 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                            'StrMessage.Append(Language.getMessage(mstrModuleName, 118, "This is to notify you that a company access has been"))
                            StrMessage.Append(Language.getMessage(mstrModuleName, 118, "This is to notify you that changes have been made in company access by"))
                            'Sohail (31 Oct 2019) -- End

                        Else
                            'Sohail (31 Oct 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                            'StrMessage.Append(Language.getMessage(mstrModuleName, 119, "This is to notify you that a operational privileges has been"))
                            StrMessage.Append(Language.getMessage(mstrModuleName, 119, "This is to notify you that changes have been made in privileges by"))
                            'Sohail (31 Oct 2019) -- End
                        End If


                        If xIsUserNewlyCreated = False AndAlso xNewAssignRole.Length > 0 Then
                            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 116, "assigned to"))
                            StrMessage.Append(" <b>" & xstrAssignedApproever & "</b>.")
                            StrMessage.Append(" </span></p>")
                            'Sohail (01 Nov 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
                        ElseIf xIsUserNewlyCreated = True Then
                            StrMessage.Append(" </span></p>")
                            'Sohail (01 Nov 2019) -- End
                        Else
                            'Sohail (31 Oct 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                            'StrMessage.Append(" " & Language.getMessage(mstrModuleName, 120, "changed for"))
                            StrMessage.Append(" <b>" & xstrUserName & "</b> ")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 122, "for"))
                            'Sohail (31 Oct 2019) -- End
                            StrMessage.Append(" <b>" & xstrAssignedApproever & "</b> ")
                            'Sohail (31 Oct 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                            'StrMessage.Append(" </span></p>")
                            'Sohail (31 Oct 2019) -- End
                        End If

                        'Sohail (31 Oct 2019) -- Start
                        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                        If xNewAssignRole.Length > 0 Then
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px;'>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 107, "Details as below:") & " </span></p>")

                            StrMessage.Append("<TABLE border = '1' style='width:100%;' >")
                            StrMessage.Append("<TR bgcolor= 'SteelBlue'>")
                            StrMessage.Append("<TD align = 'LEFT' style='width:50%;' ><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#ffffff; '>" & Language.getMessage(mstrModuleName, 123, "Old Role") & "</span></TD>")
                            StrMessage.Append("<TD align = 'LEFT' style='width:50%;' ><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#ffffff; '>" & Language.getMessage(mstrModuleName, 124, "New Role") & "</span></TD>")
                            StrMessage.Append("</TR>")
                            StrMessage.Append("<TR>")
                            StrMessage.Append("<TD align = 'LEFT' ><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & strOldRole & "</span></TD>")
                            StrMessage.Append("<TD align = 'LEFT' ><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xNewAssignRole & "</span></TD>")
                            StrMessage.Append("</TR>")
                            StrMessage.Append("</TABLE>")
                        End If

                        'Sohail (01 Nov 2019) -- Start
                        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
                        'If xIsUserNewlyCreated = False AndAlso xNewAssignRole.Length > 0 Then
                        If xIsUserNewlyCreated = True OrElse xNewAssignRole.Length > 0 Then
                            'Sohail (01 Nov 2019) -- End
                            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 125, "The change has been made by"))
                            StrMessage.Append(" <b>" & xstrUserName & "</b> ")
                        Else
                            StrMessage.Append(Language.getMessage(mstrModuleName, 126, "with"))
                            StrMessage.Append(" <b>" & strOldRole & "</b> ")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 127, "role") & " ")
                        End If

                        StrMessage.Append(Language.getMessage(mstrModuleName, 111, "from machine"))
                        StrMessage.Append(" <b>" & xstrMachine & "</b> ")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 110, "and IP"))
                        StrMessage.Append(" <b>" & xstrIp & "</b> ")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 128, "on"))
                        If dtCurrentDateTime <> Nothing Then
                            StrMessage.Append(" <b>" & Format(dtCurrentDateTime, "dd-MMM-yyyy") & "</b> ")
                        Else
                            StrMessage.Append(" <b>" & Format(DateAndTime.Now, "dd-MMM-yyyy") & "</b> ")
                        End If
                        StrMessage.Append(Language.getMessage(mstrModuleName, 120, "at"))
                        If dtCurrentDateTime <> Nothing Then
                            StrMessage.Append(" <b>" & Format(dtCurrentDateTime, "hh:mm tt") & "</b> ")
                        Else
                            StrMessage.Append(" <b>" & Format(DateAndTime.Now, "hh:mm tt") & "</b> ")
                        End If
                        StrMessage.Append(Language.getMessage(mstrModuleName, 112, "For more details, please refer to the privilege level report"))
                        StrMessage.Append("</span></p>")
                        'Sohail (31 Oct 2019) -- End
                      
                        'Sohail (01 Nov 2019) -- Start
                        'NMB Payroll UAT Enhancement # : Remove this section of additional informational for newly assigned and deleted operational privilege.
                        If 1 = 2 Then
                            'Sohail (01 Nov 2019) -- End
                        If xIsUserNewlyCreated Then

                            If xNewlyAddedCompanyPrivilegeIds.Length > 0 Then
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 105, "Newly assigned company privilage:") & " </span></p>")

                                Dim strNewCompany As String = CreateTableFromList(True, xNewlyAddedCompanyPrivilegeIds)
                                StrMessage.Append(strNewCompany)
                            End If


                            If xNewlyAddedPrivilegeIds.Length > 0 Then
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 106, "Newly assigned oprational privilage:") & " </span></p>")

                                Dim strPrivilege As String = CreateTableFromList(False, xNewlyAddedPrivilegeIds)
                                StrMessage.Append(strPrivilege)
                            End If

                        Else
                            If xNewlyAddedCompanyPrivilegeIds.Length > 0 Then
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 105, "Newly assigned company privilage:") & " </span></p>")

                                Dim strNewCompany As String = CreateTableFromList(True, xNewlyAddedCompanyPrivilegeIds)
                                StrMessage.Append(strNewCompany)
                            End If

                            If xDeletedCompanyPrivilegeIds.Length > 0 Then
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 121, "Removed company privilage:") & " </span></p>")

                                Dim strNewCompany As String = CreateTableFromList(True, xDeletedCompanyPrivilegeIds)
                                StrMessage.Append(strNewCompany)
                            End If


                            If xNewlyAddedPrivilegeIds.Length > 0 Then
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 106, "Newly assigned oprational privilage:") & " </span></p>")

                                Dim strPrivilege As String = CreateTableFromList(False, xNewlyAddedPrivilegeIds)
                                StrMessage.Append(strPrivilege)
                            End If

                            If xDeletedPrivilegeIds.Length > 0 Then
                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 115, "Deleted oprational privilage:") & " </span></p>")

                                Dim strPrivilege As String = CreateTableFromList(False, xDeletedPrivilegeIds)
                                StrMessage.Append(strPrivilege)
                            End If
                        End If
                            'Sohail (01 Nov 2019) -- Start
                            'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below.
                        End If
                        'Sohail (01 Nov 2019) -- End

                        'Sohail (31 Oct 2019) -- Start
                        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                        'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px;'>")
                        'StrMessage.Append(Language.getMessage(mstrModuleName, 107, "Details as below:") & " </span></p>")

                        'If xNewAssignRole.Length > 0 Then
                        '    StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px;'>")
                        '    StrMessage.Append(Language.getMessage(mstrModuleName, 108, "Role Assigned:") & " </span></b>")
                        '    StrMessage.Append("<span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        '    StrMessage.Append(xNewAssignRole & " </span></p>")
                        'End If
                        'Sohail (31 Oct 2019) -- End

                        'Sohail (31 Oct 2019) -- Start
                        'NMB Enhancement # : Email notification to be sent out upon role assignment on user creation should read as below..
                        'StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(Language.getMessage(mstrModuleName, 109, "Company:") & " </span></b>")
                        'StrMessage.Append("<span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(xstrCompanyName & " </span></p>")

                        'StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(Language.getMessage(mstrModuleName, 110, "Machine IP:") & " </span></b>")
                        'StrMessage.Append("<span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(xstrIp & " </span></p>")

                        'StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(Language.getMessage(mstrModuleName, 111, "Machine Name:") & " </span></b>")
                        'StrMessage.Append("<span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(xstrMachine & " </span></p>")

                        'StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(Language.getMessage(mstrModuleName, 112, "User:") & " </span></b>")
                        'StrMessage.Append("<span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; '>")
                        'StrMessage.Append(xstrUserName & " </span></p>")
                        'Sohail (31 Oct 2019) -- End
                        StrMessage.Append("</BODY></HTML>")


                        objSendMail._ToEmail = iRow("email").ToString().Trim()
                        If xIsUserNewlyCreated Then
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 113, "New user created")
                        Else
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 114, "User information edited")
                        End If

                        objSendMail._Message = StrMessage.ToString()
                        objSendMail._Form_Name = xstrFormName
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = xeMode
                        objSendMail._UserUnkid = xintUserId
                        objSendMail._SenderAddress = xstrSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.Configuration
                        Company._Object._Companyunkid = xintCompanyId
                        objSendMail.SendMail(xintCompanyId)

                    Next
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [19-OCT-2019] -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This User is already defined. Please define new User.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot login, your account is locked.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.")
			Language.setMessage(mstrModuleName, 5, "You can login after")
			Language.setMessage(mstrModuleName, 6, " Or Please contact Administrator.")
			Language.setMessage(mstrModuleName, 7, " Minute(s).")
			Language.setMessage(mstrModuleName, 8, " As per password policy settings your account will be locked after")
			Language.setMessage(mstrModuleName, 9, " attempts. And you have")
			Language.setMessage(mstrModuleName, 10, " attempts left.")
			Language.setMessage(mstrModuleName, 11, "Please contact Administrator.")
			Language.setMessage(mstrModuleName, 12, "User Created Manually")
			Language.setMessage(mstrModuleName, 13, "User(s) Imported From")
			Language.setMessage(mstrModuleName, 14, "First Name")
			Language.setMessage(mstrModuleName, 15, "Last Name")
			Language.setMessage(mstrModuleName, 16, "Email")
			Language.setMessage(mstrModuleName, 18, "USER LIST")
			Language.setMessage(mstrModuleName, 19, "User Role")
			Language.setMessage(mstrModuleName, 20, "User Name")
			Language.setMessage(mstrModuleName, 21, "No")
			Language.setMessage(mstrModuleName, 22, "Yes")
			Language.setMessage(mstrModuleName, 23, "Manager/Desktop")
			Language.setMessage(mstrModuleName, 100, "Dear")
			Language.setMessage(mstrModuleName, 101, "This is to notify you that a new user")
			Language.setMessage(mstrModuleName, 102, "This is to notify you that a role has been")
			Language.setMessage(mstrModuleName, 103, "This is to notify you that changes have been made in company access and privileges by")
			Language.setMessage(mstrModuleName, 105, "Newly assigned company privilage:")
			Language.setMessage(mstrModuleName, 106, "Newly assigned oprational privilage:")
			Language.setMessage(mstrModuleName, 107, "Details as below:")
			Language.setMessage(mstrModuleName, 110, "and IP")
			Language.setMessage(mstrModuleName, 111, "from machine")
			Language.setMessage(mstrModuleName, 112, "For more details, please refer to the privilege level report")
			Language.setMessage(mstrModuleName, 113, "New user created")
			Language.setMessage(mstrModuleName, 114, "User information edited")
			Language.setMessage(mstrModuleName, 115, "Deleted oprational privilage:")
			Language.setMessage(mstrModuleName, 116, "assigned to")
			Language.setMessage(mstrModuleName, 117, " has been created.")
			Language.setMessage(mstrModuleName, 118, "This is to notify you that changes have been made in company access by")
			Language.setMessage(mstrModuleName, 119, "This is to notify you that changes have been made in privileges by")
			Language.setMessage(mstrModuleName, 120, "at")
			Language.setMessage(mstrModuleName, 121, "Removed company privilage:")
			Language.setMessage(mstrModuleName, 122, "for")
			Language.setMessage(mstrModuleName, 123, "Old Role")
			Language.setMessage(mstrModuleName, 124, "New Role")
			Language.setMessage(mstrModuleName, 125, "The change has been made by")
			Language.setMessage(mstrModuleName, 126, "with")
			Language.setMessage(mstrModuleName, 127, "role")
			Language.setMessage(mstrModuleName, 128, "on")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'---------------------------------------------{CHANGED ON 10 AUG 2015 SANDEEP (Aruti SaaS)}-------------------------------------------'
'
'
'
'
'
'
'Public Class clsUserAddEdit1
'    Private Shared ReadOnly mstrModuleName As String = "clsUserAddEdit"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintUserunkid As Integer
'    Private mstrUsername As String = String.Empty
'    Private mstrPassword As String = String.Empty
'    Private mblnIspasswordexpire As Boolean
'    Private mintRoleunkid As Integer
'    Private mblnIsrighttoleft As Boolean
'    Private mintExp_Days As Integer
'    Private mintLanguageunkid As Integer
'    Private mblnIsactive As Boolean = True
'    'Sandeep [ 21 Aug 2010 ] -- Start
'    Private mstrAssignPrivilegeIDs As String = ""
'    Private mstrAssignCompanyPrivilegeIDs As String = ""
'    Private mstrAssignAccessPrivilegeIDs As String = ""
'    Private mstrUserAccessLevels As String = ""
'    Private mintCompanyUnkid As Integer = 0
'    Private mintYearId As Integer = 0
'    Public Privilege As New clsUserPrivilege
'    'Sandeep [ 21 Aug 2010 ] -- End 


'    'Sandeep [ 29 NOV 2010 ] -- Start
'    Private mstrReportCompanyIds As String = String.Empty
'    Private mstrReportIDs As String = String.Empty
'    'Sandeep [ 29 NOV 2010 ] -- End 


'    'S.SANDEEP [ 30 May 2011 ] -- START
'    'ISSUE : FINCA REQ.
'    Private mdtCreationDate As Date = Nothing
'    Private mblnIsLocked As Boolean = False
'    Private mdtLockedTime As DateTime
'    Private mdtLockedDuration As DateTime
'    Public PWDOptions As clsPassowdOptions
'    Private mintCreatedById As Integer = 0
'    'S.SANDEEP [ 30 May 2011 ] -- END 

'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mstrFirstname As String = String.Empty
'    Private mstrLastname As String = String.Empty
'    Private mstrAddress1 As String = String.Empty
'    Private mstrAddress2 As String = String.Empty
'    Private mstrPhone As String = String.Empty
'    Private mstrEmail As String = String.Empty
'    'S.SANDEEP [ 07 NOV 2011 ] -- END


'    'Pinkal (12-Oct-2011) -- Start
'    'Enhancement : TRA Changes
'    Private mblnIsADuser As Boolean = False
'    'Pinkal (12-Oct-2011) -- End



'    'Pinkal (21-Jun-2012) -- Start
'    'Enhancement : TRA Changes
'    Private mblnIsManager As Boolean = False
'    Private mstrUserdomain As String = String.Empty
'    'Pinkal (21-Jun-2012) -- End

'    Private mdtRelogin_Date As Date 'Sohail (04 Jul 2012)

'    'S.SANDEEP [ 08 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Dim mDicRolePrivilege As New Dictionary(Of Integer, DataTable)
'    'S.SANDEEP [ 08 NOV 2012 ] -- END

'    'S.SANDEEP [ 09 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private blnShowChangePasswdfrm As Boolean = False
'    Private intRetUserId As Integer = 0
'    'S.SANDEEP [ 09 NOV 2012 ] -- END

'    'S.SANDEEP [ 01 DEC 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintEmployeeUnkid As Integer = 0
'    Private mintEmployeeCompanyUnkid As Integer = 0
'    Private mdtSuspended_From_Date As Date = Nothing
'    Private mdtSuspended_To_Date As Date = Nothing
'    Private mdtProbation_From_Date As Date = Nothing
'    Private mdtProbation_To_Date As Date = Nothing
'    Private mdtEmpl_Enddate As Date = Nothing
'    Private mdtTermination_From_Date As Date = Nothing
'    Private mdtTermination_To_Date As Date = Nothing
'    Private mdtAppointeddate As Date = Nothing
'    Private mdtReinstatement_Date As Date = Nothing
'    'S.SANDEEP [ 01 DEC 2012 ] -- END


'    'S.SANDEEP [ 31 DEC 2013 ] -- START
'    Private iObjDataOpr As clsDataOperation
'    'S.SANDEEP [ 31 DEC 2013 ] -- END


'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'            Call GetData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set username
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Username() As String
'        Get
'            Return mstrUsername
'        End Get
'        Set(ByVal value As String)
'            mstrUsername = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set password
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Password() As String
'        Get
'            Return mstrPassword
'        End Get
'        Set(ByVal value As String)
'            mstrPassword = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set ispasswordexpire
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Ispasswordexpire() As Boolean
'        Get
'            Return mblnIspasswordexpire
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIspasswordexpire = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set roleunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Roleunkid() As Integer
'        Get
'            Return mintRoleunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintRoleunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isrighttoleft
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isrighttoleft() As Boolean
'        Get
'            Return mblnIsrighttoleft
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsrighttoleft = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set exp_days
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Exp_Days() As Integer
'        Get
'            Return mintExp_Days
'        End Get
'        Set(ByVal value As Integer)
'            mintExp_Days = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set languageunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Languageunkid() As Integer
'        Get
'            Return mintLanguageunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLanguageunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = value
'        End Set
'    End Property


'    'Sandeep [ 21 Aug 2010 ] -- Start
'    Public WriteOnly Property _AssignPrivilegeIDs() As String
'        Set(ByVal value As String)
'            mstrAssignPrivilegeIDs = value
'        End Set
'    End Property

'    Public WriteOnly Property _AssignCompanyPrivilegeIDs() As String
'        Set(ByVal value As String)
'            mstrAssignCompanyPrivilegeIDs = value
'        End Set
'    End Property

'    Public WriteOnly Property _AssignAccessPrivilegeIDs() As String
'        Set(ByVal value As String)
'            mstrAssignAccessPrivilegeIDs = value
'        End Set
'    End Property

'    Public WriteOnly Property _CompanyUnkid() As Integer
'        Set(ByVal value As Integer)
'            mintCompanyUnkid = value
'        End Set
'    End Property

'    Public WriteOnly Property _UserAccessLevels() As String
'        Set(ByVal value As String)
'            mstrUserAccessLevels = value
'        End Set
'    End Property

'    Public WriteOnly Property _YearId() As Integer
'        Set(ByVal value As Integer)
'            mintYearId = value
'        End Set
'    End Property
'    'Sandeep [ 21 Aug 2010 ] -- End 

'    'Sandeep [ 29 NOV 2010 ] -- Start
'    Public Property _ReportCompanyIds() As String
'        Get
'            Return mstrReportCompanyIds
'        End Get
'        Set(ByVal value As String)
'            mstrReportCompanyIds = value
'        End Set
'    End Property

'    Public WriteOnly Property _ReportIDs() As String
'        Set(ByVal value As String)
'            mstrReportIDs = value
'        End Set
'    End Property
'    'Sandeep [ 29 NOV 2010 ] -- End

'    'S.SANDEEP [ 30 May 2011 ] -- START
'    'ISSUE : FINCA REQ.
'    ''' <summary>
'    ''' Purpose: Get or Set Creation Date
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Creation_Date() As Date
'        Get
'            Return mdtCreationDate
'        End Get
'        Set(ByVal value As Date)
'            mdtCreationDate = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set Account Locking
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _IsAcc_Locked() As Boolean
'        Get
'            Return mblnIsLocked
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsLocked = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set Time User Account Locked
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Locked_Time() As DateTime
'        Get
'            Return mdtLockedTime
'        End Get
'        Set(ByVal value As DateTime)
'            mdtLockedTime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set Time Duration User Account Locked
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _LockedDuration() As DateTime
'        Get
'            Return mdtLockedDuration
'        End Get
'        Set(ByVal value As DateTime)
'            mdtLockedDuration = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set Created By User
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _CreatedById() As Integer
'        Get
'            Return mintCreatedById
'        End Get
'        Set(ByVal value As Integer)
'            mintCreatedById = value
'        End Set
'    End Property
'    'S.SANDEEP [ 30 May 2011 ] -- END 

'    'S.SANDEEP [ 12 OCT 2011 ] -- START
'    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'    ''' <summary>
'    ''' Purpose: Get or Set firstname
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Firstname() As String
'        Get
'            Return mstrFirstname
'        End Get
'        Set(ByVal value As String)
'            mstrFirstname = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set lastname
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Lastname() As String
'        Get
'            Return mstrLastname
'        End Get
'        Set(ByVal value As String)
'            mstrLastname = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set address1
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Address1() As String
'        Get
'            Return mstrAddress1
'        End Get
'        Set(ByVal value As String)
'            mstrAddress1 = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set address2
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Address2() As String
'        Get
'            Return mstrAddress2
'        End Get
'        Set(ByVal value As String)
'            mstrAddress2 = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set phone
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Phone() As String
'        Get
'            Return mstrPhone
'        End Get
'        Set(ByVal value As String)
'            mstrPhone = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set email
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Email() As String
'        Get
'            Return mstrEmail
'        End Get
'        Set(ByVal value As String)
'            mstrEmail = value
'        End Set
'    End Property
'    'S.SANDEEP [ 12 OCT 2011 ] -- END 


'    'Pinkal (12-Oct-2011) -- Start
'    'Enhancement : TRA Changes

'    ''' <summary>
'    ''' Purpose: Get or Set isaduser
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _IsAduser() As Boolean
'        Get
'            Return mblnIsADuser
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsADuser = value
'        End Set
'    End Property

'    'Pinkal (12-Oct-2011) -- End


'    'Pinkal (21-Jun-2012) -- Start
'    'Enhancement : TRA Changes

'    ''' <summary>
'    ''' Purpose: Get or Set isManager
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _IsManager() As Boolean
'        Get
'            Return mblnIsManager
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsManager = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set UserDomain
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _UserDomain() As String
'        Get
'            Return mstrUserdomain
'        End Get
'        Set(ByVal value As String)
'            mstrUserdomain = value
'        End Set
'    End Property

'    'Pinkal (21-Jun-2012) -- End

'    'Sohail (04 Jul 2012) -- Start
'    ''' <summary>
'    ''' Purpose: Get or Set relogin_date
'    ''' Modify By: Sohail
'    ''' </summary>
'    Public Property _Relogin_Date() As Date
'        Get
'            Return mdtRelogin_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtRelogin_Date = Value
'        End Set
'    End Property
'    'Sohail (04 Jul 2012) -- End


'    'S.SANDEEP [ 09 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public ReadOnly Property _ShowChangePasswdfrm() As Boolean
'        Get
'            Return blnShowChangePasswdfrm
'        End Get
'    End Property

'    Public ReadOnly Property _RetUserId() As Integer
'        Get
'            Return intRetUserId
'        End Get
'    End Property
'    'S.SANDEEP [ 09 NOV 2012 ] -- END

'    'S.SANDEEP [ 01 DEC 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Property _EmployeeUnkid() As Integer
'        Get
'            Return mintEmployeeUnkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeUnkid = value
'        End Set
'    End Property

'    Public Property _EmployeeCompanyUnkid() As Integer
'        Get
'            Return mintEmployeeCompanyUnkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeCompanyUnkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set suspended_from_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Suspended_From_Date() As Date
'        Get
'            Return mdtSuspended_From_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtSuspended_From_Date = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set suspended_to_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Suspended_To_Date() As Date
'        Get
'            Return mdtSuspended_To_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtSuspended_To_Date = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set probation_from_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Probation_From_Date() As Date
'        Get
'            Return mdtProbation_From_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtProbation_From_Date = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set probation_to_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Probation_To_Date() As Date
'        Get
'            Return mdtProbation_To_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtProbation_To_Date = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set empl_enddate
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Empl_Enddate() As Date
'        Get
'            Return mdtEmpl_Enddate
'        End Get
'        Set(ByVal value As Date)
'            mdtEmpl_Enddate = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set termination_from_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Termination_Date() As Date
'        Get
'            Return mdtTermination_From_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtTermination_From_Date = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set termination_to_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Retirement_Date() As Date
'        Get
'            Return mdtTermination_To_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtTermination_To_Date = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set appointeddate
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Appointeddate() As Date
'        Get
'            Return mdtAppointeddate
'        End Get
'        Set(ByVal value As Date)
'            mdtAppointeddate = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reinstatement_date
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Reinstatement_Date() As Date
'        Get
'            Return mdtReinstatement_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtReinstatement_Date = Value
'        End Set
'    End Property
'    'S.SANDEEP [ 01 DEC 2012 ] -- END


'    'S.SANDEEP [ 31 DEC 2013 ] -- START
'    Public WriteOnly Property _DataOpr() As clsDataOperation
'        Set(ByVal value As clsDataOperation)
'            iObjDataOpr = value
'        End Set
'    End Property
'    'S.SANDEEP [ 31 DEC 2013 ] -- END


'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [ 04 JAN 2013 ] -- START
'        'objDataOperation = New clsDataOperation
'        If iObjDataOpr Is Nothing Then
'            objDataOperation = New clsDataOperation
'        Else
'            objDataOperation = iObjDataOpr
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 04 JAN 2013 ] -- END

'        Try

'            'S.SANDEEP [ 30 May 2011 ] -- START
'            'ISSUE : FINCA REQ.
'            'strQ = "SELECT " & _
'            '              "  userunkid " & _
'            '              ", username " & _
'            '              ", password " & _
'            '              ", ispasswordexpire " & _
'            '              ", roleunkid " & _
'            '              ", isrighttoleft " & _
'            '              ", exp_days " & _
'            '              ", languageunkid " & _
'            '              ", isactive " & _
'            '              "FROM hrmsConfiguration..cfuser_master " & _
'            '             "WHERE userunkid = @userunkid "


'            'S.SANDEEP [ 12 OCT 2011 ] -- START
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            'strQ = "SELECT " & _
'            '  "  userunkid " & _
'            '  ", username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive " & _
'            '  " ,ISNULL(creationdate,'') AS creationdate " & _
'            '  " ,ISNULL(islocked,0) AS islocked " & _
'            '  " ,lockedtime " & _
'            '  " ,lockedduration " & _
'            '  " ,ISNULL(createdbyunkid,0) AS createdbyunkid " & _
'            '  "FROM hrmsConfiguration..cfuser_master " & _
'            ' "WHERE userunkid = @userunkid "



'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes




'            'strQ = "SELECT " & _
'            '  "  userunkid " & _
'            '  ", username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive " & _
'            '        " ,ISNULL(creationdate,'') AS creationdate " & _
'            '        " ,ISNULL(islocked,0) AS islocked " & _
'            '        " ,lockedtime " & _
'            '        " ,lockedduration " & _
'            '        " ,ISNULL(createdbyunkid,0) AS createdbyunkid " & _
'            '          ", ISNULL(firstname,'') AS firstname " & _
'            '          ", ISNULL(lastname,'') AS lastname " & _
'            '          ", ISNULL(address1,'') AS address1 " & _
'            '          ", ISNULL(address2,'') AS address2 " & _
'            '          ", ISNULL(phone,'') AS phone " & _
'            '          ", ISNULL(email,'') AS email " & _
'            '        "FROM hrmsConfiguration..cfuser_master " & _
'            ' "WHERE userunkid = @userunkid "



'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            'strQ = "SELECT " & _
'            '  "  userunkid " & _
'            '  ", username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive " & _
'            '    " ,ISNULL(creationdate,'') AS creationdate " & _
'            '    " ,ISNULL(islocked,0) AS islocked " & _
'            '    " ,lockedtime " & _
'            '    " ,lockedduration " & _
'            '    " ,ISNULL(createdbyunkid,0) AS createdbyunkid " & _
'            '      ", ISNULL(firstname,'') AS firstname " & _
'            '      ", ISNULL(lastname,'') AS lastname " & _
'            '      ", ISNULL(address1,'') AS address1 " & _
'            '      ", ISNULL(address2,'') AS address2 " & _
'            '      ", ISNULL(phone,'') AS phone " & _
'            '      ", ISNULL(email,'') AS email " & _
'            '     ", ISNULL(isaduser,0) as  isaduser " & _
'            '    "FROM hrmsConfiguration..cfuser_master " & _
'            ' "WHERE userunkid = @userunkid "

'            strQ = "SELECT " & _
'              "  userunkid " & _
'              ", username " & _
'              ", password " & _
'              ", ispasswordexpire " & _
'              ", roleunkid " & _
'              ", isrighttoleft " & _
'              ", exp_days " & _
'              ", languageunkid " & _
'              ", isactive " & _
'                    " ,ISNULL(creationdate,'') AS creationdate " & _
'                    " ,ISNULL(islocked,0) AS islocked " & _
'                    " ,lockedtime " & _
'                    " ,lockedduration " & _
'                    " ,ISNULL(createdbyunkid,0) AS createdbyunkid " & _
'                      ", ISNULL(firstname,'') AS firstname " & _
'                      ", ISNULL(lastname,'') AS lastname " & _
'                      ", ISNULL(address1,'') AS address1 " & _
'                      ", ISNULL(address2,'') AS address2 " & _
'                      ", ISNULL(phone,'') AS phone " & _
'                      ", ISNULL(email,'') AS email " & _
'                     ", ISNULL(isaduser,0) as  isaduser " & _
'              ", ISNULL(ismanager,0) as  ismanager " & _
'              ", ISNULL(userdomain,'') as  userdomain " & _
'                        ", relogin_date " & _
'                      ", employeeunkid " & _
'                      ", companyunkid " & _
'                      ", suspended_from_date " & _
'                      ", suspended_to_date " & _
'                      ", probation_from_date " & _
'                      ", probation_to_date " & _
'                      ", empl_enddate " & _
'                      ", termination_from_date " & _
'                      ", termination_to_date " & _
'                      ", appointeddate " & _
'                      ", reinstatement_date " & _
'                    "FROM hrmsConfiguration..cfuser_master " & _
'                  "WHERE userunkid = @userunkid " 'Sohail (04 Jul 2012) - [relogin_date]'S.SANDEEP [ 01 DEC 2012 (employeeunkid,companyunkid)]
'            'Pinkal (21-Jun-2012) -- End



'            'Pinkal (12-Oct-2011) -- End


'            'S.SANDEEP [ 12 OCT 2011 ] -- END 


'            'S.SANDEEP [ 30 May 2011 ] -- END 

'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                mstrUsername = dtRow.Item("username").ToString
'                'Sandeep [ 21 Aug 2010 ] -- Start
'                'mstrPassword = clsSecurity.Decrypt(dtRow.Item("password"), "eZee").ToString
'                mstrPassword = clsSecurity.Decrypt(dtRow.Item("password"), "ezee").ToString
'                'Sandeep [ 21 Aug 2010 ] -- End 
'                mblnIspasswordexpire = CBool(dtRow.Item("ispasswordexpire"))
'                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
'                mblnIsrighttoleft = CBool(dtRow.Item("isrighttoleft"))
'                mintExp_Days = CInt(dtRow.Item("exp_days"))
'                mintLanguageunkid = CInt(dtRow.Item("languageunkid"))
'                mblnIsactive = CBool(dtRow.Item("isactive"))


'                'S.SANDEEP [ 30 May 2011 ] -- START
'                'ISSUE : FINCA REQ.
'                mblnIsLocked = CBool(dtRow.Item("islocked"))
'                If dtRow.Item("creationdate").ToString.Trim.Length > 0 Then
'                    mdtCreationDate = dtRow.Item("creationdate")
'                End If
'                If dtRow.Item("lockedtime").ToString.Trim.Length > 0 Then
'                    mdtLockedTime = dtRow.Item("lockedtime")
'                End If
'                If dtRow.Item("lockedduration").ToString.Trim.Length > 0 Then
'                    mdtLockedDuration = dtRow.Item("lockedduration")
'                End If
'                mintCreatedById = CInt(dtRow.Item("createdbyunkid"))
'                'S.SANDEEP [ 30 May 2011 ] -- END 


'                'S.SANDEEP [ 12 OCT 2011 ] -- START
'                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'                mstrFirstname = dtRow.Item("firstname").ToString
'                mstrLastname = dtRow.Item("lastname").ToString
'                mstrAddress1 = dtRow.Item("address1").ToString
'                mstrAddress2 = dtRow.Item("address2").ToString
'                mstrPhone = dtRow.Item("phone").ToString
'                mstrEmail = dtRow.Item("email").ToString
'                'S.SANDEEP [ 12 OCT 2011 ] -- END 


'                'Pinkal (12-Oct-2011) -- Start
'                'Enhancement : TRA Changes
'                mblnIsADuser = CBool(dtRow.Item("isaduser"))
'                'Pinkal (12-Oct-2011) -- End


'                'Pinkal (21-Jun-2012) -- Start
'                'Enhancement : TRA Changes
'                mblnIsManager = CBool(dtRow.Item("ismanager"))
'                mstrUserdomain = dtRow.Item("userdomain").ToString
'                'Pinkal (21-Jun-2012) -- End

'                'Sohail (04 Jul 2012) -- Start
'                'TRA - ENHANCEMENT
'                If IsDBNull(dtRow.Item("relogin_date")) = True Then
'                    mdtRelogin_Date = Nothing
'                Else
'                    mdtRelogin_Date = dtRow.Item("relogin_date")
'                End If
'                'Sohail (04 Jul 2012) -- End

'                'S.SANDEEP [ 01 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                mintEmployeeUnkid = CInt(dtRow.Item("employeeunkid"))
'                mintEmployeeCompanyUnkid = CInt(dtRow.Item("companyunkid"))
'                If IsDBNull(dtRow.Item("suspended_from_date")) = False Then
'                    mdtSuspended_From_Date = dtRow.Item("suspended_from_date")
'                Else
'                    mdtSuspended_From_Date = Nothing
'                End If
'                If IsDBNull(dtRow.Item("suspended_to_date")) = False Then
'                    mdtSuspended_To_Date = dtRow.Item("suspended_to_date")
'                Else
'                    mdtSuspended_To_Date = Nothing
'                End If
'                If IsDBNull(dtRow.Item("probation_from_date")) = False Then
'                    mdtProbation_From_Date = dtRow.Item("probation_from_date")
'                Else
'                    mdtProbation_From_Date = Nothing
'                End If
'                If IsDBNull(dtRow.Item("probation_to_date")) = False Then
'                    mdtProbation_To_Date = dtRow.Item("probation_to_date")
'                Else
'                    mdtProbation_To_Date = Nothing
'                End If
'                If IsDBNull(dtRow.Item("empl_enddate")) = False Then
'                    mdtEmpl_Enddate = dtRow.Item("empl_enddate")
'                Else
'                    mdtEmpl_Enddate = Nothing
'                End If
'                If IsDBNull(dtRow.Item("termination_from_date")) = False Then
'                    mdtTermination_From_Date = dtRow.Item("termination_from_date")
'                Else
'                    mdtTermination_From_Date = Nothing
'                End If
'                If IsDBNull(dtRow.Item("termination_to_date")) = False Then
'                    mdtTermination_To_Date = dtRow.Item("termination_to_date")
'                Else
'                    mdtTermination_To_Date = Nothing
'                End If
'                If IsDBNull(dtRow.Item("appointeddate")) = False Then
'                    mdtAppointeddate = dtRow.Item("appointeddate")
'                Else
'                    mdtAppointeddate = Nothing
'                End If
'                If IsDBNull(dtRow.Item("reinstatement_date")) = False Then
'                    mdtReinstatement_Date = dtRow.Item("reinstatement_date")
'                Else
'                    mdtReinstatement_Date = Nothing
'                End If
'                'S.SANDEEP [ 01 DEC 2012 ] -- END

'                Exit For
'            Next
'            'Sandeep [ 21 Aug 2010 ] -- Start
'            Call Privilege.setUserPrivilege(mintUserunkid)
'            'Sandeep [ 21 Aug 2010 ] -- End


'            'S.SANDEEP [ 30 May 2011 ] -- START
'            PWDOptions = New clsPassowdOptions
'            'S.SANDEEP [ 30 May 2011 ] -- END 

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 31 DEC 2013 ] -- START
'            'objDataOperation = Nothing
'            If iObjDataOpr Is Nothing Then
'                objDataOperation = Nothing
'            End If
'            'S.SANDEEP [ 31 DEC 2013 ] -- END
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    ''' Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnAdUser As Boolean = False) As DataSet
'    Public Function GetList(ByVal strTableName As String, _
'                            Optional ByVal blnOnlyActive As Boolean = True, _
'                            Optional ByVal blnAdUser As Boolean = False, _
'                            Optional ByVal blnOnlyEmp As Boolean = False) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try


'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'strQ = "SELECT " & _
'            '           "  hrmsConfiguration..cfuser_master.userunkid " & _
'            '           ", hrmsConfiguration..cfuser_master.username " & _
'            '           ", hrmsConfiguration..cfuser_master.password " & _
'            '           ", hrmsConfiguration..cfuser_master.ispasswordexpire " & _
'            '           ", hrmsConfiguration..cfuser_master.roleunkid " & _
'            '           ", hrmsConfiguration..cfuser_master.isrighttoleft " & _
'            '           ", hrmsConfiguration..cfuser_master.exp_days " & _
'            '           ", hrmsConfiguration..cfuser_master.languageunkid " & _
'            '           ", hrmsConfiguration..cfuser_master.isactive " & _
'            '           ", hrmsConfiguration..cfrole_master.name AS rolename " & _            
'            '       "FROM hrmsConfiguration..cfuser_master,hrmsConfiguration..cfrole_master " & _
'            '       "WHERE hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid "


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes

'            'strQ = "SELECT " & _
'            '                "  hrmsConfiguration..cfuser_master.userunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.username " & _
'            '                ", hrmsConfiguration..cfuser_master.password " & _
'            '                ", hrmsConfiguration..cfuser_master.ispasswordexpire " & _
'            '                ", hrmsConfiguration..cfuser_master.roleunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.isrighttoleft " & _
'            '                ", hrmsConfiguration..cfuser_master.exp_days " & _
'            '                ", hrmsConfiguration..cfuser_master.languageunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.isactive " & _
'            '                ", hrmsConfiguration..cfrole_master.name AS rolename " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') AS firstname " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS lastname " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.address1,'') AS address1 " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.address2,'') AS address2 " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.phone,'') AS phone " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.email,'') AS email " & _
'            '           " FROM hrmsConfiguration..cfuser_master,hrmsConfiguration..cfrole_master " & _
'            '           " WHERE hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid "


'            'S.SANDEEP [ 21 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'            'strQ = "SELECT " & _
'            '                "  hrmsConfiguration..cfuser_master.userunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.username " & _
'            '                ", hrmsConfiguration..cfuser_master.password " & _
'            '                ", hrmsConfiguration..cfuser_master.ispasswordexpire " & _
'            '                ", hrmsConfiguration..cfuser_master.roleunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.isrighttoleft " & _
'            '                ", hrmsConfiguration..cfuser_master.exp_days " & _
'            '                ", hrmsConfiguration..cfuser_master.languageunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.isactive " & _
'            '                ", hrmsConfiguration..cfrole_master.name AS rolename " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') AS firstname " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS lastname " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.address1,'') AS address1 " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.address2,'') AS address2 " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.phone,'') AS phone " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.email,'') AS email " & _
'            '               ", ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) AS isaduser " & _
'            '          " FROM hrmsConfiguration..cfuser_master " & _
'            '          " LEFT JOIN hrmsConfiguration..cfrole_master on hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid " & _
'            '          " WHERE  1 = 1"


'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            'strQ = "SELECT " & _
'            '                "  hrmsConfiguration..cfuser_master.userunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.username " & _
'            '                ", hrmsConfiguration..cfuser_master.password " & _
'            '                ", hrmsConfiguration..cfuser_master.ispasswordexpire " & _
'            '                ", hrmsConfiguration..cfuser_master.roleunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.isrighttoleft " & _
'            '                ", hrmsConfiguration..cfuser_master.exp_days " & _
'            '                ", hrmsConfiguration..cfuser_master.languageunkid " & _
'            '                ", hrmsConfiguration..cfuser_master.isactive " & _
'            '                ", hrmsConfiguration..cfrole_master.name AS rolename " & _
'            '       ", hrmsConfiguration..cfuser_master.roleunkid " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') AS firstname " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS lastname " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.address1,'') AS address1 " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.address2,'') AS address2 " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.phone,'') AS phone " & _
'            '           ", ISNULL(hrmsConfiguration..cfuser_master.email,'') AS email " & _
'            '               ", ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) AS isaduser " & _
'            '          " FROM hrmsConfiguration..cfuser_master " & _
'            '          " LEFT JOIN hrmsConfiguration..cfrole_master on hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid " & _
'            '          " WHERE  1 = 1"

'            strQ = "SELECT " & _
'                            "  hrmsConfiguration..cfuser_master.userunkid " & _
'                            ", hrmsConfiguration..cfuser_master.username " & _
'                            ", hrmsConfiguration..cfuser_master.password " & _
'                            ", hrmsConfiguration..cfuser_master.ispasswordexpire " & _
'                            ", hrmsConfiguration..cfuser_master.roleunkid " & _
'                            ", hrmsConfiguration..cfuser_master.isrighttoleft " & _
'                            ", hrmsConfiguration..cfuser_master.exp_days " & _
'                            ", hrmsConfiguration..cfuser_master.languageunkid " & _
'                            ", hrmsConfiguration..cfuser_master.isactive " & _
'                            ", hrmsConfiguration..cfrole_master.name AS rolename " & _
'                   ", hrmsConfiguration..cfuser_master.roleunkid " & _
'                       ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') AS firstname " & _
'                       ", ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS lastname " & _
'                       ", ISNULL(hrmsConfiguration..cfuser_master.address1,'') AS address1 " & _
'                       ", ISNULL(hrmsConfiguration..cfuser_master.address2,'') AS address2 " & _
'                       ", ISNULL(hrmsConfiguration..cfuser_master.phone,'') AS phone " & _
'                       ", ISNULL(hrmsConfiguration..cfuser_master.email,'') AS email " & _
'                           ", ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) AS isaduser " & _
'                        ", ISNULL(hrmsConfiguration..cfuser_master.ismanager,0) AS ismanager " & _
'                        ", ISNULL(hrmsConfiguration..cfuser_master.userdomain,'') AS userdomain " & _
'                        ", hrmsConfiguration..cfuser_master.relogin_date " & _
'                        ", hrmsConfiguration..cfuser_master.employeeunkid " & _
'                        ", hrmsConfiguration..cfuser_master.companyunkid " & _
'                        ", hrmsConfiguration..cfuser_master.suspended_from_date " & _
'                        ", hrmsConfiguration..cfuser_master.suspended_to_date " & _
'                        ", hrmsConfiguration..cfuser_master.probation_from_date " & _
'                        ", hrmsConfiguration..cfuser_master.probation_to_date " & _
'                        ", hrmsConfiguration..cfuser_master.empl_enddate " & _
'                        ", hrmsConfiguration..cfuser_master.termination_from_date " & _
'                        ", hrmsConfiguration..cfuser_master.termination_to_date " & _
'                        ", hrmsConfiguration..cfuser_master.appointeddate " & _
'                        ", hrmsConfiguration..cfuser_master.reinstatement_date " & _
'                        ", hrmsConfiguration..cfuser_master.lockedduration " & _
'                      " FROM hrmsConfiguration..cfuser_master " & _
'                      " LEFT JOIN hrmsConfiguration..cfrole_master on hrmsConfiguration..cfuser_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid " & _
'                    " WHERE  1 = 1 " 'S.SANDEEP [ 01 DEC 2012 (employeeunkid,companyunkid)]


'            'Pinkal (28-Feb-2014) -- Start [", hrmsConfiguration..cfuser_master.lockedduration " & _]


'            'Pinkal (21-Jun-2012) -- End


'            'S.SANDEEP [ 21 MAY 2012 ] -- END

'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            If blnOnlyActive Then
'                strQ &= " And  hrmsConfiguration..cfuser_master.isactive = 1 "
'            End If

'            If blnAdUser Then
'                strQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or userunkid = 1) "
'            Else
'                strQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or userunkid = 1) "
'            End If

'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If blnOnlyEmp Then
'                strQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR userunkid = 1) "
'                'S.SANDEEP [ 06 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'            Else
'                strQ &= " AND ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0 "
'                'S.SANDEEP [ 06 DEC 2012 ] -- END
'            End If
'            'S.SANDEEP [ 01 DEC 2012 ] -- END


'            'If blnAdUser = False And User._Object._Userunkid <> 1 Then

'            'Pinkal (12-Oct-2011) -- End

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (cfuser_master) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mstrUsername) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This User is already defined. Please define new User.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [ 31 DEC 2013 ] -- START
'        'objDataOperation = New clsDataOperation
'        ''Sandeep [ 21 Aug 2010 ] -- Start
'        'objDataOperation.BindTransaction()
'        ''Sandeep [ 21 Aug 2010 ] -- End 
'        If iObjDataOpr Is Nothing Then
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'        Else
'            objDataOperation = iObjDataOpr
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 31 DEC 2013 ] -- END



'        Try
'            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
'            'Sandeep [ 21 Aug 2010 ] -- Start
'            'objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrPassword, "eZee").ToString)
'            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrPassword, "ezee").ToString)
'            'Sandeep [ 21 Aug 2010 ] -- End
'            objDataOperation.AddParameter("@ispasswordexpire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspasswordexpire.ToString)
'            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
'            objDataOperation.AddParameter("@isrighttoleft", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsrighttoleft.ToString)
'            objDataOperation.AddParameter("@exp_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExp_Days.ToString)
'            objDataOperation.AddParameter("@languageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageunkid.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


'            'S.SANDEEP [ 30 May 2011 ] -- START
'            'ISSUE : FINCA REQ.
'            objDataOperation.AddParameter("@creationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreationDate)
'            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
'            If mdtLockedTime = Nothing Then
'                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedTime)
'            End If

'            If mdtLockedDuration = Nothing Then
'                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedDuration)
'            End If

'            objDataOperation.AddParameter("@createdbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreatedById)


'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
'            objDataOperation.AddParameter("@lastname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLastname.ToString)
'            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
'            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
'            objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone.ToString)
'            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
'            'S.SANDEEP [ 07 NOV 2011 ] -- END


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            objDataOperation.AddParameter("@isaduser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsADuser.ToString)
'            'Pinkal (12-Oct-2011) -- End


'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes
'            objDataOperation.AddParameter("@ismanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsManager.ToString)
'            objDataOperation.AddParameter("@userdomain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUserdomain.ToString())
'            'Pinkal (21-Jun-2012) -- End

'            'Sohail (04 Jul 2012) -- Start
'            'TRA - ENHANCEMENT
'            If mdtRelogin_Date <> Nothing Then
'                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRelogin_Date)
'            Else
'                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            'Sohail (04 Jul 2012) -- End

'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
'            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeCompanyUnkid.ToString)
'            If mdtSuspended_From_Date <> Nothing Then
'                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_From_Date)
'            Else
'                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtSuspended_To_Date <> Nothing Then
'                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_To_Date)
'            Else
'                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtProbation_From_Date <> Nothing Then
'                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_From_Date)
'            Else
'                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtProbation_To_Date <> Nothing Then
'                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_To_Date)
'            Else
'                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtEmpl_Enddate <> Nothing Then
'                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmpl_Enddate)
'            Else
'                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtTermination_From_Date <> Nothing Then
'                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_From_Date)
'            Else
'                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtTermination_To_Date <> Nothing Then
'                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_To_Date)
'            Else
'                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtAppointeddate <> Nothing Then
'                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppointeddate)
'            Else
'                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtReinstatement_Date <> Nothing Then
'                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatement_Date)
'            Else
'                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            'S.SANDEEP [ 01 DEC 2012 ] -- END


'            'strQ = "INSERT INTO hrmsConfiguration..cfuser_master ( " & _
'            '  "  username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive" & _
'            '") VALUES (" & _
'            '  "  @username " & _
'            '  ", @password " & _
'            '  ", @ispasswordexpire " & _
'            '  ", @roleunkid " & _
'            '  ", @isrighttoleft " & _
'            '  ", @exp_days " & _
'            '  ", @languageunkid " & _
'            '  ", @isactive" & _
'            '"); SELECT @@identity"


'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'strQ = "INSERT INTO hrmsconfiguration..cfuser_master ( " & _
'            '  "  username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive" & _
'            '  ", creationdate " & _
'            '  ", islocked " & _
'            '  ", lockedtime " & _
'            '  ", lockedduration " & _
'            '  ", createdbyunkid " & _
'            '") VALUES (" & _
'            '  "  @username " & _
'            '  ", @password " & _
'            '  ", @ispasswordexpire " & _
'            '  ", @roleunkid " & _
'            '  ", @isrighttoleft " & _
'            '  ", @exp_days " & _
'            '  ", @languageunkid " & _
'            '  ", @isactive" & _
'            '  ", @creationdate " & _
'            '  ", @islocked " & _
'            '  ", @lockedtime " & _
'            '  ", @lockedduration " & _
'            '  ", @createdbyunkid " & _
'            '"); SELECT @@identity"




'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes


'            'strQ = "INSERT INTO cfuser_master ( " & _
'            '  "  username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive" & _
'            '          ", creationdate " & _
'            '          ", islocked " & _
'            '          ", lockedtime " & _
'            '          ", lockedduration " & _
'            '          ", createdbyunkid " & _
'            '            ", firstname " & _
'            '            ", lastname " & _
'            '            ", address1 " & _
'            '            ", address2 " & _
'            '            ", phone " & _
'            '            ", email" & _
'            '") VALUES (" & _
'            '  "  @username " & _
'            '  ", @password " & _
'            '  ", @ispasswordexpire " & _
'            '  ", @roleunkid " & _
'            '  ", @isrighttoleft " & _
'            '  ", @exp_days " & _
'            '  ", @languageunkid " & _
'            '  ", @isactive" & _
'            '          ", @creationdate " & _
'            '          ", @islocked " & _
'            '          ", @lockedtime " & _
'            '          ", @lockedduration " & _
'            '          ", @createdbyunkid " & _
'            '            ", @firstname " & _
'            '            ", @lastname " & _
'            '            ", @address1 " & _
'            '            ", @address2 " & _
'            '            ", @phone " & _
'            '            ", @email" & _
'            '"); SELECT @@identity"



'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            'strQ = "INSERT INTO cfuser_master ( " & _
'            '  "  username " & _
'            '  ", password " & _
'            '  ", ispasswordexpire " & _
'            '  ", roleunkid " & _
'            '  ", isrighttoleft " & _
'            '  ", exp_days " & _
'            '  ", languageunkid " & _
'            '  ", isactive" & _
'            '  ", creationdate " & _
'            '  ", islocked " & _
'            '  ", lockedtime " & _
'            '  ", lockedduration " & _
'            '  ", createdbyunkid " & _
'            '    ", firstname " & _
'            '    ", lastname " & _
'            '    ", address1 " & _
'            '    ", address2 " & _
'            '    ", phone " & _
'            '    ", email" & _
'            '  ", isaduser " & _
'            '") VALUES (" & _
'            '  "  @username " & _
'            '  ", @password " & _
'            '  ", @ispasswordexpire " & _
'            '  ", @roleunkid " & _
'            '  ", @isrighttoleft " & _
'            '  ", @exp_days " & _
'            '  ", @languageunkid " & _
'            '  ", @isactive" & _
'            '  ", @creationdate " & _
'            '  ", @islocked " & _
'            '  ", @lockedtime " & _
'            '  ", @lockedduration " & _
'            '  ", @createdbyunkid " & _
'            '    ", @firstname " & _
'            '    ", @lastname " & _
'            '    ", @address1 " & _
'            '    ", @address2 " & _
'            '    ", @phone " & _
'            '    ", @email" & _
'            '  ", @isaduser " & _
'            '"); SELECT @@identity"



'            strQ = "INSERT INTO hrmsConfiguration..cfuser_master ( " & _
'              "  username " & _
'              ", password " & _
'              ", ispasswordexpire " & _
'              ", roleunkid " & _
'              ", isrighttoleft " & _
'              ", exp_days " & _
'              ", languageunkid " & _
'              ", isactive" & _
'                      ", creationdate " & _
'                      ", islocked " & _
'                      ", lockedtime " & _
'                      ", lockedduration " & _
'                      ", createdbyunkid " & _
'                        ", firstname " & _
'                        ", lastname " & _
'                        ", address1 " & _
'                        ", address2 " & _
'                        ", phone " & _
'                        ", email" & _
'                      ", isaduser " & _
'             ", ismanager " & _
'             ", userdomain " & _
'             ", relogin_date" & _
'                        ", employeeunkid " & _
'                        ", companyunkid " & _
'                        ", suspended_from_date " & _
'                        ", suspended_to_date " & _
'                        ", probation_from_date " & _
'                        ", probation_to_date " & _
'                        ", empl_enddate " & _
'                        ", termination_from_date " & _
'                        ", termination_to_date " & _
'                        ", appointeddate " & _
'                        ", reinstatement_date" & _
'            ") VALUES (" & _
'              "  @username " & _
'              ", @password " & _
'              ", @ispasswordexpire " & _
'              ", @roleunkid " & _
'              ", @isrighttoleft " & _
'              ", @exp_days " & _
'              ", @languageunkid " & _
'              ", @isactive" & _
'                      ", @creationdate " & _
'                      ", @islocked " & _
'                      ", @lockedtime " & _
'                      ", @lockedduration " & _
'                      ", @createdbyunkid " & _
'                        ", @firstname " & _
'                        ", @lastname " & _
'                        ", @address1 " & _
'                        ", @address2 " & _
'                        ", @phone " & _
'                        ", @email" & _
'                      ", @isaduser " & _
'             ",@ismanager " & _
'             ",@userdomain " & _
'             ", @relogin_date" & _
'                        ", @employeeunkid " & _
'                        ", @companyunkid " & _
'                        ", @suspended_from_date " & _
'                        ", @suspended_to_date " & _
'                        ", @probation_from_date " & _
'                        ", @probation_to_date " & _
'                        ", @empl_enddate " & _
'                        ", @termination_from_date " & _
'                        ", @termination_to_date " & _
'                        ", @appointeddate " & _
'                        ", @reinstatement_date" & _
'                     "); SELECT @@identity" 'S.SANDEEP [ 01 DEC 2012 (employeeunkid,companyunkid)]



'            'Pinkal (21-Jun-2012) -- End


'            'Pinkal (12-Oct-2011) -- End

'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            'S.SANDEEP [ 30 May 2011 ] -- END 



'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintUserunkid = dsList.Tables(0).Rows(0).Item(0)
'            'Sandeep [ 21 Aug 2010 ] -- Start


'            'S.SANDEEP [ 08 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If mstrAssignPrivilegeIDs <> "" Then
'            '    Dim strPrivilegeUnkid() As String = Split(mstrAssignPrivilegeIDs, ",")
'            '    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'            '        strQ = "INSERT INTO hrmsconfiguration..cfuser_privilege(userunkid, privilegeunkid) " & _
'            '                         "VALUES(@userunkid, @privilegeunkid)"
'            '        objDataOperation.ClearParameters()
'            '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            '        objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))
'            '        objDataOperation.ExecNonQuery(strQ)
'            '        If objDataOperation.ErrorMessage <> "" Then
'            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'            '            Throw exForce
'            '        End If
'            '    Next
'            'End If
'            'If mstrAssignCompanyPrivilegeIDs <> "" Then
'            '    Dim strPrivilegeUnkid() As String = Split(mstrAssignCompanyPrivilegeIDs, ",")
'            '    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'            '        'Sandeep [ 04 NOV 2010 ] -- Start
'            '        strQ = "INSERT INTO hrmsconfiguration..cfcompanyaccess_privilege (userunkid,yearunkid) " & _
'            '               "VALUES(@userunkid,@yearunkid) "
'            '        'strQ = "INSERT INTO cfcompanyaccess_privilege (userunkid,yearunkid) " & _
'            '        '       "SELECT @userunkid,@yearunkid FROM hrmsConfiguration..cfcompanyaccess_privilege WHERE NOT EXISTS (SELECT * FROM hrmsConfiguration..cfcompanyaccess_privilege WHERE yearunkid = @yearunkid) "
'            '        'Sandeep [ 04 NOV 2010 ] -- End 
'            '        objDataOperation.ClearParameters()
'            '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))
'            '        objDataOperation.ExecNonQuery(strQ)
'            '        If objDataOperation.ErrorMessage <> "" Then
'            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'            '            Throw exForce
'            '        End If
'            '    Next
'            'End If

'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Dim blnIsInserted As Boolean = False
'            'S.SANDEEP [ 01 DEC 2012 ] -- END

'            If mstrAssignPrivilegeIDs <> "" Then
'                Dim strPrivilegeUnkid() As String = Split(mstrAssignPrivilegeIDs, ",")
'                Dim dsTrans As New DataSet
'                For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                    objDataOperation.ClearParameters()
'                    If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = '" & mintUserunkid & "' AND privilegeunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then
'                        strQ = "INSERT INTO hrmsconfiguration..cfuser_privilege(userunkid, privilegeunkid) " & _
'                                   " VALUES(@userunkid, @privilegeunkid); SELECT @@identity "

'                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                        objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

'                        dsTrans = objDataOperation.ExecQuery(strQ, "List")

'                        If objDataOperation.ErrorMessage <> "" Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                        End If

'                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfuser_privilege", "userprivilegeunkid", dsTrans.Tables(0).Rows(0)(0), 1, 1, True) = False Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                            'S.SANDEEP [ 01 DEC 2012 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                        Else
'                            blnIsInserted = True
'                            'S.SANDEEP [ 01 DEC 2012 ] -- END
'                        End If

'                    End If
'                Next
'            End If
'            If mstrAssignCompanyPrivilegeIDs <> "" Then
'                Dim strPrivilegeUnkid() As String = Split(mstrAssignCompanyPrivilegeIDs, ",")
'                Dim dsTrans As New DataSet
'                For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                    objDataOperation.ClearParameters()
'                    If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = '" & mintUserunkid & "' AND yearunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then
'                        strQ = "INSERT INTO hrmsconfiguration..cfcompanyaccess_privilege (userunkid,yearunkid) " & _
'                                   "VALUES(@userunkid,@yearunkid); SELECT @@identity "

'                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

'                        dsTrans = objDataOperation.ExecQuery(strQ, "List")

'                        If objDataOperation.ErrorMessage <> "" Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                        End If

'                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfcompanyaccess_privilege", "companyacessunkid", dsTrans.Tables(0).Rows(0)(0), 1, 1, True) = False Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                            'S.SANDEEP [ 01 DEC 2012 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                        Else
'                            blnIsInserted = True
'                            'S.SANDEEP [ 01 DEC 2012 ] -- END
'                        End If

'                    End If
'                Next
'            End If
'            'S.SANDEEP [ 08 NOV 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If mstrAssignAccessPrivilegeIDs <> "" Then
'            '    Dim strPrivilegeUnkid() As String = Split(mstrAssignAccessPrivilegeIDs, ",")
'            '    Dim strAccessLevel() As String = Split(mstrUserAccessLevels, ",")
'            '    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'            '        strQ = "INSERT INTO hrmsconfiguration..cfuseraccess_privilege (userunkid,jobunkid,job_level,companyunkid,yearunkid) " & _
'            '                        "VALUES(@userunkid,@jobunkid,@job_level,@companyunkid,@yearunkid) "

'            '        objDataOperation.ClearParameters()
'            '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            '        objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))
'            '        objDataOperation.AddParameter("@job_level", SqlDbType.Int, eZeeDataType.INT_SIZE, strAccessLevel(iCnt))
'            '        objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid)
'            '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)

'            '        objDataOperation.ExecNonQuery(strQ)

'            '        If objDataOperation.ErrorMessage <> "" Then
'            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'            '            Throw exForce
'            '        End If
'            '    Next
'            'End If
'            'S.SANDEEP [ 04 FEB 2012 ] -- END


'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If blnIsInserted = False Then
'                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "", "", -1, 1, 0, True) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'            End If
'            'S.SANDEEP [ 01 DEC 2012 ] -- END


'            'S.SANDEEP [ 31 DEC 2013 ] -- START
'            'objDataOperation.ReleaseTransaction(True)
'            If iObjDataOpr Is Nothing Then
'                objDataOperation.ReleaseTransaction(True)
'            End If
'            'S.SANDEEP [ 31 DEC 2013 ] -- END

'            'Sandeep [ 21 Aug 2010 ] -- End
'            Return True
'        Catch ex As Exception
'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objDataOperation.ReleaseTransaction(False)
'            'Sandeep [ 21 Aug 2010 ] -- End
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 31 DEC 2013 ] -- START
'            'objDataOperation = Nothing
'            If iObjDataOpr Is Nothing Then
'                objDataOperation = Nothing
'            End If
'            'S.SANDEEP [ 31 DEC 2013 ] -- END
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (cfuser_master) </purpose>
'    ''' S.SANDEEP [ 24 JUNE 2011 ] -- START
'    '''ISSUE : DELETION OF PRIVILEGE WHILE UNLOCKING USER
'    '''Public Function Update() As Boolean
'    Public Function Update(Optional ByVal blnIsFromUnlock As Boolean = False) As Boolean
'        'S.SANDEEP [ 24 JUNE 2011 ] -- END 
'        If isExist(mstrUsername, mintUserunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This User is already defined. Please define new User.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'S.SANDEEP [ 31 DEC 2013 ] -- START
'        objDataOperation = New clsDataOperation
'        ''Sandeep [ 21 Aug 2010 ] -- Start
'        'objDataOperation.BindTransaction()
'        ''Sandeep [ 21 Aug 2010 ] -- End 

'        If iObjDataOpr Is Nothing Then
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'        Else
'            objDataOperation = iObjDataOpr
'            objDataOperation.ClearParameters()
'        End If
'        'S.SANDEEP [ 31 DEC 2013 ] -- END

'        Try
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
'            'Sandeep [ 21 Aug 2010 ] -- Start
'            'objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrPassword, "eZee").ToString)
'            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrPassword, "ezee").ToString)
'            'Sandeep [ 21 Aug 2010 ] -- End
'            objDataOperation.AddParameter("@ispasswordexpire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspasswordexpire.ToString)
'            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
'            objDataOperation.AddParameter("@isrighttoleft", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsrighttoleft.ToString)
'            objDataOperation.AddParameter("@exp_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExp_Days.ToString)
'            objDataOperation.AddParameter("@languageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguageunkid.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
'            'S.SANDEEP [ 30 May 2011 ] -- START
'            'ISSUE : FINCA REQ.
'            objDataOperation.AddParameter("@creationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreationDate)
'            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
'            If mdtLockedTime = Nothing Then
'                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedTime)
'            End If
'            If mdtLockedDuration = Nothing Then
'                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedDuration)
'            End If

'            objDataOperation.AddParameter("@createdbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreatedById)



'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
'            objDataOperation.AddParameter("@lastname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLastname.ToString)
'            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
'            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
'            objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone.ToString)
'            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
'            'S.SANDEEP [ 07 NOV 2011 ] -- END



'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            objDataOperation.AddParameter("@isaduser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsADuser.ToString)
'            'Pinkal (12-Oct-2011) -- End



'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes
'            objDataOperation.AddParameter("@ismanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsManager.ToString)
'            objDataOperation.AddParameter("@userdomain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUserdomain.ToString)
'            'Pinkal (21-Jun-2012) -- End

'            'Sohail (04 Jul 2012) -- Start
'            'TRA - ENHANCEMENT
'            If mdtRelogin_Date <> Nothing Then
'                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRelogin_Date)
'            Else
'                objDataOperation.AddParameter("@relogin_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            'Sohail (04 Jul 2012) -- End

'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
'            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeCompanyUnkid.ToString)
'            If mdtSuspended_From_Date <> Nothing Then
'                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_From_Date)
'            Else
'                objDataOperation.AddParameter("@suspended_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtSuspended_To_Date <> Nothing Then
'                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSuspended_To_Date)
'            Else
'                objDataOperation.AddParameter("@suspended_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtProbation_From_Date <> Nothing Then
'                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_From_Date)
'            Else
'                objDataOperation.AddParameter("@probation_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtProbation_To_Date <> Nothing Then
'                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProbation_To_Date)
'            Else
'                objDataOperation.AddParameter("@probation_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtEmpl_Enddate <> Nothing Then
'                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmpl_Enddate)
'            Else
'                objDataOperation.AddParameter("@empl_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtTermination_From_Date <> Nothing Then
'                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_From_Date)
'            Else
'                objDataOperation.AddParameter("@termination_from_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtTermination_To_Date <> Nothing Then
'                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTermination_To_Date)
'            Else
'                objDataOperation.AddParameter("@termination_to_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtAppointeddate <> Nothing Then
'                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppointeddate)
'            Else
'                objDataOperation.AddParameter("@appointeddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            If mdtReinstatement_Date <> Nothing Then
'                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatement_Date)
'            Else
'                objDataOperation.AddParameter("@reinstatement_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            'S.SANDEEP [ 01 DEC 2012 ] -- END


'            'strQ = "UPDATE hrmsConfiguration..cfuser_master SET " & _
'            '          "  username = @username" & _
'            '          ", password = @password" & _
'            '          ", ispasswordexpire = @ispasswordexpire" & _
'            '          ", roleunkid = @roleunkid" & _
'            '          ", isrighttoleft = @isrighttoleft" & _
'            '          ", exp_days = @exp_days" & _
'            '          ", languageunkid = @languageunkid" & _
'            '          ", isactive = @isactive " & _
'            '        "WHERE userunkid = @userunkid "

'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'strQ = "UPDATE hrmsconfiguration..cfuser_master SET " & _
'            '              "  username = @username" & _
'            '              ", password = @password" & _
'            '              ", ispasswordexpire = @ispasswordexpire" & _
'            '              ", roleunkid = @roleunkid" & _
'            '              ", isrighttoleft = @isrighttoleft" & _
'            '              ", exp_days = @exp_days" & _
'            '              ", languageunkid = @languageunkid" & _
'            '              ", isactive = @isactive " & _
'            '                      ", creationdate = @creationdate" & _
'            '                      ", islocked = @islocked" & _
'            '                      ", lockedtime = @lockedtime" & _
'            '                      ", lockedduration = @lockedduration " & _
'            '                      ", createdbyunkid = @createdbyunkid " & _
'            '            "WHERE userunkid = @userunkid "


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes

'            ' strQ = "UPDATE cfuser_master SET " & _
'            '  "  username = @username" & _
'            '  ", password = @password" & _
'            '  ", ispasswordexpire = @ispasswordexpire" & _
'            '  ", roleunkid = @roleunkid" & _
'            '  ", isrighttoleft = @isrighttoleft" & _
'            '  ", exp_days = @exp_days" & _
'            '  ", languageunkid = @languageunkid" & _
'            '  ", isactive = @isactive " & _
'            '          ", creationdate = @creationdate" & _
'            '          ", islocked = @islocked" & _
'            '          ", lockedtime = @lockedtime" & _
'            '          ", lockedduration = @lockedduration " & _
'            '          ", createdbyunkid = @createdbyunkid " & _
'            '          ", firstname = @firstname" & _
'            '          ", lastname = @lastname" & _
'            '          ", address1 = @address1" & _
'            '          ", address2 = @address2" & _
'            '          ", phone = @phone" & _
'            '          ", email = @email " & _
'            '"WHERE userunkid = @userunkid "



'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            ' strQ = "UPDATE hrmsconfiguration..cfuser_master SET " & _
'            '   "  username = @username" & _
'            '   ", password = @password" & _
'            '   ", ispasswordexpire = @ispasswordexpire" & _
'            '   ", roleunkid = @roleunkid" & _
'            '   ", isrighttoleft = @isrighttoleft" & _
'            '   ", exp_days = @exp_days" & _
'            '   ", languageunkid = @languageunkid" & _
'            '   ", isactive = @isactive " & _
'            '   ", creationdate = @creationdate" & _
'            '   ", islocked = @islocked" & _
'            '   ", lockedtime = @lockedtime" & _
'            '   ", lockedduration = @lockedduration " & _
'            '   ", createdbyunkid = @createdbyunkid " & _
'            '   ", firstname = @firstname" & _
'            '   ", lastname = @lastname" & _
'            '   ", address1 = @address1" & _
'            '   ", address2 = @address2" & _
'            '   ", phone = @phone" & _
'            '   ", email = @email " & _
'            '", isaduser = @isaduser " & _
'            ' "WHERE userunkid = @userunkid "

'            strQ = "UPDATE hrmsconfiguration..cfuser_master SET " & _
'              "  username = @username" & _
'              ", password = @password" & _
'              ", ispasswordexpire = @ispasswordexpire" & _
'              ", roleunkid = @roleunkid" & _
'              ", isrighttoleft = @isrighttoleft" & _
'              ", exp_days = @exp_days" & _
'              ", languageunkid = @languageunkid" & _
'              ", isactive = @isactive " & _
'                      ", creationdate = @creationdate" & _
'                      ", islocked = @islocked" & _
'                      ", lockedtime = @lockedtime" & _
'                      ", lockedduration = @lockedduration " & _
'                      ", createdbyunkid = @createdbyunkid " & _
'                      ", firstname = @firstname" & _
'                      ", lastname = @lastname" & _
'                      ", address1 = @address1" & _
'                      ", address2 = @address2" & _
'                      ", phone = @phone" & _
'                      ", email = @email " & _
'                   ", isaduser = @isaduser " & _
'             ", ismanager = @ismanager " & _
'             ", userdomain = @userdomain " & _
'             ", relogin_date = @relogin_date " & _
'                       ", employeeunkid = @employeeunkid" & _
'                       ", companyunkid = @companyunkid " & _
'                       ", suspended_from_date = @suspended_from_date " & _
'                       ", suspended_to_date = @suspended_to_date " & _
'                       ", probation_from_date = @probation_from_date " & _
'                       ", probation_to_date = @probation_to_date " & _
'                       ", empl_enddate = @empl_enddate " & _
'                       ", termination_from_date = @termination_from_date " & _
'                       ", termination_to_date = @termination_to_date " & _
'                       ", appointeddate = @appointeddate" & _
'                       ", reinstatement_date = @reinstatement_date " & _
'                   "WHERE userunkid = @userunkid " 'S.SANDEEP [ 01 DEC 2012 (employeeunkid,companyunkid)]


'            'Pinkal (21-Jun-2012) -- End

'            'Pinkal (12-Oct-2011) -- End



'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            'S.SANDEEP [ 30 May 2011 ] -- END 

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Dim blnIsInserted As Boolean = False
'            'S.SANDEEP [ 01 DEC 2012 ] -- END

'            'S.SANDEEP [ 24 JUNE 2011 ] -- START
'            'ISSUE : DELETION OF PRIVILEGE WHILE UNLOCKING USER
'            If blnIsFromUnlock = False Then


'                'S.SANDEEP [ 08 NOV 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'strQ = "DELETE FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = @userunkid"

'                'objDataOperation.ExecNonQuery(strQ)
'                'If objDataOperation.ErrorMessage <> "" Then
'                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                '    Throw exForce
'                'End If

'                'If mstrAssignPrivilegeIDs.Length > 0 Then
'                '    Dim strPrivilegeUnkid() As String = Split(mstrAssignPrivilegeIDs, ",")
'                '    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                '        strQ = "INSERT INTO hrmsconfiguration..cfuser_privilege(userunkid, privilegeunkid) " & _
'                '                    "VALUES(@userunkid, @privilegeunkid)"

'                '        objDataOperation.ClearParameters()
'                '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                '        objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

'                '        objDataOperation.ExecNonQuery(strQ)
'                '        If objDataOperation.ErrorMessage <> "" Then
'                '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                '            Throw exForce
'                '        End If
'                '    Next
'                'End If

'                'strQ = "DELETE FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = @userunkid"
'                'objDataOperation.ExecNonQuery(strQ)
'                'If objDataOperation.ErrorMessage <> "" Then
'                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                '    Throw exForce
'                'End If
'                'If mstrAssignCompanyPrivilegeIDs.Length > 0 Then
'                '    Dim strPrivilegeUnkid() As String = Split(mstrAssignCompanyPrivilegeIDs, ",")
'                '    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                '        strQ = "INSERT INTO hrmsconfiguration..cfcompanyaccess_privilege (userunkid,yearunkid) " & _
'                '                        "VALUES(@userunkid,@yearunkid) "
'                '        objDataOperation.ClearParameters()
'                '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))
'                '        objDataOperation.ExecNonQuery(strQ)
'                '        If objDataOperation.ErrorMessage <> "" Then
'                '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                '            Throw exForce
'                '        End If
'                '    Next
'                'End If

'                Dim dsTran As New DataSet

'                strQ = "SELECT userprivilegeunkid,userunkid,privilegeunkid FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = " & mintUserunkid

'                dsTran = objDataOperation.ExecQuery(strQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If dsTran.Tables(0).Rows.Count > 0 Then
'                    Dim dRemPvg() As DataRow = dsTran.Tables(0).Select("privilegeunkid NOT IN (" & IIf(mstrAssignPrivilegeIDs = "", "0", mstrAssignPrivilegeIDs) & ")")
'                    If dRemPvg.Length > 0 Then
'                        For iCnt As Integer = 0 To dRemPvg.Length - 1
'                            If dRemPvg(iCnt).Item("userprivilegeunkid").ToString.Trim.Length > 0 Then
'                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfuser_privilege", "userprivilegeunkid", dRemPvg(iCnt)("userprivilegeunkid"), 2, 3, True) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                    'S.SANDEEP [ 01 DEC 2012 ] -- START
'                                    'ENHANCEMENT : TRA CHANGES
'                                Else
'                                    blnIsInserted = True
'                                    'S.SANDEEP [ 01 DEC 2012 ] -- END
'                                End If

'                                strQ = "DELETE FROM hrmsconfiguration..cfuser_privilege WHERE userprivilegeunkid = '" & dRemPvg(iCnt)("userprivilegeunkid") & "' "

'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            End If
'                        Next
'                    End If
'                End If

'                If mstrAssignPrivilegeIDs.Length > 0 Then
'                    Dim strPrivilegeUnkid() As String = Split(mstrAssignPrivilegeIDs, ",")
'                    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                        objDataOperation.ClearParameters()
'                        If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfuser_privilege WHERE userunkid = '" & mintUserunkid & "' AND privilegeunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then

'                            strQ = "INSERT INTO hrmsconfiguration..cfuser_privilege(userunkid, privilegeunkid) " & _
'                                        "VALUES(@userunkid, @privilegeunkid); SELECT @@identity "

'                            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                            objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

'                            dsTran = objDataOperation.ExecQuery(strQ, "List")

'                            If objDataOperation.ErrorMessage <> "" Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                            End If

'                            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfuser_privilege", "userprivilegeunkid", dsTran.Tables(0).Rows(0)(0), 2, 1, True) = False Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                                'S.SANDEEP [ 01 DEC 2012 ] -- START
'                                'ENHANCEMENT : TRA CHANGES
'                            Else
'                                blnIsInserted = True
'                                'S.SANDEEP [ 01 DEC 2012 ] -- END
'                            End If
'                        End If
'                    Next
'                End If

'                strQ = "SELECT companyacessunkid,userunkid,yearunkid FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = " & mintUserunkid

'                dsTran = objDataOperation.ExecQuery(strQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If dsTran.Tables(0).Rows.Count > 0 Then
'                    Dim dRemPvg() As DataRow = dsTran.Tables(0).Select("yearunkid NOT IN (" & IIf(mstrAssignCompanyPrivilegeIDs = "", "0", mstrAssignCompanyPrivilegeIDs) & ")")
'                    If dRemPvg.Length > 0 Then
'                        For iCnt As Integer = 0 To dRemPvg.Length - 1
'                            If dRemPvg(iCnt).Item("yearunkid").ToString.Trim.Length > 0 Then
'                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfcompanyaccess_privilege", "companyacessunkid", dRemPvg(iCnt)("companyacessunkid"), 2, 3, True) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                    'S.SANDEEP [ 01 DEC 2012 ] -- START
'                                    'ENHANCEMENT : TRA CHANGES
'                                Else
'                                    blnIsInserted = True
'                                    'S.SANDEEP [ 01 DEC 2012 ] -- END
'                                End If

'                                strQ = "DELETE FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE companyacessunkid = '" & dRemPvg(iCnt)("companyacessunkid") & "' "

'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            End If
'                        Next
'                    End If
'                End If

'                If mstrAssignCompanyPrivilegeIDs.Length > 0 Then
'                    Dim strPrivilegeUnkid() As String = Split(mstrAssignCompanyPrivilegeIDs, ",")
'                    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                        objDataOperation.ClearParameters()
'                        If objDataOperation.RecordCount("SELECT * FROM hrmsconfiguration..cfcompanyaccess_privilege WHERE userunkid = '" & mintUserunkid & "' AND yearunkid = '" & strPrivilegeUnkid(iCnt) & "'") <= 0 Then

'                            strQ = "INSERT INTO hrmsconfiguration..cfcompanyaccess_privilege (userunkid,yearunkid) " & _
'                                            "VALUES(@userunkid,@yearunkid); SELECT @@identity "

'                            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))

'                            dsTran = objDataOperation.ExecQuery(strQ, "List")

'                            If objDataOperation.ErrorMessage <> "" Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                            End If

'                            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "cfcompanyaccess_privilege", "companyacessunkid", dsTran.Tables(0).Rows(0)(0), 2, 1, True) = False Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                                'S.SANDEEP [ 01 DEC 2012 ] -- START
'                                'ENHANCEMENT : TRA CHANGES
'                            Else
'                                blnIsInserted = True
'                                'S.SANDEEP [ 01 DEC 2012 ] -- END
'                            End If

'                        End If
'                    Next
'                End If
'                'S.SANDEEP [ 08 NOV 2012 ] -- END






'                'S.SANDEEP [ 04 FEB 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'strQ = "DELETE FROM hrmsconfiguration..cfuseraccess_privilege WHERE userunkid = @userunkid And companyunkid = @companyunkid "
'                'objDataOperation.ClearParameters()
'                'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                'objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid)
'                'objDataOperation.ExecNonQuery(strQ)
'                'If objDataOperation.ErrorMessage <> "" Then
'                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                '    Throw exForce
'                'End If
'                'If mstrAssignAccessPrivilegeIDs.Length > 0 Then
'                '    Dim strPrivilegeUnkid() As String = Split(mstrAssignAccessPrivilegeIDs, ",")
'                '    Dim strAccessLevel() As String = Split(mstrUserAccessLevels, ",")
'                '    For iCnt As Integer = 0 To strPrivilegeUnkid.Length - 1
'                '        strQ = "INSERT INTO hrmsconfiguration..cfuseraccess_privilege (userunkid,jobunkid,job_level,companyunkid,yearunkid) " & _
'                '                       "VALUES(@userunkid,@jobunkid,@job_level,@companyunkid,@yearunkid) "

'                '        objDataOperation.ClearParameters()
'                '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                '        objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strPrivilegeUnkid(iCnt))
'                '        objDataOperation.AddParameter("@job_level", SqlDbType.Int, eZeeDataType.INT_SIZE, strAccessLevel(iCnt))
'                '        objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid)
'                '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)

'                '        objDataOperation.ExecNonQuery(strQ)
'                '        If objDataOperation.ErrorMessage <> "" Then
'                '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                '            Throw exForce
'                '        End If
'                '    Next
'                'End If
'                'S.SANDEEP [ 04 FEB 2012 ] -- END

'            End If
'            'S.SANDEEP [ 24 JUNE 2011 ] -- END 


'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If blnIsInserted = False Then
'                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfuser_master", "userunkid", mintUserunkid, "", "", -1, 2, 0, True) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'            End If
'            'S.SANDEEP [ 01 DEC 2012 ] -- END


'            'S.SANDEEP [ 31 DEC 2013 ] -- START
'            ''Sandeep [ 21 Aug 2010 ] -- Start
'            'objDataOperation.ReleaseTransaction(True)
'            ''Sandeep [ 31 DEC 2010 ] -- End

'            If iObjDataOpr Is Nothing Then
'                objDataOperation.ReleaseTransaction(True)
'            End If
'            'S.SANDEEP [ 04 JAN 2013 ] -- END

'            Return True
'        Catch ex As Exception
'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objDataOperation.ReleaseTransaction(False)
'            'Sandeep [ 21 Aug 2010 ] -- End
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'S.SANDEEP [ 31 DEC 2013 ] -- START
'            'objDataOperation = Nothing
'            If iObjDataOpr Is Nothing Then
'                objDataOperation = Nothing
'            End If
'            'S.SANDEEP [ 31 DEC 2013 ] -- END
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (cfuser_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'Anjan (23 Nov 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
'            'strQ = "DELETE FROM hrmsConfiguration..cfuser_master " & _
'            '            "WHERE userunkid = @userunkid "

'            strQ = "UPDATE hrmsConfiguration..cfuser_master SET isactive = 0 " & _
'            " WHERE userunkid = @userunkid "
'            'Anjan (23 Nov 2012)-End 

'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "<Query>"

'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes


'            'strQ = "SELECT " & _
'            '              "  userunkid " & _
'            '              ", username " & _
'            '              ", password " & _
'            '              ", ispasswordexpire " & _
'            '              ", roleunkid " & _
'            '              ", isrighttoleft " & _
'            '              ", exp_days " & _
'            '              ", languageunkid " & _
'            '              ", isactive " & _
'            '           "FROM hrmsConfiguration..cfuser_master " & _
'            '           "WHERE username = @name "



'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            'strQ = "SELECT " & _
'            '              "  userunkid " & _
'            '              ", username " & _
'            '              ", password " & _
'            '              ", ispasswordexpire " & _
'            '              ", roleunkid " & _
'            '              ", isrighttoleft " & _
'            '              ", exp_days " & _
'            '              ", languageunkid " & _
'            '              ", isactive " & _
'            '              ", creationdate " & _
'            '              ", islocked " & _
'            '              ", lockedtime " & _
'            '              ", lockedduration " & _
'            '              ", createdbyunkid " & _
'            '              ", firstname " & _
'            '              ", lastname " & _
'            '              ", address1 " & _
'            '              ", address2 " & _
'            '              ", phone " & _
'            '              ", email" & _
'            '              ", isaduser " & _
'            '           "FROM hrmsConfiguration..cfuser_master " & _
'            '           "WHERE username = @name "

'            strQ = "SELECT " & _
'                          "  userunkid " & _
'                          ", username " & _
'                          ", password " & _
'                          ", ispasswordexpire " & _
'                          ", roleunkid " & _
'                          ", isrighttoleft " & _
'                          ", exp_days " & _
'                          ", languageunkid " & _
'                          ", isactive " & _
'                          ", creationdate " & _
'                          ", islocked " & _
'                          ", lockedtime " & _
'                          ", lockedduration " & _
'                          ", createdbyunkid " & _
'                          ", firstname " & _
'                          ", lastname " & _
'                          ", address1 " & _
'                          ", address2 " & _
'                          ", phone " & _
'                          ", email" & _
'                          ", isaduser " & _
'                       ", ismanager " & _
'                       ", userdomain " & _
'                       "FROM hrmsConfiguration..cfuser_master " & _
'                       "WHERE username = @name "

'            'Pinkal (21-Jun-2012) -- End


'            'Pinkal (12-Oct-2011) -- End


'            'Anjan (10 Feb 2012)-Start
'            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
'            strQ &= " AND isactive = 1 "
'            'Anjan (10 Feb 2012)-End 

'            If intUnkid > 0 Then
'                strQ &= " AND userunkid <> @userunkid"
'            End If

'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            'objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    ''' Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal blnAdUser As Boolean = False) As DataSet 'Anjan (15 Jan 2012)
'    Public Function getComboList(Optional ByVal strListName As String = "List", _
'                                 Optional ByVal mblnFlag As Boolean = False, _
'                                 Optional ByVal blnAdUser As Boolean = False, _
'                                 Optional ByVal blnOnlyEmp As Boolean = False, _
'                                 Optional ByVal intCompanyId As Integer = 0, _
'                                 Optional ByVal intPrivilegeId As Integer = 0, Optional ByVal intYearid As Integer = 0) As DataSet   'Pinkal (09-Jan-2013) -- Start

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'S.SANDEEP [ 06 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If mblnFlag = True Then
'            '    strQ = "SELECT 0 As userunkid , @ItemName As  name  UNION "
'            'End If
'            'strQ &= "SELECT userunkid,username As name FROM hrmsConfiguration..cfuser_master WHERE isactive =1 "

'            If mblnFlag = True Then
'                strQ = "SELECT 0 As userunkid , @ItemName As  name , '' As Display UNION "
'            End If
'            strQ &= "SELECT hrmsConfiguration..cfuser_master.userunkid,hrmsConfiguration..cfuser_master.username  As name,ISNULL(firstname,'') + ' ' + ISNULL(lastname,'') AS Display FROM hrmsConfiguration..cfuser_master "
'            'S.SANDEEP [ 06 DEC 2012 ] -- END



'            'Pinkal (09-Jan-2013) -- Start
'            'Enhancement : TRA Changes

'            If intCompanyId > 0 Then
'                strQ &= " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
'                        "   AND hrmsConfiguration..cfuser_privilege.privilegeunkid = " & intPrivilegeId & " " & _
'                        " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON hrmsConfiguration..cfuser_master.userunkid = hrmsConfiguration..cfcompanyaccess_privilege.userunkid  " & _
'                        " JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cffinancial_year_tran.yearunkid =  hrmsConfiguration..cfcompanyaccess_privilege.yearunkid "
'                'Pinkal (15-Jan-2013) -- Start
'                'Enhancement : TRA Changes

'                If intYearid <= 0 Then intYearid = FinancialYear._Object._YearUnkid
'                If intYearid > 0 Then
'                    strQ &= " AND hrmsConfiguration..cffinancial_year_tran.yearunkid = '" & intYearid & "' "
'                End If



'                'Pinkal (15-Jan-2013) -- End

'            End If

'            strQ &= " WHERE isactive =1 "


'            If blnAdUser Then
'                strQ &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
'            Else
'                strQ &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "
'            End If

'            'S.SANDEEP [ 01 DEC 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If blnOnlyEmp Then
'                strQ &= " AND (hrmsConfiguration..cfuser_master.employeeunkid > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "

'                'S.SHARMA [ 22 JAN 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If intCompanyId > 0 Then
'                    strQ &= " AND (hrmsConfiguration..cfuser_master.companyunkid = " & intCompanyId & " OR hrmsConfiguration..cfuser_master.companyunkid <=0) "
'                End If
'                'S.SHARMA [ 22 JAN 2013 ] -- END

'                'S.SANDEEP [ 06 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'            Else
'                strQ &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0  OR hrmsConfiguration..cfuser_master.userunkid = 1) "
'                'S.SANDEEP [ 06 DEC 2012 ] -- END
'            End If
'            'S.SANDEEP [ 01 DEC 2012 ] -- END
'            'If blnOnlyEmp Then
'            '    strQ &= " AND hrmsConfiguration..cfuser_master.companyunkid = " & intCompanyId
'            'End If

'            'Pinkal (09-Jan-2013) -- End

'            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

'            dsList = objDataOperation.ExecQuery(strQ, strListName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'        End Try
'    End Function


'    'Sandeep [ 21 Aug 2010 ] -- Start


'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    '''' <summary>
'    '''' Modify By: Sandeep J. Sharma
'    '''' </summary>
'    '''' <purpose> Get the List of Privileges </purpose>
'    'Public Function getPrivilegeList(ByVal intRoleId As Integer, ByVal intUserId As Integer) As DataSet
'    '    Dim dsList As New DataSet
'    '    Dim StrQ As String = String.Empty
'    '    Dim exForce As Exception
'    '    Dim dsAbilityPrivilege As New DataSet
'    '    objDataOperation = New clsDataOperation
'    '    Try


'    '        'Sandeep [ 14 Oct 2010 ] -- Start
'    '        'StrQ = "SELECT hrmsConfiguration..cfrole_master.privilegeunkid FROM hrmsConfiguration..cfrole_master WHERE roleunkid = @RoleId"
'    '        StrQ = "SELECT ISNULL(hrmsConfiguration..cfrole_master.privilegeunkid,'-1') As privilegeunkid FROM hrmsConfiguration..cfrole_master WHERE roleunkid = @RoleId"
'    '        'Sandeep [ 14 Oct 2010 ] -- End 
'    '        objDataOperation.AddParameter("@RoleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleId)

'    '        dsAbilityPrivilege = objDataOperation.ExecQuery(StrQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If
'    '        'Sandeep [ 14 Oct 2010 ] -- Start
'    '        'If dsAbilityPrivilege.Tables(0).Rows.Count >= 1 Then
'    '        If dsAbilityPrivilege.Tables(0).Rows.Count >= 1 And dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "-1" Then
'    '            'Sandeep [ 14 Oct 2010 ] -- End 



'    '            'Sandeep [ 23 Oct 2010 ] -- Start
'    '            'StrQ = "SELECT " & _
'    '            '           "    cfuserprivilege_master.privilegeunkid " & _
'    '            '           "   ,privilege_name " & _
'    '            '           "   ,cfuserprivilege_master.privilegegroupunkid " & _
'    '            '           "   ,cfuserprivilegegroup_master.name AS group_name " & _
'    '            '           "   ,(CASE WHEN cfuser_privilege.privilegeunkid IS NULL THEN 0 ELSE 1 END ) AS assign " & _
'    '            '           "FROM cfuserprivilege_master " & _
'    '            '           "    LEFT OUTER JOIN cfuser_privilege ON cfuserprivilege_master.privilegeunkid = cfuser_privilege.privilegeunkid " & _
'    '            '           "    AND cfuser_privilege.userunkid = @userunkid " & _
'    '            '           "   ,cfuserprivilegegroup_master " & _
'    '            '           "WHERE cfuserprivilege_master.privilegegroupunkid = cfuserprivilegegroup_master.privilegegroupunkid " & _
'    '            '           "    AND cfuserprivilege_master.privilegeunkid IN (  " & dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") & ") " & _
'    '            '            "    AND isdeleted = 'N' " & _
'    '            '           " ORDER BY cfuserprivilege_master.privilegegroupunkid  "

'    '            StrQ = "SELECT " & _
'    '                       "    hrmsconfiguration..cfuserprivilege_master.privilegeunkid " & _
'    '                       "   ,privilege_name " & _
'    '                       "   ,hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid " & _
'    '                       "   ,hrmsconfiguration..cfuserprivilegegroup_master.name AS group_name " & _
'    '                       "   ,(CASE WHEN hrmsconfiguration..cfuser_privilege.privilegeunkid IS NULL THEN 0 ELSE 1 END ) AS assign " & _
'    '                       "FROM hrmsconfiguration..cfuserprivilege_master " & _
'    '                       "    LEFT OUTER JOIN hrmsconfiguration..cfuser_privilege ON hrmsconfiguration..cfuserprivilege_master.privilegeunkid = hrmsconfiguration..cfuser_privilege.privilegeunkid " & _
'    '                       "    AND hrmsconfiguration..cfuser_privilege.userunkid = @userunkid " & _
'    '                       "   ,hrmsconfiguration..cfuserprivilegegroup_master " & _
'    '                       "WHERE hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsconfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
'    '                       "    AND hrmsconfiguration..cfuserprivilege_master.privilegeunkid IN (SELECT Distinct hrmsconfiguration..cfuser_privilege.privilegeunkid " & _
'    '                       "FROM hrmsconfiguration..cfuser_privilege, hrmsconfiguration..cfuserprivilege_master " & _
'    '                       "WHERE userunkid = @mainuserunkid) " & _
'    '                       "    AND isdeleted = 'N' " & _
'    '                       " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
'    '            'Sandeep [ 23 Oct 2010 ] -- End 

'    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
'    '            'Sandeep [ 07 FEB 2011 ] -- START
'    '            'objDataOperation.AddParameter("@mainuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 1)
'    '            objDataOperation.AddParameter("@mainuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'    '            'Sandeep [ 07 FEB 2011 ] -- END 

'    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

'    '            If objDataOperation.ErrorMessage <> "" Then
'    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '                Throw exForce
'    '            End If
'    '        End If

'    '        Return dsList

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "getPrivilegeList", mstrModuleName)
'    '        Return Nothing
'    '    Finally
'    '        exForce = Nothing
'    '        dsList.Dispose()
'    '        dsList = Nothing
'    '        objDataOperation = Nothing
'    '    End Try
'    'End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Get the List of Privileges </purpose>
'    Public Function getPrivilegeList(ByVal intRoleId As Integer, ByVal intUserId As Integer) As DataSet
'        Dim dsList As New DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsAbilityPrivilege As New DataSet
'        objDataOperation = New clsDataOperation
'        Try

'            StrQ = "SELECT ISNULL(hrmsConfiguration..cfrole_master.privilegeunkid,'-1') As privilegeunkid FROM hrmsConfiguration..cfrole_master WHERE roleunkid = @RoleId"

'            objDataOperation.AddParameter("@RoleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleId)

'            dsAbilityPrivilege = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes

'            If dsAbilityPrivilege.Tables(0).Rows.Count > 0 Then

'                If dsAbilityPrivilege.Tables(0).Rows.Count >= 1 And dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "-1" Then

'                    StrQ = "SELECT " & _
'                               "    hrmsconfiguration..cfuserprivilege_master.privilegeunkid " & _
'                               "   ,privilege_name " & _
'                               "   ,hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid " & _
'                               "   ,hrmsconfiguration..cfuserprivilegegroup_master.name AS group_name " & _
'                               "   ,(CASE WHEN hrmsconfiguration..cfuser_privilege.privilegeunkid IS NULL THEN 0 ELSE 1 END ) AS assign " & _
'                               "FROM hrmsconfiguration..cfuserprivilege_master " & _
'                               "    LEFT OUTER JOIN hrmsconfiguration..cfuser_privilege ON hrmsconfiguration..cfuserprivilege_master.privilegeunkid = hrmsconfiguration..cfuser_privilege.privilegeunkid " & _
'                               "    AND hrmsconfiguration..cfuser_privilege.userunkid = @userunkid " & _
'                               "   ,hrmsconfiguration..cfuserprivilegegroup_master " & _
'                               "WHERE hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsconfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
'                               "    AND hrmsconfiguration..cfuserprivilege_master.privilegeunkid IN (SELECT Distinct hrmsconfiguration..cfuser_privilege.privilegeunkid " & _
'                               "FROM hrmsconfiguration..cfuser_privilege, hrmsconfiguration..cfuserprivilege_master " & _
'                               "WHERE userunkid = @mainuserunkid) " & _
'                               "    AND isdeleted = 'N' "
'                    If intUserId <= 0 Then
'                        If dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "" And dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") <> "-1" Then
'                            StrQ &= "AND cfuserprivilege_master.privilegeunkid NOT IN (" & dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") & ") " & _
'                            "UNION " & _
'                                "SELECT " & _
'                                   " hrmsconfiguration..cfuserprivilege_master.privilegeunkid " & _
'                                   ",privilege_name " & _
'                                   ",hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid " & _
'                                   ",hrmsconfiguration..cfuserprivilegegroup_master.name AS group_name " & _
'                                   ",1 AS assign " & _
'                                "FROM hrmsconfiguration..cfuserprivilege_master,hrmsconfiguration..cfuserprivilegegroup_master " & _
'                                "WHERE hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsconfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
'                                "AND privilegeunkid IN (" & dsAbilityPrivilege.Tables(0).Rows(0).Item("privilegeunkid") & ") " & _
'                               " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
'                        Else
'                            StrQ &= " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
'                        End If
'                    Else
'                        StrQ &= " ORDER BY hrmsconfiguration..cfuserprivilege_master.privilegegroupunkid  "
'                    End If

'                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)

'                    'Anjan (25 Oct 2012)-Start
'                    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
'                    'Issue : on andrews request that on those user who has priviliege for allow to view user list can add and remove privileige from user add/edti.
'                    'objDataOperation.AddParameter("@mainuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'                    objDataOperation.AddParameter("@mainuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 1)
'                    'Anjan (25 Oct 2012)-End 



'                    dsList = objDataOperation.ExecQuery(StrQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                End If
'            End If

'            'Pinkal (12-Oct-2011) -- End

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "getPrivilegeList", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function
'    'S.SANDEEP [ 07 NOV 2011 ] -- END




'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Get the List of Company Access Privileges </purpose>
'    Public Function getCompanyPrivilegeList(ByVal intUserId As Integer) As DataSet
'        Dim dsList As New DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        Try
'            StrQ = "SELECT " & _
'                       "	 ISNULL(hrmsConfiguration..cfcompany_master.name,'')AS group_name " & _
'                       "	,ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name,'') AS privilege_name " & _
'                       "	,CASE WHEN hrmsconfiguration..cfcompanyaccess_privilege.yearunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                       "	,hrmsConfiguration..cfcompany_master.companyunkid As privilegegroupunkid " & _
'                       "	," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid As privilegeunkid " & _
'                       "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
'                       "	LEFT OUTER JOIN hrmsconfiguration..cfcompanyaccess_privilege ON " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = hrmsconfiguration..cfcompanyaccess_privilege.yearunkid " & _
'                       "	AND hrmsconfiguration..cfcompanyaccess_privilege.userunkid = @userunkid " & _
'                       ",hrmsConfiguration..cfcompany_master " & _
'                       "WHERE hrmsConfiguration..cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
'                       "	AND ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 " & _
'                       "ORDER BY hrmsConfiguration..cfcompany_master.companyunkid "

'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "getCompanyPrivilegeList", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Get the List of Roles With Privileges </purpose>
'    Public Function GetAbilityGroupList() As DataSet
'        Dim dsList As New DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            'StrQ = "SELECT " & _
'            '           "    hrmsConfiguration..cfuserabilitylevel_master.userabilitylevelunkid " & _
'            '           "	,hrmsConfiguration..cfuserabilitylevel_master.roleunkid " & _
'            '           "	,hrmsConfiguration..cfuserabilitylevel_master.parentunkid " & _
'            '           "	,hrmsConfiguration..cfuserabilitylevel_master.privilegeunkid " & _
'            '           "	,hrmsConfiguration..cfuserabilitylevel_master.isactive " & _
'            '           "	,ISNULL(cfrole_master.name,'') AS name " & _
'            '           "FROM hrmsConfiguration..cfuserabilitylevel_master " & _
'            '           "    LEFT JOIN hrmsConfiguration..cfrole_master ON hrmsConfiguration..cfuserabilitylevel_master.roleunkid = hrmsConfiguration..cfrole_master.roleunkid " & _
'            '           "WHERE hrmsConfiguration..cfrole_master.isactive = 1 "

'            StrQ = "SELECT " & _
'                       "	hrmsConfiguration..cfrole_master.roleunkid " & _
'                       "  ,hrmsConfiguration..cfrole_master.name " & _
'                       "   ,ISNULL(hrmsConfiguration..cfrole_master.privilegeunkid ,'') As privilegeunkid " & _
'                       "FROM hrmsConfiguration..cfrole_master " & _
'                       "WHERE hrmsConfiguration..cfrole_master.isactive = 1 " & _
'                       "ORDER BY hrmsConfiguration..cfrole_master.roleunkid "

'            dsList = objDataOperation.ExecQuery(StrQ, "list")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetAbilityGroupList", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Get the List of Groups With Privileges </purpose>
'    Public Function GetPrivilegeGroupList() As DataSet
'        Dim dsList As New DataSet
'        Dim StrQ As String
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = "SELECT " & _
'                        "    privilegeunkid " & _
'                        "   ,privilege_name " & _
'                        "   ,hrmsConfiguration..cfuserprivilege_master.privilegegroupunkid " & _
'                        "   ,hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
'                        "   ,hrmsConfiguration..cfuserprivilegegroup_master.name " & _
'                        "FROM hrmsConfiguration..cfuserprivilege_master,hrmsConfiguration..cfuserprivilegegroup_master " & _
'                        "WHERE hrmsConfiguration..cfuserprivilege_master.privilegegroupunkid = hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid " & _
'                        "ORDER BY hrmsConfiguration..cfuserprivilegegroup_master.privilegegroupunkid "

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetPrivilegeGroupList", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Get the List of Ability With Privileges </purpose>
'    Public Function GetPrivilegeAbilityList() As DataSet
'        Dim dsList As New DataSet
'        Dim i As Integer
'        Dim j As Integer
'        Dim exForce As Exception
'        Dim intGroupId As Integer = 0
'        Try
'            objDataOperation = New clsDataOperation

'            Dim dtTemp As New DataTable
'            Dim dtcol As DataColumn

'            dtcol = New DataColumn("Privilege")
'            dtcol.DataType = System.Type.GetType("System.String")
'            dtTemp.Columns.Add(dtcol)

'            Dim dsAbilityList As New DataSet
'            dsAbilityList = GetAbilityGroupList()
'            For i = 0 To dsAbilityList.Tables(0).Rows.Count - 1
'                dtcol = New DataColumn()
'                dtcol.ColumnName = dsAbilityList.Tables(0).Rows(i).Item("roleunkid").ToString
'                dtcol.Caption = dsAbilityList.Tables(0).Rows(i).Item("name")
'                dtcol.DataType = System.Type.GetType("System.Boolean")
'                dtTemp.Columns.Add(dtcol)
'            Next

'            dtcol = New DataColumn("isprivilegegroup")
'            dtcol.DataType = System.Type.GetType("System.Boolean")
'            dtTemp.Columns.Add(dtcol)

'            dtcol = New DataColumn("privilegeunkid")
'            dtcol.DataType = System.Type.GetType("System.Int32")
'            dtTemp.Columns.Add(dtcol)

'            dtcol = New DataColumn("privilegeGroupunkidunkid")
'            dtcol.DataType = System.Type.GetType("System.Int32")
'            dtTemp.Columns.Add(dtcol)

'            Dim drTemp As DataRow
'            Dim dsPrivilege As New DataSet
'            dsPrivilege = GetPrivilegeGroupList()
'            Dim intParentRowIndex As Integer = 0
'            Dim intTemp As Integer = 0
'            For i = 0 To dsPrivilege.Tables(0).Rows.Count - 1
'                If intGroupId <> dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid") Then
'                    drTemp = dtTemp.NewRow()
'                    drTemp.Item("privilege") = dsPrivilege.Tables(0).Rows(i).Item("name")
'                    For j = 0 To dsAbilityList.Tables(0).Rows.Count - 1
'                        drTemp.Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = False
'                    Next
'                    drTemp.Item("isprivilegegroup") = True
'                    drTemp.Item("privilegeunkid") = -1
'                    drTemp.Item("privilegeGroupunkidunkid") = dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid")
'                    dtTemp.Rows.Add(drTemp)
'                    intGroupId = dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid")
'                    intParentRowIndex += 1
'                    intTemp = 0
'                End If
'                drTemp = dtTemp.NewRow()
'                drTemp.Item("isprivilegegroup") = False
'                For j = 0 To dsAbilityList.Tables(0).Rows.Count - 1
'                    Dim strPrivilegeUnkid As String() = Split(Replace(dsAbilityList.Tables(0).Rows(j)("privilegeunkid").ToString, " ", ""), ",")
'                    Dim index As Integer = Array.IndexOf(strPrivilegeUnkid, dsPrivilege.Tables(0).Rows(i).Item("privilegeunkid").ToString)
'                    If index >= 0 Then
'                        drTemp.Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = True
'                        dtTemp.Rows(intParentRowIndex - intTemp - 1).Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = True
'                    Else
'                        drTemp.Item(dsAbilityList.Tables(0).Rows(j)("roleunkid") & "") = False
'                    End If
'                Next
'                drTemp.Item("privilegeunkid") = dsPrivilege.Tables(0).Rows(i).Item("privilegeunkid")
'                drTemp.Item("privilegeGroupunkidunkid") = dsPrivilege.Tables(0).Rows(i).Item("privilegegroupunkid")
'                drTemp.Item("privilege") = Space(5) & dsPrivilege.Tables(0).Rows(i).Item("privilege_name")
'                dtTemp.Rows.Add(drTemp)
'                intParentRowIndex += 1
'                intTemp += 1
'            Next

'            dsList.Tables.Clear()

'            dsList.Tables.Add(dtTemp)

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetPrivilegeAbilityList", mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Set the List of Ability With Privileges </purpose>
'    Public Function SaveAbilityLevelPrivilege(ByVal intAbilityLevelUnkid As Integer, ByVal strPrivilegeUnkid As String) As Boolean
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim StrQ As String = ""
'        Try
'            'S.SANDEEP [ 08 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mDicRolePrivilege.Keys.Count <= 0 Then
'                Call GetRole_Privilege()
'            End If
'            'S.SANDEEP [ 08 NOV 2012 ] -- END

'            objDataOperation = New clsDataOperation
'            'S.SANDEEP [ 08 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.BindTransaction()

'            If mDicRolePrivilege.Keys.Count > 0 Then
'                Dim dsUsr As New DataSet
'                StrQ = "SELECT userunkid,roleunkid FROM hrmsConfiguration..cfuser_master WHERE isactive = 1 AND roleunkid = '" & intAbilityLevelUnkid & "' AND userunkid <> 1"
'                dsUsr = objDataOperation.ExecQuery(StrQ, "List")
'                If mDicRolePrivilege.ContainsKey(intAbilityLevelUnkid) = True Then
'                    Dim dRemPvg() As DataRow = mDicRolePrivilege(intAbilityLevelUnkid).Select("word NOT IN(" & IIf(strPrivilegeUnkid = "", "0", strPrivilegeUnkid) & ")")
'                    If dRemPvg.Length > 0 Then
'                        If dsUsr.Tables(0).Rows.Count > 0 Then
'                            For Each dURow As DataRow In dsUsr.Tables(0).Rows
'                                'THIS IS FOR REMOVING THE PRIVILEGE FROM " CFUSER_PRIVILEGE " WHEN UNASSIGNED FROM ABILITY LEVEL -- START
'                                For i As Integer = 0 To dRemPvg.Length - 1
'                                    If dRemPvg(i)("word").ToString.Trim.Length > 0 Then
'                                        StrQ = "SELECT userprivilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & dURow.Item("userunkid") & "' AND privilegeunkid = '" & dRemPvg(i)("word") & "'"
'                                        dsList = objDataOperation.ExecQuery(StrQ, "List")
'                                        If dsList.Tables(0).Rows.Count > 0 Then
'                                            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cfuser_privilege", "userprivilegeunkid", dsList.Tables(0).Rows(0)("userprivilegeunkid"), True) = False Then
'                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                                Throw exForce
'                                            End If
'                                            StrQ = "DELETE FROM hrmsConfiguration..cfuser_privilege WHERE userprivilegeunkid = '" & dsList.Tables(0).Rows(0)("userprivilegeunkid") & "' "
'                                            objDataOperation.ExecNonQuery(StrQ)
'                                            If objDataOperation.ErrorMessage <> "" Then
'                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                                Throw exForce
'                                            End If
'                                        End If
'                                    End If
'                                Next
'                                'THIS IS FOR REMOVING THE PRIVILEGE FROM " CFUSER_PRIVILEGE " WHEN UNASSIGNED FROM ABILITY LEVEL -- END 
'                            Next
'                        End If
'                    End If
'                    For Each dURow As DataRow In dsUsr.Tables(0).Rows
'                        'THIS IS FOR INSERT THE PRIVILEGE TO " CFUSER_PRIVILEGE " WHEN ASSIGNED FROM ABILITY LEVEL -- START
'                        If strPrivilegeUnkid.Trim.Length > 0 Then
'                            For Each StrPrvgId As String In strPrivilegeUnkid.Trim.Split(",")
'                                If objDataOperation.RecordCount("SELECT userunkid,privilegeunkid FROM hrmsConfiguration..cfuser_privilege WHERE userunkid = '" & dURow.Item("userunkid") & "' AND privilegeunkid = '" & StrPrvgId & "'") <= 0 Then
'                                    StrQ = "INSERT INTO hrmsConfiguration..cfuser_privilege (userunkid,privilegeunkid) " & _
'                                           "SELECT TOP 1 '" & dURow.Item("userunkid") & "','" & StrPrvgId & "'; SELECT @@identity "

'                                    dsList = objDataOperation.ExecQuery(StrQ, "List")

'                                    If objDataOperation.ErrorMessage <> "" Then
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If
'                                    If dsList.Tables("List").Rows(0)(0).ToString.Trim.Length > 0 Then
'                                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "cfuser_privilege", "userprivilegeunkid", dsList.Tables("List").Rows(0)(0), True) = False Then
'                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                            Throw exForce
'                                        End If
'                                    End If
'                                End If
'                            Next
'                        End If
'                        'THIS IS FOR INSERT THE PRIVILEGE TO " CFUSER_PRIVILEGE " WHEN ASSIGNED FROM ABILITY LEVEL -- START
'                    Next
'                End If
'            End If
'            'S.SANDEEP [ 08 NOV 2012 ] -- END



'            StrQ = "UPDATE hrmsConfiguration..cfrole_master " & _
'                        "SET hrmsConfiguration..cfrole_master.privilegeunkid = @privilegeunkid " & _
'                        "WHERE hrmsConfiguration..cfrole_master.roleunkid = @RoleId "

'            objDataOperation.ClearParameters()

'            objDataOperation.AddParameter("@privilegeunkid", SqlDbType.VarChar, 8000, strPrivilegeUnkid)
'            objDataOperation.AddParameter("@RoleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAbilityLevelUnkid.ToString)

'            objDataOperation.ExecNonQuery(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'S.SANDEEP [ 08 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.ReleaseTransaction(True)
'            'S.SANDEEP [ 08 NOV 2012 ] -- END

'            Return True

'        Catch ex As Exception
'            'S.SANDEEP [ 08 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.ReleaseTransaction(False)
'            'S.SANDEEP [ 08 NOV 2012 ] -- END
'            DisplayError.Show("-1", ex.Message, "SaveAbilityLevelPrivilege", mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            dsList.Dispose()
'            dsList = Nothing
'        End Try
'    End Function



'    'S.SANDEEP [ 04 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Public Function GetCompanyJobsList(ByVal intCompanyId As Integer, ByVal intUseUnkid As Integer, ByVal intYearId As Integer) As DataSet
'    '    Dim dsList As New DataSet
'    '    Dim StrQ As String = String.Empty
'    '    Dim exForce As Exception
'    '    Dim strDataBaseName As String = String.Empty
'    '    Try
'    '        objDataOperation = New clsDataOperation

'    '        StrQ = "SELECT " & _
'    '                    "	database_name " & _
'    '                    "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
'    '                    "WHERE companyunkid = @CompanyId " & _
'    '                    " AND yearunkid = @YearId "

'    '        objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
'    '        objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

'    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        If dsList.Tables(0).Rows.Count > 0 Then
'    '            strDataBaseName = dsList.Tables(0).Rows(0)("database_name")
'    '        Else
'    '            strDataBaseName = ""
'    '        End If

'    '        If strDataBaseName.Length > 0 Then

'    '            'Sandeep [ 16 MAY 2011 ] -- Start
'    '            'StrQ = "SELECT " & _
'    '            '            "  " & strDataBaseName & "..hrjob_master.jobunkid " & _
'    '            '            " ," & strDataBaseName & "..hrjob_master.job_name " & _
'    '            '            " ,ISNULL(" & strDataBaseName & "..hrjob_master.job_level,0) as job_level " & _
'    '            '            " ,CASE WHEN cfuseraccess_privilege.jobunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'    '            '            "FROM " & strDataBaseName & "..hrjob_master " & _
'    '            '            "	LEFT OUTER JOIN cfuseraccess_privilege ON " & strDataBaseName & "..hrjob_master.jobunkid = cfuseraccess_privilege.jobunkid " & _
'    '            '            "		AND cfuseraccess_privilege.companyunkid = @CompanyId AND cfuseraccess_privilege.userunkid = @UserId " & _
'    '            '            "      AND cfuseraccess_privilege.yearunkid = @YearId " & _
'    '            '            "WHERE ISNULL(" & strDataBaseName & "..hrjob_master.isactive,0) = 1 "

'    'StrQ = "SELECT " & _
'    '            "  " & strDataBaseName & "..hrjob_master.jobunkid " & _
'    '            " ," & strDataBaseName & "..hrjob_master.job_name " & _
'    '            " ,ISNULL(" & strDataBaseName & "..hrjob_master.job_level,0) as job_level " & _
'    '                        " ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege.jobunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'    '            "FROM " & strDataBaseName & "..hrjob_master " & _
'    '                        "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege ON " & strDataBaseName & "..hrjob_master.jobunkid = hrmsConfiguration..cfuseraccess_privilege.jobunkid " & _
'    '                        "		AND hrmsConfiguration..cfuseraccess_privilege.companyunkid = @CompanyId AND hrmsConfiguration..cfuseraccess_privilege.userunkid = @UserId " & _
'    '                        "      AND hrmsConfiguration..cfuseraccess_privilege.yearunkid = @YearId " & _
'    '            "WHERE ISNULL(" & strDataBaseName & "..hrjob_master.isactive,0) = 1 "
'    '            'Sandeep [ 16 MAY 2011 ] -- End 

'    '            objDataOperation.ClearParameters()
'    '            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
'    '            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUseUnkid)
'    '            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

'    '            dsList = objDataOperation.ExecQuery(StrQ, "Jobs")

'    '            If objDataOperation.ErrorMessage <> "" Then
'    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '                Throw exForce
'    '            End If

'    '        End If

'    '        Return dsList

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "GetCompanyJobsList", mstrModuleName)
'    '        Return Nothing
'    '    End Try
'    'End Function
'    'S.SANDEEP [ 04 FEB 2012 ] -- END

'    'Sandeep [ 21 Aug 2010 ] -- End 




'    'S.SANDEEP [ 04 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function GetUserAccessList(ByVal StrAllocation As String, Optional ByVal intUserId As Integer = -1, Optional ByVal mintCompanyId As Integer = -1) As DataTable 'S.SANDEEP [ 31 MAY 2012 ] -- START -- END
'        'Public Function GetUserAccessList(ByVal menAllocation As enAllocation, Optional ByVal intUserId As Integer = -1, Optional ByVal mintCompanyId As Integer = -1) As DataTable
'        Dim dtUAccess As New DataTable("Access")
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Dim dsTranList As New DataSet
'        Dim StrDBName As String = String.Empty
'        Dim intComId As Integer = 0
'        Dim intYearId As Integer = 0
'        Dim intRowCnt As Integer = 0
'        Try
'            objDataOperation = New clsDataOperation

'            dtUAccess.Columns.Add("IsAssign", System.Type.GetType("System.Boolean")).DefaultValue = 0
'            dtUAccess.Columns.Add("UserAccess", System.Type.GetType("System.String")).DefaultValue = ""
'            dtUAccess.Columns.Add("CompanyId", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtUAccess.Columns.Add("YearId", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtUAccess.Columns.Add("AllocationId", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtUAccess.Columns.Add("AllocationLevel", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dtUAccess.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = 0
'            dtUAccess.Columns.Add("TotAllocation", System.Type.GetType("System.Int32")).DefaultValue = 0
'            'S.SANDEEP [ 31 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            dtUAccess.Columns.Add("AllocationRefId", System.Type.GetType("System.Int32")).DefaultValue = 0
'            'S.SANDEEP [ 31 MAY 2012 ] -- END


'            StrQ = "SELECT " & _
'                   "	 ISNULL(hrmsConfiguration..cfcompany_master.name,'')+' - ( '+ ISNULL(hrmsConfiguration..cffinancial_year_tran.database_name,'')+' )' AS GrpName " & _
'                   "	,hrmsConfiguration..cfcompany_master.companyunkid AS CompId " & _
'                   "	,hrmsConfiguration..cffinancial_year_tran.yearunkid AS YearId " & _
'                   "	,1 AS IsGrp " & _
'                   "	,hrmsConfiguration..cffinancial_year_tran.database_name AS DBName " & _
'                   "FROM hrmsConfiguration..cfcompany_master " & _
'                   "	JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cfcompany_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
'                   "    JOIN sys.databases ON sys.databases.name = hrmsConfiguration..cffinancial_year_tran.database_name " & _
'                   "WHERE hrmsConfiguration..cfcompany_master.isactive = 1 "
'            If mintCompanyId > 0 Then
'                StrQ &= " AND hrmsConfiguration..cfcompany_master.companyunkid = '" & mintCompanyId & "' "
'            End If
'            StrQ &= "ORDER BY hrmsConfiguration..cfcompany_master.companyunkid,hrmsConfiguration..cffinancial_year_tran.yearunkid DESC "

'            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'                StrDBName = "" : intComId = 0 : intYearId = 0
'                StrDBName = dtRow.Item("DBName").ToString.Trim
'                intComId = dtRow.Item("CompId")
'                intYearId = dtRow.Item("YearId")

'                'S.SANDEEP [ 31 MAY 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If intUserId > 0 Then
'                '    Select Case menAllocation
'                '        Case enAllocation.JOBS
'                '            StrQ = "SELECT " & _
'                '                           "	 " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
'                '                           "	," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
'                '                           "	,ISNULL(" & StrDBName & "..hrjob_master.job_level, 0) AS AllocationLevel " & _
'                '                           "	,ISNULL(assign,0) AS assign " & _
'                '                           "	,ISNULL(CompId," & intComId & ") AS CompId " & _
'                '                           "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
'                '                           "	,0 AS IsGrp " & _
'                '                           "FROM " & StrDBName & "..hrjob_master " & _
'                '                           "LEFT JOIN " & _
'                '                           "( " & _
'                '                           "	SELECT " & _
'                '                    "	 " & StrDBName & "..hrjob_master.jobunkid " & _
'                '                           "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                '                           "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
'                '                           "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
'                '                    ",   0 AS IsGrp " & _
'                '                    "FROM " & StrDBName & "..hrjob_master " & _
'                '                           "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrjob_master.jobunkid " & _
'                '                           "	JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid AND referenceunkid = '" & menAllocation & "' " & _
'                '                           "		AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "'  " & _
'                '                           "		AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
'                '                           "	WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive, 0) = 1 " & _
'                '                           ") AS A ON " & StrDBName & "..hrjob_master.jobunkid = A.jobunkid " & _
'                '                    "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
'                '        Case enAllocation.DEPARTMENT
'                '            StrQ = "SELECT " & _
'                '                   "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
'                '                   "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
'                '                   "	,0 AS AllocationLevel " & _
'                '                   "	,ISNULL(assign,0) AS assign " & _
'                '                   "	,ISNULL(CompId," & intComId & ") AS CompId " & _
'                '                   "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
'                '                   "	,0 AS IsGrp " & _
'                '                   "FROM " & StrDBName & "..hrdepartment_master " & _
'                '                   "LEFT JOIN " & _
'                '                   "( " & _
'                '                   "	SELECT " & _
'                '                   "	  " & StrDBName & "..hrdepartment_master.departmentunkid " & _
'                '                   "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                '                   "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
'                '                   "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
'                '                   "	 ,0 AS IsGrp " & _
'                '                   "	FROM " & StrDBName & "..hrdepartment_master " & _
'                '                   "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrdepartment_master.departmentunkid " & _
'                '                   "	JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
'                '                   "        AND referenceunkid = '" & menAllocation & "' " & _
'                '                   "		AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
'                '                   "		AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
'                '                   "	WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 " & _
'                '                   ") AS A ON " & StrDBName & "..hrdepartment_master.departmentunkid = A.departmentunkid " & _
'                '                   "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "
'                '    End Select

'                'Else
'                '    Select Case menAllocation
'                '        Case enAllocation.JOBS
'                '            StrQ = "SELECT " & _
'                '                           "     " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
'                '                           "    ," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
'                '                           "    ,ISNULL(" & StrDBName & "..hrjob_master.job_level,0) AS AllocationLevel " & _
'                '                    ",0 AS assign " & _
'                '                    "," & intComId & " AS CompId " & _
'                '                    "," & intYearId & " AS YearId " & _
'                '                    ",0 AS IsGrp " & _
'                '                    "FROM " & StrDBName & "..hrjob_master " & _
'                '                    "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
'                '        Case enAllocation.DEPARTMENT
'                '            StrQ = "SELECT " & _
'                '                   "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
'                '                   "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
'                '                   "	,0 AS AllocationLevel " & _
'                '                   "	,0 AS assign " & _
'                '                   "	," & intComId & " AS CompId " & _
'                '                   "	," & intComId & " AS YearId " & _
'                '                   "    ,0 AS IsGrp " & _
'                '                   "FROM " & StrDBName & "..hrdepartment_master " & _
'                '                   "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "
'                '    End Select
'                'End If
'                'If mintCompanyId <= 0 Then
'                '    intRowCnt = -1
'                '    Select Case menAllocation
'                '        Case enAllocation.JOBS
'                '            intRowCnt = objDataOperation.RecordCount("SELECT " & StrDBName & "..hrjob_master.jobunkid FROM " & StrDBName & "..hrjob_master WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1")
'                '        Case enAllocation.DEPARTMENT
'                '            intRowCnt = objDataOperation.RecordCount("SELECT " & StrDBName & "..hrdepartment_master.departmentunkid FROM " & StrDBName & "..hrdepartment_master WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive,0) = 1")
'                '    End Select
'                'End If

'                For Each IntKey As Integer In StrAllocation.Split(",")
'                    If intUserId > 0 Then
'                        Select Case IntKey
'                            Case enAllocation.JOBS
'                                StrQ = "SELECT " & _
'                                               "	 " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
'                                               "	," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
'                                               "	,ISNULL(" & StrDBName & "..hrjob_master.job_level, 0) AS AllocationLevel " & _
'                                               "	,ISNULL(assign,0) AS assign " & _
'                                               "	,ISNULL(CompId," & intComId & ") AS CompId " & _
'                                               "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
'                                               "	,0 AS IsGrp " & _
'                                               "FROM " & StrDBName & "..hrjob_master " & _
'                                               "LEFT JOIN " & _
'                                               "( " & _
'                                               "	SELECT " & _
'                                        "	 " & StrDBName & "..hrjob_master.jobunkid " & _
'                                               "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                                               "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
'                                               "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
'                                        ",   0 AS IsGrp " & _
'                                        "FROM " & StrDBName & "..hrjob_master " & _
'                                               "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrjob_master.jobunkid " & _
'                                                   "	JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid AND referenceunkid = '" & IntKey & "' " & _
'                                               "		AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "'  " & _
'                                               "		AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
'                                               "	WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive, 0) = 1 " & _
'                                               ") AS A ON " & StrDBName & "..hrjob_master.jobunkid = A.jobunkid " & _
'                                        "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
'                            Case enAllocation.DEPARTMENT
'                                StrQ = "SELECT " & _
'                                       "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
'                                       "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
'                                       "	,0 AS AllocationLevel " & _
'                                       "	,ISNULL(assign,0) AS assign " & _
'                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
'                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
'                                       "	,0 AS IsGrp " & _
'                                       "FROM " & StrDBName & "..hrdepartment_master " & _
'                                       "LEFT JOIN " & _
'                                       "( " & _
'                                       "	SELECT " & _
'                                       "	  " & StrDBName & "..hrdepartment_master.departmentunkid " & _
'                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
'                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
'                                       "	 ,0 AS IsGrp " & _
'                                       "	FROM " & StrDBName & "..hrdepartment_master " & _
'                                       "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrdepartment_master.departmentunkid " & _
'                                       "	JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
'                                           "        AND referenceunkid = '" & IntKey & "' " & _
'                                       "		AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
'                                       "		AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
'                                       "	WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 " & _
'                                       ") AS A ON " & StrDBName & "..hrdepartment_master.departmentunkid = A.departmentunkid " & _
'                                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "
'                            Case enAllocation.CLASS_GROUP
'                                StrQ = "SELECT " & _
'                                       "	 " & StrDBName & "..hrclassgroup_master.classgroupunkid AS AllocationId " & _
'                                       "	," & StrDBName & "..hrclassgroup_master.name AS Allocation " & _
'                                       "	,0 AS AllocationLevel " & _
'                                       "	,ISNULL(assign,0) AS assign " & _
'                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
'                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
'                                       "	,0 AS IsGrp " & _
'                                       "FROM " & StrDBName & "..hrclassgroup_master " & _
'                                       "LEFT JOIN " & _
'                                       "( " & _
'                                       "	SELECT " & _
'                                       "	  " & StrDBName & "..hrclassgroup_master.classgroupunkid " & _
'                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
'                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
'                                       "	 ,0 AS IsGrp " & _
'                                       "	FROM " & StrDBName & "..hrclassgroup_master " & _
'                                       "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrclassgroup_master.classgroupunkid " & _
'                                       "	JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
'                                       "        AND referenceunkid = '" & IntKey & "' " & _
'                                       "		AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
'                                       "		AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
'                                       "	WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 " & _
'                                       ") AS A ON " & StrDBName & "..hrclassgroup_master.classgroupunkid = A.classgroupunkid " & _
'                                       "WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 "
'                            Case enAllocation.CLASSES
'                                StrQ = "SELECT " & _
'                                       "	 " & StrDBName & "..hrclasses_master.classesunkid AS AllocationId " & _
'                                       "	," & StrDBName & "..hrclasses_master.name AS Allocation " & _
'                                       "	,0 AS AllocationLevel " & _
'                                       "	,ISNULL(assign,0) AS assign " & _
'                                       "	,ISNULL(CompId," & intComId & ") AS CompId " & _
'                                       "	,ISNULL(YearId," & intYearId & ")AS YearId " & _
'                                       "	,0 AS IsGrp " & _
'                                       "FROM " & StrDBName & "..hrclasses_master " & _
'                                       "LEFT JOIN " & _
'                                       "( " & _
'                                       "	SELECT " & _
'                                       "	  " & StrDBName & "..hrclasses_master.classesunkid " & _
'                                       "	 ,CASE WHEN hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.companyunkid, 0) AS CompId " & _
'                                       "	 ,ISNULL(hrmsConfiguration..cfuseraccess_privilege_master.yearunkid, 0) AS YearId " & _
'                                       "	 ,0 AS IsGrp " & _
'                                       "	FROM " & StrDBName & "..hrclasses_master " & _
'                                       "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid  = " & StrDBName & "..hrclasses_master.classesunkid " & _
'                                       "	JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
'                                       "        AND referenceunkid = '" & IntKey & "' " & _
'                                       "		AND hrmsConfiguration..cfuseraccess_privilege_master.userunkid = '" & intUserId & "' AND hrmsConfiguration..cfuseraccess_privilege_master.companyunkid = '" & intComId & "' " & _
'                                       "		AND hrmsConfiguration..cfuseraccess_privilege_master.yearunkid = '" & intYearId & "' " & _
'                                       "	WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 " & _
'                                       ") AS A ON " & StrDBName & "..hrclasses_master.classesunkid = A.classesunkid " & _
'                                       "WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 "
'                        End Select
'                    Else
'                        Select Case IntKey
'                            Case enAllocation.JOBS
'                                StrQ = "SELECT " & _
'                                               "     " & StrDBName & "..hrjob_master.jobunkid AS AllocationId " & _
'                                               "    ," & StrDBName & "..hrjob_master.job_name AS Allocation " & _
'                                               "    ,ISNULL(" & StrDBName & "..hrjob_master.job_level,0) AS AllocationLevel " & _
'                                        ",0 AS assign " & _
'                                        "," & intComId & " AS CompId " & _
'                                        "," & intYearId & " AS YearId " & _
'                                        ",0 AS IsGrp " & _
'                                        "FROM " & StrDBName & "..hrjob_master " & _
'                                        "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
'                            Case enAllocation.DEPARTMENT
'                                StrQ = "SELECT " & _
'                                       "	 " & StrDBName & "..hrdepartment_master.departmentunkid AS AllocationId " & _
'                                       "	," & StrDBName & "..hrdepartment_master.name AS Allocation " & _
'                                       "	,0 AS AllocationLevel " & _
'                                       "	,0 AS assign " & _
'                                       "	," & intComId & " AS CompId " & _
'                                       "	," & intComId & " AS YearId " & _
'                                       "    ,0 AS IsGrp " & _
'                                       "FROM " & StrDBName & "..hrdepartment_master " & _
'                                       "WHERE ISNULL(" & StrDBName & "..hrdepartment_master.isactive, 0) = 1 "
'                            Case enAllocation.CLASS_GROUP
'                                StrQ = "SELECT " & _
'                                       "	 " & StrDBName & "..hrclassgroup_master.classgroupunkid AS AllocationId " & _
'                                       "	," & StrDBName & "..hrclassgroup_master.name AS Allocation " & _
'                                       "	,0 AS AllocationLevel " & _
'                                       "	,0 AS assign " & _
'                                       "	," & intComId & " AS CompId " & _
'                                       "	," & intComId & " AS YearId " & _
'                                       "    ,0 AS IsGrp " & _
'                                       "FROM " & StrDBName & "..hrclassgroup_master " & _
'                                       "WHERE ISNULL(" & StrDBName & "..hrclassgroup_master.isactive, 0) = 1 "
'                            Case enAllocation.CLASSES
'                                StrQ = "SELECT " & _
'                                       "	 " & StrDBName & "..hrclasses_master.classesunkid AS AllocationId " & _
'                                       "	," & StrDBName & "..hrclasses_master.name AS Allocation " & _
'                                       "	,0 AS AllocationLevel " & _
'                                       "	,0 AS assign " & _
'                                       "	," & intComId & " AS CompId " & _
'                                       "	," & intComId & " AS YearId " & _
'                                       "    ,0 AS IsGrp " & _
'                                       "FROM " & StrDBName & "..hrclasses_master " & _
'                                       "WHERE ISNULL(" & StrDBName & "..hrclasses_master.isactive, 0) = 1 "
'                        End Select
'                    End If

'                    dsTranList = objDataOperation.ExecQuery(StrQ, "TranList")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If


'                    'S.SANDEEP [ 31 MAY 2012 ] -- START
'                    'ENHANCEMENT : TRA DISCIPLINE CHANGES
'                    'If intUserId > 0 Then
'                    '    'Dim dtTemp As DataRow() = dsTranList.Tables("TranList").Select("CompId = " & intComId & " AND IsGrp = 0 AND YearId = " & intYearId)
'                    '    'If dtTemp.Length = intRowCnt Then
'                    '    '    dtURow.Item("IsAssign") = 1
'                    '    'End If
'                    'End If
'                    'S.SANDEEP [ 31 MAY 2012 ] -- END

'                    Dim StrGrp As String = ""
'                    Select Case IntKey
'                        Case enAllocation.JOBS
'                            StrGrp = "[ " & enAllocation.JOBS.ToString & " ]"
'                        Case enAllocation.DEPARTMENT
'                            StrGrp = "[ " & enAllocation.DEPARTMENT.ToString & " ]"
'                        Case enAllocation.CLASS_GROUP
'                            StrGrp = "[ " & enAllocation.CLASS_GROUP.ToString.Replace("_", " ") & " ]"
'                        Case enAllocation.CLASSES
'                            StrGrp = "[ " & enAllocation.CLASSES.ToString & " ]"
'                    End Select

'                    Dim dtURow As DataRow = dtUAccess.NewRow

'                    dtURow.Item("UserAccess") = dtRow.Item("GrpName") & Space(5) & StrGrp
'                    dtURow.Item("CompanyId") = dtRow.Item("CompId")
'                    dtURow.Item("YearId") = dtRow.Item("YearId")
'                    dtURow.Item("IsGrp") = dtRow.Item("IsGrp")
'                    dtURow.Item("TotAllocation") = intRowCnt
'                    dtURow.Item("AllocationRefId") = IntKey

'                    dtUAccess.Rows.Add(dtURow)

'                    For Each dtTRow As DataRow In dsTranList.Tables("TranList").Rows
'                        dtURow = dtUAccess.NewRow
'                        'Select Case menAllocation
'                        '    Case enAllocation.JOBS
'                        '        dtURow.Item("UserAccess") = Space(5) & dtTRow.Item("Allocation").ToString
'                        'Case enAllocation.DEPARTMENT
'                        dtURow.Item("UserAccess") = Space(5) & dtTRow.Item("Allocation").ToString
'                        'End Select
'                        dtURow.Item("CompanyId") = dtRow.Item("CompId")
'                        dtURow.Item("YearId") = dtRow.Item("YearId")
'                        dtURow.Item("IsGrp") = dtTRow.Item("IsGrp")
'                        dtURow.Item("IsAssign") = dtTRow.Item("assign")
'                        dtURow.Item("AllocationId") = dtTRow.Item("AllocationId")
'                        dtURow.Item("AllocationLevel") = dtTRow.Item("AllocationLevel")
'                        dtURow.Item("TotAllocation") = intRowCnt
'                        dtURow.Item("AllocationRefId") = IntKey
'                        dtUAccess.Rows.Add(dtURow)
'                    Next
'                Next
'            Next

'            'S.SANDEEP [ 31 MAY 2012 ] -- END



'            Return dtUAccess

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetUserAccessList", mstrModuleName)
'            Return Nothing
'        Finally
'            dtUAccess.Dispose()
'        End Try
'    End Function

'    'S.SANDEEP [ 24 JUNE 2011 ] -- START
'    'ISSUE : USER ACCESS LEVEL PRIVILEGE
'    'Public Function GetUserAccessList(Optional ByVal intUserId As Integer = -1) As DataTable
'    '    Dim dtUAccess As New DataTable("Access")
'    '    Dim StrQ As String = String.Empty
'    '    Dim exForce As Exception
'    '    Dim dsList As New DataSet
'    '    Dim dsTranList As New DataSet
'    '    Dim StrDBName As String = String.Empty
'    '    Dim intComId As Integer = 0
'    '    Dim intYearId As Integer = 0
'    '    Dim intRowCnt As Integer = 0
'    '    Try
'    '        objDataOperation = New clsDataOperation

'    '        dtUAccess.Columns.Add("IsAssign", System.Type.GetType("System.Boolean")).DefaultValue = 0
'    '        dtUAccess.Columns.Add("UserAccess", System.Type.GetType("System.String")).DefaultValue = ""
'    '        dtUAccess.Columns.Add("CompanyId", System.Type.GetType("System.Int32")).DefaultValue = -1
'    '        dtUAccess.Columns.Add("YearId", System.Type.GetType("System.Int32")).DefaultValue = -1
'    '        dtUAccess.Columns.Add("JobId", System.Type.GetType("System.Int32")).DefaultValue = -1
'    '        dtUAccess.Columns.Add("JobLevel", System.Type.GetType("System.Int32")).DefaultValue = -1
'    '        dtUAccess.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = 0
'    '        dtUAccess.Columns.Add("TotJob", System.Type.GetType("System.Int32")).DefaultValue = 0

'    '        StrQ = "SELECT " & _
'    '               "	 ISNULL(hrmsConfiguration..cfcompany_master.name,'')+' - ( '+ ISNULL(dbo.cffinancial_year_tran.database_name,'')+' )' AS GrpName " & _
'    '               "	,hrmsConfiguration..cfcompany_master.companyunkid AS CompId " & _
'    '               "	,hrmsConfiguration..cffinancial_year_tran.yearunkid AS YearId " & _
'    '               "	,1 AS IsGrp " & _
'    '               "	,hrmsConfiguration..cffinancial_year_tran.database_name AS DBName " & _
'    '               "FROM hrmsConfiguration..cfcompany_master " & _
'    '               "	JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cfcompany_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
'    '               "    JOIN sys.databases ON sys.databases.name = hrmsConfiguration..cffinancial_year_tran.database_name " & _
'    '               "WHERE hrmsConfiguration..cfcompany_master.isactive = 1 " & _
'    '               "ORDER BY hrmsConfiguration..cfcompany_master.companyunkid " 'S.SANDEEP [ 04 FEB 2012 sys.databases ] -- START - END

'    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
'    '            Dim dtURow As DataRow = dtUAccess.NewRow

'    '            StrDBName = "" : intComId = 0 : intYearId = 0

'    '            StrDBName = dtRow.Item("DBName").ToString.Trim
'    '            intComId = dtRow.Item("CompId")
'    '            intYearId = dtRow.Item("YearId")

'    '            If intUserId > 0 Then
'    '                StrQ = "SELECT " & _
'    '                        "	 " & StrDBName & "..hrjob_master.jobunkid " & _
'    '                        ",   " & StrDBName & "..hrjob_master.job_name " & _
'    '                        ",   ISNULL(" & StrDBName & "..hrjob_master.job_level,0) AS job_level " & _
'    '                        ",   CASE WHEN hrmsConfiguration..cfuseraccess_privilege.jobunkid IS NULL THEN 0 ELSE 1 END AS assign " & _
'    '                        ",   ISNULL(hrmsConfiguration..cfuseraccess_privilege.companyunkid,0) AS CompId " & _
'    '                        ",   ISNULL(hrmsConfiguration..cfuseraccess_privilege.yearunkid,0) AS YearId " & _
'    '                        ",   0 AS IsGrp " & _
'    '                        "FROM " & StrDBName & "..hrjob_master " & _
'    '                        "	LEFT OUTER JOIN hrmsConfiguration..cfuseraccess_privilege ON " & StrDBName & "..hrjob_master.jobunkid = hrmsConfiguration..cfuseraccess_privilege.jobunkid " & _
'    '                        "		AND hrmsConfiguration..cfuseraccess_privilege.userunkid = " & intUserId & "  " & _
'    '                        "       AND hrmsConfiguration..cfuseraccess_privilege.companyunkid = " & intComId & " " & _
'    '                        "       AND hrmsConfiguration..cfuseraccess_privilege.yearunkid = " & intYearId & " " & _
'    '                        "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
'    '            Else
'    '                StrQ = "SELECT " & _
'    '                        " " & StrDBName & "..hrjob_master.jobunkid " & _
'    '                        "," & StrDBName & "..hrjob_master.job_name " & _
'    '                        ",ISNULL(" & StrDBName & "..hrjob_master.job_level,0) AS job_level " & _
'    '                        ",0 AS assign " & _
'    '                        "," & intComId & " AS CompId " & _
'    '                        "," & intYearId & " AS YearId " & _
'    '                        ",0 AS IsGrp " & _
'    '                        "FROM " & StrDBName & "..hrjob_master " & _
'    '                        "WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1 "
'    '            End If

'    '            intRowCnt = -1
'    '            intRowCnt = objDataOperation.RecordCount("SELECT " & StrDBName & "..hrjob_master.jobunkid FROM " & StrDBName & "..hrjob_master WHERE ISNULL(" & StrDBName & "..hrjob_master.isactive,0) = 1")

'    '            dsTranList = objDataOperation.ExecQuery(StrQ, "TranList")

'    '            If objDataOperation.ErrorMessage <> "" Then
'    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '                Throw exForce
'    '            End If

'    '            If intUserId > 0 Then
'    '                Dim dtTemp As DataRow() = dsTranList.Tables("TranList").Select("CompId = " & intComId & " AND IsGrp = 0 AND YearId = " & intYearId)

'    '                If dtTemp.Length = intRowCnt Then
'    '                    dtURow.Item("IsAssign") = 1
'    '                End If
'    '            End If


'    '            dtURow.Item("UserAccess") = dtRow.Item("GrpName")
'    '            dtURow.Item("CompanyId") = dtRow.Item("CompId")
'    '            dtURow.Item("YearId") = dtRow.Item("YearId")
'    '            dtURow.Item("IsGrp") = dtRow.Item("IsGrp")
'    '            dtURow.Item("TotJob") = intRowCnt

'    '            dtUAccess.Rows.Add(dtURow)





'    '            For Each dtTRow As DataRow In dsTranList.Tables("TranList").Rows
'    '                dtURow = dtUAccess.NewRow

'    '                dtURow.Item("UserAccess") = Space(5) & dtTRow.Item("job_name").ToString & " - " & Language.getMessage(mstrModuleName, 4, "Level : ") & dtTRow.Item("job_level").ToString
'    '                dtURow.Item("CompanyId") = dtRow.Item("CompId")
'    '                dtURow.Item("YearId") = dtRow.Item("YearId")
'    '                dtURow.Item("IsGrp") = dtTRow.Item("IsGrp")
'    '                dtURow.Item("IsAssign") = dtTRow.Item("assign")
'    '                dtURow.Item("JobId") = dtTRow.Item("jobunkid")
'    '                dtURow.Item("JobLevel") = dtTRow.Item("job_level")
'    '                dtURow.Item("TotJob") = intRowCnt

'    '                dtUAccess.Rows.Add(dtURow)
'    '            Next
'    '        Next

'    '        Return dtUAccess

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "GetUserAccessList", mstrModuleName)
'    '        Return Nothing
'    '    Finally
'    '        dtUAccess.Dispose()
'    '    End Try
'    'End Function

'    'S.SANDEEP [ 04 FEB 2012 ] -- END







'    'S.SANDEEP [ 04 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function IsAccessExist(ByVal intUserId As Integer, _
'                                  ByVal intCompId As Integer, _
'                                  ByVal intYearId As Integer, _
'                                  ByVal intAllocation As Integer, _
'                                  ByVal intRefId As Integer, _
'                                  Optional ByVal intLevel As Integer = -1) As Integer 'S.SANDEEP [ 31 MAY 2012 ] -- START -- END
'        'Public Function IsAccessExist(ByVal intUserId As Integer, ByVal intCompId As Integer, ByVal intYearId As Integer, ByVal intAllocation As Integer, ByVal menAllocation As enAllocation, Optional ByVal intLevel As Integer = -1) As Integer
'        Dim dsList As New DataSet
'        Dim StrQ As String = ""
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = "SELECT " & _
'                   "    hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
'                   "FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
'                   "    JOIN hrmsConfiguration..cfuseraccess_privilege_tran ON hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
'                   "WHERE userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND allocationunkid = @AllocationId AND referenceunkid = '" & intRefId & "' "

'            If intLevel > 0 Then
'                StrQ &= " AND allocationlevel = @ALevel "
'                objDataOperation.AddParameter("@ALevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)
'            End If

'            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
'            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
'            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
'            objDataOperation.AddParameter("@AllocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocation)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                Return dsList.Tables(0).Rows(0)("useraccessprivilegeunkid")
'            Else
'                Return 0
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Function

'    'S.SANDEEP [ 31 MAY 2012 ] -- START
'    'ENHANCEMENT : TRA DISCIPLINE CHANGES
'    'Public Function InsertAccess(ByVal intUserId As Integer, _
'    '                             ByVal intCompId As Integer, _
'    '                             ByVal intYearId As Integer, _
'    '                             ByVal intAllocationId As Integer, _
'    '                             ByVal intAllocationLevel As Integer, _
'    '                             ByVal menAllocation As enAllocation) As Boolean
'    Public Function InsertAccess(ByVal intUserId As Integer, _
'                                 ByVal intCompId As Integer, _
'                                 ByVal intYearId As Integer, _
'                                 ByVal intAllocationId As Integer, _
'                                 ByVal intAllocationLevel As Integer, _
'                                 ByVal intRefId As Integer) As Boolean
'        'S.SANDEEP [ 31 MAY 2012 ] -- END
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Dim intAccessUnkid As Integer = -1
'        Dim objDataOperation As New clsDataOperation
'        Try
'            objDataOperation.BindTransaction()


'            StrQ = " SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE " & _
'                   " userunkid = @UserId AND companyunkid = @CompId AND yearunkid = @YearId AND referenceunkid = @RefId "

'            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
'            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
'            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
'            objDataOperation.AddParameter("@RefId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefId)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                intAccessUnkid = dsList.Tables("List").Rows(0)(0)
'            End If

'            If intAccessUnkid <= 0 Then
'                StrQ = "INSERT INTO hrmsConfiguration..cfuseraccess_privilege_master (userunkid,companyunkid,yearunkid,referenceunkid) " & _
'                                   "VALUES (@UserId,@CompId,@YearId,@RefId); SELECT @@identity "

'                objDataOperation.ClearParameters()
'                objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
'                objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
'                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
'                objDataOperation.AddParameter("@RefId", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefId)

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                intAccessUnkid = dsList.Tables("List").Rows(0)(0)
'            End If

'            StrQ = "INSERT INTO hrmsConfiguration..cfuseraccess_privilege_tran (useraccessprivilegeunkid,allocationunkid,allocationlevel) " & _
'                   "VALUES (" & intAccessUnkid & ",@AllocationId,@AllocationLevel) "

'            objDataOperation.AddParameter("@AllocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationId)
'            objDataOperation.AddParameter("@AllocationLevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationLevel)

'            objDataOperation.ExecNonQuery(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            DisplayError.Show("-1", ex.Message, "InsertAccess", mstrModuleName)
'            Return False
'        Finally
'        End Try
'    End Function

'    'S.SANDEEP [ 31 MAY 2012 ] -- START
'    'ENHANCEMENT : TRA DISCIPLINE CHANGES
'    'Public Function DeleteAccess(ByVal intUserId As Integer, ByVal menAllocation As enAllocation) As Boolean

'    'S.SANDEEP [ 01 JUNE 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Public Function DeleteAccess(ByVal intUserId As Integer, ByVal intRefId As Integer) As Boolean
'    Public Function DeleteAccess(ByVal intUserId As Integer, ByVal intRefId As Integer, ByVal intCompanyId As Integer, ByVal intYearId As Integer) As Boolean
'        'S.SANDEEP [ 01 JUNE 2012 ] -- END

'        'S.SANDEEP [ 31 MAY 2012 ] -- END
'        Dim exForce As Exception
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet

'        objDataOperation = New clsDataOperation
'        Try
'            objDataOperation.BindTransaction()


'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'StrQ = "SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = '" & intUserId & "' " & _
'            '       " AND referenceunkid = '" & intRefId & "' "

'            'S.SANDEEP [ 20 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = '" & intUserId & "' " & _
'            '       " AND referenceunkid = '" & intRefId & "' AND companyunkid = '" & intCompanyId & "' AND yearunkid = '" & intYearId & "' "

'            'S.SANDEEP [ 26 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = '" & intUserId & "' " & _
'            '       " AND referenceunkid = '" & intRefId & "'"

'            StrQ = "SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = '" & intUserId & "' " & _
'                      " AND referenceunkid = '" & intRefId & "' AND companyunkid = '" & intCompanyId & "' AND yearunkid = '" & intYearId & "' "
'            'S.SANDEEP [ 26 NOV 2012 ] -- END


'            'S.SANDEEP [ 20 NOV 2012 ] -- END


'            'S.SANDEEP [ 01 JUNE 2012 ] -- END

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dRow As DataRow In dsList.Tables("List").Rows
'                StrQ = "DELETE FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE useraccessprivilegeunkid = '" & dRow.Item("useraccessprivilegeunkid") & "'; " & _
'                       " DELETE FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid = '" & dRow.Item("useraccessprivilegeunkid") & "' "

'                objDataOperation.ExecNonQuery(StrQ)

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'            Next

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            DisplayError.Show("-1", ex.Message, "DeleteAccess", mstrModuleName)
'            Return False
'        Finally
'        End Try
'    End Function
'    'Public Function InsertAccess(ByVal intUserId As Integer, _
'    '                             ByVal intCompId As Integer, _
'    '                             ByVal intYearId As Integer, _
'    '                             ByVal intJobId As Integer, _
'    '                             ByVal intLevel As Integer) As Boolean
'    '    Dim StrQ As String = String.Empty
'    '    Dim exForce As Exception
'    '    Try
'    '        objDataOperation = New clsDataOperation

'    '        StrQ = "INSERT INTO hrmsConfiguration..cfuseraccess_privilege (userunkid,jobunkid,job_level,companyunkid,yearunkid) " & _
'    '               "VALUES (@UserId,@JobId,@JobLevel,@CompId,@YearId) "

'    '        objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
'    '        objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
'    '        objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
'    '        objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
'    '        objDataOperation.AddParameter("@JobLevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)

'    '        objDataOperation.ExecNonQuery(StrQ)

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Return True

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "InsertAccess", mstrModuleName)
'    '        Return False
'    '    Finally
'    '    End Try
'    'End Function
'    'Public Function IsAccessExist(ByVal intUserId As Integer, _
'    '                              ByVal intCompId As Integer, _
'    '                              ByVal intYearId As Integer, _
'    '                              ByVal intJobId As Integer, _
'    '                              ByVal intLevel As Integer) As Integer

'    '    Dim dsList As New DataSet
'    '    Dim StrQ As String = ""
'    '    Dim exForce As Exception
'    '    Try
'    '        objDataOperation = New clsDataOperation

'    '        StrQ = "SELECT " & _
'    '                "   useraccessprivilegeunkid " & _
'    '                "FROM hrmsConfiguration..cfuseraccess_privilege " & _
'    '                "WHERE userunkid = @UserId AND jobunkid = @JobId " & _
'    '                "   AND job_level = @JobLevel AND companyunkid = @CompId " & _
'    '                "   AND yearunkid = @YearId "

'    '        objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
'    '        objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompId)
'    '        objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
'    '        objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
'    '        objDataOperation.AddParameter("@JobLevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)

'    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        If dsList.Tables(0).Rows.Count > 0 Then
'    '            Return dsList.Tables(0).Rows(0)("useraccessprivilegeunkid")
'    '        Else
'    '            Return 0
'    '        End If

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'    '    End Try
'    'End Function
'    'Public Function DeleteAccess(ByVal intUserId As Integer) As Boolean
'    '    Dim exForce As Exception
'    '    Dim StrQ As String = String.Empty
'    '    Try
'    '        objDataOperation = New clsDataOperation

'    '        StrQ = "DELETE FROM hrmsConfiguration..cfuseraccess_privilege WHERE userunkid = " & intUserId


'    '        objDataOperation.ExecNonQuery(StrQ)

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Return True

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "DeleteAccess", mstrModuleName)
'    '        Return False
'    '    Finally
'    '    End Try
'    'End Function
'    'S.SANDEEP [ 04 FEB 2012 ] -- END



'    'S.SANDEEP [ 24 JUNE 2011 ] -- END 


'    'S.SANDEEP [ 28 JULY 2011 ] -- START
'    'ENHANCEMENT : CHANGE IN LOGIN FLOW. 
'    Public Function IsValidLogin(ByVal StrLoginName As String, ByVal StrPassword As String, ByVal IntLoginModId As Integer, ByVal StrDataBase As String, Optional ByVal IsADUserAlllow As Boolean = False) As Integer
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim iRetValue As Integer = -1
'        Dim iUserId As Integer = -1
'        Dim dsList As New DataSet
'        Dim StrMessage As String = String.Empty
'        Dim objUAL As New clsUser_Acc_Lock
'        Try
'            mstrMessage = ""
'            objDataOperation = New clsDataOperation
'            iUserId = Return_UserId(StrLoginName, StrDataBase, IntLoginModId)

'            'S.SANDEEP [ 09 NOV 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            intRetUserId = iUserId
'            'S.SANDEEP [ 09 NOV 2012 ] -- END

'            Select Case IntLoginModId
'                Case enLoginMode.USER
'                    StrQ = "SELECT " & _
'                           " " & StrDataBase & "..cfuser_master.userunkid AS LId " & _
'                           "FROM " & StrDataBase & "..cfuser_master " & _
'                           "WHERE " & StrDataBase & "..cfuser_master.username = @LName " & _
'                           "    AND " & StrDataBase & "..cfuser_master.password = @LPass " & _
'                           "    AND " & StrDataBase & "..cfuser_master.isactive = 1 "


'                    'Pinkal (12-Oct-2011) -- Start
'                    'Enhancement : TRA Changes
'                    If iUserId <> 1 Then
'                        StrQ &= " AND isnull(isaduser,0) = @IsADUserAlllow"
'                    End If
'                    'Pinkal (12-Oct-2011) -- End


'                Case enLoginMode.EMPLOYEE
'                    StrQ = ""
'            End Select
'            objDataOperation.AddParameter("@LName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrLoginName.ToString)
'            objDataOperation.AddParameter("@LPass", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeCommonLib.clsSecurity.Encrypt(StrPassword, "ezee"))


'            'Pinkal (12-Oct-2011) -- Start
'            'Enhancement : TRA Changes
'            objDataOperation.AddParameter("@IsADUserAlllow", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IsADUserAlllow.ToString())
'            'Pinkal (12-Oct-2011) -- End


'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If iUserId > 0 And iUserId <> 1 Then
'                _Userunkid = iUserId
'                'S.SANDEEP [ 01 DEC 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                If _EmployeeUnkid > 0 AndAlso _IsManager = False Then
'                    mstrMessage = "Sorry, you cannot login. Reason : You are not authorized to login as manager. Please contact Administrator."
'                    iRetValue = -1
'                    GoTo 10
'                End If

'                If _EmployeeUnkid > 0 AndAlso _IsManager = True Then
'                    If _Reinstatement_Date = Nothing Then
'                        'CHECKING FOR SUSPENSION DATES >> START
'                        If _Suspended_To_Date <> Nothing AndAlso _Suspended_From_Date <> Nothing Then
'                            If ConfigParameter._Object._CurrentDateAndTime.Date <= _Suspended_To_Date.Date And _
'                               ConfigParameter._Object._CurrentDateAndTime.Date >= _Suspended_From_Date.Date Then
'                                mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
'                                iRetValue = -1
'                                GoTo 10
'                            End If
'                        End If
'                        'CHECKING FOR SUSPENSION DATES >> END

'                        'Anjan [28 May 2014] -- Start
'                        'ENHANCEMENT : Requested by Rutta/Andrew as at TRA and AKFTZ employees were not able to login on probabted period.
'                        'CHECKING FOR PROBATION DATES >> START
'                        'If _Probation_From_Date <> Nothing AndAlso _Probation_To_Date <> Nothing Then
'                        '    If ConfigParameter._Object._CurrentDateAndTime.Date <= _Probation_To_Date.Date And _
'                        '       ConfigParameter._Object._CurrentDateAndTime.Date >= _Probation_From_Date.Date Then
'                        '        mstrMessage = "Sorry, you cannot login. Reason : You are in probation period. Please contact Administrator."
'                        '        iRetValue = -1
'                        '        GoTo 10
'                        '    End If
'                        'End If
'                        'CHECKING FOR PROBATION DATES >> END
'                        'Anjan [28 May 2014] -- End

'                        'CHECKING FOR END OF CONTRACT DATES >> START
'                        If _Empl_Enddate <> Nothing Then
'                            If ConfigParameter._Object._CurrentDateAndTime.Date >= _Empl_Enddate.Date Then
'                                mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
'                                iRetValue = -1
'                                GoTo 10
'                            End If
'                        End If
'                        'CHECKING FOR END OF CONTRACT DATES >> END

'                        'CHECKING FOR TERMINATION DATE >> START
'                        If _Termination_Date <> Nothing Then
'                            If ConfigParameter._Object._CurrentDateAndTime.Date >= _Termination_Date.Date Then
'                                mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
'                                iRetValue = -1
'                                GoTo 10
'                            End If
'                        End If
'                        'CHECKING FOR TERMINATION DATE >> END

'                        'CHECKING FOR RETIREMENT DATE >> START
'                        If _Retirement_Date <> Nothing Then
'                            If ConfigParameter._Object._CurrentDateAndTime.Date >= _Retirement_Date.Date Then
'                                mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
'                                iRetValue = -1
'                                GoTo 10
'                            End If
'                        End If
'                        'CHECKING FOR RETIREMENT DATE >> END
'                    Else
'                        If ConfigParameter._Object._CurrentDateAndTime.Date < _Reinstatement_Date.Date Then
'                            mstrMessage = "Sorry, you cannot login. Reason : Your Reinstatement date is less than the today date. Please contact Administrator."
'                            iRetValue = -1
'                            GoTo 10
'                        End If

'                        'If _Empl_Enddate <> Nothing Then
'                        '    If _Reinstatement_Date.Date <= _Empl_Enddate.Date Then
'                        '        mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
'                        '        iRetValue = -1
'                        '        GoTo 10
'                        '    End If
'                        'End If
'                        'If _Termination_Date <> Nothing Then
'                        '    If _Reinstatement_Date.Date <= _Termination_Date.Date Then
'                        '        mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
'                        '        iRetValue = -1
'                        '        GoTo 10
'                        '    End If
'                        'End If
'                        'If _Suspended_From_Date <> Nothing Then
'                        '    If _Reinstatement_Date.Date <= _Suspended_From_Date.Date Then
'                        '        mstrMessage = "Sorry, you cannot login. Reason : Your Reinstatement date is less than the suspension date. Please contact Administrator."
'                        '        iRetValue = -1
'                        '        GoTo 10
'                        '    End If
'                        'End If
'                        'If _Probation_To_Date <> Nothing Then
'                        '    If _Reinstatement_Date.Date <= _Probation_To_Date.Date Then
'                        '        mstrMessage = "Sorry, you cannot login. Reason : Your Reinstatement date is less than the probation date. Please contact Administrator."
'                        '        iRetValue = -1
'                        '        GoTo 10
'                        '    End If
'                        'End If
'                    End If
'                End If
'                'S.SANDEEP [ 01 DEC 2012 ] -- END
'                Dim intDaysDiff As Integer = 0
'                If _Exp_Days.ToString.Length = 8 Then
'                    Dim dtDate As Date = CDate(eZeeDate.convertDate(_Exp_Days.ToString).ToShortDateString)
'                    intDaysDiff = CInt(DateDiff(DateInterval.Day, dtDate, ConfigParameter._Object._CurrentDateAndTime.AddDays(-1)))
'                    If intDaysDiff >= 0 Then
'                        mstrMessage = "Sorry, you cannot login. Reason : Your Password is expired. Please contact Administrator."

'                        'S.SANDEEP [ 09 NOV 2012 ] -- START
'                        'ENHANCEMENT : TRA CHANGES
'                        blnShowChangePasswdfrm = True
'                        'S.SANDEEP [ 09 NOV 2012 ] -- END

'                        iRetValue = -1
'                        GoTo 10
'                    End If
'                End If

'                'Sohail (04 Jul 2012) -- Start
'                'TRA - ENHANCEMENT
'                If _Relogin_Date.Date <> Nothing AndAlso _Relogin_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
'                    mstrMessage = "Sorry, You can not relogin before " & _Relogin_Date.ToShortDateString & "."
'                    iRetValue = -1
'                    GoTo 10
'                    'S.SANDEEP [ 29 JAN 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                ElseIf _Relogin_Date.Date <> Nothing AndAlso _Relogin_Date.Date = ConfigParameter._Object._CurrentDateAndTime.Date Then
'                    If _Relogin_Date.ToString("HH:mm") > ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm") Then
'                        mstrMessage = "Sorry, You can relogin today after " & _Relogin_Date.ToShortTimeString & "."
'                        iRetValue = -1
'                        GoTo 10
'                    End If
'                    'S.SANDEEP [ 29 JAN 2013 ] -- END
'                ElseIf _Relogin_Date.Date <> Nothing Then
'                    _Relogin_Date = Nothing
'                    If Update(True) = False Then
'                        iRetValue = -1
'                        GoTo 10
'                    End If
'                    _Userunkid = iUserId
'                End If
'                'Sohail (04 Jul 2012) -- End

'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                If _IsAcc_Locked = True Then
'                    Dim StrMsg As String = ""
'                    StrMsg = Language.getMessage(mstrModuleName, 5, "You can relogin after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
'                    StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
'                    mstrMessage = "Sorry, You cannot login, Your account is locked." & " " & StrMsg
'                    iRetValue = -1
'                    GoTo 10
'                End If
'                iRetValue = dsList.Tables("List").Rows(0).Item("LId")
'            Else
'                Dim intTry As Integer = 0
'                If iUserId > 0 And iUserId <> 1 And iRetValue <= 0 Then
'                    IIf(_Userunkid <= 0, _Userunkid = iUserId, _Userunkid)
'                    If _IsAcc_Locked = True Then
'                        Dim StrMsg As String = ""
'                        If PWDOptions._IsUnlockedByAdmin = True Then
'                            StrMsg = Language.getMessage(mstrModuleName, 11, "Please contact Administrator.")
'                        Else
'                            StrMsg = Language.getMessage(mstrModuleName, 5, "You can relogin after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
'                            StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
'                        End If
'                        mstrMessage = "Sorry, You cannot login, Your account is locked." & " " & StrMsg
'                        GoTo 10
'                    End If
'                    If PWDOptions._IsAccLockSet = True Then
'                        intTry = PWDOptions._LockUserAfter
'                        'S.SANDEEP [ 09 MAR 2013 ] -- START
'                        'ENHANCEMENT : TRA CHANGES
'                        objUAL._Attempt_Date = ConfigParameter._Object._CurrentDateAndTime
'                        'S.SANDEEP [ 09 MAR 2013 ] -- END
'                        objUAL._Userunkid = iUserId
'                        If PWDOptions._LockUserAfter = objUAL._Attempts Then
'                            Dim StrMsg As String = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot login, your account is locked.")
'                            If PWDOptions._IsUnlockedByAdmin = False Then
'                                StrMsg &= Language.getMessage(mstrModuleName, 5, "You can relogin after ") & PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 7, " Minute(s).")
'                            End If
'                            StrMsg &= Language.getMessage(mstrModuleName, 6, " Or Please contact Administrator.")
'                            mstrMessage = StrMsg
'                        End If
'                        If objUAL._Attempts <> 0 Then
'                            intTry = PWDOptions._LockUserAfter - objUAL._Attempts
'                        End If
'                        StrMessage = Language.getMessage(mstrModuleName, 8, " As per password policy settings your account will be locked after ") & PWDOptions._LockUserAfter & Language.getMessage(mstrModuleName, 9, " attempts. And you have ") & intTry - 1 & Language.getMessage(mstrModuleName, 10, " attempts left.")
'                    End If

'                    If StrMessage.Trim.Length > 0 Then
'                        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.") & " " & StrMessage
'                        StrMessage = String.Empty
'                        objUAL._Userunkid = iUserId
'                        objUAL._Ip = getIP()
'                        objUAL._Machine_Name = getHostName()
'                        objUAL._Unlockuserunkid = 0
'                        objUAL._Isunlocked = objUAL._Isunlocked
'                        Dim dtTime As DateTime = ConfigParameter._Object._CurrentDateAndTime
'                        If objUAL._Attempts <> 0 Then
'                            objUAL._Attempts = objUAL._Attempts + 1
'                            If objUAL._Attempts = PWDOptions._LockUserAfter Then
'                                _IsAcc_Locked = True
'                                _Locked_Time = ConfigParameter._Object._CurrentDateAndTime
'                                If PWDOptions._IsUnlockedByAdmin = False Then
'                                    _LockedDuration = dtTime.AddMinutes(PWDOptions._AlloUserRetry)
'                                Else
'                                    _LockedDuration = Nothing
'                                End If
'                                Update(True)
'                                objUAL._Lockedtime = ConfigParameter._Object._CurrentDateAndTime
'                            End If
'                            Call objUAL.Update()
'                        Else
'                            objUAL._Attempts = 1
'                            objUAL._Lockedtime = Nothing
'                            Call objUAL.Insert()
'                        End If
'                    Else
'                        iRetValue = -1
'                        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.")
'                    End If
'                Else
'                    iRetValue = -1
'                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.")
'                End If
'            End If

'10:         Return iRetValue

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValidLogin", mstrModuleName)
'        Finally
'            StrQ = String.Empty
'            dsList.Dispose()
'        End Try
'    End Function


'    'S.SANDEEP [ 20 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Private Function Return_UserId(ByVal StrUserName As String, ByVal StrDBName As String, ByVal IntLModeId As Integer) As Integer
'    Public Function Return_UserId(ByVal StrUserName As String, ByVal StrDBName As String, ByVal IntLModeId As Integer) As Integer
'        'S.SANDEEP [ 20 NOV 2012 ] -- END
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Try
'            objDataOperation = New clsDataOperation

'            Select Case IntLModeId
'                Case enLoginMode.USER
'                    StrQ = "SELECT " & _
'                           " " & StrDBName & "..cfuser_master.userunkid AS LId " & _
'                           "FROM " & StrDBName & "..cfuser_master " & _
'                           "WHERE " & StrDBName & "..cfuser_master.username = @UName " & _
'                           "   AND " & StrDBName & "..cfuser_master.isactive = 1 "

'                    'S.SANDEEP [ 06 DEC 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    Dim objPwd As New clsPassowdOptions

'                    Dim drOption As DataRow() = objPwd._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

'                    If drOption.Length > 0 Then

'                        If objPwd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
'                            If objPwd._IsEmployeeAsUser = False Then
'                                StrQ &= "   AND (ISNULL(" & StrDBName & "..cfuser_master.employeeunkid,0) < = 0 OR userunkid =1)"
'                            Else
'                                StrQ &= "   AND (ISNULL(" & StrDBName & "..cfuser_master.employeeunkid,0) > 0 OR userunkid =1)"
'                            End If
'                        ElseIf objPwd._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Or objPwd._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
'                            StrQ &= " AND (isnull(isaduser,0) = 1 OR userunkid =1) "

'                        End If

'                    End If



'                    'S.SANDEEP [ 06 DEC 2012 ] -- END

'                Case enLoginMode.EMPLOYEE

'            End Select


'            objDataOperation.AddParameter("@UName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrUserName.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                Return dsList.Tables(0).Rows(0).Item("LId")
'            Else
'                Return -1
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Return_UserId", mstrModuleName)
'            Return -1
'        Finally
'            StrQ = Nothing : dsList.Dispose()
'        End Try
'    End Function



'    'Pinkal (12-Oct-2011) -- Start
'    'Enhancement : TRA Changes

'    Public Function ImportADUser(ByVal dtUser As DataTable) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Try

'            If dtUser.Rows.Count <= 0 Then Return True

'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()


'            'Pinkal (21-Jun-2012) -- Start
'            'Enhancement : TRA Changes

'            'strQ = "INSERT INTO cfuser_master ( " & _
'            '          "  username " & _
'            '          ", password " & _
'            '          ", ispasswordexpire " & _
'            '          ", roleunkid " & _
'            '          ", isrighttoleft " & _
'            '          ", exp_days " & _
'            '          ", languageunkid " & _
'            '          ", isactive" & _
'            '          ", creationdate " & _
'            '          ", islocked " & _
'            '          ", lockedtime " & _
'            '          ", lockedduration " & _
'            '          ", createdbyunkid " & _
'            '          ", firstname " & _
'            '          ", lastname " & _
'            '          ", address1 " & _
'            '          ", address2 " & _
'            '          ", phone " & _
'            '          ", email" & _
'            '          ", isaduser " & _
'            '        ") VALUES (" & _
'            '          "  @username " & _
'            '          ", @password " & _
'            '          ", @ispasswordexpire " & _
'            '          ", @roleunkid " & _
'            '          ", @isrighttoleft " & _
'            '          ", @exp_days " & _
'            '          ", @languageunkid " & _
'            '          ", @isactive" & _
'            '          ", @creationdate " & _
'            '          ", @islocked " & _
'            '          ", @lockedtime " & _
'            '          ", @lockedduration " & _
'            '          ", @createdbyunkid " & _
'            '          ", @firstname " & _
'            '          ", @lastname " & _
'            '          ", @address1 " & _
'            '          ", @address2 " & _
'            '          ", @phone " & _
'            '          ", @email" & _
'            '          ", @isaduser " & _
'            '        "); "


'            strQ = "INSERT INTO cfuser_master ( " & _
'                      "  username " & _
'                      ", password " & _
'                      ", ispasswordexpire " & _
'                      ", roleunkid " & _
'                      ", isrighttoleft " & _
'                      ", exp_days " & _
'                      ", languageunkid " & _
'                      ", isactive" & _
'                      ", creationdate " & _
'                      ", islocked " & _
'                      ", lockedtime " & _
'                      ", lockedduration " & _
'                      ", createdbyunkid " & _
'                      ", firstname " & _
'                      ", lastname " & _
'                      ", address1 " & _
'                      ", address2 " & _
'                      ", phone " & _
'                      ", email" & _
'                      ", isaduser " & _
'                    ", ismanager " & _
'                    ", userdomain " & _
'                    ") VALUES (" & _
'                      "  @username " & _
'                      ", @password " & _
'                      ", @ispasswordexpire " & _
'                      ", @roleunkid " & _
'                      ", @isrighttoleft " & _
'                      ", @exp_days " & _
'                      ", @languageunkid " & _
'                      ", @isactive" & _
'                      ", @creationdate " & _
'                      ", @islocked " & _
'                      ", @lockedtime " & _
'                      ", @lockedduration " & _
'                      ", @createdbyunkid " & _
'                      ", @firstname " & _
'                      ", @lastname " & _
'                      ", @address1 " & _
'                      ", @address2 " & _
'                      ", @phone " & _
'                      ", @email" & _
'                      ", @isaduser " & _
'                    ", @ismanager " & _
'                    ", @userdomain " & _
'                    "); "

'            'Pinkal (21-Jun-2012) -- End

'            For i As Integer = 0 To dtUser.Rows.Count - 1

'                If isExist(dtUser.Rows(i)("username").ToString) Then
'                    mstrMessage = Language.getMessage(mstrModuleName, 1, "This User is already defined. Please define new User.")
'                    objDataOperation.ReleaseTransaction(False)
'                    Return False
'                End If

'                objDataOperation.ClearParameters()
'                objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("username").ToString)
'                objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(dtUser.Rows(i)("password").ToString, "ezee").ToString)
'                objDataOperation.AddParameter("@ispasswordexpire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dtUser.Rows(i)("ispasswordexpire").ToString.ToString)
'                objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtUser.Rows(i)("roleunkid").ToString.ToString)
'                objDataOperation.AddParameter("@isrighttoleft", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dtUser.Rows(i)("isrighttoleft").ToString.ToString)
'                objDataOperation.AddParameter("@exp_days", SqlDbType.Int, eZeeDataType.INT_SIZE, dtUser.Rows(i)("exp_days").ToString.ToString)
'                objDataOperation.AddParameter("@languageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtUser.Rows(i)("languageunkid").ToString.ToString)
'                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dtUser.Rows(i)("isactive").ToString.ToString)
'                objDataOperation.AddParameter("@creationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreationDate)
'                objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                objDataOperation.AddParameter("@lockedduration", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                objDataOperation.AddParameter("@createdbyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
'                objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("firstname").ToString.ToString)
'                objDataOperation.AddParameter("@lastname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("lastname").ToString.ToString)
'                objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("address1").ToString.ToString)
'                objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("address2").ToString.ToString)
'                objDataOperation.AddParameter("@phone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("phone").ToString.ToString)
'                objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("email").ToString.ToString)
'                objDataOperation.AddParameter("@isaduser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dtUser.Rows(i)("isaduser").ToString.ToString)


'                'Pinkal (21-Jun-2012) -- Start
'                'Enhancement : TRA Changes
'                objDataOperation.AddParameter("@ismanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsManager)
'                objDataOperation.AddParameter("@userdomain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtUser.Rows(i)("userdomain").ToString.ToString)
'                'Pinkal (21-Jun-2012) -- End


'                objDataOperation.ExecNonQuery(strQ)

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'            Next
'            objDataOperation.ReleaseTransaction(True)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ImportADUser", mstrModuleName)
'            objDataOperation.ReleaseTransaction(False)
'            Return False
'        End Try
'        Return True
'    End Function


'    'Pinkal (12-Oct-2011) -- End

'    'S.SANDEEP [ 28 JULY 2011 ] -- END 

'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function Get_UserBy_PrivilegeId(ByVal intPrivilegeUnkid As Integer, Optional ByVal xYearId As Integer = 0) As DataSet 'S.SANDEEP [ 17 NOV 2014 ] -- START YEAR ID INCLUDED FOR COMPANY ACCESS PRIVILEGE  -- END
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Try
'            objDataOperation = New clsDataOperation

'            'S.SANDEEP [ 17 NOV 2014 ] -- START
'            If xYearId <= 0 Then xYearId = FinancialYear._Object._YearUnkid

'            'StrQ = "SELECT " & _
'            '       "     ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS UName " & _
'            '       "    ,ISNULL(hrmsConfiguration..cfuser_master.email,'') AS UEmail " & _
'            '       ", ISNULL(hrmsConfiguration..cfuser_privilege.userunkid, 0) AS UId " & _
'            '       "FROM hrmsConfiguration..cfuser_privilege " & _
'            '       "    JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
'            '       "WHERE privilegeunkid = @PvgId " & _
'            '       "    AND ISNULL(hrmsConfiguration..cfuser_master.email,'') <> '' "

'            StrQ = "SELECT " & _
'                   "     ISNULL(hrmsConfiguration..cfuser_master.firstname,'')+' '+ISNULL(hrmsConfiguration..cfuser_master.lastname,'') AS UName " & _
'                   "    ,ISNULL(hrmsConfiguration..cfuser_master.email,'') AS UEmail " & _
'                   ", ISNULL(hrmsConfiguration..cfuser_privilege.userunkid, 0) AS UId " & _
'                   "FROM hrmsConfiguration..cfuser_privilege " & _
'                   " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfuser_privilege.userunkid = cfcompanyaccess_privilege.userunkid " & _
'                   "    JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
'                   "WHERE privilegeunkid = @PvgId AND yearunkid = '" & xYearId & "' " & _
'                   "    AND ISNULL(hrmsConfiguration..cfuser_master.email,'') <> '' "
'            'S.SANDEEP [ 17 NOV 2014 ] -- END


'            'Sohail (24 Dec 2013) - Send link in salary change notification - [userunkid]

'            objDataOperation.AddParameter("@PvgId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeUnkid.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Get_UserBy_PrivilegeId", mstrModuleName)
'            Return Nothing
'        Finally
'            StrQ = Nothing : dsList.Dispose()
'        End Try
'    End Function
'    'S.SANDEEP [ 18 SEP 2012 ] -- END


'    'S.SANDEEP [ 08 NOV 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub GetRole_Privilege()
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet
'        Dim dsTran As New DataSet
'        Dim exForce As Exception
'        Try
'            dsTran = GetAbilityGroupList()

'            objDataOperation = New clsDataOperation

'            If dsTran.Tables(0).Rows.Count > 0 Then
'                For Each dRow As DataRow In dsTran.Tables(0).Rows


'                    'Pinkal (09-Nov-2012) -- Start
'                    'Enhancement : TRA Changes

'                    'StrQ = "DECLARE @words VARCHAR (MAX) = '" & dRow.Item("privilegeunkid").ToString & "' " & _
'                    '       "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
'                    '       "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
'                    '       "SELECT @words += ',', @start = 1, @stop = len(@words)+1 " & _
'                    '       "WHILE @start < @stop begin " & _
'                    '       "    SELECT " & _
'                    '       "        @end   = CHARINDEX(',',@words,@start) " & _
'                    '       "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
'                    '       "      , @start = @end+1 " & _
'                    '       "    INSERT @split VALUES (@word) " & _
'                    '       "END " & _
'                    '       "SELECT * FROM @split ORDER BY word"

'                    StrQ = "DECLARE @words VARCHAR (MAX)  " & _
'                           " SET @words = '" & dRow.Item("privilegeunkid").ToString & "' " & _
'                           "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
'                           "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
'                           "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
'                           "WHILE @start < @stop begin " & _
'                           "    SELECT " & _
'                           "        @end   = CHARINDEX(',',@words,@start) " & _
'                           "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
'                           "      , @start = @end+1 " & _
'                           "    INSERT @split VALUES (@word) " & _
'                           "END " & _
'                           "SELECT * FROM @split ORDER BY word"

'                    'Pinkal (09-Nov-2012) -- End

'                    dsList = objDataOperation.ExecQuery(StrQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    mDicRolePrivilege.Add(dRow.Item("roleunkid"), dsList.Tables(0))
'                Next
'            End If
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "GetRole_Privilege", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 08 NOV 2012 ] -- END

'    'S.SANDEEP [ 01 DEC 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function Return_UserId(ByVal intEmpUnkid As Integer, _
'                                  ByVal intCompanyUnkid As Integer) As Integer
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = "SELECT " & _
'                   "  userunkid AS UsrId " & _
'                   "FROM hrmsConfiguration..cfuser_master " & _
'                   "WHERE employeeunkid = @EmpId " & _
'                   "  AND companyunkid = @CompnayId AND isactive = 1 "

'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
'            objDataOperation.AddParameter("@CompnayId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)

'            dsList = objDataOperation.ExecQuery(StrQ, "Lst")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                Return dsList.Tables(0).Rows(0).Item("UsrId")
'            Else
'                Return -1
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Return_UserId", mstrModuleName)
'            Return -1
'        End Try
'    End Function
'    'S.SANDEEP [ 01 DEC 2012 ] -- END


'    'S.SANDEEP [ 01 APR 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function isUExist(ByVal iEmpId As Integer, ByVal iCmpId As Integer) As Integer
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim iUsrid As Integer = 0
'        Dim objDataOperation As New clsDataOperation

'        Try

'            strQ = "SELECT " & _
'                   "  userunkid " & _
'                   "FROM hrmsConfiguration..cfuser_master " & _
'                   "WHERE employeeunkid = '" & iEmpId & "' " & _
'                   " AND companyunkid = '" & iCmpId & "' " & _
'                   " AND isactive = 1 "


'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                iUsrid = dsList.Tables(0).Rows(0).Item("userunkid")
'            End If

'            Return iUsrid

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'        End Try
'    End Function
'    'S.SANDEEP [ 01 APR 2013 ] -- END



'    'Pinkal (03-Nov-2014) -- Start
'    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

'    Public Function GetUserAccessFromUser(ByVal iUserId As Integer, ByVal iCompanyId As Integer, ByVal iYearId As Integer, ByVal StrAllocation As String) As DataTable
'        Dim dtUserAccess As DataTable = Nothing
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try

'            objDataOperation.ClearParameters()
'            strQ = " SELECT userunkid,companyunkid,yearunkid,referenceunkid,allocationunkid,allocationlevel " & _
'                      " FROM hrmsConfiguration..cfuseraccess_privilege_master " & _
'                      " JOIN hrmsConfiguration..cfuseraccess_privilege_tran on cfuseraccess_privilege_master.useraccessprivilegeunkid = cfuseraccess_privilege_tran.useraccessprivilegeunkid " & _
'                      " WHERE userunkid = @userId AND companyunkid =@companyId AND yearunkid = @yearId AND referenceunkid in (" & StrAllocation & ")"

'            objDataOperation.AddParameter("@userId", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
'            objDataOperation.AddParameter("@companyId", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompanyId)
'            objDataOperation.AddParameter("@yearId", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearId)
'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
'                dtUserAccess = dsList.Tables(0)
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetUserAccessFromUser; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'        End Try
'        Return dtUserAccess
'    End Function

'    'Pinkal (03-Nov-2014) -- End



'End Class