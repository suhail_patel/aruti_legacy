﻿'************************************************************************************************************************************
'Class Name : clsUser_Acc_Lock.vb
'Purpose    :
'Date       :14/06/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

'S.SANDEEP [01-OCT-2018] -- START
'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
' ------------------------------------------- > ADDED : hrmsConfiguration..
'S.SANDEEP [01-OCT-2018] -- END

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsUser_Acc_Lock
    Private Const mstrModuleName = "clsUser_Acc_Lock"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintUserunkid As Integer
    Private mintAttempts As Integer
    Private mblnIsunlocked As Boolean
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
    Private mintUnlockuserunkid As Integer = -1
    Private mdtLockedtime As Date
    'S.SANDEEP [ 09 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtAttempt_Date As Date
    'S.SANDEEP [ 09 MAR 2013 ] -- END

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set attempts
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Attempts() As Integer
        Get
            Return mintAttempts
        End Get
        Set(ByVal value As Integer)
            mintAttempts = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isunlocked
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isunlocked() As Boolean
        Get
            Return mblnIsunlocked
        End Get
        Set(ByVal value As Boolean)
            mblnIsunlocked = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Machine_Name() As String
        Get
            Return mstrMachine_Name
        End Get
        Set(ByVal value As String)
            mstrMachine_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockuserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Unlockuserunkid() As Integer
        Get
            Return mintUnlockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintUnlockuserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockedtime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Lockedtime() As Date
        Get
            Return mdtLockedtime
        End Get
        Set(ByVal value As Date)
            mdtLockedtime = Value
        End Set
    End Property

    'S.SANDEEP [ 09 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set attempt_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Attempt_Date() As Date
        Get
            Return mdtAttempt_Date
        End Get
        Set(ByVal value As Date)
            mdtAttempt_Date = Value
        End Set
    End Property
    'S.SANDEEP [ 09 MAR 2013 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  userunkid " & _
                   ", attempts " & _
                   ", isunlocked " & _
                   ", ip " & _
                   ", machine_name " & _
                   ", unlockuserunkid " & _
                   ", lockedtime " & _
                   ", ISNULL(attempt_date,'') AS attempt_date " & _
                  "FROM hrmsConfiguration..cfuser_acc_lock " & _
                  "WHERE userunkid = @userunkid AND isunlocked = 0 " & _
                  " AND CONVERT(CHAR(8),attempt_date,112) = @Date "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END


            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAttempt_Date))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintattempts = CInt(dtRow.Item("attempts"))
                mblnisunlocked = CBool(dtRow.Item("isunlocked"))
                mstrip = dtRow.Item("ip").ToString
                mstrMachine_Name = dtRow.Item("machine_name").ToString
                mintUnlockuserunkid = CInt(dtRow.Item("unlockuserunkid"))
                If dtRow.Item("lockedtime").ToString.Trim.Length > 0 Then
                    mdtLockedtime = dtRow.Item("lockedtime")
                End If

                'S.SANDEEP [ 09 MAR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If dtRow.Item("attempt_date").ToString.Trim.Length > 0 Then
                    mdtAttempt_Date = dtRow.Item("attempt_date")
                End If
                'S.SANDEEP [ 09 MAR 2013 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrmsConfiguration..cfuser_acc_lock.userunkid " & _
                   " ,attempts " & _
                   " ,isunlocked " & _
                   " ,ip " & _
                   " ,machine_name " & _
                   " ,unlockuserunkid " & _
                   ", hrmsConfiguration..cfuser_master.lockedtime " & _
                   " ,ISNULL(hrmsConfiguration..cfuser_master.username,'') AS Uname " & _
                   " ,CONVERT(CHAR(8),attempt_date,112) AS attempt_date " & _
                   "FROM hrmsConfiguration..cfuser_acc_lock " & _
                   " JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_acc_lock.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                   "WHERE hrmsConfiguration..cfuser_acc_lock.isunlocked = 0 AND hrmsConfiguration..cfuser_master.isactive = 1 AND hrmsConfiguration..cfuser_master.islocked  = 1 "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfuser_acc_lock) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@attempts", SqlDbType.int, eZeeDataType.INT_SIZE, mintattempts.ToString)
            objDataOperation.AddParameter("@isunlocked", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisunlocked.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrip.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            If mdtLockedtime = Nothing Then
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedtime)
            End If

            'S.SANDEEP [ 09 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtAttempt_Date = Nothing Then
                objDataOperation.AddParameter("@attempt_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@attempt_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttempt_Date)
            End If
            'S.SANDEEP [ 09 MAR 2013 ] -- END

            strQ = "INSERT INTO hrmsConfiguration..cfuser_acc_lock ( " & _
                    "  userunkid " & _
                    ", attempts " & _
                    ", isunlocked " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", unlockuserunkid " & _
                    ", lockedtime" & _
                    ", attempt_date" & _
                  ") VALUES (" & _
                    "  @userunkid " & _
                    ", @attempts " & _
                    ", @isunlocked " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @unlockuserunkid " & _
                    ", @lockedtime" & _
                    ", @attempt_date" & _
                  "); SELECT @@identity"
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfuser_acc_lock) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@attempts", SqlDbType.int, eZeeDataType.INT_SIZE, mintattempts.ToString)
            objDataOperation.AddParameter("@isunlocked", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisunlocked.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrip.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)

            If mdtLockedtime = Nothing Then
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@lockedtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockedtime)
            End If

            'S.SANDEEP [ 09 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtAttempt_Date = Nothing Then
                objDataOperation.AddParameter("@attempt_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@attempt_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttempt_Date)
            End If
            'S.SANDEEP [ 09 MAR 2013 ] -- END

            strQ = "UPDATE hrmsConfiguration..cfuser_acc_lock SET " & _
                            "  attempts = @attempts" & _
                            ", isunlocked = @isunlocked " & _
                            ", ip = @ip " & _
                            ", machine_name = @machine_name " & _
                            ", unlockuserunkid = @unlockuserunkid " & _
                            ", lockedtime = @lockedtime " & _
                   ", attempt_date = @attempt_date " & _
                          " WHERE userunkid = @userunkid AND isunlocked = 0 "
            'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfuser_acc_lock) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "UPDATE hrmsConfiguration..cfuser_acc_lock " & _
            " isunlocked = 1 " & _
            "WHERE userunkid = @userunkid "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intUnkid As Integer, ByVal dtAttemptDate As Date) As Integer 'S.SANDEEP [ 09 MAR 2013 ] -- START (attempt_date) -- END
        'Public Function isExist(ByVal intUnkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  userunkid " & _
                   "FROM hrmsConfiguration..cfuser_acc_lock " & _
                   " Where userunkid = @userunkid AND isunlocked = 0 " & _
                   " AND CONVERT(CHAR(8),attempt_date,112) = @Date "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtAttemptDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class