﻿'************************************************************************************************************************************
'Class Name : clsPassowdOptions.vb
'Purpose    :
'Date       :02 June 2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsPassowdOptions
    Private Const mstrModuleName = "clsPassowdOptions"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("PasswdOptions")
        Try
            mdtTran.Columns.Add("passwdunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("passwdoptionid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("passwdminlen", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("passwdmaxlen", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("lockuserafter", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("allowuserretry", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("isunlockedbyadmin", System.Type.GetType("System.Boolean")).DefaultValue = 0
            'S.SANDEEP [ 01 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtTran.Columns.Add("isemp_user", System.Type.GetType("System.Boolean")).DefaultValue = CBool(0)
            'S.SANDEEP [ 01 DEC 2012 ] -- END

            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String"))

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            mdtTran.Columns.Add("isenablealluser", System.Type.GetType("System.Boolean")).DefaultValue = CBool(0)
            'S.SANDEEP [10 AUG 2015] -- END

            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            mdtTran.Columns.Add("passwdlastusednumber", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("passwdlastuseddays", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Hemant (25 May 2021) -- End


            Call FillPasswordOptions()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : NEW ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Variables "

    Private mintPasswordUnkid As Integer = -1
    Private mintPasswordOptionId As Integer = -1
    Private mintPasswordMinLen As Integer = -1
    Private mintPasswordMaxLen As Integer = -1
    Private mintPasswordLength As Integer = -1
    Private mintLockUserAfter As Integer = -1
    Private mintAlloUserRetry As Integer = -1
    Private mblnIsUnlockedByAdmin As Boolean = False
    Private mblnIsPasswordExpiredSet As Boolean = False
    Private mblnIsPasswordLenghtSet As Boolean = False
    Private mblnIsAccLockSet As Boolean = False
    Private mdtTran As DataTable = Nothing
    'Pinkal (21-jun-2011)  --Start
    Private mblnIsApplc_Idle As Boolean = False
    Private mintIdleMinutes As Integer = -1
    'Pinkal (21-jun-2011)  --End

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsLoginModeSet As Boolean = True
    Private mintUserLogingModeId As Integer = 1
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 01 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsUsrNameLengthSet As Boolean = False
    Private mintUsrNameLength As Integer = -1

    Private mblnIsPasswdPolicySet As Boolean = False
    Private mblnIsUCaseMandatory As Boolean = False
    Private mblnIsLCaseMandatory As Boolean = False
    Private mblnIsNCaseMandatory As Boolean = False
    Private mblnIsSCaseMandatory As Boolean = False

    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mblnIsPasswordComplexity As Boolean = False
    'Pinkal (18-Aug-2018) -- End

    'S.SANDEEP [ 01 NOV 2012 ] -- END

    'S.SANDEEP [ 01 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsEmployeeAsUser As Boolean = False
    'S.SANDEEP [ 01 DEC 2012 ] -- END

    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Private mblnEnableAllUser As Boolean = False
    'S.SANDEEP [10 AUG 2015] -- END

    'Hemant (25 May 2021) -- Start
    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
    Private mblnIsPasswordHistory As Boolean = False
    Private mintPasswdLastUsedNumber As Integer = -1
    Private mintPasswdLastUsedDays As Integer = -1
    'Hemant (25 May 2021) -- End

    Private mblnProhibitPasswordChange As Boolean = False 'Sohail (15 Apr 2012)
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public ReadOnly Property _PasswordUnkid() As Integer
        Get
            Return mintPasswordUnkid
        End Get
    End Property

    Public ReadOnly Property _PasswordOptionId() As Integer
        Get
            Return mintPasswordOptionId
        End Get
    End Property

    Public ReadOnly Property _PasswordMinLen() As Integer
        Get
            Return mintPasswordMinLen
        End Get
    End Property

    Public ReadOnly Property _PasswordMaxLen() As Integer
        Get
            Return mintPasswordMaxLen
        End Get
    End Property

    Public ReadOnly Property _PasswordLength() As Integer
        Get
            Return mintPasswordLength
        End Get
    End Property

    Public ReadOnly Property _LockUserAfter() As Integer
        Get
            Return mintLockUserAfter
        End Get
    End Property

    Public ReadOnly Property _AlloUserRetry() As Integer
        Get
            Return mintAlloUserRetry
        End Get
    End Property

    Public ReadOnly Property _IsUnlockedByAdmin() As Boolean
        Get
            Return mblnIsUnlockedByAdmin
        End Get
    End Property

    Public ReadOnly Property _IsPasswordExpiredSet() As Boolean
        Get
            Return mblnIsPasswordExpiredSet
        End Get
    End Property

    Public ReadOnly Property _IsPasswordLenghtSet() As Boolean
        Get
            Return mblnIsPasswordLenghtSet
        End Get
    End Property

    Public ReadOnly Property _IsAccLockSet() As Boolean
        Get
            Return mblnIsAccLockSet
        End Get
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    'Pinkal (21-jun-2011)  --Start
    Public ReadOnly Property _IsApplication_Idle() As Boolean
        Get
            Return mblnIsApplc_Idle
        End Get
    End Property

    Public ReadOnly Property _Idletime() As Integer
        Get
            Return mintIdleMinutes
        End Get
    End Property
    'Pinkal (21-jun-2011)  --End

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _IsLoginModeSet() As Boolean
        Get
            Return mblnIsLoginModeSet
        End Get
    End Property

    Public ReadOnly Property _UserLogingModeId() As Integer
        Get
            Return mintUserLogingModeId
        End Get
    End Property
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 01 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _IsUsrNameLengthSet() As Boolean
        Get
            Return mblnIsUsrNameLengthSet
        End Get
    End Property

    Public ReadOnly Property _UsrNameLength() As Integer
        Get
            Return mintUsrNameLength
        End Get
    End Property

    Public ReadOnly Property _IsPasswdPolicySet() As Boolean
        Get
            Return mblnIsPasswdPolicySet
        End Get
    End Property

    Public ReadOnly Property _IsUpperCase_Mandatory() As Boolean
        Get
            Return mblnIsUCaseMandatory
        End Get
    End Property

    Public ReadOnly Property _IsLowerCase_Mandatory() As Boolean
        Get
            Return mblnIsLCaseMandatory
        End Get
    End Property

    Public ReadOnly Property _IsNumeric_Mandatory() As Boolean
        Get
            Return mblnIsNCaseMandatory
        End Get
    End Property

    Public ReadOnly Property _IsSpecalChars_Mandatory() As Boolean
        Get
            Return mblnIsSCaseMandatory
        End Get
    End Property
    'S.SANDEEP [ 01 NOV 2012 ] -- END

    'S.SANDEEP [ 01 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _IsEmployeeAsUser() As Boolean
        Get
            Return mblnIsEmployeeAsUser
        End Get
    End Property
    'S.SANDEEP [ 01 DEC 2012 ] -- END

    'Sohail (15 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _ProhibitPasswordChange() As Boolean
        Get
            Return mblnProhibitPasswordChange
        End Get
    End Property
    'Sohail (15 Apr 2013) -- End

    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Public ReadOnly Property _EnableAllUser() As Boolean
        Get
            Return mblnEnableAllUser
        End Get
    End Property
    'S.SANDEEP [10 AUG 2015] -- END


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Public ReadOnly Property _IsPasswordComplexity() As Boolean
        Get
            Return mblnIsPasswordComplexity
        End Get
    End Property
    'Pinkal (18-Aug-2018) -- End

    'Hemant (25 May 2021) -- Start
    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
    Public ReadOnly Property _IsPasswordHistory() As Boolean
        Get
            Return mblnIsPasswordHistory
        End Get
    End Property

    Public ReadOnly Property _PasswdLastUsedNumber() As Integer
        Get
            Return mintPasswdLastUsedNumber
        End Get
    End Property

    Public ReadOnly Property _PasswdLastUsedDays() As Integer
        Get
            Return mintPasswdLastUsedDays
        End Get
    End Property
    'Hemant (25 May 2021) -- End

#End Region

#Region " Private & Public Functions "

    Private Sub FillPasswordOptions(Optional ByVal StrListName As String = "List")
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dRow As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                     " passwdunkid " & _
                     ",passwdoptionid " & _
                     ",passwdminlen " & _
                     ",passwdmaxlen " & _
                     ",lockuserafter " & _
                     ",allowuserretry " & _
                     ",isunlockedbyadmin " & _
                   ",isemp_user " & _
                   ",ISNULL(isenablealluser,0) AS isenablealluser " & _
                     ",'' AS AUD " & _
                     ",ISNULL(passwdlastusednumber, 0) AS passwdlastusednumber " & _
                     ",ISNULL(passwdlastuseddays, 0) AS passwdlastuseddays " & _
                   "FROM hrmsConfiguration..cfpasswd_options " 'S.SANDEEP [ 01 DEC 2012 isemp_user ] -- START -- END
            'Hemant (25 May 2021) -- [passwdlastusednumber, passwdlastuseddays]
            'S.SANDEEP [10 AUG 2015] -- START {isenablealluser} -- END

            dsList = objDataOperation.ExecQuery(StrQ, StrListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(StrListName).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(StrListName).Rows
                    dRow = mdtTran.NewRow

                    dRow.Item("passwdunkid") = dtRow.Item("passwdunkid")
                    dRow.Item("passwdoptionid") = dtRow.Item("passwdoptionid")
                    dRow.Item("passwdminlen") = dtRow.Item("passwdminlen")
                    dRow.Item("passwdmaxlen") = dtRow.Item("passwdmaxlen")
                    dRow.Item("lockuserafter") = dtRow.Item("lockuserafter")
                    dRow.Item("allowuserretry") = dtRow.Item("allowuserretry")
                    dRow.Item("isunlockedbyadmin") = dtRow.Item("isunlockedbyadmin")
                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dRow.Item("isemp_user") = dtRow.Item("isemp_user")
                    'S.SANDEEP [ 01 DEC 2012 ] -- END
                    dRow.Item("AUD") = dtRow.Item("AUD")

                    'S.SANDEEP [10 AUG 2015] -- START
                    'ENHANCEMENT : Aruti SaaS Changes
                    dRow.Item("isenablealluser") = dtRow.Item("isenablealluser")
                    'S.SANDEEP [10 AUG 2015] -- END

                    'Hemant (25 May 2021) -- Start
                    'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                    dRow.Item("passwdlastusednumber") = dtRow.Item("passwdlastusednumber")
                    dRow.Item("passwdlastuseddays") = dtRow.Item("passwdlastuseddays")
                    'Hemant (25 May 2021) -- End

                    mdtTran.Rows.Add(dRow)

                    Select Case dtRow.Item("passwdoptionid")
                        Case enPasswordOption.PASSWD_EXPIRATION
                            mblnIsPasswordExpiredSet = True
                            mintPasswordMinLen = dtRow.Item("passwdminlen")
                            mintPasswordMaxLen = dtRow.Item("passwdmaxlen")
                        Case enPasswordOption.PASSWD_LENGTH
                            mblnIsPasswordLenghtSet = True
                            mintPasswordLength = dtRow.Item("passwdminlen")
                        Case enPasswordOption.PASSWD_ACC_LOCKOUT
                            mblnIsAccLockSet = True
                            mintLockUserAfter = dtRow.Item("lockuserafter")
                            mintAlloUserRetry = dtRow.Item("allowuserretry")
                            mblnIsUnlockedByAdmin = dtRow.Item("isunlockedbyadmin")
                            'Pinkal (21-jun-2011)  --Start
                        Case enPasswordOption.APPLIC_IDLE_STATE
                            mblnIsApplc_Idle = True
                            mintIdleMinutes = dtRow.Item("passwdminlen")
                            'Pinkal (21-jun-2011)  --End

                            'S.SANDEEP [ 07 NOV 2011 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case enPasswordOption.USER_LOGIN_MODE
                            mblnIsLoginModeSet = True
                            Select Case CInt(dtRow.Item("passwdminlen"))
                                Case enAuthenticationMode.BASIC_AUTHENTICATION
                                    mintUserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION
                                    'S.SANDEEP [ 01 DEC 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    mblnIsEmployeeAsUser = CBool(dtRow.Item("isemp_user"))
                                    'S.SANDEEP [ 01 DEC 2012 ] -- END
                                Case enAuthenticationMode.AD_BASIC_AUTHENTICATION
                                    mintUserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION
                                Case enAuthenticationMode.AD_SSO_AUTHENTICATION
                                    mintUserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION
                                Case Else
                                    mintUserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION
                            End Select
                            'S.SANDEEP [ 07 NOV 2011 ] -- END

                            'S.SANDEEP [10 AUG 2015] -- START
                            'ENHANCEMENT : Aruti SaaS Changes
                            mblnEnableAllUser = CBool(dtRow.Item("isenablealluser"))
                            'S.SANDEEP [10 AUG 2015] -- END

                            'S.SANDEEP [ 01 NOV 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case enPasswordOption.USER_NAME_LENGTH
                            mblnIsUsrNameLengthSet = True
                            mintUsrNameLength = dtRow.Item("passwdminlen")
                        Case enPasswordOption.ENF_PASSWD_POLICY
                            mblnIsPasswdPolicySet = True
                            Select Case CInt(dtRow.Item("passwdminlen"))
                                Case enPasswordPolicy.PASSWD_UCASE_CMPL
                                    mblnIsUCaseMandatory = True
                                Case enPasswordPolicy.PASSWD_LCASE_CMPL
                                    mblnIsLCaseMandatory = True
                                Case enPasswordPolicy.PASSWD_NCASE_CMPL
                                    mblnIsNCaseMandatory = True
                                Case enPasswordPolicy.PASSWD_SPLCH_CMPL
                                    mblnIsSCaseMandatory = True

                                    'Pinkal (18-Aug-2018) -- Start
                                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                                Case enPasswordPolicy.PASSWD_COMPLEXITY
                                    mblnIsPasswordComplexity = True
                                    'Pinkal (18-Aug-2018) -- End

                            End Select
                            'S.SANDEEP [ 01 NOV 2012 ] -- END

                            'Sohail (15 Apr 2013) -- Start
                            'TRA - ENHANCEMENT
                        Case enPasswordOption.PROHIBIT_PASSWD_CHANGE
                            mblnProhibitPasswordChange = CBool(dtRow.Item("passwdminlen"))
                            'Sohail (15 Apr 2013) -- End

                            'Hemant (25 May 2021) -- Start
                            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                        Case enPasswordOption.PASSWD_HISTORY
                            mblnIsPasswordHistory = True
                            mintPasswdLastUsedNumber = dtRow.Item("passwdlastusednumber")
                            mintPasswdLastUsedDays = dtRow.Item("passwdlastuseddays")
                            'Hemant (25 May 2021) -- End

                    End Select

                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillPasswordOptions ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Public Function InsertUpdateDeletePasswordOption() As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO cfpasswd_options ( " & _
                                          "  passwdoptionid " & _
                                          ", passwdminlen " & _
                                          ", passwdmaxlen " & _
                                          ", lockuserafter " & _
                                          ", allowuserretry " & _
                                          ", isunlockedbyadmin" & _
                                          ", isemp_user " & _
                                          ", isenablealluser " & _
                                          ", passwdlastusednumber " & _
                                          ", passwdlastuseddays " & _
                                       ") VALUES (" & _
                                          "  @passwdoptionid " & _
                                          ", @passwdminlen " & _
                                          ", @passwdmaxlen " & _
                                          ", @lockuserafter " & _
                                          ", @allowuserretry " & _
                                          ", @isunlockedbyadmin" & _
                                          ", @isemp_user " & _
                                          ", @isenablealluser " & _
                                          ", @passwdlastusednumber " & _
                                          ", @passwdlastuseddays " & _
                                       "); SELECT @@identity" 'S.SANDEEP [ 01 DEC 2012 (isemp_user) ]
                                'Hemant (25 May 2021) -- [passwdlastusednumber,passwdlastuseddays]
                                'S.SANDEEP [10 AUG 2015] -- START {isenablealluser} -- END


                                objDataOperation.AddParameter("@passwdoptionid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdoptionid").ToString)
                                objDataOperation.AddParameter("@passwdminlen", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdminlen").ToString)
                                objDataOperation.AddParameter("@passwdmaxlen", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdmaxlen").ToString)
                                objDataOperation.AddParameter("@lockuserafter", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lockuserafter").ToString)
                                objDataOperation.AddParameter("@allowuserretry", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allowuserretry").ToString)
                                objDataOperation.AddParameter("@isunlockedbyadmin", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isunlockedbyadmin").ToString)
                                'S.SANDEEP [ 01 DEC 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOperation.AddParameter("@isemp_user", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isemp_user").ToString)
                                'S.SANDEEP [ 01 DEC 2012 ] -- END

                                'S.SANDEEP [10 AUG 2015] -- START
                                'ENHANCEMENT : Aruti SaaS Changes
                                objDataOperation.AddParameter("@isenablealluser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isenablealluser").ToString)
                                'S.SANDEEP [10 AUG 2015] -- END

                                'Hemant (25 May 2021) -- Start
                                'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                                objDataOperation.AddParameter("@passwdlastusednumber", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdlastusednumber").ToString)
                                objDataOperation.AddParameter("@passwdlastuseddays", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdlastuseddays").ToString)
                                'Hemant (25 May 2021) -- End

                                'Pinkal (06-Aug-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim mintPwdId As Integer = dsList.Tables(0).Rows(0)(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfpasswd_options", "passwdunkid", mintPwdId, True) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END




                            Case "U"
                                StrQ = "UPDATE cfpasswd_options SET " & _
                                        "  passwdoptionid = @passwdoptionid" & _
                                        ", passwdminlen = @passwdminlen" & _
                                        ", passwdmaxlen = @passwdmaxlen" & _
                                        ", lockuserafter = @lockuserafter" & _
                                        ", allowuserretry = @allowuserretry" & _
                                        ", isunlockedbyadmin = @isunlockedbyadmin " & _
                                        ", isemp_user = @isemp_user " & _
                                        ", isenablealluser = @isenablealluser " & _
                                        ", passwdlastusednumber = @passwdlastusednumber " & _
                                        ", passwdlastuseddays = @passwdlastuseddays " & _
                                      "WHERE passwdunkid = @passwdunkid " 'S.SANDEEP [ 01 DEC 2012 (isemp_user) ]
                                'Hemant (25 May 2021) -- [passwdlastusednumber,passwdlastuseddays]
                                'S.SANDEEP [10 AUG 2015] -- START {isenablealluser} -- END

                                objDataOperation.AddParameter("@passwdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdunkid").ToString)
                                objDataOperation.AddParameter("@passwdoptionid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdoptionid").ToString)
                                objDataOperation.AddParameter("@passwdminlen", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdminlen").ToString)
                                objDataOperation.AddParameter("@passwdmaxlen", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdmaxlen").ToString)
                                objDataOperation.AddParameter("@lockuserafter", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lockuserafter").ToString)
                                objDataOperation.AddParameter("@allowuserretry", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("allowuserretry").ToString)
                                objDataOperation.AddParameter("@isunlockedbyadmin", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isunlockedbyadmin").ToString)

                                'S.SANDEEP [ 01 DEC 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOperation.AddParameter("@isemp_user", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isemp_user").ToString)
                                'S.SANDEEP [ 01 DEC 2012 ] -- END


                                'S.SANDEEP [10 AUG 2015] -- START
                                'ENHANCEMENT : Aruti SaaS Changes
                                objDataOperation.AddParameter("@isenablealluser", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isenablealluser").ToString)
                                'S.SANDEEP [10 AUG 2015] -- END

                                'Hemant (25 May 2021) -- Start
                                'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
                                objDataOperation.AddParameter("@passwdlastusednumber", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdlastusednumber").ToString)
                                objDataOperation.AddParameter("@passwdlastuseddays", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdlastuseddays").ToString)
                                'Hemant (25 May 2021) -- End

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (06-Aug-2012) -- Start
                                'Enhancement : TRA Changes

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfpasswd_options", CInt(.Item("passwdunkid")), "passwdunkid", 2, objDataOperation) Then

                                    If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfpasswd_options", "passwdunkid", CInt(.Item("passwdunkid")), True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                            Case "D"


                                'Pinkal (06-Aug-2012) -- Start
                                'Enhancement : TRA Changes

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfpasswd_options", "passwdunkid", CInt(.Item("passwdunkid")), True) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                                StrQ = "DELETE FROM cfpasswd_options " & _
                                       "WHERE passwdunkid = @passwdunkid "
                                objDataOperation.AddParameter("@passwdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("passwdunkid").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure : InsertUpdateDeletePasswordOption ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
