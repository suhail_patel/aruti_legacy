﻿'************************************************************************************************************************************
'Class Name : clsPasswordHistory.vb
'Purpose    :
'Date       :25-May-2021
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsPasswordHistory
    Private Shared ReadOnly mstrModuleName As String = "clsPasswordHistory"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mintUserunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtTransactiondate As DateTime
    Private mstrNewPassword As String = String.Empty
    Private mstrOldPassword As String = String.Empty
    Private mintCompanyunkid As Integer
    Private mintChangedUserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsSaveAsEmployee As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Tranguid(Optional ByVal xDataOp As clsDataOperation = Nothing) As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
            Call GetData(xDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Transactiondate() As DateTime
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As DateTime)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set new_password
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _NewPassword() As String
        Get
            Return mstrNewPassword
        End Get
        Set(ByVal value As String)
            mstrNewPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set new_password
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _OldPassword() As String
        Get
            Return mstrOldPassword
        End Get
        Set(ByVal value As String)
            mstrOldPassword = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changed_userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ChangedUserunkid() As Integer
        Get
            Return mintChangedUserunkid
        End Get
        Set(ByVal value As Integer)
            mintChangedUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
       
        Try
            strQ = "SELECT " & _
                      "  tranguid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", new_password " & _
                      ", old_password " & _
                      ", companyunkid " & _
                      ", changed_userunkid " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                     "FROM hrmsconfiguration..cfpassword_history " & _
                     "WHERE tranguid = @tranguid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrTranguid = dtRow.Item("tranguid").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mstrNewPassword = dtRow.Item("new_password").ToString
                mstrOldPassword = dtRow.Item("old_password").ToString
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintChangedUserunkid = CInt(dtRow.Item("changed_userunkid"))
                mstrIp = dtRow.Item("ip").ToString
                mstrHostname = dtRow.Item("host").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal iUserID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = " SELECT " & _
                      "  tranguid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", new_password " & _
                      ", old_password " & _
                      ", companyunkid " & _
                      ", changed_userunkid " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                      " FROM hrmsconfiguration..cfpassword_history "

            strQ &= " WHERE 1  = 1 "

            If iUserID > 0 Then
                strQ &= " AND hrmsconfiguration..cfpassword_history.userunkid = @userunkid"
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfpassword_history) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@new_password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrNewPassword, "ezee").ToString)
            objDataOperation.AddParameter("@old_password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrOldPassword, "ezee").ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@changed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangedUserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "INSERT INTO hrmsconfiguration..cfpassword_history ( " & _
                      "  tranguid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", new_password " & _
                      ", old_password " & _
                      ", companyunkid " & _
                      ", changed_userunkid " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @tranguid " & _
                      ", @userunkid " & _
                      ", @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @new_password " & _
                      ", @old_password " & _
                      ", @companyunkid " & _
                      ", @changed_userunkid " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfpassword_history) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@new_password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrNewPassword, "ezee").ToString)
            objDataOperation.AddParameter("@old_password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(mstrOldPassword, "ezee").ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@changed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangedUserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "UPDATE hrmsconfiguration..cfpassword_history SET " & _
                      "  userunkid = @userunkid " & _
                      ", employeeunkid = @employeeunkid " & _
                      ", transactiondate = @transactiondate " & _
                      ", new_password = @new_password " & _
                      ", old_password = @old_password " & _
                      ", companyunkid = @companyunkid " & _
                      ", changed_userunkid = @changed_userunkid " & _
                      ", ip = @ip " & _
                      ", host = @host " & _
                      ", isweb = @isweb " & _
                    "WHERE tranguid = @tranguid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFilterDataList(ByVal strTableName As String, _
                                      ByVal iUserID As Integer, _
                                      ByVal iEmployeeID As Integer, _
                                      ByVal iCompanyID As Integer, _
                                      Optional ByVal iTopCount As Integer = 0, _
                                      Optional ByVal strFilter As String = "", _
                                      Optional ByVal strOrderBy As String = "" _
                                      ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strTopCount As String = String.Empty

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            If iTopCount > 0 Then
                strTopCount = " Top " & iTopCount
            End If
            strQ = " SELECT " & _
                      " " & strTopCount & " " & _
                      "  tranguid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", new_password " & _
                      ", old_password " & _
                      ", companyunkid " & _
                      ", changed_userunkid " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                      " FROM hrmsconfiguration..cfpassword_history "

            strQ &= " WHERE 1  = 1 "

            If iUserID > 0 Then
                strQ &= " AND hrmsconfiguration..cfpassword_history.userunkid = @userunkid"
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserID)
            End If

            If iEmployeeID > 0 Then
                strQ &= " AND hrmsconfiguration..cfpassword_history.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeID)
            End If

            If iCompanyID > 0 Then
                strQ &= " AND hrmsconfiguration..cfpassword_history.companyunkid = @companyunkid"
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompanyID)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " " & strOrderBy & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

End Class
