﻿'************************************************************************************************************************************
'Class Name : clsUser_dashboard.vb
'Purpose    :
'Date       :05/31/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsUser_dashboard
    Private Const mstrModuleName = "clsUser_dashboard"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintUserdashboardunkid As Integer
    Private mintCompanyunkid As Integer
    Private mintUserunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintItemtypeid As Integer
    Private mstrFormName As String = ""
    Private mstrClientIp As String = ""
    Private mstrHostName As String = ""
    Private mblnIsweb As Boolean
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userdashboardunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userdashboardunkid() As Integer
        Get
            Return mintUserdashboardunkid
        End Get
        Set(ByVal value As Integer)
            mintUserdashboardunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemtypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Itemtypeid() As Integer
        Get
            Return mintItemtypeid
        End Get
        Set(ByVal value As Integer)
            mintItemtypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xEmployeeId As Integer, ByVal xItemTypeId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = " SELECT " & _
                      "  userdashboardunkid " & _
                      ", companyunkid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", itemtypeid " & _
                      " FROM hrmsconfiguration..cfuser_dashboard " & _
                      " WHERE 1= 1  "

            If xCompanyId > 0 Then
                strQ &= "AND companyunkid = @companyunkid "
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            End If

            If xUserId > 0 Then
                strQ &= "AND userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            End If

            If xEmployeeId > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
                'Pinkal (09-Aug-2021) -- End
            End If

            If xItemTypeId > 0 Then
                strQ &= "AND itemtypeid = @itemtypeid "
                objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintUserdashboardunkid = CInt(dtRow.Item("userdashboardunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintItemtypeid = CInt(dtRow.Item("itemtypeid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xEmployeeId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = " IF OBJECT_ID('tempdb..#DashBoardSetting') IS NOT NULL " & _
                      " DROP TABLE #DashBoardSetting " & _
                      " CREATE TABLE #DashBoardSetting(ItemtypeId int,Ischeck bit) " & _
                      " INSERT INTO #DashBoardSetting(ItemtypeId,Ischeck) " & _
                      " SELECT " & enDashboardItems.Crd_TodayBirthday & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_PendingTask & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_EmpOnLeave & " AS IdItemtypeId,CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_WorkAnniversary & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_UpcomingHolidays & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_TeamMembers & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_NewlyHired & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_StaffTurnOver & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck UNION " & _
                      " SELECT " & enDashboardItems.Crd_LeaveAnalysis & " AS ItemtypeId, CAST (0 AS BIT) AS Ischeck ; "

            strQ &= " SELECT " & _
                      "  ISNULL(userdashboardunkid,0) AS userdashboardunkid " & _
                      ", ISNULL(companyunkid,@companyunkid) AS companyunkid " & _
                      ", ISNULL(userunkid,@userunkid) AS userunkid " & _
                      ", ISNULL(employeeunkid,@employeeunkid) AS employeeunkid " & _
                      ", #DashBoardSetting.itemtypeid " & _
                      ", CASE WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_TodayBirthday & " THEN @Crd_TodayBirthday " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_PendingTask & " THEN @Crd_PendingTask " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_EmpOnLeave & " THEN @Crd_EmpOnLeave " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_WorkAnniversary & " THEN @Crd_WorkAnniversary " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_UpcomingHolidays & " THEN @Crd_UpcomingHolidays " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_TeamMembers & " THEN @Crd_TeamMembers " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_NewlyHired & " THEN @Crd_NewlyHired " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_StaffTurnOver & " THEN @Crd_StaffTurnOver " & _
                      "           WHEN #DashBoardSetting.itemtypeid = " & enDashboardItems.Crd_LeaveAnalysis & " THEN @Crd_LeaveAnalysis " & _
                      "  END AS name " & _
                      ", CASE WHEN #DashBoardSetting.ItemtypeId = cfuser_dashboard.ItemtypeId THEN CAST (1 AS BIT) ELSE CAST(0 AS BIT) END ischeck " & _
                      " FROM #DashBoardSetting  " & _
                      " LEFT JOIN hrmsconfiguration..cfuser_dashboard ON #DashBoardSetting.ItemtypeId = cfuser_dashboard.ItemtypeId "

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid "
            End If

            If xUserId > 0 Then
                strQ &= " AND userunkid = @userunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            objDataOperation.ClearParameters()

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            'objDataOperation.AddParameter("@Crd_TodayBirthday", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1126, "Today's Birthday"))
            'objDataOperation.AddParameter("@Crd_PendingTask", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1127, "Pending Task"))
            'objDataOperation.AddParameter("@Crd_EmpOnLeave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1128, "Employee On Leave"))
            'objDataOperation.AddParameter("@Crd_WorkAnniversary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1129, "Today's Work Anniversary"))
            'objDataOperation.AddParameter("@Crd_UpcomingHolidays", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1130, "Upcoming Holidays"))
            'objDataOperation.AddParameter("@Crd_TeamMembers", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1131, "Team Members"))
            'objDataOperation.AddParameter("@Crd_NewlyHired", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1132, "Newly Hired"))
            'objDataOperation.AddParameter("@Crd_StaffTurnOver", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1133, "Staff Turnover"))
            'objDataOperation.AddParameter("@Crd_LeaveAnalysis", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1134, "Leave Analysis"))


            objDataOperation.AddParameter("@Crd_TodayBirthday", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1131, "Today's Birthday"))
            objDataOperation.AddParameter("@Crd_PendingTask", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1132, "Pending Task"))
            objDataOperation.AddParameter("@Crd_EmpOnLeave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1133, "Employee On Leave"))
            objDataOperation.AddParameter("@Crd_WorkAnniversary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1134, "Today's Work Anniversary"))
            objDataOperation.AddParameter("@Crd_UpcomingHolidays", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1135, "Upcoming Holidays"))
            objDataOperation.AddParameter("@Crd_TeamMembers", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1136, "Team Members"))
            objDataOperation.AddParameter("@Crd_NewlyHired", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1137, "Newly Hired"))
            objDataOperation.AddParameter("@Crd_StaffTurnOver", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1138, "Staff Turnover"))
            objDataOperation.AddParameter("@Crd_LeaveAnalysis", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1139, "Leave Analysis"))

            'Pinkal (09-Aug-2021) -- End


            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim xCount As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count
            Dim mstrDefaulID As String = enDashboardItems.Crd_TodayBirthday & "," & enDashboardItems.Crd_PendingTask & "," & enDashboardItems.Crd_EmpOnLeave
            If xCount <= 0 Then
                dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateDefaultRow(mstrDefaulID, x))
                dsList.AcceptChanges()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfuser_dashboard) </purpose>
    Public Function Insert(ByVal dctDashBoardItems As Dictionary(Of Integer, Boolean)) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If dctDashBoardItems Is Nothing OrElse dctDashBoardItems.Keys.Count <= 0 Then Return True

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            For Each pair As KeyValuePair(Of Integer, Boolean) In dctDashBoardItems

                mintUserdashboardunkid = 0
                mintItemtypeid = pair.Key

                objDataOperation.ClearParameters()

                If isExist(mintCompanyunkid, mintUserunkid, mintEmployeeunkid, CInt(pair.Key), objDataOperation) Then
                    If pair.Value = False Then
                        If Delete(mintCompanyunkid, mintUserunkid, mintEmployeeunkid, pair.Key, objDataOperation) Then
                            Continue For
                        End If
                    Else
                        Continue For
                    End If
                End If

                If pair.Value = False Then Continue For

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, pair.Key)

                strQ = "INSERT INTO hrmsconfiguration..cfuser_dashboard ( " & _
                          "  companyunkid " & _
                          ", userunkid " & _
                          ", employeeunkid " & _
                          ", itemtypeid" & _
                        ") VALUES (" & _
                          "  @companyunkid " & _
                          ", @userunkid " & _
                          ", @employeeunkid " & _
                          ", @itemtypeid" & _
                        "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintUserdashboardunkid = dsList.Tables(0).Rows(0).Item(0)

                If ATInsertUserDashboardItems(enAuditType.ADD, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfuser_dashboard) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintUserdashboardunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@userdashboardunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserdashboardunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemtypeid.ToString)

            strQ = " UPDATE hrmsconfiguration..cfuser_dashboard SET " & _
                      "  companyunkid = @companyunkid" & _
                      ", userunkid = @userunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", itemtypeid = @itemtypeid " & _
                      "  WHERE userdashboardunkid = @userdashboardunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfuser_dashboard) </purpose>
    Public Function Delete(ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xEmployeeId As Integer, ByVal xItemTypeId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try

            GetData(xCompanyId, xUserId, xEmployeeId, xItemTypeId, objDataOperation)

            If ATInsertUserDashboardItems(enAuditType.DELETE, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()

            strQ = "DELETE FROM hrmsconfiguration..cfuser_dashboard WHERE 1 = 1  "

            If xCompanyId > 0 Then
                strQ &= "AND companyunkid = @companyunkid "
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            End If

            If xUserId > 0 Then
                strQ &= "AND userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            End If

            If xEmployeeId > 0 Then
                strQ &= "AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            End If

            If xItemTypeId > 0 Then
                strQ &= "AND itemtypeid = @itemtypeid "
                objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)
            End If


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintUserdashboardunkid = 0

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@userdashboardunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xEmployeeId As Integer, ByVal xItemTypeId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If

            objDataOperation.ClearParameters()


            strQ = " SELECT " & _
                      "  userdashboardunkid " & _
                      ", companyunkid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", itemtypeid " & _
                      " FROM hrmsconfiguration..cfuser_dashboard " & _
                      " WHERE 1 = 1 "

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid"
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            End If

            If xUserId > 0 Then
                strQ &= " AND userunkid = @userunkid"
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            End If

            If xItemTypeId > 0 Then
                strQ &= " AND itemtypeid = @itemtypeid"
                objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ATInsertUserDashboardItems(ByVal xAuditType As Integer, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO hrmsconfiguration..atcfuser_dashboard ( " & _
                      " userdashboardunkid " & _
                      ", companyunkid " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", itemtypeid" & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @userdashboardunkid " & _
                      ", @companyunkid " & _
                      ", @userunkid " & _
                      ", @employeeunkid " & _
                      ", @itemtypeid" & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @form_name " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @isweb" & _
                    "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@userdashboardunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserdashboardunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemtypeid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            If mstrClientIp.Trim.Length > 0 Then
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIp)
            Else
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrHostName.Length > 0 Then
                objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            Else
                objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertUserDashboardItems; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function UpdateDefaultRow(ByVal mstrDefaultId As String, ByVal dr As DataRow) As Boolean
        Try
            If mstrDefaultId.Trim.Length <= 0 Then Return True

            If mstrDefaultId.Trim.Length > 0 Then
                Dim ar() As String = mstrDefaultId.Trim.Split(CChar(","))
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        If ar(i) = CInt(dr("ItemTypeId")) Then dr("ischeck") = True
                        dr.AcceptChanges()
                    Next
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateDefaultRow; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUserDashBoardSetting(ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xEmployeeId As Integer) As Dictionary(Of Integer, Integer)
        Dim mdctUserDashBoard As Dictionary(Of Integer, Integer) = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = " SELECT " & _
                      "  ISNULL(userdashboardunkid,0) AS userdashboardunkid " & _
                      ", ISNULL(companyunkid,@companyunkid) AS companyunkid " & _
                      ", ISNULL(userunkid,@userunkid) AS userunkid " & _
                      ", ISNULL(employeeunkid,@employeeunkid) AS employeeunkid " & _
                      ", itemtypeid " & _
                      " FROM hrmsconfiguration..cfuser_dashboard  " & _
                      " WHERE 1 = 1 "

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid "
            End If

            If xUserId > 0 Then
                strQ &= " AND userunkid = @userunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdctUserDashBoard = dsList.Tables(0).AsEnumerable().ToDictionary(Function(x) x.Field(Of Integer)("itemtypeid"), Function(y) y.Field(Of Integer)("userdashboardunkid"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUserDashBoardSetting; Module Name: " & mstrModuleName)
        End Try
        Return mdctUserDashBoard
    End Function
End Class