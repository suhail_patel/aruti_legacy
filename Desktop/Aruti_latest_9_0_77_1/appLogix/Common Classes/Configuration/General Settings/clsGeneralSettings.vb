﻿Imports eZeeCommonLib

Public Class clsGeneralSettings
    Private ReadOnly mstrModuleName As String = "clsGeneralSettings"
    Dim objInI As clsINI

#Region " Constructor "
    Public Sub New()
        Dim objAppSettings As clsApplicationSettings
        objAppSettings = New clsApplicationSettings
        objInI = New clsINI(objAppSettings._ApplicationPath & "aruti.ini")
    End Sub
#End Region

#Region " Property Variable(s) "
    Private mstrSection As String = String.Empty
#End Region

#Region " Propertie(s) "
    Public Property _Section() As String
        Get
            Return mstrSection
        End Get
        Set(ByVal value As String)
            mstrSection = value
            If mstrSection = enArutiApplicatinType.Aruti_Configuration.ToString Or mstrSection = enArutiApplicatinType.Aruti_Payroll.ToString Then
                mstrSection = "Aruti_Payroll"
            End If
        End Set
    End Property

    Public Property _BitPerSecond() As String
        Get
            Return objInI.GetString(mstrSection, "BitPerSecond", "9600")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "BitPerSecond", value)
        End Set
    End Property

    Public Property _DataBits() As String
        Get
            Return objInI.GetString(mstrSection, "DataBits", "8")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "DataBits", value)
        End Set
    End Property

    Public Property _Parity() As String
        Get
            Return objInI.GetString(mstrSection, "Parity", "None")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "Parity", value)
        End Set
    End Property

    Public Property _StopBits() As String
        Get
            Return objInI.GetString(mstrSection, "StopBits", "1")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "StopBits", value)
        End Set
    End Property

    Public Property _Port() As String
        Get
            Return objInI.GetString(mstrSection, "Port", "1")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "Port", value)
        End Set
    End Property

    Public Property _ReaderType() As String
        Get
            Return objInI.GetString(mstrSection, "ReaderType")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "ReaderType", value)
        End Set
    End Property

    Public Property _DelayTime() As String
        Get
            Return objInI.GetString(mstrSection, "DelayTime", "3")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "DelayTime", value)
        End Set
    End Property

    Public Property _WorkingMode() As String
        Get
            Return objInI.GetString(mstrSection, "WorkingMode")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "WorkingMode", value)
        End Set
    End Property

    Public Property _ServerName() As String
        Get
            'S.SANDEEP [22 OCT 2015] -- START
            'Return objInI.GetString(mstrSection, "Server")
            Return objInI.GetString(mstrSection, "Server", "(local)")
            'S.SANDEEP [22 OCT 2015] -- END
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "Server", value)
        End Set
    End Property

    Public Property _RefreshInterval() As String
        Get
            Return objInI.GetString(mstrSection, "RefreshInterval", "5")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "RefreshInterval", value)
        End Set
    End Property


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Public Property _AD_ServerIP() As String
        Get
            Return objInI.GetString(mstrSection, "ADUserServer", "")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "ADUserServer", value)
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End


    'S.SANDEEP [ 10 NOV 2014 ] -- START
    Public Property _AutoUpdate_Server() As String
        Get
            Return objInI.GetString(mstrSection, "AutoUpdateServer", "")
        End Get
        Set(ByVal value As String)
            objInI.WriteString(mstrSection, "AutoUpdateServer", value)
        End Set
    End Property
    'S.SANDEEP [ 10 NOV 2014 ] -- END

    'S.SANDEEP [21-NOV-2017] -- START
    Public Property _SQLPortNumber() As Integer
        Get
            Return objInI.GetString(mstrSection, "SQLPort", 0)
        End Get
        Set(ByVal value As Integer)
            objInI.WriteString(mstrSection, "SQLPort", value)
        End Set
    End Property
    'S.SANDEEP [21-NOV-2017] -- END

#End Region

End Class
