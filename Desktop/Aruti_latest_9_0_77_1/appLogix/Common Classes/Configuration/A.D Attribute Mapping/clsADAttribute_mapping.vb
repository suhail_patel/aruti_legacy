﻿'************************************************************************************************************************************
'Class Name : clsADAttribute_mapping.vb
'Purpose    :
'Date       :23-Aug-2018
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsADAttribute_mapping
    Private Const mstrModuleName = "clsADAttribute_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    'Pinkal (09-Mar-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mintCompanyId As Integer
    'Pinkal (09-Mar-2020) -- End

    Private mintAttributeunkid As Integer
    Private mintUserunkid As Integer
    Private mstrFormName As String
    Private mstrClientIP As String
    Private mstrHostName As String
    Private mblnIsWeb As Boolean
    Private mintAuditUserId As Integer
    Private mdtAuditDate As DateTime
    Private mdtTable As DataTable = Nothing

#End Region

#Region "Enum"

    Public Enum enAttributes
        LoginUserName = 1
        Password = 2
        FirstName = 3
        MiddleName = 4
        LastName = 5
        DisplayName = 6
        JobTitle = 7
        Department = 8
        EmailAddress = 9
        Telephone = 10
        Mobile = 11
        City = 12
        State = 13
        Country = 14

        'Pinkal (22-Feb-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        Street = 15
        POBox = 16
        Office = 17
        Company = 18
        'Pinkal (22-Feb-2020) -- End

        'Pinkal (09-Mar-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        EntryPoint = 19
        HierarchyLevel = 20
        'Pinkal (09-Mar-2020) -- End


        'Pinkal (04-Apr-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        Description = 21
        'Pinkal (04-Apr-2020) -- End

        'Pinkal (18-Mar-2021) -- Start 
        'NMB Enhancmenet : AD Enhancement for NMB.
        Transfer_Allocation = 22
        'Pinkal (18-Mar-2021) -- End

    End Enum

#End Region

#Region "Constructor"

    Public Sub New()
        Dim drRow As DataRow = Nothing
        mdtTable = New DataTable()

        'Pinkal (09-Mar-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        mdtTable.Columns.Add("companyunkid", Type.GetType("System.Int32")).DefaultValue = 0
        'Pinkal (09-Mar-2020) -- End

        mdtTable.Columns.Add("attributeunkid", Type.GetType("System.Int32")).DefaultValue = 0
        mdtTable.Columns.Add("attributename", Type.GetType("System.String")).DefaultValue = ""
        mdtTable.Columns.Add("attId", Type.GetType("System.Int32")).DefaultValue = 0
        mdtTable.Columns.Add("mappingunkid", Type.GetType("System.Int32")).DefaultValue = 0
        mdtTable.Columns.Add("isallocation", Type.GetType("System.Boolean")).DefaultValue = False
        mdtTable.Columns.Add("allocationName", Type.GetType("System.String")).DefaultValue = ""
        mdtTable.Columns.Add("oupath", Type.GetType("System.String")).DefaultValue = ""
        mdtTable.Columns.Add("isvoid", Type.GetType("System.Boolean")).DefaultValue = False
        mdtTable.Columns.Add("voiduserunkid", Type.GetType("System.Int32")).DefaultValue = 0
        mdtTable.Columns.Add("voiddatetime", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.LoginUserName.ToString()
        drRow("attId") = enAttributes.LoginUserName
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Password.ToString()
        drRow("attId") = enAttributes.Password
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.FirstName.ToString()
        drRow("attId") = enAttributes.FirstName
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.MiddleName.ToString()
        drRow("attId") = enAttributes.MiddleName
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.LastName.ToString()
        drRow("attId") = enAttributes.LastName
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.DisplayName.ToString()
        drRow("attId") = enAttributes.DisplayName
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.JobTitle.ToString()
        drRow("attId") = enAttributes.JobTitle
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Department.ToString()
        drRow("attId") = enAttributes.Department
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.EmailAddress.ToString()
        drRow("attId") = enAttributes.EmailAddress
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Telephone.ToString()
        drRow("attId") = enAttributes.Telephone
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Mobile.ToString()
        drRow("attId") = enAttributes.Mobile
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.City.ToString()
        drRow("attId") = enAttributes.City
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.State.ToString()
        drRow("attId") = enAttributes.State
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Country.ToString()
        drRow("attId") = enAttributes.Country
        mdtTable.Rows.Add(drRow)


        'Pinkal (22-Feb-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Street.ToString()
        drRow("attId") = enAttributes.Street
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.POBox.ToString()
        drRow("attId") = enAttributes.POBox
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Office.ToString()
        drRow("attId") = enAttributes.Office
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Company.ToString()
        drRow("attId") = enAttributes.Company
        mdtTable.Rows.Add(drRow)

        'Pinkal (22-Feb-2020) -- End

        'Pinkal (09-Mar-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.EntryPoint.ToString()
        drRow("attId") = enAttributes.EntryPoint
        mdtTable.Rows.Add(drRow)

        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.HierarchyLevel.ToString()
        drRow("attId") = enAttributes.HierarchyLevel
        mdtTable.Rows.Add(drRow)
        'Pinkal (09-Mar-2020) -- End


        'Pinkal (04-Apr-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Description.ToString()
        drRow("attId") = enAttributes.Description
        mdtTable.Rows.Add(drRow)
        'Pinkal (04-Apr-2020) -- End

        'Pinkal (18-Mar-2021) -- Start 
        'NMB Enhancmenet : AD Enhancement for NMB.
        drRow = mdtTable.NewRow()
        drRow("attributename") = enAttributes.Transfer_Allocation.ToString().Replace("_", " ")
        drRow("attId") = enAttributes.Transfer_Allocation
        mdtTable.Rows.Add(drRow)
        'Pinkal (18-Mar-2021) -- End

    End Sub


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Pinkal (09-Mar-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CompanyId() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    'Pinkal (09-Mar-2020) -- End

    ''' <summary>
    ''' Purpose: Get or Set attributeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Attributeunkid() As Integer
        Get
            Return mintAttributeunkid
        End Get
        Set(ByVal value As Integer)
            mintAttributeunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set attributename
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Attributename() As String
    '    Get
    '        Return mstrAttributename
    '    End Get
    '    Set(ByVal value As String)
    '        mstrAttributename = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set mappingunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Mappingunkid() As Integer
    '    Get
    '        Return mintMappingunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintMappingunkid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set isallocation
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Isallocation() As Boolean
    '    Get
    '        Return mblnIsallocation
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsallocation = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set oupath
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Oupath() As String
    '    Get
    '        Return mstrOupath
    '    End Get
    '    Set(ByVal value As String)
    '        mstrOupath = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set _dtTable
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtTable() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set isvoid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Isvoid() As Boolean
    '    Get
    '        Return mblnIsvoid
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsvoid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiduserunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Voiduserunkid() As Integer
    '    Get
    '        Return mintVoiduserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintVoiduserunkid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiddatetime
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Voiddatetime() As Date
    '    Get
    '        Return mdtVoiddatetime
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtVoiddatetime = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set _FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _FromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FromWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _AuditDate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

#End Region

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Sub GetData()
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '                  "  attributeunkid " & _
    '                  ", attributename " & _
    '                  ", mappingunkid " & _
    '                  ", isallocation " & _
    '                  ", oupath " & _
    '                  ", userunkid " & _
    '                  ", isvoid " & _
    '                  ", voiduserunkid " & _
    '                  ", voiddatetime " & _
    '                  " FROM cfadattribute_mapping " & _
    '                  " WHERE attributeunkid = @attributeunkid"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@attributeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttributeunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            mintAttributeunkid = CInt(dtRow.Item("attributeunkid"))
    '            mstrAttributename = dtRow.Item("attributename").ToString
    '            mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
    '            mblnIsallocation = CBool(dtRow.Item("isallocation"))
    '            mstrOupath = dtRow.Item("oupath").ToString
    '            mintUserunkid = CInt(dtRow.Item("userunkid"))
    '            mblnIsvoid = CBool(dtRow.Item("isvoid"))
    '            mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
    '            mdtVoiddatetime = dtRow.Item("voiddatetime")
    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xCompanyId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal objDoperation As clsDataOperation = Nothing, Optional ByVal mstrFilter As String = "") As DataTable

        'Pinkal (04-Apr-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[Optional ByVal mstrFilter As String = ""]

        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoperation
        End If

        Try
            strQ = " SELECT " & _
                      "  companyunkid " & _
                      ", attributeunkid " & _
                      ", attributename " & _
                      ", CASE WHEN attributename = @LoginUserName THEN " & CInt(enAttributes.LoginUserName) & _
                      "            WHEN attributename = @Password THEN " & CInt(enAttributes.Password) & _
                      "            WHEN attributename = @FirstName THEN " & CInt(enAttributes.FirstName) & _
                      "            WHEN attributename = @MiddleName THEN " & CInt(enAttributes.MiddleName) & _
                      "            WHEN attributename = @LastName THEN " & CInt(enAttributes.LastName) & _
                      "            WHEN attributename = @DisplayName THEN " & CInt(enAttributes.DisplayName) & _
                      "            WHEN attributename = @Jobtitle THEN " & CInt(enAttributes.JobTitle) & _
                      "            WHEN attributename = @Department THEN " & CInt(enAttributes.Department) & _
                      "            WHEN attributename = @Emailaddress THEN " & CInt(enAttributes.EmailAddress) & _
                      "            WHEN attributename = @Telephone THEN " & CInt(enAttributes.Telephone) & _
                      "            WHEN attributename = @Mobile THEN " & CInt(enAttributes.Mobile) & _
                      "            WHEN attributename = @City THEN " & CInt(enAttributes.City) & _
                      "            WHEN attributename = @Region THEN " & CInt(enAttributes.State) & _
                      "            WHEN attributename = @Country THEN " & CInt(enAttributes.Country) & _
                      "            WHEN attributename = @Street THEN " & CInt(enAttributes.Street) & _
                      "            WHEN attributename = @POBox THEN " & CInt(enAttributes.POBox) & _
                      "            WHEN attributename = @Office THEN " & CInt(enAttributes.Office) & _
                      "            WHEN attributename = @Company THEN " & CInt(enAttributes.Company) & _
                      "            WHEN attributename = @EntryPoint THEN " & CInt(enAttributes.EntryPoint) & _
                      "            WHEN attributename = @HierarchyLevel THEN " & CInt(enAttributes.HierarchyLevel) & _
                      "            WHEN attributename = @Description THEN " & CInt(enAttributes.Description) & _
                      "            WHEN attributename = @Transfer_Allocation THEN " & CInt(enAttributes.Transfer_Allocation) & _
                      " END attId " & _
                      ", mappingunkid " & _
                      ", isallocation " & _
                      ", CASE WHEN isallocation = 1 THEN CASE WHEN mappingunkid = " & CInt(enAllocation.BRANCH) & " THEN  @Branch " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.DEPARTMENT_GROUP) & " THEN  @DepartmentGroup " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.DEPARTMENT) & " THEN  @Department1 " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.SECTION_GROUP) & " THEN  @SectionGroup " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.SECTION) & " THEN  @Section " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.UNIT_GROUP) & " THEN  @UnitGroup " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.UNIT) & " THEN  @Unit " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.TEAM) & " THEN  @Team " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.JOB_GROUP) & " THEN  @JobGroup " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.JOBS) & " THEN  @Job " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.CLASS_GROUP) & " THEN  @ClassGroup " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.CLASSES) & " THEN  @Classs " & _
                      "                                                                 WHEN mappingunkid = " & CInt(enAllocation.COST_CENTER) & " THEN  @CostCenter " & _
                      "  END " & _
                      "  ELSE '' END allocationName " & _
                      ", oupath " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      " FROM hrmsConfiguration..cfadattribute_mapping " & _
                      " WHERE companyunkid = @companyId "

            'Pinkal (18-Mar-2021) --NMB Enhancmenet : AD Enhancement for NMB. [  "            WHEN attributename = @Transfer_Allocation THEN " & CInt(enAttributes.Transfer_Allocation) & _]

            'Pinkal (18-Mar-2021) -- End

            'Pinkal (04-Apr-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ "            WHEN attributename = @Description THEN " & CInt(enAttributes.Description) & _]

            'Pinkal (09-Mar-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273][ WHEN attributename = @EntryPoint THEN " & CInt(enAttributes.EntryPoint)]

            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If


            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (04-Apr-2020) -- End 


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@LoginUserName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.LoginUserName.ToString())
            objDataOperation.AddParameter("@Password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Password.ToString())
            objDataOperation.AddParameter("@FirstName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.FirstName.ToString())
            objDataOperation.AddParameter("@MiddleName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.MiddleName.ToString())
            objDataOperation.AddParameter("@LastName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.LastName.ToString())
            objDataOperation.AddParameter("@DisplayName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.DisplayName.ToString())
            objDataOperation.AddParameter("@Jobtitle", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.JobTitle.ToString())
            objDataOperation.AddParameter("@Department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Department.ToString())
            objDataOperation.AddParameter("@Emailaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.EmailAddress.ToString())
            objDataOperation.AddParameter("@Telephone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Telephone.ToString())
            objDataOperation.AddParameter("@Mobile", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Mobile.ToString())
            objDataOperation.AddParameter("@City", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.City.ToString())
            objDataOperation.AddParameter("@Region", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.State.ToString())
            objDataOperation.AddParameter("@Country", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Country.ToString())


            'Pinkal (22-Feb-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            objDataOperation.AddParameter("@Street", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Street.ToString())
            objDataOperation.AddParameter("@POBox", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.POBox.ToString())
            objDataOperation.AddParameter("@Office", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Office.ToString())
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Company.ToString())
            'Pinkal (22-Feb-2020) -- End


            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            objDataOperation.AddParameter("@EntryPoint", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.EntryPoint.ToString())
            objDataOperation.AddParameter("@HierarchyLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.HierarchyLevel.ToString())
            objDataOperation.AddParameter("@companyId", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            'Pinkal (09-Mar-2020) -- End


            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            objDataOperation.AddParameter("@Description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Description.ToString())
            'Pinkal (04-Apr-2020) -- End

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            objDataOperation.AddParameter("@Transfer_Allocation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, enAttributes.Transfer_Allocation.ToString().Replace("_", " "))
            'Pinkal (18-Mar-2021) -- End

            objDataOperation.AddParameter("@Branch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DepartmentGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@Department1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SectionGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@Section", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UnitGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@Unit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@Team", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JobGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@Job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@ClassGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@Classs", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
            objDataOperation.AddParameter("@CostCenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 586, "Cost Center"))


            dtTable = objDataOperation.ExecQuery(strQ, strTableName).Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDoperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfadattribute_mapping) </purpose>
    Public Function Insert(ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If

            strQ = " INSERT INTO hrmsConfiguration..cfadattribute_mapping ( " & _
                     "  companyunkid " & _
                     ", attributename " & _
                     ", mappingunkid " & _
                     ", isallocation " & _
                     ", oupath " & _
                     ", userunkid " & _
                     ", isvoid " & _
                     ", voiduserunkid " & _
                     ", voiddatetime" & _
                   ") VALUES (" & _
                     "  @companyunkid " & _
                     ", @attributename " & _
                     ", @mappingunkid " & _
                     ", @isallocation " & _
                     ", @oupath " & _
                     ", @userunkid " & _
                     ", @isvoid " & _
                     ", @voiduserunkid " & _
                     ", @voiddatetime" & _
                   "); SELECT @@identity"

            'Pinkal (09-Mar-2020) -- 'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ "  @companyunkid " & _]


            'dtTable = New DataView(dtTable, "mappingunkid > 0", "", DataViewRowState.CurrentRows).ToTable()

            For Each drRow As DataRow In dtTable.Rows
                Dim xMappingID As Integer = -1

                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                Dim xOriginalMappingId As Integer = CInt(drRow("mappingunkid"))
                Dim mblnOriginalIsAllocation As Boolean = CBool(drRow("isallocation"))
                Dim mstrOriginalOUPath As String = drRow("oupath").ToString()
                Dim blnIsAllocation As Boolean = False
                Dim xOUPath As String = ""


                'Pinkal (22-May-2020) -- Start
                'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
                'If isExist(drRow("attributename").ToString(), xMappingID, blnIsAllocation, xOUPath, objDataOperation) Then
                If isExist(mintCompanyId, drRow("attributename").ToString(), xMappingID, blnIsAllocation, xOUPath, objDataOperation) Then
                    'Pinkal (22-May-2020) -- End
                    'Pinkal (04-Apr-2020) -- End
                    If xMappingID > 0 AndAlso xMappingID <> CInt(drRow("mappingunkid")) Then

                        'Pinkal (04-Apr-2020) -- Start
                        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                        'If Delete(drRow, mintUserunkid, objDataOperation) = False Then
                        If Delete(drRow, xMappingID, blnIsAllocation, xOUPath, mintUserunkid, objDataOperation) = False Then
                            'Pinkal (04-Apr-2020) -- End
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        Else
                            'Pinkal (04-Apr-2020) -- Start
                            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                            drRow("mappingunkid") = xOriginalMappingId
                            drRow("isallocation") = mblnOriginalIsAllocation
                            drRow("oupath") = mstrOriginalOUPath
                            'Pinkal (04-Apr-2020) -- End
                            If CInt(drRow("mappingunkid")) <= 0 Then Continue For
                        End If
                    Else
                        Continue For
                    End If
                ElseIf CInt(drRow("mappingunkid")) <= 0 Then
                    Continue For
                End If

                objDataOperation.ClearParameters()

                'Pinkal (09-Mar-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)
                'Pinkal (09-Mar-2020) -- End

                objDataOperation.AddParameter("@attributename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, drRow("attributename").ToString)
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("mappingunkid").ToString))
                objDataOperation.AddParameter("@isallocation", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(drRow("isallocation").ToString))
                objDataOperation.AddParameter("@oupath", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, drRow("oupath").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(drRow("isvoid").ToString))
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("voiduserunkid").ToString))
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(drRow("voiddatetime")), DBNull.Value, drRow("voiddatetime")))

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                drRow("attributeunkid") = dsList.Tables(0).Rows(0).Item(0)

                If ATInsertADAttrubute(drRow, 1, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Delete Database Table (cfadattribute_mapping) </purpose>

    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    'Public Function Delete(ByVal dRow As DataRow, ByVal xVoidUserId As Integer, ByVal xObjDataOperation As clsDataOperation) As Boolean
    Public Function Delete(ByVal dRow As DataRow, ByVal xMappingId As Integer, ByVal xIsAllocation As Boolean, ByVal xOuPath As String, ByVal xVoidUserId As Integer, ByVal xObjDataOperation As clsDataOperation) As Boolean
        'Pinkal (04-Apr-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            xObjDataOperation.ClearParameters()
            xObjDataOperation.AddParameter("@attributeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dRow("attributeunkid").ToString))
            xObjDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
            xObjDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xVoidUserId.ToString)

            'Pinkal (22-May-2020) -- Start
            'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
            xObjDataOperation.AddParameter("@Companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)
            'Pinkal (22-May-2020) -- End

            strQ = " UPDATE hrmsConfiguration..cfadattribute_mapping SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = Getdate() " & _
                      " WHERE attributeunkid = @attributeunkid AND Companyunkid = @Companyunkid  "


            'Pinkal (22-May-2020) -- ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. [AND Companyunkid = @Companyunkid ]

            Call xObjDataOperation.ExecNonQuery(strQ)

            If xObjDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(xObjDataOperation.ErrorNumber & ": " & xObjDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            dRow("mappingunkid") = xMappingId
            dRow("isallocation") = xIsAllocation
            dRow("oupath") = xOuPath
            'Pinkal (04-Apr-2020) -- End


            If ATInsertADAttrubute(dRow, 3, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "<Query>"

    '        objDataOperation.AddParameter("@attributeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End TryThis is to notify you that mailbox activation for 
    'End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>


    'Pinkal (22-May-2020) -- Start
    'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
    'Public Function isExist(ByVal xAttributeName As String, ByRef xMappindID As Integer, ByRef blnIsAllocation As Boolean, ByRef xOUPath As String, Optional ByVal xobjDataOperation As clsDataOperation = Nothing) As Boolean
    Public Function isExist(ByVal xCompanyId As Integer, ByVal xAttributeName As String, ByRef xMappindID As Integer, ByRef blnIsAllocation As Boolean, ByRef xOUPath As String, Optional ByVal xobjDataOperation As clsDataOperation = Nothing) As Boolean
        'Pinkal (22-May-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xobjDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  companyunkid " & _
                      ", attributeunkid " & _
                      ", attributename " & _
                      ", mappingunkid " & _
                      ", isallocation " & _
                      ", oupath " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                     "FROM hrmsConfiguration..cfadattribute_mapping " & _
                     "WHERE isvoid = 0 AND attributename = @attributename AND companyunkid = @companyunkid  "


            'Pinkal (22-May-2020) -- 'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. [AND companyunkid = @companyunkid]

            'Pinkal (09-Mar-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[companyunkid]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@attributename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xAttributeName)

            'Pinkal (22-May-2020) -- Start
            'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            'Pinkal (22-May-2020) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                xMappindID = CInt(dsList.Tables(0).Rows(0)("mappingunkid"))

                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                blnIsAllocation = CBool(dsList.Tables(0).Rows(0)("isallocation"))
                xOUPath = dsList.Tables(0).Rows(0)("oupath").ToString()
                'Pinkal (04-Apr-2020) -- End

            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ATInsertADAttrubute(ByVal drRow As DataRow, ByVal xAuditType As Integer, ByVal xobjDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = " INSERT INTO hrmsConfiguration..atcfadattribute_mapping ( " & _
                         "  companyunkid " & _
                         ", attributeunkid " & _
                         ", attributename " & _
                         ", mappingunkid " & _
                         ", isallocation " & _
                         ", oupath " & _
                         ", audittype " & _
                         ", audituserunkid " & _
                         ", auditdatetime " & _
                         ", ip " & _
                         ", host " & _
                         ", form_name " & _
                         ", isweb" & _
                       ") VALUES (" & _
                         "  @companyunkid " & _
                         ", @attributeunkid " & _
                         ", @attributename " & _
                         ", @mappingunkid " & _
                         ", @isallocation " & _
                         ", @oupath " & _
                         ", @audittype " & _
                         ", @audituserunkid " & _
                         ", @auditdatetime " & _
                         ", @ip " & _
                         ", @host " & _
                         ", @form_name " & _
                         ", @isweb" & _
                       "); SELECT @@identity"

            'Pinkal (09-Mar-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].["  @companyunkid " & _]

            xobjDataOperation.ClearParameters()

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            xobjDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)
            'Pinkal (09-Mar-2020) -- End

            xobjDataOperation.AddParameter("@attributeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("attributeunkid").ToString))
            xobjDataOperation.AddParameter("@attributename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, drRow("attributename").ToString)
            xobjDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("mappingunkid").ToString))
            xobjDataOperation.AddParameter("@isallocation", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(drRow("isallocation").ToString))
            xobjDataOperation.AddParameter("@oupath", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, drRow("oupath").ToString)
            xobjDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            xobjDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId.ToString)
            xobjDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            xobjDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP.ToString)
            xobjDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName.ToString)
            xobjDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName.ToString)
            xobjDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb.ToString)
            dsList = xobjDataOperation.ExecQuery(strQ, "List")

            If xobjDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(xobjDataOperation.ErrorNumber & ": " & xobjDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function


    'Pinkal (09-Mar-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEntryPointAllocation(ByVal xEntryPointID As Integer, ByVal mstrDataBaseName As String) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            objDataOperation = New clsDataOperation()
            objDataOperation.ClearParameters()

            Select Case xEntryPointID

                Case enAllocation.BRANCH

                    strQ = " SELECT stationunkid AS AllocationId ,name AS AllocationName FROM " & mstrDataBaseName & "..hrstation_master WHERE isactive =1 "

                Case enAllocation.DEPARTMENT_GROUP

                    strQ = "SELECT deptgroupunkid AS AllocationId,name AS AllocationName FROM " & mstrDataBaseName & "..hrdepartment_group_master WHERE isactive =1 "

                Case enAllocation.DEPARTMENT

                    strQ = "SELECT departmentunkid AS AllocationId ,name AS AllocationName FROM " & mstrDataBaseName & "..hrdepartment_master WHERE isactive =1 "

                Case enAllocation.SECTION_GROUP

                    strQ = "SELECT sectiongroupunkid AS AllocationId ,name AS AllocationName FROM  " & mstrDataBaseName & "..hrsectiongroup_master WHERE isactive =1 "

                Case enAllocation.SECTION

                    strQ = "SELECT sectionunkid AS AllocationId ,name AS AllocationName FROM " & mstrDataBaseName & "..hrsection_master WHERE isactive =1 "

                Case enAllocation.UNIT_GROUP

                    strQ = "SELECT unitgroupunkid AS AllocationId ,name AS AllocationName FROM " & mstrDataBaseName & "..hrunitgroup_master WHERE isactive =1 "

                Case enAllocation.UNIT

                    strQ = "SELECT unitunkid AS AllocationId,name AS AllocationName FROM " & mstrDataBaseName & "..hrunit_master WHERE isactive =1 "

                Case enAllocation.TEAM

                    strQ = "SELECT teamunkid AS AllocationId,name AS AllocationName FROM " & mstrDataBaseName & "..hrteam_master WHERE isactive =1 "

                Case enAllocation.JOB_GROUP

                    strQ = "SELECT jobgroupunkid AS AllocationId,name AS AllocationName FROM " & mstrDataBaseName & "..hrjobgroup_master WHERE isactive =1 "

                Case enAllocation.JOBS

                    strQ = "SELECT jobunkid AS AllocationId, job_name As AllocationName FROM " & mstrDataBaseName & "..hrjob_master WHERE isactive =1 "

                Case enAllocation.CLASS_GROUP

                    strQ = "SELECT classgroupunkid AS AllocationId ,name AS AllocationName FROM " & mstrDataBaseName & "..hrclassgroup_master WHERE isactive =1 "

                Case enAllocation.CLASSES

                    strQ = "SELECT classesunkid AS AllocationId, name AS AllocationName  FROM " & mstrDataBaseName & "..hrclasses_master WHERE isactive =1 "

                Case enAllocation.COST_CENTER

                    strQ = " SELECT costcenterunkid AS AllocationId, costcentername AS AllocationName FROM " & mstrDataBaseName & "..prcostcenter_master WHERE isactive =1"

            End Select

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEntryPointAllocation; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    'Pinkal (09-Mar-2020) -- End


End Class