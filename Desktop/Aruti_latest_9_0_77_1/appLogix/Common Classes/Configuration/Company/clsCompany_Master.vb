﻿'************************************************************************************************************************************
'Class Name : clsCompany_Master.vb
'Purpose    :
'Date       :28/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
'Imports UpdateData
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsCompany_Master
    Private Shared ReadOnly mstrModuleName As String = "clsCompany_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    'Sandeep [ 01 MARCH 2011 ] -- Start
    Dim objCBankTran As New clsCompany_Bank_tran
    'Sandeep [ 01 MARCH 2011 ] -- End 

    'S.SANDEEP [ 12 OCT 2011 ] -- START

    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Dim objConfig As New clsConfigOptions
    Dim objConfig As clsConfigOptions
    'S.SANDEEP [ 26 SEPT 2013 ] -- END

    'S.SANDEEP [ 12 OCT 2011 ] -- END 



#Region " Private variables "
    Private mintCompanyunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mintCityunkid As Integer
    Private mintPostalunkid As Integer
    Private mintStateunkid As Integer
    Private mintCountryunkid As Integer
    Private mstrPhone1 As String = String.Empty
    Private mstrPhone2 As String = String.Empty
    Private mstrPhone3 As String = String.Empty
    Private mstrFax As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mstrWebsite As String = String.Empty
    Private mstrCompany_Reg_No As String = String.Empty
    Private mstrRegisterdno As String = String.Empty
    Private mstrTinno As String = String.Empty
    Private mstrVatno As String = String.Empty
    Private mstrNssfno As String = String.Empty
    Private mstrDistrict As String = String.Empty
    Private mstrPpfno As String = String.Empty
    Private mstrPayrollno As String = String.Empty
    Private mimgImage As Image
    Private mintFin_Start_Day As Integer
    Private mintFin_Start_Month As Integer
    Private mintFin_Start_Year As Integer
    Private mintFin_End_Day As Integer
    Private mintFin_End_Month As Integer
    Private mintFin_End_Year As Integer
    Private mintBankgroupunkid As Integer
    Private mintBranchunkid As Integer
    Private mstrBank_No As String = String.Empty
    Private mstrAccountno As String = String.Empty
    Private mstrSendername As String = String.Empty
    Private mstrSenderaddress As String = String.Empty
    Private mstrReference As String = String.Empty
    Private mstrMailserverip As String = String.Empty
    Private mintMailserverport As Integer
    Private mstrUsername As String = String.Empty
    Private mstrPassword As String = String.Empty
    Private mblnIsloginssl As Boolean
    'Hemant (01 Feb 2022) -- Start
    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
    Private mintProtocolunkid As Integer
    'Hemant (01 Feb 2022) -- End
    Private mblnIsreg1captionused As Boolean
    Private mstrReg1_Caption As String = String.Empty
    Private mstrReg1_Value As String = String.Empty
    Private mblnIsreg2captionused As Boolean
    Private mstrReg2_Caption As String = String.Empty
    Private mstrReg2_Value As String = String.Empty
    Private mblnIsreg3captionused As Boolean
    Private mstrReg3_Caption As String = String.Empty
    Private mstrReg3_Value As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrMail_Body As String = String.Empty
    'Sohail (26 Jul 2010) -- Start
    Public mdtFinanceStartDate As Date = Now.Today
    Public mdtFinanceEndDate As Date = Now.Today
    'Sohail (26 Jul 2010) -- End
    Private mstrNewDatabaseName As String = String.Empty

    'Sandeep [ 07 APRIL 2011 ] -- Start
    Private mstrCountryName As String = String.Empty
    Private mstrStateName As String = String.Empty
    Private mstrCityName As String = String.Empty
    Private mstrPostCode As String = String.Empty
    'Sandeep [ 07 APRIL 2011 ] -- End 


    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Private mintLocalizationCountryUnkid As Integer
    Private mstrLocalizationCurrencySign As String
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    Private mimgStamp As Image 'Sohail (27 Mar 2014)
    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
    Private mblnIsCertificateAuthorization As Boolean = False
    'S.SANDEEP [11-AUG-2017] -- END
    'Sohail (30 Nov 2017) -- Start
    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    Private mintEmail_Type As Integer = 0 '0 = SMTP, 1 = EWS
    Private mstrEWS_URL As String = String.Empty
    Private mstrEWS_Domain As String = String.Empty
    'Sohail (30 Nov 2017) -- End
    'Sohail (13 Dec 2017) -- Start
    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    Private mblnFromSendEmail As Boolean = False
    'Sohail (13 Dec 2017) -- End

    'S.SANDEEP [21-SEP-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002552}
    Private mblnIsbypassproxy As Boolean = False
    'S.SANDEEP [21-SEP-2018] -- END

#End Region

#Region " Financial Year "
    Private mintYearUnkid As Integer = -1
    Private mstrFinancialYear_Name As String = String.Empty
    Private mstrDatabaseName As String = String.Empty
    Private mint_Fin_Company_Id As Integer = 0
    Private mblnIsFin_Close As Boolean = False

    'Sohail (24 Aug 2010) -- Start
    Private mdtDatabase_Start_Date As Date
    Private mdtDatabase_End_Date As Date
    'Sohail (24 Aug 2010) -- End
    Private gstrConfigDatabaseName As String = "hrmsConfiguration"
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Sohail (13 Dec 2017) -- Start
    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    Public WriteOnly Property _FromSendEmail() As Boolean
        Set(ByVal value As Boolean)
            mblnFromSendEmail = value
        End Set
    End Property
    'Sohail (13 Dec 2017) -- End

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set postalunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Postalunkid() As Integer
        Get
            Return mintPostalunkid
        End Get
        Set(ByVal value As Integer)
            mintPostalunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Phone1() As String
        Get
            Return mstrPhone1
        End Get
        Set(ByVal value As String)
            mstrPhone1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Phone2() As String
        Get
            Return mstrPhone2
        End Get
        Set(ByVal value As String)
            mstrPhone2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone3
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Phone3() As String
        Get
            Return mstrPhone3
        End Get
        Set(ByVal value As String)
            mstrPhone3 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fax
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fax() As String
        Get
            Return mstrFax
        End Get
        Set(ByVal value As String)
            mstrFax = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set website
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Website() As String
        Get
            Return mstrWebsite
        End Get
        Set(ByVal value As String)
            mstrWebsite = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company_reg_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Company_Reg_No() As String
        Get
            Return mstrCompany_Reg_No
        End Get
        Set(ByVal value As String)
            mstrCompany_Reg_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set registerdno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Registerdno() As String
        Get
            Return mstrRegisterdno
        End Get
        Set(ByVal value As String)
            mstrRegisterdno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tinno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Tinno() As String
        Get
            Return mstrTinno
        End Get
        Set(ByVal value As String)
            mstrTinno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vatno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Vatno() As String
        Get
            Return mstrVatno
        End Get
        Set(ByVal value As String)
            mstrVatno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nssfno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Nssfno() As String
        Get
            Return mstrNssfno
        End Get
        Set(ByVal value As String)
            mstrNssfno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set district
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _District() As String
        Get
            Return mstrDistrict
        End Get
        Set(ByVal value As String)
            mstrDistrict = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ppfno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ppfno() As String
        Get
            Return mstrPpfno
        End Get
        Set(ByVal value As String)
            mstrPpfno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set payrollno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Payrollno() As String
        Get
            Return mstrPayrollno
        End Get
        Set(ByVal value As String)
            mstrPayrollno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set image
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Image() As Image
        Get
            Return mimgImage
        End Get
        Set(ByVal value As Image)
            mimgImage = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fin_start_day
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fin_Start_Day() As Integer
        Get
            Return mintFin_Start_Day
        End Get
        Set(ByVal value As Integer)
            mintFin_Start_Day = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fin_start_month
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fin_Start_Month() As Integer
        Get
            Return mintFin_Start_Month
        End Get
        Set(ByVal value As Integer)
            mintFin_Start_Month = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fin_start_year
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fin_Start_Year() As Integer
        Get
            Return mintFin_Start_Year
        End Get
        Set(ByVal value As Integer)
            mintFin_Start_Year = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fin_end_day
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fin_End_Day() As Integer
        Get
            Return mintFin_End_Day
        End Get
        Set(ByVal value As Integer)
            mintFin_End_Day = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fin_end_month
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fin_End_Month() As Integer
        Get
            Return mintFin_End_Month
        End Get
        Set(ByVal value As Integer)
            mintFin_End_Month = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fin_end_year
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fin_End_Year() As Integer
        Get
            Return mintFin_End_Year
        End Get
        Set(ByVal value As Integer)
            mintFin_End_Year = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bankgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Bankgroupunkid() As Integer
        Get
            Return mintBankgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintBankgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bank_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Bank_No() As String
        Get
            Return mstrBank_No
        End Get
        Set(ByVal value As String)
            mstrBank_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Accountno() As String
        Get
            Return mstrAccountno
        End Get
        Set(ByVal value As String)
            mstrAccountno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sendername
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Sendername() As String
        Get
            Return mstrSendername
        End Get
        Set(ByVal value As String)
            mstrSendername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set senderaddress
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Senderaddress() As String
        Get
            Return mstrSenderaddress
        End Get
        Set(ByVal value As String)
            mstrSenderaddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reference
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reference() As String
        Get
            Return mstrReference
        End Get
        Set(ByVal value As String)
            mstrReference = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mailserverip
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Mailserverip() As String
        Get
            Return mstrMailserverip
        End Get
        Set(ByVal value As String)
            mstrMailserverip = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mailserverport
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Mailserverport() As Integer
        Get
            Return mintMailserverport
        End Get
        Set(ByVal value As Integer)
            mintMailserverport = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set username
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Username() As String
        Get
            Return mstrUsername
        End Get
        Set(ByVal value As String)
            mstrUsername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set password
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Password() As String
        Get
            Return mstrPassword
        End Get
        Set(ByVal value As String)
            mstrPassword = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isloginssl
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isloginssl() As Boolean
        Get
            Return mblnIsloginssl
        End Get
        Set(ByVal value As Boolean)
            mblnIsloginssl = Value
        End Set
    End Property

    'Hemant (01 Feb 2022) -- Start
    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
    Public Property _Protocolunkid() As Integer
        Get
            Return mintProtocolunkid
        End Get
        Set(ByVal value As Integer)
            mintProtocolunkid = value
        End Set
    End Property
    'Hemant (01 Feb 2022) -- End

    ''' <summary>
    ''' Purpose: Get or Set isreg1captionused
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isreg1captionused() As Boolean
        Get
            Return mblnIsreg1captionused
        End Get
        Set(ByVal value As Boolean)
            mblnIsreg1captionused = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reg1_caption
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reg1_Caption() As String
        Get
            Return mstrReg1_Caption
        End Get
        Set(ByVal value As String)
            mstrReg1_Caption = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reg1_value
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reg1_Value() As String
        Get
            Return mstrReg1_Value
        End Get
        Set(ByVal value As String)
            mstrReg1_Value = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isreg2captionused
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isreg2captionused() As Boolean
        Get
            Return mblnIsreg2captionused
        End Get
        Set(ByVal value As Boolean)
            mblnIsreg2captionused = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reg2_caption
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reg2_Caption() As String
        Get
            Return mstrReg2_Caption
        End Get
        Set(ByVal value As String)
            mstrReg2_Caption = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reg2_value
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reg2_Value() As String
        Get
            Return mstrReg2_Value
        End Get
        Set(ByVal value As String)
            mstrReg2_Value = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isreg3captionused
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isreg3captionused() As Boolean
        Get
            Return mblnIsreg3captionused
        End Get
        Set(ByVal value As Boolean)
            mblnIsreg3captionused = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reg3_caption
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reg3_Caption() As String
        Get
            Return mstrReg3_Caption
        End Get
        Set(ByVal value As String)
            mstrReg3_Caption = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reg3_value
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reg3_Value() As String
        Get
            Return mstrReg3_Value
        End Get
        Set(ByVal value As String)
            mstrReg3_Value = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Mail_Body() As String
        Get
            Return mstrMail_Body
        End Get
        Set(ByVal value As String)
            mstrMail_Body = value
        End Set
    End Property
    'Sohail (26 Jul 2010) -- Start
    ''' <summary>
    ''' Get Financial Start Date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _Financial_Start_Date() As Date
        Get
            Return mdtFinanceStartDate
        End Get
    End Property

    ''' <summary>
    ''' Get Financial End Date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _Financial_End_Date() As Date
        Get
            Return mdtFinanceEndDate
        End Get
    End Property
    'Sohail (26 Jul 2010) -- End



    'Sandeep [ 07 APRIL 2011 ] -- Start
    Public Property _Country_Name() As String
        Get
            Return mstrCountryName
        End Get
        Set(ByVal value As String)
            mstrCountryName = value
        End Set
    End Property

    Public Property _State_Name() As String
        Get
            Return mstrStateName
        End Get
        Set(ByVal value As String)
            mstrStateName = value
        End Set
    End Property

    Public Property _City_Name() As String
        Get
            Return mstrCityName
        End Get
        Set(ByVal value As String)
            mstrCityName = value
        End Set
    End Property

    Public Property _Post_Code_No() As String
        Get
            Return mstrPostCode
        End Get
        Set(ByVal value As String)
            mstrPostCode = value
        End Set
    End Property
    'Sandeep [ 07 APRIL 2011 ] -- End 

    'Sohail (09 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    'Sohail (04 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    'Private mdtTotalActiveEmployeeAsOnFromDate As Date = Nothing
    'Public WriteOnly Property _Total_Active_Employee_AsOnFromDate() As Date
    '    Set(ByVal value As Date)
    '        mdtTotalActiveEmployeeAsOnFromDate = value
    '    End Set
    'End Property

    'Private mdtTotalActiveEmployeeAsOnToDate As Date = Nothing
    'Public WriteOnly Property _Total_Active_Employee_AsOnToDate() As Date
    '    Set(ByVal value As Date)
    '        mdtTotalActiveEmployeeAsOnToDate = value
    '    End Set
    'End Property
    Private mdtTotalActiveEmployeeAsOnDate As Date = Nothing
    Public WriteOnly Property _Total_Active_Employee_AsOnDate() As Date
        Set(ByVal value As Date)
            mdtTotalActiveEmployeeAsOnDate = value
        End Set
    End Property
    'Sohail (04 Jun 2013) -- End

    '''' <summary>
    '' For Aruti Licence : Return Total No. of Active Employees for current Company based on _Total_Active_Employee_AsOnFromDate AND _Total_Active_Employee_AsOnToDate. For overall all employees, set _Total_Active_Employee_AsOnFromDate = Nothing and _Total_Active_Employee_AsOnToDate = Nothing.
    '' </summary>
    '' <value></value>
    '' <returns></returns>
    '' <remarks></remarks>
    'Public ReadOnly Property _Total_Active_Employee() As Integer
    '    Get
    '        Return GetTotalActiveEmployeeCount(mdtTotalActiveEmployeeAsOnFromDate, mdtTotalActiveEmployeeAsOnToDate)
    '    End Get
    'End Property

    ''' <summary>
    ''' For Aruti Licence : Return Total No. of Active Employees for All Active Company based on _Total_Active_Employee_AsOnFromDate AND _Total_Active_Employee_AsOnToDate. For overall all employees, set _Total_Active_Employee_AsOnFromDate = Nothing and _Total_Active_Employee_AsOnToDate = Nothing.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _Total_Active_Employee_ForAllCompany() As Integer
        Get
            Return GetTotalActiveEmployeeCountForAllCompany(mdtTotalActiveEmployeeAsOnDate)
        End Get
    End Property
    'Public ReadOnly Property _Total_Active_Employee_ForAllCompany() As Integer
    '    Get
    '        Return GetTotalActiveEmployeeCountForAllCompany(mdtTotalActiveEmployeeAsOnFromDate, mdtTotalActiveEmployeeAsOnToDate)
    '    End Get
    'End Property
    'Sohail (09 Apr 2013) -- End

    'Sohail (10 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' For Aruti Licence : Return Total No. of Active Companies.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _Total_Active_Company() As Integer
        Get
            Return GetTotalActiveCompanyCount()
        End Get
    End Property
    'Sohail (10 Apr 2013) -- End

    'Sohail (27 Mar 2014) -- Start
    'ENHANCEMENT - Show Company Stamp on Payslip.
    ''' <summary>
    ''' Purpose: Get or Set stamp
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Stamp() As Image
        Get
            Return mimgStamp
        End Get
        Set(ByVal value As Image)
            mimgStamp = value
        End Set
    End Property
    'Sohail (27 Mar 2014) -- End

    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
    Public Property _IsCertificateAuthorization() As Boolean
        Get
            Return mblnIsCertificateAuthorization
        End Get
        Set(ByVal value As Boolean)
            mblnIsCertificateAuthorization = value
        End Set
    End Property
    'S.SANDEEP [11-AUG-2017] -- END

    'Sohail (30 Nov 2017) -- Start
    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    ''' <summary>
    ''' Get EWS Type : 0 = SMTP, 1 = EWS
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>0 = SMTP, 1 = EWS</remarks>
    Public Property _Email_Type() As Integer
        Get
            Return mintEmail_Type
        End Get
        Set(ByVal value As Integer)
            mintEmail_Type = value
        End Set
    End Property

    Public Property _EWS_URL() As String
        Get
            Return mstrEWS_URL
        End Get
        Set(ByVal value As String)
            mstrEWS_URL = value
        End Set
    End Property

    Public Property _EWS_Domain() As String
        Get
            Return mstrEWS_Domain
        End Get
        Set(ByVal value As String)
            mstrEWS_Domain = value
        End Set
    End Property
    'Sohail (30 Nov 2017) -- End

#Region " Financial  Properties "
    Public Property _YearUnkid() As Integer
        Get
            Return mintYearUnkid
        End Get
        Set(ByVal value As Integer)
            mintYearUnkid = value
            Call GetFinalcialYear_Data()
        End Set
    End Property

    Public Property _FinancialYear_Name() As String
        Get
            Return mstrFinancialYear_Name
        End Get
        Set(ByVal value As String)
            mstrFinancialYear_Name = value
        End Set
    End Property

    Public Property _DatabaseName() As String
        Get
            Return mstrDatabaseName
        End Get
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _Fin_Company_Id() As Integer
        Get
            Return mint_Fin_Company_Id
        End Get
        Set(ByVal value As Integer)
            mint_Fin_Company_Id = value
        End Set
    End Property

    Public Property _IsFin_Close() As Boolean
        Get
            Return mblnIsFin_Close
        End Get
        Set(ByVal value As Boolean)
            mblnIsFin_Close = value
        End Set
    End Property

    'Sohail (24 Aug 2010) -- Start
    Public Property _Database_Start_Date() As Date
        Get
            Return mdtDatabase_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtDatabase_Start_Date = value
        End Set
    End Property

    Public Property _Database_End_Date() As Date
        Get
            Return mdtDatabase_End_Date
        End Get
        Set(ByVal value As Date)
            mdtDatabase_End_Date = value
        End Set
    End Property
    'Sohail (24 Aug 2010) -- End

    Public ReadOnly Property _ConfigDatabaseName() As String
        Get
            Return gstrConfigDatabaseName
        End Get
    End Property
#End Region

    Public ReadOnly Property _NewDataBaseName() As String
        Get
            Return mstrNewDatabaseName
        End Get
    End Property


    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Public Property _Localization_Country() As Integer
        Get
            Return mintLocalizationCountryUnkid
        End Get
        Set(ByVal value As Integer)
            mintLocalizationCountryUnkid = value
        End Set
    End Property

    Public Property _Localization_Currency() As String
        Get
            Return mstrLocalizationCurrencySign
        End Get
        Set(ByVal value As String)
            mstrLocalizationCurrencySign = value
        End Set
    End Property
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    'S.SANDEEP [21-SEP-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002552}
    ''' <summary>
    ''' Purpose: Get or Set isbypassproxy
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Isbypassproxy() As Boolean
        Get
            Return mblnIsbypassproxy
        End Get
        Set(ByVal value As Boolean)
            mblnIsbypassproxy = Value
        End Set
    End Property
    'S.SANDEEP [21-SEP-2018] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 29 NOV 2010 ] -- Start
            'strQ = "SELECT " & _
            '  "  companyunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", cityunkid " & _
            '  ", postalunkid " & _
            '  ", stateunkid " & _
            '  ", countryunkid " & _
            '  ", phone1 " & _
            '  ", phone2 " & _
            '  ", phone3 " & _
            '  ", fax " & _
            '  ", email " & _
            '  ", website " & _
            '  ", company_reg_no " & _
            '  ", registerdno " & _
            '  ", tinno " & _
            '  ", vatno " & _
            '  ", nssfno " & _
            '  ", district " & _
            '  ", ppfno " & _
            '  ", payrollno " & _
            '  ", image " & _
            '  ", fin_start_day " & _
            '  ", fin_start_month " & _
            '  ", fin_start_year " & _
            '  ", fin_end_day " & _
            '  ", fin_end_month " & _
            '  ", fin_end_year " & _
            '  ", bankgroupunkid " & _
            '  ", branchunkid " & _
            '  ", bank_no " & _
            '  ", accountno " & _
            '  ", sendername " & _
            '  ", senderaddress " & _
            '  ", reference " & _
            '  ", mailserverip " & _
            '  ", mailserverport " & _
            '  ", username " & _
            '  ", password " & _
            '  ", isloginssl " & _
            '  ", isreg1captionused " & _
            '  ", reg1_caption " & _
            '  ", reg1_value " & _
            '  ", isreg2captionused " & _
            '  ", reg2_caption " & _
            '  ", reg2_value " & _
            '  ", isreg3captionused " & _
            '  ", reg3_caption " & _
            '  ", reg3_value " & _
            '  ", isactive " & _
            '  ",mail_body " & _
            ' "FROM cfcompany_master " & _
            ' "WHERE companyunkid = @companyunkid "


            'Sandeep [ 07 APRIL 2011 ] -- Start
            'strQ = "SELECT " & _
            '  "  companyunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", cityunkid " & _
            '  ", postalunkid " & _
            '  ", stateunkid " & _
            '  ", countryunkid " & _
            '  ", phone1 " & _
            '  ", phone2 " & _
            '  ", phone3 " & _
            '  ", fax " & _
            '  ", email " & _
            '  ", website " & _
            '  ", company_reg_no " & _
            '  ", registerdno " & _
            '  ", tinno " & _
            '  ", vatno " & _
            '  ", nssfno " & _
            '  ", district " & _
            '  ", ppfno " & _
            '  ", payrollno " & _
            '  ", image " & _
            '  ", fin_start_day " & _
            '  ", fin_start_month " & _
            '  ", fin_start_year " & _
            '  ", fin_end_day " & _
            '  ", fin_end_month " & _
            '  ", fin_end_year " & _
            '  ", bankgroupunkid " & _
            '  ", branchunkid " & _
            '  ", bank_no " & _
            '  ", accountno " & _
            '  ", sendername " & _
            '  ", senderaddress " & _
            '  ", reference " & _
            '  ", mailserverip " & _
            '  ", mailserverport " & _
            '  ", username " & _
            '  ", password " & _
            '  ", isloginssl " & _
            '  ", isreg1captionused " & _
            '  ", reg1_caption " & _
            '  ", reg1_value " & _
            '  ", isreg2captionused " & _
            '  ", reg2_caption " & _
            '  ", reg2_value " & _
            '  ", isreg3captionused " & _
            '  ", reg3_caption " & _
            '  ", reg3_value " & _
            '  ", isactive " & _
            '  ",mail_body " & _
            ' "FROM hrmsConfiguration..cfcompany_master " & _
            ' "WHERE companyunkid = @companyunkid "

            strQ = "SELECT " & _
              "  hrmsConfiguration..cfcompany_master.companyunkid " & _
              ", hrmsConfiguration..cfcompany_master.code " & _
              ", hrmsConfiguration..cfcompany_master.name " & _
              ", hrmsConfiguration..cfcompany_master.address1 " & _
              ", hrmsConfiguration..cfcompany_master.address2 " & _
              ", hrmsConfiguration..cfcompany_master.cityunkid " & _
              ", hrmsConfiguration..cfcompany_master.postalunkid " & _
              ", hrmsConfiguration..cfcompany_master.stateunkid " & _
              ", hrmsConfiguration..cfcompany_master.countryunkid " & _
              ", hrmsConfiguration..cfcompany_master.phone1 " & _
              ", hrmsConfiguration..cfcompany_master.phone2 " & _
              ", hrmsConfiguration..cfcompany_master.phone3 " & _
              ", hrmsConfiguration..cfcompany_master.fax " & _
              ", hrmsConfiguration..cfcompany_master.email " & _
              ", hrmsConfiguration..cfcompany_master.website " & _
              ", hrmsConfiguration..cfcompany_master.company_reg_no " & _
              ", hrmsConfiguration..cfcompany_master.registerdno " & _
              ", hrmsConfiguration..cfcompany_master.tinno " & _
              ", hrmsConfiguration..cfcompany_master.vatno " & _
              ", hrmsConfiguration..cfcompany_master.nssfno " & _
              ", hrmsConfiguration..cfcompany_master.district " & _
              ", hrmsConfiguration..cfcompany_master.ppfno " & _
              ", hrmsConfiguration..cfcompany_master.payrollno " & _
              ", hrmsConfiguration..cfcompany_master.image " & _
              ", hrmsConfiguration..cfcompany_master.fin_start_day " & _
              ", hrmsConfiguration..cfcompany_master.fin_start_month " & _
              ", hrmsConfiguration..cfcompany_master.fin_start_year " & _
              ", hrmsConfiguration..cfcompany_master.fin_end_day " & _
              ", hrmsConfiguration..cfcompany_master.fin_end_month " & _
              ", hrmsConfiguration..cfcompany_master.fin_end_year " & _
              ", hrmsConfiguration..cfcompany_master.bankgroupunkid " & _
              ", hrmsConfiguration..cfcompany_master.branchunkid " & _
              ", hrmsConfiguration..cfcompany_master.bank_no " & _
              ", hrmsConfiguration..cfcompany_master.accountno " & _
              ", hrmsConfiguration..cfcompany_master.sendername " & _
              ", hrmsConfiguration..cfcompany_master.senderaddress " & _
              ", hrmsConfiguration..cfcompany_master.reference " & _
              ", hrmsConfiguration..cfcompany_master.mailserverip " & _
              ", hrmsConfiguration..cfcompany_master.mailserverport " & _
              ", hrmsConfiguration..cfcompany_master.username " & _
              ", hrmsConfiguration..cfcompany_master.password " & _
              ", hrmsConfiguration..cfcompany_master.isloginssl " & _
              ", hrmsConfiguration..cfcompany_master.isreg1captionused " & _
              ", hrmsConfiguration..cfcompany_master.reg1_caption " & _
              ", hrmsConfiguration..cfcompany_master.reg1_value " & _
              ", hrmsConfiguration..cfcompany_master.isreg2captionused " & _
              ", hrmsConfiguration..cfcompany_master.reg2_caption " & _
              ", hrmsConfiguration..cfcompany_master.reg2_value " & _
              ", hrmsConfiguration..cfcompany_master.isreg3captionused " & _
              ", hrmsConfiguration..cfcompany_master.reg3_caption " & _
              ", hrmsConfiguration..cfcompany_master.reg3_value " & _
              ", hrmsConfiguration..cfcompany_master.isactive " & _
              ", hrmsConfiguration..cfcompany_master.mail_body " & _
              ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS CountryName " & _
              ", ISNULL(hrmsConfiguration..cfstate_master.name,'') AS StateName " & _
              ", ISNULL(hrmsConfiguration..cfcity_master.name,'') AS CityName " & _
              ", ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS PostCode " & _
              ", hrmsConfiguration..cfcompany_master.stamp " & _
              ", ISNULL(hrmsConfiguration..cfcompany_master.iscrt_authenticated, 0) AS iscrt_authenticated " & _
              ", ISNULL(hrmsConfiguration..cfcompany_master.email_type, 0) AS email_type " & _
              ", ISNULL(hrmsConfiguration..cfcompany_master.ews_url, '') AS ews_url " & _
              ", ISNULL(hrmsConfiguration..cfcompany_master.ews_domain, '') AS ews_domain " & _
              ", ISNULL(hrmsConfiguration..cfcompany_master.isbypassproxy,CAST(0 AS BIT)) AS isbypassproxy  " & _
              ", ISNULL(hrmsConfiguration..cfcompany_master.protocolunkid, 0) AS protocolunkid " & _
             "FROM hrmsConfiguration..cfcompany_master " & _
              " LEFT JOIN hrmsConfiguration..cfzipcode_master ON hrmsConfiguration..cfcompany_master.postalunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
              " LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcompany_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
              " LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfcompany_master.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
              " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcompany_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
             "WHERE companyunkid = @companyunkid "
            'Hemant (01 Feb 2022) -- [protocolunkid]
            'S.SANDEEP [21-SEP-2018] -- START {#0002552} [] -- END
            'Sohail (30 Nov 2017) - [email_type, ews_url, ews_domain]
            'Sohail (27 Mar 2014) - [stamp]
            'Sandeep [ 07 APRIL 2011 ] -- End 
            'Sandeep [ 29 NOV 2010 ] -- End 
            'S.SANDEEP [11-AUG-2017] -- START {iscrt_authenticated} -- END

            

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrAddress1 = dtRow.Item("address1").ToString
                mstrAddress2 = dtRow.Item("address2").ToString
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mintPostalunkid = CInt(dtRow.Item("postalunkid"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mstrPhone1 = dtRow.Item("phone1").ToString
                mstrPhone2 = dtRow.Item("phone2").ToString
                mstrPhone3 = dtRow.Item("phone3").ToString
                mstrFax = dtRow.Item("fax").ToString
                mstrEmail = dtRow.Item("email").ToString
                mstrWebsite = dtRow.Item("website").ToString
                mstrCompany_Reg_No = dtRow.Item("company_reg_no").ToString
                mstrRegisterdno = dtRow.Item("registerdno").ToString
                mstrTinno = dtRow.Item("tinno").ToString
                mstrVatno = dtRow.Item("vatno").ToString
                mstrNssfno = dtRow.Item("nssfno").ToString
                mstrDistrict = dtRow.Item("district").ToString
                mstrPpfno = dtRow.Item("ppfno").ToString
                mstrPayrollno = dtRow.Item("payrollno").ToString
                mimgImage = eZeeDataType.data2Image(dtRow.Item("image"))
                mintFin_Start_Day = CInt(dtRow.Item("fin_start_day"))
                mintFin_Start_Month = CInt(dtRow.Item("fin_start_month"))
                mintFin_Start_Year = CInt(dtRow.Item("fin_start_year"))
                mintFin_End_Day = CInt(dtRow.Item("fin_end_day"))
                mintFin_End_Month = CInt(dtRow.Item("fin_end_month"))
                mintFin_End_Year = CInt(dtRow.Item("fin_end_year"))
                mintBankgroupunkid = CInt(dtRow.Item("bankgroupunkid"))
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mstrBank_No = dtRow.Item("bank_no").ToString
                mstrAccountno = dtRow.Item("accountno").ToString
                mstrSendername = dtRow.Item("sendername").ToString
                mstrSenderaddress = dtRow.Item("senderaddress").ToString
                mstrReference = dtRow.Item("reference").ToString
                mstrMailserverip = dtRow.Item("mailserverip").ToString
                mintMailserverport = CInt(dtRow.Item("mailserverport"))
                mstrUsername = dtRow.Item("username").ToString
                mstrPassword = dtRow.Item("password").ToString
                mblnIsloginssl = CBool(dtRow.Item("isloginssl"))
                mblnIsreg1captionused = CBool(dtRow.Item("isreg1captionused"))
                mstrReg1_Caption = dtRow.Item("reg1_caption").ToString
                mstrReg1_Value = dtRow.Item("reg1_value").ToString
                mblnIsreg2captionused = CBool(dtRow.Item("isreg2captionused"))
                mstrReg2_Caption = dtRow.Item("reg2_caption").ToString
                mstrReg2_Value = dtRow.Item("reg2_value").ToString
                mblnIsreg3captionused = CBool(dtRow.Item("isreg3captionused"))
                mstrReg3_Caption = dtRow.Item("reg3_caption").ToString
                mstrReg3_Value = dtRow.Item("reg3_value").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrMail_Body = CStr(dtRow.Item("mail_body"))

                'Sandeep [ 07 APRIL 2011 ] -- Start
                mstrCountryName = CStr(dtRow.Item("CountryName"))
                mstrStateName = CStr(dtRow.Item("StateName"))
                mstrCityName = CStr(dtRow.Item("CityName"))
                mstrPostCode = CStr(dtRow.Item("PostCode"))
                'Sandeep [ 07 APRIL 2011 ] -- End 

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                mintEmail_Type = CInt(dtRow.Item("email_type"))
                mstrEWS_URL = dtRow.Item("ews_url").ToString
                mstrEWS_Domain = dtRow.Item("ews_domain").ToString
                'Sohail (30 Nov 2017) -- End



                'S.SANDEEP [ 12 OCT 2011 ] -- START

                'S.SANDEEP [ 26 SEPT 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objConfig = New clsConfigOptions
                'S.SANDEEP [ 26 SEPT 2013 ] -- END

                'Pinkal (11-Dec-2018) -- Start
                'Issue - Login page is taking time to login in NMB.
                'objConfig._Companyunkid = mintCompanyunkid
                'mintLocalizationCountryUnkid = objConfig._CountryUnkid
                'mstrLocalizationCurrencySign = objConfig._Currency
                objConfig.IsValue_Changed("CountryUnkid", mintCompanyunkid.ToString(), mintLocalizationCountryUnkid)
                objConfig.IsValue_Changed("CurrencySign", mintCompanyunkid.ToString(), mstrLocalizationCurrencySign)
                'Pinkal (11-Dec-2018) -- End


                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                mimgStamp = eZeeDataType.data2Image(dtRow.Item("stamp")) 'Sohail (27 Mar 2014)

                'S.SANDEEP [11-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
                mblnIsCertificateAuthorization = CBool(dtRow.Item("iscrt_authenticated"))
                'S.SANDEEP [11-AUG-2017] -- END

                'S.SANDEEP [21-SEP-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002552}
                mblnIsbypassproxy = CBool(dtRow.Item("isbypassproxy"))
                'S.SANDEEP [21-SEP-2018] -- END

                'Hemant (01 Feb 2022) -- Start
                'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                mintProtocolunkid = CInt(dtRow.Item("protocolunkid"))
                'Hemant (01 Feb 2022) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (13 Dec 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'objDataOperation = Nothing
            If mblnFromSendEmail = False Then objDataOperation = Nothing
            'Sohail (13 Dec 2017) -- End
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            'Sandeep [ 29 NOV 2010 ] -- Start
            'strQ = "SELECT " & _
            '  "  companyunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", cityunkid " & _
            '  ", postalunkid " & _
            '  ", stateunkid " & _
            '  ", countryunkid " & _
            '  ", phone1 " & _
            '  ", phone2 " & _
            '  ", phone3 " & _
            '  ", fax " & _
            '  ", email " & _
            '  ", website " & _
            '  ", company_reg_no " & _
            '  ", registerdno " & _
            '  ", tinno " & _
            '  ", vatno " & _
            '  ", nssfno " & _
            '  ", district " & _
            '  ", ppfno " & _
            '  ", payrollno " & _
            '  ", image " & _
            '  ", fin_start_day " & _
            '  ", fin_start_month " & _
            '  ", fin_start_year " & _
            '  ", fin_end_day " & _
            '  ", fin_end_month " & _
            '  ", fin_end_year " & _
            '  ", bankgroupunkid " & _
            '  ", branchunkid " & _
            '  ", bank_no " & _
            '  ", accountno " & _
            '  ", sendername " & _
            '  ", senderaddress " & _
            '  ", reference " & _
            '  ", mailserverip " & _
            '  ", mailserverport " & _
            '  ", username " & _
            '  ", password " & _
            '  ", isloginssl " & _
            '  ", isreg1captionused " & _
            '  ", reg1_caption " & _
            '  ", reg1_value " & _
            '  ", isreg2captionused " & _
            '  ", reg2_caption " & _
            '  ", reg2_value " & _
            '  ", isreg3captionused " & _
            '  ", reg3_caption " & _
            '  ", reg3_value " & _
            '  ", isactive " & _
            '  ",mail_body " & _
            ' "FROM cfcompany_master "

            strQ = "SELECT " & _
              "  companyunkid " & _
              ", code " & _
              ", name " & _
              ", address1 " & _
              ", address2 " & _
              ", cityunkid " & _
              ", postalunkid " & _
              ", stateunkid " & _
              ", countryunkid " & _
              ", phone1 " & _
              ", phone2 " & _
              ", phone3 " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", company_reg_no " & _
              ", registerdno " & _
              ", tinno " & _
              ", vatno " & _
              ", nssfno " & _
              ", district " & _
              ", ppfno " & _
              ", payrollno " & _
              ", image " & _
              ", fin_start_day " & _
              ", fin_start_month " & _
              ", fin_start_year " & _
              ", fin_end_day " & _
              ", fin_end_month " & _
              ", fin_end_year " & _
              ", bankgroupunkid " & _
              ", branchunkid " & _
              ", bank_no " & _
              ", accountno " & _
              ", sendername " & _
              ", senderaddress " & _
              ", reference " & _
              ", mailserverip " & _
              ", mailserverport " & _
              ", username " & _
              ", password " & _
              ", isloginssl " & _
              ", isreg1captionused " & _
              ", reg1_caption " & _
              ", reg1_value " & _
              ", isreg2captionused " & _
              ", reg2_caption " & _
              ", reg2_value " & _
              ", isreg3captionused " & _
              ", reg3_caption " & _
              ", reg3_value " & _
              ", isactive " & _
              ", mail_body " & _
              ", stamp " & _
              ", iscrt_authenticated " & _
              ", email_type " & _
              ", ews_url " & _
              ", ews_domain " & _
              ", isbypassproxy " & _
              ", ISNULL(protocolunkid, 0) AS protocolunkid " & _
             "FROM hrmsConfiguration..cfcompany_master "

            'Hemant (01 Feb 2022) -- [protocolunkid]
            'S.SANDEEP [21-SEP-2018] -- START {#0002552} [isbypassproxy] -- END
            'Sohail (30 Nov 2017) - [email_type, ews_url, ews_domain]
            'Sohail (27 Mar 2014) - [stamp]
            'Sandeep [ 29 NOV 2010 ] -- End 
            'S.SANDEEP [11-AUG-2017] -- START {iscrt_authenticated} -- END

            

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function



    'Sandeep [ 01 MARCH 2011 ] -- Start
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfcompany_master) </purpose>
    ''' Public Function Insert() As Boolean
    Public Function Insert(Optional ByVal dtTable As DataTable = Nothing) As Boolean
        'Sandeep [ 01 MARCH 2011 ] -- End 

        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Company is already defined. Please define new Company.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Sandeep [ 01 MARCH 2011 ] -- Start
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation
        'Sandeep [ 01 MARCH 2011 ] -- End 


        If makeCompDatabase() = False Then
            'Sandeep [ 07 FEB 2011 ] -- START
            'objDataOperation.ExecNonQuery("IF EXISTS (SELECT * FROM sys.databases WHERE name ='" & mstrNewDatabaseName & "'" & ") DROP DATABASE " & mstrNewDatabaseName)
            Dim objDo As New clsDataOperation
            objDo.ExecNonQuery("USE master")
            objDo.ExecNonQuery("IF EXISTS (SELECT * FROM sys.databases WHERE name = '" & mstrNewDatabaseName & "' ) DROP DATABASE " & mstrNewDatabaseName)
            objDo = Nothing
            'Sandeep [ 07 FEB 2011 ] -- END 
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Company was not created successfully. Please contact Aruti Support team.")
            Return False
        End If


        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@postalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostalunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@phone1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone1.ToString)
            objDataOperation.AddParameter("@phone2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone2.ToString)
            objDataOperation.AddParameter("@phone3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone3.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebsite.ToString)
            objDataOperation.AddParameter("@company_reg_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_Reg_No.ToString)
            objDataOperation.AddParameter("@registerdno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegisterdno.ToString)
            objDataOperation.AddParameter("@tinno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTinno.ToString)
            objDataOperation.AddParameter("@vatno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVatno.ToString)
            objDataOperation.AddParameter("@nssfno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrNssfno.ToString)
            objDataOperation.AddParameter("@district", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDistrict.ToString)
            objDataOperation.AddParameter("@ppfno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPpfno.ToString)
            objDataOperation.AddParameter("@payrollno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPayrollno.ToString)

            If mimgImage Is Nothing Then
                objDataOperation.AddParameter("@image", SqlDbType.Binary, 0, DBNull.Value)
            Else
                Dim imgLogo() As Byte = eZeeDataType.image2Data(mimgImage)
                objDataOperation.AddParameter("@image", SqlDbType.Binary, imgLogo.Length, imgLogo)
            End If

            objDataOperation.AddParameter("@fin_start_day", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_Start_Day.ToString)
            objDataOperation.AddParameter("@fin_start_month", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_Start_Month.ToString)
            objDataOperation.AddParameter("@fin_start_year", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_Start_Year.ToString)
            objDataOperation.AddParameter("@fin_end_day", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_End_Day.ToString)
            objDataOperation.AddParameter("@fin_end_month", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_End_Month.ToString)
            objDataOperation.AddParameter("@fin_end_year", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_End_Year.ToString)
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@bank_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBank_No.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)
            objDataOperation.AddParameter("@sendername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSendername.ToString)
            objDataOperation.AddParameter("@senderaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSenderaddress.ToString)
            objDataOperation.AddParameter("@reference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference.ToString)
            objDataOperation.AddParameter("@mailserverip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMailserverip.ToString)
            objDataOperation.AddParameter("@mailserverport", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMailserverport.ToString)
            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPassword.ToString)
            objDataOperation.AddParameter("@isloginssl", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloginssl.ToString)
            objDataOperation.AddParameter("@isreg1captionused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreg1captionused.ToString)
            objDataOperation.AddParameter("@reg1_caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg1_Caption.ToString)
            objDataOperation.AddParameter("@reg1_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg1_Value.ToString)
            objDataOperation.AddParameter("@isreg2captionused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreg2captionused.ToString)
            objDataOperation.AddParameter("@reg2_caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg2_Caption.ToString)
            objDataOperation.AddParameter("@reg2_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg2_Value.ToString)
            objDataOperation.AddParameter("@isreg3captionused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreg3captionused.ToString)
            objDataOperation.AddParameter("@reg3_caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg3_Caption.ToString)
            objDataOperation.AddParameter("@reg3_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg3_Value.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@mail_body", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMail_Body.ToString)
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            objDataOperation.AddParameter("@email_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmail_Type)
            objDataOperation.AddParameter("@ews_url", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEWS_URL.ToString)
            objDataOperation.AddParameter("@ews_domain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEWS_Domain.ToString)
            'Sohail (30 Nov 2017) -- End

            'Sohail (27 Mar 2014) -- Start
            'ENHANCEMENT - Show Company Stamp on Payslip.
            If mimgStamp Is Nothing Then
                objDataOperation.AddParameter("@stamp", SqlDbType.Binary, 0, DBNull.Value)
            Else
                Dim imgStamp() As Byte = eZeeDataType.image2Data(mimgStamp)
                objDataOperation.AddParameter("@stamp", SqlDbType.Binary, imgStamp.Length, imgStamp)
            End If
            'Sohail (27 Mar 2014) -- End

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
            objDataOperation.AddParameter("@iscrt_authenticated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCertificateAuthorization)
            'S.SANDEEP [11-AUG-2017] -- END

            'S.SANDEEP [21-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002552}
            objDataOperation.AddParameter("@isbypassproxy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbypassproxy.ToString)
            'S.SANDEEP [21-SEP-2018] -- END

            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            objDataOperation.AddParameter("@protocolunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProtocolunkid.ToString)
            'Hemant (01 Feb 2022) -- End

            'Sandeep [ 29 NOV 2010 ] -- Start
            'strQ = "INSERT INTO cfcompany_master ( " & _
            '  "  code " & _
            '  ", name " & _
            '  ", address1 " & _
            '  ", address2 " & _
            '  ", cityunkid " & _
            '  ", postalunkid " & _
            '  ", stateunkid " & _
            '  ", countryunkid " & _
            '  ", phone1 " & _
            '  ", phone2 " & _
            '  ", phone3 " & _
            '  ", fax " & _
            '  ", email " & _
            '  ", website " & _
            '  ", company_reg_no " & _
            '  ", registerdno " & _
            '  ", tinno " & _
            '  ", vatno " & _
            '  ", nssfno " & _
            '  ", district " & _
            '  ", ppfno " & _
            '  ", payrollno " & _
            '  ", image " & _
            '  ", fin_start_day " & _
            '  ", fin_start_month " & _
            '  ", fin_start_year " & _
            '  ", fin_end_day " & _
            '  ", fin_end_month " & _
            '  ", fin_end_year " & _
            '  ", bankgroupunkid " & _
            '  ", branchunkid " & _
            '  ", bank_no " & _
            '  ", accountno " & _
            '  ", sendername " & _
            '  ", senderaddress " & _
            '  ", reference " & _
            '  ", mailserverip " & _
            '  ", mailserverport " & _
            '  ", username " & _
            '  ", password " & _
            '  ", isloginssl " & _
            '  ", mail_body " & _
            '  ", isreg1captionused " & _
            '  ", reg1_caption " & _
            '  ", reg1_value " & _
            '  ", isreg2captionused " & _
            '  ", reg2_caption " & _
            '  ", reg2_value " & _
            '  ", isreg3captionused " & _
            '  ", reg3_caption " & _
            '  ", reg3_value " & _
            '  ", isactive" & _
            '") VALUES (" & _
            '  "  @code " & _
            '  ", @name " & _
            '  ", @address1 " & _
            '  ", @address2 " & _
            '  ", @cityunkid " & _
            '  ", @postalunkid " & _
            '  ", @stateunkid " & _
            '  ", @countryunkid " & _
            '  ", @phone1 " & _
            '  ", @phone2 " & _
            '  ", @phone3 " & _
            '  ", @fax " & _
            '  ", @email " & _
            '  ", @website " & _
            '  ", @company_reg_no " & _
            '  ", @registerdno " & _
            '  ", @tinno " & _
            '  ", @vatno " & _
            '  ", @nssfno " & _
            '  ", @district " & _
            '  ", @ppfno " & _
            '  ", @payrollno " & _
            '  ", @image " & _
            '  ", @fin_start_day " & _
            '  ", @fin_start_month " & _
            '  ", @fin_start_year " & _
            '  ", @fin_end_day " & _
            '  ", @fin_end_month " & _
            '  ", @fin_end_year " & _
            '  ", @bankgroupunkid " & _
            '  ", @branchunkid " & _
            '  ", @bank_no " & _
            '  ", @accountno " & _
            '  ", @sendername " & _
            '  ", @senderaddress " & _
            '  ", @reference " & _
            '  ", @mailserverip " & _
            '  ", @mailserverport " & _
            '  ", @username " & _
            '  ", @password " & _
            '  ", @isloginssl " & _
            '  ", @mail_body " & _
            '  ", @isreg1captionused " & _
            '  ", @reg1_caption " & _
            '  ", @reg1_value " & _
            '  ", @isreg2captionused " & _
            '  ", @reg2_caption " & _
            '  ", @reg2_value " & _
            '  ", @isreg3captionused " & _
            '  ", @reg3_caption " & _
            '  ", @reg3_value " & _
            '  ", @isactive" & _
            '"); SELECT @@identity"
            strQ = "INSERT INTO hrmsConfiguration..cfcompany_master ( " & _
              "  code " & _
              ", name " & _
              ", address1 " & _
              ", address2 " & _
              ", cityunkid " & _
              ", postalunkid " & _
              ", stateunkid " & _
              ", countryunkid " & _
              ", phone1 " & _
              ", phone2 " & _
              ", phone3 " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", company_reg_no " & _
              ", registerdno " & _
              ", tinno " & _
              ", vatno " & _
              ", nssfno " & _
              ", district " & _
              ", ppfno " & _
              ", payrollno " & _
              ", image " & _
              ", fin_start_day " & _
              ", fin_start_month " & _
              ", fin_start_year " & _
              ", fin_end_day " & _
              ", fin_end_month " & _
              ", fin_end_year " & _
              ", bankgroupunkid " & _
              ", branchunkid " & _
              ", bank_no " & _
              ", accountno " & _
              ", sendername " & _
              ", senderaddress " & _
              ", reference " & _
              ", mailserverip " & _
              ", mailserverport " & _
              ", username " & _
              ", password " & _
              ", isloginssl " & _
              ", mail_body " & _
              ", isreg1captionused " & _
              ", reg1_caption " & _
              ", reg1_value " & _
              ", isreg2captionused " & _
              ", reg2_caption " & _
              ", reg2_value " & _
              ", isreg3captionused " & _
              ", reg3_caption " & _
              ", reg3_value " & _
              ", isactive" & _
              ", stamp " & _
              ", iscrt_authenticated " & _
              ", email_type " & _
              ", ews_url " & _
              ", ews_domain " & _
              ", isbypassproxy " & _
              ", protocolunkid " & _
            ") VALUES (" & _
              "  @code " & _
              ", @name " & _
              ", @address1 " & _
              ", @address2 " & _
              ", @cityunkid " & _
              ", @postalunkid " & _
              ", @stateunkid " & _
              ", @countryunkid " & _
              ", @phone1 " & _
              ", @phone2 " & _
              ", @phone3 " & _
              ", @fax " & _
              ", @email " & _
              ", @website " & _
              ", @company_reg_no " & _
              ", @registerdno " & _
              ", @tinno " & _
              ", @vatno " & _
              ", @nssfno " & _
              ", @district " & _
              ", @ppfno " & _
              ", @payrollno " & _
              ", @image " & _
              ", @fin_start_day " & _
              ", @fin_start_month " & _
              ", @fin_start_year " & _
              ", @fin_end_day " & _
              ", @fin_end_month " & _
              ", @fin_end_year " & _
              ", @bankgroupunkid " & _
              ", @branchunkid " & _
              ", @bank_no " & _
              ", @accountno " & _
              ", @sendername " & _
              ", @senderaddress " & _
              ", @reference " & _
              ", @mailserverip " & _
              ", @mailserverport " & _
              ", @username " & _
              ", @password " & _
              ", @isloginssl " & _
              ", @mail_body " & _
              ", @isreg1captionused " & _
              ", @reg1_caption " & _
              ", @reg1_value " & _
              ", @isreg2captionused " & _
              ", @reg2_caption " & _
              ", @reg2_value " & _
              ", @isreg3captionused " & _
              ", @reg3_caption " & _
              ", @reg3_value " & _
              ", @isactive" & _
              ", @stamp " & _
              ", @iscrt_authenticated " & _
              ", @email_type " & _
              ", @ews_url " & _
              ", @ews_domain " & _
              ", @isbypassproxy " & _
              ", @protocolunkid " & _
            "); SELECT @@identity"

            'Hemant (01 Feb 2022) -- [protocolunkid]
            'S.SANDEEP [21-SEP-2018] -- START {#0002552} [isbypassproxy] -- END
            'Sohail (30 Nov 2017) - [email_type, ews_url, ews_domain]
            'Sandeep [ 29 NOV 2010 ] -- End 
            'S.SANDEEP [11-AUG-2017] -- START {iscrt_authenticated} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCompanyunkid = dsList.Tables(0).Rows(0).Item(0)

            'CREATING THE FINANCIAL DATABASE FOR THE NEW COMPANY
            Dim dtStDate As Date = Nothing
            Dim dtEndDate As Date = Nothing

            dtStDate = eZeeDate.convertDate(mintFin_Start_Year & mintFin_Start_Month.ToString("0#") & mintFin_Start_Day.ToString("0#"))
            dtEndDate = eZeeDate.convertDate(mintFin_End_Year & mintFin_End_Month.ToString("0#") & mintFin_End_Day.ToString("0#"))


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'strQ = "INSERT INTO " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran  " & _
            '                      "VALUES ('" & mintFin_Start_Year & "-" & mintFin_End_Year & "','" & "tran_" & mstrCode.Replace(" ", "_") & "_" & MonthName(mintFin_Start_Month, True) & mintFin_Start_Year & "_" & MonthName(mintFin_End_Month, True) & mintFin_End_Year & "'," & CInt(mintCompanyunkid) & "," & 0 & "," & "@StDate" & "," & "@EdDate" & ")"

            strQ = "INSERT INTO " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran  " & _
                   " VALUES ('" & mintFin_Start_Year & "-" & mintFin_End_Year & "','" & "tran_" & mstrCode.Replace(" ", "_") & "_" & MonthName(mintFin_Start_Month, True) & mintFin_Start_Year & "_" & MonthName(mintFin_End_Month, True) & mintFin_End_Year & "'," & CInt(mintCompanyunkid) & "," & 0 & "," & "@StDate" & "," & "@EdDate" & "); SELECT @@identity"
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.AddParameter("@StDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtStDate)
            objDataOperation.AddParameter("@EdDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtEndDate)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyunkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cffinancial_year_tran", "yearunkid", dsList.Tables(0).Rows(0)(0), True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END




            'Pinkal (28-oct-2010) -- START

            dsList = GetFinancialYearList(mintCompanyunkid)

            If dsList.Tables(0).Rows.Count > 0 Then

                'Sandeep [ 04 NOV 2010 ] -- Start
                'strQ = "INSERT INTO hrmsConfiguration..cfcompanyaccess_privilege " & _
                '                       "(userunkid " & _
                '                       ",yearunkid) " & _
                '                       "values " & _
                '                       "(@userunkid " & _
                '                       ",@yearunkid)"
                'Sandeep [ 29 NOV 2010 ] -- Start
                'strQ = "INSERT INTO hrmsConfiguration..cfcompanyaccess_privilege (userunkid, yearunkid ) " & _
                '       "SELECT @userunkid,@yearunkid FROM hrmsConfiguration..cfcompanyaccess_privilege WHERE NOT EXISTS (SELECT * FROM hrmsConfiguration..cfcompanyaccess_privilege WHERE yearunkid = @yearunkid) "

                strQ = "INSERT INTO hrmsConfiguration..cfcompanyaccess_privilege (userunkid, yearunkid ) " & _
                       "SELECT @userunkid,@yearunkid WHERE NOT EXISTS (SELECT * FROM hrmsConfiguration..cfcompanyaccess_privilege WHERE yearunkid = @yearunkid) "
                'Sandeep [ 29 NOV 2010 ] -- End 

                'Sandeep [ 04 NOV 2010 ] -- End 

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 1)
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables(0).Rows(0)("yearunkid"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'Pinkal (28-oct-2010) -- END


            'Sandeep [ 01 MARCH 2011 ] -- Start

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If dtTable IsNot Nothing Then
            '    objCBankTran._CompanyId = mintCompanyunkid
            '    objCBankTran._SetDataTable = dtTable
            '    If objCBankTran.InsertUpdateDelete_BankTranList() = False Then
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            If dtTable IsNot Nothing Then
                If dtTable.Rows.Count > 0 Then
                objCBankTran._CompanyId = mintCompanyunkid
                objCBankTran._SetDataTable = dtTable
                    With objCBankTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId                       
                        ._AuditDate = mdtAuditDate
                    End With
                If objCBankTran.InsertUpdateDelete_BankTranList() = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyunkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", mintCompanyunkid, "cfcompanybank_tran", "companybanktranunkid", -1, 1, 0, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

           End If
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            'Sandeep [ 01 MARCH 2011 ] -- End 


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            If mintLocalizationCountryUnkid > 0 Then

                'S.SANDEEP [ 26 SEPT 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If objConfig Is Nothing Then objConfig = New clsConfigOptions
                'S.SANDEEP [ 26 SEPT 2013 ] -- END


                If objConfig._Companyunkid <= 0 Then
                    objConfig._Companyunkid = mintCompanyunkid
                    objConfig._CountryUnkid = mintLocalizationCountryUnkid
                    objConfig._Currency = mstrLocalizationCurrencySign
                    objConfig.updateParam()
                End If

                Dim StrDBName As String = "tran_" & mstrCode.Replace(" ", "_") & "_" & MonthName(mintFin_Start_Month, True) & mintFin_Start_Year & "_" & MonthName(mintFin_End_Month, True) & mintFin_End_Year
                strQ = "INSERT INTO " & StrDBName & "..cfexchange_rate(countryunkid,currency_name,currency_sign,exchange_rate,isbasecurrency,isactive,exchange_rate1,exchange_rate2,isprefix,digits_after_decimal,exchange_date) " & _
                       " VALUES (" & mintLocalizationCountryUnkid & ",'" & mstrLocalizationCurrencySign & "','" & mstrLocalizationCurrencySign & "','1.0000000000',1,1,'1.0000000000','1.0000000000',0,2,@StDate); SELECT @@identity "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@StDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtStDate)

                'objDataOperation.ExecNonQuery(strQ)
                dsList = objDataOperation.ExecQuery(strQ, "List")


                If objDataOperation.ErrorMessage = "" Then

                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If objConfig Is Nothing Then objConfig = New clsConfigOptions
                    'S.SANDEEP [ 26 SEPT 2013 ] -- END

                    objConfig._Companyunkid = mintCompanyunkid

                    objConfig._Base_CurrencyId = CInt(dsList.Tables(0).Rows(0).Item(0))
                    ' ConfigParameter._Object._CurrencyFormat = "#,###,###,###,###,###,###,###,###,##0." & New String(CChar("0"), objExRate._Digits_After_Decimal)
                    ' ConfigParameter._Object._CountryUnkid = objExRate._Countryunkid
                    ' ConfigParameter._Object._Currency = objExRate._Currency_Sign

                    objConfig._CurrencyFormat = "#,###,###,##0.00"
                    objConfig.updateParam()
                End If

            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 



            objDataOperation.ReleaseTransaction(True)


            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 07 FEB 2011 ] -- START
            Dim objDo As New clsDataOperation
            objDo.ExecNonQuery("USE master")
            objDo.ExecNonQuery("IF EXISTS (SELECT * FROM sys.databases WHERE name = '" & mstrNewDatabaseName & "' ) DROP DATABASE " & mstrNewDatabaseName)
            objDo = Nothing
            'Sandeep [ 07 FEB 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function makeCompDatabase() As Boolean
        Dim exForce As Exception
        Dim strDatabaseName As String
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim inttest As Integer = -1
        Try
            Dim objComp As New clsCompany_Master
            dsList = GetFinancialYearList(mintCompanyunkid)

            strDatabaseName = "tran_" & mstrCode.Replace(" ", "_") & "_" & MonthName(mintFin_Start_Month, True) & mintFin_Start_Year & "_" & MonthName(mintFin_End_Month, True) & mintFin_End_Year
            ' strDatabaseName = "tran_" & mstrCode.Replace(" ", "_")

            mstrNewDatabaseName = strDatabaseName

            'S.SANDEEP [ 10 May 2013 ] -- START
            'ENHANCEMENT : Database Setup Changes (SMO/DMO)
            'Dim objDO As New clsDataOperation
            'Dim objUpdateData As New UpdateData.clsUpdateData
            'objUpdateData.UpdateData(objDO, mstrNewDatabaseName)

            '' objDataOperation.ExecNonQuery("IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = '[" & strDatabaseName & "]') CREATE DATABASE " & strDatabaseName)

            ''If objDataOperation.ErrorMessage <> "" Then
            ''    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            ''    Throw exForce
            ''End If

            ''If objDataOperation.ErrorMessage = "" Then

            ''    'Dim objAppSetting As New clsApplicationSettings
            ''    'Shell(objAppSetting._ApplicationPath & "TRAN_DMO.exe -c -d " & strDatabaseName, AppWinStyle.Hide, True)
            ''End If

            ''S.SANDEEP [ 01 SEP 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "SQL_SMO.exe") = True Then
            '    Shell(AppSettings._Object._ApplicationPath & "SQL_SMO.exe " & mstrNewDatabaseName & " " & "/A", AppWinStyle.Hide, True)
            'End If
            ''S.SANDEEP [ 01 SEP 2012 ] -- END

            objDataOperation.ExecNonQuery("USE [master]")
            objDataOperation.ExecNonQuery("IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = '[" & strDatabaseName & "]') CREATE DATABASE " & strDatabaseName)

            If objDataOperation.ErrorMessage <> "" Then
                eZeeMsgBox.Show("Sorry, Error in creating database. Please contact Aruti Support Team.", enMsgBoxStyle.Information)
                Return False
            End If

            System.Threading.Thread.Sleep(1000)

            If Dir(AppSettings._Object._ApplicationPath & "Aruti_SQL.exe") <> "" Then
                Call Shell(AppSettings._Object._ApplicationPath & "Aruti_SQL.exe " & "/T" & " " & strDatabaseName, AppWinStyle.NormalFocus, True)
                Call Shell(AppSettings._Object._ApplicationPath & "Aruti_SQL.exe " & "/S" & " " & strDatabaseName, AppWinStyle.NormalFocus, True)
            End If
            'S.SANDEEP [ 10 May 2013 ] -- END

            eZeeDatabase.change_database("hrmsConfiguration")
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "makeCompDatabase", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function


    'Sandeep [ 01 MARCH 2011 ] -- Start

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfcompany_master) </purpose>
    ''' Public Function Update() As Boolean
    Public Function Update(Optional ByVal dtTable As DataTable = Nothing) As Boolean
        'Sandeep [ 01 MARCH 2011 ] -- End 
        If isExist(mstrCode, , mintCompanyunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName, mintCompanyunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Company is already defined. Please define new Company.")
            Return False
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Sandeep [ 01 MARCH 2011 ] -- Start
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation

        objDataOperation.BindTransaction()
        'Sandeep [ 01 MARCH 2011 ] -- End 

        Try
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@postalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostalunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@phone1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone1.ToString)
            objDataOperation.AddParameter("@phone2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone2.ToString)
            objDataOperation.AddParameter("@phone3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone3.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebsite.ToString)
            objDataOperation.AddParameter("@company_reg_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_Reg_No.ToString)
            objDataOperation.AddParameter("@registerdno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegisterdno.ToString)
            objDataOperation.AddParameter("@tinno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTinno.ToString)
            objDataOperation.AddParameter("@vatno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVatno.ToString)
            objDataOperation.AddParameter("@nssfno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrNssfno.ToString)
            objDataOperation.AddParameter("@district", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDistrict.ToString)
            objDataOperation.AddParameter("@ppfno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPpfno.ToString)
            objDataOperation.AddParameter("@payrollno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPayrollno.ToString)

            If mimgImage Is Nothing Then
                objDataOperation.AddParameter("@image", SqlDbType.Binary, 0, DBNull.Value)
            Else
                Dim imgLogo() As Byte = eZeeDataType.image2Data(mimgImage)
                objDataOperation.AddParameter("@image", SqlDbType.Binary, imgLogo.Length, imgLogo)
            End If

            objDataOperation.AddParameter("@fin_start_day", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_Start_Day.ToString)
            objDataOperation.AddParameter("@fin_start_month", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_Start_Month.ToString)
            objDataOperation.AddParameter("@fin_start_year", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_Start_Year.ToString)
            objDataOperation.AddParameter("@fin_end_day", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_End_Day.ToString)
            objDataOperation.AddParameter("@fin_end_month", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_End_Month.ToString)
            objDataOperation.AddParameter("@fin_end_year", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFin_End_Year.ToString)
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankgroupunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@bank_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBank_No.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)
            objDataOperation.AddParameter("@sendername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSendername.ToString)
            objDataOperation.AddParameter("@senderaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSenderaddress.ToString)
            objDataOperation.AddParameter("@reference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference.ToString)
            objDataOperation.AddParameter("@mailserverip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMailserverip.ToString)
            objDataOperation.AddParameter("@mailserverport", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMailserverport.ToString)
            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPassword.ToString)
            objDataOperation.AddParameter("@isloginssl", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloginssl.ToString)
            objDataOperation.AddParameter("@isreg1captionused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreg1captionused.ToString)
            objDataOperation.AddParameter("@reg1_caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg1_Caption.ToString)
            objDataOperation.AddParameter("@reg1_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg1_Value.ToString)
            objDataOperation.AddParameter("@isreg2captionused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreg2captionused.ToString)
            objDataOperation.AddParameter("@reg2_caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg2_Caption.ToString)
            objDataOperation.AddParameter("@reg2_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg2_Value.ToString)
            objDataOperation.AddParameter("@isreg3captionused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreg3captionused.ToString)
            objDataOperation.AddParameter("@reg3_caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg3_Caption.ToString)
            objDataOperation.AddParameter("@reg3_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReg3_Value.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@mail_body", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMail_Body.ToString)
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            objDataOperation.AddParameter("@email_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmail_Type)
            objDataOperation.AddParameter("@ews_url", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEWS_URL.ToString)
            objDataOperation.AddParameter("@ews_domain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEWS_Domain.ToString)
            'Sohail (30 Nov 2017) -- End

            'Sohail (27 Mar 2014) -- Start
            'ENHANCEMENT - Show Company Stamp on Payslip.
            If mimgStamp Is Nothing Then
                objDataOperation.AddParameter("@stamp", SqlDbType.Image, 0, DBNull.Value)
            Else
                Dim imgStamp() As Byte = eZeeDataType.image2Data(mimgStamp)
                objDataOperation.AddParameter("@stamp", SqlDbType.Image, imgStamp.Length, imgStamp)
            End If
            'Sohail (27 Mar 2014) -- End

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
            objDataOperation.AddParameter("@iscrt_authenticated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCertificateAuthorization)
            'S.SANDEEP [11-AUG-2017] -- END

            'S.SANDEEP [21-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002552}
            objDataOperation.AddParameter("@isbypassproxy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbypassproxy.ToString)
            'S.SANDEEP [21-SEP-2018] -- END

            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            objDataOperation.AddParameter("@protocolunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProtocolunkid.ToString)
            'Hemant (01 Feb 2022) -- End


            'Sandeep [ 29 NOV 2010 ] -- Start

            'strQ = "UPDATE cfcompany_master SET " & _
            '  "  code = @code" & _
            '  ", name = @name" & _
            '  ", address1 = @address1" & _
            '  ", address2 = @address2" & _
            '  ", cityunkid = @cityunkid" & _
            '  ", postalunkid = @postalunkid" & _
            '  ", stateunkid = @stateunkid" & _
            '  ", countryunkid = @countryunkid" & _
            '  ", phone1 = @phone1" & _
            '  ", phone2 = @phone2" & _
            '  ", phone3 = @phone3" & _
            '  ", fax = @fax" & _
            '  ", email = @email" & _
            '  ", website = @website" & _
            '  ", company_reg_no = @company_reg_no" & _
            '  ", registerdno = @registerdno" & _
            '  ", tinno = @tinno" & _
            '  ", vatno = @vatno" & _
            '  ", nssfno = @nssfno" & _
            '  ", district = @district" & _
            '  ", ppfno = @ppfno" & _
            '  ", payrollno = @payrollno" & _
            '  ", image = @image" & _
            '  ", fin_start_day = @fin_start_day" & _
            '  ", fin_start_month = @fin_start_month" & _
            '  ", fin_start_year = @fin_start_year" & _
            '  ", fin_end_day = @fin_end_day" & _
            '  ", fin_end_month = @fin_end_month" & _
            '  ", fin_end_year = @fin_end_year" & _
            '  ", bankgroupunkid = @bankgroupunkid" & _
            '  ", branchunkid = @branchunkid" & _
            '  ", bank_no = @bank_no" & _
            '  ", accountno = @accountno" & _
            '  ", sendername = @sendername" & _
            '  ", senderaddress = @senderaddress" & _
            '  ", reference = @reference" & _
            '  ", mailserverip = @mailserverip" & _
            '  ", mailserverport = @mailserverport" & _
            '  ", username = @username" & _
            '  ", password = @password" & _
            '  ", isloginssl = @isloginssl" & _
            '  ", mail_body = @mail_body " & _
            '  ", isreg1captionused = @isreg1captionused" & _
            '  ", reg1_caption = @reg1_caption" & _
            '  ", reg1_value = @reg1_value" & _
            '  ", isreg2captionused = @isreg2captionused" & _
            '  ", reg2_caption = @reg2_caption" & _
            '  ", reg2_value = @reg2_value" & _
            '  ", isreg3captionused = @isreg3captionused" & _
            '  ", reg3_caption = @reg3_caption" & _
            '  ", reg3_value = @reg3_value" & _
            '  ", isactive = @isactive " & _
            '"WHERE companyunkid = @companyunkid "

            strQ = "UPDATE hrmsConfiguration..cfcompany_master SET " & _
              "  code = @code" & _
              ", name = @name" & _
              ", address1 = @address1" & _
              ", address2 = @address2" & _
              ", cityunkid = @cityunkid" & _
              ", postalunkid = @postalunkid" & _
              ", stateunkid = @stateunkid" & _
              ", countryunkid = @countryunkid" & _
              ", phone1 = @phone1" & _
              ", phone2 = @phone2" & _
              ", phone3 = @phone3" & _
              ", fax = @fax" & _
              ", email = @email" & _
              ", website = @website" & _
              ", company_reg_no = @company_reg_no" & _
              ", registerdno = @registerdno" & _
              ", tinno = @tinno" & _
              ", vatno = @vatno" & _
              ", nssfno = @nssfno" & _
              ", district = @district" & _
              ", ppfno = @ppfno" & _
              ", payrollno = @payrollno" & _
              ", image = @image" & _
              ", fin_start_day = @fin_start_day" & _
              ", fin_start_month = @fin_start_month" & _
              ", fin_start_year = @fin_start_year" & _
              ", fin_end_day = @fin_end_day" & _
              ", fin_end_month = @fin_end_month" & _
              ", fin_end_year = @fin_end_year" & _
              ", bankgroupunkid = @bankgroupunkid" & _
              ", branchunkid = @branchunkid" & _
              ", bank_no = @bank_no" & _
              ", accountno = @accountno" & _
              ", sendername = @sendername" & _
              ", senderaddress = @senderaddress" & _
              ", reference = @reference" & _
              ", mailserverip = @mailserverip" & _
              ", mailserverport = @mailserverport" & _
              ", username = @username" & _
              ", password = @password" & _
              ", isloginssl = @isloginssl" & _
              ", mail_body = @mail_body " & _
              ", isreg1captionused = @isreg1captionused" & _
              ", reg1_caption = @reg1_caption" & _
              ", reg1_value = @reg1_value" & _
              ", isreg2captionused = @isreg2captionused" & _
              ", reg2_caption = @reg2_caption" & _
              ", reg2_value = @reg2_value" & _
              ", isreg3captionused = @isreg3captionused" & _
              ", reg3_caption = @reg3_caption" & _
              ", reg3_value = @reg3_value" & _
              ", isactive = @isactive " & _
              ", stamp = @stamp " & _
              ", iscrt_authenticated = @iscrt_authenticated " & _
              ", email_type = @email_type " & _
              ", ews_url = @ews_url " & _
              ", ews_domain = @ews_domain " & _
              ", isbypassproxy = @isbypassproxy " & _
              ", protocolunkid = @protocolunkid " & _
            "WHERE companyunkid = @companyunkid "

            'Hemant (01 Feb 2022) -- [protocolunkid]
            'S.SANDEEP [21-SEP-2018] -- START {#0002552} [isbypassproxy] -- END
            'Sohail (30 Nov 2017) - [email_type, ews_url, ews_domain]
            'Sandeep [ 29 NOV 2010 ] -- End 
            'S.SANDEEP [11-AUG-2017] -- START {iscrt_authenticated} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sandeep [ 01 MARCH 2011 ] -- Start

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If dtTable IsNot Nothing Then
            '    objCBankTran._CompanyId = mintCompanyunkid
            '    objCBankTran._SetDataTable = dtTable
            '    If objCBankTran.InsertUpdateDelete_BankTranList() = False Then
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If

            If dtTable IsNot Nothing Then
                Dim dt() As DataRow = dtTable.Select("AUD=''")
                If dt.Length = dtTable.Rows.Count Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyunkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", mintCompanyunkid, "cfcompanybank_tran", "companybanktranunkid", -1, 2, 0, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Else
                objCBankTran._CompanyId = mintCompanyunkid
                objCBankTran._SetDataTable = dtTable
                    With objCBankTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId                        
                        ._AuditDate = mdtAuditDate
                    End With
                If objCBankTran.InsertUpdateDelete_BankTranList() = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
                With objCBankTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId                    
                    ._AuditDate = mdtAuditDate
                End With
                If objCBankTran.UpdatePastPaymentAccountNo(mintCompanyunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Sohail (21 Jul 2012) -- End
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            objDataOperation.ReleaseTransaction(True)
            'Sandeep [ 01 MARCH 2011 ] -- End 

            Return True
        Catch ex As Exception
            'Sandeep [ 01 MARCH 2011 ] -- Start
            objDataOperation.ReleaseTransaction(False)
            'Sandeep [ 01 MARCH 2011 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfcompany_master) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Delete(ByVal intUnkid As Integer) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, ByVal intUserunkid As Integer) As Boolean
        'Shani(24-Aug-2015) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsInUse As Boolean = False
        Dim dsIsUsed As New DataSet
        objDataOperation = New clsDataOperation
        Try
            mstrMessage = ""
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            strQ = "SELECT database_name FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
            "WHERE companyunkid = @companyunkid "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows

                    strQ = "SELECT DB_NAME(dbid)FROM sys.sysprocesses WHERE dbid > 0 AND DB_NAME(dbid) = '" & dtRow.Item("database_name") & "'"

                    dsIsUsed = objDataOperation.ExecQuery(strQ, "Used")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsIsUsed.Tables(0).Rows.Count > 0 Then
                        blnIsInUse = True
                    End If
                Next
            End If

            If blnIsInUse = False Then


                'Sandeep [ 01 MARCH 2011 ] -- Start
                'Sandeep [ 29 NOV 2010 ] -- Start
                'strQ = "UPDATE cfcompany_master SET " & _
                '       " isactive = 0 " & _
                '       " WHERE companyunkid = @companyunkid "

                strQ = "UPDATE hrmsConfiguration..cfcompany_master SET " & _
                       " isactive = 0 " & _
                       " WHERE companyunkid = @companyunkid ; "
                'Sandeep [ 29 NOV 2010 ] -- End 


                Dim objCommonATLog As New clsCommonATLog
                Dim dsTran As DataSet = objCommonATLog.GetChildList(objDataOperation, "cfcompanybank_tran", "companyunkid", intUnkid)
                objCommonATLog = Nothing
                If dsTran.Tables(0).Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsTran.Tables(0).Rows
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyunkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", intUnkid, "cfcompanybank_tran", "companybanktranunkid", dtRow.Item("companybanktranunkid"), 3, 3, True) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END

                    Next
                Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyunkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", intUnkid, "cfcompanybank_tran", "companybanktranunkid", -1, 3, 0, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                strQ &= "UPDATE cfcompanybank_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                       " WHERE companyunkid = @companyunkid "

                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                'Shani(24-Aug-2015) -- End

                Dim objConfig As New clsConfigOptions
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, objConfig._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmCompany_AddEdit", 10, "You have added incorrect Bank information. Please insert correct Bank Information."))
                objConfig = Nothing
                'Sandeep [ 01 MARCH 2011 ] -- End 


                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Else
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this company. Reason : This company is already in use.")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 29 NOV 2010 ] -- Start

            'strQ = "SELECT " & _
            '              "  companyunkid " & _
            '              ", code " & _
            '              ", name " & _
            '              ", address1 " & _
            '              ", address2 " & _
            '              ", cityunkid " & _
            '              ", postalunkid " & _
            '              ", stateunkid " & _
            '              ", countryunkid " & _
            '              ", phone1 " & _
            '              ", phone2 " & _
            '              ", phone3 " & _
            '              ", fax " & _
            '              ", email " & _
            '              ", website " & _
            '              ", company_reg_no " & _
            '              ", registerdno " & _
            '              ", tinno " & _
            '              ", vatno " & _
            '              ", nssfno " & _
            '              ", district " & _
            '              ", ppfno " & _
            '              ", payrollno " & _
            '              ", image " & _
            '              ", fin_start_day " & _
            '              ", fin_start_month " & _
            '              ", fin_start_year " & _
            '              ", fin_end_day " & _
            '              ", fin_end_month " & _
            '              ", fin_end_year " & _
            '              ", bankgroupunkid " & _
            '              ", branchunkid " & _
            '              ", bank_no " & _
            '              ", accountno " & _
            '              ", sendername " & _
            '              ", senderaddress " & _
            '              ", reference " & _
            '              ", mailserverip " & _
            '              ", mailserverport " & _
            '              ", username " & _
            '              ", password " & _
            '              ", isloginssl " & _
            '              ", isreg1captionused " & _
            '              ", reg1_caption " & _
            '              ", reg1_value " & _
            '              ", isreg2captionused " & _
            '              ", reg2_caption " & _
            '              ", reg2_value " & _
            '              ", isreg3captionused " & _
            '              ", reg3_caption " & _
            '              ", reg3_value " & _
            '              ", isactive " & _
            '             "FROM cfcompany_master " & _
            '             "WHERE 1=1 "

            strQ = "SELECT " & _
                          "  companyunkid " & _
                          ", code " & _
                          ", name " & _
                          ", address1 " & _
                          ", address2 " & _
                          ", cityunkid " & _
                          ", postalunkid " & _
                          ", stateunkid " & _
                          ", countryunkid " & _
                          ", phone1 " & _
                          ", phone2 " & _
                          ", phone3 " & _
                          ", fax " & _
                          ", email " & _
                          ", website " & _
                          ", company_reg_no " & _
                          ", registerdno " & _
                          ", tinno " & _
                          ", vatno " & _
                          ", nssfno " & _
                          ", district " & _
                          ", ppfno " & _
                          ", payrollno " & _
                          ", image " & _
                          ", fin_start_day " & _
                          ", fin_start_month " & _
                          ", fin_start_year " & _
                          ", fin_end_day " & _
                          ", fin_end_month " & _
                          ", fin_end_year " & _
                          ", bankgroupunkid " & _
                          ", branchunkid " & _
                          ", bank_no " & _
                          ", accountno " & _
                          ", sendername " & _
                          ", senderaddress " & _
                          ", reference " & _
                          ", mailserverip " & _
                          ", mailserverport " & _
                          ", username " & _
                          ", password " & _
                          ", isloginssl " & _
                          ", isreg1captionused " & _
                          ", reg1_caption " & _
                          ", reg1_value " & _
                          ", isreg2captionused " & _
                          ", reg2_caption " & _
                          ", reg2_value " & _
                          ", isreg3captionused " & _
                          ", reg3_caption " & _
                          ", reg3_value " & _
                          ", isactive " & _
                          ", stamp " & _
                          ", iscrt_authenticated " & _
                          ", email_type " & _
                          ", ews_url " & _
                          ", ews_domain " & _
                          ", isbypassproxy " & _
                          ", ISNULL(protocolunkid,0) AS protocolunkid " & _
                         "FROM hrmsConfiguration..cfcompany_master " & _
                         "WHERE 1=1 "

            'Hemant (01 Feb 2022) -- [protocolunkid]
            'S.SANDEEP [21-SEP-2018] -- START {#0002552} [isbypassproxy] -- END
            'Sohail (30 Nov 2017) - [email_type, ews_url, ews_domain]
            'Sandeep [ 29 NOV 2010 ] -- End 
            'S.SANDEEP [11-AUG-2017] -- START {iscrt_authenticated} -- END

            

            If strName.Length > 0 Then
                strQ &= "AND name = @name "
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
            End If

            If intUnkid > 0 Then
                strQ &= " AND companyunkid <> @companyunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (26 Jul 2010) -- Start
    ''' <summary>
    ''' Get Financial Start Date and End Date
    ''' Modify By : Sohail
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetFinancialDate(ByVal intCompanyUnkID As Integer)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 29 NOV 2010 ] -- Start
            'strQ = "select fin_start_day, fin_start_month, fin_start_year, fin_end_day, fin_end_month, fin_end_year from cfcompany_master " & _
            '                    "WHERE isactive = 1 " & _
            '                    "AND companyunkid = @companyunkid "

            strQ = "select fin_start_day, fin_start_month, fin_start_year, fin_end_day, fin_end_month, fin_end_year from hrmsConfiguration..cfcompany_master " & _
                    "WHERE isactive = 1 " & _
                    "AND companyunkid = @companyunkid "
            'Sandeep [ 29 NOV 2010 ] -- End 

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                With dsList.Tables("List").Rows(0)
                    mdtFinanceStartDate = DateSerial(CInt(.Item("fin_start_year").ToString), CInt(.Item("fin_start_month").ToString), CInt(.Item("fin_start_day").ToString))
                    mdtFinanceEndDate = DateSerial(CInt(.Item("fin_end_year").ToString), CInt(.Item("fin_end_month").ToString), CInt(.Item("fin_end_day").ToString))
                End With
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFinancialDate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub
    'Sohail (26 Jul 2010) -- End

#Region " Private Functions "
    Public Function GetFinancialYearList(ByVal intCompanyId As Integer, Optional ByVal intUserId As Integer = -1, Optional ByVal strTableName As String = "List" _
                                         , Optional ByVal intYearUnkId As Integer = 0) As DataSet 'Sohail (16 Jul 2012) - [intYearUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (28-oct-2010) -- START
        ' objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation
        'Pinkal (28-oct-2010) -- END

        Try

            'Sandeep [ 29 NOV 2010 ] -- Start
            'If intUserId > -1 Then
            '    strQ = "SELECT " & _
            '                     " " & FinancialYear._Object._ConfigDatabaseName & ".. cffinancial_year_tran.yearunkid " & _
            '                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name " & _
            '                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name " & _
            '                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
            '                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.isclosed " & _
            '                     "," & FinancialYear._Object._ConfigDatabaseName & "..cfcompany_master.name As CompanyName " & _
            '               "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
            '               "JOIN cfcompany_master ON cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
            '                "	JOIN cfcompanyaccess_privilege ON " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = cfcompanyaccess_privilege.yearunkid " & _
            '                "		AND cfcompanyaccess_privilege.userunkid = @UserId " & _
            '                "WHERE cfcompany_master.companyunkid = @CompanyId " & _
            '                "   AND ISNULL(cfcompany_master.isactive,0) = 1 "
            '    objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            'Else
            '    strQ = "SELECT " & _
            '             " " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
            '             "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name " & _
            '             "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name " & _
            '             "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
            '             "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.isclosed " & _
            '             ",cfcompany_master.name As CompanyName " & _
            '           "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
            '           "JOIN cfcompany_master ON cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
            '                    "WHERE cfcompany_master.companyunkid = @CompanyId " & _
            '                    "AND ISNULL(cfcompany_master.isactive,0) = 1 "
            'End If

            If intUserId > -1 Then
                strQ = "SELECT " & _
                                 " " & FinancialYear._Object._ConfigDatabaseName & ".. cffinancial_year_tran.yearunkid " & _
                                 "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name " & _
                                 "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name " & _
                                 "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                                 "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.isclosed " & _
                                 "," & FinancialYear._Object._ConfigDatabaseName & "..cfcompany_master.name As CompanyName " & _
                                 ",CONVERT(CHAR(8), " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.start_date, 112) AS start_date " & _
                                 ",CONVERT(CHAR(8), " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.end_date, 112) AS end_date " & _
                           "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
                           " JOIN hrmsConfiguration..cfcompany_master ON hrmsConfiguration..cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                           " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = hrmsConfiguration..cfcompanyaccess_privilege.yearunkid " & _
                           " JOIN sys.databases ON sys.databases.name=hrmsConfiguration ..cffinancial_year_tran.database_name " & _
                           "		AND hrmsConfiguration..cfcompanyaccess_privilege.userunkid = @UserId " & _
                            "WHERE hrmsConfiguration..cfcompany_master.companyunkid = @CompanyId " & _
                            "   AND ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 "
               
                'Sohail (16 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                If intYearUnkId > 0 Then
                    strQ &= "AND	 cffinancial_year_tran.yearunkid = @yearunkid "
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkId)
                End If
                'Sohail (16 Jul 2012) -- End

                'Sandeep ( 18 JAN 2011 ) -- START
                strQ &= " ORDER BY " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid  Desc "
                'Sandeep ( 18 JAN 2011 ) -- END 
                objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            Else
                strQ = "SELECT " & _
                         " " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
                         "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name " & _
                         "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name " & _
                         "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                         "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.isclosed " & _
                         ",hrmsConfiguration..cfcompany_master.name As CompanyName " & _
                         ",CONVERT(CHAR(8), " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.start_date, 112) AS start_date " & _
                         ",CONVERT(CHAR(8), " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.end_date, 112) AS end_date " & _
                       "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran "
                'Sandeep [ 10 FEB 2011 ] -- Start
                strQ &= " JOIN sys.databases ON sys.databases.name=hrmsConfiguration ..cffinancial_year_tran.database_name "
                'Sandeep [ 10 FEB 2011 ] -- End 
                strQ &= "JOIN hrmsConfiguration..cfcompany_master ON hrmsConfiguration..cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                                "WHERE hrmsConfiguration..cfcompany_master.companyunkid = @CompanyId " & _
                                "AND ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 "
                
                'Sohail (16 Jul 2012) -- Start
                'TRA - ENHANCEMENT
                If intYearUnkId > 0 Then
                    strQ &= "AND	 cffinancial_year_tran.yearunkid = @yearunkid "
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkId)
                End If
                'Sohail (16 Jul 2012) -- End
            End If
            'Sandeep [ 29 NOV 2010 ] -- End 

            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFinancialYearList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Sub GetFinalcialYear_Data()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            'Sohail (24 Aug 2010) -- Start
            'Changes : Add StartDate and EndDate
            'strQ = "SELECT " & _
            '         " yearunkid " & _
            '         ",financialyear_name " & _
            '         ",database_name " & _
            '         ",companyunkid " & _
            '         ",isclosed " & _
            '       "FROM cffinancial_year_tran " & _
            '       "WHERE yearunkid = @yearunkid "
            strQ = "SELECT " & _
                     " " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid " & _
                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name " & _
                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.database_name " & _
                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.isclosed " & _
                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.start_date " & _
                     "," & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.end_date " & _
                   "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
                   "WHERE yearunkid = @yearunkid "
            'Sohail (24 Aug 2010) -- End

            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                mintYearUnkid = CInt(dtRow.Item("yearunkid"))
                mstrFinancialYear_Name = CStr(dtRow.Item("financialyear_name"))
                mstrDatabaseName = CStr(dtRow.Item("database_name"))
                mint_Fin_Company_Id = CInt(dtRow.Item("companyunkid"))
                mblnIsFin_Close = CBool(dtRow.Item("isclosed"))
                'Sohail (24 Aug 2010) -- Start
                If IsDBNull(dtRow.Item("start_date")) Then
                    mdtDatabase_Start_Date = Nothing
                Else
                    mdtDatabase_Start_Date = CDate(dtRow.Item("start_date").ToString)
                End If

                If IsDBNull(dtRow.Item("end_date")) Then
                    mdtDatabase_End_Date = Nothing
                Else
                    mdtDatabase_End_Date = CDate(dtRow.Item("end_date").ToString)
                End If
                'Sohail (24 Aug 2010) -- End
                Exit For
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFinalcialYear_Data", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Public Function GetCompanyAcessList(ByVal intUserId As Integer, Optional ByVal strListName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "     DISTINCT ISNULL(hrmsConfiguration..cfcompany_master.name,'') As name " & _
                   "    ,hrmsConfiguration..cfcompany_master.companyunkid As companyunkid " & _
                   "    ,hrmsConfiguration..cfcompany_master.code " & _
                   "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
                   "   JOIN hrmsConfiguration..cfcompanyaccess_privilege ON " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = hrmsConfiguration..cfcompanyaccess_privilege.yearunkid " & _
                   "   AND hrmsConfiguration..cfcompanyaccess_privilege.userunkid = @UserId " & _
                   ",hrmsConfiguration..cfcompany_master " & _
                   "WHERE hrmsConfiguration..cfcompany_master.companyunkid = " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.companyunkid " & _
                   "   AND ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 " & _
                   "ORDER BY hrmsConfiguration..cfcompany_master.companyunkid "

            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCompanyAcessList", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sandeep [ 21 Aug 2010 ] -- End 


    'Sandeep [ 10 FEB 2011 ] -- Start

    ''Sandeep [ 23 Oct 2010 ] -- Start
    'Public Function GetFullList(Optional ByVal strListName As String = "List") As DataSet
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Try

    '        objDataOperation = New clsDataOperation

    '        'Sandeep [ 04 NOV 2010 ] -- Start
    '        'StrQ = "SELECT " & _
    '        '            "	 database_name AS database_name " & _
    '        '            "   ,ISNULL(cfcompany_master.name,'') AS CompanyName " & _
    '        '            "   ,yearunkid As yearunkid " & _
    '        '            "   ,cfcompany_master.companyunkid As companyunkid " & _
    '        '            "FROM cffinancial_year_tran " & _
    '        '            "   JOIN cfcompany_master ON cffinancial_year_tran.companyunkid = cfcompany_master.companyunkid " & _
    '        '            "WHERE ISNULL(cfcompany_master.isactive,0) = 1 "


    '        'Sandeep [ 29 NOV 2010 ] -- Start
    '        'StrQ = "SELECT " & _
    '        '       "	 database_name AS database_name " & _
    '        '       "   ,ISNULL(cfcompany_master.name,'') AS CompanyName " & _
    '        '       "   ,yearunkid As yearunkid " & _
    '        '       "   ,cfcompany_master.companyunkid As companyunkid " & _
    '        '       "   ,financialyear_name As financialyear_name " & _
    '        '       "FROM cffinancial_year_tran " & _
    '        '       "   JOIN cfcompany_master ON cffinancial_year_tran.companyunkid = cfcompany_master.companyunkid " & _
    '        '       "WHERE ISNULL(cfcompany_master.isactive,0) = 1 "

            'StrQ = "SELECT " & _
            '       "	 database_name AS database_name " & _
    '               "   ,ISNULL(hrmsConfiguration..cfcompany_master.name,'') AS CompanyName " & _
            '       "   ,yearunkid As yearunkid " & _
    '               "   ,hrmsConfiguration..cfcompany_master.companyunkid As companyunkid " & _
            '       "   ,financialyear_name As financialyear_name " & _
    '               "FROM hrmsConfiguration..cffinancial_year_tran " & _
    '               "   JOIN hrmsConfiguration..cfcompany_master ON hrmsConfiguration..cffinancial_year_tran.companyunkid = hrmsConfiguration..cfcompany_master.companyunkid " & _
    '               "WHERE ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 "
    '        'Sandeep [ 29 NOV 2010 ] -- End 

    '        'Sandeep [ 04 NOV 2010 ] -- End 

    '        dsList = objDataOperation.ExecQuery(StrQ, strListName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetFullList", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
    ''Sandeep [ 23 Oct 2010 ] -- End 

  
    Public Function Get_DataBaseList(Optional ByVal StrList As String = "List" _
                                     , Optional ByVal blnAddArutiHRMS As Boolean = False, Optional ByVal blnIsOpenDB As Boolean = False _
                                     , Optional ByVal blnAddArutiImages As Boolean = True) As DataTable 'Sohail (04 Jul 2012) - [blnAddArutiHRMS]
        '                            'Sohail (08 Apr 2013) - [blnAddArutiImages]
        Dim dsGrpList As New DataSet
        Dim dsTranList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation



            'Pinkal (04-Apr-2013) -- Start
            'Enhancement : TRA Changes

            Dim mblnIsInDatabase As Boolean = False
            StrQ = " SELECT database_id FROM sys.databases WHERE name  = 'arutiimages' "
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "db")
            If dsList.Tables(0).Rows.Count > 0 Then
                mblnIsInDatabase = True
            End If
            dsList = Nothing

            'Pinkal (04-Apr-2013) -- End



            StrQ = "	SELECT " & _
                   "		 0 AS CId " & _
                   "		,'hrmsConfiguration' AS CName " & _
                   "		,1 AS IsGroup "

            'Sohail (04 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If blnAddArutiHRMS = True Then
                StrQ &= "UNION " & _
                   "	SELECT " & _
                   "		 0 AS CId " & _
                   "		,'arutihrms' AS CName " & _
                   "		,1 AS IsGroup "
            End If
            'Sohail (04 Jul 2012) -- End



            'Pinkal (04-Apr-2013) -- Start
            'Enhancement : TRA Changes

            'Sohail (09 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'If mblnIsInDatabase Then
            If mblnIsInDatabase AndAlso blnAddArutiImages Then
                'Sohail (09 Apr 2013) -- End
                StrQ &= "UNION " & _
                    "	SELECT " & _
                    "		 0 AS CId " & _
                    "		,'arutiimages' AS CName " & _
                    "		,1 AS IsGroup "
            End If

            'Pinkal (04-Apr-2013) -- End



            StrQ &= "UNION " & _
                   "	SELECT " & _
                   "		 hrmsConfiguration..cfcompany_master.companyunkid AS CId " & _
                   "		,ISNULL(hrmsConfiguration..cfcompany_master.name,'') AS CName " & _
                   "		,1 AS IsGroup " & _
                   "	FROM hrmsConfiguration..cfcompany_master " & _
                   "	WHERE ISNULL(hrmsConfiguration..cfcompany_master.isactive,0) = 1 " & _
                   "	ORDER BY CId "

            dsGrpList = objDataOperation.ExecQuery(StrQ, "GrpData")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsGrpList.Tables("GrpData").Rows.Count > 0 Then
                dtTable = New DataTable(StrList)
                dtTable.Columns.Add("Database", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("Path", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = 0
                dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = 0
                dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("GrpPath", System.Type.GetType("System.String")).DefaultValue = String.Empty
                dtTable.Columns.Add("YearId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dtTable.Columns.Add("IsTaken", System.Type.GetType("System.Boolean")).DefaultValue = 0
                dtTable.Columns.Add("isclosed", System.Type.GetType("System.Boolean")).DefaultValue = 0 'Sohail (08 Apr 2013)

                'Pinkal (14-Dec-2017) -- Start
                'Enhancement - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
                dtTable.Columns.Add("start_date", System.Type.GetType("System.DateTime"))
                dtTable.Columns.Add("end_date", System.Type.GetType("System.DateTime"))
                'Pinkal (14-Dec-2017) -- End

                StrQ = "SELECT " & _
                           "	 database_name AS dname " & _
                           "	,companyunkid AS companyunkid " & _
                           "    ,hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                           "    ,hrmsConfiguration..cffinancial_year_tran.isclosed " & _
                           "    ,hrmsConfiguration..cffinancial_year_tran.start_date " & _
                           "    ,hrmsConfiguration..cffinancial_year_tran.end_date " & _
                       "FROM hrmsConfiguration..cffinancial_year_tran " & _
                           " JOIN sys.databases ON sys.databases.NAME=hrmsConfiguration ..cffinancial_year_tran.database_name "


                'Pinkal (14-Dec-2017) -- 'Enhancement - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.[hrmsConfiguration..cffinancial_year_tran.start_date,hrmsConfiguration..cffinancial_year_tran.end_date]

                'Sohail (08 Apr 2013) - [isclosed]

                'Anjan [ 15 Apr 2013 ] -- Start
                'ENHANCEMENT : License CHANGES
                If blnIsOpenDB = True Then
                    StrQ &= " WHERE isclosed = 0 "
                End If
                'Anjan [ 15 Apr 2013 ] -- End



                dsTranList = objDataOperation.ExecQuery(StrQ, "TList")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dtFilter As DataTable

                For Each dtRow As DataRow In dsGrpList.Tables("GrpData").Rows

                    Dim objConfig = New clsConfigOptions
                    objConfig._Companyunkid = CInt(dtRow.Item("CId"))

                    Dim dRow As DataRow = dtTable.NewRow

                    dRow.Item("Database") = dtRow.Item("CName")
                    dRow.Item("IsGrp") = dtRow.Item("IsGroup")
                    dRow.Item("GrpId") = dtRow.Item("CId")
                    dRow.Item("GrpPath") = objConfig._DatabaseExportPath.ToString

                    dtTable.Rows.Add(dRow)

                    dtFilter = New DataView(dsTranList.Tables("TList"), "companyunkid = '" & dtRow.Item("CId") & "'", "", DataViewRowState.CurrentRows).ToTable
                    If dtFilter.Rows.Count > 0 Then
                        For Each dTrRow As DataRow In dtFilter.Rows
                            Dim dRowT As DataRow = dtTable.NewRow
                            dRowT.Item("Database") = Space(5) & dTrRow.Item("dname")
                            dRowT.Item("IsGrp") = False
                            dRowT.Item("GrpId") = dtRow.Item("CId")
                            dRowT.Item("Path") = objConfig._DatabaseExportPath.ToString
                            dRowT.Item("YearId") = dTrRow.Item("yearunkid")
                            dRowT.Item("isclosed") = dTrRow.Item("isclosed") 'Sohail (08 Apr 2013)

                            'Pinkal (14-Dec-2017) -- Start
                            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
                            dRowT.Item("start_date") = CDate(dTrRow.Item("start_date"))
                            dRowT.Item("end_date") = CDate(dTrRow.Item("end_date"))
                            'Pinkal (14-Dec-2017) -- End

                            dtTable.Rows.Add(dRowT)
                        Next
                    End If
                Next
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_DataBaseList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    



    'Sandeep [ 10 FEB 2011 ] -- End 

    'Sohail (09 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetTotalActiveEmployeeCount(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkID As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal dtTotalActiveEmployeeAsOnFromDate As Date _
                                           , ByVal dtTotalActiveEmployeeAsOnToDate As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           ) As Integer
        'Sohail (15 Feb 2016) - [xDatabaseName, xUserUnkid, xYearUnkID, xCompanyUnkid, xUserModeSetting, xOnlyApproved]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If dtTotalActiveEmployeeAsOnFromDate = Nothing Then dtTotalActiveEmployeeAsOnFromDate = CDate("01/Jan/1900")
            If dtTotalActiveEmployeeAsOnToDate = Nothing Then dtTotalActiveEmployeeAsOnToDate = CDate("01/Jan/9800")

            'Sohail (15 Feb 2016) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtTotalActiveEmployeeAsOnFromDate, dtTotalActiveEmployeeAsOnToDate, , , xDatabaseName)
            'S.SANDEEP [15 NOV 2016] -- START
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtTotalActiveEmployeeAsOnToDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkID, xUserModeSetting)
            'S.SANDEEP [15 NOV 2016] -- END
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtTotalActiveEmployeeAsOnToDate, xDatabaseName)
            'Sohail ((15 Feb 2016) -- End

            'Sohail (15 Feb 2016) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'strQ = "SELECT  COUNT(*) AS TotalEmployee " & _
            '        "FROM    hremployee_master " & _
            '        "WHERE   1 = 1 " & _
            '                "AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
            '                "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
            '                "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "
            strQ = "SELECT  COUNT(*) AS TotalEmployee " & _
                   "FROM    hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE   1 = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry
            End If
            'Sohail (15 Feb 2016) -- End

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTotalActiveEmployeeAsOnFromDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTotalActiveEmployeeAsOnToDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0).Item("TotalEmployee"))
            Else
                Return 0
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalActiveEmployeeCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (04 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    'Public Function GetTotalActiveEmployeeCountForAllCompany(ByVal dtTotalActiveEmployeeAsOnFromDate As Date, ByVal dtTotalActiveEmployeeAsOnToDate As Date) As Integer
    '    Dim dsList As DataSet = Nothing
    '    Dim objDataOperation As clsDataOperation
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim intToalCount As Integer = 0
    '    Dim sQry As String = "" 'Sohail (10 May 2013)

    '    objDataOperation = New clsDataOperation

    '    Try
    '        If dtTotalActiveEmployeeAsOnFromDate = Nothing Then dtTotalActiveEmployeeAsOnFromDate = CDate("01/Jan/1900")
    '        If dtTotalActiveEmployeeAsOnToDate = Nothing Then dtTotalActiveEmployeeAsOnToDate = CDate("01/Jan/9800")

    '        'Sohail (10 May 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'dtTable = Get_DataBaseList("Database", False, False)
    '        'dtTable = New DataView(dtTable, "isclosed = 0 AND IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable
    '        sQry = "SELECT  database_name AS dname " & _
    '                  ", cffinancial_year_tran.companyunkid AS companyunkid " & _
    '                  ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
    '                  ", hrmsConfiguration..cffinancial_year_tran.isclosed " & _
    '                "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '                        "JOIN sys.databases ON sys.databases.NAME = hrmsConfiguration..cffinancial_year_tran.database_name " & _
    '                        "JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
    '                            "AND isclosed = 0 " & _
    '                            "AND ISNULL(cfcompany_master.isactive, 0) = 1 "

    '        dsList = objDataOperation.ExecQuery(sQry, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (10 May 2013) -- End



    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTotalActiveEmployeeAsOnFromDate))
    '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTotalActiveEmployeeAsOnToDate))

    '        For Each dtRow As DataRow In dsList.Tables("List").Rows


    '            strQ &= "UNION ALL SELECT  1 AS totalemp " & _
    '                    "FROM    " & dtRow.Item("dname").ToString.Trim & "..hremployee_master " & _
    '                    "WHERE   1 = 1 " & _
    '                            "AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                            "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
    '                            "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
    '                            "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "
    '        Next

    '        strQ = "SELECT  COUNT(totalemp) AS TotalEmployee FROM  ( " & strQ.Substring(9) & " ) AS a " 'Remove starting "UNION ALL" from string.

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            intToalCount = CInt(dsList.Tables("List").Rows(0).Item("TotalEmployee"))
    '        End If

    '        Return intToalCount

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetTotalActiveEmployeeCountForAllCompany; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Public Function GetTotalActiveEmployeeCountForAllCompany(ByVal dtTotalActiveEmployeeAsOnDate As Date) As Integer
        Dim dsList As DataSet = Nothing
        Dim objDataOperation As clsDataOperation
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intToalCount As Integer = 0
        Dim sQry As String = ""

        objDataOperation = New clsDataOperation

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry As String
            'Shani(24-Aug-2015) -- End
            If dtTotalActiveEmployeeAsOnDate = Nothing Then dtTotalActiveEmployeeAsOnDate = CDate("01/Jan/9800")

            sQry = "SELECT  database_name AS dname " & _
                      ", cffinancial_year_tran.companyunkid AS companyunkid " & _
                      ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                      ", hrmsConfiguration..cffinancial_year_tran.isclosed " & _
                    "FROM    hrmsConfiguration..cffinancial_year_tran " & _
                            "JOIN sys.databases ON sys.databases.NAME = hrmsConfiguration..cffinancial_year_tran.database_name " & _
                            "JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
                                "AND isclosed = 0 " & _
                                "AND ISNULL(cfcompany_master.isactive, 0) = 1 "

            dsList = objDataOperation.ExecQuery(sQry, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTotalActiveEmployeeAsOnDate))
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtTotalActiveEmployeeAsOnDate)) 'Sohail (21 Aug 2014)
            'Shani(24-Aug-2015) -- End

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                xDateJoinQry = "" : xDateFilterQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtTotalActiveEmployeeAsOnDate, dtTotalActiveEmployeeAsOnDate, , , dtRow.Item("dname").ToString.Trim)

                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2014) -- Start
                'Enhancement - IN VOLTAMP,IT WAS DISPLAYING WRONG ACTIVE EMPLOYEE COUNT.IT DISPLAYED TERMINETED AND EOC DATE EMPLOYEE INCLUDING ACTIVE EMPLOYEE INSTEAD OF DISPLAY ONLY ACTIVE EMPLOYEE. IT WAS  DUR TO LOGIC SET FOR PAYROLL THAT IN ONE MONTH EMPLOYEES CANNOT BE PROCESSED MORE THAN LICENSE BOUGHT.
                'strQ &= "UNION ALL SELECT  1 AS totalemp " & _
                '        "FROM    " & dtRow.Item("dname").ToString.Trim & "..hremployee_master AS A " & _
                '        "LEFT JOIN " & dtRow.Item("dname").ToString.Trim & "..hremployee_master AS B ON A.employeeunkid = B.employeeunkid " & _
                '                "AND ISNULL(CONVERT(CHAR(8), B.termination_from_date, 112), @startdate) <= @startdate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), B.termination_to_date, 112), @startdate) <= @startdate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), B.empl_enddate, 112), @startdate) <= @startdate " & _
                '        "WHERE   1 = 1 " & _
                '        "AND    B.employeeunkid IS NULL "

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'strQ &= "UNION ALL SELECT  1 AS totalemp " & _
                '        "FROM    " & dtRow.Item("dname").ToString.Trim & "..hremployee_master " & _
                '        "WHERE   1 = 1 " & _
                '                "AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) >= @startdate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) >= @startdate " & _
                '                "AND ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) >= @startdate "

                strQ &= "UNION ALL SELECT  1 AS totalemp " & _
                        "FROM    " & dtRow.Item("dname").ToString.Trim & "..hremployee_master "

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                strQ &= " WHERE 1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If

                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2014) -- End
            Next

            If dsList.Tables("List").Rows.Count > 0 Then 'Anjan [ 09 Sep 2013 ]
            strQ = "SELECT  COUNT(totalemp) AS TotalEmployee FROM  ( " & strQ.Substring(9) & " ) AS a " 'Remove starting "UNION ALL" from string.

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intToalCount = CInt(dsList.Tables("List").Rows(0).Item("TotalEmployee"))
            End If
            End If 'Anjan [ 09 Sep 2013 ]            

            Return intToalCount

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalActiveEmployeeCountForAllCompany; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (04 Jun 2013) -- End
    'Sohail (09 Apr 2013) -- End

    'Sohail (10 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetTotalActiveCompanyCount() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  COUNT(*) AS TotalCompany " & _
                    "FROM    hrmsConfiguration..cfcompany_master " & _
                    "WHERE   isactive = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0).Item("TotalCompany"))
            Else
                Return 0
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalActiveCompanyCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (10 Apr 2013) -- End

    'Sohail (13 Mar 2020) -- Start
    'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
    Public Function GetIdByCode(ByVal strCompanyCode As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intId As Integer = 0

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                        " companyunkid " & _
                 "FROM hrmsConfiguration..cfcompany_master " & _
                 "WHERE isactive = 1 " & _
                    "AND code = @code "

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCompanyCode)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("companyunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetIdByCode; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return intId
    End Function
    'Sohail (13 Mar 2020) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Company is already defined. Please define new Company.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this company. Reason : This company is already in use.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Company was not created successfully. Please contact Aruti Support team.")
			Language.setMessage("frmCompany_AddEdit", 10, "You have added incorrect Bank information. Please insert correct Bank Information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
