﻿'************************************************************************************************************************************
'Class Name : clsAnnouncement_document.vb
'Purpose    :
'Date       :07/04/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

Public Class clsAnnouncement_document

    Private Shared ReadOnly mstrModuleName As String = "clsAnnouncement_document"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mintanndocumentunkid As Integer
    Private mintAnnouncementunkid As Integer
    Private mdtTran As DataTable
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set announcementunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Announcementunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintAnnouncementunkid
        End Get
        Set(ByVal value As Integer)
            mintAnnouncementunkid = value
            GetAnnouncementDocument("List", True, mintAnnouncementunkid, objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtDocumentList
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtDocumentList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("anndocumentunkid")
            dCol.DataType = GetType(Int32)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("announcementunkid")
            dCol.DataType = GetType(Int32)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("attached_date")
            dCol.DataType = GetType(DateTime)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("filename")
            dCol.DataType = GetType(String)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("fileuniquename")
            dCol.DataType = GetType(String)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("file_data")
            dCol.DataType = GetType(System.Byte())
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = GetType(Int32)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = GetType(Boolean)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = GetType(DateTime)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = GetType(Int32)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = GetType(String)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = GetType(String)
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = GetType(String)
            mdtTran.Columns.Add(dCol)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetAnnouncementDocument(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                                                             , Optional ByVal xAnnouncementId As Integer = -1, Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = " SELECT " & _
                       " anndocumentunkid " & _
                       " ,cfannouncement_document.announcementunkid " & _
                       " ,ISNULL(cfannouncement_master.title, '') AS Title " & _
                       " ,ISNULL(cfannouncement_master.announcement, '') AS Announcement " & _
                       " ,cfannouncement_document.attached_date " & _
                       " ,cfannouncement_document.filename " & _
                       " ,cfannouncement_document.fileuniquename " & _
                       " ,cfannouncement_document.file_data " & _
                       " ,cfannouncement_document.userunkid " & _
                       " ,cfannouncement_document.isvoid " & _
                       " ,cfannouncement_document.voiddatetime " & _
                       " ,cfannouncement_document.voiduserunkid " & _
                       " ,cfannouncement_document.voidreason " & _
                       " ,'' AS AUD " & _
                       " ,'' AS GUID " & _
                       " FROM hrmsConfiguration..cfannouncement_document " & _
                       " LEFT JOIN hrmsConfiguration..cfannouncement_master ON cfannouncement_master.announcementunkid = cfannouncement_document.announcementunkid " & _
                       " WHERE 1= 1 "

            If blnOnlyActive Then
                strQ &= " AND cfannouncement_document.isvoid = 0 AND cfannouncement_master.isvoid = 0 "
            End If

            If xAnnouncementId <> -1 Then
                strQ &= " AND cfannouncement_document.announcementunkid = @announcementunkid  "
                objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAnnouncementId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAnnouncementDocument; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Insert_Update_DeleteDocument(Optional ByVal xAnnouncementId As Integer = 0 _
                                                 , Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mblnFlag As Boolean = False
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDoOperation
            End If

            If mdtTran Is Nothing OrElse mdtTran.Rows.Count <= 0 Then
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If


            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")

                            Case "A"


                                StrQ = "INSERT INTO hrmsConfiguration..cfannouncement_document ( " & _
                                           "  announcementunkid " & _
                                           ", attached_date " & _
                                           ", filename " & _
                                           ", fileuniquename " & _
                                           ", file_data " & _
                                           ", userunkid " & _
                                           ", isvoid " & _
                                           ", voiddatetime " & _
                                           ", voiduserunkid " & _
                                           ", voidreason " & _
                                       ") VALUES (" & _
                                           "  @announcementunkid " & _
                                           ", @attached_date " & _
                                           ", @filename " & _
                                           ", @fileuniquename " & _
                                           ", @file_data " & _
                                           ", @userunkid " & _
                                           ", @isvoid " & _
                                           ", @voiddatetime " & _
                                           ", @voiduserunkid " & _
                                           ", @voidreason " & _
                                       "); SELECT @@identity"

                                objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid)
                                
                                If IsDBNull(.Item("attached_date")) = False Then
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                Else
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename"))
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename"))

                                If IsDBNull(.Item("file_data")) = False Then
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, DirectCast(.Item("file_data"), Byte()).Length, .Item("file_data"))
                                Else
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, 0, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintanndocumentunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertDocumentAuditTrail(objDataOperation, 1, mintanndocumentunkid, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If



                            Case "U"

                                StrQ = " UPDATE hrmsConfiguration..cfannouncement_document SET " & _
                                           "  announcementunkid = @announcementunkid" & _
                                           ", attached_date = @attached_date " & _
                                           ", filename = @filename " & _
                                           ", fileuniquename = @fileuniquename  " & _
                                           ", file_data = @file_data " & _
                                           ", userunkid = @userunkid " & _
                                           " WHERE isvoid = 0 AND anndocumentunkid = @anndocumentunkid AND announcementunkid = @announcementunkid "

                                objDataOperation.AddParameter("@anndocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintanndocumentunkid)
                                objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid)

                                If IsDBNull(.Item("attached_date")) = False Then
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                Else
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename"))
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename"))

                                If IsDBNull(.Item("file_data")) = False Then
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, DirectCast(.Item("file_data"), Byte()).Length, .Item("file_data"))
                                Else
                                    objDataOperation.AddParameter("@file_data", SqlDbType.VarBinary, 0, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertDocumentAuditTrail(objDataOperation, 2, mintanndocumentunkid, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "D"

                                StrQ = "UPDATE hrmsConfiguration..cfannouncement_document SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       " WHERE isvoid = 0 AND  anndocumentunkid = @anndocumentunkid "

                                objDataOperation.AddParameter("@anndocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("anndocumentunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If CInt(.Item("anndocumentunkid")) > 0 Then
                                    If InsertDocumentAuditTrail(objDataOperation, 3, CInt(.Item("anndocumentunkid")), mdtTran.Rows(i)) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select
                    End If
                End With
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If mblnFlag Then objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Insert_Update_DeleteDocument", mstrModuleName)
        Finally
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function DeleteDocuments(ByVal objDoOperation As clsDataOperation, ByVal xAnnouncementId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            GetAnnouncementDocument("List", True, xAnnouncementId, objDoOperation)

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then

                StrQ = "UPDATE hrmsConfiguration..cfannouncement_document SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid" & _
                           ", voiddatetime = @voiddatetime" & _
                           ", voidreason = @voidreason " & _
                           " WHERE isvoid = 0 AND  anndocumentunkid = @anndocumentunkid "

                For Each dr In mdtTran.Rows
                    objDoOperation.ClearParameters()
                    objDoOperation.AddParameter("@anndocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("anndocumentunkid"))
                    objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    objDoOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                    Call objDoOperation.ExecNonQuery(strQ)

                    If objDoOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If InsertDocumentAuditTrail(objDoOperation, 3, CInt(dr("anndocumentunkid")), dr) = False Then
                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteDocuments; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertDocumentAuditTrail(ByVal objDoOperation As clsDataOperation, ByVal xAuditType As Integer, ByVal xAnnDocumentID As Integer, ByVal dr As DataRow) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDoOperation.ClearParameters()

            objDoOperation.AddParameter("@anndocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAnnDocumentID)
            objDoOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid)
            If IsDBNull(dr("attached_date")) = False Then
                objDoOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dr("attached_date"))
            Else
                objDoOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDoOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("filename"))
            objDoOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("fileuniquename"))

            If IsDBNull(dr("file_data")) = False Then
                objDoOperation.AddParameter("@file_data", SqlDbType.VarBinary, DirectCast(dr("file_data"), Byte()).Length, dr("file_data"))
            Else
                objDoOperation.AddParameter("@file_data", SqlDbType.VarBinary, 0, DBNull.Value)
            End If


            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.Trim.Length > 0 Then
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            Else
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrWebHostName.Length > 0 Then
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            Else
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO hrmsconfiguration..atcfannouncement_document ( " & _
                      "  anndocumentunkid " & _
                      ", announcementunkid " & _
                      ", attached_date " & _
                      ", filename " & _
                      ", fileuniquename " & _
                      ", file_data " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                     ") VALUES (" & _
                      "  @anndocumentunkid " & _
                      ", @announcementunkid " & _
                      ", @attached_date " & _
                      ", @filename " & _
                      ", @fileuniquename " & _
                      ", @file_data " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GetDate() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDocumentAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

#End Region

End Class
