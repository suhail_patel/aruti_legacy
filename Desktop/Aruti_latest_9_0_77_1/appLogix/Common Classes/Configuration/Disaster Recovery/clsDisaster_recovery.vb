﻿'************************************************************************************************************************************
'Class Name : clsDisaster_recovery.vb
'Purpose    :
'Date       :07/07/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsDisaster_recovery
    Private Const mstrModuleName = "clsDisaster_recovery"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDisasterrecoveryunkid As Integer
    Private mstrTask_Name As String = String.Empty
    Private mstrMachine_Ip As String = String.Empty
    Private mintCompanyunkid As Integer
    Private mstrEmail1 As String = String.Empty
    Private mstrEmail2 As String = String.Empty
    Private mstrEmail3 As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer = -1
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disasterrecoveryunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Disasterrecoveryunkid() As Integer
        Get
            Return mintDisasterrecoveryunkid
        End Get
        Set(ByVal value As Integer)
            mintDisasterrecoveryunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set task_name
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Task_Name() As String
        Get
            Return mstrTask_Name
        End Get
        Set(ByVal value As String)
            mstrTask_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine_ip
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Machine_Ip() As String
        Get
            Return mstrMachine_Ip
        End Get
        Set(ByVal value As String)
            mstrMachine_Ip = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Email1() As String
        Get
            Return mstrEmail1
        End Get
        Set(ByVal value As String)
            mstrEmail1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Email2() As String
        Get
            Return mstrEmail2
        End Get
        Set(ByVal value As String)
            mstrEmail2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email3
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Email3() As String
        Get
            Return mstrEmail3
        End Get
        Set(ByVal value As String)
            mstrEmail3 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  disasterrecoveryunkid " & _
              ", task_name " & _
              ", machine_ip " & _
              ", companyunkid " & _
              ", userunkid " & _
              ", email1 " & _
              ", email2 " & _
              ", email3 " & _
              ", isactive " & _
             "FROM hrmsConfiguration..cfdisaster_recovery " & _
             "WHERE disasterrecoveryunkid = @disasterrecoveryunkid "

            objDataOperation.AddParameter("@disasterrecoveryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDisasterrecoveryUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdisasterrecoveryunkid = CInt(dtRow.Item("disasterrecoveryunkid"))
                mstrtask_name = dtRow.Item("task_name").ToString
                mstrMachine_Ip = dtRow.Item("machine_ip").ToString
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mstremail1 = dtRow.Item("email1").ToString
                mstremail2 = dtRow.Item("email2").ToString
                mstremail3 = dtRow.Item("email3").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  disasterrecoveryunkid " & _
              ", task_name " & _
              ", machine_ip " & _
              ", companyunkid " & _
              ", userunkid " & _
              ", email1 " & _
              ", email2 " & _
              ", email3 " & _
              ", isactive " & _
             "FROM hrmsConfiguration..cfdisaster_recovery "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfdisaster_recovery) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@task_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtask_name.ToString)
            objDataOperation.AddParameter("@machine_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Ip.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@email1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail1.ToString)
            objDataOperation.AddParameter("@email2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail2.ToString)
            objDataOperation.AddParameter("@email3", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail3.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "INSERT INTO hrmsConfiguration..cfdisaster_recovery ( " & _
              "  task_name " & _
              ", machine_ip " & _
              ", companyunkid " & _
              ", userunkid " & _
              ", email1 " & _
              ", email2 " & _
              ", email3 " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @task_name " & _
              ", @machine_ip " & _
              ", @companyunkid " & _
              ", @userunkid " & _
              ", @email1 " & _
              ", @email2 " & _
              ", @email3 " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDisasterrecoveryUnkId = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfdisaster_recovery) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintDisasterrecoveryunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@disasterrecoveryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdisasterrecoveryunkid.ToString)
            objDataOperation.AddParameter("@task_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtask_name.ToString)
            objDataOperation.AddParameter("@machine_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Ip.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@email1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail1.ToString)
            objDataOperation.AddParameter("@email2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail2.ToString)
            objDataOperation.AddParameter("@email3", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail3.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "UPDATE hrmsConfiguration..cfdisaster_recovery SET " & _
              "  task_name = @task_name" & _
              ", machine_ip = @machine_ip" & _
              ", companyunkid = @companyunkid" & _
              ", userunkid = @userunkid " & _
              ", email1 = @email1" & _
              ", email2 = @email2" & _
              ", email3 = @email3" & _
              ", isactive = @isactive " & _
            "WHERE disasterrecoveryunkid = @disasterrecoveryunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfdisaster_recovery) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM hrmsConfiguration..cfdisaster_recovery " & _
            "WHERE disasterrecoveryunkid = @disasterrecoveryunkid "

            objDataOperation.AddParameter("@disasterrecoveryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@disasterrecoveryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  disasterrecoveryunkid " & _
              ", task_name " & _
              ", machine_ip " & _
              ", companyunkid " & _
              ", userunkid " & _
              ", email1 " & _
              ", email2 " & _
              ", email3 " & _
              ", isactive " & _
             "FROM hrmsConfiguration..cfdisaster_recovery " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND disasterrecoveryunkid <> @disasterrecoveryunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@disasterrecoveryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUnkId(ByVal strTaskName As String, ByVal strMachineIp As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = -1

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  disasterrecoveryunkid " & _
                    "FROM    hrmsConfiguration..cfdisaster_recovery " & _
                    "WHERE   isactive = 1 " & _
                            "AND task_name = @task_name " & _
                            "AND machine_ip = @machine_ip "

            objDataOperation.AddParameter("@task_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strTaskName)
            objDataOperation.AddParameter("@machine_ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strMachineIp)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("disasterrecoveryunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function
End Class