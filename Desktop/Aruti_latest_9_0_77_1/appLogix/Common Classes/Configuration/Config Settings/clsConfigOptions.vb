﻿Imports eZeeCommonLib
Imports Aruti.Data.Language

Public Class clsConfigOptions

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsConfigOptions"
    Private mdtParam As DataTable
    Private mintCompanyUnkid As Integer = -1

    'Pinkal (18-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mintConfigOptionID As Integer = 0
    'Pinkal (18-Apr-2013) -- End


    'Pinkal (21-Oct-2013) -- Start
    'Enhancement : Oman Changes
    Private mintASR_Leave1 As Integer = -1
    Private mintASR_Leave2 As Integer = -1
    Private mintASR_Leave3 As Integer = -1
    Private mintASR_Leave4 As Integer = -1
    Private mstrSpecial_LeaveIds As String = String.Empty
    'Pinkal (21-Oct-2013) -- End


    'Pinkal (1-Jul-2014) -- Start
    'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
    Private mintASR_Leave5 As Integer = -1
    'Pinkal (1-Jul-2014) -- End


    'Pinkal (09-Jun-2015) -- Start
    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
    Private mstrEmpTimesheetSetting As String = ""

    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
    'Private mblnSetDeviceInOutStatus As Boolean = False
    'Pinkal (06-May-2016) -- End

    'Pinkal (09-Jun-2015) -- End



    'Pinkal (25-Feb-2015) -- Start
    'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
    Private mblnASR_ShowTotalhrs As Boolean = False
    'Pinkal (25-Feb-2015) -- End

'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    'Gajanan [30-Mar-2019] -- Start
    'Private mstrSkipApprovalOnEmpDatavalue As String = CInt(enScreenName.frmQualificationsList) & "," & _
    '                                                   CInt(enScreenName.frmEmployeeRefereeList) & "," & _
    '                                                   CInt(enScreenName.frmJobHistory_ExperienceList) & "," & _
    '                                                   CInt(enScreenName.frmEmployee_Skill_List)


    Private mstrSkipApprovalOnEmpDatavalue As String = CInt(enScreenName.frmQualificationsList) & "," & _
                                                       CInt(enScreenName.frmEmployeeRefereeList) & "," & _
                                                       CInt(enScreenName.frmJobHistory_ExperienceList) & "," & _
                                                       CInt(enScreenName.frmEmployee_Skill_List) & "," & _
                                                       CInt(enScreenName.frmIdentityInfoList) & "," & _
                                                       CInt(enScreenName.frmDependantsAndBeneficiariesList) & "," & _
                                                       CInt(enScreenName.frmAddressList) & "," & _
                                                       CInt(enScreenName.frmEmergencyAddressList) & "," & _
                                                       CInt(enScreenName.frmMembershipInfoList) & "," & _
                                                       CInt(enScreenName.frmBirthinfo) & "," & _
                                                       CInt(enScreenName.frmOtherinfo)

    'S.SANDEEP |15-APR-2019| -- START {CInt(enScreenName.frmMembershipInfoList) ,CInt(enScreenName.frmBirthinfo),CInt(enScreenName.frmotherinfo)}-- END
'Gajanan [17-DEC-2018] -- End
    'Gajanan [30-Mar-2019] -- End
#End Region

    'Sohail (01 Jan 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Constructor "
    Public Sub New()
        Try
            Dim objGSettings As New clsGeneralSettings
            objGSettings._Section = gApplicationType.ToString
            mstrArutiSelfServiceURL = "http://" & IIf(objGSettings._ServerName.Trim.ToUpper = "(LOCAL)", getIP(), objGSettings._ServerName).ToString & "/ArutiSelfService"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (01 Jan 2013) -- End

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
#Region " ENUMS "

    Public Enum enForcastedSetting
        BY_MONTH = 1
        BY_YEAR = 2

        'Pinkal (03-Nov-2014) -- Start
        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
        BY_DAY = 3
        'Pinkal (03-Nov-2014) -- End
    End Enum

#End Region
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#Region " Propertie(s) "

    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyUnkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
            Call FillData()
        End Set
    End Property

'Sohail (10 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' For Aruti Licence : Return Licence given for No. of Employees.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _NoOfEmployees() As Integer
        Get
            'Sohail (01 May 2013) -- Start
            'TRA - ENHANCEMENT
            'Return CInt(ArtLic._Object.NoOfEmployees)
            Dim intEmp As Integer
            Integer.TryParse(GetKeyValue(0, "Emp"), intEmp)
            Return intEmp
            'Sohail (01 May 2013) -- End
        End Get
    End Property

    ''' <summary>
    ''' For Aruti Licence : Return Licence given for No. of Companies.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _NoOfCompany() As Integer
        Get
            'Sohail (01 May 2013) -- Start
            'TRA - ENHANCEMENT
            'Return CInt(ArtLic._Object.NoOfEmployees)
            Dim intComp As Integer
            Integer.TryParse(GetKeyValue(0, "Comp"), intComp)
            Return intComp
            'Sohail (01 May 2013) -- End
        End Get
    End Property
    'Sohail (10 Apr 2013) -- End

    'Sohail (08 May 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _TnAActivate() As String
        Get
            Return GetKeyValue(0, "TnAActivate")
        End Get
    End Property

    Public ReadOnly Property _PayrollProcess() As String
        Get
            Return GetKeyValue(0, "PayrollProcess")
        End Get
    End Property

    Public ReadOnly Property _DaysLeft() As Integer
        Get
            Dim dtEnd As Date
            Dim dt As String = GetKeyValue(0, "PayrollProcess")
            If dt.Trim <> "" Then
                dtEnd = eZeeDate.convertDate(dt)
                Return DateDiff(DateInterval.Day, ConfigParameter._Object._CurrentDateAndTime.Date, dtEnd)
            Else
                Return -1
            End If
        End Get
    End Property

    Public ReadOnly Property _IsExpire() As Boolean
        Get
            If _IsArutiDemo = True Then
                If _DaysLeft < 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property _IsArutiDemo() As Boolean
        Get
            If GetKeyValue(0, "TnAActivate") <> "Aruti Lic Activated" Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    'Sohail (08 May 2013) -- End

    'Pinkal (18-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _ConfigOptionid() As Integer
        Get
            Return mintConfigOptionID
        End Get
        Set(ByVal value As Integer)
            mintConfigOptionID = value
        End Set
    End Property

    'Pinkal (18-Apr-2013) -- End

#End Region

#Region " Refreshing "
    Public Sub Refresh()
        'S.SANDEEP [ 22 MAY 2014 ] -- START
        mintCompanyUnkid = Company._Object._Companyunkid
        Call SetDefaultData()
        'S.SANDEEP [ 22 MAY 2014 ] -- END
        mdtParam = New DataTable
        Call setLocalVars()
        'Call setMachineSettings()
    End Sub
#End Region

#Region " General Settings "
    '-- Payslip 
    Private mintPayslipVocNoType As Integer = 1
    Private mstrPayslipVocPrefix As String = ""
    Private mintNextPayslipVocNo As Integer = 1
    '-- Loan
    Private mintLoanVocNoType As Integer = 1
    Private mstrLoanVocPrefix As String = ""
    Private mintNextLoanVocNo As Integer = 1
    '-- Savings
    Private mintSavingsVocNoType As Integer = 1
    Private mstrSavingsVocPrefix As String = ""
    Private mintNextSavingsVocNo As Integer = 1
    '-- Payment
    Private mintPaymentVocNoType As Integer = 1
    Private mstrPaymentVocPrefix As String = ""
    Private mintNextPaymentVocNo As Integer = 1
    '-- View Option
    Private mintDisplayView As Integer = 14
    '-- Last Leave Process Date
    Private mdtLeaveProcessDate As Date = Nothing
    '-- Export / Import Parameters
    Private mintFileFormat As Integer = 1
    Private mstrSeparator As String = ""
    'Identity Sensitive
    Private mblnIsIdSensitive As Boolean = False
    '-- Misc
    Private mintLoanApplicationNoType As Integer = 1
    Private mstrLoanApplicationPrifix As String = ""
    Private mintNextLoanApplicationNo As Integer = 1

    Private mintLeaveFormNoType As Integer = 1
    Private mstrLeaveFormNoPrifix As String = ""
    Private mintNextLeaveFormNo As Integer = 1

    Private mintEmployeeCodeNotype As Integer = 1
    Private mstrEmployeeCodePrifix As String = ""
    Private mintNextEmployeeCodeNo As Integer = 1

    'Sohail (28 May 2014) -- Start
    'Enhancement - Staff Requisition.
    Private mintStaffReqFormNoType As Integer = 1
    Private mstrStaffReqFormNoPrefix As String = ""
    Private mintNextStaffReqFormNo As Integer = 1
    Private mblnApplyStaffRequisition As Boolean = False
    'Sohail (28 May 2014) -- End
    Private mblnEnforceManpowerPlanning As Boolean = False     'Hemant (03 Sep 2019)

    Private mintRetirementBy As Integer = 1
    Private mintRetirementValue As Integer = 55

    Private mblnFirstNamethenSurname As Boolean = True
    Private mstrWeekendDays As String = "0,0,0,0,0,1,1"

    Private mintApplicantCodeNoType As Integer = 1
    Private mstrApplicantCodePrifix As String = ""
    Private mintNextApplicantCodeNo As Integer = 1

    Private mintNoofDigitAfterDecimalPoint As Integer = 2

    'Anjan (21 Dec 2010)-Start
    Private mdblRoundOffType As Double = 1
    Private mdblManualRoundOffLimit As Double = 1
    'Anjan (21 Dec 2010)-End

    'Sohail (22 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintPaymentRoundingType As Integer = 0
    Private mintPaymentRoundingMultiple As Integer = 0
    'Sohail (22 Oct 2013) -- End
    Private mdecCFRoundingAbove As Decimal = 0 'Sohail (21 Mar 2014)

'Sandeep [ 17 DEC 2010 ] -- Start
    Private mblnIsDenominationCompulsory As Boolean = False
    'Sandeep [ 17 DEC 2010 ] -- End 


    'Sandeep [ 10 FEB 2011 ] -- Start
    Private mblnIsDailyBackupEnabled As Boolean = False
    Private mdtBackupTime As Date = Nothing
    'Sandeep [ 10 FEB 2011 ] -- End 

    'Sandeep [ 01 MARCH 2011 ] -- Start
    Private mintBaseCurrencyId As Integer = 0
    Private mstrCurrencyFormat As String = "#,###,###,##0.00"
    'Sandeep [ 01 MARCH 2011 ] -- End 


    'S.SANDEEP [ 29 JUNE 2011 ] -- START
    'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    Private mblnIncludeInactiveEmployee As Boolean = False
    'S.SANDEEP [ 29 JUNE 2011 ] -- END 




    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Private mintSickSheetNoType As Integer = 1
    Private mstrSickSheetPrifix As String = ""
    Private mintNextSickSheetNo As Integer = 1
    'Pinkal (12-Oct-2011) -- End

    'Sohail (19 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintBatchPostingNoType As Integer = 1
    Private mstrBatchPostingPrifix As String = ""
    Private mintNextBatchPostingNo As Integer = 1
    'Sohail (19 Dec 2012) -- End


 'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsDependantAgeLimitMandatory As Boolean = False
    'S.SANDEEP [ 07 NOV 2011 ] -- END




    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIsLvApproverMandatoryForLeaveType As Boolean = False
    'Pinkal (06-Feb-2012) -- End

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAssetDeclarationInstruction As String = String.Empty
    'Sohail (06 Apr 2012) -- End
    'Sohail (06 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrDatabaseServer As String = String.Empty
    Private mstrDatabaseName As String = String.Empty
    Private mstrDatabaseUserName As String = String.Empty
    Private mstrDatabaseUserPassword As String = String.Empty
    Private mstrDatabaseOwner As String = String.Empty
    'Sohail (06 Jun 2012) -- End
    'Sohail (16 Nov 2018) -- Start
    'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
    Private mstrRecruitmentCareerLink As String = String.Empty
    Private mstrRecruitmentWebServiceLink As String = "http://www.arutihr.com/ArutiHRM/arutirecruitmentservice.asmx"
    'Sohail (16 Nov 2018) -- End

    Private mblnDoNotAllowOverDeductionPayment As Boolean = True 'Sohail (29 Jun 2012)

    Private mstrArutiSelfServiceURL As String = String.Empty 'Sohail (01 Jan 2013)


    'Anjan [ 20 Feb 2013 ] -- Start
    'ENHANCEMENT : TRA CHANGES
    Private mblnSetPaySlipPayApproval As Boolean = False
    'Anjan [ 20 Feb 2013 ] -- End
    Private mblnSetPaySlipPaymentMandatory As Boolean = True 'Sohail (21 Mar 2013)
    Private mblnApplyPayPerActivity As Boolean = False 'Sohail (20 Jul 2013)
    'Sohail (04 Aug 2016) -- Start
    'Enhancement - 63.1 - Option on configuration to Send Password protected E-Payslip.
    Private mblnSendPasswordProtectedEPayslip As Boolean = True
    'Sohail (04 Aug 2016) -- End
    'Sohail (23 Nov 2020) -- Start
    'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
    Private mblnIgnoreNegativeNetPayEmployeesOnJV As Boolean = False
    'Sohail (23 Nov 2020) -- End

    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Private mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year
    Private mintELCMonths As Integer = 0

    'Pinkal (06-Feb-2013) -- End



    'Pinkal (15-Sep-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnClosePayrollPeriodIfLeaveIssue As Boolean = False
    'Pinkal (15-Sep-2013) -- End


    'Pinkal (20-Sep-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnPolicyManagementTNA As Boolean = False
    'Pinkal (20-Sep-2013) -- End


    'Pinkal (15-Nov-2013) -- Start
    'Enhancement : Oman Changes
    Private mblnDonotAttendanceinSeconds As Boolean = False
    'Pinkal (15-Nov-2013) -- End


    'Pinkal (30-Nov-2013) -- Start
    'Enhancement : Oman Changes
    Private mstrOT1Order As String = ""
    Private mstrOT2Order As String = ""
    Private mstrOT3Order As String = ""
    Private mstrOT4Order As String = ""
    'Pinkal (30-Nov-2013) -- End


    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes
    Private mblnIsSeparateTnAPeriod As Boolean = False
    'Pinkal (03-Jan-2014) -- End




    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes
    Private mblnIsAutomaticLeaveIssueOnFinalApproval As Boolean = False
    'Pinkal (01-Feb-2014) -- End


    'Anjan [06 April 2015] -- Start
    'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
    Private mblnImportPeoplesoftData As Boolean = False
    'Anjan [06 April 2015] -- End

    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
     Private mblnIsLoanApproverMandatoryForLoanScheme As Boolean = False
    'Pinkal (14-Apr-2015) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendLoanEmailFromDesktopMSS As Boolean = False
    Private mblnIsSendLoanEmailFromESS As Boolean = True
    'Nilay (10-Dec-2016) -- End

    'Sohail (16 May 2018) -- Start
    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
    Private mdecAdvance_NetPayPercentage As Decimal = 0
    Private mintAdvance_NetPayTranheadUnkid As Integer = 0
    'Sohail (16 May 2018) -- End

    'Gajanan (23-May-2018) -- Start
    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
    Private minAdvance_DontAllowAfterDays As Integer = 0
    'Gajanan (23-May-2018) -- End

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private mintAdvance_CostCenterunkid As Integer = -1
    'Sohail (14 Mar 2019) -- End

    'SHANI (27 JUL 2015) -- Start
    'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
    Private mblnDependantDocsAttachmentMandatory As Boolean = False
    Private mblnQualificationCertificateAttachmentMandatory As Boolean = False
    'SHANI (27 JUL 2015) -- End 

    'Pinkal (30-Jul-2015) -- Start
    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
    Private mblnAllowTochangeAttendanceAfterPayment As Boolean = False
    'Pinkal (30-Jul-2015) -- End
    '** Properties *************


    'Pinkal (22-Mar-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
    Private mblnSectorRouteAssignToEmp As Boolean = False
    'Pinkal (22-Mar-2016) -- End


    'Pinkal (16-Apr-2016) -- Start
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Private mstrCompanyDateFormat As String = "dd-MMM-yyyy"
    Private mstrCompanyDateSeparator As String = "-"
    'Pinkal (16-Apr-2016) -- End

    'Nilay (07 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
    Private mblnAllowToExceedTimeAssignedToActivity As Boolean = False
    'Nilay (07 Feb 2017) -- End

    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
    Private mblnAllowOverTimeToEmpTimesheet As Boolean = False
    'Pinkal (21-Oct-2016) -- End

    'Nilay (27 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mblnNotAllowIncompleteTimesheet As Boolean = True
    Private mblnNotAllowClosePeriodIncompleteSubmitApproval As Boolean = True
    'Nilay (27 Feb 2017) -- End

    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mblnDescriptionMandatoryForActivity As Boolean = False
    Private mblnAllowActivityHoursByPercentage As Boolean = True
    'Nilay (21 Mar 2017) -- End

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mintLeaveAccrueTenureSetting As Integer = enLeaveAccrueTenureSetting.Yearly
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 1
    'Pinkal (18-Nov-2016) -- End

    'Shani (23-Nov-2016) -- Start
    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
    Private mblnUseAgreedScore As Boolean = False
    'Shani (23-Nov-2016) -- End


    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.
    Private mstrADIPAddress As String = ""
    Private mstrADDomin As String = ""
    Private mstrADDomainUser As String = ""
    Private mstrADDomainUserPwd As String = ""
    'Pinkal (03-Apr-2017) -- End


    'Pinkal (11-AUG-2017) -- Start
    'Enhancement - Working On B5 Plus Company TnA Enhancements.
    Private mblnIsHolidayConsiderOnWeekend As Boolean = True
    Private mblnIsDayOffConsiderOnWeekend As Boolean = True
    Private mblnIsHolidayConsiderOnDayoff As Boolean = True
    'Pinkal (11-AUG-2017) -- End


    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
    Private mintRestrictEmpForBgTimesheetOnAfterDate As Integer = -1
    Private mintDisableADUserAgainAfterMins As Integer = -1
    'Pinkal (23-AUG-2017) -- End


    ''' <summary>
    ''' Indicate Payslip Voc No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _PayslipVocNoType() As Integer
        Get
            Return mintPayslipVocNoType
        End Get
        Set(ByVal intPayslipVocNoType As Integer)
            mintPayslipVocNoType = intPayslipVocNoType
            setParamVal("PayslipVocNoType", mintPayslipVocNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Payslip Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _PayslipVocPrefix() As String
        Get
            Return mstrPayslipVocPrefix
        End Get
        Set(ByVal strPayslipVocPrefix As String)
            mstrPayslipVocPrefix = strPayslipVocPrefix
            setParamVal("PayslipVocPrefix", mstrPayslipVocPrefix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Payslip Next Voc No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextPayslipVocNo() As Integer
        Get
            Return mintNextPayslipVocNo
        End Get
        Set(ByVal intNextPayslipVocNo As Integer)
            mintNextPayslipVocNo = intNextPayslipVocNo
            setParamVal("NextPayslipVocNo", mintNextPayslipVocNo)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Loan Voc No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _LoanVocNoType() As Integer
        Get
            Return mintLoanVocNoType
        End Get
        Set(ByVal intLoanVocNoType As Integer)
            mintLoanVocNoType = intLoanVocNoType
            setParamVal("LoanVocNoType", mintLoanVocNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Loan Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _LoanVocPrefix() As String
        Get
            Return mstrLoanVocPrefix
        End Get
        Set(ByVal strLoanVocPrefix As String)
            mstrLoanVocPrefix = strLoanVocPrefix
            setParamVal("LoanVocPrefix", mstrLoanVocPrefix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Loan Next Voc No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextLoanVocNo() As Integer
        Get
            Return mintNextLoanVocNo
        End Get
        Set(ByVal intNextLoanVocNo As Integer)
            mintNextLoanVocNo = intNextLoanVocNo
            setParamVal("NextLoanVocNo", mintNextLoanVocNo)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Savings Voc No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _SavingsVocNoType() As Integer
        Get
            Return mintSavingsVocNoType
        End Get
        Set(ByVal intSavingsVocNoType As Integer)
            mintSavingsVocNoType = intSavingsVocNoType
            setParamVal("SavingsVocNoType", mintSavingsVocNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Savings Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _SavingsVocPrefix() As String
        Get
            Return mstrSavingsVocPrefix
        End Get
        Set(ByVal strSavingsVocPrefix As String)
            mstrSavingsVocPrefix = strSavingsVocPrefix
            setParamVal("SavingsVocPrefix", mstrSavingsVocPrefix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Savings Next Voc No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextSavingsVocNo() As Integer
        Get
            Return mintNextSavingsVocNo
        End Get
        Set(ByVal intNextSavingsVocNo As Integer)
            mintNextSavingsVocNo = intNextSavingsVocNo
            setParamVal("NextSavingsVocNo", mintNextSavingsVocNo)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Payment Voc No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _PaymentVocNoType() As Integer
        Get
            Return mintPaymentVocNoType
        End Get
        Set(ByVal intPaymentVocNoType As Integer)
            mintPaymentVocNoType = intPaymentVocNoType
            setParamVal("PaymentVocNoType", mintPaymentVocNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Payment Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _PaymentVocPrefix() As String
        Get
            Return mstrPaymentVocPrefix
        End Get
        Set(ByVal strPaymentVocPrefix As String)
            mstrPaymentVocPrefix = strPaymentVocPrefix
            setParamVal("PaymentVocPrefix", mstrPaymentVocPrefix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Payment Next Voc No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextPaymentVocNo() As Integer
        Get
            Return mintNextPaymentVocNo
        End Get
        Set(ByVal intNextPaymentVocNo As Integer)
            mintNextPaymentVocNo = intNextPaymentVocNo
            setParamVal("NextPaymentVocNo", mintNextPaymentVocNo)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Dispay Days In Grid
    ''' </summary>
    Public Property _DisplayView() As Integer
        Get
            Return mintDisplayView
        End Get
        Set(ByVal intDisplayView As Integer)
            mintDisplayView = intDisplayView
            setParamVal("DisplayView", mintDisplayView)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Last Leave Process Date
    ''' </summary>
    Public Property _LeaveProcessDate() As Date
        Get
            Return eZeeDate.convertDate(mdtLeaveProcessDate.ToString).ToShortDateString
        End Get
        Set(ByVal dtLeaveProcessDate As Date)
            mdtLeaveProcessDate = dtLeaveProcessDate
            setParamVal("LeaveProcessDate", eZeeDate.convertDate(mdtLeaveProcessDate).ToString)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Last Leave Process Date
    ''' </summary>
    Public Property _FileFormat() As Integer
        Get
            Return mintFileFormat
        End Get
        Set(ByVal intFileFormat As Integer)
            mintFileFormat = intFileFormat
            setParamVal("FileFormat", mintFileFormat)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Last Leave Process Date
    ''' </summary>
    Public Property _Separator() As String
        Get
            Return mstrSeparator
        End Get
        Set(ByVal strSeparator As String)
            mstrSeparator = strSeparator
            setParamVal("Separator", mstrSeparator)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Is Identity Required
    ''' </summary>
    Public Property _IsIdSensitive() As Boolean
        Get
            Return mblnIsIdSensitive
        End Get
        Set(ByVal blnIsIdSensitive As Boolean)
            mblnIsIdSensitive = blnIsIdSensitive
            setParamVal("IsIdSensitive", mblnIsIdSensitive)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Loan Application No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _LoanApplicationNoType() As Integer
        Get
            Return mintLoanApplicationNoType
        End Get
        Set(ByVal intLoanApplicationNoType As Integer)
            mintLoanApplicationNoType = intLoanApplicationNoType
            setParamVal("LoanApplicationNoType", mintLoanApplicationNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Loan Application No Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _LoanApplicationPrifix() As String
        Get
            Return mstrLoanApplicationPrifix
        End Get
        Set(ByVal strLoanApplicationPrifix As String)
            mstrLoanApplicationPrifix = strLoanApplicationPrifix
            setParamVal("LoanApplicationPrifix", mstrLoanApplicationPrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Loan Application Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextLoanApplicationNo() As Integer
        Get
            Return mintNextLoanApplicationNo
        End Get
        Set(ByVal intNextLoanApplicationNo As Integer)
            mintNextLoanApplicationNo = intNextLoanApplicationNo
            setParamVal("NextLoanApplicationNo", mintNextLoanApplicationNo)
        End Set
    End Property


    ''' <summary>
    ''' Indicate Leave Form No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _LeaveFormNoType() As Integer
        Get
            Return mintLeaveFormNoType
        End Get
        Set(ByVal intLeaveFormNoType As Integer)
            mintLeaveFormNoType = intLeaveFormNoType
            setParamVal("LeaveFormNoType", mintLeaveFormNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Leave Form No Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _LeaveFormNoPrifix() As String
        Get
            Return mstrLeaveFormNoPrifix
        End Get
        Set(ByVal strLeaveFormNoPrifix As String)
            mstrLeaveFormNoPrifix = strLeaveFormNoPrifix
            setParamVal("LeaveFormNoPrifix", mstrLeaveFormNoPrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Leave Form Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextLeaveFormNo() As Integer
        Get
            Return mintNextLeaveFormNo
        End Get
        Set(ByVal intNextLeaveFormNo As Integer)
            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            'mintNextLeaveFormNo = mintNextLeaveFormNo 
            mintNextLeaveFormNo = intNextLeaveFormNo
            'Sandeep [ 16 Oct 2010 ] -- End 
            setParamVal("NextLeaveFormNo", mintNextLeaveFormNo)
        End Set
    End Property

    'Sohail (28 May 2014) -- Start
    'Enhancement - Staff Requisition.
    ''' <summary>
    ''' Indicate Staff Requisition Form No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _StaffReqFormNoType() As Integer
        Get
            Return mintStaffReqFormNoType
        End Get
        Set(ByVal intStaffReqFormNoType As Integer)
            mintStaffReqFormNoType = intStaffReqFormNoType
            setParamVal("StaffReqFormNoType", mintStaffReqFormNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Staff Requisition Form No Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _StaffReqFormNoPrefix() As String
        Get
            Return mstrStaffReqFormNoPrefix
        End Get
        Set(ByVal strStaffReqFormNoPrefix As String)
            mstrStaffReqFormNoPrefix = strStaffReqFormNoPrefix
            setParamVal("StaffReqFormNoPrefix", mstrStaffReqFormNoPrefix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Staff Requisition Form Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextStaffReqFormNo() As Integer
        Get
            Return mintNextStaffReqFormNo
        End Get
        Set(ByVal intNextStaffReqFormNo As Integer)
            mintNextStaffReqFormNo = intNextStaffReqFormNo
            setParamVal("NextStaffReqFormNo", mintNextStaffReqFormNo)
        End Set
    End Property

    ''' <summary>
    ''' Indicate ApplyStaffRequisition
    ''' </summary>
    Public Property _ApplyStaffRequisition() As Boolean
        Get
            Return mblnApplyStaffRequisition
        End Get
        Set(ByVal blnApplyStaffRequisition As Boolean)
            mblnApplyStaffRequisition = blnApplyStaffRequisition
            setParamVal("ApplyStaffRequisition", mblnApplyStaffRequisition)
        End Set
    End Property
    'Sohail (28 May 2014) -- End

    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 7 : During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs. – TC001 ).
    Public Property _EnforceManpowerPlanning() As Boolean
        Get
            Return mblnEnforceManpowerPlanning
        End Get
        Set(ByVal blnEnforceManpowerPlanning As Boolean)
            mblnEnforceManpowerPlanning = blnEnforceManpowerPlanning
            setParamVal("EnforceManpowerPlanning", mblnEnforceManpowerPlanning)
        End Set
    End Property
    'Hemant (03 Sep 2019) -- End

    ''' <summary>
    ''' Indicate Employee Code No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _EmployeeCodeNotype() As Integer
        Get
            Return mintEmployeeCodeNotype
        End Get
        Set(ByVal intEmployeeCodeNotype As Integer)
            mintEmployeeCodeNotype = intEmployeeCodeNotype
            setParamVal("EmployeeCodeNotype", mintEmployeeCodeNotype)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Employee Code Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _EmployeeCodePrifix() As String
        Get
            Return mstrEmployeeCodePrifix
        End Get
        Set(ByVal strEmployeeCodePrifix As String)
            mstrEmployeeCodePrifix = strEmployeeCodePrifix
            setParamVal("EmployeeCodePrifix", mstrEmployeeCodePrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Employee Code Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextEmployeeCodeNo() As Integer
        Get
            Return mintNextEmployeeCodeNo
        End Get
        Set(ByVal intNextEmployeeCodeNo As Integer)
            mintNextEmployeeCodeNo = intNextEmployeeCodeNo
            setParamVal("NextEmployeeCodeNo", mintNextEmployeeCodeNo)
        End Set
    End Property


    ''' <summary>
    ''' Indicate Applicant Code No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _ApplicantCodeNotype() As Integer
        Get
            Return mintApplicantCodeNoType
        End Get
        Set(ByVal intApplicantCodeNoType As Integer)
            mintApplicantCodeNoType = intApplicantCodeNoType
            setParamVal("ApplicantCodeNoType", mintApplicantCodeNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Applicant Code Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _ApplicantCodePrifix() As String
        Get
            Return mstrApplicantCodePrifix
        End Get
        Set(ByVal strApplicantCodePrifix As String)
            mstrApplicantCodePrifix = strApplicantCodePrifix
            setParamVal("ApplicantCodePrifix", mstrApplicantCodePrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Applicant Code Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextApplicantCodeNo() As Integer
        Get
            Return mintNextApplicantCodeNo
        End Get
        Set(ByVal intNextApplicantCodeNo As Integer)
            mintNextApplicantCodeNo = intNextApplicantCodeNo
            setParamVal("NextApplicantCodeNo", mintNextApplicantCodeNo)
        End Set
    End Property




    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.

    Private mintLeaveCancelFormNotype As Integer = 0
    Private mstrLeaveCancelFormNoPrifix As String = ""
    Private mintNextLeaveCancelFormNo As Integer = 1

    ''' <summary>
    ''' Indicate Leave Cancel Form No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _LeaveCancelFormNotype() As Integer
        Get
            Return mintLeaveCancelFormNotype
        End Get
        Set(ByVal value As Integer)
            mintLeaveCancelFormNotype = value
            setParamVal("LeaveCancelFormNoType", mintLeaveCancelFormNotype)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Leave Cancel Form No Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _LeaveCancelFormNoPrifix() As String
        Get
            Return mstrLeaveCancelFormNoPrifix
        End Get
        Set(ByVal value As String)
            mstrLeaveCancelFormNoPrifix = value
            setParamVal("LeaveCancelFormNoPrifix", mstrLeaveCancelFormNoPrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Leave Cancel Form Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextLeaveCancelFormNo() As Integer
        Get
            Return mintNextLeaveCancelFormNo
        End Get
        Set(ByVal value As Integer)
            mintNextLeaveCancelFormNo = value
            setParamVal("NextLeaveCancelFormNo", mintNextLeaveCancelFormNo)
        End Set
    End Property

    'Pinkal (03-May-2019) -- End



    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.

    Private mintClaimRetirementFormNoType As Integer = 1
    Public Property _ClaimRetirementFormNoType() As Integer
        Get
            Return mintClaimRetirementFormNoType
        End Get
        Set(ByVal value As Integer)
            mintClaimRetirementFormNoType = value
            setParamVal("ClaimRetirementFormNoType", mintClaimRetirementFormNoType)
        End Set
    End Property

    Private mstrClaimRetirementFormNoPrifix As String = ""
    Public Property _ClaimRetirementFormNoPrifix() As String
        Get
            Return mstrClaimRetirementFormNoPrifix
        End Get
        Set(ByVal value As String)
            mstrClaimRetirementFormNoPrifix = value
            setParamVal("ClaimRetirementFormNoPrifix", mstrClaimRetirementFormNoPrifix)
        End Set
    End Property

    Private mintNextClaimRetirementFormNo As Integer = 1
    Public Property _NextClaimRetirementFormNo() As Integer
        Get
            Return mintNextClaimRetirementFormNo
        End Get
        Set(ByVal value As Integer)
            mintNextClaimRetirementFormNo = value
            setParamVal("NextClaimRetirementFormNo", mintNextClaimRetirementFormNo)
        End Set
    End Property

    'Pinkal (11-Sep-2019) -- End




    ''' <summary>
    ''' Indicate Retirement By Option
    ''' This is for ByAge Or ByYear
    ''' </summary>
    Public Property _RetirementBy() As Integer
        Get
            Return mintRetirementBy
        End Get
        Set(ByVal intRetirementBy As Integer)
            mintRetirementBy = intRetirementBy
            setParamVal("RetirementBy", mintRetirementBy)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Retirement By Value
    ''' This is for ByAge Or ByYear
    ''' </summary>
    Public Property _RetirementValue() As Integer
        Get
            Return mintRetirementValue
        End Get
        Set(ByVal intRetirementValue As Integer)
            mintRetirementValue = intRetirementValue
            setParamVal("RetirementValue", mintRetirementValue)
        End Set
    End Property

    'S.SANDEEP [ 27 AUG 2014 ] -- START
    Private mintFRetirementBy As Integer = 1
    Private mintFRetirementValue As Integer = 55

    Public Property _FRetirementBy() As Integer
        Get
            Return mintFRetirementBy
        End Get
        Set(ByVal value As Integer)
            mintFRetirementBy = value
            setParamVal("FRetirementBy", mintFRetirementBy)
        End Set
    End Property

    Public Property _FRetirementValue() As Integer
        Get
            Return mintFRetirementValue
        End Get
        Set(ByVal value As Integer)
            mintFRetirementValue = value
            setParamVal("FRetirementValue", mintFRetirementValue)
        End Set
    End Property
    'S.SANDEEP [ 27 AUG 2014 ] -- END

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    Private mintTrainingNeedFormNoType As Integer = 1
    Public Property _TrainingNeedFormNoType() As Integer
        Get
            Return mintTrainingNeedFormNoType
        End Get
        Set(ByVal value As Integer)
            mintTrainingNeedFormNoType = value
            setParamVal("TrainingNeedFormNoType", mintTrainingNeedFormNoType)
        End Set
    End Property

    Private mstrTrainingNeedFormNoPrefix As String = ""
    Public Property _TrainingNeedFormNoPrefix() As String
        Get
            Return mstrTrainingNeedFormNoPrefix
        End Get
        Set(ByVal value As String)
            mstrTrainingNeedFormNoPrefix = value
            setParamVal("TrainingNeedFormNoPrefix", mstrTrainingNeedFormNoPrefix)
        End Set
    End Property

    Private mintNextTrainingNeedFormNo As Integer = 1
    Public Property _NextTrainingNeedFormNo() As Integer
        Get
            Return mintNextTrainingNeedFormNo
        End Get
        Set(ByVal value As Integer)
            mintNextTrainingNeedFormNo = value
            setParamVal("NextTrainingNeedFormNo", mintNextTrainingNeedFormNo)
        End Set
    End Property
    'Sohail (14 Nov 2019) -- End


    ''' <summary>
    ''' Indicates First Name first Or Surname should come First
    ''' </summary>
    Public Property _FirstNamethenSurname() As Boolean
        Get
            Return mblnFirstNamethenSurname
        End Get
        Set(ByVal blnFirstNamethenSurname As Boolean)
            mblnFirstNamethenSurname = blnFirstNamethenSurname
            setParamVal("FirstNamethenSurname", mblnFirstNamethenSurname)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Weekend Days
    ''' </summary>
    Public Property _WeekendDays() As String
        Get
            Return mstrWeekendDays
        End Get
        Set(ByVal strWeekendDays As String)
            mstrWeekendDays = strWeekendDays
            setParamVal("WeekendDays", mstrWeekendDays)
        End Set
    End Property

    'Sandeep [ 16 Oct 2010 ] -- Start
    ''' <summary>
    ''' Returns Server Date
    ''' </summary>
    Public ReadOnly Property _CurrentDateAndTime() As Date
        Get
            Return GetCurrentDateAndTime()
        End Get
    End Property
    'Sandeep [ 16 Oct 2010 ] -- End 

    'Anjan (21 Dec 2010)-Start
    ''' <Summary>
    ''' Returns Round Off Type
    ''' </Summary>
    Public Property _RoundOff_Type() As Double
        Get
            Return mdblRoundOffType
        End Get
        Set(ByVal dblRoundoffType As Double)
            mdblRoundOffType = dblRoundoffType
            setParamVal("RoundOffType", mdblRoundOffType)
        End Set
    End Property

    '''<Summary>
    ''' Returns Manual Round Off Limit
    '''</Summary>
    Public Property _ManualRoundOff_Limit() As Double
        Get
            Return mdblManualRoundOffLimit
        End Get
        Set(ByVal dblManualRoundOffLimit As Double)
            mdblManualRoundOffLimit = dblManualRoundOffLimit
            setParamVal("ManualRoundOffLimit", mdblManualRoundOffLimit)
        End Set
    End Property

    'Anjan (21 Dec 2010)-End

    'Sohail (22 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _PaymentRoundingType() As Integer
        Get
            Return mintPaymentRoundingType
        End Get
        Set(ByVal intPaymentRoundingType As Integer)
            mintPaymentRoundingType = intPaymentRoundingType
            setParamVal("PaymentRoundingType", mintPaymentRoundingType)
        End Set
    End Property

    Public Property _PaymentRoundingMultiple() As Integer
        Get
            Return mintPaymentRoundingMultiple
        End Get
        Set(ByVal intPaymentRoundingMultiple As Integer)
            mintPaymentRoundingMultiple = intPaymentRoundingMultiple
            setParamVal("PaymentRoundingMultiple", mintPaymentRoundingMultiple)
        End Set
    End Property
    'Sohail (22 Oct 2013) -- End

    'Sohail (21 Mar 2014) -- Start
    'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
    Public Property _CFRoundingAbove() As Decimal
        Get
            Return mdecCFRoundingAbove
        End Get
        Set(ByVal decCFRoundingAbove As Decimal)
            mdecCFRoundingAbove = decCFRoundingAbove
            setParamVal("CFRoundingAbove", mdecCFRoundingAbove)
        End Set
    End Property
    'Sohail (21 Mar 2014) -- End

'Sandeep [ 17 DEC 2010 ] -- Start
    ''' <summary>
    ''' Indicates Whether Denomination Compulsory
    ''' </summary>
    Public Property _IsDenominationCompulsory() As Boolean
        Get
            Return mblnIsDenominationCompulsory
        End Get
        Set(ByVal blnIsDenominationCompulsory As Boolean)
            mblnIsDenominationCompulsory = blnIsDenominationCompulsory
            setParamVal("IsDenominationCompulsory", mblnIsDenominationCompulsory)
        End Set
    End Property
    'Sandeep [ 17 DEC 2010 ] -- End 

    'Sandeep [ 10 FEB 2011 ] -- Start
    Public Property _IsDailyBackupEnabled() As Boolean
        Get
            Return mblnIsDailyBackupEnabled
        End Get
        Set(ByVal blnIsDailyBackupEnabled As Boolean)
            mblnIsDailyBackupEnabled = blnIsDailyBackupEnabled
            setParamVal("IsDailyBackupEnabled", mblnIsDailyBackupEnabled)
        End Set
    End Property

    Public Property _BackupTime() As Date
        Get
            Return mdtBackupTime
        End Get
        Set(ByVal dtBackupTime As Date)
            mdtBackupTime = dtBackupTime
            setParamVal("BackupTime", Format(mdtBackupTime, "HH:mm:ss"))
        End Set
    End Property
    'Sandeep [ 10 FEB 2011 ] -- End 



    'Sandeep [ 01 MARCH 2011 ] -- Start
    Public Property _Base_CurrencyId() As Integer
        Get
            Return mintBaseCurrencyId
        End Get
        Set(ByVal intBaseCurrencyId As Integer)
            mintBaseCurrencyId = intBaseCurrencyId
            setParamVal("BaseCurrencyId", mintBaseCurrencyId)
        End Set
    End Property

    Public Property _CurrencyFormat() As String
        Get
            Return mstrCurrencyFormat
        End Get
        Set(ByVal strCurrencyFormat As String)
            mstrCurrencyFormat = strCurrencyFormat
            setParamVal("CurrencyFormat", mstrCurrencyFormat)
        End Set
    End Property
    'Sandeep [ 01 MARCH 2011 ] -- End 

    
    'S.SANDEEP [ 29 JUNE 2011 ] -- START
    'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    Public Property _IsIncludeInactiveEmp() As Boolean
        Get
            Return mblnIncludeInactiveEmployee
        End Get
        Set(ByVal blnIncludeInactiveEmployee As Boolean)
            mblnIncludeInactiveEmployee = blnIncludeInactiveEmployee
            setParamVal("IncludeInactiveEmployee", mblnIncludeInactiveEmployee)
        End Set
    End Property
    'S.SANDEEP [ 29 JUNE 2011 ] -- END 


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Indicate Sick Sheet No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _SickSheetNotype() As Integer
        Get
            Return mintSickSheetNoType
        End Get
        Set(ByVal intSickSheetNoType As Integer)
            mintSickSheetNoType = intSickSheetNoType
            setParamVal("SickSheetNoType", mintSickSheetNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate  Sick Sheet Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _SickSheetPrifix() As String
        Get
            Return mstrSickSheetPrifix
        End Get
        Set(ByVal strSickSheetPrifix As String)
            mstrSickSheetPrifix = strSickSheetPrifix
            setParamVal("SickSheetPrefix", mstrSickSheetPrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Sick Sheet Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextSickSheetNo() As Integer
        Get
            Return mintNextSickSheetNo
        End Get
        Set(ByVal intNextSickSheetNo As Integer)
            mintNextSickSheetNo = intNextSickSheetNo
            setParamVal("NextSickSheetNo", mintNextSickSheetNo)
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End

    'Sohail (19 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Indicate Batch Posting No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _BatchPostingNotype() As Integer
        Get
            Return mintBatchPostingNoType
        End Get
        Set(ByVal intBatchPostingNoType As Integer)
            mintBatchPostingNoType = intBatchPostingNoType
            setParamVal("BatchPostingNoType", mintBatchPostingNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate  Batch Posting Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _BatchPostingPrifix() As String
        Get
            Return mstrBatchPostingPrifix
        End Get
        Set(ByVal strBatchPostingPrifix As String)
            mstrBatchPostingPrifix = strBatchPostingPrifix
            setParamVal("BatchPostingPrefix", mstrBatchPostingPrifix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Batch Posting Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextBatchPostingNo() As Integer
        Get
            Return mintNextBatchPostingNo
        End Get
        Set(ByVal intNextBatchPostingNo As Integer)
            mintNextBatchPostingNo = intNextBatchPostingNo
            setParamVal("NextBatchPostingNo", mintNextBatchPostingNo)
        End Set
    End Property
    'Sohail (19 Dec 2012) -- End

'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property _IsDependant_AgeLimit_Set() As Boolean
        Get
            Return mblnIsDependantAgeLimitMandatory
        End Get
        Set(ByVal blnIsDependantAgeLimitMandatory As Boolean)
            mblnIsDependantAgeLimitMandatory = blnIsDependantAgeLimitMandatory
            setParamVal("IsDependantAgeLimitMandatory", mblnIsDependantAgeLimitMandatory)
        End Set
    End Property
    'S.SANDEEP [ 07 NOV 2011 ] -- END



    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    Public Property _IsLeaveApprover_ForLeaveType() As Boolean
        Get
            Return mblnIsLvApproverMandatoryForLeaveType
        End Get
        Set(ByVal IsLvApproverMandatoryForLeaveType As Boolean)
            mblnIsLvApproverMandatoryForLeaveType = IsLvApproverMandatoryForLeaveType
            setParamVal("IsLvApproverMandatoryForLeaveType", mblnIsLvApproverMandatoryForLeaveType)
        End Set
    End Property
    'Pinkal (06-Feb-2012) -- End

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _AssetDeclarationInstruction() As String
        Get
            Return mstrAssetDeclarationInstruction

        End Get
        Set(ByVal strAssetDeclarationInstruction As String)
            mstrAssetDeclarationInstruction = strAssetDeclarationInstruction
            setParamVal("AssetDeclarationInstruction", mstrAssetDeclarationInstruction)
        End Set
    End Property
    'Sohail (06 Apr 2012) -- End

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintBasicSalaryComputation As Integer = enBasicSalaryComputation.WITH_WEEKENDS_INCLUDED
    Public Property _BasicSalaryComputation() As Integer
        Get
            Return mintBasicSalaryComputation
        End Get
        Set(ByVal intBasicSalaryComputation As Integer)
            mintBasicSalaryComputation = intBasicSalaryComputation
            setParamVal("BasicSalaryComputation", mintBasicSalaryComputation)
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'Sohail (08 Feb 2022) -- Start
    'Enhancement : OLD-548 : Give Option to apply Basic Salary Computation Setting on Flat Rate Heads.
    Private mintFlatRateHeadsComputation As Integer = enBasicSalaryComputation.WITH_WEEKENDS_INCLUDED
    Public Property _FlatRateHeadsComputation() As Integer
        Get
            Return mintFlatRateHeadsComputation
        End Get
        Set(ByVal intFlatRateHeadsComputation As Integer)
            mintFlatRateHeadsComputation = intFlatRateHeadsComputation
            setParamVal("FlatRateHeadsComputation", mintFlatRateHeadsComputation)
        End Set
    End Property
    'Sohail ((08 Feb 2022) -- End

    'Sohail (06 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintDatabaseServerSetting As Integer = enDatabaseServerSetting.DEFAULT_SETTING
    Public Property _DatabaseServerSetting() As Integer
        Get
            Return mintDatabaseServerSetting
        End Get
        Set(ByVal intDatabaseServerSetting As Integer)
            mintDatabaseServerSetting = intDatabaseServerSetting
            setParamVal("DatabaseServerSetting", mintDatabaseServerSetting)
        End Set
    End Property

    Public Property _DatabaseServer() As String
        Get
            Return mstrDatabaseServer

        End Get
        Set(ByVal strDatabaseServer As String)
            mstrDatabaseServer = strDatabaseServer
            setParamVal("DatabaseServer", mstrDatabaseServer)
        End Set
    End Property

    Public Property _DatabaseName() As String
        Get
            Return mstrDatabaseName

        End Get
        Set(ByVal strDatabaseName As String)
            mstrDatabaseName = strDatabaseName
            setParamVal("DatabaseName", mstrDatabaseName)
        End Set
    End Property

    Public Property _DatabaseUserName() As String
        Get
            Return mstrDatabaseUserName

        End Get
        Set(ByVal strDatabaseUserName As String)
            mstrDatabaseUserName = strDatabaseUserName
            setParamVal("DatabaseUserName", mstrDatabaseUserName)
        End Set
    End Property

    Public Property _DatabaseUserPassword() As String
        Get
            Return mstrDatabaseUserPassword

        End Get
        Set(ByVal strDatabaseUserPassword As String)
            mstrDatabaseUserPassword = strDatabaseUserPassword
            setParamVal("DatabaseUserPassword", mstrDatabaseUserPassword)
        End Set
    End Property

    Public Property _DatabaseOwner() As String
        Get
            Return mstrDatabaseOwner

        End Get
        Set(ByVal strDatabaseOwner As String)
            mstrDatabaseOwner = strDatabaseOwner
            setParamVal("DatabaseOwner", mstrDatabaseOwner)
        End Set
    End Property
    'Sohail (06 Jun 2012) -- End

    'Sohail (16 Nov 2018) -- Start
    'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
    Public Property _RecruitmentCareerLink() As String
        Get
            Return mstrRecruitmentCareerLink

        End Get
        Set(ByVal strRecruitmentCareerLink As String)
            mstrRecruitmentCareerLink = strRecruitmentCareerLink
            setParamVal("RecruitmentCareerLink", mstrRecruitmentCareerLink)
        End Set
    End Property

    Public Property _RecruitmentWebServiceLink() As String
        Get
            Return mstrRecruitmentWebServiceLink

        End Get
        Set(ByVal strRecruitmentWebServiceLink As String)
            mstrRecruitmentWebServiceLink = strRecruitmentWebServiceLink
            setParamVal("RecruitmentWebServiceLink", mstrRecruitmentWebServiceLink)
        End Set
    End Property
    'Sohail (16 Nov 2018) -- End

    'Sohail (29 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _DoNotAllowOverDeductionForPayment() As Boolean
        Get
            Return mblnDoNotAllowOverDeductionPayment
        End Get
        Set(ByVal blnDoNotAllowOverDeductionPayment As Boolean)
            mblnDoNotAllowOverDeductionPayment = blnDoNotAllowOverDeductionPayment
            setParamVal("DoNotAllowOverDeductionPayment", mblnDoNotAllowOverDeductionPayment)
        End Set
    End Property
    'Sohail (29 Jun 2012) -- End

    'Sohail (01 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _ArutiSelfServiceURL() As String
        Get
            Return mstrArutiSelfServiceURL

        End Get
        Set(ByVal strArutiSelfServiceURL As String)
            mstrArutiSelfServiceURL = strArutiSelfServiceURL
            setParamVal("ArutiSelfServiceURL", mstrArutiSelfServiceURL)
        End Set
    End Property
    'Sohail (01 Jan 2013) -- End



    'Anjan [ 20 Feb 2013 ] -- Start
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Indicates First Name first Or Surname should come First
    ''' </summary>
    Public Property _SetPayslipPaymentApproval() As Boolean
        Get
            Return mblnSetPaySlipPayApproval
        End Get
        Set(ByVal blnSetPaySlipPayApproval As Boolean)
            mblnSetPaySlipPayApproval = blnSetPaySlipPayApproval
            setParamVal("SetPaySlipPayApproval", mblnSetPaySlipPayApproval)
        End Set
    End Property
    'Anjan [ 20 Feb 2013 ] -- End

    'Sohail (21 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _SetPaySlipPaymentMandatory() As Boolean
        Get
            Return mblnSetPaySlipPaymentMandatory
        End Get
        Set(ByVal blnSetPaySlipPaymentMandatory As Boolean)
            mblnSetPaySlipPaymentMandatory = blnSetPaySlipPaymentMandatory
            setParamVal("SetPaySlipPaymentMandatory", mblnSetPaySlipPaymentMandatory)
        End Set
    End Property
    'Sohail (21 Mar 2013) -- End

    'Sohail (20 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _ApplyPayPerActivity() As Boolean
        Get
            Return mblnApplyPayPerActivity
        End Get
        Set(ByVal blnApplyPayPerActivity As Boolean)
            mblnApplyPayPerActivity = blnApplyPayPerActivity
            setParamVal("ApplyPayPerActivity", mblnApplyPayPerActivity)
        End Set
    End Property
    'Sohail (20 Jul 2013) -- End

    'Sohail (04 Aug 2016) -- Start
    'Enhancement - 63.1 - Option on configuration to Send Password protected E-Payslip.
    Public Property _SendPasswordProtectedEPayslip() As Boolean
        Get
            Return mblnSendPasswordProtectedEPayslip
        End Get
        Set(ByVal blnSendPasswordProtectedEPayslip As Boolean)
            mblnSendPasswordProtectedEPayslip = blnSendPasswordProtectedEPayslip
            setParamVal("SendPasswordProtectedEPayslip", mblnSendPasswordProtectedEPayslip)
        End Set
    End Property
    'Sohail (04 Aug 2016) -- End

    'Sohail (23 Nov 2020) -- Start
    'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
    Public Property _IgnoreNegativeNetPayEmployeesOnJV() As Boolean
        Get
            Return mblnIgnoreNegativeNetPayEmployeesOnJV
        End Get
        Set(ByVal blnIgnoreNegativeNetPayEmployeesOnJV As Boolean)
            mblnIgnoreNegativeNetPayEmployeesOnJV = blnIgnoreNegativeNetPayEmployeesOnJV
            setParamVal("IgnoreNegativeNetPayEmployeesOnJV", mblnIgnoreNegativeNetPayEmployeesOnJV)
        End Set
    End Property
    'Sohail (23 Nov 2020) -- End

    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _LeaveBalanceSetting() As Integer
        Get
            Return mintLeaveBalanceSetting

        End Get
        Set(ByVal intLeaveBalanceSetting As Integer)
            mintLeaveBalanceSetting = intLeaveBalanceSetting
            setParamVal("LeaveBalanceSetting", mintLeaveBalanceSetting)
        End Set
    End Property

    Public Property _ELCmonths() As Integer
        Get
            Return mintELCMonths

        End Get
        Set(ByVal intELCMonths As Integer)
            mintELCMonths = intELCMonths
            setParamVal("ELCMonths", mintELCMonths)
        End Set
    End Property

    'Pinkal (06-Feb-2013) -- End

    'Sohail (29 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mblnShowFirstAppointmentDate As Boolean = False
    Public Property _ShowFirstAppointmentDate() As Boolean
        Get
            Return mblnShowFirstAppointmentDate
        End Get
        Set(ByVal blnShowFirstAppointmentDate As Boolean)
            mblnShowFirstAppointmentDate = blnShowFirstAppointmentDate
            setParamVal("ShowFirstAppointmentDate", mblnShowFirstAppointmentDate)
        End Set
    End Property
    'Sohail (29 Mar 2013) -- End


    'Pinkal (15-Sep-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _ClosePayrollPeriodIfLeaveIssue() As Boolean
        Get
            Return mblnClosePayrollPeriodIfLeaveIssue
        End Get
        Set(ByVal value As Boolean)
            mblnClosePayrollPeriodIfLeaveIssue = value
            setParamVal("ClosePayrollPeriodIfLeaveIssue", mblnClosePayrollPeriodIfLeaveIssue)
        End Set
    End Property

    'Pinkal (15-Sep-2013) -- End


    'Pinkal (20-Sep-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _PolicyManagementTNA() As Boolean
        Get
            Return mblnPolicyManagementTNA
        End Get
        Set(ByVal value As Boolean)
            mblnPolicyManagementTNA = value
            setParamVal("PolicyManagementTNA", mblnPolicyManagementTNA)
        End Set
    End Property

    'Pinkal (20-Sep-2013) -- End


    'Pinkal (15-Nov-2013) -- Start
    'Enhancement : Oman Changes

    Public Property _DonotAttendanceinSeconds() As Boolean
        Get
            Return mblnDonotAttendanceinSeconds
        End Get
        Set(ByVal value As Boolean)
            mblnDonotAttendanceinSeconds = value
            setParamVal("DonotAttendanceinSeconds", mblnDonotAttendanceinSeconds)
        End Set
    End Property

    'Pinkal (15-Nov-2013) -- End



    'Pinkal (30-Nov-2013) -- Start
    'Enhancement : Oman Changes

    Public Property _OT1Order() As String
        Get
            Return mstrOT1Order
        End Get
        Set(ByVal value As String)
            mstrOT1Order = value
            setParamVal("OT1_Order", mstrOT1Order)
        End Set
    End Property

    Public Property _OT2Order() As String
        Get
            Return mstrOT2Order
        End Get
        Set(ByVal value As String)
            mstrOT2Order = value
            setParamVal("OT2_Order", mstrOT2Order)
        End Set
    End Property

    Public Property _OT3Order() As String
        Get
            Return mstrOT3Order
        End Get
        Set(ByVal value As String)
            mstrOT3Order = value
            setParamVal("OT3_Order", mstrOT3Order)
        End Set
    End Property

    Public Property _OT4Order() As String
        Get
            Return mstrOT4Order
        End Get
        Set(ByVal value As String)
            mstrOT4Order = value
            setParamVal("OT4_Order", mstrOT4Order)
        End Set
    End Property

    'Pinkal (30-Nov-2013) -- End


    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes

    Public Property _IsSeparateTnAPeriod() As Boolean
        Get
            Return mblnIsSeparateTnAPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnIsSeparateTnAPeriod = value
            setParamVal("IsSeparateTnAPeriod", mblnIsSeparateTnAPeriod)
        End Set
    End Property

    'Pinkal (03-Jan-2014) -- End


    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes
    Public Property _IsAutomaticIssueOnFinalApproval() As Boolean
        Get
            Return mblnIsAutomaticLeaveIssueOnFinalApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsAutomaticLeaveIssueOnFinalApproval = value
            setParamVal("IsAutomaticLeaveIssueOnFinalApproval", mblnIsAutomaticLeaveIssueOnFinalApproval)
        End Set
    End Property

    'Pinkal (01-Feb-2014) -- End


    'Anjan [06 April 2015] -- Start
    'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
    Public Property _IsImportPeoplesoftData() As Boolean
        Get
            Return mblnImportPeoplesoftData
        End Get
        Set(ByVal blnImportPeoplesoftData As Boolean)
            mblnImportPeoplesoftData = blnImportPeoplesoftData
            setParamVal("ImportPeoplesoftData", mblnImportPeoplesoftData)
        End Set
    End Property
    'Anjan [06 April 2015] -- End

    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

    Public Property _IsLoanApprover_ForLoanScheme() As Boolean
        Get
            Return mblnIsLoanApproverMandatoryForLoanScheme
        End Get
        Set(ByVal IsLoanApproverMandatoryForLoanScheme As Boolean)
            mblnIsLoanApproverMandatoryForLoanScheme = IsLoanApproverMandatoryForLoanScheme
            setParamVal("IsLoanApproverMandatoryForLoanScheme", mblnIsLoanApproverMandatoryForLoanScheme)
        End Set
    End Property

    'Pinkal (14-Apr-2015) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Public Property _IsSendLoanEmailFromDesktopMSS() As Boolean
        Get
            Return mblnIsSendLoanEmailFromDesktopMSS
        End Get
        Set(ByVal IsSendLoanEmailFromDesktopMSS As Boolean)
            mblnIsSendLoanEmailFromDesktopMSS = IsSendLoanEmailFromDesktopMSS
            setParamVal("IsSendLoanEmailFromDesktopMSS", mblnIsSendLoanEmailFromDesktopMSS)
        End Set
    End Property

    Public Property _IsSendLoanEmailFromESS() As Boolean
        Get
            Return mblnIsSendLoanEmailFromESS
        End Get
        Set(ByVal IsSendLoanEmailFromESS As Boolean)
            mblnIsSendLoanEmailFromESS = IsSendLoanEmailFromESS
            setParamVal("IsSendLoanEmailFromESS", mblnIsSendLoanEmailFromESS)
        End Set
    End Property
    'Nilay (10-Dec-2016) -- End

    'Sohail (16 May 2018) -- Start
    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
    Public Property _Advance_NetPayPercentage() As Decimal
        Get
            Return mdecAdvance_NetPayPercentage
        End Get
        Set(ByVal Advance_NetPayPercentage As Decimal)
            mdecAdvance_NetPayPercentage = Advance_NetPayPercentage
            setParamVal("Advance_NetPayPercentage", mdecAdvance_NetPayPercentage)
        End Set
    End Property

    Public Property _Advance_NetPayTranheadUnkid() As Integer
        Get
            Return mintAdvance_NetPayTranheadUnkid
        End Get
        Set(ByVal Advance_NetPayTranheadUnkid As Integer)
            mintAdvance_NetPayTranheadUnkid = Advance_NetPayTranheadUnkid
            setParamVal("Advance_NetPayTranheadUnkid", mintAdvance_NetPayTranheadUnkid)
        End Set
    End Property
    'Sohail (16 May 2018) -- End

    'Gajanan (23-May-2018) -- Start
    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
    Public Property _Advance_DontAllowAfterDays() As Integer
        Get
            Return minAdvance_DontAllowAfterDays
        End Get
        Set(ByVal Advance_DontAllowAfterDays As Integer)
            minAdvance_DontAllowAfterDays = Advance_DontAllowAfterDays
            setParamVal("Advance_DontAllowAfterDays", minAdvance_DontAllowAfterDays)
        End Set
    End Property
    'Gajanan (23-May-2018) -- End   

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Public Property _Advance_CostCenterunkid() As Integer
        Get
            Return mintAdvance_CostCenterunkid
        End Get
        Set(ByVal value As Integer)
            mintAdvance_CostCenterunkid = value
            setParamVal("Advance_CostCenterunkid", mintAdvance_CostCenterunkid)
        End Set
    End Property
    'Sohail (14 Mar 2019) -- End

    'SHANI (27 JUL 2015) -- Start
    'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
    Public Property _DependantDocsAttachmentMandatory() As Boolean
        Get
            Return mblnDependantDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnDependantDocsAttachmentMandatory = value
            setParamVal("DependantDocsAttachmentMandatory", mblnDependantDocsAttachmentMandatory)
        End Set
    End Property

    Public Property _QualificationCertificateAttachmentMandatory() As Boolean
        Get
            Return mblnQualificationCertificateAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnQualificationCertificateAttachmentMandatory = value
            setParamVal("QualificationCertificateAttachmentMandatory", mblnQualificationCertificateAttachmentMandatory)
        End Set
    End Property
    'SHANI (27 JUL 2015) -- End 

    'Pinkal (30-Jul-2015) -- Start
    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)

    Public Property _AllowToChangeAttendanceAfterPayment() As Boolean
        Get
            Return mblnAllowTochangeAttendanceAfterPayment
        End Get
        Set(ByVal value As Boolean)
            mblnAllowTochangeAttendanceAfterPayment = value
            setParamVal("AllowTochangeAttendanceAfterPayment", mblnAllowTochangeAttendanceAfterPayment)
        End Set
    End Property

    'Pinkal (30-Jul-2015) -- End



    'Pinkal (22-Mar-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
    Public Property _SectorRouteAssignToEmp() As Boolean
        Get
            Return mblnSectorRouteAssignToEmp
        End Get
        Set(ByVal value As Boolean)
            mblnSectorRouteAssignToEmp = value
            setParamVal("SectorRouteAssignToEmps", mblnSectorRouteAssignToEmp)
        End Set
    End Property
    'Pinkal (22-Mar-2016) -- End


    'Pinkal (20-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private mblnSectorRouteAssignToExpense As Boolean = False
    Public Property _SectorRouteAssignToExpense() As Boolean
        Get
            Return mblnSectorRouteAssignToExpense
        End Get
        Set(ByVal value As Boolean)
            mblnSectorRouteAssignToExpense = value
            setParamVal("SectorRouteAssignToExpense", mblnSectorRouteAssignToExpense)
        End Set
    End Property
    'Pinkal (20-Feb-2019) -- End



    'Pinkal (16-Apr-2016) -- Start
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Public Property _CompanyDateFormat() As String
        Get
            Return mstrCompanyDateFormat
        End Get
        Set(ByVal value As String)
            mstrCompanyDateFormat = value
            setParamVal("CompanyDateFormat", mstrCompanyDateFormat)
        End Set
    End Property

    Public Property _CompanyDateSeparator() As String
        Get
            Return mstrCompanyDateSeparator
        End Get
        Set(ByVal value As String)
            mstrCompanyDateSeparator = value
            setParamVal("CompanyDateSeparator", mstrCompanyDateSeparator)
        End Set
    End Property


    'Pinkal (16-Apr-2016) -- End

    'Nilay (07 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
    Public Property _AllowToExceedTimeAssignedToActivity() As Boolean
        Get
            Return mblnAllowToExceedTimeAssignedToActivity
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToExceedTimeAssignedToActivity = value
            setParamVal("AllowToExceedTimeAssignedToActivity", mblnAllowToExceedTimeAssignedToActivity)
        End Set
    End Property
    'Nilay (07 Feb 2017) -- End

    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.

    Public Property _AllowOverTimeToEmpTimesheet() As Boolean
        Get
            Return mblnAllowOverTimeToEmpTimesheet
        End Get
        Set(ByVal value As Boolean)
            mblnAllowOverTimeToEmpTimesheet = value
            setParamVal("AllowOverTimeToEmpTimesheet", mblnAllowOverTimeToEmpTimesheet)
        End Set
    End Property
    'Pinkal (21-Oct-2016) -- End

    'Nilay (27 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Public Property _NotAllowIncompleteTimesheet() As Boolean
        Get
            Return mblnNotAllowIncompleteTimesheet
        End Get
        Set(ByVal value As Boolean)
            mblnNotAllowIncompleteTimesheet = value
            setParamVal("NotAllowIncompleteTimesheet", mblnNotAllowIncompleteTimesheet)
        End Set
    End Property

    Public Property _NotAllowClosePeriodIncompleteSubmitApproval() As Boolean
        Get
            Return mblnNotAllowClosePeriodIncompleteSubmitApproval
        End Get
        Set(ByVal value As Boolean)
            mblnNotAllowClosePeriodIncompleteSubmitApproval = value
            setParamVal("NotAllowClosePeriodIncompleteSubmitApproval", mblnNotAllowClosePeriodIncompleteSubmitApproval)
        End Set
    End Property
    'Nilay (27 Feb 2017) -- End

    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Public Property _DescriptionMandatoryForActivity() As Boolean
        Get
            Return mblnDescriptionMandatoryForActivity
        End Get
        Set(ByVal value As Boolean)
            mblnDescriptionMandatoryForActivity = value
            setParamVal("DescriptionMandatoryForActivity", mblnDescriptionMandatoryForActivity)
        End Set
    End Property

    Public Property _AllowActivityHoursByPercentage() As Boolean
        Get
            Return mblnAllowActivityHoursByPercentage
        End Get
        Set(ByVal value As Boolean)
            mblnAllowActivityHoursByPercentage = value
            setParamVal("AllowActivityHoursByPercentage", mblnAllowActivityHoursByPercentage)
        End Set
    End Property
    'Nilay (21 Mar 2017) -- End


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Public Property _LeaveAccrueTenureSetting() As Integer
        Get
            Return mintLeaveAccrueTenureSetting
        End Get
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
            setParamVal("LeaveAccrueTenureSetting", mintLeaveAccrueTenureSetting)
        End Set
    End Property

    Public Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Get
            Return mintLeaveAccrueDaysAfterEachMonth
        End Get
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
            setParamVal("LeaveAccrueDaysAfterEachMonth", mintLeaveAccrueDaysAfterEachMonth)
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- End

    'Shani (23-Nov-2016) -- Start
    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
    Public Property _IsUseAgreedScore() As Boolean
        Get
            Return mblnUseAgreedScore
        End Get
        Set(ByVal value As Boolean)
            mblnUseAgreedScore = value
            setParamVal("UseAgreedScore", mblnUseAgreedScore)
        End Set
    End Property
    'Shani (23-Nov-2016) -- End


    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.

    Public Property _ADIPAddress() As String
        Get
            Return mstrADIPAddress
        End Get
        Set(ByVal value As String)
            mstrADIPAddress = value
            setParamVal("ADIPAddress", mstrADIPAddress)
        End Set
    End Property

    Public Property _ADDomain() As String
        Get
            Return mstrADDomin
        End Get
        Set(ByVal value As String)
            mstrADDomin = value
            setParamVal("ADDomain", mstrADDomin)
        End Set
    End Property

    Public Property _ADDomainUser() As String
        Get
            Return mstrADDomainUser
        End Get
        Set(ByVal value As String)
            mstrADDomainUser = value
            setParamVal("ADDomainUser", mstrADDomainUser)
        End Set
    End Property

    Public Property _ADDomainUserPwd() As String
        Get
            Return mstrADDomainUserPwd
        End Get
        Set(ByVal value As String)
            mstrADDomainUserPwd = value
            setParamVal("ADDomainUserPwd", mstrADDomainUserPwd)
        End Set
    End Property

    'Pinkal (03-Apr-2017) -- End


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mblnCreateADUserFromEmpMst As Boolean = False
    Public Property _CreateADUserFromEmpMst() As Boolean
        Get
            Return mblnCreateADUserFromEmpMst
        End Get
        Set(ByVal value As Boolean)
            mblnCreateADUserFromEmpMst = value
            setParamVal("CreateADUserFromEmpMst", mblnCreateADUserFromEmpMst)
        End Set
    End Property

    Private mblnUserMustChangePwdOnNextLogon As Boolean = False
    Public Property _UserMustchangePwdOnNextLogon() As Boolean
        Get
            Return mblnUserMustChangePwdOnNextLogon
        End Get
        Set(ByVal value As Boolean)
            mblnUserMustChangePwdOnNextLogon = value
            setParamVal("UserMustChangePwdOnNextLogon", mblnUserMustChangePwdOnNextLogon)
        End Set
    End Property
    'Pinkal (18-Aug-2018) -- End



    'Pinkal (11-AUG-2017) -- Start
    'Enhancement - Working On B5 Plus Company TnA Enhancements.

    Public Property _IsHolidayConsiderOnWeekend() As Boolean
        Get
            Return mblnIsHolidayConsiderOnWeekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsHolidayConsiderOnWeekend = value
            setParamVal("IsHolidayConsiderOnWeekend", mblnIsHolidayConsiderOnWeekend)
        End Set
    End Property

    Public Property _IsDayOffConsiderOnWeekend() As Boolean
        Get
            Return mblnIsDayOffConsiderOnWeekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsDayOffConsiderOnWeekend = value
            setParamVal("IsDayOffConsiderOnWeekend", mblnIsDayOffConsiderOnWeekend)
        End Set
    End Property

    Public Property _IsHolidayConsiderOnDayoff() As Boolean
        Get
            Return mblnIsHolidayConsiderOnDayoff
        End Get
        Set(ByVal value As Boolean)
            mblnIsHolidayConsiderOnDayoff = value
            setParamVal("IsHolidayConsiderOnDayoff", mblnIsHolidayConsiderOnDayoff)
        End Set
    End Property

    'Pinkal (11-AUG-2017) -- End

    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.

    Public Property _RestrictEmpForBgTimesheetOnAfterDate() As Integer
        Get
            Return mintRestrictEmpForBgTimesheetOnAfterDate
        End Get
        Set(ByVal value As Integer)
            mintRestrictEmpForBgTimesheetOnAfterDate = value
            setParamVal("RestrictEmpForBgTimesheetOnAfterDate", mintRestrictEmpForBgTimesheetOnAfterDate)
        End Set
    End Property

    Public Property _DisableADUserAgainAfterMins() As Integer
        Get
            Return mintDisableADUserAgainAfterMins
        End Get
        Set(ByVal value As Integer)
            mintDisableADUserAgainAfterMins = value
            setParamVal("DisableADUserAgainAfterMins", mintDisableADUserAgainAfterMins)
        End Set
    End Property


    'Pinkal (23-AUG-2017) -- End

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private mblnShowBgTimesheetActivity As Boolean = True
    Public Property _ShowBgTimesheetActivity() As Boolean
        Get
            Return mblnShowBgTimesheetActivity
        End Get
        Set(ByVal value As Boolean)
            setParamVal("ShowBgTimesheetActivity", value)
        End Set
    End Property

    Private mblnShowBgTimesheetAssignedActivityPercentage As Boolean = True
    Public Property _ShowBgTimesheetAssignedActivityPercentage() As Boolean
        Get
            Return mblnShowBgTimesheetAssignedActivityPercentage
        End Get
        Set(ByVal value As Boolean)
            setParamVal("ShowBgTimesheetAssignedActivityPercentage", value)
        End Set
    End Property

    Private mblnShowBgTimesheetActivityProject As Boolean = True
    Public Property _ShowBgTimesheetActivityProject() As Boolean
        Get
            Return mblnShowBgTimesheetActivityProject
        End Get
        Set(ByVal value As Boolean)
            setParamVal("ShowBgTimesheetActivityProject", value)
        End Set
    End Property

    Private mblnShowBgTimesheetActivityHrsDetail As Boolean = True
    Public Property _ShowBgTimesheetActivityHrsDetail() As Boolean
        Get
            Return mblnShowBgTimesheetActivityHrsDetail
        End Get
        Set(ByVal value As Boolean)
            setParamVal("ShowBgTimesheetActivityHrsDetail", value)
        End Set
    End Property

    Private mblnRemarkMandatoryForTimesheetSubmission As Boolean = False
    Public Property _RemarkMandatoryForTimesheetSubmission() As Boolean
        Get
            Return mblnRemarkMandatoryForTimesheetSubmission
        End Get
        Set(ByVal value As Boolean)
            setParamVal("RemarkMandatoryForTimesheetSubmission", value)
        End Set
    End Property
    'Pinkal (28-Jul-2018) -- End

 'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .
    Private mblnSkipEmployeeApprovalFlow As Boolean = True
    Public Property _SkipEmployeeApprovalFlow() As Boolean
        Get
            Return mblnSkipEmployeeApprovalFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipEmployeeApprovalFlow = value
            setParamVal("SkipEmployeeApprovalFlow", mblnSkipEmployeeApprovalFlow)
        End Set
    End Property

    Private mblnSkipEmployeeMovementApprovalFlow As Boolean = True
    Public Property _SkipEmployeeMovementApprovalFlow() As Boolean
        Get
            Return mblnSkipEmployeeMovementApprovalFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipEmployeeMovementApprovalFlow = value
            setParamVal("SkipEmployeeMovementApprovalFlow", mblnSkipEmployeeMovementApprovalFlow)
        End Set
    End Property

    Private mstrEmpMandatoryFieldsIDs As String = ""
    Public Property _EmpMandatoryFieldsIDs() As String
        Get
            Return mstrEmpMandatoryFieldsIDs
        End Get
        Set(ByVal value As String)
            mstrEmpMandatoryFieldsIDs = value
            setParamVal("EmpMandatoryFieldsIDs", mstrEmpMandatoryFieldsIDs)
        End Set
    End Property

    Private mstrPendingEmployeeScreenIDs As String = ""
    Public Property _PendingEmployeeScreenIDs() As String
        Get
            Return mstrPendingEmployeeScreenIDs
        End Get
        Set(ByVal value As String)
            mstrPendingEmployeeScreenIDs = value
            setParamVal("PendingEmployeeScreenIDs", mstrPendingEmployeeScreenIDs)
        End Set
    End Property

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : On the references page on recruitment portal, provide all those fields on configuration where they can be made mandatory or optional.
    Private mstrRecruitMandatoryFieldsIDs As String = ""
    Public Property _RecruitMandatoryFieldsIDs() As String
        Get
            If mintCompanyUnkid > 0 Then
                If IsKeyExist(mintCompanyUnkid, "RecruitMandatoryFieldsIDs") = False Then
                    Dim strIDs As String = ""
                    If mblnRecMiddleNameMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.MiddleName
                    End If
                    If mblnRecGenderMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Gender
                    End If
                    If mblnRecBirthDayMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.BirthDate
                    End If
                    If mblnRecNationalityMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Nationality
                    End If
                    If mblnRecAddress1Mandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Address1
                    End If
                    If mblnRecAddress2Mandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Address2
                    End If
                    If mblnRecCountryMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Country
                    End If
                    If mblnRecStateMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.State
                    End If
                    If mblnRecCityMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.City
                    End If
                    If mblnRecRegionMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Region
                    End If
                    If mblnRecPostCodeMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.PostCode
                    End If
                    If mblnRecLanguage1Mandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Language1
                    End If
                    If mblnRecMaritalStatusMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.MaritalStatus
                    End If
                    If mblnRecMotherTongueMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.MotherTongue
                    End If
                    If mblnRecCurrentSalaryMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.CurrentSalary
                    End If
                    If mblnRecExpectedSalaryMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.ExpectedSalary
                    End If
                    If mblnRecExpectedBenefitsMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.ExpectedBenefits
                    End If
                    If mblnRecNoticePeriodMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.NoticePeriod
                    End If
                    If mblnRecQualificationStartDateMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.QualificationStartDate
                    End If
                    If mblnRecQualificationAwardDateMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.QualificationAwardDate
                    End If
                    If mblnRecHighestQualificationMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.HighestQualification
                    End If
                    If mblnRecEmployerMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.Employer
                    End If
                    If mblnRecCompanyNameJobHistoryMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.CompanyName
                    End If
                    If mblnRecPositionHeldMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.PositionHeld
                    End If
                    If mblnRecOneCurriculamVitaeMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.CurriculumVitae
                    End If
                    If mblnRecOneCoverLetterMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.CoverLetter
                    End If
                    If mblnRecEarliestPossibleStartDateMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.EarliestPossibleStartDate
                    End If
                    If mblnRecVacancyFoundOutFromMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.VacancyFoundOutFrom
                    End If
                    If mblnRecCommentsMandatory = True Then
                        strIDs &= "," & enMandatorySettingRecruitment.ApplicantComments
                    End If
                    If strIDs.Trim <> "" Then
                        mstrRecruitMandatoryFieldsIDs = strIDs.Substring(1)
                    End If
                End If
            End If
            Return mstrRecruitMandatoryFieldsIDs
        End Get
        Set(ByVal value As String)
            mstrRecruitMandatoryFieldsIDs = value
            setParamVal("RecruitMandatoryFieldsIDs", mstrRecruitMandatoryFieldsIDs)
        End Set
    End Property

    Private mintRecruitMandatorySkills As Integer = 0
    Public Property _RecruitMandatorySkills() As Integer
        Get
            If mintCompanyUnkid > 0 Then
                If IsKeyExist(mintCompanyUnkid, "RecruitMandatorySkills") = False Then
                    If mblnRecOneSkillMandatory = True Then
                        mintRecruitMandatorySkills = 1
                    End If
                End If
            End If
            Return mintRecruitMandatorySkills
        End Get
        Set(ByVal value As Integer)
            mintRecruitMandatorySkills = value
            setParamVal("RecruitMandatorySkills", mintRecruitMandatorySkills)
        End Set
    End Property

    Private mintRecruitMandatoryQualifications As Integer = 0
    Public Property _RecruitMandatoryQualifications() As Integer
        Get
            If mintCompanyUnkid > 0 Then
                If IsKeyExist(mintCompanyUnkid, "RecruitMandatoryQualifications") = False Then
                    If mblnRecOneQualificationMandatory = True Then
                        mintRecruitMandatoryQualifications = 1
                    End If
                End If
            End If
            Return mintRecruitMandatoryQualifications
        End Get
        Set(ByVal value As Integer)
            mintRecruitMandatoryQualifications = value
            setParamVal("RecruitMandatoryQualifications", mintRecruitMandatoryQualifications)
        End Set
    End Property

    Private mintRecruitMandatoryJobExperiences As Integer = 0
    Public Property _RecruitMandatoryJobExperiences() As Integer
        Get
            If mintCompanyUnkid > 0 Then
                If IsKeyExist(mintCompanyUnkid, "RecruitMandatoryJobExperiences") = False Then
                    If mblnRecOneJobExperienceMandatory = True Then
                        mintRecruitMandatoryJobExperiences = 1
                    End If
                End If
            End If
            Return mintRecruitMandatoryJobExperiences
        End Get
        Set(ByVal value As Integer)
            mintRecruitMandatoryJobExperiences = value
            setParamVal("RecruitMandatoryJobExperiences", mintRecruitMandatoryJobExperiences)
        End Set
    End Property

    Private mintRecruitMandatoryReferences As Integer = 0
    Public Property _RecruitMandatoryReferences() As Integer
        Get
            If mintCompanyUnkid > 0 Then
                If IsKeyExist(mintCompanyUnkid, "RecruitMandatoryReferences") = False Then
                    If mblnRecOneReferenceMandatory = True Then
                        mintRecruitMandatoryReferences = 1
                    End If
                End If
            End If
            Return mintRecruitMandatoryReferences
        End Get
        Set(ByVal value As Integer)
            mintRecruitMandatoryReferences = value
            setParamVal("RecruitMandatoryReferences", mintRecruitMandatoryReferences)
        End Set
    End Property
    'Sohail (18 Feb 2020) -- End

    'Sohail (06 Oct 2021) -- Start
    'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
    Private mintRecruitmentEmailSetupUnkid As Integer = 0
    Public Property _RecruitmentEmailSetupUnkid() As Integer
        Get
            Return mintRecruitmentEmailSetupUnkid
        End Get
        Set(ByVal value As Integer)
            mintRecruitmentEmailSetupUnkid = value
            setParamVal("RecruitmentEmailSetupUnkid", mintRecruitmentEmailSetupUnkid)
        End Set
    End Property
    'Sohail (06 Oct 2021) -- End


    Private mstrRejectTransferUserNotification As String = ""
    Public Property _RejectTransferUserNotification() As String
        Get
            Return mstrRejectTransferUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectTransferUserNotification = value
            setParamVal("RejectTransferUserNotification", mstrRejectTransferUserNotification)
        End Set
    End Property

    Private mstrRejectReCategorizationUserNotification As String = ""
    Public Property _RejectReCategorizationUserNotification() As String
        Get
            Return mstrRejectReCategorizationUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectReCategorizationUserNotification = value
            setParamVal("RejectReCategorizationUserNotification", mstrRejectReCategorizationUserNotification)
        End Set
    End Property

    Private mstrRejectProbationUserNotification As String = ""
    Public Property _RejectProbationUserNotification() As String
        Get
            Return mstrRejectProbationUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectProbationUserNotification = value
            setParamVal("RejectProbationUserNotification", mstrRejectProbationUserNotification)
        End Set
    End Property

    Private mstrRejectConfirmationUserNotification As String = ""
    Public Property _RejectConfirmationUserNotification() As String
        Get
            Return mstrRejectConfirmationUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectConfirmationUserNotification = value
            setParamVal("RejectConfirmationUserNotification", mstrRejectConfirmationUserNotification)
        End Set
    End Property

    Private mstrRejectSuspensionUserNotification As String = ""
    Public Property _RejectSuspensionUserNotification() As String
        Get
            Return mstrRejectSuspensionUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectSuspensionUserNotification = value
            setParamVal("RejectSuspensionUserNotification", mstrRejectSuspensionUserNotification)
        End Set
    End Property

    Private mstrRejectTerminationUserNotification As String = ""
    Public Property _RejectTerminationUserNotification() As String
        Get
            Return mstrRejectTerminationUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectTerminationUserNotification = value
            setParamVal("RejectTerminationUserNotification", mstrRejectTerminationUserNotification)
        End Set
    End Property

    Private mstrRejectRetirementUserNotification As String = ""
    Public Property _RejectRetirementUserNotification() As String
        Get
            Return mstrRejectRetirementUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectRetirementUserNotification = value
            setParamVal("RejectRetirementUserNotification", mstrRejectRetirementUserNotification)
        End Set
    End Property

    Private mstrRejectWorkPermitUserNotification As String = ""
    Public Property _RejectWorkPermitUserNotification() As String
        Get
            Return mstrRejectWorkPermitUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectWorkPermitUserNotification = value
            setParamVal("RejectWorkPermitUserNotification", mstrRejectWorkPermitUserNotification)
        End Set
    End Property

    Private mstrRejectResidentPermitUserNotification As String = ""
    Public Property _RejectResidentPermitUserNotification() As String
        Get
            Return mstrRejectResidentPermitUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectResidentPermitUserNotification = value
            setParamVal("RejectResidentPermitUserNotification", mstrRejectResidentPermitUserNotification)
        End Set
    End Property

    Private mstrRejectCostCenterPermitUserNotification As String = ""
    Public Property _RejectCostCenterPermitUserNotification() As String
        Get
            Return mstrRejectCostCenterPermitUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectCostCenterPermitUserNotification = value
            setParamVal("RejectCostCenterPermitUserNotification", mstrRejectCostCenterPermitUserNotification)
        End Set
    End Property

    'S.SANDEEP [20-JUN-2018] -- End

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private mstrRejectExemptionUserNotification As String = ""
    Public Property _RejectExemptionUserNotification() As String
        Get
            Return mstrRejectExemptionUserNotification
        End Get
        Set(ByVal value As String)
            mstrRejectExemptionUserNotification = value
            setParamVal("RejectExemptionUserNotification", mstrRejectExemptionUserNotification)
        End Set
    End Property
    'Sohail (21 Oct 2019) -- End

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    Private mblnSetRelieverAsMandatoryForApproval As Boolean = False
    Public Property _SetRelieverAsMandatoryForApproval() As Boolean
        Get
            Return mblnSetRelieverAsMandatoryForApproval
        End Get
        Set(ByVal value As Boolean)
            mblnSetRelieverAsMandatoryForApproval = value
            setParamVal("SetRelieverAsMandatoryForApproval", mblnSetRelieverAsMandatoryForApproval)
        End Set
    End Property

    Private mblnAllowToSetRelieverOnLvFormForESS As Boolean = False  'USED FOR ESS AND EMPLOYEE CAN ABLE TO SET RELIEVER ON LEAVE FORM IF THIS SETTING IS CHECKED.
    Public Property _AllowToSetRelieverOnLvFormForESS() As Boolean
        Get
            Return mblnAllowToSetRelieverOnLvFormForESS
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToSetRelieverOnLvFormForESS = value
            setParamVal("AllowToSetRelieverOnLvFormForESS", mblnAllowToSetRelieverOnLvFormForESS)
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (13-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private mstrRelieverAllocation As String = ""
    Public Property _RelieverAllocation() As String
        Get
            Return mstrRelieverAllocation
        End Get
        Set(ByVal value As String)
            mstrRelieverAllocation = value
            setParamVal("RelieverAllocation", mstrRelieverAllocation)
        End Set
    End Property
    'Pinkal (13-Mar-2019) -- End


    'Pinkal (30-May-2020) -- Start
    'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.
    Private mblnAllowToCancelLeaveForClosedPeriod As Boolean = False
    Public Property _AllowToCancelLeaveForClosedPeriod() As Boolean
        Get
            Return mblnAllowToCancelLeaveForClosedPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToCancelLeaveForClosedPeriod = value
            setParamVal("AllowToCancelLeaveForClosedPeriod", mblnAllowToCancelLeaveForClosedPeriod)
        End Set
    End Property

    'Pinkal (30-May-2020) -- End



    'Pinkal (13-Jun-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Private mblnAllowLvApplicationApplyForBackDates As Boolean = False
    Public Property _AllowLvApplicationApplyForBackDates() As Boolean
        Get
            Return mblnAllowLvApplicationApplyForBackDates
        End Get
        Set(ByVal value As Boolean)
            mblnAllowLvApplicationApplyForBackDates = value
            setParamVal("AllowLvApplicationApplyForBackDates", mblnAllowLvApplicationApplyForBackDates)
        End Set
    End Property
    'Pinkal (13-Jun-2019) -- End

    'Pinkal (19-Oct-2018) -- Start
    'Enhancement - OT Requisition Settings Enhancement for NMB.

    Private mblnSetOTRequisitionMandatory As Boolean = False
    Public Property _SetOTRequisitionMandatory() As Boolean
        Get
            Return mblnSetOTRequisitionMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnSetOTRequisitionMandatory = value
            setParamVal("SetOTRequisitionMandatory", mblnSetOTRequisitionMandatory)
        End Set
    End Property

    Private mstrCapOTHrsForHODApprovers As String = ""
    Public Property _CapOTHrsForHODApprovers() As String
        Get
            Return mstrCapOTHrsForHODApprovers
        End Get
        Set(ByVal value As String)
            mstrCapOTHrsForHODApprovers = value
            setParamVal("CapOTHrsForHODApprovers", mstrCapOTHrsForHODApprovers)
        End Set
    End Property

    'Pinkal (19-Oct-2018) -- End


    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.


    'Pinkal (02-Jun-2020) -- Start
    'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.

    'Private mintAllowApplyOTForEachMonthDay As Integer = 0
    'Public Property _AllowApplyOTForEachMonthDay() As Integer
    '    Get
    '        Return mintAllowApplyOTForEachMonthDay
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintAllowApplyOTForEachMonthDay = value
    '        setParamVal("AllowApplyOTMonthDay", mintAllowApplyOTForEachMonthDay)
    '    End Set
    'End Property

    Private mintOTTenureDays As Integer = 0
    Public Property _OTTenureDays() As Integer
        Get
            Return mintOTTenureDays
        End Get
        Set(ByVal value As Integer)
            mintOTTenureDays = value
            setParamVal("OTTenureDays", mintOTTenureDays)
        End Set
    End Property


    'Pinkal (02-Jun-2020) -- End

    'Pinkal (24-Oct-2019) -- End




 'Gajanan [13-AUG-2018] -- Start
    'Enhancement - Implementing Grievance Module.
    Private mintGrievanceApprovalSetting As Integer = enGrievanceApproval.ApproverEmpMapping
    Public Property _GrievanceApprovalSetting() As Integer
        Get
            Return mintGrievanceApprovalSetting
        End Get
        Set(ByVal value As Integer)
            mintGrievanceApprovalSetting = value
            setParamVal("GrievanceApprovalSetting", mintGrievanceApprovalSetting)
        End Set
    End Property
    'Gajanan [13-AUG-2018] -- End


    'Pinkal (03-Dec-2018) -- Start
    'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
    Private mstrAssetDeclarationFromDate As String = ""
    Public Property _AssetDeclarationFromDate() As String
        Get
            Return mstrAssetDeclarationFromDate
        End Get
        Set(ByVal value As String)
            mstrAssetDeclarationFromDate = value
            setParamVal("AssetDeclarationFromDate", mstrAssetDeclarationFromDate)
        End Set
    End Property

    Private mstrAssetDeclarationToDate As String = ""
    Public Property _AssetDeclarationToDate() As String
        Get
            Return mstrAssetDeclarationToDate
        End Get
        Set(ByVal value As String)
            mstrAssetDeclarationToDate = value
            setParamVal("AssetDeclarationToDate", mstrAssetDeclarationToDate)
        End Set
    End Property

    'Hemant (01 Mar 2022) -- Start            
    'Private mintEmpUnLockDaysAfterAssetDeclarationToDate As Integer
    'Public Property _UnLockDaysAfterAssetDeclarationToDate() As Integer
    '    Get
    '        Return mintEmpUnLockDaysAfterAssetDeclarationToDate
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEmpUnLockDaysAfterAssetDeclarationToDate = value
    '        setParamVal("EmpUnLockDaysAfterAssetDeclarationToDate", mintEmpUnLockDaysAfterAssetDeclarationToDate)
    '    End Set
    'End Property
    Private mintDontAllowAssetDeclarationAfterDays As Integer
    Public Property _DontAllowAssetDeclarationAfterDays() As Integer
        Get
            Return mintDontAllowAssetDeclarationAfterDays
        End Get
        Set(ByVal value As Integer)
            mintDontAllowAssetDeclarationAfterDays = value
            setParamVal("DontAllowAssetDeclarationAfterDays", mintDontAllowAssetDeclarationAfterDays)
        End Set
    End Property
    'Hemant (01 Mar 2022) -- End

    Private mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate As Integer
    Public Property _NewEmpUnLockDaysforAssetDecWithinAppointmentDate() As Integer
        Get
            Return mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate
        End Get
        Set(ByVal value As Integer)
            mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate = value
            setParamVal("NewEmpUnLockDaysforAssetDecWithinAppointmentDate", mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate)
        End Set
    End Property


    'Pinkal (03-Dec-2018) -- End

    'Sohail (04 Feb 2020) -- Start
    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
    Private mstrNonDisclosureDeclarationFromDate As String = ""
    Public Property _NonDisclosureDeclarationFromDate() As String
        Get
            Return mstrNonDisclosureDeclarationFromDate
        End Get
        Set(ByVal value As String)
            mstrNonDisclosureDeclarationFromDate = value
            setParamVal("NonDisclosureDeclarationFromDate", mstrNonDisclosureDeclarationFromDate)
        End Set
    End Property

    Private mstrNonDisclosureDeclarationToDate As String = ""
    Public Property _NonDisclosureDeclarationToDate() As String
        Get
            Return mstrNonDisclosureDeclarationToDate
        End Get
        Set(ByVal value As String)
            mstrNonDisclosureDeclarationToDate = value
            setParamVal("NonDisclosureDeclarationToDate", mstrNonDisclosureDeclarationToDate)
        End Set
    End Property

    Private mintEmpLockDaysAfterNonDisclosureDeclarationToDate As Integer
    Public Property _LockDaysAfterNonDisclosureDeclarationToDate() As Integer
        Get
            Return mintEmpLockDaysAfterNonDisclosureDeclarationToDate
        End Get
        Set(ByVal value As Integer)
            mintEmpLockDaysAfterNonDisclosureDeclarationToDate = value
            setParamVal("EmpLockDaysAfterNonDisclosureDeclarationToDate", mintEmpLockDaysAfterNonDisclosureDeclarationToDate)
        End Set
    End Property

    Private mintNewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate As Integer
    Public Property _NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate() As Integer
        Get
            Return mintNewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate
        End Get
        Set(ByVal value As Integer)
            mintNewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate = value
            setParamVal("NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate", mintNewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate)
        End Set
    End Property

    Private mstrNonDisclosureDeclaration As String = ""
    Public Property _NonDisclosureDeclaration() As String
        Get
            If mstrNonDisclosureDeclaration.Trim = "" Then
                mstrNonDisclosureDeclaration = "a.	I,  #employeename#, (hereinafter referred to as " & Chr(34) & "the Employee" & Chr(34) & ") shall not, either during the continuance of my employment or thereafter, use to the detriment or prejudice of NMB Bank Plc (hereinafter referred to as " & Chr(34) & "the Bank" & Chr(34) & "), or divulge to any person any trade secret or other confidential information concerning the business or affairs of the Bank or its staff’s information that have come to my knowledge due to and during my employment with the Bank." & vbCrLf & vbCrLf
                mstrNonDisclosureDeclaration &= "b.	In the event of cessation of my employment with the Bank for whatever reason, I shall return to the Bank any papers or other information belonging to or containing information relative to the Bank’s affairs that might be in my possession." & vbCrLf & vbCrLf
                mstrNonDisclosureDeclaration &= "c.	Signing of this agreement constitutes an undertaking that I will not remove from the Bank or photocopy any bank related papers or media for my own use or anybody else’s subsequent use. I shall also during the continuance of my employment with the Bank comply with the terms of any regulations, rules or laws relating to Data Protection." & vbCrLf & vbCrLf
                mstrNonDisclosureDeclaration &= "d.	I am aware and understand that failure to adhere to this confidentiality agreement may result into appropriate disciplinary actions being taken against me." & vbCrLf & vbCrLf
                mstrNonDisclosureDeclaration &= "This declaration is also made pursuant to Section 48 of the Banking and Financial Institutions Act of 2006."
            End If
            Return mstrNonDisclosureDeclaration
        End Get
        Set(ByVal value As String)
            mstrNonDisclosureDeclaration = value
            setParamVal("NonDisclosureDeclaration", mstrNonDisclosureDeclaration)
        End Set
    End Property

    Private mintNonDisclosureWitnesser1 As Integer
    Public Property _NonDisclosureWitnesser1() As Integer
        Get
            Return mintNonDisclosureWitnesser1
        End Get
        Set(ByVal value As Integer)
            mintNonDisclosureWitnesser1 = value
            setParamVal("NonDisclosureWitnesser1", mintNonDisclosureWitnesser1)
        End Set
    End Property

    Private mintNonDisclosureWitnesser2 As Integer
    Public Property _NonDisclosureWitnesser2() As Integer
        Get
            Return mintNonDisclosureWitnesser2
        End Get
        Set(ByVal value As Integer)
            mintNonDisclosureWitnesser2 = value
            setParamVal("NonDisclosureWitnesser2", mintNonDisclosureWitnesser2)
        End Set
    End Property
    'Sohail (04 Feb 2020) -- End

    'Sohail (07 Apr 2021) -- Start
    'TRA Enhancement : : Setting on configuration to hide Non-Dislcosure declaration link on menu for TRA.
    Private mblnApplyNonDisclosureDeclaration As Boolean = True
    Public Property _ApplyNonDisclosureDeclaration() As Boolean
        Get
            Return mblnApplyNonDisclosureDeclaration
        End Get
        Set(ByVal value As Boolean)
            mblnApplyNonDisclosureDeclaration = value
            setParamVal("ApplyNonDisclosureDeclaration", mblnApplyNonDisclosureDeclaration)
        End Set
    End Property
    'Sohail (07 Apr 2021) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mstrSkipApprovalOnEmpData As String = mstrSkipApprovalOnEmpDatavalue
    Public Property _SkipApprovalOnEmpData() As String
        Get
            Return mstrSkipApprovalOnEmpData
        End Get
        Set(ByVal SkipApprovalOnEmpData As String)
            mstrSkipApprovalOnEmpData = SkipApprovalOnEmpData
            setParamVal("SkipApprovalOnEmpData", mstrSkipApprovalOnEmpData)
        End Set
    End Property


    'Gajanan |30-MAR-2019| -- START
    Private mstrOldSkipApprovalOnEmpData As String
    Public Property _oldSkipApprovalOnEmpData() As String
        Get
            Return mstrOldSkipApprovalOnEmpData
        End Get
        Set(ByVal SkipApprovalOnEmpData As String)
            mstrOldSkipApprovalOnEmpData = SkipApprovalOnEmpData
            setParamVal("OldSkipApprovalOnEmpData", mstrOldSkipApprovalOnEmpData)
        End Set
    End Property
    'Gajanan |30-MAR-2019| -- END


    Private mdicEmployeeDataRejectedUserIds As New Dictionary(Of Integer, String)
    Public Property _EmployeeDataRejectedUserIds() As Dictionary(Of Integer, String)
        Get
            Return mdicEmployeeDataRejectedUserIds
        End Get
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicEmployeeDataRejectedUserIds = value
            For Each ikey As Integer In mdicEmployeeDataRejectedUserIds.Keys
                setParamVal("_RejEmpData_" & ikey.ToString(), mdicEmployeeDataRejectedUserIds(ikey))
            Next
        End Set
    End Property
    'Gajanan [17-DEC-2018] -- End

    'Sohail (10 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - Error email setting on configuration.
    Private mstrAdministratorEmailForError As String
    Public Property _AdministratorEmailForError() As String
        Get
            Return mstrAdministratorEmailForError
        End Get
        Set(ByVal value As String)
            mstrAdministratorEmailForError = value
            setParamVal("AdministratorEmailForError", mstrAdministratorEmailForError)
        End Set
    End Property
    'Sohail (10 Apr 2019) -- End



    'Pinkal (20-May-2019) -- Start
    'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
    Private mblnClaimRemarkMandatoryForClaim As Boolean = False
    Public Property _ClaimRemarkMandatoryForClaim() As Boolean
        Get
            Return mblnClaimRemarkMandatoryForClaim
        End Get
        Set(ByVal value As Boolean)
            mblnClaimRemarkMandatoryForClaim = value
            setParamVal("ClaimRemarkMandatoryForClaim", mblnClaimRemarkMandatoryForClaim)
        End Set
    End Property
        'Pinkal (20-May-2019) -- End


    'Pinkal (04-Feb-2020) -- Start
    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
    Private mblnEmpNotificationForClaimApplicationByManager As Boolean = False
    Public Property _EmpNotificationForClaimApplicationByManager() As Boolean
        Get
            Return mblnEmpNotificationForClaimApplicationByManager
        End Get
        Set(ByVal value As Boolean)
            mblnEmpNotificationForClaimApplicationByManager = value
            setParamVal("EmpNotificationForClaimApplicationbyManager", mblnEmpNotificationForClaimApplicationByManager)
        End Set
    End Property
    'Pinkal (04-Feb-2020) -- End


    'Pinkal (30-May-2020) -- Start
    'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
    Private mblnAllowOnlyOneExpenseInClaimApplication As Boolean = False
    Public Property _AllowOnlyOneExpenseInClaimApplication() As Boolean
        Get
            Return mblnAllowOnlyOneExpenseInClaimApplication
        End Get
        Set(ByVal value As Boolean)
            mblnAllowOnlyOneExpenseInClaimApplication = value
            setParamVal("AllowOnlyOneExpenseInClaimApplication", mblnAllowOnlyOneExpenseInClaimApplication)
        End Set
    End Property
    'Pinkal (30-May-2020) -- End



    'Gajanan [27-May-2019] -- Start      
    'Private mblnDomicileAddressDocsAttachmentMandatory As Boolean = False
    'Private mblnDomicileAddressDocsAttachmentMandatory As Boolean = False
    'Public Property _DomicileDocsAttachmentMandatory() As Boolean
    '    Get
    '        Return mblnDependantDocsAttachmentMandatory
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnDependantDocsAttachmentMandatory = value
    '        setParamVal("DependantDocsAttachmentMandatory", mblnDependantDocsAttachmentMandatory)
    '    End Set
    'End Property
    'Gajanan [27-May-2019] -- End


    'Pinkal (07-Jun-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Private mblnLockUserOnSuspension As Boolean = True
    Public Property _LockUserOnSuspension() As Boolean
        Get
            Return mblnLockUserOnSuspension
        End Get
        Set(ByVal value As Boolean)
            mblnLockUserOnSuspension = value
            setParamVal("LockUserOnSuspension", mblnLockUserOnSuspension)
        End Set
    End Property
    'Pinkal (07-Jun-2019) -- End

'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Private mstrScoreCalibrationFormNoPrifix As String = ""
    Public Property _ScoreCalibrationFormNotype() As Integer
        Get
            Return mintScoreCalibrationFormNotype
        End Get
        Set(ByVal value As Integer)
            mintScoreCalibrationFormNotype = value
            setParamVal("ScoreCalibrationFormNotype", mintScoreCalibrationFormNotype)
        End Set
    End Property

    Private mintScoreCalibrationFormNotype As Integer = 0
    Public Property _ScoreCalibrationFormNoPrifix() As String
        Get
            Return mstrScoreCalibrationFormNoPrifix
        End Get
        Set(ByVal value As String)
            mstrScoreCalibrationFormNoPrifix = value
            setParamVal("ScoreCalibrationFormNoPrifix", mstrScoreCalibrationFormNoPrifix)
        End Set
    End Property

    Private mintNextScoreCalibrationFormNo As Integer = 1
    Public Property _NextScoreCalibrationFormNo() As Integer
        Get
            Return mintNextScoreCalibrationFormNo
        End Get
        Set(ByVal value As Integer)
            mintNextScoreCalibrationFormNo = value
            setParamVal("NextScoreCalibrationFormNo", mintNextScoreCalibrationFormNo)
        End Set
    End Property

    Private mblnIsCalibrationSettingActive As Boolean = False
    Public Property _IsCalibrationSettingActive() As Boolean
        Get
            Return mblnIsCalibrationSettingActive
        End Get
        Set(ByVal value As Boolean)
            mblnIsCalibrationSettingActive = value
            setParamVal("IsCalibrationSettingActive", mblnIsCalibrationSettingActive)
        End Set
    End Property
    'S.SANDEEP |27-MAY-2019| -- END

    'Pinkal (13-Aug-2019) -- Start
    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
    Private mblnAllowEmpAssignedProjectExceedTime As Boolean = False
    Public Property _AllowEmpAssignedProjectExceedTime() As Boolean
        Get
            Return mblnAllowEmpAssignedProjectExceedTime
        End Get
        Set(ByVal value As Boolean)
            mblnAllowEmpAssignedProjectExceedTime = value
            setParamVal("AllowEmpAssignedProjectExceedTime", mblnAllowEmpAssignedProjectExceedTime)
        End Set
    End Property
    'Pinkal (13-Aug-2019) -- End

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private mintDisplayCalibrationUserAllocationId As Integer = 0
    Public Property _DisplayCalibrationUserAllocationId() As Integer
        Get
            Return mintDisplayCalibrationUserAllocationId
        End Get
        Set(ByVal value As Integer)
            mintDisplayCalibrationUserAllocationId = value
            setParamVal("DisplayCalibrationUserAllocationId", mintDisplayCalibrationUserAllocationId)
        End Set
    End Property
        'S.SANDEEP |26-AUG-2019| -- END


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Private mintClaimRetirementTypeId As Integer = enClaimRetirementType.General_Retirement
    Public Property _ClaimRetirementTypeId() As enClaimRetirementType
        Get
            Return mintClaimRetirementTypeId
        End Get
        Set(ByVal value As enClaimRetirementType)
            mintClaimRetirementTypeId = value
            setParamVal("ClaimRetirementTypeId", mintClaimRetirementTypeId)
        End Set
    End Property
    'Pinkal (11-Sep-2019) -- End


    'Pinkal (03-Jan-2020) -- Start
    'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.

    Private mstrApplicableLeaveStatus As String = ""
    Public Property _ApplicableLeaveStatus() As String
        Get
            Return mstrApplicableLeaveStatus
        End Get
        Set(ByVal value As String)
            mstrApplicableLeaveStatus = value
            setParamVal("ApplicableLeaveStatus", mstrApplicableLeaveStatus)
        End Set
    End Property

    'Pinkal (03-Jan-2020) -- End


    'Pinkal (30-Mar-2020) -- Start
    'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
    Private mblnAllowToCancelLeaveButRetainExpense As Boolean = False
    Public Property _AllowToCancelLeaveButRetainExpense() As Boolean
        Get
            Return mblnAllowToCancelLeaveButRetainExpense
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToCancelLeaveButRetainExpense = value
            setParamVal("AllowToCancelLeaveButRetainExpense", mblnAllowToCancelLeaveButRetainExpense)
        End Set
    End Property
    'Pinkal (30-Mar-2020) -- End



    'Gajanan [02-June-2020] -- Start
    'Enhancement Configure Option That Allow Period Closer Before XX Days.
    Private mblnAllowToClosePeriodBefore As Boolean = False
    Public Property _AllowToClosePeriodBefore() As Boolean
        Get
            Return mblnAllowToClosePeriodBefore
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToClosePeriodBefore = value
            setParamVal("AllowToClosePeriodBefore", mblnAllowToClosePeriodBefore)
        End Set
    End Property

    Private mintClosePeriodDaysBefore As Integer = 0
    Public Property _ClosePeriodDaysBefore() As Integer
        Get
            Return mintClosePeriodDaysBefore
        End Get
        Set(ByVal value As Integer)
            mintClosePeriodDaysBefore = value
            setParamVal("ClosePeriodDaysBefore", mintClosePeriodDaysBefore)
        End Set
    End Property
    'Gajanan [02-June-2020] -- End


    'Gajanan [19-June-2020] -- Start
    'Enhancement NMB Employee Signature Enhancement.

    Private mblnAllowEmployeeToAddEditSignatureESS As Boolean = False
    Public Property _AllowEmployeeToAddEditSignatureESS() As Boolean
        Get
            Return mblnAllowEmployeeToAddEditSignatureESS
        End Get
        Set(ByVal value As Boolean)
            mblnAllowEmployeeToAddEditSignatureESS = value
            setParamVal("AllowEmployeeToAddEditSignatureESS", mblnAllowEmployeeToAddEditSignatureESS)
        End Set
    End Property

    'Gajanan [19-June-2020] -- End

    'Gajanan [13-Nov-2020] -- Start
    'Final Recruitment UAT v1 Changes.
    Private mblnAllowToSyncJobWithVacancyMaster As Boolean = False
    Public Property _AllowToSyncJobWithVacancyMaster() As Boolean
        Get
            Return mblnAllowToSyncJobWithVacancyMaster
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToSyncJobWithVacancyMaster = value
            setParamVal("AllowToSyncJobWithVacancyMaster", mblnAllowToSyncJobWithVacancyMaster)
        End Set
    End Property
    'Gajanan [13-Nov-2020] -- End

'Pinkal (12-Nov-2020) -- Start
    'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
    Private mblnSkipApprovalFlowForApplicantShortListing As Boolean = False
    Public Property _SkipApprovalFlowForApplicantShortListing() As Boolean
        Get
            Return mblnSkipApprovalFlowForApplicantShortListing
        End Get
        Set(ByVal value As Boolean)
            mblnSkipApprovalFlowForApplicantShortListing = value
            setParamVal("SkipApprovalFlowForApplicantShortListing", mblnSkipApprovalFlowForApplicantShortListing)
        End Set
    End Property

    Private mblnSkipApprovalFlowForFinalApplicant As Boolean = False
    Public Property _SkipApprovalFlowForFinalApplicant() As Boolean
        Get
            Return mblnSkipApprovalFlowForFinalApplicant
        End Get
        Set(ByVal value As Boolean)
            mblnSkipApprovalFlowForFinalApplicant = value
            setParamVal("SkipApprovalFlowForFinalApplicant", mblnSkipApprovalFlowForFinalApplicant)
        End Set
    End Property
    'Pinkal (12-Nov-2020) -- End


    'Pinkal (27-Nov-2020) -- Start
    'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
    Private mblnAllowEmpSystemAccessOnActualEOCRetirementDate As Boolean = False
    Public Property _AllowEmpSystemAccessOnActualEOCRetirementDate() As Boolean
        Get
            Return mblnAllowEmpSystemAccessOnActualEOCRetirementDate
        End Get
        Set(ByVal value As Boolean)
            mblnAllowEmpSystemAccessOnActualEOCRetirementDate = value
            setParamVal("AllowEmpSystemAccessOnActualEOCRetirementDate", mblnAllowEmpSystemAccessOnActualEOCRetirementDate)
        End Set
    End Property
    'Pinkal (27-Nov-2020) -- End


    'Pinkal (19-Dec-2020) -- Start
    'Enhancement  -  Working on Discipline module for NMB.
    Private mblnAllowToViewESSVisibleWhileFilligDisciplineCharge As Boolean = False
    Public Property _AllowToViewESSVisibleWhileFilligDisciplineCharge() As Boolean
        Get
            Return mblnAllowToViewESSVisibleWhileFilligDisciplineCharge
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToViewESSVisibleWhileFilligDisciplineCharge = value
            setParamVal("AllowToViewESSVisibleWhileFilligDisciplineCharge", mblnAllowToViewESSVisibleWhileFilligDisciplineCharge)
        End Set
    End Property
    'Pinkal (19-Dec-2020) -- End



    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.
    'Private mintEmpBirthdayAllocation As Integer = CInt(enAllocation.DEPARTMENT)
    'Public Property _EmpBirthdayAllocation() As Integer
    '    Get
    '        Return mintEmpBirthdayAllocation
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEmpBirthdayAllocation = value
    '        setParamVal("EmpBirthdayAllocation", mintEmpBirthdayAllocation)
    '    End Set
    'End Property

    'Private mintEmpStaffTurnOverAllocation As Integer = CInt(enAllocation.DEPARTMENT)
    'Public Property _EmpStaffTurnOverAllocation() As Integer
    '    Get
    '        Return mintEmpStaffTurnOverAllocation
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEmpStaffTurnOverAllocation = value
    '        setParamVal("EmpStaffTurnOverAllocation", mintEmpStaffTurnOverAllocation)
    '    End Set
    'End Property

    'Private mintEmpOnLeaveAllocation As Integer = CInt(enAllocation.DEPARTMENT)
    'Public Property _EmpOnLeaveAllocation() As Integer
    '    Get
    '        Return mintEmpOnLeaveAllocation
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEmpOnLeaveAllocation = value
    '        setParamVal("EmpOnLeaveAllocation", mintEmpOnLeaveAllocation)
    '    End Set
    'End Property

    'Private mintEmpTeamMembersAllocation As Integer = CInt(enAllocation.DEPARTMENT)
    'Public Property _EmpTeamMembersAllocation() As Integer
    '    Get
    '        Return mintEmpTeamMembersAllocation
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEmpTeamMembersAllocation = value
    '        setParamVal("EmpTeamMembersAllocation", mintEmpTeamMembersAllocation)
    '    End Set
    'End Property

    'Private mintEmpWorkAnniversaryAllocation As Integer = CInt(enAllocation.DEPARTMENT)
    'Public Property _EmpWorkAnniversaryAllocation() As Integer
    '    Get
    '        Return mintEmpWorkAnniversaryAllocation
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEmpWorkAnniversaryAllocation = value
    '        setParamVal("EmpWorkAnniversaryAllocation", mintEmpWorkAnniversaryAllocation)
    '    End Set
    'End Property

    Private mstrEmpBirthdayAllocation As String = ""
    Public Property _EmpBirthdayAllocation() As String
        Get
            Return mstrEmpBirthdayAllocation
        End Get
        Set(ByVal value As String)
            mstrEmpBirthdayAllocation = value
            setParamVal("EmpBirthdayAllocation", mstrEmpBirthdayAllocation)
        End Set
    End Property

    Private mstrEmpStaffTurnOverAllocation As String = ""
    Public Property _EmpStaffTurnOverAllocation() As String
        Get
            Return mstrEmpStaffTurnOverAllocation
        End Get
        Set(ByVal value As String)
            mstrEmpStaffTurnOverAllocation = value
            setParamVal("EmpStaffTurnOverAllocation", mstrEmpStaffTurnOverAllocation)
        End Set
    End Property

    Private mstrEmpOnLeaveAllocation As String = ""
    Public Property _EmpOnLeaveAllocation() As String
        Get
            Return mstrEmpOnLeaveAllocation
        End Get
        Set(ByVal value As String)
            mstrEmpOnLeaveAllocation = value
            setParamVal("EmpOnLeaveAllocation", mstrEmpOnLeaveAllocation)
        End Set
    End Property

    Private mstrEmpTeamMembersAllocation As String = ""
    Public Property _EmpTeamMembersAllocation() As String
        Get
            Return mstrEmpTeamMembersAllocation
        End Get
        Set(ByVal value As String)
            mstrEmpTeamMembersAllocation = value
            setParamVal("EmpTeamMembersAllocation", mstrEmpTeamMembersAllocation)
        End Set
    End Property

    Private mstrEmpWorkAnniversaryAllocation As String = ""
    Public Property _EmpWorkAnniversaryAllocation() As String
        Get
            Return mstrEmpWorkAnniversaryAllocation
        End Get
        Set(ByVal value As String)
            mstrEmpWorkAnniversaryAllocation = value
            setParamVal("EmpWorkAnniversaryAllocation", mstrEmpWorkAnniversaryAllocation)
        End Set
    End Property
    'Pinkal (09-Aug-2021) -- End



    'Pinkal (27-Aug-2021) -- Start
    'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
    Private mintMaxEmpPlannedLeave As Integer = 0
    Public Property _MaxEmpPlannedLeave() As Integer
        Get
            Return mintMaxEmpPlannedLeave
        End Get
        Set(ByVal value As Integer)
            mintMaxEmpPlannedLeave = value
            setParamVal("MaxEmpPlannedLeave", mintMaxEmpPlannedLeave)
        End Set
    End Property

    Private mstrMaxEmpLeavePlannerAllocation As String = ""
    Public Property _MaxEmpLeavePlannerAllocation() As String
        Get
            Return mstrMaxEmpLeavePlannerAllocation
        End Get
        Set(ByVal value As String)
            mstrMaxEmpLeavePlannerAllocation = value
            setParamVal("MaxEmpLeavePlannerAllocation", mstrMaxEmpLeavePlannerAllocation)
        End Set
    End Property
    'Pinkal (27-Aug-2021) -- End



    'Hemant (01 Jan 2021) -- Start 
    'Enhancement #OLD-238 - Improved Employee Profile page
    Private mintAllocationWorkStation As Integer = CInt(enAllocation.DEPARTMENT)
    Public Property _AllocationWorkStation() As Integer
        Get
            Return mintAllocationWorkStation
        End Get
        Set(ByVal value As Integer)
            mintAllocationWorkStation = value
            setParamVal("AllocationWorkStation", mintAllocationWorkStation)
        End Set
    End Property
    'Hemant (01 Jan 2021) -- End


   'Pinkal (01-Feb-2021) -- Start
    'Enhancement NMB - Working Training Enhancements for New UI.

    Private mblnAllowToAddTrainingwithoutTNAProcess As Boolean = False
    Public Property _AllowToAddTrainingwithoutTNAProcess() As Boolean
        Get
            Return mblnAllowToAddTrainingwithoutTNAProcess
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToAddTrainingwithoutTNAProcess = value
            setParamVal("AllowToAddTrainingwithoutTNAProcess", mblnAllowToAddTrainingwithoutTNAProcess)
        End Set
    End Property

    Private mblnAllowToApplyRequestTrainingNotinTrainingPlan As Boolean = False
    Public Property _AllowToApplyRequestTrainingNotinTrainingPlan() As Boolean
        Get
            Return mblnAllowToApplyRequestTrainingNotinTrainingPlan
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToApplyRequestTrainingNotinTrainingPlan = value
            setParamVal("AllowToApplyRequestTrainingNotinTrainingPlan", mblnAllowToApplyRequestTrainingNotinTrainingPlan)
        End Set
    End Property

    Private mblnTrainingRequireForForeignTravelling As Boolean = False
    Public Property _TrainingRequireForForeignTravelling() As Boolean
        Get
            Return mblnTrainingRequireForForeignTravelling
        End Get
        Set(ByVal value As Boolean)
            mblnTrainingRequireForForeignTravelling = value
            setParamVal("TrainingRequireForForeignTravelling", mblnTrainingRequireForForeignTravelling)
        End Set
    End Property

    'Pinkal (01-Feb-2021) -- End

    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Private mintUnRetiredImprestToPayrollAfterDays As Integer = 0
    Public Property _UnRetiredImprestToPayrollAfterDays() As Integer
        Get
            Return mintUnRetiredImprestToPayrollAfterDays
        End Get
        Set(ByVal value As Integer)
            mintUnRetiredImprestToPayrollAfterDays = value
            setParamVal("UnRetiredImprestToPayrollAfterDays", mintUnRetiredImprestToPayrollAfterDays)
        End Set
    End Property
    'Pinkal (10-Feb-2021) -- End

    'Gajanan [4-May-2020] -- Start
    Private mstrAppointeDateUserNotification As String = ""
    Public Property _AppointeDateUserNotification() As String
        Get
            Return mstrAppointeDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrAppointeDateUserNotification = value
            setParamVal("AppointeDateUserNotification", mstrAppointeDateUserNotification)
        End Set
    End Property

    Private mstrConfirmDateUserNotification As String = ""
    Public Property _ConfirmDateUserNotification() As String
        Get
            Return mstrConfirmDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrConfirmDateUserNotification = value
            setParamVal("ConfirmDateUserNotification", mstrConfirmDateUserNotification)
        End Set
    End Property

    Private mstrBirthDateUserNotification As String = ""
    Public Property _BirthDateUserNotification() As String
        Get
            Return mstrBirthDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrBirthDateUserNotification = value
            setParamVal("BirthDateUserNotification", mstrBirthDateUserNotification)
        End Set
    End Property

    Private mstrSuspensionDateUserNotification As String = ""
    Public Property _SuspensionDateUserNotification() As String
        Get
            Return mstrSuspensionDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrSuspensionDateUserNotification = value
            setParamVal("SuspensionDateUserNotification", mstrSuspensionDateUserNotification)
        End Set
    End Property

    Private mstrProbationDateUserNotification As String = ""
    Public Property _ProbationDateUserNotification() As String
        Get
            Return mstrProbationDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrProbationDateUserNotification = value
            setParamVal("ProbationDateUserNotification", mstrProbationDateUserNotification)
        End Set
    End Property

    Private mstrEocDateUserNotification As String = ""
    Public Property _EocDateUserNotification() As String
        Get
            Return mstrEocDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrEocDateUserNotification = value
            setParamVal("EocDateUserNotification", mstrEocDateUserNotification)
        End Set
    End Property

    Private mstrLeavingDateUserNotification As String = ""
    Public Property _LeavingDateUserNotification() As String
        Get
            Return mstrLeavingDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrLeavingDateUserNotification = value
            setParamVal("LeavingDateUserNotification", mstrLeavingDateUserNotification)
        End Set
    End Property

    Private mstrRetirementDateUserNotification As String = ""
    Public Property _RetirementDateUserNotification() As String
        Get
            Return mstrRetirementDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrRetirementDateUserNotification = value
            setParamVal("RetirementDateUserNotification", mstrRetirementDateUserNotification)
        End Set
    End Property

    Private mstrReinstatementDateUserNotification As String = ""
    Public Property _ReinstatementDateUserNotification() As String
        Get
            Return mstrReinstatementDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrReinstatementDateUserNotification = value
            setParamVal("ReinstatementDateUserNotification", mstrReinstatementDateUserNotification)
        End Set
    End Property

    Private mstrMarriageDateUserNotification As String = ""
    Public Property _MarriageDateUserNotification() As String
        Get
            Return mstrMarriageDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrMarriageDateUserNotification = value
            setParamVal("MarriageDateUserNotification", mstrMarriageDateUserNotification)
        End Set
    End Property

    Private mstrExemptionDateUserNotification As String = ""
    Public Property _ExemptionDateUserNotification() As String
        Get
            Return mstrExemptionDateUserNotification
        End Get
        Set(ByVal value As String)
            mstrExemptionDateUserNotification = value
            setParamVal("ExemptionDateUserNotification", mstrExemptionDateUserNotification)
        End Set
    End Property
    'Gajanan [4-May-2020] -- End

    'Hemant (28 Jul 2021) -- Start             
    'ENHANCEMENT : OLD-293 - Training Evaluation
    Private mblnPreTrainingEvaluationSubmitted As Boolean = False
    Public Property _PreTrainingEvaluationSubmitted() As Boolean
        Get
            Return mblnPreTrainingEvaluationSubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnPreTrainingEvaluationSubmitted = value
            setParamVal("PreTrainingEvaluationSubmitted", mblnPreTrainingEvaluationSubmitted)
        End Set
    End Property
    'Hemant (28 Jul 2021) -- End
    'Hemant (04 Sep 2021) -- Start
    'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
    Private mstrPreTrainingFeedbackInstruction As String = ""
    Public Property _PreTrainingFeedbackInstruction() As String
        Get
            Return mstrPreTrainingFeedbackInstruction
        End Get
        Set(ByVal value As String)
            mstrPreTrainingFeedbackInstruction = value
            setParamVal("PreTrainingFeedbackInstruction", mstrPreTrainingFeedbackInstruction)
        End Set
    End Property

    Private mstrPostTrainingFeedbackInstruction As String = ""
    Public Property _PostTrainingFeedbackInstruction() As String
        Get
            Return mstrPostTrainingFeedbackInstruction
        End Get
        Set(ByVal value As String)
            mstrPostTrainingFeedbackInstruction = value
            setParamVal("PostTrainingFeedbackInstruction", mstrPostTrainingFeedbackInstruction)
        End Set
    End Property

    Private mstrDaysAfterTrainingFeedbackInstruction As String = ""
    Public Property _DaysAfterTrainingFeedbackInstruction() As String
        Get
            Return mstrDaysAfterTrainingFeedbackInstruction
        End Get
        Set(ByVal value As String)
            mstrDaysAfterTrainingFeedbackInstruction = value
            setParamVal("DaysAfterTrainingFeedbackInstruction", mstrDaysAfterTrainingFeedbackInstruction)
        End Set
    End Property
    'Hemant (04 Sep 2021) -- End

    'Hemant (23 Sep 2021) -- Start
    'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
    Private mblnSkipTrainingRequisitionAndApproval As Boolean = False
    Public Property _SkipTrainingRequisitionAndApproval() As Boolean
        Get
            Return mblnSkipTrainingRequisitionAndApproval
        End Get
        Set(ByVal value As Boolean)
            mblnSkipTrainingRequisitionAndApproval = value
            setParamVal("SkipTrainingRequisitionAndApproval", mblnSkipTrainingRequisitionAndApproval)
        End Set
    End Property
    'Hemant (23 Sep 2021) -- End

    'Hemant (24 Nov 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
    Private mblnSkipEOCValidationOnLoanTenure As Boolean = False
    Public Property _SkipEOCValidationOnLoanTenure() As Boolean
        Get
            Return mblnSkipEOCValidationOnLoanTenure
        End Get
        Set(ByVal value As Boolean)
            mblnSkipEOCValidationOnLoanTenure = value
            setParamVal("SkipEOCValidationOnLoanTenure", mblnSkipEOCValidationOnLoanTenure)
        End Set
    End Property
    'Hemant (24 Nov 2021) -- End

    'Hemant (09 Feb 2022) -- Start            
    'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
    Private mblnPostTrainingEvaluationBeforeCompleteTraining As Boolean = False
    Public Property _PostTrainingEvaluationBeforeCompleteTraining() As Boolean
        Get
            Return mblnPostTrainingEvaluationBeforeCompleteTraining
        End Get
        Set(ByVal value As Boolean)
            mblnPostTrainingEvaluationBeforeCompleteTraining = value
            setParamVal("PostTrainingEvaluationBeforeCompleteTraining", mblnPostTrainingEvaluationBeforeCompleteTraining)
        End Set
    End Property
    'Hemant (09 Feb 2022) -- End

#End Region


#Region " Display Settings "
    '-- Country
    Private mintCountryUnkid As Integer = 0
    '-- Currency
    Private mstrCurrencySign As String = ""
    '-- Captions
    Private mstrReg1Caption As String = "Registration No1"
    Private mstrReg2Caption As String = "Registration No2"
    Private mstrReg3Caption As String = "Registration No3"

    '** Properties *************
    ''' <summary>
    ''' Indicate The Local Country
    ''' </summary>
    Public Property _CountryUnkid() As Integer
        Get
            Return mintCountryUnkid
        End Get
        Set(ByVal intCountryUnkid As Integer)
            mintCountryUnkid = intCountryUnkid
            setParamVal("CountryUnkid", mintCountryUnkid)
        End Set
    End Property

    ''' <summary>
    ''' Indicate The Local Currency
    ''' </summary>
    Public Property _Currency() As String
        Get
            Return mstrCurrencySign
        End Get
        Set(ByVal strCurrencySign As String)
            mstrCurrencySign = strCurrencySign
            setParamVal("CurrencySign", mstrCurrencySign)
        End Set
    End Property

    ''' <summary>
    ''' Indicates The Registration1 Caption
    ''' </summary>
    Public Property _Reg1Caption() As String
        Get
            Return mstrReg1Caption
        End Get
        Set(ByVal strReg1Caption As String)
            mstrReg1Caption = strReg1Caption
            setParamVal("Reg1Caption", mstrReg1Caption)
        End Set
    End Property

    ''' <summary>
    ''' Indicates The Registration2 Caption
    ''' </summary>
    Public Property _Reg2Caption() As String
        Get
            Return mstrReg2Caption
        End Get
        Set(ByVal strReg2Caption As String)
            mstrReg2Caption = strReg2Caption
            setParamVal("Reg2Caption", mstrReg2Caption)
        End Set
    End Property

    ''' <summary>
    ''' Indicates The Registration3 Caption
    ''' </summary>
    Public Property _Reg3Caption() As String
        Get
            Return mstrReg3Caption
        End Get
        Set(ByVal strReg3Caption As String)
            mstrReg3Caption = strReg3Caption
            setParamVal("Reg3Caption", mstrReg3Caption)
        End Set
    End Property

#End Region

#Region " Paths Settings "
    '-- General Path
    Private mstrPhotoPath As String = ""
    Private mstrIdentifyPath As String = ""
    Private mstrCertificatePath As String = ""
    Private mstrOtherPath As String = ""
    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrDocumentPath As String = ""
    'S.SANDEEP [ 18 FEB 2012 ] -- START

    'S.SANDEEP [ 28 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsImgInDataBase As Boolean = False
    'S.SANDEEP [ 28 MAR 2013 ] -- END

    '-- Report Settings
    Private mstrExportReportPath As String = ""
    Private mblnOpenAfterExport As Boolean = False
    '-- Database Export Path
    Private mstrDatabaseExportPath As String = ""
    '-- Export Data Path
    Private mstrExportDataPath As String = ""


    'Pinkal (24-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrConfigurationExportDataPath As String = ""
    'Pinkal (24-Jan-2013) -- End



    '** Properties *************
    ''' <summary>
    ''' Indicates Photo Path
    ''' </summary>
    Public Property _PhotoPath() As String
        Get
            Return mstrPhotoPath
        End Get
        Set(ByVal strPhotoPath As String)
            mstrPhotoPath = strPhotoPath
            setParamVal("PhotoPath", mstrPhotoPath)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Identity Path
    ''' </summary>
    Public Property _IdentityPath() As String
        Get
            Return mstrIdentifyPath
        End Get
        Set(ByVal strIdentifyPath As String)
            mstrIdentifyPath = strIdentifyPath
            setParamVal("IdentityPath", mstrIdentifyPath)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Certificate Path
    ''' </summary>
    Public Property _CertificatePath() As String
        Get
            Return mstrCertificatePath
        End Get
        Set(ByVal strCertificatePath As String)
            mstrCertificatePath = strCertificatePath
            setParamVal("CertificatePath", mstrCertificatePath)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Other Path
    ''' </summary>
    Public Property _OtherPath() As String
        Get
            Return mstrOtherPath
        End Get
        Set(ByVal strOtherPath As String)
            mstrOtherPath = strOtherPath
            setParamVal("OtherPath", mstrOtherPath)
        End Set
    End Property

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Indicates Documents Path
    ''' </summary>
    Public Property _Document_Path() As String
        Get
            Return mstrDocumentPath
        End Get
        Set(ByVal strDocumentPath As String)
            mstrDocumentPath = strDocumentPath
            setParamVal("DocumentPath", mstrDocumentPath)
        End Set
    End Property
    'S.SANDEEP [ 18 FEB 2012 ] -- START



    ''' <summary>
    ''' Indicates Export Report Path
    ''' </summary>
    Public Property _ExportReportPath() As String
        Get
            Return mstrExportReportPath
        End Get
        Set(ByVal strExportReportPath As String)
            mstrExportReportPath = strExportReportPath
            setParamVal("ExportReportPath", mstrExportReportPath)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Open After Export
    ''' </summary>
    Public Property _OpenAfterExport() As Boolean
        Get
            Return mblnOpenAfterExport
        End Get
        Set(ByVal blnOpenAfterExport As Boolean)
            mblnOpenAfterExport = blnOpenAfterExport
            setParamVal("OpenAfterExport", mblnOpenAfterExport)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Database Export Path
    ''' </summary>
    Public Property _DatabaseExportPath() As String
        Get
            Return mstrDatabaseExportPath
        End Get
        Set(ByVal strDatabaseExportPath As String)
            mstrDatabaseExportPath = strDatabaseExportPath
            setParamVal("DatabaseExportPath", mstrDatabaseExportPath)
        End Set
    End Property

    ''' <summary>
    ''' Indicates Export  Data Path
    ''' </summary>
    Public Property _ExportDataPath() As String
        Get
            Return mstrExportDataPath
        End Get
        Set(ByVal strExportDataPath As String)
            mstrExportDataPath = strExportDataPath
            setParamVal("ExportDataPath", mstrExportDataPath)
        End Set
    End Property


    'Pinkal (24-Jan-2013) -- Start
    'Enhancement : TRA Changes
    ''' <summary>
    ''' Indicates Configuration Database Export Path
    ''' </summary>
    Public Property _ConfigDatabaseExportPath() As String
        Get
            Return mstrConfigurationExportDataPath
        End Get
        Set(ByVal strConfigDatabaseExportPath As String)
            mstrConfigurationExportDataPath = strConfigDatabaseExportPath
            setParamVal("ConfigDatabaseExportPath", mstrConfigurationExportDataPath)
        End Set
    End Property
    'Pinkal (24-Jan-2013) -- End


    'S.SANDEEP [ 28 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property _IsImgInDataBase() As Boolean
        Get
            Return mblnIsImgInDataBase
        End Get
        Set(ByVal value As Boolean)
            mblnIsImgInDataBase = value
            setParamVal("IsImgInDataBase", mblnIsImgInDataBase)
        End Set
    End Property
    'S.SANDEEP [ 28 MAR 2013 ] -- END

#End Region

#Region " Integration "
    '-- Accouting S/W
    Private mintAccountingSoftWare As Integer = 0


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    'Private mintCommunicationTypeId As Integer = -1
    'Private mintDeviceId As Integer = 0
    'Private mintConnectionTypeId As Integer = -1
    'Private mstrIPAddress As String = ""
    'Private mstrPortNo As String = ""
    'Private mstrBaudRate As String = ""
    'Private mstrMachineSr As String = ""

    'Pinkal (20-Jan-2012) -- End

    'Anjan (21 Jul 2011)-Start
    'Issue : Removing formula and setting total manually.
    Private mintWebClientCode As Integer = 0
    Private mstrAuthenticationCode As String = String.Empty
    Private mstrWebURL As String = String.Empty
    'Anjan (21 Jul 2011)-End 
    Private mstrWebURLInternal As String = String.Empty 'Sohail (30 Dec 2011)

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrEditAddress As Boolean = False
    Private mstrEditPersonalInfo As Boolean = False
    Private mstrEditEmergencyAddress As Boolean = False
    Private mstrAddIdentity As Boolean = False
    Private mstrEditIdentity As Boolean = False
    Private mstrDeleteIdentity As Boolean = False
    Private mstrAddDependants As Boolean = False
    Private mstrEditDependants As Boolean = False
    Private mstrDeleteDependants As Boolean = False
    Private mstrAddMembership As Boolean = False
    Private mstrEditMembership As Boolean = False
    Private mstrDeleteMembership As Boolean = False
    Private mstrAddSkills As Boolean = False
    Private mstrEditSkills As Boolean = False
    Private mstrDeleteSkills As Boolean = False
    Private mstrAddQualification As Boolean = False
    Private mstrEditQualification As Boolean = False
    Private mstrDeleteQualification As Boolean = False
    Private mstrAddExperience As Boolean = False
    Private mstrEditExperience As Boolean = False
    Private mstrDeleteExperience As Boolean = False
    Private mstrAddReference As Boolean = False
    Private mstrEditReference As Boolean = False
    Private mstrDeleteReference As Boolean = False
    'Anjan (02 Mar 2012)-End 

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrAppDeclaration As String = String.Empty
    'Anjan (02 Mar 2012)-End 

    'Sohail (07 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAccounting_TransactionReference As String = String.Empty
    Private mstrAccounting_JournalType As String = String.Empty
    Private mstrAccounting_JVGroupCode As String = String.Empty
    'Sohail (07 Aug 2012) -- End
    'Sohail (19 Feb 2016) -- Start
    'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
    Private mstrAccounting_Country As String = String.Empty
    'Sohail (19 Feb 2016) -- End

    'Hemant (15 Mar 2019) -- Start
    'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
    Private mstrB_Area As String = String.Empty
    'Hemant (15 Mar 2019) -- End

    'Sohail (25 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintEFTIntegration As Integer = 0
    Private mstrEFTCode As String = String.Empty
    Private mintPSPFIntegration As Integer = 0 'Sohail (25 Jan 2013) -- End
    'Sohail (14 Mar 2013)
    Private mintMobileMoneyEFTIntegration As Integer = 0 'Sohail (08 Dec 2014)

    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnAllowtoAddEditImgForESS As Boolean = False
    'Pinkal (01-Apr-2013) -- End

    'Sohail (30 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrSMimeRunPath As String = ""
    Private mstrDatafileExportPath As String = ""
    Private mstrDatafileName As String = ""
    'Sohail (30 Apr 2013) -- End
    'Sohail (09 Jan 2016) -- Start
    'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
    Private mstrEFTCitiDirectCountryCode As String = ""
    Private mblnEFTCitiDirectSkipPriorityFlag As Boolean = False
    Private mstrEFTCitiDirectChargesIndicator As String = ""
    Private mblnEFTCitiDirectAddPaymentDetail As Boolean = False
    'Sohail (09 Jan 2016) -- End

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118
    Private mblnAllowtoViewSignatureESS As Boolean = False
    'S.SANDEEP [16-Jan-2018] -- END
    'Hemant (19 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
    Private mblnDontAllowAdvanceOnESS As Boolean = False
    'Hemant (19 Aug 2019) -- End
    'Hemant (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2
    Private mintAssetDeclarationTemplate As Integer = Convert.ToInt16(enAsset_Declaration_Template.Template1)
    'Hemant (06 Oct 2018) -- End

    ''' <summary>
    ''' Indicates Accounting Integration S/W
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _AccountingSoftWare() As Integer
        Get
            Return mintAccountingSoftWare
        End Get
        Set(ByVal intAccountingSoftWare As Integer)
            mintAccountingSoftWare = intAccountingSoftWare
            setParamVal("AccountingSoftWare", mintAccountingSoftWare)
        End Set
    End Property



    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    '''' <summary>
    '''' Indicates CommunicationTypeId
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _CommunicationTypeId() As Integer
    '    Get
    '        Return mintCommunicationTypeId
    '    End Get
    '    Set(ByVal intCommunicationTypeId As Integer)
    '        mintCommunicationTypeId = intCommunicationTypeId
    '        setParamVal("CommTypeId", mintCommunicationTypeId)
    '    End Set
    'End Property

    '''' <summary>
    '''' Indicates DeviceId
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _DeviceId() As Integer
    '    Get
    '        Return mintDeviceId
    '    End Get
    '    Set(ByVal intDeviceId As Integer)
    '        mintDeviceId = intDeviceId
    '        setParamVal("DeviceId", mintDeviceId)
    '    End Set
    'End Property

    '''' <summary>
    '''' Indicates ConnectionTypeId
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _ConnectionTypeId() As Integer
    '    Get
    '        Return mintConnectionTypeId
    '    End Get
    '    Set(ByVal intConnetionTypeID As Integer)
    '        mintConnectionTypeId = intConnetionTypeID
    '        setParamVal("ConnectionTypeId", mintConnectionTypeId)
    '    End Set
    'End Property

    '''' <summary>
    '''' Indicates IpAddress
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _IpAddress() As String
    '    Get
    '        Return mstrIPAddress
    '    End Get
    '    Set(ByVal strIpAddress As String)
    '        mstrIPAddress = strIpAddress
    '        setParamVal("IpAddress", mstrIPAddress)
    '    End Set
    'End Property

    '''' <summary>
    '''' Indicates PortNo
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _PortNo() As String
    '    Get
    '        Return mstrPortNo
    '    End Get
    '    Set(ByVal strPortNo As String)
    '        mstrPortNo = strPortNo
    '        setParamVal("PortNo", mstrPortNo)
    '    End Set
    'End Property

    '''' <summary>
    '''' Indicates BaudRate
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _BaudRate() As String
    '    Get
    '        Return mstrBaudRate
    '    End Get
    '    Set(ByVal strBaudRate As String)
    '        mstrBaudRate = strBaudRate
    '        setParamVal("BaudRate", mstrBaudRate)
    '    End Set
    'End Property

    '''' <summary>
    '''' Indicates MachineSr
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property _MachineSr() As String
    '    Get
    '        Return mstrMachineSr
    '    End Get
    '    Set(ByVal strMachineSr As String)
    '        mstrMachineSr = strMachineSr
    '        setParamVal("MachineSr", mstrMachineSr)
    '    End Set
    'End Property

    'Pinkal (20-Jan-2012) -- End

    'Anjan (21 Jul 2011)-Start
    'purpose : Included web settings.
    Public Property _WebClientCode() As Integer
        Get
            Return mintWebClientCode
        End Get
        Set(ByVal intWebClientCode As Integer)
            mintWebClientCode = intWebClientCode
            setParamVal("WebClientCode", mintWebClientCode)
        End Set
    End Property

    Public Property _AuthenticationCode() As String
        Get
            Return mstrAuthenticationCode
        End Get
        Set(ByVal strAuthenticationCode As String)
            mstrAuthenticationCode = strAuthenticationCode
            setParamVal("AuthenticationCode", mstrAuthenticationCode)

        End Set
    End Property

    Public Property _WebURL() As String
        Get
            Return mstrWebURL

        End Get
        Set(ByVal strWebURL As String)
            mstrWebURL = strWebURL
            setParamVal("WebURL", mstrWebURL)
        End Set
    End Property
    'Anjan (21 Jul 2011)-End 

    'Sohail (30 Dec 2011) -- Start
    Public Property _WebURLInternal() As String
        Get
            Return mstrWebURLInternal

        End Get
        Set(ByVal strWebURLInternal As String)
            mstrWebURLInternal = strWebURLInternal
            setParamVal("WebURLInternal", mstrWebURLInternal)
        End Set
    End Property
    'Sohail (30 Dec 2011) -- End

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public Property _AllowEditAddress() As Boolean
        Get
            Return mstrEditAddress
        End Get
        Set(ByVal strEditAddress As Boolean)
            mstrEditAddress = strEditAddress
            setParamVal("EDITADDRESS", mstrEditAddress)
        End Set
    End Property

    Public Property _AllowEditPersonalInfo() As Boolean
        Get
            Return mstrEditPersonalInfo
        End Get
        Set(ByVal strEditPersonalInfo As Boolean)
            mstrEditPersonalInfo = strEditPersonalInfo
            setParamVal("EDITPERSONALINFO", mstrEditPersonalInfo)
        End Set
    End Property

    Public Property _AllowEditEmergencyAddress() As Boolean
        Get
            Return mstrEditEmergencyAddress
        End Get
        Set(ByVal strEditEmergencyAddress As Boolean)
            mstrEditEmergencyAddress = strEditEmergencyAddress
            setParamVal("EDITEMERGENCYADDRESS", mstrEditEmergencyAddress)
        End Set
    End Property

    Public Property _AllowAddIdentity() As Boolean
        Get
            Return mstrAddIdentity
        End Get
        Set(ByVal strAddIdentity As Boolean)
            mstrAddIdentity = strAddIdentity
            setParamVal("ADDIDENTITY", mstrAddIdentity)
        End Set
    End Property

    Public Property _AllowEditIdentity() As Boolean
        Get
            Return mstrEditIdentity
        End Get
        Set(ByVal strEditIdentity As Boolean)
            mstrEditIdentity = strEditIdentity
            setParamVal("EDITIDENTITY", mstrEditIdentity)
        End Set
    End Property

    Public Property _AllowDeleteIdentity() As Boolean
        Get
            Return mstrDeleteIdentity
        End Get
        Set(ByVal strDeleteIdentity As Boolean)
            mstrDeleteIdentity = strDeleteIdentity
            setParamVal("DELETEIDENTITY", mstrDeleteIdentity)
        End Set
    End Property

    Public Property _AllowAddDependants() As Boolean
        Get
            Return mstrAddDependants
        End Get
        Set(ByVal strAddDependants As Boolean)
            mstrAddDependants = strAddDependants
            setParamVal("ADDDEPENDANTS", mstrAddDependants)
        End Set
    End Property

    Public Property _AllowEditDependants() As Boolean
        Get
            Return mstrEditDependants
        End Get
        Set(ByVal strEditDependants As Boolean)
            mstrEditDependants = strEditDependants
            setParamVal("EDITDEPENDANTS", mstrEditDependants)
        End Set
    End Property

    Public Property _AllowDeleteDependants() As Boolean
        Get
            Return mstrDeleteDependants
        End Get
        Set(ByVal strDeleteDependants As Boolean)
            mstrDeleteDependants = strDeleteDependants
            setParamVal("DELETEDEPENDANTS", mstrDeleteDependants)
        End Set
    End Property


    Public Property _AllowAddMembership() As Boolean
        Get
            Return mstrAddMembership
        End Get
        Set(ByVal strAddMembership As Boolean)
            mstrAddMembership = strAddMembership
            setParamVal("ADDMEMBERSHIP", mstrAddMembership)
        End Set
    End Property

    Public Property _AllowEditMembership() As Boolean
        Get
            Return mstrEditMembership
        End Get
        Set(ByVal strEditMembership As Boolean)
            mstrEditMembership = strEditMembership
            setParamVal("EDITMEMBERSHIP", mstrEditMembership)
        End Set
    End Property

    Public Property _AllowDeleteMembership() As Boolean
        Get
            Return mstrDeleteMembership
        End Get
        Set(ByVal strDeleteMembership As Boolean)
            mstrDeleteMembership = strDeleteMembership
            setParamVal("DELETEMEMBERSHIP", mstrDeleteMembership)
        End Set
    End Property

    Public Property _AllowAddSkills() As Boolean
        Get
            Return mstrEditSkills
        End Get
        Set(ByVal strAddSkills As Boolean)
            mstrAddSkills = strAddSkills
            setParamVal("ADDSKILLS", mstrAddSkills)
        End Set
    End Property

    Public Property _AllowEditSkills() As Boolean
        Get
            Return mstrEditSkills
        End Get
        Set(ByVal strEditSkills As Boolean)
            mstrEditSkills = strEditSkills
            setParamVal("EDITSKILLS", mstrEditSkills)
        End Set
    End Property

    Public Property _AllowDeleteSkills() As Boolean
        Get
            Return mstrDeleteSkills
        End Get
        Set(ByVal strDeleteSkills As Boolean)
            mstrDeleteSkills = strDeleteSkills
            setParamVal("DELETESKILLS", mstrDeleteSkills)
        End Set
    End Property

    Public Property _AllowAddQualifications() As Boolean
        Get
            Return mstrAddQualification
        End Get
        Set(ByVal strAddQualification As Boolean)
            mstrAddQualification = strAddQualification
            setParamVal("ADDQUALIFICATION", mstrAddQualification)
        End Set
    End Property

    Public Property _AllowEditQualifications() As Boolean
        Get
            Return mstrEditQualification
        End Get
        Set(ByVal strAddQualification As Boolean)
            mstrAddQualification = strAddQualification
            setParamVal("EDITQUALIFICATION", mstrAddQualification)
        End Set
    End Property

    Public Property _AllowDeleteQualifications() As Boolean
        Get
            Return mstrDeleteQualification
        End Get
        Set(ByVal strDeleteQualification As Boolean)
            mstrDeleteQualification = strDeleteQualification
            setParamVal("DELETEQUALIFICATION", mstrDeleteQualification)
        End Set
    End Property

    Public Property _AllowAddExperience() As Boolean
        Get
            Return mstrAddExperience
        End Get
        Set(ByVal strAddExperience As Boolean)
            mstrAddExperience = strAddExperience
            setParamVal("ADDEXPERIENCE", mstrAddExperience)
        End Set
    End Property

    Public Property _AllowEditExperience() As Boolean
        Get
            Return mstrEditExperience
        End Get
        Set(ByVal strEditExperience As Boolean)
            mstrEditExperience = strEditExperience
            setParamVal("EDITEXPERIENCE", mstrEditExperience)
        End Set
    End Property

    Public Property _AllowDeleteExperience() As Boolean
        Get
            Return mstrDeleteExperience
        End Get
        Set(ByVal strDeleteExperience As Boolean)
            mstrDeleteExperience = strDeleteExperience
            setParamVal("DELETEEXPERIENCE", mstrDeleteExperience)
        End Set
    End Property

    Public Property _AllowAddReference() As Boolean
        Get
            Return mstrAddReference
        End Get
        Set(ByVal strAddReference As Boolean)
            mstrAddReference = strAddReference
            setParamVal("ADDREFERENCE", mstrAddReference)
        End Set
    End Property

    Public Property _AllowEditReference() As Boolean
        Get
            Return mstrEditReference
        End Get
        Set(ByVal strEditReference As Boolean)
            mstrEditReference = strEditReference
            setParamVal("EDITREFERENCE", mstrEditReference)
        End Set
    End Property

    Public Property _AllowDeleteReference() As Boolean
        Get
            Return mstrDeleteReference
        End Get
        Set(ByVal strDeleteReference As Boolean)
            mstrDeleteReference = strDeleteReference
            setParamVal("DELETEREFERENCE", mstrDeleteReference)
        End Set
    End Property

    'S.SANDEEP [ 06 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnAllowChangeCompanyEmail As Boolean = False
    Public Property _AllowChangeCompanyEmail() As Boolean
        Get
            Return mblnAllowChangeCompanyEmail
        End Get
        Set(ByVal value As Boolean)
            mblnAllowChangeCompanyEmail = value
            setParamVal("CHANGECOMPANYEMAIL", mblnAllowChangeCompanyEmail)
        End Set
    End Property
    'S.SANDEEP [ 06 DEC 2012 ] -- END

    'Anjan (02 Mar 2012)-End 
    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public Property _ApplicantDeclaration() As String
        Get
            Return mstrAppDeclaration
        End Get
        Set(ByVal strAppDeclaration As String)
            mstrAppDeclaration = strAppDeclaration
            setParamVal("APPLICANTDECLARATION", mstrAppDeclaration)
        End Set
    End Property
    'Anjan (02 Mar 2012)-End 

    'Sohail (07 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Accounting_TransactionReference() As String
        Get
            Return mstrAccounting_TransactionReference
        End Get
        Set(ByVal strAccounting_TransactionReference As String)
            mstrAccounting_TransactionReference = strAccounting_TransactionReference
            setParamVal("Accounting_TransactionReference", mstrAccounting_TransactionReference)
        End Set
    End Property

    Public Property _Accounting_JournalType() As String
        Get
            Return mstrAccounting_JournalType
        End Get
        Set(ByVal strAccounting_JournalType As String)
            mstrAccounting_JournalType = strAccounting_JournalType
            setParamVal("Accounting_JournalType", mstrAccounting_JournalType)
        End Set
    End Property

    Public Property _Accounting_JVGroupCode() As String
        Get
            Return mstrAccounting_JVGroupCode
        End Get
        Set(ByVal strAccounting_JVGroupCode As String)
            mstrAccounting_JVGroupCode = strAccounting_JVGroupCode
            setParamVal("Accounting_JVGroupCode", mstrAccounting_JVGroupCode)
        End Set
    End Property
    'Sohail (07 Aug 2012) -- End

    'Sohail (19 Feb 2016) -- Start
    'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
    Public Property _Accounting_Country() As String
        Get
            Return mstrAccounting_Country
        End Get
        Set(ByVal strAccounting_Country As String)
            mstrAccounting_Country = strAccounting_Country
            setParamVal("Accounting_Country", mstrAccounting_Country)
        End Set
    End Property
    'Sohail (19 Feb 2016) -- End

    'Hemant (15 Mar 2019) -- Start
    'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
    Public Property _B_Area() As String
        Get
            Return mstrB_Area
        End Get
        Set(ByVal strB_Area As String)
            mstrB_Area = strB_Area
            setParamVal("B_Area", mstrB_Area)
        End Set
    End Property
    'Hemant (15 Mar 2019) -- End

    'Sohail (25 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _EFTIntegration() As Integer
        Get
            Return mintEFTIntegration
        End Get
        Set(ByVal intEFTIntegration As Integer)
            mintEFTIntegration = intEFTIntegration
            setParamVal("EFTIntegration", mintEFTIntegration)
        End Set
    End Property

    Public Property _EFT_Code() As String
        Get
            Return mstrEFTCode
        End Get
        Set(ByVal strEFTCode As String)
            mstrEFTCode = strEFTCode
            setParamVal("EFTCode", mstrEFTCode)
        End Set
    End Property
    'Sohail (25 Jan 2013) -- End

    'Sohail (14 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _PSPFIntegration() As Integer
        Get
            Return mintPSPFIntegration
        End Get
        Set(ByVal intPSPFIntegration As Integer)
            mintPSPFIntegration = intPSPFIntegration
            setParamVal("PSPFIntegration", mintPSPFIntegration)
        End Set
    End Property
    'Sohail (14 Mar 2013) -- End

    'Sohail (08 Dec 2014) -- Start
    'Enhancement - New Mobile Money EFT integration with MPESA.
    Public Property _MobileMoneyEFTIntegration() As Integer
        Get
            Return mintMobileMoneyEFTIntegration
        End Get
        Set(ByVal intMobileMoneyEFTIntegration As Integer)
            mintMobileMoneyEFTIntegration = intMobileMoneyEFTIntegration
            setParamVal("MobileMoneyEFTIntegration", mintMobileMoneyEFTIntegration)
        End Set
    End Property
    'Sohail (08 Dec 2014) -- End

    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mblnAllowToViewPersonalSalaryCalculationReport As Boolean = False
    Public Property _AllowToViewPersonalSalaryCalculationReport() As Boolean
        Get
            Return mblnAllowToViewPersonalSalaryCalculationReport
        End Get
        Set(ByVal blnAllowToViewPersonalSalaryCalculationReport As Boolean)
            mblnAllowToViewPersonalSalaryCalculationReport = blnAllowToViewPersonalSalaryCalculationReport
            setParamVal("VIEWPERSONALSALARYCALCULATIONREPORT", mblnAllowToViewPersonalSalaryCalculationReport)
        End Set
    End Property

    Private mblnAllowToViewEDDetailReport As Boolean = False
    Public Property _AllowToViewEDDetailReport() As Boolean
        Get
            Return mblnAllowToViewEDDetailReport
        End Get
        Set(ByVal blnAllowToViewEDDetailReport As Boolean)
            mblnAllowToViewEDDetailReport = blnAllowToViewEDDetailReport
            setParamVal("VIEWEDDETAILREPORT", mblnAllowToViewEDDetailReport)
        End Set
    End Property
    'Sohail (25 Mar 2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public Property _AllowToAddEditImageForESS() As Boolean
        Get
            Return mblnAllowtoAddEditImgForESS
        End Get
        Set(ByVal blnAllowtoAddEditImgForESS As Boolean)
            mblnAllowtoAddEditImgForESS = blnAllowtoAddEditImgForESS
            setParamVal("AddEditImageForESS", mblnAllowtoAddEditImgForESS)
        End Set
    End Property

    'Pinkal (01-Apr-2013) -- End

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118
    Public Property _AllowtoViewSignatureESS() As Boolean
        Get
            Return mblnAllowtoViewSignatureESS
        End Get
        Set(ByVal value As Boolean)
            mblnAllowtoViewSignatureESS = value
            setParamVal("AllowtoViewSignatureESS", mblnAllowtoViewSignatureESS)
        End Set
    End Property
    'S.SANDEEP [16-Jan-2018] -- END
    'Hemant (19 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
    Public Property _DontAllowAdvanceOnESS() As Boolean
        Get
            Return mblnDontAllowAdvanceOnESS
        End Get
        Set(ByVal value As Boolean)
            mblnDontAllowAdvanceOnESS = value
            setParamVal("DontAllowAdvanceOnESS", mblnDontAllowAdvanceOnESS)
        End Set
    End Property
    'Hemant (19 Aug 2019) -- End

    'Sohail (30 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _SMimeRunPath() As String
        Get
            Return mstrSMimeRunPath
        End Get
        Set(ByVal strSMimeRunPath As String)
            mstrSMimeRunPath = strSMimeRunPath
            setParamVal("SMimeRunPath", mstrSMimeRunPath)
        End Set
    End Property

    Public Property _DatafileExportPath() As String
        Get
            Return mstrDatafileExportPath
        End Get
        Set(ByVal strDatafileExportPath As String)
            mstrDatafileExportPath = strDatafileExportPath
            setParamVal("DatafileExportPath", mstrDatafileExportPath)
        End Set
    End Property

    Public Property _DatafileName() As String
        Get
            Return mstrDatafileName
        End Get
        Set(ByVal strDatafileName As String)
            mstrDatafileName = strDatafileName
            setParamVal("DatafileName", mstrDatafileName)
        End Set
    End Property
    'Sohail (30 Apr 2013) -- End

    'Sohail (09 Jan 2016) -- Start
    'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
    Public Property _EFTCitiDirectCountryCode() As String
        Get
            Return mstrEFTCitiDirectCountryCode
        End Get
        Set(ByVal strEFTCitiDirectCountryCode As String)
            mstrEFTCitiDirectCountryCode = strEFTCitiDirectCountryCode
            setParamVal("EFTCitiDirectCountryCode", mstrEFTCitiDirectCountryCode)
        End Set
    End Property

    Public Property _EFTCitiDirectSkipPriorityFlag() As Boolean
        Get
            Return mblnEFTCitiDirectSkipPriorityFlag
        End Get
        Set(ByVal blnEFTCitiDirectSkipPriorityFlag As Boolean)
            mblnEFTCitiDirectSkipPriorityFlag = blnEFTCitiDirectSkipPriorityFlag
            setParamVal("EFTCitiDirectSkipPriorityFlag", mblnEFTCitiDirectSkipPriorityFlag)
        End Set
    End Property

    Public Property _EFTCitiDirectChargesIndicator() As String
        Get
            Return mstrEFTCitiDirectChargesIndicator
        End Get
        Set(ByVal strEFTCitiDirectChargesIndicator As String)
            mstrEFTCitiDirectChargesIndicator = strEFTCitiDirectChargesIndicator
            setParamVal("EFTCitiDirectChargesIndicator", mstrEFTCitiDirectChargesIndicator)
        End Set
    End Property

    Public Property _EFTCitiDirectAddPaymentDetail() As Boolean
        Get
            Return mblnEFTCitiDirectAddPaymentDetail
        End Get
        Set(ByVal blnEFTCitiDirectAddPaymentDetail As Boolean)
            mblnEFTCitiDirectAddPaymentDetail = blnEFTCitiDirectAddPaymentDetail
            setParamVal("EFTCitiDirectAddPaymentDetail", mblnEFTCitiDirectAddPaymentDetail)
        End Set
    End Property
    'Sohail (09 Jan 2016) -- End

    'Hemant (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2   

    Public Property _AssetDeclarationTemplate() As Integer
        Get
            Return mintAssetDeclarationTemplate
        End Get
        Set(ByVal value As Integer)
            mintAssetDeclarationTemplate = value
            setParamVal("AssetDeclarationTemplate", mintAssetDeclarationTemplate)
        End Set
    End Property
    'Hemant (06 Oct 2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private mstrImportCostCenterP2PServiceURL As String = ""
    Public Property _ImportCostCenterP2PServiceURL() As String
        Get
            Return mstrImportCostCenterP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrImportCostCenterP2PServiceURL = value
            setParamVal("ImportCostCenterP2PServiceURL", mstrImportCostCenterP2PServiceURL)
        End Set
    End Property

    Private mstrOpexRequestCCP2PServiceURL As String = ""
    Public Property _OpexRequestCCP2PServiceURL() As String
        Get
            Return mstrOpexRequestCCP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrOpexRequestCCP2PServiceURL = value
            setParamVal("OpexRequestCCP2PServiceURL", mstrOpexRequestCCP2PServiceURL)
        End Set
    End Property

    Private mstrCapextRequestCCP2PServiceURL As String = ""
    Public Property _CapexRequestCCP2PServiceURL() As String
        Get
            Return mstrCapextRequestCCP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrCapextRequestCCP2PServiceURL = value
            setParamVal("CapexRequestCCP2PServiceURL", mstrCapextRequestCCP2PServiceURL)
        End Set
    End Property

    Private mstrBgtRequestValidationP2PServiceURL As String = ""
    Public Property _BgtRequestValidationP2PServiceURL() As String
        Get
            Return mstrBgtRequestValidationP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrBgtRequestValidationP2PServiceURL = value
            setParamVal("BudgetRequestValidationP2PServiceURL", mstrBgtRequestValidationP2PServiceURL)
        End Set
    End Property

    Private mstrNewRequisitionRequestP2PServiceURL As String = ""
    Public Property _NewRequisitionRequestP2PServiceURL() As String
        Get
            Return mstrNewRequisitionRequestP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrNewRequisitionRequestP2PServiceURL = value
            setParamVal("NewRequisitionRequestP2PServiceURL", mstrNewRequisitionRequestP2PServiceURL)
        End Set
    End Property

    Private mstrRequisitionStatusP2PServiceURL As String = ""
    Public Property _RequisitionStatusP2PServiceURL() As String
        Get
            Return mstrRequisitionStatusP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrRequisitionStatusP2PServiceURL = value
            setParamVal("RequisitionStatusP2PServiceURL", mstrRequisitionStatusP2PServiceURL)
        End Set
    End Property

    'Pinkal (20-Nov-2018) -- End

    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Private mstrGetTokenP2PServiceURL As String = ""
    Public Property _GetTokenP2PServiceURL() As String
        Get
            Return mstrGetTokenP2PServiceURL
        End Get
        Set(ByVal value As String)
            mstrGetTokenP2PServiceURL = value
            setParamVal("GetTokenP2PServiceURL", mstrGetTokenP2PServiceURL)
        End Set
    End Property
    'Pinkal (29-Aug-2019) -- End

 'Sohail (20 Nov 2018) -- Start
    'NMB Enhancement - SMS Integration to send notification 75.1.
    Private mblnSMSCompanyDetailToNewEmp As Boolean = False
    Public Property _SMSCompanyDetailToNewEmp() As Boolean
        Get
            Return mblnSMSCompanyDetailToNewEmp
        End Get
        Set(ByVal value As Boolean)
            mblnSMSCompanyDetailToNewEmp = value
            setParamVal("SMSCompanyDetailToNewEmp", mblnSMSCompanyDetailToNewEmp)
        End Set
    End Property

    Private mstrSMSGatewayEmail As String = String.Empty
    Public Property _SMSGatewayEmail() As String
        Get
            Return mstrSMSGatewayEmail
        End Get
        Set(ByVal value As String)
            mstrSMSGatewayEmail = value
            setParamVal("SMSGatewayEmail", mstrSMSGatewayEmail)
        End Set
    End Property

    Private mintSMSGatewayEmailType As Integer
    Public Property _SMSGatewayEmailType() As Integer
        Get
            Return mintSMSGatewayEmailType
        End Get
        Set(ByVal value As Integer)
            mintSMSGatewayEmailType = value
            setParamVal("SMSGatewayEmailType", mintSMSGatewayEmailType)
        End Set
    End Property
    'Sohail (20 Nov 2018) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Private mstrOracleHostName As String = String.Empty
    Public Property _OracleHostName() As String
        Get
            Return mstrOracleHostName
        End Get
        Set(ByVal value As String)
            mstrOracleHostName = value
            setParamVal("OracleHostName", mstrOracleHostName)
        End Set
    End Property

    Private mstrOraclePortNo As String = String.Empty
    Public Property _OraclePortNo() As String
        Get
            Return mstrOraclePortNo
        End Get
        Set(ByVal value As String)
            mstrOraclePortNo = value
            setParamVal("OraclePortNo", mstrOraclePortNo)
        End Set
    End Property

    Private mstrOracleServiceName As String = String.Empty
    Public Property _OracleServiceName() As String
        Get
            Return mstrOracleServiceName
        End Get
        Set(ByVal value As String)
            mstrOracleServiceName = value
            setParamVal("OracleServiceName", mstrOracleServiceName)
        End Set
    End Property

    Private mstrOracleUserName As String = String.Empty
    Public Property _OracleUserName() As String
        Get
            Return mstrOracleUserName
        End Get
        Set(ByVal value As String)
            mstrOracleUserName = value
            setParamVal("OracleUserName", mstrOracleUserName)
        End Set
    End Property

    Private mstrOracleUserPassword As String = String.Empty
    Public Property _OracleUserPassword() As String
        Get
            Return mstrOracleUserPassword
        End Get
        Set(ByVal value As String)
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - store encrypted password.
            'mstrOracleUserPassword = value
            mstrOracleUserPassword = clsSecurity.Encrypt(value, "ezee")
            'Sohail (14 Mar 2019) -- End
            setParamVal("OracleUserPassword", mstrOracleUserPassword)
        End Set
    End Property
    'Sohail (26 Nov 2018) -- End

    'Sohail (02 Mar 2019) -- Start
    'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
    Private mstrSQLDataSource As String = "."
    Public Property _SQLDataSource() As String
        Get
            Return mstrSQLDataSource
        End Get
        Set(ByVal value As String)
            mstrSQLDataSource = value
            setParamVal("SQLDataSource", mstrSQLDataSource)
        End Set
    End Property

    Private mstrSQLDatabaseName As String = String.Empty
    Public Property _SQLDatabaseName() As String
        Get
            Return mstrSQLDatabaseName
        End Get
        Set(ByVal value As String)
            mstrSQLDatabaseName = value
            setParamVal("SQLDatabaseName", mstrSQLDatabaseName)
        End Set
    End Property

    Private mstrSQLDatabaseOwnerName As String = String.Empty
    Public Property _SQLDatabaseOwnerName() As String
        Get
            Return mstrSQLDatabaseOwnerName
        End Get
        Set(ByVal value As String)
            mstrSQLDatabaseOwnerName = value
            setParamVal("SQLDatabaseOwnerName", mstrSQLDatabaseOwnerName)
        End Set
    End Property

    Private mstrSQLUserName As String = String.Empty
    Public Property _SQLUserName() As String
        Get
            Return mstrSQLUserName
        End Get
        Set(ByVal value As String)
            mstrSQLUserName = value
            setParamVal("SQLUserName", mstrSQLUserName)
        End Set
    End Property

    Private mstrSQLUserPassword As String = String.Empty
    Public Property _SQLUserPassword() As String
        Get
            Return mstrSQLUserPassword
        End Get
        Set(ByVal value As String)
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - store encrypted password.
            'mstrSQLUserPassword = value
            mstrSQLUserPassword = clsSecurity.Encrypt(value, "ezee")
            'Sohail (14 Mar 2019) -- End
            setParamVal("SQLUserPassword", mstrSQLUserPassword)
        End Set
    End Property

    Private mintBRJVVocNoType As Integer = 1
    Public Property _BRJVVocNoType() As Integer
        Get
            Return mintBRJVVocNoType
        End Get
        Set(ByVal value As Integer)
            mintBRJVVocNoType = value
            setParamVal("BRJVVocNoType", mintBRJVVocNoType)
        End Set
    End Property

    Private mstrBRJVVocPrefix As String = ""
    Public Property _BRJVVocPrefix() As String
        Get
            Return mstrBRJVVocPrefix
        End Get
        Set(ByVal value As String)
            mstrBRJVVocPrefix = value
            setParamVal("BRJVVocPrefix", mstrBRJVVocPrefix)
        End Set
    End Property

    Private mintNextBRJVVocNo As Integer = 1
    Public Property _NextBRJVVocNo() As Integer
        Get
            Return mintNextBRJVVocNo
        End Get
        Set(ByVal value As Integer)
            mintNextBRJVVocNo = value
            setParamVal("NextBRJVVocNo", mintNextBRJVVocNo)
        End Set
    End Property
    'Sohail (02 Mar 2019) -- End

    'Hemant (11 Dec 2019) -- Start
    'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
    Private mintEFTVocNoType As Integer = 1
    Public Property _EFTVocNoType() As Integer
        Get
            Return mintEFTVocNoType
        End Get
        Set(ByVal value As Integer)
            mintEFTVocNoType = value
            setParamVal("EFTVocNoType", mintEFTVocNoType)
        End Set
    End Property

    Private mstrEFTVocPrefix As String = ""
    Public Property _EFTVocPrefix() As String
        Get
            Return mstrEFTVocPrefix
        End Get
        Set(ByVal value As String)
            mstrEFTVocPrefix = value
            setParamVal("EFTVocPrefix", mstrEFTVocPrefix)
        End Set
    End Property

    Private mintNextEFTVocNo As Integer = 1
    Public Property _NextEFTVocNo() As Integer
        Get
            Return mintNextEFTVocNo
        End Get
        Set(ByVal value As Integer)
            mintNextEFTVocNo = value
            setParamVal("NextEFTVocNo", mintNextEFTVocNo)
        End Set
    End Property
    'Hemant (11 Dec 2019) -- End

    'Gajanan |30-MAR-2019| -- START
    Private mblnAllowViewEmployeeScale As Boolean = False
    Public Property _AllowViewEmployeeScale() As Boolean
        Get
            Return mblnAllowViewEmployeeScale
        End Get
        Set(ByVal value As Boolean)
            mblnAllowViewEmployeeScale = value
            setParamVal("VIEWEMPLOYEESCALE", mblnAllowViewEmployeeScale)
        End Set
    End Property
    'Gajanan |30-MAR-2019| -- END


    'Gajanan [8-April-2019] -- Start
    Private mblnAllowToAddEditEmployeePresentAddress As Boolean = False
    Public Property _AllowToAddEditEmployeePresentAddress() As Boolean
        Get
            Return mblnAllowToAddEditEmployeePresentAddress
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToAddEditEmployeePresentAddress = value
            setParamVal("ADDEDITEMPLOYEEPRESENTADDRESS", mblnAllowToAddEditEmployeePresentAddress)
        End Set
    End Property

    Private mblnAllowToAddEditEmployeeDomicileAddress As Boolean = False
    Public Property _AllowToAddEditEmployeeDomicileAddress() As Boolean
        Get
            Return mblnAllowToAddEditEmployeeDomicileAddress
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToAddEditEmployeeDomicileAddress = value
            setParamVal("ADDEDITEMPLOYEEDOMICILEADDRESS", mblnAllowToAddEditEmployeeDomicileAddress)
        End Set
    End Property

    Private mblnAllowToAddEditEmployeeRecruitmentAddress As Boolean = False
    Public Property _AllowToAddEditEmployeeRecruitmentAddress() As Boolean
        Get
            Return mblnAllowToAddEditEmployeeRecruitmentAddress
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToAddEditEmployeeRecruitmentAddress = value
            setParamVal("ADDEDITEMPLOYEERECRUITMENTADDRESS", mblnAllowToAddEditEmployeeRecruitmentAddress)
        End Set
    End Property

    'Gajanan [8-April-2019] -- End  


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mblnAllowToEditLeaveApplication As Boolean = True
    Public Property _AllowToEditLeaveApplication() As Boolean
        Get
            Return mblnAllowToEditLeaveApplication
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToEditLeaveApplication = value
            setParamVal("AllowToEditLeaveApplication", mblnAllowToEditLeaveApplication)
        End Set
    End Property

    Private mblnAllowToDeleteLeaveApplication As Boolean = True
    Public Property _AllowToDeleteLeaveApplication() As Boolean
        Get
            Return mblnAllowToDeleteLeaveApplication
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToDeleteLeaveApplication = value
            setParamVal("AllowToDeleteLeaveApplication", mblnAllowToDeleteLeaveApplication)
        End Set
    End Property

    'Pinkal (03-May-2019) -- End

    'Gajanan [13-July-2019] -- End


    'Gajanan [29-AUG-2019] -- Start      
    'Enhancements: Settings for mandatory options in online recruitment
    Private mblnRecQualificationStartDateMandatory As Boolean = False
    Public Property _QualificationStartDateMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecQualificationStartDateMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecQualificationStartDateMandatory = value
            setParamVal("RecQualificationStartDateMandatory", mblnRecQualificationStartDateMandatory)
        End Set
    End Property

    Private mblnRecQualificationAwardDateMandatory As Boolean = False
    Public Property _QualificationAwardDateMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecQualificationAwardDateMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecQualificationAwardDateMandatory = value
            setParamVal("RecQualificationAwardDateMandatory", mblnRecQualificationAwardDateMandatory)
        End Set
    End Property
    'Gajanan [29-AUG-2019] -- End  


    'Pinkal (15-Nov-2019) -- Start
    'Enhancement  St. Jude[0004149]  - Employee Timesheet Report should be optional for viewing in Employee SelfService.
    Private mblnAllowToviewEmpTimesheetReportForESS As Boolean = True
    Public Property _AllowToviewEmpTimesheetReportForESS() As Boolean
        Get
            Return mblnAllowToviewEmpTimesheetReportForESS
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToviewEmpTimesheetReportForESS = value
            setParamVal("AllowToviewEmpTimesheetReportForESS", mblnAllowToviewEmpTimesheetReportForESS)
        End Set
    End Property
    'Pinkal (15-Nov-2019) -- End


    'Gajanan [9-Dec-2019] -- Start   
    'Enhancement:Worked On Orbit Integration Configuration Setup

    Private mblnIsOrbitIntegrated As Boolean = False
    Private mstrOrbitUsername As String = String.Empty
    Private mstrOrbitPassword As String = String.Empty
    Private mstrOrbitAgentUsername As String = String.Empty
    Private mdicOrbitServiceCollection As New Dictionary(Of Integer, String)
    Private mstrOrbitFailedRequestNotificationEmails As String = String.Empty


    Public Property _OrbitServiceCollection() As Dictionary(Of Integer, String)
        Get
            Return mdicOrbitServiceCollection
        End Get
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicOrbitServiceCollection = value
            For Each ikey As Integer In mdicOrbitServiceCollection.Keys
                setParamVal("_OrbSrv_" & ikey.ToString(), mdicOrbitServiceCollection(ikey))
            Next
        End Set
    End Property

    Public Property _OrbitUsername() As String
        Get
            Return mstrOrbitUsername
        End Get
        Set(ByVal value As String)
            mstrOrbitUsername = value
            setParamVal("OrbitUsername", mstrOrbitUsername)
        End Set
    End Property

    Public Property _OrbitPassword() As String
        Get
            Return mstrOrbitPassword
        End Get
        Set(ByVal value As String)
            mstrOrbitPassword = value
            setParamVal("OrbitPassword", mstrOrbitPassword)
        End Set
    End Property

    Public Property _OrbitAgentUsername() As String
        Get
            Return mstrOrbitAgentUsername
        End Get
        Set(ByVal value As String)
            mstrOrbitAgentUsername = value
            setParamVal("OrbitAgentUsername", mstrOrbitAgentUsername)
        End Set
    End Property

    Public Property _IsOrbitIntegrated() As Boolean
        Get
            Return mblnIsOrbitIntegrated
        End Get
        Set(ByVal value As Boolean)
            mblnIsOrbitIntegrated = value
            setParamVal("IsOrbitIntegrated", mblnIsOrbitIntegrated)
        End Set
    End Property

    Public Property _OrbitFailedRequestNotificationEmails() As String
        Get
            Return mstrOrbitFailedRequestNotificationEmails
        End Get
        Set(ByVal value As String)
            mstrOrbitFailedRequestNotificationEmails = value
            setParamVal("OrbitFailedRequestNotificationEmails", mstrOrbitFailedRequestNotificationEmails)
        End Set
    End Property
    'Gajanan [9-Dec-2019] -- End



    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mstrExchangeServerIP As String = ""
    Public Property _ExchangeServerIP() As String
        Get
            Return mstrExchangeServerIP
        End Get
        Set(ByVal value As String)
            mstrExchangeServerIP = value
            setParamVal("ExchangeServerIP", mstrExchangeServerIP)
        End Set
    End Property

    Private mstrExchangeServerUserName As String = ""
    Public Property _ExchangeServerUserName() As String
        Get
            Return mstrExchangeServerUserName
        End Get
        Set(ByVal value As String)
            mstrExchangeServerUserName = value
            setParamVal("ExchangeServerUserName", mstrExchangeServerUserName)
        End Set
    End Property

    Private mstrExchangeServerUserPassword As String = ""
    Public Property _ExchangeServerUserPassword() As String
        Get
            Return mstrExchangeServerUserPassword
        End Get
        Set(ByVal value As String)
            mstrExchangeServerUserPassword = clsSecurity.Encrypt(value, "ezee")
            setParamVal("ExchangeServerUserPassword", mstrExchangeServerUserPassword)
        End Set
    End Property

    Private mstrExchangeDatabaseName As String = ""
    Public Property _ExchangeServerDatabaseName() As String
        Get
            Return mstrExchangeDatabaseName
        End Get
        Set(ByVal value As String)
            mstrExchangeDatabaseName = value
            setParamVal("ExchangeServerDatabaseName", mstrExchangeDatabaseName)
        End Set
    End Property
    'Pinkal (04-Apr-2020) -- End


    'Pinkal (22-May-2020) -- Start
    'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
    Private mintEnableExchangeMailBoxAfterMins As Integer = 0
    Public Property _EnableExchangeMailBoxAfterMins() As Integer
        Get
            Return mintEnableExchangeMailBoxAfterMins
        End Get
        Set(ByVal value As Integer)
            mintEnableExchangeMailBoxAfterMins = value
            setParamVal("EnableExchangeMailBoxAfterMins", mintEnableExchangeMailBoxAfterMins)
        End Set
    End Property
    'Pinkal (22-May-2020) -- End



    'Pinkal (27-Jul-2020) -- Start
    'Enhancement NMB -   Working on Skip Enable Exchange Server Email for Specfic allocations.
    Private mstrSkipExchangeEnableEmailAllocations As String = ""
    Public Property _SkipExchangeEnableEmailAllocations() As String
        Get
            Return mstrSkipExchangeEnableEmailAllocations
        End Get
        Set(ByVal value As String)
            mstrSkipExchangeEnableEmailAllocations = value
            setParamVal("SkipExchangeEnableEmailAllocations", mstrSkipExchangeEnableEmailAllocations)
        End Set
    End Property
    'Pinkal (27-Jul-2020) -- End

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mintTrainingNeedAllocationID As Integer = CInt(enAllocation.DEPARTMENT)
    Public Property _TrainingNeedAllocationID() As Integer
        Get
            Return mintTrainingNeedAllocationID
        End Get
        Set(ByVal value As Integer)
            mintTrainingNeedAllocationID = value
            setParamVal("TrainingNeedAllocationID", mintTrainingNeedAllocationID)
        End Set
    End Property
    'Hemant (26 Mar 2021) -- End

    'Sohail (10 Feb 2022) -- Start
    'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
    Private mintTrainingBudgetAllocationID As Integer = 0
    Public Property _TrainingBudgetAllocationID() As Integer
        Get
            If mintTrainingBudgetAllocationID <= 0 Then
                mintTrainingBudgetAllocationID = mintTrainingNeedAllocationID
            End If
            Return mintTrainingBudgetAllocationID
        End Get
        Set(ByVal value As Integer)
            mintTrainingBudgetAllocationID = value
            setParamVal("TrainingBudgetAllocationID", mintTrainingBudgetAllocationID)
        End Set
    End Property

    Private mintTrainingCostCenterAllocationID As Integer = 0
    Public Property _TrainingCostCenterAllocationID() As Integer
        Get
            Return mintTrainingCostCenterAllocationID
        End Get
        Set(ByVal value As Integer)
            mintTrainingCostCenterAllocationID = value
            setParamVal("TrainingCostCenterAllocationID", mintTrainingCostCenterAllocationID)
        End Set
    End Property

    Private mintTrainingRemainingBalanceBasedOnID As Integer = enTrainingRemainingBalanceBasedOn.Enrolled_Amount
    Public Property _TrainingRemainingBalanceBasedOnID() As Integer
        Get
            Return mintTrainingRemainingBalanceBasedOnID
        End Get
        Set(ByVal value As Integer)
            mintTrainingRemainingBalanceBasedOnID = value
            setParamVal("TrainingRemainingBalanceBasedOnID", mintTrainingRemainingBalanceBasedOnID)
        End Set
    End Property
    'Sohail (10 Feb 2022) -- End

    'S.SANDEEP |21-DEC-2021| -- START
    'ISSUE : GIVING PERIOD SELECTION ON TRAINING SETTING FOR FETCHING COMPETENCIES
    Private mintTrainingCompetenciesPeriodId As Integer = 0
    Public Property _TrainingCompetenciesPeriodId() As Integer
        Get
            Return mintTrainingCompetenciesPeriodId
        End Get
        Set(ByVal value As Integer)
            mintTrainingCompetenciesPeriodId = value
            setParamVal("TrainingCompetenciesPeriodId", mintTrainingCompetenciesPeriodId)
        End Set
    End Property

        'S.SANDEEP |21-DEC-2021| -- END

    'Hemant (09 Feb 2022) -- Start            
    'OLD-561(NMB) : Option to complete training should have expiry days
    Private mintDaysFromLastDateOfTrainingToAllowCompleteTraining As Integer = 0
    Public Property _DaysFromLastDateOfTrainingToAllowCompleteTraining() As Integer
        Get
            Return mintDaysFromLastDateOfTrainingToAllowCompleteTraining
        End Get
        Set(ByVal value As Integer)
            mintDaysFromLastDateOfTrainingToAllowCompleteTraining = value
            setParamVal("DaysFromLastDateOfTrainingToAllowCompleteTraining", mintDaysFromLastDateOfTrainingToAllowCompleteTraining)
        End Set
    End Property

    Private mintDaysForReminderEmailBeforeExpiryToCompleteTraining As Integer = 0
    Public Property _DaysForReminderEmailBeforeExpiryToCompleteTraining() As Integer
        Get
            Return mintDaysForReminderEmailBeforeExpiryToCompleteTraining
        End Get
        Set(ByVal value As Integer)
            mintDaysForReminderEmailBeforeExpiryToCompleteTraining = value
            setParamVal("DaysForReminderEmailBeforeExpiryToCompleteTraining", mintDaysForReminderEmailBeforeExpiryToCompleteTraining)
        End Set
    End Property
    'Hemant (09 Feb 2022) -- End

    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Private mintTrainingApproverAllocationID As Integer = CInt(enAllocation.DEPARTMENT)
    Public Property _TrainingApproverAllocationID() As Integer
        Get
            Return mintTrainingApproverAllocationID
        End Get
        Set(ByVal value As Integer)
            mintTrainingApproverAllocationID = value
            setParamVal("TrainingApproverAllocationID", mintTrainingApproverAllocationID)
        End Set
    End Property
    'Hemant (09 Feb 2022) -- End
#End Region

#Region " Settings "

    '=======> Report General Settings Variables 
    Private mdblPageMargin As Double = 0
    Private mintNoofPagestoPrint As Integer = 1
    Private mblnShowPropertyInfo As Boolean = False
    Private mblnShowLogoRightSide As Boolean = False

    '=======> Report Specific Settings Variables 
    Private mstrDisplayLogo As String = String.Empty
    Private mstrPreparedBy As String = String.Empty
    Private mstrApprovedBy As String = String.Empty
    Private mstrCheckedBy As String = String.Empty
    Private mstrReceivedBy As String = String.Empty
    'Anjan (11 Mar 2011)-Start
    Private mstrLeftMargin As String = String.Empty
    Private mdblLeftMargin As Double = 0.25
    Private mstrRightMargin As String = String.Empty
    Private mdblRightMargin As Double = 0.25
    'Anjan (11 Mar 2011)-End

    Private mblnIsDisplayLogo As Boolean = False
    Private mblnIsShowPreparedBy As Boolean = False
    Private mblnIsShowApprovedBy As Boolean = False
    Private mblnIsShowCheckedBy As Boolean = False
    Private mblnIsShowReceivedBy As Boolean = False


    'Sohail (26 Nov 2011) -- Start
    '***** Payslip Report Settings *********
    'TRA - Customize Payslip Staff Details i.e Branch,Department,Job,Grade,Appointment Date ,Retirement Date
    Private mblnShowBranchOnPayslip As Boolean = False
    Private mblnShowDepartmentOnPayslip As Boolean = False
    Private mblnShowJobOnPayslip As Boolean = False
    Private mblnShowGradeOnPayslip As Boolean = False

    'Anjan [22 Apr 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Private mblnShowGradeGroupOnPayslip As Boolean = False
    Private mblnShowGradeLevelOnPayslip As Boolean = False
    'Anjan [22 Apr 2014 ] -- End


    Private mblnShowAppointmentDateOnPayslip As Boolean = False
    Private mblnShowRetirementDateOnPayslip As Boolean = False
    'Sohail (26 Nov 2011) -- End
    'Sohail (19 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnShowEOCDateOnPayslip As Boolean = False
    Private mblnShowLeavingDateOnPayslip As Boolean = False
    'Sohail (19 Dec 2012) -- End
    'Sohail (05 Dec 2011) -- Start
    'TRA - Customize Payslip Staff Details i.e Branch,Department,Job,Grade,Appointment Date ,Retirement Date
    Private mintPayslipTemplate As Integer = 1
    Private mblnShowDepartmentGroupOnPayslip As Boolean = False
    Private mblnShowSectionGroupOnPayslip As Boolean = False
    Private mblnShowSectionOnPayslip As Boolean = False
    Private mblnShowUnitGroupOnPayslip As Boolean = False
    Private mblnShowUnitOnPayslip As Boolean = False
    Private mblnShowTeamOnPayslip As Boolean = False
    'Sohail (05 Dec 2011) -- End
    Private mblnOrderByDescendingOnPayslip As Boolean = False 'Sohail (18 Feb 2012)
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnIgnoreZeroValueHeadsOnPayslip As Boolean = True
    Private mblnShowLoanBalanceOnPayslip As Boolean = True
    Private mblnShowSavingBalanceOnPayslip As Boolean = False
    Private mblnShowEmployerContributionOnPayslip As Boolean = False
    Private mblnShowAllHeadsOnPayslip As Boolean = False
    Private mintLogoOnPayslip As Integer = enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
    'Sohail (16 May 2012) -- End
    Private mintLeaveTypeOnPayslip As Integer = 0 'Sohail (31 Aug 2012)
    'Sohail (11 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnShowClassGroupOnPayslip As Boolean = False
    Private mblnShowClassOnPayslip As Boolean = False
    Private mblnShowCumulativeAccrualOnPayslip As Boolean = False
    'Sohail (11 Sep 2012) -- End
    Private mblnShowCategoryOnPayslip As Boolean = False 'Sohail (31 Aug 2013)
    Private mblnSetBasicSalaryAsOtherEarningOnStatutoryReport As Boolean = False 'Sohail (07 Sep 2013)
    Private mblnShowInformationalHeadsOnPayslip As Boolean = True 'Sohail (19 Sep 2013)
    'Sohail (23 Dec 2013) -- Start
    'Enhancement - Oman
    Private mblnShowBirthDateOnPayslip As Boolean = False
    Private mblnShowAgeOnPayslip As Boolean = False
    Private mblnShowNoOfDependantsOnPayslip As Boolean = False
    Private mblnShowMonthlySalaryOnPayslip As Boolean = False


    'Anjan [01 September 2016] -- Start
    'ENHANCEMENT : Including Payslip settings on configuration.
    Private mblnShowBankAccountNoOnPayslip As Boolean = False
    'Anjan [01 Sepetember 2016] -- End

    'Sohail (23 Dec 2013) -- End
    'Sohail (09 Jan 2014) -- Start
    'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
    Private mintOT1HourHeadID As Integer = 0
    Private mstrOT1HourHeadName As String = ""
    Private mintOT2HourHeadID As Integer = 0
    Private mstrOT2HourHeadName As String = ""
    Private mintOT3HourHeadID As Integer = 0
    Private mstrOT3HourHeadName As String = ""
    Private mintOT4HourHeadID As Integer = 0
    Private mstrOT4HourHeadName As String = ""
    Private mintOT1AmountHeadID As Integer = 0
    Private mstrOT1AmountHeadName As String = ""
    Private mintOT2AmountHeadID As Integer = 0
    Private mstrOT2AmountHeadName As String = ""
    Private mintOT3AmountHeadID As Integer = 0
    Private mstrOT3AmountHeadName As String = ""
    Private mintOT4AmountHeadID As Integer = 0
    Private mstrOT4AmountHeadName As String = ""
    'Sohail (09 Jan 2014) -- End
    'Sohail (26 May 2021) -- Start
    'Netis Gabon Enhancement : OLD-357 : New Payslip  Template "Template 19".
    Private mintMembership1 As Integer = 0
    Private mintMembership2 As Integer = 0
    'Sohail (26 May 2021) -- End
    Private mblnShowSalaryOnHoldOnPayslip As Boolean = True 'Sohail (24 Feb 2014)
    'Sohail (24 Mar 2014) -- Start
    'ENHANCEMENT - Show Company Name, Emp code, Membership and Payment Details on Payslip.
    Private mblnShowCompanyNameOnPayslip As Boolean = True
    Private mblnShowEmployeeCodeOnPayslip As Boolean = True
    Private mblnShowMembershipOnPayslip As Boolean = True
    Private mblnShowPaymentDetailsOnPayslip As Boolean = True
    'Sohail (24 Mar 2014) -- End
    Private mblnShowCompanyStampOnPayslip As Boolean = False 'Sohail (27 Mar 2014)

    'Sohail (28 Jul 2014) -- Start
    'Enhancement - Show Exchange Rate option for Payslip Template 9 for GHANA.
    Private mblnShowExchangeRateOnPayslip As Boolean = False
    Private mintActualHeadID As Integer = 0
    'Sohail (28 Jul 2014) -- End
    'Sohail (11 Sep 2014) -- Start
    'Enhancement - New Payslip Template 10 for FINCA DRC.
    Private mintWorkingDaysHeadID As Integer = 0
    Private mintTaxableAmountHeadID As Integer = 0
    Private mintIPRCalculationHeadID As Integer = 0
    Private mint13thPayHeadID As Integer = 0
    Private mintAmtToBePaidHeadID As Integer = 0
    Private mintEmplCostHeadID As Integer = 0
    Private mintNoOfDependantHeadID As Integer = 0
    Private mintNoOfChildHeadID As Integer = 0
    'Sohail (11 Sep 2014) -- End
    'Sohail (20 Sep 2016) -- Start
    'Enhancement - 63.1 - New Payslip Template 15 for TIMAJA.
    Private mintCumulativeTaxHeadID As Integer = 0
    Private mintDailyBasicSalaryHeadID As Integer = 0
    Private mintAccruedLeavePayHeadID As Integer = 0
    'Sohail (20 Sep 2016) -- End


    'S.SANDEEP [ 28 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    '***** Statutory Report Settings *********
    Private mblnShowBasicSalaryOnStatutoryReport As Boolean = False
    'S.SANDEEP [ 28 FEB 2013 ] -- END

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrEmployeeAsOnDate As String = eZeeDate.convertDate(DateAndTime.Now.Date)
    'Sohail (06 Jan 2012) -- End

    'S.SANDEEP [ 08 June 2011 ] -- START
    'ISSUE : LEAVE C/F OPTIONS
    Private mintLeaveCF_OptionId As Integer = 1
    Private mintLeaveMaxValue As Integer = -1
    'S.SANDEEP [ 08 June 2011 ] -- END 



    'Pinkal (31-Jan-2014) -- Start
    'Enhancement : TRA Changes
    '***** Leave Report Settings *********
    Private mblnShowBasicSalaryOnLeaveFormReport As Boolean = False
    'Pinkal (31-Jan-2014) -- End



    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private mintsickSheetTemplate As Integer = 1
    Private mintSickSheetAllocationId As Integer = 0
    'Pinkal (12-Sep-2014) -- End
    'Sohail (11 Mar 2016) -- Start
    'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
    Private mblnShowPaymentApprovalDetailsOnPayrollSummaryReport As Boolean = False
    'Sohail (11 Mar 2016) -- End

    'Pinkal (18-Jun-2016) -- Start
    'Enhancement - IMPLEMENTING Leave Liability Setting to Leave Liablity Report.
    Private mstrLeaveLiabilityReportSetting As String = enLeaveLiabilitySetting.Days_With_Weekend_Included
    'Pinkal (18-Jun-2016) -- End

    'Nilay (03-Oct-2016) -- Start
    'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
    Private mintLeaveAccruedDaysHeadID As Integer = 0
    Private mintLeaveTakenHeadID As Integer = 0
    Private mintAssessableIncomeHeadID As Integer = 0
    Private mintTaxFreeIncomeHeadID As Integer = 0
    'Nilay (03-Oct-2016) -- End

    'Nilay (24-Oct-2016) -- Start
    'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
    Private mintNAPSATaxHeadID As Integer = 0
    'Nilay (24-Oct-2016) -- End


    'Varsha (10 Nov 2017) -- Start
    'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
    Private mintDefaultSelfServiceTheme As Integer = enSelfServiceTheme.Blue
    'Varsha (10 Nov 2017) -- End


    '=======> Report General Settings Properties
    Public Property _TOPPageMarging() As Double
        Get
            Return mdblPageMargin
        End Get
        Set(ByVal dblPageMargin As Double)
            mdblPageMargin = dblPageMargin
            setParamVal("PageMargin", mdblPageMargin)
        End Set
    End Property

    Public Property _NoofPagestoPrint() As Integer
        Get
            Return mintNoofPagestoPrint
        End Get
        Set(ByVal intNoofPagestoPrint As Integer)
            mintNoofPagestoPrint = intNoofPagestoPrint
            setParamVal("NoofPagestoPrint", mintNoofPagestoPrint)
        End Set
    End Property

    Public Property _ShowPropertyInfo() As Boolean
        Get
            Return mblnShowPropertyInfo
        End Get
        Set(ByVal blnShowPropertyInfo As Boolean)
            mblnShowPropertyInfo = blnShowPropertyInfo
            setParamVal("ShowPropertyInfo", mblnShowPropertyInfo)
        End Set
    End Property

    Public Property _ShowLogoRightSide() As Boolean
        Get
            Return mblnShowLogoRightSide
        End Get
        Set(ByVal blnShowLogoRightSide As Boolean)
            mblnShowLogoRightSide = blnShowLogoRightSide
            setParamVal("ShowLogoRightSide", mblnShowLogoRightSide)
        End Set
    End Property

    '=======> Report Specific Settings Propetties
    ''' <summary>
    ''' Please Don't Use this Property Instead Use the Boolean For Matching Purpose This is Sepecific Property only For Configuration
    ''' Returms The Comma Separated Value
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returms The Comma Separated Value</returns>
    ''' <remarks></remarks>
    Public Property _DisplayLogo() As String
        Get
            Return mstrDisplayLogo
        End Get
        Set(ByVal strDisplayLogo As String)
            mstrDisplayLogo = strDisplayLogo
            setParamVal("DisplayLogo", mstrDisplayLogo)
        End Set
    End Property

    ''' <summary>
    ''' Please Don't Use this Property Instead Use the Boolean For Matching Purpose
    ''' Returms The Comma Separated Value
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returms The Comma Separated Value</returns>
    ''' <remarks></remarks>
    Public Property _PreparedBy() As String
        Get
            Return mstrPreparedBy
        End Get
        Set(ByVal strPreparedBy As String)
            mstrPreparedBy = strPreparedBy
            setParamVal("PreparedBy", mstrPreparedBy)
        End Set
    End Property

    ''' <summary>
    ''' Please Don't Use this Property Instead Use the Boolean For Matching Purpose This is Sepecific Property only For Configuration
    ''' Returms The Comma Separated Value
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returms The Comma Separated Value</returns>
    ''' <remarks></remarks>
    Public Property _ApprovedBy() As String
        Get
            Return mstrApprovedBy
        End Get
        Set(ByVal strApprovedBy As String)
            mstrApprovedBy = strApprovedBy
            setParamVal("ApprovedBy", mstrApprovedBy)
        End Set
    End Property

    ''' <summary>
    ''' Please Don't Use this Property Instead Use the Boolean For Matching Purpose This is Sepecific Property only For Configuration
    ''' Returms The Comma Separated Value
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returms The Comma Separated Value</returns>
    ''' <remarks></remarks>
    Public Property _CheckedBy() As String
        Get
            Return mstrCheckedBy
        End Get
        Set(ByVal strCheckedBy As String)
            mstrCheckedBy = strCheckedBy
            setParamVal("CheckedBy", mstrCheckedBy)
        End Set
    End Property

    ''' <summary>
    ''' Please Don't Use this Property Instead Use the Boolean For Matching Purpose This is Sepecific Property only For Configuration
    ''' Returms The Comma Separated Value
    ''' </summary>
    ''' <value></value>
    ''' <returns>Returms The Comma Separated Value</returns>
    ''' <remarks></remarks>
    Public Property _ReceivedBy() As String
        Get
            Return mstrReceivedBy
        End Get
        Set(ByVal strReceivedBy As String)
            mstrReceivedBy = strReceivedBy
            setParamVal("ReceivedBy", mstrReceivedBy)
        End Set
    End Property

    'Anjan (11 Mar 2011)-Start
    Public Property _LeftMargin() As String
        Get
            Return mstrLeftMargin
        End Get
        Set(ByVal value As String)
            mstrLeftMargin = value
            setParamVal("LeftMargin", mstrLeftMargin)
        End Set
    End Property
    Public ReadOnly Property _GetLeftMargin() As Double
        Get
            Return mdblLeftMargin
        End Get
    End Property

    Public Property _RightMargin() As String
        Get
            Return mstrRightMargin
        End Get
        Set(ByVal value As String)
            mstrRightMargin = value
            setParamVal("RightMargin", mstrRightMargin)
        End Set
    End Property

    Public ReadOnly Property _GetRightMargin() As Double
        Get
            Return mdblRightMargin
        End Get
    End Property


    'Anjan (11 Mar 2011)-End


    ''' <summary>
    ''' Returns True If Company Logo is to be Displayed in Report
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _IsDisplayLogo() As Boolean
        Get
            Return mblnIsDisplayLogo
        End Get
    End Property

    ''' <summary>
    ''' Returns True If Show Prepared By  is to be Displayed in Report
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _IsShowPreparedBy() As Boolean
        Get
            Return mblnIsShowPreparedBy
        End Get
    End Property

    ''' <summary>
    ''' Returns True If Show Approved By is to be Displayed in Report
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _IsShowApprovedBy() As Boolean
        Get
            Return mblnIsShowApprovedBy
        End Get
    End Property

    ''' <summary>
    ''' Returns True If Show Checked By is to be Displayed in Report
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _IsShowCheckedBy() As Boolean
        Get
            Return mblnIsShowCheckedBy
        End Get
    End Property

    ''' <summary>
    ''' Returns True If Show Received By is to be Displayed in Report
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _IsShowReceivedBy() As Boolean
        Get
            Return mblnIsShowReceivedBy
        End Get
    End Property

    'S.SANDEEP [ 08 June 2011 ] -- START
    'ISSUE : LEAVE C/F OPTIONS
    ''' <summary>
    ''' Get or Set Leave C/F Option Id
    ''' </summary>
    ''' <returns>Returms Leave CF Option Id</returns>
    ''' <remarks></remarks>
    Public Property _LeaveCF_OptionId() As Integer
        Get
            Return mintLeaveCF_OptionId
        End Get
        Set(ByVal intLeaveCF_OptionId As Integer)
            mintLeaveCF_OptionId = intLeaveCF_OptionId
            setParamVal("LeaveCF_OptionId", mintLeaveCF_OptionId)
        End Set
    End Property

    ''' <summary>
    ''' Get or Set Leave Max Value Only If Set Max Option is Set Else 0.
    ''' </summary>
    ''' <returns>Returms Leave CF Option Id</returns>
    ''' <remarks></remarks>
    Public Property _LeaveMaxValue() As Integer
        Get
            Return mintLeaveMaxValue
        End Get
        Set(ByVal intLeaveMaxValue As Integer)
            mintLeaveMaxValue = intLeaveMaxValue
            setParamVal("LeaveMaxValue", mintLeaveMaxValue)
        End Set
    End Property
    'S.SANDEEP [ 08 June 2011 ] -- END 

    'Sohail (26 Nov 2011) -- Start
    'TRA - Customize Payslip Staff Details i.e Branch,Department,Job,Grade,Appointment Date ,Retirement Date
    Public Property _PayslipTemplate() As Integer
        Get
            Return mintPayslipTemplate
        End Get
        Set(ByVal intPayslipTemplate As Integer)
            mintPayslipTemplate = intPayslipTemplate
            setParamVal("PayslipTemplate", mintPayslipTemplate)
        End Set
    End Property

    Public Property _ShowBranchOnPayslip() As Boolean
        Get
            Return mblnShowBranchOnPayslip
        End Get
        Set(ByVal blnShowBranchOnPayslip As Boolean)
            mblnShowBranchOnPayslip = blnShowBranchOnPayslip
            setParamVal("ShowBranchOnPayslip", mblnShowBranchOnPayslip)
        End Set
    End Property

    Public Property _ShowDepartmentOnPayslip() As Boolean
        Get
            Return mblnShowDepartmentOnPayslip
        End Get
        Set(ByVal blnShowDepartmentOnPayslip As Boolean)
            mblnShowDepartmentOnPayslip = blnShowDepartmentOnPayslip
            setParamVal("ShowDepartmentOnPayslip", mblnShowDepartmentOnPayslip)
        End Set
    End Property

    Public Property _ShowJobOnPayslip() As Boolean
        Get
            Return mblnShowJobOnPayslip
        End Get
        Set(ByVal blnShowJobOnPayslip As Boolean)
            mblnShowJobOnPayslip = blnShowJobOnPayslip
            setParamVal("ShowJobOnPayslip", mblnShowJobOnPayslip)
        End Set
    End Property

    Public Property _ShowGradeOnPayslip() As Boolean
        Get
            Return mblnShowGradeOnPayslip
        End Get
        Set(ByVal blnShowGradeOnPayslip As Boolean)
            mblnShowGradeOnPayslip = blnShowGradeOnPayslip
            setParamVal("ShowGradeOnPayslip", mblnShowGradeOnPayslip)
        End Set
    End Property


    'Anjan [22 Apr 2014] -- Start
    'ENHANCEMENT : Requested by Rutta
    Public Property _ShowGradeGroupOnPayslip() As Boolean
        Get
            Return mblnShowGradeGroupOnPayslip
        End Get
        Set(ByVal blnShowGradeGroupOnPayslip As Boolean)
            mblnShowGradeGroupOnPayslip = blnShowGradeGroupOnPayslip
            setParamVal("ShowGradeGroupOnPayslip", mblnShowGradeGroupOnPayslip)
        End Set
    End Property
    Public Property _ShowGradeLevelOnPayslip() As Boolean
        Get
            Return mblnShowGradeLevelOnPayslip
        End Get
        Set(ByVal blnShowGradeLevelOnPayslip As Boolean)
            mblnShowGradeLevelOnPayslip = blnShowGradeLevelOnPayslip
            setParamVal("ShowGradeLevelOnPayslip", mblnShowGradeLevelOnPayslip)
        End Set
    End Property
    'Anjan [22 Apr 2014 ] -- End



    Public Property _ShowAppointmentDateOnPayslip() As Boolean
        Get
            Return mblnShowAppointmentDateOnPayslip
        End Get
        Set(ByVal blnShowAppointmentDateOnPayslip As Boolean)
            mblnShowAppointmentDateOnPayslip = blnShowAppointmentDateOnPayslip
            setParamVal("ShowAppointmentDateOnPayslip", mblnShowAppointmentDateOnPayslip)
        End Set
    End Property

    Public Property _ShowRetirementDateOnPayslip() As Boolean
        Get
            Return mblnShowRetirementDateOnPayslip
        End Get
        Set(ByVal blnShowRetirementDateOnPayslip As Boolean)
            mblnShowRetirementDateOnPayslip = blnShowRetirementDateOnPayslip
            setParamVal("ShowRetirementDateOnPayslip", mblnShowRetirementDateOnPayslip)
        End Set
    End Property
    'Sohail (26 Nov 2011) -- End
    'Sohail (19 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _ShowEOCDateOnPayslip() As Boolean
        Get
            Return mblnShowEOCDateOnPayslip
        End Get
        Set(ByVal blnShowEOCDateOnPayslip As Boolean)
            mblnShowEOCDateOnPayslip = blnShowEOCDateOnPayslip
            setParamVal("ShowEOCDateOnPayslip", mblnShowEOCDateOnPayslip)
        End Set
    End Property

    Public Property _ShowLeavingDateOnPayslip() As Boolean
        Get
            Return mblnShowLeavingDateOnPayslip
        End Get
        Set(ByVal blnShowLeavingDateOnPayslip As Boolean)
            mblnShowLeavingDateOnPayslip = blnShowLeavingDateOnPayslip
            setParamVal("ShowLeavingDateOnPayslip", mblnShowLeavingDateOnPayslip)
        End Set
    End Property
    'Sohail (19 Dec 2012) -- End
    'Sohail (05 Dec 2011) -- Start
    'TRA - Customize Payslip Staff Details i.e Branch,Department,Job,Grade,Appointment Date ,Retirement Date
    Public Property _ShowDepartmentGroupOnPayslip() As Boolean
        Get
            Return mblnShowDepartmentGroupOnPayslip
        End Get
        Set(ByVal blnShowDepartmentGroupOnPayslip As Boolean)
            mblnShowDepartmentGroupOnPayslip = blnShowDepartmentGroupOnPayslip
            setParamVal("ShowDepartmentGroupOnPayslip", mblnShowDepartmentGroupOnPayslip)
        End Set
    End Property

    Public Property _ShowSectionGroupOnPayslip() As Boolean
        Get
            Return mblnShowSectionGroupOnPayslip
        End Get
        Set(ByVal blnShowSectionGroupOnPayslip As Boolean)
            mblnShowSectionGroupOnPayslip = blnShowSectionGroupOnPayslip
            setParamVal("ShowSectionGroupOnPayslip", mblnShowSectionGroupOnPayslip)
        End Set
    End Property

    Public Property _ShowSectionOnPayslip() As Boolean
        Get
            Return mblnShowSectionOnPayslip
        End Get
        Set(ByVal blnShowSectionOnPayslip As Boolean)
            mblnShowSectionOnPayslip = blnShowSectionOnPayslip
            setParamVal("ShowSectionOnPayslip", mblnShowSectionOnPayslip)
        End Set
    End Property

    Public Property _ShowUnitGroupOnPayslip() As Boolean
        Get
            Return mblnShowUnitGroupOnPayslip
        End Get
        Set(ByVal blnShowUnitGroupOnPayslip As Boolean)
            mblnShowUnitGroupOnPayslip = blnShowUnitGroupOnPayslip
            setParamVal("ShowUnitGroupOnPayslip", mblnShowUnitGroupOnPayslip)
        End Set
    End Property

    Public Property _ShowUnitOnPayslip() As Boolean
        Get
            Return mblnShowUnitOnPayslip
        End Get
        Set(ByVal blnShowUnitOnPayslip As Boolean)
            mblnShowUnitOnPayslip = blnShowUnitOnPayslip
            setParamVal("ShowUnitOnPayslip", mblnShowUnitOnPayslip)
        End Set
    End Property

    Public Property _ShowTeamOnPayslip() As Boolean
        Get
            Return mblnShowTeamOnPayslip
        End Get
        Set(ByVal blnShowTeamOnPayslip As Boolean)
            mblnShowTeamOnPayslip = blnShowTeamOnPayslip
            setParamVal("ShowTeamOnPayslip", mblnShowTeamOnPayslip)
        End Set
    End Property
    'Sohail (05 Dec 2011) -- End

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _EmployeeAsOnDate() As String
        Get
            Return mstrEmployeeAsOnDate
        End Get
        Set(ByVal dtEmployeeAsOnDate As String)
            mstrEmployeeAsOnDate = dtEmployeeAsOnDate
            setParamVal("EmployeeAsOnDate", mstrEmployeeAsOnDate)
        End Set
    End Property
    'Sohail (06 Jan 2012) -- End

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _OrderByDescendingOnPayslip() As Boolean
        Get
            Return mblnOrderByDescendingOnPayslip
        End Get
        Set(ByVal blnOrderByDescendingOnPayslip As Boolean)
            mblnOrderByDescendingOnPayslip = blnOrderByDescendingOnPayslip
            setParamVal("OrderByDescendingOnPayslip", mblnOrderByDescendingOnPayslip)
        End Set
    End Property
    'Sohail (18 Feb 2012) -- End

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _IgnoreZeroValueHeadsOnPayslip() As Boolean
        Get
            Return mblnIgnoreZeroValueHeadsOnPayslip
        End Get
        Set(ByVal blnIgnoreZeroValueHeadsOnPayslip As Boolean)
            mblnIgnoreZeroValueHeadsOnPayslip = blnIgnoreZeroValueHeadsOnPayslip
            setParamVal("IgnoreZeroValueHeadsOnPayslip", mblnIgnoreZeroValueHeadsOnPayslip)
        End Set
    End Property

    Public Property _ShowLoanBalanceOnPayslip() As Boolean
        Get
            Return mblnShowLoanBalanceOnPayslip
        End Get
        Set(ByVal blnShowLoanBalanceOnPayslip As Boolean)
            mblnShowLoanBalanceOnPayslip = blnShowLoanBalanceOnPayslip
            setParamVal("ShowLoanBalanceOnPayslip", mblnShowLoanBalanceOnPayslip)
        End Set
    End Property

    Public Property _ShowSavingBalanceOnPayslip() As Boolean
        Get
            Return mblnShowSavingBalanceOnPayslip
        End Get
        Set(ByVal blnShowSavingBalanceOnPayslip As Boolean)
            mblnShowSavingBalanceOnPayslip = blnShowSavingBalanceOnPayslip
            setParamVal("ShowSavingBalanceOnPayslip", mblnShowSavingBalanceOnPayslip)
        End Set
    End Property

    Public Property _ShowEmployerContributionOnPayslip() As Boolean
        Get
            Return mblnShowEmployerContributionOnPayslip
        End Get
        Set(ByVal blnShowEmployerContributionOnPayslip As Boolean)
            mblnShowEmployerContributionOnPayslip = blnShowEmployerContributionOnPayslip
            setParamVal("ShowEmployerContributionOnPayslip", mblnShowEmployerContributionOnPayslip)
        End Set
    End Property

    Public Property _ShowAllHeadsOnPayslip() As Boolean
        Get
            Return mblnShowAllHeadsOnPayslip
        End Get
        Set(ByVal blnShowAllHeadsOnPayslip As Boolean)
            mblnShowAllHeadsOnPayslip = blnShowAllHeadsOnPayslip
            setParamVal("ShowAllHeadsOnPayslip", mblnShowAllHeadsOnPayslip)
        End Set
    End Property

    Public Property _LogoOnPayslip() As Integer
        Get
            Return mintLogoOnPayslip
        End Get
        Set(ByVal intLogoOnPayslip As Integer)
            mintLogoOnPayslip = intLogoOnPayslip
            setParamVal("LogoOnPayslip", mintLogoOnPayslip)
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Sohail (31 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _LeaveTypeOnPayslip() As Integer
        Get
            Return mintLeaveTypeOnPayslip
        End Get
        Set(ByVal intLeaveTypeOnPayslip As Integer)
            mintLeaveTypeOnPayslip = intLeaveTypeOnPayslip
            setParamVal("LeaveTypeOnPayslip", mintLeaveTypeOnPayslip)
        End Set
    End Property
    'Sohail (31 Aug 2012) -- End

    'Sohail (11 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _ShowClassGroupOnPayslip() As Boolean
        Get
            Return mblnShowClassGroupOnPayslip
        End Get
        Set(ByVal blnShowClassGroupOnPayslip As Boolean)
            mblnShowClassGroupOnPayslip = blnShowClassGroupOnPayslip
            setParamVal("ShowClassGroupOnPayslip", mblnShowClassGroupOnPayslip)
        End Set
    End Property

    Public Property _ShowClassOnPayslip() As Boolean
        Get
            Return mblnShowClassOnPayslip
        End Get
        Set(ByVal blnShowClassOnPayslip As Boolean)
            mblnShowClassOnPayslip = blnShowClassOnPayslip
            setParamVal("ShowClassOnPayslip", mblnShowClassOnPayslip)
        End Set
    End Property

    Public Property _ShowCumulativeAccrualOnPayslip() As Boolean
        Get
            Return mblnShowCumulativeAccrualOnPayslip
        End Get
        Set(ByVal blnShowCumulativeAccrualOnPayslip As Boolean)
            mblnShowCumulativeAccrualOnPayslip = blnShowCumulativeAccrualOnPayslip
            setParamVal("ShowCumulativeAccrualOnPayslip", mblnShowCumulativeAccrualOnPayslip)
        End Set
    End Property
    'Sohail (11 Sep 2012) -- End

    'Hemant (23 Dec 2020) -- Start
    'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
    Private mblnShowRemainingNoOfLoanInstallmentsOnPayslip As Boolean = False
    Public Property _ShowRemainingNoOfLoanInstallmentsOnPayslip() As Boolean
        Get
            Return mblnShowRemainingNoofLoanInstallmentsOnPayslip
        End Get
        Set(ByVal blnShowRemainingNoofLoanInstallmentsOnPayslip As Boolean)
            mblnShowRemainingNoofLoanInstallmentsOnPayslip = blnShowRemainingNoofLoanInstallmentsOnPayslip
            setParamVal("ShowRemainingNoOfLoanInstallmentsOnPayslip", mblnShowRemainingNoOfLoanInstallmentsOnPayslip)
        End Set
    End Property

    'Hemant (23 Dec 2020) -- End

    'S.SANDEEP [ 28 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property _ShowBasicSalaryOnStatutoryReport() As Boolean
        Get
            Return mblnShowBasicSalaryOnStatutoryReport
        End Get
        Set(ByVal blnShowBasicSalaryOnStatutoryReport As Boolean)
            mblnShowBasicSalaryOnStatutoryReport = blnShowBasicSalaryOnStatutoryReport
            setParamVal("ShowBasicSalaryOnStatutoryReport", mblnShowBasicSalaryOnStatutoryReport)
        End Set
    End Property
    'S.SANDEEP [ 28 FEB 2013 ] -- END


    'Pinkal (31-Jan-2014) -- Start
    'Enhancement : TRA Changes

    Public Property _ShowBasicSalaryOnLeaveFormReport() As Boolean
        Get
            Return mblnShowBasicSalaryOnLeaveFormReport
        End Get
        Set(ByVal blnShowBasicSalaryOnLeaveFormReport As Boolean)
            mblnShowBasicSalaryOnLeaveFormReport = blnShowBasicSalaryOnLeaveFormReport
            setParamVal("ShowBasicSalaryOnLeaveFormReport", mblnShowBasicSalaryOnLeaveFormReport)
        End Set
    End Property

    'Pinkal (31-Jan-2014) -- End


    'Sohail (31 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _ShowCategoryOnPayslip() As Boolean
        Get
            Return mblnShowCategoryOnPayslip
        End Get
        Set(ByVal blnShowCategoryOnPayslip As Boolean)
            mblnShowCategoryOnPayslip = blnShowCategoryOnPayslip
            setParamVal("ShowCategoryOnPayslip", mblnShowCategoryOnPayslip)
        End Set
    End Property
    'Sohail (31 Aug 2013) -- End

    'Sohail (19 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _ShowInformationalHeadsOnPayslip() As Boolean
        Get
            Return mblnShowInformationalHeadsOnPayslip
        End Get
        Set(ByVal blnShowInformationalHeadsOnPayslip As Boolean)
            mblnShowInformationalHeadsOnPayslip = blnShowInformationalHeadsOnPayslip
            setParamVal("ShowInformationalHeadsOnPayslip", mblnShowInformationalHeadsOnPayslip)
        End Set
    End Property
    'Sohail (19 Sep 2013) -- End

    'Sohail (23 Dec 2013) -- Start
    'Enhancement - Oman
    Public Property _ShowBirthDateOnPayslip() As Boolean
        Get
            Return mblnShowBirthDateOnPayslip
        End Get
        Set(ByVal blnShowBirthDateOnPayslip As Boolean)
            mblnShowBirthDateOnPayslip = blnShowBirthDateOnPayslip
            setParamVal("ShowBirthDateOnPayslip", mblnShowBirthDateOnPayslip)
        End Set
    End Property

    Public Property _ShowAgeOnPayslip() As Boolean
        Get
            Return mblnShowAgeOnPayslip
        End Get
        Set(ByVal blnShowAgeOnPayslip As Boolean)
            mblnShowAgeOnPayslip = blnShowAgeOnPayslip
            setParamVal("ShowAgeOnPayslip", mblnShowAgeOnPayslip)
        End Set
    End Property

    Public Property _ShowNoOfDependantsOnPayslip() As Boolean
        Get
            Return mblnShowNoOfDependantsOnPayslip
        End Get
        Set(ByVal blnShowNoOfDependantsOnPayslip As Boolean)
            mblnShowNoOfDependantsOnPayslip = blnShowNoOfDependantsOnPayslip
            setParamVal("ShowNoOfDependantsOnPayslip", mblnShowNoOfDependantsOnPayslip)
        End Set
    End Property

    Public Property _ShowMonthlySalaryOnPayslip() As Boolean
        Get
            Return mblnShowMonthlySalaryOnPayslip
        End Get
        Set(ByVal blnShowMonthlySalaryOnPayslip As Boolean)
            mblnShowMonthlySalaryOnPayslip = blnShowMonthlySalaryOnPayslip
            setParamVal("ShowMonthlySalaryOnPayslip", mblnShowMonthlySalaryOnPayslip)
        End Set
    End Property
    'Sohail (23 Dec 2013) -- End


    'Anjan [01 September 2016] -- Start
    'ENHANCEMENT : Including Payslip settings on configuration.
    Public Property _ShowBankAccountNoOnPayslip() As Boolean
        Get
            Return mblnShowBankAccountNoOnPayslip
        End Get
        Set(ByVal blnShowBankAccountNoOnPayslip As Boolean)
            mblnShowBankAccountNoOnPayslip = blnShowBankAccountNoOnPayslip
            setParamVal("ShowBankAccountNoOnPayslip", mblnShowBankAccountNoOnPayslip)
        End Set
    End Property
    'Anjan [01 Sepetember 2016] -- End


    'Sohail (09 Jan 2014) -- Start
    'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
    Public Property _OT1HourHeadId() As Integer
        Get
            Return mintOT1HourHeadID
        End Get
        Set(ByVal intOT1HourHeadID As Integer)
            mintOT1HourHeadID = intOT1HourHeadID
            setParamVal("OT1HourHeadID", mintOT1HourHeadID)
        End Set
    End Property

    Public Property _OT1HourHeadName() As String
        Get
            Return mstrOT1HourHeadName
        End Get
        Set(ByVal strOT1HourHeadName As String)
            mstrOT1HourHeadName = strOT1HourHeadName
            setParamVal("OT1HourHeadName", mstrOT1HourHeadName)
        End Set
    End Property

    Public Property _OT2HourHeadId() As Integer
        Get
            Return mintOT2HourHeadID
        End Get
        Set(ByVal intOT2HourHeadID As Integer)
            mintOT2HourHeadID = intOT2HourHeadID
            setParamVal("OT2HourHeadID", mintOT2HourHeadID)
        End Set
    End Property

    Public Property _OT2HourHeadName() As String
        Get
            Return mstrOT2HourHeadName
        End Get
        Set(ByVal strOT2HourHeadName As String)
            mstrOT2HourHeadName = strOT2HourHeadName
            setParamVal("OT2HourHeadName", mstrOT2HourHeadName)
        End Set
    End Property

    Public Property _OT3HourHeadId() As Integer
        Get
            Return mintOT3HourHeadID
        End Get
        Set(ByVal intOT3HourHeadID As Integer)
            mintOT3HourHeadID = intOT3HourHeadID
            setParamVal("OT3HourHeadID", mintOT3HourHeadID)
        End Set
    End Property

    Public Property _OT3HourHeadName() As String
        Get
            Return mstrOT3HourHeadName
        End Get
        Set(ByVal strOT3HourHeadName As String)
            mstrOT3HourHeadName = strOT3HourHeadName
            setParamVal("OT3HourHeadName", mstrOT3HourHeadName)
        End Set
    End Property

    Public Property _OT4HourHeadId() As Integer
        Get
            Return mintOT4HourHeadID
        End Get
        Set(ByVal intOT4HourHeadID As Integer)
            mintOT4HourHeadID = intOT4HourHeadID
            setParamVal("OT4HourHeadID", mintOT4HourHeadID)
        End Set
    End Property

    Public Property _OT4HourHeadName() As String
        Get
            Return mstrOT4HourHeadName
        End Get
        Set(ByVal strOT4HourHeadName As String)
            mstrOT4HourHeadName = strOT4HourHeadName
            setParamVal("OT4HourHeadName", mstrOT4HourHeadName)
        End Set
    End Property

    Public Property _OT1AmountHeadId() As Integer
        Get
            Return mintOT1AmountHeadID
        End Get
        Set(ByVal intOT1AmountHeadID As Integer)
            mintOT1AmountHeadID = intOT1AmountHeadID
            setParamVal("OT1AmountHeadID", mintOT1AmountHeadID)
        End Set
    End Property

    Public Property _OT1AmountHeadName() As String
        Get
            Return mstrOT1AmountHeadName
        End Get
        Set(ByVal strOT1AmountHeadName As String)
            mstrOT1AmountHeadName = strOT1AmountHeadName
            setParamVal("OT1AmountHeadName", mstrOT1AmountHeadName)
        End Set
    End Property

    Public Property _OT2AmountHeadId() As Integer
        Get
            Return mintOT2AmountHeadID
        End Get
        Set(ByVal intOT2AmountHeadID As Integer)
            mintOT2AmountHeadID = intOT2AmountHeadID
            setParamVal("OT2AmountHeadID", mintOT2AmountHeadID)
        End Set
    End Property

    Public Property _OT2AmountHeadName() As String
        Get
            Return mstrOT2AmountHeadName
        End Get
        Set(ByVal strOT2AmountHeadName As String)
            mstrOT2AmountHeadName = strOT2AmountHeadName
            setParamVal("OT2AmountHeadName", mstrOT2AmountHeadName)
        End Set
    End Property

    Public Property _OT3AmountHeadId() As Integer
        Get
            Return mintOT3AmountHeadID
        End Get
        Set(ByVal intOT3AmountHeadID As Integer)
            mintOT3AmountHeadID = intOT3AmountHeadID
            setParamVal("OT3AmountHeadID", mintOT3AmountHeadID)
        End Set
    End Property

    Public Property _OT3AmountHeadName() As String
        Get
            Return mstrOT3AmountHeadName
        End Get
        Set(ByVal strOT3AmountHeadName As String)
            mstrOT3AmountHeadName = strOT3AmountHeadName
            setParamVal("OT3AmountHeadName", mstrOT3AmountHeadName)
        End Set
    End Property

    Public Property _OT4AmountHeadId() As Integer
        Get
            Return mintOT4AmountHeadID
        End Get
        Set(ByVal intOT4AmountHeadID As Integer)
            mintOT4AmountHeadID = intOT4AmountHeadID
            setParamVal("OT4AmountHeadID", mintOT4AmountHeadID)
        End Set
    End Property

    Public Property _OT4AmountHeadName() As String
        Get
            Return mstrOT4AmountHeadName
        End Get
        Set(ByVal strOT4AmountHeadName As String)
            mstrOT4AmountHeadName = strOT4AmountHeadName
            setParamVal("OT4AmountHeadName", mstrOT4AmountHeadName)
        End Set
    End Property
    'Sohail (09 Jan 2014) -- End

    'Sohail (26 May 2021) -- Start
    'Netis Gabon Enhancement : OLD-357 : New Payslip  Template "Template 19".
    Public Property _Membership1() As Integer
        Get
            Return mintMembership1
        End Get
        Set(ByVal intMembership1 As Integer)
            mintMembership1 = intMembership1
            setParamVal("Membership1", mintMembership1)
        End Set
    End Property

    Public Property _Membership2() As Integer
        Get
            Return mintMembership2
        End Get
        Set(ByVal intMembership2 As Integer)
            mintMembership2 = intMembership2
            setParamVal("Membership2", mintMembership2)
        End Set
    End Property
    'Sohail (26 May 2021) -- End

    'Sohail (24 Feb 2014) -- Start
    'Enhancement - Show Salary On Hold
    Public Property _ShowSalaryOnHoldOnPayslip() As Boolean
        Get
            Return mblnShowSalaryOnHoldOnPayslip
        End Get
        Set(ByVal blnShowSalaryOnHoldOnPayslip As Boolean)
            mblnShowSalaryOnHoldOnPayslip = blnShowSalaryOnHoldOnPayslip
            setParamVal("ShowSalaryOnHoldOnPayslip", mblnShowSalaryOnHoldOnPayslip)
        End Set
    End Property
    'Sohail (24 Feb 2014) -- End

    'Sohail (24 Mar 2014) -- Start
    'ENHANCEMENT - Show Company Name, Emp code, Membership and Payment Details on Payslip.
    Public Property _ShowCompanyNameOnPayslip() As Boolean
        Get
            Return mblnShowCompanyNameOnPayslip
        End Get
        Set(ByVal blnShowCompanyNameOnPayslip As Boolean)
            mblnShowCompanyNameOnPayslip = blnShowCompanyNameOnPayslip
            setParamVal("ShowCompanyNameOnPayslip", mblnShowCompanyNameOnPayslip)
        End Set
    End Property

    Public Property _ShowEmployeeCodeOnPayslip() As Boolean
        Get
            Return mblnShowEmployeeCodeOnPayslip
        End Get
        Set(ByVal blnShowEmployeeCodeOnPayslip As Boolean)
            mblnShowEmployeeCodeOnPayslip = blnShowEmployeeCodeOnPayslip
            setParamVal("ShowEmployeeCodeOnPayslip", mblnShowEmployeeCodeOnPayslip)
        End Set
    End Property

    Public Property _ShowMembershipOnPayslip() As Boolean
        Get
            Return mblnShowMembershipOnPayslip
        End Get
        Set(ByVal blnShowMembershipOnPayslip As Boolean)
            mblnShowMembershipOnPayslip = blnShowMembershipOnPayslip
            setParamVal("ShowMembershipOnPayslip", mblnShowMembershipOnPayslip)
        End Set
    End Property

    Public Property _ShowPaymentDetailsOnPayslip() As Boolean
        Get
            Return mblnShowPaymentDetailsOnPayslip
        End Get
        Set(ByVal blnShowPaymentDetailsOnPayslip As Boolean)
            mblnShowPaymentDetailsOnPayslip = blnShowPaymentDetailsOnPayslip
            setParamVal("ShowPaymentDetailsOnPayslip", mblnShowPaymentDetailsOnPayslip)
        End Set
    End Property
    'Sohail (24 Mar 2014) -- End

    'Sohail (07 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _SetBasicSalaryAsOtherEarningOnStatutoryReport() As Boolean
        Get
            Return mblnSetBasicSalaryAsOtherEarningOnStatutoryReport
        End Get
        Set(ByVal blnSetBasicSalaryAsOtherEarningOnStatutoryReport As Boolean)
            mblnSetBasicSalaryAsOtherEarningOnStatutoryReport = blnSetBasicSalaryAsOtherEarningOnStatutoryReport
            setParamVal("SetBasicSalaryAsOtherEarningOnStatutoryReport", mblnSetBasicSalaryAsOtherEarningOnStatutoryReport)
        End Set
    End Property
    'Sohail (07 Sep 2013) -- End

    'Sohail (27 Mar 2014) -- Start
    'ENHANCEMENT - Show Company Stamp on Payslip.
    Public Property _ShowCompanyStampOnPayslip() As Boolean
        Get
            Return mblnShowCompanyStampOnPayslip
        End Get
        Set(ByVal blnShowCompanyStampOnPayslip As Boolean)
            mblnShowCompanyStampOnPayslip = blnShowCompanyStampOnPayslip
            setParamVal("ShowCompanyStampOnPayslip", mblnShowCompanyStampOnPayslip)
        End Set
    End Property
    'Sohail (27 Mar 2014) -- End

    'Sohail (28 Jul 2014) -- Start
    'Enhancement - Show Exchange Rate option for Payslip Template 9 for GHANA.
    Public Property _ShowExchangeRateOnPayslip() As Boolean
        Get
            Return mblnShowExchangeRateOnPayslip
        End Get
        Set(ByVal blnShowExchangeRateOnPayslip As Boolean)
            mblnShowExchangeRateOnPayslip = blnShowExchangeRateOnPayslip
            setParamVal("ShowExchangeRateOnPayslip", mblnShowExchangeRateOnPayslip)
        End Set
    End Property

    Public Property _ActualHeadId() As Integer
        Get
            Return mintActualHeadID
        End Get
        Set(ByVal intActualHeadID As Integer)
            mintActualHeadID = intActualHeadID
            setParamVal("ActualHeadID", mintActualHeadID)
        End Set
    End Property
    'Sohail (28 Jul 2014) -- End

    'Sohail (11 Sep 2014) -- Start
    'Enhancement - New Payslip Template 10 for FINCA DRC.
    Public Property _WorkingDaysHeadId() As Integer
        Get
            Return mintWorkingDaysHeadID
        End Get
        Set(ByVal intWorkingDaysHeadID As Integer)
            mintWorkingDaysHeadID = intWorkingDaysHeadID
            setParamVal("WorkingDaysHeadID", mintWorkingDaysHeadID)
        End Set
    End Property

    Public Property _TaxableAmountHeadId() As Integer
        Get
            Return mintTaxableAmountHeadID
        End Get
        Set(ByVal intTaxableAmountHeadID As Integer)
            mintTaxableAmountHeadID = intTaxableAmountHeadID
            setParamVal("TaxableAmountHeadID", mintTaxableAmountHeadID)
        End Set
    End Property

    Public Property _IPRCalculationHeadId() As Integer
        Get
            Return mintIPRCalculationHeadID
        End Get
        Set(ByVal intIPRCalculationHeadID As Integer)
            mintIPRCalculationHeadID = intIPRCalculationHeadID
            setParamVal("IPRCalculationHeadID", mintIPRCalculationHeadID)
        End Set
    End Property

    Public Property _13thPayHeadId() As Integer
        Get
            Return mint13thPayHeadID
        End Get
        Set(ByVal int13thPayHeadID As Integer)
            mint13thPayHeadID = int13thPayHeadID
            setParamVal("13thPayHeadID", mint13thPayHeadID)
        End Set
    End Property

    Public Property _AmtToBePaidHeadId() As Integer
        Get
            Return mintAmtToBePaidHeadID
        End Get
        Set(ByVal intAmtToBePaidHeadID As Integer)
            mintAmtToBePaidHeadID = intAmtToBePaidHeadID
            setParamVal("AmtToBePaidHeadID", mintAmtToBePaidHeadID)
        End Set
    End Property

    Public Property _EmplCostHeadId() As Integer
        Get
            Return mintEmplCostHeadID
        End Get
        Set(ByVal intEmplCostHeadID As Integer)
            mintEmplCostHeadID = intEmplCostHeadID
            setParamVal("EmplCostHeadID", mintEmplCostHeadID)
        End Set
    End Property

    Public Property _NoOfDependantHeadId() As Integer
        Get
            Return mintNoOfDependantHeadID
        End Get
        Set(ByVal intNoOfDependantHeadID As Integer)
            mintNoOfDependantHeadID = intNoOfDependantHeadID
            setParamVal("NoOfDependantHeadID", mintNoOfDependantHeadID)
        End Set
    End Property

    Public Property _NoOfChildHeadId() As Integer
        Get
            Return mintNoOfChildHeadID
        End Get
        Set(ByVal intNoOfChildHeadID As Integer)
            mintNoOfChildHeadID = intNoOfChildHeadID
            setParamVal("NoOfChildHeadID", mintNoOfChildHeadID)
        End Set
    End Property

    Public Property _CumulativeTaxHeadId() As Integer
        Get
            Return mintCumulativeTaxHeadID
        End Get
        Set(ByVal intCumulativeTaxHeadID As Integer)
            mintCumulativeTaxHeadID = intCumulativeTaxHeadID
            setParamVal("CumulativeTaxHeadID", mintCumulativeTaxHeadID)
        End Set
    End Property

    Public Property _DailyBasicSalaryHeadId() As Integer
        Get
            Return mintDailyBasicSalaryHeadID
        End Get
        Set(ByVal intDailyBasicSalaryHeadID As Integer)
            mintDailyBasicSalaryHeadID = intDailyBasicSalaryHeadID
            setParamVal("DailyBasicSalaryHeadID", mintDailyBasicSalaryHeadID)
        End Set
    End Property

    Public Property _AccruedLeavePayHeadId() As Integer
        Get
            Return mintAccruedLeavePayHeadID
        End Get
        Set(ByVal intAccruedLeavePayHeadID As Integer)
            mintAccruedLeavePayHeadID = intAccruedLeavePayHeadID
            setParamVal("AccruedLeavePayHeadID", mintAccruedLeavePayHeadID)
        End Set
    End Property
    'Sohail (11 Sep 2014) -- End

    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

    Public Property _SickSheetTemplate() As Integer
        Get
            Return mintsickSheetTemplate
        End Get
        Set(ByVal Value As Integer)
            mintsickSheetTemplate = Value
            setParamVal("SickSheetTemplate", mintsickSheetTemplate)
        End Set
    End Property

    Public Property _SickSheetAllocationId() As Integer
        Get
            Return mintSickSheetAllocationId
        End Get
        Set(ByVal Value As Integer)
            mintSickSheetAllocationId = Value
            setParamVal("SickSheetAllocationId", mintSickSheetAllocationId)
        End Set
    End Property

    'Pinkal (12-Sep-2014) -- End

    'Sohail (11 Mar 2016) -- Start
    'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
    Public Property _ShowPaymentApprovalDetailsOnStatutoryReport() As Boolean
        Get
            Return mblnShowPaymentApprovalDetailsOnPayrollSummaryReport
        End Get
        Set(ByVal blnShowPaymentApprovalDetailsOnPayrollSummaryReport As Boolean)
            mblnShowPaymentApprovalDetailsOnPayrollSummaryReport = blnShowPaymentApprovalDetailsOnPayrollSummaryReport
            setParamVal("ShowPaymentApprovalDetailsOnPayrollSummaryReport", mblnShowPaymentApprovalDetailsOnPayrollSummaryReport)
        End Set
    End Property
    'Sohail (11 Mar 2016) -- End


    'Pinkal (18-Jun-2016) -- Start
    'Enhancement - IMPLEMENTING Leave Liability Setting to Leave Liablity Report.

    Public Property _LeaveLiabilityReportSetting() As String
        Get
            Return mstrLeaveLiabilityReportSetting
        End Get
        Set(ByVal value As String)
            mstrLeaveLiabilityReportSetting = value
            setParamVal("LeaveLiabilityReportSetting", mstrLeaveLiabilityReportSetting)
        End Set
    End Property

    'Pinkal (18-Jun-2016) -- End

    'Nilay (03-Oct-2016) -- Start
    'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
    Public Property _LeaveAccruedDaysHeadID() As Integer
        Get
            Return mintLeaveAccruedDaysHeadID
        End Get
        Set(ByVal value As Integer)
            mintLeaveAccruedDaysHeadID = value
            setParamVal("LeaveAccruedDaysHeadID", mintLeaveAccruedDaysHeadID)
        End Set
    End Property

    Public Property _LeaveTakenHeadID() As Integer
        Get
            Return mintLeaveTakenHeadID
        End Get
        Set(ByVal value As Integer)
            mintLeaveTakenHeadID = value
            setParamVal("LeaveTakenHeadID", mintLeaveTakenHeadID)
        End Set
    End Property

    Public Property _AssessableIncomeHeadID() As Integer
        Get
            Return mintAssessableIncomeHeadID
        End Get
        Set(ByVal value As Integer)
            mintAssessableIncomeHeadID = value
            setParamVal("AssessableIncomeHeadID", mintAssessableIncomeHeadID)
        End Set
    End Property

    Public Property _TaxFreeIncomeHeadID() As Integer
        Get
            Return mintTaxFreeIncomeHeadID
        End Get
        Set(ByVal value As Integer)
            mintTaxFreeIncomeHeadID = value
            setParamVal("TaxFreeIncomeHeadID", mintTaxFreeIncomeHeadID)
        End Set
    End Property
    'Nilay (03-Oct-2016) -- End

    'Nilay (24-Oct-2016) -- Start
    'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
    Public Property _NAPSATaxHeadID() As Integer
        Get
            Return mintNAPSATaxHeadID
        End Get
        Set(ByVal value As Integer)
            mintNAPSATaxHeadID = value
            setParamVal("NAPSATaxHeadID", mintNAPSATaxHeadID)
        End Set
    End Property
    'Nilay (24-Oct-2016) -- End


    'Varsha (10 Nov 2017) -- Start
    'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
    Public Property _DefaultSelfServiceTheme() As Integer
        Get
            Return mintDefaultSelfServiceTheme
        End Get
        Set(ByVal intDefaultSelfServiceTheme As Integer)
            mintDefaultSelfServiceTheme = intDefaultSelfServiceTheme
            setParamVal("DefaultSelfServiceTheme", mintDefaultSelfServiceTheme)
        End Set
    End Property
    'Varsha (10 Nov 2017) -- End


    'Pinkal (19-Feb-2020) -- Start
    'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
    Dim mblnLeaveIssueAsonDateOnLeaveBalanceListReport As Boolean = False

    Public Property _LeaveIssueAsonDateOnLeaveBalanceListReport() As Boolean
        Get
            Return mblnLeaveIssueAsonDateOnLeaveBalanceListReport
        End Get
        Set(ByVal value As Boolean)
            mblnLeaveIssueAsonDateOnLeaveBalanceListReport = value
            setParamVal("LeaveIssueAsonDateOnLeaveBalanceListReport", mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
        End Set
    End Property

    'Pinkal (19-Feb-2020) -- End


    'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Private mblnRunCompetenceAssessmentSeparately As Boolean = False
    Public Property _RunCompetenceAssessmentSeparately() As Boolean
        Get
            Return mblnRunCompetenceAssessmentSeparately
        End Get
        Set(ByVal value As Boolean)
            mblnRunCompetenceAssessmentSeparately = value
            setParamVal("RunCompetenceAssessmentSeparately", value)
        End Set
    End Property
    'S.SANDEEP |13-NOV-2020| -- END

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private mblnIsPDP_Perf_Integrated As Boolean = False
    Public Property _IsPDP_Perf_Integrated() As Boolean
        Get
            Return mblnIsPDP_Perf_Integrated
        End Get
        Set(ByVal value As Boolean)
            mblnIsPDP_Perf_Integrated = value
            setParamVal("IsPDP_Perf_Integrated", mblnIsPDP_Perf_Integrated)
        End Set
    End Property
    'S.SANDEEP |03-MAY-2021| -- END

    'S.SANDEEP |13-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
    Private mblnAllowToCloseCaseWOProceedings As Boolean = False
    Public Property _AllowtocloseCaseWO_Proceedings() As Boolean
        Get
            Return mblnAllowToCloseCaseWOProceedings
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToCloseCaseWOProceedings = value
            setParamVal("AllowToCloseCaseWOProceedings", mblnAllowToCloseCaseWOProceedings)
        End Set
    End Property
    'S.SANDEEP |13-MAY-2021| -- END

'S.SANDEEP |17-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
    Private mblnDontAllowRatingBeyond100 As Boolean = False
    Public Property _DontAllowRatingBeyond100() As Boolean
        Get
            Return mblnDontAllowRatingBeyond100
        End Get
        Set(ByVal value As Boolean)
            mblnDontAllowRatingBeyond100 = value
            setParamVal("DontAllowRatingBeyond100", mblnDontAllowRatingBeyond100)
        End Set
    End Property
    'S.SANDEEP |17-MAY-2021| -- END

    'Hemant (24 Sep 2021) -- Start             
    'ISSUE/ENHANCEMENT : OLD-481(ZRA) - Give Configurable Option to include Case Information on Hearing Schedule Notification
    Private mblnAllowToIncludeCaseInfoHearingNotification As Boolean = False
    Public Property _AllowToIncludeCaseInfoHearingNotification() As Boolean
        Get
            Return mblnAllowToIncludeCaseInfoHearingNotification
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToIncludeCaseInfoHearingNotification = value
            setParamVal("AllowToIncludeCaseInfoHearingNotification", mblnAllowToIncludeCaseInfoHearingNotification)
        End Set
    End Property
    'Hemant (24 Sep 2021) -- End

    'Hemant (24 Sep 2021) -- Start             
    'ISSUE/ENHANCEMENT : OLD-482(ZRA) - Give Option to Notify The Accused/Charged Employee of Hearing Schedule
    Private mblnAllowToNotifyChargedEmployeeOfHearing As Boolean = False
    Public Property _AllowToNotifyChargedEmployeeOfHearing() As Boolean
        Get
            Return mblnAllowToNotifyChargedEmployeeOfHearing
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToNotifyChargedEmployeeOfHearing = value
            setParamVal("AllowToNotifyChargedEmployeeOfHearing", mblnAllowToNotifyChargedEmployeeOfHearing)
        End Set
    End Property
    'Hemant (24 Sep 2021) -- End

    'S.SANDEEP |01-OCT-2021| -- START
    Private mintDefaultHearingNotificationType As Integer = 0
    Public Property _DefaultHearingNotificationType() As Integer
        Get
            Return mintDefaultHearingNotificationType
        End Get
        Set(ByVal value As Integer)
            mintDefaultHearingNotificationType = value
            setParamVal("DefaultHearingNotificationType", mintDefaultHearingNotificationType)
        End Set
    End Property
    'S.SANDEEP |01-OCT-2021| -- END

#End Region

#Region " Private Methods "

    ''' <summary>
    ''' For assign database tabel (hrmsConfiguration..cfconfiguration) value into class property.
    ''' Modify By: Sandeep
    ''' </summary>
    Public Sub setLocalVars()
        Try
            mdtParam = createParamDataTable()

            For Each dtRow As DataRow In mdtParam.Rows

                'S.SANDEEP [09-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#292}
                If dtRow.Item("key_name").ToString.ToUpper.StartsWith("_FLXSRV_") Then
                    For Each item In [Enum].GetValues(GetType(enFlexcubeServiceInfo))
                        If CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_FLXSRV_", "")) = item Then
                            If mdicFlexcubeServiceCollection.ContainsKey(CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_FLXSRV_", ""))) = False Then
                                mdicFlexcubeServiceCollection.Add(item, CStr(dtRow.Item("key_value")))
                                Exit For
                            End If
                        End If
                    Next
                End If
                'S.SANDEEP [09-AUG-2018] -- END


                'Gajanan [9-Dec-2019] -- Start   
                'Enhancement:Worked On Orbit Integration Configuration Setup
                If dtRow.Item("key_name").ToString.ToUpper.StartsWith("_ORBSRV_") Then
                    For Each item In [Enum].GetValues(GetType(enOrbitServiceInfo))
                        If CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_ORBSRV_", "")) = item Then
                            If mdicOrbitServiceCollection.ContainsKey(CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_ORBSRV_", ""))) = False Then
                                mdicOrbitServiceCollection.Add(item, CStr(dtRow.Item("key_value")))
                                Exit For
                            End If
                        End If
                    Next
                End If
                'Gajanan [9-Dec-2019] -- End



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If dtRow.Item("key_name").ToString.ToUpper.StartsWith("_REJEMPDATA_") Then
                    For Each item In [Enum].GetValues(GetType(enScreenName))
                        If CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_REJEMPDATA_", "")) = item Then
                            If mdicEmployeeDataRejectedUserIds.ContainsKey(CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_REJEMPDATA_", ""))) = False Then
                                Dim strUserIds As String = ""
                                GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), strUserIds)
                                mdicEmployeeDataRejectedUserIds.Add(item, strUserIds)
                                Exit For
                            End If
                        End If
                    Next
                End If
                'Gajanan [17-DEC-2018] -- End

                'S.SANDEEP |10-AUG-2021| -- START
                If dtRow.Item("key_name").ToString.ToUpper.StartsWith("_ESTAT_") Then
                    For Each item In [Enum].GetValues(GetType(enObjective_Status))
                        If CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_ESTAT_", "")) = item Then
                            If mdicBSC_StatusColors.ContainsKey(CInt(dtRow.Item("key_name").ToString.ToUpper.Replace("_ESTAT_", ""))) = False Then
                                mdicBSC_StatusColors.Add(item, CInt(dtRow.Item("key_value")))
                                Exit For
                            End If
                        End If
                    Next
                End If
                'S.SANDEEP |10-AUG-2021| -- END

                Select Case dtRow.Item("key_name").ToString.ToUpper
                    '************** GENERAL ************* ==> START
                    '-- Payslip 
                    Case "PAYSLIPVOCNOTYPE"
                        mintPayslipVocNoType = CInt(dtRow.Item("key_value"))
                    Case "PAYSLIPVOCPREFIX"
                        mstrPayslipVocPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTPAYSLIPVOCNO"
                        mintNextPayslipVocNo = CInt(dtRow.Item("key_value"))

                        '-- Loan
                    Case "LOANVOCNOTYPE"
                        mintLoanVocNoType = CInt(dtRow.Item("key_value"))
                    Case "LOANVOCPREFIX"
                        mstrLoanVocPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTLOANVOCNO"
                        mintNextLoanVocNo = CInt(dtRow.Item("key_value"))

                        '-- Savings
                    Case "SAVINGSVOCNOTYPE"
                        mintSavingsVocNoType = CInt(dtRow.Item("key_value"))
                    Case "SAVINGSVOCPREFIX"
                        mstrSavingsVocPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTSAVINGSVOCNO"
                        mintNextSavingsVocNo = CInt(dtRow.Item("key_value"))

                        '-- Payment
                    Case "PAYMENTVOCNOTYPE"
                        mintPaymentVocNoType = CInt(dtRow.Item("key_value"))
                    Case "PAYMENTVOCPREFIX"
                        mstrPaymentVocPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTPAYMENTVOCNO"
                        mintNextPaymentVocNo = CInt(dtRow.Item("key_value"))

                        '-- View Option
                    Case "DISPLAYVIEW"
                        mintDisplayView = CInt(dtRow.Item("key_value"))

                        '-- Last Leave Process Date
                    Case "LEAVEPROCESSDATE"
                        mdtLeaveProcessDate = CStr(dtRow.Item("key_value"))

                        '-- Export / Import Parameters
                    Case "FILEFORMAT"
                        mintFileFormat = CInt(dtRow.Item("key_value"))
                    Case "SEPARATOR"
                        mstrSeparator = CStr(dtRow.Item("key_value"))
                        '-- Identity Sensitive
                    Case "ISIDSENSITIVE"
                        mblnIsIdSensitive = CBool(dtRow.Item("key_value"))
                        '-- Misc
                    Case "LOANAPPLICATIONNOTYPE"
                        mintLoanApplicationNoType = CInt(dtRow.Item("key_value"))
                    Case "LOANAPPLICATIONPRIFIX"
                        mstrLoanApplicationPrifix = CStr(dtRow.Item("key_value"))
                    Case "NEXTLOANAPPLICATIONNO"
                        mintNextLoanApplicationNo = CInt(dtRow.Item("key_value"))

                    Case "LEAVEFORMNOTYPE"
                        mintLeaveFormNoType = CInt(dtRow.Item("key_value"))
                    Case "LEAVEFORMNOPRIFIX"
                        mstrLeaveFormNoPrifix = CStr(dtRow.Item("key_value"))
                    Case "NEXTLEAVEFORMNO"
                        mintNextLeaveFormNo = CInt(dtRow.Item("key_value"))

                        'Sohail (28 May 2014) -- Start
                        'Enhancement - Staff Requisition.
                    Case "STAFFREQFORMNOTYPE"
                        mintStaffReqFormNoType = CInt(dtRow.Item("key_value"))
                    Case "STAFFREQFORMNOPREFIX"
                        mstrStaffReqFormNoPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTSTAFFREQFORMNO"
                        mintNextStaffReqFormNo = CInt(dtRow.Item("key_value"))
                    Case "APPLYSTAFFREQUISITION"
                        mblnApplyStaffRequisition = CBool(dtRow.Item("key_value"))
                        'Sohail (28 May 2014) -- End

                        'Hemant (03 Sep 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 7 : During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs. – TC001 ).
                    Case "ENFORCEMANPOWERPLANNING"
                        mblnEnforceManpowerPlanning = CBool(dtRow.Item("key_value"))
                        'Hemant (03 Sep 2019) -- End

                    Case "EMPLOYEECODENOTYPE"
                        mintEmployeeCodeNotype = CInt(dtRow.Item("key_value"))
                    Case "EMPLOYEECODEPRIFIX"
                        mstrEmployeeCodePrifix = CStr(dtRow.Item("key_value"))
                    Case "NEXTEMPLOYEECODENO"
                        mintNextEmployeeCodeNo = CInt(dtRow.Item("key_value"))

                    Case "APPLICANTCODENOTYPE"
                        mintApplicantCodeNoType = CInt(dtRow.Item("key_value"))
                    Case "APPLICANTCODEPRIFIX"
                        mstrApplicantCodePrifix = CStr(dtRow.Item("key_value"))
                    Case "NEXTAPPLICANTCODENO"
                        mintNextApplicantCodeNo = CInt(dtRow.Item("key_value"))

                    Case "RETIREMENTBY"
                        mintRetirementBy = CInt(dtRow.Item("key_value"))
                    Case "RETIREMENTVALUE"
                        mintRetirementValue = CInt(dtRow.Item("key_value"))

                        'S.SANDEEP [ 27 AUG 2014 ] -- START
                    Case "FRETIREMENTBY"
                        mintFRetirementBy = CInt(dtRow.Item("key_value"))
                    Case "FRETIREMENTVALUE"
                        mintFRetirementValue = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 27 AUG 2014 ] -- END

                    Case "FIRSTNAMETHENSURNAME"
                        mblnFirstNamethenSurname = CBool(dtRow.Item("key_value"))

                    Case "WEEKENDDAYS"
                        mstrWeekendDays = CStr(dtRow.Item("key_value"))

                    Case "NOOFDIGITAFTERDECIMALPOINT"
                        mintNoofDigitAfterDecimalPoint = CInt(dtRow.Item("key_value"))
                    Case "ROUNDOFFTYPE"
                        mdblRoundOffType = CDec(dtRow.Item("key_value"))
                    Case "MANUALROUNDOFFLIMIT"
                        mdblManualRoundOffLimit = CDec(dtRow.Item("key_value"))

                        'Sandeep [ 17 DEC 2010 ] -- Start
                    Case "ISDENOMINATIONCOMPULSORY"
                        mblnIsDenominationCompulsory = CBool(dtRow.Item("key_value"))
                        'Sandeep [ 17 DEC 2010 ] -- End 

                        'Sandeep [ 10 FEB 2011 ] -- Start
                    Case "ISDAILYBACKUPENABLED"
                        mblnIsDailyBackupEnabled = CBool(dtRow.Item("key_value"))
                    Case "BACKUPTIME"
                        If dtRow.Item("key_value").ToString <> "00.00.00" Then
                            mdtBackupTime = CDate(GetCurrentDateAndTime.Date & " " & dtRow.Item("key_value"))
                        End If

                        'Sandeep [ 10 FEB 2011 ] -- End 

                        'S.SANDEEP [ 29 JUNE 2011 ] -- START
                        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                    Case "INCLUDEINACTIVEEMPLOYEE"
                        mblnIncludeInactiveEmployee = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 29 JUNE 2011 ] -- END 


                        'Sohail (22 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "PAYMENTROUNDINGTYPE"
                        mintPaymentRoundingType = CDec(dtRow.Item("key_value"))

                    Case "PAYMENTROUNDINGMULTIPLE"
                        mintPaymentRoundingMultiple = CDec(dtRow.Item("key_value"))
                        'Sohail (22 Oct 2013) -- End

                        'Sohail (21 Mar 2014) -- Start
                        'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                    Case "CFROUNDINGABOVE"
                        mdecCFRoundingAbove = CDec(dtRow.Item("key_value"))
                        'Sohail (21 Mar 2014) -- End

                        'Pinkal (12-Oct-2011) -- Start
                        'Enhancement : TRA Changes


                    Case "SICKSHEETNOTYPE"
                        mintSickSheetNoType = CInt(dtRow.Item("key_value"))
                    Case "SICKSHEETPREFIX"
                        mstrSickSheetPrifix = CStr(dtRow.Item("key_value"))
                    Case "NEXTSICKSHEETNO"
                        mintNextSickSheetNo = CInt(dtRow.Item("key_value"))

                        'Pinkal (12-Oct-2011) -- End

                        'S.SANDEEP [ 07 NOV 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "ISDEPENDANTAGELIMITMANDATORY"
                        mblnIsDependantAgeLimitMandatory = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 07 NOV 2011 ] -- END



                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes
                    Case "ISLVAPPROVERMANDATORYFORLEAVETYPE"
                        mblnIsLvApproverMandatoryForLeaveType = CBool(dtRow.Item("key_value"))
                        'Pinkal (06-Feb-2012) -- End


                        'Anjan [ 20 Feb 2013 ] -- Start
                        'ENHANCEMENT : TRA CHANGES
                    Case "SETPAYSLIPPAYAPPROVAL"
                        mblnSetPaySlipPayApproval = CBool(dtRow.Item("key_value"))
                        'Anjan [ 20 Feb 2013 ] -- End

                        'Sohail (21 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "SETPAYSLIPPAYMENTMANDATORY"
                        mblnSetPaySlipPaymentMandatory = CBool(dtRow.Item("key_value"))
                        'Sohail (21 Mar 2013) -- End

                        'Sohail (20 Jul 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "APPLYPAYPERACTIVITY"
                        mblnApplyPayPerActivity = CBool(dtRow.Item("key_value"))
                        'Sohail (20 Jul 2013) -- End

                        'Sohail (04 Aug 2016) -- Start
                        'Enhancement - 63.1 - Option on configuration to Send Password protected E-Payslip.
                    Case "SENDPASSWORDPROTECTEDEPAYSLIP"
                        mblnSendPasswordProtectedEPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (04 Aug 2016) -- End

                        'Sohail (23 Nov 2020) -- Start
                        'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
                    Case "IGNORENEGATIVENETPAYEMPLOYEESONJV"
                        mblnIgnoreNegativeNetPayEmployeesOnJV = CBool(dtRow.Item("key_value"))
                        'Sohail (23 Nov 2020) -- End

                        'Sohail (29 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "SHOWFIRSTAPPOINTMENTDATE"
                        mblnShowFirstAppointmentDate = CBool(dtRow.Item("key_value"))
                        'Sohail (29 Mar 2013) -- End
                        'Anjan [06 April 2015] -- Start
                        'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
                    Case "IMPORTPEOPLESOFTDATA"
                        mblnImportPeoplesoftData = CBool(dtRow.Item("key_value"))
                        'Anjan [06 April 2015] -- End

                        'Pinkal (14-Apr-2015) -- Start
                        'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
                    Case "ISLOANAPPROVERMANDATORYFORLOANSCHEME"
                        mblnIsLoanApproverMandatoryForLoanScheme = CBool(dtRow.Item("key_value"))
                        'Pinkal (14-Apr-2015) -- End

                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    Case "ISSENDLOANEMAILFROMDESKTOPMSS"
                        mblnIsSendLoanEmailFromDesktopMSS = CBool(dtRow.Item("key_value"))

                    Case "ISSENDLOANEMAILFROMESS"
                        mblnIsSendLoanEmailFromESS = CBool(dtRow.Item("key_value"))
                        'Nilay (10-Dec-2016) -- End

                        'Sohail (16 May 2018) -- Start
                        'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                    Case "ADVANCE_NETPAYPERCENTAGE"
                        mdecAdvance_NetPayPercentage = CDec(dtRow.Item("key_value"))

                    Case "ADVANCE_NETPAYTRANHEADUNKID"
                        mintAdvance_NetPayTranheadUnkid = CInt(dtRow.Item("key_value"))
                        'Sohail (16 May 2018) -- End

                        'Gajanan (23-May-2018) -- Start
                        'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                    Case "ADVANCE_DONTALLOWAFTERDAYS"
                        minAdvance_DontAllowAfterDays = CInt(dtRow.Item("key_value"))
                        'Gajanan (23-May-2018) -- End 

                        'Sohail (14 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                    Case "ADVANCE_COSTCENTERUNKID"
                        mintAdvance_CostCenterunkid = CInt(dtRow.Item("key_value"))
                        'Sohail (14 Mar 2019) -- End

                        'SHANI (27 JUL 2015) -- Start
                        'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
                    Case "DEPENDANTDOCSATTACHMENTMANDATORY"
                        mblnDependantDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                    Case "QUALIFICATIONCERTIFICATEATTACHMENTMANDATORY"
                        mblnQualificationCertificateAttachmentMandatory = CBool(dtRow.Item("key_value"))
                        'SHANI (27 JUL 2015) -- End 

                        '************** GENERAL ************* ==> END

                        '************** DISPLAY ************* ==> START
                        '-- Country
                    Case "COUNTRYUNKID"
                        mintCountryUnkid = CInt(dtRow.Item("key_value"))

                        '-- Currency
                    Case "CURRENCYSIGN"
                        mstrCurrencySign = CStr(dtRow.Item("key_value"))

                        '-- Captions
                    Case "REG1CAPTION"
                        mstrReg1Caption = CStr(dtRow.Item("key_value"))
                    Case "REG2CAPTION"
                        mstrReg2Caption = CStr(dtRow.Item("key_value"))
                    Case "REG3CAPTION"
                        mstrReg3Caption = CStr(dtRow.Item("key_value"))
                        '************** DISPLAY ************* ==> END

                        '************** PATHS ************* ==> START
                        '-- General Path
                    Case "PHOTOPATH"
                        mstrPhotoPath = CStr(dtRow.Item("key_value"))
                    Case "IDENTITYPATH"
                        mstrIdentifyPath = CStr(dtRow.Item("key_value"))
                    Case "CERTIFICATEPATH"
                        mstrCertificatePath = CStr(dtRow.Item("key_value"))
                    Case "OTHERPATH"
                        mstrOtherPath = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 18 FEB 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "DOCUMENTPATH"
                        mstrDocumentPath = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 18 FEB 2012 ] -- START



                        '-- Report Settings
                    Case "EXPORTREPORTPATH"
                        mstrExportReportPath = CStr(dtRow.Item("key_value"))
                    Case "OPENAFTEREXPORT"
                        mblnOpenAfterExport = CBool(dtRow.Item("key_value"))

                        '-- Database Export Path
                    Case "DATABASEEXPORTPATH"
                        mstrDatabaseExportPath = CStr(dtRow.Item("key_value"))
                        '-- Export Data Path
                    Case "EXPORTDATAPATH"
                        mstrExportDataPath = CStr(dtRow.Item("key_value"))
                        '************** PATHS ************* ==> END

                        '************** INTEGRATION ************* ==> START
                        '-- Accouting S/W
                    Case "ACCOUNTINGSOFTWARE"
                        mintAccountingSoftWare = CInt(dtRow.Item("key_value"))



                        'Pinkal (20-Jan-2012) -- Start
                        'Enhancement : TRA Changes

                        'Case "COMMTYPEID"
                        '    mintCommunicationTypeId = CInt(dtRow.Item("key_value"))

                        'Case "DEVICEID"
                        '    mintDeviceId = CInt(dtRow.Item("key_value"))

                        'Case "CONNECTIONTYPEID"
                        '    mintConnectionTypeId = CInt(dtRow.Item("key_value"))

                        'Case "IPADDRESS"
                        '    mstrIPAddress = CStr(dtRow.Item("key_value"))

                        'Case "PORTNO"
                        '    mstrPortNo = CStr(dtRow.Item("key_value"))

                        'Case "BAUDRATE"
                        '    mstrBaudRate = CStr(dtRow.Item("key_value"))

                        'Case "MACHINESR"
                        '    mstrMachineSr = CStr(dtRow.Item("key_value"))

                        'Pinkal (20-Jan-2012) -- End

                        'Anjan (21 Jul 2011)-Start
                        'purpose : included web settings.
                    Case "WEBCLIENTCODE"
                        mintWebClientCode = CInt(dtRow.Item("key_value"))
                    Case "AUTHENTICATIONCODE"
                        mstrAuthenticationCode = CStr(dtRow.Item("key_value"))
                    Case "WEBURL"
                        mstrWebURL = CStr(dtRow.Item("key_value"))
                        'Anjan (21 Jul 2011)-End 

                        'Sohail (30 Dec 2011) -- Start
                    Case "WEBURLINTERNAL"
                        mstrWebURLInternal = CStr(dtRow.Item("key_value"))
                        'Sohail (30 Dec 2011) -- End



                        'Anjan (02 Mar 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    Case "EDITADDRESS"
                        mstrEditAddress = CStr(dtRow.Item("key_value"))
                    Case "EDITPERSONALINFO"
                        mstrEditPersonalInfo = CStr(dtRow.Item("key_value"))
                    Case "EDITEMERGENCYADDRESS"
                        mstrEditEmergencyAddress = CStr(dtRow.Item("key_value"))
                    Case "ADDIDENTITY"
                        mstrAddIdentity = CStr(dtRow.Item("key_value"))
                    Case "EDITIDENTITY"
                        mstrEditIdentity = CStr(dtRow.Item("key_value"))
                    Case "DELETEIDENTITY"
                        mstrDeleteIdentity = CStr(dtRow.Item("key_value"))
                    Case "ADDDEPENDANTS"
                        mstrAddDependants = CStr(dtRow.Item("key_value"))
                    Case "EDITDEPENDANTS"
                        mstrEditDependants = CStr(dtRow.Item("key_value"))
                    Case "DELETEDEPENDANTS"
                        mstrDeleteDependants = CStr(dtRow.Item("key_value"))
                    Case "ADDMEMBERSHIP"
                        mstrAddMembership = CStr(dtRow.Item("key_value"))
                    Case "EDITMEMBERSHIP"
                        mstrEditMembership = CStr(dtRow.Item("key_value"))
                    Case "DELETEMEMBERSHIP"
                        mstrDeleteMembership = CStr(dtRow.Item("key_value"))
                    Case "ADDSKILLS"
                        mstrAddSkills = CStr(dtRow.Item("key_value"))
                    Case "EDITSKILLS"
                        mstrEditSkills = CStr(dtRow.Item("key_value"))
                    Case "DELETESKILLS"
                        mstrDeleteSkills = CStr(dtRow.Item("key_value"))
                    Case "ADDQUALIFICATION"
                        mstrAddQualification = CStr(dtRow.Item("key_value"))
                    Case "EDITQUALIFICATION"
                        mstrEditQualification = CStr(dtRow.Item("key_value"))
                    Case "DELETEQUALIFICATION"
                        mstrDeleteQualification = CStr(dtRow.Item("key_value"))
                    Case "ADDEXPERIENCE"
                        mstrAddExperience = CStr(dtRow.Item("key_value"))
                    Case "EDITEXPERIENCE"
                        mstrEditExperience = CStr(dtRow.Item("key_value"))
                    Case "DELETEEXPERIENCE"
                        mstrDeleteExperience = CStr(dtRow.Item("key_value"))
                    Case "ADDREFERENCE"
                        mstrAddReference = CStr(dtRow.Item("key_value"))
                    Case "EDITREFERENCE"
                        mstrEditReference = CStr(dtRow.Item("key_value"))
                    Case "DELETEREFERENCE"
                        mstrDeleteReference = CStr(dtRow.Item("key_value"))

                        'Anjan (02 Mar 2012)-End 
                        'S.SANDEEP [ 06 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "CHANGECOMPANYEMAIL"
                        mblnAllowChangeCompanyEmail = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 06 DEC 2012 ] -- END

                        'Anjan (02 Mar 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    Case "APPLICANTDECLARATION"
                        mstrAppDeclaration = CStr(dtRow.Item("key_value"))
                        'Anjan (02 Mar 2012)-End 

                        'Sohail (25 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "VIEWPERSONALSALARYCALCULATIONREPORT"
                        mblnAllowToViewPersonalSalaryCalculationReport = CStr(dtRow.Item("key_value"))

                    Case "VIEWEDDETAILREPORT"
                        mblnAllowToViewEDDetailReport = CStr(dtRow.Item("key_value"))
                        'Sohail (25 Mar 2013) -- End

                        'Pinkal (01-Apr-2013) -- Start
                        'Enhancement : TRA Changes

                    Case "ADDEDITIMAGEFORESS"
                        mblnAllowtoAddEditImgForESS = CBool(dtRow.Item("key_value"))

                        'Pinkal (01-Apr-2013) -- End

                        'Sohail (30 Apr 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "SMIMERUNPATH"
                        mstrSMimeRunPath = CStr(dtRow.Item("key_value"))

                    Case "DATAFILEEXPORTPATH"
                        mstrDatafileExportPath = CStr(dtRow.Item("key_value"))

                    Case "DATAFILENAME"
                        mstrDatafileName = CStr(dtRow.Item("key_value"))
                        'Sohail (30 Apr 2013) -- End

                        'Sohail (09 Jan 2016) -- Start
                        'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
                    Case "EFTCITIDIRECTCOUNTRYCODE"
                        mstrEFTCitiDirectCountryCode = CStr(dtRow.Item("key_value"))

                    Case "EFTCITIDIRECTSKIPPRIORITYFLAG"
                        mblnEFTCitiDirectSkipPriorityFlag = CBool(dtRow.Item("key_value"))

                    Case "EFTCITIDIRECTCHARGESINDICATOR"
                        mstrEFTCitiDirectChargesIndicator = CStr(dtRow.Item("key_value"))

                    Case "EFTCITIDIRECTADDPAYMENTDETAIL"
                        mblnEFTCitiDirectAddPaymentDetail = CBool(dtRow.Item("key_value"))
                        'Sohail (09 Jan 2016) -- End


                        '************** INTEGRATION ************* ==> END

                        'Sandeep [ 07 FEB 2011 ] -- START
                        '************** SETTINGS ************* ==> START
                    Case "PAGEMARGIN"
                        mdblPageMargin = CDec(dtRow.Item("key_value"))
                    Case "NOOFPAGESTOPRINT"
                        mintNoofPagestoPrint = CInt(dtRow.Item("key_value"))
                    Case "SHOWPROPERTYINFO"
                        mblnShowPropertyInfo = CBool(dtRow.Item("key_value"))
                    Case "DISPLAYLOGO"
                        mstrDisplayLogo = CStr(dtRow.Item("key_value"))
                    Case "PREPAREDBY"
                        mstrPreparedBy = CStr(dtRow.Item("key_value"))
                    Case "APPROVEDBY"
                        mstrApprovedBy = CStr(dtRow.Item("key_value"))
                    Case "CHECKEDBY"
                        mstrCheckedBy = CStr(dtRow.Item("key_value"))
                    Case "RECEIVEDBY"
                        mstrReceivedBy = CStr(dtRow.Item("key_value"))
                    Case "SHOWLOGORIGHTSIDE"
                        mblnShowLogoRightSide = CBool(dtRow.Item("key_value"))
                        '************** SETTINGS ************* ==> END
                        'Sandeep [ 07 FEB 2011 ] -- END 

                        'Anjan (11 Mar 2011)-Start

                    Case "LEFTMARGIN"
                        mstrLeftMargin = CStr(dtRow.Item("key_value"))
                    Case "RIGHTMARGIN"
                        mstrRightMargin = CStr(dtRow.Item("key_value"))
                        'Anjan (11 Mar 2011)-End


                        'Sandeep [ 01 MARCH 2011 ] -- Start
                    Case "BASECURRENCYID"
                        mintBaseCurrencyId = CInt(dtRow.Item("key_value"))
                    Case "CURRENCYFORMAT"
                        mstrCurrencyFormat = CStr(dtRow.Item("key_value"))
                        'Sandeep [ 01 MARCH 2011 ] -- End 

                        'S.SANDEEP [ 08 June 2011 ] -- START
                        'ISSUE : LEAVE C/F OPTIONS
                    Case "LEAVECF_OPTIONID"
                        mintLeaveCF_OptionId = CInt(dtRow.Item("key_value"))
                    Case "LEAVEMAXVALUE"
                        mintLeaveMaxValue = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 08 June 2011 ] -- END

                        'Sohail (26 Nov 2011) -- Start
                        'TRA - Customize Payslip Staff Details i.e Branch,Department,Job,Grade,Appointment Date ,Retirement Date
                    Case "SHOWBRANCHONPAYSLIP"
                        mblnShowBranchOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWDEPARTMENTONPAYSLIP"
                        mblnShowDepartmentOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWJOBONPAYSLIP"
                        mblnShowJobOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWGRADEONPAYSLIP"
                        mblnShowGradeOnPayslip = CBool(dtRow.Item("key_value"))


                        'Anjan [22 Apr 2014] -- Start
                        'ENHANCEMENT : Requested by Rutta
                    Case "SHOWGRADEGROUPONPAYSLIP"
                        mblnShowGradeGroupOnPayslip = CBool(dtRow.Item("key_value"))
                    Case "SHOWGRADELEVELONPAYSLIP"
                        mblnShowGradeLevelOnPayslip = CBool(dtRow.Item("key_value"))
                        'Anjan [22 Apr 2014 ] -- End


                    Case "SHOWAPPOINTMENTDATEONPAYSLIP"
                        mblnShowAppointmentDateOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWRETIREMENTDATEONPAYSLIP"
                        mblnShowRetirementDateOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (26 Nov 2011) -- End
                        'Sohail (05 Dec 2011) -- Start
                        'TRA - Customize Payslip Staff Details i.e Branch,Department,Job,Grade,Appointment Date ,Retirement Date
                    Case "PAYSLIPTEMPLATE"
                        mintPayslipTemplate = CInt(dtRow.Item("key_value"))

                    Case "SHOWDEPARTMENTGROUPONPAYSLIP"
                        mblnShowDepartmentGroupOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWSECTIONGROUPONPAYSLIP"
                        mblnShowSectionGroupOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWSECTIONONPAYSLIP"
                        mblnShowSectionOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWUNITGROUPONPAYSLIP"
                        mblnShowUnitGroupOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWUNITONPAYSLIP"
                        mblnShowUnitOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWTEAMONPAYSLIP"
                        mblnShowTeamOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (05 Dec 2011) -- End

                        'Sohail (06 Jan 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "EMPLOYEEASONDATE"
                        mstrEmployeeAsOnDate = dtRow.Item("key_value")
                        'Sohail (06 Jan 2012) -- End

                        'S.SANDEEP [ 29 DEC 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES 
                        'TYPE : EMPLOYEMENT CONTRACT PROCESS
                    Case "REVIEWERINFO"
                        mstrReviewerInfo = CStr(dtRow.Item("key_value"))
                    Case "ISALLOWFINALSAVE"
                        mblnIsAllowFinalSave = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 29 DEC 2011 ] -- END

                        'S.SANDEEP [ 23 JAN 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES :         
                    Case "BSC_BYEMPLOYEE"
                        mblnBSC_ByEmployee = CBool(dtRow.Item("key_value"))
                    Case "ISBSCOBJECTIVESAVED"
                        mblnIsBSCObjectiveSaved = CBool(dtRow.Item("key_value"))
                    Case "COMPANYNEEDREVIEWER"
                        mblnCompanyNeedReviewer = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 23 JAN 2012 ] -- END

                        'S.SANDEEP [ 04 FEB 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "USERACCESSMODESETTING"
                        'S.SANDEEP [ 31 MAY 2012 ] -- START
                        'ENHANCEMENT : TRA DISCIPLINE CHANGES
                        'mintUserAccessModeSetting = CInt(dtRow.Item("key_value"))
                        mstrUserAccessModeSetting = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 31 MAY 2012 ] -- END


                        'S.SANDEEP [ 04 FEB 2012 ] -- END

                        'Sohail (18 Feb 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "ORDERBYDESCENDINGONPAYSLIP"
                        mblnOrderByDescendingOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (18 Feb 2012) -- End


                        'S.SANDEEP [ 05 MARCH 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "CONFIRMATIONMONTH"
                        mintConfirmationMonth = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 05 MARCH 2012 ] -- END

                        'Sohail (06 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "ASSETDECLARATIONINSTRUCTION"
                        mstrAssetDeclarationInstruction = CStr(dtRow.Item("key_value"))
                        'Sohail (06 Apr 2012) -- End

                        'Sohail (17 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "BASICSALARYCOMPUTATION"
                        mintBasicSalaryComputation = CInt(dtRow.Item("key_value"))
                        'Sohail (17 Apr 2012) -- End

                        'Sohail (08 Feb 2022) -- Start
                        'Enhancement : OLD-548 : Give Option to apply Basic Salary Computation Setting on Flat Rate Heads.
                    Case "FLATRATEHEADSCOMPUTATION"
                        mintFlatRateHeadsComputation = CInt(dtRow.Item("key_value"))
                        'Sohail ((08 Feb 2022) -- End

                        'Sohail (16 May 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "IGNOREZEROVALUEHEADSONPAYSLIP"
                        mblnIgnoreZeroValueHeadsOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWLOANBALANCEONPAYSLIP"
                        mblnShowLoanBalanceOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWSAVINGBALANCEONPAYSLIP"
                        mblnShowSavingBalanceOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWEMPLOYERCONTRIBUTIONONPAYSLIP"
                        mblnShowEmployerContributionOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWALLHEADSONPAYSLIP"
                        mblnShowAllHeadsOnPayslip = CBool(dtRow.Item("key_value"))

                        'Sohail (19 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "SHOWINFORMATIONALHEADSONPAYSLIP"
                        mblnShowInformationalHeadsOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (19 Sep 2013) -- End

                        'Sohail (23 Dec 2013) -- Start
                        'Enhancement - Oman
                    Case "SHOWBIRTHDATEONPAYSLIP"
                        mblnShowBirthDateOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWAGEONPAYSLIP"
                        mblnShowAgeOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWNOOFDEPENDANTSONPAYSLIP"
                        mblnShowNoOfDependantsOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWMONTHLYSALARYONPAYSLIP"
                        mblnShowMonthlySalaryOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (23 Dec 2013) -- End


                        'Anjan [01 September 2016] -- Start
                        'ENHANCEMENT : Including Payslip settings on configuration.
                    Case "SHOWBANKACCOUNTNOONPAYSLIP"
                        mblnShowBankAccountNoOnPayslip = CBool(dtRow.Item("key_value"))
                        'Anjan [01 Sepetember 2016] -- End

                        'Hemant (23 Dec 2020) -- Start
                        'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
                    Case "SHOWREMAININGNOOFLOANINSTALLMENTSONPAYSLIP"
                        mblnShowRemainingNoOfLoanInstallmentsOnPayslip = CBool(dtRow.Item("key_value"))
                        'Hemant (23 Dec 2020) -- End

                        'Sohail (09 Jan 2014) -- Start
                        'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
                    Case "OT1HOURHEADID"
                        mintOT1HourHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT1HOURHEADNAME"
                        mstrOT1HourHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT2HOURHEADID"
                        mintOT2HourHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT2HOURHEADNAME"
                        mstrOT2HourHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT3HOURHEADID"
                        mintOT3HourHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT3HOURHEADNAME"
                        mstrOT3HourHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT4HOURHEADID"
                        mintOT4HourHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT4HOURHEADNAME"
                        mstrOT4HourHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT1AMOUNTHEADID"
                        mintOT1AmountHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT1AMOUNTHEADNAME"
                        mstrOT1AmountHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT2AMOUNTHEADID"
                        mintOT2AmountHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT2AMOUNTHEADNAME"
                        mstrOT2AmountHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT3AMOUNTHEADID"
                        mintOT3AmountHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT3AMOUNTHEADNAME"
                        mstrOT3AmountHeadName = CStr(dtRow.Item("key_value"))

                    Case "OT4AMOUNTHEADID"
                        mintOT4AmountHeadID = CInt(dtRow.Item("key_value"))

                    Case "OT4AMOUNTHEADNAME"
                        mstrOT4AmountHeadName = CStr(dtRow.Item("key_value"))
                        'Sohail (09 Jan 2014) -- End

                        'Sohail (26 May 2021) -- Start
                        'Netis Gabon Enhancement : OLD-357 : New Payslip  Template "Template 19".
                    Case "MEMBERSHIP1"
                        mintMembership1 = CInt(dtRow.Item("key_value"))

                    Case "MEMBERSHIP2"
                        mintMembership2 = CInt(dtRow.Item("key_value"))
                        'Sohail (26 May 2021) -- End

                        'Sohail (24 Feb 2014) -- Start
                        'Enhancement - Show Salary On Hold
                    Case "SHOWSALARYONHOLDONPAYSLIP"
                        mblnShowSalaryOnHoldOnPayslip = CStr(dtRow.Item("key_value"))
                        'Sohail (24 Feb 2014) -- End

                        'Sohail (24 Mar 2014) -- Start
                        'ENHANCEMENT - Show Company Name, Emp code, Membership and Payment Details on Payslip.
                    Case "SHOWCOMPANYNAMEONPAYSLIP"
                        mblnShowCompanyNameOnPayslip = CStr(dtRow.Item("key_value"))

                    Case "SHOWEMPLOYEECODEONPAYSLIP"
                        mblnShowEmployeeCodeOnPayslip = CStr(dtRow.Item("key_value"))

                    Case "SHOWMEMBERSHIPONPAYSLIP"
                        mblnShowMembershipOnPayslip = CStr(dtRow.Item("key_value"))

                    Case "SHOWPAYMENTDETAILSONPAYSLIP"
                        mblnShowPaymentDetailsOnPayslip = CStr(dtRow.Item("key_value"))
                        'Sohail (24 Mar 2014) -- End

                        'Sohail (27 Mar 2014) -- Start
                        'ENHANCEMENT - Show Company Stamp on Payslip.
                    Case "SHOWCOMPANYSTAMPONPAYSLIP"
                        mblnShowCompanyStampOnPayslip = CStr(dtRow.Item("key_value"))
                        'Sohail (27 Mar 2014) -- End

                        'Sohail (28 Jul 2014) -- Start
                        'Enhancement - Show Exchange Rate option for Payslip Template 9 for GHANA.
                    Case "SHOWEXCHANGERATEONPAYSLIP"
                        mblnShowExchangeRateOnPayslip = CStr(dtRow.Item("key_value"))

                    Case "ACTUALHEADID"
                        mintActualHeadID = CInt(dtRow.Item("key_value"))
                        'Sohail (28 Jul 2014) -- End

                        'Sohail (11 Sep 2014) -- Start
                        'Enhancement - New Payslip Template 10 for FINCA DRC.
                    Case "WORKINGDAYSHEADID"
                        mintWorkingDaysHeadID = CInt(dtRow.Item("key_value"))

                    Case "TAXABLEAMOUNTHEADID"
                        mintTaxableAmountHeadID = CInt(dtRow.Item("key_value"))

                    Case "IPRCALCULATIONHEADID"
                        mintIPRCalculationHeadID = CInt(dtRow.Item("key_value"))

                    Case "13THPAYHEADID"
                        mint13thPayHeadID = CInt(dtRow.Item("key_value"))

                    Case "AMTTOBEPAIDHEADID"
                        mintAmtToBePaidHeadID = CInt(dtRow.Item("key_value"))

                    Case "EMPLCOSTHEADID"
                        mintEmplCostHeadID = CInt(dtRow.Item("key_value"))

                    Case "NOOFDEPENDANTHEADID"
                        mintNoOfDependantHeadID = CInt(dtRow.Item("key_value"))

                    Case "NOOFCHILDHEADID"
                        mintNoOfChildHeadID = CInt(dtRow.Item("key_value"))
                        'Sohail (11 Sep 2014) -- End

                        'Sohail (20 Sep 2016) -- Start
                        'Enhancement - 63.1 - New Payslip Template 15 for TIMAJA.
                    Case "CUMULATIVETAXHEADID"
                        mintCumulativeTaxHeadID = CInt(dtRow.Item("key_value"))

                    Case "DAILYBASICSALARYHEADID"
                        mintDailyBasicSalaryHeadID = CInt(dtRow.Item("key_value"))

                    Case "ACCRUEDLEAVEPAYHEADID"
                        mintAccruedLeavePayHeadID = CInt(dtRow.Item("key_value"))
                        'Sohail (20 Sep 2016) -- End

                    Case "LOGOONPAYSLIP"
                        mintLogoOnPayslip = CInt(dtRow.Item("key_value"))
                        'Sohail (16 May 2012) -- End

                        'S.SANDEEP [ 16 MAY 2012 ] -- START
                        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    Case "FORCASTEDVIEWSETTING"
                        mintForcastedViewSetting = CInt(dtRow.Item("key_value"))
                    Case "FORCASTEDVALUE"
                        mintForcastedValue = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 16 MAY 2012 ] -- END

                        'S.SANDEEP [ 24 MAY 2012 ] -- START
                        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    Case "FORCASTEDEOCVIEWSETTING"
                        mintForcastedEOCViewSetting = CInt(dtRow.Item("key_value"))
                    Case "FORCASTEDEOCVALUE"
                        mintForcastedEOCValue = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 24 MAY 2012 ] -- END


                        'Pinkal (03-Nov-2014) -- Start
                        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
                    Case "FORCASTEDELCVIEWSETTING"
                        mintForcastedELCViewSetting = CInt(dtRow.Item("key_value"))
                    Case "FORCASTEDELCVALUE"
                        mintForcastedELCValue = CInt(dtRow.Item("key_value"))
                        'Pinkal (03-Nov-2014) -- End


                        'Sohail (06 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "DATABASESERVERSETTING"
                        mintDatabaseServerSetting = CInt(dtRow.Item("key_value"))

                    Case "DATABASESERVER"
                        mstrDatabaseServer = CStr(dtRow.Item("key_value"))

                    Case "DATABASENAME"
                        mstrDatabaseName = CStr(dtRow.Item("key_value"))

                    Case "DATABASEUSERNAME"
                        mstrDatabaseUserName = CStr(dtRow.Item("key_value"))

                    Case "DATABASEUSERPASSWORD"
                        mstrDatabaseUserPassword = CStr(dtRow.Item("key_value"))

                    Case "DATABASEOWNER"
                        mstrDatabaseOwner = CStr(dtRow.Item("key_value"))
                        'Sohail (06 Jun 2012) -- End

                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                    Case "RECRUITMENTCAREERLINK"
                        mstrRecruitmentCareerLink = CStr(dtRow.Item("key_value"))

                    Case "RECRUITMENTWEBSERVICELINK"
                        mstrRecruitmentWebServiceLink = CStr(dtRow.Item("key_value"))
                        'Sohail (16 Nov 2018) -- End

                        'Sohail (29 Jun 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "DONOTALLOWOVERDEDUCTIONPAYMENT"
                        mblnDoNotAllowOverDeductionPayment = CBool(dtRow.Item("key_value"))
                        'Sohail (29 Jun 2012) -- End

                        'Sohail (07 Aug 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "ACCOUNTING_TRANSACTIONREFERENCE"
                        mstrAccounting_TransactionReference = CStr(dtRow.Item("key_value"))

                    Case "ACCOUNTING_JOURNALTYPE"
                        mstrAccounting_JournalType = CStr(dtRow.Item("key_value"))

                    Case "ACCOUNTING_JVGROUPCODE"
                        mstrAccounting_JVGroupCode = CStr(dtRow.Item("key_value"))
                        'Sohail (07 Aug 2012) -- End

                        'Sohail (19 Feb 2016) -- Start
                        'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
                    Case "ACCOUNTING_COUNTRY"
                        mstrAccounting_Country = CStr(dtRow.Item("key_value"))
                        'Sohail (19 Feb 2016) -- End

                        'Hemant (15 Mar 2019) -- Start
                        'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
                    Case "B_AREA"
                        mstrB_Area = CStr(dtRow.Item("key_value"))
                        'Hemant (15 Mar 2019) -- End

                        'S.SANDEEP [ 18 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "CHECKED_COLUMNS"
                        mstrCheckedFields = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 18 AUG 2012 ] -- END

                        'Sohail (31 Aug 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "LEAVETYPEONPAYSLIP"
                        mintLeaveTypeOnPayslip = CInt(dtRow.Item("key_value"))
                        'Sohail (31 Aug 2012) -- End

                        'Sohail (11 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "SHOWCLASSGROUPONPAYSLIP"
                        mblnShowClassGroupOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWCLASSONPAYSLIP"
                        mblnShowClassOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWCUMULATIVEACCRUALONPAYSLIP"
                        mblnShowCumulativeAccrualOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (11 Sep 2012) -- End

                        'Sohail (31 Aug 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "SHOWCATEGORYONPAYSLIP"
                        mblnShowCategoryOnPayslip = CBool(dtRow.Item("key_value"))
                        'Sohail (31 Aug 2013) -- End

                        'Sohail (07 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "SETBASICSALARYASOTHEREARNINGONSTATUTORYREPORT"
                        mblnSetBasicSalaryAsOtherEarningOnStatutoryReport = CBool(dtRow.Item("key_value"))
                        'Sohail (07 Sep 2013) -- End

                        'S.SANDEEP [ 18 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "NOTIFY_DATES"
                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_EDate_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("||")(2), mstrNtf_EDate_Users)
                            If mstrNtf_EDate_Users.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_EDate_Users
                            End If
                            mstrNtf_EDate_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END
                    Case "NOTIFY_ALLOCATION"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_Alloc_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("||")(2), mstrNtf_Alloc_Users)

                            'Gajanan [15-NOV-2019] -- Start   
                            'If mstrNtf_EDate_Users.Trim.Length > 0 Then
                            '    dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_Alloc_Users
                            'End If

                            If mstrNtf_Alloc_Users.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_Alloc_Users
                            End If
                            'Gajanan [15-NOV-2019] -- End

                            mstrNtf_Alloc_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END



                    Case "NOTIFY_EMPLDATA"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_EData_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("||")(2), mstrNtf_EData_Users)
                            If mstrNtf_EData_Users.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_EData_Users
                            End If
                            mstrNtf_EData_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'S.SANDEEP [ 18 SEP 2012 ] -- END

                        'S.SANDEEP [ 03 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "NOTIFY_BANK"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_Bank_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("||")(2), mstrNtf_Bank_Users)
                            If mstrNtf_Bank_Users.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_Bank_Users
                            End If
                            mstrNtf_Bank_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'S.SANDEEP [ 03 OCT 2012 ] -- END

                        'Sohail (09 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "NOTIFY_PAYROLL"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_Payroll_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("||")(2), mstrNtf_Payroll_Users)
                            If mstrNtf_Payroll_Users.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_Payroll_Users
                            End If
                            mstrNtf_Payroll_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'Sohail (09 Mar 2013) -- End

                        'Nilay (17-Aug-2016) -- Start
                        'ENHANCEMENT : Add Loan Savings Notification Settings
                    Case "NOTIFY_LOANADVANCE"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_LoanAdvance_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("||")(2), mstrNtf_LoanAdvance_Users)
                            If mstrNtf_LoanAdvance_Users.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("||")(0) & "||" & mstrNtf_LoanAdvance_Users
                            End If
                            mstrNtf_LoanAdvance_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'Nilay (17-Aug-2016) -- END

                        'Sohail (07 Jun 2016) -- Start
                        'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
                    Case "NOTIFY_BUDGET_FUNDSOURCEPERCENTAGE"
                        mdecNtf_Budget_FundSourcePercentage = CDec(dtRow.Item("key_value"))

                    Case "NOTIFY_BUDGET_FUNDACTIVITYPERCENTAGE"
                        mdecNtf_Budget_FundActivityPercentage = CDec(dtRow.Item("key_value"))

                    Case "NOTIFY_BUDGET"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_Budget_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNtf_Budget_Users)
                            mstrNtf_Budget_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'Sohail (07 Jun 2016) -- End

                        'Pinkal (03-Nov-2014) -- Start
                        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
                    Case "NOTIFY_ISSUEDLEAVE"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_IssuedLeave_Users = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNtf_IssuedLeave_Users)
                            mstrNtf_IssuedLeave_Users = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'Pinkal (03-Nov-2014) -- Endq


                        'S.SANDEEP [ 21 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "ALLOCATION_HIERARCHY"
                        mstrAllocation_Hierarchy = CStr(dtRow.Item("key_value"))
                    Case "ISALLOCATION_HIERARCHY_SET"
                        mblnIsAllocation_Hierarchy_Set = CBool(dtRow.Item("key_value"))
                    Case "ALLOCATIONIDX"
                        mintAllocationIdx = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 21 SEP 2012 ] -- END


                        'Pinkal (01-Dec-2012) -- Start
                        'Enhancement : TRA Changes

                    Case "COMPANYDOMAIN"
                        mstrCompanyDomain = dtRow.Item("key_value")

                    Case "DISPLAYNAMESETTING"
                        mintDisplayNamesetting = dtRow.Item("key_value")
                        'Pinkal (01-Dec-2012) -- End

                        'S.SANDEEP [ 01 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "ISPASSWORDAUTOGENERATED"
                        mblnIsPasswordAutoGenerated = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 01 DEC 2012 ] -- END

                        'S.SANDEEP [ 14 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "SENDDETAILTOEMPLOYEE"
                        mblnSendDetailToEmployee = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 14 DEC 2012 ] -- END

                        'Sohail (19 Dec 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case "SHOWEOCDATEONPAYSLIP"
                        mblnShowEOCDateOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "SHOWLEAVINGDATEONPAYSLIP"
                        mblnShowLeavingDateOnPayslip = CBool(dtRow.Item("key_value"))

                    Case "BATCHPOSTINGNOTYPE"
                        mintBatchPostingNoType = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 23 JULY 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Case "BATCHPOSTINGPRIFIX" 
                    Case "BATCHPOSTINGPREFIX"
                        'S.SANDEEP [ 23 JULY 2013 ] -- END
                        mstrBatchPostingPrifix = CStr(dtRow.Item("key_value"))
                    Case "NEXTBATCHPOSTINGNO"
                        mintNextBatchPostingNo = CInt(dtRow.Item("key_value"))
                        'Sohail (19 Dec 2012) -- End

                        'Sohail (01 Jan 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "ARUTISELFSERVICEURL"
                        mstrArutiSelfServiceURL = CStr(dtRow.Item("key_value"))
                        'Sohail (01 Jan 2013) -- End

                        'Sohail (25 Jan 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "EFTINTEGRATION"
                        mintEFTIntegration = CInt(dtRow.Item("key_value"))

                    Case "EFTCODE"
                        mstrEFTCode = dtRow.Item("key_value")
                        'Sohail (25 Jan 2013) -- End

                        'Sohail (14 Mar 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case "PSPFINTEGRATION"
                        mintPSPFIntegration = CInt(dtRow.Item("key_value"))
                        'Sohail (14 Mar 2013) -- End

                        'Sohail (08 Dec 2014) -- Start
                        'Enhancement - New Mobile Money EFT integration with MPESA.
                    Case "MOBILEMONEYEFTINTEGRATION"
                        mintMobileMoneyEFTIntegration = CInt(dtRow.Item("key_value"))
                        'Sohail (08 Dec 2014) -- End

                        'Pinkal (24-Jan-2013) -- Start
                        'Enhancement : TRA Changes
                    Case "CONFIGDATABASEEXPORTPATH"
                        mstrConfigurationExportDataPath = CStr(dtRow.Item("key_value"))
                        'Pinkal (24-Jan-2013) -- End

                        'Pinkal (06-Feb-2013) -- Start
                        'Enhancement : TRA Changes

                    Case "LEAVEBALANCESETTING"
                        mintLeaveBalanceSetting = CStr(dtRow.Item("key_value"))

                    Case "ELCMONTHS"
                        mintELCMonths = CStr(dtRow.Item("key_value"))

                        'Pinkal (06-Feb-2013) -- End


                        'Pinkal (15-Sep-2013) -- Start
                        'Enhancement : TRA Changes

                    Case "CLOSEPAYROLLPERIODIFLEAVEISSUE"
                        mblnClosePayrollPeriodIfLeaveIssue = CBool(dtRow.Item("key_value"))

                        'Pinkal (15-Sep-2013) -- End


                        'Pinkal (20-Sep-2013) -- Start
                        'Enhancement : TRA Changes
                    Case "POLICYMANAGEMENTTNA"
                        mblnPolicyManagementTNA = CBool(dtRow.Item("key_value"))
                        'Pinkal (20-Sep-2013) -- End


                        'Pinkal (30-Nov-2013) -- Start
                        'Enhancement : Oman Changes

                    Case "DONOTATTENDANCEINSECONDS"
                        mblnDonotAttendanceinSeconds = CBool(dtRow.Item("key_value"))

                    Case "OT1_ORDER"
                        mstrOT1Order = CStr(dtRow.Item("key_value"))

                    Case "OT2_ORDER"
                        mstrOT2Order = CStr(dtRow.Item("key_value"))

                    Case "OT3_ORDER"
                        mstrOT3Order = CStr(dtRow.Item("key_value"))

                    Case "OT4_ORDER"
                        mstrOT4Order = CStr(dtRow.Item("key_value"))

                        'Pinkal (30-Nov-2013) -- End

                        'Pinkal (03-Jan-2014) -- Start
                        'Enhancement : Oman Changess

                    Case "ISSEPARATETNAPERIOD"
                        mblnIsSeparateTnAPeriod = CBool(dtRow.Item("key_value"))

                        'Pinkal (03-Jan-2014) -- End



                        'S.SANDEEP [ 28 FEB 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "SHOWBASICSALARYONSTATUTORYREPORT"
                        mblnShowBasicSalaryOnStatutoryReport = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 28 FEB 2013 ] -- END


                        'Pinkal (31-Jan-2014) -- Start
                        'Enhancement : Oman Changes
                    Case "SHOWBASICSALARYONLEAVEFORMREPORT"
                        mblnShowBasicSalaryOnLeaveFormReport = CBool(dtRow.Item("key_value"))
                        'Pinkal (31-Jan-2014) -- End

                        'Sohail (11 Mar 2016) -- Start
                        'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                    Case "SHOWPAYMENTAPPROVALDETAILSONPAYROLLSUMMARYREPORT"
                        mblnShowPaymentApprovalDetailsOnPayrollSummaryReport = CBool(dtRow.Item("key_value"))
                        'Sohail (11 Mar 2016) -- End

                        'Pinkal (18-Jun-2016) -- Start
                        'Enhancement - IMPLEMENTING Leave Liability Setting to Leave Liablity Report.
                    Case "LEAVELIABILITYREPORTSETTING"
                        mstrLeaveLiabilityReportSetting = CStr(dtRow.Item("key_value"))
                        'Pinkal (18-Jun-2016) -- End


                        'Pinkal (12-Mar-2013) -- Start
                        'Enhancement : TRA Changes

                    Case "ALLOWTOVIEWPAYSLIPONESS"
                        mblnAllowToviewPaysliponEss = CBool(dtRow.Item("key_value"))
                    Case "VIEWPAYSLIPDAYSBEFORE"
                        mintViewPayslipDaysBefore = CInt(dtRow.Item("key_value"))

                        'Pinkal (12-Mar-2013) -- End

                        'Nilay (03-Nov-2016) -- Start
                        'Enhancement : Option to Show / hide payslip on ESS
                    Case "SHOWHIDEPAYSLIPONESS"
                        mblnShowHidePayslipOnESS = CBool(dtRow.Item("key_value"))
                        'Nilay (03-Nov-2016) -- End

                        'Sohail (29 Mar 2017) -- Start
                        'CCK Enhancement - 65.1 - Payslip to be accessible on ess only after payment authorisation. Provide setting on configuration as earlier agreed. 
                    Case "DONTSHOWPAYSLIPONESSIFPAYMENTNOTAUTHORIZED"
                        mblnDontShowPayslipOnESSIfPaymentNotAuthorized = CBool(dtRow.Item("key_value"))
                        'Sohail (29 Mar 2017) -- End

                        'Sohail (15 May 2020) -- Start
                        'NMB Enhancement # : Option Don't Show Payslip On ESS If Payment is Not Done on aruti configuration.
                    Case "DONTSHOWPAYSLIPONESSIFPAYMENTNOTDONE"
                        mblnDontShowPayslipOnESSIfPaymentNotDone = CBool(dtRow.Item("key_value"))
                        'Sohail (15 May 2020) -- End

                        'Sohail (23 Jan 2017) -- Start
                        'CCBRT Enhancement - 65.1 - Show Statutory Message on ESS Payslip.
                    Case "STATUTORYMESSAGEONPAYSLIPONESS"
                        mstrStatutoryMessageOnPayslipOnESS = CStr(dtRow.Item("key_value"))
                        'Sohail (23 Jan 2017) -- End

                        'S.SANDEEP [ 28 MAR 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "ISIMGINDATABASE"
                        mblnIsImgInDataBase = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 28 MAR 2013 ] -- END

                        'S.SANDEEP [ 08 APR 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "ERC_SENDNTF_MANUALLY"
                        mblnERC_SendNtf_Manually = CBool(dtRow.Item("key_value"))
                    Case "ERC_RESENDNFT_MANUALLY"
                        mblnERC_ResendNft_Manually = CBool(dtRow.Item("key_value"))

                    Case "ERC_EOC_SETTING"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrERC_EOC_Setting = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_EOC_Setting)
                            If mstrERC_EOC_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_EOC_Setting
                            End If
                            mstrERC_EOC_Setting = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                    Case "ERC_RET_SETTING"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrERC_RET_Setting = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_RET_Setting)
                            If mstrERC_RET_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_RET_Setting
                            End If
                            mstrERC_RET_Setting = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                    Case "ERC_CNF_SETTING"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrERC_CNF_Setting = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_CNF_Setting)
                            If mstrERC_CNF_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_CNF_Setting
                            End If
                            mstrERC_CNF_Setting = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                    Case "ERC_PED_SETTING"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrERC_PED_Setting = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_PED_Setting)
                            If mstrERC_PED_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_PED_Setting
                            End If
                            mstrERC_PED_Setting = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                    Case "ERC_BRT_SETTING"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrERC_BRT_Setting = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(3), mstrERC_BRT_Setting)
                            If mstrERC_BRT_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & mstrERC_BRT_Setting
                            End If
                            mstrERC_BRT_Setting = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'S.SANDEEP [ 16 NOV 2013 ] -- START
                    Case "ERC_ID_SETTING"

                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrERC_ID_Setting = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(3), mstrERC_ID_Setting)
                            If mstrERC_ID_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & mstrERC_ID_Setting
                            End If
                            mstrERC_ID_Setting = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                    Case "ERC_ID_TEMPLATEID"
                        mintERC_ID_TemplateId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 16 NOV 2013 ] -- END
                    Case "ERC_EOC_TEMPLATEID"
                        mintERC_EOC_TemplateId = CInt(dtRow.Item("key_value"))
                    Case "ERC_RET_TEMPLATEID"
                        mintERC_RET_TemplateId = CInt(dtRow.Item("key_value"))
                    Case "ERC_CNF_TEMPLATEID"
                        mintERC_CNF_TemplateId = CInt(dtRow.Item("key_value"))
                    Case "ERC_PED_TEMPLATEID"
                        mintERC_PED_TemplateId = CInt(dtRow.Item("key_value"))
                    Case "ERC_BRT_TEMPLATEID"
                        mintERC_BRT_TemplateId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 08 APR 2013 ] -- END

                        'S.SANDEEP [ 09 AUG 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "ALLOWASSESSOR_BEFORE_EMP"
                        mblnAllowAssessorAssessment = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 09 AUG 2013 ] -- END

                        'S.SANDEEP [ 14 AUG 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "PERFORMANCECOMPUTATION"
                        mintPerformanceComputation = CInt(dtRow.Item("key_value"))
                    Case "WAVGEMPCOMPUTATIONTYPE"
                        mblnAvgEmpComputType = CBool(dtRow.Item("key_value"))
                    Case "WAVGASECOMPUTTYPE"
                        mblnAvgAseComputType = CBool(dtRow.Item("key_value"))
                    Case "WAVGREVCOMPUTTYPE"
                        mblnAvgRevComputType = CBool(dtRow.Item("key_value"))
                    Case "WSCRCOMPUTTYPE"
                        mintScrComputType = CInt(dtRow.Item("key_value"))
                    Case "EXCLUDEPROBATEDEMPONPERFORMANCE"
                        mblnExcludeProbatedEmpOnPerformance = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 14 AUG 2013 ] -- END

                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        'ENHANCEMENT : ENHANCEMENT
                    Case "CONSIDERITEMWEIGHTASNUMBER"
                        mblnConsiderItemWeightAsNumber = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                        'Pinkal (21-Oct-2013) -- Start
                        'Enhancement : Oman Changes

                    Case "ASR_LEAVE1"
                        mintASR_Leave1 = CStr(dtRow.Item("key_value"))

                    Case "ASR_LEAVE2"
                        mintASR_Leave2 = CStr(dtRow.Item("key_value"))

                    Case "ASR_LEAVE3"
                        mintASR_Leave3 = CStr(dtRow.Item("key_value"))

                    Case "ASR_LEAVE4"
                        mintASR_Leave4 = CStr(dtRow.Item("key_value"))

                    Case "ASR_LVTYPE"
                        mstrSpecial_LeaveIds = CStr(dtRow.Item("key_value"))

                        'Pinkal (21-Oct-2013) -- End


                        'Pinkal (1-Jul-2014) -- Start
                        'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
                    Case "ASR_LEAVE5"
                        mintASR_Leave5 = CStr(dtRow.Item("key_value"))
                        'Pinkal (1-Jul-2014) -- End


                        'Pinkal (25-Feb-2015) -- Start
                        'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
                    Case "ASR_SHOWTOTALHRS"
                        mblnASR_ShowTotalhrs = CStr(dtRow.Item("key_value"))
                        'Pinkal (25-Feb-2015) -- End



                        'Pinkal (09-Jun-2015) -- Start
                        'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                    Case "EMPTIMESHEETSETTING"
                        mstrEmpTimesheetSetting = CStr(dtRow.Item("key_value"))


                        'Pinkal (06-May-2016) -- Start
                        'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                        'Case "SETDEVICEINOUTSTATUS"
                        'mblnSetDeviceInOutStatus = CBool(dtRow.Item("key_value"))
                        'Pinkal (06-May-2016) -- End


                        'Pinkal (09-Jun-2015) -- End



                        'S.SANDEEP [ 20 DEC 2013 ] -- START
                    Case "FIRSTCHECKINLASTCHECKOUT"
                        mblnFirstCheckInLastCheckOut = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [ 20 DEC 2013 ] -- END

                        'S.SANDEEP [ 04 FEB 2014 ] -- START
                    Case "CLAIMREQUESTVOCNOTYPE"
                        mintClaimRequestVocNoType = CInt(dtRow.Item("key_value"))
                    Case "CLAIMREQUESTPREFIX"
                        mstrClaimRequestPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTCLAIMREQUESTVOCNO"
                        mintNextClaimRequestVocNo = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 04 FEB 2014 ] -- END

                        'Pinkal (06-Mar-2014) -- Start
                        'Enhancement : TRA Changes
                    Case "CLAIMREQUEST_PAYMENTAPPROVALWITHLEAVEAPPROVAL"
                        mblnPaymentApprovalwithLeaveApproval = CBool(dtRow.Item("key_value"))
                        'Pinkal (06-Mar-2014) -- End


                        'Pinkal (01-Feb-2014) -- Start
                        'Enhancement : TRA Changes

                    Case "ISAUTOMATICLEAVEISSUEONFINALAPPROVAL"
                        mblnIsAutomaticLeaveIssueOnFinalApproval = CBool(dtRow.Item("key_value"))

                        'Pinkal (01-Feb-2014) -- End 


                        'Pinkal (30-Jul-2015) -- Start
                        'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                    Case "ALLOWTOCHANGEATTENDANCEAFTERPAYMENT"
                        mblnAllowTochangeAttendanceAfterPayment = CBool(dtRow.Item("key_value"))
                        'Pinkal (30-Jul-2015) -- End

                        'Pinkal (22-Mar-2016) -- Start
                        'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                    Case "SECTORROUTEASSIGNTOEMPS"
                        mblnSectorRouteAssignToEmp = CBool(dtRow.Item("key_value"))
                        'Pinkal (22-Mar-2016) -- End

                        'Pinkal (20-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    Case "SECTORROUTEASSIGNTOEXPENSE"
                        mblnSectorRouteAssignToExpense = CBool(dtRow.Item("key_value"))
                        'Pinkal (20-Feb-2019) -- End


                        'Pinkal (16-Apr-2016) -- Start
                        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    Case "COMPANYDATEFORMAT"
                        mstrCompanyDateFormat = CStr(dtRow.Item("key_value"))

                    Case "COMPANYDATESEPARATOR"
                        mstrCompanyDateSeparator = CStr(dtRow.Item("key_value"))

                        'Pinkal (16-Apr-2016) -- End


                        'S.SANDEEP [ 19 JUL 2014 ] -- START
                    Case "HOURAMTMAPPING"
                        mstrHourAmtMapping = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 19 JUL 2014 ] -- END

                        'S.SANDEEP [ 29 SEP 2014 ] -- START
                    Case "CUSTOMPAYROLLREPORTHEADSIDS"
                        mstrCustomPayrollReportHeadsIds = CStr(dtRow.Item("key_value"))
                    Case "CUSTOMPAYROLLREPORTALLOCIDS"
                        mstrCustomPayrollReportAllocIds = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 29 SEP 2014 ] -- END

                        'Sohail (20 Apr 2015) -- Start
                        'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                    Case "CUSTOMPAYROLLREPORTTEMPLATE2HEADSIDS"
                        mstrCustomPayrollReportTemplate2HeadsIds = CStr(dtRow.Item("key_value"))

                    Case "CUSTOMPAYROLLREPORTTEMPLATE2ALLOCIDS"
                        mstrCustomPayrollReportTemplate2AllocIds = CStr(dtRow.Item("key_value"))
                        'Sohail (20 Apr 2015) -- End

                        'Sohail (09 Apr 2015) -- Start
                        'CCK Enhancement - New report Detailed Salary Breakdown report.
                    Case "DETAILEDSALARYBREAKDOWNREPORTHEADSIDS"
                        mstrDetailedSalaryBreakdownReportHeadsIds = CStr(dtRow.Item("key_value"))
                        'Sohail (09 Apr 2015) -- End

                        'Sohail (20 Feb 2017) -- Start
                        'CCBRT Enhancement - 65.1 - New Report 'Detailed Salary Breakdown Report by Cost Center Group'.
                    Case "DETAILEDSALARYBREAKDOWNREPORTBYCCENTERGROUPHEADSIDS"
                        mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds = CStr(dtRow.Item("key_value"))
                        'Sohail (20 Feb 2017) -- End

                        'Sohail (01 Mar 2021) -- Start
                        'NMB Enhancement : : New screen Departmental Training Need in New UI.
                    Case "DEPTTRAININGNEEDLISTCOLUMNSIDS"
                        mstrDeptTrainingNeedListColumnsIDs = CStr(dtRow.Item("key_value"))
                        'Sohail (01 Mar 2021) -- End

                        'Sohail (29 May 2015) -- Start
                        'Enhancement - Time Zone Minute Difference for Online Recruitment Vacancy closing date.
                    Case "TIMEZONEMINUTEDIFFERENCE"
                        mintTimeZoneMinuteDifference = CStr(dtRow.Item("key_value"))
                        'Sohail (29 May 2015) -- End

                        'Sohail (05 Dec 2016) -- Start
                        'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                    Case "ADMINEMAIL"
                        mstrAdminEmail = CStr(dtRow.Item("key_value"))

                    Case "RECMIDDLENAMEMANDATORY"
                        mblnRecMiddleNameMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECGENDERMANDATORY"
                        mblnRecGenderMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECBIRTHDAYMANDATORY"
                        mblnRecBirthDayMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECADDRESS1MANDATORY"
                        mblnRecAddress1Mandatory = CBool(dtRow.Item("key_value"))

                    Case "RECADDRESS2MANDATORY"
                        mblnRecAddress2Mandatory = CBool(dtRow.Item("key_value"))

                    Case "RECCOUNTRYMANDATORY"
                        mblnRecCountryMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECSTATEMANDATORY"
                        mblnRecStateMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECCITYMANDATORY"
                        mblnRecCityMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECPOSTCODEMANDATORY"
                        mblnRecPostCodeMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECMARITALSTATUSMANDATORY"
                        mblnRecMaritalStatusMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECLANGUAGE1MANDATORY"
                        mblnRecLanguage1Mandatory = CBool(dtRow.Item("key_value"))

                    Case "RECNATIONALITYMANDATORY"
                        mblnRecNationalityMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECONESKILLMANDATORY"
                        mblnRecOneSkillMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECONEQUALIFICATIONMANDATORY"
                        mblnRecOneQualificationMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECONEJOBEXPERIENCEMANDATORY"
                        mblnRecOneJobExperienceMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECONEREFERENCEMANDATORY"
                        mblnRecOneReferenceMandatory = CBool(dtRow.Item("key_value"))
                        'Sohail (05 Dec 2016) -- End

                        'Sohail (04 Jul 2019) -- Start
                        'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
                    Case "RECONECURRICULAMVITAEMANDATORY"
                        mblnRecOneCurriculamVitaeMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECONECOVERLETTERMANDATORY"
                        mblnRecOneCoverLetterMandatory = CBool(dtRow.Item("key_value"))
                        'Sohail (04 Jul 2019) -- End

                        'Nilay (13 Apr 2017) -- Start
                        'Enhancements: Settings for mandatory options in online recruitment
                    Case "RECAPPLICANTQUALIFICATIONSORTBY"
                        mintRecApplicantQualificationSortBy = CInt(dtRow.Item("key_value"))

                    Case "RECREGIONMANDATORY"
                        mblnRecRegionMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECHIGHESTQUALIFICATIONMANDATORY"
                        mblnRecHighestQualificationMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECEMPLOYERMANDATORY"
                        mblnRecEmployerMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECMOTHERTONGUEMANDATORY"
                        mblnRecMotherTongueMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECCURRENTSALARYMANDATORY"
                        mblnRecCurrentSalaryMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECEXPECTEDSALARYMANDATORY"
                        mblnRecExpectedSalaryMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECEXPECTEDBENEFITSMANDATORY"
                        mblnRecExpectedBenefitsMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECNOTICEPERIODMANDATORY"
                        mblnRecNoticePeriodMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECEARLIESTPOSSIBLESTARTDATEMANDATORY"
                        mblnRecEarliestPossibleStartDateMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECCOMMENTSMANDATORY"
                        mblnRecCommentsMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECVACANCYFOUNDOUTFROMMANDATORY"
                        mblnRecVacancyFoundOutFromMandatory = CBool(dtRow.Item("key_value"))
                        'Nilay (13 Apr 2017) -- End

                        'Sohail (24 Jul 2017) -- Start
                        'Enhancement - 69.1 - Settings for mandatory options in online recruitment.
                    Case "RECCOMPANYNAMEJOBHISTORYMANDATORY"
                        mblnRecCompanyNameJobHistoryMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECPOSITIONHELDMANDATORY"
                        mblnRecPositionHeldMandatory = CBool(dtRow.Item("key_value"))
                        'Sohail (24 Jul 2017) -- End

                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : option Exclude employees from payroll during Suspension period to prorate salary and flat rate heads on employee suspension will be given on configuration.
                    Case "EXCLUDEEMPFROMPAYROLLDURINGSUSPENSION"
                        mblnExcludeEmpFromPayrollDuringSuspension = CBool(dtRow.Item("key_value"))
                        'Sohail (09 Oct 2019) -- End

                        'Pinkal (07-OCT-2014) -- Start
                        'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 
                    Case "COUNTATTENDANCENOTLVIFLVAPPLIED"
                        mblnCountAttendanceNotLVIfLVapplied = CStr(dtRow.Item("key_value"))
                        'Pinkal (07-OCT-2014) -- End

                        'Pinkal (12-Sep-2014) -- Start
                        'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

                    Case "SICKSHEETTEMPLATE"
                        mintsickSheetTemplate = CInt(dtRow.Item("key_value"))

                    Case "SICKSHEETALLOCATIONID"
                        mintSickSheetAllocationId = CInt(dtRow.Item("key_value"))

                        'Pinkal (12-Sep-2014) -- End

                        'S.SANDEEP [ 05 NOV 2014 ] -- START
                    Case "CASCADINGTYPEID"
                        mintCascadingTypeId = CInt(dtRow.Item("key_value"))
                    Case "SCORINGOPTIONID"
                        mintScoringOptionId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP |04-JAN-2021| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                    Case "CMPTSCORINGOPTIONID"
                        mintCmptScoringOptionId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP |04-JAN-2021| -- END                    
                    Case "ALLOWPERIODICREVIEW"
                        mblnAllowPeriodicReview = CBool(dtRow.Item("key_value"))
                    Case "FOLLOWEMPLOYEEHIERARCHY"
                        mblnFollowEmployeeHierarchy = CBool(dtRow.Item("key_value"))
                    Case "ASSESSMENT_INSTRUCTIONS"
                        mstrAssessmentInstructions = CStr(dtRow.Item("key_value"))
                    Case "PERF_EVALUATIONORDER"
                        mstrEvaluationOrder = CStr(dtRow.Item("key_value"))
                    Case "VIEWTITLES_INPLANNING"
                        mstrViewTitles_InPlanning = CStr(dtRow.Item("key_value"))
                    Case "VIEWTITLES_INEVALUATION"
                        mstrViewTitles_InEvaluation = CStr(dtRow.Item("key_value"))
                    Case "COLWIDTH_INPLANNING"
                        mstrColWidth_InPlanning = CStr(dtRow.Item("key_value"))
                    Case "COLWIDTH_INEVALUATION"
                        mstrColWidth_InEvaluation = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [ 05 NOV 2014 ] -- END

                        'S.SANDEEP [ 01 JAN 2015 ] -- START
                    Case "ASSESSMENTREPORTTEMPLATEID"
                        mintAssessmentReportTemplateId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [ 01 JAN 2015 ] -- END

                        'S.SANDEEP [29 JAN 2015] -- START
                    Case "SELFASSIGNCOMPETENCIES"
                        mblnSelfAssignCompetencies = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [29 JAN 2015] -- END

                        'S.SANDEEP [14 MAR 2015] -- START
                    Case "PROBATIONMONTH"
                        mintProbationMonth = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [14 MAR 2015] -- END

                        'S.SANDEEP [01 DEC 2015] -- START
                    Case "SKIPAPPROVALFLOWINPLANNING"
                        mblnSkipApprovalFlowInPlanning = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [01 DEC 2015] -- END

                        'S.SANDEEP [23 DEC 2015] -- START
                    Case "ONLYONEITEMPERCOMPETENCYCATEGORY"
                        mblnOnlyOneItemPerCompetencyCategory = CBool(dtRow.Item("key_value"))
                    Case "INCLUDECUSTOMITEMINPLANNING"
                        'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                        'mblnIncludeCustomItemInPlanning = CBool(dtRow.Item("key_value"))
                        'Shani (26-Sep-2016) -- End


                        'S.SANDEEP [23 DEC 2015] -- END

                        'Nilay (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
                    Case "SALARYANNIVERSARYSETTING"
                        mintSalaryAnniversarySetting = CInt(dtRow.Item("key_value"))
                    Case "SALARYANNIVERSARYMONTHBY"
                        mintSalaryAnniversaryMonthBy = CInt(dtRow.Item("key_value"))
                    Case "ISALLOWTOCLOSEPERIOD"
                        mblnIsAllowToClosePeriod = CBool(dtRow.Item("key_value"))
                        'Nilay (27 Apr 2016) -- End

                        'S.SANDEEP [24 MAY 2016] -- START
                    Case "DISCIPLINEREFNOTYPE"
                        mintDisciplineRefNoType = CInt(dtRow.Item("key_value"))
                    Case "DISCIPLINEREFPREFIX"
                        mstrDisciplineRefPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTDISCIPLINEREFNO"
                        mintNextDisciplineRefNo = CInt(dtRow.Item("key_value"))
                    Case "PREVENTDISCIPLINECHARGEUNLESSEMPLOYEENOTIFIED"
                        mblnPreventDisciplineChargeunlessEmployeeNotified = CBool(dtRow.Item("key_value"))
                    Case "PREVENTDISCIPLINECHARGEUNLESSEMPLOYEERECEIPT"
                        mblnPreventDisciplineChargeunlessEmployeeReceipt = CBool(dtRow.Item("key_value"))
                    Case "FIREDISCIPLINEWARNINGNOTIFICATION"
                        mstrFireDisciplineWarningNotification = CStr(dtRow.Item("key_value"))
                    Case "DISCIPLINEPROCEEDINGNOTIFICATION"
                        mstrDisciplineProceedingNotification = CStr(dtRow.Item("key_value"))
                    Case "DISCIPLINECHARGENOTIFICATION"
                        mstrDisciplineChargeNotification = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [24 MAY 2016] -- END


                        'S.SANDEEP [21 SEP 2016] -- START
                        'ENHANCEMENT : GIVING USER SELECTION ON NOTIFICATION CENTER
                    Case "DISCIPLINENOTIFYWARNINGUSERIDS"
                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrDisciplineNotifyWarningUserIds = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrDisciplineNotifyWarningUserIds)
                            mstrDisciplineNotifyWarningUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END

                    Case "DISCIPLINENOTIFYPROCEEDINGUSERIDS"
                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrDisciplineNotifyProceedingUserIds = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrDisciplineNotifyProceedingUserIds)
                            mstrDisciplineNotifyProceedingUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END

                    Case "DISCIPLINENOTIFYCHARGECLOSEDUSERIDS"
                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrDisciplineNotifyChargeClosedUserIds = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrDisciplineNotifyChargeClosedUserIds)
                            mstrDisciplineNotifyChargeClosedUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END

                        'S.SANDEEP [21 SEP 2016] -- END


                        'S.SANDEEP [03 OCT 2016] -- START
                        'ENHANCEMENT : VOLTAMP DAILY TIME SHEET CHANGES
                    Case "SELECTEDALLOCATIONFORDAILYTIMESHEETREPORT_VOLTAMP"
                        mintSelectedAllocationForDailyTimeSheetReport_Voltamp = CInt(dtRow.Item("key_value"))
                    Case "SHOWEMPLOYEESTATUSFORDAILYTIMESHEETREPORT_VOLTAMP"
                        mblnShowEmployeeStatusForDailyTimeSheetReport_Voltamp = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [03 OCT 2016] -- END


                        'Nilay (03-Oct-2016) -- Start
                        'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
                    Case "LEAVEACCRUEDDAYSHEADID"
                        mintLeaveAccruedDaysHeadID = CStr(dtRow.Item("key_value"))
                    Case "LEAVETAKENHEADID"
                        mintLeaveTakenHeadID = CStr(dtRow.Item("key_value"))
                    Case "ASSESSABLEINCOMEHEADID"
                        mintAssessableIncomeHeadID = CStr(dtRow.Item("key_value"))
                    Case "TAXFREEINCOMEHEADID"
                        mintTaxFreeIncomeHeadID = CStr(dtRow.Item("key_value"))
                        'Nilay (03-Oct-2016) -- End

                        'Nilay (24-Oct-2016) -- Start
                        'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
                    Case "NAPSATAXHEADID"
                        mintNAPSATaxHeadID = CStr(dtRow.Item("key_value"))
                        'Nilay (24-Oct-2016) -- End
                        'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    Case "GOALSACCOMPLISHEDREQUIRESAPPROVAL"
                        mblnGoalsAccomplishedRequiresApproval = CBool(dtRow.Item("key_value"))
                    Case "ENABLEBSCAUTOMATICRATING"
                        mblnEnableBSCAutomaticRating = CBool(dtRow.Item("key_value"))
                    Case "DONTALLOWTOEDITSCOREGENBYSYS"
                        mblnDontAllowToEditScoreGenbySys = CBool(dtRow.Item("key_value"))
                        'Shani (26-Sep-2016) -- End

                        'Nilay (07 Feb 2017) -- Start
                        'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
                    Case "ALLOWTOEXCEEDTIMEASSIGNEDTOACTIVITY"
                        mblnAllowToExceedTimeAssignedToActivity = CBool(dtRow.Item("key_value"))
                        'Nilay (07 Feb 2017) -- End

                        'Pinkal (21-Oct-2016) -- Start
                        'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.

                    Case "ALLOWOVERTIMETOEMPTIMESHEET"
                        mblnAllowOverTimeToEmpTimesheet = CBool(dtRow.Item("key_value"))
                        'Pinkal (21-Oct-2016) -- End

                        'Nilay (27 Feb 2017) -- Start
                        'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    Case "NOTALLOWINCOMPLETETIMESHEET"
                        mblnNotAllowIncompleteTimesheet = CBool(dtRow.Item("key_value"))

                    Case "NOTALLOWCLOSEPERIODINCOMPLETESUBMITAPPROVAL"
                        mblnNotAllowClosePeriodIncompleteSubmitApproval = CBool(dtRow.Item("key_value"))
                        'Nilay (27 Feb 2017) -- End

                        'Nilay (21 Mar 2017) -- Start
                        'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    Case "DESCRIPTIONMANDATORYFORACTIVITY"
                        mblnDescriptionMandatoryForActivity = CBool(dtRow.Item("key_value"))

                    Case "ALLOWACTIVITYHOURSBYPERCENTAGE"
                        mblnAllowActivityHoursByPercentage = CBool(dtRow.Item("key_value"))
                        'Nilay (21 Mar 2017) -- End

                        'Pinkal (18-Nov-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    Case "LEAVEACCRUETENURESETTING"
                        mintLeaveAccrueTenureSetting = CInt(dtRow.Item("key_value"))

                    Case "LEAVEACCRUEDAYSAFTEREACHMONTH"
                        mintLeaveAccrueDaysAfterEachMonth = CInt(dtRow.Item("key_value"))
                        'Pinkal (18-Nov-2016) -- End
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    Case "USEAGREEDSCORE"
                        mblnUseAgreedScore = CBool(dtRow.Item("key_value"))
                        'Shani (23-Nov-2016) -- End

                        'S.SANDEEP [25-JAN-2017] -- START
                        'ISSUE/ENHANCEMENT :
                    Case "ASSIGNBYJOBCOMPETENCIES"
                        mblnAssignByJobCompetencies = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [25-JAN-2017] -- END

                        'S.SANDEEP [25-JAN-2017] -- START
                        'ISSUE/ENHANCEMENT :
                    Case "REVIEWERSCORESETTING"
                        mintReviewerScoreSetting = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [25-JAN-2017] -- END


                        'Shani(23-FEB-2017) -- Start
                        'Enhancement - Add new custom item saving setting requested by (aga khan)
                    Case "ISALLOWCUSTOMITEMFINALSAVE"
                        mblnIsAllowCustomItemFinalSave = CBool(dtRow.Item("key_value"))
                        'Shani(23-FEB-2017) -- End

                        'S.SANDEEP [28-FEB-2017] -- START
                        'ISSUE/ENHANCEMENT : CCBRT, MONTHLY PAYROLL REPORT
                    Case "CHECKEDMONTHLYPAYROLLREPORT1HEADIDS"
                        mstrCheckedMonthlyPayrollReport1HeadIds = CStr(dtRow.Item("key_value"))
                    Case "CHECKEDMONTHLYPAYROLLREPORT2HEADIDS"
                        mstrCheckedMonthlyPayrollReport2HeadIds = CStr(dtRow.Item("key_value"))
                    Case "CHECKEDMONTHLYPAYROLLREPORT3HEADIDS"
                        mstrCheckedMonthlyPayrollReport3HeadIds = CStr(dtRow.Item("key_value"))
                    Case "CHECKEDMONTHLYPAYROLLREPORT4HEADIDS"
                        mstrCheckedMonthlyPayrollReport4HeadIds = CStr(dtRow.Item("key_value"))
                    Case "CHECKEDMONTHLYPAYROLLREPORTLEAVEIDS"
                        mstrCheckedMonthlyPayrollReportLeaveIds = CStr(dtRow.Item("key_value"))
                    Case "MONTHLYPAYROLLREPORTSKIPABSENTFROMTNA"
                        mblnMonthlyPayrollReportSkipAbsentFromTnA = CBool(dtRow.Item("key_value"))
                    Case "MONTHLYPAYROLLREPORTINCLUDEPENDINGAPPROVERFORMS"
                        mblnMonthlyPayrollReportIncludePendingApproverForms = CBool(dtRow.Item("key_value"))
                    Case "INCLUDESYSTEMRETIREMENTMONTHLYPAYROLLREPORT"
                        mblnIncludeSystemRetirementMonthlyPayrollReport = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [28-FEB-2017] -- END

                        'S.SANDEEP [06-MAR-2017] -- START
                        'ISSUE/ENHANCEMENT : Training Module Notification
                    Case "NTF_TRAININGLEVELI_EVALUSERIDS"
                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_TrainingLevelI_EvalUserIds = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNtf_TrainingLevelI_EvalUserIds)
                            mstrNtf_TrainingLevelI_EvalUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END

                    Case "NTF_TRAININGLEVELIII_EVALUSERIDS"
                        'S.SANDEEP [14-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
                        'mstrNtf_TrainingLevelIII_EvalUserIds = CStr(dtRow.Item("key_value"))
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNtf_TrainingLevelIII_EvalUserIds)
                            mstrNtf_TrainingLevelIII_EvalUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [14-APR-2017] -- END


                        'S.SANDEEP [06-MAR-2017] -- END


                        'S.SANDEEP [22-MAR-2017] -- START
                        'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                    Case "PEREPORT1_HEALTHINSURANCEMAPPEDHEADID"
                        mintPEReport1_HealthInsuranceMappedHeadId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT2_HEALTHINSURANCEMAPPEDHEADID"
                        mintPEReport2_HealthInsuranceMappedHeadId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT3_HEALTHINSURANCEMAPPEDHEADID"
                        mintPEReport3_HealthInsuranceMappedHeadId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT4_HEALTHINSURANCEMAPPEDHEADID"
                        mintPEReport4_HealthInsuranceMappedHeadId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT5_HEALTHINSURANCEMAPPEDHEADID"
                        mintPEReport5_HealthInsuranceMappedHeadId = CInt(dtRow.Item("key_value"))

                    Case "PEREPORT1_SELECTEDMEMBERSHIPIDS"
                        mstrPEReport1_SelectedMembershipIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT2_SELECTEDMEMBERSHIPIDS"
                        mstrPEReport2_SelectedMembershipIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT3_SELECTEDMEMBERSHIPIDS"
                        mstrPEReport3_SelectedMembershipIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT4_SELECTEDMEMBERSHIPIDS"
                        mstrPEReport4_SelectedMembershipIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT5_SELECTEDMEMBERSHIPIDS"
                        mstrPEReport5_SelectedMembershipIds = CStr(dtRow.Item("key_value"))

                    Case "PEREPORT1_ALLOCATIONTYPEID"
                        mintPEReport1_AllocationTypeId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT2_ALLOCATIONTYPEID"
                        mintPEReport2_AllocationTypeId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT3_ALLOCATIONTYPEID"
                        mintPEReport3_AllocationTypeId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT4_ALLOCATIONTYPEID"
                        mintPEReport4_AllocationTypeId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT5_ALLOCATIONTYPEID"
                        mintPEReport5_AllocationTypeId = CInt(dtRow.Item("key_value"))

                    Case "PEREPORT1_SELECTEDALLOCATIONIDS"
                        mstrPEReport1_SelectedAllocationIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT2_SELECTEDALLOCATIONIDS"
                        mstrPEReport2_SelectedAllocationIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT3_SELECTEDALLOCATIONIDS"
                        mstrPEReport3_SelectedAllocationIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT4_SELECTEDALLOCATIONIDS"
                        mstrPEReport4_SelectedAllocationIds = CStr(dtRow.Item("key_value"))
                    Case "PEREPORT5_SELECTEDALLOCATIONIDS"
                        mstrPEReport5_SelectedAllocationIds = CStr(dtRow.Item("key_value"))

                    Case "PEREPORT1_INCREMENTREASONMAPPEDID"
                        mintPEReport1_IncrementReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT2_INCREMENTREASONMAPPEDID"
                        mintPEReport2_IncrementReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT3_INCREMENTREASONMAPPEDID"
                        mintPEReport3_IncrementReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT4_INCREMENTREASONMAPPEDID"
                        mintPEReport4_IncrementReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT5_INCREMENTREASONMAPPEDID"
                        mintPEReport5_IncrementReasonMappedId = CInt(dtRow.Item("key_value"))

                    Case "PEREPORT1_PROMOTIONREASONMAPPEDID"
                        mintPEReport1_PromotionReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT2_PROMOTIONREASONMAPPEDID"
                        mintPEReport2_PromotionReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT3_PROMOTIONREASONMAPPEDID"
                        mintPEReport3_PromotionReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT4_PROMOTIONREASONMAPPEDID"
                        mintPEReport4_PromotionReasonMappedId = CInt(dtRow.Item("key_value"))
                    Case "PEREPORT5_PROMOTIONREASONMAPPEDID"
                        mintPEReport5_PromotionReasonMappedId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [22-MAR-2017] -- END

                        'Pinkal (03-Apr-2017) -- Start
                        'Enhancement - Working On Active directory Changes for PACRA.

                    Case "ADIPADDRESS"
                        mstrADIPAddress = dtRow.Item("key_value").ToString()

                    Case "ADDOMAIN"
                        mstrADDomin = dtRow.Item("key_value").ToString()

                    Case "ADDOMAINUSER"
                        mstrADDomainUser = dtRow.Item("key_value").ToString()

                    Case "ADDOMAINUSERPWD"
                        mstrADDomainUserPwd = clsSecurity.Encrypt(dtRow.Item("key_value").ToString().Trim(), "ezee")

                        'Pinkal (03-Apr-2017) -- End


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    Case "CREATEADUSERFROMEMPMST"
                        mblnCreateADUserFromEmpMst = CBool(dtRow.Item("key_value").ToString())
                    Case "USERMUSTCHANGEPWDONNEXTLOGON"
                        mblnUserMustChangePwdOnNextLogon = CBool(dtRow.Item("key_value").ToString())
                        'Pinkal (18-Aug-2018) -- End


                        'S.SANDEEP [10-MAY-2017] -- START
                        'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
                    Case "ISSYMMETRYINTEGRATED"
                        mblnIsSymmetryIntegrated = CBool(dtRow.Item("key_value"))
                    Case "SYMMETRYDATASERVERADDRESS"
                        mstrSymmetryDataServerAddress = CStr(dtRow.Item("key_value"))
                    Case "SYMMETRYDATABASENAME"
                        mstrSymmetryDatabaseName = CStr(dtRow.Item("key_value"))
                    Case "SYMMETRYDATABASEUSERNAME"
                        mstrSymmetryDatabaseUserName = CStr(dtRow.Item("key_value"))
                    Case "SYMMETRYDATABASEPASSWORD"
                        mstrSymmetryDatabasePassword = clsSecurity.Decrypt(CStr(dtRow.Item("key_value")), "ezee")
                    Case "SYMMETRYCARDNUMBERTYPEID"
                        mintSymmetryCardNumberTypeId = CInt(dtRow.Item("key_value"))
                    Case "SYMMETRYMARKASDEFAULT"
                        mblnSymmetryMarkAsDefault = CBool(dtRow.Item("key_value"))
                    Case "SYMMETRYAUTHENTICATIONMODEID"
                        mintSymmetryAuthenticationModeId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [10-MAY-2017] -- END


                        'Pinkal (11-AUG-2017) -- Start
                        'Enhancement - Working On B5 Plus Company TnA Enhancements.
                    Case "ISHOLIDAYCONSIDERONWEEKEND"
                        mblnIsHolidayConsiderOnWeekend = CBool(dtRow.Item("key_value"))
                    Case "ISDAYOFFCONSIDERONWEEKEND"
                        mblnIsDayOffConsiderOnWeekend = CBool(dtRow.Item("key_value"))
                    Case "ISHOLIDAYCONSIDERONDAYOFF"
                        mblnIsHolidayConsiderOnDayoff = CBool(dtRow.Item("key_value"))
                        'Pinkal (11-AUG-2017) -- End


                        'Pinkal (23-AUG-2017) -- Start
                        'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
                    Case "RESTRICTEMPFORBGTIMESHEETONAFTERDATE"
                        mintRestrictEmpForBgTimesheetOnAfterDate = CInt(dtRow.Item("key_value"))

                    Case "DISABLEADUSERAGAINAFTERMINS"
                        mintDisableADUserAgainAfterMins = CInt(dtRow.Item("key_value"))

                        'Pinkal (23-AUG-2017) -- End


                        'Pinkal (04-Oct-2017) -- Start
                        'Enhancement - Working SAGEM Database Device Integration.

                    Case "SAGEMDATASERVERADDRESS"
                        mstrSagemDataServerAddress = dtRow.Item("key_value").ToString()

                    Case "SAGEMDATABASENAME"
                        mstrSagemDatabaseName = dtRow.Item("key_value").ToString()

                    Case "SAGEMDATABASEUSERNAME"
                        mstrSagemDatabaseUserName = dtRow.Item("key_value").ToString()

                    Case "SAGEMDATABASEPASSWORD"
                        mstrSagemDatabasePassword = dtRow.Item("key_value").ToString()

                        'Pinkal (04-Oct-2017) -- End

                        'Varsha (10 Nov 2017) -- Start
                        'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
                    Case "DEFAULTSELFSERVICETHEME"
                        mintDefaultSelfServiceTheme = dtRow.Item("key_value").ToString()
                        'Varsha (10 Nov 2017) -- End

                        'S.SANDEEP [25-OCT-2017] -- START
                    Case "SHOWMYGOALS"
                        mblnShowMyGoals = CBool(dtRow.Item("key_value"))
                    Case "SHOWCUSTOMHEADERS"
                        mblnShowCustomHeaders = CBool(dtRow.Item("key_value"))
                    Case "SHOWGOALSAPPROVAL"
                        mblnShowGoalsApproval = CBool(dtRow.Item("key_value"))
                    Case "SHOWSELFASSESSMENT"
                        mblnShowSelfAssessment = CBool(dtRow.Item("key_value"))
                    Case "SHOWMYCOMPTENCIES"
                        mblnShowMyComptencies = CBool(dtRow.Item("key_value"))
                    Case "SHOWVIEWPERFORMANCEASSESSMENT"
                        mblnShowViewPerformanceAssessment = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [25-OCT-2017] -- END

                        'S.SANDEEP [29-NOV-2017] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                    Case "NTF_GOALSUNLOCKUSERIDS"
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNtf_GoalsUnlockUserIds)
                            mstrNtf_GoalsUnlockUserIds = CStr(dtRow.Item("key_value"))
                        End If
                    Case "NTF_FINALACKNOWLEDGEMENTUSERIDS"
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNtf_FinalAcknowledgementUserIds)
                            mstrNtf_FinalAcknowledgementUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP [29-NOV-2017] -- END 

                        'S.SANDEEP [16-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 118
                    Case "ALLOWTOVIEWSIGNATUREESS"
                        mblnAllowtoViewSignatureESS = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [16-Jan-2018] -- END

                        'Pinkal (12-Feb-2018) -- Start
                        'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.
                    Case "SENDJOBCONFIRMATIONEMAIL"
                        mblnSendJobConfirmationEmail = CBool(dtRow.Item("key_value"))

                    Case "ISVANCANYALERT"
                        mblnVacancyAlert = CBool(dtRow.Item("key_value"))

                    Case "MAXVACANCYALERT"
                        mintMaxVacancyAlert = CInt(dtRow.Item("key_value"))

                        'Pinkal (12-Feb-2018) -- End

                        'S.SANDEEP [14-Apr-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#187|#ARUTI-109}
                    Case "ERC_LEAVEEND_SETTING"
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then

                            'S.SANDEEP [28-JUL-2018] -- START
                            'GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(3), mstrERC_LeaveEnd_Setting)
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_LeaveEnd_Setting)
                            'S.SANDEEP [28-JUL-2018] -- END

                            If mstrERC_LeaveEnd_Setting.Trim.Length > 0 Then
                                'S.SANDEEP [28-JUL-2018] -- START
                                'dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & mstrERC_LeaveEnd_Setting
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_LeaveEnd_Setting
                                'S.SANDEEP [28-JUL-2018] -- END
                            End If
                            mstrERC_LeaveEnd_Setting = CStr(dtRow.Item("key_value"))
                        End If
                    Case "ERC_LEAVEEND_TEMPLATEID"
                        mintERC_LeaveEnd_TemplateId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [14-Apr-2018] -- END



                        'Pinkal (16-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.

                    Case "ERC_LEAVEPLANNER_SETTING"

                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_LeavePlanner_Setting)
                            If mstrERC_LeavePlanner_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_LeavePlanner_Setting
                            End If
                            mstrERC_LeavePlanner_Setting = CStr(dtRow.Item("key_value"))
                        End If

                    Case "ERC_LEAVEPLANNER_TEMPLATEID"
                        mintERC_LeavePlanner_TemplateId = CInt(dtRow.Item("key_value"))

                        'Pinkal (16-Oct-2018) -- End


                        'Pinkal (03-Dec-2018) -- Start
                        'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.

                    Case "ERC_ASSETDECFROMDATE_SETTING"
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_AssetDecFromDate_Setting)
                            If mstrERC_AssetDecFromDate_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_AssetDecFromDate_Setting
                            End If
                            mstrERC_AssetDecFromDate_Setting = CStr(dtRow.Item("key_value"))
                        End If

                    Case "ERC_ASSETDECFROMDATE_TEMPLATEID"
                        mintERC_AssetDecFromDate_TemplateId = CInt(dtRow.Item("key_value"))

                    Case "ERC_ASSETDECTODATE_SETTING"
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")).Split("|")(4), mstrERC_AssetDecToDate_Setting)
                            If mstrERC_AssetDecToDate_Setting.Trim.Length > 0 Then
                                dtRow.Item("key_value") = CStr(dtRow.Item("key_value")).Split("|")(0) & "|" & CStr(dtRow.Item("key_value")).Split("|")(1) & "|" & CStr(dtRow.Item("key_value")).Split("|")(2) & "|" & CStr(dtRow.Item("key_value")).Split("|")(3) & "|" & mstrERC_AssetDecToDate_Setting
                            End If
                            mstrERC_AssetDecToDate_Setting = CStr(dtRow.Item("key_value"))
                        End If

                    Case "ERC_ASSETDECTODATE_TEMPLATEID"
                        mintERC_AssetDecToDate_TemplateId = CInt(dtRow.Item("key_value"))

                        'Pinkal (03-Dec-2018) -- End



                        'Pinkal (18-May-2018) -- Start
                        'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
                    Case "ATTACHAPPLICANTCV"
                        mblnAttachApplicantCV = CBool(dtRow.Item("key_value"))
                        'Pinkal (18-May-2018) -- End


                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                    Case "SHOWBGTIMESHEETACTIVITY"
                        mblnShowBgTimesheetActivity = CBool(dtRow.Item("key_value"))

                    Case "SHOWBGTIMESHEETASSIGNEDACTIVITYPERCENTAGE"
                        mblnShowBgTimesheetAssignedActivityPercentage = CBool(dtRow.Item("key_value"))

                    Case "SHOWBGTIMESHEETACTIVITYPROJECT"
                        mblnShowBgTimesheetActivityProject = CBool(dtRow.Item("key_value"))

                    Case "SHOWBGTIMESHEETACTIVITYHRSDETAIL"
                        mblnShowBgTimesheetActivityHrsDetail = CBool(dtRow.Item("key_value"))

                    Case "REMARKMANDATORYFORTIMESHEETSUBMISSION"
                        mblnRemarkMandatoryForTimesheetSubmission = CBool(dtRow.Item("key_value"))

                        'Pinkal (28-Jul-2018) -- End


                        'S.SANDEEP [20-JUN-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow For NMB .
                    Case "SKIPEMPLOYEEAPPROVALFLOW"
                        mblnSkipEmployeeApprovalFlow = CBool(dtRow.Item("key_value"))

                    Case "SKIPEMPLOYEEMOVEMENTAPPROVALFLOW"
                        mblnSkipEmployeeMovementApprovalFlow = CBool(dtRow.Item("key_value"))

                    Case "EMPMANDATORYFIELDSIDS"
                        mstrEmpMandatoryFieldsIDs = CStr(dtRow.Item("key_value"))

                    Case "PENDINGEMPLOYEESCREENIDS"
                        mstrPendingEmployeeScreenIDs = CStr(dtRow.Item("key_value"))

                        'S.SANDEEP [20-JUN-2018] -- End

                        'Sohail (18 Feb 2020) -- Start
                        'NMB Enhancement # : On the references page on recruitment portal, provide all those fields on configuration where they can be made mandatory or optional.
                    Case "RECRUITMANDATORYFIELDSIDS"
                        mstrRecruitMandatoryFieldsIDs = CStr(dtRow.Item("key_value"))

                    Case "RECRUITMANDATORYSKILLS"
                        mintRecruitMandatorySkills = CInt(dtRow.Item("key_value"))

                    Case "RECRUITMANDATORYQUALIFICATIONS"
                        mintRecruitMandatoryQualifications = CInt(dtRow.Item("key_value"))

                    Case "RECRUITMANDATORYJOBEXPERIENCES"
                        mintRecruitMandatoryJobExperiences = CInt(dtRow.Item("key_value"))

                    Case "RECRUITMANDATORYREFERENCES"
                        mintRecruitMandatoryReferences = CInt(dtRow.Item("key_value"))
                        'Sohail (18 Feb 2020) -- End

                        'Sohail (06 Oct 2021) -- Start
                        'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
                    Case "RECRUITMENTEMAILSETUPUNKID"
                        mintRecruitmentEmailSetupUnkid = CInt(dtRow.Item("key_value"))
                        'Sohail (06 Oct 2021) -- End

                        'S.SANDEEP [20-JUN-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#244}
                    Case "EMPLOYEEREJECTNOTIFICATIONTEMPLATEID"
                        mintEmployeeRejectNotificationTemplateId = CInt(dtRow.Item("key_value"))
                    Case "EMPLOYEEREJECTNOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrEmployeeRejectNotificationUserIds)
                    Case "REJECTTRANSFERUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectTransferUserNotification)
                    Case "REJECTRECATEGORIZATIONUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectReCategorizationUserNotification)
                    Case "REJECTPROBATIONUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectProbationUserNotification)
                    Case "REJECTCONFIRMATIONUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectConfirmationUserNotification)
                    Case "REJECTSUSPENSIONUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectSuspensionUserNotification)
                    Case "REJECTTERMINATIONUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectTerminationUserNotification)
                    Case "REJECTRETIREMENTUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectRetirementUserNotification)
                    Case "REJECTWORKPERMITUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectWorkPermitUserNotification)
                    Case "REJECTRESIDENTPERMITUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectResidentPermitUserNotification)
                    Case "REJECTCOSTCENTERPERMITUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectCostCenterPermitUserNotification)
                        'S.SANDEEP [20-JUN-2018] -- END
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case "REJECTEXEMPTIONUSERNOTIFICATION"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrRejectExemptionUserNotification)
                        'Sohail (21 Oct 2019) -- End

                        'S.SANDEEP [18-AUG-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#290}
                    Case "PAEMPNTFSETTINGVALUE"
                        mstrPAEmpNtfSettingValue = CStr(dtRow.Item("key_value"))
                    Case "PAASRNTFSETTINGVALUE"
                        mstrPAAsrNtfSettingValue = CStr(dtRow.Item("key_value"))
                    Case "PAREVNTFSETTINGVALUE"
                        mstrPARevNtfSettingValue = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP [18-AUG-2018] -- END

                        'S.SANDEEP [09-AUG-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#292}
                    Case "ISHRFLEXCUBEINTEGRATED"
                        mblnIsHRFlexcubeIntegrated = CBool(dtRow.Item("key_value"))
                    Case "FLEXCUBEMSGIDNOTYPE"
                        mintFlexcubeMsgIDNoType = CInt(dtRow.Item("key_value"))
                    Case "FLEXCUBEMSGIDPREFIX"
                        mstrFlexcubeMsgIDPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTFLEXCUBEMSGID"
                        mintNextFlexcubeMsgID = CInt(dtRow.Item("key_value"))
                    Case "FLEXCUBEACCOUNTCATEGORY"
                        mstrFlexcubeAccountCategory = CStr(dtRow.Item("key_value"))
                    Case "FLEXCUBEACCOUNTCLASS"
                        mstrFlexcubeAccountClass = CStr(dtRow.Item("key_value"))
                    Case "FLEXSRVRUNNINGTIME"
                        mdtFlexSrvRunningTime = CType(dtRow.Item("key_value"), DateTime)
                        'S.SANDEEP [09-AUG-2018] -- END

                        'Pinkal (01-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                    Case "SETRELIEVERASMANDATORYFORAPPROVAL"
                        mblnSetRelieverAsMandatoryForApproval = CBool(dtRow.Item("key_value"))

                    Case "ALLOWTOSETRELIEVERONLVFORMFORESS"
                        mblnAllowToSetRelieverOnLvFormForESS = CBool(dtRow.Item("key_value"))
                        'Pinkal (01-Oct-2018) -- End


                        'Pinkal (25-Mar-2019) -- Start
                        'Enhancement - Working on Leave Changes for NMB.
                    Case "ALLOWLVAPPLICATIONAPPLYFORBACKDATES"
                        mblnAllowLvApplicationApplyForBackDates = CBool(dtRow.Item("key_value"))
                        'Pinkal (25-Mar-2019) -- End


                        'Pinkal (13-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    Case "RELIEVERALLOCATION"
                        mstrRelieverAllocation = CStr(dtRow.Item("key_value"))
                        'Pinkal (13-Mar-2019) -- End


                        'Pinkal (19-Oct-2018) -- Start
                        'Enhancement -  OT Requisition Settings Enhancement for NMB.
                    Case "SETOTREQUISITIONMANDATORY"
                        mblnSetOTRequisitionMandatory = CBool(dtRow.Item("key_value"))

                    Case "CAPOTHRSFORHODAPPROVERS"
                        mstrCapOTHrsForHODApprovers = CStr(dtRow.Item("key_value"))
                        'Pinkal (19-Oct-2018) -- End

                        'Gajanan [13-AUG-2018] -- Start
                        'Enhancement - Implementing Grievance Module.
                    Case "GRIEVANCEAPPROVALSETTING"
                        mintGrievanceApprovalSetting = CInt(dtRow.Item("key_value"))

                    Case "GRIEVANCEREFNOTYPE"
                        mintGrievanceRefNoType = CInt(dtRow.Item("key_value"))
                    Case "GRIEVANCEREFPREFIX"
                        mstrGrievanceRefPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTGRIEVANCEREFNO"
                        mintNextGrievanceRefNo = CInt(dtRow.Item("key_value"))
                        'Gajanan [13-AUG-2018] -- End

                        'Hemant (06 Oct 2018) -- Start
                        'Enhancement : Implementing New Module of Asset Declaration Template 2
                    Case "ASSETDECLARATIONTEMPLATE"
                        mintAssetDeclarationTemplate = Convert.ToInt16(dtRow.Item("key_value"))
                        'Hemant (06 Oct 2018) -- End

                   'S.SANDEEP [09-OCT-2018] -- START
                    Case "ENABLETRANINGREQUISITION"
                        mblnEnableTraningRequisition = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP [09-OCT-2018] -- END

                        'Sohail (12 Oct 2018) -- Start
                        'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
                    Case "STAFFREQUISITIONFINALAPPROVEDNOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrStaffRequisitionFinalApprovedNotificationUserIds)
                        'Sohail (12 Oct 2018) -- End


                        'S.SANDEEP [09-OCT-2018] -- START
                    Case "DONOTALLOWTOREQUESTTRAININGAFTERDAYS"
                        mintDonotAllowToRequestTrainingAfterDays = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP [09-OCT-2018] -- END

                        'Hemant (19 Nov 2018) -- Start
                        'Enhancement : Changes for NMB Requirement for Email Notification After Final Saved In Asset Declaration 
                    Case "ASSETDECLARATIONFINALSAVEDNOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrAssetDeclarationFinalSavedNotificationUserIds)
                        'Hemant (19 Nov 2018) -- End

                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                    Case "IMPORTCOSTCENTERP2PSERVICEURL"
                        mstrImportCostCenterP2PServiceURL = dtRow.Item("key_value").ToString()

                    Case "OPEXREQUESTCCP2PSERVICEURL"
                        mstrOpexRequestCCP2PServiceURL = dtRow.Item("key_value").ToString()

                    Case "CAPEXREQUESTCCP2PSERVICEURL"
                        mstrCapextRequestCCP2PServiceURL = dtRow.Item("key_value").ToString()

                    Case "BUDGETREQUESTVALIDATIONP2PSERVICEURL"
                        mstrBgtRequestValidationP2PServiceURL = dtRow.Item("key_value").ToString()

                    Case "NEWREQUISITIONREQUESTP2PSERVICEURL"
                        mstrNewRequisitionRequestP2PServiceURL = dtRow.Item("key_value").ToString()

                    Case "REQUISITIONSTATUSP2PSERVICEURL"
                        mstrRequisitionStatusP2PServiceURL = dtRow.Item("key_value").ToString()

                        'Pinkal (20-Nov-2018) -- End

 'Sohail (20 Nov 2018) -- Start
                        'NMB Enhancement - SMS Integration to send notification 75.1.
                    Case "SMSCOMPANYDETAILTONEWEMP"
                        mblnSMSCompanyDetailToNewEmp = dtRow.Item("key_value").ToString

                    Case "SMSGATEWAYEMAIL"
                        mstrSMSGatewayEmail = dtRow.Item("key_value").ToString

                    Case "SMSGATEWAYEMAILTYPE"
                        mintSMSGatewayEmailType = CInt(dtRow.Item("key_value"))
                        'Sohail (20 Nov 2018) -- End

                        'Hemant (23 Nov 2018) -- Start
                        'Enhancement : Changes As per Rutta Request for UAT 
                    Case "ASSETDECLARATIONUNLOCKFINALSAVEDNOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrAssetDeclarationUnlockFinalSavedNotificationUserIds)
                        'Hemant (23 Nov 2018) -- End

                        'Sohail (19 Feb 2020) -- Start
                        'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
                    Case "NONDISCLOSUREDECLARATIONACKNOWLEDGEDNOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrNonDisclosureDeclarationAcknowledgedNotificationUserIds)
                        'Sohail (19 Feb 2020) -- End

                        'Sohail (07 Mar 2020) -- Start
                        'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration..
                    Case "FLEXCUBEJVPOSTEDORACLENOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrFlexCubeJVPostedOracleNotificationUserIds)
                        'Sohail (07 Mar 2020) -- End

                        'Sohail (26 Nov 2018) -- Start
                        'NMB Enhancement - Flex Cube JV Integration in 75.1.
                    Case "ORACLEHOSTNAME"
                        mstrOracleHostName = dtRow.Item("key_value").ToString

                    Case "ORACLEPORTNO"
                        mstrOraclePortNo = dtRow.Item("key_value").ToString

                    Case "ORACLESERVICENAME"
                        mstrOracleServiceName = dtRow.Item("key_value").ToString

                    Case "ORACLEUSERNAME"
                        mstrOracleUserName = dtRow.Item("key_value").ToString

                    Case "ORACLEUSERPASSWORD"
                        'Sohail (14 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - store encrypted password.
                        'mstrOracleUserPassword = dtRow.Item("key_value").ToString
                        If IsKeyExist(mintCompanyUnkid, "Advance_CostCenterunkid") = True Then
                            mstrOracleUserPassword = clsSecurity.Decrypt(CStr(dtRow.Item("key_value")), "ezee")
                        Else
                        mstrOracleUserPassword = dtRow.Item("key_value").ToString
                        End If
                        'Sohail (14 Mar 2019) -- End
                        'Sohail (26 Nov 2018) -- End

                        'Sohail (02 Mar 2019) -- Start
                        'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
                    Case "SQLDATASOURCE"
                        mstrSQLDataSource = dtRow.Item("key_value").ToString

                    Case "SQLDATABASENAME"
                        mstrSQLDatabaseName = dtRow.Item("key_value").ToString

                    Case "SQLDATABASEOWNERNAME"
                        mstrSQLDatabaseOwnerName = dtRow.Item("key_value").ToString

                    Case "SQLUSERNAME"
                        mstrSQLUserName = dtRow.Item("key_value").ToString

                    Case "SQLUSERPASSWORD"
                        'Sohail (14 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - store encrypted password.
                        'mstrSQLUserPassword = dtRow.Item("key_value").ToString
                        If IsKeyExist(mintCompanyUnkid, "Advance_CostCenterunkid") = True Then
                            mstrSQLUserPassword = clsSecurity.Decrypt(CStr(dtRow.Item("key_value")), "ezee")
                        Else
                        mstrSQLUserPassword = dtRow.Item("key_value").ToString
                        End If
                        'Sohail (14 Mar 2019) -- End

                    Case "BRJVVOCNOTYPE"
                        mintBRJVVocNoType = CInt(dtRow.Item("key_value"))
                    Case "BRJVVOCPREFIX"
                        mstrBRJVVocPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTBRJVVOCNO"
                        mintNextBRJVVocNo = CInt(dtRow.Item("key_value"))
                        'Sohail (02 Mar 2019) -- End


                        'Pinkal (03-Dec-2018) -- Start
                        'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.

                    Case "ASSETDECLARATIONFROMDATE"
                        mstrAssetDeclarationFromDate = dtRow.Item("key_value").ToString

                    Case "ASSETDECLARATIONTODATE"
                        mstrAssetDeclarationToDate = dtRow.Item("key_value").ToString

                        'Hemant (01 Mar 2022) -- Start            
                        'Case "EMPUNLOCKDAYSAFTERASSETDECLARATIONTODATE"
                        '    mintEmpUnLockDaysAfterAssetDeclarationToDate = dtRow.Item("key_value").ToString
                    Case "DONTALLOWASSETDECLARATIONAFTERDAYS"
                        mintDontAllowAssetDeclarationAfterDays = dtRow.Item("key_value").ToString
                        'Hemant (01 Mar 2022) -- End

                    Case "NEWEMPUNLOCKDAYSFORASSETDECWITHINAPPOINTMENTDATE"
                        mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate = dtRow.Item("key_value").ToString

                        'Pinkal (03-Dec-2018) -- End

                        'Sohail (04 Feb 2020) -- Start
                        'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
                    Case "NONDISCLOSUREDECLARATIONFROMDATE"
                        mstrNonDisclosureDeclarationFromDate = dtRow.Item("key_value").ToString

                    Case "NONDISCLOSUREDECLARATIONTODATE"
                        mstrNonDisclosureDeclarationToDate = dtRow.Item("key_value").ToString

                    Case "EMPLOCKDAYSAFTERNONDISCLOSUREDECLARATIONTODATE"
                        mintEmpLockDaysAfterNonDisclosureDeclarationToDate = dtRow.Item("key_value").ToString

                    Case "NEWEMPUNLOCKDAYSFORNONDISCLOSUREDECWITHINAPPOINTMENTDATE"
                        mintNewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate = dtRow.Item("key_value").ToString

                    Case "NONDISCLOSUREDECLARATION"
                        mstrNonDisclosureDeclaration = dtRow.Item("key_value").ToString

                    Case "NONDISCLOSUREWITNESSER1"
                        mintNonDisclosureWitnesser1 = dtRow.Item("key_value").ToString

                    Case "NONDISCLOSUREWITNESSER2"
                        mintNonDisclosureWitnesser2 = dtRow.Item("key_value").ToString
                        'Sohail (04 Feb 2020) -- End

                        'Sohail (07 Apr 2021) -- Start
                        'TRA Enhancement : : Setting on configuration to hide Non-Dislcosure declaration link on menu for TRA.
                    Case "APPLYNONDISCLOSUREDECLARATION"
                        mblnApplyNonDisclosureDeclaration = CBool(dtRow.Item("key_value"))
                        'Sohail (07 Apr 2021) -- End

                        'S.SANDEEP |18-JAN-2019| -- START
                    Case "GOALTYPEINCLUSION"
                        mintGoalTypeInclusion = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP |18-JAN-2019| -- END

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Case "SKIPAPPROVALONEMPDATA"
                       'Gajanan |30-MAR-2019| -- START
                        If IsKeyExist(mintCompanyUnkid, "SkipApprovalOnEmpData") = False Then
                            mstrSkipApprovalOnEmpData = mstrSkipApprovalOnEmpDatavalue
                        Else
                        mstrSkipApprovalOnEmpData = CStr(dtRow.Item("key_value"))
                        End If
                       'Gajanan |30-MAR-2019| -- END
                        'Gajanan [17-DEC-2018] -- End


                        'Gajanan |30-MAR-2019| -- START
                    Case "OLDSKIPAPPROVALONEMPDATA"
                        mstrOldSkipApprovalOnEmpData = CStr(dtRow.Item("key_value"))
                        'Gajanan |30-MAR-2019| -- END


                        'S.SANDEEP |05-MAR-2019| -- START
                        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    Case "SHOWARUTISIGNATURE"
                        mblnShowArutisignatureinemailnotification = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |05-MAR-2019| -- END

                        'S.SANDEEP |14-MAR-2019| -- START
                        'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
                    Case "MAKEEMPASSESSCOMMENTSMANDATORY"
                        mblnMakeEmpAssessCommentsMandatory = CBool(dtRow.Item("key_value"))
                    Case "MAKEASRASSESSCOMMENTSMANDATORY"
                        mblnMakeAsrAssessCommentsMandatory = CBool(dtRow.Item("key_value"))
                    Case "MAKEREVASSESSCOMMENTSMANDATORY"
                        mblnMakeRevAssessCommentsMandatory = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |14-MAR-2019| -- END


                        'Gajanan |30-MAR-2019| -- START
                    Case "VIEWEMPLOYEESCALE"
                        mblnAllowViewEmployeeScale = CBool(dtRow.Item("key_value"))
                        'Gajanan |30-MAR-2019| -- END


                        'Gajanan [8-April-2019] -- Start
                    Case "ADDEDITEMPLOYEEPRESENTADDRESS"
                        mblnAllowToAddEditEmployeePresentAddress = CBool(dtRow.Item("key_value"))

                    Case "ADDEDITEMPLOYEEDOMICILEADDRESS"
                        mblnAllowToAddEditEmployeeDomicileAddress = CBool(dtRow.Item("key_value"))

                    Case "ADDEDITEMPLOYEERECRUITMENTADDRESS"
                        mblnAllowToAddEditEmployeeRecruitmentAddress = CBool(dtRow.Item("key_value"))
                        'Gajanan [8-April-2019] -- End

                        'Sohail (10 Apr 2019) -- Start
                        'NMB Enhancement - 76.1 - Error email setting on configuration.
                    Case "ADMINISTRATOREMAILFORERROR"
                        mstrAdministratorEmailForError = CStr(dtRow.Item("key_value"))
                        'Sohail (10 Apr 2019) -- End

                        'S.SANDEEP |09-APR-2019| -- START
                    Case "ISWORKANNIVERSARYENABLED"
                        mblnIsWorkAnniversaryEnabled = CBool(dtRow.Item("key_value"))
                    Case "WORKANNIVYEAR"
                        mintWorkAnnivYear = CInt(dtRow.Item("key_value"))
                    Case "WORKANNIVLETTERTEMPLATEID"
                        mintWorkAnnivLetterTemplateId = CInt(dtRow.Item("key_value"))
                    Case "WORKANNIVSUBJECT"
                        mstrWorkAnnivSubject = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP |09-APR-2019| -- END

                        'Pinkal (01-Apr-2019) -- Start
                        'Enhancement - Working on Leave Changes for NMB.
                    Case "LEAVEAPPLICATIONESCALATIONSETTINGS"
                        mintLeaveApplicationEscalationSettings = CInt(dtRow.Item("key_value"))

                    Case "LEAVEREMINDEROCCURRENCEAFTERDAYS"
                        mintLeaveReminderOccurrenceAfterDays = CInt(dtRow.Item("key_value"))

                    Case "LEAVEESCALATIONREJECTIONREASON"
                        mstrLeaveEscalationRejectionReason = dtRow.Item("key_value").ToString().Trim()
                        'Pinkal (01-Apr-2019) -- End

                        'S.SANDEEP |25-APR-2019| -- START
                    Case "WORKANNIVSMS"
                        mblnWorkAnnivSMS = CBool(dtRow.Item("key_value"))
                    Case "WORKANNIVUSR"
                        mblnWorkAnnivUsr = CBool(dtRow.Item("key_value"))
                    Case "WORKANNIVUSERIDS"
                        mstrWorkAnnivUserIds = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP |25-APR-2019| -- END


                        'Pinkal (03-May-2019) -- Start
                        'Enhancement - Working on Leave UAT Changes for NMB.
                    Case "ALLOWTOEDITLEAVEAPPLICATION"
                        mblnAllowToEditLeaveApplication = CBool(dtRow.Item("key_value"))

                    Case "ALLOWTODELETELEAVEAPPLICATION"
                        mblnAllowToDeleteLeaveApplication = CBool(dtRow.Item("key_value"))

                    Case "LEAVECANCELFORMNOTYPE"
                        mintLeaveCancelFormNotype = CInt(dtRow.Item("key_value"))

                    Case "LEAVECANCELFORMNOPRIFIX"
                        mstrLeaveCancelFormNoPrifix = CStr(dtRow.Item("key_value"))

                    Case "NEXTLEAVECANCELFORMNO"
                        mintNextLeaveCancelFormNo = CInt(dtRow.Item("key_value"))
                        'Pinkal (03-May-2019) -- End


                        'Pinkal (20-May-2019) -- Start
                        'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
                    Case "CLAIMREMARKMANDATORYFORCLAIM"
                        mblnClaimRemarkMandatoryForClaim = CBool(dtRow.Item("key_value"))
                        'Pinkal (20-May-2019) -- End


                        'Gajanan [27-May-2019] -- Start              
                    Case "DOMICILEADDRESSDOCSATTACHMENTMANDATORY"
                        mblnDomicileAddressDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                        'Gajanan [27-May-2019] -- End


                        'Pinkal (07-Jun-2019) -- Start
                        'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    Case "LOCKUSERONSUSPENSION"
                        mblnLockUserOnSuspension = CBool(dtRow.Item("key_value"))
                        'Pinkal (07-Jun-2019) -- End

'S.SANDEEP |27-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                    Case "SCORECALIBRATIONFORMNOPRIFIX"
                        mstrScoreCalibrationFormNoPrifix = CStr(dtRow.Item("key_value"))
                    Case "SCORECALIBRATIONFORMNOTYPE"
                        mintScoreCalibrationFormNotype = CInt(dtRow.Item("key_value"))
                    Case "NEXTSCORECALIBRATIONFORMNO"
                        mintNextScoreCalibrationFormNo = CInt(dtRow.Item("key_value"))
                    Case "ISCALIBRATIONSETTINGACTIVE"
                        mblnIsCalibrationSettingActive = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |27-MAY-2019| -- END

                        'Gajanan [13-July-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                    Case "EMPLOYEEDISAGREEGRIEVANCENOTIFICATIONTEMPLATEID"
                        mintEmployeeDisagreeGrievanceNotificationTemplateId = CInt(dtRow.Item("key_value"))

                    Case "EMPLOYEEDISAGREEGRIEVANCENOTIFICATIONUSERIDS"
                        mstrEmployeeDisagreeGrievanceNotificationUserIds = CStr(dtRow.Item("key_value"))

                    Case "EMPLOYEEDISAGREEGRIEVANCENOTIFICATIONEMAILTITLE"
                        mstrEmployeeDisagreeGrievanceNotificationEmailTitle = CStr(dtRow.Item("key_value"))
                        'Gajanan [13-July-2019] -- End

                        'Hemant (19 Aug 2019) -- Start
                        'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
                    Case "DONTALLOWADVANCEONESS"
                        mblnDontAllowAdvanceOnESS = CBool(dtRow.Item("key_value"))
                        'Hemant (19 Aug 2019) -- End

                        'Pinkal (13-Aug-2019) -- Start
                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    Case "ALLOWEMPASSIGNEDPROJECTEXCEEDTIME"
                        mblnAllowEmpAssignedProjectExceedTime = CBool(dtRow.Item("key_value"))
                        'Pinkal (13-Aug-2019) -- End

                        'S.SANDEEP |26-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                    Case "DISPLAYCALIBRATIONUSERALLOCATIONID"
                        mintDisplayCalibrationUserAllocationId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP |26-AUG-2019| -- END


                        'Pinkal (29-Aug-2019) -- Start
                        'Enhancement NMB - Working on P2P Get Token Service URL.
                    Case "GETTOKENP2PSERVICEURL"
                        mstrGetTokenP2PServiceURL = CStr(dtRow.Item("key_value"))
                        'Pinkal (29-Aug-2019) -- End

                        'Gajanan [29-AUG-2019] -- Start      
                        'Enhancements: Settings for mandatory options in online recruitment
                    Case "RECQUALIFICATIONAWARDDATEMANDATORY"
                        mblnRecQualificationAwardDateMandatory = CBool(dtRow.Item("key_value"))

                    Case "RECQUALIFICATIONSTARTDATEMANDATORY"
                        mblnRecQualificationStartDateMandatory = CBool(dtRow.Item("key_value"))
                        'Gajanan [29-AUG-2019] -- End


                        'Gajanan [31-AUG-2019] -- Start      
                        'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                    Case "NEWJOBADDNOTIFICATIONUSERIDS"
                        mstrNewJobAddNotificationUserIds = CStr(dtRow.Item("key_value"))
                        'Gajanan [31-AUG-2019] -- End


                        'Gajanan [19-OCT-2019] -- Start    
                        'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    Case "ADDEDITUSERNOTIFICATIONUSERIDS"
                        mstrAddEditUserNotificationUserIds = CStr(dtRow.Item("key_value"))
                        'Gajanan [19-OCT-2019] -- End


                        'Pinkal (11-Sep-2019) -- Start
                        'Enhancement NMB - Working On Claim Retirement for NMB.
                    Case "CLAIMRETIREMENTTYPEID"
                        mintClaimRetirementTypeId = CInt(dtRow.Item("key_value"))

                    Case "CLAIMRETIREMENTFORMNOTYPE"
                        mintClaimRetirementFormNoType = CInt(dtRow.Item("key_value"))

                    Case "CLAIMRETIREMENTFORMNOPRIFIX"
                        mstrClaimRetirementFormNoPrifix = dtRow.Item("key_value").ToString()

                    Case "NEXTCLAIMRETIREMENTFORMNO"
                        mintNextClaimRetirementFormNo = CInt(dtRow.Item("key_value"))

                        'Pinkal (11-Sep-2019) -- End

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : New Screen Training Need Form.
                    Case "TRAININGNEEDFORMNOTYPE"
                        mintTrainingNeedFormNoType = CInt(dtRow.Item("key_value"))

                    Case "TRAININGNEEDFORMNOPREFIX"
                        mstrTrainingNeedFormNoPrefix = dtRow.Item("key_value").ToString()

                    Case "NEXTTRAININGNEEDFORMNO"
                        mintNextTrainingNeedFormNo = CInt(dtRow.Item("key_value"))
                        'Sohail (14 Nov 2019) -- End

                        'Gajanan [26-OCT-2019] -- Start    
                        'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    Case "APPLYMIGRATIONENFORCEMENT"
                        mblnApplyMigrationEnforcement = CBool(dtRow.Item("key_value"))
                        'Gajanan [26-OCT-2019] -- End

                        'S.SANDEEP |01-OCT-2019| -- START
                        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    Case "SHOWINTERDICTIONDATE"
                        mblnShowInterdictionDate = CBool(dtRow.Item("key_value"))
                    Case "NOTIFICATIONONDISCIPLINEFILING"
                        mblnNotificationOnDisciplineFiling = CBool(dtRow.Item("key_value"))
                    Case "SHOWREPONSETYPEONPOSTING"
                        mblnShowReponseTypeOnPosting = CBool(dtRow.Item("key_value"))
                    Case "PROCEEDINGAGAINSTEACHCOUNT"
                        mblnAddProceedingAgainstEachCount = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |01-OCT-2019| -- END

                        'Hemant (07 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
                    Case "INTERVIEWSCHEDULINGTEMPLATEID"
                        mintInterviewSchedulingTemplateId = CStr(dtRow.Item("key_value"))
                        'Hemant (07 Oct 2019) -- End
                        'Hemant (07 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                    Case "BATCHSCHEDULINGTEMPLATEID"
                        mintBatchSchedulingTemplateId = CStr(dtRow.Item("key_value"))
                        'Hemant (07 Oct 2019) -- End



                        'Gajanan [1-NOV-2019] -- Start    
                        'Enhancement:Worked On NMB ESS Comment  

                    Case "PRESENTADDRESSDOCSATTACHMENTMANDATORY"
                        mblnPresentAddressDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                    Case "RECRUITMENTADDRESSDOCSATTACHMENTMANDATORY"
                        mblnRecruitmentAddressDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                    Case "IDENTITYDOCSATTACHMENTMANDATORY"
                        mblnIdentityDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                    Case "JOBEXPERIENCEDOCSATTACHMENTMANDATORY"
                        mblnJobExperienceDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                        'Gajanan [1-NOV-2019] -- End

                        'Pinkal (24-Oct-2019) -- Start
                        'Enhancement NMB - Working On OT Enhancement for NMB.

                        'Pinkal (02-Jun-2020) -- Start
                        'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
                        'Case "ALLOWAPPLYOTMONTHDAY"
                        '    mintAllowApplyOTForEachMonthDay = CInt(dtRow.Item("key_value"))

                    Case "OTTENUREDAYS"
                        mintOTTenureDays = CInt(dtRow.Item("key_value"))

                        'Pinkal (02-Jun-2020) -- End


                        'Pinkal (24-Oct-2019) -- End

                        'Gajanan [6-NOV-2019] -- Start    
                        'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                        'Case "STAFFREQUISITIONDOCSATTACHMENTMANDATORY"
                        '    mblnStaffRequisitionDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))


                        'Gajanan [15-NOV-2019] -- Start   
                        'Enhancement:Worked On Staff Requisition New Attachment Mandatory Option FOR NMB   
                    Case "ADDITIONALSTAFFREQUISITIONDOCSATTACHMENTMANDATORY"
                        mblnAdditionalStaffRequisitionDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                    Case "ADDITIONALANDREPLACEMENTSTAFFREQUISITIONDOCSATTACHMENTMANDATORY"
                        mblnAdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                    Case "REPLACEMENTSTAFFREQUISITIONDOCSATTACHMENTMANDATORY"
                        mblnReplacementStaffRequisitionDocsAttachmentMandatory = CBool(dtRow.Item("key_value"))
                        'Gajanan [15-NOV-2019] -- End


                        'Gajanan [6-NOV-2019] -- End


                        'Hemant (30 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                    Case "JOBCONFIRMATIONTEMPLATEID"
                        mintJobConfirmationTemplateId = CStr(dtRow.Item("key_value"))
                    Case "JOBALERTTEMPLATEID"
                        mintJobAlertTemplateId = CStr(dtRow.Item("key_value"))
                    Case "INTERNALJOBALERTTEMPLATEID"
                        mintInternalJobAlertTemplateId = CStr(dtRow.Item("key_value"))
                        'Hemant (30 Oct 2019) -- End


                        'Pinkal (15-Nov-2019) -- Start
                        'Enhancement  St. Jude[0004149]  - Employee Timesheet Report should be optional for viewing in Employee SelfService.
                    Case "ALLOWTOVIEWEMPTIMESHEETREPORTFORESS"
                        mblnAllowToviewEmpTimesheetReportForESS = CBool(dtRow.Item("key_value"))
                        'Pinkal (15-Nov-2019) -- End

                        'S.SANDEEP |25-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : FlexCube
                    Case "FAILEDREQUESTNOTIFICATIONEMAILS"
                        mstrFailedRequestNotificationEmails = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP |25-NOV-2019| -- END


                        'Gajanan [24-OCT-2019] -- Start   
                        'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    Case "GRIEVANCEREPORTINGTOMAXAPPROVALLEVEL"
                        mintGrievanceReportingToMaxApprovalLevel = CInt(dtRow.Item("key_value"))
                        'Gajanan [24-OCT-2019] -- End

                        'Hemant (11 Dec 2019) -- Start
                        'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                    Case "EFTVOCNOTYPE"
                        mintEFTVocNoType = CInt(dtRow.Item("key_value"))
                    Case "EFTVOCPREFIX"
                        mstrEFTVocPrefix = CStr(dtRow.Item("key_value"))
                    Case "NEXTEFTVOCNO"
                        mintNextEFTVocNo = CInt(dtRow.Item("key_value"))
                        'Hemant (11 Dec 2019) -- End

  'Gajanan [9-Dec-2019] -- Start   
                        'Enhancement:Worked On Orbit Integration Configuration Setup
                    Case "ORBITUSERNAME"
                        mstrOrbitUsername = CStr(dtRow.Item("key_value"))
                    Case "ORBITPASSWORD"
                        mstrOrbitPassword = CStr(dtRow.Item("key_value"))
                    Case "ORBITAGENTUSERNAME"
                        mstrOrbitAgentUsername = CStr(dtRow.Item("key_value"))
                    Case "ISORBITINTEGRATED"
                        mblnIsOrbitIntegrated = CBool(dtRow.Item("key_value"))
                    Case "ORBITFAILEDREQUESTNOTIFICATIONEMAILS"
                        mstrOrbitFailedRequestNotificationEmails = CBool(dtRow.Item("key_value"))
                        'Gajanan [9-Dec-2019] -- End

                        'S.SANDEEP |24-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                    Case "PASCORINGROUDINGFACTOR"
                        mdecPAScoringRoudingFactor = CDec(dtRow.Item("key_value"))
                        'S.SANDEEP |24-DEC-2019| -- END


                        'Pinkal (03-Jan-2020) -- Start
                        'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
                    Case "APPLICABLELEAVESTATUS"
                        mstrApplicableLeaveStatus = CStr(dtRow.Item("key_value"))
                        'Pinkal (03-Jan-2020) -- End

                        'Pinkal (04-Feb-2020) -- Start
                        'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    Case "EMPNOTIFICATIONFORCLAIMAPPLICATIONBYMANAGER"
                        mblnEmpNotificationForClaimApplicationByManager = CBool(dtRow.Item("key_value"))
                        'Pinkal (04-Feb-2020) -- End


                        'Pinkal (19-Feb-2020) -- Start
                        'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                    Case "LEAVEISSUEASONDATEONLEAVEBALANCELISTREPORT"
                        mblnLeaveIssueAsonDateOnLeaveBalanceListReport = CBool(dtRow.Item("key_value"))
                        'Pinkal (19-Feb-2020) -- End


                        'Pinkal (30-Mar-2020) -- Start
                        'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
                    Case "ALLOWTOCANCELLEAVEBUTRETAINEXPENSE"
                        mblnAllowToCancelLeaveButRetainExpense = CBool(dtRow.Item("key_value"))
                        'Pinkal (30-Mar-2020) -- End


                        'Pinkal (04-Apr-2020) -- Start
                        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    Case "ACTIVEDIRECTORYNOTIFICATIONUSERIDS"
                        GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrActiveDirectoryNotificationUserIds)

                    Case "EXCHANGESERVERIP"
                        mstrExchangeServerIP = CStr(dtRow.Item("key_value"))

                    Case "EXCHANGESERVERUSERNAME"
                        mstrExchangeServerUserName = CStr(dtRow.Item("key_value"))

                    Case "EXCHANGESERVERUSERPASSWORD"
                        mstrExchangeServerUserPassword = clsSecurity.Decrypt(CStr(dtRow.Item("key_value")), "ezee")

                    Case "EXCHANGESERVERDATABASENAME"
                        mstrExchangeDatabaseName = CStr(dtRow.Item("key_value"))

                        'Pinkal (04-Apr-2020) -- End

    				'Gajanan [28-May-2020] -- Start
                    Case "ALLOWTOVIEWSCOREWHILEDOINGASSESMENT"
                        'mblnAllowToViewScoreWhileDoingAssesment = CBool(dtRow.Item("key_value"))
                        mblnAllowToViewScoreWhileDoingAssesment = False
                    'Gajanan [28-May-2020] -- End
                       'Pinkal (22-May-2020) -- Start
    'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
                    Case "ENABLEEXCHANGEMAILBOXAFTERMINS"
                        mintEnableExchangeMailBoxAfterMins = CInt(dtRow.Item("key_value"))
                        'Pinkal (22-May-2020) -- End


                        'Pinkal (30-May-2020) -- Start
                        'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.
                    Case "ALLOWTOCANCELLEAVEFORCLOSEDPERIOD"
                        mblnAllowToCancelLeaveForClosedPeriod = CBool(dtRow.Item("key_value"))
                        'Pinkal (30-May-2020) -- End


                        'Pinkal (30-May-2020) -- Start 
                        'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
                    Case "ALLOWONLYONEEXPENSEINCLAIMAPPLICATION"
                        mblnAllowOnlyOneExpenseInClaimApplication = CBool(dtRow.Item("key_value"))
                        'Pinkal (30-May-2020) -- End


                        'Gajanan [02-June-2020] -- Start
                        'Enhancement Configure Option That Allow Period Closer Before XX Days.
                    Case "ALLOWTOCLOSEPERIODBEFORE"
                        mblnAllowToClosePeriodBefore = CBool(dtRow.Item("key_value"))

                    Case "CLOSEPERIODDAYSBEFORE"
                        mintClosePeriodDaysBefore = CInt(dtRow.Item("key_value"))
                        'Gajanan [02-June-2020] -- End

                        'Gajanan [19-June-2020] -- Start
                        'Enhancement NMB Employee Signature Enhancement.
                    Case "ALLOWEMPLOYEETOADDEDITSIGNATUREESS"
                        mblnAllowEmployeeToAddEditSignatureESS = CBool(dtRow.Item("key_value"))
                        'Gajanan [19-June-2020] -- End

                        'S.SANDEEP |27-JUN-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
                    Case "ALLOWTERMINATIONIFPAYMENTDONE"
                        mblnAllowTerminationIfPaymentDone = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |27-JUN-2020| -- END

'Gajanan [1-July-2020] -- Start
                        'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
                    Case "ADDEDITTRANSECTIONHEADNOTIFICATIONUSERIDS"
                        mstrAddEditTransectionHeadNotificationUserIds = CStr(dtRow.Item("key_value"))

                    Case "ONIMPORTINCLUDETRANSECTIONHEADWHILESENDINGNOTIFICATION"
                        mblnOnImportIncludeTransectionHeadWhileSendingNotification = CBool(dtRow.Item("key_value"))
                        'Gajanan [1-July-2020] -- End


                        'S.SANDEEP |06-JUL-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD)
                    Case "ALLOWREHIREONCLOSEDPERIOD"
                        mblnAllowRehireOnClosedPeriod = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |06-JUL-2020| -- END


                        'Pinkal (27-Jul-2020) -- Start
                        'Enhancement NMB -   Working on Skip Enable Exchange Server Email for Specfic allocations.
                    Case "SKIPEXCHANGEENABLEEMAILALLOCATIONS"
                        mstrSkipExchangeEnableEmailAllocations = dtRow.Item("key_value").ToString()
                        'Pinkal (27-Jul-2020) -- End

                        'Gajanan [13-Nov-2020] -- Start
                        'Final Recruitment UAT v1 Changes.
                    Case "ALLOWTOSYNCJOBWITHVACANCYMASTER"
                        mblnAllowToSyncJobWithVacancyMaster = CBool(dtRow.Item("key_value"))
                        'Gajanan [13-Nov-2020] -- End
'Pinkal (27-Oct-2020) -- Start
                        'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
                    Case "RUNDAILYATTENDANCESERVICETIME"
                        mdtRunDailyAttendanceServiceTime = CDate(dtRow.Item("key_value"))

                    Case "SENDTNAEARLYLATEREPORTSUSERIDS"
                        mstrSendTnAEarlyLateReportsUserIds = dtRow.Item("key_value").ToString()

                    Case "TNAFAILURENOTIFICATIONUSERIDS"
                        mstrTnAFailureNotificationUserIds = dtRow.Item("key_value").ToString()
                        'Pinkal (27-Oct-2020) -- End

                        'Pinkal (12-Nov-2020) -- Start
                        'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                    Case "SKIPAPPROVALFLOWFORAPPLICANTSHORTLISTING"
                        mblnSkipApprovalFlowForApplicantShortListing = CBool(dtRow.Item("key_value"))

                    Case "SKIPAPPROVALFLOWFORFINALAPPLICANT"
                        mblnSkipApprovalFlowForFinalApplicant = CBool(dtRow.Item("key_value"))

                        'Pinkal (12-Nov-2020) -- End
'S.SANDEEP |12-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : SPRINT-6
                    Case "DISCIPLINENOTIFYCHARGEPOSTEDUSERIDS"
                        If CStr(dtRow.Item("key_value")).Trim.Length > 0 Then
                            GetActiveUserList_CSV(CStr(dtRow.Item("key_value")), mstrDisciplineNotifyChargePostedUserIds)
                            mstrDisciplineNotifyChargePostedUserIds = CStr(dtRow.Item("key_value"))
                        End If
                        'S.SANDEEP |12-NOV-2020| -- END

  'Pinkal (27-Nov-2020) -- Start
                        'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                    Case "ALLOWEMPSYSTEMACCESSONACTUALEOCRETIREMENTDATE"
                        mblnAllowEmpSystemAccessOnActualEOCRetirementDate = CBool(dtRow.Item("key_value"))
                        'Pinkal (27-Nov-2020) -- End

                        'Pinkal (19-Dec-2020) -- Start
                        'Enhancement  -  Working on Discipline module for NMB.
                    Case "ALLOWTOVIEWESSVISIBLEWHILEFILLIGDISCIPLINECHARGE"
                        mblnAllowToViewESSVisibleWhileFilligDisciplineCharge = CBool(dtRow.Item("key_value"))
                        'Pinkal (19-Dec-2020) -- End

'S.SANDEEP |13-NOV-2020| -- START
                        'ISSUE/ENHANCEMENT : COMPETENCIES
                    Case "RUNCOMPETENCEASSESSMENTSEPARATELY"
                        mblnRunCompetenceAssessmentSeparately = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |13-NOV-2020| -- END



                        'Pinkal (09-Aug-2021)-- Start
                        'NMB New UI Enhancements.

                        'Case "EMPBIRTHDAYALLOCATION"
                        '    mintEmpBirthdayAllocation = CInt(dtRow.Item("key_value"))

                        'Case "EMPSTAFFTURNOVERALLOCATION"
                        '    mintEmpStaffTurnOverAllocation = CInt(dtRow.Item("key_value"))

                        'Case "EMPONLEAVEALLOCATION"
                        '    mintEmpOnLeaveAllocation = CInt(dtRow.Item("key_value"))

                        'Case "EMPTEAMMEMBERSALLOCATION"
                        '    mintEmpTeamMembersAllocation = CInt(dtRow.Item("key_value"))

                        'Case "EMPWORKANNIVERSARYALLOCATION"
                        '    mintEmpWorkAnniversaryAllocation = CInt(dtRow.Item("key_value"))

                    Case "EMPBIRTHDAYALLOCATION"
                        mstrEmpBirthdayAllocation = dtRow.Item("key_value").ToString()

                    Case "EMPSTAFFTURNOVERALLOCATION"
                        mstrEmpStaffTurnOverAllocation = dtRow.Item("key_value").ToString()

                    Case "EMPONLEAVEALLOCATION"
                        mstrEmpOnLeaveAllocation = dtRow.Item("key_value").ToString()

                    Case "EMPTEAMMEMBERSALLOCATION"
                        mstrEmpTeamMembersAllocation = dtRow.Item("key_value").ToString()

                    Case "EMPWORKANNIVERSARYALLOCATION"
                        mstrEmpWorkAnniversaryAllocation = dtRow.Item("key_value").ToString()

                        'Pinkal (09-Aug-2021) -- End


                        'Hemant (01 Jan 2021) -- Start
                        'Enhancement #OLD-238 - Improved Employee Profile page
                    Case "ALLOCATIONWORKSTATION"
                        mintAllocationWorkStation = CInt(dtRow.Item("key_value"))
                        'Hemant (01 Jan 2021) -- End


                        'Pinkal (01-Feb-2021) -- Start
                        'Enhancement NMB - Working Training Enhancements for New UI.
                    Case "ALLOWTOADDTRAININGWITHOUTTNAPROCESS"
                        mblnAllowToAddTrainingwithoutTNAProcess = CBool(dtRow.Item("key_value"))

                    Case "ALLOWTOAPPLYREQUESTTRAININGNOTINTRAININGPLAN"
                        mblnAllowToApplyRequestTrainingNotinTrainingPlan = CBool(dtRow.Item("key_value"))

                    Case "TRAININGREQUIREFORFOREIGNTRAVELLING"
                        mblnTrainingRequireForForeignTravelling = CBool(dtRow.Item("key_value"))

                        'Pinkal (01-Feb-2021) -- End

                        'S.SANDEEP |09-FEB-2021| -- START
                        'ISSUE/ENHANCEMENT : Competencies Period Changes
                    Case "COMPETENCIESASSESSINSTRUCTIONS"
                        mstrCompetenciesAssessInstructions = CStr(dtRow.Item("key_value"))
                        'S.SANDEEP |09-FEB-2021| -- END

'Pinkal (10-Feb-2021) -- Start
                        'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                    Case "UNRETIREDIMPRESTTOPAYROLLAFTERDAYS"
                        mintUnRetiredImprestToPayrollAfterDays = CInt(dtRow.Item("key_value"))
                        'Pinkal (10-Feb-2021) -- End

                        'Hemant (26 Mar 2021) -- Start
                        'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    Case "TRAININGNEEDALLOCATIONID"
                        mintTrainingNeedAllocationID = CInt(dtRow.Item("key_value"))
                        'Hemant (26 Mar 2021) -- End

                        'Sohail (10 Feb 2022) -- Start
                        'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                    Case "TRAININGBUDGETALLOCATIONID"
                        mintTrainingBudgetAllocationID = CInt(dtRow.Item("key_value"))

                    Case "TRAININGCOSTCENTERALLOCATIONID"
                        mintTrainingCostCenterAllocationID = CInt(dtRow.Item("key_value"))

                    Case "TRAININGREMAININGBALANCEBASEDONID"
                        mintTrainingRemainingBalanceBasedOnID = CInt(dtRow.Item("key_value"))
                        'Sohail (10 Feb 2022) -- End

                        'Gajanan [4-May-2021] -- Start
                    Case "APPOINTEDATEUSERNOTIFICATION"
                        mstrAppointeDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "CONFIRMDATEUSERNOTIFICATION"
                        mstrConfirmDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "BIRTHDATEUSERNOTIFICATION"
                        mstrBirthDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "SUSPENSIONDATEUSERNOTIFICATION"
                        mstrSuspensionDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "PROBATIONDATEUSERNOTIFICATION"
                        mstrProbationDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "EOCDATEUSERNOTIFICATION"
                        mstrEocDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "LEAVINGDATEUSERNOTIFICATION"
                        mstrLeavingDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "RETIREMENTDATEUSERNOTIFICATION"
                        mstrRetirementDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "REINSTATEMENTDATEUSERNOTIFICATION"
                        mstrReinstatementDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "MARRIAGEDATEUSERNOTIFICATION"
                        mstrMarriageDateUserNotification = CStr(dtRow.Item("key_value"))

                    Case "EXEMPTIONDATEUSERNOTIFICATION"
                        mstrExemptionDateUserNotification = CStr(dtRow.Item("key_value"))
                        'Gajanan [4-May-2021] -- End

'S.SANDEEP |03-MAY-2021| -- START
                        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                    Case "ISPDP_PERF_INTEGRATED"
                        mblnIsPDP_Perf_Integrated = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |03-MAY-2021| -- END

                        'S.SANDEEP |13-MAY-2021| -- START
                        'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
                    Case "ALLOWTOCLOSECASEWOPROCEEDINGS"
                        mblnAllowToCloseCaseWOProceedings = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |13-MAY-2021| -- END

 'S.SANDEEP |17-MAY-2021| -- START
                        'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
                    Case "DONTALLOWRATINGBEYOND100"
                        mblnDontAllowRatingBeyond100 = CBool(dtRow.Item("key_value"))                           
                        'S.SANDEEP |17-MAY-2021| -- END

                        'Hemant (28 Jul 2021) -- Start             
                        'ENHANCEMENT : OLD-293 - Training Evaluation
                    Case "PRETRAININGEVALUATIONSUBMITTED"
                        mblnPreTrainingEvaluationSubmitted = CBool(dtRow.Item("key_value"))
                        'Hemant (28 Jul 2021) -- End

                        'Hemant (04 Sep 2021) -- Start
                        'ENHANCEMENT : OLD-458 - Additional Box  for evaluation instructions on every Evaluation Category.
                    Case "PRETRAININGFEEDBACKINSTRUCTION"
                        mstrPreTrainingFeedbackInstruction = CStr(dtRow.Item("key_value"))

                    Case "POSTTRAININGFEEDBACKINSTRUCTION"
                        mstrPostTrainingFeedbackInstruction = CStr(dtRow.Item("key_value"))

                    Case "DAYSAFTERTRAININGFEEDBACKINSTRUCTION"
                        mstrDaysAfterTrainingFeedbackInstruction = CStr(dtRow.Item("key_value"))
                        'Hemant (04 Sep 2021) -- End

                        'Hemant (23 Sep 2021) -- Start
                        'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
                    Case "SKIPTRAININGREQUISITIONANDAPPROVAL"
                        mblnSkipTrainingRequisitionAndApproval = CBool(dtRow.Item("key_value"))
                        'Hemant (23 Sep 2021) -- End

                        'Hemant (24 Sep 2021) -- Start             
                        'ISSUE/ENHANCEMENT : OLD-481(ZRA) - Give Configurable Option to include Case Information on Hearing Schedule Notification
                    Case "ALLOWTOINCLUDECASEINFOHEARINGNOTIFICATION"
                        mblnAllowToIncludeCaseInfoHearingNotification = CBool(dtRow.Item("key_value"))
                        'Hemant (24 Sep 2021) -- End

                        'Hemant (24 Sep 2021) -- Start             
                        'ISSUE/ENHANCEMENT : OLD-482(ZRA) - Give Option to Notify The Accused/Charged Employee of Hearing Schedule
                    Case "ALLOWTONOTIFYCHARGEDEMPLOYEEOFHEARING"
                        mblnAllowToNotifyChargedEmployeeOfHearing = CBool(dtRow.Item("key_value"))
                        'Hemant (24 Sep 2021) -- End

'Pinkal (27-Aug-2021) -- Start
                        'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
                    Case "MAXEMPPLANNEDLEAVE"
                        mintMaxEmpPlannedLeave = CInt(dtRow.Item("key_value"))

                    Case "MAXEMPLEAVEPLANNERALLOCATION"
                        mstrMaxEmpLeavePlannerAllocation = dtRow.Item("key_value").ToString().Trim()
                        'Pinkal (27-Aug-2021) -- End

                        'S.SANDEEP |01-OCT-2021| -- START
                    Case "DEFAULTHEARINGNOTIFICATIONTYPE"
                        mintDefaultHearingNotificationType = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP |01-OCT-2021| -- END

                        'Hemant (24 Nov 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                    Case "SKIPEOCVALIDATIONONLOANTENURE"
                        mblnSkipEOCValidationOnLoanTenure = CBool(dtRow.Item("key_value"))
                        'Hemant (24 Nov 2021) -- End

                        'S.SANDEEP |10-AUG-2021| -- START
                    Case "CONSIDERZEROASNUMUPDATEPROGRESS"
                        mblnConsiderZeroAsNumUpdateProgress = CBool(dtRow.Item("key_value"))
                        'S.SANDEEP |10-AUG-2021| -- END

                        'S.SANDEEP |21-DEC-2021| -- START
                        'ISSUE : GIVING PERIOD SELECTION ON TRAINING SETTING FOR FETCHING COMPETENCIES
                    Case "TRAININGCOMPETENCIESPERIODID"
                        mintTrainingCompetenciesPeriodId = CInt(dtRow.Item("key_value"))
                        'S.SANDEEP |21-DEC-2021| -- END

                        'Hemant (09 Feb 2022) -- Start            
                        'OLD-561(NMB) : Option to complete training should have expiry days
                    Case "DAYSFROMLASTDATEOFTRAININGTOALLOWCOMPLETETRAINING"
                        mintDaysFromLastDateOfTrainingToAllowCompleteTraining = CInt(dtRow.Item("key_value"))

                    Case "DAYSFORREMINDEREMAILBEFOREEXPIRYTOCOMPLETETRAINING"
                        mintDaysForReminderEmailBeforeExpiryToCompleteTraining = CInt(dtRow.Item("key_value"))
                        'Hemant (09 Feb 2022) -- End

                        'Hemant (09 Feb 2022) -- Start            
                        'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
                    Case "POSTTRAININGEVALUATIONBEFORECOMPLETETRAINING"
                        mblnPostTrainingEvaluationBeforeCompleteTraining = CBool(dtRow.Item("key_value"))
                        'Hemant (09 Feb 2022) -- End

                        'Hemant (09 Feb 2022) -- Start            
                        'OLD-549(NMB) : Give new screen for training approver allocation mapping
                    Case "TRAININGAPPROVERALLOCATIONID"
                        mintTrainingApproverAllocationID = CInt(dtRow.Item("key_value"))
                        'Hemant (09 Feb 2022) -- End

                End Select
            Next
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            '    DisplayError.Show("-1", ex.Message, "setLocalVars", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: setLocalVars; Module Name: " & mstrModuleName)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Write a value into the Class data Table.
    ''' </summary>
    ''' <param name="ParameterKey">a ParameterKey that represents any of the Parameter keys
    ''' where you want to read.</param> 
    ''' <param name="ParameterValue">an Object with the value to be read.</param> 
    Private Sub setParamVal(ByVal ParameterKey As String, ByVal ParameterValue As String)

        Dim dtRows As DataRow()
        Dim dtRow As DataRow
        Try
            dtRows = mdtParam.Select("key_name = '" & ParameterKey.ToUpper & "'")
            Dim iValue As String = String.Empty
            If dtRows.Length > 0 Then
                dtRow = dtRows(0)
            Else
                dtRow = mdtParam.NewRow
                dtRow.Item("isNew") = True
            End If

            dtRow.Item("key_name") = ParameterKey
            dtRow.Item("key_value") = ParameterValue
            IsValue_Changed(ParameterKey, mintCompanyUnkid, iValue)
            If iValue <> ParameterValue Then
                dtRow.Item("isChanged") = True
            End If
            If Not dtRows.Length > 0 Then
                mdtParam.Rows.Add(dtRow)
            End If

            mdtParam.AcceptChanges()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "setParamVal", mstrModuleName)
        Finally
            dtRows = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Create Configuration Prameter's Datatable From Database table hrmsConfiguration..cfconfiguration.
    ''' Modify By: Sandeep
    ''' </summary>
    Private Function createParamDataTable() As DataTable
        Dim dtParam As New DataTable("notice")
        Dim strQ As String = ""

        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try


            strQ = "SELECT key_name, " & _
                        "key_value, " & _
                        "CAST (0 AS BIT) AS isChanged, " & _
                        "CAST (0 AS BIT) AS isNew " & _
                    "FROM hrmsConfiguration..cfconfiguration " & _
                    "WHERE (hrmsConfiguration..cfconfiguration.companyunkid = @CompanyId  OR hrmsConfiguration..cfconfiguration.companyunkid <= 0)  "

            'Pinkal (18-Aug-2018) --  Active Directory Integration Requirement For NMB [Ref No : 273].[ (hrmsConfiguration..cfconfiguration.companyunkid = @CompanyId  OR hrmsConfiguration..cfconfiguration.companyunkid <= 0)]


            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid)

            dtParam = objDataOperation.ExecQuery(strQ, "ParamList").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dtParam

        Catch ex As System.Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Call DisplayError.Show(-1, ex.Message, "createParamDataTable", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: createParamDataTable; Module Name: " & mstrModuleName)
            'Pinkal (13-Aug-2020) -- End
            Return dtParam
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Insert / update a class data table changed value into the cfParam Table.
    ''' </summary>
    Public Function updateParam() As Boolean

        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim exForce As Exception
        Dim dtRows As DataRow()

        Try

            dtRows = mdtParam.Select("isChanged = 1")

            For Each dtRow As DataRow In dtRows
                objDataOperation.ClearParameters()
                'S.SANDEEP [ 08 APR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objDataOperation.AddParameter("@key_value", SqlDbType.VarChar, 8000, dtRow.Item("key_value").ToString)
                objDataOperation.AddParameter("@key_value", SqlDbType.VarChar, dtRow.Item("key_value").ToString.Length, dtRow.Item("key_value").ToString)
                'S.SANDEEP [ 08 APR 2013 ] -- END
                objDataOperation.AddParameter("@key_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtRow.Item("key_name").ToString)
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid.ToString)

                If dtRow.Item("isNew") Then
                    strQ = "INSERT INTO hrmsConfiguration..cfconfiguration( " & _
                                "key_value, " & _
                                "key_name, " & _
                                "companyunkid " & _
                            ") VALUES ( " & _
                                "@key_value, " & _
                                "@key_name, " & _
                                "@companyunkid ) "
                Else
                    'Sandeep [ 10 FEB 2011 ] -- Start
                    'strQ = "UPDATE hrmsConfiguration..cfconfiguration SET " & _
                    '            "key_value = @key_value " & _
                    '            ",companyunkid = @companyunkid " & _
                    '        "WHERE key_name = @key_name "

                    strQ = "UPDATE hrmsConfiguration..cfconfiguration SET " & _
                                "key_value = @key_value " & _
                                ",companyunkid = @companyunkid " & _
                                ", Syncdatetime = NULL " & _
                            "WHERE key_name = @key_name AND companyunkid = @companyunkid "
                    'Hemant (30 Oct 2019) --  Syncdatetime = NULL]
                    'Sandeep [ 10 FEB 2011 ] -- End 
                End If

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                Else
                    dtRow.Item("isNew") = 0
                    dtRow.Item("isChanged") = 0
                End If

                'S.SANDEEP [ 27 AUG 2014 ] -- START
                If dtRow.Item("key_name").ToString.ToUpper = "RETIREMENTVALUE" Or dtRow.Item("key_name").ToString.ToUpper = "FRETIREMENTVALUE" Then
                    Dim iUpdateModeId As Integer = 0
                    Select Case dtRow.Item("key_name").ToString.ToUpper
                        Case "RETIREMENTVALUE"
                            iUpdateModeId = 1
                        Case "FRETIREMENTVALUE"
                            iUpdateModeId = 2
                    End Select
                    Update_Retirement_Date(CInt(dtRow.Item("key_value")), iUpdateModeId, mintCompanyUnkid, objDataOperation)
                End If
                'S.SANDEEP [ 27 AUG 2014 ] -- END

                'S.SANDEEP [09-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#292}
                If dtRow.Item("key_name").ToString.ToUpper = "FLEXSRVRUNNINGTIME" Then
                    Dim objAppSetting As New clsApplicationSettings
                    objAppSetting._FlexcubeRunningTime = eZeeDate.convertTime(CType(dtRow.Item("key_value"), DateTime)).ToString().Substring(0, 5)
                End If
                'S.SANDEEP [09-AUG-2018] -- END

            Next

            gobjConfigOptions = New clsConfigOptions

            Return True
        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "updateParam", mstrModuleName)
            Return False
        Finally
            objDataOperation = Nothing
            dtRows = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function isParamFill() As Boolean
        Dim objData As New clsDataOperation
        Dim btnFlag As Boolean = True
        Dim strQ As String
        Try

            strQ = "SELECT * FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @companyunkid"

            objData.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid.ToString)

            btnFlag = objData.RecordCount(strQ) > 1
            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "isParamFill", mstrModuleName)
        Finally
            objData.Dispose()
            objData = Nothing
        End Try
        Return btnFlag
    End Function

    Private Sub FillData()
        Try
            mdtParam = New DataTable
            Call setLocalVars()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillData", mstrModuleName)
        End Try
    End Sub

    Private Function GetCurrentDateAndTime() As Date
        Dim dtCurrentDateTime As Date
        Dim StrQ As String = ""
        Dim objDataoperation As New clsDataOperation
        Dim dsList As DataSet
        Dim exForce As Exception
        Try

            StrQ = " SELECT GETDATE() "

            'dtCurrentDateTime = objDataoperation.ExecQuery(StrQ, "DateTime").Tables(0).Rows(0)(0)
            dsList = objDataoperation.ExecQuery(StrQ, "DateTime")

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

            dtCurrentDateTime = dsList.Tables(0).Rows(0)(0)


            Return dtCurrentDateTime

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCurrentDateAndTime", mstrModuleName)
            Return Now
        Finally
            exForce = Nothing
            If objDataoperation IsNot Nothing Then
                objDataoperation.Dispose()
                objDataoperation = Nothing
            End If
        End Try
    End Function

    Public Sub GetReportSettings(ByVal intReportId As Integer)
        Try
            'Sohail (23 Sep 2016) -- Start
            'Issue -  63.1 - If any report having these setting TRUE then thereafter all reports setting returned true as there is no else part.
            mblnIsDisplayLogo = False
            mblnIsShowApprovedBy = False
            mblnIsShowPreparedBy = False
            mblnIsShowCheckedBy = False
            mblnIsShowReceivedBy = False
            mdblLeftMargin = 0.25
            mdblRightMargin = 0.25
            'Sohail (23 Sep 2016) -- End

            If _DisplayLogo.Length > 0 Then
                For Each strId As String In _DisplayLogo.Split(",")
                    If CInt(strId) = intReportId Then
                        mblnIsDisplayLogo = True
                    End If
                Next
            End If

            If _ApprovedBy.Length > 0 Then
                For Each strId As String In _ApprovedBy.Split(",")
                    If CInt(strId) = intReportId Then
                        mblnIsShowApprovedBy = True
                    End If
                Next
            End If

            If _PreparedBy.Length > 0 Then
                For Each strId As String In _PreparedBy.Split(",")
                    If CInt(strId) = intReportId Then
                        mblnIsShowPreparedBy = True
                    End If
                Next
            End If

            If _CheckedBy.Length > 0 Then
                For Each strId As String In _CheckedBy.Split(",")
                    If CInt(strId) = intReportId Then
                        mblnIsShowCheckedBy = True
                    End If
                Next
            End If

            If _ReceivedBy.Length > 0 Then
                For Each strId As String In _ReceivedBy.Split(",")
                    If CInt(strId) = intReportId Then
                        mblnIsShowReceivedBy = True
                    End If
                Next
            End If


            'Anjan (11 Mar 2011)-Start
            If _LeftMargin.Length > 0 Then
                For Each strid As String In _LeftMargin.Split(",")
                    If strid.Contains("|") Then
                        If CInt(strid.Substring(0, strid.IndexOf("|"))) = intReportId Then
                            mdblLeftMargin = CDec(strid.Substring(strid.IndexOf("|") + 1))
                        End If
                    End If
                Next
            End If

            If _RightMargin.Length > 0 Then
                For Each strid As String In _RightMargin.Split(",")
                    If strid.Contains("|") Then
                        If CInt(strid.Substring(0, strid.IndexOf("|"))) = intReportId Then
                            mdblRightMargin = CDec(strid.Substring(strid.IndexOf("|") + 1))
                        End If
                    End If
                Next
            End If
            'Anjan (11 Mar 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetReportSettings", mstrModuleName)
        End Try
    End Sub

    Public Function GetMaxAppCode(ByVal intcompanyid As Integer) As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim dbname As String = ""
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = " SELECT TOP 1 database_name AS dName  FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & intcompanyid & " AND isclosed = 0 order by yearunkid desc "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables(0).Rows.Count > 0 Then
                dbname = dsList.Tables(0).Rows(0).Item("dName").ToString.Trim
            Else
                Return -1
            End If
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            'strQ = "SELECT  ISNULL(CASE WHEN ISNUMERIC(applicant_code) = 1 " & _
            '        "THEN MAX(CAST(applicant_code AS INT)) + 1 " & _
            '        "END ,1) AS aCode " & _
            '        "FROM " & dbname & "..rcapplicant_master " & _
            '        "WHERE(IsNumeric(applicant_code) = 1) " & _
            '        "GROUP BY ISNUMERIC(applicant_code)"

            'S.SANDEEP [ 29 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'strQ = "SELECT ISNULL(MAX(CAST(SUBSTRING(applicant_code," & _ApplicantCodePrifix.Length + 1 & ",255) AS INT)),0) + 1 AS aCode FROM " & dbname & "..rcapplicant_master "
            strQ = "SELECT ISNULL(MAX(CAST(" & dbname & ".dbo.udf_GetNumeric(ISNULL(applicant_code,0)) AS DECIMAL(8000))),0) + 1 as aCode FROM " & dbname & "..rcapplicant_master "
            'S.SANDEEP [ 22 OCT 2013 ] -- END
            'strQ = "SELECT ISNULL(MAX(CAST(SUBSTRING(applicant_code,PATINDEX('%[0-9]%', applicant_code),LEN(PATINDEX('%[0-9]%', applicant_code))) AS INT)),0) + 1 AS aCode FROM " & dbname & "..rcapplicant_master"
            'S.SANDEEP [ 29 JULY 2013 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("aCode")
            Else
                Return -1
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMaxAppCode", mstrModuleName)
            Return -1
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
            strQ = Nothing
        End Try
    End Function

    'Sohail (01 May 2013) -- Start
    'TRA - ENHANCEMENT

    'S.SANDEEP |26-APR-2019| -- START
    'Public Function GetKeyValue(ByVal intCompanyUnkId As Integer, ByVal strKeyName As String) As String
    '    Dim dsList As DataSet
    '    Dim strQ As String = ""
    '    Dim strKeyValue As String = ""

    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    Try


    '        strQ = "SELECT key_name, " & _
    '                    "key_value, " & _
    '                    "CAST (0 AS BIT) AS isNew " & _
    '                "FROM hrmsConfiguration..cfconfiguration " & _
    '                "WHERE key_name = @key_name "

    '        If intCompanyUnkId > 0 Then
    '            strQ &= " AND companyunkid =  @CompanyId "

    '            'Pinkal (16-Apr-2016) -- Start
    '            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    '            'objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid)
    '            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)
    '            'Pinkal (16-Apr-2016) -- End
    '        Else
    '            strQ &= " AND companyunkid IS NULL "
    '        End If


    '        'Pinkal (16-Apr-2016) -- Start
    '        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    '        'objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyUnkid)
    '        'Pinkal (16-Apr-2016) -- End
    '        objDataOperation.AddParameter("@key_name", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, strKeyName)

    '        dsList = objDataOperation.ExecQuery(strQ, "KeyValue")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables("KeyValue").Rows.Count > 0 Then
    '            strKeyValue = dsList.Tables("KeyValue").Rows(0).Item("key_value").ToString
    '            If strKeyValue.Trim <> "" AndAlso intCompanyUnkId <= 0 Then
    '                If ArtLic._Object.HotelName.Trim = "" Then
    '                    Dim objGroupMaster As New clsGroup_Master
    '                    objGroupMaster._Groupunkid = 1
    '                    If objGroupMaster._Groupname.Trim <> "" Then
    '                        ArtLic._Object.HotelName = objGroupMaster._Groupname
    '                    End If
    '                End If
    '                strKeyValue = ArtLic._Object.Decrypt(strKeyValue)
    '            End If
    '        End If
    '        'Pinkal (16-Apr-2016) -- Start
    '        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.(S)
    '        If strKeyName.Trim.ToUpper = "COMPANYDATEFORMAT" AndAlso strKeyValue.Trim.Length <= 0 Then
    '            strKeyValue = "dd-MMM-yyyy"
    '        End If
    '        'Pinkal (16-Apr-2016) -- End



    '        Return strKeyValue

    '    Catch ex As System.Exception
    '        Call DisplayError.Show(-1, ex.Message, "GetKeyValue", mstrModuleName)
    '        Return strKeyValue
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '    End Try

    'End Function

    ''S.SANDEEP |11-APR-2019| -- START
    'Public Function GetKeyValue(ByVal intCompanyUnkId As Integer, ByVal strKeyNames() As String) As Dictionary(Of String, String)
    '    Dim xDicKeyValue As New Dictionary(Of String, String)
    '    Dim dsList As DataSet
    '    Dim strQ As String = ""
    '    Dim strKeyValue As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        If strKeyNames.Length > 0 Then
    '            Dim strInQryParam As String = String.Empty
    '            strInQryParam = String.Join("','", strKeyNames.ToArray())
    '            If strInQryParam.Trim.Length > 0 Then strInQryParam = "'" & strInQryParam & "'"

    '            strQ = "SELECT " & _
    '                   "     key_name " & _
    '                   "    ,key_value " & _
    '                   "FROM hrmsConfiguration..cfconfiguration " & _
    '                   "WHERE key_name IN (" & strInQryParam & ") " & _
    '                   " AND key_value <> '' "

    '            If intCompanyUnkId > 0 Then
    '                strQ &= " AND companyunkid =  @CompanyId "
    '                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)
    '            Else
    '                strQ &= " AND companyunkid IS NULL "
    '            End If

    '            dsList = objDataOperation.ExecQuery(strQ, "KeyValue")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables("KeyValue").Rows.Count > 0 Then
    '                xDicKeyValue = dsList.Tables("KeyValue").AsEnumerable().ToDictionary(Function(x) x.Field(Of String)("key_name"), _
    '                                                                                     Function(y) y.Field(Of String)("key_value"))
    '            End If

    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetKeyValue; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return xDicKeyValue
    'End Function
    ''S.SANDEEP |11-APR-2019| -- END


    Public Function GetKeyValue(ByVal intCompanyUnkId As Integer, ByVal strKeyName As String, Optional ByVal xDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet
        Dim strQ As String = ""
        Dim strKeyValue As String = ""

        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        Try

            'Pinkal (30-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            objDataOperation.ClearParameters()
            'Pinkal (30-Aug-2019) -- End


            strQ = "SELECT key_name, " & _
                        "key_value, " & _
                        "CAST (0 AS BIT) AS isNew " & _
                    "FROM hrmsConfiguration..cfconfiguration " & _
                    "WHERE key_name = @key_name "

            If intCompanyUnkId > 0 Then
                strQ &= " AND companyunkid =  @CompanyId "

                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)
            Else
                strQ &= " AND companyunkid IS NULL "
            End If


            objDataOperation.AddParameter("@key_name", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, strKeyName)

            dsList = objDataOperation.ExecQuery(strQ, "KeyValue")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("KeyValue").Rows.Count > 0 Then
                strKeyValue = dsList.Tables("KeyValue").Rows(0).Item("key_value").ToString
                If strKeyValue.Trim <> "" AndAlso intCompanyUnkId <= 0 Then
                    If ArtLic._Object.HotelName.Trim = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        If objGroupMaster._Groupname.Trim <> "" Then
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If
                    End If
                    strKeyValue = ArtLic._Object.Decrypt(strKeyValue)
                End If
            End If
            If strKeyName.Trim.ToUpper = "COMPANYDATEFORMAT" AndAlso strKeyValue.Trim.Length <= 0 Then
                strKeyValue = "dd-MMM-yyyy"
            End If

            Return strKeyValue

        Catch ex As System.Exception
            'Call DisplayError.Show(-1, ex.Message, "GetKeyValue", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetKeyValue; Module Name: " & mstrModuleName)
            Return strKeyValue
        Finally
            exForce = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function GetKeyValue(ByVal intCompanyUnkId As Integer, ByVal strKeyNames() As String, Optional ByVal xDataOperation As clsDataOperation = Nothing) As Dictionary(Of String, String)
        Dim xDicKeyValue As New Dictionary(Of String, String)
        Dim dsList As DataSet
        Dim strQ As String = ""
        Dim strKeyValue As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            If strKeyNames.Length > 0 Then
                Dim strInQryParam As String = String.Empty
                strInQryParam = String.Join("','", strKeyNames.ToArray())
                If strInQryParam.Trim.Length > 0 Then strInQryParam = "'" & strInQryParam & "'"

                strQ = "SELECT " & _
                       "     key_name " & _
                       "    ,key_value " & _
                       "FROM hrmsConfiguration..cfconfiguration " & _
                       "WHERE key_name IN (" & strInQryParam & ") " & _
                       " AND key_value <> '' "

                If intCompanyUnkId > 0 Then
                    strQ &= " AND companyunkid =  @CompanyId "
                    objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)
                Else
                    strQ &= " AND companyunkid IS NULL "
                End If

                dsList = objDataOperation.ExecQuery(strQ, "KeyValue")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("KeyValue").Rows.Count > 0 Then
                    xDicKeyValue = dsList.Tables("KeyValue").AsEnumerable().ToDictionary(Function(x) x.Field(Of String)("key_name"), _
                                                                                         Function(y) y.Field(Of String)("key_value"))
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetKeyValue; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return xDicKeyValue
    End Function

    'S.SANDEEP |26-APR-2019| -- END

   

    Public Function IsKeyExist(ByVal intCompanyUnkId As Integer, ByVal strKeyName As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet
        Dim strQ As String = ""

        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try


            strQ = "SELECT key_name, " & _
                        "key_value, " & _
                        "CAST (0 AS BIT) AS isNew " & _
                    "FROM hrmsConfiguration..cfconfiguration " & _
                    "WHERE key_name = @key_name "

            If intCompanyUnkId > 0 Then
                strQ &= " AND companyunkid =  @CompanyId "
                objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)
            Else
                strQ &= " AND companyunkid IS NULL "
            End If

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - store encrypted password.
            objDataOperation.ClearParameters()
            'Sohail (14 Mar 2019) -- End
            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)
            objDataOperation.AddParameter("@key_name", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, strKeyName)

            dsList = objDataOperation.ExecQuery(strQ, "KeyValue")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("KeyValue").Rows.Count > 0

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "IsKeyExist", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function
    'Sohail (01 May 2013) -- End

    'Pinkal (18-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public Sub GetData(ByVal intCompanyID As Integer, ByVal mstrKeyName As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            'Pinkal (01-Feb-2021) -- Start
            'Enhancement NMB - Working Training Enhancements for New UI.

            strQ = "SELECT  ISNULL(configunkid,0) configunkid " & _
                      " FROM hrmsconfiguration..cfconfiguration " & _
                      " WHERE companyunkid = @companyunkid " & _
                      " AND UPPER(key_name) = @keyname"

            'Pinkal (01-Feb-2021) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyID.ToString)
            objDataOperation.AddParameter("@keyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrKeyName.Trim.ToUpper)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintConfigOptionID = 0
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintConfigOptionID = CInt(dtRow.Item("configunkid"))
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Pinkal (18-Apr-2013) -- End



    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

    Public Sub IsValue_Changed(ByVal iKeyName As String, ByVal iCompanyName As String, ByRef iValue As String, Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objData As clsDataOperation = Nothing
        Try
            If objDoOperation Is Nothing Then
                objData = New clsDataOperation
            Else
                objData = objDoOperation
            End If
            objData.ClearParameters()

            StrQ = "SELECT ISNULL(key_value,'') AS iValue FROM hrmsConfiguration..cfconfiguration WHERE UPPER(key_name) = '" & iKeyName.ToUpper & "' AND companyunkid = '" & iCompanyName & "' "
            dsList = objData.ExecQuery(StrQ, "List")
            If dsList.Tables(0).Rows.Count > 0 Then
                iValue = dsList.Tables(0).Rows(0).Item("iValue")
            Else
                iValue = ""
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValue_Changed; Module Name: " & mstrModuleName)
        Finally
            If objDoOperation Is Nothing Then objData = Nothing
        End Try
    End Sub
    'Pinkal (18-Aug-2018) -- End


    'Pinkal (30-Nov-2013) -- Start
    'Enhancement : Oman Changes

    Public Function GetOTOrder(ByVal mblnWorkingDays As Boolean, ByVal mblnweekend As Boolean, ByVal mblnDayoff As Boolean, ByVal mblnHoliday As Boolean) As Dictionary(Of Integer, String)
        Dim arOT1Order() As String = Nothing
        Dim arOT2Order() As String = Nothing
        Dim arOT3Order() As String = Nothing
        Dim arOT4Order() As String = Nothing
        Dim mintOT1No As Integer = 0
        Dim mintOT2No As Integer = 0
        Dim mintOT3No As Integer = 0
        Dim mintOT4No As Integer = 0
        Dim dctOTOrder As New Dictionary(Of Integer, String)
        Try

            arOT1Order = ConfigParameter._Object._OT1Order.Split("|")
            arOT2Order = ConfigParameter._Object._OT2Order.Split("|")
            arOT3Order = ConfigParameter._Object._OT3Order.Split("|")
            arOT4Order = ConfigParameter._Object._OT4Order.Split("|")

            If mblnWorkingDays Then
                mintOT1No = CInt(arOT1Order(0))
                mintOT2No = CInt(arOT2Order(0))
                mintOT3No = CInt(arOT3Order(0))
                mintOT4No = CInt(arOT4Order(0))

            ElseIf mblnweekend Then
                mintOT1No = CInt(arOT1Order(1))
                mintOT2No = CInt(arOT2Order(1))
                mintOT3No = CInt(arOT3Order(1))
                mintOT4No = CInt(arOT4Order(1))

            ElseIf mblnDayoff Then
                mintOT1No = CInt(arOT1Order(2))
                mintOT2No = CInt(arOT2Order(2))
                mintOT3No = CInt(arOT3Order(2))
                mintOT4No = CInt(arOT4Order(2))

            ElseIf mblnHoliday Then
                mintOT1No = CInt(arOT1Order(3))
                mintOT2No = CInt(arOT2Order(3))
                mintOT3No = CInt(arOT3Order(3))
                mintOT4No = CInt(arOT4Order(3))

            End If

            dctOTOrder.Add(mintOT1No, "1")  '1,2,3,4 ARE OT1,OT2,OT3,OT4
            dctOTOrder.Add(mintOT2No, "2")
            dctOTOrder.Add(mintOT3No, "3")
            dctOTOrder.Add(mintOT4No, "4")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOTOrder", mstrModuleName)
        Finally
            Array.Clear(arOT1Order, 0, arOT1Order.Length - 1)
            Array.Clear(arOT2Order, 0, arOT2Order.Length - 1)
            Array.Clear(arOT3Order, 0, arOT3Order.Length - 1)
            Array.Clear(arOT4Order, 0, arOT4Order.Length - 1)
            arOT1Order = Nothing
            arOT2Order = Nothing
            arOT3Order = Nothing
            arOT4Order = Nothing
        End Try
        Return dctOTOrder
    End Function

    'Pinkal (30-Nov-2013) -- End

    'S.SANDEEP [ 27 AUG 2014 ] -- START
    Public Sub Update_Retirement_Date(ByVal iValue As Integer, _
                                      ByVal iUpdateMode As Integer, _
                                      ByVal iCompanyId As Integer, _
                                      ByVal iDataOpr As clsDataOperation)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   " 'UPDATE ' + database_name + '..hremployee_master SET termination_to_date = CASE WHEN birthdate IS NULL THEN DATEADD(YEAR,' + CAST(" & iValue & " AS NVARCHAR(MAX)) + ',appointeddate) ELSE DATEADD(YEAR,' + CAST(" & iValue & " AS NVARCHAR(MAX)) + ',birthdate) END WHERE gender = " & iUpdateMode & " ' AS QRY " & _
                   "FROM cffinancial_year_tran " & _
                   " JOIN sys.databases ON databases.name = cffinancial_year_tran.database_name " & _
                   "WHERE cffinancial_year_tran.companyunkid = " & iCompanyId & " "

            dsList = iDataOpr.ExecQuery(StrQ, "List")

            If iDataOpr.ErrorMessage <> "" Then
                Throw New Exception(iDataOpr.ErrorNumber & " : " & iDataOpr.ErrorMessage)
            End If

            For Each iRow As DataRow In dsList.Tables(0).Rows
                StrQ = iRow.Item("QRY").ToString.Trim
                iDataOpr.ExecNonQuery(StrQ)
                If iDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(iDataOpr.ErrorNumber & " : " & iDataOpr.ErrorMessage)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_Retirement_Date", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 27 AUG 2014 ] -- END

#End Region

    'TYPE : EMPLOYEMENT CONTRACT PROCESS
#Region " Other Properties "
    Private mstrReviewerInfo As String
    Public Property _ReviewerInfo() As String
        Get
            Return mstrReviewerInfo
        End Get
        Set(ByVal strReviewerInfo As String)
            mstrReviewerInfo = strReviewerInfo
            setParamVal("ReviewerInfo", mstrReviewerInfo)
        End Set
    End Property

    Private mblnIsAllowFinalSave As Boolean = False
    Public Property _IsAllowFinalSave() As Boolean
        Get
            Return mblnIsAllowFinalSave
        End Get
        Set(ByVal blnIsAllowFinalSave As Boolean)
            mblnIsAllowFinalSave = blnIsAllowFinalSave
            setParamVal("ISALLOWFINALSAVE", mblnIsAllowFinalSave)
        End Set
    End Property

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : 
    Private mblnBSC_ByEmployee As Boolean = True
    Public Property _IsBSC_ByEmployee() As Boolean
        Get
            Return mblnBSC_ByEmployee
        End Get
        Set(ByVal blnBSC_ByEmployee As Boolean)
            mblnBSC_ByEmployee = blnBSC_ByEmployee
            setParamVal("BSC_ByEmployee", mblnBSC_ByEmployee)
        End Set
    End Property

    Private mblnIsBSCObjectiveSaved As Boolean = False
    Public Property _IsBSCObjectiveSaved() As Boolean
        Get
            Return mblnIsBSCObjectiveSaved
        End Get
        Set(ByVal blnIsBSCObjectiveSaved As Boolean)
            mblnIsBSCObjectiveSaved = blnIsBSCObjectiveSaved
            setParamVal("IsBSCObjectiveSaved", mblnIsBSCObjectiveSaved)
        End Set
    End Property

    Private mblnCompanyNeedReviewer As Boolean = True
    Public Property _IsCompanyNeedReviewer() As Boolean
        Get
            Return mblnCompanyNeedReviewer
        End Get
        Set(ByVal blnCompanyNeedReviewer As Boolean)
            mblnCompanyNeedReviewer = blnCompanyNeedReviewer
            Call setParamVal("CompanyNeedReviewer", mblnCompanyNeedReviewer)
        End Set
    End Property
    'S.SANDEEP [ 23 JAN 2012 ] -- END

    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    'Private mintUserAccessModeSetting As Integer = enAllocation.JOBS

    'S.SANDEEP [ 31 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private mintUserAccessModeSetting As Integer = enAllocation.DEPARTMENT
    Private mstrUserAccessModeSetting As String = enAllocation.DEPARTMENT
    'S.SANDEEP [ 31 MAY 2012 ] -- END

    'S.SANDEEP [ 16 MAY 2012 ] -- END


    'S.SANDEEP [ 31 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Property _UserAccessModeSetting() As Integer
    '    Get
    '        Return mintUserAccessModeSetting
    '    End Get
    '    Set(ByVal intUserAccessModeSetting As Integer)
    '        mintUserAccessModeSetting = intUserAccessModeSetting
    '        setParamVal("UserAccessModeSetting", mintUserAccessModeSetting)
    '    End Set
    'End Property

    Public Property _UserAccessModeSetting() As String
        Get
            Return mstrUserAccessModeSetting
        End Get
        Set(ByVal strUserAccessModeSetting As String)
            mstrUserAccessModeSetting = strUserAccessModeSetting
            setParamVal("UserAccessModeSetting", mstrUserAccessModeSetting)
        End Set
    End Property
    'S.SANDEEP [ 31 MAY 2012 ] -- END

   
    'S.SANDEEP [ 04 FEB 2012 ] -- END


    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'Nilay (03-Oct-2016) -- Start
    'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
    'Private mintConfirmationMonth As Integer = -1
    Private mintConfirmationMonth As Integer = 1
    'Nilay (03-Oct-2016) -- End
    Public Property _ConfirmationMonth() As Integer
        Get
            Return mintConfirmationMonth
        End Get
        Set(ByVal intConfirmationMonth As Integer)
            mintConfirmationMonth = intConfirmationMonth
            setParamVal("ConfirmationMonth", mintConfirmationMonth)
        End Set
    End Property
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private mintForcastedViewSetting As Integer = enForcastedSetting.BY_MONTH
    Public Property _Forcasted_ViewSetting() As Integer
        Get
            Return mintForcastedViewSetting
        End Get
        Set(ByVal intForcastedViewSetting As Integer)
            mintForcastedViewSetting = intForcastedViewSetting
            setParamVal("ForcastedViewSetting", mintForcastedViewSetting)
        End Set
    End Property
    Private mintForcastedValue As Integer = 6
    Public Property _ForcastedValue() As Integer
        Get
            Return mintForcastedValue
        End Get
        Set(ByVal intForcastedValue As Integer)
            mintForcastedValue = intForcastedValue
            setParamVal("ForcastedValue", mintForcastedValue)
        End Set
    End Property
    'S.SANDEEP [ 16 MAY 2012 ] -- END

    'S.SANDEEP [ 24 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private mintForcastedEOCViewSetting As Integer = enForcastedSetting.BY_MONTH
    Private mintForcastedEOCValue As Integer = 6
    Public Property _ForcastedEOC_ViewSetting() As Integer
        Get
            Return mintForcastedEOCViewSetting
        End Get
        Set(ByVal intForcastedEOCViewSetting As Integer)
            mintForcastedEOCViewSetting = intForcastedEOCViewSetting
            setParamVal("ForcastedEOCViewSetting", mintForcastedEOCViewSetting)
        End Set
    End Property
    Public Property _ForcastedEOCValue() As Integer
        Get
            Return mintForcastedEOCValue
        End Get
        Set(ByVal intForcastedEOCValue As Integer)
            mintForcastedEOCValue = intForcastedEOCValue
            setParamVal("ForcastedEOCValue", mintForcastedEOCValue)
        End Set
    End Property
    'S.SANDEEP [ 24 MAY 2012 ] -- END


    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

    Private mintForcastedELCViewSetting As Integer = enForcastedSetting.BY_DAY
    Private mintForcastedELCValue As Integer = 30
    Public Property _ForcastedELCViewSetting() As Integer
        Get
            Return mintForcastedELCViewSetting
        End Get
        Set(ByVal Value As Integer)
            mintForcastedELCViewSetting = Value
            setParamVal("ForcastedELCViewSetting", mintForcastedELCViewSetting)
        End Set
    End Property
    Public Property _ForcastedELCValue() As Integer
        Get
            Return mintForcastedELCValue
        End Get
        Set(ByVal Value As Integer)
            mintForcastedELCValue = Value
            setParamVal("ForcastedELCValue", mintForcastedELCValue)
        End Set
    End Property

    'Pinkal (03-Nov-2014) -- End



    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrCheckedFields As String = String.Empty
    Public Property _Checked_Fields() As String
        Get
            Return mstrCheckedFields
        End Get
        Set(ByVal value As String)
            mstrCheckedFields = value
            setParamVal("Checked_Columns", mstrCheckedFields)
        End Set
    End Property
    'S.SANDEEP [ 18 AUG 2012 ] -- END


    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrNtf_EDate_Users As String = String.Empty
    Private mstrNtf_Alloc_Users As String = String.Empty
    Private mstrNtf_EData_Users As String = String.Empty

    Public Property _Notify_Dates() As String
        Get
            Return mstrNtf_EDate_Users
        End Get
        Set(ByVal strNtf_EDate_Users As String)
            mstrNtf_EDate_Users = strNtf_EDate_Users
            setParamVal("Notify_Dates", mstrNtf_EDate_Users)
        End Set
    End Property

    Public Property _Notify_Allocation() As String
        Get
            Return mstrNtf_Alloc_Users
        End Get
        Set(ByVal strNtf_Alloc_Users As String)
            mstrNtf_Alloc_Users = strNtf_Alloc_Users
            setParamVal("Notify_Allocation", mstrNtf_Alloc_Users)
        End Set
    End Property

    Public Property _Notify_EmplData() As String
        Get
            Return mstrNtf_EData_Users
        End Get
        Set(ByVal strNtf_EData_Users As String)
            mstrNtf_EData_Users = strNtf_EData_Users
            setParamVal("Notify_EmplData", mstrNtf_EData_Users)
        End Set
    End Property
    'S.SANDEEP [ 18 SEP 2012 ] -- END

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrNtf_Bank_Users As String = String.Empty
    Public Property _Notify_Bank_Users() As String
        Get
            Return mstrNtf_Bank_Users
        End Get
        Set(ByVal value As String)
            mstrNtf_Bank_Users = value
            setParamVal("Notify_Bank", mstrNtf_Bank_Users)
        End Set
    End Property
    'S.SANDEEP [ 03 OCT 2012 ] -- END

    'Sohail (09 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrNtf_Payroll_Users As String = String.Empty
    Public Property _Notify_Payroll_Users() As String
        Get
            Return mstrNtf_Payroll_Users
        End Get
        Set(ByVal value As String)
            mstrNtf_Payroll_Users = value
            setParamVal("Notify_Payroll", mstrNtf_Payroll_Users)
        End Set
    End Property
    'Sohail (09 Mar 2013) -- End

    'Nilay (17-Aug-2016) -- Start
    'ENHANCEMENT : Add Loan Savings Notification Settings
    Private mstrNtf_LoanAdvance_Users As String = String.Empty
    Public Property _Notify_LoanAdvance_Users() As String
        Get
            Return mstrNtf_LoanAdvance_Users
        End Get
        Set(ByVal value As String)
            mstrNtf_LoanAdvance_Users = value
            setParamVal("Notify_LoanAdvance", mstrNtf_LoanAdvance_Users)
        End Set
    End Property
    'Nilay (17-Aug-2016) -- END

    'Sohail (07 Jun 2016) -- Start
    'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
    Private mdecNtf_Budget_FundSourcePercentage As Decimal = 0
    Public Property _Notify_Budget_FundSourcePercentage() As Decimal
        Get
            Return mdecNtf_Budget_FundSourcePercentage
        End Get
        Set(ByVal value As Decimal)
            mdecNtf_Budget_FundSourcePercentage = value
            setParamVal("Notify_Budget_FundSourcePercentage", mdecNtf_Budget_FundSourcePercentage)
        End Set
    End Property

    Private mdecNtf_Budget_FundActivityPercentage As Decimal = 0
    Public Property _Notify_Budget_FundActivityPercentage() As Decimal
        Get
            Return mdecNtf_Budget_FundActivityPercentage
        End Get
        Set(ByVal value As Decimal)
            mdecNtf_Budget_FundActivityPercentage = value
            setParamVal("Notify_Budget_FundActivityPercentage", mdecNtf_Budget_FundActivityPercentage)
        End Set
    End Property

    Private mstrNtf_Budget_Users As String = String.Empty
    Public Property _Notify_Budget_Users() As String
        Get
            Return mstrNtf_Budget_Users
        End Get
        Set(ByVal value As String)
            mstrNtf_Budget_Users = value
            setParamVal("Notify_Budget", mstrNtf_Budget_Users)
        End Set
    End Property
    'Sohail (07 Jun 2016) -- End


    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

    Private mstrNtf_IssuedLeave_Users As String = String.Empty
    Public Property _Notify_IssuedLeave_Users() As String
        Get
            Return mstrNtf_IssuedLeave_Users
        End Get
        Set(ByVal value As String)
            mstrNtf_IssuedLeave_Users = value
            setParamVal("Notify_IssuedLeave", mstrNtf_IssuedLeave_Users)
        End Set
    End Property

    'Pinkal (03-Nov-2014) -- End


    'S.SANDEEP [ 21 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAllocation_Hierarchy As String = enAllocation.BRANCH & "|" & enAllocation.DEPARTMENT_GROUP & "|" & enAllocation.DEPARTMENT & _
                                                 enAllocation.SECTION_GROUP & "|" & enAllocation.SECTION & "|" & enAllocation.UNIT_GROUP & "|" & _
                                                 enAllocation.UNIT & "|" & enAllocation.TEAM
    Public Property _Allocation_Hierarchy() As String
        Get
            Return mstrAllocation_Hierarchy
        End Get
        Set(ByVal srtAllocation_Hierarchy As String)
            mstrAllocation_Hierarchy = srtAllocation_Hierarchy
            setParamVal("Allocation_Hierarchy", mstrAllocation_Hierarchy)
        End Set
    End Property

    Private mblnIsAllocation_Hierarchy_Set As Boolean = False
    Public Property _IsAllocation_Hierarchy_Set() As Boolean
        Get
            Return mblnIsAllocation_Hierarchy_Set
        End Get
        Set(ByVal value As Boolean)
            mblnIsAllocation_Hierarchy_Set = value
            setParamVal("IsAllocation_Hierarchy_Set", mblnIsAllocation_Hierarchy_Set)
        End Set
    End Property

    Private mintAllocationIdx As Integer = 0
    Public Property _AllocationIdx() As Integer
        Get
            Return mintAllocationIdx
        End Get
        Set(ByVal value As Integer)
            mintAllocationIdx = value
            setParamVal("AllocationIdx", mintAllocationIdx)
        End Set
    End Property
    'S.SANDEEP [ 21 SEP 2012 ] -- END


    'Pinkal (01-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Private mstrCompanyDomain As String = String.Empty
    Public Property _CompanyDomain() As String
        Get
            Return mstrCompanyDomain
        End Get
        Set(ByVal value As String)
            mstrCompanyDomain = value
            setParamVal("CompanyDomain", mstrCompanyDomain)
        End Set
    End Property

    Private mintDisplayNamesetting As Integer = 0
    Public Property _DisplayNameSetting() As Integer
        Get
            Return mintDisplayNamesetting
        End Get
        Set(ByVal value As Integer)
            mintDisplayNamesetting = value
            setParamVal("DisplayNameSetting", mintDisplayNamesetting)
        End Set
    End Property

    'Pinkal (01-Dec-2012) -- End

    'S.SANDEEP [ 01 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsPasswordAutoGenerated As Boolean = False
    Public Property _IsPasswordAutoGenerated() As Boolean
        Get
            Return mblnIsPasswordAutoGenerated
        End Get
        Set(ByVal value As Boolean)
            mblnIsPasswordAutoGenerated = value
            setParamVal("IsPasswordAutoGenerated", mblnIsPasswordAutoGenerated)
        End Set
    End Property
    'S.SANDEEP [ 01 DEC 2012 ] -- END

    'S.SANDEEP [ 14 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnSendDetailToEmployee As Boolean = False
    Public Property _SendDetailToEmployee() As Boolean
        Get
            Return mblnSendDetailToEmployee
        End Get
        Set(ByVal value As Boolean)
            mblnSendDetailToEmployee = value
            setParamVal("SendDetailToEmployee", mblnSendDetailToEmployee)
        End Set
    End Property
    'S.SANDEEP [ 14 DEC 2012 ] -- END

    'Pinkal (12-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Private mblnAllowToviewPaysliponEss As Boolean = False
    Public Property _AllowToviewPaysliponEss() As Boolean
        Get
            Return mblnAllowToviewPaysliponEss
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToviewPaysliponEss = value
            setParamVal("AllowToviewPaysliponESS", mblnAllowToviewPaysliponEss)
        End Set
    End Property

    Private mintViewPayslipDaysBefore As Integer = 0
    Public Property _ViewPayslipDaysBefore() As Integer
        Get
            Return mintViewPayslipDaysBefore
        End Get
        Set(ByVal value As Integer)
            mintViewPayslipDaysBefore = value
            setParamVal("ViewPayslipDaysBefore", mintViewPayslipDaysBefore)
        End Set
    End Property
    'Pinkal (12-Mar-2013) -- End

    'Nilay (03-Nov-2016) -- Start
    'Enhancement : Option to Show / hide payslip on ESS
    Private mblnShowHidePayslipOnESS As Boolean = True
    Public Property _ShowHidePayslipOnESS() As Boolean
        Get
            Return mblnShowHidePayslipOnESS
        End Get
        Set(ByVal value As Boolean)
            mblnShowHidePayslipOnESS = value
            setParamVal("ShowHidePayslipOnESS", mblnShowHidePayslipOnESS)
        End Set
    End Property
    'Nilay (03-Nov-2016) -- End

    'Sohail (29 Mar 2017) -- Start
    'CCK Enhancement - 65.1 - Payslip to be accessible on ess only after payment authorisation. Provide setting on configuration as earlier agreed. 
    Private mblnDontShowPayslipOnESSIfPaymentNotAuthorized As Boolean = False
    Public Property _DontShowPayslipOnESSIfPaymentNotAuthorized() As Boolean
        Get
            Return mblnDontShowPayslipOnESSIfPaymentNotAuthorized
        End Get
        Set(ByVal value As Boolean)
            mblnDontShowPayslipOnESSIfPaymentNotAuthorized = value
            setParamVal("DontShowPayslipOnESSIfPaymentNotAuthorized", mblnDontShowPayslipOnESSIfPaymentNotAuthorized)
        End Set
    End Property
    'Sohail (29 Mar 2017) -- End

    'Sohail (15 May 2020) -- Start
    'NMB Enhancement # : Option Don't Show Payslip On ESS If Payment is Not Done on aruti configuration.
    Private mblnDontShowPayslipOnESSIfPaymentNotDone As Boolean = True
    Public Property _DontShowPayslipOnESSIfPaymentNotDone() As Boolean
        Get
            Return mblnDontShowPayslipOnESSIfPaymentNotDone
        End Get
        Set(ByVal value As Boolean)
            mblnDontShowPayslipOnESSIfPaymentNotDone = value
            setParamVal("DontShowPayslipOnESSIfPaymentNotDone", mblnDontShowPayslipOnESSIfPaymentNotDone)
        End Set
    End Property
    'Sohail (15 May 2020) -- End

    'Sohail (23 Jan 2017) -- Start
    'CCBRT Enhancement - 65.1 - Show Statutory Message on ESS Payslip.
    Private mstrStatutoryMessageOnPayslipOnESS As String = "For Internal Use Only"
    Public Property _StatutoryMessageOnPayslipOnESS() As String
        Get
            Return mstrStatutoryMessageOnPayslipOnESS
        End Get
        Set(ByVal value As String)
            mstrStatutoryMessageOnPayslipOnESS = value
            setParamVal("StatutoryMessageOnPayslipOnESS", mstrStatutoryMessageOnPayslipOnESS)
        End Set
    End Property
    'Sohail (23 Jan 2017) -- End

    'S.SANDEEP [ 09 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnAllowAssessorAssessment As Boolean = False
    Public Property _AllowAssessor_Before_Emp() As Boolean
        Get
            Return mblnAllowAssessorAssessment
        End Get
        Set(ByVal value As Boolean)
            mblnAllowAssessorAssessment = value
            setParamVal("AllowAssessor_Before_Emp", mblnAllowAssessorAssessment)
        End Set
    End Property
    'S.SANDEEP [ 09 AUG 2013 ] -- END

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintPerformanceComputation As Integer = 0
    Public Property _PerformanceComputation() As Integer
        Get
            Return mintPerformanceComputation
        End Get
        Set(ByVal value As Integer)
            mintPerformanceComputation = value
            setParamVal("PerformanceComputation", mintPerformanceComputation)
        End Set
    End Property

    Private mblnAvgEmpComputType As Boolean = False
    Private mblnAvgAseComputType As Boolean = False
    Private mblnAvgRevComputType As Boolean = False
    Private mintScrComputType As Integer = 0

    Public Property _WAvgEmpComputationType() As Boolean
        Get
            Return mblnAvgEmpComputType
        End Get
        Set(ByVal value As Boolean)
            mblnAvgEmpComputType = value
            setParamVal("WAvgEmpComputationType", mblnAvgEmpComputType)
        End Set
    End Property

    Public Property _WAvgAseComputType() As Boolean
        Get
            Return mblnAvgAseComputType
        End Get
        Set(ByVal value As Boolean)
            mblnAvgAseComputType = value
            setParamVal("WAvgAseComputType", mblnAvgAseComputType)
        End Set
    End Property

    Public Property _WAvgRevComputType() As Boolean
        Get
            Return mblnAvgRevComputType
        End Get
        Set(ByVal value As Boolean)
            mblnAvgRevComputType = value
            setParamVal("WAvgRevComputType", mblnAvgRevComputType)
        End Set
    End Property

    Public Property _WScrComputType() As Integer
        Get
            Return mintScrComputType
        End Get
        Set(ByVal value As Integer)
            mintScrComputType = value
            setParamVal("WScrComputType", mintScrComputType)
        End Set
    End Property
    
   

    Private mblnExcludeProbatedEmpOnPerformance As Boolean = False
    Public Property _ExcludeProbatedEmpOnPerformance() As Boolean
        Get
            Return mblnExcludeProbatedEmpOnPerformance
        End Get
        Set(ByVal value As Boolean)
            mblnExcludeProbatedEmpOnPerformance = value
            setParamVal("ExcludeProbatedEmpOnPerformance", mblnExcludeProbatedEmpOnPerformance)
        End Set
    End Property
    'S.SANDEEP [ 14 AUG 2013 ] -- END
  'S.SANDEEP [ 22 OCT 2013 ] -- START
    'ENHANCEMENT : ENHANCEMENT
    Private mblnConsiderItemWeightAsNumber As Boolean = False
    Public Property _ConsiderItemWeightAsNumber() As Boolean
        Get
            Return mblnConsiderItemWeightAsNumber
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderItemWeightAsNumber = value
            setParamVal("ConsiderItemWeightAsNumber", mblnConsiderItemWeightAsNumber)
        End Set
    End Property
    'S.SANDEEP [ 22 OCT 2013 ] -- END

    'Pinkal (21-Oct-2013) -- Start
    'Enhancement : Oman Changes


    Public Property _ASR_Leave1() As Integer
        Get
            Return mintASR_Leave1
        End Get
        Set(ByVal value As Integer)
            mintASR_Leave1 = value
            setParamVal("ASR_Leave1", mintASR_Leave1)
        End Set
    End Property

    Public Property _ASR_Leave2() As Integer
        Get
            Return mintASR_Leave2
        End Get
        Set(ByVal value As Integer)
            mintASR_Leave2 = value
            setParamVal("ASR_Leave2", mintASR_Leave2)
        End Set
    End Property

    Public Property _ASR_Leave3() As Integer
        Get
            Return mintASR_Leave3
        End Get
        Set(ByVal value As Integer)
            mintASR_Leave3 = value
            setParamVal("ASR_Leave3", mintASR_Leave3)
        End Set
    End Property

    Public Property _ASR_Leave4() As Integer
        Get
            Return mintASR_Leave4
        End Get
        Set(ByVal value As Integer)
            mintASR_Leave4 = value
            setParamVal("ASR_Leave4", mintASR_Leave4)
        End Set
    End Property

    Public Property _ASR_LvType() As String
        Get
            Return mstrSpecial_LeaveIds
        End Get
        Set(ByVal value As String)
            mstrSpecial_LeaveIds = value
            setParamVal("ASR_LvType", mstrSpecial_LeaveIds)
        End Set
    End Property

    'Pinkal (21-Oct-2013) -- End

    'Pinkal (1-Jul-2014) -- Start
    'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

    Public Property _ASR_Leave5() As Integer
        Get
            Return mintASR_Leave5
        End Get
        Set(ByVal value As Integer)
            mintASR_Leave5 = value
            setParamVal("ASR_Leave5", mintASR_Leave5)
        End Set
    End Property

    'Pinkal (1-Jul-2014) -- End

    'Pinkal (09-Jun-2015) -- Start
    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
    Public Property _EmpTimesheetSetting() As String
        Get
            Return mstrEmpTimesheetSetting
        End Get
        Set(ByVal value As String)
            mstrEmpTimesheetSetting = value
            setParamVal("EmpTimesheetSetting", mstrEmpTimesheetSetting)
        End Set
    End Property


    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
    'Public Property _SetDeviceInOutStatus() As Boolean
    '    Get
    '        Return mblnSetDeviceInOutStatus
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnSetDeviceInOutStatus = value
    '        setParamVal("SetDeviceInOutStatus", mblnSetDeviceInOutStatus)
    '    End Set
    'End Property
    'Pinkal (06-May-2016) -- End

    'Pinkal (09-Jun-2015) -- End




    'Pinkal (25-Feb-2015) -- Start
    'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].

    Public Property _ASR_ShowTotalHrs() As Boolean
        Get
            Return mblnASR_ShowTotalhrs
        End Get
        Set(ByVal value As Boolean)
            mblnASR_ShowTotalhrs = value
            setParamVal("ASR_ShowTotalHrs", mblnASR_ShowTotalhrs)
        End Set
    End Property

    'Pinkal (25-Feb-2015) -- End


    'S.SANDEEP [ 20 DEC 2013 ] -- START
    Private mblnFirstCheckInLastCheckOut As Boolean = False
    Public Property _FirstCheckInLastCheckOut() As Boolean
        Get
            Return mblnFirstCheckInLastCheckOut
        End Get
        Set(ByVal value As Boolean)
            mblnFirstCheckInLastCheckOut = value
            setParamVal("FirstCheckInLastCheckOut", mblnFirstCheckInLastCheckOut)
        End Set
    End Property
    'S.SANDEEP [ 20 DEC 2013 ] -- END

    'S.SANDEEP [ 04 FEB 2014 ] -- START
    '-- Claim & Request 
    Private mintClaimRequestVocNoType As Integer = 1
    Private mstrClaimRequestPrefix As String = ""
    Private mintNextClaimRequestVocNo As Integer = 1

    ''' <summary>
    ''' Indicate Claim Request No Type
    ''' It May be Automatic Or Number Type
    ''' </summary>
    Public Property _ClaimRequestVocNoType() As Integer
        Get
            Return mintClaimRequestVocNoType
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestVocNoType = value
            setParamVal("ClaimRequestVocNoType", mintClaimRequestVocNoType)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Claim Request Prefix Value
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _ClaimRequestPrefix() As String
        Get
            Return mstrClaimRequestPrefix
        End Get
        Set(ByVal value As String)
            mstrClaimRequestPrefix = value
            setParamVal("ClaimRequestPrefix", mstrClaimRequestPrefix)
        End Set
    End Property

    ''' <summary>
    ''' Indicate Claim Request Next No
    ''' This Works for Automatic Generated No
    ''' </summary>
    Public Property _NextClaimRequestVocNo() As Integer
        Get
            Return mintNextClaimRequestVocNo
        End Get
        Set(ByVal value As Integer)
            mintNextClaimRequestVocNo = value
            setParamVal("NextClaimRequestVocNo", mintNextClaimRequestVocNo)
        End Set
    End Property
    'S.SANDEEP [ 04 FEB 2014 ] -- END



    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False

    ''' <summary>
    ''' Indicates For Claim Request Payment Approval Run in simultaneously with Leave Approval
    ''' </summary>
    Public Property _PaymentApprovalwithLeaveApproval() As Boolean
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
            setParamVal("ClaimRequest_PaymentApprovalwithLeaveApproval", mblnPaymentApprovalwithLeaveApproval)
        End Set
    End Property

    'Pinkal (06-Mar-2014) -- End

    'S.SANDEEP [ 19 JUL 2014 ] -- START
    Private mstrHourAmtMapping As String = String.Empty
    Public Property _HourAmtMapping() As String
        Get
            Return mstrHourAmtMapping
        End Get
        Set(ByVal value As String)
            mstrHourAmtMapping = value
            setParamVal("HourAmtMapping", mstrHourAmtMapping)
        End Set
    End Property
    'S.SANDEEP [ 19 JUL 2014 ] -- END

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Private mstrCustomPayrollReportHeadsIds As String = String.Empty
    Public Property _CustomPayrollReportHeadsIds() As String
        Get
            Return mstrCustomPayrollReportHeadsIds
        End Get
        Set(ByVal value As String)
            mstrCustomPayrollReportHeadsIds = value
            setParamVal("CustomPayrollReportHeadsIds", mstrCustomPayrollReportHeadsIds)
        End Set
    End Property

    Private mstrCustomPayrollReportAllocIds As String = String.Empty
    Public Property _CustomPayrollReportAllocIds() As String
        Get
            Return mstrCustomPayrollReportAllocIds
        End Get
        Set(ByVal value As String)
            mstrCustomPayrollReportAllocIds = value
            setParamVal("CustomPayrollReportAllocIds", mstrCustomPayrollReportAllocIds)
        End Set
    End Property
    'S.SANDEEP [ 29 SEP 2014 ] -- END

    'Sohail (20 Apr 2015) -- Start
    'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
    Private mstrCustomPayrollReportTemplate2HeadsIds As String = String.Empty
    Public Property _CustomPayrollReportTemplate2HeadsIds() As String
        Get
            Return mstrCustomPayrollReportTemplate2HeadsIds
        End Get
        Set(ByVal value As String)
            mstrCustomPayrollReportTemplate2HeadsIds = value
            setParamVal("CustomPayrollReportTemplate2HeadsIds", mstrCustomPayrollReportTemplate2HeadsIds)
        End Set
    End Property

    Private mstrCustomPayrollReportTemplate2AllocIds As String = String.Empty
    Public Property _CustomPayrollReportTemplate2AllocIds() As String
        Get
            Return mstrCustomPayrollReportTemplate2AllocIds
        End Get
        Set(ByVal value As String)
            mstrCustomPayrollReportTemplate2AllocIds = value
            setParamVal("CustomPayrollReportTemplate2AllocIds", mstrCustomPayrollReportTemplate2AllocIds)
        End Set
    End Property
    'Sohail (20 Apr 2015) -- End

    'Sohail (09 Apr 2015) -- Start
    'CCK Enhancement - New report Detailed Salary Breakdown report.
    Private mstrDetailedSalaryBreakdownReportHeadsIds As String = String.Empty
    Public Property _DetailedSalaryBreakdownReportHeadsIds() As String
        Get
            Return mstrDetailedSalaryBreakdownReportHeadsIds
        End Get
        Set(ByVal value As String)
            mstrDetailedSalaryBreakdownReportHeadsIds = value
            setParamVal("DetailedSalaryBreakdownReportHeadsIds", mstrDetailedSalaryBreakdownReportHeadsIds)
        End Set
    End Property
    'Sohail (09 Apr 2015) -- End

    'Sohail (20 Feb 2017) -- Start
    'CCBRT Enhancement - 65.1 - New Report 'Detailed Salary Breakdown Report by Cost Center Group'.
    Private mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds As String = String.Empty
    Public Property _DetailedSalaryBreakdownReportByCCenterGroupHeadsIds() As String
        Get
            Return mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds
        End Get
        Set(ByVal value As String)
            mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds = value
            setParamVal("DetailedSalaryBreakdownReportByCCenterGroupHeadsIds", mstrDetailedSalaryBreakdownReportByCCenterGroupHeadsIds)
        End Set
    End Property
    'Sohail (20 Feb 2017) -- End

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Private mstrDeptTrainingNeedListColumnsIDs As String = String.Empty
    Public Property _DeptTrainingNeedListColumnsIDs() As String
        Get
            Return mstrDeptTrainingNeedListColumnsIDs
        End Get
        Set(ByVal value As String)
            mstrDeptTrainingNeedListColumnsIDs = value
            setParamVal("DeptTrainingNeedListColumnsIDs", mstrDeptTrainingNeedListColumnsIDs)
        End Set
    End Property
    'Sohail (01 Mar 2021) -- End

    'Sohail (29 May 2015) -- Start
    'Enhancement - Time Zone Minute Difference for Online Recruitment Vacancy closing date.
    Private mintTimeZoneMinuteDifference As Integer = 0
    Public Property _TimeZoneMinuteDifference() As Integer
        Get
            Return mintTimeZoneMinuteDifference
        End Get
        Set(ByVal value As Integer)
            mintTimeZoneMinuteDifference = value
            setParamVal("TimeZoneMinuteDifference", mintTimeZoneMinuteDifference)
        End Set
    End Property
    'Sohail (29 May 2015) -- End

    'Sohail (05 Dec 2016) -- Start
    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
    Private mstrAdminEmail As String = ""
    Public Property _AdminEmail() As String
        Get
            Return mstrAdminEmail
        End Get
        Set(ByVal value As String)
            mstrAdminEmail = value
            setParamVal("AdminEmail", mstrAdminEmail)
        End Set
    End Property

    Private mblnRecMiddleNameMandatory As Boolean = False
    Public Property _MiddleNameMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecMiddleNameMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecMiddleNameMandatory = value
            setParamVal("RecMiddleNameMandatory", mblnRecMiddleNameMandatory)
        End Set
    End Property

    Private mblnRecGenderMandatory As Boolean = True 'ticked by default
    Public Property _GenderMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecGenderMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecGenderMandatory = value
            setParamVal("RecGenderMandatory", mblnRecGenderMandatory)
        End Set
    End Property

    Private mblnRecBirthDayMandatory As Boolean = True 'ticked by default
    Public Property _BirthDateMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecBirthDayMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecBirthDayMandatory = value
            setParamVal("RecBirthDayMandatory", mblnRecBirthDayMandatory)
        End Set
    End Property

    Private mblnRecAddress1Mandatory As Boolean = True 'ticked by default
    Public Property _Address1MandatoryInRecruitment() As Boolean
        Get
            Return mblnRecAddress1Mandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecAddress1Mandatory = value
            setParamVal("RecAddress1Mandatory", mblnRecAddress1Mandatory)
        End Set
    End Property

    Private mblnRecAddress2Mandatory As Boolean = False
    Public Property _Address2MandatoryInRecruitment() As Boolean
        Get
            Return mblnRecAddress2Mandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecAddress2Mandatory = value
            setParamVal("RecAddress2Mandatory", mblnRecAddress2Mandatory)
        End Set
    End Property

    Private mblnRecCountryMandatory As Boolean = False
    Public Property _CountryMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecCountryMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecCountryMandatory = value
            setParamVal("RecCountryMandatory", mblnRecCountryMandatory)
        End Set
    End Property

    Private mblnRecStateMandatory As Boolean = False
    Public Property _StateMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecStateMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecStateMandatory = value
            setParamVal("RecStateMandatory", mblnRecStateMandatory)
        End Set
    End Property

    Private mblnRecCityMandatory As Boolean = False
    Public Property _CityMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecCityMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecCityMandatory = value
            setParamVal("RecCityMandatory", mblnRecCityMandatory)
        End Set
    End Property

    Private mblnRecPostCodeMandatory As Boolean = False
    Public Property _PostCodeMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecPostCodeMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecPostCodeMandatory = value
            setParamVal("RecPostCodeMandatory", mblnRecPostCodeMandatory)
        End Set
    End Property

    Private mblnRecMaritalStatusMandatory As Boolean = True 'ticked by default
    Public Property _MaritalStatusMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecMaritalStatusMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecMaritalStatusMandatory = value
            setParamVal("RecMaritalStatusMandatory", mblnRecMaritalStatusMandatory)
        End Set
    End Property

    Private mblnRecLanguage1Mandatory As Boolean = False
    Public Property _Language1MandatoryInRecruitment() As Boolean
        Get
            Return mblnRecLanguage1Mandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecLanguage1Mandatory = value
            setParamVal("RecLanguage1Mandatory", mblnRecLanguage1Mandatory)
        End Set
    End Property

    Private mblnRecNationalityMandatory As Boolean = True 'ticked by default
    Public Property _NationalityMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecNationalityMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecNationalityMandatory = value
            setParamVal("RecNationalityMandatory", mblnRecNationalityMandatory)
        End Set
    End Property

    Private mblnRecOneSkillMandatory As Boolean = False
    Public Property _OneSkillMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecOneSkillMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecOneSkillMandatory = value
            setParamVal("RecOneSkillMandatory", mblnRecOneSkillMandatory)
        End Set
    End Property

    Private mblnRecOneQualificationMandatory As Boolean = True 'ticked by default
    Public Property _OneQualificationMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecOneQualificationMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecOneQualificationMandatory = value
            setParamVal("RecOneQualificationMandatory", mblnRecOneQualificationMandatory)
        End Set
    End Property

    Private mblnRecOneJobExperienceMandatory As Boolean = False
    Public Property _OneJobExperienceMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecOneJobExperienceMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecOneJobExperienceMandatory = value
            setParamVal("RecOneJobExperienceMandatory", mblnRecOneJobExperienceMandatory)
        End Set
    End Property

    Private mblnRecOneReferenceMandatory As Boolean = False
    Public Property _OneReferenceMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecOneReferenceMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecOneReferenceMandatory = value
            setParamVal("RecOneReferenceMandatory", mblnRecOneReferenceMandatory)
        End Set
    End Property
    'Sohail (05 Dec 2016) -- End

    'Sohail (04 Jul 2019) -- Start
    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
    Private mblnRecOneCurriculamVitaeMandatory As Boolean = False
    Public Property _OneCurriculamVitaeMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecOneCurriculamVitaeMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecOneCurriculamVitaeMandatory = value
            setParamVal("RecOneCurriculamVitaeMandatory", mblnRecOneCurriculamVitaeMandatory)
        End Set
    End Property

    Private mblnRecOneCoverLetterMandatory As Boolean = False
    Public Property _OneCoverLetterMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecOneCoverLetterMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecOneCoverLetterMandatory = value
            setParamVal("RecOneCoverLetterMandatory", mblnRecOneCoverLetterMandatory)
        End Set
    End Property
    'Sohail (04 Jul 2019) -- End

    'Nilay (13 Apr 2017) -- Start
    'Enhancements: Settings for mandatory options in online recruitment
    Private mintRecApplicantQualificationSortBy As Integer = 0
    Public Property _ApplicantQualificationSortByInRecruitment() As Integer
        Get
            Return mintRecApplicantQualificationSortBy
        End Get
        Set(ByVal value As Integer)
            mintRecApplicantQualificationSortBy = value
            setParamVal("RecApplicantQualificationSortBy", mintRecApplicantQualificationSortBy)
        End Set
    End Property

    Private mblnRecRegionMandatory As Boolean = False
    Public Property _RegionMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecRegionMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecRegionMandatory = value
            setParamVal("RecRegionMandatory", mblnRecRegionMandatory)
        End Set
    End Property

    Private mblnRecHighestQualificationMandatory As Boolean = False
    Public Property _HighestQualificationMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecHighestQualificationMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecHighestQualificationMandatory = value
            setParamVal("RecHighestQualificationMandatory", mblnRecHighestQualificationMandatory)
        End Set
    End Property

    Private mblnRecEmployerMandatory As Boolean = False
    Public Property _EmployerMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecEmployerMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecEmployerMandatory = value
            setParamVal("RecEmployerMandatory", mblnRecEmployerMandatory)
        End Set
    End Property

    Private mblnRecMotherTongueMandatory As Boolean = False
    Public Property _MotherTongueMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecMotherTongueMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecMotherTongueMandatory = value
            setParamVal("RecMotherTongueMandatory", mblnRecMotherTongueMandatory)
        End Set
    End Property

    Private mblnRecCurrentSalaryMandatory As Boolean = False
    Public Property _CurrentSalaryMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecCurrentSalaryMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecCurrentSalaryMandatory = value
            setParamVal("RecCurrentSalaryMandatory", mblnRecCurrentSalaryMandatory)
        End Set
    End Property

    Private mblnRecExpectedSalaryMandatory As Boolean = False
    Public Property _ExpectedSalaryMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecExpectedSalaryMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecExpectedSalaryMandatory = value
            setParamVal("RecExpectedSalaryMandatory", mblnRecExpectedSalaryMandatory)
        End Set
    End Property

    Private mblnRecExpectedBenefitsMandatory As Boolean = False
    Public Property _ExpectedBenefitsMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecExpectedBenefitsMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecExpectedBenefitsMandatory = value
            setParamVal("RecExpectedBenefitsMandatory", mblnRecExpectedBenefitsMandatory)
        End Set
    End Property

    Private mblnRecNoticePeriodMandatory As Boolean = False
    Public Property _NoticePeriodMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecNoticePeriodMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecNoticePeriodMandatory = value
            setParamVal("RecNoticePeriodMandatory", mblnRecNoticePeriodMandatory)
        End Set
    End Property

    Private mblnRecEarliestPossibleStartDateMandatory As Boolean = False
    Public Property _EarliestPossibleStartDateMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecEarliestPossibleStartDateMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecEarliestPossibleStartDateMandatory = value
            setParamVal("RecEarliestPossibleStartDateMandatory", mblnRecEarliestPossibleStartDateMandatory)
        End Set
    End Property

    Private mblnRecCommentsMandatory As Boolean = False
    Public Property _CommentsMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecCommentsMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecCommentsMandatory = value
            setParamVal("RecCommentsMandatory", mblnRecCommentsMandatory)
        End Set
    End Property

    Private mblnRecVacancyFoundOutFromMandatory As Boolean = False
    Public Property _VacancyFoundOutFromMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecVacancyFoundOutFromMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecVacancyFoundOutFromMandatory = value
            setParamVal("RecVacancyFoundOutFromMandatory", mblnRecVacancyFoundOutFromMandatory)
        End Set
    End Property
    'Nilay (13 Apr 2017) -- End

    'Sohail (24 Jul 2017) -- Start
    'Enhancement - 69.1 - Settings for mandatory options in online recruitment.
    Private mblnRecCompanyNameJobHistoryMandatory As Boolean = True
    Public Property _CompanyNameJobHistoryMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecCompanyNameJobHistoryMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecCompanyNameJobHistoryMandatory = value
            setParamVal("RecCompanyNameJobHistoryMandatory", mblnRecCompanyNameJobHistoryMandatory)
        End Set
    End Property

    Private mblnRecPositionHeldMandatory As Boolean = False
    Public Property _PositionHeldMandatoryInRecruitment() As Boolean
        Get
            Return mblnRecPositionHeldMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecPositionHeldMandatory = value
            setParamVal("RecPositionHeldMandatory", mblnRecPositionHeldMandatory)
        End Set
    End Property
    'Sohail (24 Jul 2017) -- End

    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : option Exclude employees from payroll during Suspension period to prorate salary and flat rate heads on employee suspension will be given on configuration.
    Private mblnExcludeEmpFromPayrollDuringSuspension As Boolean = False
    Public Property _ExcludeEmpFromPayrollDuringSuspension() As Boolean
        Get
            Return mblnExcludeEmpFromPayrollDuringSuspension
        End Get
        Set(ByVal value As Boolean)
            mblnExcludeEmpFromPayrollDuringSuspension = value
            setParamVal("ExcludeEmpFromPayrollDuringSuspension", mblnExcludeEmpFromPayrollDuringSuspension)
        End Set
    End Property
    'Sohail (09 Oct 2019) -- End

    'Pinkal (07-OCT-2014) -- Start
    'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 
    Private mblnCountAttendanceNotLVIfLVapplied As Boolean = True

    Public Property _CountAttendanceNotLVIfLVapplied() As Boolean
        Get
            Return mblnCountAttendanceNotLVIfLVapplied
        End Get
        Set(ByVal value As Boolean)
            mblnCountAttendanceNotLVIfLVapplied = value
            setParamVal("CountAttendanceNotLVIfLVapplied", mblnCountAttendanceNotLVIfLVapplied)
        End Set
    End Property

    'Pinkal (07-OCT-2014) -- End

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private mintCascadingTypeId As Integer = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT
    Public Property _CascadingTypeId() As Integer
        Get
            Return mintCascadingTypeId
        End Get
        Set(ByVal value As Integer)
            mintCascadingTypeId = value
            setParamVal("CascadingTypeId", mintCascadingTypeId)
        End Set
    End Property

    Private mintScoringOptionId As Integer = enScoringOption.SC_WEIGHTED_BASED
    Public Property _ScoringOptionId() As Integer
        Get
            Return mintScoringOptionId
        End Get
        Set(ByVal value As Integer)
            mintScoringOptionId = value
            setParamVal("ScoringOptionId", mintScoringOptionId)
        End Set
    End Property

    'S.SANDEEP |04-JAN-2021| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Private mintCmptScoringOptionId As Integer = enScoringOption.SC_WEIGHTED_BASED
    Public Property _CmptScoringOptionId() As Integer
        Get
            Return mintCmptScoringOptionId
        End Get
        Set(ByVal value As Integer)
            mintCmptScoringOptionId = value
            setParamVal("CmptScoringOptionId", mintCmptScoringOptionId)
        End Set
    End Property
    'S.SANDEEP |04-JAN-2021| -- END

    Private mblnAllowPeriodicReview As Boolean = False
    Public Property _AllowPeriodicReview() As Boolean
        Get
            Return mblnAllowPeriodicReview
        End Get
        Set(ByVal value As Boolean)
            mblnAllowPeriodicReview = value
            setParamVal("AllowPeriodicReview", mblnAllowPeriodicReview)
        End Set
    End Property

    Private mblnFollowEmployeeHierarchy As Boolean = False
    Public Property _FollowEmployeeHierarchy() As Boolean
        Get
            Return mblnFollowEmployeeHierarchy
        End Get
        Set(ByVal value As Boolean)
            mblnFollowEmployeeHierarchy = value
            setParamVal("FollowEmployeeHierarchy", mblnFollowEmployeeHierarchy)
        End Set
    End Property

    Private mstrAssessmentInstructions As String = String.Empty
    Public Property _Assessment_Instructions() As String
        Get
            Return mstrAssessmentInstructions
        End Get
        Set(ByVal value As String)
            mstrAssessmentInstructions = value
            setParamVal("Assessment_Instructions", mstrAssessmentInstructions)
        End Set
    End Property

    'S.SANDEEP |09-FEB-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Period Changes
    Private mstrCompetenciesAssessInstructions As String = String.Empty
    Public Property _CompetenciesAssess_Instructions() As String
        Get
            Return mstrCompetenciesAssessInstructions
        End Get
        Set(ByVal value As String)
            mstrCompetenciesAssessInstructions = value
            setParamVal("CompetenciesAssessInstructions", mstrCompetenciesAssessInstructions)
        End Set
    End Property
    'S.SANDEEP |09-FEB-2021| -- END

    Private mstrEvaluationOrder As String = _
        enEvaluationOrder.PE_BSC_SECTION & "|" & enEvaluationOrder.PE_COMPETENCY_SECTION & "|" & enEvaluationOrder.PE_CUSTOM_SECTION
    Public Property _Perf_EvaluationOrder() As String
        Get
            Return mstrEvaluationOrder
        End Get
        Set(ByVal value As String)
            mstrEvaluationOrder = value
            setParamVal("Perf_EvaluationOrder", mstrEvaluationOrder)
        End Set
    End Property

    Private mstrViewTitles_InPlanning As String = String.Empty
    Public Property _ViewTitles_InPlanning() As String
        Get
            Return mstrViewTitles_InPlanning
        End Get
        Set(ByVal value As String)
            mstrViewTitles_InPlanning = value
            setParamVal("ViewTitles_InPlanning", mstrViewTitles_InPlanning)
        End Set
    End Property

    Private mstrViewTitles_InEvaluation As String = String.Empty
    Public Property _ViewTitles_InEvaluation() As String
        Get
            Return mstrViewTitles_InEvaluation
        End Get
        Set(ByVal value As String)
            mstrViewTitles_InEvaluation = value
            setParamVal("ViewTitles_InEvaluation", mstrViewTitles_InEvaluation)
        End Set
    End Property

    Private mstrColWidth_InPlanning As String = String.Empty
    Public Property _ColWidth_InPlanning() As String
        Get
            Return mstrColWidth_InPlanning
        End Get
        Set(ByVal value As String)
            mstrColWidth_InPlanning = value
            setParamVal("ColWidth_InPlanning", mstrColWidth_InPlanning)
        End Set
    End Property

    Private mstrColWidth_InEvaluation As String = String.Empty
    Public Property _ColWidth_InEvaluation() As String
        Get
            Return mstrColWidth_InEvaluation
        End Get
        Set(ByVal value As String)
            mstrColWidth_InEvaluation = value
            setParamVal("ColWidth_InEvaluation", mstrColWidth_InEvaluation)
        End Set
    End Property
    'S.SANDEEP [ 05 NOV 2014 ] -- END

    'S.SANDEEP [ 01 JAN 2015 ] -- START
    Private mintAssessmentReportTemplateId As Integer = 0
    Public Property _AssessmentReportTemplateId() As Integer
        Get
            Return mintAssessmentReportTemplateId
        End Get
        Set(ByVal value As Integer)
            mintAssessmentReportTemplateId = value
            setParamVal("AssessmentReportTemplateId", mintAssessmentReportTemplateId)
        End Set
    End Property
    'S.SANDEEP [ 01 JAN 2015 ] -- END

    'S.SANDEEP [29 JAN 2015] -- START
    Private mblnSelfAssignCompetencies As Boolean = False
    Public Property _Self_Assign_Competencies() As Boolean
        Get
            Return mblnSelfAssignCompetencies
        End Get
        Set(ByVal value As Boolean)
            mblnSelfAssignCompetencies = value
            setParamVal("SelfAssignCompetencies", mblnSelfAssignCompetencies)
        End Set
    End Property
    'S.SANDEEP [29 JAN 2015] -- END

    'S.SANDEEP [14 MAR 2015] -- START
    Private mintProbationMonth As Integer = 1 'Nilay (03-Oct-2016)
    Public Property _ProbationMonth() As Integer
        Get
            Return mintProbationMonth
        End Get
        Set(ByVal value As Integer)
            mintProbationMonth = value
            setParamVal("ProbationMonth", mintProbationMonth)
        End Set
    End Property
    'S.SANDEEP [14 MAR 2015] -- END

    'S.SANDEEP [01 DEC 2015] -- START
    Private mblnSkipApprovalFlowInPlanning As Boolean = False
    Public Property _SkipApprovalFlowInPlanning() As Boolean
        Get
            Return mblnSkipApprovalFlowInPlanning
        End Get
        Set(ByVal value As Boolean)
            mblnSkipApprovalFlowInPlanning = value
            setParamVal("SkipApprovalFlowInPlanning", mblnSkipApprovalFlowInPlanning)
        End Set
    End Property
    'S.SANDEEP [01 DEC 2015] -- END


    'S.SANDEEP [23 DEC 2015] -- START
    Private mblnOnlyOneItemPerCompetencyCategory As Boolean = False
    Public Property _OnlyOneItemPerCompetencyCategory() As Boolean
        Get
            Return mblnOnlyOneItemPerCompetencyCategory
        End Get
        Set(ByVal value As Boolean)
            mblnOnlyOneItemPerCompetencyCategory = value
            setParamVal("OnlyOneItemPerCompetencyCategory", mblnOnlyOneItemPerCompetencyCategory)
        End Set
    End Property

    ' ''Private mblnIncludeCustomItemInPlanning As Boolean = False
    ' ''Public Property _IncludeCustomItemInPlanning() As Boolean
    ' ''    Get
    ' ''        Return mblnIncludeCustomItemInPlanning
    ' ''    End Get
    ' ''    Set(ByVal value As Boolean)
    ' ''        mblnIncludeCustomItemInPlanning = value
    ' ''        setParamVal("IncludeCustomItemInPlanning", mblnIncludeCustomItemInPlanning)
    ' ''    End Set
    ' ''End Property
    'S.SANDEEP [23 DEC 2015] -- END

    'Nilay (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
    Private mintSalaryAnniversarySetting As Integer = 0
    Public Property _SalaryAnniversarySetting() As Integer
        Get
            Return mintSalaryAnniversarySetting
        End Get
        Set(ByVal value As Integer)
            mintSalaryAnniversarySetting = value
            setParamVal("SalaryAnniversarySetting", mintSalaryAnniversarySetting)
        End Set
    End Property

    Private mintSalaryAnniversaryMonthBy As Integer = 0
    Public Property _SalaryAnniversaryMonthBy() As Integer
        Get
            Return mintSalaryAnniversaryMonthBy
        End Get
        Set(ByVal value As Integer)
            mintSalaryAnniversaryMonthBy = value
            setParamVal("SalaryAnniversaryMonthBy", mintSalaryAnniversaryMonthBy)
        End Set
    End Property

    Private mblnIsAllowToClosePeriod As Boolean = False
    Public Property _IsAllowToClosePeriod() As Boolean
        Get
            Return mblnIsAllowToClosePeriod
        End Get
        Set(ByVal value As Boolean)
            mblnIsAllowToClosePeriod = value
            setParamVal("IsAllowToClosePeriod", mblnIsAllowToClosePeriod)
        End Set
    End Property
    'Nilay (27 Apr 2016) -- End

    'S.SANDEEP [24 MAY 2016] -- START
    Private mintDisciplineRefNoType As Integer = 1
    Private mstrDisciplineRefPrefix As String = ""
    Private mintNextDisciplineRefNo As Integer = 1
    Public Property _DisciplineRefNoType() As Integer
        Get
            Return mintDisciplineRefNoType
        End Get
        Set(ByVal value As Integer)
            mintDisciplineRefNoType = value
            setParamVal("DisciplineRefNoType", mintDisciplineRefNoType)
        End Set
    End Property
    Public Property _DisciplineRefPrefix() As String
        Get
            Return mstrDisciplineRefPrefix
        End Get
        Set(ByVal value As String)
            mstrDisciplineRefPrefix = value
            setParamVal("DisciplineRefPrefix", mstrDisciplineRefPrefix)
        End Set
    End Property
    Public Property _NextDisciplineRefNo() As Integer
        Get
            Return mintNextDisciplineRefNo
        End Get
        Set(ByVal value As Integer)
            mintNextDisciplineRefNo = value
            setParamVal("NextDisciplineRefNo", mintNextDisciplineRefNo)
        End Set
    End Property

    Private mblnPreventDisciplineChargeunlessEmployeeNotified As Boolean = False
    Private mblnPreventDisciplineChargeunlessEmployeeReceipt As Boolean = False
    Public Property _PreventDisciplineChargeunlessEmployeeNotified() As Boolean
        Get
            Return mblnPreventDisciplineChargeunlessEmployeeNotified
        End Get
        Set(ByVal value As Boolean)
            mblnPreventDisciplineChargeunlessEmployeeNotified = value
            setParamVal("PreventDisciplineChargeunlessEmployeeNotified", mblnPreventDisciplineChargeunlessEmployeeNotified)
        End Set
    End Property
    Public Property _PreventDisciplineChargeunlessEmployeeReceipt() As Boolean
        Get
            Return mblnPreventDisciplineChargeunlessEmployeeReceipt
        End Get
        Set(ByVal value As Boolean)
            mblnPreventDisciplineChargeunlessEmployeeReceipt = value
            setParamVal("PreventDisciplineChargeunlessEmployeeReceipt", mblnPreventDisciplineChargeunlessEmployeeReceipt)
        End Set
    End Property

    Private mstrFireDisciplineWarningNotification As String = ""
    Private mstrDisciplineProceedingNotification As String = ""
    Private mstrDisciplineChargeNotification As String = ""
    Public Property _FireDisciplineWarningNotification() As String
        Get
            Return mstrFireDisciplineWarningNotification
        End Get
        Set(ByVal value As String)
            mstrFireDisciplineWarningNotification = value
            setParamVal("FireDisciplineWarningNotification", mstrFireDisciplineWarningNotification)
        End Set
    End Property
    Public Property _DisciplineProceedingNotification() As String
        Get
            Return mstrDisciplineProceedingNotification
        End Get
        Set(ByVal value As String)
            mstrDisciplineProceedingNotification = value
            setParamVal("DisciplineProceedingNotification", mstrDisciplineProceedingNotification)
        End Set
    End Property
    Public Property _DisciplineChargeNotification() As String
        Get
            Return mstrDisciplineChargeNotification
        End Get
        Set(ByVal value As String)
            mstrDisciplineChargeNotification = value
            setParamVal("DisciplineChargeNotification", mstrDisciplineChargeNotification)
        End Set
    End Property
    'S.SANDEEP [24 MAY 2016] -- END


    'S.SANDEEP [21 SEP 2016] -- START
    'ENHANCEMENT : GIVING USER SELECTION ON NOTIFICATION CENTER
    Private mstrDisciplineNotifyWarningUserIds As String = ""
    Public Property _DisciplineNotifyWarningUserIds() As String
        Get
            Return mstrDisciplineNotifyWarningUserIds
        End Get
        Set(ByVal value As String)
            mstrDisciplineNotifyWarningUserIds = value
            setParamVal("DisciplineNotifyWarningUserIds", mstrDisciplineNotifyWarningUserIds)
        End Set
    End Property

    Private mstrDisciplineNotifyProceedingUserIds As String = ""
    Public Property _DisciplineNotifyProceedingUserIds() As String
        Get
            Return mstrDisciplineNotifyProceedingUserIds
        End Get
        Set(ByVal value As String)
            mstrDisciplineNotifyProceedingUserIds = value
            setParamVal("DisciplineNotifyProceedingUserIds", mstrDisciplineNotifyProceedingUserIds)
        End Set
    End Property

    Private mstrDisciplineNotifyChargeClosedUserIds As String = ""
    Public Property _DisciplineNotifyChargeClosedUserIds() As String
        Get
            Return mstrDisciplineNotifyChargeClosedUserIds
        End Get
        Set(ByVal value As String)
            mstrDisciplineNotifyChargeClosedUserIds = value
            setParamVal("DisciplineNotifyChargeClosedUserIds", mstrDisciplineNotifyChargeClosedUserIds)
        End Set
    End Property
    'S.SANDEEP [21 SEP 2016] -- END

    'S.SANDEEP |12-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : SPRINT-6
    Private mstrDisciplineNotifyChargePostedUserIds As String = ""
    Public Property _DisciplineNotifyChargePostedUserIds() As String
        Get
            Return mstrDisciplineNotifyChargePostedUserIds
        End Get
        Set(ByVal value As String)
            mstrDisciplineNotifyChargePostedUserIds = value
            setParamVal("DisciplineNotifyChargePostedUserIds", mstrDisciplineNotifyChargePostedUserIds)
        End Set
    End Property
    'S.SANDEEP |12-NOV-2020| -- END
    

    'S.SANDEEP [03 OCT 2016] -- START
    'ENHANCEMENT : VOLTAMP DAILY TIME SHEET CHANGES
    Private mintSelectedAllocationForDailyTimeSheetReport_Voltamp As Integer = 0
    Public Property _SelectedAllocationForDailyTimeSheetReport_Voltamp() As Integer
        Get
            Return mintSelectedAllocationForDailyTimeSheetReport_Voltamp
        End Get
        Set(ByVal value As Integer)
            mintSelectedAllocationForDailyTimeSheetReport_Voltamp = value
            setParamVal("SELECTEDALLOCATIONFORDAILYTIMESHEETREPORT_VOLTAMP", mintSelectedAllocationForDailyTimeSheetReport_Voltamp)
        End Set
    End Property

    Private mblnShowEmployeeStatusForDailyTimeSheetReport_Voltamp As Boolean = True
    Public Property _ShowEmployeeStatusForDailyTimeSheetReport_Voltamp() As Boolean
        Get
            Return mblnShowEmployeeStatusForDailyTimeSheetReport_Voltamp
        End Get
        Set(ByVal value As Boolean)
            mblnShowEmployeeStatusForDailyTimeSheetReport_Voltamp = value
            setParamVal("SHOWEMPLOYEESTATUSFORDAILYTIMESHEETREPORT_VOLTAMP", mblnShowEmployeeStatusForDailyTimeSheetReport_Voltamp)
        End Set
    End Property
    'S.SANDEEP [03 OCT 2016] -- END
    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mblnGoalsAccomplishedRequiresApproval As Boolean = False
    Public Property _GoalsAccomplishedRequiresApproval() As Boolean
        Get
            Return mblnGoalsAccomplishedRequiresApproval
        End Get
        Set(ByVal value As Boolean)
            mblnGoalsAccomplishedRequiresApproval = value
            setParamVal("GoalsAccomplishedRequiresApproval", mblnGoalsAccomplishedRequiresApproval)
        End Set
    End Property


    Private mblnEnableBSCAutomaticRating As Boolean
    Public Property _EnableBSCAutomaticRating() As Boolean
        Get
            Return mblnEnableBSCAutomaticRating
        End Get
        Set(ByVal value As Boolean)
            mblnEnableBSCAutomaticRating = value
            setParamVal("EnableBSCAutomaticRating", mblnEnableBSCAutomaticRating)
        End Set
    End Property

    Private mblnDontAllowToEditScoreGenbySys As Boolean
    Public Property _DontAllowToEditScoreGenbySys() As Boolean
        Get
            Return mblnDontAllowToEditScoreGenbySys
        End Get
        Set(ByVal value As Boolean)
            mblnDontAllowToEditScoreGenbySys = value
            setParamVal("DontAllowToEditScoreGenbySys", mblnDontAllowToEditScoreGenbySys)
        End Set
    End Property

    'Shani (26-Sep-2016) -- End


    'S.SANDEEP [25-JAN-2017] -- START
    'ISSUE/ENHANCEMENT :
    Private mblnAssignByJobCompetencies As Boolean = False
    Public Property _AssignByJobCompetencies() As Boolean
        Get
            Return mblnAssignByJobCompetencies
        End Get
        Set(ByVal value As Boolean)
            mblnAssignByJobCompetencies = value
            setParamVal("AssignByJobCompetencies", mblnAssignByJobCompetencies)
        End Set
    End Property
    'S.SANDEEP [25-JAN-2017] -- END

    'S.SANDEEP [25-JAN-2017] -- START
    'ISSUE/ENHANCEMENT :
    Private mintReviewerScoreSetting As Integer = enReviewerScoreSetting.None
    Public Property _ReviewerScoreSetting() As Integer
        Get
            Return mintReviewerScoreSetting
        End Get
        Set(ByVal value As Integer)
            mintReviewerScoreSetting = value
            setParamVal("ReviewerScoreSetting", mintReviewerScoreSetting)
        End Set
    End Property
    'S.SANDEEP [25-JAN-2017] -- END

    'Shani(23-FEB-2017) -- Start
    'Enhancement - Add new custom item saving setting requested by (aga khan)
    Private mblnIsAllowCustomItemFinalSave As Boolean = False
    Public Property _IsAllowCustomItemFinalSave() As Boolean
        Get
            Return mblnIsAllowCustomItemFinalSave
        End Get
        Set(ByVal blnIsAllowCustomItemFinalSave As Boolean)
            mblnIsAllowCustomItemFinalSave = blnIsAllowCustomItemFinalSave
            setParamVal("ISALLOWCUSTOMITEMFINALSAVE", mblnIsAllowCustomItemFinalSave)
        End Set
    End Property
    'Shani(23-FEB-2017) -- End

    'S.SANDEEP [28-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : CCBRT, MONTHLY PAYROLL REPORT
    Private mstrCheckedMonthlyPayrollReport1HeadIds As String = String.Empty
    Private mstrCheckedMonthlyPayrollReport2HeadIds As String = String.Empty
    Private mstrCheckedMonthlyPayrollReport3HeadIds As String = String.Empty
    Private mstrCheckedMonthlyPayrollReport4HeadIds As String = String.Empty

    Private mstrCheckedMonthlyPayrollReportLeaveIds As String = String.Empty
    Private mblnMonthlyPayrollReportSkipAbsentFromTnA As Boolean = True
    Private mblnMonthlyPayrollReportIncludePendingApproverForms As Boolean = False
    Private mblnIncludeSystemRetirementMonthlyPayrollReport As Boolean = True

    Public Property _CheckedMonthlyPayrollReport1HeadIds() As String
        Get
            Return mstrCheckedMonthlyPayrollReport1HeadIds
        End Get
        Set(ByVal value As String)
            mstrCheckedMonthlyPayrollReport1HeadIds = value
            setParamVal("CheckedMonthlyPayrollReport1HeadIds", mstrCheckedMonthlyPayrollReport1HeadIds)
        End Set
    End Property

    Public Property _CheckedMonthlyPayrollReport2HeadIds() As String
        Get
            Return mstrCheckedMonthlyPayrollReport2HeadIds
        End Get
        Set(ByVal value As String)
            mstrCheckedMonthlyPayrollReport2HeadIds = value
            setParamVal("CheckedMonthlyPayrollReport2HeadIds", mstrCheckedMonthlyPayrollReport2HeadIds)
        End Set
    End Property

    Public Property _CheckedMonthlyPayrollReport3HeadIds() As String
        Get
            Return mstrCheckedMonthlyPayrollReport3HeadIds
        End Get
        Set(ByVal value As String)
            mstrCheckedMonthlyPayrollReport3HeadIds = value
            setParamVal("CheckedMonthlyPayrollReport3HeadIds", mstrCheckedMonthlyPayrollReport3HeadIds)
        End Set
    End Property

    Public Property _CheckedMonthlyPayrollReport4HeadIds() As String
        Get
            Return mstrCheckedMonthlyPayrollReport4HeadIds
        End Get
        Set(ByVal value As String)
            mstrCheckedMonthlyPayrollReport4HeadIds = value
            setParamVal("CheckedMonthlyPayrollReport4HeadIds", mstrCheckedMonthlyPayrollReport4HeadIds)
        End Set
    End Property

    Public Property _CheckedMonthlyPayrollReportLeaveIds() As String
        Get
            Return mstrCheckedMonthlyPayrollReportLeaveIds
        End Get
        Set(ByVal value As String)
            mstrCheckedMonthlyPayrollReportLeaveIds = value
            setParamVal("CheckedMonthlyPayrollReportLeaveIds", mstrCheckedMonthlyPayrollReportLeaveIds)
        End Set
    End Property

    Public Property _SkipAbsentFromTnAMonthlyPayrollReport() As Boolean
        Get
            Return mblnMonthlyPayrollReportSkipAbsentFromTnA
        End Get
        Set(ByVal value As Boolean)
            mblnMonthlyPayrollReportSkipAbsentFromTnA = value
            setParamVal("MonthlyPayrollReportSkipAbsentFromTnA", mblnMonthlyPayrollReportSkipAbsentFromTnA)
        End Set
    End Property

    Public Property _IncludePendingApproverFormsInMonthlyPayrollReport() As Boolean
        Get
            Return mblnMonthlyPayrollReportIncludePendingApproverForms
        End Get
        Set(ByVal value As Boolean)
            mblnMonthlyPayrollReportIncludePendingApproverForms = value
            setParamVal("MonthlyPayrollReportIncludePendingApproverForms", mblnMonthlyPayrollReportIncludePendingApproverForms)
        End Set
    End Property

    Public Property _IncludeSystemRetirementMonthlyPayrollReport() As Boolean
        Get
            Return mblnIncludeSystemRetirementMonthlyPayrollReport
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeSystemRetirementMonthlyPayrollReport = value
            setParamVal("IncludeSystemRetirementMonthlyPayrollReport", mblnIncludeSystemRetirementMonthlyPayrollReport)
        End Set
    End Property
    'S.SANDEEP [28-FEB-2017] -- END


'S.SANDEEP [06-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : Training Module Notification
    Private mstrNtf_TrainingLevelI_EvalUserIds As String = String.Empty
    Public Property _Ntf_TrainingLevelI_EvalUserIds() As String
        Get
            Return mstrNtf_TrainingLevelI_EvalUserIds
        End Get
        Set(ByVal value As String)
            mstrNtf_TrainingLevelI_EvalUserIds = value
            setParamVal("Ntf_TrainingLevelI_EvalUserIds", mstrNtf_TrainingLevelI_EvalUserIds)
        End Set
    End Property

    Private mstrNtf_TrainingLevelIII_EvalUserIds As String = String.Empty
    Public Property _Ntf_TrainingLevelIII_EvalUserIds() As String
        Get
            Return mstrNtf_TrainingLevelIII_EvalUserIds
        End Get
        Set(ByVal value As String)
            mstrNtf_TrainingLevelIII_EvalUserIds = value
            setParamVal("Ntf_TrainingLevelIII_EvalUserIds", mstrNtf_TrainingLevelIII_EvalUserIds)
        End Set
    End Property
    'S.SANDEEP [06-MAR-2017] -- END

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # (38,41)
    Private mstrNtf_GoalsUnlockUserIds As String = String.Empty
    Public Property _Ntf_GoalsUnlockUserIds() As String
        Get
            Return mstrNtf_GoalsUnlockUserIds
        End Get
        Set(ByVal value As String)
            mstrNtf_GoalsUnlockUserIds = value
            setParamVal("Ntf_GoalsUnlockUserIds", mstrNtf_GoalsUnlockUserIds)
        End Set
    End Property

    Private mstrNtf_FinalAcknowledgementUserIds As String = String.Empty
    Public Property _Ntf_FinalAcknowledgementUserIds() As String
        Get
            Return mstrNtf_FinalAcknowledgementUserIds
        End Get
        Set(ByVal value As String)
            mstrNtf_FinalAcknowledgementUserIds = value
            setParamVal("Ntf_FinalAcknowledgementUserIds", mstrNtf_FinalAcknowledgementUserIds)
        End Set
    End Property
    'S.SANDEEP [29-NOV-2017] -- END


    'S.SANDEEP [22-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
    Private mintPEReport1_HealthInsuranceMappedHeadId As Integer = 0
    Private mintPEReport2_HealthInsuranceMappedHeadId As Integer = 0
    Private mintPEReport3_HealthInsuranceMappedHeadId As Integer = 0
    Private mintPEReport4_HealthInsuranceMappedHeadId As Integer = 0
    Private mintPEReport5_HealthInsuranceMappedHeadId As Integer = 0
    Public Property _PEReport1_HealthInsuranceMappedHeadId() As Integer
        Get
            Return mintPEReport1_HealthInsuranceMappedHeadId
        End Get
        Set(ByVal value As Integer)
            mintPEReport1_HealthInsuranceMappedHeadId = value
            Call setParamVal("PEReport1_HealthInsuranceMappedHeadId", mintPEReport1_HealthInsuranceMappedHeadId)
        End Set
    End Property
    Public Property _PEReport2_HealthInsuranceMappedHeadId() As Integer
        Get
            Return mintPEReport2_HealthInsuranceMappedHeadId
        End Get
        Set(ByVal value As Integer)
            mintPEReport2_HealthInsuranceMappedHeadId = value
            Call setParamVal("PEReport2_HealthInsuranceMappedHeadId", mintPEReport2_HealthInsuranceMappedHeadId)
        End Set
    End Property
    Public Property _PEReport3_HealthInsuranceMappedHeadId() As Integer
        Get
            Return mintPEReport3_HealthInsuranceMappedHeadId
        End Get
        Set(ByVal value As Integer)
            mintPEReport3_HealthInsuranceMappedHeadId = value
            Call setParamVal("PEReport3_HealthInsuranceMappedHeadId", mintPEReport3_HealthInsuranceMappedHeadId)
        End Set
    End Property
    Public Property _PEReport4_HealthInsuranceMappedHeadId() As Integer
        Get
            Return mintPEReport4_HealthInsuranceMappedHeadId
        End Get
        Set(ByVal value As Integer)
            mintPEReport4_HealthInsuranceMappedHeadId = value
            Call setParamVal("PEReport4_HealthInsuranceMappedHeadId", mintPEReport4_HealthInsuranceMappedHeadId)
        End Set
    End Property
    Public Property _PEReport5_HealthInsuranceMappedHeadId() As Integer
        Get
            Return mintPEReport5_HealthInsuranceMappedHeadId
        End Get
        Set(ByVal value As Integer)
            mintPEReport5_HealthInsuranceMappedHeadId = value
            Call setParamVal("PEReport5_HealthInsuranceMappedHeadId", mintPEReport5_HealthInsuranceMappedHeadId)
        End Set
    End Property

    Private mstrPEReport1_SelectedMembershipIds As String = String.Empty
    Private mstrPEReport2_SelectedMembershipIds As String = String.Empty
    Private mstrPEReport3_SelectedMembershipIds As String = String.Empty
    Private mstrPEReport4_SelectedMembershipIds As String = String.Empty
    Private mstrPEReport5_SelectedMembershipIds As String = String.Empty
    Public Property _PEReport1_SelectedMembershipIds() As String
        Get
            Return mstrPEReport1_SelectedMembershipIds
        End Get
        Set(ByVal value As String)
            mstrPEReport1_SelectedMembershipIds = value
            setParamVal("PEReport1_SelectedMembershipIds", mstrPEReport1_SelectedMembershipIds)
        End Set
    End Property
    Public Property _PEReport2_SelectedMembershipIds() As String
        Get
            Return mstrPEReport2_SelectedMembershipIds
        End Get
        Set(ByVal value As String)
            mstrPEReport2_SelectedMembershipIds = value
            setParamVal("PEReport2_SelectedMembershipIds", mstrPEReport2_SelectedMembershipIds)
        End Set
    End Property
    Public Property _PEReport3_SelectedMembershipIds() As String
        Get
            Return mstrPEReport3_SelectedMembershipIds
        End Get
        Set(ByVal value As String)
            mstrPEReport3_SelectedMembershipIds = value
            setParamVal("PEReport3_SelectedMembershipIds", mstrPEReport3_SelectedMembershipIds)
        End Set
    End Property
    Public Property _PEReport4_SelectedMembershipIds() As String
        Get
            Return mstrPEReport4_SelectedMembershipIds
        End Get
        Set(ByVal value As String)
            mstrPEReport4_SelectedMembershipIds = value
            setParamVal("PEReport4_SelectedMembershipIds", mstrPEReport4_SelectedMembershipIds)
        End Set
    End Property
    Public Property _PEReport5_SelectedMembershipIds() As String
        Get
            Return mstrPEReport5_SelectedMembershipIds
        End Get
        Set(ByVal value As String)
            mstrPEReport5_SelectedMembershipIds = value
            setParamVal("PEReport5_SelectedMembershipIds", mstrPEReport5_SelectedMembershipIds)
        End Set
    End Property

    Private mintPEReport1_AllocationTypeId As Integer = enAllocation.CLASS_GROUP
    Private mintPEReport2_AllocationTypeId As Integer = enAllocation.CLASS_GROUP
    Private mintPEReport3_AllocationTypeId As Integer = enAllocation.CLASS_GROUP
    Private mintPEReport4_AllocationTypeId As Integer = enAllocation.CLASS_GROUP
    Private mintPEReport5_AllocationTypeId As Integer = enAllocation.CLASS_GROUP
    Public Property _PEReport1_AllocationTypeId() As Integer
        Get
            Return mintPEReport1_AllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintPEReport1_AllocationTypeId = value
            setParamVal("PEReport1_AllocationTypeId", mintPEReport1_AllocationTypeId)
        End Set
    End Property
    Public Property _PEReport2_AllocationTypeId() As Integer
        Get
            Return mintPEReport2_AllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintPEReport2_AllocationTypeId = value
            setParamVal("PEReport2_AllocationTypeId", mintPEReport2_AllocationTypeId)
        End Set
    End Property
    Public Property _PEReport3_AllocationTypeId() As Integer
        Get
            Return mintPEReport3_AllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintPEReport3_AllocationTypeId = value
            setParamVal("PEReport3_AllocationTypeId", mintPEReport3_AllocationTypeId)
        End Set
    End Property
    Public Property _PEReport4_AllocationTypeId() As Integer
        Get
            Return mintPEReport4_AllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintPEReport4_AllocationTypeId = value
            setParamVal("PEReport4_AllocationTypeId", mintPEReport4_AllocationTypeId)
        End Set
    End Property
    Public Property _PEReport5_AllocationTypeId() As Integer
        Get
            Return mintPEReport5_AllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintPEReport5_AllocationTypeId = value
            setParamVal("PEReport5_AllocationTypeId", mintPEReport5_AllocationTypeId)
        End Set
    End Property

    Private mstrPEReport1_SelectedAllocationIds As String = String.Empty
    Private mstrPEReport2_SelectedAllocationIds As String = String.Empty
    Private mstrPEReport3_SelectedAllocationIds As String = String.Empty
    Private mstrPEReport4_SelectedAllocationIds As String = String.Empty
    Private mstrPEReport5_SelectedAllocationIds As String = String.Empty
    Public Property _PEReport1_SelectedAllocationIds() As String
        Get
            Return mstrPEReport1_SelectedAllocationIds
        End Get
        Set(ByVal value As String)
            mstrPEReport1_SelectedAllocationIds = value
            setParamVal("PEReport1_SelectedAllocationIds", mstrPEReport1_SelectedAllocationIds)
        End Set
    End Property
    Public Property _PEReport2_SelectedAllocationIds() As String
        Get
            Return mstrPEReport2_SelectedAllocationIds
        End Get
        Set(ByVal value As String)
            mstrPEReport2_SelectedAllocationIds = value
            setParamVal("PEReport2_SelectedAllocationIds", mstrPEReport2_SelectedAllocationIds)
        End Set
    End Property
    Public Property _PEReport3_SelectedAllocationIds() As String
        Get
            Return mstrPEReport3_SelectedAllocationIds
        End Get
        Set(ByVal value As String)
            mstrPEReport3_SelectedAllocationIds = value
            setParamVal("PEReport3_SelectedAllocationIds", mstrPEReport3_SelectedAllocationIds)
        End Set
    End Property
    Public Property _PEReport4_SelectedAllocationIds() As String
        Get
            Return mstrPEReport4_SelectedAllocationIds
        End Get
        Set(ByVal value As String)
            mstrPEReport4_SelectedAllocationIds = value
            setParamVal("PEReport4_SelectedAllocationIds", mstrPEReport4_SelectedAllocationIds)
        End Set
    End Property
    Public Property _PEReport5_SelectedAllocationIds() As String
        Get
            Return mstrPEReport5_SelectedAllocationIds
        End Get
        Set(ByVal value As String)
            mstrPEReport5_SelectedAllocationIds = value
            setParamVal("PEReport5_SelectedAllocationIds", mstrPEReport5_SelectedAllocationIds)
        End Set
    End Property

    Private mintPEReport1_IncrementReasonMappedId As Integer = 0
    Private mintPEReport2_IncrementReasonMappedId As Integer = 0
    Private mintPEReport3_IncrementReasonMappedId As Integer = 0
    Private mintPEReport4_IncrementReasonMappedId As Integer = 0
    Private mintPEReport5_IncrementReasonMappedId As Integer = 0
    Public Property _PEReport1_IncrementReasonMappedId() As Integer
        Get
            Return mintPEReport1_IncrementReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport1_IncrementReasonMappedId = value
            setParamVal("PEReport1_IncrementReasonMappedId", mintPEReport1_IncrementReasonMappedId)
        End Set
    End Property
    Public Property _PEReport2_IncrementReasonMappedId() As Integer
        Get
            Return mintPEReport2_IncrementReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport2_IncrementReasonMappedId = value
            setParamVal("PEReport2_IncrementReasonMappedId", mintPEReport2_IncrementReasonMappedId)
        End Set
    End Property
    Public Property _PEReport3_IncrementReasonMappedId() As Integer
        Get
            Return mintPEReport3_IncrementReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport3_IncrementReasonMappedId = value
            setParamVal("PEReport3_IncrementReasonMappedId", mintPEReport3_IncrementReasonMappedId)
        End Set
    End Property
    Public Property _PEReport4_IncrementReasonMappedId() As Integer
        Get
            Return mintPEReport4_IncrementReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport4_IncrementReasonMappedId = value
            setParamVal("PEReport4_IncrementReasonMappedId", mintPEReport4_IncrementReasonMappedId)
        End Set
    End Property
    Public Property _PEReport5_IncrementReasonMappedId() As Integer
        Get
            Return mintPEReport5_IncrementReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport5_IncrementReasonMappedId = value
            setParamVal("PEReport5_IncrementReasonMappedId", mintPEReport5_IncrementReasonMappedId)
        End Set
    End Property

    Private mintPEReport1_PromotionReasonMappedId As Integer = 0
    Private mintPEReport2_PromotionReasonMappedId As Integer = 0
    Private mintPEReport3_PromotionReasonMappedId As Integer = 0
    Private mintPEReport4_PromotionReasonMappedId As Integer = 0
    Private mintPEReport5_PromotionReasonMappedId As Integer = 0
    Public Property _PEReport1_PromotionReasonMappedId() As Integer
        Get
            Return mintPEReport1_PromotionReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport1_PromotionReasonMappedId = value
            setParamVal("PEReport1_PromotionReasonMappedId", mintPEReport1_PromotionReasonMappedId)
        End Set
    End Property
    Public Property _PEReport2_PromotionReasonMappedId() As Integer
        Get
            Return mintPEReport2_PromotionReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport2_PromotionReasonMappedId = value
            setParamVal("PEReport2_PromotionReasonMappedId", mintPEReport2_PromotionReasonMappedId)
        End Set
    End Property
    Public Property _PEReport3_PromotionReasonMappedId() As Integer
        Get
            Return mintPEReport3_PromotionReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport3_PromotionReasonMappedId = value
            setParamVal("PEReport3_PromotionReasonMappedId", mintPEReport3_PromotionReasonMappedId)
        End Set
    End Property
    Public Property _PEReport4_PromotionReasonMappedId() As Integer
        Get
            Return mintPEReport4_PromotionReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport4_PromotionReasonMappedId = value
            setParamVal("PEReport4_PromotionReasonMappedId", mintPEReport4_PromotionReasonMappedId)
        End Set
    End Property
    Public Property _PEReport5_PromotionReasonMappedId() As Integer
        Get
            Return mintPEReport5_PromotionReasonMappedId
        End Get
        Set(ByVal value As Integer)
            mintPEReport5_PromotionReasonMappedId = value
            setParamVal("PEReport5_PromotionReasonMappedId", mintPEReport5_PromotionReasonMappedId)
        End Set
    End Property
    'S.SANDEEP [22-MAR-2017] -- END


    'S.SANDEEP [10-MAY-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
    Private mblnIsSymmetryIntegrated As Boolean = False
    Private mstrSymmetryDataServerAddress As String = String.Empty
    Private mstrSymmetryDatabaseName As String = String.Empty
    Private mstrSymmetryDatabaseUserName As String = String.Empty
    Private mstrSymmetryDatabasePassword As String = String.Empty
    Private mintSymmetryCardNumberTypeId As Integer = 0
    Private mblnSymmetryMarkAsDefault As Boolean = False
    Private mintSymmetryAuthenticationModeId As Integer = 0

    Public Property _IsSymmetryIntegrated() As Boolean
        Get
            Return mblnIsSymmetryIntegrated
        End Get
        Set(ByVal value As Boolean)
            mblnIsSymmetryIntegrated = value
            setParamVal("IsSymmetryIntegrated", mblnIsSymmetryIntegrated)
        End Set
    End Property
    Public Property _SymmetryDataServerAddress() As String
        Get
            Return mstrSymmetryDataServerAddress
        End Get
        Set(ByVal value As String)
            mstrSymmetryDataServerAddress = value
            setParamVal("SymmetryDataServerAddress", mstrSymmetryDataServerAddress)
        End Set
    End Property
    Public Property _SymmetryDatabaseName() As String
        Get
            Return mstrSymmetryDatabaseName
        End Get
        Set(ByVal value As String)
            mstrSymmetryDatabaseName = value
            setParamVal("SymmetryDatabaseName", mstrSymmetryDatabaseName)
        End Set
    End Property
    Public Property _SymmetryDatabaseUserName() As String
        Get
            Return mstrSymmetryDatabaseUserName
        End Get
        Set(ByVal value As String)
            mstrSymmetryDatabaseUserName = value
            setParamVal("SymmetryDatabaseUserName", mstrSymmetryDatabaseUserName)
        End Set
    End Property
    Public Property _SymmetryDatabasePassword() As String
        Get
            Return mstrSymmetryDatabasePassword
        End Get
        Set(ByVal value As String)
            mstrSymmetryDatabasePassword = clsSecurity.Encrypt(value, "ezee")
            setParamVal("SymmetryDatabasePassword", mstrSymmetryDatabasePassword)
        End Set
    End Property
    Public Property _SymmetryCardNumberTypeId() As Integer
        Get
            Return mintSymmetryCardNumberTypeId
        End Get
        Set(ByVal value As Integer)
            mintSymmetryCardNumberTypeId = value
            setParamVal("SymmetryCardNumberTypeId", mintSymmetryCardNumberTypeId)
        End Set
    End Property
    Public Property _SymmetryMarkAsDefault() As Boolean
        Get
            Return mblnSymmetryMarkAsDefault
        End Get
        Set(ByVal value As Boolean)
            mblnSymmetryMarkAsDefault = value
            setParamVal("SymmetryMarkAsDefault", mblnSymmetryMarkAsDefault)
        End Set
    End Property
    Public Property _SymmetryAuthenticationModeId() As Integer
        Get
            Return mintSymmetryAuthenticationModeId
        End Get
        Set(ByVal value As Integer)
            mintSymmetryAuthenticationModeId = value
            setParamVal("SymmetryAuthenticationModeId", mintSymmetryAuthenticationModeId)
        End Set
    End Property
    'S.SANDEEP [10-MAY-2017] -- END



    'Pinkal (04-Oct-2017) -- Start
    'Enhancement - Working SAGEM Database Device Integration.

    Private mstrSagemDataServerAddress As String = String.Empty
    Private mstrSagemDatabaseName As String = String.Empty
    Private mstrSagemDatabaseUserName As String = String.Empty
    Private mstrSagemDatabasePassword As String = String.Empty

    Public Property _SagemDataServerAddress() As String
        Get
            Return mstrSagemDataServerAddress
        End Get
        Set(ByVal value As String)
            mstrSagemDataServerAddress = value
            setParamVal("SagemDataServerAddress", mstrSagemDataServerAddress)
        End Set
    End Property

    Public Property _SagemDatabaseName() As String
        Get
            Return mstrSagemDatabaseName
        End Get
        Set(ByVal value As String)
            mstrSagemDatabaseName = value
            setParamVal("SagemDatabaseName", mstrSagemDatabaseName)
        End Set
    End Property

    Public Property _SagemDatabaseUserName() As String
        Get
            Return mstrSagemDatabaseUserName
        End Get
        Set(ByVal value As String)
            mstrSagemDatabaseUserName = value
            setParamVal("SagemDatabaseUserName", mstrSagemDatabaseUserName)
        End Set
    End Property

    Public Property _SagemDatabasePassword() As String
        Get
            Return mstrSagemDatabasePassword
        End Get
        Set(ByVal value As String)
            mstrSagemDatabasePassword = clsSecurity.Encrypt(value, "ezee")
            setParamVal("SagemDatabasePassword", mstrSagemDatabasePassword)
        End Set
    End Property

    'Pinkal (04-Oct-2017) -- End


    'S.SANDEEP [25-OCT-2017] -- START
    Private mblnShowMyGoals As Boolean = True
    Private mblnShowCustomHeaders As Boolean = True
    Private mblnShowGoalsApproval As Boolean = True
    Private mblnShowSelfAssessment As Boolean = True
    Private mblnShowMyComptencies As Boolean = True
    Private mblnShowViewPerformanceAssessment As Boolean = True
    Public Property _ShowMyGoals() As Boolean
        Get
            Return mblnShowMyGoals
        End Get
        Set(ByVal value As Boolean)
            mblnShowMyGoals = value
            setParamVal("ShowMyGoals", mblnShowMyGoals)
        End Set
    End Property
    Public Property _ShowCustomHeaders() As Boolean
        Get
            Return mblnShowCustomHeaders
        End Get
        Set(ByVal value As Boolean)
            mblnShowCustomHeaders = value
            setParamVal("ShowCustomHeaders", mblnShowCustomHeaders)
        End Set
    End Property
    Public Property _ShowGoalsApproval() As Boolean
        Get
            Return mblnShowGoalsApproval
        End Get
        Set(ByVal value As Boolean)
            mblnShowGoalsApproval = value
            setParamVal("ShowGoalsApproval", mblnShowGoalsApproval)
        End Set
    End Property
    Public Property _ShowSelfAssessment() As Boolean
        Get
            Return mblnShowSelfAssessment
        End Get
        Set(ByVal value As Boolean)
            mblnShowSelfAssessment = value
            setParamVal("ShowSelfAssessment", mblnShowSelfAssessment)
        End Set
    End Property
    Public Property _ShowMyComptencies() As Boolean
        Get
            Return mblnShowMyComptencies
        End Get
        Set(ByVal value As Boolean)
            mblnShowMyComptencies = value
            setParamVal("ShowMyComptencies", mblnShowMyComptencies)
        End Set
    End Property
    Public Property _ShowViewPerformanceAssessment() As Boolean
        Get
            Return mblnShowViewPerformanceAssessment
        End Get
        Set(ByVal value As Boolean)
            mblnShowViewPerformanceAssessment = value
            setParamVal("ShowViewPerformanceAssessment", mblnShowViewPerformanceAssessment)
        End Set
    End Property
    'S.SANDEEP [25-OCT-2017] -- END



    'Pinkal (12-Feb-2018) -- Start
    'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.
    Private mblnSendJobConfirmationEmail As Boolean = False
    Public Property _SendJobConfirmationEmail() As Boolean
        Get
            Return mblnSendJobConfirmationEmail
        End Get
        Set(ByVal value As Boolean)
            mblnSendJobConfirmationEmail = value
            setParamVal("SendJobConfirmationEmail", mblnSendJobConfirmationEmail)
        End Set
    End Property

    Private mblnVacancyAlert As Boolean = False
    Public Property _isVacancyAlert() As Boolean
        Get
            Return mblnVacancyAlert
        End Get
        Set(ByVal value As Boolean)
            mblnVacancyAlert = value
            setParamVal("isVancanyalert", mblnVacancyAlert)
        End Set
    End Property

    Private mintMaxVacancyAlert As Integer = 0
    Public Property _MaxVacancyAlert() As Integer
        Get
            Return mintMaxVacancyAlert
        End Get
        Set(ByVal value As Integer)
            mintMaxVacancyAlert = value
            setParamVal("MaxVacancyAlert", mintMaxVacancyAlert)
        End Set
    End Property

    'Pinkal (12-Feb-2018) -- End

    'Pinkal (18-May-2018) -- Start
    'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.

    Private mblnAttachApplicantCV As Boolean = False
    Public Property _AttachApplicantCV() As Boolean
        Get
            Return mblnAttachApplicantCV
        End Get
        Set(ByVal value As Boolean)
            mblnAttachApplicantCV = value
            setParamVal("AttachApplicantCV", mblnAttachApplicantCV)
        End Set
    End Property

    'Pinkal (18-May-2018) -- End

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private mintEmployeeRejectNotificationTemplateId As Integer = 0
    Public Property _EmployeeRejectNotificationTemplateId() As Integer
        Get
            Return mintEmployeeRejectNotificationTemplateId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeRejectNotificationTemplateId = value
            setParamVal("EmployeeRejectNotificationTemplateId", mintEmployeeRejectNotificationTemplateId)
        End Set
    End Property

    Private mstrEmployeeRejectNotificationUserIds As String = ""
    Public Property _EmployeeRejectNotificationUserIds() As String
        Get
            Return mstrEmployeeRejectNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrEmployeeRejectNotificationUserIds = value
            setParamVal("EmployeeRejectNotificationUserIds", mstrEmployeeRejectNotificationUserIds)
        End Set
    End Property
    'S.SANDEEP [20-JUN-2018] -- END


    'S.SANDEEP [09-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#292}
    Private mblnIsHRFlexcubeIntegrated As Boolean = False
    Private mintFlexcubeMsgIDNoType As Integer = 1
    Private mstrFlexcubeMsgIDPrefix As String = ""
    Private mintNextFlexcubeMsgID As Integer = 1

    Private mdicFlexcubeServiceCollection As New Dictionary(Of Integer, String)

    Public Property _IsHRFlexcubeIntegrated() As Boolean
        Get
            Return mblnIsHRFlexcubeIntegrated
        End Get
        Set(ByVal value As Boolean)
            mblnIsHRFlexcubeIntegrated = value
            setParamVal("IsHRFlexcubeIntegrated", mblnIsHRFlexcubeIntegrated)
        End Set
    End Property

    Public Property _FlexcubeMsgIDNoType() As Integer
        Get
            Return mintFlexcubeMsgIDNoType
        End Get
        Set(ByVal value As Integer)
            mintFlexcubeMsgIDNoType = value
            setParamVal("FlexcubeMsgIDNoType", mintFlexcubeMsgIDNoType)
        End Set
    End Property
    Public Property _FlexcubeMsgIDPrefix() As String
        Get
            Return mstrFlexcubeMsgIDPrefix
        End Get
        Set(ByVal value As String)
            mstrFlexcubeMsgIDPrefix = value
            setParamVal("FlexcubeMsgIDPrefix", mstrFlexcubeMsgIDPrefix)
        End Set
    End Property
    Public Property _NextFlexcubeMsgID() As Integer
        Get
            Return mintNextFlexcubeMsgID
        End Get
        Set(ByVal value As Integer)
            mintNextFlexcubeMsgID = value
            setParamVal("NextFlexcubeMsgID", mintNextFlexcubeMsgID)
        End Set
    End Property

    Private mstrFlexcubeAccountCategory As String = String.Empty
    Private mstrFlexcubeAccountClass As String = String.Empty

    Public Property _FlexcubeAccountCategory() As String
        Get
            Return mstrFlexcubeAccountCategory
        End Get
        Set(ByVal value As String)
            mstrFlexcubeAccountCategory = value
            setParamVal("FlexcubeAccountCategory", mstrFlexcubeAccountCategory)
        End Set
    End Property
    Public Property _FlexcubeAccountClass() As String
        Get
            Return mstrFlexcubeAccountClass
        End Get
        Set(ByVal value As String)
            mstrFlexcubeAccountClass = value
            setParamVal("FlexcubeAccountClass", mstrFlexcubeAccountClass)
        End Set
    End Property
    Public Property _FlexcubeServiceCollection() As Dictionary(Of Integer, String)
        Get
            Return mdicFlexcubeServiceCollection
        End Get
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicFlexcubeServiceCollection = value
            For Each ikey As Integer In mdicFlexcubeServiceCollection.Keys
                setParamVal("_FlxSrv_" & ikey.ToString(), mdicFlexcubeServiceCollection(ikey))
            Next
        End Set
    End Property

    Private mdtFlexSrvRunningTime As DateTime
    Public Property _FlexSrvRunningTime() As DateTime
        Get
            Return mdtFlexSrvRunningTime
        End Get
        Set(ByVal value As DateTime)
            mdtFlexSrvRunningTime = value
            setParamVal("FlexSrvRunningTime", mdtFlexSrvRunningTime)
        End Set
    End Property
    'S.SANDEEP [09-AUG-2018] -- END
    
    'Gajanan [13-AUG-2018] -- Start
    'Enhancement - Implementing Grievance Module.
    Private mintGrievanceRefNoType As Integer = 1
    Private mstrGrievanceRefPrefix As String = ""
    Private mintNextGrievanceRefNo As Integer = 1
    Public Property _GrievanceRefNoType() As Integer
        Get
            Return mintGrievanceRefNoType
        End Get
        Set(ByVal value As Integer)
            mintGrievanceRefNoType = value
            setParamVal("GrievanceRefNoType", mintGrievanceRefNoType)
        End Set
    End Property
    Public Property _GrievanceRefPrefix() As String
        Get
            Return mstrGrievanceRefPrefix
        End Get
        Set(ByVal value As String)
            mstrGrievanceRefPrefix = value
            setParamVal("GrievanceRefPrefix", mstrGrievanceRefPrefix)
        End Set
    End Property
    Public Property _NextGrievanceRefNo() As Integer
        Get
            Return mintNextGrievanceRefNo
        End Get
        Set(ByVal value As Integer)
            mintNextGrievanceRefNo = value
            setParamVal("NextGrievanceRefNo", mintNextGrievanceRefNo)
        End Set
    End Property
    'Gajanan(13-AUG-2018) -- End

'S.SANDEEP [09-OCT-2018] -- START
    Private mblnEnableTraningRequisition As Boolean = False
    Public Property _EnableTraningRequisition() As Boolean
        Get
            Return mblnEnableTraningRequisition
        End Get
        Set(ByVal value As Boolean)
            mblnEnableTraningRequisition = value
            setParamVal("EnableTraningRequisition", mblnEnableTraningRequisition)
        End Set
    End Property
    'S.SANDEEP [09-OCT-2018] -- END


    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
    Private mstrStaffRequisitionFinalApprovedNotificationUserIds As String = ""
    Public Property _StaffRequisitionFinalApprovedNotificationUserIds() As String
        Get
            Return mstrStaffRequisitionFinalApprovedNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrStaffRequisitionFinalApprovedNotificationUserIds = value
            setParamVal("StaffRequisitionFinalApprovedNotificationUserIds", mstrStaffRequisitionFinalApprovedNotificationUserIds)
        End Set
    End Property
    'Sohail (12 Oct 2018) -- End




    'S.SANDEEP [09-OCT-2018] -- START
    Private mintDonotAllowToRequestTrainingAfterDays As Integer = 1
  
    Public Property _DonotAllowToRequestTrainingAfterDays() As Integer
        Get
            Return mintDonotAllowToRequestTrainingAfterDays
        End Get
        Set(ByVal value As Integer)
            mintDonotAllowToRequestTrainingAfterDays = value
            setParamVal("DonotAllowToRequestTrainingAfterDays", mintDonotAllowToRequestTrainingAfterDays)
        End Set
    End Property

    'S.SANDEEP [09-OCT-2018] -- END

    'Hemant (19 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement for Email Notification After Final Saved In Asset Declaration
    Private mstrAssetDeclarationFinalSavedNotificationUserIds As String = ""
    Public Property _AssetDeclarationFinalSavedNotificationUserIds() As String
        Get
            Return mstrAssetDeclarationFinalSavedNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrAssetDeclarationFinalSavedNotificationUserIds = value
            setParamVal("AssetDeclarationFinalSavedNotificationUserIds", mstrAssetDeclarationFinalSavedNotificationUserIds)
        End Set
    End Property
    'Hemant (19 Nov 2018) -- End

    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
    Private mstrAssetDeclarationUnlockFinalSavedNotificationUserIds As String = ""
    Public Property _AssetDeclarationUnlockFinalSavedNotificationUserIds() As String
        Get
            Return mstrAssetDeclarationUnlockFinalSavedNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrAssetDeclarationUnlockFinalSavedNotificationUserIds = value
            setParamVal("AssetDeclarationUnlockFinalSavedNotificationUserIds", mstrAssetDeclarationUnlockFinalSavedNotificationUserIds)
        End Set
    End Property
    'Hemant (23 Nov 2018) -- End

    'Sohail (19 Feb 2020) -- Start
    'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
    Private mstrNonDisclosureDeclarationAcknowledgedNotificationUserIds As String = ""
    Public Property _NonDisclosureDeclarationAcknowledgedNotificationUserIds() As String
        Get
            Return mstrNonDisclosureDeclarationAcknowledgedNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrNonDisclosureDeclarationAcknowledgedNotificationUserIds = value
            setParamVal("NonDisclosureDeclarationAcknowledgedNotificationUserIds", mstrNonDisclosureDeclarationAcknowledgedNotificationUserIds)
        End Set
    End Property
    'Sohail (19 Feb 2020) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
    Private mstrFlexCubeJVPostedOracleNotificationUserIds As String = ""
    Public Property _FlexCubeJVPostedOracleNotificationUserIds() As String
        Get
            Return mstrFlexCubeJVPostedOracleNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrFlexCubeJVPostedOracleNotificationUserIds = value
            setParamVal("FlexCubeJVPostedOracleNotificationUserIds", mstrFlexCubeJVPostedOracleNotificationUserIds)
        End Set
    End Property
    'Sohail (07 Mar 2020) -- End


    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mstrActiveDirectoryNotificationUserIds As String = ""
    Public Property _ActiveDirectoryNotificationUserIds() As String
        Get
            Return mstrActiveDirectoryNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrActiveDirectoryNotificationUserIds = value
            setParamVal("ActiveDirectoryNotificationUserIds", mstrActiveDirectoryNotificationUserIds)
        End Set
    End Property
    'Pinkal (04-Apr-2020) -- End


    'S.SANDEEP |18-JAN-2019| -- START
    Private mintGoalTypeInclusion As Integer = 0
    Public Property _GoalTypeInclusion() As Integer
        Get
            Return mintGoalTypeInclusion
        End Get
        Set(ByVal value As Integer)
            mintGoalTypeInclusion = value
            setParamVal("GoalTypeInclusion", mintGoalTypeInclusion)
        End Set
    End Property
    'S.SANDEEP |18-JAN-2019| -- END

    'S.SANDEEP |05-MAR-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mblnShowArutisignatureinemailnotification As Boolean = True
    Public Property _ShowArutisignatureinemailnotification() As Boolean
        Get
            Return mblnShowArutisignatureinemailnotification
        End Get
        Set(ByVal value As Boolean)
            mblnShowArutisignatureinemailnotification = value
            setParamVal("ShowArutisignature", mblnShowArutisignatureinemailnotification)
        End Set
    End Property
    'S.SANDEEP |05-MAR-2019| -- END

    'S.SANDEEP |14-MAR-2019| -- START
    'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
    Private mblnMakeEmpAssessCommentsMandatory As Boolean = False
    Private mblnMakeAsrAssessCommentsMandatory As Boolean = False
    Private mblnMakeRevAssessCommentsMandatory As Boolean = False
    Public Property _MakeEmpAssessCommentsMandatory() As Boolean
        Get
            Return mblnMakeEmpAssessCommentsMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnMakeEmpAssessCommentsMandatory = value
            setParamVal("MakeEmpAssessCommentsMandatory", value)
        End Set
    End Property
    Public Property _MakeAsrAssessCommentsMandatory() As Boolean
        Get
            Return mblnMakeAsrAssessCommentsMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnMakeAsrAssessCommentsMandatory = value
            setParamVal("MakeAsrAssessCommentsMandatory", value)
        End Set
    End Property
    Public Property _MakeRevAssessCommentsMandatory() As Boolean
        Get
            Return mblnMakeRevAssessCommentsMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnMakeRevAssessCommentsMandatory = value
            setParamVal("MakeRevAssessCommentsMandatory", value)
        End Set
    End Property
    'S.SANDEEP |14-MAR-2019| -- END

    'S.SANDEEP |09-APR-2019| -- START
    Private mblnIsWorkAnniversaryEnabled As Boolean = False
    Private mintWorkAnnivYear As Integer = 1
    Private mintWorkAnnivLetterTemplateId As Integer = 0
    Private mstrWorkAnnivSubject As String = ""

    Public Property _IsWorkAnniversaryEnabled() As Boolean
        Get
            Return mblnIsWorkAnniversaryEnabled
        End Get
        Set(ByVal value As Boolean)
            mblnIsWorkAnniversaryEnabled = value
            setParamVal("IsWorkAnniversaryEnabled", mblnIsWorkAnniversaryEnabled)
        End Set
    End Property

    Public Property _WorkAnnivYear() As Integer
        Get
            Return mintWorkAnnivYear
        End Get
        Set(ByVal value As Integer)
            mintWorkAnnivYear = value
            setParamVal("WorkAnnivYear", mintWorkAnnivYear)
        End Set
    End Property

    Public Property _WorkAnnivLetterTemplateId() As Integer
        Get
            Return mintWorkAnnivLetterTemplateId
        End Get
        Set(ByVal value As Integer)
            mintWorkAnnivLetterTemplateId = value
            setParamVal("WorkAnnivLetterTemplateId", mintWorkAnnivLetterTemplateId)
        End Set
    End Property

    Public Property _WorkAnnivSubject() As String
        Get
            Return mstrWorkAnnivSubject
        End Get
        Set(ByVal value As String)
            mstrWorkAnnivSubject = value
            setParamVal("WorkAnnivSubject", mstrWorkAnnivSubject)
        End Set
    End Property
    'S.SANDEEP |09-APR-2019| -- END


    'Pinkal (01-Apr-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.

    Private mintLeaveApplicationEscalationSettings As Integer = enLeaveEscalationOptions.None
    Public Property _LeaveApplicationEscalationSettings() As Integer
        Get
            Return mintLeaveApplicationEscalationSettings
        End Get
        Set(ByVal value As Integer)
            mintLeaveApplicationEscalationSettings = value
            setParamVal("LeaveApplicationEscalationSettings", value)
        End Set
    End Property


    Private mintLeaveReminderOccurrenceAfterDays As Integer = 0
    Public Property _LeaveReminderOccurrenceAfterDays() As Integer
        Get
            Return mintLeaveReminderOccurrenceAfterDays
        End Get
        Set(ByVal value As Integer)
            mintLeaveReminderOccurrenceAfterDays = value
            setParamVal("LeaveReminderOccurrenceAfterDays", value)
        End Set
    End Property


    Private mstrLeaveEscalationRejectionReason As String = ""
    Public Property _LeaveEscalationRejectionReason() As String
        Get
            Return mstrLeaveEscalationRejectionReason
        End Get
        Set(ByVal value As String)
            mstrLeaveEscalationRejectionReason = value
            setParamVal("LeaveEscalationRejectionReason", value)
        End Set
    End Property

    'Pinkal (01-Apr-2019) -- End

    'S.SANDEEP |25-APR-2019| -- START
    Private mblnWorkAnnivSMS As Boolean = False
    Private mblnWorkAnnivUsr As Boolean = False
    Private mstrWorkAnnivUserIds As String = ""

    Public Property _WorkAnnivSMS() As Boolean
        Get
            Return mblnWorkAnnivSMS
        End Get
        Set(ByVal value As Boolean)
            mblnWorkAnnivSMS = value
            setParamVal("WorkAnnivSMS", mblnWorkAnnivSMS)
        End Set
    End Property

    Public Property _WorkAnnivUsr() As Boolean
        Get
            Return mblnWorkAnnivUsr
        End Get
        Set(ByVal value As Boolean)
            mblnWorkAnnivUsr = value
            setParamVal("WorkAnnivUsr", mblnWorkAnnivUsr)
        End Set
    End Property

    Public Property _WorkAnnivUserIds() As String
        Get
            Return mstrWorkAnnivUserIds
        End Get
        Set(ByVal value As String)
            mstrWorkAnnivUserIds = value
            setParamVal("WorkAnnivUserIds", mstrWorkAnnivUserIds)
        End Set
    End Property
    'S.SANDEEP |25-APR-2019| -- END



    'Gajanan [13-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
    Private mintEmployeeDisagreeGrievanceNotificationTemplateId As Integer = 0
    Public Property _EmployeeDisagreeGrievanceNotificationTemplateId() As Integer
        Get
            Return mintEmployeeDisagreeGrievanceNotificationTemplateId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeDisagreeGrievanceNotificationTemplateId = value
            setParamVal("EmployeeDisagreeGrievanceNotificationTemplateId", mintEmployeeDisagreeGrievanceNotificationTemplateId)
        End Set
    End Property


    Private mstrEmployeeDisagreeGrievanceNotificationUserIds As String = ""
    Public Property _EmployeeDisagreeGrievanceNotificationUserIds() As String
        Get
            Return mstrEmployeeDisagreeGrievanceNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrEmployeeDisagreeGrievanceNotificationUserIds = value
            setParamVal("EmployeeDisagreeGrievanceNotificationUserIds", mstrEmployeeDisagreeGrievanceNotificationUserIds)
        End Set
    End Property

    Private mstrEmployeeDisagreeGrievanceNotificationEmailTitle As String = ""
    Public Property _EmployeeDisagreeGrievanceNotificationEmailTitle() As String
        Get
            Return mstrEmployeeDisagreeGrievanceNotificationEmailTitle
        End Get
        Set(ByVal value As String)
            mstrEmployeeDisagreeGrievanceNotificationEmailTitle = value
            setParamVal("EmployeeDisagreeGrievanceNotificationEmailTitle", mstrEmployeeDisagreeGrievanceNotificationEmailTitle)
        End Set
    End Property
    'Gajanan [13-July-2019] -- End


    'Gajanan [31-AUG-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Private mstrNewJobAddNotificationUserIds As String = ""
    Public Property _NewJobAddNotificationUserIds() As String
        Get
            Return mstrNewJobAddNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrNewJobAddNotificationUserIds = value
            setParamVal("NewJobAddNotificationUserIds", mstrNewJobAddNotificationUserIds)
        End Set
    End Property

    'Gajanan [31-AUG-2019] -- End

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private mblnShowInterdictionDate As Boolean = True
    Public Property _ShowInterdictionDate() As Boolean
        Get
            Return mblnShowInterdictionDate
        End Get
        Set(ByVal value As Boolean)
            mblnShowInterdictionDate = value
            setParamVal("ShowInterdictionDate", mblnShowInterdictionDate)
        End Set
    End Property

    Private mblnNotificationOnDisciplineFiling As Boolean = True
    Public Property _NotificationOnDisciplineFiling() As Boolean
        Get
            Return mblnNotificationOnDisciplineFiling
        End Get
        Set(ByVal value As Boolean)
            mblnNotificationOnDisciplineFiling = value
            setParamVal("NotificationOnDisciplineFiling", mblnNotificationOnDisciplineFiling)
        End Set
    End Property

    Private mblnShowReponseTypeOnPosting As Boolean = True
    Public Property _ShowReponseTypeOnPosting() As Boolean
        Get
            Return mblnShowReponseTypeOnPosting
        End Get
        Set(ByVal value As Boolean)
            mblnShowReponseTypeOnPosting = value
            setParamVal("ShowReponseTypeOnPosting", mblnShowReponseTypeOnPosting)
        End Set
    End Property

    Private mblnAddProceedingAgainstEachCount As Boolean = True
    Public Property _AddProceedingAgainstEachCount() As Boolean
        Get
            Return mblnAddProceedingAgainstEachCount
        End Get
        Set(ByVal value As Boolean)
            mblnAddProceedingAgainstEachCount = value
            setParamVal("ProceedingAgainstEachCount", mblnAddProceedingAgainstEachCount)
        End Set
    End Property
    'S.SANDEEP |01-OCT-2019| -- END


    'Gajanan [19-OCT-2019] -- Start    
    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
    Private mstrAddEditUserNotificationUserIds As String = ""
    Public Property _AddEditUserNotificationUserIds() As String
        Get
            Return mstrAddEditUserNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrAddEditUserNotificationUserIds = value
            setParamVal("AddEditUserNotificationUserIds", mstrAddEditUserNotificationUserIds)
        End Set
    End Property
    'Gajanan [19-OCT-2019] -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Private mintInterviewSchedulingTemplateId As Integer
    Public Property _InterviewSchedulingTemplateId() As Integer
        Get
            Return mintInterviewSchedulingTemplateId
        End Get
        Set(ByVal value As Integer)
            mintInterviewSchedulingTemplateId = value
            setParamVal("InterviewSchedulingTemplateId", mintInterviewSchedulingTemplateId)
        End Set
    End Property
    'Hemant (07 Oct 2019) -- End
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Private mintBatchSchedulingTemplateId As Integer
    Public Property _BatchSchedulingTemplateId() As Integer
        Get
            Return mintBatchSchedulingTemplateId
        End Get
        Set(ByVal value As Integer)
            mintBatchSchedulingTemplateId = value
            setParamVal("BatchSchedulingTemplateId", mintBatchSchedulingTemplateId)
        End Set
    End Property
    'Hemant (07 Oct 2019) -- End

    'Gajanan [26-OCT-2019] -- Start    
    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
    Private mblnApplyMigrationEnforcement As Boolean = False
    Public Property _ApplyMigrationEnforcement() As Boolean
        Get
            Return mblnApplyMigrationEnforcement
        End Get
        Set(ByVal value As Boolean)
            mblnApplyMigrationEnforcement = value
            setParamVal("ApplyMigrationEnforcement", mblnApplyMigrationEnforcement)
        End Set
    End Property
    'Gajanan [26-OCT-2019] -- End



    'Gajanan [1-NOV-2019] -- Start    
    'Enhancement:Worked On NMB ESS Comment  

    Private mblnDomicileAddressDocsAttachmentMandatory As Boolean = False
    Public Property _DomicileDocsAttachmentMandatory() As Boolean
        Get
            Return mblnDomicileAddressDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnDomicileAddressDocsAttachmentMandatory = value
            setParamVal("DomicileAddressDocsAttachmentMandatory", mblnDomicileAddressDocsAttachmentMandatory)
        End Set
    End Property


    Private mblnPresentAddressDocsAttachmentMandatory As Boolean = False
    Public Property _PresentAddressDocsAttachmentMandatory() As Boolean
        Get
            Return mblnPresentAddressDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnPresentAddressDocsAttachmentMandatory = value
            setParamVal("PresentAddressDocsAttachmentMandatory", mblnPresentAddressDocsAttachmentMandatory)
        End Set
    End Property

    Private mblnRecruitmentAddressDocsAttachmentMandatory As Boolean = False
    Public Property _RecruitmentAddressDocsAttachmentMandatory() As Boolean
        Get
            Return mblnRecruitmentAddressDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnRecruitmentAddressDocsAttachmentMandatory = value
            setParamVal("RecruitmentAddressDocsAttachmentMandatory", mblnRecruitmentAddressDocsAttachmentMandatory)
        End Set
    End Property

    Private mblnIdentityDocsAttachmentMandatory As Boolean = False
    Public Property _IdentityDocsAttachmentMandatory() As Boolean
        Get
            Return mblnIdentityDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnIdentityDocsAttachmentMandatory = value
            setParamVal("IdentityDocsAttachmentMandatory", mblnIdentityDocsAttachmentMandatory)
        End Set
    End Property

    Private mblnJobExperienceDocsAttachmentMandatory As Boolean = False
    Public Property _JobExperienceDocsAttachmentMandatory() As Boolean
        Get
            Return mblnJobExperienceDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnJobExperienceDocsAttachmentMandatory = value
            setParamVal("JobExperienceDocsAttachmentMandatory", mblnJobExperienceDocsAttachmentMandatory)
        End Set
    End Property
    'Gajanan [1-NOV-2019] -- End

    'Hemant (30 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
    Private mintJobConfirmationTemplateId As Integer
    Public Property _JobConfirmationTemplateId() As Integer
        Get
            Return mintJobConfirmationTemplateId
        End Get
        Set(ByVal value As Integer)
            mintJobConfirmationTemplateId = value
            setParamVal("JobConfirmationTemplateId", mintJobConfirmationTemplateId)
        End Set
    End Property
    Private mintJobAlertTemplateId As Integer
    Public Property _JobAlertTemplateId() As Integer
        Get
            Return mintJobAlertTemplateId
        End Get
        Set(ByVal value As Integer)
            mintJobAlertTemplateId = value
            setParamVal("JobAlertTemplateId", mintJobAlertTemplateId)
        End Set
    End Property

    Private mintInternalJobAlertTemplateId As Integer
    Public Property _InternalJobAlertTemplateId() As Integer
        Get
            Return mintInternalJobAlertTemplateId
        End Get
        Set(ByVal value As Integer)
            mintInternalJobAlertTemplateId = value
            setParamVal("InternalJobAlertTemplateId", mintInternalJobAlertTemplateId)
        End Set
    End Property
    'Hemant (30 Oct 2019) -- End

    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   




    'Gajanan [15-NOV-2019] -- Start   
    'Enhancement:Worked On Staff Requisition New Attachment Mandatory Option FOR NMB   

    'Private mblnStaffRequisitionDocsAttachmentMandatory As Boolean = False
    'Public Property _StaffRequisitionDocsAttachmentMandatory() As Boolean
    '    Get
    '        Return mblnStaffRequisitionDocsAttachmentMandatory
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnStaffRequisitionDocsAttachmentMandatory = value
    '        setParamVal("StaffRequisitionDocsAttachmentMandatory", mblnStaffRequisitionDocsAttachmentMandatory)
    '    End Set
    'End Property


    Private mblnAdditionalStaffRequisitionDocsAttachmentMandatory As Boolean = False
    Public Property _AdditionalStaffRequisitionDocsAttachmentMandatory() As Boolean
        Get
            Return mblnAdditionalStaffRequisitionDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnAdditionalStaffRequisitionDocsAttachmentMandatory = value
            setParamVal("AdditionalStaffRequisitionDocsAttachmentMandatory", mblnAdditionalStaffRequisitionDocsAttachmentMandatory)
        End Set
    End Property

    Private mblnAdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory As Boolean = False
    Public Property _AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory() As Boolean
        Get
            Return mblnAdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnAdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory = value
            setParamVal("AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory", mblnAdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory)
        End Set
    End Property

    Private mblnReplacementStaffRequisitionDocsAttachmentMandatory As Boolean = False
    Public Property _ReplacementStaffRequisitionDocsAttachmentMandatory() As Boolean
        Get
            Return mblnReplacementStaffRequisitionDocsAttachmentMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnReplacementStaffRequisitionDocsAttachmentMandatory = value
            setParamVal("ReplacementStaffRequisitionDocsAttachmentMandatory", mblnReplacementStaffRequisitionDocsAttachmentMandatory)
        End Set
    End Property

    'Gajanan [15-NOV-2019] -- End

    'Gajanan [6-NOV-2019] -- End


    'S.SANDEEP |25-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube
    Private mstrFailedRequestNotificationEmails As String = String.Empty
    Public Property _FailedRequestNotificationEmails() As String
        Get
            Return mstrFailedRequestNotificationEmails
        End Get
        Set(ByVal value As String)
            mstrFailedRequestNotificationEmails = value
            setParamVal("FailedRequestNotificationEmails", mstrFailedRequestNotificationEmails)
        End Set
    End Property
    'S.SANDEEP |25-NOV-2019| -- END

    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Private mintGrievanceReportingToMaxApprovalLevel As Integer = 0
    Public Property _GrievanceReportingToMaxApprovalLevel() As Integer
        Get
            Return mintGrievanceReportingToMaxApprovalLevel
        End Get
        Set(ByVal value As Integer)
            mintGrievanceReportingToMaxApprovalLevel = value
            setParamVal("GrievanceReportingToMaxApprovalLevel", mintGrievanceReportingToMaxApprovalLevel)
        End Set
    End Property

    'Gajanan [24-OCT-2019] -- End

    'S.SANDEEP |24-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
    Private mdecPAScoringRoudingFactor As Decimal = 0
    Public Property _PAScoringRoudingFactor() As Decimal
        Get
            Return mdecPAScoringRoudingFactor
        End Get
        Set(ByVal value As Decimal)
            mdecPAScoringRoudingFactor = value
            setParamVal("PAScoringRoudingFactor", mdecPAScoringRoudingFactor)
        End Set
    End Property
    'S.SANDEEP |24-DEC-2019| -- END


    'Gajanan [28-May-2020] -- Start
    Private mblnAllowToViewScoreWhileDoingAssesment As Boolean = False
    Public Property _AllowToViewScoreWhileDoingAssesment() As Boolean
        Get
            Return mblnAllowToViewScoreWhileDoingAssesment
        End Get
        Set(ByVal value As Boolean)
            mblnAllowToViewScoreWhileDoingAssesment = value
            setParamVal("AllowToViewScoreWhileDoingAssesment", mblnAllowToViewScoreWhileDoingAssesment)
        End Set
    End Property
    'Gajanan [28-May-2020] -- End

    'S.SANDEEP |27-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
    Private mblnAllowTerminationIfPaymentDone As Boolean = False
    Public Property _AllowTerminationIfPaymentDone() As Boolean
        Get
            Return mblnAllowTerminationIfPaymentDone
        End Get
        Set(ByVal value As Boolean)
            mblnAllowTerminationIfPaymentDone = value
            setParamVal("AllowTerminationIfPaymentDone", mblnAllowTerminationIfPaymentDone)
        End Set
    End Property
    'S.SANDEEP |27-JUN-2020| -- END

    'S.SANDEEP |06-JUL-2020| -- START
    'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD)
    Private mblnAllowRehireOnClosedPeriod As Boolean = False
    Public Property _AllowRehireOnClosedPeriod() As Boolean
        Get
            Return mblnAllowRehireOnClosedPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnAllowRehireOnClosedPeriod = value
            setParamVal("AllowRehireOnClosedPeriod", mblnAllowRehireOnClosedPeriod)
        End Set
    End Property
    'S.SANDEEP |06-JUL-2020| -- END

'Gajanan [1-July-2020] -- Start
    'Enhancement #ARUTI-1276 Assist to provide Email notification to some selected users when Transaction head is EDITED or ADDED
    Private mstrAddEditTransectionHeadNotificationUserIds As String = ""
    Public Property _AddEditTransectionHeadNotificationUserIds() As String
        Get
            Return mstrAddEditTransectionHeadNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrAddEditTransectionHeadNotificationUserIds = value
            setParamVal("AddEditTransectionHeadNotificationUserIds", mstrAddEditTransectionHeadNotificationUserIds)
        End Set
    End Property

    Private mblnOnImportIncludeTransectionHeadWhileSendingNotification As Boolean = True
    Public Property _OnImportIncludeTransectionHeadWhileSendingNotification() As Boolean
        Get
            Return mblnOnImportIncludeTransectionHeadWhileSendingNotification
        End Get
        Set(ByVal value As Boolean)
            mblnOnImportIncludeTransectionHeadWhileSendingNotification = value
            setParamVal("OnImportIncludeTransectionHeadWhileSendingNotification", mblnOnImportIncludeTransectionHeadWhileSendingNotification)
        End Set
    End Property
    'Gajanan [1-July-2020] -- End


    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Private mdtRunDailyAttendanceServiceTime As DateTime = Nothing
    Public Property _RunDailyAttendanceServiceTime() As DateTime
        Get
            Return mdtRunDailyAttendanceServiceTime
        End Get
        Set(ByVal value As DateTime)
            mdtRunDailyAttendanceServiceTime = value
            setParamVal("RunDailyAttendanceServiceTime", mdtRunDailyAttendanceServiceTime)
        End Set
    End Property

    Private mstrSendTnAEarlyLateReportsUserIds As String = ""
    Public Property _SendTnAEarlyLateReportsUserIds() As String
        Get
            Return mstrSendTnAEarlyLateReportsUserIds
        End Get
        Set(ByVal value As String)
            mstrSendTnAEarlyLateReportsUserIds = value
            setParamVal("SendTnAEarlyLateReportsUserIds", mstrSendTnAEarlyLateReportsUserIds)
        End Set
    End Property

    Private mstrTnAFailureNotificationUserIds As String = ""
    Public Property _TnAFailureNotificationUserIds() As String
        Get
            Return mstrTnAFailureNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrTnAFailureNotificationUserIds = value
            setParamVal("TnAFailureNotificationUserIds", mstrTnAFailureNotificationUserIds)
        End Set
    End Property


    'Pinkal (27-Oct-2020) -- End


    'S.SANDEEP |10-AUG-2021| -- START
    Dim mdicBSC_StatusColors As New Dictionary(Of Integer, Integer)
    Public Property _BSC_StatusColors() As Dictionary(Of Integer, Integer)
        Get
            Return mdicBSC_StatusColors
        End Get
        Set(ByVal value As Dictionary(Of Integer, Integer))
            mdicBSC_StatusColors = value
            For Each ikey As Integer In mdicBSC_StatusColors.Keys
                setParamVal("_eStat_" & ikey.ToString(), mdicBSC_StatusColors(ikey))
            Next
        End Set
    End Property

    Dim mblnConsiderZeroAsNumUpdateProgress As Boolean = False
    Public Property _ConsiderZeroAsNumUpdateProgress() As Boolean
        Get
            Return mblnConsiderZeroAsNumUpdateProgress
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderZeroAsNumUpdateProgress = value
            setParamVal("ConsiderZeroAsNumUpdateProgress", mblnConsiderZeroAsNumUpdateProgress)
        End Set
    End Property
    'S.SANDEEP |10-AUG-2021| -- END


#End Region


    'S.SANDEEP [ 11 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {WEB CHANGES}
#Region " Auto Update "

    Public Function Insert_Update_Setting(ByVal intCompanyId As Integer, ByVal mblnIsAutoUpdateEnabled As Boolean) As Boolean
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1
        Try
            Dim objDataOpr As New clsDataOperation
            StrQ = "SELECT * FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = '" & intCompanyId & "' "
            iCnt = objDataOpr.RecordCount(StrQ)

            If iCnt <= 0 Then
                StrQ = "INSERT INTO hrmsConfiguration..cfconfiguration (key_name, key_value, companyunkid) " & _
                       "SELECT 'Auto_Update_Enabled','" & mblnIsAutoUpdateEnabled & "',-200 "
            Else
                StrQ = "UPDATE hrmsConfiguration..cfconfiguration SET key_value = '" & mblnIsAutoUpdateEnabled & "' WHERE companyunkid = '" & intCompanyId & "' "
            End If

            objDataOpr.ExecNonQuery(StrQ)

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            Return True

        Catch ex As Exception
            Throw New Exception("Function Insert_Update_Setting : " & ex.Message & "ModuleName :" & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_Auto_Update_Setting(ByVal intCompanyUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mblnValue As Boolean = False
        Try
            Dim objDataOpr As New clsDataOperation

            StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = '" & intCompanyUnkid & "'"

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mblnValue = CBool(dsList.Tables(0).Rows(0)("key_value"))
            End If

            Return mblnValue

        Catch ex As Exception
            Throw New Exception("Function Get_Auto_Update_Setting : " & ex.Message & "ModuleName :" & mstrModuleName)
        Finally
        End Try
    End Function


#End Region
    'S.SANDEEP [ 11 APRIL 2012 ] -- END

    'S.SANDEEP [ 08 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " ERC NOTIFICATION "

    Private mblnERC_SendNtf_Manually As Boolean = True
    Private mblnERC_ResendNft_Manually As Boolean = False

    Private mstrERC_EOC_Setting As String = String.Empty
    Private mstrERC_RET_Setting As String = String.Empty
    Private mstrERC_CNF_Setting As String = String.Empty
    Private mstrERC_PED_Setting As String = String.Empty
    Private mstrERC_BRT_Setting As String = String.Empty
    'S.SANDEEP [ 16 NOV 2013 ] -- START
    Private mstrERC_ID_Setting As String = String.Empty
    'S.SANDEEP [ 16 NOV 2013 ] -- END

    Private mintERC_EOC_TemplateId As Integer = 0
    Private mintERC_RET_TemplateId As Integer = 0
    Private mintERC_CNF_TemplateId As Integer = 0
    Private mintERC_PED_TemplateId As Integer = 0
    Private mintERC_BRT_TemplateId As Integer = 0
    'S.SANDEEP [ 16 NOV 2013 ] -- START
    Private mintERC_ID_TemplateId As Integer = 0
    'S.SANDEEP [ 16 NOV 2013 ] -- END

    Public Property _ERC_SendNtf_Manually() As Boolean
        Get
            Return mblnERC_SendNtf_Manually
        End Get
        Set(ByVal value As Boolean)
            mblnERC_SendNtf_Manually = value
            setParamVal("ERC_SendNtf_Manually", mblnERC_SendNtf_Manually)
        End Set
    End Property

    Public Property _ERC_ResendNft_Manually() As Boolean
        Get
            Return mblnERC_ResendNft_Manually
        End Get
        Set(ByVal value As Boolean)
            mblnERC_ResendNft_Manually = value
            setParamVal("ERC_ResendNft_Manually", mblnERC_ResendNft_Manually)
        End Set
    End Property

    Public Property _ERC_EOC_Setting() As String
        Get
            Return mstrERC_EOC_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_EOC_Setting = value
            setParamVal("ERC_EOC_Setting", mstrERC_EOC_Setting)
        End Set
    End Property

    Public Property _ERC_RET_Setting() As String
        Get
            Return mstrERC_RET_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_RET_Setting = value
            setParamVal("ERC_RET_Setting", mstrERC_RET_Setting)
        End Set
    End Property

    Public Property _ERC_CNF_Setting() As String
        Get
            Return mstrERC_CNF_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_CNF_Setting = value
            setParamVal("ERC_CNF_Setting", mstrERC_CNF_Setting)
        End Set
    End Property

    Public Property _ERC_PED_Setting() As String
        Get
            Return mstrERC_PED_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_PED_Setting = value
            setParamVal("ERC_PED_Setting", mstrERC_PED_Setting)
        End Set
    End Property

    Public Property _ERC_BRT_Setting() As String
        Get
            Return mstrERC_BRT_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_BRT_Setting = value
            setParamVal("ERC_BRT_Setting", mstrERC_BRT_Setting)
        End Set
    End Property

    'S.SANDEEP [ 16 NOV 2013 ] -- START
    Public Property _ERC_ID_Setting() As String
        Get
            Return mstrERC_ID_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_ID_Setting = value
            setParamVal("ERC_ID_Setting", mstrERC_ID_Setting)
        End Set
    End Property
    'S.SANDEEP [ 16 NOV 2013 ] -- END

    Public Property _ERC_EOC_TemplateId() As Integer
        Get
            Return mintERC_EOC_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_EOC_TemplateId = value
            setParamVal("ERC_EOC_TemplateId", mintERC_EOC_TemplateId)
        End Set
    End Property

    Public Property _ERC_RET_TemplateId() As Integer
        Get
            Return mintERC_RET_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_RET_TemplateId = value
            setParamVal("ERC_RET_TemplateId", mintERC_RET_TemplateId)
        End Set
    End Property

    Public Property _ERC_CNF_TemplateId() As Integer
        Get
            Return mintERC_CNF_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_CNF_TemplateId = value
            setParamVal("ERC_CNF_TemplateId", mintERC_CNF_TemplateId)
        End Set
    End Property

    Public Property _ERC_PED_TemplateId() As Integer
        Get
            Return mintERC_PED_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_PED_TemplateId = value
            setParamVal("ERC_PED_TemplateId", mintERC_PED_TemplateId)
        End Set
    End Property

    Public Property _ERC_BRT_TemplateId() As Integer
        Get
            Return mintERC_BRT_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_BRT_TemplateId = value
            setParamVal("ERC_BRT_TemplateId", mintERC_BRT_TemplateId)
        End Set
    End Property

    'S.SANDEEP [ 16 NOV 2013 ] -- START
    Public Property _ERC_ID_TemplateId() As Integer
        Get
            Return mintERC_ID_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_ID_TemplateId = value
            setParamVal("ERC_ID_TemplateId", mintERC_ID_TemplateId)
        End Set
    End Property
    'S.SANDEEP [ 16 NOV 2013 ] -- END

    'S.SANDEEP [14-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#187|#ARUTI-109}
    Private mstrERC_LeaveEnd_Setting = ""
    Private mintERC_LeaveEnd_TemplateId = 0

    Public Property _ERC_LeaveEnd_Setting() As String
        Get
            Return mstrERC_LeaveEnd_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_LeaveEnd_Setting = value
            setParamVal("ERC_LEAVEEND_SETTING", mstrERC_LeaveEnd_Setting)
        End Set
    End Property

    Public Property _ERC_LeaveEnd_TemplateId() As Integer
        Get
            Return mintERC_LeaveEnd_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_LeaveEnd_TemplateId = value
            setParamVal("ERC_LEAVEEND_TEMPLATEID", mintERC_LeaveEnd_TemplateId)
        End Set
    End Property
    'S.SANDEEP [14-Apr-2018] -- END


    'Pinkal (16-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mstrERC_LeavePlanner_Setting = ""
    Private mintERC_LeavePlanner_TemplateId = 0

    Public Property _ERC_LeavePlanner_Setting() As String
        Get
            Return mstrERC_LeavePlanner_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_LeavePlanner_Setting = value
            setParamVal("ERC_LEAVEPLANNER_SETTING", mstrERC_LeavePlanner_Setting)
        End Set
    End Property

    Public Property _ERC_LeavePlanner_TemplateId() As Integer
        Get
            Return mintERC_LeavePlanner_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_LeavePlanner_TemplateId = value
            setParamVal("ERC_LEAVEPLANNER_TEMPLATEID", mintERC_LeavePlanner_TemplateId)
        End Set
    End Property
    'Pinkal (16-Oct-2018) -- End


    'Pinkal (03-Dec-2018) -- Start
    'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.

    Private mstrERC_AssetDecFromDate_Setting = ""
    Private mintERC_AssetDecFromDate_TemplateId = 0

    Public Property _ERC_AssetDecFromDate_Setting() As String
        Get
            Return mstrERC_AssetDecFromDate_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_AssetDecFromDate_Setting = value
            setParamVal("ERC_ASSETDECFROMDATE_SETTING", mstrERC_AssetDecFromDate_Setting)
        End Set
    End Property

    Public Property _ERC_AssetDecFromDate_TemplateId() As Integer
        Get
            Return mintERC_AssetDecFromDate_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_AssetDecFromDate_TemplateId = value
            setParamVal("ERC_ASSETDECFROMDATE_TEMPLATEID", mintERC_AssetDecFromDate_TemplateId)
        End Set
    End Property


    Private mstrERC_AssetDecToDate_Setting = ""
    Private mintERC_AssetDecToDate_TemplateId = 0

    Public Property _ERC_AssetDecToDate_Setting() As String
        Get
            Return mstrERC_AssetDecToDate_Setting
        End Get
        Set(ByVal value As String)
            mstrERC_AssetDecToDate_Setting = value
            setParamVal("ERC_ASSETDECTODATE_SETTING", mstrERC_AssetDecToDate_Setting)
        End Set
    End Property

    Public Property _ERC_AssetDecToDate_TemplateId() As Integer
        Get
            Return mintERC_AssetDecToDate_TemplateId
        End Get
        Set(ByVal value As Integer)
            mintERC_AssetDecToDate_TemplateId = value
            setParamVal("ERC_ASSETDECTODATE_TEMPLATEID", mintERC_AssetDecToDate_TemplateId)
        End Set
    End Property

    'Pinkal (03-Dec-2018) -- End



    'S.SANDEEP [18-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#290}
    Private mstrPAEmpNtfSettingValue As String = ""
    Private mstrPAAsrNtfSettingValue As String = ""
    Private mstrPARevNtfSettingValue As String = ""

    Public Property _ERC_PAEmpNtfSetting() As String
        Get
            Return mstrPAEmpNtfSettingValue
        End Get
        Set(ByVal value As String)
            mstrPAEmpNtfSettingValue = value
            setParamVal("PAEmpNtfSettingValue", mstrPAEmpNtfSettingValue)
        End Set
    End Property

    Public Property _ERC_PAAsrNtfSetting() As String
        Get
            Return mstrPAAsrNtfSettingValue
        End Get
        Set(ByVal value As String)
            mstrPAAsrNtfSettingValue = value
            setParamVal("PAAsrNtfSettingValue", mstrPAAsrNtfSettingValue)
        End Set
    End Property

    Public Property _ERC_PARevNtfSetting() As String
        Get
            Return mstrPARevNtfSettingValue
        End Get
        Set(ByVal value As String)
            mstrPARevNtfSettingValue = value
            setParamVal("PARevNtfSettingValue", mstrPARevNtfSettingValue)
        End Set
    End Property
    'S.SANDEEP [18-AUG-2018] -- END

#End Region
    'S.SANDEEP [ 08 APR 2013 ] -- END

    'S.SANDEEP [ 28 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Create_Image_Database() As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOp As New clsDataOperation

            StrQ = "IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'arutiimages') " & _
                   "BEGIN " & _
                         "CREATE DATABASE arutiimages " & _
                         "EXECUTE('USE [arutiimages] " & _
                         "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[hremployee_images]'') AND type in (N''U'')) " & _
                         "BEGIN " & _
                         "CREATE TABLE [dbo].[hremployee_images]( " & _
                              "[employeeimgunkid] [int] IDENTITY(1,1) NOT NULL, " & _
                              "[companyunkid] [int] NULL, " & _
                              "[employeeunkid] [int] NULL, " & _
                              "[photo] [image] NULL, " & _
                              "[userunkid] [int] NULL, " & _
                              "[isvoid] [bit] NULL, " & _
                              "[voiduserunkid] [int] NULL, " & _
                              "[voiddatetime] [datetime] NULL, " & _
                              "[loginemployeeunkid] [int] NULL, " & _
                              "[voidloginemployeeunkid] [int] NULL, " & _
                              "[symsignature] [image] NULL, " & _
                          "CONSTRAINT [PK_hremp_images] PRIMARY KEY CLUSTERED " & _
                         "( " & _
                              "[employeeimgunkid] ASC " & _
                         ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
                         ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] " & _
                         "END " & _
                         "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[hrdependant_images]'') AND type in (N''U'')) " & _
                         "BEGIN " & _
                         "CREATE TABLE [dbo].[hrdependant_images]( " & _
                              "[dependantimgunkid] [int] IDENTITY(1,1) NOT NULL, " & _
                              "[companyunkid] [int] NULL, " & _
                              "[employeeunkid] [int] NULL, " & _
                              "[dependantunkid] [int] NULL, " & _
                              "[photo] [image] NULL, " & _
                              "[userunkid] [int] NULL, " & _
                              "[isvoid] [bit] NULL, " & _
                              "[voiduserunkid] [int] NULL, " & _
                              "[voiddatetime] [datetime] NULL, " & _
                              "[loginemployeeunkid] [int] NULL, " & _
                              "[voidloginemployeeunkid] [int] NULL, " & _
                          "CONSTRAINT [PK_hrdependant_images] PRIMARY KEY CLUSTERED " & _
                         "( " & _
                              "[dependantimgunkid] ASC " & _
                         ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
                         ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] " & _
                         "END " & _
                         "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[rcapplicant_images]'') AND type in (N''U'')) " & _
                         "BEGIN " & _
                         "CREATE TABLE [dbo].[rcapplicant_images]( " & _
                              "[applicantimgunkid] [int] IDENTITY(1,1) NOT NULL, " & _
                              "[companyunkid] [int] NULL, " & _
                              "[applicantunkid] [int] NULL, " & _
                              "[photo] [image] NULL, " & _
                              "[userunkid] [int] NULL, " & _
                              "[isvoid] [bit] NULL, " & _
                              "[voiduserunkid] [int] NULL, " & _
                              "[voiddatetime] [datetime] NULL, " & _
                          "CONSTRAINT [PK_rcapplicant_images] PRIMARY KEY CLUSTERED " & _
                         "( " & _
                              "[applicantimgunkid] ASC " & _
                         ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
                         ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] " & _
                         "END " & _
                         "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[athremployee_images]'') AND type in (N''U'')) " & _
                         "BEGIN " & _
                         "CREATE TABLE [dbo].[athremployee_images]( " & _
                              "[atemployeeimgunkid] [int] IDENTITY(1,1) NOT NULL, " & _
                              "[employeeimgunkid] [int] NULL, " & _
                              "[companyunkid] [int] NULL, " & _
                              "[employeeunkid] [int] NULL, " & _
                              "[audittype] [int] NULL, " & _
                              "[audituserunkid] [int] NULL, " & _
                              "[auditdatetime] [datetime] NULL, " & _
                              "[ip] [nvarchar](100) NULL, " & _
                              "[machine_name] [nvarchar](500) NOT NULL, " & _
                              "[form_name] [nvarchar](500) NULL, " & _
                              "[module_name1] [nvarchar](500) NULL, " & _
                              "[module_name2] [nvarchar](500) NULL, " & _
                              "[module_name3] [nvarchar](500) NULL, " & _
                              "[module_name4] [nvarchar](500) NULL, " & _
                              "[module_name5] [nvarchar](500) NULL, " & _
                              "[isweb] [bit] NULL, " & _
                              "[loginemployeeunkid] [int] NULL, " & _
                          "CONSTRAINT [PK_athremployee_images] PRIMARY KEY CLUSTERED " & _
                         "( " & _
                              "[atemployeeimgunkid] ASC " & _
                         ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
                         ") ON [PRIMARY] " & _
                         "END " & _
                         "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[athrdependant_images]'') AND type in (N''U'')) " & _
                         "BEGIN " & _
                         "CREATE TABLE [dbo].[athrdependant_images]( " & _
                              "[atdependantimgunkid] [int] IDENTITY(1,1) NOT NULL, " & _
                              "[dependantimgunkid] [int] NULL, " & _
                              "[companyunkid] [int] NULL, " & _
                              "[employeeunkid] [int] NULL, " & _
                              "[dependantunkid] [int] NULL, " & _
                              "[audittype] [int] NULL, " & _
                              "[audituserunkid] [int] NULL, " & _
                              "[auditdatetime] [datetime] NULL, " & _
                              "[ip] [nvarchar](100) NULL, " & _
                              "[machine_name] [nvarchar](500) NULL, " & _
                              "[form_name] [nvarchar](500) NULL, " & _
                              "[module_name1] [nvarchar](500) NULL, " & _
                              "[module_name2] [nvarchar](500) NULL, " & _
                              "[module_name3] [nvarchar](500) NULL, " & _
                              "[module_name4] [nvarchar](500) NULL, " & _
                              "[module_name5] [nvarchar](500) NULL, " & _
                              "[isweb] [bit] NULL, " & _
                              "[loginemployeeunkid] [int] NULL, " & _
                          "CONSTRAINT [PK_athrdependant_images] PRIMARY KEY CLUSTERED " & _
                         "( " & _
                              "[atdependantimgunkid] ASC " & _
                         ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
                         ") ON [PRIMARY] " & _
                         "END " & _
                         "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[atrcapplicant_images]'') AND type in (N''U'')) " & _
                         "BEGIN " & _
                         "CREATE TABLE [dbo].[atrcapplicant_images]( " & _
                              "[atapplicantimgunkid] [int] IDENTITY(1,1) NOT NULL, " & _
                              "[applicantimgunkid] [int] NULL, " & _
                              "[companyunkid] [int] NULL, " & _
                              "[applicantunkid] [int] NULL, " & _
                              "[photo] [image] NULL, " & _
                              "[audittype] [int] NULL, " & _
                              "[audituserunkid] [int] NULL, " & _
                              "[auditdatetime] [datetime] NULL, " & _
                              "[ip] [nvarchar](100) NULL, " & _
                              "[machine_name] [nvarchar](500) NULL, " & _
                              "[form_name] [nvarchar](500) NULL, " & _
                              "[module_name1] [nvarchar](500) NULL, " & _
                              "[module_name2] [nvarchar](500) NULL, " & _
                              "[module_name3] [nvarchar](500) NULL, " & _
                              "[module_name4] [nvarchar](500) NULL, " & _
                              "[module_name5] [nvarchar](500) NULL, " & _
                              "[isweb] [bit] NULL, " & _
                              "[loginemployeeunkid] [int] NULL, " & _
                          "CONSTRAINT [PK_atrcapplicant_images] PRIMARY KEY CLUSTERED " & _
                         "( " & _
                              "[atapplicantimgunkid] ASC " & _
                         ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " & _
                         ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] " & _
                         "END') " & _
                    "END "
            'S.SANDEEP [11-AUG-2017] -- START {symsignature} -- END
            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Dim objDo As New clsDataOperation

            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            'objDo.ExecNonQuery("USE master")
            'objDo.ExecNonQuery("IF EXISTS (SELECT * FROM sys.databases WHERE name = 'arutiimages' ) DROP DATABASE arutiimages")
            'Pinkal (06-May-2016) -- End
            objDo = Nothing
            DisplayError.Show("-1", ex.Message, "Create_Image_Database", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 28 MAR 2013 ] -- END


    'S.SANDEEP [ 22 MAY 2014 ] -- START
    Private Sub SetDefaultData()
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            If mintCompanyUnkid > 0 Then
                Dim objDataOp As New clsDataOperation
                StrQ = "DECLARE @CompId AS INT SET @CompId = " & mintCompanyUnkid & " " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'PayslipVocNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'PayslipVocNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextPayslipVocNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextPayslipVocNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'PaymentVocNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'PaymentVocNoType' ) " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'PaymentVocPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'PaymentVocPrefix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextPaymentVocNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextPaymentVocNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'LoanVocNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'LoanVocNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'LoanVocPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'LoanVocPrefix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextLoanVocNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextLoanVocNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'SavingsVocNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'SavingsVocNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'SavingsVocPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'SavingsVocPrefix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextSavingsVocNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextSavingsVocNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'LoanApplicationNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'LoanApplicationNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'LoanApplicationPrifix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'LoanApplicationPrifix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextLoanApplicationNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextLoanApplicationNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'LeaveFormNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'LeaveFormNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'LeaveFormNoPrifix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'LeaveFormNoPrifix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextLeaveFormNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextLeaveFormNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'EmployeeCodeNotype',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'EmployeeCodeNotype') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'EmployeeCodePrifix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'EmployeeCodePrifix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextEmployeeCodeNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextEmployeeCodeNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'ApplicantCodeNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'ApplicantCodeNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'ApplicantCodePrifix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'ApplicantCodePrifix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextApplicantCodeNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextApplicantCodeNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'SickSheetNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'SickSheetNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'SickSheetPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'SickSheetPrefix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextSickSheetNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextSickSheetNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'BatchPostingNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'BatchPostingNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'BatchPostingPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'BatchPostingPrefix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextBatchPostingNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextBatchPostingNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'StaffReqFormNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'StaffReqFormNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'StaffReqFormNoPrifix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'StaffReqFormNoPrifix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextStaffReqFormNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextStaffReqFormNo') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'DisciplineRefNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'DisciplineRefNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextDisciplineRefNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextDisciplineRefNo') "

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'ScoreCalibrationFormNotype',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'ScoreCalibrationFormNotype') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'ScoreCalibrationFormNoPrifix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'ScoreCalibrationFormNoPrifix') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextScoreCalibrationFormNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextScoreCalibrationFormNo') "
                'S.SANDEEP |27-MAY-2019| -- END

                'S.SANDEEP [25-JAN-2017] -- START
                'ISSUE/ENHANCEMENT :
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'ReviewerScoreSetting','" & enReviewerScoreSetting.ReviewerScoreNeeded & "',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'ReviewerScoreSetting') " '
                'S.SANDEEP [25-JAN-2017] -- END

                'Gajanan [13-AUG-2018] -- Start
                'Enhancement - Implementing Grievance Module.
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'GrievanceRefNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'GrievanceRefNoType') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextGrievanceRefNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextGrievanceRefNo') "
                'Gajanan [13-AUG-2018] -- End

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : New Screen Training Need Form.
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'TrainingNeedFormNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'TrainingNeedFormNoType') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'TrainingNeedFormNoPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'TrainingNeedFormNoPrefix') " & _
                       "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextTrainingNeedFormNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextTrainingNeedFormNo') "
                'Sohail (14 Nov 2019) -- End

                'S.SANDEEP [24 MAY 2016] -- START (DisciplineRefNoType,NextDisciplineRefNo) -- END
                'Sohail (28 May 2014) -- Start
                'Enhancement - Staff Requisition.
               
                'Sohail (28 May 2014) -- End


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'SkipApprovalOnEmpData','" & mstrSkipApprovalOnEmpDatavalue & "',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'SkipApprovalOnEmpData') "

                'Gajanan |30-MAR-2019| -- START
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'OldSkipApprovalOnEmpData','" & mstrOldSkipApprovalOnEmpData & "',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'OldSkipApprovalOnEmpData') "
                Dim SkipApprovalData As List(Of String) = mstrSkipApprovalOnEmpDatavalue.Split(","c).ToList()
                Dim UsedSkipApprovalData As List(Of String) = GetKeyValue(mintCompanyUnkid, "OldSkipApprovalOnEmpData").Split(","c).ToList()
                Dim finalSkipApprovalData As List(Of String) = SkipApprovalData.Except(UsedSkipApprovalData).ToList()
                Dim NewEmpSkipApprovalDataValue As String = String.Join(",", finalSkipApprovalData.ToArray())
                StrQ &= "Update hrmsConfiguration..cfconfiguration set key_value = '" & NewEmpSkipApprovalDataValue & "' WHERE companyunkid = @CompId AND key_name = 'SkipApprovalOnEmpData' "
                'Gajanan |30-MAR-2019| -- END

                'Gajanan [17-DEC-2018] -- End

                'Sohail (02 Mar 2019) -- Start
                'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'BRJVVocNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'BRJVVocNoType') " & _
                          "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'BRJVVocPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'BRJVVocPrefix') " & _
                          "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextBRJVVocNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextBRJVVocNo') "
				'Sohail (14 Mar 2019) - [BRJVnVocPrefix] = [BRJVVocPrefix]
                'Sohail (02 Mar 2019) -- End

                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'EFTVocNoType',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'EFTVocNoType') " & _
                         "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'EFTVocPrefix','',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'EFTVocPrefix') " & _
                         "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT 'NextEFTVocNo',1,@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = 'NextEFTVocNo') "
                'Hemant (11 Dec 2019) -- End


                'S.SANDEEP |14 DEC 2021| -- START
                StrQ &= "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_1','-16744448',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_1') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_2','-16776961',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_2') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_3','-16777216',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_3') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_4','-65536',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_4') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_5','-8388480',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_5') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_6','-16776961',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_6') " & _
                        "INSERT INTO hrmsConfiguration..cfconfiguration (key_name,key_value,companyunkid) SELECT '_eStat_7','-5952982',@CompId WHERE NOT EXISTS(SELECT key_name FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = @CompId AND key_name = '_eStat_7') "
                'S.SANDEEP |14 DEC 2021| -- END

                objDataOp.ExecNonQuery(StrQ)

                If objDataOp.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    Throw exForce
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultData", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 22 MAY 2014 ] -- END


    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.

#Region "Handpunch Configuration "

    Public Function Create_Handpunch_Database() As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOp As New clsDataOperation
            objDataOp.ClearParameters()

            StrQ = " IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'Handpunch_Data') " & _
                         " BEGIN " & _
                         "      CREATE DATABASE Handpunch_Data " & _
                         "      EXECUTE('USE [Handpunch_Data]  " & _
                         "      IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[Variables]'') AND type in (N''U'')) " & _
                         "      BEGIN " & _
                         "            CREATE TABLE [dbo].[Variables]( " & _
                         "            [VariableName] [varchar](30) NOT NULL," & _
                         "            [VariableValue] [nvarchar](255) NOT NULL, " & _
                         "            [Description] [nvarchar](255) NULL, " & _
                         "            CONSTRAINT [PK_Variables] PRIMARY KEY CLUSTERED ([VariableName] ASC) " & _
                         "            ) " & _
                         "          INSERT INTO Variables(VariableName, VariableValue, Description) " & _
                         "          SELECT ''DownloadLogsInterval'', ''30'', ''Download Attendance Logs Schedule Time (in minutes). Should be between 5min – 1440min (24 hours)'' UNION ALL " & _
                         "          SELECT ''DownloadUsersTime'', ''03:00'', ''The time of the day when to download users'' UNION ALL " & _
                         "          SELECT ''DownloadUsersDayOfWeek'', ''SA;WE'', ''Day(s) of the week when to download users. Possible value are: MO, TU, WE, TH, FR, SA, SU. semicolumn(;) as seporator. Ex SA;WE = Saturday and Wednesday'' " & _
                         "      END " & _
                         "      IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[DeviceList]'') AND type in (N''U'')) " & _
                         "      BEGIN " & _
                         "              CREATE TABLE [dbo].[DeviceList]( " & _
                         "              [DeviceID] [int] NOT NULL, " & _
                         "              [Active] [bit] NOT NULL CONSTRAINT DF_DeviceList_Active DEFAULT (1), " & _
                         "              [DeviceName] [nvarchar](100) NOT NULL CONSTRAINT [UQ_DeviceList_DeviceName] UNIQUE, " & _
                         "              [DeviceHost] [nvarchar](250) NOT NULL, " & _
                         "              [DevicePort] [int] NOT NULL CONSTRAINT DF_DeviceList_DevicePort DEFAULT (3001), " & _
                         "              [DeviceAddress] [tinyint] NOT NULL CONSTRAINT DF_DeviceList_DeviceAddress DEFAULT (0), " & _
                         "              [Timeout] [tinyint] NOT NULL CONSTRAINT DF_DeviceList_Timeout DEFAULT (6), " & _
                         "              CONSTRAINT [PK_DeviceList] PRIMARY KEY CLUSTERED ([DeviceID] ASC), " & _
                         "              CONSTRAINT [CK_DeviceList_DevicePort] CHECK  (DevicePort BETWEEN 1 AND 65535)) " & _
                         "      END " & _
                         "      IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[DeviceUsers]'') AND type in (N''U'')) " & _
                         "      BEGIN " & _
                         "              CREATE TABLE [dbo].[DeviceUsers]( " & _
                         "              [DeviceUserID] [bigint] NOT NULL CONSTRAINT CK_AttendanceLogs_EmployeeID  CHECK (DeviceUserID BETWEEN 1 AND 9999999999)," & _
                         "              [DeviceID] [int] NOT NULL, " & _
                         "              [UserName] [nvarchar](50) NOT NULL, " & _
                         "              [TimeZone] [tinyint] NULL, " & _
                         "              [Threshold] [tinyint] NULL, " & _
                         "              [Privilege] [tinyint] NOT NULL, " & _
                         "              [HandTemplate] [nvarchar](50) NULL, " & _
                         "              [DownloadTime] [datetime] NOT NULL, " & _
                         "              CONSTRAINT [PK_DeviceUsers] PRIMARY KEY CLUSTERED ([DeviceUserID] ASC, [DeviceID] ASC), " & _
                         "              CONSTRAINT [FK_DeviceUsers_DeviceList] FOREIGN KEY([DeviceID]) " & _
                         "              REFERENCES [dbo].[DeviceList]([DeviceID])) " & _
                         "      END " & _
                         "      IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''[dbo].[AttendanceLogs]'') AND type in (N''U'')) " & _
                         "      BEGIN " & _
                         "              CREATE TABLE [dbo].[AttendanceLogs]( " & _
                         "              [AttendanceLogID] [bigint] IDENTITY(1,1) NOT NULL, " & _
                         "              [DeviceUserID] [bigint] NOT NULL, " & _
                         "              [DeviceID] [int] NOT NULL, " & _
                         "              [AttendanceTime] [datetime] NOT NULL, " & _
                         "              [TACode] [tinyint] NULL, " & _
                         "              [DownloadTime] [datetime] NOT NULL, " & _
                         "              CONSTRAINT [PK_AttendanceLogs] PRIMARY KEY CLUSTERED ([AttendanceLogID] ASC), " & _
                         "              CONSTRAINT [FK_AttendanceLogs_DeviceList] FOREIGN KEY([DeviceID]) " & _
                         "              REFERENCES [dbo].[DeviceList]([DeviceID]))" & _
                         "      END; " & _
                         " ') " & _
                         " END "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            StrQ = " EXECUTE('USE [Handpunch_Data]') ;  IF EXISTS(SELECT * FROM sys.databases WHERE name = 'Handpunch_Data') " & _
                         " BEGIN " & _
                         "      USE [Handpunch_Data] " & _
                         "END "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            StrQ = " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PR_GetSettings]') AND type in (N'P', N'PC')) " & _
                         " BEGIN " & _
                         "          DROP PROCEDURE [PR_GetSettings]  " & _
                         " END"

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            StrQ = " CREATE PROCEDURE [PR_GetSettings] " & _
                         " AS " & _
                         " SELECT VariableName, VariableValue FROM Variables  "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            StrQ = " IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PR_GetSettings]') AND type in (N'P', N'PC')) " & _
                         " BEGIN " & _
                         "          DROP PROCEDURE [dbo].[PR_GetSettings]  " & _
                         " END "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            StrQ = " CREATE PROCEDURE [dbo].[PR_GetSettings] " & _
                         " AS " & _
                         " SELECT VariableName, VariableValue FROM Variables"


            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            StrQ = " IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PR_GetAllActiveDevices]') AND type in (N'P', N'PC')) " & _
                         " BEGIN " & _
                         "         DROP PROCEDURE [dbo].[PR_GetAllActiveDevices] " & _
                         " END ;"

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            StrQ = " CREATE PROCEDURE [dbo].[PR_GetAllActiveDevices] " & _
                         " AS " & _
                         " SELECT DeviceID, DeviceName, DeviceHost, DevicePort, DeviceAddress, Timeout FROM DeviceList WHERE  Active = 1 "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            StrQ = " IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PR_AddAttLog]') AND type in (N'P', N'PC')) " & _
                         " BEGIN " & _
                         "     DROP PROCEDURE [dbo].[PR_AddAttLog] " & _
                         " END "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            StrQ = " CREATE PROCEDURE [dbo].[PR_AddAttLog] " & _
                         "  @DeviceUserID bigint, " & _
                         "  @DeviceID int, " & _
                         "  @AttendanceTime datetime, " & _
                         "  @TACode tinyint, " & _
                         "  @DownloadTime datetime " & _
                         " AS " & _
                         "      BEGIN " & _
                         "      SET NOCOUNT ON; " & _
                         "       DECLARE @Err int, " & _
                         "       @Msg varchar(100), " & _
                         "       @ErrorParameter nvarchar(20) " & _
                         "       IF NOT EXISTS (SELECT * FROM DeviceList WHERE DeviceID = @DeviceID) " & _
                         "       BEGIN " & _
                         "              SELECT @Err=50000, " & _
                         "              @Msg='The device with ID %s is not found.', @ErrorParameter=@DeviceID " & _
                         "              GoTo HANDLE_ERROR " & _
                         "      END " & _
                         "      INSERT INTO AttendanceLogs (DeviceUserID, DeviceID, AttendanceTime, TACode, DownloadTime) " & _
                         "      VALUES (@DeviceUserID, @DeviceID, @AttendanceTime, @TACode, @DownloadTime) " & _
                         "      SET @Err=@@ERROR " & _
                         " HANDLE_ERROR: " & _
                         "  IF @Err>=50000 " & _
                         "      RAISERROR (@Msg, 16, 1, @ErrorParameter) " & _
                         "      RETURN @Err " & _
                         " END "


            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            StrQ = " IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PR_AddUpdateUser]') AND type in (N'P', N'PC')) " & _
                         " BEGIN " & _
                         "      DROP PROCEDURE [dbo].[PR_AddUpdateUser] " & _
                         " END "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            StrQ = " CREATE PROCEDURE [dbo].[PR_AddUpdateUser] " & _
                         " @DeviceUserID bigint, " & _
                         " @DeviceID int, " & _
                         " @UserName nvarchar(50), " & _
                         " @TimeZone tinyint, " & _
                         " @Threshold tinyint, " & _
                         " @Privilege tinyint, " & _
                         " @HandTemplate nvarchar(50), " & _
                         " @DownloadTime datetime " & _
                         " AS " & _
                         "      DECLARE @Err int, " & _
                         "      @Msg varchar(100), " & _
                         "      @ErrorParameter nvarchar(20) " & _
                         "      IF NOT EXISTS (SELECT * FROM DeviceList WHERE DeviceID = @DeviceID) " & _
                         "      BEGIN " & _
                         "        SELECT @Err=50000,    @Msg='The device with ID %s is not found.', @ErrorParameter=@DeviceID " & _
                         "         GoTo HANDLE_ERROR " & _
                         "       END " & _
                         "       IF EXISTS (SELECT * FROM DeviceUsers WHERE DeviceUserID = @DeviceUserID AND DeviceID = @DeviceID) " & _
                         "      BEGIN " & _
                         "              UPDATE DeviceUsers " & _
                         "              SET UserName = @UserName, " & _
                         "              TimeZone = @TimeZone, " & _
                         "              Threshold = @Threshold, " & _
                         "              Privilege = @Privilege, " & _
                         "              HandTemplate = @HandTemplate, " & _
                         "              DownloadTime = @DownloadTime " & _
                         "              WHERE DeviceUserID = @DeviceUserID AND DeviceID = @DeviceID " & _
                         "      END ELSE " & _
                         "     BEGIN " & _
                         "          INSERT INTO DeviceUsers (DeviceUserID, DeviceID, UserName, TimeZone, Threshold, Privilege, HandTemplate, DownloadTime) " & _
                         "          VALUES (@DeviceUserID, @DeviceID, @UserName, @TimeZone, @Threshold, @Privilege, @HandTemplate, @DownloadTime) " & _
                         "      END " & _
                         "	SET @Err=@@ERROR " & _
                         "  HANDLE_ERROR: " & _
                         "  IF @Err>=50000 " & _
                         "      RAISERROR (@Msg, 16, 1, @ErrorParameter) " & _
                         "  RETURN @Err ;"

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            StrQ = " IF NOT EXISTS (SELECT loginname FROM master.dbo.syslogins WHERE name = 'HandPunch_user' ) " & _
                     " BEGIN " & _
                     "    EXECUTE('USE [Handpunch_Data]');  " & _
                     "    CREATE LOGIN [HandPunch_user] WITH PASSWORD=N'aRuti_hAndpUnch999', DEFAULT_DATABASE=[HandPunch_DATA], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF" & _
                     "    EXECUTE('USE [Handpunch_Data]');  " & _
                     "    CREATE USER [HandPunch_user] FOR LOGIN [HandPunch_user] " & _
                     "    EXECUTE('USE [Handpunch_Data]');  " & _
                     "    EXEC sp_addrolemember N'db_owner', N'HandPunch_user' " & _
                     " END "


            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_Handpunch_Database; Module Name: " & mstrModuleName)
        Finally
            Dim objDo As New clsDataOperation
            objDo.ExecNonQuery("USE hrmsconfiguration")
            objDo = Nothing
        End Try
    End Function

    Public Function InsertHandpunchDevice(ByVal mstrDeviceCode As String, ByVal mstrIP As String, ByVal mstrPort As String) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim mintDeviceID As Integer = 0
        Dim exForce As Exception
        Try
            Dim objDataOp As New clsDataOperation
            objDataOp.ClearParameters()


            strQ = "SELECT ISNULL(MAX(DeviceID),0) AS DeviceID  FROM handpunch_data..DeviceList "
            Dim dsList As DataSet = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintDeviceID = CInt(dsList.Tables(0).Rows(0)("DeviceID")) + 1
            End If


            strQ = " INSERT INTO handpunch_data..DeviceList " & _
                      " (    " & _
                      "     DeviceID " & _
                      ",    DeviceName " & _
                      ",    DeviceHost " & _
                      ",    DevicePort " & _
                      ",    DeviceAddress " & _
                      ",    Timeout " & _
                      " ) " & _
                      " Values " & _
                      "(  " & _
                      "     @DeviceID " & _
                      ",    @DeviceName " & _
                      ",    @DeviceHost " & _
                      ",    @DevicePort " & _
                      ",    @DeviceAddress " & _
                      ",    @Timeout " & _
                      ") "

            objDataOp.ClearParameters()
            objDataOp.AddParameter("@DeviceID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeviceID)
            objDataOp.AddParameter("@DeviceName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceCode)
            objDataOp.AddParameter("@DeviceHost", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIP)
            objDataOp.AddParameter("@DevicePort", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPort)
            objDataOp.AddParameter("@DeviceAddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "0")
            objDataOp.AddParameter("@Timeout", SqlDbType.Int, eZeeDataType.INT_SIZE, 6)
            objDataOp.ExecNonQuery(strQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertHandpunchDevice; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return mblnFlag
    End Function

    Public Function UpdateHandpunchDeviceStatus(ByVal mstrDeviceCode As String) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOp As New clsDataOperation

            strQ = " Update handpunch_data..DeviceList SET active = 0 WHERE DeviceName = @DeviceName"
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@DeviceName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceCode.ToString())
            objDataOp.ExecNonQuery(strQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateHandpunchDeviceStatus; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return mblnFlag
    End Function

    Public Function UpdateHandpunchConfig(ByVal intLogsInterval As Integer, ByVal strUsersDayWeek As String, ByVal strDownLoadUserTime As String) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOp As New clsDataOperation

            strQ = " Update handpunch_data..Variables SET VariableValue = @VariableValue WHERE VariableName = 'DownloadLogsInterval' "
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@VariableValue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intLogsInterval.ToString())
            objDataOp.ExecNonQuery(strQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            strQ = " Update handpunch_data..Variables SET VariableValue = @VariableValue WHERE VariableName = 'DownloadUsersDayOfWeek' "
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@VariableValue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strUsersDayWeek)
            objDataOp.ExecNonQuery(strQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            strQ = " Update handpunch_data..Variables SET VariableValue = @VariableValue WHERE VariableName = 'DownloadUsersTime' "
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@VariableValue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDownLoadUserTime)
            objDataOp.ExecNonQuery(strQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateHandpunchConfig; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return mblnFlag
    End Function

    Public Function GetHandpunchConfig() As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOp As New clsDataOperation

            strQ = "SELECT * FROM sys.databases WHERE name = 'handpunch_data'"
            objDataOp.ClearParameters()
            Dim dsList As DataSet = objDataOp.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then Return dtList

            strQ = " USE handpunch_data; SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Variables]') AND type in (N'U') "
            objDataOp.ClearParameters()
            dsList = objDataOp.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then Return dtList

            dsList.Tables(0).Clear()
            strQ = " SELECT VariableName,VariableValue FROM handpunch_data..Variables"
            objDataOp.ClearParameters()
            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                dtList = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHandpunchConfig; Module Name: " & mstrModuleName)
        End Try
        Return dtList
    End Function

#End Region

    'Pinkal (06-May-2016) -- End



    'Pinkal (04-Oct-2017) -- Start
    'Enhancement - Working SAGEM Database Device Integration.

#Region "SAGEM Configuration"

    Public Function TestConnection_Sagem(ByVal strServer As String, _
                                       ByVal strDatabase As String, _
                                       ByVal strUserName As String, _
                                       ByVal strPassword As String) As Boolean
        Try
            Dim StrConn As String = ""
            Using oSQL As SqlClient.SqlConnection = New SqlClient.SqlConnection
                StrConn = "Data Source=" & strServer & ";Initial Catalog=" & strDatabase & "; User ID=" & strUserName & ";Password = " & strPassword & " "
                oSQL.ConnectionString = StrConn
                Try
                    oSQL.Open()
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message & "; Procedure Name: TestConnection_Sagem; Module Name: " & mstrModuleName)
                End Try
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: TestConnection_Sagem; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function



#End Region

    'Pinkal (04-Oct-2017) -- End


    'S.SANDEEP [14-APR-2017] -- START
    'ISSUE/ENHANCEMENT : GET ONLY ACTIVE USER FROM CHECKED USER'S LIST STORED IN CONFIGURATION {NOTIFICATION}
    'S.SANDEEP [23-OCT-2017] -- START
    Public Sub GetActiveUserList_CSV(ByVal strCSVUserListIds As String, ByRef strActiveCSVUsrIds As String)
        'S.SANDEEP [23-OCT-2017] -- END
        'Private Sub GetActiveUserList_CSV(ByVal strCSVUserListIds As String, ByRef strActiveCSVUsrIds As String)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            strActiveCSVUsrIds = ""
            'S.SANDEEP [15-JUN-2017] -- START
            'ISSUE/ENHANCEMENT : THIS CODE WILL OPTIMIZE TAKING LONG TIME WHEN MORE CSV USER ID'S ARE THERE
            Dim strvalue As String() = strCSVUserListIds.Split(",")
            strCSVUserListIds = String.Join(",", strvalue.Select(Function(x) x.ToString).Distinct().ToArray())
            'S.SANDEEP [15-JUN-2017] -- END
            Using objDataOpr As New clsDataOperation
                StrQ = "DECLARE @words VARCHAR (MAX) " & _
                       "SET @words = '" & strCSVUserListIds & "' " & _
                       "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                       "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                       "    SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                       "    WHILE @start < @stop begin " & _
                       "        SELECT " & _
                       "         @end = CHARINDEX(',',@words,@start) " & _
                       "        ,@word = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                       "        ,@start = @end+1 " & _
                       "    INSERT @split VALUES (@word) " & _
                       "    END " & _
                       "SELECT DISTINCT " & _
                       "     CAST(word AS INT) AS usrid " & _
                       "    ,ISNULL(cfuser_master.companyunkid,0) AS coId " & _
                       "    ,ISNULL(cfuser_master.employeeunkid,0) AS eId " & _
                       "    ,ISNULL(cffinancial_year_tran.database_name,'') AS dName " & _
                       "    ,ISNULL(cfconfiguration.key_value ,CONVERT(CHAR(8),GETDATE(),112)) AS eDate " & _
                       "    ,ISNULL(hrmsConfiguration..cffinancial_year_tran.isclosed,0) AS isclosed " & _
                       "FROM @split " & _
                       "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = [@split].word " & _
                       "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid " & _
                       "    LEFT JOIN hrmsConfiguration..cfconfiguration ON cfconfiguration.companyunkid = cffinancial_year_tran.companyunkid " & _
                       "    AND cfuser_master.companyunkid = cfconfiguration.companyunkid AND UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                       "    AND hrmsConfiguration..cffinancial_year_tran.isclosed = 0 " & _
                       "WHERE cfuser_master.isactive = 1 " & _
                       "ORDER BY CAST(word AS INT) "
                'S.SANDEEP [15-JUN-2017] -- START
                'ISSUE/ENHANCEMENT : THIS CODE WILL OPTIMIZE TAKING LONG TIME WHEN MORE CSV USER ID'S ARE THERE
                '-- REMOVED "SELECT " & _
                '-- ADDED "SELECT DISTINCT " & _
                'S.SANDEEP [15-JUN-2017] -- END

                'S.SANDEEP [21-JUL-2017] -- START
                '-- REMOVED : AND hrmsConfiguration..cffinancial_year_tran.isclosed = 0 -- FROM WHERE BLOCK
                '-- ADDED :  AND hrmsConfiguration..cffinancial_year_tran.isclosed = 0  -- BEFORE WHERE BLOCK
                'S.SANDEEP [21-JUL-2017] -- END

                'S.SANDEEP [01-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : Is Closed Field Added
                ' -- ADDDED hrmsConfiguration..cffinancial_year_tran.isclosed
                'S.SANDEEP [01-AUG-2017] -- END




                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If

                Dim mDicValues As New Dictionary(Of Integer, String)
                Dim iManualUsr As List(Of Integer) = Nothing
                Dim iImportUsr As List(Of Integer) = Nothing
                Dim iCommonUsr As List(Of Integer) = Nothing
                Dim strActiveUsrIds As String = String.Empty
                If dsList.Tables(0).Rows.Count > 0 Then

                    'S.SANDEEP [01-AUG-2017] -- START
                    'ISSUE/ENHANCEMENT : ISSUE FOR SAME KEY ADDED MESSAGE
                    'iManualUsr = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("coId") <= 0 AndAlso x.Field(Of Integer)("eId") <= 0).Select(Function(y) y.Field(Of Integer)("usrid")).ToList()
                    'iImportUsr = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("coId") > 0 AndAlso x.Field(Of Integer)("eId") > 0).Select(Function(y) y.Field(Of Integer)("usrid")).ToList()
                    'mDicValues = dsList.Tables(0).DefaultView.ToTable(True, "coId", "eDate", "isclosed").AsEnumerable().ToDictionary(Function(x) x.Field(Of Integer)("coId"), Function(y) y.Field(Of String)("eDate"))

                    iManualUsr = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("coId") <= 0 AndAlso x.Field(Of Integer)("eId") <= 0 AndAlso x.Field(Of Boolean)("isclosed") = False).Select(Function(y) y.Field(Of Integer)("usrid")).ToList()
                    iImportUsr = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("coId") > 0 AndAlso x.Field(Of Integer)("eId") > 0 AndAlso x.Field(Of Boolean)("isclosed") = False).Select(Function(y) y.Field(Of Integer)("usrid")).ToList()
                    mDicValues = dsList.Tables(0).DefaultView.ToTable(True, "coId", "eDate", "isclosed").AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isclosed") = False).ToDictionary(Function(x) x.Field(Of Integer)("coId"), Function(y) y.Field(Of String)("eDate"))
                    'S.SANDEEP [01-AUG-2017] -- END


                    'mDicValues = dsList.Tables(0).AsEnumerable().[Select](Function(row) New With {Key .attribute1_name = row.Field(Of Integer)("coId"), _
                    '             Key .attribute2_name = row.Field(Of String)("dName")}).Distinct().ToDictionary(Function(a) a.attribute1_name, Function(b) b.attribute2_name)

                    Dim StrFQ = "SELECT " & _
                                "    cfuser_master.userunkid " & _
                                "FROM hrmsConfiguration..cfuser_master " & _
                                "#JOIN_QUERY# " & _
                                "WHERE cfuser_master.isactive = 1 AND ISNULL(cfuser_master.companyunkid,0) = #CoId# " & _
                                "#FILTER#"

                    If mDicValues.Keys.Count > 0 Then
                        Dim objUsr As New clsUserAddEdit
                        Dim strFilter, strJoin As String
                        For Each iCoId As Integer In mDicValues.Keys
                            strFilter = "" : strJoin = "" : strActiveUsrIds = ""
                            StrQ = StrFQ
                            If iCoId <= 0 Then
                                StrQ = StrQ.Replace("#JOIN_QUERY#", "")
                                StrQ = StrQ.Replace("#CoId#", iCoId)
                                StrQ = StrQ.Replace("#FILTER#", "")
                            Else
                                objUsr.GetActiveFilterCondition(iCoId, objDataOpr, strJoin, strFilter, eZeeDate.convertDate(mDicValues(iCoId).ToString).Date)
                                StrQ = StrQ.Replace("#JOIN_QUERY#", strJoin)
                                StrQ = StrQ.Replace("#CoId#", iCoId)
                                StrQ = StrQ.Replace("#FILTER#", strFilter)
                            End If

                            dsList = objDataOpr.ExecQuery(StrQ, "List")

                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If

                            If dsList.Tables(0).Rows.Count > 0 Then
                                Dim iUserIds As IEnumerable(Of Integer) = Nothing
                                iCommonUsr = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("userunkid")).ToList()
                                If iCoId <= 0 Then
                                    iUserIds = (From c In iCommonUsr Join m In iManualUsr On c Equals m Select c)
                                Else
                                    iUserIds = (From c In iCommonUsr Join m In iImportUsr On c Equals m Select c)
                                End If
                                If iUserIds IsNot Nothing Then
                                    strActiveUsrIds = String.Join(",", iUserIds.Select(Function(c) c.ToString).ToArray)
                                End If
                                If strActiveUsrIds.Trim.Length > 0 Then
                                    strActiveCSVUsrIds &= "," & strActiveUsrIds
                                End If
                            End If
                        Next
                        objUsr = Nothing
                    End If
                End If
                If strActiveCSVUsrIds.Trim.Length > 0 Then
                    strActiveCSVUsrIds = Mid(strActiveCSVUsrIds, 2)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActiveUserList_CSV; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14-APR-2017] -- END

    'S.SANDEEP [10-MAY-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
    Public Function TestConnection_Symmetry(ByVal strServer As String, _
                                        ByVal strDatabase As String, _
                                        ByVal intAuthenticateType As Integer, _
                                        ByVal strUserName As String, _
                                        ByVal strPassword As String) As Boolean
        Try
            Dim StrConn As String = ""
            Using oSQL As SqlClient.SqlConnection = New SqlClient.SqlConnection
                StrConn = "Data Source=" & strServer & ";Initial Catalog=" & strDatabase & "; "
                If intAuthenticateType = 0 Then 'Windows
                    StrConn &= "Integrated Security=True "
                ElseIf intAuthenticateType = 1 Then 'User
                    StrConn &= "User ID=" & strUserName & ";Password = " & strPassword & " "
                End If
                oSQL.ConnectionString = StrConn
                Try
                    oSQL.Open()
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message & "; Procedure Name: TestConnection_Symmetry; Module Name: " & mstrModuleName)
                End Try
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: TestConnection_Symmetry; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [10-MAY-2017] -- END

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Public Function TestConnection_Oracle(ByVal strOracleHostName As String, _
                                        ByVal strOraclePortNo As String, _
                                        ByVal strOracleServiceName As String, _
                                        ByVal strOracleUserName As String, _
                                        ByVal strOracleUserPassword As String) As Boolean
        Try
            Dim StrConn As String = ""
            Using oOracle As OracleClient.OracleConnection = New OracleClient.OracleConnection
                StrConn = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & strOracleHostName & ")(PORT=" & strOraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & strOracleServiceName & "))); User Id=" & strOracleUserName & ";Password=" & strOracleUserPassword & "; "
                oOracle.ConnectionString = StrConn
                Try
                    oOracle.Open()
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message & "; Procedure Name: TestConnection_Oracle; Module Name: " & mstrModuleName)
                End Try
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: TestConnection_Oracle; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'Sohail (26 Nov 2018) -- End

    'Sohail (02 Mar 2019) -- Start
    'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
    Public Function TestConnection_SQL(ByVal strSQLDataSource As String, _
                                        ByVal strSQLDatabaseName As String, _
                                        ByVal strSQLDatabaseOwnerName As String, _
                                        ByVal strSQLUserName As String, _
                                        ByVal strSQLUserPassword As String) As Boolean
        Try
            Dim StrConn As String = ""
            Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                StrConn = "Data Source=" & strSQLDataSource & "; Initial Catalog=" & strSQLDatabaseName & "; user id =" & strSQLUserName & "; Password=" & strSQLUserPassword & "; MultipleActiveResultSets=true; "
                oSql.ConnectionString = StrConn
                Try
                    oSql.Open()
                    Return True
                Catch ex As Exception
                    Throw New Exception(ex.Message & "; Procedure Name: TestConnection_SQL; Module Name: " & mstrModuleName)
                End Try
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: TestConnection_SQL; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'Sohail (02 Mar 2019) -- End

End Class