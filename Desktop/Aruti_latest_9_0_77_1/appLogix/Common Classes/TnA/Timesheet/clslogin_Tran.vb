﻿'************************************************************************************************************************************
'Class Name : clslogin_Tran.vb
'Purpose    :
'Date       :06/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 2
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clslogin_Tran
    Private Shared ReadOnly mstrModuleName As String = "clslogin_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoginunkid As Integer
    Private mintLoginsummaryunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintShiftunkid As Integer
    Private mdtLogindate As Date
    Private mdtFromdate As Date
    Private mdtTodate As Date
    Private mdtcheckintime As Date
    Private mdtCheckouttime As Date
    Private mintHoldunkid As Integer
    Private mblnIsPost As Boolean = False
    Private mintWorkhour As Integer
    Private mdblDayType As Double
    Private mintBreakhr As Integer
    Private mintTotalhr As Integer
    Private mintTotalOvertimehr As Integer
    Private mintShorthr As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIspaidLeave As Boolean
    Private mblnIsUnpaidLeave As Boolean
    Private mblnIsAbsentProcess As Boolean
    Private mblnIsactive As Boolean = True
    Private mintInOuType As Integer = -1
    Private mintSourceType As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As DateTime
    Private mstrVoidReason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mintTotalNighthrs As Integer
    Private mblnIsWeekend As Boolean = False
    Private mblnIsholiday As Boolean = False
    Private mintPolicyId As Integer = 0
    Private mintOT2 As Integer = 0
    Private mintOT3 As Integer = 0
    Private mintOT4 As Integer = 0
    Private mintearly_coming As Integer = 0
    Private mintlate_coming As Integer = 0
    Private mintearly_going As Integer = 0
    Private mintlate_going As Integer = 0
    Private mintOperationType As Integer = 0
    Private mdtRoundoffInTime As DateTime = Nothing
    Private mdtRoundoffOutTime As DateTime = Nothing
    Private mblnIsDayOffshift As Boolean = False
    Private mdtOriginal_Intime As DateTime
    Private mdtOriginal_Outtime As DateTime
    Dim mdctFinalOTOrder As Dictionary(Of String, Integer)
    'Private mstrWebClientIP As String = ""
    'Private mstrWebHostName As String = ""
    Private mblnIsgraceroundoff As Boolean = False
    Private mintManualroundofftypeid As Integer = 0
    Private mintRoundminutes As Integer = 0
    Private mintRoundlimit As Integer = 0
    Private mintTeaHr As Integer = 0
    Private mdecDayFraction As Decimal = 0
    'Pinkal (30-Jul-2015) -- Start
    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
    Private mintCompanyId As Integer = 0
    'Pinkal (30-Jul-2015) -- End

    Private mintLateComing_WOGrace As Integer = 0
    Private mintLateComing_AfterGrace As Integer = 0


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.
    Private mstrDeviceAtt_DeviceNo As String = ""
    Private mstrDeviceAtt_IPAddress As String = ""
    Private mstrDeviceAtt_EnrollNo As String = ""
    Private mdtDeviceAtt_LoginDate As Date = Nothing
    Private mdtDeviceAtt_LoginTime As DateTime = Nothing
    Private mstrDeviceAtt_VerifyMode As String = ""
    Private mstrDeviceAtt_InOutMode As String = ""
    Private mstrDeviceAtt_IsError As Boolean = False
    'Pinkal (27-Mar-2017) -- End


    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .
    Private mintDayID As Integer = -1
    'Pinkal (18-Jul-2017) -- End


    'Pinkal (25-AUG-2017) -- Start
    'Enhancement - Working on B5 plus TnA Enhancement.
    Private mintLeaveTypeId As Integer = 0
    'Pinkal (25-AUG-2017) -- End


#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginunkid(Optional ByVal objDooperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintLoginunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginunkid = value
            Call GetData(objDooperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginsummaryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoginSummaryunkid() As Integer
        Get
            Return mintLoginsummaryunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginsummaryunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftunkid() As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set logindate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Logindate() As Date
        Get
            Return mdtLogindate
        End Get
        Set(ByVal value As Date)
            mdtLogindate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fromdate() As Date
        Get
            Return mdtFromdate
        End Get
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set todate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Todate() As Date
        Get
            Return mdtTodate
        End Get
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set checkintime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _checkintime() As Date
        Get
            Return mdtcheckintime
        End Get
        Set(ByVal value As Date)
            mdtcheckintime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set checkouttime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Checkouttime() As Date
        Get
            Return mdtCheckouttime
        End Get
        Set(ByVal value As Date)
            mdtCheckouttime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holdunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holdunkid() As Integer
        Get
            Return mintHoldunkid
        End Get
        Set(ByVal value As Integer)
            mintHoldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispost
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsPost() As Boolean
        Get
            Return mblnIsPost
        End Get
        Set(ByVal value As Boolean)
            mblnIsPost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set daytype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DayType() As Integer
        Get
            Return mdblDayType
        End Get
        Set(ByVal value As Integer)
            mdblDayType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workhour
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Workhour() As Integer
        Get
            Return mintWorkhour
        End Get
        Set(ByVal value As Integer)
            mintWorkhour = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set breakhr
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Breakhr() As Integer
        Get
            Return mintBreakhr
        End Get
        Set(ByVal value As Integer)
            mintBreakhr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalhr
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Totalhr() As Integer
        Get
            Return mintTotalhr
        End Get
        Set(ByVal value As Integer)
            mintTotalhr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalovertimehr
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalOvertimehr() As Integer
        Get
            Return mintTotalOvertimehr
        End Get
        Set(ByVal value As Integer)
            mintTotalOvertimehr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shorthr
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shorthr() As Integer
        Get
            Return mintShorthr
        End Get
        Set(ByVal value As Integer)
            mintShorthr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispaidleave
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsPaidLeave() As Boolean
        Get
            Return mblnIspaidLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIspaidLeave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isunpaidleave
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsUnPaidLeave() As Boolean
        Get
            Return mblnIsUnpaidLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsUnpaidLeave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isabsentprocess
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsAbsentProcess() As Boolean
        Get
            Return mblnIsAbsentProcess
        End Get
        Set(ByVal value As Boolean)
            mblnIsAbsentProcess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set inoutype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _InOutType() As Integer
        Get
            Return mintInOuType
        End Get
        Set(ByVal value As Integer)
            mintInOuType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sourcetype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SourceType() As Integer
        Get
            Return mintSourceType
        End Get
        Set(ByVal value As Integer)
            mintSourceType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LoginEmployeeUnkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalnighthr
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalNighthr() As Integer
        Get
            Return mintTotalNighthrs
        End Get
        Set(ByVal value As Integer)
            mintTotalNighthrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweekend
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeekend() As Boolean
        Get
            Return mblnIsWeekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeekend = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isholiday
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsHoliday() As Boolean
        Get
            Return mblnIsholiday
        End Get
        Set(ByVal value As Boolean)
            mblnIsholiday = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _PolicyId() As Integer
        Get
            Return mintPolicyId
        End Get
        Set(ByVal value As Integer)
            mintPolicyId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot2() As Integer
        Get
            Return mintOT2
        End Get
        Set(ByVal value As Integer)
            mintOT2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot3
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot3() As Integer
        Get
            Return mintOT3
        End Get
        Set(ByVal value As Integer)
            mintOT3 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot4
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot4() As Integer
        Get
            Return mintOT4
        End Get
        Set(ByVal value As Integer)
            mintOT4 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set early_coming
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Early_Coming() As Integer
        Get
            Return mintearly_coming
        End Get
        Set(ByVal value As Integer)
            mintearly_coming = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set late_coming
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Late_Coming() As Integer
        Get
            Return mintlate_coming
        End Get
        Set(ByVal value As Integer)
            mintlate_coming = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set early_going
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Early_Going() As Integer
        Get
            Return mintearly_going
        End Get
        Set(ByVal value As Integer)
            mintearly_going = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set late_going
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Late_Going() As Integer
        Get
            Return mintlate_going
        End Get
        Set(ByVal value As Integer)
            mintlate_going = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set late_going
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _OperationType() As Integer
        Get
            Return mintOperationType
        End Get
        Set(ByVal value As Integer)
            mintOperationType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roundoffintime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roundoff_Intime() As Date
        Get
            Return mdtRoundoffInTime
        End Get
        Set(ByVal value As Date)
            mdtRoundoffInTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roundoffouttime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roundoff_Outtime() As Date
        Get
            Return mdtRoundoffOutTime
        End Get
        Set(ByVal value As Date)
            mdtRoundoffOutTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdayoffshift
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsDayOffShift() As Boolean
        Get
            Return mblnIsDayOffshift
        End Get
        Set(ByVal value As Boolean)
            mblnIsDayOffshift = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Original_InTime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Original_InTime() As DateTime
        Get
            Return mdtOriginal_Intime
        End Get
        Set(ByVal value As DateTime)
            mdtOriginal_Intime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Original_OutTime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Original_OutTime() As DateTime
        Get
            Return mdtOriginal_Outtime
        End Get
        Set(ByVal value As DateTime)
            mdtOriginal_Outtime = value
        End Set
    End Property

    'Public Property _WebClientIP() As String
    '    Get
    '        Return mstrWebClientIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    Public Property _Isgraceroundoff() As Boolean
        Get
            Return mblnIsgraceroundoff
        End Get
        Set(ByVal value As Boolean)
            mblnIsgraceroundoff = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Manualroundofftypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Manualroundofftypeid() As Integer
        Get
            Return mintManualroundofftypeid
        End Get
        Set(ByVal value As Integer)
            mintManualroundofftypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Roundminutes
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roundminutes() As Integer
        Get
            Return mintRoundminutes
        End Get
        Set(ByVal value As Integer)
            mintRoundminutes = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Roundlimit
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roundlimit() As Integer
        Get
            Return mintRoundlimit
        End Get
        Set(ByVal value As Integer)
            mintRoundlimit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TeaHr
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TeaHr() As Integer
        Get
            Return mintTeaHr
        End Get
        Set(ByVal value As Integer)
            mintTeaHr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveDayFraction
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LeaveDayFraction() As Decimal
        Get
            Return mdecDayFraction
        End Get
        Set(ByVal value As Decimal)
            mdecDayFraction = value
        End Set
    End Property


    'Pinkal (30-Jul-2015) -- Start
    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
    Public Property _CompanyId() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property
    'Pinkal (30-Jul-2015) -- End

    ''' <summary>
    ''' Purpose: Get or Set LateComingWOGrace
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LateComingWOGrace() As Integer
        Get
            Return mintLateComing_WOGrace
        End Get
        Set(ByVal value As Integer)
            mintLateComing_WOGrace = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _LateComing_AfterGrace
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LateComing_AfterGrace() As Integer
        Get
            Return mintLateComing_AfterGrace
        End Get
        Set(ByVal value As Integer)
            mintLateComing_AfterGrace = value
        End Set
    End Property


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data .

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_DeviceNo
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_DeviceNo() As String
        Get
            Return mstrDeviceAtt_DeviceNo
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_DeviceNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_IPAddress
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_IPAddress() As String
        Get
            Return mstrDeviceAtt_IPAddress
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_IPAddress = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_EnrollNo
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_EnrollNo() As String
        Get
            Return mstrDeviceAtt_EnrollNo
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_EnrollNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_LoginDate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_LoginDate() As Date
        Get
            Return mdtDeviceAtt_LoginDate
        End Get
        Set(ByVal value As Date)
            mdtDeviceAtt_LoginDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_LoginTime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_LoginTime() As DateTime
        Get
            Return mdtDeviceAtt_LoginTime
        End Get
        Set(ByVal value As DateTime)
            mdtDeviceAtt_LoginTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_VerifyMode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_VerifyMode() As String
        Get
            Return mstrDeviceAtt_VerifyMode
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_VerifyMode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_InOutMode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_InOutMode() As String
        Get
            Return mstrDeviceAtt_InOutMode
        End Get
        Set(ByVal value As String)
            mstrDeviceAtt_InOutMode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeviceAtt_IsError
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeviceAtt_IsError() As Boolean
        Get
            Return mstrDeviceAtt_IsError
        End Get
        Set(ByVal value As Boolean)
            mstrDeviceAtt_IsError = value
        End Set
    End Property

    'Pinkal (27-Mar-2017) -- End


    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .
    ''' <summary>
    ''' Purpose: Get or Set DayId
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _DayId() as Integer
        Get
            Return mintDayID
        End Get
        Set(ByVal value As Integer)
            mintDayID = value
        End Set
    End Property

    'Pinkal (18-Jul-2017) -- End


    'Pinkal (25-AUG-2017) -- Start
    'Enhancement - Working on B5 plus TnA Enhancement.

    ''' <summary>
    ''' Purpose: Get or Set LeaveTypeId
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LeaveTypeId() as Integer
        Get
            Return mintLeaveTypeId
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    'Pinkal (25-AUG-2017) -- End


#End Region

#Region "For LOGIN TRAN Table"

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDooperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        If objDooperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDooperation
        End If


        Try

            'Pinkal (20-Sep-2013) -- Start
            'Enhancement : TRA Changes

            strQ = "SELECT " & _
              "  loginunkid " & _
              ", employeeunkid " & _
              ", logindate " & _
              ", checkintime " & _
              ", checkouttime " & _
              ", holdunkid " & _
              ", workhour " & _
              ", breakhr " & _
              ", remark " & _
              ", inouttype " & _
              ", sourcetype " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ",roundoff_intime " & _
              ",roundoff_outtime " & _
              ",operationtype " & _
              ",original_intime " & _
              ",original_outtime " & _
              ", ISNULL(teahr,0) AS teahr " & _
             "FROM tnalogin_tran " & _
             "WHERE loginunkid = @loginunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoginunkid = CInt(dtRow.Item("loginunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtLogindate = dtRow.Item("logindate")

                If Not dtRow.Item("checkintime") Is DBNull.Value Then
                    mdtcheckintime = dtRow.Item("checkintime")
                Else
                    mdtcheckintime = Nothing
                End If

                If Not dtRow.Item("checkouttime") Is DBNull.Value Then
                    mdtCheckouttime = dtRow.Item("checkouttime")
                Else
                    mdtCheckouttime = Nothing
                End If

                mintHoldunkid = CInt(dtRow.Item("holdunkid"))
                mintWorkhour = CInt(dtRow.Item("workhour"))
                mintBreakhr = CInt(dtRow.Item("breakhr"))
                If Not dtRow.Item("userunkid") Is DBNull.Value Then
                    mintUserunkid = CInt(dtRow.Item("userunkid"))
                End If

                If Not dtRow.Item("inouttype") Is DBNull.Value Then
                    mintInOuType = CInt(dtRow.Item("inouttype"))
                End If

                If Not dtRow.Item("sourcetype") Is DBNull.Value Then
                    mintSourceType = CInt(dtRow.Item("sourcetype"))
                End If

                mstrRemark = dtRow.Item("remark")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If Not dtRow.Item("voiduserunkid") Is DBNull.Value Then
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                End If
                If Not dtRow.Item("roundoff_intime") Is DBNull.Value Then
                    mdtRoundoffInTime = CDate(dtRow.Item("roundoff_intime"))
                End If
                If Not dtRow.Item("voidreason") Is DBNull.Value Then
                    mstrVoidReason = CStr(dtRow.Item("voidreason"))
                End If

                If Not dtRow.Item("voiddatetime") Is DBNull.Value Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                If Not dtRow.Item("roundoff_outtime") Is DBNull.Value Then
                    mdtRoundoffOutTime = CStr(dtRow.Item("roundoff_outtime"))
                End If
                If Not dtRow.Item("operationtype") Is DBNull.Value Then
                    mintOperationType = CStr(dtRow.Item("operationtype"))
                End If
                'Pinkal (20-Sep-2013) -- End


                'Pinkal (15-Nov-2013) -- Start
                'Enhancement : Oman Changes

                If Not dtRow.Item("original_intime") Is DBNull.Value Then
                    mdtOriginal_Intime = CDate(dtRow.Item("original_intime"))
                Else
                    mdtOriginal_Intime = Nothing
                End If

                If Not dtRow.Item("original_outtime") Is DBNull.Value Then
                    mdtOriginal_Outtime = CDate(dtRow.Item("original_outtime"))
                Else
                    mdtOriginal_Outtime = Nothing
                End If

                'Pinkal (15-Nov-2013) -- End


                'Pinkal (28-Jan-2014) -- Start
                'Enhancement : Oman Changes [Voltamp]

                If Not dtRow.Item("teahr") Is DBNull.Value Then
                    mintTeaHr = CInt(dtRow.Item("teahr"))
                Else
                    mintTeaHr = 0
                End If

                'Pinkal (28-Jan-2014) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  loginunkid " & _
              ", employeeunkid " & _
              ", logindate " & _
              ", CASE WHEN roundoff_intime IS NULL  THEN checkintime ELSE roundoff_intime END checkintime " & _
              ", CASE WHEN roundoff_outtime IS  NULL THEN checkouttime ELSE roundoff_outtime END checkouttime " & _
              ", holdunkid " & _
              ", workhour " & _
              ", breakhr " & _
              ", remark " & _
              ", inouttype " & _
              ", sourcetype " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ",original_intime " & _
              ",original_outtime " & _
              ", ISNULL(teahr,0) AS teahr " & _
             "FROM tnalogin_tran " & _
             " WHERE employeeunkid=@employeeunkid AND logindate = @logindate "


            'Pinkal (28-Jan-2014) -- Start [ ", ISNULL(teahr,0) AS teahr " ]  For Voltamp


            If blnOnlyActive Then
                strQ &= " AND isnull(isvoid,0) = 0 "
            End If

            strQ &= " order by checkintime"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal objDataOperation As clsDataOperation, ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "SELECT " & _
              "  loginunkid " & _
              ", employeeunkid " & _
              ", logindate " & _
              ", checkintime " & _
              ", checkouttime " & _
              ", holdunkid " & _
              ", workhour " & _
              ", breakhr " & _
              ", remark " & _
              ", inouttype " & _
              ", sourcetype " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", roundoff_intime " & _
              ", roundoff_outtime " & _
               ",original_intime " & _
              ",original_outtime " & _
              ", ISNULL(teahr,0) AS teahr " & _
             "FROM tnalogin_tran " & _
             " WHERE employeeunkid=@employeeunkid AND logindate = @logindate "

            'Pinkal (28-Jan-2014) -- Start [ ", ISNULL(teahr,0) AS teahr " ]  For Voltamp

            If blnOnlyActive Then
                strQ &= " AND isnull(isvoid,0) = 0 "
            End If

            strQ &= " order by checkintime"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function Insert(ByVal xDatabaseName As String _
                         , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                         , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                         , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                         , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                         , ByVal blnPolicyManagementTNA As Boolean _
                         , ByVal blnDonotAttendanceinSeconds As Boolean _
                         , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                 , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                 , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                 , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                         , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intEmpId As Integer = -1 _
                         , Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal strUserAccessLevelFilterString As String = "" _
                                 , Optional ByVal mstrFilter As String = "") As Boolean


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        If mdtcheckintime <> Nothing Then
            If IsExistTime(mdtcheckintime) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
                Return False
                'ElseIf IsExistTime(mdtCheckouttime) Then
                '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
                '    Return False
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)

            If blnDonotAttendanceinSeconds Then

                'Pinkal (22-Jun-2015) -- Start
                'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
                'objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, CDate(mdtcheckintime.ToShortDateString() & " " & mdtcheckintime.ToString("HH:mm")), DBNull.Value))
                'objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString() & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime.ToString("f"), DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime.ToString("f"), DBNull.Value))
                'Pinkal (22-Jun-2015) -- End
            Else
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            objDataOperation.AddParameter("@original_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
            objDataOperation.AddParameter("@original_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))

            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
            objDataOperation.AddParameter("@workhour", SqlDbType.Float, eZeeDataType.INT_SIZE, mintWorkhour.ToString)
            objDataOperation.AddParameter("@breakhr", SqlDbType.Float, eZeeDataType.INT_SIZE, mintBreakhr.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@inouttype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInOuType.ToString)
            objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSourceType.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)
            'Hemant (26 Oct 2018) -- Start
            'Enhancement : Implementing New Module of In/Out Daily Attendance Operations
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            'Hemant (26 Oct 2018) -- End
            strQ = "INSERT INTO tnalogin_tran ( " & _
              "  employeeunkid " & _
              ", logindate " & _
              ", checkintime " & _
              ", checkouttime " & _
              ", holdunkid " & _
              ", workhour " & _
              ", breakhr " & _
              ", remark " & _
              ", inouttype " & _
              ", sourcetype " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ",original_intime " & _
              ",original_outtime " & _
              ",loginemployeeunkid " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @logindate " & _
              ", @checkintime " & _
              ", @checkouttime " & _
              ", @holdunkid " & _
              ", @workhour " & _
              ", @breakhr " & _
              ", @remark " & _
              ", @inouttype " & _
              ", @sourcetype " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @original_intime " & _
              ", @original_outtime " & _
              ", @loginemployeeunkid " & _
            "); SELECT @@identity"
            'Hemant (26 Oct 2018) -- Start/End
            'Enhancement : Implementing New Module of In/Out Daily Attendance Operations  
            'Included loginemployeeunkid field in query.  
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoginunkid = dsList.Tables(0).Rows(0).Item(0)


            'START FOR INSERT LOGIN SUMMERY


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            'InsertLoginSummary(objDataOperation, xDatabaseName _
            '                 , xUserUnkid, xYearUnkid _
            '                 , xCompanyUnkid, eZeeDate.convertDate(mdtStartDate) _
            '                 , eZeeDate.convertDate(mdtEndDate), xUserModeSetting _
            '                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                 , blnPolicyManagementTNA _
            '                 , blnDonotAttendanceinSeconds _
            '                 , blnFirstCheckInLastCheckOut _
            '                 , blnApplyUserAccessFilter, intEmpId _
            '                 , strUserAccessLevelFilterString, mstrFilter)


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.

            'InsertLoginSummary(objDataOperation, xDatabaseName _
            '                 , xUserUnkid, xYearUnkid _
            '                , xCompanyUnkid, mdtStartDate _
            '                , mdtEndDate, xUserModeSetting _
            '                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                 , blnPolicyManagementTNA _
            '                 , blnDonotAttendanceinSeconds _
            '                 , blnFirstCheckInLastCheckOut _
            '                 , blnApplyUserAccessFilter, intEmpId _
            '                 , strUserAccessLevelFilterString, mstrFilter)



            'Pinkal (01-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	

            'InsertLoginSummary(objDataOperation, xDatabaseName _
            '                 , xUserUnkid, xYearUnkid _
            '                , xCompanyUnkid, mdtStartDate _
            '                , mdtEndDate, xUserModeSetting _
            '                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                 , blnPolicyManagementTNA _
            '                 , blnDonotAttendanceinSeconds _
            '                 , blnFirstCheckInLastCheckOut _
            '                             , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
            '                             , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
            '                 , strUserAccessLevelFilterString, mstrFilter)

            InsertLoginSummary(objDataOperation, xDatabaseName _
                             , xUserUnkid, xYearUnkid _
                            , xCompanyUnkid, mdtStartDate _
                            , mdtEndDate, xUserModeSetting _
                             , xOnlyApproved, xIncludeIn_ActiveEmployee _
                             , blnPolicyManagementTNA _
                             , blnDonotAttendanceinSeconds _
                             , blnFirstCheckInLastCheckOut _
                                         , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                         , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                      , strUserAccessLevelFilterString, mstrFilter, mintLoginunkid)


            'Pinkal (01-Feb-2022) -- End


            'Pinkal (11-AUG-2017) -- End

            'Pinkal (22-Mar-2016) -- End

            

            'END FOR INSERT LOGIN SUMMERY


            If blnFirstCheckInLastCheckOut = False Then
                UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtLogindate)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE

            InsertAuditTrailLogin(objDataOperation, 1, blnDonotAttendanceinSeconds)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnalogin_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String _
                          , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                          , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                          , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                          , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                          , ByVal blnPolicyManagementTNA As Boolean _
                          , ByVal blnDonotAttendanceinSeconds As Boolean _
                          , ByVal blnFirstCheckInLastCheckOut As Boolean _
                          , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                          , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                          , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                          , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intEmpId As Integer = -1 _
                          , Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal strUserAccessLevelFilterString As String = "" _
                          , Optional ByVal mstrFilter As String = "") As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnPolicyManagementTNA,blnDonotAttendanceinSeconds,blnFirstCheckInLastCheckOut} -- END


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        If IsExistTime(mdtCheckouttime, mintLoginunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
            Return False
        ElseIf IsExistTime(mdtCheckouttime, mintLoginunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            If blnDonotAttendanceinSeconds Then

                'Pinkal (13-Jan-2015) -- Start
                'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
                'objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime.ToString("dd-MM-yyyy HH:mm"), DBNull.Value))
                'objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime.ToString("dd-MM-yyyy HH:mm"), DBNull.Value))
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime.ToString("f"), DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime.ToString("f"), DBNull.Value))
                'Pinkal (13-Jan-2015) -- End
            Else
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
            objDataOperation.AddParameter("@workhour", SqlDbType.Float, eZeeDataType.INT_SIZE, mintWorkhour.ToString)
            objDataOperation.AddParameter("@breakhr", SqlDbType.Float, eZeeDataType.INT_SIZE, mintBreakhr.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark)
            objDataOperation.AddParameter("@inouttype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInOuType.ToString)
            objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSourceType.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)
            'Hemant (26 Oct 2018) -- Start
            'Enhancement : Implementing New Module of In/Out Daily Attendance Operations
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
             'Hemant (26 Oct 2018) -- End 

            strQ = "UPDATE tnalogin_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", logindate = @logindate" & _
              ", checkintime = @checkintime" & _
              ", checkouttime = @checkouttime" & _
              ", holdunkid = @holdunkid" & _
              ", workhour = @workhour" & _
              ", breakhr = @breakhr" & _
              ", remark = @remark" & _
              ", inouttype = @inouttype " & _
              ", sourcetype = @sourcetype " & _
              ", userunkid = @userunkid " & _
              ", isvoid = @isvoid " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", loginemployeeunkid = @loginemployeeunkid "
               'Hemant (26 Oct 2018) -- Start/End
               'Enhancement : Implementing New Module of In/Out Daily Attendance Operations
               'Included loginemployeeunkid field in query.  

            If mintOperationType <> enTnAOperation.Edit AndAlso mintOperationType <> enTnAOperation.RoundOff Then

                strQ &= ",original_intime = @original_intime " & _
                          ",original_outtime = @original_outtime "

                objDataOperation.AddParameter("@original_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
                objDataOperation.AddParameter("@original_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))

            End If


            strQ &= " WHERE loginunkid = @loginunkid "

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mdtcheckintime <> Nothing Then
                If blnFirstCheckInLastCheckOut = False Then
                    UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtcheckintime)
                End If
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'START FOR INSERT LOGIN SUMMERY


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.

            'InsertLoginSummary(objDataOperation, xDatabaseName _
            '                  , xUserUnkid, xYearUnkid _
            '                  , xCompanyUnkid, mdtStartDate _
            '                  , mdtEndDate, xUserModeSetting _
            '                  , xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                  , blnPolicyManagementTNA _
            '                  , blnDonotAttendanceinSeconds _
            '                  , blnFirstCheckInLastCheckOut _
            '                  , blnApplyUserAccessFilter, intEmpId _
            '                  , strUserAccessLevelFilterString, mstrFilter)

            InsertLoginSummary(objDataOperation, xDatabaseName _
                             , xUserUnkid, xYearUnkid _
                             , xCompanyUnkid, mdtStartDate _
                             , mdtEndDate, xUserModeSetting _
                             , xOnlyApproved, xIncludeIn_ActiveEmployee _
                             , blnPolicyManagementTNA _
                             , blnDonotAttendanceinSeconds _
                             , blnFirstCheckInLastCheckOut _
                              , blnIsHolidayConsiderOnWeekend _
                              , blnIsDayOffConsiderOnWeekend _
                              , blnIsHolidayConsiderOnDayoff _
                             , blnApplyUserAccessFilter, intEmpId _
                             , strUserAccessLevelFilterString, mstrFilter)

            'Pinkal (11-AUG-2017) -- End

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'END FOR INSERT LOGIN SUMMERY

            'START AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE
            InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) '' FOR TIMESHEET MANUAL UPDATE ENTRY INSERT AUDIT TYPE = 2
            'END AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' START USED FOR EDIT CALCULATION 

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnalogin_tran) </purpose>
    Public Function UpdateForEditOperation(ByVal mdtDate As DataTable, ByVal intOpeationType As Integer _
                                         , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                         , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                         , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                         , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                         , ByVal blnPolicyManagementTNA As Boolean _
                                         , ByVal blnDonotAttendanceinSeconds As Boolean _
                                         , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                         , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                         , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                         , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                         , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                         , Optional ByVal intEmpId As Integer = -1 _
                                         , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                         , Optional ByVal mstrFilter As String = "") As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnPolicyManagementTNA,blnDonotAttendanceinSeconds,blnFirstCheckInLastCheckOut} -- END
        'Public Function UpdateForEditOperation(ByVal mdtDate As DataTable, ByVal intOpeationType As Integer) As Boolean        


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = "UPDATE tnalogin_tran SET " & _
                     "  employeeunkid = @employeeunkid " & _
                     ", logindate = @log_date" & _
                     ", workhour = @workhour" & _
                     ", breakhr = @breakhr" & _
                     ", operationtype = @operationtype " & _
                     ", userunkid = @userunkid " & _
                     ", checkintime = @checkintime" & _
                     ", checkouttime = @checkouttime " & _
                     ", sourcetype = @sourcetype " & _
                     " WHERE loginunkid = @login_unkid AND isvoid = 0 "

            For i = 0 To mdtDate.Rows.Count - 1

                With mdtDate.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")

                            Case "U"

                                If IsExistTime(.Item("checkouttime"), .Item("loginunkid"), objDataOperation) Then
                                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
                                    Return False
                                ElseIf IsExistTime(.Item("checkouttime"), .Item("loginunkid"), objDataOperation) Then
                                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
                                    Return False
                                End If


                                'Pinkal (15-Nov-2013) -- Start
                                'Enhancement : Oman Changes
                                ' mintLoginunkid = .Item("loginunkid")
                                _Loginunkid = .Item("loginunkid")
                                'Pinkal (15-Nov-2013) -- End
                                mdtLogindate = .Item("logindate")

                                If .Item("checkintime") <> Nothing Then
                                    mdtcheckintime = .Item("checkintime")
                                End If
                                If .Item("checkouttime") <> Nothing Then
                                    mdtCheckouttime = .Item("checkouttime")
                                End If

                                mintOperationType = intOpeationType

                                'Pinkal (21-Oct-2013) -- Start
                                'Enhancement : Oman Changes
                                'mintTotalOvertimehr = .Item("workhour")
                                mintTotalhr = .Item("workhour")
                                'Pinkal (21-Oct-2013) -- End


                                mintBreakhr = .Item("breakhr")
                                mintSourceType = .Item("sourcetype")

                                objDataOperation.AddParameter("@login_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginunkid"))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@log_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("logindate"))


                                'Pinkal (15-Nov-2013) -- Start
                                'Enhancement : Oman Changes

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                If blnDonotAttendanceinSeconds Then
                                    'If ConfigParameter._Object._DonotAttendanceinSeconds Then
                                    'S.SANDEEP [04 JUN 2015] -- END
                                    objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, CDate(CDate(.Item("checkintime")).ToShortDateString & " " & CDate(.Item("checkintime")).ToString("HH:mm")), DBNull.Value))
                                    objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, CDate(CDate(.Item("checkouttime")).ToShortDateString() & " " & CDate(.Item("checkouttime")).ToString("HH:mm")), DBNull.Value))
                                Else
                                    objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, .Item("checkintime"), DBNull.Value))
                                    objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, .Item("checkouttime"), DBNull.Value))
                                End If

                                'Pinkal (15-Nov-2013) -- End

                                objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, intOpeationType.ToString)
                                objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
                                objDataOperation.AddParameter("@workhour", SqlDbType.Float, eZeeDataType.INT_SIZE, .Item("workhour").ToString)
                                objDataOperation.AddParameter("@breakhr", SqlDbType.Float, eZeeDataType.INT_SIZE, .Item("breakhr").ToString)
                                objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("sourcetype").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtLogindate)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'START FOR INSERT LOGIN SUMMERY


                                'Pinkal (11-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.


                                'InsertLoginSummary(objDataOperation, xDatabaseName _
                                '            , xUserUnkid, xYearUnkid _
                                '                 , xCompanyUnkid, mdtStartDate _
                                '                 , mdtEndDate, xUserModeSetting _
                                '            , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                '            , blnPolicyManagementTNA _
                                '            , blnDonotAttendanceinSeconds _
                                '            , blnFirstCheckInLastCheckOut _
                                '            , blnApplyUserAccessFilter, intEmpId _
                                '            , strUserAccessLevelFilterString, mstrFilter)

                                InsertLoginSummary(objDataOperation, xDatabaseName _
                                                 , xUserUnkid, xYearUnkid _
                                          , xCompanyUnkid, mdtStartDate _
                                          , mdtEndDate, xUserModeSetting _
                                                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                 , blnPolicyManagementTNA _
                                                 , blnDonotAttendanceinSeconds _
                                                 , blnFirstCheckInLastCheckOut _
                                              , blnIsHolidayConsiderOnWeekend _
                                              , blnIsDayOffConsiderOnWeekend _
                                              , blnIsHolidayConsiderOnDayoff _
                                                 , blnApplyUserAccessFilter, intEmpId _
                                                 , strUserAccessLevelFilterString, mstrFilter)

                                'Pinkal (11-AUG-2017) -- End
                           

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'END FOR INSERT LOGIN SUMMERY

                                ' START FOR INSERT AUDIT TRAIL

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'InsertAuditTrailLogin(objDataOperation, 2)
                                InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds)
                                'S.SANDEEP [04 JUN 2015] -- END


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                ' END FOR INSERT AUDIT TRAIL


                        End Select

                    End If

                End With

            Next


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateForEditOperation; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' END USED FOR EDIT CALCULATION 

    ' START USED FOR ROUNDOFF CALCULATION 

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnalogin_tran) </purpose>
    Public Function UpdateForRoundOffOperation(ByVal mdtDate As DataTable, ByVal intOpeationType As Integer _
                                             , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                             , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                             , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                             , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                             , ByVal blnPolicyManagementTNA As Boolean _
                                             , ByVal blnDonotAttendanceinSeconds As Boolean _
                                             , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                             , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                             , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                             , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                             , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                             , Optional ByVal intEmpId As Integer = -1 _
                                             , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                             , Optional ByVal mstrFilter As String = "") As Boolean 'S.SANDEEP [04 JUN 2015] -- START {} -- END
        'Public Function UpdateForRoundOffOperation(ByVal mdtDate As DataTable, ByVal intOpeationType As Integer) As Boolean

        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = "UPDATE tnalogin_tran SET " & _
                     "  employeeunkid = @employeeunkid " & _
                     ", logindate = @log_date" & _
                     ", workhour = @workhour" & _
                     ", operationtype = @operationtype " & _
                     ", userunkid = @userunkid " & _
                     ", roundoff_intime = @roundoff_intime" & _
                     ", roundoff_outtime = @roundoff_outtime " & _
                     " WHERE loginunkid = @login_unkid AND isvoid = 0 "

            For i = 0 To 1

                With mdtDate.Rows(0)
                    objDataOperation.ClearParameters()

                    If i = 0 Then
                        _Loginunkid = .Item("loginunkid")
                        objDataOperation.AddParameter("@login_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginunkid"))
                    Else
                        _Loginunkid = .Item("Maxloginunkid")
                        objDataOperation.AddParameter("@login_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("Maxloginunkid"))
                    End If

                    mdtLogindate = .Item("logindate")

                    mintOperationType = intOpeationType


                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@log_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("logindate"))

                    If i = 0 Then
                        If .Item("loginunkid") <> .Item("Maxloginunkid") Then

                            'Pinkal (15-Nov-2013) -- Start
                            'Enhancement : Oman Changes

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If ConfigParameter._Object._DonotAttendanceinSeconds Then
                            If blnDonotAttendanceinSeconds Then
                                'S.SANDEEP [04 JUN 2015] -- END

                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, CDate(CDate(.Item("checkintime")).ToShortDateString() & " " & CDate(.Item("checkintime")).ToString("HH:mm")), DBNull.Value))
                                mdtRoundoffInTime = CDate(CDate(.Item("checkintime")).ToShortDateString() & " " & CDate(.Item("checkintime")).ToString("HH:mm"))
                            Else
                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, .Item("checkintime"), DBNull.Value))
                                mdtRoundoffInTime = .Item("checkintime")
                            End If

                            objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            mdtRoundoffOutTime = Nothing

                            'Pinkal (15-Nov-2013) -- End
                        Else

                            'Pinkal (15-Nov-2013) -- Start
                            'Enhancement : Oman Changes

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If ConfigParameter._Object._DonotAttendanceinSeconds Then
                            If blnDonotAttendanceinSeconds Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, CDate(CDate(.Item("checkintime")).ToShortDateString() & " " & CDate(.Item("checkintime")).ToString("HH:mm")), DBNull.Value))
                                objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, CDate(CDate(.Item("checkouttime")).ToShortDateString() & " " & CDate(.Item("checkouttime")).ToString("HH:mm")), DBNull.Value))
                            Else
                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, .Item("checkintime"), DBNull.Value))
                                objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, .Item("checkouttime"), DBNull.Value))
                            End If

                            'Pinkal (15-Nov-2013) -- End

                        End If
                    Else
                        If .Item("loginunkid") <> .Item("Maxloginunkid") Then
                            objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)


                            'Pinkal (15-Nov-2013) -- Start
                            'Enhancement : Oman Changes

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If ConfigParameter._Object._DonotAttendanceinSeconds Then
                            If blnDonotAttendanceinSeconds Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, CDate(CDate(.Item("checkouttime")).ToShortDateString & " " & CDate(.Item("checkouttime")).ToString("HH:mm")), DBNull.Value))
                                mdtRoundoffOutTime = CDate(CDate(.Item("checkouttime")).ToShortDateString & " " & CDate(.Item("checkouttime")).ToString("HH:mm"))
                            Else
                                objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, .Item("checkouttime"), DBNull.Value))
                                mdtRoundoffOutTime = .Item("checkouttime")
                            End If

                            'Pinkal (15-Nov-2013) -- End

                            mdtRoundoffInTime = Nothing

                        Else

                            'Pinkal (15-Nov-2013) -- Start
                            'Enhancement : Oman Changes

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If ConfigParameter._Object._DonotAttendanceinSeconds Then
                            If blnDonotAttendanceinSeconds Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, CDate(CDate(.Item("checkintime")).ToShortDateString & " " & CDate(.Item("checkintime")).ToString("HH:mm")), DBNull.Value))
                                objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, CDate(CDate(.Item("checkouttime")).ToShortDateString & " " & CDate(.Item("checkouttime")).ToString("HH:mm")), DBNull.Value))
                            Else
                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkintime") <> Nothing, .Item("checkintime"), DBNull.Value))
                                objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("checkouttime") <> Nothing, .Item("checkouttime"), DBNull.Value))
                            End If

                            'Pinkal (15-Nov-2013) -- End

                        End If
                    End If

                    objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, intOpeationType.ToString)
                    objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)


                    'Pinkal (09-Nov-2013) -- Start
                    'Enhancement : Oman Changes

                    'If i = 0 Then
                    '    objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, .Item("checkintime"), mdtCheckouttime))
                    'Else
                    '    objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, mdtcheckintime, .Item("checkouttime")))
                    'End If


                    'Pinkal (15-Nov-2013) -- Start
                    'Enhancement : Oman Changes

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If ConfigParameter._Object._DonotAttendanceinSeconds Then
                    If blnDonotAttendanceinSeconds Then
                        'S.SANDEEP [04 JUN 2015] -- END
                        Dim dtCheckin As DateTime = CDate(CDate(.Item("checkintime")).ToShortDateString & " " & CDate(.Item("checkintime")).ToString("HH:mm"))
                        Dim dtCheckout As DateTime = CDate(CDate(.Item("checkintime")).ToShortDateString & " " & CDate(.Item("checkouttime")).ToString("HH:mm"))
                        objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, dtCheckin, dtCheckout))
                    Else
                        objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, .Item("checkintime"), .Item("checkouttime")))
                    End If

                    'Pinkal (15-Nov-2013) -- End

                    'Pinkal (09-Nov-2013) -- End


                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtLogindate)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'START FOR INSERT LOGIN SUMMERY


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'InsertLoginSummary(objDataOperation, xDatabaseName _
                    '                 , xUserUnkid, xYearUnkid _
                    '               , xCompanyUnkid, mdtStartDate _
                    '               , mdtEndDate, xUserModeSetting _
                    '                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
                    '                 , blnPolicyManagementTNA _
                    '                 , blnDonotAttendanceinSeconds _
                    '                 , blnFirstCheckInLastCheckOut _
                    '                 , blnApplyUserAccessFilter, intEmpId _
                    '                 , strUserAccessLevelFilterString, mstrFilter)

                    InsertLoginSummary(objDataOperation, xDatabaseName _
                                     , xUserUnkid, xYearUnkid _
                                   , xCompanyUnkid, mdtStartDate _
                                   , mdtEndDate, xUserModeSetting _
                                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                     , blnPolicyManagementTNA _
                                     , blnDonotAttendanceinSeconds _
                                     , blnFirstCheckInLastCheckOut _
                                                  , blnIsHolidayConsiderOnWeekend _
                                                  , blnIsDayOffConsiderOnWeekend _
                                                  , blnIsHolidayConsiderOnDayoff _
                                     , blnApplyUserAccessFilter, intEmpId _
                                     , strUserAccessLevelFilterString, mstrFilter)

                    'Pinkal (11-AUG-2017) -- End



                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'END FOR INSERT LOGIN SUMMERY

                    ' START FOR INSERT AUDIT TRAIL

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'InsertAuditTrailLogin(objDataOperation, 2)
                    InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds)
                    'S.SANDEEP [04 JUN 2015] -- END


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    ' END FOR INSERT AUDIT TRAIL


                End With

            Next


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateForRoundOffOperation; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' END USED FOR ROUNDOFF CALCULATION 

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function RecalculateTimings(ByVal mdtEmployee As DataTable, ByVal xDatabaseName As String _
                                       , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                       , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                       , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                       , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                       , ByVal blnPolicyManagementTNA As Boolean, ByVal blnDonotAttendanceinSeconds As Boolean, ByVal blnFirstCheckInLastCheckOut As Boolean _
                                       , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                       , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                       , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                       , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intEmpId As Integer = -1 _
                                       , Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal strUserAccessLevelFilterString As String = "" _
                                       , Optional ByVal mstrFilter As String = "") As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnPolicyManagementTNA} -- END
        'Public Function RecalculateTimings(ByVal mdtStartDate As DateTime, ByVal mdtEndDate As DateTime, ByVal mdtEmployee As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim mintDays As Integer = DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date.AddDays(1))
            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim objShiftTran As New clsshift_tran
            Dim objEmployee As New clsEmployee_Master
            Dim objholiday As New clsemployee_holiday
            Dim objEmpDayOff As New clsemployee_dayoff_Tran

            For i As Integer = 0 To mdtEmployee.Rows.Count - 1

                mintEmployeeunkid = CInt(mdtEmployee.Rows(i)("employeeunkid"))

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = mintEmployeeunkid
                objEmployee._Employeeunkid(mdtEndDate) = mintEmployeeunkid
                'S.SANDEEP [04 JUN 2015] -- END

                For j As Integer = 0 To mintDays - 1

                    mdtLogindate = mdtStartDate.AddDays(j).Date
                    dsList = GetList(objDataOperation, "List", True)

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                            _Roundoff_Intime = Nothing
                            _Roundoff_Outtime = Nothing
                            _checkintime = Nothing
                            _Checkouttime = Nothing
                            _Totalhr = 0
                            _TotalOvertimehr = 0
                            _Ot2 = 0
                            _Ot3 = 0
                            _Ot4 = 0
                            _TotalNighthr = 0
                            _Shorthr = 0
                            _IsDayOffShift = False
                            _IsWeekend = False
                            _IsHoliday = False
                            _DayType = 0
                            _PolicyId = 0
                            _Shiftunkid = 0
                            _Early_Coming = 0
                            _Early_Going = 0
                            _Late_Coming = 0
                            _Late_Going = 0

                            _Loginunkid = CInt(dsList.Tables(0).Rows(k)("loginunkid"))

                            _Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(mdtLogindate, mintEmployeeunkid)

                            If IsDBNull(dsList.Tables(0).Rows(k)("checkintime")) OrElse IsDBNull(dsList.Tables(0).Rows(k)("Checkouttime")) Then Continue For

                            strQ = " SELECT lvleaveIssue_tran.leaveissuetranunkid, ISNULL(lvleaveIssue_tran.ispaid,0) as ispaid " & _
                                      " FROM lvleaveIssue_master " & _
                                      " JOIN lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
                                      " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                                      " WHERE lvleaveIssue_master.employeeunkid = @employeeunkid AND convert(char(8),lvleaveIssue_tran.leavedate,112) = @leavedate and lvleaveIssue_master.isvoid = 0"

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
                            Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then

                                If CBool(dList.Tables(0).Rows(0)("ispaid")) = True Then
                                    mblnIspaidLeave = True
                                    mblnIsUnpaidLeave = False
                                ElseIf CBool(dList.Tables(0).Rows(0)("ispaid")) = False Then
                                    mblnIsUnpaidLeave = True
                                    mblnIspaidLeave = False
                                End If

                            Else
                                mblnIsUnpaidLeave = False
                                mblnIspaidLeave = False
                            End If

                            objShiftTran.GetShiftTran(_Shiftunkid)
                            Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")

                            If drWeekend.Length > 0 Then
                                mblnIsWeekend = True
                            Else
                                mblnIsWeekend = False
                            End If


                            intEmpId = mintEmployeeunkid

                            Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                       , mdtStartDate, mdtEndDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter _
                                                                       , intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)

                            Dim drRow() As DataRow = dsData.Tables("List").Select("employeeunkid = " & mintEmployeeunkid & " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'")

                            For Each dr As DataRow In drRow
                                If eZeeDate.convertDate(dr("holidaydate")).Date = mdtLogindate.Date Then
                                    mblnIsholiday = True
                                Else
                                    mblnIsholiday = False
                                End If
                            Next

                            mblnIsDayOffshift = objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation)
                            mintearly_coming = 0
                            mintlate_coming = 0
                            mintearly_going = 0
                            mintlate_going = 0



                            'Pinkal (11-AUG-2017) -- Start
                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                            'If InsertLoginSummary(objDataOperation, xDatabaseName _
                            '                    , xUserUnkid, xYearUnkid _
                            '                    , xCompanyUnkid, mdtStartDate _
                            '                    , mdtEndDate, xUserModeSetting _
                            '                    , xOnlyApproved, xIncludeIn_ActiveEmployee _
                            '                    , blnPolicyManagementTNA _
                            '                    , blnDonotAttendanceinSeconds _
                            '                    , blnFirstCheckInLastCheckOut _
                            '                    , blnApplyUserAccessFilter, intEmpId _
                            '                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                            If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                , xUserUnkid, xYearUnkid _
                                                , xCompanyUnkid, mdtStartDate _
                                                , mdtEndDate, xUserModeSetting _
                                                , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                , blnPolicyManagementTNA _
                                                , blnDonotAttendanceinSeconds _
                                                , blnFirstCheckInLastCheckOut _
                                                             , blnIsHolidayConsiderOnWeekend _
                                                             , blnIsDayOffConsiderOnWeekend _
                                                             , blnIsHolidayConsiderOnDayoff _
                                                , blnApplyUserAccessFilter, intEmpId _
                                                , strUserAccessLevelFilterString, mstrFilter) = False Then

                                'Pinkal (11-AUG-2017) -- End


                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        Next

                    End If

                Next

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: RecalculateTimings; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function AutoRoundOff(ByVal mdtEmployee As DataTable, _
                                 ByVal xDatabaseName As String, _
                                 ByVal xUserUnkid As Integer, _
                                 ByVal xYearUnkid As Integer, _
                                 ByVal xCompanyUnkid As Integer, _
                                 ByVal mdtStartDate As DateTime, _
                                 ByVal mdtEndDate As DateTime, _
                                 ByVal xUserModeSetting As String, _
                                 ByVal xOnlyApproved As Boolean, _
                                 ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                 ByVal blnPolicyManagementTNA As Boolean, _
                                 ByVal blnDonotAttendanceinSeconds As Boolean, _
                                 ByVal blnFirstCheckInLastCheckOut As Boolean, _
                                 ByVal blnIsHolidayConsiderOnWeekend As Boolean, _
                                 ByVal blnIsDayOffConsiderOnWeekend As Boolean, _
                                 ByVal blnIsHolidayConsiderOnDayoff As Boolean, _
                                 Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                 Optional ByVal intEmpId As Integer = -1, _
                                 Optional ByVal objDataOperation As clsDataOperation = Nothing, _
                                 Optional ByVal strUserAccessLevelFilterString As String = "", _
                                 Optional ByVal mstrFilter As String = "") As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnPolicyManagementTNA,blnDonotAttendanceinSeconds} -- END
        'Public Function AutoRoundOff(ByVal mdtStartDate As DateTime, ByVal mdtEndDate As DateTime, ByVal mdtEmployee As DataTable) As Boolean


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim mintDays As Integer = DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date.AddDays(1))
            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim objPolicyTran As New clsPolicy_tran
            Dim objEmpPolicyTran As New clsemployee_policy_tran
            Dim objShiftTran As New clsshift_tran
            Dim objEmployee As New clsEmployee_Master
            Dim objholiday As New clsemployee_holiday
            Dim objEmpDayOff As New clsemployee_dayoff_Tran

            For i As Integer = 0 To mdtEmployee.Rows.Count - 1

                mintEmployeeunkid = CInt(mdtEmployee.Rows(i)("employeeunkid"))

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = mintEmployeeunkid
                objEmployee._Employeeunkid(mdtEndDate) = mintEmployeeunkid
                'S.SANDEEP [04 JUN 2015] -- END


                For j As Integer = 0 To mintDays - 1

                    mdtLogindate = mdtStartDate.AddDays(j).Date
                    dsList = GetList(objDataOperation, "List", True)

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                            Dim mdtShiftInTime As DateTime = Nothing
                            Dim mdtShiftOutTime As DateTime = Nothing
                            Dim mintltcome_graceinsec As Integer = 0
                            Dim mintelrcome_graceinsec As Integer = 0
                            Dim mintltgoing_graceinsec As Integer = 0
                            Dim mintelrgoing_graceinsec As Integer = 0

                            strQ = " SELECT lvleaveIssue_tran.leaveissuetranunkid,ISNULL(lvleaveIssue_tran.ispaid,0) AS ispaid " & _
                                      " FROM lvleaveIssue_master " & _
                                      " JOIN lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
                                      " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                                      " WHERE lvleaveIssue_master.employeeunkid = @employeeunkid AND convert(char(8),lvleaveIssue_tran.leavedate,112) = @leavedate and lvleaveIssue_master.isvoid = 0"

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
                            Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then

                                If CBool(dList.Tables(0).Rows(0)("ispaid")) = True Then
                                    mblnIspaidLeave = True
                                    mblnIsUnpaidLeave = False
                                ElseIf CBool(dList.Tables(0).Rows(0)("ispaid")) = False Then
                                    mblnIsUnpaidLeave = True
                                    mblnIspaidLeave = False
                                End If

                            Else
                                mblnIsUnpaidLeave = False
                                mblnIspaidLeave = False
                            End If

                            _Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(mdtLogindate, mintEmployeeunkid)
                            objShiftTran.GetShiftTran(_Shiftunkid)
                            Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")

                            If drWeekend.Length > 0 Then
                                mblnIsWeekend = True
                            Else
                                mblnIsWeekend = False
                            End If




                            'Pinkal (22-AUG-2017) -- Start
                            'Enhancement - Worked Performance Issue in Auto Round Off.

                            'Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate _
                            '                                          , mdtEndDate, xUserModeSetting, xOnlyApproved _
                            '                                          , xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter _
                            '                                          , mintEmployeeunkid, objDataOperation, strUserAccessLevelFilterString, mstrFilter)

                            'Dim drHoliday() As DataRow = dsData.Tables("List").Select("employeeunkid = " & mintEmployeeunkid & " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'")

                            'For Each dr As DataRow In drHoliday
                            '    If eZeeDate.convertDate(dr("holidaydate")).Date = mdtLogindate.Date Then
                            '        mblnIsholiday = True
                            '    Else
                            '        mblnIsholiday = False
                            '    End If
                            'Next
                            intEmpId = mintEmployeeunkid

                            mstrFilter = " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'"

                            Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate _
                                                                      , mdtEndDate, xUserModeSetting, xOnlyApproved _
                                                                      , xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter _
                                                                      , intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)

                            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                                If eZeeDate.convertDate(dsData.Tables(0).Rows(0)("holidaydate")).Date = mdtLogindate.Date Then
                                    mblnIsholiday = True
                                Else
                                    mblnIsholiday = False
                                End If
                            End If

                            'Pinkal (22-AUG-2017) -- End

                            mblnIsDayOffshift = objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation)

                            mintearly_coming = 0
                            mintlate_coming = 0
                            mintearly_going = 0
                            mintlate_going = 0


                            'Pinkal (21-Feb-2018) -- Start
                            'Enhancement - Forget to put Open shift Checking.
                            Dim objShift As New clsNewshift_master
                            objShift._Shiftunkid = _Shiftunkid
                            If objShift._IsOpenShift = False Then
                            Dim drDayName() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))
                            If drDayName.Length > 0 Then
                                mdtShiftInTime = CDate(mdtLogindate.Date & " " & CDate(drDayName(0)("starttime")).ToShortTimeString)
                                mdtShiftOutTime = CDate(mdtLogindate.Date & " " & CDate(drDayName(0)("endtime")).ToShortTimeString)
                            End If
                            End If
                            objShift = Nothing
                            'Pinkal (21-Feb-2018) -- End


                            _PolicyId = objEmpPolicyTran.GetPolicyTranID(mdtLogindate.Date, mintEmployeeunkid)
                            objPolicyTran.GetPolicyTran(_PolicyId)
                            Dim dtPolicyTran As DataTable = objPolicyTran._DataList
                            Dim drPolicy() As DataRow = dtPolicyTran.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))

                            If drPolicy.Length > 0 Then
                                mintelrcome_graceinsec = CInt(drPolicy(0)("elrcome_graceinsec"))
                                mintltcome_graceinsec = CInt(drPolicy(0)("ltcome_graceinsec"))
                                mintelrgoing_graceinsec = CInt(drPolicy(0)("elrgoing_graceinsec"))
                                mintltgoing_graceinsec = CInt(drPolicy(0)("ltgoing_graceinsec"))
                            End If

                            '---------------------------------- START FOR ROUNDING OFF IN TIME---------------------------------------


                            'Pinkal (28-Jan-2014) -- Start
                            'Enhancement : Oman Changes AS PER MR.PRABHAKAR REQUEST
                            'Dim mintMinLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Min(loginunkid)", "roundoff_intime IS NULL")), 0, dsList.Tables(0).Compute("Min(loginunkid)", "roundoff_intime IS NULL")))
                            Dim mintMinLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Min(loginunkid)", "")), 0, dsList.Tables(0).Compute("Min(loginunkid)", "")))
                            'Pinkal (28-Jan-2014) -- End



                            If mintMinLoginId > 0 Then     ' START FOR mintMinLoginId

                                Dim drRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMinLoginId)
                                If drRow.Length > 0 Then

                                    _Roundoff_Intime = Nothing
                                    _Roundoff_Outtime = Nothing
                                    _checkintime = Nothing
                                    _Checkouttime = Nothing
                                    _Totalhr = 0
                                    _TotalOvertimehr = 0
                                    _Ot2 = 0
                                    _Ot3 = 0
                                    _Ot4 = 0
                                    _TotalNighthr = 0
                                    _Shorthr = 0
                                    _Early_Coming = 0
                                    _Early_Going = 0
                                    _Late_Coming = 0
                                    _Late_Going = 0
                                    _IsDayOffShift = False
                                    _IsWeekend = False
                                    _IsHoliday = False
                                    _Loginunkid = 0

                                    If IsDBNull(drRow(0)("checkintime")) OrElse IsDBNull(drRow(0)("checkouttime")) Then Continue For

                                    _checkintime = CDate(drRow(0)("checkintime"))
                                    _Checkouttime = CDate(drRow(0)("checkouttime"))


                                    If mdtShiftInTime > CDate(drRow(0)("checkintime")) AndAlso DateDiff(DateInterval.Minute, CDate(drRow(0)("checkintime")), mdtShiftInTime) <= (mintelrcome_graceinsec / 60) _
                                        AndAlso DateDiff(DateInterval.Minute, CDate(drRow(0)("checkintime")), mdtShiftInTime) > 0 Then
                                        _Roundoff_Intime = CDate(drRow(0)("checkintime")).AddSeconds(DateDiff(DateInterval.Second, CDate(drRow(0)("checkintime")), mdtShiftInTime))

                                    ElseIf mdtShiftInTime < CDate(drRow(0)("checkintime")) AndAlso DateDiff(DateInterval.Minute, mdtShiftInTime, CDate(drRow(0)("checkintime"))) <= (mintltcome_graceinsec / 60) _
                                             AndAlso DateDiff(DateInterval.Minute, mdtShiftInTime, CDate(drRow(0)("checkintime"))) > 0 Then
                                        _Roundoff_Intime = CDate(drRow(0)("checkintime")).AddSeconds(DateDiff(DateInterval.Second, mdtShiftInTime, CDate(drRow(0)("checkintime"))) * -1)

                                    Else
                                        _Roundoff_Intime = _checkintime
                                    End If

                                End If

                                If _Roundoff_Intime <> Nothing Then   'START FOR _Roundoff_Intime

                                    strQ = " UPDATE tnalogin_tran SET roundoff_intime = @roundoff_intime,workhour = @workhour,operationtype = @operationtype,isgraceroundoff = @isgraceroundoff WHERE loginunkid = @loginunkid AND isvoid = 0 AND roundoff_intime IS NULL "
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Roundoff_Intime)
                                    objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, _Roundoff_Intime, _Checkouttime))
                                    objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                    objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLoginId)
                                    objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)

                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    _Loginunkid = mintMinLoginId
                                    _OperationType = enTnAOperation.RoundOff
                                    _Isgraceroundoff = True


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                    , xUserUnkid, xYearUnkid _
                                    '                    , xCompanyUnkid, mdtStartDate _
                                    '                    , mdtEndDate, xUserModeSetting _
                                    '                    , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                    , blnPolicyManagementTNA _
                                    '                    , blnDonotAttendanceinSeconds _
                                    '                    , blnFirstCheckInLastCheckOut _
                                    '                    , blnApplyUserAccessFilter, intEmpId _
                                    '                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                    If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                        , xUserUnkid, xYearUnkid _
                                                        , xCompanyUnkid, mdtStartDate _
                                                        , mdtEndDate, xUserModeSetting _
                                                        , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                        , blnPolicyManagementTNA _
                                                        , blnDonotAttendanceinSeconds _
                                                        , blnFirstCheckInLastCheckOut _
                                                                      , blnIsHolidayConsiderOnWeekend _
                                                                      , blnIsDayOffConsiderOnWeekend _
                                                                      , blnIsHolidayConsiderOnDayoff _
                                                        , blnApplyUserAccessFilter, intEmpId _
                                                        , strUserAccessLevelFilterString, mstrFilter) = False Then

                                        'Pinkal (11-AUG-2017) -- End

                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If   'END FOR _Roundoff_Intime


                            End If   ' END FOR mintMinLoginId

                            '---------------------------------- END FOR ROUNDING OFF IN TIME---------------------------------------

                            '---------------------------------- START FOR ROUNDING OFF OUT TIME---------------------------------------

                            _Roundoff_Intime = Nothing
                            _Roundoff_Outtime = Nothing
                            dsList = GetList(objDataOperation, "List", True)


                            'Pinkal (28-Jan-2014) -- Start
                            'Enhancement : Oman Changes AS PER MR.PRABHAKAR REQUEST
                            'Dim mintMaxLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Max(loginunkid)", "roundoff_outtime IS NULL")), 0, dsList.Tables(0).Compute("Max(loginunkid)", "roundoff_outtime IS NULL")))
                            Dim mintMaxLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Max(loginunkid)", "")), 0, dsList.Tables(0).Compute("Max(loginunkid)", "")))
                            'Pinkal (28-Jan-2014) -- End

                            If mintMaxLoginId > 0 Then  ' START FOR mintMaxLoginId

                                Dim drRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMaxLoginId)
                                If drRow.Length > 0 Then

                                    _Roundoff_Intime = Nothing
                                    _Roundoff_Outtime = Nothing
                                    _checkintime = Nothing
                                    _Checkouttime = Nothing
                                    _Totalhr = 0
                                    _TotalOvertimehr = 0
                                    _Ot2 = 0
                                    _Ot3 = 0
                                    _Ot4 = 0
                                    _TotalNighthr = 0
                                    _Shorthr = 0
                                    _Early_Coming = 0
                                    _Early_Going = 0
                                    _Late_Coming = 0
                                    _Late_Going = 0
                                    _IsDayOffShift = False
                                    _IsWeekend = False
                                    _IsHoliday = False

                                    If IsDBNull(drRow(0)("checkintime")) OrElse IsDBNull(drRow(0)("checkouttime")) Then Continue For

                                    _checkintime = CDate(drRow(0)("checkintime"))
                                    _Checkouttime = CDate(drRow(0)("checkouttime"))

                                    If mdtShiftOutTime > CDate(drRow(0)("checkouttime")) AndAlso DateDiff(DateInterval.Minute, CDate(drRow(0)("checkouttime")), mdtShiftOutTime) <= (mintelrgoing_graceinsec / 60) _
                                        AndAlso DateDiff(DateInterval.Minute, CDate(drRow(0)("checkouttime")), mdtShiftOutTime) > 0 Then
                                        _Roundoff_Outtime = CDate(drRow(0)("checkouttime")).AddSeconds(DateDiff(DateInterval.Second, CDate(drRow(0)("checkouttime")), mdtShiftOutTime))

                                    ElseIf mdtShiftOutTime < CDate(drRow(0)("checkouttime")) AndAlso DateDiff(DateInterval.Minute, mdtShiftOutTime, CDate(drRow(0)("checkouttime"))) <= (mintltgoing_graceinsec / 60) _
                                             AndAlso DateDiff(DateInterval.Minute, mdtShiftOutTime, CDate(drRow(0)("checkouttime"))) > 0 Then
                                        _Roundoff_Outtime = CDate(drRow(0)("checkouttime")).AddSeconds(DateDiff(DateInterval.Second, mdtShiftOutTime, CDate(drRow(0)("checkouttime"))) * -1)

                                    Else
                                        _Roundoff_Outtime = _Checkouttime
                                    End If

                                End If

                                If _Roundoff_Outtime <> Nothing Then   ' START FOR _Roundoff_Outtime

                                    strQ = " UPDATE tnalogin_tran SET roundoff_outtime = @roundoff_outtime,workhour = @workhour,operationtype = @operationtype,isgraceroundoff = @isgraceroundoff WHERE loginunkid = @loginunkid AND isvoid = 0 AND roundoff_outtime IS NULL "
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Roundoff_Outtime)

                                    If Not IsDBNull(drRow(0)("roundoff_intime")) Then
                                        objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drRow(0)("roundoff_intime")), _Roundoff_Outtime))
                                    Else
                                        objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, _checkintime, _Roundoff_Outtime))
                                    End If
                                    objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                    objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoginId)
                                    objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    _Loginunkid = mintMaxLoginId
                                    _OperationType = enTnAOperation.RoundOff
                                    _Isgraceroundoff = True


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                     , xUserUnkid, xYearUnkid _
                                    '                     , xCompanyUnkid, mdtStartDate _
                                    '                     , mdtEndDate, xUserModeSetting _
                                    '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                     , blnPolicyManagementTNA _
                                    '                     , blnDonotAttendanceinSeconds _
                                    '                     , blnFirstCheckInLastCheckOut _
                                    '                     , blnApplyUserAccessFilter, intEmpId _
                                    '                     , strUserAccessLevelFilterString, mstrFilter) = False Then

                                    If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                         , xUserUnkid, xYearUnkid _
                                                         , xCompanyUnkid, mdtStartDate _
                                                         , mdtEndDate, xUserModeSetting _
                                                         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                         , blnPolicyManagementTNA _
                                                         , blnDonotAttendanceinSeconds _
                                                         , blnFirstCheckInLastCheckOut _
                                                                       , blnIsHolidayConsiderOnWeekend _
                                                                       , blnIsDayOffConsiderOnWeekend _
                                                                       , blnIsHolidayConsiderOnDayoff _
                                                         , blnApplyUserAccessFilter, intEmpId _
                                                         , strUserAccessLevelFilterString, mstrFilter) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'Pinkal (11-AUG-2017) -- End

                                    If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                ElseIf IsDBNull(drRow(0)("roundoff_intime")) = False AndAlso _Roundoff_Outtime = Nothing Then

                                    If IsDBNull(drRow(0)("roundoff_intime")) Then
                                        strQ = " UPDATE tnalogin_tran SET roundoff_intime = checkintime,operationtype = @operationtype,isgraceroundoff = @isgraceroundoff WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                        objDataOperation.ClearParameters()
                                        objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                        objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLoginId)
                                        objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                        objDataOperation.ExecNonQuery(strQ)
                                    End If

                                    strQ = " UPDATE tnalogin_tran SET roundoff_outtime = checkouttime,workhour = @workhour,operationtype = @operationtype,isgraceroundoff = @isgraceroundoff WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drRow(0)("roundoff_intime")), _Checkouttime))
                                    objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                    objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoginId)
                                    objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    _Loginunkid = mintMaxLoginId
                                    _OperationType = enTnAOperation.RoundOff
                                    _Isgraceroundoff = True


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                     , xUserUnkid, xYearUnkid _
                                    '                     , xCompanyUnkid, mdtStartDate _
                                    '                     , mdtEndDate, xUserModeSetting _
                                    '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                     , blnPolicyManagementTNA _
                                    '                     , blnDonotAttendanceinSeconds _
                                    '                     , blnFirstCheckInLastCheckOut _
                                    '                     , blnApplyUserAccessFilter, intEmpId _
                                    '                     , strUserAccessLevelFilterString, mstrFilter) = False Then

                                    If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                         , xUserUnkid, xYearUnkid _
                                                         , xCompanyUnkid, mdtStartDate _
                                                         , mdtEndDate, xUserModeSetting _
                                                         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                         , blnPolicyManagementTNA _
                                                         , blnDonotAttendanceinSeconds _
                                                         , blnFirstCheckInLastCheckOut _
                                                                       , blnIsHolidayConsiderOnWeekend _
                                                                       , blnIsHolidayConsiderOnWeekend _
                                                                       , blnIsHolidayConsiderOnDayoff _
                                                         , blnApplyUserAccessFilter, intEmpId _
                                                         , strUserAccessLevelFilterString, mstrFilter) = False Then

                                        'Pinkal (11-AUG-2017) -- End

                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                    If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                        'S.SANDEEP [04 JUN 2015] -- END
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If     ' END FOR _Roundoff_Outtime 


                            End If   ' END FOR mintMaxLoginId

                            '---------------------------------- END FOR ROUNDING OFF OUT TIME---------------------------------------


                        Next

                    End If

                Next

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: AutoRoundOff; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnalogin_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal xDatabaseName As String _
                                                        , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                        , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                                        , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                                        , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                        , ByVal blnPolicyManagementTNA As Boolean, ByVal blnFirstCheckInLastCheckOut As Boolean _
                                                        , ByVal blnDonotAttendanceinSeconds As Boolean _
                                                        , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                                        , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                                        , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                                        , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                        , Optional ByVal intEmpId As Integer = -1 _
                                                        , Optional ByVal objDataOperation As clsDataOperation = Nothing _
                                                        , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                                        , Optional ByVal mstrFilter As String = "") As Boolean

        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            strQ = "Update tnalogin_tran set isvoid = 1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
            "WHERE loginunkid = @loginunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.ExecNonQuery(strQ)

            If blnFirstCheckInLastCheckOut = False Then
                UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtcheckintime)
            End If


            'START FOR INSERT LOGIN SUMMERY

            strQ = "Select loginsummaryunkid from tnalogin_summary where login_date = @login_date and employeeunkid = @emp_unkid"
            objDataOperation.AddParameter("@emp_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@login_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            dsList = objDataOperation.ExecQuery(strQ, "GetLoginSummaryID")
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                mintLoginsummaryunkid = CInt(dsList.Tables(0).Rows(0)("loginsummaryunkid"))
            End If


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.

            'InsertLoginSummary(objDataOperation, xDatabaseName _
            '                 , xUserUnkid, xYearUnkid _
            '                 , xCompanyUnkid, mdtStartDate _
            '                 , mdtEndDate, xUserModeSetting _
            '                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                 , blnPolicyManagementTNA _
            '                 , blnDonotAttendanceinSeconds _
            '                 , blnFirstCheckInLastCheckOut _
            '                 , blnApplyUserAccessFilter, intEmpId _
            '                 , strUserAccessLevelFilterString, mstrFilter, mintLoginsummaryunkid)

            InsertLoginSummary(objDataOperation, xDatabaseName _
                             , xUserUnkid, xYearUnkid _
                             , xCompanyUnkid, mdtStartDate _
                             , mdtEndDate, xUserModeSetting _
                             , xOnlyApproved, xIncludeIn_ActiveEmployee _
                             , blnPolicyManagementTNA _
                             , blnDonotAttendanceinSeconds _
                             , blnFirstCheckInLastCheckOut _
                                        , blnIsHolidayConsiderOnWeekend _
                                        , blnIsDayOffConsiderOnWeekend _
                                        , blnIsHolidayConsiderOnDayoff _
                             , blnApplyUserAccessFilter, intEmpId _
                             , strUserAccessLevelFilterString, mstrFilter, mintLoginsummaryunkid)

            'Pinkal (11-AUG-2017) -- End


            'END FOR INSERT LOGIN SUMMERY

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'START FOR INSERT AUDIT TRAIL LOGIN

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'InsertAuditTrailLogin(objDataOperation, 3)
            InsertAuditTrailLogin(objDataOperation, 3, blnDonotAttendanceinSeconds)
            'S.SANDEEP [04 JUN 2015] -- END

            'END FOR INSERT AUDIT TRAIL LOGIN

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnalogin_tran) </purpose>
    Public Function GetLoginSourceType(ByVal intEmployeeid As Integer, ByVal logindate As DateTime, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Try
            Dim dsList As DataSet = Nothing
            Dim strQ As String = ""
            Dim exForce As Exception

            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            strQ = " Select isnull(Sourcetype,-1) as Sourcetype from tnalogin_tran " & _
                      " where employeeunkid = @emloyeeunkid and convert(char(8),logindate,112) = @logindate "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emloyeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(logindate))
            dsList = objDataOperation.ExecQuery(strQ, "Source")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables("Source").Rows.Count > 0 Then
                Return CInt(dsList.Tables("Source").Rows(0)("Sourcetype"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoginSourceType", mstrModuleName)
        End Try
        Return -1
    End Function


    'START FOR USE ONLY FOR IMPORTING TIMSESHEET DATA OTHERWISE NOT TO USE

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnalogin_tran) </purpose>
    Public Function Delete(ByVal intEmployeeId As Integer, ByVal dtDate As Date, ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean
        'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

        'Public Function Delete(ByVal intEmployeeId As Integer, ByVal dtDate As Date) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            mintEmployeeunkid = intEmployeeId
            mdtLogindate = dtDate

            dsList = GetList(objDataOperation, "List", False)


            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If dsList IsNot Nothing Then
                If dsList.Tables("List").Rows.Count > 0 Then
                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        '     If CInt(dsList.Tables("List").Rows(i)("holdunkid")) > 0 Then
                        mintLoginunkid = CInt(dsList.Tables("List").Rows(i)("loginunkid"))
                        mdtcheckintime = CDate(dsList.Tables("List").Rows(i)("checkintime"))
                        mdtCheckouttime = CDate(dsList.Tables("List").Rows(i)("checkouttime"))
                        mintHoldunkid = CInt(dsList.Tables("List").Rows(i)("holdunkid"))
                        mintWorkhour = CInt(dsList.Tables("List").Rows(i)("workhour"))
                        mintBreakhr = CInt(dsList.Tables("List").Rows(i)("breakhr"))
                        mstrRemark = dsList.Tables("List").Rows(i)("remark").ToString
                        mintInOuType = dsList.Tables("List").Rows(i)("inouttype").ToString

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'InsertAuditTrailLogin(objDataOperation, 3)
                        InsertAuditTrailLogin(objDataOperation, 3, blnDonotAttendanceinSeconds)
                        'S.SANDEEP [04 JUN 2015] -- END

                        'End If
                    Next

                End If
            End If

            'Pinkal (29-Aug-2012) -- End

            strQ = "DELETE " & _
                "FROM tnalogin_summary " & _
                "WHERE CONVERT(CHAR(8),login_date,112) = @dtdate " & _
                "AND employeeunkid = @employeeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@dtdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDate))
            objDataOperation.ExecNonQuery(strQ)

            strQ = "DELETE " & _
                   "FROM tnalogin_tran " & _
                   "WHERE CONVERT(CHAR(8),logindate,112) = @dtdate " & _
                   "AND employeeunkid = @employeeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@dtdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDate))

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Delete", mstrModuleName)
            Return False
        Finally
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnalogin_tran) </purpose>
    Public Function IsEmployeeLoginExist(ByVal mdtDate As String, ByVal mstrEmployeeCode As String, Optional ByVal intEmployeeId As Integer = -1, Optional ByRef intLoginunkid As Integer = -1, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim iCnt As Integer = 0

        Try
            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT tnalogin_tran.loginunkid " & _
                      ", hremployee_master.employeeunkid " & _
                  "FROM tnalogin_tran " & _
                      "LEFT JOIN hremployee_master " & _
                      "ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
                  "WHERE CONVERT(CHAR(8), logindate, 112) = @dtdate " & _
               " AND tnalogin_tran.isvoid = 0 "

            If intEmployeeId > 0 Then
                strQ &= " AND tnalogin_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            End If

            If mstrEmployeeCode.Trim.Length > 0 Then
                strQ &= " AND hremployee_master.employeecode = @employeecode "
                objDataOperation.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeCode)
            End If

            objDataOperation.AddParameter("@dtdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, mdtDate)


            'Pinkal (24-Dec-2013) -- Start
            'Enhancement : Oman Changes

            'iCnt = objDataOperation.RecordCount(strQ)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                intLoginunkid = CInt(dsList.Tables(0).Rows(0)("loginunkid"))
                iCnt = dsList.Tables(0).Rows.Count
            End If

            'Pinkal (24-Dec-2013) -- End


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return (iCnt > 0)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "IsEmployeeExist", mstrModuleName)
            Return True
        Finally

            If objDoOperation Is Nothing Then
                If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
                objDataOperation = Nothing
            End If
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnalogin_tran) </purpose>
    Public Function GetEmployeeLoginDate(ByVal strList As String, ByVal mintEmployeeID As Integer, ByVal mdtLoginLogoutTime As Date) As DataSet
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing

        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT  tnalogin_tran.loginunkid " & _
                          ", tnalogin_tran.employeeunkid " & _
                          ", tnalogin_tran.logindate " & _
                          ", tnalogin_tran.checkintime " & _
                          ", tnalogin_tran.checkouttime " & _
                          ", tnalogin_tran.holdunkid " & _
                          ", tnalogin_tran.workhour " & _
                          ", tnalogin_tran.breakhr " & _
                          ", tnalogin_tran.remark " & _
                          ", tnalogin_tran.isvoid " & _
                          ", tnalogin_tran.voiduserunkid " & _
                          ", tnalogin_tran.voidreason " & _
                          ", tnalogin_tran.inouttype " & _
                          ", tnalogin_tran.userunkid " & _
                          ", tnalogin_tran.voiddatetime " & _
                          ", tnalogin_tran.sourcetype " & _
                    "FROM    tnalogin_tran " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid  " & _
                    "WHERE ISNULL(isvoid, 0) = 0 AND hremployee_master.employeeunkid = @employeeunkid "


            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes

            'strQ &= "AND (checkintime = @logintime OR checkouttime = @logintime) "

            strQ &= "AND (original_intime = @logintime OR original_outtime = @logintime) "

            'Pinkal (15-Nov-2013) -- End


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
            objDataOperation.AddParameter("@logintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLoginLogoutTime)

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLoginDate; Module Name: " & mstrModuleName)
        Finally
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
        Return dsList
    End Function

    'END FOR USE ONLY FOR IMPORTING TIMSESHEET DATA OTHERWISE NOT TO USE


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function CheckforEmployee(ByVal intEmployeeid As Integer, ByVal dtLogindate As DateTime) As Integer
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT * " & _
           "FROM tnalogin_tran " & _
           "WHERE employeeunkid = @employeeunkid " & _
           "AND convert(char(10),logindate,101) = @logindate and checkouttime is not null and isvoid = 0"


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtLogindate)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CheckforEmployee; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsExistTime(ByVal dtTime As Date, Optional ByVal intUnkId As Integer = -1, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean  'Pinkal (20-Sep-2013) -- Start [Optional ByVal objDataOperation As clsDataOperation = Nothing]
        Dim strQ As String
        Dim strName As String = ""
        Dim exForce As Exception
        Dim intCnt As Integer = 1
        Try


            'Pinkal (20-Sep-2013) -- Start
            'Enhancement : TRA Changes

            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            'Pinkal (20-Sep-2013) -- End



            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	

            strQ = "SELECT  " & _
                        " loginunkid " & _
                    "FROM tnalogin_tran WITH (NOLOCK) " & _
                    "WHERE employeeunkid = @empid " & _
                    "AND CONVERT(CHAR(8),logindate,112) = @dtdate " & _
                    "AND @dtintime  BETWEEN checkintime AND checkouttime AND isvoid = 0"

            'Pinkal (11-Feb-2022) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@dtdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLogindate))
            objDataOperation.AddParameter("@dtintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtTime)

            If intUnkId <> -1 Then
                strQ &= "AND loginunkid <> @loginunkid "
                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)
            End If

            intCnt = objDataOperation.RecordCount(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw (exForce)
            End If

            Return (intCnt > 0)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "IsExistTime", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function


    ' 'Pinkal (07-Jan-2015) -- Start
    ' Enhancement - CHANGES IN IMPORT TIME & ATTENDANCE DATA FROM DEVICE FOR OCEAN LINK

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoginType(ByVal intEmployeeId As Integer, Optional ByVal dtLoginDate As Date = Nothing) As Integer
        Dim inType As Integer = -1
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            objDataOperation = New clsDataOperation


            strQ = "SELECT " & _
                       "  inouttype " & _
                       ", checkintime " & _
                       ", loginunkid " & _
                       ", CONVERT(CHAR(8),logindate,112) AS logindate " & _
                   " FROM tnalogin_tran " & _
                   " WHERE employeeunkid = @employeeid AND isvoid = 0 "

            If dtLoginDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),logindate,112) = @logindate"
                objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtLoginDate))
            End If

            strQ &= " ORDER BY checkintime  "

            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtrow As DataRow In dsList.Tables(0).Rows
                inType = dtrow.Item("inouttype")
                If IsDBNull(dtrow.Item("checkintime")) = False Then
                    mdtcheckintime = dtrow.Item("checkintime")
                End If
                mdtLogindate = eZeeDate.convertDate(dtrow.Item("logindate"))
                mintLoginunkid = CInt(dtrow.Item("loginunkid"))
            Next

            If inType = 1 Or inType = -1 Then
                Return 0
            Else
                Return 1
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getLoginType", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    ' 'Pinkal (07-Jan-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsValidPassword(ByVal intEmployeeid As Integer, ByVal strPassword As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim Count As Integer = 1
        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                        "employeeunkid " & _
                    "FROM hremployee_master " & _
                    "WHERE isactive = 1 " & _
                    "AND password = @password " & _
                    "AND employeeunkid=@empid"

            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(strPassword, "ezee"))
            objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)

            Count = objDataOperation.RecordCount(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return (Count > 0)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "isExist", mstrModuleName)
            Return True
        Finally
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateBreakTime(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal dtDate As Date) As Boolean
        Dim strQ As String
        Dim dsList As New DataSet
        Dim intBreakhr As Integer

        Try

            mintEmployeeunkid = intEmpId
            mdtLogindate = dtDate.Date
            dsList = GetList(objDataOperation, "List", True)

            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1

                objDataOperation.ClearParameters()

                strQ = "UPDATE tnalogin_tran SET " & _
                       " breakhr = @breakhr " & _
                       "WHERE loginunkid = @loginunkid AND isvoid = 0 "
                If i = 0 Then
                    intBreakhr = 0
                Else
                    If dsList.Tables("List").Rows(i - 1).Item("checkouttime") Is DBNull.Value Or dsList.Tables("List").Rows(i - 1).Item("checkouttime").ToString = "" Then
                        dsList.Tables("List").Rows(i - 1).Item("checkouttime") = CDate(dsList.Tables("List").Rows(i).Item("checkintime"))
                    End If

                    If CDate(dsList.Tables("List").Rows(i - 1).Item("checkouttime")) < CDate(dsList.Tables("List").Rows(i).Item("checkintime")) Then
                        intBreakhr = DateDiff(DateInterval.Second, CDate(dsList.Tables("List").Rows(i - 1).Item("checkouttime")), CDate(dsList.Tables("List").Rows(i).Item("checkintime")))
                    Else
                        intBreakhr = DateDiff(DateInterval.Second, CDate(dsList.Tables("List").Rows(i).Item("checkintime")), CDate(dsList.Tables("List").Rows(i - 1).Item("checkouttime")))
                    End If

                End If

                objDataOperation.AddParameter("@breakhr", SqlDbType.Int, eZeeDataType.INT_SIZE, intBreakhr)
                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables("List").Rows(i).Item("loginunkid")))

                objDataOperation.ExecNonQuery(strQ)

            Next
            Return True

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "UpdateBreakTime", mstrModuleName)
            Return False
        End Try
    End Function

    ' START FOR THIS METHOD IS SPECIALLY USED FOR EMPLOYEE ABSENT

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation, ByVal blnDonotAttendanceinSeconds As Boolean, ByVal blnFirstCheckInLastCheckOut As Boolean) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds,blnFirstCheckInLastCheckOut} -- END
        'Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)


            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._DonotAttendanceinSeconds Then
            If blnDonotAttendanceinSeconds Then
                'S.SANDEEP [04 JUN 2015] -- END
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, CDate(mdtcheckintime.ToShortDateString & " " & mdtcheckintime.ToString("HH:mm")), DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))

                'Pinkal (24-Dec-2013) -- Start
                'Enhancement : Oman Changes
                'objDataOperation.AddParameter("@original_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Intime <> Nothing, CDate(mdtOriginal_Intime.ToShortDateString & " " & mdtOriginal_Intime.ToString("hh:MM")), DBNull.Value))
                'objDataOperation.AddParameter("@original_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Outtime <> Nothing, CDate(mdtOriginal_Outtime.ToShortDateString & " " & mdtOriginal_Outtime.ToString("HH:mm")), DBNull.Value))
                'Pinkal (24-Dec-2013) -- End
            Else
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            objDataOperation.AddParameter("@original_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
            objDataOperation.AddParameter("@original_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))

            'Pinkal (15-Nov-2013) -- End


            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
            objDataOperation.AddParameter("@workhour", SqlDbType.Float, eZeeDataType.INT_SIZE, mintWorkhour.ToString)
            objDataOperation.AddParameter("@breakhr", SqlDbType.Float, eZeeDataType.INT_SIZE, mintBreakhr.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@inouttype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInOuType.ToString)
            objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSourceType.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)


            strQ = "INSERT INTO tnalogin_tran ( " & _
              "  employeeunkid " & _
              ", logindate " & _
              ", checkintime " & _
              ", checkouttime " & _
              ", holdunkid " & _
              ", workhour " & _
              ", breakhr " & _
              ", remark " & _
              ", inouttype " & _
              ", sourcetype " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ",original_intime " & _
              ",original_outtime " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @logindate " & _
              ", @checkintime " & _
              ", @checkouttime " & _
              ", @holdunkid " & _
              ", @workhour " & _
              ", @breakhr " & _
              ", @remark " & _
              ", @inouttype " & _
              ", @sourcetype " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @original_intime " & _
              ", @original_outtime " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")


            'S.SANDEEP [ 07 JAN 2014 ] -- START
            'UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtLogindate)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If blnFirstCheckInLastCheckOut = False Then
                'If ConfigParameter._Object._FirstCheckInLastCheckOut = False Then
                'S.SANDEEP [04 JUN 2015] -- END
                UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtLogindate)
            End If
            'S.SANDEEP [ 07 JAN 2014 ] -- END



            mintLoginunkid = dsList.Tables(0).Rows(0).Item(0)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'InsertAuditTrailLogin(objDataOperation, 1)
            InsertAuditTrailLogin(objDataOperation, 1, blnDonotAttendanceinSeconds)
            'S.SANDEEP [04 JUN 2015] -- END


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Pinkal (12-Oct-2011) -- End



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ' END FOR THIS METHOD IS SPECIALLY USED FOR EMPLOYEE ABSENT

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function GlobalAddTimesheetData(ByVal mstrEmployeeId As String, ByVal dtFromDate As DateTime, ByVal dtToDate As DateTime _
                                            , ByVal mblnIncludeHoliday As Boolean, ByVal mblnIncludeWeekend As Boolean, ByVal mblnIncludeDayOff As Boolean _
                                            , ByVal dtIntime As DateTime, ByVal dtOuttime As DateTime, ByVal mblnOverWriteIfExist As Boolean _
                                            , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer, ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                            , ByVal blnPolicyManagementTNA As Boolean, ByVal blnDonotAttendanceinSeconds As Boolean _
                                            , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                            , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                            , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                            , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                            , Optional ByVal intEmpId As Integer = -1 _
                                            , Optional ByVal objDataOperation As clsDataOperation = Nothing _
                                            , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                            , Optional ByVal mstrFilter As String = "", Optional ByVal intShiftunkId As Integer = -1 _
                                            , Optional ByVal intPolicyunkId As Integer = -1) As Boolean
        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'blnPolicyManagementTNA ----------------------------------------------------------- ADDED
        'S.SANDEEP [04 JUN 2015] -- END


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End


        Dim blnFlag As Boolean = False
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim mstrReason As String = mstrVoidReason
            Dim intDays As Integer = DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date) + 1
            Dim arEmpId() As String = mstrEmployeeId.Trim.Split(",")

            Dim objEmpHoliday As New clsemployee_holiday
            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim objShiftTran As New clsshift_tran
            Dim objEmpPolicyTran As New clsemployee_policy_tran
            Dim objPolicyTran As New clsPolicy_tran
            Dim objEmployee As New clsEmployee_Master

            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            mstrMessage = ""
            'Pinkal (13-Jan-2015) -- End


                'Pinkal (30-Jul-2015) -- Start
                'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)

                Dim objconfig As New clsConfigOptions
                objconfig._Companyunkid = mintCompanyId

            For i As Integer = 0 To arEmpId.Length - 1

                mintEmployeeunkid = CInt(arEmpId(i))
                objEmployee._Employeeunkid(dtToDate) = mintEmployeeunkid


                For j As Integer = 0 To intDays - 1

                    If objconfig._AllowToChangeAttendanceAfterPayment = False Then

                        'Pinkal (13-Jan-2015) -- Start
                        'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
                        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                            Dim objPaymentTran As New clsPayment_tran
                            Dim objPeriod As New clsMasterData
                            Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtFromDate.AddDays(j).Date)

                            Dim mdtTnAStartdate As Date = Nothing
                            If intPeriodId > 0 Then
                                Dim objTnAPeriod As New clscommom_period_Tran
                                mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                            End If

                            If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintEmployeeunkid, mdtTnAStartdate, intPeriodId, dtFromDate.AddDays(j).Date) > 0 Then
                                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Payroll payment is already done for the checked employee(s) for the selected date tenure, due to this some employees will be skipped for that particular date(s) during this process.")
                                Continue For
                            End If
                        End If
                        'Pinkal (13-Jan-2015) -- End
                    End If

                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.
                    'objconfig = Nothing
                    'Pinkal (06-Jan-2016) -- End


                    'Pinkal (30-Jul-2015) -- End

                    Dim dsTermination As New DataSet
                    Dim dsRetirement As New DataSet

                    Dim objDates As New clsemployee_dates_tran
                    dsTermination = objDates.Get_Current_Dates(dtToDate, enEmp_Dates_Transaction.DT_TERMINATION, mintEmployeeunkid)
                    dsRetirement = objDates.Get_Current_Dates(dtToDate, enEmp_Dates_Transaction.DT_RETIREMENT, mintEmployeeunkid)
                    objDates = Nothing

                    mdtLogindate = dtFromDate.AddDays(j)

                    Dim StartDate As Date = IIf(objEmployee._Appointeddate.Date > mdtLogindate, objEmployee._Appointeddate.Date, mdtLogindate)
                    Dim EndDate As Date = mdtLogindate


                    'Pinkal (13-Jul-2018) -- Start
                    'Enhancement - Solved Issue in Abood for adding Employee.

                    'If dsTermination.Tables(0).Rows.Count > 0  AndAlso CDate(dsTermination.Tables(0).Rows(0)("date2")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date2")).Date < mdtLogindate, CDate(dsTermination.Tables(0).Rows(0)("date2")).Date, mdtLogindate.Date)
                    'End If

                    'If dsRetirement.Tables(0).Rows.Count > 0  AndAlso CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date, EndDate.Date)
                    'End If

                    'If dsTermination.Tables(0).Rows.Count > 0  AndAlso CDate(dsTermination.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsTermination.Tables(0).Rows(0)("date1")).Date, EndDate)
                    'End If


                    If dsTermination.Tables(0).Rows.Count > 0 AndAlso IsDBNull(dsTermination.Tables(0).Rows(0)("date2")) = False AndAlso CDate(dsTermination.Tables(0).Rows(0)("date2")).Date <> Nothing Then
                        EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date2")).Date < mdtLogindate, CDate(dsTermination.Tables(0).Rows(0)("date2")).Date, mdtLogindate.Date)
                    End If

                    If dsRetirement.Tables(0).Rows.Count > 0 AndAlso IsDBNull(dsRetirement.Tables(0).Rows(0)("date1")) = False AndAlso CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                        EndDate = IIf(CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date, EndDate.Date)
                    End If

                    If dsTermination.Tables(0).Rows.Count > 0 AndAlso IsDBNull(dsTermination.Tables(0).Rows(0)("date1")) = False AndAlso CDate(dsTermination.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                        EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsTermination.Tables(0).Rows(0)("date1")).Date, EndDate)
                    End If

                    'Pinkal (13-Jul-2018) -- End

                    dsTermination.Dispose() : dsRetirement.Dispose()


                    If StartDate.Date > mdtLogindate OrElse EndDate < mdtLogindate Then
                        Continue For
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    '========== START FOR EXCLUDE EMPLOYEE HOLIDAY==================================
                    Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, mdtLogindate)
                    If dtHoliday.Rows.Count > 0 AndAlso mblnIncludeHoliday = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE HOLIDAY====================================


                    '========== START FOR EXCLUDE EMPLOYEE WEEKEND==================================
                    Dim intShiftID As Integer = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtLogindate, mintEmployeeunkid)

                    If intShiftunkId > 0 AndAlso intShiftunkId <> intShiftID Then
                        Continue For

                    ElseIf intShiftID <= 0 Then
                        Continue For

                    End If

                    objShiftTran.GetShiftTran(intShiftID)
                    Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                    If drRow.Length > 0 AndAlso mblnIncludeWeekend = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE WEEKEND===================================


                    '========== START FOR EXCLUDE EMPLOYEE DAY OFF==================================

                    If objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation) AndAlso mblnIncludeDayOff = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE DAY OFF====================================



                    'Pinkal (09-Jun-2015) -- Start
                    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                    Dim intPolicyId As Integer = 0

                    If blnPolicyManagementTNA Then
                        intPolicyId = objEmpPolicyTran.GetPolicyTranID(mdtLogindate, mintEmployeeunkid)
                    If intPolicyunkId > 0 Then
                            'Pinkal (06-Jan-2016) -- Start
                            'Enhancement - Working on Changes in SS for Leave Module.
                            'intPolicyId = objEmpPolicyTran.GetPolicyTranID(mdtLogindate, mintEmployeeunkid)
                            'Pinkal (06-Jan-2016) -- End
                            If intPolicyunkId <> intPolicyId Then Continue For
                        ElseIf intPolicyId <= 0 Then
                            Continue For
                        End If
                    End If

                    'Pinkal (09-Jun-2015) -- End


                    mintLoginunkid = 0
                    mdtcheckintime = CDate(dtIntime.AddDays(j).Date & " " & dtIntime.ToShortTimeString)
                    mdtCheckouttime = CDate(dtOuttime.AddDays(j).Date & " " & dtOuttime.ToShortTimeString)
                    mdtOriginal_Intime = CDate(dtIntime.AddDays(j).Date & " " & dtIntime.ToShortTimeString)
                    mdtOriginal_Outtime = CDate(dtOuttime.AddDays(j).Date & " " & dtOuttime.ToShortTimeString)
                    mintSourceType = enInOutSource.GlobalAdd
                    mintInOuType = 1
                    mintWorkhour = DateDiff(DateInterval.Second, mdtcheckintime, mdtCheckouttime)
                    mintShiftunkid = intShiftID
                    mintPolicyId = intPolicyId

                    mblnIsholiday = False
                    mblnIspaidLeave = False
                    mblnIsUnpaidLeave = False
                    mblnIsDayOffshift = False
                    mblnIsholiday = False
                    mblnIsWeekend = False


                    'Pinkal (18-Jul-2017) -- Start
                    'Enhancement - TnA Enhancement for B5 Plus Company .
                    mblnIsvoid = 0
                    mintVoiduserunkid = -1
                    mstrVoidReason = ""
                    mdtVoiddatetime = Nothing
                    'Pinkal (18-Jul-2017) -- End



                    'Pinkal (06-May-2016) -- Start
                    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                    mstrVoidReason = ""
                    'Pinkal (06-May-2016) -- End


                    Dim mblnIsUpdate As Boolean = False

                    If IsEmployeeLoginExist(eZeeDate.convertDate(mdtLogindate), "", mintEmployeeunkid, mintLoginunkid, objDataOperation) Then
                        If mblnOverWriteIfExist = False Then Continue For
                        mblnIsUpdate = True
                    End If


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)

                    If blnDonotAttendanceinSeconds Then
                        objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(mdtcheckintime.ToShortDateString() & " " & mdtcheckintime.ToString("HH:mm")))
                        objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(dtOuttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString() & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))
                    Else
                        objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtcheckintime)
                        objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
                    End If

                    objDataOperation.AddParameter("@original_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
                    objDataOperation.AddParameter("@original_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))

                    objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
                    objDataOperation.AddParameter("@workhour", SqlDbType.Float, eZeeDataType.INT_SIZE, mintWorkhour)
                    objDataOperation.AddParameter("@breakhr", SqlDbType.Float, eZeeDataType.INT_SIZE, 0)
                    objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark)
                    objDataOperation.AddParameter("@inouttype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInOuType)
                    objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSourceType)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)



                    If mblnIsUpdate Then
                        objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginunkid)

                        strQ = "UPDATE tnalogin_tran SET " & _
                                  "  employeeunkid = @employeeunkid" & _
                                  ", logindate = @logindate" & _
                                  ", checkintime = @checkintime" & _
                                  ", checkouttime = @checkouttime" & _
                                  ", holdunkid = @holdunkid" & _
                                  ", workhour = @workhour" & _
                                  ", breakhr = @breakhr" & _
                                  ", remark = @remark" & _
                                  ", inouttype = @inouttype " & _
                                  ", sourcetype = @sourcetype " & _
                                  ", userunkid = @userunkid " & _
                                  ", isvoid = @isvoid " & _
                                  ", voiduserunkid = @voiduserunkid " & _
                                  ", voiddatetime = @voiddatetime " & _
                                  ", voidreason = @voidreason " & _
                                  " WHERE loginunkid = @loginunkid "

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Else
                        strQ = "INSERT INTO tnalogin_tran ( " & _
                                  "  employeeunkid " & _
                                  ", logindate " & _
                                  ", checkintime " & _
                                  ", checkouttime " & _
                                   ",original_intime " & _
                                  ",original_outtime " & _
                                  ", holdunkid " & _
                                  ", workhour " & _
                                  ", breakhr " & _
                                  ", remark " & _
                                  ", inouttype " & _
                                  ", sourcetype " & _
                                  ", userunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason " & _
                                ") VALUES (" & _
                                  "  @employeeunkid " & _
                                  ", @logindate " & _
                                  ", @checkintime " & _
                                  ", @checkouttime " & _
                                  ", @original_intime " & _
                                  ", @original_outtime " & _
                                  ", @holdunkid " & _
                                  ", @workhour " & _
                                  ", @breakhr " & _
                                  ", @remark " & _
                                  ", @inouttype " & _
                                  ", @sourcetype " & _
                                  ", @userunkid " & _
                                  ", @isvoid " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason " & _
                                "); SELECT @@identity"

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintLoginunkid = dsList.Tables(0).Rows(0).Item(0)
                    End If


                    'START FOR INSERT LOGIN SUMMERY

                    mintearly_coming = 0
                    mintearly_going = 0
                    mintlate_coming = 0
                    mintlate_going = 0


                    'Pinkal (18-Jul-2017) -- Start
                    'Enhancement - TnA Enhancement for B5 Plus Company .

                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                    '         , xUserUnkid, xYearUnkid _
                    '         , xCompanyUnkid, mdtFromdate _
                    '         , mdtTodate, xUserModeSetting _
                    '         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                    '         , blnPolicyManagementTNA _
                    '         , blnDonotAttendanceinSeconds _
                    '         , blnFirstCheckInLastCheckOut _
                    '         , blnApplyUserAccessFilter, intEmpId _
                    '         , strUserAccessLevelFilterString, mstrFilter) = False Then


                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                    '                             , xUserUnkid, xYearUnkid _
                    '                            , xCompanyUnkid, mdtLogindate _
                    '                            , mdtLogindate, xUserModeSetting _
                    '                             , xOnlyApproved, xIncludeIn_ActiveEmployee _
                    '                             , blnPolicyManagementTNA _
                    '                             , blnDonotAttendanceinSeconds _
                    '                             , blnFirstCheckInLastCheckOut _
                    '                             , blnApplyUserAccessFilter, intEmpId _
                    '                             , strUserAccessLevelFilterString, mstrFilter) = False Then

                    If InsertLoginSummary(objDataOperation, xDatabaseName _
                             , xUserUnkid, xYearUnkid _
                            , xCompanyUnkid, mdtLogindate _
                            , mdtLogindate, xUserModeSetting _
                             , xOnlyApproved, xIncludeIn_ActiveEmployee _
                             , blnPolicyManagementTNA _
                             , blnDonotAttendanceinSeconds _
                             , blnFirstCheckInLastCheckOut _
                                                     , blnIsHolidayConsiderOnWeekend _
                                                     , blnIsDayOffConsiderOnWeekend _
                                                     , blnIsHolidayConsiderOnDayoff _
                             , blnApplyUserAccessFilter, intEmpId _
                             , strUserAccessLevelFilterString, mstrFilter) = False Then

                        'Pinkal (11-AUG-2017) -- End

                        'Pinkal (18-Jul-2017) -- End

                        'S.SANDEEP [04 JUN 2015] -- END

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                    'END FOR INSERT LOGIN SUMMERY


                    If blnFirstCheckInLastCheckOut = False Then
                        If UpdateBreakTime(objDataOperation, mintEmployeeunkid, mdtLogindate) = False Then
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If

                    'AUDIT TRAIL IS USED FOR ONLY HOLD EMPLOYEE

                    If mblnIsUpdate Then
                        If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If

                    Else

                        If InsertAuditTrailLogin(objDataOperation, 1, blnDonotAttendanceinSeconds) = False Then
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If

                    End If

                Next

            Next


                'Pinkal (30-Jul-2015) -- Start
                'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                objconfig = Nothing
                'Pinkal (30-Jul-2015) -- End

            objDataOperation.ReleaseTransaction(True)


            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            If mstrMessage.Trim.Length > 0 Then
                Return False
            End If
            'Pinkal (13-Jan-2015) -- End

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GlobalAddTimesheetData", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function GlobalEditTimesheetData(ByVal mstrEmployeeId As String, ByVal dtFromDate As DateTime, ByVal dtToDate As DateTime _
                                            , ByVal mblnIncludeHoliday As Boolean, ByVal mblnIncludeWeekend As Boolean, ByVal mblnIncludeDayOff As Boolean _
                                            , ByVal dtIntime As DateTime, ByVal dtOuttime As DateTime _
                                            , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                            , ByVal xCompanyUnkid As Integer, ByVal xUserModeSetting As String _
                                            , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                            , ByVal blnPolicyManagementTNA As Boolean _
                                            , ByVal blnDonotAttendanceinSeconds As Boolean _
                                                                , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                                                , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                                                , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                                                , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                                                , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                , Optional ByVal intEmpId As Integer = -1 _
                                                                , Optional ByVal objDataOperation As clsDataOperation = Nothing _
                                                                , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                                                , Optional ByVal mstrFilter As String = "", Optional ByVal intShiftunkId As Integer = -1 _
                                                                , Optional ByVal intPolicyunkId As Integer = -1) As Boolean



        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End


        Dim blnFlag As Boolean = False
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim mstrReason As String = mstrVoidReason
            Dim intDays As Integer = DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date) + 1
            Dim arEmpId() As String = mstrEmployeeId.Trim.Split(",")

            Dim objEmpHoliday As New clsemployee_holiday
            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim objShiftTran As New clsshift_tran
            Dim objEmpPolicyTran As New clsemployee_policy_tran
            Dim objPolicyTran As New clsPolicy_tran

            Dim objEmployee As New clsEmployee_Master

            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            mstrMessage = ""
            'Pinkal (13-Jan-2015) -- End


                'Pinkal (30-Jul-2015) -- Start
                'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = mintCompanyId
                'Pinkal (30-Jul-2015) -- End

            For i As Integer = 0 To arEmpId.Length - 1

                mintEmployeeunkid = CInt(arEmpId(i))
                objEmployee._Employeeunkid(dtToDate) = mintEmployeeunkid


                For j As Integer = 0 To intDays - 1

                    'Pinkal (30-Jul-2015) -- Start
                    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                    If objConfig._AllowToChangeAttendanceAfterPayment = False Then
                        'Pinkal (13-Jan-2015) -- Start
                        'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
                        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                            Dim objPaymentTran As New clsPayment_tran
                            Dim objPeriod As New clsMasterData
                            Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtFromDate.AddDays(j).Date)

                            Dim mdtTnAStartdate As Date = Nothing
                            If intPeriodId > 0 Then
                                Dim objTnAPeriod As New clscommom_period_Tran
                                mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                            End If

                            If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintEmployeeunkid, mdtTnAStartdate, intPeriodId, dtFromDate.AddDays(j).Date) > 0 Then
                                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Payroll payment is already done for the checked employee(s) for the selected date tenure, due to this some employees will be skipped for that particular date(s) during this process.")
                                Continue For
                            End If
                        End If
                        'Pinkal (13-Jan-2015) -- End
                    End If
                    'Pinkal (30-Jul-2015) -- End


                    Dim dsTermination As New DataSet
                    Dim dsRetirement As New DataSet

                    Dim objDates As New clsemployee_dates_tran
                    dsTermination = objDates.Get_Current_Dates(dtToDate, enEmp_Dates_Transaction.DT_TERMINATION, mintEmployeeunkid)
                    dsRetirement = objDates.Get_Current_Dates(dtToDate, enEmp_Dates_Transaction.DT_RETIREMENT, mintEmployeeunkid)
                    objDates = Nothing

                    mdtLogindate = dtFromDate.AddDays(j)

                    Dim StartDate As Date = IIf(objEmployee._Appointeddate.Date > mdtLogindate, objEmployee._Appointeddate.Date, mdtLogindate)
                    Dim EndDate As Date = mdtLogindate

                    'S.SANDEEP [21-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#ARUTI-57}
                    'If dsTermination.Tables(0).Rows.Count > 0 AndAlso CDate(dsTermination.Tables(0).Rows(0)("date2")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date2")).Date < mdtLogindate, CDate(dsTermination.Tables(0).Rows(0)("date2")).Date, mdtLogindate.Date)
                    'End If

                    If dsTermination.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(dsTermination.Tables(0).Rows(0)("date2")) = False Then
                        EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date2")).Date < mdtLogindate, CDate(dsTermination.Tables(0).Rows(0)("date2")).Date, mdtLogindate.Date)
                    End If
                    End If
                    'S.SANDEEP [21-Mar-2018] -- END
                    

                    If dsRetirement.Tables(0).Rows.Count > 0 AndAlso CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                        EndDate = IIf(CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date, EndDate.Date)
                    End If

                    'S.SANDEEP [21-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#ARUTI-57}
                    'If dsTermination.Tables(0).Rows.Count > 0 AndAlso CDate(dsTermination.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsTermination.Tables(0).Rows(0)("date1")).Date, EndDate)
                    'End If
                    If dsTermination.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(dsTermination.Tables(0).Rows(0)("date1")) = False Then
                        EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsTermination.Tables(0).Rows(0)("date1")).Date, EndDate)
                    End If
                    End If
                    'S.SANDEEP [21-Mar-2018] -- END
                    

                    dsTermination.Dispose() : dsRetirement.Dispose()

                    If StartDate.Date > mdtLogindate OrElse EndDate < mdtLogindate Then
                        Continue For
                    End If



                    '========== START FOR EXCLUDE EMPLOYEE HOLIDAY==================================
                    Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, mdtLogindate)
                    If dtHoliday.Rows.Count > 0 AndAlso mblnIncludeHoliday = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE HOLIDAY====================================


                    '========== START FOR EXCLUDE EMPLOYEE WEEKEND==================================
                    Dim intShiftID As Integer = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtLogindate, mintEmployeeunkid)

                    If intShiftunkId > 0 AndAlso intShiftunkId <> intShiftID Then
                        Continue For

                    ElseIf intShiftID <= 0 Then
                        Continue For

                    End If

                    objShiftTran.GetShiftTran(intShiftID)
                    Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                    If drRow.Length > 0 AndAlso mblnIncludeWeekend = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE WEEKEND===================================


                    '========== START FOR EXCLUDE EMPLOYEE DAY OFF==================================

                    If objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation) AndAlso mblnIncludeDayOff = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE DAY OFF====================================


                    'Pinkal (09-Jun-2015) -- Start
                    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                    Dim intPolicyId As Integer = 0


                    If blnPolicyManagementTNA Then
                        If intPolicyunkId > 0 Then
                            intPolicyId = objEmpPolicyTran.GetPolicyTranID(mdtLogindate, mintEmployeeunkid)
                            If intPolicyunkId <> intPolicyId Then Continue For
                            If intPolicyId <= 0 Then Continue For
                        End If
                    End If


                    'Pinkal (09-Jun-2015) -- End

                    If dtIntime <> Nothing Then   ' --- START FOR CHECK IN TIME 

                        dsList = GetList(objDataOperation, "List", True)

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                            Dim mintMinLogunkiId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Min(loginunkid)", "")), 0, dsList.Tables(0).Compute("Min(loginunkid)", "")))

                            If mintMinLogunkiId > 0 Then

                                Dim drLoginRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMinLogunkiId)

                                If drLoginRow.Length > 0 Then

                                    Dim mdtIntime As DateTime = Nothing

                                    If ConfigParameter._Object._DonotAttendanceinSeconds Then

                                        'Pinkal (18-Jul-2017) -- Start
                                        'Enhancement - TnA Enhancement for B5 Plus Company .
                                        'mdtIntime = CDate(dtIntime.ToShortDateString & " " & dtIntime.ToString("HH:mm"))
                                        mdtIntime = CDate(dtIntime.AddDays(j).ToShortDateString & " " & dtIntime.ToString("HH:mm"))
                                        'Pinkal (18-Jul-2017) -- End
                                    Else
                                        mdtIntime = dtIntime.AddDays(j)
                                    End If

                                    strQ = " Update tnalogin_tran set checkintime = @checkintime,workhour = @workhour,operationtype = @operationtype,sourcetype= @sourcetype WHERE  loginunkid = @loginunkid AND isvoid = 0 "
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLogunkiId)
                                    objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.Edit)
                                    objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIntime)
                                    objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, enInOutSource.GlobalEdit)

                                    If CDate(drLoginRow(0)("checkouttime")) <> Nothing Then
                                        If mdtIntime <= CDate(drLoginRow(0)("checkouttime")) Then
                                            objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, mdtIntime, CDate(drLoginRow(0)("checkouttime"))))
                                        Else
                                            objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drLoginRow(0)("checkouttime")), mdtIntime))
                                        End If
                                    Else
                                        objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                    End If

                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    _Loginunkid = mintMinLogunkiId
                                    _OperationType = enTnAOperation.Edit
                                    _Shiftunkid = intShiftID
                                    _PolicyId = intPolicyId


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    intEmpId = mintEmployeeunkid
                                    'Pinkal (11-AUG-2017) -- End

                                    _Early_Coming = 0
                                    _Early_Going = 0
                                    _Late_Coming = 0
                                    _Late_Going = 0


                                    'Pinkal (18-Jul-2017) -- Start
                                    'Enhancement - TnA Enhancement for B5 Plus Company .

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                     , xUserUnkid, xYearUnkid _
                                    '                     , xCompanyUnkid, mdtFromdate _
                                    '                     , mdtTodate, xUserModeSetting _
                                    '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                     , blnPolicyManagementTNA _
                                    '                     , blnDonotAttendanceinSeconds _
                                    '                     , blnFirstCheckInLastCheckOut _
                                    '                     , blnApplyUserAccessFilter, intEmpId _
                                    '                     , strUserAccessLevelFilterString, mstrFilter) = False Then


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                     , xUserUnkid, xYearUnkid _
                                    '                    , xCompanyUnkid, mdtLogindate _
                                    '                    , mdtLogindate, xUserModeSetting _
                                    '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                     , blnPolicyManagementTNA _
                                    '                     , blnDonotAttendanceinSeconds _
                                    '                     , blnFirstCheckInLastCheckOut _
                                    '                     , blnApplyUserAccessFilter, intEmpId _
                                    '                     , strUserAccessLevelFilterString, mstrFilter) = False Then

                                    If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                         , xUserUnkid, xYearUnkid _
                                                        , xCompanyUnkid, mdtLogindate _
                                                        , mdtLogindate, xUserModeSetting _
                                                         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                         , blnPolicyManagementTNA _
                                                         , blnDonotAttendanceinSeconds _
                                                         , blnFirstCheckInLastCheckOut _
                                                                    , blnIsHolidayConsiderOnWeekend _
                                                                    , blnIsDayOffConsiderOnWeekend _
                                                                    , blnIsHolidayConsiderOnDayoff _
                                                         , blnApplyUserAccessFilter, intEmpId _
                                                         , strUserAccessLevelFilterString, mstrFilter) = False Then

                                        'Pinkal (11-AUG-2017) -- End


                                        'Pinkal (18-Jul-2017) -- End

                                        'If InsertLoginSummary(objDataOperation) = False Then
                                        'S.SANDEEP [04 JUN 2015] -- END
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                        'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                        'S.SANDEEP [04 JUN 2015] -- END

                                        'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                        'S.SANDEEP [04 JUN 2015] -- END
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                            End If

                        End If

                    End If  ' --- END FOR CHECK IN TIME

                    If dtOuttime <> Nothing Then   ' --- START FOR CHECK OUT TIME

                        dsList = GetList(objDataOperation, "List", True)

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                            Dim mintMaxLogunkiId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Max(loginunkid)", "")), 0, dsList.Tables(0).Compute("Max(loginunkid)", "")))

                            If mintMaxLogunkiId > 0 Then

                                Dim drLoginRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMaxLogunkiId)

                                If drLoginRow.Length > 0 Then

                                    Dim mdtOuttime As DateTime = Nothing

                                    If ConfigParameter._Object._DonotAttendanceinSeconds Then

                                        'Pinkal (18-Jul-2017) -- Start
                                        'Enhancement - TnA Enhancement for B5 Plus Company .
                                        'mdtOuttime = CDate(dtOuttime.ToShortDateString() & " " & dtOuttime.ToString("HH:mm"))
                                        mdtOuttime = CDate(dtOuttime.AddDays(j).ToShortDateString() & " " & dtOuttime.ToString("HH:mm"))
                                        'Pinkal (18-Jul-2017) -- End
                                    Else
                                        mdtOuttime = dtOuttime.AddDays(j)
                                    End If

                                    strQ = " Update tnalogin_tran set checkouttime = @checkouttime,workhour = @workhour,operationtype = @operationtype,sourcetype= @sourcetype WHERE  loginunkid = @loginunkid AND isvoid = 0 "
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLogunkiId)
                                    objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.Edit)
                                    objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtOuttime)
                                    objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, enInOutSource.GlobalEdit)

                                    If CDate(drLoginRow(0)("checkintime")) <> Nothing Then
                                        If mdtOuttime >= CDate(drLoginRow(0)("checkintime")) Then
                                            objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drLoginRow(0)("checkintime")), mdtOuttime))
                                        Else
                                            objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, mdtOuttime, CDate(drLoginRow(0)("checkintime"))))
                                        End If
                                    Else
                                        objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                    End If

                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    _Loginunkid = mintMaxLogunkiId
                                    _OperationType = enTnAOperation.Edit
                                    _Shiftunkid = intShiftID
                                    _PolicyId = intPolicyId


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    intEmpId = mintEmployeeunkid
                                    'Pinkal (11-AUG-2017) -- End


                                    _Early_Coming = 0
                                    _Early_Going = 0
                                    _Late_Coming = 0
                                    _Late_Going = 0

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'If InsertLoginSummary(objDataOperation) = False Then


                                    'Pinkal (18-Jul-2017) -- Start
                                    'Enhancement - TnA Enhancement for B5 Plus Company .

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                     , xUserUnkid, xYearUnkid _
                                    '                     , xCompanyUnkid, mdtFromdate _
                                    '                     , mdtTodate, xUserModeSetting _
                                    '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                     , blnPolicyManagementTNA _
                                    '                     , blnDonotAttendanceinSeconds _
                                    '                     , blnFirstCheckInLastCheckOut _
                                    '                     , blnApplyUserAccessFilter, intEmpId _
                                    '                     , strUserAccessLevelFilterString, mstrFilter) = False Then


                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                    'If InsertLoginSummary(objDataOperation, xDatabaseName _
                                    '                     , xUserUnkid, xYearUnkid _
                                    '                     , xCompanyUnkid, mdtLogindate _
                                    '                     , mdtLogindate, xUserModeSetting _
                                    '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                    '                     , blnPolicyManagementTNA _
                                    '                     , blnDonotAttendanceinSeconds _
                                    '                     , blnFirstCheckInLastCheckOut _
                                    '                     , blnApplyUserAccessFilter, intEmpId _
                                    '                     , strUserAccessLevelFilterString, mstrFilter) = False Then

                                    If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                         , xUserUnkid, xYearUnkid _
                                                         , xCompanyUnkid, mdtLogindate _
                                                         , mdtLogindate, xUserModeSetting _
                                                         , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                         , blnPolicyManagementTNA _
                                                         , blnDonotAttendanceinSeconds _
                                                         , blnFirstCheckInLastCheckOut _
                                                                    , blnIsHolidayConsiderOnWeekend _
                                                                    , blnIsDayOffConsiderOnWeekend _
                                                                    , blnIsHolidayConsiderOnDayoff _
                                                         , blnApplyUserAccessFilter, intEmpId _
                                                         , strUserAccessLevelFilterString, mstrFilter) = False Then

                                        'Pinkal (11-AUG-2017) -- End



                                        'Pinkal (18-Jul-2017) -- End

                                        'S.SANDEEP [04 JUN 2015] -- END
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                        'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                        'S.SANDEEP [04 JUN 2015] -- END
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                            End If

                        End If

                    End If  ' --- END FOR CHECK OUT TIME

                Next

            Next

                'Pinkal (30-Jul-2015) -- Start
                'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                objConfig = Nothing
                'Pinkal (30-Jul-2015) -- End


            objDataOperation.ReleaseTransaction(True)


            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            If mstrMessage.Trim.Length > 0 Then
                Return False
            End If
            'Pinkal (13-Jan-2015) -- End

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GlobalEditTimesheetData", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function GlobalVoidtTimesheetData(ByVal mstrEmployeeId As String, ByVal dtFromDate As DateTime, ByVal dtToDate As DateTime _
                                             , ByVal mblnIncludeHoliday As Boolean, ByVal mblnIncludeWeekend As Boolean, ByVal mblnIncludeDayOff As Boolean _
                                             , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                             , ByVal xCompanyUnkid As Integer, ByVal xUserModeSetting As String _
                                             , ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                             , ByVal blnPolicyManagementTNA As Boolean _
                                             , ByVal blnDonotAttendanceinSeconds As Boolean _
                                                                 , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                                                 , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                                                 , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                                                 , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                                                 , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                 , Optional ByVal intEmpId As Integer = -1 _
                                                                 , Optional ByVal objDataOperation As clsDataOperation = Nothing _
                                                                 , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                                                 , Optional ByVal mstrFilter As String = "", Optional ByVal intShiftunkId As Integer = -1 _
                                                                 , Optional ByVal intPolicyunkId As Integer = -1) As Boolean

        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        Dim blnFlag As Boolean = False
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim mstrReason As String = mstrVoidReason
            Dim intDays As Integer = DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date) + 1
            Dim arEmpId() As String = mstrEmployeeId.Trim.Split(",")

            Dim objEmpHoliday As New clsemployee_holiday
            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim objShiftTran As New clsshift_tran
            Dim objEmpPolicyTran As New clsemployee_policy_tran
            Dim objPolicyTran As New clsPolicy_tran

            'Pinkal (13-Jan-2014) -- Start
            'Enhancement : Oman Changes

            Dim objEmployee As New clsEmployee_Master

            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            mstrMessage = ""
            'Pinkal (13-Jan-2015) -- End


                'Pinkal (30-Jul-2015) -- Start
                'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = mintCompanyId
                'Pinkal (30-Jul-2015) -- End


            For i As Integer = 0 To arEmpId.Length - 1

                mintEmployeeunkid = CInt(arEmpId(i))

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = mintEmployeeunkid
                objEmployee._Employeeunkid(dtToDate) = mintEmployeeunkid
                'S.SANDEEP [04 JUN 2015] -- END


                For j As Integer = 0 To intDays - 1

                    'Pinkal (30-Jul-2015) -- Start
                    'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                    If objConfig._AllowToChangeAttendanceAfterPayment = False Then
                        'Pinkal (13-Jan-2015) -- Start
                        'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
                        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                            Dim objPaymentTran As New clsPayment_tran
                            Dim objPeriod As New clsMasterData
                            Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtFromDate.AddDays(j).Date)

                            Dim mdtTnAStartdate As Date = Nothing
                            If intPeriodId > 0 Then
                                Dim objTnAPeriod As New clscommom_period_Tran
                                mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                            End If

                            If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintEmployeeunkid, mdtTnAStartdate, intPeriodId, dtFromDate.AddDays(j).Date) > 0 Then
                                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Payroll payment is already done for the checked employee(s) for the selected date tenure, due to this some employees will be skipped for that particular date(s) during this process.")
                                Continue For
                            End If
                        End If
                        'Pinkal (13-Jan-2015) -- End
                    End If
                    'Pinkal (30-Jul-2015) -- End


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'mdtLogindate = dtFromDate.AddDays(j)

                    'Dim StartDate As Date = IIf(objEmployee._Appointeddate.Date > mdtLogindate, objEmployee._Appointeddate.Date, mdtLogindate)
                    'Dim EndDate As Date = mdtLogindate

                    'If objEmployee._Termination_From_Date.Date <> Nothing Then
                    '    EndDate = IIf(objEmployee._Termination_From_Date < mdtLogindate, objEmployee._Termination_From_Date.Date, mdtLogindate.Date)
                    'End If
                    'If objEmployee._Termination_To_Date.Date <> Nothing Then
                    '    EndDate = IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate.Date)
                    'End If
                    'If objEmployee._Empl_Enddate.Date <> Nothing Then
                    '    EndDate = IIf(objEmployee._Empl_Enddate.Date < EndDate, objEmployee._Empl_Enddate.Date, EndDate)
                    'End If

                    'If StartDate.Date > mdtLogindate OrElse EndDate < mdtLogindate Then
                    '    Continue For
                    'End If

                    Dim dsTermination As New DataSet
                    Dim dsRetirement As New DataSet

                    Dim objDates As New clsemployee_dates_tran
                    dsTermination = objDates.Get_Current_Dates(dtToDate, enEmp_Dates_Transaction.DT_TERMINATION, mintEmployeeunkid)
                    dsRetirement = objDates.Get_Current_Dates(dtToDate, enEmp_Dates_Transaction.DT_RETIREMENT, mintEmployeeunkid)
                    objDates = Nothing

                    mdtLogindate = dtFromDate.AddDays(j)

                    Dim StartDate As Date = IIf(objEmployee._Appointeddate.Date > mdtLogindate, objEmployee._Appointeddate.Date, mdtLogindate)
                    Dim EndDate As Date = mdtLogindate

                    'S.SANDEEP [21-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#ARUTI-57}
                    'If dsTermination.Tables(0).Rows.Count > 0 AndAlso CDate(dsTermination.Tables(0).Rows(0)("date2")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date2")).Date < mdtLogindate, CDate(dsTermination.Tables(0).Rows(0)("date2")).Date, mdtLogindate.Date)
                    'End If

                    If dsTermination.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(dsTermination.Tables(0).Rows(0)("date2")) = False Then
                        EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date2")).Date < mdtLogindate, CDate(dsTermination.Tables(0).Rows(0)("date2")).Date, mdtLogindate.Date)
                    End If
                    End If
                    'S.SANDEEP [21-Mar-2018] -- END
                    

                    If dsRetirement.Tables(0).Rows.Count > 0 AndAlso CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                        EndDate = IIf(CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsRetirement.Tables(0).Rows(0)("date1")).Date, EndDate.Date)
                    End If

                    'S.SANDEEP [21-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#ARUTI-57}
                    'If dsTermination.Tables(0).Rows.Count > 0 AndAlso CDate(dsTermination.Tables(0).Rows(0)("date1")).Date <> Nothing Then
                    '    EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsTermination.Tables(0).Rows(0)("date1")).Date, EndDate)
                    'End If

                    If dsTermination.Tables(0).Rows.Count > 0 Then
                        If IsDBNull(dsTermination.Tables(0).Rows(0)("date1")) = False Then
                        EndDate = IIf(CDate(dsTermination.Tables(0).Rows(0)("date1")).Date < EndDate, CDate(dsTermination.Tables(0).Rows(0)("date1")).Date, EndDate)
                    End If
                    End If
                    'S.SANDEEP [21-Mar-2018] -- END
                    

                    dsTermination.Dispose() : dsRetirement.Dispose()

                    If StartDate.Date > mdtLogindate OrElse EndDate < mdtLogindate Then
                        Continue For
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Pinkal (13-Jan-2014) -- End


                    '========== START FOR EXCLUDE EMPLOYEE HOLIDAY==================================
                    Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, mdtLogindate)
                    If dtHoliday.Rows.Count > 0 AndAlso mblnIncludeHoliday = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE HOLIDAY====================================


                    '========== START FOR EXCLUDE EMPLOYEE WEEKEND==================================
                    Dim intShiftID As Integer = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtLogindate, mintEmployeeunkid)

                    If intShiftunkId > 0 AndAlso intShiftunkId <> intShiftID Then
                        Continue For

                        'Pinkal (13-Jan-2014) -- Start
                        'Enhancement : Oman Changes

                    ElseIf intShiftID <= 0 Then
                        Continue For

                        'Pinkal (13-Jan-2014) -- End

                    End If

                    objShiftTran.GetShiftTran(intShiftID)
                    Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                    If drRow.Length > 0 AndAlso mblnIncludeWeekend = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE WEEKEND===================================


                    '========== START FOR EXCLUDE EMPLOYEE DAY OFF==================================

                    If objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation) AndAlso mblnIncludeDayOff = False Then
                        Continue For
                    End If
                    '========== END FOR EXCLUDE EMPLOYEE DAY OFF====================================


                    'Pinkal (09-Jun-2015) -- Start
                    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If ConfigParameter._Object._PolicyManagementTNA Then
                    '    If intPolicyunkId > 0 Then
                    '        Dim intPolicyId As Integer = objEmpPolicyTran.GetPolicyTranID(mdtLogindate, mintEmployeeunkid)
                    '        If intPolicyunkId <> intPolicyId Then Continue For
                    '        If intPolicyId <= 0 Then Continue For
                    '    End If
                    'End If
                    If blnPolicyManagementTNA Then
                        If intPolicyunkId > 0 Then
                            Dim intPolicyId As Integer = objEmpPolicyTran.GetPolicyTranID(mdtLogindate, mintEmployeeunkid)
                            If intPolicyunkId <> intPolicyId Then Continue For
                            If intPolicyId <= 0 Then Continue For
                        End If
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Pinkal (09-Jun-2015) -- End

                    dsList = GetList(objDataOperation, "List", True)

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                            _Loginunkid = CInt(dsList.Tables(0).Rows(k)("loginunkid"))

                            strQ = " Update tnalogin_tran set isvoid = 1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid,sourcetype= @sourcetype " & _
                                      " WHERE  loginunkid = @loginunkid "

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrReason)
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginunkid)
                            objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, enInOutSource.GlobalDelete)
                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            _Loginunkid = CInt(dsList.Tables(0).Rows(k)("loginunkid"))

                            'START FOR INSERT LOGIN SUMMERY

                            strQ = "Select loginsummaryunkid from tnalogin_summary where CONVERT(Char(8),login_date,112) = @logindate and employeeunkid = @employeeunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtLogindate))
                            Dim dsLoginSummaryID As DataSet = objDataOperation.ExecQuery(strQ, "GetLoginSummaryID")
                            If Not dsLoginSummaryID Is Nothing And dsLoginSummaryID.Tables(0).Rows.Count > 0 Then
                                mintLoginsummaryunkid = CInt(dsLoginSummaryID.Tables(0).Rows(0)("loginsummaryunkid"))
                            End If


                            'Pinkal (11-AUG-2017) -- Start
                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                            'If InsertLoginSummary(objDataOperation, xDatabaseName _
                            '                     , xUserUnkid, xYearUnkid _
                            '                     , xCompanyUnkid, mdtFromdate _
                            '                     , mdtTodate, xUserModeSetting _
                            '                     , xOnlyApproved, xIncludeIn_ActiveEmployee _
                            '                     , blnPolicyManagementTNA _
                            '                     , blnDonotAttendanceinSeconds _
                            '                     , blnFirstCheckInLastCheckOut _
                            '                     , blnApplyUserAccessFilter, intEmpId _
                            '                     , strUserAccessLevelFilterString, mstrFilter, mintLoginsummaryunkid) = False Then

                            If InsertLoginSummary(objDataOperation, xDatabaseName _
                                                 , xUserUnkid, xYearUnkid _
                                                 , xCompanyUnkid, mdtFromdate _
                                                 , mdtTodate, xUserModeSetting _
                                                 , xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                 , blnPolicyManagementTNA _
                                                 , blnDonotAttendanceinSeconds _
                                                 , blnFirstCheckInLastCheckOut _
                                                             , blnIsHolidayConsiderOnWeekend _
                                                             , blnIsDayOffConsiderOnWeekend _
                                                             , blnIsHolidayConsiderOnDayoff _
                                                 , blnApplyUserAccessFilter, intEmpId _
                                                 , strUserAccessLevelFilterString, mstrFilter, mintLoginsummaryunkid) = False Then

                                'Pinkal (11-AUG-2017) -- End


                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'END FOR INSERT LOGIN SUMMERY

                            'START FOR INSERT AUDIT TRAIL LOGIN

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            If InsertAuditTrailLogin(objDataOperation, 3, blnDonotAttendanceinSeconds) = False Then
                                'If InsertAuditTrailLogin(objDataOperation, 3) = False Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            'END FOR INSERT AUDIT TRAIL LOGIN

                        Next


                        'Pinkal (06-Jul-2018) -- Start
                        'Enhancement - Solving Global Delete Issue in Employee Timesheet.

                    Else

                        strQ = "DELETE FROM tnalogin_summary WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),login_date,112) = @logindate"

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLogindate.Date))
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Pinkal (12-Jun-2018) -- End

                    End If

                Next
            Next

                'Pinkal (30-Jul-2015) -- Start
                'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
                objConfig = Nothing
                'Pinkal (30-Jul-2015) -- End


            objDataOperation.ReleaseTransaction(True)


            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            If mstrMessage.Trim.Length > 0 Then
                Return False
            End If
            'Pinkal (13-Jan-2015) -- End

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GlobalVoidtTimesheetData", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function GetEmployeeMaxLoginDate(ByVal mdtTodayDate As DateTime, ByVal intEmployeeId As Integer) As DataTable
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            strQ = "SELECT  loginunkid " & _
                      ", logindate " & _
                      " FROM    ( SELECT    loginunkid " & _
                                       ", logindate " & _
                                       ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY loginunkid DESC ) AS ROWNO " & _
                                       " FROM  tnalogin_tran " & _
                                       "WHERE  employeeunkid = @employeeunkid " & _
                                                "AND isvoid = 0 " & _
                                                "AND logindate = ( SELECT MAX(logindate) AS 'MaxLoginDate' " & _
                                                                            " FROM  tnalogin_tran " & _
                                                                            " WHERE employeeunkid = @employeeunkid " & _
                                                                            " AND ( original_intime < @logintime " & _
                                                                            " OR original_outtime < @logintime " & _
                                                                            " ) " & _
                                                                            "AND isvoid = 0 " & _
                                                                         ") " & _
                                 ") AS a " & _
                    " WHERE a.ROWNO = 1 "
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@logintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodayDate)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "MaxDate")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeMaxLoginDate", mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function ManualRoundOff(ByVal mdtStartDate As DateTime, ByVal mdtEndDate As DateTime _
                                                   , ByVal mdtEmployee As DataTable, ByVal mdtRange As DataTable _
                                                   , ByVal iMinutes As Integer, ByVal iLimit As Integer _
                                                   , ByVal eManRType As enManualRoundOffType, ByVal xDatabaseName As String _
                                                   , ByVal xUserUnkid As Integer _
                                                   , ByVal xYearUnkid As Integer _
                                                   , ByVal xCompanyUnkid As Integer _
                                                   , ByVal xUserModeSetting As String _
                                                   , ByVal xOnlyApproved As Boolean _
                                                   , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                   , ByVal blnPolicyManagementTNA As Boolean _
                                                   , ByVal blnDonotAttendanceinSeconds As Boolean _
                                                   , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                                   , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                                   , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                                   , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                                   , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                   , Optional ByVal intEmpId As Integer = -1 _
                                                   , Optional ByVal objDataOperation As clsDataOperation = Nothing _
                                                   , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                                   , Optional ByVal mstrFilter As String = "") As Boolean


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal blnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal blnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal blnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End

        'Public Function ManualRoundOff(ByVal mdtStartDate As DateTime, _
        '                           ByVal mdtEndDate As DateTime, _
        '                           ByVal mdtEmployee As DataTable, _
        '                           ByVal mdtRange As DataTable, _
        '                           ByVal iMinutes As Integer, _
        '                           ByVal iLimit As Integer, _
        '                           ByVal eManRType As enManualRoundOffType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            Dim mintDays As Integer = DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date.AddDays(1))
            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim objPolicyTran As New clsPolicy_tran
            Dim objEmpPolicyTran As New clsemployee_policy_tran
            Dim objShiftTran As New clsshift_tran
            Dim objholiday As New clsemployee_holiday
            Dim objEmpDayOff As New clsemployee_dayoff_Tran

            For i As Integer = 0 To mdtEmployee.Rows.Count - 1
                mintEmployeeunkid = CInt(mdtEmployee.Rows(i)("employeeunkid"))


                For j As Integer = 0 To mintDays - 1
                    mdtLogindate = mdtStartDate.AddDays(j).Date
                    dsList = GetList(objDataOperation, "List", True)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            Dim mdtShiftInTime As DateTime = Nothing
                            Dim mdtShiftOutTime As DateTime = Nothing
                            Dim mintltcome_graceinsec As Integer = 0
                            Dim mintelrcome_graceinsec As Integer = 0
                            Dim mintltgoing_graceinsec As Integer = 0
                            Dim mintelrgoing_graceinsec As Integer = 0

                            strQ = " SELECT lvleaveIssue_tran.leaveissuetranunkid,ISNULL(lvleaveIssue_tran.ispaid,0) AS ispaid " & _
                                      " FROM lvleaveIssue_master " & _
                                      " JOIN lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
                                      " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                                      " WHERE lvleaveIssue_master.employeeunkid = @employeeunkid AND convert(char(8),lvleaveIssue_tran.leavedate,112) = @leavedate and lvleaveIssue_master.isvoid = 0"

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
                            Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then
                                If CBool(dList.Tables(0).Rows(0)("ispaid")) = True Then
                                    mblnIspaidLeave = True
                                    mblnIsUnpaidLeave = False
                                ElseIf CBool(dList.Tables(0).Rows(0)("ispaid")) = False Then
                                    mblnIsUnpaidLeave = True
                                    mblnIspaidLeave = False
                                End If
                            Else
                                mblnIsUnpaidLeave = False
                                mblnIspaidLeave = False
                            End If

                            _Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(mdtLogindate, mintEmployeeunkid)
                            objShiftTran.GetShiftTran(_Shiftunkid)
                            Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")

                            If drWeekend.Length > 0 Then
                                mblnIsWeekend = True
                            Else
                                mblnIsWeekend = False
                            End If

                            'Pinkal (22-AUG-2017) -- Start
                            'Enhancement - Worked Performance Issue in Auto Round Off.

                            'Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                            '                                                 , xCompanyUnkid, mdtStartDate.Date, mdtEndDate.Date _
                            '                                                 , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter _
                            '                                                 , intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)
                            'Dim drHoliday() As DataRow = dsData.Tables("List").Select("employeeunkid = " & mintEmployeeunkid & " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'")

                            'For Each dr As DataRow In drHoliday
                            '    If eZeeDate.convertDate(dr("holidaydate")).Date = mdtLogindate.Date Then
                            '        mblnIsholiday = True
                            '    Else
                            '        mblnIsholiday = False
                            '    End If
                            'Next

                            intEmpId = mintEmployeeunkid

                            mstrFilter = " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'"

                            Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                                                             , xCompanyUnkid, mdtStartDate.Date, mdtEndDate.Date _
                                                                             , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter _
                                                                             , intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)

                            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                                If eZeeDate.convertDate(dsData.Tables(0).Rows(0)("holidaydate")).Date = mdtLogindate.Date Then
                                    mblnIsholiday = True
                                Else
                                    mblnIsholiday = False
                                End If
                            End If
                            'Pinkal (22-AUG-2017) -- End

                            mblnIsDayOffshift = objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation)

                            mintearly_coming = 0
                            mintlate_coming = 0
                            mintearly_going = 0
                            mintlate_going = 0

                            _PolicyId = objEmpPolicyTran.GetPolicyTranID(mdtLogindate.Date, mintEmployeeunkid)


                            '---------------------------------- START FOR ROUNDING OFF ---------------------------------------
                            Select Case eManRType
                                Case enManualRoundOffType.ROUND_IN_TIME
                                    Dim mintMinLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Min(loginunkid)", "")), 0, dsList.Tables(0).Compute("Min(loginunkid)", "")))

                                    If mintMinLoginId > 0 Then     ' START FOR mintMinLoginId
                                        Dim drRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMinLoginId)
                                        If drRow.Length > 0 Then
                                            _Roundoff_Intime = Nothing
                                            _Roundoff_Outtime = Nothing
                                            _checkintime = Nothing
                                            _Checkouttime = Nothing
                                            _Totalhr = 0
                                            _TotalOvertimehr = 0
                                            _Ot2 = 0
                                            _Ot3 = 0
                                            _Ot4 = 0
                                            _TotalNighthr = 0
                                            _Shorthr = 0
                                            _Early_Coming = 0
                                            _Early_Going = 0
                                            _Late_Coming = 0
                                            _Late_Going = 0
                                            _IsDayOffShift = False
                                            _IsWeekend = False
                                            _IsHoliday = False
                                            _Isgraceroundoff = False
                                            _Manualroundofftypeid = eManRType
                                            _Roundminutes = iMinutes
                                            _Roundlimit = iLimit

                                            If IsDBNull(drRow(0)("checkintime")) OrElse IsDBNull(drRow(0)("checkouttime")) Then Continue For

                                            _checkintime = CDate(drRow(0)("checkintime"))
                                            _Checkouttime = CDate(drRow(0)("checkouttime"))

                                            Dim dtTime As DateTime = _checkintime
                                            If mdtRange IsNot Nothing AndAlso mdtRange.Rows.Count > 0 Then
                                                Dim lBound, uBound, mBound As Integer
                                                lBound = 0 : uBound = 0 : mBound = 0
                                                mBound = iLimit
                                                Dim tRange() As DataRow = mdtRange.Select(dtTime.Minute & " >= lbound AND " & dtTime.Minute & " <= ubound")
                                                If tRange.Length > 0 Then
                                                    lBound = CInt(tRange(0).Item("lbound"))
                                                    uBound = CInt(tRange(0).Item("ubound"))
                                                    If lBound > 0 Then mBound = mBound + lBound - 1
                                                    If dtTime.Minute > mBound Then
                                                        While dtTime.Minute < uBound
                                                            dtTime = dtTime.AddMinutes(1)
                                                        End While
                                                        If dtTime.Minute = 59 Then
                                                            dtTime = dtTime.AddMinutes(1)
                                                        End If
                                                    ElseIf dtTime.Minute <= mBound Then
                                                        If dtTime.Minute <> lBound Then
                                                            While dtTime.Minute >= lBound AndAlso dtTime.Minute > 0
                                                                dtTime = dtTime.AddMinutes(-1)
                                                            End While
                                                        End If
                                                    End If
                                                    _Roundoff_Intime = dtTime
                                                End If
                                            End If
                                            If _Roundoff_Intime <> Nothing Then   'START FOR _Roundoff_Intime
                                                strQ = " UPDATE tnalogin_tran SET " & _
                                                       "  roundoff_outtime = @roundoff_outtime " & _
                                                       " ,workhour = @workhour " & _
                                                       " ,operationtype = @operationtype " & _
                                                       " ,isgraceroundoff = @isgraceroundoff " & _
                                                       " ,manualroundofftypeid = @manualroundofftypeid " & _
                                                       " ,roundminutes = @roundminutes " & _
                                                       " ,roundlimit = @roundlimit " & _
                                                       " ,roundoff_intime = @roundoff_intime " & _
                                                       "WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                                objDataOperation.ClearParameters()
                                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Roundoff_Intime)
                                                'S.SANDEEP [ 20 JAN 2014 ] -- START
                                                If Not IsDBNull(drRow(0)("roundoff_outtime")) Then
                                                    objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, drRow(0)("roundoff_outtime"))
                                                Else
                                                    objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Checkouttime)
                                                End If
                                                'S.SANDEEP [ 20 JAN 2014 ] -- END
                                                objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, _Roundoff_Intime, _Checkouttime))
                                                objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLoginId)
                                                objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                                objDataOperation.AddParameter("@manualroundofftypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eManRType)
                                                objDataOperation.AddParameter("@roundminutes", SqlDbType.Int, eZeeDataType.INT_SIZE, iMinutes)
                                                objDataOperation.AddParameter("@roundlimit", SqlDbType.Int, eZeeDataType.INT_SIZE, iLimit)

                                                objDataOperation.ExecNonQuery(strQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                _Loginunkid = mintMinLoginId
                                                _OperationType = enTnAOperation.RoundOff
                                                _Isgraceroundoff = False
                                                _Manualroundofftypeid = eManRType
                                                _Roundminutes = iMinutes
                                                _Roundlimit = iLimit
                                                _Roundoff_Outtime = _Checkouttime


                                                'Pinkal (11-AUG-2017) -- Start
                                                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                'If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                '                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                '                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                '                    , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                                                '                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                                If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                                                 , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds _
                                                                                 , blnFirstCheckInLastCheckOut _
                                                                                 , blnIsHolidayConsiderOnWeekend _
                                                                                 , blnIsDayOffConsiderOnWeekend _
                                                                                 , blnIsHolidayConsiderOnDayoff _
                                                                                 , blnApplyUserAccessFilter, intEmpId _
                                                                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                                    'Pinkal (11-AUG-2017) -- End

                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If
                                        End If
                                    End If


                                Case enManualRoundOffType.ROUND_OUT_TIME
                                    _Roundoff_Intime = Nothing
                                    _Roundoff_Outtime = Nothing
                                    dsList = GetList(objDataOperation, "List", True)

                                    Dim mintMinLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Min(loginunkid)", "")), 0, dsList.Tables(0).Compute("Min(loginunkid)", "")))
                                    Dim mintMaxLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Max(loginunkid)", "")), 0, dsList.Tables(0).Compute("Max(loginunkid)", "")))

                                    If mintMaxLoginId > 0 Then  ' START FOR mintMaxLoginId
                                        Dim drRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMaxLoginId)
                                        _Roundoff_Intime = Nothing
                                        _Roundoff_Outtime = Nothing
                                        _checkintime = Nothing
                                        _Checkouttime = Nothing
                                        _Totalhr = 0
                                        _TotalOvertimehr = 0
                                        _Ot2 = 0
                                        _Ot3 = 0
                                        _Ot4 = 0
                                        _TotalNighthr = 0
                                        _Shorthr = 0
                                        _Early_Coming = 0
                                        _Early_Going = 0
                                        _Late_Coming = 0
                                        _Late_Going = 0
                                        _IsDayOffShift = False
                                        _IsWeekend = False
                                        _IsHoliday = False
                                        _Isgraceroundoff = False
                                        _Manualroundofftypeid = eManRType
                                        _Roundminutes = iMinutes
                                        _Roundlimit = iLimit

                                        If IsDBNull(drRow(0)("checkintime")) OrElse IsDBNull(drRow(0)("checkouttime")) Then Continue For

                                        _checkintime = CDate(drRow(0)("checkintime"))
                                        _Checkouttime = CDate(drRow(0)("checkouttime"))

                                        Dim dtTime As DateTime = _Checkouttime
                                        If mdtRange IsNot Nothing AndAlso mdtRange.Rows.Count > 0 Then
                                            Dim lBound, uBound, mBound As Integer
                                            lBound = 0 : uBound = 0 : mBound = 0
                                            mBound = iLimit
                                            Dim tRange() As DataRow = mdtRange.Select(dtTime.Minute & " >= lbound AND " & dtTime.Minute & " <= ubound")
                                            If tRange.Length > 0 Then
                                                lBound = CInt(tRange(0).Item("lbound"))
                                                uBound = CInt(tRange(0).Item("ubound"))
                                                If lBound > 0 Then mBound = mBound + lBound - 1
                                                If dtTime.Minute > mBound Then
                                                    While dtTime.Minute < uBound
                                                        dtTime = dtTime.AddMinutes(1)
                                                    End While
                                                    If dtTime.Minute = 59 Then
                                                        dtTime = dtTime.AddMinutes(1)
                                                    End If
                                                ElseIf dtTime.Minute <= mBound Then
                                                    If dtTime.Minute <> lBound Then
                                                        While dtTime.Minute >= lBound AndAlso dtTime.Minute > 0
                                                            dtTime = dtTime.AddMinutes(-1)
                                                        End While
                                                    End If
                                                End If
                                                _Roundoff_Outtime = dtTime
                                            End If
                                        End If

                                        If _Roundoff_Outtime <> Nothing Then   ' START FOR _Roundoff_Outtime

                                            strQ = " UPDATE tnalogin_tran SET " & _
                                                   "  roundoff_outtime = @roundoff_outtime " & _
                                                   " ,workhour = @workhour " & _
                                                   " ,operationtype = @operationtype " & _
                                                   " ,isgraceroundoff = @isgraceroundoff " & _
                                                   " ,manualroundofftypeid = @manualroundofftypeid " & _
                                                   " ,roundminutes = @roundminutes " & _
                                                   " ,roundlimit = @roundlimit " & _
                                                   " ,roundoff_intime = @roundoff_intime " & _
                                                   "WHERE loginunkid = @loginunkid AND isvoid = 0 "

                                            objDataOperation.ClearParameters()

                                            If Not IsDBNull(drRow(0)("roundoff_intime")) Then
                                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, drRow(0)("roundoff_intime"))
                                            Else
                                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _checkintime)
                                            End If

                                            objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Roundoff_Outtime)
                                            If Not IsDBNull(drRow(0)("roundoff_intime")) Then
                                                objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drRow(0)("roundoff_intime")), _Roundoff_Outtime))
                                            Else
                                                objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, _checkintime, _Roundoff_Outtime))
                                            End If
                                            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoginId)
                                            objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                            objDataOperation.AddParameter("@manualroundofftypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eManRType)
                                            objDataOperation.AddParameter("@roundminutes", SqlDbType.Int, eZeeDataType.INT_SIZE, iMinutes)
                                            objDataOperation.AddParameter("@roundlimit", SqlDbType.Int, eZeeDataType.INT_SIZE, iLimit)

                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            _Loginunkid = mintMaxLoginId
                                            _OperationType = enTnAOperation.RoundOff
                                            _Isgraceroundoff = False
                                            _Manualroundofftypeid = eManRType
                                            _Roundminutes = iMinutes
                                            _Roundlimit = iLimit
                                            _Roundoff_Intime = _checkintime


                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                            'If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                            '                        , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                            '                        , mdtTodate, xUserModeSetting, xOnlyApproved _
                                            '                        , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                                            '                        , strUserAccessLevelFilterString, mstrFilter) = False Then

                                            If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                                               , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                               , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                               , blnIsHolidayConsiderOnWeekend _
                                                                               , blnIsDayOffConsiderOnWeekend _
                                                                               , blnIsHolidayConsiderOnDayoff _
                                                                               , blnApplyUserAccessFilter, intEmpId _
                                                                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                                'Pinkal (11-AUG-2017) -- End

                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        ElseIf IsDBNull(drRow(0)("roundoff_intime")) = False AndAlso _Roundoff_Outtime = Nothing Then
                                            If IsDBNull(drRow(0)("roundoff_intime")) Then
                                                strQ = " UPDATE tnalogin_tran SET roundoff_intime = checkintime,operationtype = @operationtype WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                                objDataOperation.ClearParameters()
                                                objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLoginId)
                                                objDataOperation.ExecNonQuery(strQ)
                                            End If

                                            strQ = " UPDATE tnalogin_tran SET roundoff_outtime = checkouttime,workhour = @workhour,operationtype = @operationtype WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                            objDataOperation.ClearParameters()
                                            objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drRow(0)("roundoff_intime")), _Checkouttime))
                                            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoginId)
                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            _Loginunkid = mintMaxLoginId
                                            _OperationType = enTnAOperation.RoundOff
                                            _Isgraceroundoff = False
                                            _Manualroundofftypeid = eManRType
                                            _Roundminutes = iMinutes
                                            _Roundlimit = iLimit


                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                            'If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                            '                        , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                            '                        , mdtTodate, xUserModeSetting, xOnlyApproved _
                                            '                        , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                                            '                        , strUserAccessLevelFilterString, mstrFilter) = False Then

                                            If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                                              , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds _
                                                                              , blnFirstCheckInLastCheckOut _
                                                                              , blnIsHolidayConsiderOnWeekend _
                                                                              , blnIsDayOffConsiderOnWeekend _
                                                                              , blnIsHolidayConsiderOnDayoff _
                                                                              , blnApplyUserAccessFilter, intEmpId _
                                                                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                                'Pinkal (11-AUG-2017) -- End

                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                        End If
                                    End If
                                Case enManualRoundOffType.ROUND_BOTH
                                    Dim mintMinLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Min(loginunkid)", "")), 0, dsList.Tables(0).Compute("Min(loginunkid)", "")))

                                    If mintMinLoginId > 0 Then     ' START FOR mintMinLoginId
                                        Dim drRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMinLoginId)
                                        If drRow.Length > 0 Then
                                            _Roundoff_Intime = Nothing
                                            _Roundoff_Outtime = Nothing
                                            _checkintime = Nothing
                                            _Checkouttime = Nothing
                                            _Totalhr = 0
                                            _TotalOvertimehr = 0
                                            _Ot2 = 0
                                            _Ot3 = 0
                                            _Ot4 = 0
                                            _TotalNighthr = 0
                                            _Shorthr = 0
                                            _Early_Coming = 0
                                            _Early_Going = 0
                                            _Late_Coming = 0
                                            _Late_Going = 0
                                            _IsDayOffShift = False
                                            _IsWeekend = False
                                            _IsHoliday = False
                                            _Isgraceroundoff = False
                                            _Manualroundofftypeid = eManRType
                                            _Roundminutes = iMinutes
                                            _Roundlimit = iLimit

                                            If IsDBNull(drRow(0)("checkintime")) OrElse IsDBNull(drRow(0)("checkouttime")) Then Continue For

                                            _checkintime = CDate(drRow(0)("checkintime"))
                                            _Checkouttime = CDate(drRow(0)("checkouttime"))

                                            Dim dtTime As DateTime = _checkintime
                                            If mdtRange IsNot Nothing AndAlso mdtRange.Rows.Count > 0 Then
                                                Dim lBound, uBound, mBound As Integer
                                                lBound = 0 : uBound = 0 : mBound = 0
                                                mBound = iLimit
                                                Dim tRange() As DataRow = mdtRange.Select(dtTime.Minute & " >= lbound AND " & dtTime.Minute & " <= ubound")
                                                If tRange.Length > 0 Then
                                                    lBound = CInt(tRange(0).Item("lbound"))
                                                    uBound = CInt(tRange(0).Item("ubound"))
                                                    If lBound > 0 Then mBound = mBound + lBound - 1
                                                    If dtTime.Minute > mBound Then
                                                        While dtTime.Minute < uBound
                                                            dtTime = dtTime.AddMinutes(1)
                                                        End While
                                                        If dtTime.Minute = 59 Then
                                                            dtTime = dtTime.AddMinutes(1)
                                                        End If
                                                    ElseIf dtTime.Minute <= mBound Then
                                                        If dtTime.Minute <> lBound Then
                                                            While dtTime.Minute >= lBound AndAlso dtTime.Minute > 0
                                                                dtTime = dtTime.AddMinutes(-1)
                                                            End While
                                                        End If
                                                    End If
                                                    _Roundoff_Intime = dtTime
                                                End If
                                            End If
                                            If _Roundoff_Intime <> Nothing Then   'START FOR _Roundoff_Intime
                                                strQ = " UPDATE tnalogin_tran SET " & _
                                                       "  workhour = @workhour " & _
                                                       " ,operationtype = @operationtype " & _
                                                       " ,isgraceroundoff = @isgraceroundoff " & _
                                                       " ,manualroundofftypeid = @manualroundofftypeid " & _
                                                       " ,roundminutes = @roundminutes " & _
                                                       " ,roundlimit = @roundlimit " & _
                                                       " ,roundoff_intime = @roundoff_intime " & _
                                                       "WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                                objDataOperation.ClearParameters()
                                                objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Roundoff_Intime)
                                                objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, _Roundoff_Intime, _Checkouttime))
                                                objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLoginId)
                                                objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                                objDataOperation.AddParameter("@manualroundofftypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eManRType)
                                                objDataOperation.AddParameter("@roundminutes", SqlDbType.Int, eZeeDataType.INT_SIZE, iMinutes)
                                                objDataOperation.AddParameter("@roundlimit", SqlDbType.Int, eZeeDataType.INT_SIZE, iLimit)

                                                objDataOperation.ExecNonQuery(strQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                _Loginunkid = mintMinLoginId
                                                _OperationType = enTnAOperation.RoundOff
                                                _Isgraceroundoff = False
                                                _Manualroundofftypeid = eManRType
                                                _Roundminutes = iMinutes
                                                _Roundlimit = iLimit


                                                'Pinkal (11-AUG-2017) -- Start
                                                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                'If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                '                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                '                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                '                    , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                                                '                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                                If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                                                  , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds _
                                                                                  , blnFirstCheckInLastCheckOut _
                                                                                  , blnIsHolidayConsiderOnWeekend _
                                                                                  , blnIsDayOffConsiderOnWeekend _
                                                                                  , blnIsHolidayConsiderOnDayoff _
                                                                                  , blnApplyUserAccessFilter, intEmpId _
                                                                    , strUserAccessLevelFilterString, mstrFilter) = False Then


                                                    'Pinkal (11-AUG-2017) -- End

                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If
                                        End If
                                    End If

                                    _Roundoff_Intime = Nothing
                                    _Roundoff_Outtime = Nothing
                                    dsList = GetList(objDataOperation, "List", True)

                                    Dim mintMaxLoginId As Integer = CInt(IIf(IsDBNull(dsList.Tables(0).Compute("Max(loginunkid)", "")), 0, dsList.Tables(0).Compute("Max(loginunkid)", "")))

                                    If mintMaxLoginId > 0 Then  ' START FOR mintMaxLoginId
                                        Dim drRow() As DataRow = dsList.Tables(0).Select("loginunkid = " & mintMaxLoginId)
                                        _Roundoff_Intime = Nothing
                                        _Roundoff_Outtime = Nothing
                                        _checkintime = Nothing
                                        _Checkouttime = Nothing
                                        _Totalhr = 0
                                        _TotalOvertimehr = 0
                                        _Ot2 = 0
                                        _Ot3 = 0
                                        _Ot4 = 0
                                        _TotalNighthr = 0
                                        _Shorthr = 0
                                        _Early_Coming = 0
                                        _Early_Going = 0
                                        _Late_Coming = 0
                                        _Late_Going = 0
                                        _IsDayOffShift = False
                                        _IsWeekend = False
                                        _IsHoliday = False
                                        _Isgraceroundoff = False
                                        _Manualroundofftypeid = eManRType
                                        _Roundminutes = iMinutes
                                        _Roundlimit = iLimit

                                        If IsDBNull(drRow(0)("checkintime")) OrElse IsDBNull(drRow(0)("checkouttime")) Then Continue For

                                        _checkintime = CDate(drRow(0)("checkintime"))
                                        _Checkouttime = CDate(drRow(0)("checkouttime"))

                                        Dim dtTime As DateTime = _Checkouttime
                                        If mdtRange IsNot Nothing AndAlso mdtRange.Rows.Count > 0 Then
                                            Dim lBound, uBound, mBound As Integer
                                            lBound = 0 : uBound = 0 : mBound = 0
                                            mBound = iLimit
                                            Dim tRange() As DataRow = mdtRange.Select(dtTime.Minute & " >= lbound AND " & dtTime.Minute & " <= ubound")
                                            If tRange.Length > 0 Then
                                                lBound = CInt(tRange(0).Item("lbound"))
                                                uBound = CInt(tRange(0).Item("ubound"))
                                                If lBound > 0 Then mBound = mBound + lBound - 1
                                                If dtTime.Minute > mBound Then
                                                    While dtTime.Minute < uBound
                                                        dtTime = dtTime.AddMinutes(1)
                                                    End While
                                                    If dtTime.Minute = 59 Then
                                                        dtTime = dtTime.AddMinutes(1)
                                                    End If
                                                ElseIf dtTime.Minute <= mBound Then
                                                    If dtTime.Minute <> lBound Then
                                                        While dtTime.Minute >= lBound AndAlso dtTime.Minute > 0
                                                            dtTime = dtTime.AddMinutes(-1)
                                                        End While
                                                    End If
                                                End If
                                                _Roundoff_Outtime = dtTime
                                            End If
                                        End If

                                        If _Roundoff_Outtime <> Nothing Then   ' START FOR _Roundoff_Outtime

                                            strQ = " UPDATE tnalogin_tran SET " & _
                                                   "  roundoff_outtime = @roundoff_outtime " & _
                                                   " ,workhour = @workhour " & _
                                                   " ,operationtype = @operationtype " & _
                                                   " ,isgraceroundoff = @isgraceroundoff " & _
                                                   " ,manualroundofftypeid = @manualroundofftypeid " & _
                                                   " ,roundminutes = @roundminutes " & _
                                                   " ,roundlimit = @roundlimit " & _
                                                   "WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                            objDataOperation.ClearParameters()
                                            objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, _Roundoff_Outtime)
                                            If Not IsDBNull(drRow(0)("roundoff_intime")) Then
                                                objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drRow(0)("roundoff_intime")), _Roundoff_Outtime))
                                            Else
                                                objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, _checkintime, _Roundoff_Outtime))
                                            End If
                                            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoginId)
                                            objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                            objDataOperation.AddParameter("@manualroundofftypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eManRType)
                                            objDataOperation.AddParameter("@roundminutes", SqlDbType.Int, eZeeDataType.INT_SIZE, iMinutes)
                                            objDataOperation.AddParameter("@roundlimit", SqlDbType.Int, eZeeDataType.INT_SIZE, iLimit)

                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            _Loginunkid = mintMaxLoginId
                                            _OperationType = enTnAOperation.RoundOff
                                            _Isgraceroundoff = False
                                            _Manualroundofftypeid = eManRType
                                            _Roundminutes = iMinutes
                                            _Roundlimit = iLimit
                                            _Roundoff_Intime = drRow(0)("roundoff_intime")


                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                            'If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                            '                  , xYearUnkid, xCompanyUnkid, mdtStartDate _
                                            '                  , mdtEndDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                            '                  , blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId, strUserAccessLevelFilterString, mstrFilter) = False Then

                                            If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                              , xYearUnkid, xCompanyUnkid, mdtStartDate _
                                                              , mdtEndDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                                            , blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                            , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                            , blnApplyUserAccessFilter, intEmpId, strUserAccessLevelFilterString, mstrFilter) = False Then

                                                'Pinkal (11-AUG-2017) -- End

                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        ElseIf IsDBNull(drRow(0)("roundoff_intime")) = False AndAlso _Roundoff_Outtime = Nothing Then
                                            If IsDBNull(drRow(0)("roundoff_intime")) Then
                                                strQ = " UPDATE tnalogin_tran SET roundoff_intime = checkintime,operationtype = @operationtype WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                                objDataOperation.ClearParameters()
                                                objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinLoginId)
                                                objDataOperation.ExecNonQuery(strQ)
                                            End If

                                            strQ = " UPDATE tnalogin_tran SET roundoff_outtime = checkouttime,workhour = @workhour,operationtype = @operationtype WHERE loginunkid = @loginunkid AND isvoid = 0 "
                                            objDataOperation.ClearParameters()
                                            objDataOperation.AddParameter("@workhour", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Second, CDate(drRow(0)("roundoff_intime")), _Checkouttime))
                                            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, enTnAOperation.RoundOff)
                                            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoginId)
                                            objDataOperation.ExecNonQuery(strQ)

                                            If objDataOperation.ErrorMessage <> "" Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            _Loginunkid = mintMaxLoginId
                                            _OperationType = enTnAOperation.RoundOff
                                            _Isgraceroundoff = False
                                            _Manualroundofftypeid = eManRType
                                            _Roundminutes = iMinutes
                                            _Roundlimit = iLimit


                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                            'If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                            '                        , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                            '                        , mdtTodate, xUserModeSetting, xOnlyApproved _
                                            '                        , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                                            '                        , strUserAccessLevelFilterString, mstrFilter) = False Then

                                            If InsertLoginSummary(objDataOperation, xDatabaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyUnkid, mdtFromdate _
                                                                    , mdtTodate, xUserModeSetting, xOnlyApproved _
                                                                              , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds _
                                                                              , blnFirstCheckInLastCheckOut _
                                                                              , blnIsHolidayConsiderOnWeekend _
                                                                              , blnIsDayOffConsiderOnWeekend _
                                                                              , blnIsHolidayConsiderOnDayoff _
                                                                              , blnApplyUserAccessFilter, intEmpId _
                                                                    , strUserAccessLevelFilterString, mstrFilter) = False Then

                                                'Pinkal (11-AUG-2017) -- End

                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                        End If
                                    End If

                            End Select
                            '---------------------------------- START FOR ROUNDING OFF ---------------------------------------
                        Next
                    End If
                Next
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "ManualRoundOff", mstrModuleName)
        Finally
        End Try
    End Function



    'S.SANDEEP [04 APR 2015] -- START
    'Issue : Appointment Date Changes
    Public Function VoidByAppointmentDateChange(ByVal xOldDate As Date, _
                                                ByVal xNewDate As Date, _
                                                ByVal xEmployeeUnkid As Integer, _
                                                ByVal xDataOpr As clsDataOperation, _
                                                ByVal xUserId As Integer, ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

        'Public Function VoidByAppointmentDateChange(ByVal xOldDate As Date, _
        '                                    ByVal xNewDate As Date, _
        '                                    ByVal xEmployeeUnkid As Integer, _
        '                                    ByVal xDataOpr As clsDataOperation, _
        '                                    ByVal xUserId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            xDataOpr.ClearParameters()

            StrQ = "SELECT " & _
                   " loginunkid " & _
                   ",CONVERT(CHAR(8),logindate,112) AS logindate " & _
                   "FROM tnalogin_tran " & _
                   "WHERE isvoid = 0 AND employeeunkid = '" & xEmployeeUnkid & "' " & _
                   "    AND CONVERT(CHAR(8),logindate,112) BETWEEN CASE WHEN @ODate < @NDate THEN @ODate ELSE @NDate END " & _
                   "    AND CASE WHEN @NDate < @ODate THEN @ODate ELSE @NDate END "

            xDataOpr.AddParameter("@ODate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xOldDate).ToString)
            xDataOpr.AddParameter("@NDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xNewDate).ToString)

            dsList = xDataOpr.ExecQuery(StrQ, "List")

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each xRow As DataRow In dsList.Tables(0).Rows

                    StrQ = "UPDATE tnalogin_tran " & _
                       "    SET isvoid = CAST(1 AS BIT),voiduserunkid = @voiduserunkid, voiddatetime = @voiddatetime, voidreason = @voidreason " & _
                       "WHERE loginunkid = @loginunkid "

                    xDataOpr.ClearParameters()

                    xDataOpr.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("loginunkid"))
                    xDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                    xDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                    xDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "void due to change in appointment."))

                    xDataOpr.ExecNonQuery(StrQ)

                    If xDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    _Loginunkid = xRow.Item("loginunkid")   'ALL DATA

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If InsertAuditTrailLogin(xDataOpr, enAuditType.DELETE, blnDonotAttendanceinSeconds) = False Then
                        'If InsertAuditTrailLogin(xDataOpr, enAuditType.DELETE) = False Then
                        'S.SANDEEP [04 JUN 2015] -- END
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM tnalogin_summary WHERE employeeunkid = '" & xEmployeeUnkid & "' AND CONVERT(CHAR(8),login_date,112) = '" & xRow.Item("logindate") & "' "

                    If xDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailLogin; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [04 APR 2015] -- END


    'Pinkal (09-Jun-2015) -- Start
    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

    ' DON'T USE THIS METHOD. THIS IS ONLY FOR NAH HOTEL.

    Public Function GetMaxLoginDate(ByVal dtLoginDate As Date, ByVal intEmployeeID As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = " SELECT top 1 * FROM tnalogin_tran WHERE convert(char(8),logindate,112) < @date AND checkouttime is NULL AND employeeunkid = @employeeunkid AND isvoid = 0 ORDER by logindate DESC ,checkintime DESC"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtLoginDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID.ToString)
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList IsNot Nothing Then
                dtTable = dsList.Tables(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMaxLoginTimeDate; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    'Pinkal (09-Jun-2015) -- End


#End Region

#Region "For HOLD EMPLOYEE Table"



    'Pinkal (01-Sep-2017) -- Start
    'Enhancement - Solved bug for Heron Portico Giving Error when Employee Absent Process did.

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Function GetEmployeeHoldUnholdList(ByVal mdtStartdate As DateTime, ByVal mdtEnddate As DateTime) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '               " hremployee_master.employeeunkid , " & _
    '               " hremployee_master.firstname + ' ' + hremployee_master.surname as employeename , " & _
    '               " hremployee_master.departmentunkid, " & _
    '               " hremployee_master.shiftunkid, " & _
    '               " tnalogin_tran.holdunkid, " & _
    '               " ISNULL(CONVERT(CHAR(10), tnaholdemployee_master.startdate,103) + ' - ' + CONVERT(char(10), tnaholdemployee_master.enddate,103),'') AS holddate " & _
    '               " FROM hremployee_master " & _
    '               " LEFT JOIN tnalogin_tran ON tnalogin_tran.employeeunkid = hremployee_master.employeeunkid  AND isvoid = 0 " & _
    '               " LEFT JOIN tnaholdemployee_master ON tnaholdemployee_master.holdunkid = tnalogin_tran.holdunkid AND hremployee_master.isactive=1 " & _
    '               " WHERE tnalogin_tran.logindate >= @startdate AND tnalogin_tran.logindate <= @enddate " & _
    '               " GROUP BY hremployee_master.firstname,hremployee_master.employeeunkid, " & _
    '               " hremployee_master.surname,hremployee_master.departmentunkid,hremployee_master.shiftunkid,tnalogin_tran.holdunkid, " & _
    '               " tnaholdemployee_master.startdate, tnaholdemployee_master.enddate "

    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
    '        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeHoldUnholdList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    'Public Function InsertHoldEmployee() As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    'Pinkal (12-Oct-2011) -- Start
    '    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    'Pinkal (12-Oct-2011) -- End


    '    Try
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '        objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

    '        strQ = "INSERT INTO tnaholdemployee_master ( " & _
    '          "  employeeunkid " & _
    '          ", startdate " & _
    '          ", enddate " & _
    '          ", ispost " & _
    '          ", isactive" & _
    '        ") VALUES (" & _
    '          "  @employeeunkid " & _
    '          ", @startdate " & _
    '          ", @enddate " & _
    '          ", @ispost " & _
    '          ", @isactive " & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")
    '        If Not dsList Is DBNull.Value And dsList.Tables(0).Rows.Count > 0 Then
    '            mintHoldunkid = dsList.Tables(0).Rows(0).Item(0)
    '        End If

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "tnaholdemployee_master", "holdunkid", mintHoldunkid) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        objDataOperation.ReleaseTransaction(True)
    '        'Pinkal (12-Oct-2011) -- End


    '        Return True
    '    Catch ex As Exception
    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        objDataOperation.ReleaseTransaction(False)
    '        'Pinkal (12-Oct-2011) -- End
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertHoldEmployee; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    'Public Function InsertMultipleHoldEmployee(ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

    '    'Public Function InsertMultipleHoldEmployee() As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try



    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '        strQ = "select holdunkid from tnaHoldemployee_Master " & _
    '              " WHERE employeeunkid = @empid" & _
    '              " AND (@st_date between startdate and enddate " & _
    '              " OR  @ed_date between startdate and enddate)"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@st_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        objDataOperation.AddParameter("@ed_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        If dsList.Tables(0).Rows.Count > 0 Then

    '            mintHoldunkid = dsList.Tables(0).Rows(0)(0)

    '            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "tnaholdemployee_master", "holdunkid", mintHoldunkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        End If

    '        'Pinkal (12-Oct-2011) -- End

    '        ' STRAT FOR DELETE HOLD EMPLOYEE DATA IN HOLD EMPLOYEE TABLE

    '        strQ = "Delete From tnaHoldemployee_Master " & _
    '                " WHERE employeeunkid = @empid" & _
    '                " AND (@st_date between startdate and enddate " & _
    '                " OR  @ed_date between startdate and enddate)"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@st_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        objDataOperation.AddParameter("@ed_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '        objDataOperation.ExecNonQuery(strQ)

    '        ' END FOR DELETE HOLD EMPLOYEE DATA IN HOLD EMPLOYEE TABLE


    '        ' STRAT FOR INSERT HOLD EMPLOYEE DATA IN HOLD EMPLOYEE TABLE

    '        objDataOperation.ClearParameters()
    '        strQ = "INSERT INTO tnaholdemployee_master ( " & _
    '          "  employeeunkid " & _
    '          ", startdate " & _
    '          ", enddate " & _
    '          ", ispost " & _
    '          ", isactive" & _
    '        ") VALUES (" & _
    '          "  @employeeunkid " & _
    '          ", @startdate " & _
    '          ", @enddate " & _
    '          ", @ispost " & _
    '          ", @isactive " & _
    '        "); SELECT @@identity"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '        objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")


    '        ' END FOR INSERT HOLD EMPLOYEE DATA IN HOLD EMPLOYEE TABLE

    '        If Not dsList Is DBNull.Value And dsList.Tables(0).Rows.Count > 0 Then
    '            mintHoldunkid = dsList.Tables(0).Rows(0).Item(0)


    '            'Pinkal (12-Oct-2011) -- Start
    '            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "tnaholdemployee_master", "holdunkid", mintHoldunkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            'Pinkal (12-Oct-2011) -- End



    '            ' STRAT FOR UPDATE HOLDID IN ALREADY EXIST EMPLOYEE LOGIN DATA IN LOGIN TRAN TABLE

    '            objDataOperation.ClearParameters()
    '            strQ = "Update tnalogin_tran set " & _
    '                   " holdunkid = @holdunkid " & _
    '                   " where employeeunkid = @emp_id and logindate between @start_date and @end_date and isvoid = 0 "
    '            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
    '            objDataOperation.AddParameter("@emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '            objDataOperation.ExecNonQuery(strQ)



    '            'START FOR INSERT AUDIT TRAIL LOGIN

    '            strQ = "Select isnull(loginunkid,0) as loginunkid,logindate,checkintime,checkouttime,workhour,breakhr,remark,inouttype,sourcetype " & _
    '                   " from tnalogin_tran " & _
    '                   " where employeeunkid = @emp_id and logindate between @start_date and @end_date and isvoid = 0 "

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '            dsList = objDataOperation.ExecQuery(strQ, "GetLoginunkid")

    '            If dsList.Tables("GetLoginunkid").Rows.Count > 0 Then

    '                For Each dr As DataRow In dsList.Tables("GetLoginunkid").Rows
    '                    mintLoginunkid = CInt(dr("loginunkid"))
    '                    mdtLogindate = CDate(dr("logindate"))
    '                    mdtcheckintime = CDate(dr("checkintime"))
    '                    mdtCheckouttime = CDate(dr("checkouttime"))
    '                    mintWorkhour = CInt(dr("workhour"))
    '                    mintBreakhr = CInt(dr("breakhr"))
    '                    mstrRemark = CStr(dr("remark"))
    '                    mintInOuType = CInt(dr("inouttype"))
    '                    mintSourceType = CInt(dr("sourcetype"))


    '                    'Pinkal (12-Oct-2011) -- Start
    '                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '                    '  InsertAuditTrailLogin(objDataOperation, 1)  ''FOR HOLD EMPLOYYEE INSERT AUDIT TYPE = 1 

    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'InsertAuditTrailLogin(objDataOperation, 2)  ''FOR HOLD EMPLOYYEE INSERT AUDIT TYPE = 1 
    '                    InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds)  ''FOR HOLD EMPLOYYEE INSERT AUDIT TYPE = 1 
    '                    'S.SANDEEP [04 JUN 2015] -- END

    '                    'Pinkal (12-Oct-2011) -- End

    '                Next
    '            End If
    '            'END FOR INSERT AUDIT TRAIL LOGIN


    '            ' END FOR UPDATE HOLDID IN ALREADY EXIST EMPLOYEE LOGIN DATA IN LOGIN TRAN TABLE
    '        End If

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertHoldEmployee; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Update Database Table (tnalogin_tran) </purpose>
    'Public Function UpdateHoldEmployee(ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

    '    'Public Function UpdateHoldEmployee() As Boolean
    '    'If isExist(mstrName, mintLoginunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    'Pinkal (12-Oct-2011) -- Start
    '    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    'Pinkal (12-Oct-2011) -- End

    '    Try
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
    '        objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)

    '        strQ = "UPDATE tnalogin_tran SET " & _
    '          " holdunkid = @holdunkid" & _
    '          " WHERE employeeunkid = @employeeunkid AND  logindate = @logindate AND isvoid = 0"

    '        objDataOperation.ExecNonQuery(strQ)


    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '        If clsCommonATLog.IsTableDataUpdate("atcommon_log", "tnaholdemployee_master", mintHoldunkid, "holdunkid", 2) Then

    '            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "tnaholdemployee_master", "holdunkid", mintHoldunkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        End If

    '        'Pinkal (12-Oct-2011) -- End


    '        'START FOR INSERT AUDIT TRAIL LOGIN

    '        strQ = "Select isnull(loginunkid,0) as loginunkid,logindate,checkintime,checkouttime,workhour,breakhr,remark,inouttype,sourcetype " & _
    '               " from tnalogin_tran " & _
    '               " where employeeunkid = @emp_id and logindate = @logindate and isvoid = 0 "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
    '        dsList = objDataOperation.ExecQuery(strQ, "GetLoginunkid")

    '        If dsList.Tables("GetLoginunkid").Rows.Count > 0 Then

    '            For Each dr As DataRow In dsList.Tables("GetLoginunkid").Rows
    '                mintLoginunkid = CInt(dr("loginunkid"))
    '                mdtLogindate = CDate(dr("logindate"))
    '                mdtcheckintime = CDate(dr("checkintime"))
    '                mdtCheckouttime = CDate(dr("checkouttime"))
    '                mintWorkhour = CInt(dr("workhour"))
    '                mintBreakhr = CInt(dr("breakhr"))
    '                mstrRemark = CStr(dr("remark"))
    '                mintInOuType = CInt(dr("inouttype"))
    '                mintSourceType = CInt(dr("sourcetype"))


    '                'Pinkal (12-Oct-2011) -- Start
    '                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '                'InsertAuditTrailLogin(objDataOperation, 1)  ''FOR HOLD EMPLOYYEE INSERT AUDIT TYPE = 1 

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'InsertAuditTrailLogin(objDataOperation, 2)  ''FOR HOLD EMPLOYYEE INSERT AUDIT TYPE = 1 
    '                InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds)  ''FOR HOLD EMPLOYYEE INSERT AUDIT TYPE = 1 
    '                'S.SANDEEP [04 JUN 2015] -- END

    '                'Pinkal (12-Oct-2011) -- End


    '            Next
    '        End If
    '        'END FOR INSERT AUDIT TRAIL LOGIN

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        objDataOperation.ReleaseTransaction(True)
    '        'Pinkal (12-Oct-2011) -- End

    '        Return True
    '    Catch ex As Exception
    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        objDataOperation.ReleaseTransaction(False)
    '        'Pinkal (12-Oct-2011) -- End
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    'Public Function UnHoldEmployee(ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

    '    'Public Function UnHoldEmployee() As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try


    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        Dim intHoldID As Integer = 0
    '        intHoldID = mintHoldunkid
    '        'Pinkal (12-Oct-2011) -- End



    '        'START FOR UPDATE SPECIFIC EMPLOYEE LOGIN RECORD FOR SPECIFIC DATE

    '        strQ = "UPDATE tnalogin_tran SET holdunkid = 0 " & _
    '               " WHERE employeeunkid = @employeeunkid " & _
    '               " AND logindate = @fromdate "

    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@fromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        objDataOperation.ExecQuery(strQ, "List")

    '        'START FOR INSERT AUDIT TRAIL LOGIN

    '        strQ = "Select isnull(loginunkid,0) as loginunkid,holdunkid,logindate,checkintime,checkouttime,workhour,breakhr,remark,inouttype,sourcetype " & _
    '               " from tnalogin_tran " & _
    '               " where employeeunkid = @emp_id and logindate = @fromdate and isvoid = 0 "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@fromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        dsList = objDataOperation.ExecQuery(strQ, "GetLoginunkid")

    '        If dsList.Tables("GetLoginunkid").Rows.Count > 0 Then

    '            For Each dr As DataRow In dsList.Tables("GetLoginunkid").Rows
    '                mintLoginunkid = CInt(dr("loginunkid"))
    '                mintHoldunkid = CInt(dr("holdunkid"))
    '                mdtLogindate = CDate(dr("logindate"))
    '                mdtcheckintime = CDate(dr("checkintime"))
    '                mdtCheckouttime = CDate(dr("checkouttime"))
    '                mintWorkhour = CInt(dr("workhour"))
    '                mintBreakhr = CInt(dr("breakhr"))
    '                mstrRemark = CStr(dr("remark"))
    '                mintInOuType = CInt(dr("inouttype"))
    '                mintSourceType = CInt(dr("sourcetype"))

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'InsertAuditTrailLogin(objDataOperation, 2)  ''FOR UNHOLD EMPLOYYEE INSERT AUDIT TYPE = 2
    '                InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds)  ''FOR UNHOLD EMPLOYYEE INSERT AUDIT TYPE = 2
    '                'S.SANDEEP [04 JUN 2015] -- END
    '            Next
    '        End If
    '        'END FOR INSERT AUDIT TRAIL LOGIN

    '        'END FOR UPDATE SPECIFIC EMPLOYEE LOGIN RECORD FOR SPECIFIC DATE


    '        'START FOR UPDATE HOLD EMPLOYEE MASTER RECORD FOR SPECIFIC HOLD ID

    '        strQ = "Update tnaholdemployee_master set isactive = 0,ispost = @ispost " & _
    '                       "  WHERE holdunkid = @holdunkid "
    '        objDataOperation.ClearParameters()


    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        ' objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
    '        objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intHoldID.ToString)
    '        'Pinkal (12-Oct-2011) -- End


    '        objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)

    '        objDataOperation.ExecQuery(strQ, "List")

    '        'END FOR UPDATE HOLD EMPLOYEE MASTER RECORD FOR SPECIFIC HOLD ID


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "tnaholdemployee_master", "holdunkid", intHoldID) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintHoldunkid = 0

    '        'Pinkal (12-Oct-2011) -- End

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: UnHoldEmployee; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    'Public Function UnHoldMultipleEmployee(ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

    '    'Public Function UnHoldMultipleEmployee() As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    'Pinkal (12-Oct-2011) -- Start
    '    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '    Dim intHoldId As Integer = 0
    '    'Pinkal (12-Oct-2011) -- End

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try

    '        strQ = "SELECT " & _
    '               " isnull(holdunkid,0) holdunkid  from tnaholdemployee_master where holdunkid in " & _
    '               " (select holdunkid from tnalogin_tran WHERE employeeunkid = @empunkid " & _
    '               " AND logindate between @fromdate and @todate) "
    '        objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@fromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '        objDataOperation.AddParameter("@todate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)

    '        dsList = objDataOperation.ExecQuery(strQ, "GetHoldUnkId")
    '        If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then

    '            'LOOP IS USE WHEN GET MULTIPLE HOLDID

    '            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

    '                mintHoldunkid = CInt(dsList.Tables(0).Rows(i)("holdunkid"))


    '                'Pinkal (12-Oct-2011) -- Start
    '                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '                intHoldId = mintHoldunkid
    '                'Pinkal (12-Oct-2011) -- End



    '                ''START FOR UPDATE SPECIFIC EMPLOYEE LOGIN RECORD FOR SPECIFIC DATE

    '                strQ = "UPDATE tnalogin_tran SET holdunkid = 0 " & _
    '                       " WHERE employeeunkid = @employeeunkid AND logindate between @fromdate and @todate"

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '                objDataOperation.AddParameter("@fromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '                objDataOperation.AddParameter("@todate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '                objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)
    '                objDataOperation.ExecNonQuery(strQ)

    '                'START FOR INSERT AUDIT TRAIL LOGIN

    '                strQ = "Select isnull(loginunkid,0) as loginunkid,holdunkid,logindate,checkintime,checkouttime,workhour,breakhr,remark,inouttype,sourcetype " & _
    '                       " from tnalogin_tran " & _
    '                       " where employeeunkid = @emp_id and logindate between @fromdate and @todate and isvoid = 0 "

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '                objDataOperation.AddParameter("@fromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '                objDataOperation.AddParameter("@todate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '                dsList = objDataOperation.ExecQuery(strQ, "GetLoginunkid")

    '                If dsList.Tables("GetLoginunkid").Rows.Count > 0 Then

    '                    For Each dr As DataRow In dsList.Tables("GetLoginunkid").Rows
    '                        mintLoginunkid = CInt(dr("loginunkid"))
    '                        mintHoldunkid = CInt(dr("holdunkid"))
    '                        mdtLogindate = CDate(dr("logindate"))
    '                        mdtcheckintime = CDate(dr("checkintime"))
    '                        mdtCheckouttime = CDate(dr("checkouttime"))
    '                        mintWorkhour = CInt(dr("workhour"))
    '                        mintBreakhr = CInt(dr("breakhr"))
    '                        mstrRemark = CStr(dr("remark"))
    '                        mintInOuType = CInt(dr("inouttype"))
    '                        mintSourceType = CInt(dr("sourcetype"))

    '                        'S.SANDEEP [04 JUN 2015] -- START
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'InsertAuditTrailLogin(objDataOperation, 2)  ''FOR UNHOLD EMPLOYYEE INSERT AUDIT TYPE = 2
    '                        InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds)  ''FOR UNHOLD EMPLOYYEE INSERT AUDIT TYPE = 2
    '                        'S.SANDEEP [04 JUN 2015] -- END
    '                    Next
    '                End If
    '                'END FOR INSERT AUDIT TRAIL LOGIN


    '                'END FOR UPDATE SPECIFIC EMPLOYEE LOGIN RECORD FOR SPECIFIC DATE


    '                ' START CHECK FOR THE TOTAL COUNT OF SPECIIC HOLDID IN LOGIN TRAN TABLE

    '                strQ = "Select count(*) 'choldid',min(logindate) 'Mindate',max(logindate) 'Maxdate' From tnalogin_tran where holdunkid = @holdid"
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@holdid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
    '                Dim dsTempList As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                ' END CHECK FOR THE TOTAL COUNT OF SPECIIC HOLDID IN LOGIN TRAN TABLE

    '                If Not dsTempList Is Nothing And dsTempList.Tables(0).Rows.Count > 0 And CInt(dsTempList.Tables(0).Rows(0)("choldid")) = 0 Then

    '                    'START FOR UPDATE HOLD EMPLOYEE MASTER RECORD FOR SPECIFIC HOLD ID

    '                    'strQ = "Delete From tnaholdemployee_master " & _
    '                    '       " WHERE holdunkid = @holdunkid "
    '                    strQ = "Update tnaholdemployee_master set isactive = 0,ispost = @ispost " & _
    '                           "  WHERE holdunkid = @holdunkid "
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
    '                    objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)
    '                    objDataOperation.ExecNonQuery(strQ)

    '                    'Pinkal (12-Oct-2011) -- Start
    '                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "tnaholdemployee_master", "holdunkid", intHoldId) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    'Pinkal (12-Oct-2011) -- End

    '                    mintHoldunkid = 0

    '                    'END FOR UPDATE HOLD EMPLOYEE MASTER RECORD FOR SPECIFIC HOLD ID

    '                ElseIf Not dsTempList Is Nothing And dsTempList.Tables(0).Rows.Count > 0 And CInt(dsTempList.Tables(0).Rows(0)("choldid")) > 0 Then

    '                    'START FOR UPDATE SPECIFIC HOLDID RECORD 

    '                    strQ = "UPDATE tnaholdemployee_master SET startdate = @startdate,enddate=@enddate,ispost =@ispost " & _
    '                      " WHERE employeeunkid = @employeeunkid AND holdunkid = @hold_unkid"

    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '                    objDataOperation.AddParameter("@hold_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
    '                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsTempList.Tables(0).Rows(0)("Mindate").ToString)
    '                    objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dsTempList.Tables(0).Rows(0)("Maxdate").ToString)
    '                    objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)
    '                    objDataOperation.ExecNonQuery(strQ)

    '                    'END FOR UPDATE SPECIFIC HOLDID RECORD 

    '                    'Pinkal (12-Oct-2011) -- Start
    '                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '                    If clsCommonATLog.IsTableDataUpdate("atcommon_log", "tnaholdemployee_master", intHoldId, "holdunkid", 2) Then

    '                        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "tnaholdemployee_master", "holdunkid", intHoldId) = False Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                    End If

    '                    'Pinkal (12-Oct-2011) -- End

    '                End If
    '            Next
    '        Else
    '            'START FOR UPDATE HOLD EMPLOYEE MASTER RECORD FOR SPECIFIC HOLD ID WHEN IT IS NOT IN LOGIN TRAN TABLE

    '            strQ = "SELECT " & _
    '             " isnull(holdunkid,0) holdunkid  from tnaholdemployee_master " & _
    '             " where employeeunkid = @employeeunkid and startdate >= @startdate and enddate <= @enddate "
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromdate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTodate)
    '            dsList = objDataOperation.ExecQuery(strQ, "Holdid")

    '            If dsList IsNot Nothing Then
    '                If dsList.Tables("Holdid").Rows.Count > 0 Then
    '                    strQ = "Update tnaholdemployee_master set isactive = 0,ispost = @ispost " & _
    '                         "  WHERE holdunkid = @holdunkid "
    '                    For i As Integer = 0 To dsList.Tables("Holdid").Rows.Count - 1
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("Holdid").Rows(i)("holdunkid").ToString)
    '                        objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPost.ToString)
    '                        objDataOperation.ExecNonQuery(strQ)

    '                        'Pinkal (12-Oct-2011) -- Start
    '                        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '                        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "tnaholdemployee_master", "holdunkid", dsList.Tables("Holdid").Rows(i)("holdunkid").ToString) = False Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        'Pinkal (12-Oct-2011) -- End

    '                    Next
    '                End If
    '            End If

    '        End If

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: UnHoldEmployee; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function CheckforHoldEmployee(ByVal intEmployeeid As Integer, ByVal dtLogindate As DateTime, Optional ByVal objDOperation As clsDataOperation = Nothing) As Integer
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing

        Try

            If objDOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDOperation
            End If


            ' strQ = "SELECT holdunkid " & _
            '" FROM tnaholdemployee_master " & _
            '" WHERE employeeunkid = @employeeunkid " & _
            '" AND Convert(char(8),@logindate,112) between Convert(char(8),startdate,112) AND Convert(char(8),enddate,112) " & _
            '" AND isactive = 1"

            strQ = " SELECT ISNULL(holdunkid,0) AS  holdunkid " & _
                      " FROM " & _
                      " ( " & _
                      "          SELECT " & _
                      "          holdunkid " & _
                      "         ,employeeunkid " & _
                      "         ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY operationtype DESC,effectivedate DESC) AS RowNo " & _
                      "         ,operationtype " & _
                      "          FROM tnaholdemployee_master " & _
                      "          WHERE tnaholdemployee_master.isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112)  <= @logindate " & _
                      "          AND tnaholdemployee_master.employeeunkid = @employeeunkid AND operationtype = 1 " & _
                      "  ) " & _
                      "  AS A where A.RowNo = 1  "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtLogindate.Date).ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("holdunkid"))
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CheckforHoldEmployee; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (01-Sep-2017) -- End

#End Region

#Region "For LOGIN SUMMERY Table"

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertLoginSummary(ByVal objDataOperation As clsDataOperation _
                                      , ByVal xDatabaseName As String _
                                      , ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                      , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As DateTime _
                                      , ByVal mdtEndDate As DateTime, ByVal xUserModeSetting As String _
                                      , ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , ByVal blnPolicyManagementTNA As Boolean _
                                      , ByVal blnDonotAttendanceinSeconds As Boolean _
                                      , ByVal blnFirstCheckInLastCheckOut As Boolean _
                                      , ByVal blnIsHolidayConsiderOnWeekend As Boolean _
                                      , ByVal blnIsDayOffConsiderOnWeekend As Boolean _
                                      , ByVal blnIsHolidayConsiderOnDayoff As Boolean _
                                      , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                      , Optional ByVal intEmpId As Integer = -1 _
                                      , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                      , Optional ByVal mstrFilter As String = "" _
                                      , Optional ByVal intLoginsummaryunkid As Integer = 0 _
                                      , Optional ByVal blnUsedForAbsent As Boolean = False) As Boolean


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal mblnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal mblnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal mblnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim count As Integer = 0
        Dim mintltcome_graceinsec As Integer = 0
        Dim mintelrcome_graceinsec As Integer = 0
        Dim mintltgoing_graceinsec As Integer = 0
        Dim mintelrgoing_graceinsec As Integer = 0

        Try

            'START CHECK FOR LOGIN TRAN FOR CALCULATE TOTAL WORKING HOURS

            If blnUsedForAbsent = False Then

                'strQ = "select isnull(sum(workhour),0) 'workhour' from tnalogin_tran where employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1"
                strQ = "select isnull(sum(workhour),0) 'workhour' from tnalogin_tran where employeeunkid = @Emp_id AND convert(char(8),logindate,112) = @date and isvoid = 0 AND inouttype = 1"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLogindate))

                dsList = objDataOperation.ExecQuery(strQ, "CheckWorkHour")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("workhour")) = 0 Then
                        DeleteLoginSummary(objDataOperation, intLoginsummaryunkid)
                        Return True
                    Else
                        mintTotalhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                    End If

                End If
            ElseIf blnUsedForAbsent = True Then
                mintTotalhr = 0
            End If

            'END CHECK FOR LOGIN TRAN FOR CALCULATE TOTAL WORKING HOURS



            ' START CHECK FOR LOGIN SUMMARY IF RECORED EXIST FOR SPECIFIC EMPLOYEE AND SPECIFIC LOGIN DATE

            Dim intShiftID As Integer = 0
            objDataOperation.ClearParameters()
            strQ = "select count(*) 'Rcount',isnull(loginsummaryunkid,0) 'loginsummaryunkid',isnull(shiftunkid,0) 'shiftunkid' from tnalogin_summary " & _
                " where employeeunkid = @empid AND convert(char(10),login_date,101) = @logdate group by loginsummaryunkid,shiftunkid"

            objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            dsList = Nothing
            dsList = objDataOperation.ExecQuery(strQ, "CountLoginSummary")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                count = CInt(dsList.Tables(0).Rows(0)("Rcount"))
                mintLoginsummaryunkid = CInt(dsList.Tables(0).Rows(0)("loginsummaryunkid"))
                intShiftID = CInt(dsList.Tables(0).Rows(0)("shiftunkid"))
            End If

            If intShiftID > 0 AndAlso intShiftID <> mintShiftunkid Then

                strQ = "select isnull(workhour,0) 'workhour' from tnalogin_tran where employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1 AND loginunkid =  @loginunkid"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
                objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginunkid)
                dsList = objDataOperation.ExecQuery(strQ, "CheckWorkHour")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                    mintTotalhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                End If

            End If

            ' END CHECK FOR LOGIN SUMMARY IF RECORED EXIST FOR SPECIFIC EMPLOYEE AND SPECIFIC LOGIN DATE


            ' START CHECK FOR HALF DAY AND FULL DAY

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            Dim mblnIsNormalHrsForWeekend = False
            Dim mblnIsNormalHrsForHoliday = False
            Dim mblnIsNormalHrsForDayOff = False
            'Pinkal (18-Jul-2017) -- End


            'Pinkal (07-Sep-2017) -- Start
            'Enhancement - Working on B5 Plus Enhancement.
            Dim mblnIsCountOnlyShiftTime As Boolean = False
            Dim mintShiftHrsPerDayInSec As Integer = 0
            'Pinkal (07-Sep-2017) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            Dim mblnCountOTonTotalWorkedHrs As Boolean = True
            'Pinkal (12-Oct-2017) -- End


            'Pinkal (03-Aug-2018) -- Start
            'Enhancement - Changes For B5 [Ref #289]
            Dim mintCountOTMinsAfterShiftEndTime As Integer = 0
            'Pinkal (03-Aug-2018) -- End



            'Pinkal (28-Nov-2019) -- Start
            'Defect Hill Packaging - Calculating Day Type wrongly.
            Dim mblnIsOpenShift As Boolean = False
            'Pinkal (28-Nov-2019) -- End


            Dim objshift As New clsNewshift_master

            'Pinkal (01-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            'objshift._Shiftunkid = mintShiftunkid
            objshift._Shiftunkid(objDataOperation) = mintShiftunkid
            'Pinkal (01-Feb-2022) -- End


            'Pinkal (28-Nov-2019) -- Start
            'Defect Hill Packaging - Calculating Day Type wrongly.
            mblnIsOpenShift = objshift._IsOpenShift
            'Pinkal (28-Nov-2019) -- End


            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            mblnIsNormalHrsForWeekend = objshift._IsNormalHrsForWeekend
            mblnIsNormalHrsForHoliday = objshift._IsNormalHrsForHoliday
            mblnIsNormalHrsForDayOff = objshift._IsNormalHrsForDayOff
            'Pinkal (18-Jul-2017) -- End

            'Pinkal (07-Sep-2017) -- Start
            'Enhancement - Working on B5 Plus Enhancement.
            mblnIsCountOnlyShiftTime = objshift._CountOnlyShiftTime
            'Pinkal (07-Sep-2017) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            mblnCountOTonTotalWorkedHrs = objshift._CountOTonTotalWorkedHrs
            'Pinkal (12-Oct-2017) -- End


            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            If mblnIsUnpaidLeave = False AndAlso mblnIspaidLeave = False AndAlso mblnIsWeekend = False Then
                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.
                'mblnIsDayOffshift = objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid)
                mblnIsDayOffshift = objEmpDayOff.isExist(mdtLogindate, mintEmployeeunkid, -1, objDataOperation)
                'Pinkal (07-Sep-2017) -- End
            End If

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(objshift._Shiftunkid, objDataOperation)

            Dim mdtShiftInTime As DateTime = Nothing
            Dim mdtShiftOutTime As DateTime = Nothing

            mdctFinalOTOrder = New Dictionary(Of String, Integer)
            mdctFinalOTOrder.Add("OT1", 0)
            mdctFinalOTOrder.Add("OT2", 0)
            mdctFinalOTOrder.Add("OT3", 0)
            mdctFinalOTOrder.Add("OT4", 0)

            Dim mdctOTOrder As Dictionary(Of Integer, String) = Nothing
            Dim mdctFinalOTPenalty As Dictionary(Of String, Integer) = Nothing


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.


                Dim drDayName() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))
                If drDayName.Length > 0 Then

                    Dim mintShiftCountDay As Integer = 0
                    If objshift._IsOpenShift = False Then
                        mintShiftCountDay = DateDiff(DateInterval.Day, CDate(drDayName(0)("starttime")).Date, CDate(drDayName(0)("endtime")).Date)
                        mdtShiftInTime = mdtLogindate.Date & " " & CDate(drDayName(0)("starttime")).ToShortTimeString()
                    mdtShiftOutTime = mdtLogindate.Date.AddDays(mintShiftCountDay) & " " & CDate(drDayName(0)("endtime")).ToShortTimeString()

                    'Pinkal (07-Sep-2017) -- Start
                    'Enhancement - Working on B5 Plus Enhancement.
                    mintShiftHrsPerDayInSec = DateDiff(DateInterval.Second, mdtShiftInTime, mdtShiftOutTime)
                    'Pinkal (07-Sep-2017) -- End


                    'Pinkal (03-Aug-2018) -- Start
                    'Enhancement - Changes For B5 [Ref #289]
                    mintCountOTMinsAfterShiftEndTime = CInt(drDayName(0)("countotmins_aftersftendtimeinsec"))
                    'Pinkal (03-Aug-2018) -- End


                    End If

                        Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                        If drWeekend.Length > 0 Then
                            mblnIsWeekend = True
                        End If


                    'FOR HOLIDAY
                    Dim objholiday As New clsemployee_holiday

                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                'Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                '                                                                 , xCompanyUnkid, mdtFromdate.Date, mdtEndDate _
                '                                                                 , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                '                                                                 , blnApplyUserAccessFilter, intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)

                'Pinkal (31-Oct-2018) -- Start
                'Bug - System freezes when importing device attendance [Issue No : 0002987].

                'Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                '                                                             , xCompanyUnkid, mdtLogindate, mdtLogindate _
                '                                                                 , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                '                                                                 , blnApplyUserAccessFilter, intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)


                'Dim dRow1() As DataRow = dsData.Tables("List").Select("employeeunkid = " & mintEmployeeunkid & " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'")

                'For Each dr As DataRow In dRow1
                '    If eZeeDate.convertDate(dr("holidaydate")).Date = mdtLogindate.Date Then
                '        mblnIsholiday = True
                '    End If
                'Next

                    Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                                                                 , xCompanyUnkid, mdtLogindate, mdtLogindate _
                                                                                     , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                                             , blnApplyUserAccessFilter, IIf(intEmpId <= 0, mintEmployeeunkid, intEmpId), objDataOperation, strUserAccessLevelFilterString, " AND convert(char(8),lvholiday_master.holidaydate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "'")

                If dsData IsNot Nothing AndAlso dsData.Tables("List").Rows.Count > 0 Then
                    mblnIsholiday = True
                End If


                'Pinkal (31-Oct-2018) -- End


                'Pinkal (11-AUG-2017) -- End




            End If

            If mblnIsWeekend AndAlso mblnIsholiday Then
                mblnIsholiday = blnIsHolidayConsiderOnWeekend
                mblnIsWeekend = Not blnIsHolidayConsiderOnWeekend
            End If

            If mblnIsDayOffshift AndAlso mblnIsWeekend Then
                mblnIsDayOffshift = blnIsDayOffConsiderOnWeekend
                mblnIsWeekend = Not blnIsDayOffConsiderOnWeekend
            End If

            If mblnIsDayOffshift AndAlso mblnIsholiday Then
                mblnIsholiday = blnIsHolidayConsiderOnDayoff
                mblnIsDayOffshift = Not blnIsHolidayConsiderOnDayoff
            End If

            'Pinkal (11-AUG-2017) -- End



            'Pinkal (25-AUG-2017) -- Start
            'Enhancement - Working on B5 plus TnA Enhancement.
            If mblnIspaidLeave OrElse mblnIsUnpaidLeave Then
                mblnIsholiday = False
                mblnIsDayOffshift = False
                mblnIsWeekend = False
            End If
            'Pinkal (25-AUG-2017) -- End



            'Pinkal (12-Jan-2022)-- Start
            'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
            Dim mstrGroupName As String = ""
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroupName = objGroup._Groupname
            objGroup = Nothing
            'Pinkal (12-Jan-2022) -- End

            If mintTotalhr > 0 Then

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.
				If objshift._IsOpenShift = False Then
					CalculateEarlyLate_ComingGoing(objDataOperation, mdtShiftInTime, mdtShiftOutTime)
                End If
                'Pinkal (07-Sep-2017) -- End

                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                'Dim drDayName() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))

                If drDayName.Length > 0 Then


                    '    Dim mintShiftCountDay As Integer = 0
                    '    If objshift._IsOpenShift = False Then
                    '        mintShiftCountDay = DateDiff(DateInterval.Day, CDate(drDayName(0)("starttime")).Date, CDate(drDayName(0)("endtime")).Date)
                    '        mdtShiftInTime = mdtLogindate.Date & " " & CDate(drDayName(0)("starttime")).ToShortTimeString()
                    '        mdtShiftOutTime = mdtLogindate.Date.AddDays(mintShiftCountDay) & " " & CDate(drDayName(0)("endtime")).ToShortTimeString
                    '    End If

                    '    If mblnIsDayOffshift = False Then
                    '        Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLogindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                    '        If drWeekend.Length > 0 Then
                    '            mblnIsWeekend = True
                    '        End If
                    '    End If



                    '    'FOR HOLIDAY
                    '    Dim objholiday As New clsemployee_holiday
                    '    Dim dsData As DataSet = objholiday.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                    '                                                                     , xCompanyUnkid, mdtFromdate.Date, mdtEndDate _
                    '                                                                     , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                    '                                                                     , blnApplyUserAccessFilter, intEmpId, objDataOperation, strUserAccessLevelFilterString, mstrFilter)
                    '    Dim drRow() As DataRow = dsData.Tables("List").Select("employeeunkid = " & mintEmployeeunkid & " AND holidaydate = '" & eZeeDate.convertDate(mdtLogindate) & "'")

                    '    For Each dr As DataRow In drRow
                    '        If eZeeDate.convertDate(dr("holidaydate")).Date = mdtLogindate.Date Then
                    '            mblnIsholiday = True
                    '        End If
                    '    Next

                    'Pinkal (11-AUG-2017) -- End


                    'END FOR WEEKEND AND HOLIDAY INSERT BOTH TOTAL WORKING HOURS AND OVERTIME 

                    If blnPolicyManagementTNA Then
                        Dim objPolicyTran As New clsPolicy_tran
                        Dim objEmpPolicyTran As New clsemployee_policy_tran
                        mintPolicyId = objEmpPolicyTran.GetPolicyTranID(mdtLogindate.Date, mintEmployeeunkid, objDataOperation)
                        objPolicyTran.GetPolicyTran(mintPolicyId, , objDataOperation)
                        Dim dtPolicyTran As DataTable = objPolicyTran._DataList

                        Dim drPolicy() As DataRow = dtPolicyTran.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLogindate.Date), False, FirstDayOfWeek.Sunday).ToString()))
                        If drPolicy.Length > 0 Then
                            mintelrcome_graceinsec = CInt(drPolicy(0)("elrcome_graceinsec"))
                            mintltcome_graceinsec = CInt(drPolicy(0)("ltcome_graceinsec"))
                            mintelrgoing_graceinsec = CInt(drPolicy(0)("elrgoing_graceinsec"))
                            mintltgoing_graceinsec = CInt(drPolicy(0)("ltgoing_graceinsec"))

                            If blnFirstCheckInLastCheckOut = True Then
                                mintTotalhr = GetEmployeeTotalWorkHrFromPolicy(objDataOperation, drPolicy, blnDonotAttendanceinSeconds)
                            End If

                            Dim objconfig As New clsConfigOptions
                            Dim mintFinalWorkingHrs As Integer = 0


                            'Pinkal (18-Jul-2017) -- Start
                            'Enhancement - TnA Enhancement for B5 Plus Company .
                                If ConfigParameter._Object._FirstCheckInLastCheckOut Then
                                    If CInt(drPolicy(0)("countbreaktimeaftersec")) < mintTotalhr Then
                                        mintFinalWorkingHrs = (CInt(drDayName(0)("workinghrsinsec")) - CInt(drPolicy(0)("breaktimeinsec")))
                                    Else
                                        mintFinalWorkingHrs = CInt(drDayName(0)("workinghrsinsec"))
                                    End If
                                ElseIf ConfigParameter._Object._FirstCheckInLastCheckOut = False Then
                                    mintFinalWorkingHrs = CInt(drDayName(0)("workinghrsinsec"))
                                End If
                            'Pinkal (18-Jul-2017) -- End

                            If mblnIsholiday = False AndAlso mblnIsWeekend = False AndAlso mblnIsDayOffshift = False Then   ' OVERTIME GOES TO OT1,OT2 WHEN IT'S NOT WEEKEND OR HOLIDAY.
                                mdctOTOrder = objconfig.GetOTOrder(True, mblnIsWeekend, mblnIsDayOffshift, mblnIsholiday)

                                'Pinkal (18-Jul-2017) -- Start
                                'Enhancement - TnA Enhancement for B5 Plus Company .

                                'Pinkal (07-Sep-2017) -- Start
                                'Enhancement - Working on B5 Plus Enhancement.
                                'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName)

                                'Pinkal (25-Jan-2018) -- Start
                                'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                'Pinkal (28-Nov-2019) -- End

                                'Pinkal (25-Jan-2018) -- End


                                'Pinkal (07-Sep-2017) -- End


                                'Pinkal (18-Jul-2017) -- End


                            ElseIf mblnIsholiday OrElse mblnIsWeekend OrElse mblnIsDayOffshift Then  ' OVERTIME GOES TO OT1,OT2,OT3,OT4 WHEN IT'S WEEKEND OR HOLIDAY.  
                                mdctOTOrder = Nothing


                                'Pinkal (11-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                'If mblnIsholiday Then
                                '    If mblnIsDayOffshift Then mblnIsDayOffshift = False
                                '    If mblnIsWeekend Then mblnIsWeekend = False
                                'End If

                                'If mblnIsDayOffshift Then
                                '    If mblnIsWeekend Then mblnIsWeekend = False
                                'End If
                                'Pinkal (11-AUG-2017) -- End


                                mdctOTOrder = objconfig.GetOTOrder(False, mblnIsWeekend, mblnIsDayOffshift, mblnIsholiday)


                                'Pinkal (18-Jul-2017) -- Start
                                'Enhancement - TnA Enhancement for B5 Plus Company .

                                If mblnIsWeekend Then
                                    If mblnIsNormalHrsForWeekend Then

                                        'Pinkal (07-Sep-2017) -- Start
                                        'Enhancement - Working on B5 Plus Enhancement.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName)

                                        'Pinkal (25-Jan-2018) -- Start
                                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                        'Pinkal (28-Nov-2019) -- Start
                                        'Defect Hill Packaging - Calculating Day Type wrongly.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                        CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                        'Pinkal (28-Nov-2019) -- End

                                        'Pinkal (25-Jan-2018) -- End


                                        'Pinkal (07-Sep-2017) -- End
                                    Else

                                        'Pinkal (07-Sep-2017) -- Start
                                        'Enhancement - Working on B5 Plus Enhancement.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder)

                                        'Pinkal (25-Jan-2018) -- Start
                                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                        'Pinkal (28-Nov-2019) -- Start
                                        'Defect Hill Packaging - Calculating Day Type wrongly.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                        CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                        'Pinkal (28-Nov-2019) -- End

                                        'Pinkal (25-Jan-2018) -- End

                                        'Pinkal (07-Sep-2017) -- End

                                    End If

                                ElseIf mblnIsholiday Then
                                    If mblnIsNormalHrsForHoliday Then

                                        'Pinkal (07-Sep-2017) -- Start
                                        'Enhancement - Working on B5 Plus Enhancement.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName)

                                        'Pinkal (25-Jan-2018) -- Start
                                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                        'Pinkal (28-Nov-2019) -- Start
                                        'Defect Hill Packaging - Calculating Day Type wrongly.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                        CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                        'Pinkal (28-Nov-2019) -- End

                                        'Pinkal (25-Jan-2018) -- End

                                        'Pinkal (07-Sep-2017) -- End
                                    Else

                                        'Pinkal (07-Sep-2017) -- Start
                                        'Enhancement - Working on B5 Plus Enhancement.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder)

                                        'Pinkal (25-Jan-2018) -- Start
                                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                        'Pinkal (28-Nov-2019) -- Start
                                        'Defect Hill Packaging - Calculating Day Type wrongly.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                        CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                        'Pinkal (28-Nov-2019) -- End

                                        'Pinkal (25-Jan-2018) -- End


                                        'Pinkal (07-Sep-2017) -- End


                                    End If

                                ElseIf mblnIsDayOffshift Then
                                    If mblnIsNormalHrsForDayOff Then
                                        'Pinkal (07-Sep-2017) -- Start
                                        'Enhancement - Working on B5 Plus Enhancement.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName)

                                        'Pinkal (25-Jan-2018) -- Start
                                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                        'Pinkal (28-Nov-2019) -- Start
                                        'Defect Hill Packaging - Calculating Day Type wrongly.
                                        'CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                        CalculatePolicyMgmtWorkedHrs(mintFinalWorkingHrs, mdctOTOrder, drPolicy, drDayName, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                        'Pinkal (28-Nov-2019) -- End

                                        'Pinkal (25-Jan-2018) -- End

                                        'Pinkal (07-Sep-2017) -- End
                                    Else

                                        'Pinkal (07-Sep-2017) -- Start
                                        'Enhancement - Working on B5 Plus Enhancement.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder)

                                        'Pinkal (25-Jan-2018) -- Start
                                        'Enhancement - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime)

                                        'Pinkal (28-Nov-2019) -- Start
                                        'Defect Hill Packaging - Calculating Day Type wrongly.
                                        'CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation)
                                        CalculateOTOnWkPHDayOffHrs(mintFinalWorkingHrs, drPolicy, mdctOTOrder, mblnIsCountOnlyShiftTime, mdtShiftOutTime, objDataOperation, mblnIsOpenShift)
                                        'Pinkal (28-Nov-2019) -- End

                                        'Pinkal (25-Jan-2018) -- End

                                        'Pinkal (07-Sep-2017) -- End
                                    End If

                                End If

                                'Pinkal (18-Jul-2017) -- End

                                End If    ' END FOR mblnIsholiday,mblnIsWeekend , mblnIsDayOffshift

                            End If   ' END FOR drPolicy.Length > 0 

                    Else

                        'Pinkal (18-Jul-2017) -- Start
                        'Enhancement - TnA Enhancement for B5 Plus Company .

                        If (CInt(drDayName(0)("halffromhrsinsec")) <= 0 AndAlso CInt(drDayName(0)("halftohrsinsec")) <= 0) AndAlso CInt(drDayName(0)("workinghrsinsec")) > mintTotalhr Then

                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                If mblnIsOpenShift = False Then
                            mdblDayType = 0
                                ElseIf mblnIsOpenShift AndAlso mintTotalhr > 0 Then
                                    mdblDayType = 1
                                End If
                                'Pinkal (28-Nov-2019) -- End
                            End If


                            'Pinkal (24-Jan-2017) -- Start
                            'Enhancement -  0001879: Count short time before (mins) defined on any shift does not reflect / work on employee time sheet.
                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) < mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                            mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If
                            'Pinkal (24-Jan-2017) -- End

                            mdctFinalOTOrder.Item("OT1") = 0
                            'Pinkal (18-Jul-2017) -- End

                            'FOR HALF DAY
                        ElseIf mintTotalhr >= CInt(drDayName(0)("halffromhrsinsec")) And mintTotalhr <= CInt(drDayName(0)("halftohrsinsec")) Then

                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.
                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                            mdblDayType = 0.5
                            End If
                            'Pinkal (07-Sep-2017) -- End


                            'Pinkal (18-Jul-2017) -- Start
                            'Enhancement - TnA Enhancement for B5 Plus Company .
                            'mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("halftohrsinsec"))

                            'Pinkal (24-Jan-2017) -- Start
                            'Enhancement -  0001879: Count short time before (mins) defined on any shift does not reflect / work on employee time sheet.
                            'mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                            mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If
                            'Pinkal (24-Jan-2017) -- End


                            'Pinkal (18-Jul-2017) -- End
                            mdctFinalOTOrder.Item("OT1") = 0


                            'Pinkal (18-Jul-2017) -- Start
                            'Enhancement - TnA Enhancement for B5 Plus Company .

                            'FOR NOT EVEN HALF DAY & SHORT HRS
                        ElseIf (CInt(drDayName(0)("halffromhrsinsec")) > 0 AndAlso CInt(drDayName(0)("halftohrsinsec")) > 0) AndAlso mintTotalhr < CInt(drDayName(0)("halffromhrsinsec")) Then

                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.
                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                            mdblDayType = 0
                            End If
                            'Pinkal (07-Sep-2017) -- End

                            'Pinkal (24-Jan-2017) -- Start
                            'Enhancement -  0001879: Count short time before (mins) defined on any shift does not reflect / work on employee time sheet.
                            'mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                            mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If
                            'Pinkal (24-Jan-2017) -- End

                            mdctFinalOTOrder.Item("OT1") = 0
                            'Pinkal (18-Jul-2017) -- End


                            'FOR SHORT HOURS
                        ElseIf (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr) > CInt(drDayName(0)("calcshorttimebeforeinsec")) Then

                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.
                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                            mdblDayType = 1
                            End If
                            'Pinkal (07-Sep-2017) -- End


                            'Pinkal (24-Jan-2017) -- Start
                            'Enhancement -  0001879: Count short time before (mins) defined on any shift does not reflect / work on employee time sheet.
                            'mintShorthr = (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) - mintTotalhr
                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                                'mintShorthr = (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) - mintTotalhr
                                mintShorthr = (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr)
                            End If
                            'Pinkal (24-Jan-2017) -- End

                            mdctFinalOTOrder.Item("OT1") = 0


                            'FOR BEFORE SHORT HOURS TIME 
                        ElseIf (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr) <= CInt(drDayName(0)("calcshorttimebeforeinsec")) And (CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr) > 0 Then

                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.
                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                            mdblDayType = 1
                            End If
                            'Pinkal (07-Sep-2017) -- End

                            mintShorthr = 0
                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR EARLY CHECK OUT
                        ElseIf mintTotalhr < CInt(drDayName(0)("halffromhrsinsec")) Then
                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.
                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                            mdblDayType = 1
                            End If
                            'Pinkal (07-Sep-2017) -- End

                            'Pinkal (24-Jan-2017) -- Start
                            'Enhancement -  0001879: Count short time before (mins) defined on any shift does not reflect / work on employee time sheet.
                            'mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                            mintShorthr = CInt(drDayName(0)("workinghrsinsec")) - mintTotalhr
                            End If
                            'Pinkal (24-Jan-2017) -- End

                            mdctFinalOTOrder.Item("OT1") = 0

                            'FOR OVERTIME
                        ElseIf mintTotalhr > (CInt(drDayName(0)("workinghrsinsec")) + CInt(drDayName(0)("calcovertimeafterinsec"))) Then
                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.
                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End
                            Else
                            mdblDayType = 1
                            End If
                            'Pinkal (07-Sep-2017) -- End


                            'Pinkal (12-Oct-2017) -- Start
                            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).

                            If mblnCountOTonTotalWorkedHrs Then
                            mdctFinalOTOrder.Item("OT1") = mintTotalhr - (CInt(drDayName(0)("workinghrsinsec")) + CInt(drDayName(0)("calcovertimeafterinsec")))
                            Else

                                'Pinkal (03-Aug-2018) -- Start
                                'Enhancement - Changes For B5 [Ref #289]
                                If mintCountOTMinsAfterShiftEndTime <= 0 Then
                                If DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime) >= CInt(drDayName(0)("calcovertimeafterinsec")) Then
                                    mdctFinalOTOrder.Item("OT1") = DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime) - CInt(drDayName(0)("calcovertimeafterinsec"))
                                Else
                                    mdctFinalOTOrder.Item("OT1") = 0
                                End If
                                ElseIf mintCountOTMinsAfterShiftEndTime > 0 Then
                                    If (mintCountOTMinsAfterShiftEndTime / 60) < DateDiff(DateInterval.Minute, mdtShiftOutTime, mdtCheckouttime) Then
                                        mdctFinalOTOrder.Item("OT1") = DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime)
                                    ElseIf CInt(drDayName(0)("calcovertimeafterinsec")) > 0 Then
                                        If DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime) > CInt(drDayName(0)("calcovertimeafterinsec")) Then
                                            mdctFinalOTOrder.Item("OT1") = CInt(drDayName(0)("calcovertimeafterinsec")) - DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime)
                                        Else
                                            mdctFinalOTOrder.Item("OT1") = 0
                                        End If
                                    Else
                                        mdctFinalOTOrder.Item("OT1") = 0
                                    End If
                                End If
                                'Pinkal (03-Aug-2018) -- End

                            End If
                            'Pinkal (12-Oct-2017) -- End


                            mintShorthr = 0

                        ElseIf mintTotalhr <= (CInt(drDayName(0)("workinghrsinsec")) + CInt(drDayName(0)("calcovertimeafterinsec"))) Then
                            'Pinkal (07-Sep-2017) -- Start
                            'Enhancement - Working on B5 Plus Enhancement.

                            If mblnIsCountOnlyShiftTime Then
                                'Pinkal (25-Oct-2017) -- Start
                                'Enhancement - Change for B5 Plus As Per Annor Urgent Requirement .
                                If CInt(drDayName(0)("calcshorttimebeforeinsec")) < DateDiff(DateInterval.Second, mdtCheckouttime, mdtShiftOutTime) Then
                                    mintShorthr = DateDiff(DateInterval.Second, mdtCheckouttime, mdtShiftOutTime)
                                Else
                                    mintShorthr = 0
                                End If
                                'Pinkal (25-Oct-2017) -- End

                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), objDataOperation)
                                mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, mdtShiftOutTime, CInt(drDayName(0)("workinghrsinsec")), CInt(drDayName(0)("halffromhrsinsec")), CInt(drDayName(0)("halftohrsinsec")), CInt(drDayName(0)("calcshorttimebeforeinsec")), mblnIsOpenShift, objDataOperation)
                                'Pinkal (28-Nov-2019) -- End

                            Else
                            mdblDayType = 1
                                'Pinkal (25-Oct-2017) -- Start
                                'Enhancement - Change for B5 Plus As Per Annor Urgent Requirement .
                                mintShorthr = 0
                                'Pinkal (25-Oct-2017) -- End
                            End If
                            'Pinkal (07-Sep-2017) -- End


                        End If


                    End If  ' END FOR ConfigParameter._Object._PolicyManagementTNA


                    'FOR NIGHT HRS

                    If blnPolicyManagementTNA = False Then


                        'Pinkal (07-Sep-2017) -- Start
                        'Enhancement - Working on B5 Plus Enhancement.

                        '    'Pinkal (03-AUG-2015) -- Start
                        '    'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
                        '    If mdtcheckintime.Date <> Nothing AndAlso (CDate(drDayName(0)("nightfromhrs")) <> Nothing AndAlso CDate(drDayName(0)("nighttohrs")) <> Nothing) Then
                        '        'If  (CDate(drDayName(0)("nightfromhrs")) <> Nothing AndAlso CDate(drDayName(0)("nighttohrs")) <> Nothing) Then
                        '        'Pinkal (03-AUG-2015) -- End

                        '        If DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), CDate(drDayName(0)("nighttohrs")).AddDays(1)) > 0 AndAlso CDate(drDayName(0)("nightfromhrs")) <> CDate(drDayName(0)("nighttohrs")) Then

                        '            drDayName(0)("nightfromhrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString

                        '            If CDate(drDayName(0)("nighttohrs")).ToShortTimeString.Contains("AM") Then
                        '                drDayName(0)("nighttohrs") = mdtcheckintime.Date.AddDays(1) & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                        '            Else
                        '                drDayName(0)("nighttohrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                        '            End If

                        '            'Pinkal (09-Jun-2015) -- Start
                        '            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                        '            If mdtcheckintime <> Nothing AndAlso mdtCheckouttime <> Nothing Then
                        '                If mdtcheckintime <= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) Then
                        '                    mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), CDate(drDayName(0)("nighttohrs")))
                        '                ElseIf mdtcheckintime <= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime <= CDate(drDayName(0)("nighttohrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nightfromhrs")) Then
                        '                    mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), mdtCheckouttime)
                        '                ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime <= CDate(drDayName(0)("nighttohrs")) Then
                        '                    mintTotalNighthrs = DateDiff(DateInterval.Second, mdtcheckintime, mdtCheckouttime)

                        '                    'Pinkal (03-AUG-2015) -- Start
                        '                    'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
                        '                ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) Then
                        '                    mintTotalNighthrs = DateDiff(DateInterval.Second, mdtcheckintime, CDate(drDayName(0)("nighttohrs")))
                        '                    'Pinkal (03-AUG-2015) -- End
                        '                End If
                        '            End If
                        '            'Pinkal (09-Jun-2015) -- End

                        '            'START FOR WHEN EMPLOYEE DIRECTLY LOGIN TO NIGHT HRS THEN NO NEED TO CALCULATE SHORT HRS OR OVERTIME HR

                        '            'strQ = "Select Count(*) From tnalogin_tran WHERE Convert(char(8),logindate,112) = @logindate AND employeeunkid = @employeeunkid  "
                        '            'objDataOperation.ClearParameters()
                        '            'objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLogindate))
                        '            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        '            'Dim icnt As Integer = objDataOperation.RecordCount(strQ)
                        '            'If icnt <= 1 Then
                        '            '    mintShorthr = 0
                        '            'End If

                        '            'END FOR WHEN EMPLOYEE DIRECTLY LOGIN TO NIGHT HRS THEN NO NEED TO CALCULATE SHORT HRS OR OVERTIME HR

                        '        End If

                        '    End If

                        CalculateNightHrs(drDayName)

                        'Pinkal (07-Sep-2017) -- End

                    End If  ' END FOR ConfigParameter._Object._PolicyManagementTNA


                End If  ' END FOR ConfigParameter._Object._PolicyManagementTNA


                'Pinkal (06-May-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                'START FOR SET LATE COMING WITHOUT GRACE. 


                'Pinkal (11-Nov-2016) -- Start
                'Enhancement - Error Solved for KBC WHEN OPEN SHIFT IS TRUE AND ATTENDANCE ON DEVICE IO STATUS IS FALSE.

                If objshift._IsOpenShift = False Then
                If DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime) > 0 Then
                    mintLateComing_WOGrace = DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime)
                End If

                If mdtRoundoffInTime <> Nothing AndAlso mintltcome_graceinsec > 0 Then

                    If mdtShiftInTime < mdtcheckintime AndAlso DateDiff(DateInterval.Minute, mdtShiftInTime, mdtcheckintime) > (mintltcome_graceinsec / 60) _
                                    AndAlso DateDiff(DateInterval.Minute, mdtShiftInTime, mdtcheckintime) > 0 Then
                        mintLateComing_AfterGrace = DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime) - mintltcome_graceinsec
                    End If
                End If

                    'Pinkal (13-Sep-2021)-- Start
                    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    If mblnIsWeekend = True OrElse mblnIsholiday = True OrElse mblnIsDayOffshift = True Then
                        mintLateComing_WOGrace = 0
                        mintLateComing_AfterGrace = 0
                    End If
                    'Pinkal (13-Sep-2021)-- End

                End If

                'Pinkal (11-Nov-2016) -- End



                If mintPolicyId > 0 And blnPolicyManagementTNA Then   'START mintPolicyId > 0
                    mdctFinalOTPenalty = New Dictionary(Of String, Integer)

                    'Pinkal (07-Sep-2017) -- Start
                    'Enhancement - Working on B5 Plus Enhancement.

                    'mdctFinalOTPenalty.Add("OT1_Penalty", 0)
                    'mdctFinalOTPenalty.Add("OT2_Penalty", 0)
                    'mdctFinalOTPenalty.Add("OT3_Penalty", 0)
                    'mdctFinalOTPenalty.Add("OT4_Penalty", 0)

                    'Dim objOTPenalty As New clsOT_Penalty
                    'Dim dsOtPenaltyList As DataSet = objOTPenalty.GetList("List", True, mintPolicyId)
                    'If dsOtPenaltyList IsNot Nothing AndAlso dsOtPenaltyList.Tables(0).Rows.Count > 0 Then  'START dsOtPenaltyList IsNot Nothing AndAlso dsOtPenaltyList.Tables(0).Rows.Count > 0
                    '    Dim drRow() As DataRow = Nothing

                    '    Dim mintMaxPenlatyMins As Integer = CInt(IIf(IsDBNull(dsOtPenaltyList.Tables(0).Compute("Max(uptominsinsec)", "1=1")), 0, dsOtPenaltyList.Tables(0).Compute("Max(uptominsinsec)", "1=1")))

                    '    If mintltcome_graceinsec > 0 AndAlso mintLateComing_AfterGrace > 0 Then
                    '        drRow = dsOtPenaltyList.Tables(0).Select("uptominsinsec >= " & mintLateComing_AfterGrace)
                    '        If drRow.Length <= 0 AndAlso mintMaxPenlatyMins < mintLateComing_AfterGrace Then
                    '            drRow = dsOtPenaltyList.Tables(0).Select("Uptominsinsec = " & mintMaxPenlatyMins)
                    '        End If
                    '    ElseIf mintltcome_graceinsec > 0 AndAlso mintLateComing_WOGrace > 0 Then
                    '        drRow = dsOtPenaltyList.Tables(0).Select("uptominsinsec >= " & mintLateComing_WOGrace)
                    '        If drRow.Length <= 0 AndAlso mintMaxPenlatyMins < mintLateComing_WOGrace Then
                    '            drRow = dsOtPenaltyList.Tables(0).Select("Uptominsinsec = " & mintMaxPenlatyMins)
                    '        End If
                    '    End If

                    '    If drRow.Length > 0 Then  'START  drRow.Length > 0

                    '        If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString())) > 0 AndAlso _
                    '          CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '          mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(1).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec"))

                    '        ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString())) > 0 AndAlso _
                    '                CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '                CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(1).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()))
                    '        End If

                    '        If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) > 0 AndAlso _
                    '            CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '            CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(2).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec"))

                    '        ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) > 0 AndAlso _
                    '                 CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '                 CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(2).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()))
                    '        End If

                    '        If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) > 0 AndAlso _
                    '           CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '           CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(3).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec"))

                    '        ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) > 0 AndAlso _
                    '                 CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '                 CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(3).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()))
                    '        End If

                    '        If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) > 0 AndAlso _
                    '          CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '          CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(4).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec"))

                    '        ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) > 0 AndAlso _
                    '                CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                    '                CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) Then

                    '            mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(4).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()))
                    '        End If

                    '    End If  ' 'END  drRow.Length > 0

                    'End If  ''END dsOtPenaltyList IsNot Nothing AndAlso dsOtPenaltyList.Tables(0).Rows.Count > 0

                    CalculateOTPenalty(mdctOTOrder, mdctFinalOTPenalty, mintltcome_graceinsec)

                    'Pinkal (07-Sep-2017) -- End

                End If    'END mintPolicyId > 0

                'END FOR SET LATE COMING WITHOUT GRACE.
                'Pinkal (06-May-2016) -- End

				'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.

                'If objshift._IsOpenShift = False Then



                    '    Dim dsTimesheet As DataSet = GetList(objDataOperation, "List", True)
                    '    Dim drTimesheet() As DataRow = dsTimesheet.Tables(0).Select("loginunkid <> " & mintLoginunkid & " AND loginunkid = MIN(loginunkid)")
                    '    If drTimesheet.Length <= 0 Then

                    '        If mdtRoundoffInTime = Nothing Then

                    '            If mdtcheckintime <= mdtShiftInTime Then    'Early Coming   
                    '                mintearly_coming = DateDiff(DateInterval.Second, mdtcheckintime, mdtShiftInTime)
                    '            ElseIf mdtcheckintime >= mdtShiftInTime Then       'Late Coming  
                    '                mintlate_coming = DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime)
                    '            End If
                    '        Else

                    '            If mdtRoundoffInTime <= mdtShiftInTime Then    'Early Coming   
                    '                mintearly_coming = DateDiff(DateInterval.Second, mdtRoundoffInTime, mdtShiftInTime)
                    '            ElseIf mdtRoundoffInTime >= mdtShiftInTime Then       'Late Coming  
                    '                mintlate_coming = DateDiff(DateInterval.Second, mdtShiftInTime, mdtRoundoffInTime)
                    '            End If

                    '        End If
                    '    Else

                    '        If IsDBNull(drTimesheet(0)("roundoff_intime")) Then

                    '            If Not IsDBNull(drTimesheet(0)("checkintime")) AndAlso CDate(drTimesheet(0)("checkintime")) <= mdtShiftInTime Then    'Early Coming
                    '                mintearly_coming = DateDiff(DateInterval.Second, CDate(drTimesheet(0)("checkintime")), mdtShiftInTime)
                    '            ElseIf Not IsDBNull(drTimesheet(0)("checkintime")) AndAlso CDate(drTimesheet(0)("checkintime")) >= mdtShiftInTime Then       'Late Coming
                    '                mintlate_coming = DateDiff(DateInterval.Second, mdtShiftInTime, CDate(drTimesheet(0)("checkintime")))
                    '            End If


                    '        ElseIf Not IsDBNull(drTimesheet(0)("roundoff_intime")) Then

                    '            If Not IsDBNull(drTimesheet(0)("roundoff_intime")) AndAlso CDate(drTimesheet(0)("roundoff_intime")) <= mdtShiftInTime Then    'Early Coming
                    '                mintearly_coming = DateDiff(DateInterval.Second, CDate(drTimesheet(0)("roundoff_intime")), mdtShiftInTime)
                    '            ElseIf Not IsDBNull(drTimesheet(0)("roundoff_intime")) AndAlso CDate(drTimesheet(0)("roundoff_intime")) >= mdtShiftInTime Then       'Late Coming
                    '                mintlate_coming = DateDiff(DateInterval.Second, mdtShiftInTime, CDate(drTimesheet(0)("roundoff_intime")))
                    '            End If

                    '        End If

                    '    End If


                    '    If mdtRoundoffOutTime = Nothing Then

                    '        If mdtCheckouttime <> Nothing AndAlso mdtCheckouttime <= mdtShiftOutTime Then    'Early Going   --
                    '            mintearly_going = DateDiff(DateInterval.Second, mdtCheckouttime, mdtShiftOutTime)
                    '        ElseIf mdtCheckouttime <> Nothing AndAlso mdtCheckouttime >= mdtShiftOutTime Then       'Late Going  --
                    '            mintlate_going = DateDiff(DateInterval.Second, mdtShiftOutTime, mdtCheckouttime)
                    '        End If

                    '    Else
                    '        If mdtRoundoffOutTime <= mdtShiftOutTime Then    'Early Going   --
                    '            mintearly_going = DateDiff(DateInterval.Second, mdtRoundoffOutTime, mdtShiftOutTime)
                    '        ElseIf mdtRoundoffOutTime >= mdtShiftOutTime Then       'Late Going  --
                    '            mintlate_going = DateDiff(DateInterval.Second, mdtShiftOutTime, mdtRoundoffOutTime)
                    '        End If

                    '    End If

                    'CalculateEarlyLate_ComingGoing(mdtShiftInTime, mdtShiftOutTime)

                    'Pinkal (07-Sep-2017) -- End

                'End If ' END FOR ISOPEN SHIFT

            ElseIf mintLoginunkid > 0 AndAlso mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then

                If objshift._IsOpenShift = False Then

                    CalculateEarlyLate_ComingGoing(objDataOperation, mdtShiftInTime, mdtShiftOutTime)

                    If DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime) > 0 Then
                        mintLateComing_WOGrace = DateDiff(DateInterval.Second, mdtShiftInTime, mdtcheckintime)
                    End If

                    If mblnIsWeekend = True OrElse mblnIsholiday = True OrElse mblnIsDayOffshift = True Then
                        mintLateComing_WOGrace = 0
                        mintLateComing_AfterGrace = 0
                    End If


                    'Pinkal (02-Feb-2022)-- Start
                    'Problem Solving for COLAS.
                    If mintTotalhr <= 0 Then
                        Dim dsTimesheet As DataSet = GetList(objDataOperation, "List", True)
                        If dsTimesheet IsNot Nothing AndAlso dsTimesheet.Tables(0).Rows.Count > 0 AndAlso mdtLogindate <> Nothing Then
                            mintTotalhr = dsTimesheet.Tables(0).Compute("SUM(Workhour)", "loginDate = '" & mdtLogindate.Date & "' AND employeeunkid = " & mintEmployeeunkid)
                        End If
                        If mdecDayFraction > 0 Then
                            mintearly_going = 0
                            mintlate_coming = 0
                        End If
                    End If
                    'Pinkal (02-Feb-2022) -- End

                End If

            End If   ' END FOR mintTotalhr > 0 


            ' END CHECK FOR HALF DAY AND FULL DAY

InsertPoint:
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)
            objDataOperation.AddParameter("@login_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            objDataOperation.AddParameter("@day_type", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDayType)
            objDataOperation.AddParameter("@total_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalhr.ToString)
            objDataOperation.AddParameter("@ispaidleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspaidLeave.ToString)
            objDataOperation.AddParameter("@isunpaidleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsUnpaidLeave.ToString)
            objDataOperation.AddParameter("@isabsentprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAbsentProcess.ToString)
            objDataOperation.AddParameter("@total_short_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShorthr.ToString)
            objDataOperation.AddParameter("@total_nighthrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalNighthrs.ToString)
            objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeekend.ToString)
            objDataOperation.AddParameter("@isholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsholiday.ToString)
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyId.ToString)
            objDataOperation.AddParameter("@total_overtime", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTOrder.Item("OT1")))
            objDataOperation.AddParameter("@ot2", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTOrder.Item("OT2")))
            objDataOperation.AddParameter("@ot3", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTOrder.Item("OT3")))
            objDataOperation.AddParameter("@ot4", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTOrder.Item("OT4")))
            objDataOperation.AddParameter("@elrcoming_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, mintearly_coming)
            objDataOperation.AddParameter("@ltcoming_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlate_coming)
            objDataOperation.AddParameter("@elrgoing_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, mintearly_going)
            objDataOperation.AddParameter("@ltgoing_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlate_going)
            objDataOperation.AddParameter("@isdayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDayOffshift)
            objDataOperation.AddParameter("@lvdayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDayFraction)


            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            objDataOperation.AddParameter("@LateComing_WOGrace", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLateComing_WOGrace)
            objDataOperation.AddParameter("@LateComing_AfterGrace", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLateComing_AfterGrace)
            If mdctFinalOTPenalty IsNot Nothing Then
                objDataOperation.AddParameter("@ot1_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTPenalty.Item("OT1_Penalty")))
                objDataOperation.AddParameter("@ot2_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTPenalty.Item("OT2_Penalty")))
                objDataOperation.AddParameter("@ot3_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTPenalty.Item("OT3_Penalty")))
                objDataOperation.AddParameter("@ot4_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(mdctFinalOTPenalty.Item("OT4_Penalty")))
            Else
                objDataOperation.AddParameter("@ot1_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                objDataOperation.AddParameter("@ot2_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                objDataOperation.AddParameter("@ot3_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                objDataOperation.AddParameter("@ot4_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            End If
            'Pinkal (06-May-2016) -- End


            'Pinkal (25-AUG-2017) -- Start
            'Enhancement - Working on B5 plus TnA Enhancement.
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveTypeId)
            'Pinkal (25-AUG-2017) -- End


            If count = 0 Then


ForMultiShift:

                strQ = "INSERT INTO tnalogin_summary ( " & _
             "  employeeunkid " & _
             ", shiftunkid " & _
             ", login_date " & _
             ", day_type " & _
             ", total_hrs " & _
             ", ispaidleave " & _
             ", isunpaidleave " & _
             ", isabsentprocess " & _
             ", total_overtime " & _
             ", total_short_hrs" & _
                           ", total_nighthrs" & _
                           ", isweekend" & _
                           ", isholiday" & _
                         ", ot2 " & _
                         ", ot3 " & _
                         ", ot4 " & _
                         ", elrcoming_grace " & _
                         ", ltcoming_grace " & _
                         ", elrgoing_grace " & _
                         ", ltgoing_grace " & _
                         ", policyunkid " & _
                         ", isdayoff " & _
                         ", lvdayfraction " & _
                         ", ltcoming " & _
                         ", ltcoming_aftrgrace " & _
                         ", ot1_penalty " & _
                         ", ot2_penalty " & _
                         ", ot3_penalty " & _
                         ", ot4_penalty " & _
                         ", dayid " & _
                         ", leavetypeunkid " & _
                         " ) VALUES (" & _
             "  @empunkid " & _
             ", @shiftunkid " & _
             ", @login_date " & _
             ", @day_type " & _
             ", @total_hrs " & _
             ", @ispaidleave " & _
             ", @isunpaidleave " & _
             ", @isabsentprocess " & _
             ", @total_overtime " & _
             ", @total_short_hrs" & _
                           ", @total_nighthrs" & _
                           ", @isweekend" & _
                           ", @isholiday" & _
                         ", @ot2 " & _
                         ", @ot3 " & _
                         ", @ot4 " & _
                         ", @elrcoming_grace " & _
                         ", @ltcoming_grace " & _
                         ", @elrgoing_grace " & _
                         ", @ltgoing_grace " & _
                         ", @policyunkid " & _
                         ", @isdayoff " & _
                         ", @lvdayfraction " & _
                         ", @LateComing_WOGrace " & _
                         ", @LateComing_AfterGrace " & _
                         ", @ot1_penalty " & _
                         ", @ot2_penalty " & _
                         ", @ot3_penalty " & _
                         ", @ot4_penalty " & _
                         ", (DATEPART(WEEKDAY,@login_date)) -1 " & _
                         ", @leavetypeunkid " & _
                         " ); SELECT @@identity"


                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .[ ", DATEPART(WEEKDAY,@login_date) -1 " & _]


                'Pinkal (25-AUG-2017) --  'Enhancement - Working on B5 plus TnA Enhancement.[@leavetypeunkid]

                dsList = Nothing
                dsList = objDataOperation.ExecQuery(strQ, "InsertLoginSummary")

            ElseIf count > 0 Then

                If blnPolicyManagementTNA = False Then
                    If intShiftID <> mintShiftunkid Then
                        GoTo ForMultiShift
                    End If
                End If

                objDataOperation.AddParameter("@loginsummaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginsummaryunkid.ToString)

                strQ = "UPDATE tnalogin_summary SET " & _
                            "  employeeunkid = @empunkid" & _
                            ", shiftunkid = @shiftunkid" & _
                            ", login_date = @login_date" & _
                            ", day_type = @day_type" & _
                            ", total_hrs = @total_hrs" & _
                            ", ispaidleave = @ispaidleave" & _
                            ", isunpaidleave = @isunpaidleave" & _
                            ", isabsentprocess = @isabsentprocess " & _
                            ", total_overtime = @total_overtime" & _
                            ", total_short_hrs = @total_short_hrs " & _
                           ",total_nighthrs =  @total_nighthrs " & _
                           ",isweekend =  @isweekend " & _
                           ",isholiday =  @isholiday " & _
                            ", ot2 = @ot2 " & _
                            ", ot3 = @ot3 " & _
                            ", ot4 = @ot4 " & _
                            ", elrcoming_grace = @elrcoming_grace " & _
                            ", ltcoming_grace = @ltcoming_grace " & _
                            ", elrgoing_grace = @elrgoing_grace " & _
                            ", ltgoing_grace = @ltgoing_grace " & _
                            ", policyunkid = @policyunkid " & _
                            ", isdayoff = @isdayoff " & _
                            ", lvdayfraction = @lvdayfraction " & _
                            ", ltcoming = @LateComing_WOGrace " & _
                            ", ltcoming_aftrgrace = @LateComing_AfterGrace " & _
                            ", ot1_penalty = @ot1_penalty " & _
                            ", ot2_penalty = @ot2_penalty " & _
                            ", ot3_penalty = @ot3_penalty " & _
                            ", ot4_penalty = @ot4_penalty " & _
                            ", dayid = (DATEPART(WEEKDAY,@login_date)) -1 " & _
                            ", leavetypeunkid = @leavetypeunkid " & _
                            " WHERE loginsummaryunkid = @loginsummaryunkid "


                'Pinkal (18-Jul-2017) --  'Enhancement - TnA Enhancement for B5 Plus Company[ , dayid = DATEPART(WEEKDAY,@login_date) -1 " & _] .

                'Pinkal (25-AUG-2017) -- 'Enhancement - Working on B5 plus TnA Enhancement.[", leavetypeunkid = @leavetypeunkid " & _]

                objDataOperation.ExecNonQuery(strQ)

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertLoginSummary; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Delete Database Table (tnaLogin_Summary) </purpose>
    Public Sub DeleteLoginSummary(ByVal objDataOperation As clsDataOperation, ByVal intLoginsummryid As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "Delete From tnalogin_summary where loginsummaryunkid=@loginsummaryunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginsummaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoginsummryid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteLoginSummary; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeDayWorked(ByVal intEmployeeID As Integer, ByVal intMoth As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "select count(*) 'DayWorked' from tnalogin_summary where isnull(isunpaidleave,0) = 0 and employeeunkid = @employeeunkid AND DATEPART(MM,login_date) = @month"
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@month", SqlDbType.Int, eZeeDataType.INT_SIZE, intMoth.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "DayWorked")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("DayWorked"))
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeDayWorked; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLoginSummaryForAbsent(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            ' START CHECK FOR LOGIN SUMMARY IF RECORED EXIST FOR SPECIFIC EMPLOYEE AND SPECIFIC LOGIN DATE

            objDataOperation.ClearParameters()

            strQ = "select count(*) 'Rcount',isnull(loginsummaryunkid,0) 'loginsummaryunkid' from tnalogin_summary " & _
                   " where employeeunkid = @empid AND convert(char(10),login_date,101) = @logdate group by loginsummaryunkid"
            objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            dsList = objDataOperation.ExecQuery(strQ, "CountLoginSummary")

            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                mintLoginsummaryunkid = CInt(dsList.Tables(0).Rows(0)("loginsummaryunkid"))
            End If
            ' END CHECK FOR LOGIN SUMMARY IF RECORED EXIST FOR SPECIFIC EMPLOYEE AND SPECIFIC LOGIN DATE

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginsummaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginsummaryunkid.ToString)
            objDataOperation.AddParameter("@isabsentprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAbsentProcess)
            objDataOperation.AddParameter("@ispaidleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspaidLeave)
            objDataOperation.AddParameter("@isunpaidleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsUnpaidLeave)
            objDataOperation.AddParameter("@isdayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDayOffshift)
            objDataOperation.AddParameter("@isholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsholiday)
            objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeekend)
            objDataOperation.AddParameter("@lvdayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDayFraction)
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyId)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid)


            'Pinkal (25-AUG-2017) -- Start
            'Enhancement - Working on B5 plus TnA Enhancement.
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mintLeaveTypeId)


            'strQ = " UPDATE tnalogin_summary SET " & _
            '          " isabsentprocess = @isabsentprocess, ispaidleave = @ispaidleave,isunpaidleave= @isunpaidleave ,isdayoff = @isdayoff,isholiday=@isholiday,isweekend = @isweekend " & _
            '          " ,lvdayfraction=@lvdayfraction, policyunkid = @policyunkid ,shiftunkid = @shiftunkid" & _
            '          " WHERE loginsummaryunkid = @loginsummaryunkid "


            strQ = " UPDATE tnalogin_summary SET " & _
                      " isabsentprocess = @isabsentprocess, ispaidleave = @ispaidleave,isunpaidleave= @isunpaidleave ,isdayoff = @isdayoff,isholiday=@isholiday,isweekend = @isweekend " & _
                      " ,lvdayfraction=@lvdayfraction, policyunkid = @policyunkid ,shiftunkid = @shiftunkid, leavetypeunkid = @leavetypeunkid " & _
                      " WHERE loginsummaryunkid = @loginsummaryunkid "

            'Pinkal (25-AUG-2017) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoginSummaryForAbsent; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Check whether absent process is done or not for the given period
    ''' Modify By : Sohail
    ''' </summary>
    ''' <param name="dtStartDate"></param>
    ''' <param name="dtEndDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Is_Absent_Processed(ByVal dtStartDate As Date, ByVal dtEndDate As Date) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT  COUNT(isabsentprocess) AS TotalCount " & _
                    "FROM    tnalogin_summary " & _
                    "WHERE   isabsentprocess = 0 " & _
                            "AND login_date BETWEEN @StartDate " & _
                                           "AND     @EndDate "

            objDataOperation.AddParameter("@StartDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtStartDate)
            objDataOperation.AddParameter("@EndDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtEndDate)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return CInt(dsList.Tables("List").Rows(0).Item("TotalCount").ToString) = 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_Absent_Processed; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' LOGIN SUMMARY WILL SHOW LESS THAN THE ORIGINAL WORKING HOURS THAT OF TRAN TABLE. 
    ' REASON : THIS WILL DEDUCT THE TOTAL WORKING  WITH THE POLICY BREAK TIME. 
    ' DONE FOR OMAN [VOLTAMP GROUP]

    Public Function GetEmployeeTotalWorkHrFromPolicy(ByVal objDataOperation As clsDataOperation, ByVal drPolicy() As DataRow, ByVal blnDonotAttendanceinSeconds As Boolean) As Integer 'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END
        'Public Function GetEmployeeTotalWorkHrFromPolicy(ByVal objDataOperation As clsDataOperation, ByVal drPolicy() As DataRow) As Integer

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim iFinalBreakTime As Integer = 0
        Dim intWorkhr As Integer = 0
        Try

            If drPolicy(0).Item("countbreaktimeaftersec") > 0 Then
                If mintTotalhr > drPolicy(0).Item("countbreaktimeaftersec") Then

                    iFinalBreakTime = drPolicy(0).Item("breaktimeinsec")

                    If ConfigParameter._Object._FirstCheckInLastCheckOut = True Then
                        strQ = "SELECT loginunkid,workhour,breakhr FROM tnalogin_tran WHERE employeeunkid = '" & mintEmployeeunkid & "' AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND isvoid = 0 "
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        Dim mintMaxHourInSec As Integer = CInt(dsList.Tables(0).Compute("Max(workhour)", ""))
                        Dim mintMaxLoginID As Integer = CInt(dsList.Tables(0).Compute("Max(loginunkid)", "workhour=" & mintMaxHourInSec))

                        If dsList.Tables(0).Rows.Count > 0 AndAlso mintMaxHourInSec > iFinalBreakTime Then

                            'S.SANDEEP [ 20 JAN 2014 ] -- START
                            'strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime), " & _
                            '       "breakhr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                            '       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND breakhr > 0 "
                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                'Pinkal (25-AUG-2014) -- Start
                                'Enhancement - VOLTMAP MANUAL ROUNDING ISSUE FOR VTO ON (23-AUG-2014)

                                'strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,roundoff_intime,roundoff_outtime), " & _
                                '   "breakhr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                '   "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND breakhr > 0 AND isvoid = 0 "

                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END), " & _
                                   "breakhr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                   "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND breakhr > 0 AND isvoid = 0 "

                                'Pinkal (25-AUG-2014) -- End


                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime), " & _
                                       "breakhr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND breakhr > 0 AND isvoid = 0 "
                            End If
                            'S.SANDEEP [ 20 JAN 2014 ] -- END

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            'S.SANDEEP [ 20 JAN 2014 ] -- START
                            'strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime) - " & iFinalBreakTime & ", " & _
                            '       "breakhr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                            '       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND breakhr <= 0 " & _
                            '       "AND loginunkid = '" & mintMaxLoginID & "' "

                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                'Pinkal (25-AUG-2014) -- Start
                                'Enhancement - VOLTMAP MANUAL ROUNDING ISSUE FOR VTO ON (23-AUG-2014)

                                'strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,roundoff_intime,roundoff_outtime) - " & iFinalBreakTime & ", " & _
                                '       "breakhr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                '       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND breakhr <= 0 AND isvoid = 0 " & _
                                '       "AND loginunkid = '" & mintMaxLoginID & "' "

                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END) - " & iFinalBreakTime & ", " & _
                                       "breakhr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND breakhr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "


                                'Pinkal (25-AUG-2014) -- End

                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime) - " & iFinalBreakTime & ", " & _
                                       "breakhr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND breakhr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "
                            End If
                            'S.SANDEEP [ 20 JAN 2014 ] -- END



                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            _Loginunkid(objDataOperation) = mintMaxLoginID

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If


                            strQ = "select isnull(sum(workhour),0) 'workhour' from tnalogin_tran where employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1 "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@Emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                            objDataOperation.AddParameter("@date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
                            dsList = objDataOperation.ExecQuery(strQ, "CheckWorkHour")

                            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                                intWorkhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                            End If

                        End If

                    End If

                End If

            End If


            'Pinkal (28-Jan-2014) -- Start
            'Enhancement : Oman Changes

            If intWorkhr <= 0 Then intWorkhr = mintTotalhr

            iFinalBreakTime = 0
            If drPolicy(0).Item("countteatimeaftersec") > 0 Then

                If intWorkhr > drPolicy(0).Item("countteatimeaftersec") Then

                    iFinalBreakTime = drPolicy(0).Item("teatimeinsec")

                    If ConfigParameter._Object._FirstCheckInLastCheckOut = True Then
                        strQ = "SELECT loginunkid,workhour,breakhr FROM tnalogin_tran WHERE employeeunkid = '" & mintEmployeeunkid & "' AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND isvoid = 0 "
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        Dim mintMaxHourInSec As Integer = CInt(dsList.Tables(0).Compute("Max(workhour)", ""))
                        Dim mintMaxLoginID As Integer = CInt(dsList.Tables(0).Compute("Max(loginunkid)", "workhour=" & mintMaxHourInSec))

                        If dsList.Tables(0).Rows.Count > 0 AndAlso mintMaxHourInSec > iFinalBreakTime Then

                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                'Pinkal (25-AUG-2014) -- Start
                                'Enhancement - VOLTMAP MANUAL ROUNDING ISSUE FOR VTO ON (23-AUG-2014)

                                'strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,roundoff_intime,roundoff_outtime), " & _
                                '   "teahr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                '   "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND teahr > 0 AND isvoid = 0 "

                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END ), " & _
                                   "teahr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                   "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND teahr > 0 AND isvoid = 0 "

                                'Pinkal (25-AUG-2014) -- End

                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime), " & _
                                       "teahr = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND teahr > 0 AND isvoid = 0 "
                            End If

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            If _Roundoff_Intime <> Nothing AndAlso _Roundoff_Outtime <> Nothing Then

                                'Pinkal (25-AUG-2014) -- Start
                                'Enhancement - VOLTMAP MANUAL ROUNDING ISSUE FOR VTO ON (23-AUG-2014)

                                'strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,roundoff_intime,roundoff_outtime) - ISNULL(breakhr,0) - " & iFinalBreakTime & ", " & _
                                '       "teahr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                '       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND teahr <= 0 AND isvoid = 0 " & _
                                '       "AND loginunkid = '" & mintMaxLoginID & "' "

                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,CASE WHEN roundoff_intime IS NULL THEN checkintime ELSE roundoff_intime END ,CASE WHEN  roundoff_outtime IS NULL THEN checkouttime ELSE roundoff_outtime END ) - ISNULL(breakhr,0) - " & _
                                          iFinalBreakTime & ",  teahr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND teahr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "

                                'Pinkal (25-AUG-2014) -- End

                            ElseIf _checkintime <> Nothing AndAlso _Checkouttime <> Nothing Then
                                strQ = "UPDATE tnalogin_tran SET workhour =  DATEDIFF(ss,checkintime,checkouttime) - ISNULL(breakhr,0) -  " & iFinalBreakTime & ", " & _
                                       "teahr = '" & iFinalBreakTime & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' " & _
                                       "AND CONVERT(CHAR(8),logindate,112) = '" & eZeeDate.convertDate(mdtLogindate) & "' AND workhour > 0 AND teahr <= 0 AND isvoid = 0 " & _
                                       "AND loginunkid = '" & mintMaxLoginID & "' "
                            End If


                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            _Loginunkid(objDataOperation) = mintMaxLoginID

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            If InsertAuditTrailLogin(objDataOperation, 2, blnDonotAttendanceinSeconds) = False Then
                                'If InsertAuditTrailLogin(objDataOperation, 2) = False Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If


                            strQ = "select isnull(sum(workhour),0) 'workhour' from tnalogin_tran where employeeunkid = @Emp_id AND convert(char(10),logindate,101) = @date and isvoid = 0 AND inouttype = 1 "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@Emp_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                            objDataOperation.AddParameter("@date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
                            dsList = objDataOperation.ExecQuery(strQ, "CheckWorkHour")

                            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                                intWorkhr = CInt(dsList.Tables(0).Rows(0)("workhour"))
                            End If

                        End If

                    End If

                End If

            End If

            'Pinkal (28-Jan-2014) -- End


            If intWorkhr <= 0 Then intWorkhr = mintTotalhr

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeTotalWorkHrFromPolicy", mstrModuleName)
        End Try
        Return intWorkhr
    End Function

    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .

    Private Sub CalculatePolicyMgmtWorkedHrs(ByVal mintFinalWorkingHrs As Integer, ByVal mdctOTOrder As Dictionary(Of Integer, String) _
                                                                   , ByVal drPolicy() As DataRow, ByVal drDayName() As DataRow, ByVal mblnIsCountOnlyShiftTime As Boolean _
                                                                   , ByVal xShiftOutTime As DateTime, ByVal objDataOperation As clsDataOperation, ByVal blnIsOpenShift As Boolean)

        'Pinkal (28-Nov-2019) -- Defect Hill Packaging - Calculating Day Type wrongly.[ ByVal blnIsOpenShift As Boolean]

        'Pinkal (07-Sep-2017) --  Working on B5 Plus Enhancement.[ ByVal mblnIsCountOnlyShiftTime As Boolean, ByVal xShiftOutTime As DateTime]

        Try
            If mintTotalhr < mintFinalWorkingHrs Then

                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .
                'mdblDayType = 0.5

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.
                If mblnIsCountOnlyShiftTime Then
                    'Pinkal (28-Nov-2019) -- Start
                    'Defect Hill Packaging - Calculating Day Type wrongly.
                    'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                    mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                    'Pinkal (28-Nov-2019) -- End
                Else
                mdblDayType = 0
                End If
                'Pinkal (07-Sep-2017) -- End

                'Pinkal (18-Jul-2017) -- End

                'Pinkal (24-Jan-2017) -- Start
                'Enhancement -  0001879: Count short time before (mins) defined on any shift does not reflect / work on employee time sheet.
                '  mintShorthr = mintFinalWorkingHrs - mintTotalhr
                If (CInt(drDayName(0)("workinghrsinsec")) - CInt(drDayName(0)("calcshorttimebeforeinsec"))) > mintTotalhr OrElse mintTotalhr < CInt(drDayName(0)("calcshorttimebeforeinsec")) Then
                    mintShorthr = mintFinalWorkingHrs - mintTotalhr
                End If
                'Pinkal (24-Jan-2017) -- End


                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) Then

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.



                If mblnIsCountOnlyShiftTime Then
                    'Pinkal (28-Nov-2019) -- Start
                    'Defect Hill Packaging - Calculating Day Type wrongly.
                    'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                    mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                    'Pinkal (28-Nov-2019) -- End
                Else
                    mdblDayType = 0
                End If
                'Pinkal (07-Sep-2017) -- End

                If mintTotalhr > mintFinalWorkingHrs + CInt(drPolicy(0)("ltgoing_graceinsec")) AndAlso mintFinalWorkingHrs > 0 Then  'CHECK FOR BASE HOUR + LATE GOING GRACE FOR OVERTIME IF EXCEED THEN GIVE OVERTIME OTHERWISE NO OVERTIME
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = mintTotalhr - mintFinalWorkingHrs
                Else
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = 0
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0
                mintShorthr = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
                          mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) Then

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.

                If mblnIsCountOnlyShiftTime Then
                    'Pinkal (28-Nov-2019) -- Start
                    'Defect Hill Packaging - Calculating Day Type wrongly.
                    'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                    mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                    'Pinkal (28-Nov-2019) -- End
                Else
                mdblDayType = 1
                End If
                'Pinkal (07-Sep-2017) -- End

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If
                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")))
                End If
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0
                mintShorthr = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
                 mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) AndAlso _
                 mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) Then

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.

                If mblnIsCountOnlyShiftTime Then
                    'Pinkal (28-Nov-2019) -- Start
                    'Defect Hill Packaging - Calculating Day Type wrongly.
                    'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                    mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                    'Pinkal (28-Nov-2019) -- End
                Else
                mdblDayType = 1
                End If
                'Pinkal (07-Sep-2017) -- End

                mintShorthr = 0

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")))
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
                    mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) AndAlso _
                    mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) AndAlso _
                    mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then  ' FOR TOTALHOURS - BASE HOURS IS GREATER THAN EQUAL TO  OT1 AND OT2. 

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.
                If mblnIsCountOnlyShiftTime Then
                    'Pinkal (28-Nov-2019) -- Start
                    'Defect Hill Packaging - Calculating Day Type wrongly.
                    'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                    mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                    'Pinkal (28-Nov-2019) -- End
                Else
                mdblDayType = 1
                End If
                'Pinkal (07-Sep-2017) -- End
                mintShorthr = 0

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                End If

            ElseIf mintTotalhr - mintFinalWorkingHrs > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) AndAlso _
               mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) AndAlso _
               mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) AndAlso _
               mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then  ' FOR TOTALHOURS - BASE HOURS IS GREATER THAN EQUAL TO  OT1 AND OT2. 

                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.
                If mblnIsCountOnlyShiftTime Then
                    'Pinkal (28-Nov-2019) -- Start
                    'Defect Hill Packaging - Calculating Day Type wrongly.
                    'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                    mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                    'Pinkal (28-Nov-2019) -- End
                Else
                mdblDayType = 1
                End If
                'Pinkal (07-Sep-2017) -- End

                mintShorthr = 0

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec"))
                End If

                If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) += mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                                + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                                + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then


                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) += mintTotalhr - (mintFinalWorkingHrs + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                              + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                              + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))

                ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) += mintTotalhr - (CInt(drDayName(0)("workinghrsinsec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))

                ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) += mintTotalhr - (CInt(drDayName(0)("workinghrsinsec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) _
                                                               + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))


                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculatePolicyMgmtWorkedHrs; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateOTOnWkPHDayOffHrs(ByVal mintFinalWorkingHrs As Integer, ByVal drPolicy() As DataRow, ByVal mdctOTOrder As Dictionary(Of Integer, String) _
                                                                   , ByVal mblnIsCountOnlyShiftTime As Boolean, ByVal xShiftOutTime As DateTime, ByVal objDataOperation As clsDataOperation _
                                                                   , ByVal blnIsOpenShift As Boolean)

        'Pinkal (28-Nov-2019) -- Defect Hill Packaging - Calculating Day Type wrongly.[ ByVal blnIsOpenShift As Boolean]

        'Pinkal (07-Sep-2017) --  Working on B5 Plus Enhancement.[ ByVal mblnIsCountOnlyShiftTime As Boolean, ByVal xShiftOutTime As DateTime] )


        Try
            If mintTotalhr <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) Then

                If mintTotalhr < mintFinalWorkingHrs Then
                    mdblDayType = 0
                Else
                    'Pinkal (07-Sep-2017) -- Start
                    'Enhancement - Working on B5 Plus Enhancement.
                    If mblnIsCountOnlyShiftTime Then
                        'Pinkal (28-Nov-2019) -- Start
                        'Defect Hill Packaging - Calculating Day Type wrongly.
                        'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                        mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                        'Pinkal (28-Nov-2019) -- End
                    Else
                        mdblDayType = 1
                    End If
                    'Pinkal (07-Sep-2017) -- End
                End If

                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = mintTotalhr
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0
                mintShorthr = 0

            ElseIf mintTotalhr > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) Then
                If mintTotalhr < mintFinalWorkingHrs Then
                    mdblDayType = 0
                Else
                    'Pinkal (07-Sep-2017) -- Start
                    'Enhancement - Working on B5 Plus Enhancement.
                    If mblnIsCountOnlyShiftTime Then
                        'Pinkal (28-Nov-2019) -- Start
                        'Defect Hill Packaging - Calculating Day Type wrongly.
                        'mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, objDataOperation)
                        mdblDayType = CalculateDayType(mintEmployeeunkid, mdtLogindate, xShiftOutTime, mintFinalWorkingHrs, 0, 0, 0, blnIsOpenShift, objDataOperation)
                        'Pinkal (28-Nov-2019) -- End
                    Else
                        mdblDayType = 1
                    End If
                    'Pinkal (07-Sep-2017) -- End
                End If

                mintShorthr = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

                If mintTotalhr - CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) Then
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = mintTotalhr - CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = 0
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0

                ElseIf mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) AndAlso _
                         mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = 0


                ElseIf mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) AndAlso _
                         mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) <= CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")))


                ElseIf mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))) > CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) Then

                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec"))
                    mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) = CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec"))


                    If CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    ElseIf CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) > 0 Then
                        mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) += mintTotalhr - (CInt(drPolicy(0)("ot" & mdctOTOrder.Item(1).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(2).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(3).ToString() & "insec")) + CInt(drPolicy(0)("ot" & mdctOTOrder.Item(4).ToString() & "insec")))
                    End If

                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateOTOnWkPHDayOffHrs; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Jul-2017) -- End


    'Pinkal (25-AUG-2017) -- Start
    'Enhancement - Working on B5 plus TnA Enhancement.

    Public Function UpdateLoginSummaryFromLeave(ByVal objDataOperation As clsDataOperation, ByVal arDate() As String, ByVal xEmployeeId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim blnFlag As Boolean = False
        Try
            If arDate IsNot Nothing AndAlso arDate.Length > 0 Then


                Dim xLoginSummaryId As Integer = 0

                For i As Integer = 0 To arDate.Length - 1

                    strQ = " SELECT ISNULL(loginsummaryunkid, 0) loginsummaryunkid FROM tnalogin_summary WHERE employeeunkid = @empid AND CONVERT(CHAR(8), login_date, 112) = @logdate"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@empid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

                    If IsDate(arDate(i)) Then
                        objDataOperation.AddParameter("@logdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(CDate(arDate(i))))
                    Else
                        objDataOperation.AddParameter("@logdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, arDate(i))
                    End If


                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        xLoginSummaryId = CInt(dsList.Tables(0).Rows(0)("loginsummaryunkid"))

                        strQ = " UPDATE tnalogin_summary SET isabsentprocess = 0 WHERE loginsummaryunkid = @loginsummaryunkid"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@loginsummaryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoginSummaryId)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoginSummaryFromLeave; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (25-AUG-2017) -- End


    'Pinkal (07-Sep-2017) -- Start
    'Enhancement - Working on B5 Plus Enhancement.

    Private Sub CalculateOTPenalty(ByVal mdctOTOrder As Dictionary(Of Integer, String), ByVal mdctFinalOTPenalty As Dictionary(Of String, Integer), ByVal mintltcome_graceinsec As Integer)
        Try
            mdctFinalOTPenalty.Add("OT1_Penalty", 0)
            mdctFinalOTPenalty.Add("OT2_Penalty", 0)
            mdctFinalOTPenalty.Add("OT3_Penalty", 0)
            mdctFinalOTPenalty.Add("OT4_Penalty", 0)

            Dim objOTPenalty As New clsOT_Penalty
            Dim dsOtPenaltyList As DataSet = objOTPenalty.GetList("List", True, mintPolicyId)
            If dsOtPenaltyList IsNot Nothing AndAlso dsOtPenaltyList.Tables(0).Rows.Count > 0 Then  'START dsOtPenaltyList IsNot Nothing AndAlso dsOtPenaltyList.Tables(0).Rows.Count > 0
                Dim drRow() As DataRow = Nothing

                Dim mintMaxPenlatyMins As Integer = CInt(IIf(IsDBNull(dsOtPenaltyList.Tables(0).Compute("Max(uptominsinsec)", "1=1")), 0, dsOtPenaltyList.Tables(0).Compute("Max(uptominsinsec)", "1=1")))

                If mintltcome_graceinsec > 0 AndAlso mintLateComing_AfterGrace > 0 Then
                    drRow = dsOtPenaltyList.Tables(0).Select("uptominsinsec >= " & mintLateComing_AfterGrace)
                    If drRow.Length <= 0 AndAlso mintMaxPenlatyMins < mintLateComing_AfterGrace Then
                        drRow = dsOtPenaltyList.Tables(0).Select("Uptominsinsec = " & mintMaxPenlatyMins)
                    End If
                ElseIf mintltcome_graceinsec > 0 AndAlso mintLateComing_WOGrace > 0 Then
                    drRow = dsOtPenaltyList.Tables(0).Select("uptominsinsec >= " & mintLateComing_WOGrace)
                    If drRow.Length <= 0 AndAlso mintMaxPenlatyMins < mintLateComing_WOGrace Then
                        drRow = dsOtPenaltyList.Tables(0).Select("Uptominsinsec = " & mintMaxPenlatyMins)
                    End If
                End If

                If drRow.Length > 0 Then  'START  drRow.Length > 0

                    If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString())) > 0 AndAlso _
                      CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                      mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(1).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec"))

                    ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString())) > 0 AndAlso _
                            CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                            CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(1).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(1).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(1).ToString()))
                    End If

                    If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) > 0 AndAlso _
                        CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                        CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(2).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec"))

                    ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) > 0 AndAlso _
                             CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                             CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(2).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(2).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(2).ToString()))
                    End If

                    If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) > 0 AndAlso _
                       CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                       CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(3).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec"))

                    ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) > 0 AndAlso _
                             CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                             CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(3).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(3).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(3).ToString()))
                    End If

                    If CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) > 0 AndAlso _
                      CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                      CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) >= CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(4).ToString() & "_Penalty") = CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec"))

                    ElseIf CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) > 0 AndAlso _
                            CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) > 0 AndAlso _
                            CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString())) < CInt(drRow(0)("ot" & mdctOTOrder.Item(4).ToString() & "_Penaltyinsec")) Then

                        mdctFinalOTPenalty.Item("OT" & mdctOTOrder.Item(4).ToString() & "_Penalty") = CInt(mdctFinalOTOrder.Item("OT" & mdctOTOrder.Item(4).ToString()))
                    End If

                End If  ' 'END  drRow.Length > 0

            End If  ''END dsOtPenaltyList IsNot Nothing AndAlso dsOtPenaltyList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateOTPenalty; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateNightHrs(ByVal drDayName As DataRow())
        Try

            If mdtcheckintime.Date <> Nothing AndAlso (CDate(drDayName(0)("nightfromhrs")) <> Nothing AndAlso CDate(drDayName(0)("nighttohrs")) <> Nothing) Then

                If DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), CDate(drDayName(0)("nighttohrs")).AddDays(1)) > 0 AndAlso CDate(drDayName(0)("nightfromhrs")) <> CDate(drDayName(0)("nighttohrs")) Then


                    'Pinkal (14-Nov-2019) -- Start
                    'Defect NAH - Problem in getting Night hrs in 24 hrs format and open shift night hrs issue.
                    'If CDate(drDayName(0)("nighttohrs")).ToShortTimeString().Contains("AM") Then

                    If CDate(drDayName(0)("nightfromhrs")) > CDate(drDayName(0)("nighttohrs")) Then
                        If mdtcheckintime.ToString("HH") < 12 Then
                            drDayName(0)("nightfromhrs") = mdtcheckintime.Date.AddDays(-1) & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString
                            drDayName(0)("nighttohrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                        Else
                    drDayName(0)("nightfromhrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString
                        drDayName(0)("nighttohrs") = mdtcheckintime.Date.AddDays(1) & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                        End If
                    Else
                        drDayName(0)("nightfromhrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString()
                        drDayName(0)("nighttohrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nighttohrs")).ToShortTimeString
                    End If

                    'drDayName(0)("nightfromhrs") = mdtcheckintime.Date & " " & CDate(drDayName(0)("nightfromhrs")).ToShortTimeString()

                    If mdtcheckintime <> Nothing AndAlso mdtCheckouttime <> Nothing Then
                        If mdtcheckintime <= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), CDate(drDayName(0)("nighttohrs")))

                        ElseIf mdtcheckintime <= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime <= CDate(drDayName(0)("nighttohrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nightfromhrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")), mdtCheckouttime)

                        ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime <= CDate(drDayName(0)("nighttohrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, mdtcheckintime, mdtCheckouttime)

                        ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nightfromhrs")).AddDays(1) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, CDate(drDayName(0)("nightfromhrs")).AddDays(1), mdtCheckouttime)

                        ElseIf mdtcheckintime >= CDate(drDayName(0)("nightfromhrs")) AndAlso mdtCheckouttime >= CDate(drDayName(0)("nighttohrs")) Then
                            mintTotalNighthrs = DateDiff(DateInterval.Second, mdtcheckintime, CDate(drDayName(0)("nighttohrs")))

                        End If

                        'Pinkal (14-Nov-2019) -- Start
                        'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                        If mintTotalNighthrs < 0 Then mintTotalNighthrs = 0
                        'Pinkal (14-Nov-2019) -- End

                    End If

                    'START FOR WHEN EMPLOYEE DIRECTLY LOGIN TO NIGHT HRS THEN NO NEED TO CALCULATE SHORT HRS OR OVERTIME HR

                    'strQ = "Select Count(*) From tnalogin_tran WHERE Convert(char(8),logindate,112) = @logindate AND employeeunkid = @employeeunkid  "
                    'objDataOperation.ClearParameters()
                    'objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLogindate))
                    'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                    'Dim icnt As Integer = objDataOperation.RecordCount(strQ)
                    'If icnt <= 1 Then
                    '    mintShorthr = 0
                    'End If

                    'END FOR WHEN EMPLOYEE DIRECTLY LOGIN TO NIGHT HRS THEN NO NEED TO CALCULATE SHORT HRS OR OVERTIME HR

                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateNightHrs; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateEarlyLate_ComingGoing(ByVal objDataOperation As clsDataOperation, ByVal xShiftInTime As DateTime, ByVal xShiftOutTime As DateTime)
        Try
            Dim dsTimesheet As DataSet = GetList(objDataOperation, "List", True)
            Dim drTimesheet() As DataRow = dsTimesheet.Tables(0).Select("loginunkid <> " & mintLoginunkid & " AND loginunkid = MIN(loginunkid)")
            If drTimesheet.Length <= 0 Then

                If mdtRoundoffInTime = Nothing Then

                    If mdtcheckintime <= xShiftInTime Then    'Early Coming   
                        mintearly_coming = DateDiff(DateInterval.Second, mdtcheckintime, xShiftInTime)
                    ElseIf mdtcheckintime >= xShiftInTime Then       'Late Coming  
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, mdtcheckintime)
                    End If
                Else

                    If mdtRoundoffInTime <= xShiftInTime Then    'Early Coming   
                        mintearly_coming = DateDiff(DateInterval.Second, mdtRoundoffInTime, xShiftInTime)
                    ElseIf mdtRoundoffInTime >= xShiftInTime Then       'Late Coming  
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, mdtRoundoffInTime)
                    End If

                End If
            Else

                If IsDBNull(drTimesheet(0)("roundoff_intime")) Then

                    If Not IsDBNull(drTimesheet(0)("checkintime")) AndAlso CDate(drTimesheet(0)("checkintime")) <= xShiftInTime Then    'Early Coming
                        mintearly_coming = DateDiff(DateInterval.Second, CDate(drTimesheet(0)("checkintime")), xShiftInTime)
                    ElseIf Not IsDBNull(drTimesheet(0)("checkintime")) AndAlso CDate(drTimesheet(0)("checkintime")) >= xShiftInTime Then       'Late Coming
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, CDate(drTimesheet(0)("checkintime")))
                    End If


                ElseIf Not IsDBNull(drTimesheet(0)("roundoff_intime")) Then

                    If Not IsDBNull(drTimesheet(0)("roundoff_intime")) AndAlso CDate(drTimesheet(0)("roundoff_intime")) <= xShiftInTime Then    'Early Coming
                        mintearly_coming = DateDiff(DateInterval.Second, CDate(drTimesheet(0)("roundoff_intime")), xShiftInTime)
                    ElseIf Not IsDBNull(drTimesheet(0)("roundoff_intime")) AndAlso CDate(drTimesheet(0)("roundoff_intime")) >= xShiftInTime Then       'Late Coming
                        mintlate_coming = DateDiff(DateInterval.Second, xShiftInTime, CDate(drTimesheet(0)("roundoff_intime")))
                    End If

                End If

            End If


            If mdtRoundoffOutTime = Nothing Then

                If mdtCheckouttime <> Nothing AndAlso mdtCheckouttime <= xShiftOutTime Then    'Early Going   --
                    mintearly_going = DateDiff(DateInterval.Second, mdtCheckouttime, xShiftOutTime)
                ElseIf mdtCheckouttime <> Nothing AndAlso mdtCheckouttime >= xShiftOutTime Then       'Late Going  --
                    mintlate_going = DateDiff(DateInterval.Second, xShiftOutTime, mdtCheckouttime)
                End If

            Else
                If mdtRoundoffOutTime <= xShiftOutTime Then    'Early Going   --
                    mintearly_going = DateDiff(DateInterval.Second, mdtRoundoffOutTime, xShiftOutTime)
                ElseIf mdtRoundoffOutTime >= xShiftOutTime Then       'Late Going  --
                    mintlate_going = DateDiff(DateInterval.Second, xShiftOutTime, mdtRoundoffOutTime)
                End If

            End If

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If mblnIsWeekend = True OrElse mblnIsholiday = True OrElse mblnIsDayOffshift = True Then
                mintlate_coming = 0
                mintearly_coming = 0

                mintearly_going = 0
                mintlate_going = 0
            End If
            'Pinkal (13-Sep-2021)-- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateEarlyLate_ComingGoing; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function CalculateDayType(ByVal xEmployeeID As Integer, ByVal xLoginDate As DateTime, ByVal xShiftOutTime As DateTime _
                                                       , ByVal xTotalShiftWorkingHrsInSec As Integer, ByVal xHalffromHrsInSec As Integer, ByVal xHalfToHrsInSec As Integer _
                                                       , ByVal xCalcShorttimeBeforeInSec As Integer, ByVal blnIsOpenShift As Boolean, Optional ByVal objDoperation As clsDataOperation = Nothing) As Decimal

        'Pinkal (28-Nov-2019) -- Defect Hill Packaging - Calculating Day Type wrongly.[ ByVal blnIsOpenShift As Boolean]

        Dim mdclDayType As Double = 0
        Try
            Dim dtList As DataSet = GetList(objDoperation, "List", True)
            If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then
                If Not IsDBNull(dtList.Tables(0).Compute("MAX(checkouttime)", "1=1")) Then
                    Dim dtLastLogOut As DateTime = dtList.Tables(0).Compute("MAX(checkouttime)", "1=1")

                    If blnIsOpenShift = False Then

                    If dtLastLogOut <> Nothing AndAlso xShiftOutTime <> Nothing Then

                    Dim xTotalHrs As Integer = 0
                    If dtLastLogOut > xShiftOutTime Then
                        xTotalHrs = mintTotalhr - (mintearly_coming + DateDiff(DateInterval.Second, xShiftOutTime, dtLastLogOut))
                    Else
                                'Pinkal (28-Nov-2019) -- Start
                                'Defect Hill Packaging - Calculating Day Type wrongly.
                                If mintearly_coming > mintTotalhr Then
                                    xTotalHrs = mintearly_coming - mintTotalhr
                                Else
                        xTotalHrs = mintTotalhr - mintearly_coming
                    End If
                                'Pinkal (28-Nov-2019) -- End

                            End If

                    If xTotalHrs >= xHalffromHrsInSec AndAlso xTotalHrs <= xHalfToHrsInSec Then
                        mdclDayType = 0.5
                    ElseIf xTotalHrs > xHalffromHrsInSec AndAlso xTotalHrs > xHalfToHrsInSec Then

                        'Pinkal (25-Oct-2017) -- Start
                        'Enhancement - Change for B5 Plus As Per Annor Urgent Requirement .
                        'If (xTotalShiftWorkingHrsInSec - xTotalHrs) > xCalcShorttimeBeforeInSec Then
                        'mdclDayType = 0.5
                        'Else
                            mdclDayType = 1
                        'End If
                        'Pinkal (25-Oct-2017) -- End
                    Else
                        mdclDayType = 0
                    End If

                    Else
                        mdclDayType = 0
                    End If

                    ElseIf blnIsOpenShift Then

                        If mintTotalhr >= xHalffromHrsInSec AndAlso mintTotalhr <= xHalfToHrsInSec Then
                            mdclDayType = 0.5
                        ElseIf mintTotalhr > xHalffromHrsInSec AndAlso mintTotalhr > xHalfToHrsInSec Then
                            mdclDayType = 1
                        ElseIf mintTotalhr > 0 AndAlso (xHalffromHrsInSec <= 0 AndAlso xHalfToHrsInSec <= 0) Then
                            mdclDayType = 1
                        ElseIf mintTotalhr <= 0 Then
                            mdclDayType = 0
                End If

                    End If

                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CalculateDayType; Module Name: " & mstrModuleName)
        End Try
        Return mdclDayType
    End Function

    'Pinkal (07-Sep-2017) -- End



#End Region

#Region "Other Methods"

    Public Function GetEmployeeHoliday(ByVal intEmployeeID As Integer, ByVal intMonth As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT COUNT(*) 'Holiday' FROM  lvemployee_holiday " & _
                   " JOIN lvholiday_master ON lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid and isactive = 1 " & _
                   " WHERE employeeunkid = @employeeunkid AND DATEPART(MM,holidaydate) = @month"
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@month", SqlDbType.Int, eZeeDataType.INT_SIZE, intMonth.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "Holiday")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("Holiday"))
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeDayWorked; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetEmployeeLeave(ByVal intEmployeeID As Integer, ByVal intMonth As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT COUNT(*) 'EmploeeLeave' FROM  lvleaveIssue_tran " & _
                   " JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid =  lvleaveIssue_tran.leaveissueunkid " & _
                   " WHERE employeeunkid = @employeeunkid And DatePart(MM, leavedate) = @month And lvleaveIssue_tran.isvoid = 0"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@month", SqlDbType.Int, eZeeDataType.INT_SIZE, intMonth.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "Leave")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("EmploeeLeave"))
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeDayWorked; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetEmployeeLoginData(ByVal intEmployeeID As Integer, ByVal mdtStartDate As DateTime, ByVal mdtEndDate As DateTime, Optional ByVal intShiftId As Integer = -1, Optional ByVal intPolicyId As Integer = -1, Optional ByVal blnPolicyManagementTNA As Boolean = False) As DataTable
        'Public Function GetEmployeeLoginData(ByVal intEmployeeID As Integer, ByVal mdtStartDate As DateTime, ByVal mdtEndDate As DateTime, Optional ByVal intShiftId As Integer = -1, Optional ByVal intPolicyId As Integer = -1) As DataTable
        'S.SANDEEP [04 JUN 2015] -- END
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            'Pinkal (30-Oct-2013) -- Start
            'Enhancement : Oman Changes

            strQ = "SELECT MIN(tnalogin_tran.loginunkid) AS loginunkid " & _
                      ", MAX(tnalogin_tran.loginunkid) AS Maxloginunkid " & _
                      ", tnalogin_tran.employeeunkid " & _
                      ", hremployee_master.employeecode AS EmpCode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                      ", tnashift_master.shiftunkid AS shiftunkid " & _
                      ", tnashift_master.shiftname AS Shift "


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._PolicyManagementTNA Then
            '    strQ &= ", tnapolicy_master.policyunkid AS policyunkid  " & _
            '              ", tnapolicy_master.policyname AS Policy "
            'Else
            '    strQ &= ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.total_short_hrs / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), (tnalogin_summary.total_short_hrs / 60 ) % 60) / 100 ) AS TotalShortHours "
            'End If
            If blnPolicyManagementTNA Then
                strQ &= ", tnapolicy_master.policyunkid AS policyunkid  " & _
                          ", tnapolicy_master.policyname AS Policy "
            Else
                strQ &= ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.total_short_hrs / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), (tnalogin_summary.total_short_hrs / 60 ) % 60) / 100 ) AS TotalShortHours "
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            strQ &= ", CONVERT(VARCHAR,tnalogin_tran.logindate) AS logindate  " & _
                      ", '' AS BaseHrs  " & _
                      ", CASE WHEN CONVERT(VARCHAR(MAX), MIN(tnalogin_tran.roundoff_intime),100)  IS NULL THEN CONVERT(VARCHAR(MAX),MIN(tnalogin_tran.checkintime),100) ELSE CONVERT(VARCHAR(MAX),MIN(tnalogin_tran.roundoff_intime),100) END As checkintime " & _
                      ", CASE WHEN CONVERT(VARCHAR(MAX), MAX(tnalogin_tran.roundoff_outtime),100)  IS NULL THEN CONVERT(VARCHAR(MAX), MAX(tnalogin_tran.checkouttime),100) ELSE CONVERT(VARCHAR(MAX), MAX(tnalogin_tran.roundoff_outtime),100) END As checkouttime " & _
                      ", (SELECT ISNULL(SUM(t.breakhr),0) FROM tnalogin_tran t WHERE t.logindate = tnalogin_tran.logindate AND t.employeeunkid = tnalogin_tran.employeeunkid AND t.isvoid=0 ) AS BreakHr "


            'Pinkal (28-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._PolicyManagementTNA Then
            '    strQ &= ", (SELECT ISNULL(SUM(t.teahr),0) FROM tnalogin_tran t WHERE t.logindate = tnalogin_tran.logindate AND t.employeeunkid = tnalogin_tran.employeeunkid AND t.isvoid=0 ) AS TeaHr " & _
            '                ", 0.00 AS TeaHour "
            'End If
            If blnPolicyManagementTNA Then
                strQ &= ", (SELECT ISNULL(SUM(t.teahr),0) FROM tnalogin_tran t WHERE t.logindate = tnalogin_tran.logindate AND t.employeeunkid = tnalogin_tran.employeeunkid AND t.isvoid=0 ) AS TeaHr " & _
                            ", 0.00 AS TeaHour "
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (28-Jan-2014) -- End


            strQ &= ", tnalogin_summary.total_hrs AS workhour " & _
                      ", 0.00 AS BreakHour " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(( tnalogin_summary.total_hrs / 60 )/ 60)+ ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.total_hrs / 60 ) % 60)/ 100 )) AS WorkingHours " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.total_hrs / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.total_hrs / 60 ) % 60) / 100 ) AS TotalHrs " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.total_overtime / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.total_overtime / 60 ) % 60) / 100 ) AS OT1 " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.ot2 / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.ot2 / 60 ) % 60) / 100 ) AS OT2 " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.ot3 / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.ot3 / 60 ) % 60) / 100 ) AS OT3 " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.ot4 / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.ot4 / 60 ) % 60) / 100 ) AS OT4 " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.elrcoming_grace / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.elrcoming_grace / 60 ) % 60) / 100 ) AS elrcoming_grace " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.ltcoming_grace / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.ltcoming_grace / 60 ) % 60) / 100 ) AS ltcoming_grace " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.elrgoing_grace / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.elrgoing_grace / 60 ) % 60) / 100 ) AS elrgoing_grace " & _
                      ", CONVERT(DECIMAL(10, 2), FLOOR(tnalogin_summary.ltgoing_grace / 60 ) / 60) + ( CONVERT(DECIMAL(10, 2), ( tnalogin_summary.ltgoing_grace / 60 ) % 60) / 100 ) AS ltgoing_grace " & _
                      ", '' AS  InException " & _
                      ", '' AS  OutException " & _
                      " FROM    tnalogin_tran " & _
                      " JOIN hremployee_master ON tnalogin_tran.employeeunkid = hremployee_master.employeeunkid " & _
                      " JOIN tnalogin_summary ON tnalogin_summary.employeeunkid = tnalogin_tran.employeeunkid AND tnalogin_summary.login_date = tnalogin_tran.logindate " & _
                          " JOIN hremployee_shift_tran ON hremployee_shift_tran.employeeunkid = tnalogin_tran.employeeunkid AND hremployee_shift_tran.shiftunkid = tnalogin_summary.shiftunkid "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._PolicyManagementTNA Then
            '    strQ &= " JOIN hremployee_policy_tran ON hremployee_policy_tran.employeeunkid = tnalogin_tran.employeeunkid AND hremployee_policy_tran.policyunkid = tnalogin_summary.policyunkid " & _
            '                " JOIN tnapolicy_master ON tnapolicy_master.policyunkid = hremployee_policy_tran.policyunkid "
            'End If
            If blnPolicyManagementTNA Then
                strQ &= " JOIN hremployee_policy_tran ON hremployee_policy_tran.employeeunkid = tnalogin_tran.employeeunkid AND hremployee_policy_tran.policyunkid = tnalogin_summary.policyunkid " & _
                            " JOIN tnapolicy_master ON tnapolicy_master.policyunkid = hremployee_policy_tran.policyunkid "
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            strQ &= " JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_shift_tran.shiftunkid " & _
                      " WHERE   tnalogin_tran.employeeunkid = @employeeunkid " & _
                      " AND ( CONVERT(CHAR(8), logindate, 112) >= @startdate AND CONVERT(CHAR(8), logindate, 112) <= @enddate) " & _
                      "AND ISNULL(tnalogin_tran.isvoid, 0) = 0 "


            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes

            If intShiftId > 0 Then
                strQ &= " AND tnashift_master.shiftunkid = @shiftunkid "
                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftId.ToString)
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If blnPolicyManagementTNA AndAlso intPolicyId > 0 Then
                'If ConfigParameter._Object._PolicyManagementTNA AndAlso intPolicyId > 0 Then
                'S.SANDEEP [04 JUN 2015] -- END
                strQ &= " AND tnapolicy_master.policyunkid = @policyunkid "
                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPolicyId.ToString)
            End If

            'Pinkal (15-Nov-2013) -- End


            'Pinkal (27-Dec-2013) -- Start
            'Enhancement : Oman Changes
            strQ &= " AND tnalogin_tran.checkintime IS NOT NULL AND tnalogin_tran.checkouttime IS NOT NULL "
            'Pinkal (27-Dec-2013) -- End



            strQ &= "GROUP BY " & _
                      " tnalogin_tran.employeeunkid " & _
                      ", tnalogin_tran.employeeunkid " & _
                      ", hremployee_master.employeecode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                      ",  tnalogin_tran.logindate " & _
                      ", tnashift_master.shiftunkid  " & _
                      ", tnashift_master.shiftname "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If blnPolicyManagementTNA Then
                'If ConfigParameter._Object._PolicyManagementTNA Then
                'S.SANDEEP [04 JUN 2015] -- END
                strQ &= ", tnapolicy_master.policyunkid " & _
                              ", tnapolicy_master.policyname "
            Else
                strQ &= ",tnalogin_summary.total_short_hrs "
            End If

            strQ &= ", tnalogin_summary.total_hrs " & _
                      ", tnalogin_summary.total_overtime " & _
                      ", tnalogin_summary.ot2 " & _
                      ", tnalogin_summary.ot3 " & _
                      ", tnalogin_summary.ot4 " & _
                      ",tnalogin_summary.elrcoming_grace " & _
                      ",tnalogin_summary.ltcoming_grace " & _
                      ",tnalogin_summary.elrgoing_grace " & _
                      ",tnalogin_summary.ltgoing_grace "

            'Pinkal (30-Oct-2013) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtList = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeLoginData", mstrModuleName)
        End Try
        Return dtList
    End Function

    Public Function GetEmployeeDayTotalWorkingHrs(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal dtLogindate As Date) As Integer
        Dim mintEmpWorkingHr As Integer = 0
        Dim exForce As Exception
        Dim StrQ As String = ""
        Try

            StrQ = " SELECT ISNULL(total_hrs,0) total_hrs FROM tnalogin_summary WHERE employeeunkid = @employeeunkid AND CONVERT(char(8),login_date,112) = @logindate "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtLogindate))
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpWorkingHr = CInt(dsList.Tables(0).Rows(0)("total_hrs"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeDayTotalWorkingHrs; Module Name: " & mstrModuleName)
        End Try
        Return mintEmpWorkingHr
    End Function


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    Public Function InsertDeviceAttendanceData() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@deviceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceAtt_DeviceNo.ToString)
            objDataOperation.AddParameter("@ipaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceAtt_IPAddress.ToString)
            objDataOperation.AddParameter("@enrollno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceAtt_EnrollNo.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtDeviceAtt_LoginDate <> Nothing, mdtDeviceAtt_LoginDate, DBNull.Value))
            objDataOperation.AddParameter("@logintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtDeviceAtt_LoginTime <> Nothing, mdtDeviceAtt_LoginTime, DBNull.Value))
            objDataOperation.AddParameter("@verifymode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceAtt_VerifyMode.ToString)
            objDataOperation.AddParameter("@inoutmode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceAtt_InOutMode.ToString)
            objDataOperation.AddParameter("@IsError", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mstrDeviceAtt_IsError.ToString)


            strQ = " INSERT INTO tnadevice_attendance ( " & _
                      " deviceno " & _
                      ",ipaddress " & _
                      ",enrollno " & _
                      ",logindate " & _
                      ",logintime " & _
                      ",verifymode " & _
                      ",inoutmode " & _
                      ",IsError " & _
                    " ) VALUES (" & _
                      " @deviceno " & _
                      ", @ipaddress " & _
                      ",@enrollno " & _
                      ",@logindate " & _
                      ",@logintime " & _
                      ",@verifymode " & _
                      ",@inoutmode " & _
                      ",@IsError " & _
                      " ); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDeviceAttendanceData; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (06-Mar-2018) -- Start
    'BUG - BUG SOLVED FOR SHIFT SHOWING WHEN IMPORT DEVICE ATTENDANCE.

    Public Function GetDeviceAttendanceData(ByVal mblnMappedUser As Boolean, ByVal mdtFromDate As Date, ByVal mdtToDate As Date,mblnIncludeSuspendedEmploye As Boolean ) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT ISNULL(hremployee_master.employeeunkid,-1) AS EmployeeId " & _
                    ",ISNULL(hremployee_master.employeecode,'') AS EmployeeCode "

            If mblnMappedUser Then
                strQ &= ",  CASE WHEN  ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN ISNULL(hremployee_master.employeecode,'') + CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN  ' - ' ELSE  ' ' END  + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') ELSE  enrollno + @NotMapped END AS EmployeeName "
            Else
                strQ &= ",  CASE WHEN  ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN ISNULL(hremployee_master.employeecode,'') + CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) > 0 THEN  ' - ' ELSE  ' ' END  + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') ELSE enrollno + @NotRegistered END AS EmployeeName "
            End If




            'strQ &= ",ISNULL(tnashift_master.shiftunkid,-1) As ShiftId " & _
            '            ",ISNULL(tnashift_master.shiftname,'') AS ShiftName " & _
            '            ",logindate " & _
            '            ",logintime " & _
            '            ",enrollno " & _
            '            ",verifymode " & _
            '            ",inoutmode " & _
            '            ",deviceno AS Device " & _
            '            ",ipaddress " & _
            '            ",CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) <=0 THEN 1 ELSE 0 END iserror " & _
            '            ",CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) <=0 THEN '*** NOT REGISTERED ***'  ELSE '' END [Message] " & _
            '            " FROM tnadevice_attendance " & _
            '            " LEFT Join hrempid_devicemapping ON hrempid_devicemapping.deviceuserid = tnadevice_attendance.enrollno " & _
            '            " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrempid_devicemapping.employeeunkid " & _
            '            " LEFT JOIN " & _
            '            " ( " & _
            '                          " SELECT hremployee_shift_tran.shiftunkid " & _
            '                          " ,employeeunkid " & _
            '                          " ,effectivedate " & _
            '                          " , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                          " FROM hremployee_shift_tran " & _
            '                          " WHERE isvoid = 0 AND effectivedate IS NOT NULL " & _
            '           " ) AS Shf ON CONVERT(CHAR(8),Shf.effectivedate,112) <= CONVERT(char(8),tnadevice_attendance.logindate,112) AND Shf.employeeunkid = hremployee_master.employeeunkid " & _
            '           " AND shf.rno = 1 " & _
            '           " LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = shf.shiftunkid " & _
            '           " ORDER BY ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,''),logintime "


            strQ &= ", CASE WHEN ISNULL(tnashift_master.shiftunkid,-1) = -1 THEN ISNULL(ts.shiftunkid,-1) ELSE ISNULL(tnashift_master.shiftunkid,-1) END As ShiftId " & _
                   ",CASE WHEN ISNULL(tnashift_master.shiftunkid,-1) = -1 THEN ISNULL(ts.shiftname,'')  ELSE ISNULL(tnashift_master.shiftname,'') END  AS ShiftName " & _
                        ",logindate " & _
                        ",logintime " & _
                        ",enrollno " & _
                        ",verifymode " & _
                        ",inoutmode " & _
                        ",deviceno AS Device " & _
                        ",ipaddress " & _
                        ",CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) <=0 THEN 1 ELSE 0 END iserror " & _
                        ",CASE WHEN ISNULL(hremployee_master.employeeunkid,-1) <=0 THEN '*** NOT REGISTERED ***'  ELSE '' END [Message] " & _
                        " FROM tnadevice_attendance " & _
                        " LEFT Join hrempid_devicemapping ON hrempid_devicemapping.deviceuserid = tnadevice_attendance.enrollno " & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrempid_devicemapping.employeeunkid " & _
                        " LEFT JOIN " & _
                        " ( " & _
                                      " SELECT hremployee_shift_tran.shiftunkid " & _
                                      " ,employeeunkid " & _
                                      " ,effectivedate " & _
                                      " , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                      " FROM hremployee_shift_tran " & _
                                      " WHERE isvoid = 0 AND effectivedate IS NOT NULL " & _
                                 " AND CONVERT(CHAR(8), hremployee_shift_tran.effectivedate, 112) <=  @ToDate " & _
                       " ) AS Shf ON CONVERT(CHAR(8),Shf.effectivedate,112) <= CONVERT(char(8),tnadevice_attendance.logindate,112) AND Shf.employeeunkid = hremployee_master.employeeunkid " & _
                       " AND shf.rno = 1 " & _
                       " LEFT JOIN tnashift_master ON tnashift_master.shiftunkid = shf.shiftunkid " & _
                  " LEFT JOIN " & _
                   " ( " & _
                                 " SELECT hremployee_shift_tran.shiftunkid " & _
                                 " ,employeeunkid " & _
                                 " ,effectivedate " & _
                                 " , ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 " FROM hremployee_shift_tran " & _
                                 " WHERE isvoid = 0 AND effectivedate IS NOT NULL " & _
                                 " AND CONVERT(CHAR(8), hremployee_shift_tran.effectivedate, 112) <=  @FromDate " & _
                  " ) AS Shf1 ON CONVERT(CHAR(8),Shf1.effectivedate,112) <= CONVERT(char(8),tnadevice_attendance.logindate,112) AND Shf1.employeeunkid = hremployee_master.employeeunkid " & _
                  " AND Shf1.rno = 1 " & _
                  " LEFT JOIN tnashift_master AS ts ON ts.shiftunkid = shf1.shiftunkid " & _
                  " WHERE hrempid_devicemapping.isactive = 1 " & _
                       " ORDER BY ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,''),logintime "


            'Pinkal (04-Jul-2018) -- 'Enhancement - Get only Active employee Attendance Data From Device Attendance.[" WHERE hrempid_devicemapping.isactive = 1 " & _] 

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@NotMapped", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "*** NOT MAPPED ***"))
            objDataOperation.AddParameter("@NotRegistered", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "*** NOT REGISTERED ***"))
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate))

            'Pinkal (06-Mar-2018) -- End


            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (21-Dec-2019) -- Start
            'Enhancement HILL PACKAGING COMPANY LTD [0003462] -   Feature to exclude suspended employees from attendance in case they clock in while on suspension.

            dtTable = dsList.Tables(0).Copy()

            If mblnIncludeSuspendedEmploye = False Then
                Dim objEmpDates As New clsemployee_dates_tran
                Dim dtSuspendedEmp As DataTable = objEmpDates.GetEmpSuspentionProbationDetailDates(0, mdtFromDate, mdtToDate)
                objEmpDates = Nothing

                If dtSuspendedEmp IsNot Nothing AndAlso dtSuspendedEmp.Rows.Count > 0 Then
                    Dim drRow As IEnumerable(Of DataRow) = From EmpLoginList In dtTable Join EmpSuspended In dtSuspendedEmp On EmpSuspended.Field(Of Integer)("employeeunkid") Equals EmpLoginList.Field(Of Integer)("EmployeeId") And EmpSuspended.Field(Of DateTime)("Datevalue") Equals EmpLoginList.Field(Of DateTime)("logindate") Select EmpLoginList
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        drRow.ToList.ForEach(Function(x) DeleteSuspendedEmpDate(dtTable, x))
                        dtTable.AcceptChanges()
                    End If
                End If
            End If
            'Pinkal (21-Dec-2019) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDeviceAttendanceData; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    Public Function DeleteDeviceAttendanceData(Optional ByVal mstrIPAddress As String = "") As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation = Nothing
        Try
            objDataOperation = New clsDataOperation

            If mstrIPAddress.Trim.Length <= 0 Then
                strQ = "Truncate Table tnadevice_attendance"
            Else
                strQ = "Delete From tnadevice_attendance WHERE ipaddress = @ipaddress"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@ipaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIPAddress)
            End If

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteDeviceAttendanceData; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (27-Mar-2017) -- End


    'Pinkal (21-Dec-2019) -- Start
    'Enhancement HILL PACKAGING COMPANY LTD [0003462] -   Feature to exclude suspended employees from attendance in case they clock in while on suspension.

    Public Function DeleteSuspendedEmpDate(ByVal xTable As DataTable, ByVal drRow As DataRow) As Boolean
        Try
            If xTable IsNot Nothing AndAlso xTable.Rows.Count > 0 Then
                xTable.Rows.Remove(drRow)
                xTable.AcceptChanges()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteSuspendedEmpDate; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (21-Dec-2019) -- End 


    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Public Sub GetMinMaxDeviceAttendanceDate(ByRef dtMinDate As Date, ByRef dtMaxDate As Date)
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation = Nothing
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT MIN(Logindate) As MinDate, Max(LoginDate) as MaxDate From tnadevice_attendance "

            objDataOperation.ClearParameters()
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If IsDBNull(dsList.Tables(0).Rows(0)("MinDate")) = False Then
                    dtMinDate = CDate(dsList.Tables(0).Rows(0)("MinDate"))
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("MaxDate")) = False Then
                    dtMaxDate = CDate(dsList.Tables(0).Rows(0)("MaxDate"))
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMinMaxDeviceAttendanceDate; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub SendMailToUsersForEarlyLate(ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xUserName As String, ByVal xUserEmail As String, ByVal strPath As String, ByVal strFileName As String _
                                , Optional ByVal iLoginTypeId As Integer = 0, Optional ByVal iLoginEmployeeId As Integer = 0)

        Dim objMail As New clsSendMail
        Try
            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            objMail._Subject = Language.getMessage(mstrModuleName, 5, "Notification for Late Arrival/Early Departure")

            Dim strMessage As String = ""
            strMessage = "<HTML> <BODY>"

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            strMessage &= Language.getMessage(mstrModuleName, 6, "Dear") & " " & info1.ToTitleCase(xUserName.ToLower()) & ", <BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 7, "Please find the attached attendance report for your action. For more details, you may contact your system administrator.") & "<BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 8, "Regards.")


            Dim blnValue As Boolean = True
            Dim mstrArutisignature As String = ""
            Dim objconfig As New clsConfigOptions
            mstrArutisignature = objconfig.GetKeyValue(xCompanyId, "ShowArutisignature")
            blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
            If blnValue Then
                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            End If
            objconfig = Nothing

            strMessage &= "</BODY></HTML>"
            

            objMail._Message = strMessage
            objMail._ToEmail = xUserEmail

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP

            objMail._FormName = mstrFormName
            objMail._LoginEmployeeunkid = mintLogEmployeeUnkid
            objMail._ClientIP = mstrClientIP
            objMail._HostName = mstrHostName
            objMail._FromWeb = mblnIsWeb
            objMail._AuditUserId = mintAuditUserId
            objMail._CompanyUnkid = mintCompanyUnkid
            objMail._AuditDate = mdtAuditDate

            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = xUserId
            objMail._SenderAddress = xUserName
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TNA_MGT

            If strPath <> "" Then
                objMail._AttachedFiles = strFileName
                objMail.SendMail(xCompanyId, True, strPath)
            Else
                objMail.SendMail(xCompanyId)
            End If

            If IO.File.Exists(strPath & "/" & strFileName) Then
                IO.File.Delete(strPath & "/" & strFileName)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToUsersForEarlyLate; Module Name: " & mstrModuleName)
        Finally
            objMail = Nothing
        End Try
    End Sub

    Public Sub SendFailedNotificationToUser(ByVal xCompanyId As Integer, ByVal xUserId As Integer, ByVal xUserName As String, ByVal xUserEmail As String, ByVal strPath As String, ByVal strFileName As String _
                               , Optional ByVal iLoginTypeId As Integer = 0, Optional ByVal iLoginEmployeeId As Integer = 0)

        Dim objMail As New clsSendMail
        Try
            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Failure of Attendance Log")

            Dim strMessage As String = ""
            strMessage = "<HTML> <BODY>"

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            strMessage &= Language.getMessage(mstrModuleName, 6, "Dear") & " " & info1.ToTitleCase(xUserName.ToLower()) & ", <BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 10, "Please find the attached Attendance Error log for your action. For more details, you may contact your system administrator.") & "<BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 8, "Regards.")

            Dim blnValue As Boolean = True
            Dim mstrArutisignature As String = ""
            Dim objconfig As New clsConfigOptions
            mstrArutisignature = objconfig.GetKeyValue(xCompanyId, "ShowArutisignature")
            blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
            If blnValue Then
                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            End If
            objconfig = Nothing


            objMail._Message = strMessage
            objMail._ToEmail = xUserEmail

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP

            objMail._FormName = mstrFormName
            objMail._LoginEmployeeunkid = 0
            objMail._ClientIP = mstrClientIP
            objMail._HostName = mstrHostName
            objMail._FromWeb = False
            objMail._AuditUserId = mintAuditUserId
            objMail._CompanyUnkid = mintCompanyUnkid
            objMail._AuditDate = mdtAuditDate
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = xUserId
            objMail._SenderAddress = xUserName
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TNA_MGT

            If strPath <> "" Then
                objMail._AttachedFiles = strFileName
                objMail.SendMail(xCompanyId, True, strPath)
            Else
                objMail.SendMail(xCompanyId)
            End If

        

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendFailedNotificationToUser; Module Name: " & mstrModuleName)
        Finally
            objMail = Nothing
        End Try
    End Sub

    'Pinkal (27-Oct-2020) -- End



#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnalogin_tran) </purpose>
    Public Function InsertAuditTrailLogin(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If mintOperationType = enTnAOperation.RoundOff Then

                'Pinkal (01-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.	
                '_Loginunkid = mintLoginunkid
                _Loginunkid(objDataOperation) = mintLoginunkid
                'Pinkal (01-Feb-2022) -- End
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@logindate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)

            If ConfigParameter._Object._DonotAttendanceinSeconds Then
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, CDate(mdtcheckintime.ToShortDateString & " " & mdtcheckintime.ToString("HH:mm")), DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, CDate(mdtCheckouttime.ToShortDateString & " " & mdtCheckouttime.ToString("HH:mm")), DBNull.Value))
            Else
                objDataOperation.AddParameter("@checkintime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtcheckintime <> Nothing, mdtcheckintime, DBNull.Value))
                objDataOperation.AddParameter("@checkouttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCheckouttime <> Nothing, mdtCheckouttime, DBNull.Value))
            End If

            objDataOperation.AddParameter("@original_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Intime <> Nothing, mdtOriginal_Intime, DBNull.Value))
            objDataOperation.AddParameter("@original_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtOriginal_Outtime <> Nothing, mdtOriginal_Outtime, DBNull.Value))
            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
            objDataOperation.AddParameter("@workhour", SqlDbType.Float, eZeeDataType.INT_SIZE, mintWorkhour.ToString)
            objDataOperation.AddParameter("@breakhr", SqlDbType.Float, eZeeDataType.INT_SIZE, mintBreakhr.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@inouttype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInOuType.ToString)
            objDataOperation.AddParameter("@sourcetype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSourceType.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isgraceroundoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgraceroundoff)
            objDataOperation.AddParameter("@manualroundofftypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintManualroundofftypeid)
            objDataOperation.AddParameter("@roundminutes", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundminutes)
            objDataOperation.AddParameter("@roundlimit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoundlimit)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            If mintOperationType = enTnAOperation.RoundOff Then
                If ConfigParameter._Object._DonotAttendanceinSeconds Then
                    objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRoundoffInTime <> Nothing, CDate(mdtRoundoffInTime.ToShortDateString & " " & mdtRoundoffInTime.ToString("HH:mm")), DBNull.Value))
                    objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRoundoffOutTime <> Nothing, CDate(mdtRoundoffOutTime.ToShortDateString & " " & mdtRoundoffOutTime.ToString("HH:mm")), DBNull.Value))
                Else
                    objDataOperation.AddParameter("@roundoff_intime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRoundoffInTime <> Nothing, mdtRoundoffInTime, DBNull.Value))
                    objDataOperation.AddParameter("@roundoff_outtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRoundoffOutTime <> Nothing, mdtRoundoffOutTime, DBNull.Value))
                End If
            End If

            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationType)
            objDataOperation.AddParameter("@teahr", SqlDbType.Float, eZeeDataType.INT_SIZE, mintTeaHr.ToString)

            'Hemant (26 Oct 2018) -- Start
            'Enhancement : Implementing New Module of In/Out Daily Attendance Operations
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid.ToString)
            'Hemant (26 Oct 2018) -- End
            strQ = "INSERT INTO attnalogin_tran ( " & _
              "  loginunkid " & _
              ", employeeunkid " & _
              ", logindate " & _
              ", checkintime " & _
              ", checkouttime " & _
              ", holdunkid " & _
              ", workhour " & _
              ", breakhr " & _
              ", remark " & _
              ", inouttype " & _
              ", sourcetype " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", auditdatetime " & _
              ", ip " & _
              ", machine_name " & _
              ", form_name " & _
              " " & _
              " " & _
              " " & _
              " " & _
              " " & _
              ", isweb " & _
              ", operationtype " & _
              ", original_intime " & _
              ", original_outtime "

            If mintOperationType = enTnAOperation.RoundOff Then
                strQ &= ", roundoff_intime " & _
                             ", roundoff_outtime "
            End If

            strQ &= ",isgraceroundoff " & _
                    ",manualroundofftypeid " & _
                    ",roundminutes " & _
                    ",roundlimit "

            strQ &= ",teahr "
            'Hemant (26 Oct 2018) -- Start
            'Enhancement : Implementing New Module of In/Out Daily Attendance Operations
            strQ &= ", loginemployeeunkid "
            'Hemant (26 Oct 2018) -- End


            strQ &= ") VALUES (" & _
              "  @loginunkid " & _
              ", @employeeunkid " & _
              ", @logindate " & _
              ", @checkintime " & _
              ", @checkouttime " & _
              ", @holdunkid " & _
              ", @workhour " & _
              ", @breakhr " & _
              ", @remark " & _
              ", @inouttype " & _
              ", @sourcetype " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @auditdatetime " & _
              ", @ip " & _
              ", @machine_name " & _
              ", @form_name " & _
              " " & _
              " " & _
              " " & _
              " " & _
              " " & _
              ", @isweb " & _
              ", @operationtype " & _
              ", @original_intime " & _
              ", @original_outtime "

            If mintOperationType = enTnAOperation.RoundOff Then
                strQ &= ", @roundoff_intime " & _
                            ", @roundoff_outtime "
            End If

            strQ &= ", @isgraceroundoff " & _
                    ", @manualroundofftypeid " & _
                    ", @roundminutes " & _
                    ", @roundlimit "

            strQ &= ",@teahr "
            'Hemant (26 Oct 2018) -- Start
            'Enhancement : Implementing New Module of In/Out Daily Attendance Operations
            strQ &= ", @loginemployeeunkid "
            'Hemant (26 Oct 2018) -- End 

            strQ &= "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailLogin; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check Time information, this information already exists for this employee.")
			Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Sorry, Payroll payment is already done for the checked employee(s) for the selected date tenure, due to this some employees will be skipped for that particular date(s) during this process.")
			Language.setMessage(mstrModuleName, 4, "void due to change in appointment.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class