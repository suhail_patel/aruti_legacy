﻿'************************************************************************************************************************************
'Class Name : clsshift_master.vb
'Purpose    : All Shift Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :25/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsshift_master

    Private Shared ReadOnly mstrModuleName As String = "clsshift_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintShiftunkid As Integer
    Private mintShifttypeunkid As Integer
    Private mstrShiftcode As String = String.Empty
    Private mstrShiftname As String = String.Empty
    Private mdtStarttime As Date
    Private mdtEndtime As Date
    Private mdtBreaktime As Integer
    Private mstrShiftdays As String = String.Empty
    Private mintWorkinghours As Integer
    Private mintCalcOvertimeAter As Integer
    Private mintCalcShorttimeBefore As Integer
    Private mintFromHrs As Integer
    Private mintToHrs As Integer
    Private mblnIsparttime As Boolean
    Private mblnIsactive As Boolean = True
    Private mstrShiftname1 As String = String.Empty
    Private mstrShiftname2 As String = String.Empty
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftunkid() As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shifttypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shifttypeunkid() As Integer
        Get
            Return mintShifttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintShifttypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftcode() As String
        Get
            Return mstrShiftcode
        End Get
        Set(ByVal value As String)
            mstrShiftcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftname() As String
        Get
            Return mstrShiftname
        End Get
        Set(ByVal value As String)
            mstrShiftname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set starttime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Starttime() As Date
        Get
            Return mdtStarttime
        End Get
        Set(ByVal value As Date)
            mdtStarttime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set endtime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Endtime() As Date
        Get
            Return mdtEndtime
        End Get
        Set(ByVal value As Date)
            mdtEndtime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set breaktime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Breaktime() As Integer
        Get
            Return mdtBreaktime
        End Get
        Set(ByVal value As Integer)
            mdtBreaktime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftdays
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftdays() As String
        Get
            Return mstrShiftdays
        End Get
        Set(ByVal value As String)
            mstrShiftdays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workinghours
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Workinghours() As Integer
        Get
            Return mintWorkinghours
        End Get
        Set(ByVal value As Integer)
            mintWorkinghours = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calcOvertimeAter
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CalcOvertimeAter() As Integer
        Get
            Return mintCalcOvertimeAter
        End Get
        Set(ByVal value As Integer)
            mintCalcOvertimeAter = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calcShorttimeBefore
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CalcShorttimeBefore() As Integer
        Get
            Return mintCalcShorttimeBefore
        End Get
        Set(ByVal value As Integer)
            mintCalcShorttimeBefore = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set fromHrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FromHrs() As Integer
        Get
            Return mintFromHrs
        End Get
        Set(ByVal value As Integer)
            mintFromHrs = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set toHrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ToHrs() As Integer
        Get
            Return mintToHrs
        End Get
        Set(ByVal value As Integer)
            mintToHrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isparttime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isparttime() As Boolean
        Get
            Return mblnIsparttime
        End Get
        Set(ByVal value As Boolean)
            mblnIsparttime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftname1() As String
        Get
            Return mstrShiftname1
        End Get
        Set(ByVal value As String)
            mstrShiftname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftname2() As String
        Get
            Return mstrShiftname2
        End Get
        Set(ByVal value As String)
            mstrShiftname2 = value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shiftunkid " & _
              ", shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", starttime " & _
              ", endtime " & _
              ", breaktime " & _
              ", shiftdays " & _
              ", workinghours " & _
              ", calcovertimeafter " & _
              ", calcshorttimebefore " & _
              ", fromhrs " & _
              ", tohrs " & _
              ", isparttime " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2 " & _
             "FROM tnashift_master " & _
             "WHERE shiftunkid = @shiftunkid "

            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintShiftunkid = CInt(dtRow.Item("shiftunkid"))
                mintShifttypeunkid = CInt(dtRow.Item("shifttypeunkid"))
                mstrShiftcode = dtRow.Item("shiftcode").ToString
                mstrShiftname = dtRow.Item("shiftname").ToString

                'Sandeep [ 12 MARCH 2011 ] -- Start
                'mdtStarttime = dtRow.Item("starttime")
                'mdtEndtime = dtRow.Item("endtime")
                If IsDBNull(dtRow.Item("starttime")) Then
                    mdtStarttime = Nothing
                Else
                mdtStarttime = dtRow.Item("starttime")
                End If
                If IsDBNull(dtRow.Item("endtime")) Then
                    mdtEndtime = Nothing
                Else
                mdtEndtime = dtRow.Item("endtime")
                End If
                'Sandeep [ 12 MARCH 2011 ] -- End 

                mdtBreaktime = CInt(dtRow.Item("breaktime"))
                mstrShiftdays = dtRow.Item("shiftdays").ToString
                mintWorkinghours = CInt(dtRow.Item("workinghours"))
                If dtRow.Item("calcovertimeafter").ToString <> "" Then mintCalcOvertimeAter = CInt(dtRow.Item("calcovertimeafter"))
                If dtRow.Item("calcshorttimebefore").ToString <> "" Then mintCalcShorttimeBefore = CInt(dtRow.Item("calcshorttimebefore"))
                If dtRow.Item("fromhrs").ToString <> "" Then mintFromHrs = CInt(dtRow.Item("fromhrs"))
                If dtRow.Item("tohrs").ToString <> "" Then mintToHrs = CInt(dtRow.Item("tohrs"))
                mblnIsparttime = CBool(dtRow.Item("isparttime"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrShiftname1 = dtRow.Item("shiftname1").ToString
                mstrShiftname2 = dtRow.Item("shiftname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnashift_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrShiftcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Shift Code is already defined. Please define new Shift Code.")
            Return False
        ElseIf isExist("", mstrShiftname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Shift Name is already defined. Please define new Shift Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShifttypeunkid.ToString)
            objDataOperation.AddParameter("@shiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftcode.ToString)
            objDataOperation.AddParameter("@shiftname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname.ToString)
            'Sandeep [ 12 MARCH 2011 ] -- Start
            'objDataOperation.AddParameter("@starttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStarttime)
            'objDataOperation.AddParameter("@endtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndtime)

            If mdtStarttime = Nothing Then
                objDataOperation.AddParameter("@starttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@starttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStarttime)
            End If

            'Sandeep [ 12 MARCH 2011 ] -- Start
            If mdtEndtime = Nothing Then
                objDataOperation.AddParameter("@endtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@endtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndtime)
            End If
            'Sandeep [ 12 MARCH 2011 ] -- End
            'Sandeep [ 12 MARCH 2011 ] -- End
            objDataOperation.AddParameter("@breaktime", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtBreaktime.ToString)
            objDataOperation.AddParameter("@shiftdays", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftdays.ToString)
            objDataOperation.AddParameter("@workinghours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkinghours.ToString)
            objDataOperation.AddParameter("@calcovertimeafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalcOvertimeAter.ToString)
            objDataOperation.AddParameter("@calcshorttimebefore", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalcShorttimeBefore.ToString)
            objDataOperation.AddParameter("@fromhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromHrs.ToString)
            objDataOperation.AddParameter("@tohrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToHrs.ToString)
            objDataOperation.AddParameter("@isparttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsparttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@shiftname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname1.ToString)
            objDataOperation.AddParameter("@shiftname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname2.ToString)

            strQ = "INSERT INTO tnashift_master ( " & _
              "  shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", starttime " & _
              ", endtime " & _
              ", breaktime " & _
              ", shiftdays " & _
              ", workinghours " & _
              ", calcovertimeafter " & _
              ", calcshorttimebefore " & _
              ", fromhrs " & _
              ", tohrs " & _
              ", isparttime " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2" & _
            ") VALUES (" & _
              "  @shifttypeunkid " & _
              ", @shiftcode " & _
              ", @shiftname " & _
              ", @starttime " & _
              ", @endtime " & _
              ", @breaktime " & _
              ", @shiftdays " & _
              ", @workinghours " & _
              ", @calcovertimeafter " & _
              ", @calcshorttimebefore " & _
              ", @fromhrs " & _
              ", @tohrs " & _
              ", @isparttime " & _
              ", @isactive " & _
              ", @shiftname1 " & _
              ", @shiftname2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintShiftunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "tnashift_master", "shiftunkid", mintShiftunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnashift_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrShiftcode, "", mintShiftunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Shift Code is already defined. Please define new Shift Code.")
            Return False
        ElseIf isExist("", mstrShiftname, mintShiftunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Shift Name is already defined. Please define new Shift Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)
            objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShifttypeunkid.ToString)
            objDataOperation.AddParameter("@shiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftcode.ToString)
            objDataOperation.AddParameter("@shiftname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname.ToString)
            objDataOperation.AddParameter("@starttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStarttime)
            objDataOperation.AddParameter("@endtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndtime)
            objDataOperation.AddParameter("@breaktime", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtBreaktime.ToString)
            objDataOperation.AddParameter("@shiftdays", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftdays.ToString)
            objDataOperation.AddParameter("@workinghours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkinghours.ToString)
            objDataOperation.AddParameter("@calcovertimeafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalcOvertimeAter.ToString)
            objDataOperation.AddParameter("@calcshorttimebefore", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalcShorttimeBefore.ToString)
            objDataOperation.AddParameter("@fromhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromHrs.ToString)
            objDataOperation.AddParameter("@tohrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToHrs.ToString)
            objDataOperation.AddParameter("@isparttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsparttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@shiftname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname1.ToString)
            objDataOperation.AddParameter("@shiftname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname2.ToString)

            strQ = "UPDATE tnashift_master SET " & _
              "  shifttypeunkid = @shifttypeunkid" & _
              ", shiftcode = @shiftcode" & _
              ", shiftname = @shiftname" & _
              ", starttime = @starttime" & _
              ", endtime = @endtime" & _
              ", breaktime = @breaktime" & _
              ", shiftdays = @shiftdays" & _
              ", workinghours = @workinghours" & _
              ", calcovertimeafter = @calcovertimeafter " & _
              ", calcshorttimebefore = @calcshorttimebefore " & _
              ", fromhrs = @fromhrs " & _
              ", tohrs = @tohrs " & _
              ", isparttime = @isparttime" & _
              ", isactive = @isactive" & _
              ", shiftname1 = @shiftname1" & _
              ", shiftname2 = @shiftname2 " & _
            "WHERE shiftunkid = @shiftunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "tnashift_master", mintShiftunkid, "shiftunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "tnashift_master", "shiftunkid", mintShiftunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnashift_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Shift. Reason: This Shift is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "Update tnashift_master set isactive = 0 " & _
            "WHERE shiftunkid = @shiftunkid "

            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "tnashift_master", "shiftunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "Select isnull(shiftunkid,0) from hremployee_master where shiftunkid = @shiftunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = "Select isnull(shiftunkid,0) from tnalogin_summary where shiftunkid = @shiftunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shiftunkid " & _
              ", shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", starttime " & _
              ", endtime " & _
              ", breaktime " & _
              ", shiftdays " & _
              ", workinghours " & _
              ", calcovertimeafter " & _
              ", calcshorttimebefore " & _
              ", fromhrs " & _
              ", tohrs " & _
              ", isparttime " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2 " & _
             "FROM tnashift_master " & _
             "WHERE 1=1 "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND shiftcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND shiftname = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND shiftunkid <> @shiftunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shiftunkid " & _
              ", shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", starttime " & _
              ", endtime " & _
              ", breaktime " & _
              ", shiftdays " & _
              ", workinghours " & _
              ", calcovertimeafter " & _
              ", calcshorttimebefore " & _
              ", fromhrs " & _
              ", tohrs " & _
              ", isparttime " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2 " & _
             "FROM tnashift_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            'S.SANDEEP [ 27 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If mblFlag = True Then
            '    strQ = "SELECT 0 as shiftunkid, ' ' +  @name  as name UNION "
            'End If
            'strQ &= "SELECT shiftunkid, shiftname as name FROM tnashift_master where isactive = 1 ORDER BY name "

            If mblFlag = True Then
                strQ = "SELECT 0 as shiftunkid, ' ' +  @name as name, '' AS shifttype UNION "
            End If
            strQ &= "SELECT tnashift_master.shiftunkid, tnashift_master.shiftname as name, ISNULL(cfcommon_master.name,'') AS shifttype " & _
                    "FROM tnashift_master " & _
                    " JOIN cfcommon_master ON cfcommon_master.masterunkid = tnashift_master.shifttypeunkid " & _
                    "WHERE tnashift_master.isactive = 1 ORDER BY name "
            'S.SANDEEP [ 27 OCT 2012 ] -- END


            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function


    'Pinkal (25-Oct-2012) -- Start
    'Enhancement : TRA Changes

    'Public Function GetShiftWorkingHours(ByVal intShiftunkid As Integer) As Integer
    '    Dim Shifthours As Double = 0
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    objDataOperation = New clsDataOperation
    '    Try
    '        strQ = "SELECT isnull(workinghours,0) 'workinghours' FROM tnashift_master  where shiftunkid = @shiftunkid and isactive = 1"
    '        objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftunkid.ToString)

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "ShiftHours")
    '        If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
    '            Shifthours = CInt(dsList.Tables(0).Rows(0)("workinghours"))
    '        End If
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetShiftWorkingHours; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '    End Try
    '    Return Shifthours
    'End Function

    Public Function GetShiftWorkingHours(ByVal intShiftunkid As Integer, ByVal intDayId As Integer) As Integer
        Dim Shifthours As Double = 0
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT  ISNULL(tnashift_tran.workinghrs, 0) 'workinghours' " & _
                      " FROM tnashift_tran " & _
                      " JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                      " WHERE tnashift_tran.shiftunkid = @shiftunkid  AND tnashift_tran.dayid = @dayId " & _
                      " AND isactive = 1"

            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftunkid.ToString)
            objDataOperation.AddParameter("@dayId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDayId.ToString)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "ShiftHours")
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Shifthours = CInt(dsList.Tables(0).Rows(0)("workinghours"))
            End If
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList IsNot Nothing Then dsList.Dispose()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftWorkingHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return Shifthours
    End Function


    'Pinkal (25-Oct-2012) -- End

    'Pinkal (10-Mar-2011) -- Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetShiftUnkId(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " shiftunkid " & _
                      "  FROM tnashift_master " & _
                      " WHERE shiftname = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("shiftunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetShiftUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    'Pinkal (10-Mar-2011) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Shift Code is already defined. Please define new Shift Code.")
			Language.setMessage(mstrModuleName, 2, "This Shift Name is already defined. Please define new Shift Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class