﻿Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsshift_tran

    Private Shared ReadOnly mstrModuleName As String = "clsshift_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintShifttranunkid As Integer
    Private mintShiftunkid As Integer
    Private mdtShiftDays As DataTable = Nothing

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shifttranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shifttranunkid() As Integer
        Get
            Return mintShifttranunkid
        End Get
        Set(ByVal value As Integer)
            mintShifttranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftunkid() As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtshiftDays
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtShiftday() As DataTable
        Get
            Return mdtShiftDays
        End Get
        Set(ByVal value As DataTable)
            mdtShiftDays = value
        End Set
    End Property

#End Region

#Region "Methods"


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetShiftTran(Optional ByVal intShiftunkid As Integer = 0, Optional ByVal objDooperation As clsDataOperation = Nothing)
        'Pinkal (25-Jan-2018) --  'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.[Optional ByVal objDooperation As clsDataOperation = Nothing]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        'Pinkal (25-Jan-2018) -- Start
        'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
        If objDooperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDooperation
        End If
        'Pinkal (25-Jan-2018) -- End
        

        objDataOperation.ClearParameters()
        Try

            strQ = " SELECT  " & _
                       " tnashift_tran.shifttranunkid " & _
                       ",tnashift_tran.shiftunkid " & _
                       ",tnashift_tran.dayid " & _
                       ",'' AS DAYS " & _
                       ",tnashift_tran.isweekend " & _
                       ",tnashift_tran.starttime " & _
                       ",tnashift_tran.endtime " & _
                       ",(tnashift_tran.breaktime / 60) AS breaktime " & _
                       ",CONVERT(DECIMAL(5, 2), FLOOR(((tnashift_tran.workinghrs) / 60 )/ 60) + ( CONVERT(DECIMAL(5,2), ( tnashift_tran.workinghrs / 60 ) % 60)/ 100 )) AS workinghrs  " & _
                       ",(tnashift_tran.calcovertimeafter / 60) AS calcovertimeafter " & _
                       ",(tnashift_tran.calcshorttimebefore / 60)  AS calcshorttimebefore " & _
                       ",0.00 as halffromhrs " & _
                       ",0.00 as halftohrs " & _
                       ",tnashift_tran.halfdayfromhrs as halffromhrsinsec " & _
                       ",tnashift_tran.halfdaytohrs as halftohrsinsec " & _
                       ",tnashift_tran.nightfromhrs " & _
                       ",tnashift_tran.nighttohrs " & _
                       ",tnashift_tran.breaktime AS breaktimeinsec " & _
                       ",tnashift_tran.workinghrs AS workinghrsinsec " & _
                       ",tnashift_tran.calcovertimeafter AS calcovertimeafterinsec " & _
                       ",tnashift_tran.calcshorttimebefore AS calcshorttimebeforeinsec " & _
                       ",tnashift_tran.daystart_time " & _
                       " ,'' AS AUD " & _
                       ",ISNULL(tnashift_tran.countotmins_aftersftendtime/60,0) AS countotmins_aftersftendtime " & _
                       ",ISNULL(tnashift_tran.countotmins_aftersftendtime,0) AS countotmins_aftersftendtimeinsec " & _
                       ",ISNULL(tnashift_tran.isotholiday,0) AS isotholiday  " & _
                       " FROM tnashift_tran"


            'Pinkal (01-Apr-2020) -- ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. [",ISNULL(tnashift_tran.isotholiday,0) AS isotholiday  " & _]

            'Pinkal (03-Aug-2018) -- 'Enhancement - Changes For B5 [Ref #289] [   ",ISNULL(tnashift_tran.countotmins_aftersftendtime,0) AS countotmins_aftersftendtime " & _]

            If intShiftunkid > 0 Then
                strQ &= " WHERE tnashift_tran.Shiftunkid = @Shiftunkid"
                objDataOperation.AddParameter("@Shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")
            mdtShiftDays = dsList.Tables("List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (25-Jan-2018) -- Start
            'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
            If objDooperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (25-Jan-2018) -- End
        End Try

    End Sub

    Public Function InsertUpdateShiftDays(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If mdtShiftDays Is Nothing Then Return True

            For i = 0 To mdtShiftDays.Rows.Count - 1

                With mdtShiftDays.Rows(i)
                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")

                            Case "A"

                                strQ = " INSERT INTO tnashift_tran (" & _
                                              " shiftunkid " & _
                                              ",dayid " & _
                                              ",starttime" & _
                                              ",endtime " & _
                                              ",breaktime " & _
                                              ",workinghrs " & _
                                              ",calcovertimeafter " & _
                                              ",calcshorttimebefore " & _
                                              ",halfdayfromhrs " & _
                                              ",halfdaytohrs " & _
                                              ",nightfromhrs " & _
                                              ",nighttohrs " & _
                                              ",isweekend  " & _
                                              ",daystart_time " & _
                                              ",countotmins_aftersftendtime " & _
                                              ",isotholiday " & _
                                          ") VALUES (" & _
                                              " @shiftunkid " & _
                                              ",@dayid " & _
                                              ",@starttime" & _
                                              ",@endtime " & _
                                              ",@breaktime " & _
                                              ",@workinghrs " & _
                                              ",@calcovertimeafter " & _
                                              ",@calcshorttimebefore " & _
                                              ",@halfdayfromhrs " & _
                                              ",@halfdaytohrs " & _
                                              ",@nightfromhrs " & _
                                              ",@nighttohrs " & _
                                              ",@isweekend  " & _
                                              ",@daystart_time " & _
                                              ",@countotmins_aftersftendtime " & _
                                              ",@isotholiday " & _
                                              "); SELECT @@identity"


                                'Pinkal (01-Apr-2020) -- ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. [isotholiday]

                                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)
                                objDataOperation.AddParameter("@dayid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dayid").ToString)
                                objDataOperation.AddParameter("@starttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(.Item("starttime")), DBNull.Value, .Item("starttime").ToString))
                                objDataOperation.AddParameter("@endtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(.Item("endtime")), DBNull.Value, .Item("endtime").ToString))
                                objDataOperation.AddParameter("@breaktime", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("breaktimeinsec").ToString)
                                objDataOperation.AddParameter("@workinghrs", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("workinghrsinsec").ToString)
                                objDataOperation.AddParameter("@calcovertimeafter", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("calcovertimeafterinsec").ToString)
                                objDataOperation.AddParameter("@calcshorttimebefore", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("calcshorttimebeforeinsec").ToString)
                                objDataOperation.AddParameter("@halfdayfromhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("halffromhrsinsec").ToString)
                                objDataOperation.AddParameter("@halfdaytohrs", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("halftohrsinsec").ToString)
                                objDataOperation.AddParameter("@nightfromhrs", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("nightfromhrs").ToString)
                                objDataOperation.AddParameter("@nighttohrs", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("nighttohrs").ToString)
                                objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweekend").ToString)
                                objDataOperation.AddParameter("@daystart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(.Item("daystart_time")), DBNull.Value, .Item("daystart_time").ToString))

                                'Pinkal (03-Aug-2018) -- Start
                                'Enhancement - Changes For B5 [Ref #289]
                                objDataOperation.AddParameter("@countotmins_aftersftendtime", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countotmins_aftersftendtimeinsec").ToString)
                                'Pinkal (03-Aug-2018) -- End


                                'Pinkal (01-Apr-2020) -- Start
                                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                                objDataOperation.AddParameter("@isotholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isotholiday").ToString)
                                'Pinkal (01-Apr-2020) -- End


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintShifttranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("shiftunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "tnashift_master", "shiftunkid", .Item("shiftunkid"), "tnashift_tran", "shifttranunkid", mintShifttranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "tnashift_master", "shiftunkid", mintShiftunkid, "tnashift_tran", "shifttranunkid", mintShifttranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"

                                strQ = " UPDATE tnashift_tran SET " & _
                                          " shiftunkid = @shiftunkid" & _
                                          " ,dayid = @dayid" & _
                                          " ,starttime =  @starttime" & _
                                          " ,endtime = @endtime" & _
                                          " ,breaktime = @breaktime" & _
                                          " ,workinghrs = @workinghrs" & _
                                          " ,calcovertimeafter =  @calcovertimeafter" & _
                                          " ,calcshorttimebefore =  @calcshorttimebefore" & _
                                          " ,halfdayfromhrs = @halfdayfromhrs" & _
                                          " ,halfdaytohrs =  @halfdaytohrs" & _
                                          ",nightfromhrs = @nightfromhrs" & _
                                          " ,nighttohrs =  @nighttohrs" & _
                                          ",isweekend = @isweekend" & _
                                          ",daystart_time = @daystart_time" & _
                                          ",countotmins_aftersftendtime  = @countotmins_aftersftendtime " & _
                                          ",isotholiday  = @isotholiday " & _
                                          " WHERE shifttranunkid = @shifttranunkid "


                                'Pinkal (01-Apr-2020) -- ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. [ ",isotholiday  = @isotholiday " & _]


                                'Pinkal (03-Aug-2018) -- 'Enhancement - Changes For B5 [Ref #289][",countotmins_aftersftendtime  = @countotmins_aftersftendtime " & _]

                                objDataOperation.AddParameter("@shifttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("shifttranunkid").ToString)
                                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)
                                objDataOperation.AddParameter("@dayid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dayid").ToString)
                                objDataOperation.AddParameter("@starttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(.Item("starttime")), DBNull.Value, .Item("starttime").ToString))
                                objDataOperation.AddParameter("@endtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(.Item("endtime")), DBNull.Value, .Item("endtime").ToString))
                                objDataOperation.AddParameter("@breaktime", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("breaktimeinsec").ToString)
                                objDataOperation.AddParameter("@workinghrs", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("workinghrsinsec").ToString)
                                objDataOperation.AddParameter("@calcovertimeafter", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("calcovertimeafterinsec").ToString)
                                objDataOperation.AddParameter("@calcshorttimebefore", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("calcshorttimebeforeinsec").ToString)
                                objDataOperation.AddParameter("@halfdayfromhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("halffromhrsinsec").ToString)
                                objDataOperation.AddParameter("@halfdaytohrs", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("halftohrsinsec").ToString)
                                objDataOperation.AddParameter("@nightfromhrs", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("nightfromhrs").ToString)
                                objDataOperation.AddParameter("@nighttohrs", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("nighttohrs").ToString)
                                objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweekend").ToString)
                                objDataOperation.AddParameter("@daystart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(.Item("daystart_time")), DBNull.Value, .Item("daystart_time").ToString))

                                'Pinkal (03-Aug-2018) -- Start
                                'Enhancement - Changes For B5 [Ref #289]
                                objDataOperation.AddParameter("@countotmins_aftersftendtime", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countotmins_aftersftendtimeinsec").ToString)
                                'Pinkal (03-Aug-2018) -- End


                                'Pinkal (01-Apr-2020) -- Start
                                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                                objDataOperation.AddParameter("@isotholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isotholiday").ToString)
                                'Pinkal (01-Apr-2020) -- End


                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "tnashift_master", "shiftunkid", mintShiftunkid, "tnashift_tran", "shifttranunkid", CInt(.Item("shifttranunkid").ToString), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                        End Select

                    End If

                End With

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateShiftDays", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

End Class
