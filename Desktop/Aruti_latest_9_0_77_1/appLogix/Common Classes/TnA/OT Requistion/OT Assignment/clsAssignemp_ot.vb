﻿'************************************************************************************************************************************
'Class Name : clsassignemp_ot.vb
'Purpose    :
'Date       :04-Nov-2019
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsassignemp_ot
    Private Const mstrModuleName = "clsassignemp_ot"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTnaemployeeotunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrWebFormName As String = String.Empty
    Private mblnIsFromWeb As Boolean = False

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnaemployeeotunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tnaemployeeotunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTnaemployeeotunkid
        End Get
        Set(ByVal value As Integer)
            mintTnaemployeeotunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  tnaemployeeotunkid " & _
                      ", employeeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM tnaassignemp_ot " & _
                      " WHERE tnaemployeeotunkid = @tnaemployeeotunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaemployeeotunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnaemployeeotunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTnaemployeeotunkid = CInt(dtRow.Item("tnaemployeeotunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, ByVal xCompanyUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xUserUnkid As Integer _
                                    , ByVal mdtEmployeeAsonDate As Date, ByVal xUserModeSetting As String _
                                    , Optional ByVal xOnlyApproved As Boolean = True, Optional ByVal blnAddApprovalCondition As Boolean = True _
                                    , Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, Optional ByVal iEmployeeId As Integer = -1 _
                                    , Optional ByVal blnAllowUserAccess As Boolean = True, Optional ByVal mstrFilter As String = "" _
                                    , Optional ByVal mblnFlag As Boolean = False) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , blnExcludeTermEmp_PayProcess)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)
            If blnAllowUserAccess Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)


            If mblnFlag Then

                strQ = "SELECT " & _
                           "  0 AS tnaemployeeotunkid " & _
                           ",  ' ' + @select AS employeecode " & _
                           ", ' ' +  @select AS employee " & _
                           ", ' ' +  @select  AS EmpcodeName " & _
                           ", 0 AS employeeunkid " & _
                           ", 0 AS userunkid " & _
                           ", 0 AS isvoid " & _
                           ", NULL AS voiddatetime " & _
                           ", 0 AS voiduserunkid " & _
                           ", '' AS voidreason " & _
                           " UNION  "
            End If

            'Pinkal (08-Jan-2020) -- Enhancement - NMB - Working on NMB OT Requisition Requirement.[UNION ALL]

            strQ &= "SELECT " & _
                      "  tnaassignemp_ot.tnaemployeeotunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                      ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employee " & _
                      ", ISNULL(hremployee_master.employeecode,'') + ' - ' +  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')  AS EmpcodeName " & _
                      ", tnaassignemp_ot.employeeunkid " & _
                      ", tnaassignemp_ot.userunkid " & _
                      ", tnaassignemp_ot.isvoid " & _
                      ", tnaassignemp_ot.voiddatetime " & _
                      ", tnaassignemp_ot.voiduserunkid " & _
                      ", tnaassignemp_ot.voidreason " & _
                      " FROM tnaassignemp_ot " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaassignemp_ot.employeeunkid "


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If
            'Pinkal (03-Sep-2020) -- End

            If blnAllowUserAccess Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If
            'Pinkal (03-Sep-2020) -- End

            strQ &= " WHERE isvoid = 0 "

            If blnAllowUserAccess Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If iEmployeeId > 0 Then
                strQ &= " AND tnaassignemp_ot.employeeunkid = @employeeunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry
            End If

            strQ &= " ORDER BY EmpcodeName "

            objDataOperation.ClearParameters()
            If mblnFlag Then objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            If iEmployeeId > 0 Then objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnaassignemp_ot) </purpose>
    Public Function Insert(ByVal dtEmployee As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If dtEmployee.Rows.Count <= 0 Then Return True

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "INSERT INTO tnaassignemp_ot ( " & _
                 "  employeeunkid " & _
                 ", userunkid " & _
                 ", isvoid " & _
                 ", voiddatetime " & _
                 ", voiduserunkid " & _
                 ", voidreason" & _
               ") VALUES (" & _
                 "  @employeeunkid " & _
                 ", @userunkid " & _
                 ", @isvoid " & _
                 ", @voiddatetime " & _
                 ", @voiduserunkid " & _
                 ", @voidreason" & _
               "); SELECT @@identity"


            For Each dr As DataRow In dtEmployee.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")).ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

                If mdtVoiddatetime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If

                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintTnaemployeeotunkid = dsList.Tables(0).Rows(0).Item(0)
                mintEmployeeunkid = CInt(dr("employeeunkid"))

                If ATInsertAssignEmpOT(objDataOperation, 1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnaassignemp_ot) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = " UPDATE tnaassignemp_ot SET " & _
                       "  employeeunkid = @employeeunkid" & _
                       ", userunkid = @userunkid" & _
                       ", isvoid = @isvoid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidreason = @voidreason " & _
                       " WHERE isvoid =0 AND tnaemployeeotunkid = @tnaemployeeotunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaemployeeotunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnaemployeeotunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertAssignEmpOT(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnaassignemp_ot) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = " UPDATE tnaassignemp_ot SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiddatetime = GETDATE()" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE tnaemployeeotunkid = @tnaemployeeotunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaemployeeotunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (23 Apr 2020) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
            Dim xUserunkid As Integer = mintUserunkid
            'Hemant (23 Apr 2020) -- End

            _Tnaemployeeotunkid(objDataOperation) = intUnkid

            'Hemant (23 Apr 2020) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
            mintUserunkid = xUserunkid
            'Hemant (23 Apr 2020) -- End


            If ATInsertAssignEmpOT(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal xEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try


            strQ = " SELECT COUNT(*) TotalCount " & _
                         " FROM tnaot_requisition_tran " & _
                         " LEFT JOIN tnaotrequisition_approval_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid  " & _
                         " AND tnaotrequisition_approval_tran.isvoid = 0 AND tnaotrequisition_approval_tran.employeeunkid = @employeeunkid " & _
                         " WHERE tnaot_requisition_tran.isvoid = 0 AND tnaot_requisition_tran.employeeunkid = @employeeunkid  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "Used")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnIsUsed As Boolean = False
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0)("TotalCount")) > 0 Then blnIsUsed = True
            End If

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  tnaemployeeotunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM tnaassignemp_ot " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND tnaemployeeotunkid <> @tnaemployeeotunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@tnaemployeeotunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetAssignedEmployeeIds() As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeIds As String = ""
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                      " + CAST(tnaassignemp_ot.employeeunkid AS VARCHAR(MAX)) " & _
                      " FROM tnaassignemp_ot " & _
                      " WHERE tnaassignemp_ot.isvoid = 0 ORDER BY tnaassignemp_ot.employeeunkid " & _
                      " FOR XML PATH('')), 1, 1, ''), '') EmployeeIds "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssignedEmployeeIds; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mstrEmployeeIds
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function ATInsertAssignEmpOT(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO attnaassignemp_ot ( " & _
                      " tnaemployeeotunkid " & _
                      ", employeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @tnaemployeeotunkid " & _
                      ", @employeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", Getdate() " & _
                      ", @ip" & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ") "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaemployeeotunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnaemployeeotunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertAssignEmpOT; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

End Class