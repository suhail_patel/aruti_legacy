﻿'************************************************************************************************************************************
'Class Name : clsTnaotrequisition_approval_Tran.vb
'Purpose    :
'Date       :28-Jun-2019
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsTnaotrequisition_approval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsTnaotrequisition_approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintOtrequestapprovaltranunkid As Integer
    Private mintOtrequisitiontranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtApprovaldate As Date
    Private mintOttypeunkid As Integer
    Private mintPeriodunkid As Integer
    Private mdtActualstart_Time As Date
    Private mdtActualend_Time As Date
    Private mintActualot_Hours As Integer
    Private mstrAdjustment_Reason As String = String.Empty
    Private mintTnamappingunkid As Integer
    Private mintMapuserunkid As Integer
    Private mintTnalevelunkid As Integer
    Private mintTnapriority As Integer
    Private mintStatusunkid As Integer
    Private mblnIsfinalapprover As Boolean
    Private mintVisibleunkid As Integer
    Private mblnIscancel As Boolean
    Private mintCanceluserunkid As Integer
    Private mdtCanceldatetime As Date
    Private mstrCancelreason As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date

    Private mintAudittype As Integer = 0
    Private mintAudituserunkid As Integer = 0
    Private mdtAuditdatetime As Date = Nothing
    Private mstrClientIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean = False
    Private mintLoginemoployeeunkid As Integer = 0


    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.
    Private mintactuaNight_Hours As Integer
    'Pinkal (24-Oct-2019) -- End


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otrequestapprovaltranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Otrequestapprovaltranunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintOtrequestapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintOtrequestapprovaltranunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otrequisitiontranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Otrequisitiontranunkid() As Integer
        Get
            Return mintOtrequisitiontranunkid
        End Get
        Set(ByVal value As Integer)
            mintOtrequisitiontranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ottypeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Ottypeunkid() As Integer
        Get
            Return mintOttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintOttypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualstart_time
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Actualstart_Time() As Date
        Get
            Return mdtActualstart_Time
        End Get
        Set(ByVal value As Date)
            mdtActualstart_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualend_time
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Actualend_Time() As Date
        Get
            Return mdtActualend_Time
        End Get
        Set(ByVal value As Date)
            mdtActualend_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualot_hours
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Actualot_Hours() As Integer
        Get
            Return mintActualot_Hours
        End Get
        Set(ByVal value As Integer)
            mintActualot_Hours = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adjustment_reason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Adjustment_Reason() As String
        Get
            Return mstrAdjustment_Reason
        End Get
        Set(ByVal value As String)
            mstrAdjustment_Reason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnamappingunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tnamappingunkid() As Integer
        Get
            Return mintTnamappingunkid
        End Get
        Set(ByVal value As Integer)
            mintTnamappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnalevelunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tnalevelunkid() As Integer
        Get
            Return mintTnalevelunkid
        End Get
        Set(ByVal value As Integer)
            mintTnalevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnapriority
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tnapriority() As Integer
        Get
            Return mintTnapriority
        End Get
        Set(ByVal value As Integer)
            mintTnapriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalapprover
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isfinalapprover() As Boolean
        Get
            Return mblnIsfinalapprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalapprover = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Visibleunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Visibleunkid() As Integer
        Get
            Return mintVisibleunkid
        End Get
        Set(ByVal value As Integer)
            mintVisibleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Canceluserunkid() As Integer
        Get
            Return mintCanceluserunkid
        End Get
        Set(ByVal value As Integer)
            mintCanceluserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceldatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Canceldatetime() As Date
        Get
            Return mdtCanceldatetime
        End Get
        Set(ByVal value As Date)
            mdtCanceldatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Cancelreason() As String
        Get
            Return mstrCancelreason
        End Get
        Set(ByVal value As String)
            mstrCancelreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Audittype
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ClientIp() As String
        Get
            Return mstrClientIp
        End Get
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemoployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Loginemoployeeunkid() As Integer
        Get
            Return mintLoginemoployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemoployeeunkid = value
        End Set
    End Property


    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ActualNight_Hours
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ActualNight_Hours() As Integer
        Get
            Return mintactuaNight_Hours
        End Get
        Set(ByVal value As Integer)
            mintactuaNight_Hours = value
        End Set
    End Property

    'Pinkal (24-Oct-2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  otrequestapprovaltranunkid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", approvaldate " & _
                      ", ottypeunkid " & _
                      ", periodunkid " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", adjustment_reason " & _
                      ", tnamappingunkid " & _
                      ", mapuserunkid " & _
                      ", tnalevelunkid " & _
                      ", tnapriority " & _
                      ", statusunkid " & _
                      ", isfinalapprover " & _
                      ", visibleunkid " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", canceldatetime " & _
                      ", cancelreason " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", voiddatetime " & _
                      ", ISNULL(actualnight_hrs,0) AS actualnight_hrs " & _
                      " FROM tnaotrequisition_approval_tran " & _
                     " WHERE otrequestapprovaltranunkid = @otrequestapprovaltranunkid AND isvoid = 0 "


            'Pinkal (24-Oct-2019) -- 'Enhancement NMB - Working On OT Enhancement for NMB.[", ISNULL(actualnight_hrs,0) AS actualnight_hrs " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequestapprovaltranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintOtrequestapprovaltranunkid = CInt(dtRow.Item("otrequestapprovaltranunkid"))
                mintOtrequisitiontranunkid = CInt(dtRow.Item("otrequisitiontranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))

                If IsDBNull(dtRow.Item("approvaldate")) = False Then
                    mdtApprovaldate = dtRow.Item("approvaldate")
                End If

                mintOttypeunkid = CInt(dtRow.Item("ottypeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))

                If IsDBNull(dtRow.Item("actualstart_time")) = False Then
                    mdtActualstart_Time = dtRow.Item("actualstart_time")
                End If

                If IsDBNull(dtRow.Item("actualend_time")) = False Then
                    mdtActualend_Time = dtRow.Item("actualend_time")
                End If

                mintActualot_Hours = CInt(dtRow.Item("actualot_hours"))
                mstrAdjustment_Reason = dtRow.Item("adjustment_reason").ToString
                mintTnamappingunkid = CInt(dtRow.Item("tnamappingunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mintTnalevelunkid = CInt(dtRow.Item("tnalevelunkid"))
                mintTnapriority = CInt(dtRow.Item("tnapriority"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsfinalapprover = CBool(dtRow.Item("isfinalapprover"))
                mintVisibleunkid = CInt(dtRow.Item("visibleunkid"))
                mblnIscancel = CBool(dtRow.Item("iscancel"))
                mintCanceluserunkid = CInt(dtRow.Item("canceluserunkid"))

                If IsDBNull(dtRow.Item("canceldatetime")) = False Then
                    mdtCanceldatetime = dtRow.Item("canceldatetime")
                End If

                mstrCancelreason = dtRow.Item("cancelreason").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                'Pinkal (24-Oct-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                mintactuaNight_Hours = dtRow.Item("actualnight_hrs").ToString
                'Pinkal (24-Oct-2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String _
    '                                , ByVal xDatabaseName As String _
    '                                , ByVal mdtEmployeeAsOnDate As Date _
    '                                , ByVal xUserID As Integer _
    '                                , ByVal mblnMyApproval As Boolean _
    '                                , ByVal mdtStartDate As Date _
    '                                , ByVal mdtEndDate As Date _
    '                                , Optional ByVal objDOperation As clsDataOperation = Nothing _
    '                                , Optional ByVal blnOnlyActive As Boolean = True _
    '                                , Optional ByVal xOTRequisitionID As Integer = 0 _
    '                                , Optional ByVal mblnIsGrp As Boolean = False _
    '                                , Optional ByVal mstrFilter As String = "") As DataSet


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim StrQCondition As String = String.Empty
    '    Dim StrQCommonQry As String = String.Empty
    '    Dim dsCompany As New DataSet

    '    If objDOperation Is Nothing Then
    '        objDataOperation = New clsDataOperation
    '    Else
    '        objDataOperation = objDOperation
    '    End If
    '    objDataOperation.ClearParameters()

    '    Try


    '        strQ = "SELECT DISTINCT " & _
    '             "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
    '             "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
    '             "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
    '             "FROM tnaapprover_master " & _
    '             "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tnaapprover_master.mapuserunkid " & _
    '             "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
    '             "    LEFT JOIN " & _
    '             "    ( " & _
    '             "        SELECT " & _
    '             "             cfconfiguration.companyunkid " & _
    '             "            ,cfconfiguration.key_value " & _
    '             "        FROM hrmsConfiguration..cfconfiguration " & _
    '             "        WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
    '             "    ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
    '             "WHERE tnaapprover_master.isvoid = 0 AND tnaapprover_master.isswap = 0 AND tnaapprover_master.isexternalapprover = 1 "

    '        dsCompany = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)
    '        strQ = ""


    '        If mblnIsGrp Then
    '            strQ = " SELECT DISTINCT " & _
    '                      " CAST (0 AS bit) AS ischecked " & _
    '                       " , -1 AS otrequestapprovaltranunkid " & _
    '                       " , -1 AS otrequisitiontranunkid " & _
    '                       " , -1 AS periodunkid " & _
    '                       " , '' AS period " & _
    '                       " , tnaotrequisition_approval_tran.employeeunkid " & _
    '                       " , NULL AS requestdate " & _
    '                       " , CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
    '                       " , ' [ '  + @RequestDate + ' : '  + REPLACE(RTRIM(CONVERT(char(15),tnaot_requisition_tran.requestdate,106)),' ','-') + ' ]' + ' : ' + ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Particulars " & _
    '                       " , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee  " & _
    '                       ",  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
    '                       " , NULL AS approvaldate " & _
    '                       " , -1 AS tnamappingunkid " & _
    '                       " , -1 AS approveremployeeunkid " & _
    '                       " , '' AS Approver " & _
    '                       " , '' AS ApproverName " & _
    '                       " , NULL AS plannedstart_time " & _
    '                       " , NULL AS plannedend_time " & _
    '                       " , '' AS plannedot_hours " & _
    '                       " , 0 AS plannedot_hoursinsecs " & _
    '                       " , NULL AS actualstart_time " & _
    '                       " , NULL As actualend_time " & _
    '                       " , '' AS actualot_hours " & _
    '                       " , 0 AS actualot_hoursinsecs " & _
    '                       " , -1 AS OTRequisitionStatusId" & _
    '                       " , -1 as ApprovalStatusId " & _
    '                       " , -1 as visibleunkid " & _
    '                       " , '' AS status " & _
    '                       " , '' AS request_reason " & _
    '                       " , '' AS adjustment_reason " & _
    '                       " , -1 AS userunkid " & _
    '                       " , 0 AS isvoid " & _
    '                       " , -1 AS voiduserunkid " & _
    '                       " , NULL AS voiddatetime " & _
    '                       " , '' AS voidreason " & _
    '                       " , -1 AS canceluserunkid " & _
    '                       " , NULL AS canceldatetime " & _
    '                       " , '' AS cancelreason " & _
    '                       " , CAST (1 AS bit) AS IsGrp " & _
    '                       " ,-1 AS sectiongroupunkid " & _
    '                       " ,-1 AS unitgroupunkid " & _
    '                       " ,-1 AS teamunkid " & _
    '                       " ,-1 AS stationunkid " & _
    '                       " ,-1 AS deptgroupunkid " & _
    '                       " ,-1 AS departmentunkid " & _
    '                       " ,-1 AS sectionunkid " & _
    '                       " ,-1 AS unitunkid " & _
    '                       " ,-1 AS jobunkid " & _
    '                       " ,-1 AS classgroupunkid " & _
    '                       " ,-1 AS classunkid " & _
    '                       " ,-1 AS jobgroupunkid " & _
    '                       " ,-1 AS gradegroupunkid " & _
    '                       " ,-1 AS gradeunkid " & _
    '                       " ,-1 AS gradelevelunkid " & _
    '                       " ,-1 AS approveremployeeunkid " & _
    '                       " ,-1 AS mapuserunkid " & _
    '                       " ,-1 AS tnapriority " & _
    '                       " ,-1 AS tnalevelunkid " & _
    '                       " ,#ExValue# AS isexternalapprover " & _
    '                       " , '' AS actualnight_hrs " & _
    '                       " , 0 AS actualnight_hoursinsecs " & _
    '                       " ,0 AS TotalApprovedHrsinsecs " & _
    '                       " ,'00:00' AS TotalApprovedHrs " & _
    '                      " FROM  tnaot_requisition_tran  " & _
    '                      " LEFT JOIN tnaotrequisition_approval_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid AND tnaotrequisition_approval_tran.isvoid =  0 AND tnaotrequisition_approval_tran.iscancel = 0 " & _
    '                      " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = tnaot_requisition_tran.periodunkid " & _
    '                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaotrequisition_approval_tran.employeeunkid " & _
    '                      " LEFT JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid  " & _
    '                      " LEFT JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid	" & _
    '                      " #APPR_NAME_JOIN#  "

    '            'Pinkal (27-Feb-2020) -- Enhancement - Changes related To OT NMB Testing.[,0 AS TotalApprovedHrsinsecs ,'00:00' AS TotalApprovedHrs ]


    '            If blnOnlyActive Then
    '                strQ &= " WHERE  tnaot_requisition_tran.isvoid = 0  "
    '            End If

    '            If mstrFilter.Trim.Length > 0 Then
    '                strQ &= " AND " & mstrFilter
    '            End If

    '            If mblnMyApproval Then
    '                If xUserID > 0 Then
    '                    strQ &= " AND  tnaapprover_master.mapuserunkid = @userunkid "
    '                End If
    '            End If

    '            strQ &= " UNION "

    '        End If

    '        'Pinkal (07-Nov-2019) -- Enhancement NMB - Working On OT & Imprest Enhancement for NMB.[  ",  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _]


    '        strQ &= "SELECT DISTINCT " & _
    '                      " CAST (0 AS bit) AS ischecked " & _
    '                      " , tnaotrequisition_approval_tran.otrequestapprovaltranunkid " & _
    '                      " , tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
    '                      " , tnaotrequisition_approval_tran.periodunkid " & _
    '                      " , ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
    '                      " , tnaotrequisition_approval_tran.employeeunkid " & _
    '                      " , tnaot_requisition_tran.requestdate " & _
    '                      " , CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
    '                      " , '' AS Particulars " & _
    '                      " , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee  " & _
    '                      ",  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
    '                      " , tnaotrequisition_approval_tran.approvaldate " & _
    '                      " , tnaotrequisition_approval_tran.tnamappingunkid " & _
    '                      " , tnaapprover_master.approveremployeeunkid " & _
    '                      " , #APPR_NAME_VALUE# AS Approver " & _
    '                      " , #APPR_NAME# AS ApproverName " & _
    '                      " , tnaot_requisition_tran.plannedstart_time " & _
    '                      " , tnaot_requisition_tran.plannedend_time " & _
    '                      " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaot_requisition_tran.plannedot_hours  / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaot_requisition_tran.plannedot_hours % 3600) / 60 ), 2), '00:00') AS plannedot_hours " & _
    '                      " , tnaot_requisition_tran.plannedot_hours AS plannedot_hoursinsecs " & _
    '                      " , tnaotrequisition_approval_tran.actualstart_time " & _
    '                      " , tnaotrequisition_approval_tran.actualend_time " & _
    '                      " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaotrequisition_approval_tran.actualot_hours / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaotrequisition_approval_tran.actualot_hours  % 3600) / 60 ), 2), '00:00') AS actualot_hours " & _
    '                      " , tnaotrequisition_approval_tran.actualot_hours AS actualot_hoursinsecs " & _
    '                      " , tnaot_requisition_tran.statusunkid AS OTRequisitionStatusId" & _
    '                      " , tnaotrequisition_approval_tran.statusunkid as ApprovalStatusId " & _
    '                      " , tnaotrequisition_approval_tran.visibleunkid " & _
    '                      " , CASE WHEN tnaotrequisition_approval_tran.statusunkid = 1 THEN @Approve " & _
    '                      "            WHEN tnaotrequisition_approval_tran.statusunkid = 2 THEN @Pending  " & _
    '                      "            WHEN tnaotrequisition_approval_tran.statusunkid = 3 THEN @Reject " & _
    '                      "            WHEN tnaotrequisition_approval_tran.statusunkid = 6 THEN @Cancel End AS status " & _
    '                      " , tnaot_requisition_tran.request_reason " & _
    '                      " , tnaotrequisition_approval_tran.adjustment_reason " & _
    '                      " , tnaotrequisition_approval_tran.userunkid " & _
    '                      " , tnaotrequisition_approval_tran.isvoid " & _
    '                      " , tnaotrequisition_approval_tran.voiduserunkid " & _
    '                      " , tnaotrequisition_approval_tran.voiddatetime " & _
    '                      " , tnaotrequisition_approval_tran.voidreason " & _
    '                      " , tnaotrequisition_approval_tran.canceluserunkid " & _
    '                      " , tnaotrequisition_approval_tran.canceldatetime " & _
    '                      " , tnaotrequisition_approval_tran.cancelreason " & _
    '                      " , CAST (0 AS bit) AS IsGrp " & _
    '                      " , ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                      " , ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
    '                      " , ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
    '                      " , ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
    '                      " , ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
    '                      " , ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
    '                      " , ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
    '                      " , ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
    '                      " , ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
    '                      " , ISNULL(Alloc.classunkid,0) AS classunkid " & _
    '                      " , ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
    '                      " , ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
    '                      " , ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
    '                      " , ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
    '                      " , ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
    '                      " , tnaapprover_master.approveremployeeunkid " & _
    '                      " , tnaapprover_master.mapuserunkid " & _
    '                      " , tna_approverlevel_master.tnapriority " & _
    '                      " , tna_approverlevel_master.tnalevelunkid " & _
    '                      " , #ExValue#  AS isexternalapprover " & _
    '                      " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaotrequisition_approval_tran.actualnight_hrs / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaotrequisition_approval_tran.actualnight_hrs  % 3600) / 60 ), 2), '00:00') AS actualnight_hrs " & _
    '                      " , tnaotrequisition_approval_tran.actualnight_hrs AS actualnight_hoursinsecs " & _
    '                      " , ISNULL(B.TotalApproveHrs, 0) AS TotalApproveHrsinsecs " & _
    '                      " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), ISNULL(B.TotalApproveHrs, 0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(B.TotalApproveHrs, 0) % 3600) / 60), 2), '00:00') AS TotalApproveHrs " & _
    '                      " FROM tnaotrequisition_approval_tran " & _
    '                      " LEFT JOIN tnaot_requisition_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid AND tnaot_requisition_tran.isvoid = 0 " & _
    '                      " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = tnaotrequisition_approval_tran.periodunkid " & _
    '                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaotrequisition_approval_tran.employeeunkid " & _
    '                      " LEFT JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid " & _
    '                      " LEFT JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid	"

    '        'Pinkal (27-Feb-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.

    '        '[ISNULL(B.TotalApproveHrs, 0) AS TotalApproveHrsinsecs , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), ISNULL(B.TotalApproveHrs, 0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(B.TotalApproveHrs, 0) % 3600) / 60), 2), '00:00') AS TotalApproveHrs "]

    '        strQ &= " LEFT JOIN ( " & _
    '                    "                   SELECT  ota.employeeunkid " & _
    '                    "                     ,ota.tnamappingunkid " & _
    '                    "                     ,SUM(ISNULL(ota.actualot_hours, 0)) AS TotalApproveHrs  " & _
    '                    "                   FROM tnaotrequisition_approval_tran AS ota " & _
    '                    "                   JOIN tnaot_requisition_tran ot ON ot.otrequisitiontranunkid = ota.otrequisitiontranunkid AND ot.isvoid = 0 " & _
    '                    "                   WHERE ota.isvoid = 0	AND ot.statusunkid = 1 AND ota.iscancel = 0 "

    '        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '            strQ &= "               	AND CONVERT(CHAR(8), ot.requestdate, 112) >= @StartDate " & _
    '                         "                   AND CONVERT(CHAR(8), ot.requestdate, 112) <= @EndDate "
    '        End If

    '        strQ &= "                   GROUP BY ota.employeeunkid,ota.tnamappingunkid " & _
    '                     "                 ) AS B	ON B.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid AND B.employeeunkid = tnaotrequisition_approval_tran.employeeunkid "
    '        'Pinkal (27-Feb-2020) -- End


    '        strQ &= " #APPR_NAME_JOIN#  " & _
    '                      " LEFT JOIN " & _
    '                      "( " & _
    '                      "    SELECT " & _
    '                      "         stationunkid " & _
    '                      "        ,deptgroupunkid " & _
    '                      "        ,departmentunkid " & _
    '                      "        ,sectiongroupunkid " & _
    '                      "        ,sectionunkid " & _
    '                      "        ,unitgroupunkid " & _
    '                      "        ,unitunkid " & _
    '                      "        ,teamunkid " & _
    '                      "        ,classgroupunkid " & _
    '                      "        ,classunkid " & _
    '                      "        ,employeeunkid " & _
    '                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                      "    FROM hremployee_transfer_tran " & _
    '                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsOnDate " & _
    '                      " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
    '                      " LEFT JOIN " & _
    '                      "( " & _
    '                      "    SELECT " & _
    '                      "         jobunkid " & _
    '                      "        ,jobgroupunkid " & _
    '                      "        ,employeeunkid " & _
    '                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                      "    FROM hremployee_categorization_tran " & _
    '                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsOnDate " & _
    '                      ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
    '                      " JOIN " & _
    '                      " ( " & _
    '                      "    SELECT " & _
    '                      "         gradegroupunkid " & _
    '                      "        ,gradeunkid " & _
    '                      "        ,gradelevelunkid " & _
    '                      "        ,employeeunkid " & _
    '                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
    '                      "    FROM prsalaryincrement_tran " & _
    '                      "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= @EmployeeAsOnDate " & _
    '                      " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 "

    '        'Pinkal (07-Nov-2019) -- Enhancement NMB - Working On OT & Imprest Enhancement for NMB.[  ",  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _]

    '        'Pinkal (24-Oct-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[ ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaotrequisition_approval_tran.actualnight_hrs / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaotrequisition_approval_tran.actualnight_hrs  % 3600) / 60 ), 2), '00:00') AS actualnight_hrs  , tnaotrequisition_approval_tran.actualnight_hrs AS actualnight_hoursinsecs "

    '        StrQCommonQry = strQ

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            strQ &= xAdvanceJoinQry
    '        End If

    '        If blnOnlyActive Then
    '            StrQCondition &= " WHERE tnaotrequisition_approval_tran.isvoid  =  0 "
    '        End If

    '        If xOTRequisitionID > 0 Then
    '            StrQCondition &= " AND tnaotrequisition_approval_tran.otrequisitiontranunkid = @otrequisitiontranunkid "
    '            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTRequisitionID)
    '        End If

    '        StrQCondition &= " AND tnaapprover_master.isexternalapprover = #ExValue# AND hremployee_master.isapproved = 1 AND tnaotrequisition_approval_tran.iscancel = 0 "

    '        If mstrFilter.Trim.Length > 0 Then
    '            StrQCondition &= " AND " & mstrFilter
    '        End If

    '        'Pinkal (27-Feb-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.
    '        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '            StrQCondition &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) >= @StartDate  AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) <= @EndDate "
    '        End If
    '        'Pinkal (27-Feb-2020) -- End

    '        strQ &= StrQCondition

    '        strQ = strQ.Replace("#APPR_NAME_VALUE#", " ISNULL(App.firstname,'') + '  ' + ISNULL(App.othername,'') + '  ' + ISNULL(App.surname,'') + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'')")
    '        strQ = strQ.Replace("#APPR_NAME#", " ISNULL(App.firstname,'') + '  ' + ISNULL(App.othername,'') + '  ' + ISNULL(App.surname,'')")
    '        strQ = strQ.Replace("#APPR_NAME_JOIN#", "LEFT JOIN hremployee_master App ON App.employeeunkid = tnaapprover_master.approveremployeeunkid")
    '        strQ = strQ.Replace("#ExValue#", "0")

    '        'strQ &= " ORDER BY RDate, IsGrp DESC, otrequisitiontranunkid "
    '        strQ &= " ORDER BY RDate DESC, IsGrp DESC, ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') "

    '        objDataOperation.AddParameter("@RequestDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Request Date"))
    '        objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsOnDate))
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserID)
    '        objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '        objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))


    '        'Pinkal (27-Feb-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.
    '        objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '        objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '        'Pinkal (27-Feb-2020) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        For Each dr In dsCompany.Tables(0).Rows
    '            Dim dstemp As New DataSet
    '            Dim StrExJoin As String = String.Empty

    '            xDateJoinQry = "" : xAdvanceJoinQry = "" : xDateFilterQry = ""

    '            strQ = StrQCommonQry
    '            StrExJoin = " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = tnaapprover_master.mapuserunkid "
    '            If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
    '                strQ = strQ.Replace("#APPR_NAME_VALUE#", "ISNULL(UEmp.username,'')  + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'') ")
    '                strQ = strQ.Replace("#APPR_NAME#", "ISNULL(UEmp.username,'')")
    '                strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
    '            Else
    '                StrExJoin &= " JOIN " & dr("DName") & "..hremployee_master App on App.employeeunkid = UEmp.employeeunkid AND App.isapproved = 1"
    '                strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
    '                strQ = strQ.Replace("#APPR_NAME_VALUE#", "CASE WHEN ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') = '' THEN ISNULL(UEmp.username,'')  + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'')" & _
    '                                                              "ELSE ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'') END ")
    '                strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') = '' THEN ISNULL(UEmp.username,'') " & _
    '                                                              "ELSE ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') END ")
    '                If xAdvanceJoinQry.Trim.Length > 0 Then
    '                    strQ &= xAdvanceJoinQry
    '                End If

    '            End If

    '            strQ &= StrQCondition

    '            strQ = strQ.Replace("#ExValue#", "1")
    '            strQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

    '            objDataOperation.ClearParameters()

    '            If xOTRequisitionID > 0 Then
    '                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTRequisitionID)
    '            End If

    '            objDataOperation.AddParameter("@RequestDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Request Date"))
    '            objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserID)
    '            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))



    '            'Pinkal (03-Mar-2020) -- Start
    '            'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
    '            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '            'Pinkal (03-Mar-2020) -- End

    '            dstemp = objDataOperation.ExecQuery(strQ, "ExList")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables.Count <= 0 Then
    '                dsList.Tables(0).Merge(dstemp.Tables(0), True)
    '            Else
    '                Dim dtExtList As DataTable = New DataView(dstemp.Tables("ExList"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
    '                dstemp.Tables.RemoveAt(0)
    '                dstemp.Tables.Add(dtExtList.Copy)
    '                dsList.Tables(0).Merge(dstemp.Tables(0), True)
    '            End If

    '        Next


    '        Dim dtTable As DataTable

    '        'Pinkal (29-Jan-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.
    '        'dtTable = New DataView(dsList.Tables(0), "", "RDate DESC,employeeunkid,tnapriority", DataViewRowState.CurrentRows).ToTable.Copy
    '        dtTable = New DataView(dsList.Tables(0), "", "Employee,RDate DESC,employeeunkid,tnapriority", DataViewRowState.CurrentRows).ToTable.Copy
    '        'Pinkal (29-Jan-2020) -- End



    '        dsList.Tables.RemoveAt(0)
    '        dsList.Tables.Add(dtTable.Copy)


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        If objDOperation Is Nothing Then objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                                    , ByVal xDatabaseName As String _
                                    , ByVal mdtEmployeeAsOnDate As Date _
                                    , ByVal xUserID As Integer _
                                    , ByVal mblnMyApproval As Boolean _
                                    , ByVal mdtStartDate As Date _
                                    , ByVal mdtEndDate As Date _
                                    , Optional ByVal objDOperation As clsDataOperation = Nothing _
                                    , Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal xOTRequisitionID As Integer = 0 _
                                    , Optional ByVal mblnIsGrp As Boolean = False _
                                    , Optional ByVal mstrFilter As String = "") As DataSet


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim StrQCondition As String = String.Empty
        Dim StrQCommonQry As String = String.Empty
        Dim dsCompany As New DataSet

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()

        Try


            strQ = "SELECT DISTINCT " & _
                 "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                 "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                 "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                 "FROM tnaapprover_master " & _
                 "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tnaapprover_master.mapuserunkid " & _
                 "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                 "    LEFT JOIN " & _
                 "    ( " & _
                 "        SELECT " & _
                 "             cfconfiguration.companyunkid " & _
                 "            ,cfconfiguration.key_value " & _
                 "        FROM hrmsConfiguration..cfconfiguration " & _
                 "        WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                 "    ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                 "WHERE tnaapprover_master.isvoid = 0 AND tnaapprover_master.isswap = 0 AND tnaapprover_master.isexternalapprover = 1 "

            dsCompany = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)
            strQ = ""


            If mblnIsGrp Then
                strQ = " SELECT DISTINCT " & _
                          " CAST (0 AS bit) AS ischecked " & _
                           " , -1 AS otrequestapprovaltranunkid " & _
                           " , -1 AS otrequisitiontranunkid " & _
                           " , -1 AS periodunkid " & _
                           " , '' AS period " & _
                           " , tnaotrequisition_approval_tran.employeeunkid " & _
                           " , NULL AS requestdate " & _
                           " , CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
                           " , ' [ '  + @RequestDate + ' : '  + REPLACE(RTRIM(CONVERT(char(15),tnaot_requisition_tran.requestdate,106)),' ','-') + ' ]' + ' : ' + ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Particulars " & _
                           " , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee  " & _
                           ",  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
                           " , NULL AS approvaldate " & _
                           " , -1 AS tnamappingunkid " & _
                           " , -1 AS approveremployeeunkid " & _
                           " , '' AS Approver " & _
                           " , '' AS ApproverName " & _
                           " , NULL AS plannedstart_time " & _
                           " , NULL AS plannedend_time " & _
                           " , '' AS plannedot_hours " & _
                           " , 0 AS plannedot_hoursinsecs " & _
                           " , NULL AS actualstart_time " & _
                           " , NULL As actualend_time " & _
                           " , '' AS actualot_hours " & _
                           " , 0 AS actualot_hoursinsecs " & _
                           " , -1 AS OTRequisitionStatusId" & _
                           " , -1 as ApprovalStatusId " & _
                           " , -1 as visibleunkid " & _
                           " , '' AS status " & _
                           " , '' AS request_reason " & _
                           " , '' AS adjustment_reason " & _
                           " , -1 AS userunkid " & _
                           " , 0 AS isvoid " & _
                           " , -1 AS voiduserunkid " & _
                           " , NULL AS voiddatetime " & _
                           " , '' AS voidreason " & _
                           " , -1 AS canceluserunkid " & _
                           " , NULL AS canceldatetime " & _
                           " , '' AS cancelreason " & _
                           " , CAST (1 AS bit) AS IsGrp "

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= " ,-1 AS sectiongroupunkid " & _
                           " ,-1 AS unitgroupunkid " & _
                           " ,-1 AS teamunkid " & _
                           " ,-1 AS stationunkid " & _
                           " ,-1 AS deptgroupunkid " & _
                           " ,-1 AS departmentunkid " & _
                           " ,-1 AS sectionunkid " & _
                           " ,-1 AS unitunkid " & _
                           " ,-1 AS classgroupunkid " & _
                           " ,-1 AS classunkid " & _
                                " ,-1 AS jobunkid " & _
                           " ,-1 AS jobgroupunkid " & _
                           " ,-1 AS gradegroupunkid " & _
                           " ,-1 AS gradeunkid " & _
                                " ,-1 AS gradelevelunkid "
                End If

                strQ &= " ,-1 AS approveremployeeunkid " & _
                           " ,-1 AS mapuserunkid " & _
                           " ,-1 AS tnapriority " & _
                           " ,-1 AS tnalevelunkid " & _
                           " ,#ExValue# AS isexternalapprover " & _
                           " , '' AS actualnight_hrs " & _
                           " , 0 AS actualnight_hoursinsecs " & _
                           " ,0 AS TotalApprovedHrsinsecs " & _
                           " ,'00:00' AS TotalApprovedHrs " & _
                          " FROM  tnaot_requisition_tran  " & _
                          " LEFT JOIN tnaotrequisition_approval_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid AND tnaotrequisition_approval_tran.isvoid =  0 AND tnaotrequisition_approval_tran.iscancel = 0 " & _
                          " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = tnaot_requisition_tran.periodunkid " & _
                          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaotrequisition_approval_tran.employeeunkid " & _
                          " LEFT JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid AND tnaapprover_master.isvoid = 0  " & _
                          " LEFT JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid	" & _
                          " #APPR_NAME_JOIN#  "


                If blnOnlyActive Then
                    strQ &= " WHERE  tnaot_requisition_tran.isvoid = 0  "
                End If

                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.
                strQ &= " AND tnaapprover_master.isexternalapprover = #ExValue# AND hremployee_master.isapproved = 1 AND tnaotrequisition_approval_tran.iscancel = 0 "

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) >= @StartDate  AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) <= @EndDate "
                End If
                'Pinkal (12-Oct-2021)-- End

                If mblnMyApproval Then
                    If xUserID > 0 Then
                        strQ &= " AND  tnaapprover_master.mapuserunkid = @userunkid "
                    End If
                End If

                strQ &= " UNION "

            End If


            strQ &= "SELECT DISTINCT " & _
                          " CAST (0 AS bit) AS ischecked " & _
                          " , tnaotrequisition_approval_tran.otrequestapprovaltranunkid " & _
                          " , tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
                          " , tnaotrequisition_approval_tran.periodunkid " & _
                          " , ISNULL(cfcommon_period_tran.period_name,'') AS period " & _
                          " , tnaotrequisition_approval_tran.employeeunkid " & _
                          " , tnaot_requisition_tran.requestdate " & _
                          " , CONVERT(CHAR(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
                          " , '' AS Particulars " & _
                          " , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee  " & _
                          ",  ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
                          " , tnaotrequisition_approval_tran.approvaldate " & _
                          " , tnaotrequisition_approval_tran.tnamappingunkid " & _
                          " , tnaapprover_master.approveremployeeunkid " & _
                          " , #APPR_NAME_VALUE# AS Approver " & _
                          " , #APPR_NAME# AS ApproverName " & _
                          " , tnaot_requisition_tran.plannedstart_time " & _
                          " , tnaot_requisition_tran.plannedend_time " & _
                          " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaot_requisition_tran.plannedot_hours  / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaot_requisition_tran.plannedot_hours % 3600) / 60 ), 2), '00:00') AS plannedot_hours " & _
                          " , tnaot_requisition_tran.plannedot_hours AS plannedot_hoursinsecs " & _
                          " , tnaotrequisition_approval_tran.actualstart_time " & _
                          " , tnaotrequisition_approval_tran.actualend_time " & _
                          " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaotrequisition_approval_tran.actualot_hours / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaotrequisition_approval_tran.actualot_hours  % 3600) / 60 ), 2), '00:00') AS actualot_hours " & _
                          " , tnaotrequisition_approval_tran.actualot_hours AS actualot_hoursinsecs " & _
                          " , tnaot_requisition_tran.statusunkid AS OTRequisitionStatusId" & _
                          " , tnaotrequisition_approval_tran.statusunkid as ApprovalStatusId " & _
                          " , tnaotrequisition_approval_tran.visibleunkid " & _
                          " , ISNULL(App1.AppprovalStatus,'') As Status " & _
                          " , tnaot_requisition_tran.request_reason " & _
                          " , tnaotrequisition_approval_tran.adjustment_reason " & _
                          " , tnaotrequisition_approval_tran.userunkid " & _
                          " , tnaotrequisition_approval_tran.isvoid " & _
                          " , tnaotrequisition_approval_tran.voiduserunkid " & _
                          " , tnaotrequisition_approval_tran.voiddatetime " & _
                          " , tnaotrequisition_approval_tran.voidreason " & _
                          " , tnaotrequisition_approval_tran.canceluserunkid " & _
                          " , tnaotrequisition_approval_tran.canceldatetime " & _
                          " , tnaotrequisition_approval_tran.cancelreason " & _
                          " , CAST (0 AS bit) AS IsGrp "


            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            '" , CASE WHEN tnaotrequisition_approval_tran.statusunkid = 1 THEN @Approve " & _
            '             "            WHEN tnaotrequisition_approval_tran.statusunkid = 2 THEN @Pending  " & _
            '             "            WHEN tnaotrequisition_approval_tran.statusunkid = 3 THEN @Reject " & _
            '             "            WHEN tnaotrequisition_approval_tran.statusunkid = 6 THEN @Cancel End AS status " & _
            'Pinkal (12-Oct-2021)-- End

            If xAdvanceJoinQry.Trim.Length > 0 Then

                strQ &= " , ISNULL(ADF.sectiongroupunkid,0) AS sectiongroupunkid " & _
                              " , ISNULL(ADF.unitgroupunkid,0) AS unitgroupunkid " & _
                              " , ISNULL(ADF.teamunkid,0) AS teamunkid " & _
                              " , ISNULL(ADF.stationunkid,0) AS stationunkid " & _
                              " , ISNULL(ADF.deptgroupunkid,0) AS deptgroupunkid " & _
                              " , ISNULL(ADF.departmentunkid,0) AS departmentunkid " & _
                              " , ISNULL(ADF.sectionunkid,0) AS sectionunkid " & _
                              " , ISNULL(ADF.unitunkid,0) AS unitunkid " & _
                              " , ISNULL(ADF.classgroupunkid,0) AS classgroupunkid " & _
                              " , ISNULL(ADF.classunkid,0) AS classunkid " & _
                              " , ISNULL(ADF.jobunkid,0) AS jobunkid " & _
                              " , ISNULL(ADF.jobgroupunkid,0) AS jobgroupunkid " & _
                              " , ISNULL(ADF.gradegroupunkid,0) AS gradegroupunkid " & _
                              " , ISNULL(ADF.gradeunkid,0) AS gradeunkid " & _
                              " , ISNULL(ADF.gradelevelunkid,0) AS gradelevelunkid "
            End If

            strQ &= " , tnaapprover_master.approveremployeeunkid " & _
                          " , tnaapprover_master.mapuserunkid " & _
                          " , tna_approverlevel_master.tnapriority " & _
                          " , tna_approverlevel_master.tnalevelunkid " & _
                          " , #ExValue#  AS isexternalapprover " & _
                          " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaotrequisition_approval_tran.actualnight_hrs / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaotrequisition_approval_tran.actualnight_hrs  % 3600) / 60 ), 2), '00:00') AS actualnight_hrs " & _
                          " , tnaotrequisition_approval_tran.actualnight_hrs AS actualnight_hoursinsecs " & _
                          " , ISNULL(B.TotalApproveHrs, 0) AS TotalApprovedHrsinsecs " & _
                          " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), ISNULL(B.TotalApproveHrs, 0) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (ISNULL(B.TotalApproveHrs, 0) % 3600) / 60), 2), '00:00') AS TotalApprovedHrs " & _
                          " FROM tnaotrequisition_approval_tran " & _
                          " LEFT JOIN tnaot_requisition_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid AND tnaot_requisition_tran.isvoid = 0 " & _
                          " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = tnaotrequisition_approval_tran.periodunkid " & _
                          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaotrequisition_approval_tran.employeeunkid " & _
                          " LEFT JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid AND tnaapprover_master.isvoid = 0 " & _
                          " LEFT JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid	"


            strQ &= " LEFT JOIN ( " & _
                        "                   SELECT  ota.employeeunkid " & _
                        "                     ,ota.tnamappingunkid " & _
                        "                     ,SUM(ISNULL(ota.actualot_hours, 0)) AS TotalApproveHrs  " & _
                        "                   FROM tnaotrequisition_approval_tran AS ota " & _
                        "                   JOIN tnaot_requisition_tran ot ON ot.otrequisitiontranunkid = ota.otrequisitiontranunkid AND ot.isvoid = 0 " & _
                        "                   WHERE ota.isvoid = 0	AND ot.statusunkid = 1 AND ota.iscancel = 0 "

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                strQ &= "               	AND CONVERT(CHAR(8), ot.requestdate, 112) >= @StartDate " & _
                             "                   AND CONVERT(CHAR(8), ot.requestdate, 112) <= @EndDate "
            End If

                strQ &= "                   GROUP BY ota.employeeunkid,ota.tnamappingunkid " & _
                             "                 ) AS B	ON B.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid AND B.employeeunkid = tnaotrequisition_approval_tran.employeeunkid "


            strQ &= " #APPR_NAME_JOIN#  "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            strQ &= " LEFT JOIN ( " & _
                         "                     SELECT " & _
                         "                          tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
                         "                         ,tnaotrequisition_approval_tran.tnamappingunkid " & _
                         "                         ,tna_approverlevel_master.tnapriority " & _
                         "                         ,tnaotrequisition_approval_tran.statusunkid " & _
                         "                         ,ROW_NUMBER()OVER(PARTITION BY tnaotrequisition_approval_tran.otrequisitiontranunkid,tna_approverlevel_master.tnapriority ORDER BY tnaotrequisition_approval_tran.tnapriority) AS rno " & _
                         "                         ,CASE WHEN tnaotrequisition_approval_tran.statusunkid = 1 THEN  @ApprovedBy + ' ' + ISNULL(cuser.username, '') " & _
                         "                                   WHEN tnaotrequisition_approval_tran.statusunkid = 2 THEN  @Pending " & _
                         "                                   WHEN tnaotrequisition_approval_tran.statusunkid = 3 THEN  @RejectedBy + ' ' + ISNULL(cuser.username, '') " & _
                         "                          END AS 'AppprovalStatus' " & _
                         "                     FROM tnaotrequisition_approval_tran " & _
                         "                     LEFT JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid AND tnaotrequisition_approval_tran.tnalevelunkid = tnaapprover_master.tnalevelunkid  AND tnaapprover_master.isexternalapprover = #ExValue# " & _
                         "                     LEFT JOIN hrmsConfiguration..cfuser_master AS cuser	ON cuser.userunkid = tnaapprover_master.mapuserunkid " & _
                         "                     LEFT JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid " & _
                         "                     WHERE tnaotrequisition_approval_tran.isvoid = 0 And tnaotrequisition_approval_tran.iscancel = 0 " & _
                         "              ) AS App1 ON App1.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
                         " AND App1.tnapriority = tna_approverlevel_master.tnapriority and App1.rno = 1 "

            'Pinkal (12-Oct-2021)-- End


            StrQCommonQry = strQ


            If blnOnlyActive Then
                StrQCondition &= " WHERE tnaotrequisition_approval_tran.isvoid  =  0 "
            End If

            If xOTRequisitionID > 0 Then
                StrQCondition &= " AND tnaotrequisition_approval_tran.otrequisitiontranunkid = @otrequisitiontranunkid "
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTRequisitionID)
            End If

            StrQCondition &= " AND tnaapprover_master.isexternalapprover = #ExValue# AND hremployee_master.isapproved = 1 AND tnaotrequisition_approval_tran.iscancel = 0 "

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                StrQCondition &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) >= @StartDate  AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) <= @EndDate "
            End If

            strQ &= StrQCondition

            strQ = strQ.Replace("#APPR_NAME_VALUE#", " ISNULL(App.firstname,'') + '  ' + ISNULL(App.othername,'') + '  ' + ISNULL(App.surname,'') + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'')")
            strQ = strQ.Replace("#APPR_NAME#", " ISNULL(App.firstname,'') + '  ' + ISNULL(App.othername,'') + '  ' + ISNULL(App.surname,'')")
            strQ = strQ.Replace("#APPR_NAME_JOIN#", "LEFT JOIN hremployee_master App ON App.employeeunkid = tnaapprover_master.approveremployeeunkid")
            strQ = strQ.Replace("#ExValue#", "0")

            strQ &= " ORDER BY RDate DESC, IsGrp DESC, ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') "

            objDataOperation.AddParameter("@RequestDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Request Date"))
            objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsOnDate))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserID)
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

            objDataOperation.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved By :"))
            objDataOperation.AddParameter("@RejectedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected By :"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                Dim StrExJoin As String = String.Empty

                xDateJoinQry = "" : xAdvanceJoinQry = "" : xDateFilterQry = ""

                strQ = StrQCommonQry
                StrExJoin = " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = tnaapprover_master.mapuserunkid "
                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    strQ = strQ.Replace("#APPR_NAME_VALUE#", "ISNULL(UEmp.username,'')  + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'') ")
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(UEmp.username,'')")
                    strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
                Else
                    StrExJoin &= " JOIN " & dr("DName") & "..hremployee_master App on App.employeeunkid = UEmp.employeeunkid AND App.isapproved = 1"
                    strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
                    strQ = strQ.Replace("#APPR_NAME_VALUE#", "CASE WHEN ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') = '' THEN ISNULL(UEmp.username,'')  + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'')" & _
                                                                  "ELSE ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') + ' - ' + ISNULL(tna_approverlevel_master.tnalevelname,'') END ")
                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') = '' THEN ISNULL(UEmp.username,'') " & _
                                                                  "ELSE ISNULL(App.firstname,'') + ' ' + ISNULL(App.surname,'') END ")
                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                End If

                strQ &= StrQCondition

                strQ = strQ.Replace("#ExValue#", "1")
                strQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                objDataOperation.ClearParameters()

                If xOTRequisitionID > 0 Then
                    objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTRequisitionID)
                End If

                objDataOperation.AddParameter("@RequestDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Request Date"))
                objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsOnDate))
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserID)
                objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
                objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
                objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

                objDataOperation.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved By :"))
                objDataOperation.AddParameter("@RejectedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected By :"))

                dstemp = objDataOperation.ExecQuery(strQ, "ExList")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                Else
                    Dim dtExtList As DataTable = New DataView(dstemp.Tables("ExList"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
                    dstemp.Tables.RemoveAt(0)
                    dstemp.Tables.Add(dtExtList.Copy)
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next


            Dim dtTable As DataTable

            dtTable = New DataView(dsList.Tables(0), "", "Employee,RDate DESC,employeeunkid,tnapriority", DataViewRowState.CurrentRows).ToTable.Copy

            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    'Pinkal (12-Oct-2021)-- End



    '' <summary>
    '' Modify By: Pinkal Jariwala
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (tnaotrequisition_approval_tran) </purpose>
    'Public Function Insert(ByVal xDatabaseName As String, ByVal xCompanyID As Integer, ByVal xYearID As Integer _
    '                              , ByVal xUserAccessMode As String, ByVal xPrivilegeID As Integer, ByVal xEmployeeAsonDate As String _
    '                              , ByVal dtOTRequisition As DataTable) As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        If dtOTRequisition Is Nothing Then Return False


    '        strQ = "INSERT INTO tnaotrequisition_approval_tran ( " & _
    '                   "  otrequisitiontranunkid " & _
    '                   ", employeeunkid " & _
    '                   ", approvaldate " & _
    '                   ", ottypeunkid " & _
    '                   ", periodunkid " & _
    '                   ", actualstart_time " & _
    '                   ", actualend_time " & _
    '                   ", actualot_hours " & _
    '                   ", adjustment_reason " & _
    '                   ", tnamappingunkid " & _
    '                   ", mapuserunkid " & _
    '                   ", tnalevelunkid " & _
    '                   ", tnapriority " & _
    '                   ", statusunkid " & _
    '                   ", isfinalapprover " & _
    '                   ", visibleunkid " & _
    '                   ", iscancel " & _
    '                   ", canceluserunkid " & _
    '                   ", canceldatetime " & _
    '                   ", cancelreason " & _
    '                   ", userunkid " & _
    '                   ", isvoid " & _
    '                   ", voiduserunkid " & _
    '                   ", voidreason " & _
    '                   ", voiddatetime" & _
    '                 ") VALUES (" & _
    '                   "  @otrequisitiontranunkid " & _
    '                   ", @employeeunkid " & _
    '                   ", @approvaldate " & _
    '                   ", @ottypeunkid " & _
    '                   ", @periodunkid " & _
    '                   ", @actualstart_time " & _
    '                   ", @actualend_time " & _
    '                   ", @actualot_hours " & _
    '                   ", @adjustment_reason " & _
    '                   ", @tnamappingunkid " & _
    '                   ", @mapuserunkid " & _
    '                   ", @tnalevelunkid " & _
    '                   ", @tnapriority " & _
    '                   ", @statusunkid " & _
    '                   ", @isfinalapprover " & _
    '                   ", @visibleunkid " & _
    '                   ", @iscancel " & _
    '                   ", @canceluserunkid " & _
    '                   ", @canceldatetime " & _
    '                   ", @cancelreason " & _
    '                   ", @userunkid " & _
    '                   ", @isvoid " & _
    '                   ", @voiduserunkid " & _
    '                   ", @voidreason " & _
    '                   ", @voiddatetime" & _
    '                 "); SELECT @@identity"

    '        For Each dr As DataRow In dtOTRequisition.Rows
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("otrequisitiontranunkid").ToString())
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("employeeunkid").ToString())
    '            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString())
    '            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString())
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("periodunkid").ToString())

    '            If mintStatusunkid = 1 Then  'Approved
    '                objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualstart_time")))
    '                objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualend_time")))
    '                objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("actualot_hoursInSec").ToString())
    '            ElseIf mintStatusunkid = 3 Then  'Rejected
    '                objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '                objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
    '            End If

    '            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
    '            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("tnamappingunkid").ToString())
    '            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("mapuserunkid").ToString())
    '            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("tnalevelunkid").ToString())
    '            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("tnapriority").ToString())
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '            objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalapprover.ToString)
    '            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
    '            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
    '            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
    '            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
    '            objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelreason.ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            _Otrequestapprovaltraunkid(objDataOperation) = dsList.Tables(0).Rows(0).Item(0)

    '            If AtInsertApprovalTran(objDataOperation, 1) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If


    '            If mintStatusunkid = 1 Then 'Approved

    '                'Dim dtTable As DataTable = GetNextEmployeeApprovers(xDatabaseName, xCompanyID, xYearID, xUserAccessMode _
    '                '                                                                                     , xPrivilegeID, xEmployeeAsonDate, CInt(dr("tnapriority")), -1, objDataOperation _
    '                '                                                                                      , "TA.approverStatusId = 2 AND TA.requisitionStatusId = 2", True, dr("employeeunkid").ToString())

    '                'Dim dtTable As DataTable = IsApproverPresent(xDatabaseName, xUserAccessMode, xCompanyID, xYearID _
    '                '                                                                  , xPrivilegeID, mintUserunkid, CInt(dr("tnapriority")), eZeeDate.convertDate(CDate(dr("actualstart_time"))).ToString() _
    '                '                                                                  , CInt(dr("employeeunkid")), objDataOperation)

    '                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then

    '                    '/* START TO UPDATE FINAL APPROVER BOOLEAN TO TRUE
    '                    mblnIsfinalapprover = True
    '                    If Update(objDataOperation) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    '/* END TO UPDATE FINAL APPROVER BOOLEAN TO TRUE

    '                    Dim objOTRequistion As New clsOT_Requisition_Tran
    '                    objOTRequistion._Otrequisitiontranunkid(objDataOperation) = CInt(dr("otrequisitiontranunkid").ToString())
    '                    objOTRequistion._Actualstart_Time = CDate(dr("actualstart_time"))
    '                    objOTRequistion._Actualend_Time = CDate(dr("actualend_time"))
    '                    objOTRequistion._Actualot_Hours = CInt(dr("actualot_hoursInSec"))
    '                    objOTRequistion._Adjustment_Reason = mstrAdjustment_Reason
    '                    objOTRequistion._Statusunkid = mintStatusunkid
    '                    objOTRequistion._Finalmappedapproverunkid = CInt(dr("tnamappingunkid"))


    '                    objOTRequistion._Userunkid = mintUserunkid
    '                    objOTRequistion._WebFormName = mstrForm_Name
    '                    objOTRequistion._WebHostName = mstrHostname
    '                    objOTRequistion._WebClientIP = mstrClientIp
    '                    objOTRequistion._IsFromWeb = mblnIsweb

    '                    If objOTRequistion.Update(objDataOperation) = False Then
    '                        objDataOperation.ReleaseTransaction(True)
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    objOTRequistion = Nothing
    '                End If

    '            ElseIf mintStatusunkid = 3 Then 'Rejected
    '                Dim objOTRequistion As New clsOT_Requisition_Tran
    '                objOTRequistion._Otrequisitiontranunkid(objDataOperation) = CInt(dr("otrequisitiontranunkid").ToString())
    '                objOTRequistion._Statusunkid = mintStatusunkid

    '                objOTRequistion._Userunkid = mintUserunkid
    '                objOTRequistion._WebFormName = mstrForm_Name
    '                objOTRequistion._WebHostName = mstrHostname
    '                objOTRequistion._WebClientIP = mstrClientIp
    '                objOTRequistion._IsFromWeb = mblnIsweb

    '                If objOTRequistion.Update(objDataOperation) = False Then
    '                    objDataOperation.ReleaseTransaction(True)
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                objOTRequistion = Nothing
    '            End If

    '        Next

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function Insert(ByVal objDoOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try

            strQ = "INSERT INTO tnaotrequisition_approval_tran ( " & _
                       "  otrequisitiontranunkid " & _
                       ", employeeunkid " & _
                       ", approvaldate " & _
                       ", ottypeunkid " & _
                       ", periodunkid " & _
                       ", actualstart_time " & _
                       ", actualend_time " & _
                       ", actualot_hours " & _
                       ", adjustment_reason " & _
                       ", actualnight_hrs " & _
                       ", tnamappingunkid " & _
                       ", mapuserunkid " & _
                       ", tnalevelunkid " & _
                       ", tnapriority " & _
                       ", statusunkid " & _
                       ", isfinalapprover " & _
                       ", visibleunkid " & _
                       ", iscancel " & _
                       ", canceluserunkid " & _
                       ", canceldatetime " & _
                       ", cancelreason " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime" & _
                     ") VALUES (" & _
                       "  @otrequisitiontranunkid " & _
                       ", @employeeunkid " & _
                       ", @approvaldate " & _
                       ", @ottypeunkid " & _
                       ", @periodunkid " & _
                       ", @actualstart_time " & _
                       ", @actualend_time " & _
                       ", @actualot_hours " & _
                       ", @adjustment_reason " & _
                       ", @actualnight_hrs " & _
                       ", @tnamappingunkid " & _
                       ", @mapuserunkid " & _
                       ", @tnalevelunkid " & _
                       ", @tnapriority " & _
                       ", @statusunkid " & _
                       ", @isfinalapprover " & _
                       ", @visibleunkid " & _
                       ", @iscancel " & _
                       ", @canceluserunkid " & _
                       ", @canceldatetime " & _
                       ", @cancelreason " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                     "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString())
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalapprover.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
            objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours)
            'Pinkal (24-Oct-2019) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Otrequestapprovaltranunkid(objDataOperation) = dsList.Tables(0).Rows(0).Item(0)

            If AtInsertApprovalTran(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'If mintStatusunkid = 1 Then 'Approved

            'If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then

            '    '/* START TO UPDATE FINAL APPROVER BOOLEAN TO TRUE
            '    mblnIsfinalapprover = True
            '    If Update(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            '    '/* END TO UPDATE FINAL APPROVER BOOLEAN TO TRUE

            '    Dim objOTRequistion As New clsOT_Requisition_Tran
            '    objOTRequistion._Otrequisitiontranunkid(objDataOperation) = CInt(dr("otrequisitiontranunkid").ToString())
            '    objOTRequistion._Actualstart_Time = CDate(dr("actualstart_time"))
            '    objOTRequistion._Actualend_Time = CDate(dr("actualend_time"))
            '    objOTRequistion._Actualot_Hours = CInt(dr("actualot_hoursInSec"))
            '    objOTRequistion._Adjustment_Reason = mstrAdjustment_Reason
            '    objOTRequistion._Statusunkid = mintStatusunkid
            '    objOTRequistion._Finalmappedapproverunkid = CInt(dr("tnamappingunkid"))


            '    objOTRequistion._Userunkid = mintUserunkid
            '    objOTRequistion._WebFormName = mstrForm_Name
            '    objOTRequistion._WebHostName = mstrHostname
            '    objOTRequistion._WebClientIP = mstrClientIp
            '    objOTRequistion._IsFromWeb = mblnIsweb

            '    If objOTRequistion.Update(objDataOperation) = False Then
            '        objDataOperation.ReleaseTransaction(True)
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    objOTRequistion = Nothing
            'End If

            'ElseIf mintStatusunkid = 3 Then 'Rejected
            'Dim objOTRequistion As New clsOT_Requisition_Tran
            'objOTRequistion._Otrequisitiontranunkid(objDataOperation) = CInt(dr("otrequisitiontranunkid").ToString())
            'objOTRequistion._Statusunkid = mintStatusunkid

            'objOTRequistion._Userunkid = mintUserunkid
            'objOTRequistion._WebFormName = mstrForm_Name
            'objOTRequistion._WebHostName = mstrHostname
            'objOTRequistion._WebClientIP = mstrClientIp
            'objOTRequistion._IsFromWeb = mblnIsweb

            'If objOTRequistion.Update(objDataOperation) = False Then
            '    objDataOperation.ReleaseTransaction(True)
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'objOTRequistion = Nothing
            'End If

            If objDoOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnaotrequisition_approval_tran) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequestapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalapprover.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
            objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))


            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours.ToString)
            'Pinkal (24-Oct-2019) -- End


            strQ = "UPDATE tnaotrequisition_approval_tran SET " & _
                      "  otrequisitiontranunkid = @otrequisitiontranunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", approvaldate = @approvaldate" & _
                      ", ottypeunkid = @ottypeunkid" & _
                      ", periodunkid = @periodunkid" & _
                      ", actualstart_time = @actualstart_time" & _
                      ", actualend_time = @actualend_time" & _
                      ", actualot_hours = @actualot_hours" & _
                      ", adjustment_reason = @adjustment_reason" & _
                      ", actualnight_hrs = @actualnight_hrs" & _
                      ", tnamappingunkid = @tnamappingunkid" & _
                      ", mapuserunkid = @mapuserunkid" & _
                      ", tnalevelunkid = @tnalevelunkid" & _
                      ", tnapriority = @tnapriority" & _
                      ", statusunkid = @statusunkid" & _
                      ", isfinalapprover = @isfinalapprover" & _
                      ", visibleunkid = @visibleunkid" & _
                      ", iscancel = @iscancel" & _
                      ", canceluserunkid = @canceluserunkid" & _
                      ", canceldatetime = @canceldatetime" & _
                      ", cancelreason = @cancelreason" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason" & _
                      ", voiddatetime = @voiddatetime " & _
                      " WHERE otrequestapprovaltranunkid = @otrequestapprovaltranunkid "


            'Pinkal (24-Oct-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[", actualnight_hrs = @actualnight_hrs" & _]


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If AtInsertApprovalTran(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnaotrequisition_approval_tran) </purpose>
    Public Function UpdateGlobalData(ByVal xDatabaseName As String, ByVal xYearID As Integer, ByVal xUserID As Integer _
                                                   , ByVal xCompanyID As Integer, ByVal xIncludeInActiveEmployee As Boolean _
                                                   , ByVal dtApproval As DataTable, ByVal mdtEmployeeAsonDate As Date _
                                                   , ByVal mblnIncludeCapApprover As Boolean, ByVal xStartDate As Date, ByVal xEndDate As Date) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If dtApproval Is Nothing OrElse dtApproval.Rows.Count <= 0 Then
            Return False
        End If

        Dim xTempRequisitionId As Integer = 0
        Dim xApprovingOTHrs As Double = 0
        Dim mintEmpTotalactualot_hours As Integer = 0
        Dim objOTRequisitionTran As New clsOT_Requisition_Tran
        Dim objConfig As New clsConfigOptions
        Dim intTotalMins As Integer = 0
        Dim strCapOTHrsForHODApprovers As String = objConfig.GetKeyValue(xCompanyID, "CapOTHrsForHODApprovers", Nothing)
        If strCapOTHrsForHODApprovers.Length > 0 Then
            Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
            If arrCapOTHrsForHODApprovers.Length > 0 Then
                intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
            End If
        End If

        Dim mstrArutiSelfServiceURL As String = objConfig.GetKeyValue(xCompanyID, "ArutiSelfServiceURL", Nothing)

        objConfig = Nothing


        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            Dim strQUpdate As String = "UPDATE tnaotrequisition_approval_tran SET " & _
                      "  otrequisitiontranunkid = @otrequisitiontranunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", approvaldate = @approvaldate" & _
                      ", ottypeunkid = @ottypeunkid" & _
                      ", periodunkid = @periodunkid" & _
                      ", actualstart_time = @actualstart_time" & _
                      ", actualend_time = @actualend_time" & _
                      ", actualot_hours = @actualot_hours" & _
                      ", adjustment_reason = @adjustment_reason" & _
                      ", actualnight_hrs = @actualnight_hrs" & _
                      ", tnamappingunkid = @tnamappingunkid" & _
                      ", mapuserunkid = @mapuserunkid" & _
                      ", tnalevelunkid = @tnalevelunkid" & _
                      ", tnapriority = @tnapriority" & _
                      ", statusunkid = @statusunkid" & _
                      ", isfinalapprover = @isfinalapprover" & _
                      ", visibleunkid = @visibleunkid" & _
                      ", iscancel = @iscancel" & _
                      ", canceluserunkid = @canceluserunkid" & _
                      ", canceldatetime = @canceldatetime" & _
                      ", cancelreason = @cancelreason" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason" & _
                      ", voiddatetime = @voiddatetime " & _
                      " WHERE otrequestapprovaltranunkid = @otrequestapprovaltranunkid "


            For Each dr As DataRow In dtApproval.Rows


                'If mintEmployeeunkid <> CInt(dr("employeeunkid")) Then
                '    mintEmpTotalactualot_hours = CInt(dr("actualot_hoursinsecs"))
                'Else
                '    mintEmpTotalactualot_hours += CInt(dr("actualot_hoursinsecs"))
                'End If
                If mintEmployeeunkid <> CInt(dr("employeeunkid")) Then
                    xTempRequisitionId = 0
                    xApprovingOTHrs = 0
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("otrequestapprovaltranunkid").ToString())
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("otrequisitiontranunkid").ToString())
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("employeeunkid").ToString())
                If mintStatusunkid <> 3 Then
                    objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
                Else
                    objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualstart_time")))
                objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualend_time")))
                objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("actualot_hoursinsecs").ToString())
                objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("tnamappingunkid").ToString)
                objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
                objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("tnalevelunkid").ToString)
                objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("tnapriority").ToString)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalapprover.ToString)
                objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
                objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
                objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelreason.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("actualnight_hoursinsecs").ToString())


                Call objDataOperation.ExecNonQuery(strQUpdate)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                _Otrequestapprovaltranunkid(objDataOperation) = CInt(dr("otrequestapprovaltranunkid"))

                If AtInsertApprovalTran(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mblnIncludeCapApprover = False

                If mintStatusunkid = 1 Then

                    If strCapOTHrsForHODApprovers.Trim.Length > 0 Then

                        'Pinkal (29-Jan-2020) -- Start
                        'Enhancement - Changes related To OT NMB Testing.
                        'Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisitionTran.GetEmpTotalOThours(mintPeriodunkid, mintEmployeeunkid.ToString(), objDataOperation, False, True))
                        Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisitionTran.GetEmpTotalOThours(xStartDate, xEndDate, mintEmployeeunkid.ToString(), objDataOperation, False, True))
                        'Pinkal (29-Jan-2020) -- End

                        If xTempRequisitionId > 0 Then
                            xApprovingOTHrs += CalculateTime(False, dtApproval.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = mintEmployeeunkid And x.Field(Of Integer)("OTRequisitionStatusId") = 2 And x.Field(Of Integer)("otrequisitiontranunkid") = xTempRequisitionId).DefaultIfEmpty().Min(Function(x) x.Field(Of Integer)("actualot_hoursinsecs")))
                        End If

                        'If intAppliedOThours <= 0 AndAlso (intTotalMins <= CalculateTime(False, CInt(dr("actualot_hoursinsecs")))) Then
                        '    mblnIncludeCapApprover = True
                        'ElseIf intTotalMins <= (intAppliedOThours + CalculateTime(False, CInt(dr("actualot_hoursinsecs")))) Then
                        '    mblnIncludeCapApprover = True
                        'End If

                        If intAppliedOThours <= 0 AndAlso (intTotalMins <= CalculateTime(False, CInt(dr("actualot_hoursinsecs")))) Then
                            mblnIncludeCapApprover = True
                        ElseIf intTotalMins <= (intAppliedOThours + xApprovingOTHrs + CalculateTime(False, CInt(dr("actualot_hoursinsecs")))) Then
                            mblnIncludeCapApprover = True
                        End If

                    End If

                    If mblnIncludeCapApprover Then  'Only For Approved

                        Dim objTnaapprover_master As New clsTnaapprover_master
                        Dim dtApprover As DataTable = objTnaapprover_master.GetEmployeeApprover(xDatabaseName, xYearID, xCompanyID, mdtEmployeeAsonDate, False, -1 _
                                                                                          , CInt(dr("employeeunkid")), -1, Nothing, False, "tnaapprover_master.isotcap_hod = 1", mblnIncludeCapApprover)

                        If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                            Dim mintOriginalTnAPriority As Integer = mintTnapriority
                            Dim mintOriginalStatusId As Integer = mintStatusunkid
                            For Each drRow As DataRow In dtApprover.Rows
                                If isExist(CInt(dr("otrequisitiontranunkid")), CInt(drRow("tnamappingunkid")), CInt(drRow("mapuserunkid")), CInt(drRow("tnalevelunkid")), objDataOperation) = False Then
                                    mintTnamappingunkid = CInt(drRow("tnamappingunkid"))
                                    mintMapuserunkid = CInt(drRow("mapuserunkid"))
                                    mintTnalevelunkid = CInt(drRow("tnalevelunkid"))
                                    mintTnapriority = CInt(drRow("tnapriority"))
                                    mintStatusunkid = 2
                                    mintVisibleunkid = -1
                                    mdtApprovaldate = Nothing
                                    If Insert(objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If 'If Insert(objDataOperation) = False Then

                                End If '  If isExist(CInt(dr("otrequisitiontranunkid")), CInt(drRow("tnamappingunkid")), CInt(drRow("mapuserunkid")), CInt(drRow("tnalevelunkid")), objDataOperation) = False Then
                            Next

                            mintTnapriority = mintOriginalTnAPriority
                            mintStatusunkid = mintOriginalStatusId

                        End If ' If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                        objTnaapprover_master = Nothing
                    End If ' If mblnIncludeCapApprover AndAlso mintStatusunkid = 1 Then  'Only For Approved

                End If  ' If mintStatusunkid = 1 Then

                'Pinkal (23-Nov-2019) -- End


                strQ = " SELECT tnaotrequisition_approval_tran.otrequestapprovaltranunkid,tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
                         " ,tnaapprover_master.tnamappingunkid, tnaapprover_master.approveremployeeunkid,tna_approverlevel_master.tnapriority,tnaotrequisition_approval_tran.visibleunkid " & _
                         " FROM tnaotrequisition_approval_tran  " & _
                         " JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid =  tnaotrequisition_approval_tran.tnamappingunkid AND tnaapprover_master.isvoid = 0 " & _
                         " JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid " & _
                         " WHERE tnaotrequisition_approval_tran.isvoid = 0 AND tnaotrequisition_approval_tran.otrequisitiontranunkid = @otrequisitiontranunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("otrequisitiontranunkid").ToString))
                Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                    strQ = " UPDATE tnaotrequisition_approval_tran SET " & _
                              " visibleunkid = @visibleunkid " & _
                              ", actualstart_time = @actualstart_time" & _
                              ", actualend_time = @actualend_time" & _
                              ", actualot_hours = @actualot_hours" & _
                              ", actualnight_hrs = @actualnight_hrs" & _
                              " WHERE  otrequisitiontranunkid = @otrequisitiontranunkid and employeeunkid = @employeeunkid " & _
                              " AND tnamappingunkid = @tnamappingunkid AND isvoid = 0 AND iscancel = 0  "

                    Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "visibleunkid <> 1 AND tnapriority >= " & mintTnapriority, "", DataViewRowState.CurrentRows).ToTable


                    'Pinkal (23-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT Enhancement for NMB.
                    Dim mintRemainingMinPriority As Integer = mintTnapriority
                    If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then
                        'Pinkal (27-Feb-2020) -- Start
                        'Enhancement - Changes related To OT NMB Testing.
                        If dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleunkid") <> 1 And x.Field(Of Integer)("tnapriority") > mintTnapriority).Count > 0 Then
                            mintRemainingMinPriority = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleunkid") <> 1 And x.Field(Of Integer)("tnapriority") > mintTnapriority).Min(Function(x) x.Field(Of Integer)("tnapriority"))
                        End If
                        'Pinkal (27-Feb-2020) -- End
                    End If
                    'Pinkal (23-Nov-2019) -- End

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("otrequisitiontranunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString()))
                        objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("tnamappingunkid").ToString)
                        objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualstart_time").ToString))
                        objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualend_time").ToString()))
                        objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("actualot_hoursinsecs").ToString()))

                        'Pinkal (24-Oct-2019) -- Start
                        'Enhancement NMB - Working On OT Enhancement for NMB.
                        objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("actualnight_hoursinsecs").ToString()))
                        'Pinkal (24-Oct-2019) -- End


                        If mintTnapriority <> CInt(dtVisibility.Rows(i)("tnapriority")) Then
                            If mintRemainingMinPriority = CInt(dtVisibility.Rows(i)("tnapriority")) Then
                                objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 2)
                            Else
                                objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                            End If
                        Else
                            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
                        End If

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next


                    Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(tnapriority)", "tnapriority > " & mintTnapriority)), -1, dsApprover.Tables(0).Compute("Min(tnapriority)", "tnapriority > " & mintTnapriority))

                    dtVisibility = New DataView(dsApprover.Tables(0), "tnapriority = " & intMinPriority & " AND tnapriority <> -1", "", DataViewRowState.CurrentRows).ToTable

                    If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then


                        If mintStatusunkid = 1 Then 'Approved
                            strQ = " UPDATE tnaotrequisition_approval_tran set " & _
                                      " visibleunkid = 2 " & _
                                      " WHERE  otrequisitiontranunkid = @otrequisitiontranunkid and employeeunkid = @employeeunkid  " & _
                                      " AND tnamappingunkid = @tnamappingunkid AND isvoid = 0 AND iscancel = 0  "

                        ElseIf mintStatusunkid = 3 Then 'Rejected 
                            strQ = " UPDATE tnaotrequisition_approval_tran set " & _
                                      " visibleunkid = -1  " & _
                                      " WHERE  otrequisitiontranunkid = @otrequisitiontranunkid and employeeunkid = @employeeunkid  " & _
                                      " AND tnamappingunkid = @tnamappingunkid AND isvoid = 0 AND iscancel = 0  "
                        End If

                        If mintStatusunkid = 1 OrElse mintStatusunkid = 3 Then
                            For i As Integer = 0 To dtVisibility.Rows.Count - 1
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("otrequisitiontranunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString()))
                                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("tnamappingunkid").ToString)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Next
                        End If

                        mdtActualstart_Time = CDate(dr("actualstart_time"))
                        mdtActualend_Time = CDate(dr("actualend_time"))
                        mintActualot_Hours = CInt(dr("actualot_hoursinsecs"))

                        'Pinkal (24-Oct-2019) -- Start
                        'Enhancement NMB - Working On OT Enhancement for NMB.
                        mintactuaNight_Hours = CInt(dr("actualnight_hoursinsecs"))
                        'Pinkal (24-Oct-2019) -- End

                        mintOtrequisitiontranunkid = CInt(dr("otrequisitiontranunkid"))


                    End If  ' If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then


                    'Dim drRow() As DataRow = dsApprover.Tables(0).Select("tnapriority > " & mintTnapriority, "")

                    'If drRow.Length > 0 Then

                    '    strQ = " UPDATE tnaotrequisition_approval_tran set " & _
                    '                " actualstart_time = @actualstart_time" & _
                    '                ", actualend_time = @actualend_time" & _
                    '                ", actualot_hours = @actualot_hours" & _
                    '                " WHERE  otrequisitiontranunkid = @otrequisitiontranunkid and employeeunkid = @employeeunkid  " & _
                    '                " AND tnamappingunkid = @tnamappingunkid AND isvoid = 0 AND iscancel =0 "


                    '    For i As Integer = 0 To drRow.Length - 1

                    '        objDataOperation.ClearParameters()
                    '        objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("otrequisitiontranunkid").ToString)
                    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid").ToString()))
                    '        objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(i)("tnamappingunkid").ToString)
                    '        objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualstart_time").ToString))
                    '        objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr("actualend_time").ToString()))
                    '        objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("actualot_hoursinsecs").ToString()))
                    '        objDataOperation.ExecNonQuery(strQ)

                    '        If objDataOperation.ErrorMessage <> "" Then
                    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '            Throw exForce
                    '        End If

                    '    Next

                    'End If




                End If  ' If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then


                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                'If UpdateOTRequisitionFinalStatus(objDataOperation, xDatabaseName, xYearID, xUserID, xCompanyID, CDate(dr("requestdate")).Date, xIncludeInActiveEmployee, CBool(dr("isexternalapprover")), CInt(dr("tnapriority")), CInt(dr("tnamappingunkid"))) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If
                Dim xStatusId As Integer = 2
                If UpdateOTRequisitionFinalStatus(objDataOperation, xDatabaseName, xYearID, xUserID, xCompanyID, CDate(dr("requestdate")).Date, xIncludeInActiveEmployee _
                                                                , CBool(dr("isexternalapprover")), CInt(dr("tnapriority")), CInt(dr("tnamappingunkid")), mblnIncludeCapApprover, xStatusId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                dr("OTRequisitionStatusId") = xStatusId
                dr.AcceptChanges()

                'Pinkal (23-Nov-2019) -- End

                If xStatusId = 2 Then
                    xTempRequisitionId = CInt(dr("otrequisitiontranunkid"))
                Else
                    xTempRequisitionId = 0
                End If

            Next

            '/*  START TO SEND EMAIL TO EMPLOYEE WHEN OT REQUISITION IS APPROVED / REJECTED.
            If dtApproval IsNot Nothing AndAlso dtApproval.Rows.Count > 0 Then

                'Pinkal (27-Feb-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                'Dim strEmployeeIDs As String = String.Join(",", dtApproval.AsEnumerable().Where(Function(x) x.Field(Of Integer)("OTRequisitionStatusId") = mintStatusunkid And x.Field(Of Integer)("OTRequisitionStatusId") <> 2).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                Dim strEmployeeIDs As String = String.Join(",", dtApproval.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                'Pinkal (27-Feb-2020) -- End

                Dim mstrPendingEmpIds As String = String.Join(",", dtApproval.AsEnumerable().Where(Function(x) x.Field(Of Integer)("OTRequisitionStatusId") = 2).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())

                Dim xTnaPriority As Integer = -1
                Dim dtTable As DataTable = dtApproval.Clone
                If mstrPendingEmpIds.Trim.Length > 0 Then
                    For Each strEmpId In mstrPendingEmpIds.Split(CChar(","))
                        Dim intEmployeeID As Integer = CInt(strEmpId)
                        Dim drRow = dtApproval.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID)
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                            For i As Integer = 0 To drRow.Count - 1
                                dtTable.ImportRow(drRow(i))
                            Next
                            dtTable.AcceptChanges()
                        End If
                    Next

                End If


                If strEmployeeIDs.Trim.Length > 0 Then

                    Dim mintLoginTypeID As Integer = 0
                    Dim mintLoginEmployeeID As Integer = 0
                    Dim mintUserID As Integer = 0

                    If mintUserunkid > 0 Then
                        mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                        mintLoginEmployeeID = 0
                    End If

                    objOTRequisitionTran._Userunkid = mintUserunkid
                    objOTRequisitionTran._WebHostName = mstrHostname
                    objOTRequisitionTran._WebClientIP = mstrClientIp
                    objOTRequisitionTran._WebFormName = mstrForm_Name
                    objOTRequisitionTran._IsFromWeb = mblnIsweb

                    Dim arPendingEmp() As String = Nothing

                    If mstrPendingEmpIds.Trim.Length > 0 Then
                        arPendingEmp = mstrPendingEmpIds.Split(CChar(","))
                    End If


                    For Each strEmpID As String In strEmployeeIDs.Split(CChar(","))

                        Dim intEmployeeID As Integer = CInt(strEmpID)

                        If arPendingEmp IsNot Nothing AndAlso arPendingEmp.Length > 0 Then

                            If Array.FindIndex(arPendingEmp, Function(x) x = intEmployeeID) >= 0 Then

                                Dim intAppliedOThours As Double = CalculateTime(False, objOTRequisitionTran.GetEmpTotalOThours(xStartDate.Date, xEndDate.Date, intEmployeeID, objDataOperation, False, True))

                                Dim xNotificationApprovingOTHrs As Double = CalculateTime(False, dtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID And x.Field(Of Integer)("OTRequisitionStatusId") = 2).DefaultIfEmpty().Sum(Function(x) x.Field(Of Integer)("actualot_hoursinsecs")))

                                If intTotalMins < (intAppliedOThours + xNotificationApprovingOTHrs) Then
                                    mblnIncludeCapApprover = True
                                Else
                                    mblnIncludeCapApprover = False
                                End If

                                objOTRequisitionTran.Send_Notification_Approver(xDatabaseName, xYearID, xCompanyID, mdtEmployeeAsonDate, False _
                                                                                                        , CStr(intEmployeeID), xStartDate.Date, xEndDate.Date, mintTnapriority _
                                                                                                        , dtTable, mintLoginTypeID, 0, mintUserunkid, mstrArutiSelfServiceURL _
                                                                                                        , True, mblnIncludeCapApprover, mstrAdjustment_Reason)

                                Continue For
                            End If

                        End If

                        objOTRequisitionTran.Send_Notification_Employee(xDatabaseName, xYearID, xCompanyID, mdtEmployeeAsonDate, False, "", _
                                                                                   CStr(intEmployeeID), "", xStartDate.Date, xEndDate.Date, mintStatusunkid, _
                                                                                   mintLoginTypeID, 0, mintUserunkid, mstrAdjustment_Reason)

                    Next


                End If

            End If

            '/*  END TO SEND EMAIL TO EMPLOYEE WHEN OT REQUISITION IS APPROVED / REJECTED.


            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objOTRequisitionTran = Nothing
            'Pinkal (23-Nov-2019) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateGlobalData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnaotrequisition_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "DELETE FROM tnaotrequisition_approval_tran " & _
            '"WHERE otrequestapprovaltranunkid = @otrequestapprovaltranunkid "

            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xOTRequisitionId As Integer, ByVal xOTApproverId As Integer, ByVal xMapUserId As Integer _
                                  , ByVal xOTLevelId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  otrequestapprovaltranunkid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", approvaldate " & _
                      ", ottypeunkid " & _
                      ", periodunkid " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", adjustment_reason " & _
                     ", actualnight_hrs " & _
                      ", tnamappingunkid " & _
                      ", mapuserunkid " & _
                      ", tnalevelunkid " & _
                      ", tnapriority " & _
                      ", statusunkid " & _
                      ", isfinalapprover " & _
                      ", visibleunkid " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", canceldatetime " & _
                      ", cancelreason " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", voiddatetime " & _
                      " FROM tnaotrequisition_approval_tran " & _
                     " WHERE isvoid = 0 AND otrequisitiontranunkid = @otrequisitiontranunkid " & _
                     " AND tnamappingunkid = @tnamappingunkid AND mapuserunkid = @mapuserunkid AND tnalevelunkid = @tnalevelunkid  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTRequisitionId)
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTApproverId)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserId)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTLevelId)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnaotrequisition_approval_tran) </purpose>
    Public Function AtInsertApprovalTran(ByVal objDataOperation As clsDataOperation, ByVal xAuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid().ToString())
            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequestapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalapprover.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@canceldatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtCanceldatetime <> Nothing, mdtCanceldatetime, DBNull.Value))
            objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelreason.ToString)
            'Hemant (23 Apr 2020) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
            'objDataOperation.AddParameter("@loginemplopyeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemoployeeunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemoployeeunkid)
            'Hemant (23 Apr 2020) -- End
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)


            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours.ToString)
            'Pinkal (24-Oct-2019) -- End



            strQ = "INSERT INTO attnaotrequisition_approval_tran ( " & _
                      "  tranguid " & _
                      ", otrequestapprovaltranunkid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", approvaldate " & _
                      ", ottypeunkid " & _
                      ", periodunkid " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", adjustment_reason " & _
                      ", actualnight_hrs " & _
                      ", tnamappingunkid " & _
                      ", mapuserunkid " & _
                      ", tnalevelunkid " & _
                      ", tnapriority " & _
                      ", statusunkid " & _
                      ", isfinalapprover " & _
                      ", visibleunkid " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", canceldatetime " & _
                      ", cancelreason " & _
                      ", loginemployeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                       ", isweb" & _
                    ") VALUES (" & _
                      "  @tranguid " & _
                      ", @otrequestapprovaltranunkid " & _
                      ", @otrequisitiontranunkid " & _
                      ", @employeeunkid " & _
                      ", @approvaldate " & _
                      ", @ottypeunkid " & _
                      ", @periodunkid " & _
                      ", @actualstart_time " & _
                      ", @actualend_time " & _
                      ", @actualot_hours " & _
                      ", @adjustment_reason " & _
                      ", @actualnight_hrs " & _
                      ", @tnamappingunkid " & _
                      ", @mapuserunkid " & _
                      ", @tnalevelunkid " & _
                      ", @tnapriority " & _
                      ", @statusunkid " & _
                      ", @isfinalapprover " & _
                      ", @visibleunkid " & _
                      ", @iscancel " & _
                      ", @canceluserunkid " & _
                      ", @canceldatetime " & _
                      ", @cancelreason " & _
                      ", @loginemployeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @hostname " & _
                      ", @form_name " & _
                      ", @isweb" & _
                    "); "

            'Hemant (23 Apr 2020) -- [loginemplopyeeunkid --> loginemployeeunkid]
            'Pinkal (24-Oct-2019) -- 'Enhancement NMB - Working On OT Enhancement for NMB.[actualnight_hrs]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AtInsertApprovalTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateOTRequisitionFinalStatus(ByVal objDataOperation As clsDataOperation, ByVal xDatabaseName As String _
                                                                                   , ByVal xYearID As Integer, ByVal xUserID As Integer, ByVal xCompanyID As Integer _
                                                                                   , ByVal mdtEmpAsonDate As Date, ByVal xIncludeInActiveEmployee As Boolean _
                                                                                   , ByVal mblnIsExternalApprover As Boolean, ByVal mintTnAPriority As Integer _
                                                                                   , ByVal xApproverID As Integer, ByVal mblnIncludeCapApprover As Boolean _
                                                                                   , ByRef xStatusId As Integer) As Boolean

        'Pinkal (23-Nov-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[ByVal mblnIncludeCapApprover As Boolean]

        Dim exForce As Exception
        Try
            If mintStatusunkid <> 3 AndAlso mintStatusunkid <> 6 Then  'Rejected And Cancelled

                Dim objApprover As New clsTnaapprover_master

                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.

                'Dim dtApprover As DataTable = objApprover.GetEmployeeApprover(xDatabaseName, xYearID, xCompanyID, mdtEmpAsonDate, xIncludeInActiveEmployee _
                '                                                                                                , -1, mintEmployeeunkid, -1, objDataOperation, mblnIsExternalApprover _
                '                                                                                                , "tnapriority > " & mintTnAPriority)


                Dim dtApprover As DataTable = objApprover.GetEmployeeApprover(xDatabaseName, xYearID, xCompanyID, mdtEmpAsonDate, xIncludeInActiveEmployee _
                                                                                                                , -1, mintEmployeeunkid, -1, objDataOperation, mblnIsExternalApprover _
                                                                                                                , "tnapriority > " & mintTnAPriority, mblnIncludeCapApprover)

                'Pinkal (23-Nov-2019) -- End


                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count <= 0 Then

                    Dim objOTRequistion As New clsOT_Requisition_Tran
                    objOTRequistion._Otrequisitiontranunkid(objDataOperation) = mintOtrequisitiontranunkid
                    objOTRequistion._Actualstart_Time = mdtActualstart_Time
                    objOTRequistion._Actualend_Time = mdtActualend_Time
                    objOTRequistion._Actualot_Hours = mintActualot_Hours
                    objOTRequistion._Adjustment_Reason = mstrAdjustment_Reason
                    objOTRequistion._Finalmappedapproverunkid = xApproverID
                    objOTRequistion._Statusunkid = 1
                    objOTRequistion._WebClientIP = mstrClientIp
                    objOTRequistion._WebFormName = mstrForm_Name
                    objOTRequistion._WebHostName = mstrHostname
                    objOTRequistion._IsFromWeb = mblnIsweb
                    objOTRequistion._ActualNight_Hours = mintactuaNight_Hours
                    objOTRequistion._Userunkid = xUserID
                    'Hemant (23 Apr 2020) -- Start
                    'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                    objOTRequistion._Loginemployeeunkid = mintLoginemoployeeunkid
                    'Hemant (23 Apr 2020) -- End


                    If objOTRequistion.UpdateOTRequisitionStatus(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Pinkal (29-Jan-2020) -- Start
                    'Enhancement - Changes related To OT NMB Testing.
                    Dim objOTRequisitionProcess As New clstnaotrequisition_process_Tran
                    objOTRequisitionProcess._Otrequestapprovaltranunkid = mintOtrequestapprovaltranunkid
                    objOTRequisitionProcess._Otrequisitiontranunkid = mintOtrequisitiontranunkid
                    objOTRequisitionProcess._Employeeunkid = mintEmployeeunkid
                    objOTRequisitionProcess._Requestdate = objOTRequistion._Requestdate
                    objOTRequisitionProcess._Actualstart_Time = mdtActualstart_Time
                    objOTRequisitionProcess._Actualend_Time = mdtActualend_Time
                    objOTRequisitionProcess._Actualot_Hours = mintActualot_Hours
                    objOTRequisitionProcess._Actualnight_Hrs = mintactuaNight_Hours
                    objOTRequisitionProcess._Finalmappedapproverunkid = xApproverID
                    objOTRequisitionProcess._Isposted = False
                    objOTRequisitionProcess._Isprocessed = False
                    objOTRequisitionProcess._Userunkid = xUserID
                    objOTRequisitionProcess._Isvoid = False
                    objOTRequisitionProcess._Voiduserunkid = 0
                    objOTRequisitionProcess._Voiddatetime = Nothing
                    objOTRequisitionProcess._Voidreason = ""
                    objOTRequisitionProcess._WebClientIP = mstrClientIp
                    objOTRequisitionProcess._WebFormName = mstrForm_Name
                    objOTRequisitionProcess._WebHostName = mstrHostname
                    objOTRequisitionProcess._Isweb = mblnIsweb

                    If objOTRequisitionProcess.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    xStatusId = objOTRequistion._Statusunkid

                    objOTRequisitionProcess = Nothing
                    objOTRequistion = Nothing

                    'Pinkal (29-Jan-2020) -- End

                End If
                dtApprover.Rows.Clear()
                dtApprover = Nothing

            ElseIf mintStatusunkid = 3 Then 'Rejected

                Dim objOTRequistion As New clsOT_Requisition_Tran
                objOTRequistion._Otrequisitiontranunkid(objDataOperation) = mintOtrequisitiontranunkid
                objOTRequistion._Statusunkid = 3
                objOTRequistion._WebClientIP = mstrClientIp
                objOTRequistion._WebFormName = mstrForm_Name
                objOTRequistion._WebHostName = mstrHostname
                objOTRequistion._IsFromWeb = mblnIsweb
                'Hemant (23 Apr 2020) -- Start 
                'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                objOTRequistion._Userunkid = xUserID
                objOTRequistion._Loginemployeeunkid = mintLoginemoployeeunkid
                'Hemant (23 Apr 2020) -- End
                If objOTRequistion.UpdateOTRequisitionStatus(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                xStatusId = objOTRequistion._Statusunkid

                objOTRequistion = Nothing
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateOTRequisitionFinalStatus; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (tnaotrequisition_approval_tran) </purpose>
    'Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing)
    '    Dim objDataOperation As New clsDataOperation
    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Dim StrQ As String = ""
    '    Try
    '        If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '        For index As Integer = 0 To strvalues.Length - 1
    '            Select Case CInt(strvalues(index))
    '                Case enAllocation.BRANCH
    '                    strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as branchid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '                                     "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
    '                    strJoin &= "AND Fn.branchid = [@emp].branchid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

    '                Case enAllocation.DEPARTMENT_GROUP
    '                    strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as deptgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.deptgrpid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '                                     "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
    '                    strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

    '                Case enAllocation.DEPARTMENT
    '                    strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as deptid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.deptid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '                                     "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
    '                    strJoin &= "AND Fn.deptid = [@emp].deptid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

    '                Case enAllocation.SECTION_GROUP
    '                    strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as secgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.secgrpid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '                                     "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
    '                    strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

    '                Case enAllocation.SECTION
    '                    strSelect &= ",ISNULL(SC.secid,0) AS secid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as secid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.secid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '                                     "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(SC.secid,0) > 0 "
    '                    strJoin &= "AND Fn.secid = [@emp].secid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

    '                Case enAllocation.UNIT_GROUP
    '                    strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as unitgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.unitgrpid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '                                     "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
    '                    strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

    '                Case enAllocation.UNIT
    '                    strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as unitid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.unitid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '                                     "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
    '                    strJoin &= "AND Fn.unitid = [@emp].unitid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

    '                Case enAllocation.TEAM
    '                    strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as teamid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.teamid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '                                     "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
    '                    strJoin &= "AND Fn.teamid = [@emp].teamid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

    '                Case enAllocation.JOB_GROUP
    '                    strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as jgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.jgrpid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '                                     "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
    '                    strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

    '                Case enAllocation.JOBS
    '                    strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as jobid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.jobid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '                                     "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
    '                    strJoin &= "AND Fn.jobid = [@emp].jobid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

    '                Case enAllocation.CLASS_GROUP
    '                    strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as clsgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.clsgrpid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '                                     "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
    '                    strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

    '                Case enAllocation.CLASSES
    '                    strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as clsid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "            JOIN #EMP ON #EMP.clsid = UPT.allocationunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '                                     "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
    '                    strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
    '                    strJoin &= "AND Fn.clsid = [@emp].clsid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "
    '            End Select

    '            objDataOperation.ClearParameters()
    '            StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
    '            Dim strvalue = ""
    '            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

    '            objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            End If

    '            strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

    '            If strvalue.Trim.Length > 0 Then
    '                Select Case CInt(strvalues(index))
    '                    Case enAllocation.BRANCH
    '                        strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT_GROUP
    '                        strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT
    '                        strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION_GROUP
    '                        strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION
    '                        strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT_GROUP
    '                        strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT
    '                        strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

    '                    Case enAllocation.TEAM
    '                        strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

    '                    Case enAllocation.JOB_GROUP
    '                        strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.JOBS
    '                        strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASS_GROUP
    '                        strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASSES
    '                        strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

    '                End Select
    '            End If
    '        Next
    '        If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
    '        If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (tnaotrequisition_approval_tran) </purpose>
    'Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
    '                                    , ByVal intCompanyId As Integer _
    '                                    , ByVal intYearId As Integer _
    '                                    , ByVal strUserAccessMode As String _
    '                                    , ByVal intPrivilegeId As Integer _
    '                                    , ByVal xEmployeeAsOnDate As String _
    '                                    , ByVal mdtStartDate As Date _
    '                                    , ByVal mdtEndDate As Date _
    '                                    , ByVal intStatusId As Integer _
    '                                    , Optional ByVal intCurrentPriorityId As Integer = -1 _
    '                                    , Optional ByVal intUserId As Integer = -1 _
    '                                    , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                                    , Optional ByVal mstrFilterString As String = "" _
    '                                    , Optional ByVal blnAddUserAccess As Boolean = True _
    '                                    , Optional ByVal strEmployeeIds As String = "" _
    '                                    ) As DataTable 'S.SANDEEP [19-NOV-2018] -- START {mdtStartDate,mdtEndDate,intStatusId} -- END 

    '    Dim StrQ As String = String.Empty
    '    Dim dtList As DataTable
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""
    '        Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, 0, objDataOperation)

    '        Dim strUACJoin, strUACFilter As String
    '        strUACJoin = "" : strUACFilter = ""
    '        modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM", blnAddUserAccess)

    '        Dim strApprovalDataJoin As String = String.Empty

    '        strApprovalDataJoin = "LEFT JOIN " & _
    '                                          "( " & _
    '                                          "    SELECT " & _
    '                                          "         tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
    '                                          "        ,tnaotrequisition_approval_tran.otrequestapprovaltranunkid " & _
    '                                          "        ,tnaapprover_master.mapuserunkid " & _
    '                                          "        ,tnaot_requisition_tran.requestdate	" & _
    '                                          "        ,tnaot_requisition_tran.plannedstart_time	" & _
    '                                          "        ,tnaot_requisition_tran.plannedend_time	" & _
    '                                          "        ,tnaot_requisition_tran.plannedot_hours	" & _
    '                                          "        ,tnaot_requisition_tran.request_reason	" & _
    '                                          "        ,tnaot_Requisition_tran.statusunkid " & _
    '                                          "        ,tna_approverlevel_master.tnapriority " & _
    '                                          "        ,tnaotrequisition_approval_tran.statusunkid AS iStatusId " & _
    '                                          "        ,tnaotrequisition_approval_tran.approvaldate " & _
    '                                          "        ,tnaotrequisition_approval_tran.tnamappingunkid " & _
    '                                          "        ,CASE WHEN tnaotrequisition_approval_tran.statusunkid = 1 THEN @Approved  + ' ' + @by + ' :  '+ CM.[username] + ' ' " & _
    '                                          "              WHEN tnaotrequisition_approval_tran.statusunkid = 2 THEN @Pending " & _
    '                                          "              WHEN tnaotrequisition_approval_tran.statusunkid = 3 THEN @Reject + ' ' + @by  + ' : '+ CM.[username] + '' " & _
    '                                          "         END AS iStatus " & _
    '                                          "        ,ROW_NUMBER()OVER(PARTITION BY tnaotrequisition_approval_tran.employeeunkid ORDER BY tnaotrequisition_approval_tran.approvaldate DESC) AS rno " & _
    '                                          "        ,tnaotrequisition_approval_tran.employeeunkid " & _
    '                                          "        ,ISNULL(CONVERT(NVARCHAR(5), tnaotrequisition_approval_tran.actualstart_time, 108), '') as actualstart_time" & _
    '                                          "        ,ISNULL(CONVERT(NVARCHAR(5), tnaotrequisition_approval_tran.actualend_time, 108), '') as actualend_time " & _
    '                                          "        ,ISNULL(tnaotrequisition_approval_tran.adjustment_reason,'') as adjustment_reason " & _
    '                                          "    FROM tnaotrequisition_approval_tran  " & _
    '                                          "        JOIN tnaot_requisition_tran ON tnaotrequisition_approval_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid " & _
    '                                          "        JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid " & _
    '                                          "        JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaotrequisition_approval_tran.tnalevelunkid " & _
    '                                          "        LEFT JOIN hrmsConfiguration..cfuser_master AS CM ON CM.userunkid = tnaotrequisition_approval_tran.mapuserunkid " & _
    '                                          "    WHERE tnaapprover_master.isactive = 1 AND tnaapprover_master.isvoid = 0 AND tnaotrequisition_approval_tran.isvoid = 0 AND tnaotrequisition_approval_tran.iscancel = 0 " & _
    '                                          ") AS B ON B.employeeunkid = TA.employeeunkid AND B.tnapriority = Fn.tnapriority AND B.otrequisitiontranunkid =  TA.otrequisitiontranunkid  "


    '        StrQ = "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL DROP TABLE #EMP "


    '        StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '                       "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '                       "SELECT DISTINCT " & _
    '                       "     AEM.employeeunkid " & _
    '                       "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
    '                       "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
    '                       "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
    '                       "    ,ISNULL(T.classunkid,0) AS classunkid " & _
    '                       "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
    '                       "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
    '                       "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                       "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
    '                       "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
    '                       "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
    '                       "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
    '                       "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
    '                       "FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '                       "    JOIN tnaot_requisition_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid "

    '        If strUACJoin.Trim.Length > 0 AndAlso intUserId > 0 Then
    '            StrQ &= strUACJoin
    '        End If

    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        stationunkid " & _
    '                "       ,deptgroupunkid " & _
    '                "       ,departmentunkid " & _
    '                "       ,sectiongroupunkid " & _
    '                "       ,sectionunkid " & _
    '                "       ,unitgroupunkid " & _
    '                "       ,unitunkid " & _
    '                "       ,teamunkid " & _
    '                "       ,classgroupunkid " & _
    '                "       ,classunkid " & _
    '                "       ,employeeunkid " & _
    '                "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                "   FROM hremployee_transfer_tran " & _
    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "

    '        If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
    '        StrQ &= ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
    '                "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        jobgroupunkid " & _
    '                "       ,jobunkid " & _
    '                "       ,employeeunkid " & _
    '                "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                "   FROM hremployee_categorization_tran " & _
    '                "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "

    '        If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "

    '        StrQ &= ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 "
    '        StrQ &= "WHERE 1 = 1 AND TAT.isvoid = 0  AND TAT.issubmit_approval= 1 "

    '        If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND AEM.employeeunkid IN (" & strEmployeeIds & ") "

    '        StrQ &= " SELECT * INTO #EMP FROM @emp "

    '        StrQ &= "SELECT DISTINCT " & _
    '                "     CAST(0 AS BIT) AS iCheck " & _
    '                "    ,TA.otrequisitiontranunkid " & _
    '                "    ,ISNULL(TA.requestdate,B.requestdate) AS requestdate" & _
    '                "    ,TA.approvaldate " & _
    '                "    ,CONVERT(CHAR(8),ISNULL(TA.requestdate,B.requestdate),112) AS rdate " & _
    '                "    ,CONVERT(CHAR(8),TA.approvaldate,112) AS adate " & _
    '                "    ,ISNULL(B.iStatus,@Pending) AS iStatus " & _
    '                "    ,ISNULL(B.iStatusId,TA.approverStatusId) AS iStatusId " & _
    '                "    ,TA.isfinalapprover " & _
    '                "    ,TA.requisitionStatusId " & _
    '                "    ,EM.employeecode as ecode " & _
    '                "    ,EM.employeecode + ' - ' + EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
    '                "    ,CONVERT(CHAR(8),EM.appointeddate,112) AS appointeddate " & _
    '                "    ,Fn.userunkid " & _
    '                "    ,Fn.username " & _
    '                "    ,Fn.username + ' -  ' + ISNULL(Fn.LvName,'') AS Approver " & _
    '                "    ,Fn.email " & _
    '                "    ,Fn.tnapriority " & _
    '                "    ,EM.employeeunkid " & _
    '                "    ,ISNULL(Fn.uempid,0) AS uempid " & _
    '                "    ,ISNULL(Fn.ecompid,0) AS ecompid " & _
    '                "    ,ISNULL(Fn.LvName,'') AS levelname " & _
    '                "    ,ISNULL(TA.plannedstart_time,B.plannedstart_time) AS  plannedstart_time " & _
    '                "    ,ISNULL(TA.plannedend_time,B.plannedend_time) AS  plannedend_time " & _
    '                "    ,TA.actualstart_time " & _
    '                "    ,TA.actualend_time " & _
    '                "    ,ISNULL(TA.plannedot_hours,B.plannedot_hours) AS plannedot_hoursInSec " & _
    '                "    ,CASE WHEN TA.actualot_hours <=0 THEN B.plannedot_hours ELSE TA.actualot_hours END   AS actualot_hoursInSec " & _
    '                "    ,ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), CASE WHEN TA.plannedot_hours <= 0  THEN B.plannedot_hours ELSE TA.plannedot_hours END / 3600), 2) + ':'  + RIGHT('00'+ CONVERT(VARCHAR(2), (CASE WHEN TA.plannedot_hours <=0 THEN B.plannedot_hours ELSE TA.plannedot_hours END % 3600 ) / 60), 2), '00:00')  AS plannedot_hours  " & _
    '                "    ,ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), TA.actualot_hours / 3600), 2) + ':'  + RIGHT('00'+ CONVERT(VARCHAR(2), (TA.actualot_hours% 3600 ) / 60), 2), '00:00')  AS actualot_hours  " & _
    '                "    ,ISNULL(TA.request_reason,B.request_reason ) AS request_reason " & _
    '                "    ,TA.adjustment_reason " & _
    '                "    ,Fn.mapuserunkid " & _
    '                "    ,Fn.tnamappingunkid " & _
    '                "    ,Fn.tnalevelunkid " & _
    '                "    ,TA.periodunkid " & _
    '                "    ,Fn.isotcap_hod " & _
    '                "    ,ISNULL(otcap.iscap,0) AS iscap " & _
    '                "FROM @emp " & _
    '                "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
    '                "    JOIN ( " & _
    '                "                   SELECT " & _
    '                "                        tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
    '                "                       ,tnaotrequisition_approval_tran.employeeunkid " & _
    '                "                       ,NULL as requestdate " & _
    '                "                       ,tnaotrequisition_approval_tran.approvaldate " & _
    '                "                       ,tnaotrequisition_approval_tran.periodunkid " & _
    '                "                       ,NULL AS plannedstart_time " & _
    '                "                       ,NULL AS plannedend_time " & _
    '                "                       ,0 AS plannedot_hours " & _
    '                "                       ,2 AS requisitionStatusId " & _
    '                "                       ,'' AS request_reason " & _
    '                "                       ,tnaotrequisition_approval_tran.actualstart_time " & _
    '                "                       ,tnaotrequisition_approval_tran.actualend_time " & _
    '                "                       ,tnaotrequisition_approval_tran.actualot_hours " & _
    '                "                       ,tnaotrequisition_approval_tran.adjustment_reason " & _
    '                "                       ,tnaotrequisition_approval_tran.statusunkid as approverStatusId " & _
    '                "                       ,tnaotrequisition_approval_tran.isfinalapprover " & _
    '                "                       ,tnaotrequisition_approval_tran.isvoid " & _
    '                "                       ,tnaotrequisition_approval_tran.iscancel  " & _
    '                "                FROM tnaotrequisition_approval_tran " & _
    '                "                WHERE isvoid = 0 AND iscancel = 0 "

    '        If intCurrentPriorityId >= 0 Then
    '            StrQ &= " AND tnapriority <= @Priority "
    '        End If

    '        If intUserId > 0 Then
    '            StrQ &= " AND mapuserunkid = @UserId "
    '        End If

    '        StrQ &= "          UNION " & _
    '                "               SELECT " & _
    '                "                       tnaot_requisition_tran.otrequisitiontranunkid " & _
    '                "                      ,tnaot_requisition_tran.employeeunkid " & _
    '                "                      ,tnaot_requisition_tran.requestdate " & _
    '                "                      ,NULL as approvaldate " & _
    '                "                      ,tnaot_requisition_tran.periodunkid " & _
    '                "                      ,tnaot_requisition_tran.plannedstart_time " & _
    '                "                      ,tnaot_requisition_tran.plannedend_time " & _
    '                "                      ,tnaot_requisition_tran.plannedot_hours " & _
    '                "                      ,tnaot_requisition_tran.statusunkid AS RequisitionStatusId " & _
    '                "                      ,tnaot_requisition_tran.request_reason " & _
    '                "                      ,ISNULL(tnaot_requisition_tran.actualstart_time,ottime.otast) AS actualstart_time " & _
    '                "                      ,ISNULL(tnaot_requisition_tran.actualend_time,ottime.otaet) AS actualend_time " & _
    '                "                      ,ISNULL(tnaot_requisition_tran.actualot_hours,ottime.othrs) AS actualot_hours " & _
    '                "                      ,tnaot_requisition_tran.adjustment_reason " & _
    '                "                      ,2 AS ApproverStatusId " & _
    '                "                      ,0 AS isfinalapprover " & _
    '                "                      ,tnaot_requisition_tran.isvoid " & _
    '                "                      ,0 AS iscancel  " & _
    '                "               FROM tnaot_requisition_tran "
    '        'S.SANDEEP [19-NOV-2018] -- START
    '        StrQ &= "               LEFT JOIN " & _
    '                "               ( " & _
    '                "                   SELECT " & _
    '                "                        actualstart_time as otast " & _
    '                "                       ,actualend_time as otaet " & _
    '                "                       ,periodunkid as otprid " & _
    '                "                       ,employeeunkid AS otempid " & _
    '                "                       ,actualot_hours as othrs " & _
    '                "                       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY tnapriority DESC) AS rno " & _
    '                "                   FROM tnaotrequisition_approval_tran " & _
    '                "                   WHERE isvoid = 0 " & _
    '                "                   AND statusunkid = 1 " & _
    '                "               )ottime ON ottime.otempid = tnaot_requisition_tran.employeeunkid AND ottime.otprid = tnaot_requisition_tran.periodunkid AND ottime.rno = 1 "
    '        'S.SANDEEP [19-NOV-2018] -- END

    '        StrQ &= "               WHERE tnaot_requisition_tran.isvoid = 0 AND issubmit_approval = 1 AND statusunkid NOT IN (3,6)  " & _
    '                "   AND tnaot_Requisition_tran.otrequisitiontranunkid NOT IN " & _
    '                "                       (SELECT " & _
    '                "                                   tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
    '                "                        FROM tnaotrequisition_approval_tran " & _
    '                "                        WHERE tnaotrequisition_approval_tran.isvoid = 0 AND tnaotrequisition_approval_tran.iscancel = 0 "

    '        If intCurrentPriorityId >= 0 Then
    '            StrQ &= " AND tnapriority <= @Priority "
    '        End If

    '        If intUserId > 0 Then
    '            StrQ &= " AND mapuserunkid = @UserId "
    '        End If

    '        StrQ &= "                       ) " & _
    '                "    ) AS TA ON EM.employeeunkid = TA.employeeunkid " & _
    '                "    JOIN " & _
    '                "    ( " & _
    '                "        SELECT DISTINCT " & _
    '                "             cfuser_master.userunkid " & _
    '                "            ,cfuser_master.username " & _
    '                "            ,cfuser_master.email " & _
    '                              strSelect & " " & _
    '                "            ,LM.tnalevelunkid " & _
    '                "            ,LM.tnapriority " & _
    '                "            ,LM.tnalevelname AS LvName " & _
    '                "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
    '                "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
    '                "            ,EM.mapuserunkid " & _
    '                "            ,EM.tnamappingunkid AS tnamappingunkid " & _
    '                "            ,EM.isotcap_hod " & _
    '                "        FROM hrmsConfiguration..cfuser_master " & _
    '                "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                             strAccessJoin & " " & _
    '                "        JOIN " & strDatabaseName & "..tnaapprover_master EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '                "        JOIN " & strDatabaseName & "..tna_approverlevel_master AS LM ON EM.tnalevelunkid = LM.tnalevelunkid " & _
    '                "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "

    '        If strFilter.Trim.Length > 0 Then
    '            StrQ &= " AND (" & strFilter & " ) "
    '        End If

    '        StrQ &= "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '                     " ) AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
    '                     strApprovalDataJoin & " " & _
    '                     strOuterJoin & " "
    '        'S.SANDEEP [19-NOV-2018] -- START

    '        ' ADDED : EM.isotcap_hod
    '        ' ADDED : Fn.isotcap_hod
    '        ' ADDED : ISNULL(otcap.iscap,0) AS iscap
    '        ' ADDED : ISNULL(tnaot_requisition_tran.actualstart_time,ottime.otast) AS actualstart_time
    '        ' ADDED : ISNULL(tnaot_requisition_tran.actualend_time,ottime.otaet) AS actualend_time

    '        StrQ &= "LEFT JOIN " & _
    '                "( " & _
    '                "   SELECT " & _
    '                "        A.employeeunkid " & _
    '                "       ,CASE WHEN CAST(A.col2 AS INT) <=0 THEN 0 " & _
    '                "             WHEN CAST(A.col1 AS INT) >= CAST(A.col2 AS INT) THEN 1 " & _
    '                "             WHEN CAST(A.col1 AS INT) <= CAST(A.col2 AS INT) THEN 0 " & _
    '                "        END AS iscap " & _
    '                "       ,A.col1 " & _
    '                "       ,A.col2 " & _
    '                "       ,A.periodunkid " & _
    '                "   FROM " & _
    '                "   ( " & _
    '                "       SELECT " & _
    '                "            RIGHT('0' + CAST(SUM(tnaot_requisition_tran.plannedot_hours)/ 3600 AS VARCHAR), 3) AS phr " & _
    '                "           ,RIGHT('0' + CAST((SUM(tnaot_requisition_tran.plannedot_hours) / 60) % 60 AS VARCHAR), 2) AS pmm " & _
    '                "           ,tnaot_requisition_tran.employeeunkid " & _
    '                "           ,tnaot_requisition_tran.periodunkid " & _
    '                "           ,ISNULL(key_value,'00:00') AS key_value " & _
    '                "           ,SUBSTRING(ISNULL(key_value,'00:00'),0,CHARINDEX(':',ISNULL(key_value,'00:00'))) AS chr " & _
    '                "           ,SUBSTRING(ISNULL(key_value,'00:00'),CHARINDEX(':',ISNULL(key_value,'00:00'))+1,2) AS cmm " & _
    '                "           ,CAST(CAST(RIGHT('0' + CAST(SUM(tnaot_requisition_tran.plannedot_hours)/ 3600 AS VARCHAR), 3) AS INT) AS NVARCHAR(MAX)) + CAST(CAST(RIGHT('0' + CAST((SUM(tnaot_requisition_tran.plannedot_hours) / 60) % 60 AS VARCHAR), 2) AS INT) AS NVARCHAR(MAX)) AS col1 " & _
    '                "           ,CAST(CAST(SUBSTRING(ISNULL(key_value,'00:00'),0,CHARINDEX(':',ISNULL(key_value,'00:00'))) AS INT) AS NVARCHAR(MAX)) + CAST(CAST(SUBSTRING(ISNULL(key_value,'00:00'),CHARINDEX(':',ISNULL(key_value,'00:00'))+1,2) AS INT) AS NVARCHAR(MAX)) AS col2 " & _
    '                "       FROM tnaot_requisition_tran " & _
    '                "           JOIN hremployee_master ON hremployee_master.employeeunkid = tnaot_requisition_tran.employeeunkid " & _
    '                "           LEFT JOIN hrmsConfiguration..cfconfiguration ON cfconfiguration.companyunkid = hremployee_master.companyunkid AND UPPER(key_name) = 'CAPOTHRSFORHODAPPROVERS' " & _
    '                "       WHERE isvoid = 0 AND statusunkid NOT IN  (3,6) AND issubmit_approval = 1 " & _
    '                "           AND CONVERT(NVARCHAR(8),requestdate,112) BETWEEN '" & eZeeDate.convertDate(mdtStartDate).ToString() & "' AND '" & eZeeDate.convertDate(mdtEndDate).ToString() & "' " & _
    '                "       GROUP BY tnaot_requisition_tran.employeeunkid, tnaot_requisition_tran.periodunkid,ISNULL(key_value,'00:00') " & _
    '                "   ) AS A " & _
    '                ") AS otcap ON TA.employeeunkid = otcap.employeeunkid AND TA.periodunkid = otcap.periodunkid "
    '        'S.SANDEEP [19-NOV-2018] -- END

    '        StrQ &= " WHERE 1 = 1 AND TA.isvoid = 0 AND TA.iscancel  = 0  "


    '        If mstrFilterString.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrFilterString
    '        End If

    '        StrQ &= " ORDER BY EM.employeecode + ' - ' + EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname,ISNULL(TA.requestdate,b.requestdate) DESC "

    '        StrQ &= " IF OBJECT_ID('tempdb..#EMP') IS NOT NULL DROP TABLE #EMP "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
    '        objDataOperation.AddParameter("@Priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrentPriorityId)
    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))
    '        objDataOperation.AddParameter("@by", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "By"))

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        dtList = dsList.Tables("List").Copy()

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dtList
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (tnaotrequisition_approval_tran) </purpose>
    'Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
    '                                      , ByVal intCompanyId As Integer _
    '                                      , ByVal intYearId As Integer _
    '                                      , ByVal strUserAccessMode As String _
    '                                      , ByVal intPrivilegeId As Integer _
    '                                      , ByVal intUserId As Integer _
    '                                      , ByVal intPriorityId As Integer _
    '                                      , ByVal blnOnlyMyApprovals As Boolean _
    '                                      , ByVal xEmployeeAsOnDate As String _
    '                                      , ByVal blnAddUserAccess As Boolean _
    '                                      , ByVal intAppApprovalStatus As Integer _
    '                                      , ByVal mdtStartDate As Date _
    '                                      , ByVal mdtEndDate As Date _
    '                                      , ByVal intStatusId As Integer _
    '                                      , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                                      , Optional ByVal mstrFilter As String = "") As DataTable 'S.SANDEEP [19-NOV-2018] -- START {mdtStartDate,mdtEndDate,intStatusId} -- END
    '    Dim StrQ As String = String.Empty
    '    Dim dList As DataTable = Nothing
    '    Dim oData As DataTable = Nothing
    '    Try
    '        dList = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, xEmployeeAsOnDate, mdtStartDate, mdtEndDate, intStatusId, intPriorityId, intUserId, Nothing, mstrFilter, blnAddUserAccess, "")
    '        Dim dtTab As New DataTable
    '        If dList.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId And x.Field(Of Boolean)("isotcap_hod") = True).Count() > 0 Then
    '            dtTab = New DataView(dList, "userunkid = " & intUserId & " AND iscap = 1", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable().Copy
    '        End If

    '        If dtTab.Rows.Count > 0 Then
    '            dList = dtTab
    '        Else
    '            'dtTab = New DataView(dList, "isotcap_hod = 0 AND iscap = 0", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable().Copy
    '            dtTab = New DataView(dList, "iscap = 0", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable().Copy
    '            dList = dtTab
    '        End If

    '        oData = New DataView(dList, "", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable().Copy
    '        If blnOnlyMyApprovals = True Then
    '            If dList.Rows.Count > 0 Then
    '                Dim blnFlag As Boolean = False
    '                Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("tnapriority")).Distinct().ToList()
    '                iPriority.Sort()
    '                Dim intSecondLastPriority As Integer = -1
    '                If iPriority.Min() = intPriorityId Then
    '                    intSecondLastPriority = intPriorityId
    '                    blnFlag = True
    '                    If blnOnlyMyApprovals = True Then
    '                        oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                    End If
    '                Else
    '                    Try
    '                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
    '                    Catch ex As Exception
    '                        intSecondLastPriority = intPriorityId
    '                        blnFlag = True
    '                    End Try
    '                    Dim dr() As DataRow = Nothing
    '                    dr = dList.Select("tnapriority = '" & intSecondLastPriority & "'", "employeeunkid, tnapriority")
    '                    If dr.Length > 0 Then
    '                        Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = 1) ' Approved  DON'T CHANGE STATUS HERE IT"S COMPULSORY CHECKING OF APPROVED.
    '                        Dim strIds As String
    '                        If row.Count() > 0 Then
    '                            strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
    '                            If blnFlag Then
    '                                strIds = ""
    '                            End If
    '                            If strIds.Trim.Length > 0 Then
    '                                If intAppApprovalStatus <= 0 Then
    '                                    'oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 2 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                                    oData = New DataView(dList, "userunkid = " & intUserId & " AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                                Else
    '                                    oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = " & intAppApprovalStatus & " AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                                End If
    '                            Else
    '                                If intAppApprovalStatus <= 0 Then
    '                                    'oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 2", "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                                    oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                                Else
    '                                    oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = " & intAppApprovalStatus, "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                                End If
    '                            End If
    '                        Else
    '                            strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
    '                            If blnFlag = False Then
    '                                strIds = " AND employeeunkid NOT IN(" & strIds & ") "
    '                            Else
    '                                strIds = ""
    '                            End If
    '                            If intAppApprovalStatus <= 0 Then
    '                                strIds = ""
    '                                If blnFlag = False Then
    '                                    strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
    '                                    strIds = " AND employeeunkid NOT IN(" & strIds & ") "
    '                                Else
    '                                    strIds = ""
    '                                End If
    '                                'row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = 3)
    '                                'If row.Count() > 0 Then
    '                                '    strIds = String.Join(",", dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = 3).Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
    '                                '    strIds = " AND employeeunkid NOT IN(" & strIds & ") "
    '                                'Else
    '                                '    If blnFlag = False Then
    '                                '        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
    '                                '        strIds = " AND employeeunkid NOT IN(" & strIds & ") "
    '                                '    Else
    '                                '        strIds = ""
    '                                '    End If
    '                                'End If
    '                                oData = New DataView(dList, "userunkid = " & intUserId & strIds, "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                            Else
    '                                oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = " & intAppApprovalStatus & strIds, "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable()
    '                            End If
    '                        End If
    '                    Else
    '                        oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, tnapriority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return oData
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (tnaotrequisition_approval_tran) </purpose>
    'Public Function IsApproverPresent(ByVal strDatabaseName As String, _
    '                              ByVal strUserAccessMode As String, _
    '                              ByVal intCompanyId As Integer, _
    '                              ByVal intYearId As Integer, _
    '                              ByVal intPrivilegeId As Integer, _
    '                              ByVal intUserId As Integer, _
    '                              ByVal iPriority As Integer, _
    '                              ByVal xEmployeeAsOnDate As String, _
    '                              ByVal intEmployeeUnkid As Integer, _
    '                              Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataTable

    '    Dim StrQ As String = String.Empty
    '    Dim mdtTable As DataTable = Nothing
    '    Dim objDataOperation As New clsDataOperation

    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If

    '    objDataOperation.ClearParameters()

    '    Dim strMsg As String = ""
    '    Try
    '        Dim strUACJoin, strUACFilter As String
    '        strUACJoin = "" : strUACFilter = ""

    '        StrQ = "SELECT " & _
    '                   "     UM.userunkid " & _
    '                   "    ,UM.username " & _
    '                   "    ,UM.email " & _
    '                   "    ,TAL.tnapriority " & _
    '                   "    ,TAL.tnalevelname " & _
    '                   " FROM " & strDatabaseName & "..tnaapprover_master AS TAM " & _
    '                   "    JOIN hrmsConfiguration..cfuser_master AS UM ON TAM.mapuserunkid = UM.userunkid " & _
    '                   "    JOIN tna_approverlevel_master AS TAL ON TAL.tnalevelunkid = TAM.tnalevelunkid " & _
    '                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
    '                   "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
    '                   " WHERE TAM.isvoid = 0 AND CP.yearunkid = @Y AND UP.privilegeunkid = @P AND TAL.tnapriority > @priority " & _
    '                   " ORDER BY TAL.tnapriority "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, iPriority)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        Dim blnFlag As Boolean = False

    '        If dsList IsNot Nothing Then mdtTable = dsList.Tables(0).Clone()

    '        For Each dr As DataRow In dsList.Tables(0).Rows

    '            modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, CInt(dr("userunkid")), intCompanyId, intYearId, strUserAccessMode, "AEM", True)

    '            StrQ = "SELECT " & _
    '                       "     AEM.employeeunkid " & _
    '                       "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
    '                       "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
    '                       "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
    '                       "    ,ISNULL(T.classunkid,0) AS classunkid " & _
    '                       "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
    '                       "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
    '                       "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                       "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
    '                       "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
    '                       "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
    '                       "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
    '                       "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
    '                       "FROM " & strDatabaseName & "..hremployee_master AS AEM "

    '            If strUACJoin.Trim.Length > 0 Then
    '                StrQ &= strUACJoin
    '            End If

    '            StrQ &= "LEFT JOIN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        stationunkid " & _
    '                    "       ,deptgroupunkid " & _
    '                    "       ,departmentunkid " & _
    '                    "       ,sectiongroupunkid " & _
    '                    "       ,sectionunkid " & _
    '                    "       ,unitgroupunkid " & _
    '                    "       ,unitunkid " & _
    '                    "       ,teamunkid " & _
    '                    "       ,classgroupunkid " & _
    '                    "       ,classunkid " & _
    '                    "       ,employeeunkid " & _
    '                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                    "   FROM hremployee_transfer_tran " & _
    '                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                    ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
    '                    "LEFT JOIN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        jobgroupunkid " & _
    '                    "       ,jobunkid " & _
    '                    "       ,employeeunkid " & _
    '                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                    "   FROM hremployee_categorization_tran " & _
    '                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
    '                    ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
    '                    "WHERE AEM.employeeunkid = '" & intEmployeeUnkid & "' "

    '            objDataOperation.ClearParameters()
    '            Dim dsEmp As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            End If

    '            If dsEmp.Tables(0).Rows.Count > 0 Then
    '                blnFlag = True
    '                mdtTable.ImportRow(dr)
    '                Exit For
    '            End If

    '        Next

    '        'If blnFlag = False Then
    '        '    strMsg = Language.getMessage(mstrModuleName, 3, "Sorry, No approver(s) defined based on selected for employee user access setting.")
    '        '    Exit Try
    '        'End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: IsApproverPresent; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return mdtTable
    'End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Request Date")
            Language.setMessage(mstrModuleName, 2, "Approved By :")
            Language.setMessage(mstrModuleName, 3, "Rejected By :")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class