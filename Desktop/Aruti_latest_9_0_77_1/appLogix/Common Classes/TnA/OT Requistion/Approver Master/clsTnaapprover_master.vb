﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clsTnaapprover_master.vb
'Purpose    :
'Date       :19-Oct-2018
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsTnaapprover_master
    Private Shared ReadOnly mstrModuleName As String = "clsTnaapprover_master"
    Dim objDataOperation As clsDataOperation
    Dim objApproverTran As New clsTnaapprover_Tran
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTnamappingunkid As Integer
    Private mintTnalevelunkid As Integer
    Private mintMapuserunkid As Integer
    Private mintTnatypeid As Integer
    Private mintApproverEmployeeId As Integer
    Private mblnIsotcap_Hod As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mblnIsSwap As Boolean = False
    Private mblnIsExternalApprover As Boolean = False
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date

    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mintLoginEmployeeunkid As Integer
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintUserunkid As Integer

#End Region

#Region " Properties "


    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Tnamappingunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tnamappingunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTnamappingunkid
        End Get
        Set(ByVal value As Integer)
            mintTnamappingunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Tnalevelunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tnalevelunkid() As Integer
        Get
            Return mintTnalevelunkid
        End Get
        Set(ByVal value As Integer)
            mintTnalevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Mapuserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnatypeid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tnatypeid() As Integer
        Get
            Return mintTnatypeid
        End Get
        Set(ByVal value As Integer)
            mintTnatypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isotcap_hod
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isotcap_Hod() As Boolean
        Get
            Return mblnIsotcap_Hod
        End Get
        Set(ByVal value As Boolean)
            mblnIsotcap_Hod = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApproverEmployeeId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ApproverEmployeeId() As Integer
        Get
            Return mintApproverEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintApproverEmployeeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsSwap
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsSwap() As Boolean
        Get
            Return mblnIsSwap
        End Get
        Set(ByVal value As Boolean)
            mblnIsSwap = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsExternalApprover
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsExternalApprover() As Boolean
        Get
            Return mblnIsExternalApprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsExternalApprover = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                        "  tnamappingunkid " & _
                        ", tnalevelunkid " & _
                        ", mapuserunkid " & _
                        ", tnatypeid " & _
                        ", isotcap_hod " & _
                        ", approveremployeeunkid " & _
                        ", isswap " & _
                        ", isexternalapprover " & _
                        ", isactive " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voidreason " & _
                        ", voiddatetime " & _
                       " FROM tnaapprover_master " & _
                       " WHERE tnamappingunkid = @tnamappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTnamappingunkid = CInt(dtRow.Item("tnamappingunkid"))
                mintTnalevelunkid = CInt(dtRow.Item("tnalevelunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mintTnatypeid = CInt(dtRow.Item("tnatypeid"))
                mblnIsotcap_Hod = CBool(dtRow.Item("isotcap_hod"))
                mintApproverEmployeeId = CInt(dtRow.Item("approveremployeeunkid"))
                mblnIsSwap = CBool(dtRow.Item("isswap"))
                mblnIsExternalApprover = CBool(dtRow.Item("isexternalapprover"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintUserunkid = Convert.ToInt32(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString()

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "", Optional ByVal intApproverUserUnkId As Integer = 0 _
    '                        , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                        , Optional ByVal blnAddSelect As Boolean = False) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        If blnAddSelect Then
    '            strQ = "SELECT " & _
    '                    "  0 AS tnamappingunkid " & _
    '                    " ,0 AS tnalevelunkid " & _
    '                    " ,'' AS LevelCode " & _
    '                    " ,'' AS  LevelName " & _
    '                    " ,0 AS mapuserunkid " & _
    '                    " ,@Select AS approver " & _
    '                    ", 0 AS isotcap_hod " & _
    '                    " ,0 AS isactive " & _
    '                    " ,'' As Status " & _
    '                    " ,0 As userunkid " & _
    '                    " ,0 AS isvoid " & _
    '                    " ,0 AS voiduserunkid " & _
    '                    " ,NULL AS voiddatetime " & _
    '                    " ,'' AS voidreason " & _
    '                    " ,0 AS priority " & _
    '                    ", 0 AS approveremployeeunkid " & _
    '                    ", 0 AS isswap " & _
    '                    ", 0 AS isexternalapprover " & _
    '                    " UNION ALL "
    '        End If
    '        strQ &= "SELECT " & _
    '                "     tnaapprover_master.tnamappingunkid " & _
    '                "    ,tna_approverlevel_master.tnalevelunkid " & _
    '                "    ,ISNULL(tna_approverlevel_master.tnalevelcode,'') as LevelCode " & _
    '                "    ,ISNULL(tna_approverlevel_master.tnalevelname,'') as LevelName " & _
    '                "    ,tnaapprover_master.mapuserunkid " & _
    '                "    ,ISNULL(UM.username,'') AS approver " & _
    '                "    ,tnaapprover_master.isotcap_hod " & _
    '                "    ,tnaapprover_master.isactive " & _
    '                "    ,CASE WHEN tnaapprover_master.isactive = 0 THEN @InActive ELSE @Active END As Status " & _
    '                "    ,tnaapprover_master.userunkid " & _
    '                "    ,tnaapprover_master.isvoid " & _
    '                "    ,tnaapprover_master.voiduserunkid " & _
    '                "    ,tnaapprover_master.voiddatetime " & _
    '                "    ,tnaapprover_master.voidreason " & _
    '                "    ,ISNULL(tna_approverlevel_master.tnapriority,0) AS priority " & _
    '                " FROM tnaapprover_master " & _
    '                "    JOIN tna_approverlevel_master  ON tnaapprover_master.tnalevelunkid = tna_approverlevel_master.tnalevelunkid " & _
    '                "    LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = tnaapprover_master.mapuserunkid " & _
    '                "    WHERE 1 = 1 "

    '        If blnOnlyActive Then
    '            strQ &= " AND tnaapprover_master.isvoid = 0 AND tnaapprover_master.isactive = 1 "
    '        Else
    '            strQ &= " AND tnaapprover_master.isvoid = 0  AND tnaapprover_master.isactive = 0 "
    '        End If

    '        If intApproverUserUnkId > 0 Then
    '            strQ &= " AND tna_approverlevel_master.mapuserunkid = @mapuserunkid "
    '        End If

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= mstrFilter
    '        End If

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
    '        objDataOperation.AddParameter("@InActive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "In Active"))
    '        objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Active"))
    '        objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        If objDataOpr Is Nothing Then
    '            objDataOperation = Nothing
    '        End If
    '    End Try
    '    Return dsList
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xTnATypeId As Integer _
                                      , ByVal strEmployeeAsOnDate As String, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , Optional ByVal blnOnlyActive As Boolean = True _
                                      , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                      , Optional ByVal mstrFilter As String = "" _
                                      , Optional ByVal intApproverTranUnkid As Integer = 0 _
                                      , Optional ByVal blnIncludeCapApprover As Boolean = False) As DataSet

        'Pinkal (23-Nov-2019) -- 'Enhancement NMB - Working On OT Enhancement for NMB.[Optional ByVal blnIncludeCapApprover As Boolean = False]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try

            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), True, False, xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            strQ = "SELECT " & _
                      "  tnaapprover_master.tnamappingunkid " & _
                      ", tna_approverlevel_master.tnalevelunkid " & _
                      ", tna_approverlevel_master.tnalevelname " & _
                      ", tna_approverlevel_master.tnapriority " & _
                      ", tna_approverlevel_master.tnalevelcode " & _
                      ", tnaapprover_master.approveremployeeunkid " & _
                      ", #APPR_NAME# as name" & _
                      ", #DEPT_NAME# as departmentname " & _
                      ", #JOB_NAME# As  jobname " & _
                      ", tnaapprover_master.isvoid " & _
                      ", tnaapprover_master.voiddatetime " & _
                      ", tnaapprover_master.userunkid " & _
                      ", tnaapprover_master.voiduserunkid " & _
                      ", tnaapprover_master.voidreason " & _
                      ", tnaapprover_master.isexternalapprover  " & _
                      ", tnaapprover_master.isactive  " & _
                      ", CASE WHEN tnaapprover_master.isexternalapprover = 1 THEN @YES ELSE @NO END AS ExAppr " & _
                      ", ISNULL(Usr.username,'') AS MappedUser " & _
                      ", tnaapprover_master.mapuserunkid " & _
                      ", tnaapprover_master.isotcap_hod " & _
                      " FROM tnaapprover_master " & _
                      " LEFT JOIN tna_approverlevel_master on tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid " & _
                      "	LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON tnaapprover_master.mapuserunkid = Usr.userunkid " & _
                      " #EMPL_JOIN# "

            'Pinkal (03-Mar-2020) -- ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.[tnaapprover_master.mapuserunkid, tnaapprover_master.isotcap_hod]

            StrFinalQurey = strQ

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " #DATA_JOIN# "

            'Pinkal (03-Sep-2020) -- End

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE 1 = 1 AND tnaapprover_master.isexternalapprover = #ExAppr# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If


            If blnOnlyActive Then
                StrQCondition &= " AND tnaapprover_master.isvoid = 0 AND tnaapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND tnaapprover_master.isvoid = 0  AND tnaapprover_master.isactive = 0 "
            End If


            If intApproverTranUnkid > 0 Then
                StrQCondition &= " AND tnaapprover_master.tnamappingunkid = '" & intApproverTranUnkid & "' "
            End If

            StrQCondition &= " AND tnaapprover_master.isswap = 0 AND tnaapprover_master.tnatypeid = @tnatypeid "

            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            'Pinkal (04-Dec-2019) -- Start
            'Enhancement NMB - Working On Close Year Enhancement for NMB.
            'StrQCondition &= " AND isotcap_hod = @isotcap_hod"

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If blnIncludeCapApprover = False Then
            StrQCondition &= " AND isotcap_hod = @isotcap_hod "
            End If
            'Pinkal (27-Dec-2019) -- End

            'Pinkal (04-Dec-2019) -- End
            'Pinkal (23-Nov-2019) -- End

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= "AND " & mstrFilter
            End If

            Dim StrDataJoin As String = " LEFT JOIN " & _
                                                     " ( " & _
                                                     "    SELECT " & _
                                                     "        departmentunkid " & _
                                                     "        ,employeeunkid " & _
                                                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                     "    FROM #DB_Name#hremployee_transfer_tran " & _
                                                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsonDate " & _
                                                     " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                                     "  LEFT JOIN #DB_Name#hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                                                     " LEFT JOIN " & _
                                                     " ( " & _
                                                     "    SELECT " & _
                                                     "         jobunkid " & _
                                                     "        ,jobgroupunkid " & _
                                                     "        ,employeeunkid " & _
                                                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                     "    FROM #DB_Name#hremployee_categorization_tran " & _
                                                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsonDate " & _
                                                     " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                                     " LEFT JOIN #DB_Name#hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master on hremployee_master.employeeunkid = tnaapprover_master.approveremployeeunkid ")
            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#DB_Name#", "")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")
            strQ &= StrQCondition
            strQ &= StrQDtFilters
            strQ = strQ.Replace("#ExAppr#", "0")

            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnATypeId)
            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Yes"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "No"))


            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIncludeCapApprover)
            'Pinkal (23-Nov-2019) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet
            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr As DataRow In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey : StrQDtFilters = ""
                Dim dstmp As New DataSet
                If dr("DName").ToString().Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tnaapprover_master.mapuserunkid ")
                    strQ = strQ.Replace("#ExAppr#", "1")
                    strQ = strQ.Replace("#DB_Name#", "")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#SEC_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate").ToString()).Date, eZeeDate.convertDate(dr("EDate").ToString()), , , dr("DName").ToString())
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate").ToString()), dr("DName").ToString())

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                  "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tnaapprover_master.mapuserunkid " & _
                                                  "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
                    'Pinkal (03-Sep-2020) -- End

                    strQ = strQ.Replace("#DB_Name#", dr("DName").ToString() & "..")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")


                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If


                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    strQ &= StrDataJoin.Replace("#DB_Name#", dr("DName").ToString() & "..")
                    'Pinkal (03-Sep-2020) -- End


                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQDtFilters &= xDateFilterQry & " "
                        End If
                    End If

                End If

                strQ &= StrQCondition
                strQ &= StrQDtFilters
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid").ToString()

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnATypeId)
                objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Yes"))
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "No"))
                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIncludeCapApprover)
                'Pinkal (23-Nov-2019) -- End

                dstmp = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_approver_master) </purpose>
    Public Function Insert(ByVal dtAssignedEmp As DataTable) As Boolean


        'Pinkal (27-Dec-2019) -- Start
        'Enhancement - Changes related To OT NMB Testing.
        'If isExist(mintTnatypeid, mintMapuserunkid, mintTnalevelunkid, mintTnamappingunkid, mblnIsExternalApprover) Then
        If isExist(mintTnatypeid, mintMapuserunkid, mintTnalevelunkid, mintApproverEmployeeId, mintTnamappingunkid, mblnIsExternalApprover) Then
            'Pinkal (27-Dec-2019) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver already exists. Please define new Approver.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnatypeid.ToString)
            objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsotcap_Hod.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmployeeId)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))


            strQ = "INSERT INTO tnaapprover_master ( " & _
                    "  tnalevelunkid " & _
                    ", mapuserunkid " & _
                    ", tnatypeid " & _
                    ", isotcap_hod " & _
                    ", approveremployeeunkid " & _
                    ", isswap " & _
                    ", isexternalapprover " & _
                    ", isactive " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voidreason " & _
                    ", voiddatetime" & _
                  ") VALUES (" & _
                    "  @tnalevelunkid " & _
                    ", @mapuserunkid " & _
                    ", @tnatypeid " & _
                    ", @isotcap_hod " & _
                    ", @approveremployeeunkid " & _
                    ", @isswap " & _
                    ", @isexternalapprover " & _
                    ", @isactive " & _
                    ", @userunkid " & _
                    ", @isvoid " & _
                    ", @voiduserunkid " & _
                    ", @voidreason " & _
                    ", @voiddatetime" & _
                  "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTnamappingunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrailApproverMapping(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtAssignedEmp IsNot Nothing AndAlso dtAssignedEmp.Rows.Count > 0 Then
                objApproverTran._TnAMappingUnkId = mintTnamappingunkid
                objApproverTran._DataTable = dtAssignedEmp

                objApproverTran._AuditUserId = mintAuditUserId
                objApproverTran._AuditDatetime = mdtAuditDatetime
                objApproverTran._ClientIP = mstrClientIP
                objApproverTran._LoginEmployeeunkid = -1
                objApproverTran._HostName = mstrHostName
                objApproverTran._FormName = mstrFormName
                objApproverTran._IsFromWeb = mblnIsFromWeb

                If objApproverTran.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveapprover_master) </purpose>
    Public Function Update(ByVal dtAssignedEmp As DataTable) As Boolean


        'Pinkal (07-Dec-2019) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        'If isExist(mintTnatypeid, mintMapuserunkid, mintTnalevelunkid, mintTnamappingunkid, mblnIsExternalApprover) Then
        If isExist(mintTnatypeid, mintMapuserunkid, mintTnalevelunkid, mintApproverEmployeeId, mintTnamappingunkid, mblnIsExternalApprover) Then
            'Pinkal (07-Dec-2019) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver already exists. Please define new Approver.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid.ToString)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnatypeid.ToString)
            objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsotcap_Hod.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmployeeId)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))


            strQ = " UPDATE tnaapprover_master SET " & _
                      "  tnalevelunkid = @tnalevelunkid" & _
                      ", mapuserunkid = @mapuserunkid" & _
                      ", tnatypeid = @tnatypeid" & _
                      ", isotcap_hod = @isotcap_hod" & _
                      ", approveremployeeunkid = @approveremployeeunkid" & _
                      ", isswap = @isswap" & _
                      ", isexternalapprover = @isexternalapprover " & _
                      ", isactive = @isactive " & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", userunkid = @userunkid" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voidreason = @voidreason " & _
                      " WHERE tnamappingunkid = @tnamappingunkid "


            'Pinkal (08-Jan-2020) -- Enhancement - NMB - Working on NMB OT Requisition Requirement.[tnatypeid = @tnatypeid,isotcap_hod = @isotcap_hod]

            objDataOperation.ExecNonQuery(strQ)


            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dtAssignedEmp IsNot Nothing AndAlso dtAssignedEmp.Rows.Count > 0 Then
                objApproverTran._TnAMappingUnkId = mintTnamappingunkid
                objApproverTran._DataTable = dtAssignedEmp

                objApproverTran._AuditUserId = mintAuditUserId
                objApproverTran._AuditDatetime = mdtAuditDatetime
                objApproverTran._ClientIP = mstrClientIP
                objApproverTran._LoginEmployeeunkid = -1
                objApproverTran._HostName = mstrHostName
                objApproverTran._FormName = mstrFormName
                objApproverTran._IsFromWeb = mblnIsFromWeb

                If objApproverTran.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_appusermapping) </purpose>
    Public Function Delete(ByVal xTnaTypeId As Integer, ByVal intUnkid As Integer, ByVal xEmployeeAsonDate As Date) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objApproverTran._TnAMappingUnkId = intUnkid
            objApproverTran._EmployeeAsonDate = xEmployeeAsonDate
            objApproverTran.Get_Data()
            Dim dtApproverTran As DataTable = objApproverTran._DataTable.Copy()

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            If dtApproverTran IsNot Nothing AndAlso dtApproverTran.Rows.Count > 0 Then

                strQ = " UPDATE tnaapprover_tran SET " & _
                          "  isvoid = @isvoid " & _
                          " ,voiduserunkid = @voiduserunkid " & _
                          " ,voiddatetime = @voiddatetime " & _
                          " ,voidreason = @voidreason " & _
                          " WHERE tnaapprovertranunkid  = @tnaapprovertranunkid  "

                objApproverTran._AuditUserId = mintAuditUserId
                objApproverTran._AuditDatetime = mdtAuditDatetime
                objApproverTran._FormName = mstrFormName
                objApproverTran._ClientIP = mstrClientIP
                objApproverTran._HostName = mstrHostName
                objApproverTran._IsFromWeb = mblnIsFromWeb


                For Each drRow As DataRow In dtApproverTran.Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@tnaapprovertranunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("tnaapprovertranunkid")))
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objApproverTran._TnAapprovertranunkid = CInt(drRow("tnaapprovertranunkid"))

                    If objApproverTran.InsertAuditTrailApproverTran(objDataOperation, 3, CInt(drRow("employeeunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If


            strQ = " UPDATE tnaapprover_master SET " & _
                      "  isvoid = @isvoid " & _
                      " ,voiduserunkid = @voiduserunkid " & _
                      " ,voiddatetime = @voiddatetime " & _
                      " ,voidreason = @voidreason " & _
                      " WHERE tnatypeid =  @tnatypeid AND tnamappingunkid  = @tnamappingunkid  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnamappingunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@tnatypeid ", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnaTypeId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Tnamappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailApproverMapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT * FROM tnaapprover_master WHERE 1=2 AND tnamappingunkid = @mappingunkid "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xTnaTypeId As Integer, ByVal xMapUserId As Integer, ByVal intApproveLevelunkid As Integer, ByVal xApproverEmpId As Integer _
                                 , Optional ByVal intunkid As Integer = -1, Optional ByVal blnIsExternalApprover As Boolean = False) As Boolean

        'Pinkal (27-Dec-2019) -- Enhancement - Changes related To OT NMB Testing. [ByVal xApproverEmpId As Integer]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  tnamappingunkid " & _
                       ", tnalevelunkid " & _
                       ", mapuserunkid " & _
                       ", tnatypeid " & _
                       ", isotcap_hod " & _
                       ", isactive " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime " & _
                       " FROM tnaapprover_master " & _
                       " WHERE  tnatypeid = @tnatypeid " & _
                       " AND isvoid = 0  AND isswap = 0  AND isexternalapprover = @isexternalapprover "

            objDataOperation.ClearParameters()

            If intunkid > 0 Then
                strQ &= " AND tnamappingunkid <> @tnamappingunkid"
                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            If intApproveLevelunkid > 0 Then
                strQ &= " AND tnalevelunkid = @tnalevelunkid"
                objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid)
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'If xMapUserId > 0 Then
            '    strQ &= " AND mapuserunkid = @mapuserunkid"
            '    objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserId)
            'End If
            'Pinkal (03-Sep-2020) -- End


            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If xApproverEmpId > 0 Then
                strQ &= " AND approveremployeeunkid = @approveremployeeunkid "
                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverEmpId)
            End If
            'Pinkal (27-Dec-2019) -- End


            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnaTypeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InActiveApprover(ByVal xMappingId As Integer, ByVal xTnaTypeId As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " UPDATE tnaapprover_master set isactive = 1 where tnamappingunkid = @tnamappingunkid AND isvoid = 0 AND tnatypeid = @tnatypeid  "
            Else
                strQ = " UPDATE tnaapprover_master set isactive = 0 where tnamappingunkid = @tnamappingunkid AND isvoid = 0 AND tnatypeid = @tnatypeid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnaTypeId)
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Tnamappingunkid(objDataOperation) = xMappingId

            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertAuditTrailApproverMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO attnaapprover_master ( " & _
                      "  tranguid " & _
                      ", tnamappingunkid " & _
                      ", tnalevelunkid " & _
                      ", mapuserunkid " & _
                      ", tnatypeid " & _
                      ", isotcap_hod " & _
                      ", approveremployeeunkid " & _
                      ", isswap " & _
                      ", isexternalapprover " & _
                      ", isactive " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                      ", loginemployeeunkid " & _
                      " ) VALUES (" & _
                      "  @tranguid " & _
                      ", @tnamappingunkid " & _
                      ", @tnalevelunkid " & _
                      ", @mapuserunkid " & _
                      ", @tnatypeid " & _
                      ", @isotcap_hod " & _
                      ", @approveremployeeunkid " & _
                      ", @isswap " & _
                      ", @isexternalapprover " & _
                      ", @isactive " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip" & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @loginemployeeunkid " & _
                      ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnatypeid.ToString)
            objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsotcap_Hod.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmployeeId)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailApproverMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetUserPriority(ByVal xTnATypeId As Integer, ByVal xMapUserID As Integer, ByRef xPriority As Integer _
                                             , Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataTable

        'Pinkal (10-Jan-2020) -- Enhancements -  Working on OT Requisistion for NMB.[Optional ByVal blnIncludeCapApprover As Boolean = False]

        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        xPriority = -1
        Try

            Dim objDataOperation As clsDataOperation
            If objDataOpr IsNot Nothing Then
                objDataOperation = objDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT  " & _
                      "  tnaapprover_master.tnamappingunkid  " & _
                      ", tnaapprover_master.mapuserunkid " & _
                      ", tnaapprover_master.tnalevelunkid " & _
                      ", tna_approverlevel_master.tnalevelcode " & _
                      ", tna_approverlevel_master.tnalevelname " & _
                      ", tna_approverlevel_master.tnapriority " & _
                      " FROM tnaapprover_master " & _
                      " JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid " & _
                      " WHERE tnaapprover_master.mapuserunkid = @mapuserId  AND tnaapprover_master.tnatypeid = @tnatypeid " & _
                      " AND tnaapprover_master.isvoid = 0 "


            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.
            'If blnIncludeCapApprover = False Then
            '    strQ &= " AND isotcap_hod = @isotcap_hod"
            'End If
            'Pinkal (10-Jan-2020) -- End

            objDataOperation.AddParameter("@mapuserId", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserID)
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnATypeId)

            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.
            'objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIncludeCapApprover)
            'Pinkal (10-Jan-2020) -- End


            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                If dsList.Tables(0).Rows.Count > 0 Then
                    xPriority = CInt(dsList.Tables(0).Rows(0)("tnapriority"))
                End If
                dtTable = dsList.Tables(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUserPriority; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List", Optional ByVal blnIncludeDelete As Boolean = False) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                   "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                   "FROM tnaapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tnaapprover_master.mapuserunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   ") AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   ") AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE  tnaapprover_master.isswap = 0 " & _
                   " AND tnaapprover_master.isexternalapprover = 1 "


            If blnIncludeDelete = False Then
                StrQ &= " AND tnaapprover_master.isvoid = 0 "
            End If

            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalApproverList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsApprover
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetApproverEmployeeId(ByVal mintApproverEmpId As Integer, ByVal mblnExternalApprover As Boolean, Optional ByVal blnIncludeCapApprover As Boolean = False) As String
        'Pinkal (23-Nov-2019) -- Enhancement NMB - Working On OT Enhancement for NMB.[Optional ByVal blnIncludeCapApprover As Boolean = False]
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                      "+ CAST(tnaapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      " FROM  tnaapprover_master " & _
                      " JOIN tnaapprover_tran ON tnaapprover_master.tnamappingunkid = tnaapprover_tran.tnamappingunkid AND tnaapprover_tran.isvoid = 0" & _
                      " WHERE  tnaapprover_master.isvoid = 0 AND tnaapprover_master.isactive = 1 " & _
                      "  AND tnaapprover_master.isswap = 0 AND tnaapprover_master.isexternalapprover = @isexternalapprover "


            If mblnExternalApprover = False Then
                strQ &= " AND tnaapprover_master.approveremployeeunkid = @approveremployeeunkid "
            Else
                strQ &= " AND tnaapprover_master.mapuserunkid = @mapuserunkid "
            End If


            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            If blnIncludeCapApprover = False Then
                strQ &= " AND isotcap_hod = @isotcap_hod"
            End If
            'Pinkal (23-Nov-2019) -- End


            strQ &= " ORDER BY tnaapprover_tran.employeeunkid " & _
                       " FOR XML PATH('')), 1, 1, ''), '') EmployeeIds "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnExternalApprover)
            If mblnExternalApprover = False Then
                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpId)
            Else
                objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpId)
            End If


            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIncludeCapApprover)
            'Pinkal (23-Nov-2019) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeApprover(ByVal xDatabaseName As String _
                                        , ByVal xYearUnkid As Integer _
                                        , ByVal xCompanyUnkid As Integer _
                                        , ByVal mdtEmployeeAsonDate As Date _
                                        , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                        , Optional ByVal intUserunkid As Integer = -1 _
                                        , Optional ByVal strEmployeeIDs As String = "", Optional ByVal intApproverEmpId As Integer = -1 _
                                        , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                        , Optional ByVal blnExternalApprover As Boolean = False _
                                        , Optional ByVal mstrFilter As String = "" _
                                        , Optional ByVal blnIncludeCapApprover As Boolean = False) As DataTable

        'Pinkal (23-Nov-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[Optional ByVal blnIncludeCapApprover As Boolean = False]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try

            Dim StrQCondtion As String = ""
            Dim StrQCommJoin As String = ""
            Dim StrFinalQry As String = ""


            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)

            Dim dsCompany As New DataSet

            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = "SELECT " & _
                   "     tnaapprover_master.tnamappingunkid " & _
                   "    ,#NameValue# AS employeename " & _
                   "    ,#EmailValue# AS approveremail " & _
                   "    ,tnaapprover_master.tnalevelunkid " & _
                   "    ,tna_approverlevel_master.tnapriority " & _
                   "    ,tnaapprover_master.approveremployeeunkid " & _
                   "    ,tnaapprover_master.isexternalapprover " & _
                   "    ,tnaapprover_master.isotcap_hod "

            If strEmployeeIDs.Trim.Length > 0 Then

                strQ &= "   ,ISNULL(h2.employeecode,'') AS employeecode " & _
                            "   ,ISNULL(h2.employeeunkid, 0) AS employeeunkid " & _
                            "   ,ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') AS assigndemployeename " & _
                            "   ,tnaapprover_master.mapuserunkid "
            End If

            strQ &= " FROM tnaapprover_master " & _
                        "  LEFT JOIN tna_approverlevel_master ON tna_approverlevel_master.tnalevelunkid = tnaapprover_master.tnalevelunkid "

            StrQCommJoin = " #EMP_JOIN#  " & _
                                      " #DATE_JOIN# "


            If strEmployeeIDs.Trim.Length > 0 Then
                StrQCommJoin &= " JOIN tnaapprover_tran ON tnaapprover_tran.tnamappingunkid = tnaapprover_master.tnamappingunkid AND tnaapprover_tran.isvoid= 0 " & _
                                            " LEFT JOIN hremployee_master h2 ON h2.employeeunkid = tnaapprover_tran.employeeunkid "
            End If

            StrQCondtion = " WHERE tnaapprover_master.isvoid = 0 "

            If intUserunkid > 0 Then
                StrQCondtion &= " AND tnaapprover_master.mapuserunkid = @Userunkid "

                If intApproverEmpId > 0 Then

                    If blnExternalApprover Then
                        StrQCondtion &= " AND tnaapprover_master.approveremployeeunkid = @approverEmpId "
                    Else
                        StrQCondtion &= " AND hremployee_master.employeeunkid = @approverEmpId "
                    End If

                    objDataOperation.AddParameter("@approverEmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmpId.ToString)
                End If
                objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)
            End If

            If strEmployeeIDs.Trim.Length > 0 Then
                StrQCondtion &= " AND tnaapprover_tran.employeeunkid IN (" & strEmployeeIDs & ") "
            End If

            StrQCondtion &= " AND tnaapprover_master.isexternalapprover = #ExApr# "

            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            If blnIncludeCapApprover = False Then
                StrQCondtion &= " AND isotcap_hod = @isotcap_hod"
            End If
            'Pinkal (23-Nov-2019) -- End

            If mstrFilter.Trim.Length > 0 Then
                StrQCondtion &= " AND " & mstrFilter & " "
            End If

            StrFinalQry = strQ

            StrFinalQry &= StrQCommJoin & " "
            StrFinalQry &= StrQCondtion & " "
            StrFinalQry = StrFinalQry.Replace("#EMP_JOIN#", "LEFT JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = tnaapprover_master.approveremployeeunkid ")
            StrFinalQry = StrFinalQry.Replace("#DBName#", "")
            StrFinalQry = StrFinalQry.Replace("#NameValue#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
            StrFinalQry = StrFinalQry.Replace("#EmailValue#", "ISNULL(hremployee_master.email,'')")
            StrFinalQry = StrFinalQry.Replace("#DATE_JOIN#", xDateJoinQry)
            StrFinalQry = StrFinalQry.Replace("#ExApr#", "0")

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrFinalQry &= xDateFilterQry & " "
                End If
            End If


            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIncludeCapApprover)
            'Pinkal (23-Nov-2019) -- End

            dsList = objDataOperation.ExecQuery(StrFinalQry, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr As DataRow In dsCompany.Tables(0).Rows
                Dim dstmp As New DataSet

                xDateJoinQry = "" : xDateFilterQry = ""
                Dim StrDBName As String : StrFinalQry = "" : StrDBName = ""
                StrDBName = dr("DName").ToString()



                StrFinalQry = strQ

                StrFinalQry &= " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = tnaapprover_master.mapuserunkid " & vbCrLf & _
                               StrQCommJoin & " "

                StrFinalQry = StrFinalQry.Replace("#EMP_JOIN#", " LEFT JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid AND hremployee_master.isapproved = 1 ")

                StrFinalQry &= StrQCondtion & " AND tnaapprover_master.isexternalapprover = 1 AND cfuser_master.companyunkid = #CoyId# "

                StrFinalQry = StrFinalQry.Replace("#CoyId#", dr("companyunkid").ToString())
                StrFinalQry = StrFinalQry.Replace("#ExApr#", "1")

                If StrDBName.Trim.Length <= 0 Then
                    StrFinalQry = StrFinalQry.Replace("#DBName#", "")
                Else
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate").ToString()), eZeeDate.convertDate(dr("EDate").ToString()), , , dr("DName").ToString())
                    StrFinalQry = StrFinalQry.Replace("#DBName#", StrDBName & "..")
                End If

                StrFinalQry = StrFinalQry.Replace("#NameValue#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                StrFinalQry = StrFinalQry.Replace("#EmailValue#", "CASE WHEN ISNULL(hremployee_master.email,'') = '' THEN ISNULL(cfuser_master.email,'') ELSE ISNULL(hremployee_master.email,'') END ")

                StrFinalQry = StrFinalQry.Replace("#DATE_JOIN#", xDateJoinQry)

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrFinalQry &= xDateFilterQry & " "
                    End If
                End If

                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIncludeCapApprover)
                'Pinkal (23-Nov-2019) -- End

                dstmp = objDataOperation.ExecQuery(StrFinalQry, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next

            Return dsList.Tables(0)


        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprover; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeFromUser(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                  , ByVal mdtEmployeeAsOnDate As Date, ByVal blnIncludeInactiveEmployee As Boolean _
                                                                  , ByVal intUserID As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)

            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename, '  ' + @Select AS EmpCodeName  " & _
                      " UNION " & _
                      " SELECT " & _
                      "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
                      "	,ISNULL(employeecode,'') AS employeecode " & _
                      "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
                      " ,ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName " & _
                      " FROM tnaapprover_tran " & _
                      " JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaapprover_tran.tnamappingunkid AND tnaapprover_master.isvoid = 0  AND tnaapprover_master.mapuserunkid = @UserID " & _
                      "	JOIN hremployee_master ON tnaapprover_tran.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE 1 = 1  AND tnaapprover_tran.isvoid = 0 AND hremployee_master.isapproved = 1 "

            If blnIncludeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            Dim iExApproverIds As String = ""

            iExApproverIds = GetTnAApproverUnkid(intUserID, True, , objDataOperation)

            If iExApproverIds.Trim.Length > 0 Then
                strQ &= "UNION " & _
                        " SELECT " & _
                        "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
                        "	,ISNULL(employeecode,'') AS employeecode " & _
                        "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
                        "   ,ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName " & _
                        " FROM tnaapprover_tran " & _
                        "  JOIN hremployee_master ON tnaapprover_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                strQ &= " WHERE 1 = 1  AND tnaapprover_tran.isvoid = 0 AND hremployee_master.isapproved = 1 AND tnaapprover_tran.tnamappingunkid IN (" & iExApproverIds & ") "

                If blnIncludeInactiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

            End If

            strQ &= "ORDER BY employeename "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@UserID", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            objDataOperation.AddParameter("@Usertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enUserType.Timesheet_Approver)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromUser", mstrMessage)
            Return Nothing
        End Try
    End Function

    Public Function GetTnAApproverUnkid(ByVal intTnAApproverEmpUnkid As Integer, _
                                                          ByVal blnExternalApprover As Boolean, _
                                                          Optional ByVal intLevelId As Integer = -1, _
                                                          Optional ByVal objDataOpr As clsDataOperation = Nothing) As String

        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim strApproverUnkids As String = ""
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            StrQ = "SELECT " & _
                       "        @approverunkid = ISNULL(STUFF((SELECT ',' + CAST(tnamappingunkid AS NVARCHAR(10)) " & _
                       "   FROM tnaapprover_master " & _
                       "   WHERE tnaapprover_master.isvoid = 0 AND tnaapprover_master.isactive = 1 AND tnaapprover_master.isswap = 0 " & _
                       "   AND tnaapprover_master.approveremployeeunkid = @approveremployeeunkid  AND tnaapprover_master.isexternalapprover = @ExternalApprover "

            If intLevelId >= 0 Then
                StrQ &= " AND tnaapprover_master.tnalevelunkid = @tnalevelunkid "
            End If

            StrQ &= " FOR XML PATH('')),1,1,''),'') "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTnAApproverEmpUnkid)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intLevelId)
            objDataOperation.AddParameter("@ExternalApprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strApproverUnkids, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strApproverUnkids = CStr(objDataOperation.GetParameterValue("@approverunkid"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetTnAApproverUnkid ; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return strApproverUnkids
    End Function

    'Pinkal (23-Nov-2019) -- End



    'Pinkal (03-Mar-2020) -- Start
    'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.

    Public Function GetOTApproverLevels(ByVal xOTApprEmpId As Integer, ByVal blnExternalApprover As Boolean, Optional ByVal iFlag As Boolean = True) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            Dim strApproverUnkids As String = ""
            strApproverUnkids = GetTnAApproverUnkid(xOTApprEmpId, blnExternalApprover, -1, Nothing)


            If iFlag = True Then
                strQ = " SELECT 0 AS Id, @Select AS NAME UNION "
            End If

            strQ &= "SELECT " & _
                        "   tna_approverlevel_master.tnalevelunkid AS Id, tnalevelname AS NAME " & _
                        " FROM tnaapprover_master " & _
                        " JOIN tna_approverlevel_master ON tnaapprover_master.tnalevelunkid = tna_approverlevel_master.tnalevelunkid " & _
                        " WHERE tnaapprover_master.isvoid = 0 AND tnaapprover_master.isswap = 0  "

            If xOTApprEmpId > 0 Then
                strQ &= " AND approveremployeeunkid = @approveremployeeunkid "
            End If

            If strApproverUnkids.Trim.Length > 0 Then
                strQ &= " AND tnaapprover_master.tnamappingunkid IN (" & strApproverUnkids & ") "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTApprEmpId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetOTApproverLevels ; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function GetOTApproverDetails(ByVal xApproverEmployeeId As Integer, ByVal mblnIsCapApprover As Boolean, Optional ByVal xLevelId As Integer = -1) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            strQ = "SELECT " & _
                    "   tnaapprover_master.tnamappingunkid,tnaapprover_master.tnalevelunkid,tnaapprover_master.mapuserunkid,tnaapprover_master.approveremployeeunkid,tnaapprover_master.isotcap_hod,tnaapprover_master.isexternalapprover " & _
                    " FROM tnaapprover_master " & _
                    " WHERE tnaapprover_master.isvoid = 0 AND tnaapprover_master.isswap = 0  " & _
                    " AND tnaapprover_master.approveremployeeunkid = @approveremployeeunkid AND tnaapprover_master.isotcap_hod = @isotcap_hod "

            If xLevelId > 0 Then
                strQ &= " AND tnaapprover_master.tnalevelunkid = @tnalevelunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("isotcap_hod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCapApprover)
            objDataOperation.AddParameter("approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverEmployeeId)
            objDataOperation.AddParameter("tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLevelId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetOTApproverDetails ; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function


    Public Function Migration_Insert(ByVal xFromApproverTranId As Integer, ByVal xToApproverTranId As Integer, ByVal dtTable As DataTable _
                                                 , ByVal xUserId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""
        Dim objApproverTran As New clsTnaapprover_Tran
        Try

            Dim objDataOperation As clsDataOperation

            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If

            If dtTable Is Nothing Then Return True



            '/* START TO GET NEW APPROVER LEVEL ID AND PRIORITY
            Dim xNewApproverLevelId As Integer = 0
            Dim xNewApproverPriority As Integer = 0
            Dim xNewApproverMapUserId As Integer = 0

            _Tnamappingunkid(objDataOperation) = xToApproverTranId
            xNewApproverLevelId = mintTnalevelunkid
            xNewApproverMapUserId = mintMapuserunkid

            Dim objApproverLevel As New clsTna_approverlevel_master
            objApproverLevel._Tnalevelunkid(objDataOperation) = mintTnalevelunkid
            xNewApproverPriority = objApproverLevel._Tnapriority
            objApproverLevel = Nothing

            For i = 0 To dtTable.Rows.Count - 1


                strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM tnaapprover_tran WHERE tnamappingunkid = @tnamappingunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xToApproverTranId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                Dim dtEmpCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                If dtEmpCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dtEmpCount.Tables(0).Rows(0)("employeeunkid")) > 0 Then
                        Continue For
                    End If
                End If


                strQ = " Update tnaapprover_tran set " & _
                          " isvoid = 1,voiddatetime=GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE tnamappingunkid = @tnamappingunkid AND employeeunkid = @employeeunkid"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("tnamappingunkid").ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 7, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objApproverTran._TnAapprovertranunkid = CInt(dtTable.Rows(i)("tnaapprovertranunkid"))
                objApproverTran._TnAMappingUnkId = CInt(dtTable.Rows(i)("tnamappingunkid"))
                objApproverTran._AuditUserId = mintAuditUserId
                objApproverTran._AuditDatetime = mdtAuditDatetime
                objApproverTran._HostName = mstrHostName
                objApproverTran._ClientIP = mstrClientIP
                objApproverTran._FormName = mstrFormName
                objApproverTran._IsFromWeb = mblnIsFromWeb

                If objApproverTran.InsertAuditTrailApproverTran(objDataOperation, 3, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If



                strQ = "INSERT INTO tnaapprover_tran ( " & _
                                          " tnamappingunkid " & _
                                          ", employeeunkid " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                      ") VALUES (" & _
                                          "  @tnamappingunkid " & _
                                          ", @employeeunkid " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          "); SELECT @@identity"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xToApproverTranId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objApproverTran._TnAapprovertranunkid = CInt(dsList.Tables(0).Rows(0)(0))
                objApproverTran._TnAMappingUnkId = xToApproverTranId
                objApproverTran._HostName = mstrHostName
                objApproverTran._ClientIP = mstrClientIP
                objApproverTran._FormName = mstrFormName
                objApproverTran._IsFromWeb = mblnIsFromWeb
                objApproverTran._AuditUserId = mintAuditUserId
                objApproverTran._AuditDatetime = mdtAuditDatetime

                If objApproverTran.InsertAuditTrailApproverTran(objDataOperation, 1, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                Dim objOTRequisition As New clsOT_Requisition_Tran
                Dim mstrFormId As String = objOTRequisition.GetApproverPendingOTRequisition(xFromApproverTranId, dtTable.Rows(i)("employeeunkid").ToString(), objDataOperation)
                objOTRequisition = Nothing

                If mstrFormId.Trim.Length > 0 Then

                    strQ = " Update tnaotrequisition_approval_tran SET  " & _
                              " tnamappingunkid  = @tnamappingunkid ,mapuserunkid = @mapuserunkid " & _
                              " ,tnalevelunkid = @tnalevelunkid, tnapriority = @tnapriority  " & _
                              " WHERE otrequisitiontranunkid in (" & mstrFormId & ") AND tnamappingunkid = @oldtnamappingunkid " & _
                              " AND tnaotrequisition_approval_tran.isvoid = 0 AND tnaotrequisition_approval_tran.statusunkid = 2 "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@oldtnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFromApproverTranId)
                    objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xToApproverTranId)
                    objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverMapUserId)
                    objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverLevelId)
                    objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverPriority)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = " Select ISNULL(otrequestapprovaltranunkid,0) otrequestapprovaltranunkid FROM  " & _
                              " tnaotrequisition_approval_tran  " & _
                              " WHERE isvoid = 0 AND tnaotrequisition_approval_tran.statusunkid = 2 AND tnamappingunkid = @tnamappingunkid AND tnalevelunkid = @tnalevelunkid " & _
                              " AND tnapriority = @tnapriority AND mapuserunkid = @mapuserunkid AND otrequisitiontranunkid  in (" & mstrFormId & ")"

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xToApproverTranId)
                    objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverLevelId)
                    objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverPriority)
                    objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverMapUserId)

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If dsList.Tables(0).Rows.Count > 0 Then
                        Dim objOTApproval As New clsTnaotrequisition_approval_Tran
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                            'Pinkal (05-May-2020) -- Start
                            'Bug NMB -   solved bug there is no row at position 0 .
                            'objOTApproval._Otrequestapprovaltranunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(i)("otrequestapprovaltranunkid"))
                            objOTApproval._Otrequestapprovaltranunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(k)("otrequestapprovaltranunkid"))
                            'Pinkal (05-May-2020) -- End

                            objOTApproval._ClientIp = mstrClientIP
                            objOTApproval._Hostname = mstrHostName
                            objOTApproval._Form_Name = mstrFormName
                            objOTApproval._Isweb = mblnIsFromWeb
                            objOTApproval._Userunkid = xUserId
                            objOTApproval._Audituserunkid = xUserId
                            objOTApproval._Auditdatetime = mdtAuditDatetime
                            If objOTApproval.AtInsertApprovalTran(objDataOperation, 2) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next
                        objOTApproval = Nothing
                    End If

                End If

            Next

            objApproverTran._EmployeeAsonDate = ConfigParameter._Object._CurrentDateAndTime.Date
            objApproverTran._TnAMappingUnkId(objDataOperation) = xFromApproverTranId
            objApproverTran.Get_Data(objDataOperation)

            If objApproverTran._DataTable IsNot Nothing AndAlso objApproverTran._DataTable.Rows.Count <= 0 Then

                strQ = " Update tnaapprover_master set " & _
                           " isvoid = 1,voiddatetime=GetDate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                           " WHERE tnamappingunkid = @tnamappingunkid AND isvoid =  0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFromApproverTranId)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 7, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _Tnamappingunkid(objDataOperation) = xFromApproverTranId
                mintAuditUserId = xUserId
                mintUserunkid = xUserId

                If InsertAuditTrailApproverMapping(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Migration_Insert; Module Name: " & mstrModuleName)
        End Try
    End Function

    'S.SANDEEP |04-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(tnaapprovertranunkid,0) as tnaapprovertranunkid FROM tnaapprover_tran WHERE tnamappingunkid = @tnamappingunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("tnaapprovertranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try



    End Function
    'S.SANDEEP |04-MAR-2020| -- END


    'Pinkal (03-Mar-2020) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "This Approver already exists. Please define new Approver.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Yes")
			Language.setMessage(mstrModuleName, 6, "No")
			Language.setMessage(mstrModuleName, 7, "Migration")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
