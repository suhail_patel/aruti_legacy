﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clsTna_approverlevel_master.vb
'Purpose    :
'Date       :19-Oct-2018
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsTna_approverlevel_master
    Private Shared ReadOnly mstrModuleName As String = "clsTna_approverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTnalevelunkid As Integer
    Private mintTnatypeid As Integer
    Private mstrTnalevelcode As String = String.Empty
    Private mstrTnalevelname As String = String.Empty
    Private mintTnapriority As Integer
    Private mstrTnalevelname1 As String = String.Empty
    Private mstrTnalevelname2 As String = String.Empty
    Private mblnIsactive As Boolean = True

    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mintLoginEmployeeunkid As Integer
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnalevelunkid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnalevelunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTnalevelunkid
        End Get
        Set(ByVal value As Integer)
            mintTnalevelunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnatypeid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnatypeid() As Integer
        Get
            Return mintTnatypeid
        End Get
        Set(ByVal value As Integer)
            mintTnatypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnalevelcode
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnalevelcode() As String
        Get
            Return mstrTnalevelcode
        End Get
        Set(ByVal value As String)
            mstrTnalevelcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnalevelname
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnalevelname() As String
        Get
            Return mstrTnalevelname
        End Get
        Set(ByVal value As String)
            mstrTnalevelname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnapriority
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnapriority() As Integer
        Get
            Return mintTnapriority
        End Get
        Set(ByVal value As Integer)
            mintTnapriority = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnalevelname1
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnalevelname1() As String
        Get
            Return mstrTnalevelname1
        End Get
        Set(ByVal value As String)
            mstrTnalevelname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tnalevelname2
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Tnalevelname2() As String
        Get
            Return mstrTnalevelname2
        End Get
        Set(ByVal value As String)
            mstrTnalevelname2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  tnalevelunkid " & _
                      ", tnatypeid " & _
                      ", tnalevelcode " & _
                      ", tnalevelname " & _
                      ", tnapriority " & _
                      ", tnalevelname1 " & _
                      ", tnalevelname2 " & _
                      ", isactive " & _
                      " FROM tna_approverlevel_master " & _
                      " WHERE tnalevelunkid = @tnalevelunkid "

            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTnalevelunkid = CInt(dtRow.Item("tnalevelunkid"))
                mintTnatypeid = CInt(dtRow.Item("tnatypeid"))
                mstrTnalevelcode = dtRow.Item("tnalevelcode").ToString
                mstrTnalevelname = dtRow.Item("tnalevelname").ToString
                If dtRow.Item("tnapriority") IsNot DBNull.Value Then mintTnapriority = CInt(dtRow.Item("tnapriority"))
                mstrTnalevelname1 = dtRow.Item("tnalevelname1").ToString
                mstrTnalevelname2 = dtRow.Item("tnalevelname2").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnblank As Boolean = False, Optional ByVal strfilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            If mblnblank Then
                strQ = "SELECT " & _
                       "  0 AS tnalevelunkid " & _
                       ", '' AS tnalevelcode " & _
                       ", '' AS tnalevelname " & _
                       ", 0 AS tnapriority " & _
                       ", '' AS tnalevelname1 " & _
                       ", '' AS tnalevelname2 " & _
                       ", 0 AS isactive " & _
                       " UNION ALL "
            End If

            strQ &= "SELECT " & _
                    "  tnalevelunkid " & _
                    ", tnalevelcode " & _
                    ", tnalevelname " & _
                    ", tnapriority " & _
                    ", tnalevelname1 " & _
                    ", tnalevelname2 " & _
                    ", isactive " & _
                    "FROM tna_approverlevel_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            If strfilter <> "" Then
                strQ += "And " + strfilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tna_approverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintTnatypeid, mstrTnalevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Level Code is already defined. Please define new Level Code.")
            Return False
        ElseIf isExist(mintTnatypeid, "", mstrTnalevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Level Name is already defined. Please define new Level Name.")
            Return False
        ElseIf isPriorityExist(mintTnatypeid, mintTnapriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Level priority is already assigned. Please assign new Level priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnatypeid.ToString)
            objDataOperation.AddParameter("@tnalevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelcode.ToString)
            objDataOperation.AddParameter("@tnalevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname.ToString)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority.ToString)
            objDataOperation.AddParameter("@tnalevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname1.ToString)
            objDataOperation.AddParameter("@tnalevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO tna_approverlevel_master ( " & _
                      "  tnatypeid " & _
                      ", tnalevelcode " & _
                      ", tnalevelname " & _
                      ", tnapriority " & _
                      ", tnalevelname1 " & _
                      ", tnalevelname2 " & _
                      ", isactive" & _
                    ") VALUES (" & _
                      "  @tnatypeid " & _
                      ", @tnalevelcode " & _
                      ", @tnalevelname " & _
                      ", @tnapriority " & _
                      ", @tnalevelname1 " & _
                      ", @tnalevelname2 " & _
                      ", @isactive" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTnalevelunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrailForLevel(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tna_approverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintTnatypeid, mstrTnalevelcode, "", mintTnalevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Level Code is already defined. Please define new Level Code.")
            Return False
        ElseIf isExist(mintTnatypeid, "", mstrTnalevelname, mintTnalevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Level Name is already defined. Please define new Level Name.")
            Return False
        ElseIf isPriorityExist(mintTnatypeid, mintTnapriority, mintTnalevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Level priority is already assigned. Please assign new Level priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid.ToString)
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnatypeid.ToString)
            objDataOperation.AddParameter("@tnalevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelcode.ToString)
            objDataOperation.AddParameter("@tnalevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname.ToString)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority.ToString)
            objDataOperation.AddParameter("@tnalevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname1.ToString)
            objDataOperation.AddParameter("@tnalevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE tna_approverlevel_master SET " & _
                   "  tnatypeid = @tnatypeid " & _
                   ", tnalevelcode = @tnalevelcode" & _
                   ", tnalevelname = @tnalevelname" & _
                   ", tnapriority = @tnapriority" & _
                   ", tnalevelname1 = @tnalevelname1" & _
                   ", tnalevelname2 = @tnalevelname2" & _
                   ", isactive = @isactive " & _
                   "WHERE tnalevelunkid = @tnalevelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLevel(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tna_approverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " Update tna_approverlevel_master set isactive = 0 " & _
                      " WHERE tnalevelunkid  = @tnalevelunkid  "

            objDataOperation.AddParameter("@tnalevelunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Tnalevelunkid(objDataOperation) = intUnkid

            If InsertAuditTrailForLevel(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "select isnull(tnalevelunkid,0) FROM tnaapprover_master WHERE tnalevelunkid = @tnalevelunkid"
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xTnaTypeId As Integer, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "  tnalevelunkid " & _
                       ", tnatypeid " & _
                       ", tnalevelcode " & _
                       ", tnalevelname " & _
                       ", tnapriority " & _
                       ", tnalevelname1 " & _
                       ", tnalevelname2 " & _
                       ", isactive " & _
                       " FROM tna_approverlevel_master " & _
                       " WHERE isactive = 1 AND tnatypeid = @tnatypeid "

            If strCode.Length > 0 Then
                strQ &= " AND tnalevelcode = @tnalevelcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND tnalevelname = @tnalevelname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND tnalevelunkid <> @tnalevelunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnaTypeId)
            objDataOperation.AddParameter("@tnalevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@tnalevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal xTnaTypeId As Integer, ByVal mintTnapriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  tnalevelunkid " & _
                      ", tnatypeid " & _
                      ", tnalevelcode " & _
                      ", tnalevelname " & _
                      ", tnapriority " & _
                      ", isactive " & _
                      ", tnalevelname1 " & _
                      ", tnalevelname2 " & _
                     " FROM tna_approverlevel_master " & _
                     " WHERE tnapriority = @tnapriority AND isactive = 1 AND tnatypeid = @tnatypeid "

            If intUnkid > 0 Then
                strQ &= " AND tnalevelunkid <> @tnalevelunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTnaTypeId)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForLevel(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO attna_approverlevel_master ( " & _
                       "  tranguid " & _
                       ", tnatypeid " & _
                       ", tnalevelunkid " & _
                       ", tnalevelcode " & _
                       ", tnalevelname " & _
                       ", tnapriority " & _
                       ", tnalevelname1 " & _
                       ", tnalevelname2 " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip" & _
                       ", hostname" & _
                       ", form_name " & _
                       ", isweb " & _
                       ", loginemployeeunkid " & _
                   ") VALUES (" & _
                       "  @tranguid " & _
                       ", @tnatypeid " & _
                       ", @tnalevelunkid " & _
                       ", @tnalevelcode " & _
                       ", @tnalevelname " & _
                       ", @tnapriority " & _
                       ", @tnalevelname1 " & _
                       ", @tnalevelname2 " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip" & _
                       ", @hostname" & _
                       ", @form_name " & _
                       ", @isweb " & _
                       ", @loginemployeeunkid " & _
                   ");"

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@tnatypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnatypeid)
            objDataOperation.AddParameter("@tnalevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnalevelunkid)
            objDataOperation.AddParameter("@tnalevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelcode.ToString)
            objDataOperation.AddParameter("@tnalevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname.ToString)
            objDataOperation.AddParameter("@tnapriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnapriority.ToString)
            objDataOperation.AddParameter("@tnalevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname1.ToString)
            objDataOperation.AddParameter("@tnalevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTnalevelname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForLevel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as tnalevelunkid, ' ' +  @name  as name, 0 AS tnapriority UNION "
            End If
            strQ &= "SELECT " & _
                        "  tnalevelunkid " & _
                        " ,tnalevelname as name " & _
                        " ,tnapriority AS tnapriority " & _
                        "FROM tna_approverlevel_master " & _
                        "WHERE isactive = 1 ORDER BY tnapriority "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Level Code is already defined. Please define new Level Code.")
			Language.setMessage(mstrModuleName, 2, "This Level Name is already defined. Please define new Level Name.")
			Language.setMessage(mstrModuleName, 3, "This Level priority is already assigned. Please assign new Level priority.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
