﻿'************************************************************************************************************************************
'Class Name : clsemployee_absent.vb
'Purpose    : All Employe Absent Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :07/08/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 1
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
Imports System.ComponentModel

'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsemployee_absent

    Private Shared ReadOnly mstrModuleName As String = "clsemployee_absent"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPeriodunkid As Integer
    Private mdtStartdate As DateTime
    Private mdtEnddate As DateTime
    Private intLeaveCount As Integer = 0
    Private intLeavebalance As Integer = 0

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END


    'Pinkal (07-OCT-2014) -- Start
    'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 
    Private minTotalEmployeeCount As Integer = 0
    'Pinkal (07-OCT-2014) -- End


#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _StartDate() As DateTime
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As DateTime)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EndDate() As DateTime
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As DateTime)
            mdtEnddate = value
        End Set
    End Property


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END


    'Pinkal (07-OCT-2014) -- Start
    'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 
    Public ReadOnly Property _TotalEmployeeCount() As Integer
        Get
            Return minTotalEmployeeCount
        End Get
    End Property
    'Pinkal (07-OCT-2014) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertEmployeeAbsent(ByVal xDataBaseName As String, _
                                         ByVal xUserUnkid As Integer, _
                                         ByVal xYearUnkid As Integer, _
                                         ByVal xCompanyunkid As Integer, _
                                         ByVal xUserModeSetting As String, _
                                         ByVal xOnlyApproved As Boolean, _
                                         ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                         ByVal blnShowFirstAppointmentDate As Boolean, _
                                         ByVal blnPolicyManagementTNA As Boolean, _
                                         ByVal intPeriodunkid As Integer, _
                                         ByVal blnDonotAttendanceinSeconds As Boolean, _
                                         ByVal blnFirstCheckInLastCheckOut As Boolean, _
                                         ByVal blnIsHolidayConsiderOnWeekend As Boolean, _
                                         ByVal blnIsDayOffConsiderOnWeekend As Boolean, _
                                         ByVal blnIsHolidayConsiderOnDayoff As Boolean, _
                                         Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                         Optional ByVal intEmpId As Integer = -1, Optional ByVal strUserAccessLevelFilterString As String = "", _
                                         Optional ByVal mstrFilter As String = "", Optional ByVal bw As BackgroundWorker = Nothing) As Boolean


        'Pinkal (11-AUG-2017) -- Start
        'Enhancement - Working On B5 Plus Company TnA Enhancements.
        ' ByVal mblnIsHolidayConsiderOnWeekend As Boolean _
        ', ByVal mblnIsDayOffConsiderOnWeekend As Boolean _
        ', ByVal mblnIsHolidayConsiderOnDayoff As Boolean _
        'Pinkal (11-AUG-2017) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            'START FOR GET EMPLOYEE DATA
            Dim objEmployee As New clsEmployee_Master

            Dim dsEmployee As DataSet

            'Pinkal (16-Nov-2017) -- Start
            'Enhancement - Solved Employee Absent Performance Issue.

            'dsEmployee = objEmployee.GetList(xDataBaseName, _
            '                                 xUserUnkid, _
            '                                 xYearUnkid, _
            '                                 xCompanyunkid, _
            '                                 mdtStartdate, _
            '                                 mdtEnddate, _
            '                                 xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, _
            '                                 "List", blnShowFirstAppointmentDate)

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name

            dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, xDataBaseName, _
                                             xUserUnkid, _
                                             xYearUnkid, _
                                             xCompanyunkid, _
                                             mdtStartdate, _
                                             mdtEnddate, _
                                             xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, _
                                         "List", -1, False, "", blnShowFirstAppointmentDate)

            'Pinkal (16-Nov-2017) -- End



            If dsEmployee.Tables("List").Rows.Count = 0 Then Exit Function
            'END FOR GET EMPLOYEE DATA

            'START FOR GET PERIOD DATES
            Dim dsPeriod As DataSet = GeneratePeriodDate(objDataOperation)
            'END FOR GET PERIOD DATES

            Dim dtEmployee As DataTable = Nothing
            If dsEmployee.Tables(0).Rows.Count > 0 Then
                dtEmployee = New DataView(dsEmployee.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If



            Dim objlogin As New clslogin_Tran
            Dim objShift As New clsNewshift_master
            Dim objShiftTran As New clsshift_tran
            Dim objEmpHoliday As New clsemployee_holiday
            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim objEmpDayOff As New clsemployee_dayoff_Tran


            'Pinkal (07-OCT-2014) -- Start
            'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 
            minTotalEmployeeCount = dtEmployee.Rows.Count
            bw.ReportProgress(0)
            Dim intCount As Integer = 0
            'Pinkal (07-OCT-2014) -- End

            'Pinkal (12-Jan-2022)-- Start
            'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
            Dim mstrGroupName As String = ""
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroupName = objGroup._Groupname
            objGroup = Nothing
            'Pinkal (12-Jan-2022) -- End

            For i As Integer = 0 To dtEmployee.Rows.Count - 1    '------------------------------------------'START FOR EMPLOYEE LOOP

                objEmployee._Employeeunkid(mdtEnddate) = CInt(dtEmployee.Rows(i)("employeeunkid"))

                'Pinkal (16-Nov-2017) -- Start
                'Enhancement - Solved Employee Absent Performance Issue.
                intEmpId = CInt(dtEmployee.Rows(i)("employeeunkid"))
                'Pinkal (16-Nov-2017) -- End

                intLeaveCount = 0
                intLeavebalance = 0
                For j As Integer = 0 To dsPeriod.Tables("List").Rows.Count - 1   '-----------------------------------------'START FOR PERIOD DATES LOOP

                    'START FOR CHECK EMPLOYEE APPOINTDATE 
                    If objEmployee._Appointeddate.Date <> Nothing AndAlso objEmployee._Appointeddate.Date > CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate")).Date Then
                        Continue For
                    End If
                    'END FOR CHECK EMPLOYEE APPOINTDATE 


                    'Pinkal (07-Jan-2015) -- Start
                    'Enhancement - CHANGES IN IMPORT TIME & ATTENDANCE DATA FROM DEVICE FOR OCEAN LINK
                    'Issue : commented for voltamp issue reinstatement date is getting less than period date then system is not doing absent process.
                    'If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate")).Date) Then
                    '    Continue For
                    'End If


                    'Pinkal (21-Apr-2017) -- Start
                    'Enhancement - Commented because When Employee is Termindated and rehire again then it will not do absent process. 

                    'Dim objEmpDates As New clsemployee_dates_tran
                    'Dim dsRetr As New DataSet
                    'dsRetr = objEmpDates.Get_Current_Dates(mdtEnddate, enEmp_Dates_Transaction.DT_TERMINATION, CInt(dtEmployee.Rows(i)("employeeunkid")))
                    'If dsRetr.Tables(0).Rows.Count > 0 Then
                    '    If CDate(dsRetr.Tables(0).Rows(0).Item("date1")).Date < CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate")).Date Then
                    '        Continue For
                    '    End If
                    'End If

                    If objEmployee._Empl_Enddate <> Nothing AndAlso (objEmployee._Empl_Enddate.Date < CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate")).Date) Then
                        Continue For
                    End If

                    If objEmployee._Termination_From_Date <> Nothing AndAlso (objEmployee._Termination_From_Date.Date < CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate")).Date) Then
                            Continue For
                        End If

                    If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate")).Date) Then
                        Continue For
                    End If

                    'Pinkal (21-Apr-2017) -- End

                    'Pinkal (07-Jan-2015) -- End




                    objlogin._Employeeunkid = CInt(dtEmployee.Rows(i)("employeeunkid"))
                    objlogin._Logindate = CDate(dsPeriod.Tables("List").Rows(j)("PeriodDate"))
                    objShift._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(objlogin._Logindate, objlogin._Employeeunkid)
                    objlogin._Shiftunkid = objShift._Shiftunkid

                    If blnPolicyManagementTNA Then
                        Dim objEmpPolicyTran As New clsemployee_policy_tran
                        objlogin._PolicyId = objEmpPolicyTran.GetPolicyTranID(objlogin._Logindate, objlogin._Employeeunkid)
                    End If

                    objlogin._Userunkid = xUserUnkid
                    objShiftTran.GetShiftTran(objShift._Shiftunkid)
                    If objShiftTran._dtShiftday Is Nothing Or objShiftTran._dtShiftday.Rows.Count <= 0 Then Continue For

                    Dim drShiftDay() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(objlogin._Logindate.DayOfWeek.ToString()))
                    If drShiftDay.Length <= 0 Then Continue For

                    'START FOR GET EMPLOYEE LEAVE DATA
                    Dim LoginStatus As Integer = GetEmployeeLeaveData(objDataOperation, CInt(dtEmployee.Rows(i)("employeeunkid")), objlogin._Logindate.Date, _
                                                                      CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)
                    'END FOR GET EMPLOYEE LEAVE DATA
                    objlogin._IsUnPaidLeave = False
                    objlogin._IsPaidLeave = False
                    objlogin._IsWeekend = False
                    objlogin._IsHoliday = False
                    objlogin._IsDayOffShift = False


                    'Pinkal (25-AUG-2017) -- Start
                    'Enhancement - Working on B5 plus TnA Enhancement.
                    'objlogin._LeaveDayFraction = GetLeaveDayFraction(objDataOperation, CInt(dtEmployee.Rows(i)("employeeunkid")), objlogin._Logindate.Date)
                    objlogin._LeaveTypeId = 0
                    objlogin._LeaveDayFraction = GetLeaveDayFraction(objDataOperation, CInt(dtEmployee.Rows(i)("employeeunkid")), objlogin._Logindate.Date, objlogin._LeaveTypeId)
                    'Pinkal (25-AUG-2017) -- End




                    If LoginStatus = 7 Then  'Weekend 

                        If IsEmployeeLoginExist(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate) Then
                            Dim dsLoginSummary As DataSet = IsEmployeeLoginSummaryExist(objlogin._Employeeunkid, objlogin._Logindate)
                            If dsLoginSummary IsNot Nothing AndAlso dsLoginSummary.Tables(0).Rows.Count <= 0 Then

                                'Pinkal (22-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                'objlogin._DayType = 1
                                objlogin._DayType = 0
                                'Pinkal (22-AUG-2017) -- End
                                objlogin._InOutType = 1
                                objlogin._TotalOvertimehr = 0
                                objlogin._Shorthr = 0
                                objlogin._Totalhr = 0
                                objlogin._TotalNighthr = 0
                                objlogin._IsUnPaidLeave = False
                                objlogin._IsPaidLeave = False
                                objlogin._IsWeekend = True
                                objlogin._IsAbsentProcess = True
                                objlogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                                objlogin._Ot2 = 0
                                objlogin._Ot3 = 0
                                objlogin._Ot4 = 0
                                objlogin._Early_Coming = 0
                                objlogin._Late_Coming = 0
                                objlogin._Early_Going = 0
                                objlogin._Late_Going = 0


                                'Pinkal (11-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                    objlogin._IsDayOffShift = True
                                    If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                        objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                    End If
                                End If
                                'Pinkal (11-AUG-2017) -- End


                                Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                                If mintStatus = 1 Then
                                    objlogin._IsUnPaidLeave = False
                                    objlogin._IsPaidLeave = True
                                    'Pinkal (25-AUG-2017) -- Start
                                    'Enhancement - Working on B5 plus TnA Enhancement.
                                    objlogin._DayType = 1
                                    'Pinkal (25-AUG-2017) -- End
                                ElseIf mintStatus = 2 Then
                                    objlogin._IsUnPaidLeave = True
                                    objlogin._IsPaidLeave = False
                                ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                    objlogin._IsUnPaidLeave = True
                                    objlogin._IsPaidLeave = False
                                ElseIf mintStatus = 5 Then
                                    objlogin._IsUnPaidLeave = False
                                    objlogin._IsPaidLeave = False
                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsWeekend = False
                                    objlogin._IsHoliday = True
                                    If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                        objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                    End If
                                    'Pinkal (11-AUG-2017) -- End
                                End If


                                'Pinkal (25-AUG-2017) -- Start
                                'Enhancement - Working on B5 plus TnA Enhancement.
                                If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                    objlogin._IsHoliday = False
                                    objlogin._IsDayOffShift = False
                                    objlogin._IsWeekend = False
                                End If
                                'Pinkal (25-AUG-2017) -- End




                                With objlogin
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With


                                objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                    , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                         , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                         , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                         , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                         , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                  , strUserAccessLevelFilterString, mstrFilter, 0, True)





                            Else
                                objlogin._IsDayOffShift = False
                                objlogin._IsUnPaidLeave = False
                                objlogin._IsPaidLeave = False
                                objlogin._IsWeekend = True


                                'Pinkal (11-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                    objlogin._IsDayOffShift = True
                                End If

                                Dim objholiday As New clsemployee_holiday
                                Dim dtHoliday As DataTable = objholiday.GetEmployeeHoliday(objlogin._Employeeunkid, objlogin._Logindate)
                                If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                                    objlogin._IsHoliday = True
                                End If

                                'Pinkal (11-AUG-2017) -- End

                                Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                                If mintStatus = 1 Then
                                    objlogin._IsUnPaidLeave = False
                                    objlogin._IsPaidLeave = True
                                    'Pinkal (25-AUG-2017) -- Start
                                    'Enhancement - Working on B5 plus TnA Enhancement.
                                    objlogin._DayType = 1
                                    'Pinkal (25-AUG-2017) -- End
                                ElseIf mintStatus = 2 Then
                                    objlogin._IsUnPaidLeave = True
                                    objlogin._IsPaidLeave = False
                                ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                    objlogin._IsUnPaidLeave = True
                                    objlogin._IsPaidLeave = False
                                ElseIf mintStatus = 5 Then
                                    objlogin._IsUnPaidLeave = False
                                    objlogin._IsPaidLeave = False

                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsWeekend = False
                                    objlogin._IsHoliday = True
                                    'Pinkal (11-AUG-2017) -- End
                                End If

                                'Pinkal (11-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                    objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                    objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                End If

                                If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                    objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                    objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                End If

                                'Pinkal (11-AUG-2017) -- End


                                'Pinkal (25-AUG-2017) -- Start
                                'Enhancement - Working on B5 plus TnA Enhancement.
                                If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                    objlogin._IsHoliday = False
                                    objlogin._IsDayOffShift = False
                                    objlogin._IsWeekend = False
                                End If
                                'Pinkal (25-AUG-2017) -- End


                                GoTo ForPresentEmployee
                            End If
                        Else
                            objlogin._checkintime = Nothing
                            objlogin._Holdunkid = -1
                            objlogin._Checkouttime = Nothing
                            objlogin._Original_InTime = Nothing
                            objlogin._Original_OutTime = Nothing
                            objlogin._Workhour = 0
                            'Pinkal (22-AUG-2017) -- Start
                            'Enhancement - Working On B5 Plus Company TnA Enhancements.
                            'objlogin._DayType = 1
                            objlogin._DayType = 0
                            'Pinkal (22-AUG-2017) -- End
                            objlogin._InOutType = 1
                            objlogin._TotalOvertimehr = 0
                            objlogin._Shorthr = 0
                            objlogin._Totalhr = 0
                            objlogin._TotalNighthr = 0
                            objlogin._IsUnPaidLeave = False
                            objlogin._IsPaidLeave = False
                            objlogin._IsWeekend = True
                            objlogin._IsAbsentProcess = True
                            objlogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                            objlogin._Ot2 = 0
                            objlogin._Ot3 = 0
                            objlogin._Ot4 = 0
                            objlogin._Early_Coming = 0
                            objlogin._Late_Coming = 0
                            objlogin._Early_Going = 0
                            objlogin._Late_Going = 0

                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = True

                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsWeekend = False
                                    If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                        objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                    End If
                                    'Pinkal (11-AUG-2017) -- End
                            End If

                            Dim objholiday As New clsemployee_holiday
                            Dim dtHoliday As DataTable = objholiday.GetEmployeeHoliday(objlogin._Employeeunkid, objlogin._Logindate)

                            If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then

                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsDayOffShift = False
                                    'objlogin._IsWeekend = False
                                objlogin._IsHoliday = True

                                    If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                        objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                    End If

                                'Pinkal (11-AUG-2017) -- End

                            End If

                            Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                            If mintStatus = 1 Then
                                objlogin._IsUnPaidLeave = False
                                objlogin._IsPaidLeave = True
                                objlogin._IsWeekend = False
                                'Pinkal (25-AUG-2017) -- Start
                                'Enhancement - Working on B5 plus TnA Enhancement.
                                objlogin._DayType = 1
                                'Pinkal (25-AUG-2017) -- End
                            ElseIf mintStatus = 2 Then
                                objlogin._IsUnPaidLeave = True
                                objlogin._IsPaidLeave = False
                                objlogin._IsWeekend = False
                            ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                objlogin._IsUnPaidLeave = True
                                objlogin._IsPaidLeave = False
                                objlogin._IsWeekend = False
                            ElseIf mintStatus = 5 Then
                                objlogin._IsUnPaidLeave = False
                                objlogin._IsPaidLeave = False

                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsWeekend = False
                                objlogin._IsHoliday = True

                                    If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                        objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                            End If

                                    'Pinkal (11-AUG-2017) -- End

                                End If

                            'Pinkal (25-AUG-2017) -- Start
                            'Enhancement - Working on B5 plus TnA Enhancement.
                            If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                objlogin._IsHoliday = False
                                objlogin._IsDayOffShift = False
                                objlogin._IsWeekend = False
                            End If
                            'Pinkal (25-AUG-2017) -- End




                            With objlogin
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With


                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                    , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                          , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                          , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                          , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                          , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                        , strUserAccessLevelFilterString, mstrFilter, 0, True)



                                'Pinkal (11-AUG-2017) -- End


                            'Pinkal (14-Mar-2017) -- End



                        End If


                        'START FOR PAID LEAVE 
                    ElseIf LoginStatus = 1 Then

                        'Pinkal (12-Jan-2022)-- Start
                        'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                        'If IsEmployeeLoginExist(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate) Then
                        Dim xLoginunkid As Integer = 0
                        If IsEmployeeLoginExist(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, xLoginunkid) Then
                            'Pinkal (12-Jan-2022) -- End
                            Dim dsLoginSummary As DataSet = IsEmployeeLoginSummaryExist(objlogin._Employeeunkid, objlogin._Logindate)
                            If dsLoginSummary IsNot Nothing AndAlso dsLoginSummary.Tables(0).Rows.Count <= 0 Then
                                'Pinkal (22-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                'objlogin._DayType = 1
                                objlogin._DayType = 0
                                'Pinkal (22-AUG-2017) -- End
                                objlogin._InOutType = 1
                                objlogin._TotalOvertimehr = 0
                                objlogin._Shorthr = 0
                                objlogin._Totalhr = 0
                                objlogin._TotalNighthr = 0
                                objlogin._IsUnPaidLeave = False
                                objlogin._IsPaidLeave = True
                                objlogin._IsWeekend = False
                                objlogin._IsAbsentProcess = True
                                objlogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                                objlogin._Ot2 = 0
                                objlogin._Ot3 = 0
                                objlogin._Ot4 = 0
                                objlogin._Early_Coming = 0
                                objlogin._Late_Coming = 0
                                objlogin._Early_Going = 0
                                objlogin._Late_Going = 0
                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objlogin.InsertLoginSummary(objDataOperation, 0, True)



                                With objlogin
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With



                                objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                    , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                         , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                         , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                         , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                         , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                 , strUserAccessLevelFilterString, mstrFilter, 0, True)

                                'Pinkal (11-AUG-2017) -- End


                                'Pinkal (14-Mar-2017) -- End




                                'S.SANDEEP [04 JUN 2015] -- END
                            Else
                                objlogin._IsUnPaidLeave = False
                                objlogin._IsPaidLeave = True
                                objlogin._IsWeekend = False
                                objlogin._IsDayOffShift = False

                                'Pinkal (12-Jan-2022)-- Start
                                'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                                If mstrGroupName.ToUpper.Trim() = "COLAS LTD" AndAlso xLoginunkid > 0 Then

                                    objlogin._Loginunkid = xLoginunkid
                                    'Pinkal (02-Feb-2022)-- Start
                                    'Problem Solving for COLAS.
                                    'objlogin._TotalOvertimehr = 0
                                    'objlogin._Totalhr = 0
                                    'Pinkal (02-Feb-2022)-- End
                                    objlogin._Shorthr = 0
                                    objlogin._TotalNighthr = 0
                                    objlogin._Ot2 = 0
                                    objlogin._Ot3 = 0
                                    objlogin._Ot4 = 0
                                    objlogin._Early_Coming = 0
                                    objlogin._Late_Coming = 0
                                    objlogin._Early_Going = 0
                                    objlogin._Late_Going = 0

                                    objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                 , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                 , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                      , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                      , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                      , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                      , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                              , strUserAccessLevelFilterString, mstrFilter, 0, True)

                                    xLoginunkid = 0
                                    objlogin._Loginunkid = xLoginunkid
                                End If
                                'Pinkal (12-Jan-2022) -- End

                                GoTo ForPresentEmployee
                            End If

                        Else


                            objlogin._DayType = 1
                            objlogin._InOutType = 1
                            objlogin._TotalOvertimehr = 0
                            objlogin._Shorthr = 0
                            objlogin._IsHoliday = False
                            objlogin._IsWeekend = False
                            objlogin._IsUnPaidLeave = False
                            objlogin._IsPaidLeave = True
                            objlogin._IsAbsentProcess = True
                            objlogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                            objlogin._Ot2 = 0
                            objlogin._Ot3 = 0
                            objlogin._Ot4 = 0
                            objlogin._Early_Coming = 0
                            objlogin._Late_Coming = 0
                            objlogin._Early_Going = 0
                            objlogin._Late_Going = 0
                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = False
                            End If



                            With objlogin
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With


                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                    , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                     , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                     , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                     , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                     , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                          , strUserAccessLevelFilterString, mstrFilter, 0, True)


                            'Pinkal (11-AUG-2017) -- End

                            'Pinkal (14-Mar-2017) -- End



                            'S.SANDEEP [04 JUN 2015] -- END

                        End If
                        'END FOR PAID LEAVE 


                        'START FOR UNPAID LEAVE 

                        'Pinkal (24-Feb-2020) -- 'Bug [0004578] - voltamp : Unpaid Leave on Weekend considered as Weekend in Absent process.[ElseIf CBool(drShiftDay(0)("isweekend")) = False And LoginStatus = 2 Then]
                        'ElseIf CBool(drShiftDay(0)("isweekend")) = False And LoginStatus = 2 Then
                    ElseIf LoginStatus = 2 Then
                        If IsEmployeeLoginExist(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate) Then
                            Dim dsLoginSummary As DataSet = IsEmployeeLoginSummaryExist(objlogin._Employeeunkid, objlogin._Logindate)
                            If dsLoginSummary IsNot Nothing AndAlso dsLoginSummary.Tables(0).Rows.Count <= 0 Then
                                'Pinkal (22-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                'objlogin._DayType = 1
                                objlogin._DayType = 0
                                'Pinkal (22-AUG-2017) -- End
                                objlogin._InOutType = 1
                                objlogin._TotalOvertimehr = 0
                                objlogin._Shorthr = 0
                                objlogin._Totalhr = 0
                                objlogin._TotalNighthr = 0
                                objlogin._IsUnPaidLeave = True
                                objlogin._IsDayOffShift = False
                                objlogin._IsPaidLeave = False
                                objlogin._IsWeekend = False
                                objlogin._IsAbsentProcess = True
                                objlogin._IsHoliday = False
                                objlogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                                objlogin._Ot2 = 0
                                objlogin._Ot3 = 0
                                objlogin._Ot4 = 0
                                objlogin._Early_Coming = 0
                                objlogin._Late_Coming = 0
                                objlogin._Early_Going = 0
                                objlogin._Late_Going = 0

                                If objlogin._LeaveDayFraction <= 0 Then
                                    objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)
                                End If





                                With objlogin
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With


                                objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                    , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                           , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                           , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                           , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                           , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                  , strUserAccessLevelFilterString, mstrFilter, 0, True)




                            Else
                                objlogin._IsDayOffShift = False
                                objlogin._IsUnPaidLeave = True
                                objlogin._IsPaidLeave = False
                                objlogin._IsWeekend = False
                                objlogin._IsHoliday = False

                                If objlogin._LeaveDayFraction <= 0 Then
                                    objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)
                                End If

                                GoTo ForPresentEmployee
                            End If
                        Else
                            objlogin._Holdunkid = 0
                            objlogin._DayType = 0
                            objlogin._IsWeekend = False
                            objlogin._IsHoliday = False
                            objlogin._IsDayOffShift = False
                            objlogin._IsPaidLeave = False
                            objlogin._IsUnPaidLeave = True
                            objlogin._IsAbsentProcess = True
                            objlogin._TotalOvertimehr = 0
                            objlogin._Shorthr = 0
                            objlogin._Ot2 = 0
                            objlogin._Ot3 = 0
                            objlogin._Ot4 = 0
                            objlogin._Early_Coming = 0
                            objlogin._Late_Coming = 0
                            objlogin._Early_Going = 0
                            objlogin._Late_Going = 0

                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = False
                            End If

                            If objlogin._LeaveDayFraction <= 0 Then
                                objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)
                            End If



                            With objlogin
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With


                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                    , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                     , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                     , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                     , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                     , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                   , strUserAccessLevelFilterString, mstrFilter, 0, True)




                        End If
                        'END FOR UNPAID LEAVE 


                        'START FOR UNPLANNED LEAVE 
                    ElseIf CBool(drShiftDay(0)("isweekend")) = False And LoginStatus = 3 Then
                        Dim dsEmpUnplannedLeave As DataSet = IsEmployeeLoginSummaryExist(objlogin._Employeeunkid, objlogin._Logindate)
                        If dsEmpUnplannedLeave IsNot Nothing AndAlso dsEmpUnplannedLeave.Tables(0).Rows.Count > 0 Then
                            objlogin._IsUnPaidLeave = True
                            objlogin._IsHoliday = False
                            objlogin._IsWeekend = False
                            objlogin._IsDayOffShift = False
                            objlogin._IsPaidLeave = False

                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = True
                                objlogin._IsUnPaidLeave = False
                            End If

                            If objlogin._LeaveDayFraction <= 0 Then
                                objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)
                            End If

                            GoTo ForPresentEmployee
                        Else

                            objlogin._Holdunkid = 0
                            objlogin._DayType = 0
                            objlogin._IsHoliday = False
                            objlogin._IsWeekend = False
                            objlogin._IsDayOffShift = False
                            objlogin._IsPaidLeave = False
                            objlogin._IsUnPaidLeave = True
                            objlogin._IsAbsentProcess = True
                            objlogin._TotalOvertimehr = 0
                            objlogin._Shorthr = 0
                            objlogin._Ot2 = 0
                            objlogin._Ot3 = 0
                            objlogin._Ot4 = 0
                            objlogin._Early_Coming = 0
                            objlogin._Late_Coming = 0
                            objlogin._Early_Going = 0
                            objlogin._Late_Going = 0

                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = True
                                objlogin._IsUnPaidLeave = False
                            End If

                            If objlogin._LeaveDayFraction <= 0 Then
                                objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)
                            End If


                            With objlogin
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With



                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                              , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                     , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                     , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                     , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                     , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                     , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                     , strUserAccessLevelFilterString, mstrFilter, 0, True)





                            'END FOR UNPLANNED LEAVE 

                        End If

                        'START FOR PAID LEAVE BUT LEAVE BALANCE IS IN MINUS...
                    ElseIf LoginStatus = 4 Then
                        If IsEmployeeLoginExist(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate) Then

                            objlogin._IsUnPaidLeave = True
                            objlogin._IsPaidLeave = True

                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = False
                            End If
                            objlogin._IsWeekend = False
                            objlogin._IsHoliday = False
                            GoTo ForPresentEmployee
                        Else
                            objlogin._checkintime = CDate(objlogin._Logindate.Date & " " & CDate(drShiftDay(0)("starttime")).ToLongTimeString)
                            objlogin._Holdunkid = objlogin.CheckforHoldEmployee(CInt(dtEmployee.Rows(i)("employeeunkid")), objlogin._Logindate.Date, objDataOperation)
                            objlogin._Checkouttime = CDate(objlogin._Logindate.Date & " " & CDate(drShiftDay(0)("endtime")).ToLongTimeString)
                            objlogin._Workhour = CInt(drShiftDay(0)("workinghrsinsec"))
                            objlogin._Original_InTime = objlogin._checkintime
                            objlogin._Original_OutTime = objlogin._Checkouttime

                            objlogin._DayType = 0
                            objlogin._InOutType = 1
                            objlogin._TotalOvertimehr = 0
                            objlogin._Shorthr = 0
                            objlogin._IsHoliday = False
                            objlogin._IsWeekend = False
                            objlogin._IsDayOffShift = False
                            objlogin._IsUnPaidLeave = True
                            objlogin._IsPaidLeave = True
                            objlogin._IsAbsentProcess = True
                            objlogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                            objlogin._Ot2 = 0
                            objlogin._Ot3 = 0
                            objlogin._Ot4 = 0
                            objlogin._Early_Coming = 0
                            objlogin._Late_Coming = 0
                            objlogin._Early_Going = 0
                            objlogin._Late_Going = 0
                            If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                                objlogin._IsDayOffShift = False
                            End If
                            With objlogin
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With
                            objlogin.Insert(objDataOperation, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut)


                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                    , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                              , mdtEnddate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                                    , blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                    , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                    , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                    , strUserAccessLevelFilterString, mstrFilter)



                        End If
                        'END FOR PAID LEAVE BUT LEAVE BALANCE IS IN MINUS...

                        'START FOR HOLIDAY

                    ElseIf LoginStatus = 5 Then   'FOR HOLIDAY NO ENTRY IF EMPLOYEE IS NOT PRESENT ON HOLIDAY

                        objlogin._DayType = 0
                        objlogin._IsDayOffShift = False
                        objlogin._IsUnPaidLeave = False
                        objlogin._IsPaidLeave = False
                        objlogin._IsWeekend = False
                        objlogin._IsHoliday = True


                        'Pinkal (11-AUG-2017) -- Start
                        'Enhancement - Working On B5 Plus Company TnA Enhancements.
                        If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then
                            objlogin._IsDayOffShift = True
                        End If
                        'Pinkal (11-AUG-2017) -- End

                        Dim dsEmpHolidayList As DataSet = IsEmployeeLoginSummaryExist(objlogin._Employeeunkid, objlogin._Logindate)
                        If dsEmpHolidayList IsNot Nothing AndAlso dsEmpHolidayList.Tables(0).Rows.Count > 0 Then
                            'Pinkal (11-AUG-2017) -- Start
                            'Enhancement - Working On B5 Plus Company TnA Enhancements.
                            If objlogin._IsDayOffShift AndAlso objlogin._IsHoliday Then
                                objlogin._IsHoliday = blnIsHolidayConsiderOnDayoff
                                objlogin._IsDayOffShift = Not blnIsHolidayConsiderOnDayoff
                            End If
                            'Pinkal (11-AUG-2017) -- End

                            GoTo ForPresentEmployee
                        Else

                            'Pinkal (14-Mar-2017) -- Start
                            'Enhancement - Problem Solved for Voltamp for Weekend in Absent Process.

                            'objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                            '                                         , xYearUnkid, xCompanyunkid, mdtStartdate _
                            '                                         , mdtEnddate, xUserModeSetting, xOnlyApproved _
                            '                                         , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                            '                                         , strUserAccessLevelFilterString, mstrFilter)


                            With objlogin
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With

                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                     , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                            , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                      , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                      , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                      , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                      , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                            , strUserAccessLevelFilterString, mstrFilter, 0, True)


                            'Pinkal (11-AUG-2017) -- End

                            'Pinkal (14-Mar-2017) -- End



                            'S.SANDEEP [04 JUN 2015] -- END
                        End If

                        'END FOR HOLIDAY

                        'START FOR PRESENT EMPLOYEE, UPDATE ONLY ABSENT PROCESS TRUE
                    ElseIf LoginStatus = 0 Then

                        Dim dsLeaveInfo As DataSet = GetEmployeeLeaveInfoForAbsent(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate)

                        If ConfigParameter._Object._CountAttendanceNotLVIfLVapplied Then   'CONSIDER ATTENDANCE NOT LEAVE IF LEAVE APPLIED AND EMPLOYEE PRESENT ON THIS DAY

                            If objlogin._LeaveDayFraction >= 1 Then
                                If dsLeaveInfo IsNot Nothing AndAlso dsLeaveInfo.Tables(0).Rows.Count > 0 AndAlso CDate(dsLeaveInfo.Tables(0).Rows(0)("leavedate")) = objlogin._Logindate _
                                    AndAlso objlogin.GetLoginSourceType(objlogin._Employeeunkid, objlogin._Logindate, objDataOperation) <> -1 Then

                                    'Pinkal (11-Feb-2022)-- Start
                                    'Problem Solving for COLAS.
                                    If mstrGroupName.ToUpper.Trim() <> "COLAS LTD" Then
                                    UpdateAccrueBalance(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, dsLeaveInfo)
                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                    'Pinkal (11-Feb-2022)-- End

                                End If

                                objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)

                            ElseIf objlogin._LeaveDayFraction < 1 AndAlso objlogin._LeaveDayFraction > 0 Then

                                objShiftTran.GetShiftTran(objShift._Shiftunkid)   'FOR WEEKEND
                                Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(objlogin._Logindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                                If drWeekend.Length > 0 Then

                                    objlogin._IsWeekend = True

                                    Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                                    If mintStatus = 1 Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = True
                                        objlogin._DayType = 1
                                    ElseIf mintStatus = 2 Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                    ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                    ElseIf mintStatus = 5 Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = False
                                        objlogin._IsHoliday = True
                                        If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                            objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                            objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                        End If
                                    End If
                                End If

                                If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then   'FOR DAYOFF
                                    objlogin._IsDayOffShift = True
                                    If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                        objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                    End If
                                End If

                                Dim objholiday As New clsemployee_holiday
                                Dim dtHoliday As DataTable = objholiday.GetEmployeeHoliday(objlogin._Employeeunkid, objlogin._Logindate)  'FOR HOLIDAY
                                If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                                    objlogin._IsHoliday = True
                                    If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                        objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                    End If
                                End If

                                If dsLeaveInfo IsNot Nothing AndAlso dsLeaveInfo.Tables(0).Rows.Count > 0 AndAlso CDate(dsLeaveInfo.Tables(0).Rows(0)("leavedate")) = objlogin._Logindate _
                                   AndAlso objlogin.GetLoginSourceType(objlogin._Employeeunkid, objlogin._Logindate, objDataOperation) <> -1 Then

                                    If CBool(dsLeaveInfo.Tables(0).Rows(0)("ispaid")) Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = True
                                    ElseIf CBool(dsLeaveInfo.Tables(0).Rows(0)("ispaid")) = False Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                    End If
                                End If

                                If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                    objlogin._IsHoliday = False
                                    objlogin._IsDayOffShift = False
                                    objlogin._IsWeekend = False
                                End If

                                'Pinkal (02-Feb-2022)-- Start
                                'Problem Solving for COLAS.
                                If mstrGroupName.ToUpper.Trim() = "COLAS LTD" Then
                                    Dim xLoginunkid As Integer = 0
                                    If IsEmployeeLoginExist(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, xLoginunkid) Then
                                        If xLoginunkid > 0 Then
                                            objlogin._Loginunkid = xLoginunkid
                                            objlogin._Shorthr = 0
                                            objlogin._TotalNighthr = 0
                                            objlogin._Ot2 = 0
                                            objlogin._Ot3 = 0
                                            objlogin._Ot4 = 0
                                            objlogin._Early_Coming = 0
                                            objlogin._Late_Coming = 0
                                            objlogin._Early_Going = 0
                                            objlogin._Late_Going = 0

                                            objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                         , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                         , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                              , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                              , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                              , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                              , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                      , strUserAccessLevelFilterString, mstrFilter, 0, True)

                                            xLoginunkid = 0
                                            objlogin._Loginunkid = xLoginunkid
                                        End If
                                    End If
                                    'Pinkal (02-Feb-2022)-- End
                                End If

                            ElseIf objlogin._LeaveDayFraction <= 0 Then
                                If dsLeaveInfo IsNot Nothing AndAlso dsLeaveInfo.Tables(0).Rows.Count > 0 AndAlso CDate(dsLeaveInfo.Tables(0).Rows(0)("leavedate")) = objlogin._Logindate _
                                                                AndAlso objlogin.GetLoginSourceType(objlogin._Employeeunkid, objlogin._Logindate, objDataOperation) <> -1 Then
                                    objlogin._LeaveDayFraction = SetEmployeeDayFraction(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate, objShift._Shiftunkid)

                                Else

                                    objShiftTran.GetShiftTran(objShift._Shiftunkid)   'FOR WEEKEND
                                    Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(objlogin._Logindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                                    If drWeekend.Length > 0 Then

                                        objlogin._IsWeekend = True

                                        Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                                        If mintStatus = 1 Then
                                            objlogin._IsUnPaidLeave = False
                                            objlogin._IsPaidLeave = True
                                            objlogin._DayType = 1
                                        ElseIf mintStatus = 2 Then
                                            objlogin._IsUnPaidLeave = True
                                            objlogin._IsPaidLeave = False
                                        ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                            objlogin._IsUnPaidLeave = True
                                            objlogin._IsPaidLeave = False
                                            objlogin._IsWeekend = False
                                        ElseIf mintStatus = 5 Then
                                            objlogin._IsUnPaidLeave = False
                                            objlogin._IsPaidLeave = False

                                            objlogin._IsHoliday = True
                                            If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                                objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                                objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                            End If
                                        End If
                                    End If   ' If drWeekend.Length > 0 Then

                                    If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then   'FOR DAYOFF
                                        objlogin._IsDayOffShift = True
                                        If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                            objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                            objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                        End If
                                End If

                                    Dim objholiday As New clsemployee_holiday
                                    Dim dtHoliday As DataTable = objholiday.GetEmployeeHoliday(objlogin._Employeeunkid, objlogin._Logindate)  'FOR HOLIDAY
                                    If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                                        objlogin._IsHoliday = True
                                        If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                            objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                            objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                        End If
                                    End If

                                    If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                        objlogin._IsHoliday = False
                                        objlogin._IsDayOffShift = False
                                        objlogin._IsWeekend = False
                                End If

                                End If
                            End If


                        ElseIf ConfigParameter._Object._CountAttendanceNotLVIfLVapplied = False Then  'CONSIDER LEAVE  NOT ATTENDANCE IF LEAVE APPLIED AND EMPLOYEE PRESENT ON THIS DAY

                            If dsLeaveInfo IsNot Nothing AndAlso dsLeaveInfo.Tables(0).Rows.Count > 0 AndAlso CDate(dsLeaveInfo.Tables(0).Rows(0)("leavedate")) = objlogin._Logindate _
                                  AndAlso objlogin.GetLoginSourceType(objlogin._Employeeunkid, objlogin._Logindate, objDataOperation) <> -1 Then

                                If VoidAttendance(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, objlogin._Shiftunkid, blnDonotAttendanceinSeconds) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                objShiftTran.GetShiftTran(objShift._Shiftunkid)   'FOR WEEKEND
                                Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(objlogin._Logindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                                If drWeekend.Length > 0 Then

                                    objlogin._IsWeekend = True

                                    Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                                    If mintStatus = 1 Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = True
                                        objlogin._IsWeekend = False
                                        'Pinkal (25-AUG-2017) -- Start
                                        'Enhancement - Working on B5 plus TnA Enhancement.
                                        objlogin._DayType = 1
                                        'Pinkal (25-AUG-2017) -- End
                                    ElseIf mintStatus = 2 Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                        objlogin._IsWeekend = False
                                    ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                        objlogin._IsWeekend = False
                                    ElseIf mintStatus = 5 Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = False

                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                        'objlogin._IsWeekend = False
                                        objlogin._IsHoliday = True
                                        If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                            objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                            objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                        End If
                                        'Pinkal (11-AUG-2017) -- End

                                    End If
                                End If

                                If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then   'FOR DAYOFF
                                    objlogin._IsDayOffShift = True
                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsWeekend = False
                                    'objlogin._IsHoliday = False
                                    If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                        objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                    End If
                                    'Pinkal (11-AUG-2017) -- End
                                End If

                                Dim objholiday As New clsemployee_holiday
                                Dim dtHoliday As DataTable = objholiday.GetEmployeeHoliday(objlogin._Employeeunkid, objlogin._Logindate)  'FOR HOLIDAY
                                If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then

                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsDayOffShift = False
                                    'objlogin._IsWeekend = False
                                    objlogin._IsHoliday = True
                                    If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                        objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                    End If
                                    'Pinkal (11-AUG-2017) -- End

                                End If

                                If dsLeaveInfo IsNot Nothing AndAlso dsLeaveInfo.Tables(0).Rows.Count > 0 AndAlso CDate(dsLeaveInfo.Tables(0).Rows(0)("leavedate")) = objlogin._Logindate _
                                   AndAlso objlogin.GetLoginSourceType(objlogin._Employeeunkid, objlogin._Logindate, objDataOperation) <> -1 Then

                                    If CBool(dsLeaveInfo.Tables(0).Rows(0)("ispaid")) Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = True
                                    ElseIf CBool(dsLeaveInfo.Tables(0).Rows(0)("ispaid")) = False Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                    End If
                                End If
                                objlogin._IsAbsentProcess = True

                                'Pinkal (25-AUG-2017) -- Start
                                'Enhancement - Working on B5 plus TnA Enhancement.
                                If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                    objlogin._IsHoliday = False
                                    objlogin._IsDayOffShift = False
                                    objlogin._IsWeekend = False
                                End If
                                'Pinkal (25-AUG-2017) -- End


                                'Pinkal (14-Mar-2017) -- Start
                                'Enhancement - Problem Solved for Voltamp for Weekend in Absent Process.

                                'objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                '                                      , xYearUnkid, xCompanyunkid, mdtStartdate _
                                '                                      , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                '                                      , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA, blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut, blnApplyUserAccessFilter, intEmpId _
                                '                                      , strUserAccessLevelFilterString, mstrFilter)


                                With objlogin
                                    ._FormName = mstrFormName
                                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                                    ._ClientIP = mstrClientIP
                                    ._HostName = mstrHostName
                                    ._FromWeb = mblnIsWeb
                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                    ._AuditDate = mdtAuditDate
                                End With

                                objlogin.InsertLoginSummary(objDataOperation, xDataBaseName, xUserUnkid _
                                                                      , xYearUnkid, xCompanyunkid, mdtStartdate _
                                                                            , mdtEnddate, xUserModeSetting, xOnlyApproved _
                                                                         , xIncludeIn_ActiveEmployee, blnPolicyManagementTNA _
                                                                         , blnDonotAttendanceinSeconds, blnFirstCheckInLastCheckOut _
                                                                         , blnIsHolidayConsiderOnWeekend, blnIsDayOffConsiderOnWeekend _
                                                                         , blnIsHolidayConsiderOnDayoff, blnApplyUserAccessFilter, intEmpId _
                                                                            , strUserAccessLevelFilterString, mstrFilter, 0, True)


                                'Pinkal (11-AUG-2017) -- End

                                'Pinkal (14-Mar-2017) -- End


                                Continue For


                                'Pinkal (09-Nov-2015) -- Start
                                'Enhancement - WORKING ON VOLTAMP CHANGES FOR HOLIDAY WHEN EMPLOYEE PRESENT ON HOLIDAY OR WEEKEND.

                            Else

                                objShiftTran.GetShiftTran(objShift._Shiftunkid)   'FOR WEEKEND
                                Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(objlogin._Logindate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                                If drWeekend.Length > 0 Then

                                    objlogin._IsWeekend = True

                                    Dim mintStatus As Integer = GetEmployeeWeekendLeaveData(objDataOperation, objlogin._Employeeunkid, objlogin._Logindate.Date, CDate(dsPeriod.Tables("List").Rows(0)("PeriodDate")), CDate(dsPeriod.Tables("List").Rows(dsPeriod.Tables("List").Rows.Count - 1)("PeriodDate")), objShift._Shiftunkid)

                                    If mintStatus = 1 Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = True
                                        objlogin._IsWeekend = False
                                        'Pinkal (25-AUG-2017) -- Start
                                        'Enhancement - Working on B5 plus TnA Enhancement.
                                        objlogin._DayType = 1
                                        'Pinkal (25-AUG-2017) -- End
                                    ElseIf mintStatus = 2 Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                        objlogin._IsWeekend = False
                                    ElseIf mintStatus = 3 OrElse mintStatus = 4 Then
                                        objlogin._IsUnPaidLeave = True
                                        objlogin._IsPaidLeave = False
                                        objlogin._IsWeekend = False
                                    ElseIf mintStatus = 5 Then
                                        objlogin._IsUnPaidLeave = False
                                        objlogin._IsPaidLeave = False
                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                        'objlogin._IsWeekend = False
                                        objlogin._IsHoliday = True
                                        If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                            objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                            objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                        End If
                                        'Pinkal (11-AUG-2017) -- End
                                    End If

                                End If

                                If objEmpDayOff.isExist(objlogin._Logindate.Date, CInt(dtEmployee.Rows(i)("employeeunkid")), -1, objDataOperation) Then   'FOR DAYOFF
                                    objlogin._IsDayOffShift = True
                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsWeekend = False
                                    'objlogin._IsHoliday = False
                                    If objlogin._IsDayOffShift AndAlso objlogin._IsWeekend Then
                                        objlogin._IsDayOffShift = blnIsDayOffConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsDayOffConsiderOnWeekend
                                    End If
                                    'Pinkal (11-AUG-2017) -- End
                                End If

                                Dim objholiday As New clsemployee_holiday
                                Dim dtHoliday As DataTable = objholiday.GetEmployeeHoliday(objlogin._Employeeunkid, objlogin._Logindate)  'FOR HOLIDAY
                                If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                                    objlogin._IsHoliday = True
                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                    'objlogin._IsDayOffShift = False
                                    'objlogin._IsWeekend = False
                                    If objlogin._IsWeekend AndAlso objlogin._IsHoliday Then
                                        objlogin._IsHoliday = blnIsHolidayConsiderOnWeekend
                                        objlogin._IsWeekend = Not blnIsHolidayConsiderOnWeekend
                                    End If
                                    'Pinkal (11-AUG-2017) -- End
                                End If
                                dtHoliday.Clear()
                                dtHoliday = Nothing
                                objholiday = Nothing

                                'Pinkal (09-Nov-2015) -- End


                                'Pinkal (25-AUG-2017) -- Start
                                'Enhancement - Working on B5 plus TnA Enhancement.
                                If objlogin._IsPaidLeave OrElse objlogin._IsUnPaidLeave Then
                                    objlogin._IsHoliday = False
                                    objlogin._IsDayOffShift = False
                                    objlogin._IsWeekend = False
                                End If
                                'Pinkal (25-AUG-2017) -- End


                            End If

                        End If

                        'Pinkal (07-OCT-2014) -- End

ForPresentEmployee:
                        objlogin._IsAbsentProcess = True
                        With objlogin
                            ._FormName = mstrFormName
                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objlogin.UpdateLoginSummaryForAbsent(objDataOperation)
                    End If
                    'END FOR PRESENT EMPLOYEE, UPDATE ONLY ABSENT PROCESS TRUE

                Next ' ----------------------------------------------------------------------------------------------'END FOR PERIOD DATES LOOP


                If bw IsNot Nothing Then
                    intCount = intCount + 1
                    bw.ReportProgress(intCount)
                End If

            Next     ' ----------------------------------------------------------------------------------------------'END FOR EMPLOYEE LOOP


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If AtAbsentProcess(objDataOperation, intPeriodunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertEmployeeAbsent; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return False
    End Function

    'Pinkal (12-Oct-2011) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GeneratePeriodDate(ByVal objDataOperation As clsDataOperation) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim PeriodDiff As Integer = DateDiff(DateInterval.Day, _StartDate.Date, _EndDate.Date.AddDays(1))
            strQ = " DECLARE @myTable TABLE " & _
                   " ( " & _
                   "    PeriodDate DateTime " & _
                   ",   PeriodDay nvarchar(20) " & _
                   " ) " & _
                    " declare @StartDate datetime " & _
                    " declare @Days int " & _
                    " declare @CurrentDay int " & _
                    " set @StartDate = @start_date" & _
                    " set @Days = @PeriodDiff " & _
                    " set @CurrentDay = 0 " & _
                    " while @CurrentDay < @Days " & _
                    "   begin " & _
                    "        insert @myTable (PeriodDate,PeriodDay) values (dateadd(dd, @CurrentDay, @StartDate),DATEname(dw,dateadd(dd, @CurrentDay, @StartDate))) " & _
                    "        set @CurrentDay = @CurrentDay + 1 " & _
                    "   End " & _
                    " select * from @myTable "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@PeriodDiff", SqlDbType.Int, eZeeDataType.INT_SIZE, PeriodDiff)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLoginData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeLeaveData(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer _
                                                             , ByVal mdtLeaveDate As DateTime, ByVal dtStartdate As DateTime, ByVal dtEnddate As DateTime, ByVal intShiftID As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            dsList = IsEmployeeLoginSummaryExist(intEmployeeunkid, mdtLeaveDate)

            If dsList.Tables("List").Rows.Count > 0 Then
                If CInt(dsList.Tables("List").Rows(0)("loginsummaryunkid")) > 0 AndAlso CInt(dsList.Tables("List").Rows(0)("total_hrs")) > 0 Then
                    Return 0 ' FOR PRESENT
                End If
            End If


            dsList = GetEmployeeLeaveInfoForAbsent(objDataOperation, intEmployeeunkid, mdtLeaveDate)

            If dsList.Tables("List").Rows.Count > 0 Then
                If CBool(dsList.Tables("List").Rows(0)("ispaid")) = True Then

                    If intLeaveCount = 0 Then
                        intLeaveCount = GetEmployeeLeaveCount(intEmployeeunkid, dtStartdate, dtEnddate)
                        intLeavebalance = CInt(dsList.Tables("List").Rows(0)("Issue_amount")) - intLeaveCount
                    End If
                    intLeavebalance = intLeavebalance + 1
                    If Math.Round(dsList.Tables("List").Rows(0)("Accrue_amount")) < intLeavebalance Then

                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Oman Changes  [As Mr.Prabhakar had problem when Issue Leave With Paid option is tick]

                        If CInt(dsList.Tables("List").Rows(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then
                            Return 4 'FOR NEGATIVE LEAVE BALANCE PAID ISSUE LEAVE
                        Else
                            Return 1 'FOR PAID ISSUE LEAVE
                        End If

                        'Pinkal (06-May-2014) -- End
                    Else
                        Return 1 'FOR PAID ISSUE LEAVE
                    End If


                ElseIf CBool(dsList.Tables("List").Rows(0)("ispaid")) = False Then
                    Return 2 'FOR UNPAID ISSUE LEAVE
                End If

            ElseIf dsList.Tables("List").Rows.Count = 0 Then


                'Pinkal (13-Jul-2017) -- Start
                'Enhancement - Bug Solved WHEN Employee Present in Weekend but in Login summary table it was not update isweekend = 1 .
                'FOR WEEKEND
                Dim objShifttran As New clsshift_tran
                objShifttran.GetShiftTran(intShiftID)
                Dim drWeekend() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtLeaveDate.DayOfWeek.ToString()) & " AND isweekend = 1 ")
                If drWeekend.Length > 0 Then
                    Return 7 ' Weekend
                End If
                'Pinkal (13-Jul-2017) -- End


                ' START FOR CHECK WHETHER IT IS HOLIDAY OR NOT
                strQ = " SELECT lvemployee_holiday.holidayunkid,holidayname,employeeunkid,holidaydate FROM lvemployee_holiday " & _
                           " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                           " WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),holidaydate,112) = @leavedate"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If dsList.Tables("List").Rows.Count > 0 Then
                    If CInt(dsList.Tables("List").Rows(0)("holidayunkid")) > 0 Then
                        Return 5 ' FOR HOLIDAY
                    End If
                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Else
                    strQ = "SELECT 1 FROM lvleaveform WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND returndate IS NULL AND CONVERT(CHAR(8),startdate,112) <= @leavedate ORDER BY startdate DESC "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                    objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
                    dsList = objDataOperation.ExecQuery(strQ, "List")
                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If
                    If dsList.Tables("List").Rows.Count > 0 Then
                        Return 1 'FOR PAID ISSUE LEAVE
                    End If
                    'S.SANDEEP [ 26 SEPT 2013 ] -- END


                    'Pinkal (01-Feb-2014) -- Start
                    'Enhancement : OMAN Changes [Paid Leave Without Accrue Amount It is consider as paid leave for TnA] 

                    strQ = "SELECT * FROM lvleaveform WHERE isvoid = 0 AND employeeunkid = @employeeunkid  AND returndate IS NOT NULL AND  @leavedate between CONVERT(CHAR(8),startdate,112) AND  CONVERT(CHAR(8),returndate,112)  AND statusunkid = 7 ORDER BY startdate DESC "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                    objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
                    dsList = objDataOperation.ExecQuery(strQ, "List")
                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If
                    If dsList.Tables("List").Rows.Count > 0 Then

                        Dim objLvType As New clsleavetype_master
                        objLvType._Leavetypeunkid = CInt(dsList.Tables(0).Rows(0)("leavetypeunkid"))

                        If objLvType._IsAccrueAmount = False AndAlso objLvType._IsPaid AndAlso objLvType._IsShortLeave = False Then
                            Return 1 'FOR PAID ISSUE LEAVE
                        End If

                    End If

                    'Pinkal (01-Feb-2014) -- End


                End If

                ' END FOR CHECK WHETHER IT IS HOLIDAY OR NOT

                'Pinkal (24-Jan-2011) -- End

                dsList = IsEmployeeLoginSummaryExist(intEmployeeunkid, mdtLeaveDate)
                If dsList.Tables("List").Rows.Count > 0 Then
                    If CInt(dsList.Tables("List").Rows(0)("loginsummaryunkid")) > 0 AndAlso CInt(dsList.Tables("List").Rows(0)("total_hrs")) > 0 Then
                        Return 0 ' FOR PRESENT
                    Else
                        Return 3 'FOR UNPLANNED LEAVE
                    End If
                Else
                    Return 3 'FOR UNPLANNED LEAVE
                End If

            End If



        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsExist(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " Select isnull(isabsentprocess,0) isabsentprocess from tnalogin_summary " & _
                   " WHERE convert(char(8),login_date,112) >= @startdate AND convert(char(8),login_date,112) <= @enddate " & _
                   " group by isnull(isabsentprocess,0) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEnddate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables("List").Rows.Count > 0 Then Return CBool(dsList.Tables("List").Rows(0).Item(0))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Pinkal (12-Jan-2022)-- Start
    'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
    'Public Function IsEmployeeLoginExist(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, ByVal dtLogindate As DateTime) As Boolean
    Public Function IsEmployeeLoginExist(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, ByVal dtLogindate As DateTime, Optional ByRef xLoginunkid As Integer = 0) As Boolean
        'Pinkal (12-Jan-2022) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " Select isnull(loginunkid,0) as  loginunkid " & _
                   " from tnalogin_tran " & _
                   " WHERE employeeunkid = @employeeunkid AND convert(char(8),logindate,112) = @logindate and isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtLogindate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables("List").Rows.Count > 0 Then
                'Pinkal (12-Jan-2022)-- Start
                'BioCorn Issue When Employee is taken leave for half day and more than that system will show In time and out time and also show late/early comings as per Matthew's Suggestion because payment is on hold.
                xLoginunkid = CInt(dsList.Tables("List").Rows(0).Item(0))
                'Pinkal (12-Jan-2022) -- End
                Return CBool(dsList.Tables("List").Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeLoginExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function



    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsEmployeeLoginSummaryExist(ByVal intEmployeeunkid As Integer, ByVal dtLogindate As DateTime) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " Select isnull(loginsummaryunkid,0) as  loginsummaryunkid, ISNULL(total_hrs,0) AS total_hrs " & _
                   " from tnalogin_summary " & _
                   " WHERE employeeunkid = @employeeunkid AND convert(char(8),login_date,112) = @logindate"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtLogindate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeLoginSummaryExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeLeaveCount(ByVal intEmployeeunkid As Integer, ByVal dtStartDate As DateTime, ByVal dtEnddate As DateTime) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " SELECT lvleaveIssue_tran.*,lvleavetype_master.ispaid " & _
                   " FROM lvleaveIssue_master  " & _
                   " JOIN  lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0  " & _
                   " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid AND lvleavetype_master.ispaid = 1 " & _
                   " WHERE lvleaveIssue_master.employeeunkid = @employeeunkid  And leavedate >= @startdate and leavedate <= @enddate AND lvleaveIssue_master.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtStartDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtEnddate)
            Dim count As Integer = objDataOperation.RecordCount(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return count
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function


    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeLeaveInfoForAbsent(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, ByVal mdtLeaveDate As DateTime, Optional ByVal intLeaveBalanceSetting As Integer = -1) As DataSet
        Try

            If intLeaveBalanceSetting <= 0 Then
                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If

            Dim dsList As DataSet = Nothing
            Dim exForce As Exception
            Dim strQ As String = " SELECT lvleaveIssue_master.leaveissueunkid,lvleaveIssue_tran.leaveissuetranunkid,ISNULL(lvleavebalance_tran.leavebalanceunkid,0) AS leavebalanceunkid " & _
                                          ", lvleaveIssue_master.leavetypeunkid,lvleaveIssue_master.employeeunkid " & _
                                          ", lvleaveIssue_tran.leavedate,lvleaveIssue_tran.ispaid,ISNULL(lvleavebalance_tran.Accrue_Amount,0) AS Accrue_Amount " & _
                                          ", lvleaveIssue_master.formunkid,ISNULL(lvleavebalance_tran.accruesetting,0) AS accruesetting " & _
                                          ", ISNULL(lvleavebalance_tran.issue_amount,0) AS issue_amount " & _
                                          ",lvleaveIssue_tran.dayfraction " & _
                                          " FROM lvleaveIssue_master " & _
                                          " JOIN lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND  lvleaveIssue_tran.isvoid = 0 " & _
                                          " LEFT JOIN  lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = lvleaveIssue_master.employeeunkid  AND lvleavebalance_tran.leavetypeunkid = lvleaveIssue_master.leavetypeunkid  AND lvleavebalance_tran.isvoid = 0 " & _
                                          " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                                          " WHERE lvleaveIssue_master.employeeunkid = @employeeunkid AND convert(char(8),lvleaveIssue_tran.leavedate,112) = @leavedate and lvleaveIssue_master.isvoid = 0"



            'Pinkal (24-Feb-2020) -- 'Bug [0004578] - voltamp : Unpaid Leave on Weekend considered as Weekend in Absent process.[LEFT JOIN  lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = lvleaveIssue_master.employeeunkid  AND lvleavebalance_tran.leavetypeunkid = lvleaveIssue_master.leavetypeunkid  AND lvleavebalance_tran.isvoid = 0]

            'Pinkal (30-Aug-2019) -- Working on Allow people to exceed time assigned to the project initially.[",lvleaveIssue_tran.dayfraction " & _]

            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " AND lvleavebalance_tran.isopenelc  = 1 AND lvleavebalance_tran.iselc = 1 "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeLeaveInfoForAbsent", mstrModuleName)
        End Try
        Return Nothing
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Sub UpdateAccrueBalance(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, ByVal mdtLogindate As DateTime, ByVal dsLeaveInfo As DataSet, Optional ByVal intLeavebalanceSetting As Integer = -1)
        Try
            Dim strQ As String = ""
            Dim exForce As Exception
            Dim isupdate As Boolean = False

            If intLeavebalanceSetting <= 0 Then
                intLeavebalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If


            'START FOR VOID LEAVE ISSUE TRAN  FOR THAT LOGIN DATE FOR SPECIFIC EMPLOYEE

            strQ = " Update lvleaveIssue_tran set " & _
                      "  isvoid = 1,voidreason = 'Employee was present on this day.' " & _
                      ", voiduserunkid = @voiduserunkid,voiddatetime = @voiddatetime " & _
                      " where leaveissueunkid = @leaveissueunkid and leavedate = @leavedate and isvoid = 0  "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("leaveissueunkid")))
            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLogindate)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            isupdate = CBool(objDataOperation.ExecNonQuery(strQ))

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'START FOR INSERT ATLEAVE TRAN

            If isupdate Then    'CHECK WHETHER FUTHER OPERATION DOING OR NOT

                Dim objIssue As New clsleaveissue_Tran
                objIssue._Leaveissuetranunkid = CInt(dsLeaveInfo.Tables(0).Rows(0)("leaveissuetranunkid"))
                With objIssue
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objIssue.InsertAuditTrailForLeaveIssue(objDataOperation, 3, mdtLogindate) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If


                'Pinkal (30-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                'If CDec(dsLeaveInfo.Tables(0).Rows(0)("issue_amount")) > 0 Then
                If CDec(dsLeaveInfo.Tables(0).Rows(0)("dayfraction")) > 0 Then
                    'Pinkal (30-Aug-2019) -- End
                    strQ = " Update lvleavebalance_tran set " & _
                               "  issue_amount = issue_amount  - @IssueAmt " & _
                               ", balance = balance + @IssueAmt  " & _
                               ", uptolstyr_issueamt =  uptolstyr_issueamt  - @IssueAmt " & _
                               ", remaining_bal = remaining_bal  + @IssueAmt " & _
                               ", days = days - @IssueAmt " & _
                               " where employeeunkid = @employeeunkid and leavetypeunkid = @leavetypeunkid and isvoid = 0  and yearunkid = @yearunkid  "

                    If intLeavebalanceSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= " AND isopenelc = 1 AND iselc = 1 "
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
                    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("leavetypeunkid")))

                    'Pinkal (30-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    'objDataOperation.AddParameter("@IssueAmt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dsLeaveInfo.Tables(0).Rows(0)("issue_amount")))
                    objDataOperation.AddParameter("@IssueAmt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dsLeaveInfo.Tables(0).Rows(0)("dayfraction")))
                    'Pinkal (30-Aug-2019) -- End
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    ' END FOR INCREASING THE LEAVE BALANCE BY 1 

                    'START FOR INSERT ATLEAVE BALANCE

                    Dim objBal As New clsleavebalance_tran
                    objBal._LeaveBalanceunkid = CInt(dsLeaveInfo.Tables(0).Rows(0)("leavebalanceunkid"))
                    With objBal
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End If

            End If

            'END FOR INSERT ATLEAVE BALANCE


            'Pinkal (20-Jan-2014) -- Start
            'Enhancement : Oman Changes

            ' START VOID LEAVE FORM IF ALL LEAVE DAYS EMPLOYEE ARE PRESENT 
            Dim dsList As DataSet = Nothing
            strQ = "Select Count(*) TotalLeave From lvleaveIssue_tran where leaveissueunkid = @leaveissueunkid AND voidreason = 'Employee was present on this day.' AND isvoid = 1 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("leaveissueunkid")))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            Dim mdecIssueCount As Decimal = 0
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecIssueCount = CDec(dsList.Tables(0).Rows(0)("TotalLeave"))
            End If

            strQ = "Select ISNULL(approve_days,0) AS approve_days From lvleaveform where formunkid = @formunkid AND employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("formunkid")))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("leavetypeunkid")))
            dsList = objDataOperation.ExecQuery(strQ, "LeaveForm")

            Dim mdecApproveDays As Decimal = 0
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecApproveDays = CDec(dsList.Tables(0).Rows(0)("approve_days"))
            End If

            If mdecApproveDays = mdecIssueCount AndAlso mdecIssueCount > 0 Then

                strQ = " UPDATE lvleaveIssue_master set isvoid = 1,voiduserunkid = @voiduserunkid,voidreason = 'Employee was present on this day.',voiddatetime = @voiddatetime WHERE formunkid = @formunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("formunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                objDataOperation.ExecNonQuery(strQ)

                strQ = " UPDATE lvleaveform set isvoid = 1,voiduserunkid = @voiduserunkid,voidreason = 'Employee was present on this day.',voiddatetime = @voiddatetime WHERE formunkid = @formunkid  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsLeaveInfo.Tables(0).Rows(0)("formunkid")))
                objDataOperation.ExecNonQuery(strQ)


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLogEmployeeUnkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lvleaveform", "formunkid", CInt(dsLeaveInfo.Tables(0).Rows(0)("formunkid"))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            End If

            ' END VOID LEAVE FORM IF ALL LEAVE DAYS EMPLOYEE ARE PRESENT 

            'Pinkal (20-Jan-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateAccrueBalance", mstrModuleName)
        End Try
    End Sub

    '   'Pinkal (06-Feb-2013) -- End


    Private Function AtAbsentProcess(ByVal objDataOperation As clsDataOperation, ByVal intperiodunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Try
            Dim intAuditTypeId As Integer = 1


            strQ = "Select * from atabsent_process where periodunkid = " & intperiodunkid
            If objDataOperation.RecordCount(strQ) > 0 Then
                intAuditTypeId = 2
            End If






            strQ = "INSERT INTO atabsent_process " & _
                       "( " & _
                            "  periodunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", host " & _
                            ", form_name " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            ", isweb " & _
                        ")" & _
                        "VALUES " & _
                        "( " & _
                            "  @periodunkid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @host " & _
                            ", @form_name " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            " " & _
                            ", @isweb " & _
                         ");Select @@IDENTITY "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intperiodunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditTypeId.ToString())
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP())
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())


            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If






            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AtAbsentProcess; Module Name: " & mstrModuleName)
        End Try
    End Function



    'Pinkal (24-Jan-2014) -- Start
    'Enhancement : Oman Changes

    Public Function GetEmployeeWeekendLeaveData(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer _
                                                            , ByVal mdtLeaveDate As DateTime, ByVal dtStartdate As DateTime, ByVal dtEnddate As DateTime, ByVal intShiftID As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            dsList = IsEmployeeLoginSummaryExist(intEmployeeunkid, mdtLeaveDate)

            If dsList.Tables("List").Rows.Count > 0 Then
                If CInt(dsList.Tables("List").Rows(0)("loginsummaryunkid")) > 0 AndAlso CInt(dsList.Tables("List").Rows(0)("total_hrs")) > 0 Then
                    Return 0 ' FOR PRESENT
                End If
            End If

            dsList = GetEmployeeLeaveInfoForAbsent(objDataOperation, intEmployeeunkid, mdtLeaveDate)

            If dsList.Tables("List").Rows.Count > 0 Then
                If CBool(dsList.Tables("List").Rows(0)("ispaid")) = True Then

                    If intLeaveCount = 0 Then
                        intLeaveCount = GetEmployeeLeaveCount(intEmployeeunkid, dtStartdate, dtEnddate)
                        intLeavebalance = CInt(dsList.Tables("List").Rows(0)("Issue_amount")) - intLeaveCount
                    End If
                    intLeavebalance = intLeavebalance + 1

                    If Math.Round(dsList.Tables("List").Rows(0)("Accrue_amount")) < intLeavebalance Then

                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Oman Changes  [As Mr.Prabhakar had problem when Issue Leave With Paid option is tick]

                        If CInt(dsList.Tables("List").Rows(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then
                            Return 4 'FOR NEGATIVE LEAVE BALANCE PAID ISSUE LEAVE
                        Else
                            Return 1 'FOR PAID ISSUE LEAVE
                        End If

                        'Pinkal (06-May-2014) -- End

                    Else
                        Return 1 'FOR PAID ISSUE LEAVE
                    End If


                ElseIf CBool(dsList.Tables("List").Rows(0)("ispaid")) = False Then
                    Return 2 'FOR UNPAID ISSUE LEAVE
                End If

            ElseIf dsList.Tables("List").Rows.Count = 0 Then

                ' START FOR CHECK WHETHER IT IS HOLIDAY OR NOT
                strQ = " SELECT lvemployee_holiday.holidayunkid,holidayname,employeeunkid,holidaydate FROM lvemployee_holiday " & _
                           " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                           " WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),holidaydate,112) = @leavedate"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If dsList.Tables("List").Rows.Count > 0 Then
                    If CInt(dsList.Tables("List").Rows(0)("holidayunkid")) > 0 Then
                        Return 5 ' FOR HOLIDAY
                    End If

                Else
                    strQ = "SELECT 1 FROM lvleaveform WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND returndate IS NULL AND CONVERT(CHAR(8),startdate,112) <= @leavedate ORDER BY startdate DESC "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                    objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
                    dsList = objDataOperation.ExecQuery(strQ, "List")
                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If
                    If dsList.Tables("List").Rows.Count > 0 Then
                        Return 1 'FOR PAID ISSUE LEAVE
                    End If
                End If

                ' END FOR CHECK WHETHER IT IS HOLIDAY OR NOT

            End If



        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    'Pinkal (24-Jan-2014) -- End



    'Pinkal (25-AUG-2017) -- Start
    'Enhancement - Working on B5 plus TnA Enhancement.

    Private Function GetLeaveDayFraction(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeId As Integer, ByVal mdtDate As Date, ByRef xLeaveTypeId As Integer) As Decimal
        Dim mdayFraction As Decimal = 0
        Dim strQ As String = ""
        Dim dsFraction As DataSet = Nothing
        Dim exForce As Exception
        Try

            'strQ = " SELECT ISNULL(dayfraction,0.00) AS 'Fraction' FROM lvleaveIssue_tran " & _
            '          " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_master.isvoid = 0 " & _
            '          " AND employeeunkid = @employeeunkid  " & _
            '          " WHERE CONVERT(CHAR(8),leavedate,112) = @leavedate AND lvleaveIssue_tran.isvoid = 0"

            strQ = " SELECT ISNULL(dayfraction,0.00) AS 'Fraction', ISNULL(lvleaveIssue_master.leavetypeunkid,0) AS leavetypeunkid FROM lvleaveIssue_tran " & _
                      " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_master.isvoid = 0 " & _
                      " AND employeeunkid = @employeeunkid  " & _
                      " WHERE CONVERT(CHAR(8),leavedate,112) = @leavedate AND lvleaveIssue_tran.isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
            dsFraction = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsFraction IsNot Nothing AndAlso dsFraction.Tables("List").Rows.Count > 0 Then
                mdayFraction = CDec(dsFraction.Tables("List").Rows(0)("Fraction"))
                xLeaveTypeId = CInt(dsFraction.Tables("List").Rows(0)("leavetypeunkid"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLeaveDayFraction; Module Name: " & mstrModuleName)
        End Try
        Return mdayFraction
    End Function

    'Pinkal (25-AUG-2017) -- End


    Private Function SetEmployeeDayFraction(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeId As Integer, ByVal mdtDate As Date, ByVal intShiftId As Integer) As Decimal
        Try
            Dim mintEmpWkHrs As Integer = 0
            Dim objlogin As New clslogin_Tran
            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(intShiftId)
            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtDate.DayOfWeek.ToString()))
            If drShift.Length > 0 Then
                mintEmpWkHrs = objlogin.GetEmployeeDayTotalWorkingHrs(objDataOperation, intEmployeeId, mdtDate)
                If CInt(drShift(0)("halffromhrsinsec")) <= 0 AndAlso CInt(drShift(0)("halftohrsinsec")) <= 0 Then
                    Return 1
                ElseIf mintEmpWkHrs >= CInt(drShift(0)("halffromhrsinsec")) And mintEmpWkHrs <= CInt(drShift(0)("halftohrsinsec")) Then
                    Return 0.5
                Else
                    Return 1
                End If
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetEmployeeDayFraction; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (27-May-2014) -- End


    'Pinkal (07-OCT-2014) -- Start
    'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 

    Public Function VoidAttendance(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeId As Integer, ByVal mdtDate As Date, ByVal intShiftId As Integer, ByVal blnDonotAttendanceinSeconds As Boolean) As Boolean
        'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END

        'Public Function VoidAttendance(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeId As Integer, ByVal mdtDate As Date, ByVal intShiftId As Integer) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try

            strQ = " SELECT loginunkid FROM tnalogin_tran WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),logindate,112) = @logindate AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                strQ = " Update tnalogin_tran set isvoid = 1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid  WHERE employeeunkid = @employeeunkid " & _
                          "AND CONVERT(CHAR(8),logindate,112) = @logindate AND loginunkid  = @loginunkid AND isvoid = 0 "

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                    objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "Employee was on Leave on this day.")
                    objDataOperation.AddParameter("@loginunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(i)("loginunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim objlogintran As New clslogin_Tran
                    objlogintran._Loginunkid = CInt(dsList.Tables(0).Rows(i)("loginunkid"))
                    With objlogintran
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objlogintran.InsertAuditTrailLogin(objDataOperation, 3, blnDonotAttendanceinSeconds) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

                strQ = "DELETE FROM tnalogin_summary WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),login_date,112) = @logindate "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@logindate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidAttendance; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function




    'Pinkal (15-Sep-2015) -- Start
    'Enhancement - WORKING ON ABSET PROCESS ENHANCEMENT.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeAbsentProcessList(ByVal blnProcessList As Boolean, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception = Nothing
        Try
            Dim objDataOperation As New clsDataOperation

            If blnProcessList Then

                strQ = "  SELECT  " & _
                          "  tnalogin_summary.employeeunkid  " & _
                          ", ISNULL(hremployee_master.employeecode, '') AS Employeecode " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee  " & _
                          "  FROM tnalogin_summary " & _
                          "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                          "  WHERE CONVERT(char(8),login_date,112) >= @startdate AND CONVERT(char(8),login_date,112) <= @enddate AND isabsentprocess = 1 "
            Else

                'Pinkal (22-Nov-2017) -- Start
                'Enhancement - Solving Absent Process Unprocessed Employee issue.

                'strQ = "  SELECT DISTINCT" & _
                '          "  tnalogin_summary.employeeunkid  " & _
                '          ", ISNULL(hremployee_master.employeecode, '') AS Employeecode " & _
                '          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                '          "  FROM tnalogin_summary " & _
                '          "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                '          "  WHERE CONVERT(char(8),login_date,112) <= @enddate AND isabsentprocess = 0 " & _
                '          " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                '          " AND (ISNULL(CONVERT(CHAR(8),termination_from_date,112),@enddate) <= @enddate " & _
                '          " OR ISNULL(CONVERT(CHAR(8),termination_to_date,112),@enddate) <= @enddate " & _
                '          " OR ISNULL(CONVERT(CHAR(8), empl_enddate,112), @enddate) <= @enddate) "


                strQ = "  SELECT DISTINCT" & _
                          "  tnalogin_summary.employeeunkid  " & _
                          ", ISNULL(hremployee_master.employeecode, '') AS Employeecode " & _
                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                          "  FROM tnalogin_summary " & _
                          "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                          "  WHERE CONVERT(char(8),login_date,112) <= @enddate AND isabsentprocess = 0 " & _
                          " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate "

                'Pinkal (22-Nov-2017) -- End

            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate.Date))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate.Date))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeAbsentProcessList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ProcessForUnprocessedEmp(ByVal dtTable As DataTable, ByVal intPeriodunkid As Integer, ByVal mdtEnddate As Date) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = " UPDATE tnalogin_summary SET isabsentprocess = 1 WHERE isabsentprocess = 0 AND employeeunkid = @employeeunkid AND Convert(Char(8),login_date,112) <= @enddate  "

            For Each dr As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEnddate))
                objDataOperation.ExecNonQuery(strQ)
            Next

            If AtAbsentProcess(objDataOperation, intPeriodunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            mblnFlag = True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ProcessForUnprocessedEmp; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return mblnFlag
    End Function


    'Pinkal (15-Sep-2015) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
