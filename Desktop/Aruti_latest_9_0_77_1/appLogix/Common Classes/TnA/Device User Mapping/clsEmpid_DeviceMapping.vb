﻿'************************************************************************************************************************************
'Class Name : clsEmpid_DeviceMapping.vb
'Purpose    :
'Date       :04/10/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmpid_devicemapping
    Private Const mstrModuleName = "clsEmpid_devicemapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDevicemappingunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrDeviceuserid As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set devicemappingunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Devicemappingunkid() As Integer
        Get
            Return mintDevicemappingunkid
        End Get
        Set(ByVal value As Integer)
            mintDevicemappingunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deviceuserid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Deviceuserid() As String
        Get
            Return mstrDeviceuserid
        End Get
        Set(ByVal value As String)
            mstrDeviceuserid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  devicemappingunkid " & _
              ", employeeunkid " & _
              ", deviceuserid " & _
              ", userunkid " & _
              ", isactive " & _
             "FROM hrempid_devicemapping " & _
             "WHERE devicemappingunkid = @devicemappingunkid "

            objDataOperation.AddParameter("@devicemappingunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDevicemappingUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdevicemappingunkid = CInt(dtRow.Item("devicemappingunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrdeviceuserid = dtRow.Item("deviceuserid").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strDatabaseName As String, _
                            ByVal intUserUnkid As Integer, _
                            ByVal intYearUnkid As Integer, _
                            ByVal intCompanyUnkid As Integer, _
                            ByVal dtPeriodStart As DateTime, _
                            ByVal dtPeriodEnd As DateTime, _
                            ByVal strUserModeSetting As String, _
                            ByVal blnOnlyApproved As Boolean, _
                            ByVal blnIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnIncludeAllEmployee As Boolean = False, _
                            Optional ByVal intEmpId As Integer = 0, _
                            Optional ByVal strDeviceUserId As String = "", _
                            Optional ByVal mstrAdvanceFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            Dim strJOIN As String = ""
            If blnIncludeAllEmployee = True Then
                strJOIN = "RIGHT "
            Else
                strJOIN = "LEFT "
            End If

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            strQ = "SELECT  ISNULL(hrempid_devicemapping.devicemappingunkid, -1) AS devicemappingunkid " & _
                   ", hremployee_master.employeeunkid " & _
                   ", hremployee_master.employeecode " & _
                   ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                   ", hremployee_master.departmentunkid " & _
                   ", dept.name AS deptname " & _
                   ", ISNULL(hrempid_devicemapping.deviceuserid, '') AS deviceuserid " & _
                   ", ISNULL(hrempid_devicemapping.userunkid, 0) AS userunkid " & _
                   ", ISNULL(hrempid_devicemapping.isactive, 1) AS isactive " & _
                   ", '' AS AUD " & _
                   "FROM    hrempid_devicemapping " & _
                   strJOIN & " JOIN hremployee_master ON hrempid_devicemapping.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         departmentunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "    FROM hremployee_transfer_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "LEFT JOIN hrdepartment_master AS dept ON Alloc.departmentunkid = dept.departmentunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE   ISNULL(hrempid_devicemapping.isactive, 1) = 1 "

            If intEmpId > 0 Then
                strQ &= "AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            End If

            If strDeviceUserId.Trim <> "" Then
                strQ &= "AND ISNULL(hrempid_devicemapping.deviceuserid, '') = @deviceuserid "
                objDataOperation.AddParameter("@deviceuserid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDeviceUserId)
            End If

            If mstrAdvanceFilter.Trim <> "" Then
                strQ &= "AND " & mstrAdvanceFilter
            End If

            If blnIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            strQ &= " ORDER By ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnIncludeAllEmployee As Boolean = False, Optional ByVal intEmpId As Integer = 0, Optional ByVal strDeviceUserId As String = "", Optional ByVal mstrAdvanceFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        Dim strJOIN As String = ""
    '        If blnIncludeAllEmployee = True Then
    '            strJOIN = "RIGHT "
    '        Else
    '            strJOIN = "LEFT "
    '        End If

    '        strQ = "SELECT  ISNULL(hrempid_devicemapping.devicemappingunkid, -1) AS devicemappingunkid " & _
    '                      ", hremployee_master.employeeunkid " & _
    '                      ", hremployee_master.employeecode " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
    '                      ", hremployee_master.departmentunkid " & _
    '                      ", dept.name AS deptname " & _
    '                      ", ISNULL(hrempid_devicemapping.deviceuserid, '') AS deviceuserid " & _
    '                      ", ISNULL(hrempid_devicemapping.userunkid, 0) AS userunkid " & _
    '                      ", ISNULL(hrempid_devicemapping.isactive, 1) AS isactive " & _
    '                      ", '' AS AUD " & _
    '                "FROM    hrempid_devicemapping " & _
    '                        strJOIN & " JOIN hremployee_master ON hrempid_devicemapping.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN hrdepartment_master AS dept ON hremployee_master.departmentunkid = dept.departmentunkid " & _
    '                "WHERE   ISNULL(hrempid_devicemapping.isactive, 1) = 1 "

    '        If intEmpId > 0 Then
    '            strQ &= "AND hremployee_master.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
    '        End If

    '        If strDeviceUserId.Trim <> "" Then
    '            strQ &= "AND ISNULL(hrempid_devicemapping.deviceuserid, '') = @deviceuserid "
    '            objDataOperation.AddParameter("@deviceuserid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDeviceUserId)
    '        End If

    '        If mstrAdvanceFilter.Trim <> "" Then
    '            strQ &= "AND " & mstrAdvanceFilter
    '        End If

    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            strQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        End If

    '        strQ &= " ORDER By ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    

    Public Function GetEmployeeUnkID(ByVal strDeviceUserId As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intEmpID As Integer = 0

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT   hrempid_devicemapping.employeeunkid " & _
                    "FROM    hrempid_devicemapping " & _
                    "WHERE   ISNULL(hrempid_devicemapping.isactive, 1) = 1 " & _
                    "AND ISNULL(hrempid_devicemapping.deviceuserid, '') = @deviceuserid "

            objDataOperation.AddParameter("@deviceuserid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDeviceUserId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intEmpID = CInt(dsList.Tables("List").Rows(0).Item("employeeunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeUnkID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intEmpID
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrempid_devicemapping) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
            objDataOperation.AddParameter("@deviceuserid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdeviceuserid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "INSERT INTO hrempid_devicemapping ( " & _
              "  employeeunkid " & _
              ", deviceuserid " & _
              ", userunkid " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @deviceuserid " & _
              ", @userunkid " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDevicemappingUnkId = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrempid_devicemapping", "devicemappingunkid", mintDevicemappingunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrempid_devicemapping) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintDevicemappingunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@devicemappingunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdevicemappingunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
            objDataOperation.AddParameter("@deviceuserid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdeviceuserid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "UPDATE hrempid_devicemapping SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", deviceuserid = @deviceuserid" & _
              ", userunkid = @userunkid" & _
              ", isactive = @isactive " & _
            "WHERE devicemappingunkid = @devicemappingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrempid_devicemapping", "devicemappingunkid", mintDevicemappingunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrempid_devicemapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "UPDATE hrempid_devicemapping SET " & _
                        "  isactive = 0 " & _
                    "WHERE devicemappingunkid = @devicemappingunkid "

            objDataOperation.AddParameter("@devicemappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrempid_devicemapping", "devicemappingunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Void(ByVal intEmployeeUnkid As Integer) As Boolean
        'If isUsed(intEmployeeUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = -1

        Try

            intUnkId = GetUnkId(intEmployeeUnkid)

            objDataOperation = New clsDataOperation

            strQ = "UPDATE hrempid_devicemapping SET " & _
                        "  isactive = 0 " & _
                    "WHERE isactive =1 AND employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrempid_devicemapping", "devicemappingunkid", intUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertUpdate(ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            For Each dtRow As DataRow In dtTable.Rows
                If dtRow.Item("AUD") = "U" Then

                    intUnkId = GetUnkId(CInt(dtRow.Item("employeeunkid")))

                    mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))

                    'Pinkal (28-Oct-2015) -- Start
                    'Enhancement - WORKING ON MSBR CHANGES ON DEVICE USER MAPPING.
                    If IsDBNull(dtRow.Item("deviceuserid")) Then
                        mstrDeviceuserid = ""
                    Else
                    mstrDeviceuserid = dtRow.Item("deviceuserid")
                    End If
                    'Pinkal (28-Oct-2015) -- End


                    mblnIsactive = True

                    If intUnkId <= 0 Then
                        mintDevicemappingunkid = 0
                        If Insert() = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    Else
                        mintDevicemappingunkid = intUnkId
                        If Update() = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If

                End If
            Next

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@devicemappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        " 1 " & _
                 "FROM hrempid_devicemapping " & _
                 "WHERE isactive = 1 " & _
                 "AND employeeunkid = @employeeunkid "

            If intUnkid > 0 Then
                strQ &= " AND devicemappingunkid <> @devicemappingunkid"
                objDataOperation.AddParameter("@devicemappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetUnkId(ByVal intEmpId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkid As Integer = -1

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        " devicemappingunkid " & _
                 "FROM hrempid_devicemapping " & _
                 "WHERE isactive = 1 " & _
                 "AND employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkid = CInt(dsList.Tables(0).Rows(0).Item("devicemappingunkid"))
            End If

            Return intUnkid
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
            Return intUnkid
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetDeviceUserId(ByVal intEmpId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strDeviceUserId As String = ""

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        " deviceuserid " & _
                 "FROM hrempid_devicemapping " & _
                 "WHERE isactive = 1 " & _
                 "AND employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strDeviceUserId = dsList.Tables(0).Rows(0).Item("deviceuserid").ToString
            End If

            Return strDeviceUserId
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
            Return strDeviceUserId
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class