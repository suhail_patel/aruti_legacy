﻿Imports eZeeCommonLib
Imports System.Xml
Imports System.IO
Imports System.Data


Public Class clsCommonImportExport

    Private Shared ReadOnly mstrModuleName As String = "clsCommonImportExport"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Properties "


'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

#End Region

#Region "Private Methods"

    Public Sub ExportType(ByVal strFilePath As String, ByVal FileType As Integer, ByVal dsList As DataSet)
        Try

            Dim ObjFile As FileInfo

            If FileType = 1 Then   'XML

                ObjFile = New FileInfo(strFilePath & ".xml")
                If ObjFile.Exists Then
                    ObjFile.Delete()
                End If
                dsList.WriteXml(strFilePath & ".xml")
                
                ObjFile.IsReadOnly = True
                ObjFile.Attributes = FileAttributes.ReadOnly

            ElseIf FileType = 2 Then ' EXCEL
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim export As New ExcelData
                'S.SANDEEP [12-Jan-2018] -- END
                ObjFile = New FileInfo(strFilePath & ".xls")
                If ObjFile.Exists Then
                    ObjFile.Delete()
                End If
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                Aruti.Data.modGlobal.OpenXML_Export(strFilePath & ".xls", dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                ObjFile.IsReadOnly = True
                ObjFile.Attributes = FileAttributes.ReadOnly

            ElseIf FileType = 3 Then ' CSV
                Dim objCsvExport As New clsExportData
                ObjFile = New FileInfo(strFilePath & ".csv")
                If ObjFile.Exists Then
                    ObjFile.Delete()
                End If
                objCsvExport.ExportToCSV(dsList.Tables(0), strFilePath & ".csv")
                ObjFile.IsReadOnly = True
                ObjFile.Attributes = FileAttributes.ReadOnly
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function GetCommonMasterUnkId(ByVal intMasterType As Integer, ByVal mstrName As String) As Integer
        Try
            Dim objCommon As New clsCommon_Master
            Return objCommon.GetCommonMasterUnkId(intMasterType, mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommonMasterUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetCountryUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objCountry As New clsMasterData
            Return objCountry.GetCountryUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCountryUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetStateUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objState As New clsstate_master
            Return objState.GetStateUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetStateUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetCityUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objCity As New clscity_master
            Return objCity.GetCityUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCityUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetZipcodeUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objZipcode As New clszipcode_master
            Return objZipcode.GetZipCodeUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetZipcodeUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetStationUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objStation As New clsStation
            Return objStation.GetStationUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetStationUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetDepartmentGroupUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objDeptGrp As New clsDepartmentGroup
            Return objDeptGrp.GetDepartmentGrpUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDepartmentGroupUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetDepartmentUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objDept As New clsDepartment
            Return objDept.GetDepartmentUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDepartmentGroupUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetSectionUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objSection As New clsSections
            Return objSection.GetSectionUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSectionUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetClassGroupUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objClassGrp As New clsClassGroup
            Return objClassGrp.GetClassGrpUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetClassGroupUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetGradeGroupUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objGradeGrp As New clsGradeGroup
            Return objGradeGrp.GetGradeGrpUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetGradeGroupUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetGradeUnkid(ByVal mstrName As String) As Integer
        Try
            Dim objGrade As New clsGrade
            Return objGrade.GetGradeUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetGradeUnkid", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetPayrollGrpUnkId(ByVal intMasterType As Integer, ByVal mstrName As String) As Integer
        Try
            Dim objPayrollGrp As New clspayrollgroup_master
            Return objPayrollGrp.GetPayrollGrpUnkId(intMasterType, mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPayrollGrpUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetUnitUnkId(ByVal mstrName As String) As Integer
        Try
            Dim objUnit As New clsUnits
            Return objUnit.GetUnitUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUnitUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetJobGroupUnkId(ByVal mstrName As String) As Integer
        Try
            Dim objJobgrp As New clsJobGroup
            Return objJobgrp.GetJobGroupUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetJobGroupUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetJobUnkId(ByVal mstrName As String) As Integer
        Try
            Dim objJob As New clsJobs
            Return objJob.GetJobUnkId(mstrName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetJobGroupUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetSectionGroupUnkid(ByVal mstrSectionGrp As String) As Integer
        Try
            Dim objSGrp As New clsSectionGroup
            Return objSGrp.GetSectionGrpUnkId(mstrSectionGrp)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSectionGroupUnkid", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetUnitGroupUnkid(ByVal mstrUnitGroup As String) As Integer
        Try
            Dim objUGrp As New clsUnitGroup
            Return objUGrp.GetUnitGrpUnkId(mstrUnitGroup)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUnitGroupUnkid", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetTeamUnkid(ByVal mstrTeamName As String) As Integer
        Try
            Dim objTeam As New clsTeams
            Return objTeam.GetTeamUnkId(mstrTeamName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTeamUnkid", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [20 JUN 2016] -- START
    Public Function GetClassUnkid(ByVal mstrClassName As String) As Integer
        Try
            Dim objClass As New clsClass
            Return objClass.GetClassUnkId(mstrClassName)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetClassUnkid", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [20 JUN 2016] -- END

#End Region

#Region "Common Master"

    Public Function ExportCommonMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet

        Try


            'Pinkal (05-Jan-2013) -- Start
            'Enhancement : TRA Changes

            Dim objDataOperation As New clsDataOperation

            'strQ = "SELECT " & _
            '        "  code " & _
            '        ", alias " & _
            '        ", name " & _
            '        ", name1 " & _
            '        ", name2 " & _
            '        ", case when mastertype = 1 then 'ADVERTISE CATEGORY' " & _
            '                  "when mastertype = 2 then 'ALLERGIES' " & _
            '                  "when mastertype = 3 then 'ASSET CONDITION' " & _
            '                  "when mastertype = 4 then 'BENEFIT GROUP' " & _
            '                  "when mastertype = 5 then 'BLOOD GROUP' " & _
            '                  "when mastertype = 6 then 'COMPLEXION' " & _
            '                  "when mastertype = 7 then 'DISABILITIES' " & _
            '                  "when mastertype = 8 then 'EMPLOYEMENT TYPE' " & _
            '                  "when mastertype = 9 then 'ETHNICITY' " & _
            '                  "when mastertype = 10 then 'EYES COLOR' " & _
            '                  "when mastertype = 11 then 'HAIR COLOR' " & _
            '                  "when mastertype = 12 then 'IDENTITY TYPES' " & _
            '                  "when mastertype = 13 then 'INTERVIEW TYPE' " & _
            '                  "when mastertype = 14 then 'LANGUAGES' " & _
            '                  "when mastertype = 15 then 'MARRIED STATUS' " & _
            '                  "when mastertype = 16 then 'MEMBERSHIP CATEGORY' " & _
            '                  "when mastertype = 17 then 'PAY TYPE' " & _
            '                  "when mastertype = 18 then 'QUALIFICATION COURSE GROUP' " & _
            '                  "when mastertype = 19 then 'RELATIONS' " & _
            '                  "when mastertype = 20 then 'RELIGION' " & _
            '                  "when mastertype = 21 then 'RESULT GROUP' " & _
            '                  "when mastertype = 22 then 'SHIFT TYPE' " & _
            '                  "when mastertype = 23 then 'SKILL CATEGORY' " & _
            '                  "when mastertype = 24 then 'TITLE' " & _
            '                  "when mastertype = 25 then 'TRAINING RESOURCES' " & _
            '                  "when mastertype = 26 then 'ASSET STATUS' " & _
            '                  "when mastertype = 27 then 'ASSETS' " & _
            '                  "when mastertype = 28 then 'LETTER TYPE' end  mastertype  " & _
            '        ", isactive " & _
            '       "FROM cfcommon_master "

            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            'strQ = "SELECT " & _
            '        "  Code " & _
            '        ", Alias " & _
            '        ", Name " & _
            '        ", case when mastertype = 1 then @AdvCategory " & _
            '                  "when mastertype = 2 then @Allergies " & _
            '                  "when mastertype = 3 then @AssetCondition " & _
            '                  "when mastertype = 4 then @BenifitGrp " & _
            '                  "when mastertype = 5 then @BloodGrp " & _
            '                  "when mastertype = 6 then @Complexion " & _
            '                  "when mastertype = 7 then @Disabilities " & _
            '                  "when mastertype = 8 then @EmpType  " & _
            '                  "when mastertype = 9 then @Ethnicity " & _
            '                  "when mastertype = 10 then @EyesColor " & _
            '                  "when mastertype = 11 then @HairColor " & _
            '                  "when mastertype = 12 then @IdentityType " & _
            '                  "when mastertype = 13 then @InterViewType " & _
            '                  "when mastertype = 14 then @Languages " & _
            '                  "when mastertype = 15 then @MarriedStaus " & _
            '                  "when mastertype = 16 then @MembershipCategory " & _
            '                  "when mastertype = 17 then @PayType " & _
            '                  "when mastertype = 18 then @QualificationGrp " & _
            '                  "when mastertype = 19 then @Relations " & _
            '                  "when mastertype = 20 then @Religion " & _
            '                  "when mastertype = 21 then @ResultGrp " & _
            '                  "when mastertype = 22 then @ShiftType " & _
            '                  "when mastertype = 23 then @SkillCategory " & _
            '                  "when mastertype = 24 then @Title " & _
            '                  "when mastertype = 25 then @TrainingResource " & _
            '                  "when mastertype = 26 then @AssetStatus " & _
            '                  "when mastertype = 27 then @Assets " & _
            '                  "when mastertype = 28 then @LetterType " & _
            '                  "when mastertype = 29 then @Salincreason " & _
            '                  "when mastertype = 30 then @TrainingCourse " & _
            '                  "when mastertype = 31 then @StrategicGoal " & _
            '                  "when mastertype = 32 then @SourcesFunding " & _
            '                  "when mastertype = 33 then @AttachmentType " & _
            '                  "when mastertype = 34 then @Vacancy " & _
            '                  "when mastertype = 35 then @DisciplineCommitee " & _
            '                  "when mastertype = 36 then @OffenceCategory " & _
            '                  "when mastertype = 37 then @CommitteeCategory  end  [Master Type]  " & _
            '                  ", qlevel  AS [Qualification Group Level] " & _
            '                  ", dependant_limit AS [Dependant Limit] " & _
            '                  ", isrefreeallowed AS [Is Refree Allowed] " & _
            '                  ", CASE WHEN coursetypeid = 1 THEN @JobCapability WHEN coursetypeid = 2 THEN @CareerDevelopment ELSE '' END As [Course Type] " & _
            '        ", isactive " & _
            '       "FROM cfcommon_master "

            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@AdvCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "ADVERTISE CATEGORY"))
            'objDataOperation.AddParameter("@Allergies", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "ALLERGIES"))
            'objDataOperation.AddParameter("@AssetCondition", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "ASSET CONDITION"))
            'objDataOperation.AddParameter("@BenifitGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "BENEFIT GROUP"))
            'objDataOperation.AddParameter("@BloodGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "BLOOD GROUP"))
            'objDataOperation.AddParameter("@Complexion", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "COMPLEXION"))
            'objDataOperation.AddParameter("@Disabilities", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "DISABILITIES"))
            'objDataOperation.AddParameter("@EmpType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "EMPLOYEMENT TYPE"))
            'objDataOperation.AddParameter("@Ethnicity", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "ETHNICITY"))
            'objDataOperation.AddParameter("@EyesColor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "EYES COLOR"))
            'objDataOperation.AddParameter("@HairColor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "HAIR COLOR"))
            'objDataOperation.AddParameter("@IdentityType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "IDENTITY TYPES"))
            'objDataOperation.AddParameter("@InterViewType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "INTERVIEW TYPE"))
            'objDataOperation.AddParameter("@Languages", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "LANGUAGES"))
            'objDataOperation.AddParameter("@MarriedStaus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "MARRIED STATUS"))
            'objDataOperation.AddParameter("@MembershipCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "MEMBERSHIP CATEGORY"))
            'objDataOperation.AddParameter("@PayType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "PAY TYPE"))
            'objDataOperation.AddParameter("@QualificationGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "QUALIFICATION COURSE GROUP"))
            'objDataOperation.AddParameter("@Relations", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "RELATIONS"))
            'objDataOperation.AddParameter("@Religion", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "RELIGION"))
            'objDataOperation.AddParameter("@ResultGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "RESULT GROUP"))
            'objDataOperation.AddParameter("@ShiftType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "SHIFT TYPE"))
            'objDataOperation.AddParameter("@SkillCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "SKILL CATEGORY"))
            'objDataOperation.AddParameter("@Title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "TITLE"))
            'objDataOperation.AddParameter("@TrainingResource", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "TRAINING RESOURCES"))
            'objDataOperation.AddParameter("@AssetStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "ASSET STATUS"))
            'objDataOperation.AddParameter("@Assets", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "ASSETS"))
            'objDataOperation.AddParameter("@LetterType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "LETTER TYPE"))
            'objDataOperation.AddParameter("@Salincreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "SALINC REASON"))
            'objDataOperation.AddParameter("@TrainingCourse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "TRAINING COURSE MASTER"))
            'objDataOperation.AddParameter("@StrategicGoal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "STRATEGIC GOAL"))
            'objDataOperation.AddParameter("@SourcesFunding", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "SOURCES FUNDINGS"))
            'objDataOperation.AddParameter("@AttachmentType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "ATTACHMENT TYPES"))
            'objDataOperation.AddParameter("@Vacancy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "VACANCY MASTER"))
            'objDataOperation.AddParameter("@DisciplineCommitee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "DISCIPLINE COMMITTEE"))
            'objDataOperation.AddParameter("@OffenceCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "OFFENCE CATEGORY"))
            'objDataOperation.AddParameter("@CommitteeCategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "COMMITTEE MEMBERS CATEGORY"))
            'objDataOperation.AddParameter("@JobCapability", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "JOB CAPABILITY"))
            'objDataOperation.AddParameter("@CareerDevelopment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "CAREER DEVELOPMENT"))
            Dim dsCommon As DataSet = (New clsMasterData).getComboList_Common_Master("List")
            Dim dicCommon As Dictionary(Of Integer, String) = (From p In dsCommon.Tables("List") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            strQ = "SELECT " & _
                    "  Code " & _
                    ", Alias " & _
                    ", Name " & _
                              ", qlevel  AS [Qualification Group Level] " & _
                              ", dependant_limit AS [Dependant Limit] " & _
                              ", isrefreeallowed AS [Is Refree Allowed] " & _
                              ", CASE WHEN coursetypeid = 1 THEN @JobCapability WHEN coursetypeid = 2 THEN @CareerDevelopment ELSE '' END As [Course Type] " & _
                        ", isactive "

            strQ &= ", CASE cfcommon_master.mastertype "
            For Each pair In dicCommon
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS [Master Type] "

            strQ &= "FROM cfcommon_master "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@JobCapability", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "JOB CAPABILITY"))
            objDataOperation.AddParameter("@CareerDevelopment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "CAREER DEVELOPMENT"))
            'Sohail (27 Sep 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "CommonMaster")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("CommonMaster").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (05-Jan-2013) -- End





            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportCommonMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportCommonMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportCommonMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Reason Master"

    Public Function ExportReasonMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        'Dim exForce As Exception
        Dim dsList As New DataSet

        Try

            Dim objReason As New clsAction_Reason
            dsList = objReason.GetList("Reason", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Reason").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("actionreasonunkid")
            End If

             ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportReasonMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportReasonMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportReasonMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Skill Master"

    Public Function ExportSkillMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        'Dim exForce As Exception
        Dim dsList As New DataSet

        Try

            Dim objSkill As New clsskill_master
            dsList = objSkill.GetList("Skill", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Skill").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("skillunkid")
                dsList.Tables(0).Columns.Remove("skillcategoryunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("skillname1")
                dsList.Tables(0).Columns.Remove("skillname2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportSkillMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportSkillMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportSkillMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "State Master"

    Public Function ExportStateMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try

            Dim objstate As New clsstate_master
            dsList = objstate.GetList("State", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("State").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End


            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportStateMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportStateMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportStateMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "City Master"

    Public Function ExportCityMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet

        Try

            Dim objcity As New clscity_master
            dsList = objcity.GetList("City")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("City").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportCityMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportCityMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportCityMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Zipcode Master"

    Public Function ExportZipcodeMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet

        Try

            Dim objZipcode As New clszipcode_master
            dsList = objZipcode.GetList("Zipcode")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Zipcode").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("zipcodeunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportZipcodeMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportZipcodeMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportZipcodeMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Advertise Master"

    Public Function ExportAdvertiseMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet

        Try

            Dim objAdvertise As New clsAdvertise_master
            dsList = objAdvertise.GetList("Advertise", False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Advertise").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("advertiseunkid")
                dsList.Tables(0).Columns.Remove("categoryunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("pincodeunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")
            End If

             ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportAdvertiseMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportAdvertiseMaster() As Boolean
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportAdvertiseMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Quallification / Course Master"

    Public Function ExportQualificationMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objqualification As New clsqualification_master
            dsList = objqualification.GetList("Qualification")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Qualification").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End


            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("qualificationunkid")
                dsList.Tables(0).Columns.Remove("qualificationgroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("resultgroupunkid")
                dsList.Tables(0).Columns.Remove("qualificationname1")
                dsList.Tables(0).Columns.Remove("qualificationname2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportQualificationMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportQualificationMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportQualificationMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Result Master"

    Public Function ExportResultMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objResult As New clsresult_master
            dsList = objResult.GetList("Result", False)
            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("resultunkid")
                dsList.Tables(0).Columns.Remove("resultgroupunkid")

                'Pinkal (18-Aug-2013) -- Start
            'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("resultname1")
                dsList.Tables(0).Columns.Remove("resultname2")
                'Pinkal (18-Aug-2013) -- End

            End If

             ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportResultMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportResultMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportResultMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Membership Master"

    Public Function ExportMembershipMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objMembership As New clsmembership_master
            dsList = objMembership.GetList("Membership", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Membership").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("membershipunkid")
                dsList.Tables(0).Columns.Remove("membershipcategoryunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("membershipname1")
                dsList.Tables(0).Columns.Remove("membershipname2")
                dsList.Tables(0).Columns.Remove("emptranheadunkid")
                dsList.Tables(0).Columns.Remove("cotranheadunkid")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportMembershipMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportMembershipMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportMembershipMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Benefit Plan Master"

    Public Function ExportBenefit_plan_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objBenefitPlan As New clsbenefitplan_master
            dsList = objBenefitPlan.GetList("BenefitPlan", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("BenefitPlan").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("benefitplanunkid")
                dsList.Tables(0).Columns.Remove("benefitgroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportBenefit_plan_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportBenefit_plan_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportBenefit_plan_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Branch Master"

    Public Function ExportBranchMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objstation As New clsStation
            dsList = objstation.GetList("Branch", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Branch").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("stationunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("countryunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("postalunkid")
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

           ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportBranchMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportBranchMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportBranchMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Department Group Master"

    Public Function ExportDeptGroupMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objDeptGrp As New clsDepartmentGroup
            dsList = objDeptGrp.GetList("DeptGrp", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("DeptGrp").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End




            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("deptgroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDeptGroupMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDeptGroupMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDeptGroupMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Department Master"

    Public Function ExportDepartmentMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objDept As New clsDepartment
            dsList = objDept.GetList("Dept", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Dept").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("departmentunkid")
                dsList.Tables(0).Columns.Remove("deptgroupunkid")
                dsList.Tables(0).Columns.Remove("stationunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("StationName")
                dsList.Tables(0).Columns.Remove("DeptGroupName")
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDepartmentMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDepartmentMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDepartmentMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Section Master"

    Public Function ExportSectionMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objSection As New clsSections
            dsList = objSection.GetList("Section", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Section").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("sectionunkid")
                dsList.Tables(0).Columns.Remove("departmentunkid")
                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dsList.Tables(0).Columns.Remove("sectiongroupunkid")
                'S.SANDEEP [ 07 NOV 2011 ] -- END


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("DeptName")
                dsList.Tables(0).Columns.Remove("SectionGrp")
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportSectionMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportSectionMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportSectionMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Unit Master"

    Public Function ExportUnitMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objUnit As New clsUnits
            dsList = objUnit.GetList("Unit", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Unit").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("unitunkid")
                dsList.Tables(0).Columns.Remove("sectionunkid")
                dsList.Tables(0).Columns.Remove("unitgroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

             ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportUnitMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportUnitMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportUnitMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Job Group Master"

    Public Function ExportJobGroupMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objJobGrp As New clsJobGroup
            dsList = objJobGrp.GetList("JobGroup", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("JobGroup").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("jobgroupunkid")


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

             ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportJobGroupMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportJobGroupMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportJobGroupMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Job Master"

    Public Function ExportJobMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objJob As New clsJobs
            dsList = objJob.GetList("Job", False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Job").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("jobunkid")
                dsList.Tables(0).Columns.Remove("jobgroupunkid")
                dsList.Tables(0).Columns.Remove("jobheadunkid")
                dsList.Tables(0).Columns.Remove("jobunitunkid")
                dsList.Tables(0).Columns.Remove("jobsectionunkid")
                dsList.Tables(0).Columns.Remove("jobgradeunkid")
                dsList.Tables(0).Columns.Remove("Reportunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("teamunkid")
                dsList.Tables(0).Columns.Remove("job_name1")
                dsList.Tables(0).Columns.Remove("job_name2")
                'Pinkal (18-Aug-2013) -- End

            End If

             ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportJobMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportJobMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportJobMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Class Group Master"

    Public Function ExportClassGroupMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objClassGrp As New clsClassGroup
            dsList = objClassGrp.GetList("ClassGrp", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("ClassGrp").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("classgroupunkid")


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportClassGroupMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportClassGroupMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportClassGroupMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Class Master"

    Public Function ExportClassMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objClass As New clsClass
            dsList = objClass.GetList("Class", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Class").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("classesunkid")
                dsList.Tables(0).Columns.Remove("classgroupunkid")


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportClassMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportClassMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportClassMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Grade Group Master"

    Public Function ExportGradeGroupMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objGradeGrp As New clsGradeGroup
            dsList = objGradeGrp.GetList("GradeGrp", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("GradeGrp").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("gradegroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportGradeGroupMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportGradeGroupMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportGradeGroupMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Grade Master"

    Public Function ExportGradeMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objGrade As New clsGrade
            dsList = objGrade.GetList("Grade", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Grade").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("gradegroupunkid")
                dsList.Tables(0).Columns.Remove("gradeunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportGradeMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportGradeMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportGradeMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Grade Level Master"

    Public Function ExportGradeLevelMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objGradeLevel As New clsGradeLevel
            dsList = objGradeLevel.GetList("GradeLevel", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("GradeLevel").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("gradelevelunkid")
                dsList.Tables(0).Columns.Remove("gradeunkid")
                dsList.Tables(0).Columns.Remove("gradegroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

             ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportGradeLevelMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportGradeLevelMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportGradeLevelMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Shift Master"

    Public Function ExportShiftMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim strQ As String = ""
        Try


            Dim objDataOperation As New clsDataOperation
            Dim objShift As New clsshift_tran


            strQ = " SELECT " & _
                      "cfcommon_master.name AS 'ShiftType' " & _
                      ",tnashift_master.shiftcode " & _
                      ",tnashift_master.shiftname " & _
                      ", tnashift_tran.dayid " & _
                      ", '' AS DAYS " & _
                      ", tnashift_master.weekhours " & _
                      ", tnashift_tran.isweekend " & _
                      ", tnashift_tran.starttime " & _
                      ", tnashift_tran.endtime " & _
                      ", tnashift_tran.breaktime AS breaktime " & _
                      ", tnashift_tran.workinghrs AS workinghrs " & _
                      ", tnashift_tran.halfdayfromhrs AS halffromhrs " & _
                      ", tnashift_tran.halfdaytohrs AS halftohrs " & _
                      ", tnashift_tran.nightfromhrs " & _
                      ", tnashift_tran.nighttohrs " & _
                      ", tnashift_tran.calcovertimeafter AS calcovertimeafter " & _
                      ", tnashift_tran.calcshorttimebefore AS calcshorttimebefore " & _
                      ", tnashift_tran.daystart_time  " & _
                      ", tnashift_tran.countotmins_aftersftendtime " & _
                      ", tnashift_master.isopenshift " & _
                      ", tnashift_master.maxshifthrs " & _
                      ", tnashift_master.attondeviceiostatus " & _
                      ", tnashift_master.isnormalhrs_forwkend " & _
                      ", tnashift_master.isnormalhrs_forph " & _
                      ", tnashift_master.isnormalhrs_fordayoff " & _
                      ", tnashift_master.iscountshifttimeonly " & _
                      ", tnashift_master.iscnt_ot_ontotalworkhrs " & _
                      " FROM   tnashift_tran " & _
                      " JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid=tnashift_master.shifttypeunkid AND cfcommon_master.isactive = 1 AND cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.SHIFT_TYPE & _
                      " WHERE tnashift_master.isactive = 1 "

            'Pinkal (04-Dec-2019) -- 'Enhancement NMB - Working On Close Year Enhancement for NMB.[", tnashift_tran.countotmins_aftersftendtime " & _]
            '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]

            dsList = objDataOperation.ExecQuery(strQ, "ShiftMaster")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim days() As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames


            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                Select Case CInt(dsList.Tables(0).Rows(i)("dayid"))
                    Case 0
                        dsList.Tables(0).Rows(i)("DAYS") = days(0).ToString()
                    Case 1
                        dsList.Tables(0).Rows(i)("DAYS") = days(1).ToString()
                    Case 2
                        dsList.Tables(0).Rows(i)("DAYS") = days(2).ToString()
                    Case 3
                        dsList.Tables(0).Rows(i)("DAYS") = days(3).ToString()
                    Case 4
                        dsList.Tables(0).Rows(i)("DAYS") = days(4).ToString()
                    Case 5
                        dsList.Tables(0).Rows(i)("DAYS") = days(5).ToString()
                    Case 6
                        dsList.Tables(0).Rows(i)("DAYS") = days(6).ToString()
                End Select

            Next

            If dsList.Tables(0).Columns.Contains("dayid") Then
                dsList.Tables(0).Columns.Remove("dayid")
            End If

            dsList.AcceptChanges()


           

            'Pinkal (03-Jul-2013) -- End

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportShiftMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportShiftMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportShiftMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Payroll Group Master"

    Public Function ExportPayrollGroupMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objPayrollGrp As New clspayrollgroup_master
            dsList = objPayrollGrp.GetList("PayrollGrp", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("PayrollGrp").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End


            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("groupmasterunkid")
                dsList.Tables(0).Columns.Remove("grouptype_id")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("groupname1")
                dsList.Tables(0).Columns.Remove("groupname2")
                'Pinkal (18-Aug-2013) -- End

            End If

             ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportPayrollGroupMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportPayrollGroupMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportPayrollGroupMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Cost Center Master"

    Public Function ExportCostCenterMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objCostCenter As New clscostcenter_master
            dsList = objCostCenter.GetList("CostCenter", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("CostCenter").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("costcenterunkid")
                dsList.Tables(0).Columns.Remove("costcentergroupmasterunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("costcentername1")
                dsList.Tables(0).Columns.Remove("costcentername2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportCostCenterMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportCostCenterMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportCostCenterMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Pay Point Master"

    Public Function ExportPayPointMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objPayPoint As New clspaypoint_master
            dsList = objPayPoint.GetList("PayPoint", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("PayPoint").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("paypointunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("paypointname1")
                dsList.Tables(0).Columns.Remove("paypointname2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportPayPointMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportPayPointMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportPayPointMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Transaction Head Master"

    Public Function ExportTransactionHeadMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objTransactionHead As New clsTransactionHead
            dsList = objTransactionHead.GetList("TransactionHead", , , True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("TransactionHead").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("tranheadunkid")
                dsList.Tables(0).Columns.Remove("trnheadtype_id")
                dsList.Tables(0).Columns.Remove("typeof_id")
                dsList.Tables(0).Columns.Remove("calctype_id")
                dsList.Tables(0).Columns.Remove("computeon_id")
                dsList.Tables(0).Columns.Remove("formulaid")
                dsList.Tables(0).Columns.Remove("userunkid")
                dsList.Tables(0).Columns.Remove("voiduserunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportTransactionHeadMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ExportTransactionHead_formula(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objTranHeadformula As New clsTranheadFormulaTran
            dsList = objTranHeadformula.GetList(-1, "Tranheadformula")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Tranheadformula").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportTransactionHead_formula", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ExportTransactionHead_Slab(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objTranHeadslab As New clsTranheadSlabTran
            dsList = objTranHeadslab.GetList("TranHeadSlab")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("TranHeadSlab").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportTransactionHead_Slab", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ExportTransactionHead_InexcessSlab(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objTranHeadInexcess As New clsTranheadInexcessslabTran
            dsList = objTranHeadInexcess.GetList("TranHeadInexcess")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("TranHeadInexcess").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportTransactionHead_InexcessSlab", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportTransactionHeadMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportTransactionHeadMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Wages Master"

    Public Function ExportWagesMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet

        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                "  wagesunkid " & _
                ", gradegroupunkid " & _
                ", userunkid " & _
                ", isvoid " & _
                ", voiduserunkid " & _
                ", voiddatetime " & _
                ", voidreason " & _
                " FROM prwages_master "

            dsList = objDataOperation.ExecQuery(strQ, "Wages")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Wages").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportWagesMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ExportWages_Tran(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet

        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
              "  wagestranunkid " & _
              ", wagesunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", salary " & _
              ", increment " & _
              ", maximum " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              " FROM prwages_tran "

            dsList = objDataOperation.ExecQuery(strQ, "WagesTran")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("WagesTran").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportWages_Tran", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportWagesMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportWagesMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Employee Master"

    Public Function ExportEmployeeMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetList("Employee", True)
            dsList = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                         "Employee", ConfigParameter._Object._ShowFirstAppointmentDate)
            'S.SANDEEP [04 JUN 2015] -- END



            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Employee").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("employeeunkid")
                dsList.Tables(0).Columns.Remove("titleunkid")
                dsList.Tables(0).Columns.Remove("gender")
                dsList.Tables(0).Columns.Remove("employmenttypeunkid")
                dsList.Tables(0).Columns.Remove("paytypeunkid")
                dsList.Tables(0).Columns.Remove("paypointunkid")
                dsList.Tables(0).Columns.Remove("shiftunkid")
                dsList.Tables(0).Columns.Remove("birthstateunkid")
                dsList.Tables(0).Columns.Remove("birthcountryunkid")
                dsList.Tables(0).Columns.Remove("birthcityunkid")
                dsList.Tables(0).Columns.Remove("workcountryunkid")
                dsList.Tables(0).Columns.Remove("complexionunkid")
                dsList.Tables(0).Columns.Remove("bloodgroupunkid")
                dsList.Tables(0).Columns.Remove("eyecolorunkid")
                dsList.Tables(0).Columns.Remove("nationalityunkid")
                dsList.Tables(0).Columns.Remove("ethnicityunkid")
                dsList.Tables(0).Columns.Remove("religionunkid")
                dsList.Tables(0).Columns.Remove("hairunkid")
                dsList.Tables(0).Columns.Remove("language1unkid")
                dsList.Tables(0).Columns.Remove("language2unkid")
                dsList.Tables(0).Columns.Remove("language3unkid")
                dsList.Tables(0).Columns.Remove("language4unkid")
                dsList.Tables(0).Columns.Remove("maritalstatusunkid")
                dsList.Tables(0).Columns.Remove("present_countryunkid")
                dsList.Tables(0).Columns.Remove("present_postcodeunkid")
                dsList.Tables(0).Columns.Remove("present_stateunkid")
                dsList.Tables(0).Columns.Remove("present_post_townunkid")
                dsList.Tables(0).Columns.Remove("domicile_countryunkid")
                dsList.Tables(0).Columns.Remove("domicile_postcodeunkid")
                dsList.Tables(0).Columns.Remove("domicile_stateunkid")
                dsList.Tables(0).Columns.Remove("domicile_post_townunkid")
                dsList.Tables(0).Columns.Remove("emer_con_countryunkid")
                dsList.Tables(0).Columns.Remove("emer_con_postcodeunkid")
                dsList.Tables(0).Columns.Remove("emer_con_state")
                dsList.Tables(0).Columns.Remove("emer_con_post_townunkid")
                dsList.Tables(0).Columns.Remove("stationunkid")
                dsList.Tables(0).Columns.Remove("deptgroupunkid")
                dsList.Tables(0).Columns.Remove("departmentunkid")
                dsList.Tables(0).Columns.Remove("sectionunkid")
                dsList.Tables(0).Columns.Remove("unitunkid")
                dsList.Tables(0).Columns.Remove("jobgroupunkid")
                dsList.Tables(0).Columns.Remove("jobunkid")
                dsList.Tables(0).Columns.Remove("gradegroupunkid")
                dsList.Tables(0).Columns.Remove("gradeunkid")
                dsList.Tables(0).Columns.Remove("gradelevelunkid")
                dsList.Tables(0).Columns.Remove("accessunkid")
                dsList.Tables(0).Columns.Remove("classgroupunkid")
                dsList.Tables(0).Columns.Remove("classunkid")
                dsList.Tables(0).Columns.Remove("serviceunkid")
                dsList.Tables(0).Columns.Remove("costcenterunkid")
                dsList.Tables(0).Columns.Remove("tranhedunkid")
                dsList.Tables(0).Columns.Remove("actionreasonunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportEmployeeMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    'Public Function ExportEmployee_Allergies(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objEmployee As New clsEmployee_Master
    '        dsList = objEmployee.GetAllergiesList("EmployeeAllergies")


    '        'Pinkal (12-Oct-2011) -- Start
    '        'Enhancement : TRA Changes
    '        'If dsList.Tables("EmployeeAllergies").Rows.Count = 0 Then Return True
    '        'Pinkal (12-Oct-2011) -- End



    '        ExportType(strFilePath, FileType, dsList)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportEmployee_Allergies", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    'Public Function ExportEmployee_Disability(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objEmployee As New clsEmployee_Master
    '        dsList = objEmployee.GetDisabilitesList("EmployeeDisability")


    '        'Pinkal (12-Oct-2011) -- Start
    '        'Enhancement : TRA Changes
    '        'If dsList.Tables("EmployeeDisability").Rows.Count = 0 Then Return True
    '        'Pinkal (12-Oct-2011) -- End



    '        ExportType(strFilePath, FileType, dsList)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportEmployee_Disability", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    'Public Function ExportEmployee_Idinfo_tran(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objEmployee As New clsIdentity_tran
    '        dsList = objEmployee.GetIdentity_tranList("EmployeeIdInfo")


    '        'Pinkal (12-Oct-2011) -- Start
    '        'Enhancement : TRA Changes
    '        'If dsList.Tables("EmployeeIdInfo").Rows.Count = 0 Then Return True
    '        'Pinkal (12-Oct-2011) -- End



    '        ExportType(strFilePath, FileType, dsList)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportEmployee_Idinfo_tran", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    'Public Function ExportEmployee_Meminfo_tran(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objEmployee As New clsMembershipTran
    '        dsList = objEmployee.GetMembership_TranList("EmployeeMemInfo")


    '        'Pinkal (12-Oct-2011) -- Start
    '        'Enhancement : TRA Changes
    '        'If dsList.Tables("EmployeeMemInfo").Rows.Count = 0 Then Return True
    '        'Pinkal (12-Oct-2011) -- End



    '        ExportType(strFilePath, FileType, dsList)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportEmployee_Meminfo_tran", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    'Pinkal (12-Oct-2011) -- End

    Public Function ImportEmployeeMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportEmployeeMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Dependants Master"

    Public Function ExportDependantsMaster(ByVal strFilePath As String, ByVal FileType As Integer _
                                           , ByVal xDatabaseName As String _
                                           , ByVal xUserunkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyunkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserAccessModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeInactiveEmp As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           ) As Boolean
        'Sohail (18 May 2019) - [xDatabaseName, xUserunkid, xYearUnkid, xCompanyunkid, xPeriodStart, xPeriodEnd, xUserAccessModeSetting, xOnlyApproved, xIncludeInactiveEmp, blnApplyUserAccessFilter]

        Dim dsList As New DataSet
        Try
            Dim objDependants As New clsDependants_Beneficiary_tran
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objDependants.GetList("Dependants")
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'dsList = objDependants.GetList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, True, True, "Dependants", False)
            dsList = objDependants.GetList(xDatabaseName, _
                                           xUserunkid, _
                                           xYearUnkid, _
                                           xCompanyunkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserAccessModeSetting, xOnlyApproved, xIncludeInactiveEmp, "Dependants", blnApplyUserAccessFilter, , , , , xPeriodEnd)
            'Sohail (18 May 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Dependants").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("RelationId")
                dsList.Tables(0).Columns.Remove("CountryId")
                dsList.Tables(0).Columns.Remove("EmpId")
                dsList.Tables(0).Columns.Remove("DpndtTranId")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDependantsMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ExportDependants_Benefit_Tran(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objBenefit_tran As New clsDependants_Benefit_tran
            dsList = objBenefit_tran.GetList("BenefitTran")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("BenefitTran").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End


            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDependants_Benefit_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ExportDependants_Membership_Tran(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objMembership_tran As New clsDependants_Membership_tran
            dsList = objMembership_tran.GetList("MembershipTran")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("MembershipTran").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDependants_Membership_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDependantsMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDependantsMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Employee Referee Master"

    Public Function ExportEmployee_Referee_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objEmployeeRefree As New clsEmployee_Refree_tran
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployeeRefree.GetList("EmployeeRefree", True)
            dsList = objEmployeeRefree.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeRefree", True)
            'S.SANDEEP [04 JUN 2015] -- END



            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("EmployeeRefree").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("EmpId")
                dsList.Tables(0).Columns.Remove("CountryId")
                dsList.Tables(0).Columns.Remove("RefreeTranId")
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("userunkid")
                dsList.Tables(0).Columns.Remove("voiduserunkid")
                dsList.Tables(0).Columns.Remove("relationunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportEmployee_Referee_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportEmployee_Referee_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportEmployee_Referee_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Employee Assest Master"

    Public Function ExportEmployee_Assest_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objEmployeeAssest As New clsEmployee_Assets_Tran

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployeeAssest.GetList("EmployeeAssest")
            dsList = objEmployeeAssest.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeAssest")
            'S.SANDEEP [04 JUN 2015] -- END



            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("EmployeeAssest").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("EmpId")
                dsList.Tables(0).Columns.Remove("AssetId")
                dsList.Tables(0).Columns.Remove("CondId")
                dsList.Tables(0).Columns.Remove("StatusId")
                dsList.Tables(0).Columns.Remove("AssetTranUnkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportEmployee_Assest_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportEmployee_Assest_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportEmployee_Assest_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Disciplinary Action Master"

    Public Function ExportDisciplinary_Action_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objAction As New clsAction_Reason
            dsList = objAction.GetList("Action", True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Action").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("actionreasonunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDisciplinary_Action_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDisciplinary_Action_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDisciplinary_Action_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Discipline Type Master"

    Public Function ExportDiscipline_Type_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objtype As New clsDisciplineType
            dsList = objtype.GetList("DisciplineType", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("DisciplineType").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("disciplinetypeunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("offencecategoryunkid")
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDiscipline_Type_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDiscipline_Type_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDiscipline_Type_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Discipline Status Master"

    Public Function ExportDiscipline_Status_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objStatus As New clsDiscipline_Status
            dsList = objStatus.GetList("DisciplineStatus", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("DisciplineStatus").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("disciplinestatusunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("statusmodeid")
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDiscipline_Status_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDiscipline_Status_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDiscipline_Status_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Payroll Period Master"

    Public Function ExportPayroll_Period_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                      "  periodunkid " & _
                      ", period_code " & _
                      ", period_name " & _
                      ", cfcommon_period_tran.yearunkid " & _
               ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') as year " & _
                      ", modulerefid " & _
               ", convert(char(8),cfcommon_period_tran.start_date,112) as start_date " & _
               ", convert(char(8),cfcommon_period_tran.end_date,112) as end_date " & _
                      ", total_days " & _
                      ", description " & _
               ", statusid " & _
                      ", case when statusid  = 1 then '" & StatusType.Open.ToString() & "' when statusid = 2 then '" & StatusType.Close.ToString() & "' End status " & _
                      ", isactive " & _
                     "FROM cfcommon_period_tran " & _
              " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
              "WHERE cfcommon_period_tran.yearunkid = " & FinancialYear._Object._YearUnkid

            dsList = objDataOperation.ExecQuery(strQ, "Period")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Period").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportPayroll_Period_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportPayroll_Period_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportPayroll_Period_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Assessment Group Master"

    Public Function ExportAssessment_Group_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objAssessmentGrp As New clsassess_group_master
            dsList = objAssessmentGrp.GetList("AssessmentGrp", False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("AssessmentGrp").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("assessgroupunkid")
                dsList.Tables(0).Columns.Remove("stationunkid")
                dsList.Tables(0).Columns.Remove("departmentunkid")
                dsList.Tables(0).Columns.Remove("sectionunkid")
                dsList.Tables(0).Columns.Remove("unitunkid")
                dsList.Tables(0).Columns.Remove("jobgroupunkid")
                dsList.Tables(0).Columns.Remove("jobunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportAssessment_Group_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportAssessment_Group_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportAssessment_Group_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Assessment Item Master"

    Public Function ExportAssessment_Item_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objAssessmentItem As New clsassess_item_master
            dsList = objAssessmentItem.GetList("AssessmentItem", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("AssessmentItem").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("assessitemunkid")
                dsList.Tables(0).Columns.Remove("assessgroupunkid")
                dsList.Tables(0).Columns.Remove("resultgroupunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportAssessment_Item_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportAssessment_Item_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportAssessment_Item_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Bank Branch Master"

    Public Function ExportBank_Branch_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objBankBranch As New clsbankbranch_master
            dsList = objBankBranch.GetList("BankBranch", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("BankBranch").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End




            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("branchunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("pincodeunkid")
                dsList.Tables(0).Columns.Remove("bankgroupunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportBank_Branch_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportBank_Branch_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportBank_Branch_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Bank Account Type Master"

    Public Function ExportBank_Account_Type_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objBankAccType As New clsBankAccType
            dsList = objBankAccType.GetList("BankAc", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("BankAc").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("accounttypeunkid")


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("accounttype_name1")
                dsList.Tables(0).Columns.Remove("accounttype_name2")
                'Pinkal (18-Aug-2013) -- End


            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportBank_Account_Type_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportBank_Account_Type_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportBank_Account_Type_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Currency Master"

    Public Function ExportCurrency_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.GetList("Currency", False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Currency").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("exchangerateunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportCurrency_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportCurrency_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportCurrency_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Denomination Master"

    Public Function ExportDenomination_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objDenomination As New clsDenomination
            dsList = objDenomination.GetList("Denomination", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Denomination").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("denomunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("countryunkid")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportDenomination_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportDenomination_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportDenomination_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Medical Master"

    Public Function ExportMedical_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objMedical As New clsmedical_master
            dsList = objMedical.GetList("Medical", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Medical").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (18-Aug-2013) -- Start
            'Enhancement : TRA Changes
            dsList.Tables(0).Columns.Remove("mdmasterunkid")
            dsList.Tables(0).Columns.Remove("mdmastertype_id")
            dsList.Tables(0).Columns.Remove("mdmastername1")
            dsList.Tables(0).Columns.Remove("mdmastername2")
            'Pinkal (18-Aug-2013) -- End


            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportMedical_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportMedical_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportMedical_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Service Provider Master"

    Public Function ExportService_Provider_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objInstitute As New clsinstitute_master
            dsList = objInstitute.GetList("Service", True, False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Service").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("instituteunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("issync_recruit")
                dsList.Tables(0).Columns.Remove("ishospital")
                'Pinkal (18-Aug-2013) -- End

            End If

           ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportService_Provider_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportService_Provider_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportService_Provider_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Assign  Medical Category Master"

    Public Function ExportMedical_Category_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objCategory As New clsmedical_category_master
            dsList = objCategory.GetList("Category", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Category").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("medicalcategoryunkid")
                dsList.Tables(0).Columns.Remove("mdcategorymasterunkid")
            End If

             ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportMedical_Category_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportMedical_Category_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportMedical_Category_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Employee Injury Master"


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function ExportEmployee_Injury_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    Public Function ExportEmployee_Injury_Master(ByVal strDatabaseName As String, _
                                                 ByVal intUserUnkid As Integer, _
                                                 ByVal intYearUnkid As Integer, _
                                                 ByVal intCompanyUnkid As Integer, _
                                                 ByVal dtPeriodStart As Date, _
                                                 ByVal dtPeriodEnd As Date, _
                                                 ByVal strUserModeSetting As String, _
                                                 ByVal blnOnlyApproved As Boolean, _
                                                 ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                 ByVal strFilePath As String, _
                                                 ByVal FileType As Integer) As Boolean
        'Shani(24-Aug-2015) -- End
        Dim dsList As New DataSet
        Try
            Dim objInjury As New clsmedical_injury_master

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objInjury.GetList("Injury", False)
            dsList = objInjury.GetList(strDatabaseName, _
                                       intUserUnkid, _
                                       intYearUnkid, _
                                       intCompanyUnkid, _
                                       dtPeriodStart, _
                                       dtPeriodEnd, _
                                       strUserModeSetting, _
                                       blnOnlyApproved, _
                                       xIncludeIn_ActiveEmployee, _
                                       "Injury", False)
            'Shani(24-Aug-2015) -- End


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Injury").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End




            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("injuryunkid")
                dsList.Tables(0).Columns.Remove("yearunkid")
                dsList.Tables(0).Columns.Remove("periodunkid")
                dsList.Tables(0).Columns.Remove("employeeunkid")
                dsList.Tables(0).Columns.Remove("userunkid")
                dsList.Tables(0).Columns.Remove("voiduserunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("isvoid")
                dsList.Tables(0).Columns.Remove("voiddatetime")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportEmployee_Injury_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportEmployee_Injury_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportEmployee_Injury_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Leave Type Master"

    Public Function ExportLeave_Type_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objLeaveType As New clsleavetype_master
            dsList = objLeaveType.GetList("LeaveType", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("LeaveType").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("leavetypeunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("deductfromlvtypeunkid")
                dsList.Tables(0).Columns.Remove("leavename1")
                dsList.Tables(0).Columns.Remove("leavename2")
                dsList.Tables(0).Columns.Remove("gender")
                dsList.Tables(0).Columns.Remove("color")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportLeave_Type_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportLeave_Type_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportLeave_Type_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Holiday Master"

    Public Function ExportHoliday_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objHoliday As New clsholiday_master
            dsList = objHoliday.GetList("Holiday", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Holiday").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("holidayunkid")
                dsList.Tables(0).Columns.Remove("yearunkid")
                dsList.Tables(0).Columns.Remove("periodunkid")
                dsList.Tables(0).Columns.Remove("holidaytypeid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("color")
                dsList.Tables(0).Columns.Remove("holidayname1")
                dsList.Tables(0).Columns.Remove("holidayname2")
                'Pinkal (18-Aug-2013) -- End

            End If


            'Pinkal (18-Aug-2013) -- Start
            'Enhancement : TRA Changes

            For Each dr As DataRow In dsList.Tables(0).Rows
                dr("holidaydate") = eZeeDate.convertDate(dr("holidaydate"))
            Next

            'Pinkal (18-Aug-2013) -- End

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportHoliday_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportHoliday_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportHoliday_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Approver Level Master"

    Public Function ExportApprover_Level_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objApprover As New clsapproverlevel_master
            dsList = objApprover.GetList("ApproverLevel", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("ApproverLevel").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("levelunkid")


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("levelname1")
                dsList.Tables(0).Columns.Remove("levelname2")
                'Pinkal (18-Aug-2013) -- End


            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportApprover_Level_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportApprover_Level_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportApprover_Level_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Leave Planner Master"


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function ExportLeave_Planner_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objPlanner As New clsleaveplanner
    '        dsList = objPlanner.GetList("Planner", False)


    '        If dsList IsNot Nothing Then
    '            dsList.Tables(0).Columns.Remove("leaveplannerunkid")
    '            dsList.Tables(0).Columns.Remove("analysisrefid")
    '            dsList.Tables(0).Columns.Remove("employeeunkid")
    '            dsList.Tables(0).Columns.Remove("leavetypeunkid")
    '            dsList.Tables(0).Columns.Remove("statusname")
    '            dsList.Tables(0).Columns.Remove("voiduserunkid")
    '        End If

    '        ExportType(strFilePath, FileType, dsList)


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportLeave_Planner_Master", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    Public Function ExportLeave_Planner_Master(ByVal strFilePath As String, ByVal FileType As Integer, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                   , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                   , ByVal mdtEmployeeAsOnDate As Date _
                                                                   , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                   , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                                   , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                   , Optional ByVal blnOnlyActive As Boolean = True) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objPlanner As New clsleaveplanner
            dsList = objPlanner.GetList("Planner", xDatabaseName, xUserUnkid, xYearUnkid _
                                                    , xCompanyUnkid, mdtEmployeeAsOnDate, xUserModeSetting _
                                                    , xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, blnOnlyActive, "")

            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("leaveplannerunkid")
                dsList.Tables(0).Columns.Remove("analysisrefid")
                dsList.Tables(0).Columns.Remove("employeeunkid")
                dsList.Tables(0).Columns.Remove("leavetypeunkid")
                dsList.Tables(0).Columns.Remove("statusname")
                dsList.Tables(0).Columns.Remove("voiduserunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportLeave_Planner_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (24-Aug-2015) -- End

   

    Public Function ImportLeave_Planner_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportLeave_Planner_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Leave Accrue Master"


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function ExportLeave_Accrue_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objAccrue As New clsleavebalance_tran
    '        dsList = objAccrue.GetList("Accrue", False, True)


    '        'Pinkal (12-Oct-2011) -- Start
    '        'Enhancement : TRA Changes
    '        'If dsList.Tables("Accrue").Rows.Count = 0 Then Return True
    '        'Pinkal (12-Oct-2011) -- End



    '        If dsList IsNot Nothing Then
    '            dsList.Tables(0).Columns.Remove("leavebalanceunkid")
    '            dsList.Tables(0).Columns.Remove("employeeunkid")
    '            dsList.Tables(0).Columns.Remove("yearunkid")
    '            dsList.Tables(0).Columns.Remove("leavetypeunkid")
    '            dsList.Tables(0).Columns.Remove("batchunkid")
    '            dsList.Tables(0).Columns.Remove("userunkid")
    '            dsList.Tables(0).Columns.Remove("voiduserunkid")
    '        End If

    '        ExportType(strFilePath, FileType, dsList)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportLeave_Accrue_Master", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    Public Function ExportLeave_Accrue_Master(ByVal strFilePath As String, ByVal FileType As Integer, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                     , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True _
                                                                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objAccrue As New clsleavebalance_tran


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsList = objAccrue.GetList("Accrue", False, True)
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objAccrue.GetList("Accrue", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                        , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                                        , blnApplyUserAccessFilter, False, -1, False, False, False, "", Nothing)
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objAccrue.GetList("Accrue", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                        , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                                        , blnApplyUserAccessFilter, False, -1, True, True, False, "", Nothing)
            End If
            'Pinkal (24-Aug-2015) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("leavebalanceunkid")
                dsList.Tables(0).Columns.Remove("employeeunkid")
                dsList.Tables(0).Columns.Remove("yearunkid")
                dsList.Tables(0).Columns.Remove("leavetypeunkid")
                dsList.Tables(0).Columns.Remove("batchunkid")
                dsList.Tables(0).Columns.Remove("userunkid")
                dsList.Tables(0).Columns.Remove("voiduserunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportLeave_Accrue_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (24-Aug-2015) -- End

    Public Function ImportLeave_Accrue_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportLeave_Accrue_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Loan Scheme Master"

    Public Function ExportLoan_Scheme_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objScheme As New clsLoan_Scheme
            dsList = objScheme.GetList("Scheme", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Scheme").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("loanschemeunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportLoan_Scheme_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportLoan_Scheme_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportLoan_Scheme_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Saving Scheme Master"

    Public Function ExportSaving_Scheme_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objScheme As New clsSavingScheme
            dsList = objScheme.GetList("Scheme", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Scheme").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("savingschemeunkid")
                dsList.Tables(0).Columns.Remove("userunkid")
                dsList.Tables(0).Columns.Remove("voiduserunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportSaving_Scheme_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportSaving_Scheme_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportSaving_Scheme_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Applicant Master"

    Public Function ExportApplicant_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objApplicant As New clsApplicant_master
            dsList = objApplicant.GetList("Applicant", False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Applicant").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("applicantunkid")
                dsList.Tables(0).Columns.Remove("titleunkid")
                dsList.Tables(0).Columns.Remove("gender")
                dsList.Tables(0).Columns.Remove("present_countryunkid")
                dsList.Tables(0).Columns.Remove("present_stateunkid")
                dsList.Tables(0).Columns.Remove("present_post_townunkid")
                dsList.Tables(0).Columns.Remove("present_zipcode")
                dsList.Tables(0).Columns.Remove("perm_countryunkid")
                dsList.Tables(0).Columns.Remove("perm_stateunkid")
                dsList.Tables(0).Columns.Remove("perm_post_townunkid")
                dsList.Tables(0).Columns.Remove("perm_zipcode")
                dsList.Tables(0).Columns.Remove("marital_statusunkid")
                dsList.Tables(0).Columns.Remove("language1unkid")
                dsList.Tables(0).Columns.Remove("language2unkid")
                dsList.Tables(0).Columns.Remove("language3unkid")
                dsList.Tables(0).Columns.Remove("language4unkid")
                dsList.Tables(0).Columns.Remove("nationality")
                dsList.Tables(0).Columns.Remove("vacancyunkid")
                dsList.Tables(0).Columns.Remove("userunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportApplicant_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportApplicant_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportApplicant_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Vacancy Master"

    Public Function ExportVacancy_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objVacancy As New clsVacancy
            dsList = objVacancy.GetList("Vacancy", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Vacancy").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("vacancyunkid")
                dsList.Tables(0).Columns.Remove("stationunkid")
                dsList.Tables(0).Columns.Remove("deptgroupunkid")
                dsList.Tables(0).Columns.Remove("departmentunkid")
                dsList.Tables(0).Columns.Remove("sectionunkid")
                dsList.Tables(0).Columns.Remove("unitunkid")
                dsList.Tables(0).Columns.Remove("jobgroupunkid")
                dsList.Tables(0).Columns.Remove("jobunkid")
                dsList.Tables(0).Columns.Remove("employeementtypeunkid")
                dsList.Tables(0).Columns.Remove("paytypeunkid")
                dsList.Tables(0).Columns.Remove("shifttypeunkid")
                dsList.Tables(0).Columns.Remove("userunkid")
                dsList.Tables(0).Columns.Remove("voiduserunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportVacancy_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportVacancy_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportVacancy_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Training Provider Master"

    Public Function ExportTraining_Provider_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objProvider As New clsinstitute_master
            dsList = objProvider.GetList("Provider", False, False, True)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Provider").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("instituteunkid")
                dsList.Tables(0).Columns.Remove("stateunkid")
                dsList.Tables(0).Columns.Remove("cityunkid")
                dsList.Tables(0).Columns.Remove("countryunkid")


                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("ishospital")
                dsList.Tables(0).Columns.Remove("isformrequire")
                dsList.Tables(0).Columns.Remove("islocalprovider")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportTraining_Provider_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportTraining_Provider_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportTraining_Provider_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Letter Type Master"

    Public Function ExportLetter_Type_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objLetter As New clsLetterType
            dsList = objLetter.getList(clsCommon_Master.enCommonMaster.LETTER_TYPE, "LetterType")


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("LetterType").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("lettertypeunkid")
                dsList.Tables(0).Columns.Remove("lettermasterunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportLetter_Type_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportLetter_Type_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportLetter_Type_Master", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Reminder Master"

    Public Function ExportReminder_Master(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objReminder As New clsreminder_master
            dsList = objReminder.GetList("Reminder", False)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'If dsList.Tables("Reminder").Rows.Count = 0 Then Return True
            'Pinkal (12-Oct-2011) -- End



            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("reminderunkid")
                dsList.Tables(0).Columns.Remove("reminder_typeid")
                dsList.Tables(0).Columns.Remove("userunkid")
            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportReminder_Master", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportReminder_Master() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportReminder_Master", mstrModuleName)
        End Try

    End Function

#End Region

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region "Section Group Master"

    Public Function ExportSectionGrpMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objSGroup As New clsSectionGroup
            dsList = objSGroup.GetList("SGRP", False)

            If dsList.Tables("SGRP").Rows.Count = 0 Then Return True

            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("sectiongroupunkid")

                'Pinkal (18-Aug-2013) -- Start
                'Enhancement : TRA Changes
                dsList.Tables(0).Columns.Remove("name1")
                dsList.Tables(0).Columns.Remove("name2")
                'Pinkal (18-Aug-2013) -- End

            End If

            ExportType(strFilePath, FileType, dsList)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportSectionMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportSectionGrpMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportSectionMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Unit Master"

    Public Function ExportUnitGrpMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objUGrp As New clsUnitGroup
            dsList = objUGrp.GetList("Unit", False)

            If dsList.Tables("Unit").Rows.Count = 0 Then Return True

            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("sectionunkid")
                dsList.Tables(0).Columns.Remove("unitgroupunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportUnitGrpMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportUnitGrpMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportUnitGrpMaster", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Team Master"

    Public Function ExportTeamMaster(ByVal strFilePath As String, ByVal FileType As Integer) As Boolean
        Dim dsList As New DataSet
        Try
            Dim objTeam As New clsTeams
            dsList = objTeam.GetList("Team", False)

            If dsList.Tables("Team").Rows.Count = 0 Then Return True

            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Remove("unitunkid")
                dsList.Tables(0).Columns.Remove("teamunkid")
            End If

            ExportType(strFilePath, FileType, dsList)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportTeamMaster", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function ImportTeamMaster() As Boolean

        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportTeamMaster", mstrModuleName)
        End Try

    End Function

#End Region
    'S.SANDEEP [ 07 NOV 2011 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "ADVERTISE CATEGORY")
			Language.setMessage(mstrModuleName, 2, "ALLERGIES")
			Language.setMessage(mstrModuleName, 3, "ASSET CONDITION")
			Language.setMessage(mstrModuleName, 4, "BENEFIT GROUP")
			Language.setMessage(mstrModuleName, 5, "BLOOD GROUP")
			Language.setMessage(mstrModuleName, 6, "COMPLEXION")
			Language.setMessage(mstrModuleName, 7, "DISABILITIES")
			Language.setMessage(mstrModuleName, 8, "EMPLOYEMENT TYPE")
			Language.setMessage(mstrModuleName, 9, "ETHNICITY")
			Language.setMessage(mstrModuleName, 10, "EYES COLOR")
			Language.setMessage(mstrModuleName, 11, "HAIR COLOR")
			Language.setMessage(mstrModuleName, 12, "IDENTITY TYPES")
			Language.setMessage(mstrModuleName, 13, "INTERVIEW TYPE")
			Language.setMessage(mstrModuleName, 14, "LANGUAGES")
			Language.setMessage(mstrModuleName, 15, "MARRIED STATUS")
			Language.setMessage(mstrModuleName, 16, "MEMBERSHIP CATEGORY")
			Language.setMessage(mstrModuleName, 17, "PAY TYPE")
			Language.setMessage(mstrModuleName, 18, "QUALIFICATION COURSE GROUP")
			Language.setMessage(mstrModuleName, 19, "RELATIONS")
			Language.setMessage(mstrModuleName, 20, "RELIGION")
			Language.setMessage(mstrModuleName, 21, "RESULT GROUP")
			Language.setMessage(mstrModuleName, 22, "SHIFT TYPE")
			Language.setMessage(mstrModuleName, 23, "SKILL CATEGORY")
			Language.setMessage(mstrModuleName, 24, "TITLE")
			Language.setMessage(mstrModuleName, 25, "TRAINING RESOURCES")
			Language.setMessage(mstrModuleName, 26, "ASSET STATUS")
			Language.setMessage(mstrModuleName, 27, "ASSETS")
			Language.setMessage(mstrModuleName, 28, "LETTER TYPE")
			Language.setMessage(mstrModuleName, 29, "SALINC REASON")
			Language.setMessage(mstrModuleName, 30, "TRAINING COURSE MASTER")
			Language.setMessage(mstrModuleName, 31, "STRATEGIC GOAL")
			Language.setMessage(mstrModuleName, 32, "SOURCES FUNDINGS")
			Language.setMessage(mstrModuleName, 33, "ATTACHMENT TYPES")
			Language.setMessage(mstrModuleName, 34, "VACANCY MASTER")
			Language.setMessage(mstrModuleName, 35, "DISCIPLINE COMMITTEE")
			Language.setMessage(mstrModuleName, 36, "OFFENCE CATEGORY")
			Language.setMessage(mstrModuleName, 37, "COMMITTEE MEMBERS CATEGORY")
			Language.setMessage(mstrModuleName, 38, "JOB CAPABILITY")
			Language.setMessage(mstrModuleName, 39, "CAREER DEVELOPMENT")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
