﻿Imports System
Imports System.Runtime.InteropServices

Public Class CHCNetSDK

#Region "HCNetSDK.dll macro definition"
    Public Const NET_DVR_GET_ACS_EVENT As Integer = 2514 '设备事件获取
    Public Const NET_DVR_DEV_ADDRESS_MAX_LEN As Integer = 129 'device address max length
    Public Const NET_DVR_LOGIN_USERNAME_MAX_LEN As Integer = 64 'login username max length
    Public Const NET_DVR_LOGIN_PASSWD_MAX_LEN As Integer = 64 'login password max length
    Public Const SERIALNO_LEN As Integer = 48 'serial number length
    Public Const NET_DVR_PASSWORD_ERROR As Integer = 1 'Username or Password error
    Public Const NET_DVR_USER_LOCKED As Integer = 153

    Public Const ACS_CARD_NO_LEN As Integer = 32
    Public Const MACADDR_LEN As Integer = 6
    Public Const MAX_NAMELEN As Integer = 16
    Public Const NAME_LEN As Integer = 32

    Public Const NET_SDK_GET_NEXT_STATUS_SUCCESS As Integer = 1000
    Public Const NET_SDK_GET_NEXT_STATUS_NEED_WAIT As Integer = 1001
    Public Const NET_SDK_GET_NEXT_STATUS_FINISH As Integer = 1002
    Public Const NET_SDK_GET_NEXT_STATUS_FAILED As Integer = 1003


    Public Const NET_SDK_MONITOR_ID_LEN As Integer = 64
    Public Const NET_SDK_EMPLOYEE_NO_LEN As Integer = 32
#End Region

#Region "acs event upload"

    Public Const COMM_ALARM_ACS As Integer = &H5002 'access card alarm

    ' Alarm 
    ' Main Type
    Public Const MAJOR_ALARM As Integer = &H1
    ' Hypo- Type
    Public Const MINOR_ALARMIN_SHORT_CIRCUIT As Integer = &H400 ' region short circuit
    Public Const MINOR_ALARMIN_BROKEN_CIRCUIT As Integer = &H401 ' region broken circuit
    Public Const MINOR_ALARMIN_EXCEPTION As Integer = &H402 ' region exception
    Public Const MINOR_ALARMIN_RESUME As Integer = &H403 ' region resume
    Public Const MINOR_HOST_DESMANTLE_ALARM As Integer = &H404 ' host desmantle alarm
    Public Const MINOR_HOST_DESMANTLE_RESUME As Integer = &H405 '  host desmantle resume
    Public Const MINOR_CARD_READER_DESMANTLE_ALARM As Integer = &H406 ' card reader desmantle alarm
    Public Const MINOR_CARD_READER_DESMANTLE_RESUME As Integer = &H407 ' card reader desmantle resume
    Public Const MINOR_CASE_SENSOR_ALARM As Integer = &H408 ' case sensor alarm
    Public Const MINOR_CASE_SENSOR_RESUME As Integer = &H409 ' case sensor resume
    Public Const MINOR_STRESS_ALARM As Integer = &H40A ' stress alarm
    Public Const MINOR_OFFLINE_ECENT_NEARLY_FULL As Integer = &H40B ' offline ecent nearly full
    Public Const MINOR_CARD_MAX_AUTHENTICATE_FAIL As Integer = &H40C ' card max authenticate fall
    Public Const MINOR_SD_CARD_FULL As Integer = &H40D ' SD card is full
    Public Const MINOR_LINKAGE_CAPTURE_PIC As Integer = &H40E ' lingage capture picture
    Public Const MINOR_SECURITY_MODULE_DESMANTLE_ALARM As Integer = &H40F 'Door control security module desmantle alarm
    Public Const MINOR_SECURITY_MODULE_DESMANTLE_RESUME As Integer = &H410 'Door control security module desmantle resume
    Public Const MINOR_POS_START_ALARM As Integer = &H411 ' POS Start
    Public Const MINOR_POS_END_ALARM As Integer = &H412 ' POS end
    Public Const MINOR_FACE_IMAGE_QUALITY_LOW As Integer = &H413 ' face image quality low
    Public Const MINOR_FINGE_RPRINT_QUALITY_LOW As Integer = &H414 ' finger print quality low
    Public Const MINOR_FIRE_IMPORT_SHORT_CIRCUIT As Integer = &H415 ' Fire import short circuit
    Public Const MINOR_FIRE_IMPORT_BROKEN_CIRCUIT As Integer = &H416 ' Fire import broken circuit
    Public Const MINOR_FIRE_IMPORT_RESUME As Integer = &H417 ' Fire import resume
    Public Const MINOR_FIRE_BUTTON_TRIGGER As Integer = &H418 ' fire button trigger
    Public Const MINOR_FIRE_BUTTON_RESUME As Integer = &H419 ' fire button resume
    Public Const MINOR_MAINTENANCE_BUTTON_TRIGGER As Integer = &H41A ' maintenance button trigger
    Public Const MINOR_MAINTENANCE_BUTTON_RESUME As Integer = &H41B ' maintenance button resume
    Public Const MINOR_EMERGENCY_BUTTON_TRIGGER As Integer = &H41C ' emergency button trigger
    Public Const MINOR_EMERGENCY_BUTTON_RESUME As Integer = &H41D ' emergency button resume
    Public Const MINOR_DISTRACT_CONTROLLER_ALARM As Integer = &H41E ' distract controller alarm
    Public Const MINOR_DISTRACT_CONTROLLER_RESUME As Integer = &H41F ' distract controller resume
    Public Const MINOR_CHANNEL_CONTROLLER_DESMANTLE_ALARM As Integer = &H422 'channel controller desmantle alarm
    Public Const MINOR_CHANNEL_CONTROLLER_DESMANTLE_RESUME As Integer = &H423 'channel controller desmantle resume
    Public Const MINOR_CHANNEL_CONTROLLER_FIRE_IMPORT_ALARM As Integer = &H424 'channel controller fire import alarm
    Public Const MINOR_CHANNEL_CONTROLLER_FIRE_IMPORT_RESUME As Integer = &H425 'channel controller fire import resume
    Public Const MINOR_PRINTER_OUT_OF_PAPER As Integer = &H440 'printer no paper
    Public Const MINOR_LEGAL_EVENT_NEARLY_FULL As Integer = &H442 'Legal event nearly full

    ' Exception
    ' Main Type
    Public Const MAJOR_EXCEPTION As Integer = &H2
    ' Hypo- Type

    Public Const MINOR_NET_BROKEN As Integer = &H27 ' Network disconnected
    Public Const MINOR_RS485_DEVICE_ABNORMAL As Integer = &H3A ' RS485 connect status exception
    Public Const MINOR_RS485_DEVICE_REVERT As Integer = &H3B ' RS485 connect status exception recovery

    Public Const MINOR_DEV_POWER_ON As Integer = &H400 ' device power on
    Public Const MINOR_DEV_POWER_OFF As Integer = &H401 ' device power off
    Public Const MINOR_WATCH_DOG_RESET As Integer = &H402 ' watch dog reset
    Public Const MINOR_LOW_BATTERY As Integer = &H403 ' low battery
    Public Const MINOR_BATTERY_RESUME As Integer = &H404 ' battery resume
    Public Const MINOR_AC_OFF As Integer = &H405 ' AC off
    Public Const MINOR_AC_RESUME As Integer = &H406 ' AC resume
    Public Const MINOR_NET_RESUME As Integer = &H407 ' Net resume
    Public Const MINOR_FLASH_ABNORMAL As Integer = &H408 ' FLASH abnormal
    Public Const MINOR_CARD_READER_OFFLINE As Integer = &H409 ' card reader offline
    Public Const MINOR_CARD_READER_RESUME As Integer = &H40A ' card reader resume
    Public Const MINOR_INDICATOR_LIGHT_OFF As Integer = &H40B ' Indicator Light Off
    Public Const MINOR_INDICATOR_LIGHT_RESUME As Integer = &H40C ' Indicator Light Resume
    Public Const MINOR_CHANNEL_CONTROLLER_OFF As Integer = &H40D ' channel controller off
    Public Const MINOR_CHANNEL_CONTROLLER_RESUME As Integer = &H40E ' channel controller resume
    Public Const MINOR_SECURITY_MODULE_OFF As Integer = &H40F ' Door control security module off
    Public Const MINOR_SECURITY_MODULE_RESUME As Integer = &H410 ' Door control security module resume
    Public Const MINOR_BATTERY_ELECTRIC_LOW As Integer = &H411 ' battery electric low
    Public Const MINOR_BATTERY_ELECTRIC_RESUME As Integer = &H412 ' battery electric resume
    Public Const MINOR_LOCAL_CONTROL_NET_BROKEN As Integer = &H413 ' Local control net broken
    Public Const MINOR_LOCAL_CONTROL_NET_RSUME As Integer = &H414 ' Local control net resume
    Public Const MINOR_MASTER_RS485_LOOPNODE_BROKEN As Integer = &H415 ' Master RS485 loop node broken
    Public Const MINOR_MASTER_RS485_LOOPNODE_RESUME As Integer = &H416 ' Master RS485 loop node resume
    Public Const MINOR_LOCAL_CONTROL_OFFLINE As Integer = &H417 ' Local control offline
    Public Const MINOR_LOCAL_CONTROL_RESUME As Integer = &H418 ' Local control resume
    Public Const MINOR_LOCAL_DOWNSIDE_RS485_LOOPNODE_BROKEN As Integer = &H419 ' Local downside RS485 loop node broken
    Public Const MINOR_LOCAL_DOWNSIDE_RS485_LOOPNODE_RESUME As Integer = &H41A ' Local downside RS485 loop node resume
    Public Const MINOR_DISTRACT_CONTROLLER_ONLINE As Integer = &H41B ' distract controller online
    Public Const MINOR_DISTRACT_CONTROLLER_OFFLINE As Integer = &H41C ' distract controller offline
    Public Const MINOR_ID_CARD_READER_NOT_CONNECT As Integer = &H41D ' Id card reader not connected(intelligent dedicated)
    Public Const MINOR_ID_CARD_READER_RESUME As Integer = &H41E 'Id card reader connection restored(intelligent dedicated)
    Public Const MINOR_FINGER_PRINT_MODULE_NOT_CONNECT As Integer = &H41F ' fingerprint module is not connected(intelligent dedicated)
    Public Const MINOR_FINGER_PRINT_MODULE_RESUME As Integer = &H420 ' The fingerprint module connection restored(intelligent dedicated)
    Public Const MINOR_CAMERA_NOT_CONNECT As Integer = &H421 ' Camera not connected
    Public Const MINOR_CAMERA_RESUME As Integer = &H422 ' Camera connection restored
    Public Const MINOR_COM_NOT_CONNECT As Integer = &H423 ' COM not connected
    Public Const MINOR_COM_RESUME As Integer = &H424 ' COM connection restored
    Public Const MINOR_DEVICE_NOT_AUTHORIZE As Integer = &H425 ' device are not authorized
    Public Const MINOR_PEOPLE_AND_ID_CARD_DEVICE_ONLINE As Integer = &H426 ' people and ID card device online
    Public Const MINOR_PEOPLE_AND_ID_CARD_DEVICE_OFFLINE As Integer = &H427 ' people and ID card device offline
    Public Const MINOR_LOCAL_LOGIN_LOCK As Integer = &H428 ' local login lock
    Public Const MINOR_LOCAL_LOGIN_UNLOCK As Integer = &H429 'local login unlock
    Public Const MINOR_SUBMARINEBACK_COMM_BREAK As Integer = &H42A 'submarineback communicate break
    Public Const MINOR_SUBMARINEBACK_COMM_RESUME As Integer = &H42B 'submarineback communicate resume
    Public Const MINOR_MOTOR_SENSOR_EXCEPTION As Integer = &H42C 'motor sensor exception
    Public Const MINOR_CAN_BUS_EXCEPTION As Integer = &H42D 'can bus exception
    Public Const MINOR_CAN_BUS_RESUME As Integer = &H42E 'can bus resume
    Public Const MINOR_GATE_TEMPERATURE_OVERRUN As Integer = &H42F 'gate temperature over run
    Public Const MINOR_IR_EMITTER_EXCEPTION As Integer = &H430 'IR emitter exception
    Public Const MINOR_IR_EMITTER_RESUME As Integer = &H431 'IR emitter resume
    Public Const MINOR_LAMP_BOARD_COMM_EXCEPTION As Integer = &H432 'lamp board communicate exception
    Public Const MINOR_LAMP_BOARD_COMM_RESUME As Integer = &H433 'lamp board communicate resume
    Public Const MINOR_IR_ADAPTOR_COMM_EXCEPTION As Integer = &H434 'IR adaptor communicate exception
    Public Const MINOR_IR_ADAPTOR_COMM_RESUME As Integer = &H435 'IR adaptor communicate resume
    Public Const MINOR_PRINTER_ONLINE As Integer = &H436 'printer online
    Public Const MINOR_PRINTER_OFFLINE As Integer = &H437 'printer offline
    Public Const MINOR_4G_MOUDLE_ONLINE As Integer = &H438 '4G module online
    Public Const MINOR_4G_MOUDLE_OFFLINE As Integer = &H439 '4G module offline


    ' Operation  
    ' Main Type
    Public Const MAJOR_OPERATION As Integer = &H3

    ' Hypo- Type
    Public Const MINOR_LOCAL_UPGRADE As Integer = &H5A ' Upgrade  (local)
    Public Const MINOR_REMOTE_LOGIN As Integer = &H70 ' Login  (remote)
    Public Const MINOR_REMOTE_LOGOUT As Integer = &H71 ' Logout   (remote)
    Public Const MINOR_REMOTE_ARM As Integer = &H79 ' On guard   (remote)
    Public Const MINOR_REMOTE_DISARM As Integer = &H7A ' Disarm   (remote)
    Public Const MINOR_REMOTE_REBOOT As Integer = &H7B ' Reboot   (remote)
    Public Const MINOR_REMOTE_UPGRADE As Integer = &H7E ' upgrade  (remote)
    Public Const MINOR_REMOTE_CFGFILE_OUTPUT As Integer = &H86 ' Export Configuration   (remote)
    Public Const MINOR_REMOTE_CFGFILE_INTPUT As Integer = &H87 ' Import Configuration  (remote)
    Public Const MINOR_REMOTE_ALARMOUT_OPEN_MAN As Integer = &HD6 ' remote mamual open alarmout
    Public Const MINOR_REMOTE_ALARMOUT_CLOSE_MAN As Integer = &HD7 ' remote mamual close alarmout

    Public Const MINOR_REMOTE_OPEN_DOOR As Integer = &H400 ' remote open door
    Public Const MINOR_REMOTE_CLOSE_DOOR As Integer = &H401 ' remote close door (controlled)
    Public Const MINOR_REMOTE_ALWAYS_OPEN As Integer = &H402 ' remote always open door (free)
    Public Const MINOR_REMOTE_ALWAYS_CLOSE As Integer = &H403 ' remote always close door (forbiden)
    Public Const MINOR_REMOTE_CHECK_TIME As Integer = &H404 ' remote check time
    Public Const MINOR_NTP_CHECK_TIME As Integer = &H405 ' ntp check time
    Public Const MINOR_REMOTE_CLEAR_CARD As Integer = &H406 ' remote clear card
    Public Const MINOR_REMOTE_RESTORE_CFG As Integer = &H407 ' remote restore configure
    Public Const MINOR_ALARMIN_ARM As Integer = &H408 ' alarm in arm
    Public Const MINOR_ALARMIN_DISARM As Integer = &H409 ' alarm in disarm
    Public Const MINOR_LOCAL_RESTORE_CFG As Integer = &H40A ' local configure restore
    Public Const MINOR_REMOTE_CAPTURE_PIC As Integer = &H40B ' remote capture picture
    Public Const MINOR_MOD_NET_REPORT_CFG As Integer = &H40C ' modify net report cfg
    Public Const MINOR_MOD_GPRS_REPORT_PARAM As Integer = &H40D ' modify GPRS report param
    Public Const MINOR_MOD_REPORT_GROUP_PARAM As Integer = &H40E ' modify report group param
    Public Const MINOR_UNLOCK_PASSWORD_OPEN_DOOR As Integer = &H40F ' unlock password open door
    Public Const MINOR_AUTO_RENUMBER As Integer = &H410 ' auto renumber
    Public Const MINOR_AUTO_COMPLEMENT_NUMBER As Integer = &H411 ' auto complement number
    Public Const MINOR_NORMAL_CFGFILE_INPUT As Integer = &H412 ' normal cfg file input
    Public Const MINOR_NORMAL_CFGFILE_OUTTPUT As Integer = &H413 ' normal cfg file output
    Public Const MINOR_CARD_RIGHT_INPUT As Integer = &H414 ' card right input
    Public Const MINOR_CARD_RIGHT_OUTTPUT As Integer = &H415 ' card right output
    Public Const MINOR_LOCAL_USB_UPGRADE As Integer = &H416 ' local USB upgrade
    Public Const MINOR_REMOTE_VISITOR_CALL_LADDER As Integer = &H417 ' visitor call ladder
    Public Const MINOR_REMOTE_HOUSEHOLD_CALL_LADDER As Integer = &H418 ' household call ladder
    Public Const MINOR_REMOTE_ACTUAL_GUARD As Integer = &H419 'remote actual guard
    Public Const MINOR_REMOTE_ACTUAL_UNGUARD As Integer = &H41A 'remote actual unguard
    Public Const MINOR_REMOTE_CONTROL_NOT_CODE_OPER_FAILED As Integer = &H41B 'remote control not code operate failed
    Public Const MINOR_REMOTE_CONTROL_CLOSE_DOOR As Integer = &H41C 'remote control close door
    Public Const MINOR_REMOTE_CONTROL_OPEN_DOOR As Integer = &H41D 'remote control open door
    Public Const MINOR_REMOTE_CONTROL_ALWAYS_OPEN_DOOR As Integer = &H41E 'remote control always open door

    ' Additional Log Info
    ' Main Type
    Public Const MAJOR_EVENT As Integer = &H5 'event
    ' Hypo- Type
    Public Const MINOR_LEGAL_CARD_PASS As Integer = &H1 ' legal card pass
    Public Const MINOR_CARD_AND_PSW_PASS As Integer = &H2 ' swipe and password pass
    Public Const MINOR_CARD_AND_PSW_FAIL As Integer = &H3 ' swipe and password fail
    Public Const MINOR_CARD_AND_PSW_TIMEOUT As Integer = &H4 ' swipe and password timeout
    Public Const MINOR_CARD_AND_PSW_OVER_TIME As Integer = &H5 ' swipe and password over time
    Public Const MINOR_CARD_NO_RIGHT As Integer = &H6 ' card no right
    Public Const MINOR_CARD_INVALID_PERIOD As Integer = &H7 ' invalid period
    Public Const MINOR_CARD_OUT_OF_DATE As Integer = &H8 ' card out of date
    Public Const MINOR_INVALID_CARD As Integer = &H9 ' invalid card
    Public Const MINOR_ANTI_SNEAK_FAIL As Integer = &HA ' anti sneak fail
    Public Const MINOR_INTERLOCK_DOOR_NOT_CLOSE As Integer = &HB ' interlock door doesn't close
    Public Const MINOR_NOT_BELONG_MULTI_GROUP As Integer = &HC ' card no belong multi group
    Public Const MINOR_INVALID_MULTI_VERIFY_PERIOD As Integer = &HD ' invalid multi verify period
    Public Const MINOR_MULTI_VERIFY_SUPER_RIGHT_FAIL As Integer = &HE ' have no super right in multi verify mode
    Public Const MINOR_MULTI_VERIFY_REMOTE_RIGHT_FAIL As Integer = &HF ' have no remote right in multi verify mode
    Public Const MINOR_MULTI_VERIFY_SUCCESS As Integer = &H10 ' success in multi verify mode
    Public Const MINOR_LEADER_CARD_OPEN_BEGIN As Integer = &H11 ' leader card begin to open
    Public Const MINOR_LEADER_CARD_OPEN_END As Integer = &H12 ' leader card end to open
    Public Const MINOR_ALWAYS_OPEN_BEGIN As Integer = &H13 ' always open begin
    Public Const MINOR_ALWAYS_OPEN_END As Integer = &H14 ' always open end
    Public Const MINOR_LOCK_OPEN As Integer = &H15 ' lock open
    Public Const MINOR_LOCK_CLOSE As Integer = &H16 ' lock close
    Public Const MINOR_DOOR_BUTTON_PRESS As Integer = &H17 ' press door open button
    Public Const MINOR_DOOR_BUTTON_RELEASE As Integer = &H18 ' release door open button
    Public Const MINOR_DOOR_OPEN_NORMAL As Integer = &H19 ' door open normal
    Public Const MINOR_DOOR_CLOSE_NORMAL As Integer = &H1A ' door close normal
    Public Const MINOR_DOOR_OPEN_ABNORMAL As Integer = &H1B ' open door abnormal
    Public Const MINOR_DOOR_OPEN_TIMEOUT As Integer = &H1C ' open door timeout
    Public Const MINOR_ALARMOUT_ON As Integer = &H1D ' alarm out turn on
    Public Const MINOR_ALARMOUT_OFF As Integer = &H1E ' alarm out turn off
    Public Const MINOR_ALWAYS_CLOSE_BEGIN As Integer = &H1F ' always close begin
    Public Const MINOR_ALWAYS_CLOSE_END As Integer = &H20 ' always close end
    Public Const MINOR_MULTI_VERIFY_NEED_REMOTE_OPEN As Integer = &H21 ' need remote open in multi verify mode
    Public Const MINOR_MULTI_VERIFY_SUPERPASSWD_VERIFY_SUCCESS As Integer = &H22 ' superpasswd verify success in multi verify mode
    Public Const MINOR_MULTI_VERIFY_REPEAT_VERIFY As Integer = &H23 ' repeat verify in multi verify mode
    Public Const MINOR_MULTI_VERIFY_TIMEOUT As Integer = &H24 ' timeout in multi verify mode
    Public Const MINOR_DOORBELL_RINGING As Integer = &H25 ' doorbell ringing
    Public Const MINOR_FINGERPRINT_COMPARE_PASS As Integer = &H26 ' fingerprint compare pass
    Public Const MINOR_FINGERPRINT_COMPARE_FAIL As Integer = &H27 ' fingerprint compare fail
    Public Const MINOR_CARD_FINGERPRINT_VERIFY_PASS As Integer = &H28 ' card and fingerprint verify pass
    Public Const MINOR_CARD_FINGERPRINT_VERIFY_FAIL As Integer = &H29 ' card and fingerprint verify fail
    Public Const MINOR_CARD_FINGERPRINT_VERIFY_TIMEOUT As Integer = &H2A ' card and fingerprint verify timeout
    Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_PASS As Integer = &H2B ' card and fingerprint and passwd verify pass
    Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_FAIL As Integer = &H2C ' card and fingerprint and passwd verify fail
    Public Const MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_TIMEOUT As Integer = &H2D ' card and fingerprint and passwd verify timeout
    Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_PASS As Integer = &H2E ' fingerprint and passwd verify pass
    Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_FAIL As Integer = &H2F ' fingerprint and passwd verify fail
    Public Const MINOR_FINGERPRINT_PASSWD_VERIFY_TIMEOUT As Integer = &H30 ' fingerprint and passwd verify timeout
    Public Const MINOR_FINGERPRINT_INEXISTENCE As Integer = &H31 ' fingerprint inexistence
    Public Const MINOR_CARD_PLATFORM_VERIFY As Integer = &H32 ' card platform verify
    Public Const MINOR_CALL_CENTER As Integer = &H33 ' call center
    Public Const MINOR_FIRE_RELAY_TURN_ON_DOOR_ALWAYS_OPEN As Integer = &H34 ' fire relay turn on door always open
    Public Const MINOR_FIRE_RELAY_RECOVER_DOOR_RECOVER_NORMAL As Integer = &H35 ' fire relay recover door recover normal
    Public Const MINOR_FACE_AND_FP_VERIFY_PASS As Integer = &H36 ' face and finger print verify pass
    Public Const MINOR_FACE_AND_FP_VERIFY_FAIL As Integer = &H37 ' face and finger print verify fail
    Public Const MINOR_FACE_AND_FP_VERIFY_TIMEOUT As Integer = &H38 ' face and finger print verify timeout
    Public Const MINOR_FACE_AND_PW_VERIFY_PASS As Integer = &H39 ' face and password verify pass
    Public Const MINOR_FACE_AND_PW_VERIFY_FAIL As Integer = &H3A ' face and password verify fail
    Public Const MINOR_FACE_AND_PW_VERIFY_TIMEOUT As Integer = &H3B ' face and password verify timeout
    Public Const MINOR_FACE_AND_CARD_VERIFY_PASS As Integer = &H3C ' face and card verify pass
    Public Const MINOR_FACE_AND_CARD_VERIFY_FAIL As Integer = &H3D ' face and card verify fail
    Public Const MINOR_FACE_AND_CARD_VERIFY_TIMEOUT As Integer = &H3E ' face and card verify timeout
    Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_PASS As Integer = &H3F ' face and password and finger print verify pass
    Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_FAIL As Integer = &H40 ' face and password and finger print verify fail
    Public Const MINOR_FACE_AND_PW_AND_FP_VERIFY_TIMEOUT As Integer = &H41 ' face and password and finger print verify timeout
    Public Const MINOR_FACE_CARD_AND_FP_VERIFY_PASS As Integer = &H42 ' face and card and finger print verify pass
    Public Const MINOR_FACE_CARD_AND_FP_VERIFY_FAIL As Integer = &H43 ' face and card and finger print verify fail
    Public Const MINOR_FACE_CARD_AND_FP_VERIFY_TIMEOUT As Integer = &H44 ' face and card and finger print verify timeout
    Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_PASS As Integer = &H45 ' employee and finger print verify pass
    Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_FAIL As Integer = &H46 ' employee and finger print verify fail
    Public Const MINOR_EMPLOYEENO_AND_FP_VERIFY_TIMEOUT As Integer = &H47 ' employee and finger print verify timeout
    Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_PASS As Integer = &H48 ' employee and finger print and password verify pass
    Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_FAIL As Integer = &H49 ' employee and finger print and password verify fail
    Public Const MINOR_EMPLOYEENO_AND_FP_AND_PW_VERIFY_TIMEOUT As Integer = &H4A ' employee and finger print and password verify timeout
    Public Const MINOR_FACE_VERIFY_PASS As Integer = &H4B ' face verify pass
    Public Const MINOR_FACE_VERIFY_FAIL As Integer = &H4C ' face verify fail
    Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_PASS As Integer = &H4D ' employee no and face verify pass
    Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_FAIL As Integer = &H4E ' employee no and face verify fail
    Public Const MINOR_EMPLOYEENO_AND_FACE_VERIFY_TIMEOUT As Integer = &H4F ' employee no and face verify time out
    Public Const MINOR_FACE_RECOGNIZE_FAIL As Integer = &H50 ' face recognize fail
    Public Const MINOR_FIRSTCARD_AUTHORIZE_BEGIN As Integer = &H51 ' first card authorize begin
    Public Const MINOR_FIRSTCARD_AUTHORIZE_END As Integer = &H52 ' first card authorize end
    Public Const MINOR_DOORLOCK_INPUT_SHORT_CIRCUIT As Integer = &H53 ' door lock input short circuit
    Public Const MINOR_DOORLOCK_INPUT_BROKEN_CIRCUIT As Integer = &H54 ' door lock input broken circuit
    Public Const MINOR_DOORLOCK_INPUT_EXCEPTION As Integer = &H55 ' door lock input exception
    Public Const MINOR_DOORCONTACT_INPUT_SHORT_CIRCUIT As Integer = &H56 ' door contact input short circuit
    Public Const MINOR_DOORCONTACT_INPUT_BROKEN_CIRCUIT As Integer = &H57 ' door contact input broken circuit
    Public Const MINOR_DOORCONTACT_INPUT_EXCEPTION As Integer = &H58 ' door contact input exception
    Public Const MINOR_OPENBUTTON_INPUT_SHORT_CIRCUIT As Integer = &H59 ' open button input short circuit
    Public Const MINOR_OPENBUTTON_INPUT_BROKEN_CIRCUIT As Integer = &H5A ' open button input broken circuit
    Public Const MINOR_OPENBUTTON_INPUT_EXCEPTION As Integer = &H5B ' open button input exception
    Public Const MINOR_DOORLOCK_OPEN_EXCEPTION As Integer = &H5C ' door lock open exception
    Public Const MINOR_DOORLOCK_OPEN_TIMEOUT As Integer = &H5D ' door lock open timeout
    Public Const MINOR_FIRSTCARD_OPEN_WITHOUT_AUTHORIZE As Integer = &H5E ' first card open without authorize
    Public Const MINOR_CALL_LADDER_RELAY_BREAK As Integer = &H5F ' call ladder relay break
    Public Const MINOR_CALL_LADDER_RELAY_CLOSE As Integer = &H60 ' call ladder relay close
    Public Const MINOR_AUTO_KEY_RELAY_BREAK As Integer = &H61 ' auto key relay break
    Public Const MINOR_AUTO_KEY_RELAY_CLOSE As Integer = &H62 ' auto key relay close
    Public Const MINOR_KEY_CONTROL_RELAY_BREAK As Integer = &H63 ' key control relay break
    Public Const MINOR_KEY_CONTROL_RELAY_CLOSE As Integer = &H64 ' key control relay close
    Public Const MINOR_EMPLOYEENO_AND_PW_PASS As Integer = &H65 ' minor employee no and password pass
    Public Const MINOR_EMPLOYEENO_AND_PW_FAIL As Integer = &H66 ' minor employee no and password fail
    Public Const MINOR_EMPLOYEENO_AND_PW_TIMEOUT As Integer = &H67 ' minor employee no and password timeout
    Public Const MINOR_HUMAN_DETECT_FAIL As Integer = &H68 ' human detect fail
    Public Const MINOR_PEOPLE_AND_ID_CARD_COMPARE_PASS As Integer = &H69 ' the comparison with people and id card success
    Public Const MINOR_PEOPLE_AND_ID_CARD_COMPARE_FAIL As Integer = &H70 ' the comparison with people and id card failed
    Public Const MINOR_CERTIFICATE_BLOCK_LIST As Integer = &H71
    Public Const MINOR_LEGAL_MESSAGE As Integer = &H72 ' legal message
    Public Const MINOR_ILLEGAL_MESSAGE As Integer = &H73 ' illegal messag
    Public Const MINOR_MAC_DETECT As Integer = &H74 ' mac detect
    Public Const MINOR_DOOR_OPEN_OR_DORMANT_FAIL As Integer = &H75 'door open or dormant fail
    Public Const MINOR_AUTH_PLAN_DORMANT_FAIL As Integer = &H76 'auth plan dormant fail
    Public Const MINOR_CARD_ENCRYPT_VERIFY_FAIL As Integer = &H77 'card encrypt verify fail
    Public Const MINOR_SUBMARINEBACK_REPLY_FAIL As Integer = &H78 'submarineback reply fail
    Public Const MINOR_DOOR_OPEN_OR_DORMANT_OPEN_FAIL As Integer = &H82 'door open or dormant open fail
    Public Const MINOR_DOOR_OPEN_OR_DORMANT_LINKAGE_OPEN_FAIL As Integer = &H84 'door open or dormant linkage open fail
    Public Const MINOR_TRAILING As Integer = &H85 'trailing
    Public Const MINOR_HEART_BEAT As Integer = &H83 'heart beat event
    Public Const MINOR_REVERSE_ACCESS As Integer = &H86 'reverse access
    Public Const MINOR_FORCE_ACCESS As Integer = &H87 'force access
    Public Const MINOR_CLIMBING_OVER_GATE As Integer = &H88 'climbing over gate
    Public Const MINOR_PASSING_TIMEOUT As Integer = &H89 'passing timeout
    Public Const MINOR_INTRUSION_ALARM As Integer = &H8A 'intrusion alarm
    Public Const MINOR_FREE_GATE_PASS_NOT_AUTH As Integer = &H8B 'free gate pass not auth
    Public Const MINOR_DROP_ARM_BLOCK As Integer = &H8C 'drop arm block
    Public Const MINOR_DROP_ARM_BLOCK_RESUME As Integer = &H8D 'drop arm block resume
    Public Const MINOR_LOCAL_FACE_MODELING_FAIL As Integer = &H8E 'device upgrade with module failed
    Public Const MINOR_STAY_EVENT As Integer = &H8F 'stay event
    Public Const MINOR_PASSWORD_MISMATCH As Integer = &H97 'password mismatch
    Public Const MINOR_EMPLOYEE_NO_NOT_EXIST As Integer = &H98 'employee no not exist
    Public Const MINOR_COMBINED_VERIFY_PASS As Integer = &H99 'combined verify pass
    Public Const MINOR_COMBINED_VERIFY_TIMEOUT As Integer = &H9A 'combined verify timeout
    Public Const MINOR_VERIFY_MODE_MISMATCH As Integer = &H9B 'verify mode mismatch
#End Region

#Region "HCNetSDK.dll function definition"
    <DllImport("HCNetSDK/HCNetSDK.dll")> _
    Public Shared Function NET_DVR_StopRemoteConfig(ByVal lHandle As Integer) As Boolean
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_Logout_V30(ByVal m_UserID As Integer) As Boolean
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_Cleanup() As Boolean
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_GetNextRemoteConfig(ByVal lHandle As Integer, ByRef lpOutBuff As CHCNetSDK.NET_DVR_ACS_EVENT_CFG, ByVal dwOutBuffSize As Integer) As Integer
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_Init() As Boolean
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_Login_V40(ByRef pLoginInfo As NET_DVR_USER_LOGIN_INFO, ByRef lpDeviceInfo As NET_DVR_DEVICEINFO_V40) As Integer
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_GetLastError() As UInteger
    End Function

    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_SetLogToFile(ByVal nLogLevel As Integer, ByVal strLogDir As String, ByVal bAutoDel As Boolean) As Boolean
    End Function

    Public Delegate Sub RemoteConfigCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    <DllImport("HCNetSDK\HCNetSDK.dll")> _
    Public Shared Function NET_DVR_StartRemoteConfig(ByVal lUserID As Integer, ByVal dwCommand As Integer, ByVal lpInBuffer As IntPtr, ByVal dwInBufferLen As Integer, ByVal cbStateCallback As RemoteConfigCallback, ByVal pUserData As IntPtr) As Integer
    End Function

#End Region

#Region "HCNetSDK.dll structure definition"

    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure NET_DVR_DEVICEINFO_V30
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=SERIALNO_LEN, ArraySubType:=UnmanagedType.I1)> _
        Public sSerialNumber() As Byte 'serial number
        Public byAlarmInPortNum As Byte 'Number of Alarm input
        Public byAlarmOutPortNum As Byte 'Number of Alarm Output
        Public byDiskNum As Byte 'Number of Hard Disk
        Public byDVRType As Byte 'DVR Type, 1: DVR 2: ATM DVR 3: DVS ......
        Public byChanNum As Byte 'Number of Analog Channel
        Public byStartChan As Byte 'The first Channel No. E.g. DVS- 1, DVR- 1
        Public byAudioChanNum As Byte 'Number of Audio Channel
        Public byIPChanNum As Byte 'Maximum number of IP Channel  low
        Public byZeroChanNum As Byte 'Zero channel encoding number//2010- 01- 16
        Public byMainProto As Byte 'Main stream transmission protocol 0- private,  1- rtsp,2-both private and rtsp
        Public bySubProto As Byte 'Sub stream transmission protocol 0- private,  1- rtsp,2-both private and rtsp
        Public bySupport As Byte 'Ability, the 'AND' result by bit: 0- not support;  1- support
        'bySupport & 0x1,  smart search
        'bySupport & 0x2,  backup
        'bySupport & 0x4,  get compression configuration ability
        'bySupport & 0x8,  multi network adapter
        'bySupport & 0x10, support remote SADP
        'bySupport & 0x20  support Raid card
        'bySupport & 0x40 support IPSAN directory search
        Public bySupport1 As Byte 'Ability expand, the 'AND' result by bit: 0- not support;  1- support
        'bySupport1 & 0x1, support snmp v30
        'bySupport1& 0x2,support distinguish download and playback
        'bySupport1 & 0x4, support deployment level
        'bySupport1 & 0x8, support vca alarm time extension 
        'bySupport1 & 0x10, support muti disks(more than 33)
        'bySupport1 & 0x20, support rtsp over http
        'bySupport1 & 0x40, support delay preview
        'bySuppory1 & 0x80 support NET_DVR_IPPARACFG_V40, in addition  support  License plate of the new alarm information
        Public bySupport2 As Byte 'Ability expand, the 'AND' result by bit: 0- not support;  1- support
        'bySupport & 0x1, decoder support get stream by URL
        'bySupport2 & 0x2,  support FTPV40
        'bySupport2 & 0x4,  support ANR
        'bySupport2 & 0x20, support get single item of device status
        'bySupport2 & 0x40,  support stream encryt
        Public wDevType As UShort 'device type
        Public bySupport3 As Byte 'Support  epresent by bit, 0 - not support 1 - support
        'bySupport3 & 0x1-muti stream support 
        'bySupport3 & 0x8  support use delay preview parameter when delay preview
        'bySupport3 & 0x10 support the interface of getting alarmhost main status V40
        Public byMultiStreamProto As Byte 'support multi stream, represent by bit, 0-not support ;1- support; bit1-stream 3 ;bit2-stream 4, bit7-main stream, bit8-sub stream
        Public byStartDChan As Byte 'Start digital channel
        Public byStartDTalkChan As Byte 'Start digital talk channel
        Public byHighDChanNum As Byte 'Digital channel number high
        Public bySupport4 As Byte 'Support  epresent by bit, 0 - not support 1 - support
        'bySupport4 & 0x4 whether support video wall unified interface
        ' bySupport4 & 0x80 Support device upload center alarm enable
        Public byLanguageType As Byte 'support language type by bit,0-support,1-not support
        'byLanguageType 0 -old device
        'byLanguageType & 0x1 support chinese
        'byLanguageType & 0x2 support english
        Public byVoiceInChanNum As Byte 'voice in chan num
        Public byStartVoiceInChanNo As Byte 'start voice in chan num
        Public bySupport5 As Byte '0-no support,1-support,bit0-muti stream
        'bySupport5 &0x01support wEventTypeEx 
        'bySupport5 &0x04support sence expend
        Public bySupport6 As Byte
        Public byMirrorChanNum As Byte 'mirror channel num,<it represents direct channel in the recording host
        Public wStartMirrorChanNo As UShort 'start mirror chan
        Public bySupport7 As Byte 'Support  epresent by bit, 0 - not support 1 - support
        'bySupport7 & 0x1- supports INTER_VCA_RULECFG_V42 extension    
        ' bySupport7 & 0x2  Supports HVT IPC mode expansion
        ' bySupport7 & 0x04  Back lock time
        ' bySupport7 & 0x08  Set the pan PTZ position, whether to support the band channel
        ' bySupport7 & 0x10  Support for dual system upgrade backup
        ' bySupport7 & 0x20  Support OSD character overlay V50
        ' bySupport7 & 0x40  Support master slave tracking (slave camera)
        ' bySupport7 & 0x80  Support message encryption 
        Public byRes2 As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Public Structure NET_DVR_DEVICEINFO_V40
        Public struDeviceV30 As NET_DVR_DEVICEINFO_V30
        Public bySupportLock As Byte '设备支持锁定功能，该字段由SDK根据设备返回值来赋值的。bySupportLock为1时，dwSurplusLockTime和byRetryLoginTime有效
        Public byRetryLoginTime As Byte '剩余可尝试登陆的次数，用户名，密码错误时，此参数有效
        Public byPasswordLevel As Byte 'admin密码安全等级0-无效，1-默认密码，2-有效密码,3-风险较高的密码。当用户的密码为出厂默认密码（12345）或者风险较高的密码时，上层客户端需要提示用户更改密码。
        Public byProxyType As Byte '代理类型，0-不使用代理, 1-使用socks5代理, 2-使用EHome代理
        Public dwSurplusLockTime As UInteger '剩余时间，单位秒，用户锁定时，此参数有效
        Public byCharEncodeType As Byte '字符编码类型
        Public bySupportDev5 As Byte '支持v50版本的设备参数获取，设备名称和设备类型名称长度扩展为64字节
        Public bySupport As Byte '能力集扩展，位与结果：0- 不支持，1- 支持
        ' bySupport & 0x1:  保留
        ' bySupport & 0x2:  0-不支持变化上报 1-支持变化上报
        Public byLoginMode As Byte '登录模式 0-Private登录 1-ISAPI登录
        Public dwOEMCode As UInteger
        Public iResidualValidity As Integer '该用户密码剩余有效天数，单位：天，返回负值，表示密码已经超期使用，例如"-3表示密码已经超期使用3天"
        Public byResidualValidity As Byte ' iResidualValidity字段是否有效，0-无效，1-有效
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=243)> _
        Public byRes2() As Byte
    End Structure


    Public Delegate Sub LoginResultCallBack(ByVal lUserID As Integer, ByVal dwResult As UInteger, ByRef lpDeviceInfo As NET_DVR_DEVICEINFO_V30, ByVal pUser As IntPtr)
    <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure NET_DVR_USER_LOGIN_INFO
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_DEV_ADDRESS_MAX_LEN)> _
        Public sDeviceAddress As String
        Public byUseTransport As Byte
        Public wPort As UShort
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_LOGIN_USERNAME_MAX_LEN)> _
        Public sUserName As String
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NET_DVR_LOGIN_PASSWD_MAX_LEN)> _
        Public sPassword As String
        Public cbLoginResult As LoginResultCallBack
        Public pUser As IntPtr
        Public bUseAsynLogin As Boolean
        Public byProxyType As Byte
        Public byUseUTCTime As Byte
        Public byLoginMode As Byte '登录模式 0-Private 1-ISAPI 2-自适应（默认不采用自适应是因为自适应登录时，会对性能有较大影响，自适应时要同时发起ISAPI和Private登录）
        Public byHttps As Byte 'ISAPI登录时，是否使用HTTPS，0-不使用HTTPS，1-使用HTTPS 2-自适应（默认不采用自适应是因为自适应登录时，会对性能有较大影响，自适应时要同时发起HTTP和HTTPS）
        Public iProxyID As Integer
        Public byVerifyMode As Byte '认证方式，0-不认证，1-双向认证，2-单向认证；认证仅在使用TLS的时候生效;
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=119, ArraySubType:=UnmanagedType.I1)> _
        Public byRes3() As Byte
    End Structure


    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure NET_DVR_TIME
        Public dwYear As Integer
        Public dwMonth As Integer
        Public dwDay As Integer
        Public dwHour As Integer
        Public dwMinute As Integer
        Public dwSecond As Integer
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure NET_DVR_ACS_EVENT_COND
        Public dwSize As UInteger
        Public dwMajor As UInteger
        Public dwMinor As UInteger
        Public struStartTime As CHCNetSDK.NET_DVR_TIME
        Public struEndTime As CHCNetSDK.NET_DVR_TIME
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
        Public byCardNo() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NAME_LEN, ArraySubType:=UnmanagedType.I1)> _
        Public byName() As Byte
        Public dwBeginSerialNo As UInteger
        Public byPicEnable As Byte
        Public byTimeType As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2, ArraySubType:=UnmanagedType.I1)> _
        Public byRes2() As Byte
        Public dwEndSerialNo As UInteger
        Public dwIOTChannelNo As UInteger
        Public wInductiveEventType As UShort
        Public bySearchType As Byte
        Public byRes1 As Byte
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=CHCNetSDK.NET_SDK_MONITOR_ID_LEN)> _
        Public szMonitorID As String
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN, ArraySubType:=UnmanagedType.I1)> _
        Public byEmployeeNo() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=140, ArraySubType:=UnmanagedType.I1)> _
        Public byRes() As Byte

        Public Sub Init()
            byCardNo = New Byte(CHCNetSDK.ACS_CARD_NO_LEN - 1) {}
            byName = New Byte(CHCNetSDK.NAME_LEN - 1) {}
            byRes2 = New Byte(1) {}
            byEmployeeNo = New Byte(CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN - 1) {}
            byRes = New Byte(139) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure NET_DVR_ACS_EVENT_DETAIL
        Public dwSize As UInteger
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.ACS_CARD_NO_LEN)> _
        Public byCardNo() As Byte
        Public byCardType As Byte
        Public byWhiteListNo As Byte
        Public byReportChannel As Byte
        Public byCardReaderKind As Byte
        Public dwCardReaderNo As UInteger
        Public dwDoorNo As UInteger
        Public dwVerifyNo As UInteger
        Public dwAlarmInNo As UInteger
        Public dwAlarmOutNo As UInteger
        Public dwCaseSensorNo As UInteger
        Public dwRs485No As UInteger
        Public dwMultiCardGroupNo As UInteger
        Public wAccessChannel As UShort 'word
        Public byDeviceNo As Byte
        Public byDistractControlNo As Byte
        Public dwEmployeeNo As UInteger
        Public wLocalControllerID As UShort 'word
        Public byInternetAccess As Byte
        Public byType As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MACADDR_LEN)> _
        Public byMACAddr() As Byte
        Public bySwipeCardType As Byte
        Public byRes2 As Byte
        Public dwSerialNo As UInteger
        Public byChannelControllerID As Byte
        Public byChannelControllerLampID As Byte
        Public byChannelControllerIRAdaptorID As Byte
        Public byChannelControllerIREmitterID As Byte
        Public dwRecordChannelNum As UInteger
        Public pRecordChannelData As IntPtr
        Public byUserType As Byte
        Public byCurrentVerifyMode As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public byRe2() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN)> _
        Public byEmployeeNo() As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)> _
        Public byRes() As Byte
        Public Sub init()
            byCardNo = New Byte(CHCNetSDK.ACS_CARD_NO_LEN - 1) {}
            byMACAddr = New Byte(CHCNetSDK.MACADDR_LEN - 1) {}
            byRe2 = New Byte(1) {}
            byEmployeeNo = New Byte(CHCNetSDK.NET_SDK_EMPLOYEE_NO_LEN - 1) {}
            byRes = New Byte(63) {}
        End Sub
    End Structure
    'IP address
    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure NET_DVR_IPADDR

        ''' char[16]
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=16)> _
        Public sIpV4 As String

        ''' BYTE[128]
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.I1)> _
        Public byIPv6() As Byte

        Public Sub Init()
            byIPv6 = New Byte(127) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure NET_DVR_ACS_EVENT_CFG
        Public dwSize As UInteger
        Public dwMajor As UInteger
        Public dwMinor As UInteger
        Public struTime As CHCNetSDK.NET_DVR_TIME
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=CHCNetSDK.MAX_NAMELEN)> _
        Public sNetUser() As Byte
        Public struRemoteHostAddr As CHCNetSDK.NET_DVR_IPADDR
        Public struAcsEventInfo As CHCNetSDK.NET_DVR_ACS_EVENT_DETAIL
        Public dwPicDataLen As UInteger
        Public pPicData As IntPtr ' picture data
        Public wInductiveEventType As UShort
        Public byTimeType As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=61)> _
        Public byRes() As Byte

        Public Sub init()
            sNetUser = New Byte(CHCNetSDK.MAX_NAMELEN - 1) {}
            struRemoteHostAddr.Init()
            struAcsEventInfo.init()
            byRes = New Byte(60) {}
        End Sub
    End Structure
#End Region

End Class


