﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices

Public Class clsBioStar

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_InitSDK")> _
    Public Shared Function BS_InitSDK() As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_OpenInternalUDP")> _
    Public Shared Function BS_OpenInternalUDP(ByRef handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_SearchDeviceInLAN")> _
        Public Shared Function BS_SearchDeviceInLAN(ByVal handle As Integer, ByRef numOfDevice As Integer, ByVal deviceIDs As UInt32(), ByVal deviceTypes As Integer(), ByVal readerAddrs As UInt32()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_SetDeviceID")> _
       Public Shared Function BS_SetDeviceID(ByVal handle As Integer, ByVal deviceID As UInt32, ByVal deviceType As Integer) As Integer
    End Function

    'Sohail (12 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetDeviceID")> _
       Public Shared Function BS_GetDeviceID(ByVal handle As Integer, ByRef deviceID As UInt32, ByRef deviceType As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ClearLogCache")> _
      Public Shared Function BS_ClearLogCache(ByVal handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadLogCache")> _
    Public Shared Function BS_ReadLogCache(ByVal handle As Integer, ByRef logCount As Integer, ByVal logRecord As IntPtr) As Integer
    End Function
    'Sohail (12 Sep 2013) -- End

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadConfig")> _
Public Shared Function BS_ReadConfig(ByVal handle As Integer, ByVal configType As Integer, ByRef configSize As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_WriteConfig")> _
    Public Shared Function BS_WriteConfig(ByVal handle As Integer, ByVal configType As Integer, ByVal configSize As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadConfigUDP")> _
    Public Shared Function BS_ReadConfigUDP(ByVal handle As Integer, ByVal targetAddr As UInt32, ByVal targetID As UInt32, ByVal configType As Integer, ByRef configSize As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_WriteConfigUDP")> _
    Public Shared Function BS_WriteConfigUDP(ByVal handle As Integer, ByVal targetAddr As UInt32, ByVal targetID As UInt32, ByVal configType As Integer, ByVal configSize As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_OpenSocket")> _
    Public Shared Function BS_OpenSocket(ByVal addr As String, ByVal port As Integer, ByRef handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_OpenSocketEx")> _
    Public Shared Function BS_OpenSocket(ByVal addr As String, ByVal port As Integer, ByVal hostaddr As String, ByRef handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_CloseSocket")> _
    Public Shared Function BS_CloseSocket(ByVal handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_Disable")> _
    Public Shared Function BS_Disable(ByVal handle As Integer, ByVal nTimeout As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_Enable")> _
    Public Shared Function BS_Enable(ByVal handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetTime")> _
    Public Shared Function BS_GetTime(ByVal handle As Integer, ByRef timeVal As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_SetTime")> _
    Public Shared Function BS_SetTime(ByVal handle As Integer, ByVal timeVal As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserDBInfo")> _
    Public Shared Function BS_GetUserDBInfo(ByVal handle As Integer, ByRef numOfUser As Integer, ByRef numOfTemplate As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserFaceInfo")> _
    Public Shared Function BS_GetUserFaceInfo(ByVal handle As Integer, ByRef numOfUser As Integer, ByRef numOfTemplate As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllUserInfoBEPlus")> _
    Public Shared Function BS_GetAllUserInfoBEPlus(ByVal handle As Integer, ByVal userHdr As IntPtr, ByRef numOfUser As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllUserInfoEx")> _
    Public Shared Function BS_GetAllUserInfoEx(ByVal handle As Integer, ByVal userHdr As IntPtr, ByRef numOfUser As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllUserInfoDStation")> _
    Public Shared Function BS_GetAllUserInfoDStation(ByVal handle As Integer, ByVal userHdr As IntPtr, ByRef numOfUser As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllUserInfoXStation")> _
    Public Shared Function BS_GetAllUserInfoXStation(ByVal handle As Integer, ByVal userHdr As IntPtr, ByRef numOfUser As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllUserInfoBioStation2")> _
    Public Shared Function BS_GetAllUserInfoBioStation2(ByVal handle As Integer, ByVal userHdr As IntPtr, ByRef numOfUser As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllUserInfoFStation")> _
    Public Shared Function BS_GetAllUserInfoFStation(ByVal handle As Integer, ByVal userHdr As IntPtr, ByRef numOfUser As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserInfoBEPlus")> _
    Public Shared Function BS_GetUserInfoBEPlus(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserInfoDStation")> _
    Public Shared Function BS_GetUserInfoDStation(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserInfoXStation")> _
    Public Shared Function BS_GetUserInfoXStation(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserInfoBioStation2")> _
    Public Shared Function BS_GetUserInfoBioStation2(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserInfoFStation")> _
    Public Shared Function BS_GetUserInfoFStation(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_DeleteUser")> _
    Public Shared Function BS_DeleteUser(ByVal handle As Integer, ByVal userID As UInt32) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_DeleteAllUser")> _
    Public Shared Function BS_DeleteAllUser(ByVal handle As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetAllAccessGroupEx")> _
    Public Shared Function BS_GetAllAccessGroupEx(ByVal handle As Integer, ByRef numOfGroup As Integer, ByVal accessGroup As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserBEPlus")> _
    Public Shared Function BS_GetUserBEPlus(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserEx")> _
    Public Shared Function BS_GetUserEx(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserDStation")> _
    Public Shared Function BS_GetUserDStation(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr, ByVal templateData As Byte(), ByVal facetemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserXStation")> _
    Public Shared Function BS_GetUserXStation(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserBioStation2")> _
    Public Shared Function BS_GetUserBioStation2(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetUserFStation")> _
    Public Shared Function BS_GetUserFStation(ByVal handle As Integer, ByVal userID As UInt32, ByVal userHdr As IntPtr, ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollUserBEPlus")> _
    Public Shared Function BS_EnrollUserBEPlus(ByVal handle As Integer, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollMultipleUserBEPlus")> _
    Public Shared Function BS_EnrollMultipleUserBEPlus(ByVal handle As Integer, ByVal numofUser As Integer, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollUserEx")> _
    Public Shared Function BS_EnrollUserEx(ByVal handle As Integer, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollMultipleUserEx")> _
    Public Shared Function BS_EnrollMultipleUserEx(ByVal handle As Integer, ByVal numOfUser As Integer, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollUserDStation")> _
    Public Shared Function BS_EnrollUserDStation(ByVal handle As Integer, ByVal userHdr As IntPtr, ByVal fingerTemplate As Byte(), ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollMultipleUserDStation")> _
    Public Shared Function BS_EnrollMultipleUserDStation(ByVal handle As Integer, ByVal numOfUser As Integer, ByVal userHdr As IntPtr, ByVal fingerTemplate As Byte(), ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollUserXStation")> _
    Public Shared Function BS_EnrollUserXStation(ByVal handle As Integer, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollMultipleUserXStation")> _
    Public Shared Function BS_EnrollMultipleUserXStation(ByVal handle As Integer, ByVal numOfUser As Integer, ByVal userHdr As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollUserBioStation2")> _
    Public Shared Function BS_EnrollUserBioStation2(ByVal handle As Integer, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollMultipleUserBioStation2")> _
    Public Shared Function BS_EnrollMultipleUserBioStation2(ByVal handle As Integer, ByVal numOfUser As Integer, ByVal userHdr As IntPtr, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollUserFStation")> _
    Public Shared Function BS_EnrollUserFStation(ByVal handle As Integer, ByVal userHdr As IntPtr, ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_EnrollMultipleUserFStation")> _
    Public Shared Function BS_EnrollMultipleUserFStation(ByVal handle As Integer, ByVal numOfUser As Integer, ByVal userHdr As IntPtr, ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_GetLogCount")> _
    Public Shared Function BS_GetLogCount(ByVal handle As Integer, ByRef logCount As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadLog")> _
    Public Shared Function BS_ReadLog(ByVal handle As Integer, ByVal startTime As Integer, ByVal endTime As Integer, ByRef logCount As Integer, ByVal logRecord As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadLogEx")> _
    Public Shared Function BS_ReadLogEx(ByVal handle As Integer, ByVal startTime As Integer, ByVal endTime As Integer, ByRef logCount As Integer, ByVal logRecordEx As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadNextLog")> _
    Public Shared Function BS_ReadNextLog(ByVal handle As Integer, ByVal startTime As Integer, ByVal endTime As Integer, ByRef logCount As Integer, ByVal logRecord As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadNextLogEx")> _
    Public Shared Function BS_ReadNextLogEx(ByVal handle As Integer, ByVal startTime As Integer, ByVal endTime As Integer, ByRef logCount As Integer, ByVal logRecordEx As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_DeleteLog")> _
    Public Shared Function BS_DeleteLog(ByVal handle As Integer, ByVal logCount As Integer, ByRef deletedCount As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadCardIDEx")> _
    Public Shared Function BS_ReadCardIDEx(ByVal handle As Integer, ByRef cardID As UInt32, ByRef customID As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ScanTemplate")> _
    Public Shared Function BS_ScanTemplate(ByVal handle As Integer, ByVal templateData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ScanFaceTemplate")> _
    Public Shared Function BS_ScanFaceTemplate(ByVal handle As Integer, ByVal userTemplateHdr As IntPtr, ByVal imageData As Byte(), ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadOPModeConfig")> _
    Public Shared Function BS_ReadOPModeConfig(ByVal handle As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadDSOPModeConfig")> _
    Public Shared Function BS_ReadDSOPModeConfig(ByVal handle As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadXSOPModeConfig")> _
    Public Shared Function BS_ReadXSOPModeConfig(ByVal handle As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadBS2OPModeConfig")> _
    Public Shared Function BS_ReadBS2OPModeConfig(ByVal handle As Integer, ByVal data As IntPtr) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadFaceData")> _
    Public Shared Function BS_ReadFaceData(ByVal handle As Integer, ByRef imageLen As Integer, ByVal imageData As Byte(), ByVal faceTemplate As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ReadSpecificImageLog")> _
    Public Shared Function BS_ReadSpecificImageLog(ByVal handle As Integer, ByVal eventTime As Integer, ByVal eventType As Integer, ByRef imageLen As Integer, ByVal imageData As Byte()) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ConvertToUTF8")> _
    Public Shared Function BS_ConvertToUTF8(ByVal msg As Byte(), ByVal utf8Msg As Byte(), ByVal limitLen As Integer) As Integer
    End Function

    <DllImport("BS_SDK.dll", CharSet:=CharSet.Ansi, EntryPoint:="BS_ConvertToUTF16")> _
    Public Shared Function BS_ConvertToUTF16(ByVal msg As Byte(), ByVal utf16Msg As Byte(), ByVal limitLen As Integer) As Integer
    End Function

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BESysInfoData
        Public magicNo As UInt32
        Public version As Integer
        Public timestamp As UInt32
        Public checksum As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public headerReserved As Integer()

        Public ID As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public macAddr As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public boardVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public firmwareVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=40)> _
        Public reserved As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
Public Structure BESysInfoDataBLN
        Public magicNo As UInt32
        Public version As Integer
        Public timestamp As UInt32
        Public checksum As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public headerReserved As Integer()

        Public ID As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public macAddr As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public boardVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public firmwareVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public productName As Byte()
        Public language As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=31)> _
        Public reserved As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
  Public Structure BSSysInfoConfig
        Public ID As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public macAddr As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public productName As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public boardVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public firmwareVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public blackfinVer As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public kernelVer As Byte()

        Public language As Integer

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSDoor
        Public relay As Integer
        Public useRTE As Integer
        Public useDoorSensor As Integer
        Public openEvent As Integer
        ' only for BST
        Public openTime As Integer
        Public heldOpenTime As Integer
        Public forcedOpenSchedule As Integer
        Public forcedCloseSchedule As Integer
        Public RTEType As Integer
        Public sensorType As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reader As Short()
        Public useRTEEx As Byte
        Public useSoundForcedOpen As Byte
        Public useSoundHeldOpen As Byte
        Public openOnce As Byte
        Public RTE As Integer
        Public useDoorSensorEx As Byte
        Public alarmStatus As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved2 As Byte()
        Public doorSensor As Integer
        Public relayDeviceId As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSDoorConfig
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public door As BSDoor()
        Public apbType As Integer
        Public apbResetTime As Integer
        Public doorMode As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSInputFunction
        Public functionType As Integer
        Public minimumDuration As Short
        Public switchType As Short
        Public timeSchedule As Integer
        Public deviceID As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public reserved As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSInputConfig
        ' host inputs
        Public hostTamper As BSInputFunction
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public hostInput As BSInputFunction()

        ' secure I/O
        'BSInputFunction secureIO[4][4];
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public secureIO As BSInputFunction()

        ' slave
        Public slaveTamper As BSInputFunction
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public slaveInput As BSInputFunction()

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public reserved As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSOutputEvent
        Public nevent As UInt32
        ' (8 bit input device ID << 16) | 16 bit event ID 
        Public outputDeviceID As Byte
        Public outputRelayID As Byte
        Public relayOn As Byte
        Public reserved1 As Byte
        Public delay As UShort
        Public high As UShort
        Public low As UShort
        Public count As UShort
        Public priority As Integer
        ' 1(highest) ~ 99(lowest)
        Public deviceID As UInt32
        Public outputType As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=1)> _
        Public reserved2 As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSEMOutputEvent
        Public inputType As UShort
        Public outputRelayID As UShort
        Public inputDuration As UShort
        Public high As UShort
        Public low As UShort
        Public count As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public reserved3 As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSOutputConfig
        Public numOfEvent As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=200 - 16)> _
        Public outputEvent As BSOutputEvent()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public emergencyEvent As BSEMOutputEvent()
        'BSEMOutputEvent emergencyEvent[BSInputConfig::NUM_OF_SECURE_IO][BSInputConfig::NUM_OF_SECURE_INPUT];

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=31)> _
        Public reserved As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BECommandCard
        Public cardID As UInt32
        Public customID As Byte
        Public commandType As Byte
        Public needAdminFinger As Byte
        Public reserved As Byte
        Public fullCardCustomID As UInt32
    End Structure

    Public Enum BS_WIEGAND_FORMAT
        BS_WIEGAND_26BIT = &H1
        BS_WIEGAND_PASS_THRU = &H2
        BS_WIEGAND_CUSTOM = &H3
    End Enum

    Public Enum BS_WIEGAND_PARITY_TYPE
        BS_WIEGAND_EVEN_PARITY = 0
        BS_WIEGAND_ODD_PARITY = 1
    End Enum

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
Public Structure BSWiegandField
        Public bitIndex As Integer
        Public bitLength As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSWiegandParity
        Public bitIndex As Integer
        Public type As BS_WIEGAND_PARITY_TYPE
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public bitMask As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSWiegandFormatHeader
        Public format As BS_WIEGAND_FORMAT
        Public totalBits As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSWiegandPassThruData
        Public numOfIDField As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public field As BSWiegandField()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSWiegandCustomData
        Public numOfField As Integer
        Public idFieldMask As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public field As BSWiegandField()
        Public numOfParity As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public parity As BSWiegandParity()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSWiegandFormatData
        'BSWiegandPassThruData	passThruData;
        Public customData As BSWiegandCustomData
    End Structure

    '
    '        typedef union {
    '	        BSWiegandPassThruData	passThruData;
    '	        BSWiegandCustomData		customData;
    '        } BSWiegandFormatData;
    '  

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
Public Structure BSWiegandConfig
        Public outWidth As UInt32
        Public outInterval As UInt32
        Public header As BSWiegandFormatHeader
        Public data As BSWiegandFormatData
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public fieldValue As UInt32()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BEOutputPattern

        Public Enum OUTPUTPATTERN
            MAX_ARG = 3
            RED = &H0
            YELLOW
            GREEN
            CYAN
            BLUE
            MAGENTA
            WHITE
            FADEOUT = &H1000000
            HIGH_FREQ = 3136
            MIDDLE_FREQ = 2637
            LOW_FREQ = 2093
        End Enum

        Public repeat As Integer
        ' 0: indefinite, -1: don't user
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public arg As Integer()
        ' color for LED, frequency for Buzzer, -1 for last 
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public high As Short()
        ' msec
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public low As Short()
        ' msec
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BELEDBuzzerConfig
        Enum LEDDEFINE
            MAX_SIGNAL = 48
            STATUS_NORMAL = &H0
            STATUS_LOCKED
            STATUS_TAMPER_ON
            STATUS_RTC_ERROR
            STATUS_WAITING_INPUT
            STATUS_WAITING_DHCP
            STATUS_DHCP_FAILED
            STATUS_WRONG_CRYPT
            STATUS_ARMED
            SCAN_FINGER = &HC
            SCAN_CARD

            ' Output Event: Same as BSOutputEvent
            AUTH_SUCCESS = &H10
            AUTH_FAIL
            AUTH_DURESS
            ANTI_PASSBACK_FAIL
            ACCESS_NOT_GRANTED
            ENTRANCE_LIMITATION
            ADMIN_AUTH_SUCCESS
            TAMPER_ON
            DOOR_OPEN
            DOOR_CLOSED
            DOOR_FORCED_OPEN
            DOOR_HELD_OPEN_WARNING

            INPUT0_ON
            INPUT1_ON
            INPUT2_ON
            INPUT3_ON

            ARM
            DISARM

            ALARM_ON

            ' Generic Event
            GENERIC_SUCCESS = &H2A
            GENERIC_FAIL

            ARM_DISARM_READY
        End Enum

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
  Public reserved As Integer()

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public ledPattern As BEOutputPattern()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public buzzerPattern As BEOutputPattern()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public signalReserved As UShort()

    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
Public Structure BEConfigData
        ' header
        Public magicNo As UInt32
        Public version As Integer
        Public timestamp As UInt32
        Public checksum As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public headerReserved As Integer()

        ' operation mode
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public opMode As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public opModeSchedule As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public opDoubleMode As Byte()
        Public opModePerUser As Integer
        ' PROHIBITED, ALLOWED 
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public opReserved As Integer()

        ' ip
        <MarshalAs(UnmanagedType.I1)> _
        Public useDHCP As Boolean
        Public ipAddr As UInt32
        Public gateway As UInt32
        Public subnetMask As UInt32
        Public serverIpAddr As UInt32
        Public port As Integer
        <MarshalAs(UnmanagedType.I1)> _
        Public useServer As Boolean
        <MarshalAs(UnmanagedType.I1)> _
        Public synchTime As Boolean
        Public support100BaseT As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public ipReserved As Integer()

        ' fingerprint
        Public securityLevel As Integer
        Public fastMode As Integer
        Public fingerReserved1 As Integer
        Public timeout As Integer
        ' 1 ~ 20 sec
        Public matchTimeout As Integer
        ' Infinite(0) ~ 10 sec
        Public templateType As Integer
        Public fakeDetection As Integer
        <MarshalAs(UnmanagedType.I1)> _
        Public useServerMatching As Boolean
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public fingerReserved As Integer()

        ' I/O
        Public inputConfig As BSInputConfig
        Public outputConfig As BSOutputConfig
        Public doorConfig As BSDoorConfig
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public ioReserved As Integer()

        'extended serial
        Public hostID As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public slaveIDEx As UInt32()
        Public slaveType As UInt32
        ' 0 : BST, 1 : BEPL
        ' serial
        Public serialMode As Integer
        Public serialBaudrate As Integer
        Public serialReserved1 As Byte
        Public secureIO As Byte
        ' 0x01 - Secure I/O 0, 0x02, 0x04, 0x08
        Public useTermination As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public serialReserved2 As Byte()
        Public slaveID As UInt32
        ' 0 for no slave
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=17)> _
        Public reserved1 As Integer()

        ' entrance limit
        Public minEntryInterval As Integer
        ' 0 for no limit
        Public numOfEntranceLimit As Integer
        ' MAX 4
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public maxEntry As Integer()
        ' 0 (no limit) ~ 16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public entryLimitInterval As UInt32()
        Public bypassGroupId As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public entranceLimitReserved As Integer()

        ' command card
        Public numOfCommandCard As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public commandCard As BECommandCard()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public commandCardReserved As Integer()

        ' tna
        Public tnaMode As Integer
        Public autoInSchedule As Integer
        Public autoOutSchedule As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public tnaReserved As Integer()

        ' user
        Public defaultAG As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public userReserved As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=21)> _
        Public reserved2 As Integer()

        Public isLocked As Integer

        ' wiegand
        <MarshalAs(UnmanagedType.I1)> _
        Public useWiegandOutput As Boolean
        Public useWiegandInput As Integer
        Public wiegandMode As Integer
        Public wiegandReaderID As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public wiegandReserved As Integer()
        Public wiegandIdType As Integer
        Public wiegandConfig As BSWiegandConfig

        ' LED/Buzzer
        Public ledBuzzerConfig As BELEDBuzzerConfig

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=38)> _
        Public reserved3 As Integer()

        Public cardIdFormatType As Integer
        ' CARD_ID_FORMAT_NORMAL, CARD_ID_FORMAT_WIEGAND 
        Public cardIdByteOrder As Integer
        ' CARD_ID_MSB, CARD_ID_LSB 
        Public cardIdBitOrder As Integer
        ' CARD_ID_MSB, CARD_ID_LSB 

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=174)> _
        Public padding As Integer()

    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BEConfigDataBLN
        ' header
        Public magicNo As UInt32
        Public version As Integer
        Public timestamp As UInt32
        Public checksum As UInt32
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public headerReserved As Integer()

        ' operation mode
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public opMode As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public opModeSchedule As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public opDualMode As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public opReserved1 As Byte()
        Public opModePerUser As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public identificationMode As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public identificationModeSchedule As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=1)> _
        Public opReserved2 As Integer()

        ' ip
        <MarshalAs(UnmanagedType.I1)> _
        Public useDHCP As Boolean
        Public ipAddr As UInt32
        Public gateway As UInt32
        Public subnetMask As UInt32
        Public serverIpAddr As UInt32
        Public port As Integer
        <MarshalAs(UnmanagedType.I1)> _
        Public useServer As Boolean
        <MarshalAs(UnmanagedType.I1)> _
        Public synchTime As Boolean
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public ipReserved As Integer()

        ' fingerprint
        Public imageQuality As Integer
        Public securityLevel As Integer
        Public fastMode As Integer
        Public fingerReserved1 As Integer
        Public timeout As Integer
        ' 1 ~ 20 sec
        Public matchTimeout As Integer
        ' Infinite(0) ~ 10 sec
        Public templateType As Integer
        Public fakeDetection As Integer
        <MarshalAs(UnmanagedType.I1)> _
        Public useServerMatching As Boolean
        <MarshalAs(UnmanagedType.I1)> _
        Public useCheckDuplicate As Boolean
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public fingerReserved2 As Integer()

        ' padding
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3930)> _
        Public padding As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSIPConfig
        Public lanType As Integer
        <MarshalAs(UnmanagedType.I1)> _
        Public useDHCP As Boolean
        Public port As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public ipAddr As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public gateway As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public subnetMask As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public serverIP As Byte()

        Public maxConnection As Integer
        <MarshalAs(UnmanagedType.I1)> _
        Public useServer As Boolean
        Public serverPort As UInt32
        <MarshalAs(UnmanagedType.I1)> _
        Public syncTimeWithServer As Boolean

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BEUserHdr
        Public version As Integer
        Public userID As UInt32

        Public startTime As Integer
        Public expiryTime As Integer

        Public cardID As UInt32
        Public cardCustomID As Byte
        Public commandCardFlag As Byte
        Public cardFlag As Byte
        Public cardVersion As Byte

        Public adminLevel As UShort
        Public securityLevel As UShort

        Public accessGroupMask As UInt32

        Public numOfFinger As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public fingerChecksum As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public isDuress As Byte()

        Public disabled As Integer
        Public opMode As Integer
        Public dualMode As Integer

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public password As Byte()

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=15)> _
        Public reserved2 As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSUserHdrEx
        Public ID As UInt32

        Public headerVersion As UShort
        Public adminLevel As UShort
        Public securityLevel As UShort
        Public statusMask As UShort
        Public accessGroupMask As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=33)> _
        Public name As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=33)> _
        Public department As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=17)> _
        Public password As Byte()

        Public numOfFinger As UShort
        Public duressMask As UShort

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public checksum As UShort()
        ' index 0, 1
        Public authMode As UShort
        Public authLimitCount As UShort
        ' 0 for no limit 
        Public reserved As UShort
        Public timedAntiPassback As UShort
        ' in minutes. 0 for no limit 
        Public cardID As UInt32
        ' 0 for not used
        Public bypassCard As Byte
        Public disabled As Byte
        Public expireDateTime As UInt32
        Public customID As Integer
        'card Custom ID
        Public version As Integer
        ' card Info Version
        Public startDateTime As UInt32
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure DSUserHdr
        Public Enum [ENUM]
            DS_MAX_NAME_LEN = 48
            DS_MAX_PASSWORD_LEN = 16
            DS_MIN_PASSWORD_LEN = 4

            DS_TEMPLATE_SIZE = 384
            DS_FACE_TEMPLATE_SIZE = 2284

            MAX_FINGER = 10
            MAX_FINGER_TEMPLATE = 20
            MAX_FACE = 5
            MAX_FACE_TEPLATE = 5

            USER_ADMIN = 1
            USER_NORMAL = 0
        End Enum

        Public ID As UInt32

        Public headerVersion As UShort
        Public adminLevel As UShort
        Public securityLevel As UShort
        Public statusMask As UShort
        Public accessGroupMask As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public name As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public department As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public password As UShort()

        Public numOfFinger As UShort
        Public numOfFace As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public duress As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public fingerType As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved1 As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public fingerChecksum As UInt32()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public faceChecksum As UInt32()

        Public authMode As UShort
        Public bypassCard As Byte
        Public disabled As Byte

        Public cardID As UInt32
        'card ID
        Public customID As UInt32
        'card Custom ID
        Public startDateTime As UInt32
        Public expireDateTime As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public reserved2 As UInt32()

    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
Public Structure XSUserHdr
        Public Enum [ENUM]
            DS_MAX_NAME_LEN = 48
            DS_MAX_PASSWORD_LEN = 16
            DS_MIN_PASSWORD_LEN = 4

            DS_TEMPLATE_SIZE = 384
            DS_FACE_TEMPLATE_SIZE = 2284

            MAX_FINGER = 10
            MAX_FINGER_TEMPLATE = 20
            MAX_FACE = 5
            MAX_FACE_TEPLATE = 5

            USER_ADMIN = 1
            USER_NORMAL = 0
        End Enum

        Public ID As UInt32

        Public headerVersion As UShort
        Public adminLevel As UShort
        Public securityLevel As UShort
        Public statusMask As UShort
        Public accessGroupMask As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public name As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public department As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public password As UShort()

        Public numOfFinger As UShort
        Public numOfFace As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public duress As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public fingerType As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved1 As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public fingerChecksum As UInt32()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public faceChecksum As UInt32()

        Public authMode As UShort
        Public bypassCard As Byte
        Public disabled As Byte

        Public cardID As UInt32
        'card ID
        Public customID As UInt32
        'card Custom ID
        Public startDateTime As UInt32
        Public expireDateTime As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public reserved2 As UInt32()

    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BS2UserHdr
        Public Enum [ENUM]
            DS_MAX_NAME_LEN = 48
            DS_MAX_PASSWORD_LEN = 16
            DS_MIN_PASSWORD_LEN = 4

            DS_TEMPLATE_SIZE = 384
            DS_FACE_TEMPLATE_SIZE = 2284

            MAX_FINGER = 10
            MAX_FINGER_TEMPLATE = 20
            MAX_FACE = 5
            MAX_FACE_TEPLATE = 5

            USER_ADMIN = 1
            USER_NORMAL = 0
        End Enum

        Public ID As UInt32

        Public headerVersion As UShort
        Public adminLevel As UShort
        Public securityLevel As UShort
        Public statusMask As UShort
        Public accessGroupMask As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public name As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public department As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public password As UShort()

        Public numOfFinger As UShort
        Public numOfFace As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public duress As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public fingerType As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public reserved1 As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public fingerChecksum As UInt32()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public faceChecksum As UInt32()

        Public authMode As UShort
        Public bypassCard As Byte
        Public disabled As Byte

        Public cardID As UInt32
        'card ID
        Public customID As UInt32
        'card Custom ID
        Public startDateTime As UInt32
        Public expireDateTime As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public reserved2 As UInt32()

    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure FSUserHdr
        Public Enum [ENUM]
            MAX_NAME_LEN = 48
            MAX_PASSWORD_LEN = 16
            MIN_PASSWORD_LEN = 4

            MAX_FACE = 25
            FACE_TEMPLATE_SIZE = 2000
            MAX_FACE_RAW = 20
            FACE_RAW_TEMPLATE_SIZE = 37500
            MAX_IMAGE_SIZE = 8 * 1024

            USER_NORMAL = 0
            USER_ADMIN = 1

            USER_SECURITY_DEFAULT = 0
            USER_SECURITY_LOWER = 1
            USER_SECURITY_LOW = 2
            USER_SECURITY_NORMAL = 3
            USER_SECURITY_HIGH = 4
            USER_SECURITY_HIGHER = 5
        End Enum
        Public ID As UInt32

        Public headerVersion As UShort
        Public adminLevel As UShort
        Public securityLevel As UShort
        Public statusMask As UShort
        Public accessGroupMask As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public name As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public department As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public password As UShort()

        Public numOfFace As UShort
        Public numOfUpdatedFace As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=25)> _
        Public faceLen As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> _
        Public faceTemp As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=25)> _
        Public faceChecksum As UInt32()

        Public authMode As UShort
        Public bypassCard As Byte
        Public disabled As Byte

        Public cardID As UInt32
        'card ID
        Public customID As UInt32
        'card Custom ID
        Public startDateTime As UInt32
        Public expireDateTime As UInt32

        Public faceUpdatedIndex As UShort

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=40)> _
        Public reserved As UShort()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure FSUserTemplateHdr
        Public imageSize As UShort

        Public numOfFace As UShort
        Public numOfUpdatedFace As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=25)> _
        Public faceLen As UShort()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=256)> _
        Public faceTemp As Byte()
        Public numOfRawFace As UShort
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=20)> _
        Public rawfaceLen As UInt32()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSAccessGroupEx
        Public groupID As Integer

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)> _
        Public name As [String]
        Public numOfReader As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public readerID As UInt32()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public scheduleID As Integer()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public reserved As Integer()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSLogRecord
        Public eventType As Byte
        Public subEvent As Byte
        Public tnaEvent As UShort
        Public eventTime As Integer
        Public userID As UInt32
        Public reserved As UInt32
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSLogRecordEx
        Public eventType As Byte
        Public subEvent As Byte
        Public tnaEvent As UShort
        Public eventTime As Integer
        Public userID As UInt32
        Public deviceID As UInt32
        Public imageSlot As UShort
        ' NO_IMAGE for no image, WRITE_ERROR for error
        Public reserved1 As UShort
        Private reserved2 As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSImageLogHdr
        Public eventType As Byte
        Public subEvent As Byte
        Public reserved1 As UShort
        Public eventTime As Integer
        Public userID As UInt32
        Public imageSize As UInt32
        Public deviceID As UInt32
        Public reserved2 As UInt32
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BSOPModeConfig
        Public authMode As Integer
        Public identificationMode As Integer
        Public tnaMode As Integer
        Public tnaChange As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public authSchedule As Byte()
        Public identificationSchedule As Byte
        <MarshalAs(UnmanagedType.I1)> _
        Public dualMode As Boolean
        Public dualSchedule As Byte
        Public version As Byte
        Public cardMode As UShort
        <MarshalAs(UnmanagedType.I1)> _
        Public useFastIDMatching As Boolean
        Public cardIdFormatType As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=1)> _
        Public authScheduleEx As Byte()
        <MarshalAs(UnmanagedType.I1)> _
        Public usePrivateAuthMode As Boolean
        Public cardIdByteOrder As Byte
        Public cardIdBitOrder As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure DSOPModeConfig
        Public identificationMode As Integer
        Public tnaMode As Integer
        Public tnaChange As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public authSchedule As Byte()
        Public identificationSchedule As Byte
        Public dualSchedule As Byte
        Public usePrivateAuthMode As Byte
        Public cardIdFormatType As Byte

        Public cardIdByteOrder As Byte
        Public cardIdBitOrder As Byte
        Public enhancedMode As Byte
        Public fusionType As Byte

        Public fusionTimeout As Byte
        Public useDetectFace As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=82)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure XSOPModeConfig
        Public reserved1 As Byte
        Public tnaMode As Byte
        Public tnaChange As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public authSchedule As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public reserved2 As Byte()
        Public dualSchedule As Byte
        Public usePrivateAuthMode As Byte
        Public cardIdFormatType As Byte

        Public cardIdByteOrder As Byte
        Public cardIdBitOrder As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public reserved3 As Byte()

        Public useDetectFace As Byte
        Public useServerMatching As Byte
        Public matchTimeout As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=80)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure BS2OPModeConfig
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public fingerAuthSchedule As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public cardAuthSchedule As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public idAuthSchedule As Byte()

        Public tnaMode As Byte
        Public cardMode As Byte
        Public dualSchedule As Byte
        Public usePrivateAuthMode As Byte
        Public cardIdFormatType As Byte

        Public cardIdByteOrder As Byte
        Public cardIdBitOrder As Byte
        Public useDetectFace As Byte
        Public useServerMatching As Byte
        Public matchTimeout As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=77)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure FSOPModeConfig
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=6)> _
        Public faceAuthSchedule As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public cardAuthSchedule As Byte()
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public idAuthSchedule As Byte()

        Public tnaMode As Byte
        Public cardMode As Byte
        Public dualSchedule As Byte
        Public usePrivateAuthMode As Byte
        Public cardIdFormatType As Byte

        Public cardIdByteOrder As Byte
        Public cardIdBitOrder As Byte
        Public useDetectFace As Byte
        Public useServerMatching As Byte
        Public matchTimeout As Byte
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=77)> _
        Public reserved As Byte()
    End Structure

   
    Public Const BS_SUCCESS As Integer = 0
    Public Const BS_ERR_NOT_FOUND As Integer = -306

    Public Const BS_DEVICE_BIOSTATION As Integer = 0
    Public Const BS_DEVICE_BIOENTRY_PLUS As Integer = 1
    Public Const BS_DEVICE_BIOLITE As Integer = 2
    Public Const BS_DEVICE_XPASS As Integer = 3
    Public Const BS_DEVICE_DSTATION As Integer = 4
    Public Const BS_DEVICE_XSTATION As Integer = 5
    Public Const BS_DEVICE_BIOSTATION2 As Integer = 6
    Public Const BS_DEVICE_XPASS_SLIM As Integer = 7
    Public Const BS_DEVICE_FSTATION As Integer = 10
    Public Const BS_DEVICE_BIOENTRY_W As Integer = 11

    Public Const BEPLUS_CONFIG As Integer = &H50
    Public Const BEPLUS_CONFIG_SYS_INFO As Integer = &H51

    Public Const BLN_CONFIG As Integer = &H70
    Public Const BLN_CONFIG_SYS_INFO As Integer = &H71

    Public Const BS_CONFIG_SYS_INFO As Integer = &H41
    Public Const BS_CONFIG_TCPIP As Integer = &H10

    Public Const NO_ACCESS_GROUP As Integer = &HFD
    Public Const FULL_ACCESS_GROUP As Integer = &HFE

    Public Const BS_AUTH_MODE_DISABLED As Integer = 0
    Public Const BS_AUTH_FINGER_ONLY As Integer = 1020
    Public Const BS_AUTH_FINGER_N_PASSWORD As Integer = 1021
    Public Const BS_AUTH_FINGER_OR_PASSWORD As Integer = 1022
    Public Const BS_AUTH_PASS_ONLY As Integer = 1023
    Public Const BS_AUTH_CARD_ONLY As Integer = 1024


    'Private Auth
    Public Const PAUTH_FACE_ONLY As Integer = 0
    Public Const PAUTH_FACE_PIN As Integer = 1
    Public Const PAUTH_CARD_ONLY As Integer = 2
    Public Const PAUTH_CARD_PIN As Integer = 3
    Public Const PAUTH_CARD_FACE As Integer = 4
    Public Const PAUTH_CARD_FACE_PIN As Integer = 5
    Public Const PAUTH_CARD_FACE_N_PIN As Integer = 6
    Public Const PAUTH_ID_PIN As Integer = 7
    Public Const PAUTH_ID_FACE As Integer = 8
    Public Const PAUTH_ID_FACE_PIN As Integer = 9
    Public Const PAUTH_ID_FACE_N_PIN As Integer = 10
    Public Const PAUTH_FACE_KEY As Integer = 11
    Public Const PAUTH_FACE_PIN_KEY As Integer = 12

    Public Const BE_CARD_VERSION_1 As Integer = &H13

    ' user levels for BioStation
    Public Const BS_USER_ADMIN As Integer = 240
    Public Const BS_USER_NORMAL As Integer = 241

    ' security levels for BioStation
    Public Const BS_USER_SECURITY_DEFAULT As Integer = 260
    Public Const BS_USER_SECURITY_LOWER As Integer = 261
    Public Const BS_USER_SECURITY_LOW As Integer = 262
    Public Const BS_USER_SECURITY_NORMAL As Integer = 263
    Public Const BS_USER_SECURITY_HIGH As Integer = 264
    Public Const BS_USER_SECURITY_HIGHER As Integer = 265

    ' log events
    Public Const BE_EVENT_SCAN_SUCCESS As Integer = &H58
    Public Const BE_EVENT_ENROLL_BAD_FINGER As Integer = &H16
    Public Const BE_EVENT_ENROLL_SUCCESS As Integer = &H17
    Public Const BE_EVENT_ENROLL_FAIL As Integer = &H18
    Public Const BE_EVENT_ENROLL_CANCELED As Integer = &H19

    Public Const BE_EVENT_VERIFY_BAD_FINGER As Integer = &H26
    Public Const BE_EVENT_VERIFY_SUCCESS As Integer = &H27
    Public Const BE_EVENT_VERIFY_FAIL As Integer = &H28
    Public Const BE_EVENT_VERIFY_CANCELED As Integer = &H29
    Public Const BE_EVENT_VERIFY_NO_FINGER As Integer = &H2A

    Public Const BE_EVENT_IDENTIFY_BAD_FINGER As Integer = &H36
    Public Const BE_EVENT_IDENTIFY_SUCCESS As Integer = &H37
    Public Const BE_EVENT_IDENTIFY_FAIL As Integer = &H38
    Public Const BE_EVENT_IDENTIFY_CANCELED As Integer = &H39
    Public Const BE_EVENT_IDENTIFY_FINGER As Integer = &H3A
    Public Const BE_EVENT_IDENTIFY_FINGER_PIN As Integer = &H3B

    Public Const BE_EVENT_DELETE_BAD_FINGER As Integer = &H46
    Public Const BE_EVENT_DELETE_SUCCESS As Integer = &H47
    Public Const BE_EVENT_DELETE_FAIL As Integer = &H48
    Public Const BE_EVENT_DELETE_ALL_SUCCESS As Integer = &H49

    Public Const BE_EVENT_VERIFY_DURESS As Integer = &H62
    Public Const BE_EVENT_IDENTIFY_DURESS As Integer = &H63

    Public Const BE_EVENT_TAMPER_SWITCH_ON As Integer = &H64
    Public Const BE_EVENT_TAMPER_SWITCH_OFF As Integer = &H65

    Public Const BE_EVENT_SYS_STARTED As Integer = &H6A
    Public Const BE_EVENT_IDENTIFY_NOT_GRANTED As Integer = &H6D
    Public Const BE_EVENT_VERIFY_NOT_GRANTED As Integer = &H6E

    Public Const BE_EVENT_IDENTIFY_LIMIT_COUNT As Integer = &H6F
    Public Const BE_EVENT_IDENTIFY_LIMIT_TIME As Integer = &H70

    Public Const BE_EVENT_IDENTIFY_DISABLED As Integer = &H71
    Public Const BE_EVENT_IDENTIFY_EXPIRED As Integer = &H72

    Public Const BE_EVENT_APB_FAIL As Integer = &H73
    Public Const BE_EVENT_COUNT_LIMIT As Integer = &H74
    Public Const BE_EVENT_TIME_INTERVAL_LIMIT As Integer = &H75
    Public Const BE_EVENT_INVALID_AUTH_MODE As Integer = &H76
    Public Const BE_EVENT_EXPIRED_USER As Integer = &H77
    Public Const BE_EVENT_NOT_GRANTED As Integer = &H78

    Public Const BE_EVENT_DETECT_INPUT0 As Integer = &H54
    Public Const BE_EVENT_DETECT_INPUT1 As Integer = &H55

    Public Const BE_EVENT_TIMEOUT As Integer = &H90

    Public Const BS_EVENT_RELAY_ON As Integer = &H80
    Public Const BS_EVENT_RELAY_OFF As Integer = &H81

    Public Const BE_EVENT_DOOR0_OPEN As Integer = &H82
    Public Const BE_EVENT_DOOR1_OPEN As Integer = &H83
    Public Const BE_EVENT_DOOR0_CLOSED As Integer = &H84
    Public Const BE_EVENT_DOOR1_CLOSED As Integer = &H85

    Public Const BE_EVENT_DOOR0_FORCED_OPEN As Integer = &H86
    Public Const BE_EVENT_DOOR1_FORCED_OPEN As Integer = &H87

    Public Const BE_EVENT_DOOR0_HELD_OPEN As Integer = &H88
    Public Const BE_EVENT_DOOR1_HELD_OPEN As Integer = &H89

    Public Const BE_EVENT_DOOR0_RELAY_ON As Integer = &H8A
    Public Const BE_EVENT_DOOR1_RELAY_ON As Integer = &H8B

    Public Const BE_EVENT_INTERNAL_INPUT0 As Integer = &HA0
    Public Const BE_EVENT_INTERNAL_INPUT1 As Integer = &HA1
    Public Const BE_EVENT_SECONDARY_INPUT0 As Integer = &HA2
    Public Const BE_EVENT_SECONDARY_INPUT1 As Integer = &HA3

    Public Const BE_EVENT_SIO0_INPUT0 As Integer = &HB0
    Public Const BE_EVENT_SIO0_INPUT1 As Integer = &HB1
    Public Const BE_EVENT_SIO0_INPUT2 As Integer = &HB2
    Public Const BE_EVENT_SIO0_INPUT3 As Integer = &HB3

    Public Const BE_EVENT_SIO1_INPUT0 As Integer = &HB4
    Public Const BE_EVENT_SIO1_INPUT1 As Integer = &HB5
    Public Const BE_EVENT_SIO1_INPUT2 As Integer = &HB6
    Public Const BE_EVENT_SIO1_INPUT3 As Integer = &HB7

    Public Const BE_EVENT_SIO2_INPUT0 As Integer = &HB8
    Public Const BE_EVENT_SIO2_INPUT1 As Integer = &HB9
    Public Const BE_EVENT_SIO2_INPUT2 As Integer = &HBA
    Public Const BE_EVENT_SIO2_INPUT3 As Integer = &HBB

    Public Const BE_EVENT_SIO3_INPUT0 As Integer = &HBC
    Public Const BE_EVENT_SIO3_INPUT1 As Integer = &HBD
    Public Const BE_EVENT_SIO3_INPUT2 As Integer = &HBE
    Public Const BE_EVENT_SIO3_INPUT3 As Integer = &HBF

    Public Const BE_EVENT_LOCKED As Integer = &HC0
    Public Const BE_EVENT_UNLOCKED As Integer = &HC1

    Public Const BE_EVENT_TIME_SET As Integer = &HD2
    Public Const BE_EVENT_SOCK_CONN As Integer = &HD3
    Public Const BE_EVENT_SOCK_DISCONN As Integer = &HD4
    Public Const BE_EVENT_SERVER_SOCK_CONN As Integer = &HD5
    Public Const BE_EVENT_SERVER_SOCK_DISCONN As Integer = &HD6
    Public Const BE_EVENT_LINK_CONN As Integer = &HD7
    Public Const BE_EVENT_LINK_DISCONN As Integer = &HD8
    Public Const BE_EVENT_INIT_IP As Integer = &HD9
    Public Const BE_EVENT_INIT_DHCP As Integer = &HDA
    Public Const BE_EVENT_DHCP_SUCCESS As Integer = &HDB

    Public Const DF_LEN_MAX_IMAGE As Integer = 256 * 1024


    ' communication channel error

    Public Const BS_ERR_NO_AVAILABLE_CHANNEL As Integer = -100
    Public Const BS_ERR_INVALID_COMM_HANDLE As Integer = -101
    Public Const BS_ERR_CANNOT_WRITE_CHANNEL As Integer = -102
    Public Const BS_ERR_WRITE_CHANNEL_TIMEOUT As Integer = -103
    Public Const BS_ERR_CANNOT_READ_CHANNEL As Integer = -104
    Public Const BS_ERR_READ_CHANNEL_TIMEOUT As Integer = -105
    Public Const BS_ERR_CHANNEL_OVERFLOW As Integer = -106
    Public Const BS_ERR_CHANNEL_CLOSED As Integer = -107


    ' socket error
    Public Const BS_ERR_CANNOT_INIT_SOCKET As Integer = -200
    Public Const BS_ERR_CANNOT_OPEN_SOCKET As Integer = -201
    Public Const BS_ERR_CANNOT_CONNECT_SOCKET As Integer = -202
    Public Const BS_ERR_SOCKET_CLOSED As Integer = -203


    ' serial error

    Public Const BS_ERR_CANNOT_OPEN_SERIAL As Integer = -220


    ' USB error

    Public Const BS_ERR_CANNOT_OPEN_USB As Integer = -240


    ' USB memory error

    Public Const BS_ERR_INVALID_USB_MEMORY As Integer = -260
    Public Const BS_ERR_NO_MORE_USB_MEMORY As Integer = -261
    Public Const BS_ERR_CANNOT_FIND_USB_MEMORY As Integer = -262
    Public Const BS_ERR_VT_EXIST_IN_MEMORY As Integer = -263
    Public Const BS_ERR_USB_MEMORY_FULL As Integer = -264
    Public Const BS_ERR_NO_MORE_VT As Integer = -265


    ' generic command error code

    Public Const BS_ERR_BUSY As Integer = -300
    Public Const BS_ERR_INVALID_PACKET As Integer = -301
    Public Const BS_ERR_CHECKSUM As Integer = -302
    Public Const BS_ERR_UNSUPPORTED As Integer = -303
    Public Const BS_ERR_FILE_IO As Integer = -304
    Public Const BS_ERR_DISK_FULL As Integer = -305
    'Public Const BS_ERR_NOT_FOUND As Integer = -306
    Public Const BS_ERR_INVALID_PARAM As Integer = -307
    Public Const BS_ERR_RTC As Integer = -308
    Public Const BS_ERR_MEM_FULL As Integer = -309
    Public Const BS_ERR_DB_FULL As Integer = -310
    Public Const BS_ERR_INVALID_ID As Integer = -311
    Public Const BS_ERR_USB_DISABLED As Integer = -312
    Public Const BS_ERR_COM_DISABLED As Integer = -313
    Public Const BS_ERR_WRONG_PASSWORD As Integer = -314
    Public Const BS_ERR_TRY_AGAIN As Integer = -315
    Public Const BS_ERR_EXIST_FINGER As Integer = -316


    ' user related error

    Public Const BS_ERR_NO_USER As Integer = -320
    Public Const BS_ERR_CANNOT_CHANGE_IMG_VIEW As Integer = -321

    ' Public Const  server error

    Public Const BS_ERR_NO_MORE_TERMINAL As Integer = -400
    Public Const BS_ERR_TERMINAL_NOT_FOUND As Integer = -401
    Public Const BS_ERR_TERMINAL_COMM_ERROR As Integer = -402
    Public Const BS_ERR_TERMINAL_NOT_AUTHORIZED As Integer = -403
    Public Const BS_ERR_TERMINAL_BUSY As Integer = -404


    ' server db error

    Public Const BS_ERR_DB_NOT_EXIST As Integer = -500
    Public Const BS_ERR_CANNOT_CONNECT_TO_DB As Integer = -501
    Public Const BS_ERR_DB_INTERNAL_ERROR As Integer = -502


    ' SSL error

    Public Const BS_ERR_CANNOT_INIT_SSL As Integer = -601
    Public Const BS_ERR_SSL_INVALID_CTX As Integer = -602

    Public Const BS_ERR_SSL_INVALID_CERTFILE As Integer = -603
    Public Const BS_ERR_SSL_INVALID_KEYFILE As Integer = -604
    Public Const BS_ERR_SSL_INVALID_CAFILE As Integer = -605
    Public Const BS_ERR_SSL_INVALID_PATH As Integer = -606
    Public Const BS_ERR_SSL_CANNOT_CONNECT As Integer = -607
    Public Const BS_ERR_INVALID_DATA As Integer = -608
    Public Const BS_ERR_UNKNOWN As Integer = -9999

End Class
