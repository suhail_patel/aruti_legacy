﻿'************************************************************************************************************************************
'Class Name : clsapprover_Usermapping.vb
'Purpose    :
'Date       :18/09/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 1
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsapprover_Usermapping
    Private Shared ReadOnly mstrModuleName As String = "clsapprover_Usermapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMappingunkid As Integer
    Private mintApproverunkid As Integer
    Private mintUserunkid As Integer
    Private mintUserTypeid As Integer

    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mintAuditUserunkid As Integer = 0
    'Pinkal (06-Apr-2013) -- End

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = Value
            Call GetData(mintUserTypeid, , , mintMappingunkid)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set UserTypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _UserTypeid() As Integer
        Get
            Return mintUserTypeid
        End Get
        Set(ByVal value As Integer)
            mintUserTypeid = value
        End Set
    End Property


    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserunkid() As Integer
        Get
            Return mintAuditUserunkid
        End Get
        Set(ByVal value As Integer)
            mintAuditUserunkid = value
        End Set
    End Property

    'Pinkal (06-Apr-2013) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal intUsertypeid As Integer, Optional ByVal intApproverunkid As Integer = -1, Optional ByVal intUserunkid As Integer = -1, Optional ByVal intunkid As Integer = -1)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (06-Apr-2013) -- Start
        'Enhancement : TRA Changes

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        'Pinkal (06-Apr-2013) -- End

        Try
            strQ = "SELECT " & _
              "  mappingunkid " & _
              ", approverunkid " & _
              ", userunkid " & _
              ", usertypeid " & _
             "FROM hrapprover_usermapping " & _
             "WHERE usertypeid= @usertypeid "

            If intunkid > 0 Then
                strQ &= " AND  mappingunkid = @mappingunkid "
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid.ToString)
            End If

            If intApproverunkid > 0 Then
                strQ &= " AND approverunkid = @approverunkid "
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid.ToString)
            End If

            If intUserunkid > 0 Then
                strQ &= " AND Userunkid = @Userunkid "
                objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)
            End If

            objDataOperation.AddParameter("@Usertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUsertypeid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintUserTypeid = CInt(dtRow.Item("usertypeid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal UserTypeID As Integer = -1, Optional ByVal mstrFilter As String = "") As DataSet
        'Pinkal (16-Jul-2020) -- ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report[Optional ByVal mstrFilter As String = ""]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mappingunkid " & _
              ", approverunkid " & _
              ", userunkid " & _
              ", usertypeid " & _
             "FROM hrapprover_usermapping " & _
             " WHERE 1 = 1 "

            If UserTypeID > 0 Then
                strQ &= " AND usertypeid = " & UserTypeID
            End If


            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If
            'Pinkal (16-Jul-2020) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrapprover_usermapping) </purpose>
    Public Function Insert(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (06-Apr-2013) -- Start
        'Enhancement : TRA Changes
        Dim mblnIsFromApproveMst As Boolean = False

        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            mblnIsFromApproveMst = True
        End If
        objDataOperation.ClearParameters()
        'Pinkal (06-Apr-2013) -- End


        Try
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@usertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserTypeid.ToString)

            strQ = "INSERT INTO hrapprover_usermapping ( " & _
              "  approverunkid " & _
              ", userunkid " & _
              ", usertypeid " & _
            ") VALUES (" & _
              "  @approverunkid " & _
              ", @userunkid" & _
              ", @usertypeid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = dsList.Tables(0).Rows(0).Item(0)


            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrapprover_usermapping", "mappingunkid", mintMappingunkid, False, mintAuditUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mblnIsFromApproveMst = False Then
            objDataOperation.ReleaseTransaction(True)
            End If

            'Pinkal (06-Apr-2013) -- End

            Return True
        Catch ex As Exception

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            If mblnIsFromApproveMst = False Then objDataOperation.ReleaseTransaction(False)
            'Pinkal (19-Mar-2015) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            If mblnIsFromApproveMst = False Then objDataOperation = Nothing
            'Pinkal (19-Mar-2015) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrapprover_usermapping) </purpose>
    Public Function Update(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (06-Apr-2013) -- Start
        'Enhancement : TRA Changes
        Dim mblnIsFromApproveMst As Boolean = False

        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            mblnIsFromApproveMst = True
        End If
        objDataOperation.ClearParameters()
        'Pinkal (06-Apr-2013) -- End

        Try
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@usertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserTypeid.ToString)

            strQ = "UPDATE hrapprover_usermapping SET " & _
              "  approverunkid = @approverunkid" & _
              ", userunkid = @userunkid " & _
              ", usertypeid = @usertypeid " & _
            "WHERE mappingunkid = @mappingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrapprover_usermapping", mintMappingunkid, "mappingunkid", 2, objDataOperation) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrapprover_usermapping", "mappingunkid", mintMappingunkid, False, mintAuditUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mblnIsFromApproveMst = False Then
            objDataOperation.ReleaseTransaction(True)
            End If

            'Pinkal (06-Apr-2013) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrapprover_usermapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            strQ = "Select isnull(mappingunkid,0) mappingunkid from hrapprover_usermapping  WHERE mappingunkid = @mappingunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then


                'Pinkal (06-Apr-2013) -- Start
                'Enhancement : TRA Changes

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid")), False, mintAuditUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END



            End If

            'Pinkal (12-Oct-2011) -- End

            strQ = "DELETE FROM hrapprover_usermapping " & _
            "WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intUsertypeid As Integer, Optional ByVal intApproverunkid As Integer = 0, Optional ByVal intUserunkid As Integer = 0, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mappingunkid " & _
              ", approverunkid " & _
              ", userunkid " & _
              ", usertypeid " & _
              "  FROM hrapprover_usermapping " & _
              "  WHERE usertypeid=@usertypeid "

            If intApproverunkid > 0 Then
                strQ &= " AND approverunkid = @approverunkid "
                objDataOperation.AddParameter("@approverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intApproverunkid)
            End If

            If intUserunkid > 0 Then
                strQ &= " AND userunkid = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUserunkid)
            End If

            If intUnkid > 0 Then
                strQ &= " AND mappingunkid <> @mappingunkid"
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@usertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUsertypeid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function isUserMappingExists(ByVal eUType As enUserType, ByVal intApproverUnkid As Integer, ByVal intUserunkid As Integer, Optional ByVal bnlIsReviewer As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   " mappingunkid " & _
                   "FROM hrapprover_usermapping " & _
                   "JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid "

            If bnlIsReviewer = False Then
                strQ &= " AND isreviewer = 0 "
            Else
                strQ &= " AND isreviewer = 1 "
            End If

            strQ &= "WHERE hrapprover_usermapping.usertypeid= '" & eUType & "' And hrapprover_usermapping.approverunkid <> '" & intApproverUnkid & "' AND hrapprover_usermapping.userunkid = '" & intUserunkid & "' "

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            If eUType = enUserType.Assessor Then
                strQ &= " AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "
            End If
            'S.SANDEEP [27 DEC 2016] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 10 APR 2013 ] -- END

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUserWithAppoverPrivilage(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If mblFlag Then
                strQ = "SELECT 0 as userunkid, '' +  @name  as username UNION "
            End If

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'PRIVILEGE NO 264 :- ALLOW TO APPROVE LEAVE

            'strQ &= " SELECT ISNULL(hrmsConfiguration..cfuser_master.userunkid,0) userunkid,ISNULL(username,'') username FROM hrmsConfiguration..cfuser_master" & _
            '        " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '        " WHERE hrmsConfiguration..cfuser_privilege.privilegeunkid = 264 "

            strQ &= " SELECT ISNULL(hrmsConfiguration..cfuser_master.userunkid,0) userunkid,ISNULL(username,'') username FROM hrmsConfiguration..cfuser_master" & _
                    " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    " WHERE hrmsConfiguration..cfuser_privilege.privilegeunkid = 264 AND hrmsConfiguration..cfuser_master.isactive = 1"

            'Pinkal (14-Dec-2012) -- End

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUserWithPrivilage", mstrModuleName)
        End Try
        Return dsList
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class