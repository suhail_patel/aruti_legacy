﻿'************************************************************************************************************************************
'Class Name :clstables_cloning.vb
'Purpose    :Allow User to Copy Master Tables Data from Different Transaction Tables
'Date       :28-Nov-2017
'Written By :Sandeep J Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Import "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J Sharma
''' </summary>
Public Class clstables_cloning
    Private Shared ReadOnly mstrModuleName As String = "clstables_cloning"
    Dim mstrMessage As String = ""
    Dim mstrInfoMessage As String = ""

#Region " Enums "

    Public Enum enCloneGroups
        CG_Common_Master = 1
        CG_Core_Setups = 2
        CG_HR_Master = 3
        CG_Payroll_Master = 4
        CG_Leave = 5
        CG_Performance_Appraisal = 6
        CG_Utilites = 7
    End Enum

    Public Enum enCore_Setups
        CS_State_Master = 1
        CS_City_Master = 2
        CS_Zipcode_Master = 3
        CS_Institute_Master = 4
    End Enum

    Public Enum enHR_Master
        HM_Action_Master = 1
        HM_Skill_Master = 2
        HM_SkillExpt_Master = 3
        HM_Qualification_Master = 4
        HM_Result_Master = 5
    End Enum

    Public Enum enPayroll_Master
        PM_TranHead_Master = 1
        PM_TranHead_FrmlMst = 2
        PM_Activity_Master = 3
        PM_UnitMeasure_Master = 4
        PM_Account_Master = 5
    End Enum

    Public Enum enLeave
        LV_Holiday_Master = 1
        LV_LeaveType_Master = 2
    End Enum

    Public Enum enPerformanceApprsl
        PA_Assess_Period = 1
        PA_Assess_Groups = 2
        PA_Assess_Ratios = 3        
        PA_Scale_Group = 4
        PA_Assess_Scales = 5        
        PA_Cmpt_Grpups = 6
        PA_Cmpt_Category = 7
        PA_Cmpt_Master = 8
        PA_Assess_Fld_Master = 9
        PA_Assess_ViewSetting = 10
        PA_Custom_Headers = 11
        PA_Custom_Items = 12
        PA_Computation_Frmls = 13
    End Enum

    Public Enum enUtilites
        UT_Letter_Type = 1
        UT_Letter_Templates = 2
    End Enum

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public ReadOnly Property _InfoMessage() As String
        Get
            Return mstrInfoMessage
        End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetSourceDatabase(ByVal strTableName As String, ByVal blnAddSelect As Boolean) As DataTable
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dtTable As DataTable = Nothing
        Try
            Using objDataOpr As New clsDataOperation
                If blnAddSelect Then
                    StrQ = "select 0 As id, @select AS cname,0 AS yearid, '' AS DBname, '' AS sdate, '' AS edate UNION "
                End If
                StrQ &= "select " & _
                        "    cfc.companyunkid as id " & _
                        "   ,cfc.name as cname " & _
                        "   ,cff.yearunkid as yearunkid " & _
                        "   ,cff.database_name as DBname " & _
                        "   ,convert(nvarchar(8),cff.start_date,112) as sdate " & _
                        "   ,convert(nvarchar(8),cff.end_date,112) as edate " & _
                        "from hrmsConfiguration..cfcompany_master as cfc " & _
                        "   join hrmsConfiguration..cffinancial_year_tran cff on cfc.companyunkid = cff.companyunkid " & _
                        "   join sys.databases as sydb on cff.database_name = sydb.name " & _
                        "where cfc.isactive = 1 and cff.isclosed = 0 "

                If strTableName.Trim().Length <= 0 Then strTableName = "List"

                objDataOpr.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "Select"))

                dsList = objDataOpr.ExecQuery(StrQ, strTableName)

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                dtTable = dsList.Tables(strTableName).Copy() : dsList.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSourceDatabase; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    Public Function GetDestinationDatabases(ByVal strTableName As String, ByVal intCompanyId As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Try
            If strTableName.Trim().Length <= 0 Then strTableName = "List"
            dtTable = GetSourceDatabase(strTableName, False)
            If dtTable IsNot Nothing Then
                dtTable = New DataView(dtTable, "id <> '" & intCompanyId & "'", "id", DataViewRowState.CurrentRows).ToTable()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDestinationDatabases; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    Public Function GetItemsInformation(ByVal strTableName As String) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Try
            Using objDataOpr As New clsDataOperation

                StrQ = "select " & _
                              "final.itemid " & _
                             ",final.cgid " & _
                             ",final.cgname " & _
                             ",final.itemname " & _
                             ",final.tablename " & _
                             ",final.mtypeid " & _
                        "from " & _
                        "( " & _
                             "SELECT " & clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY & " AS itemid, 1 as cgid, @cm as cgname ,@ADVERTISE_CATEGORY as itemname, 'cfcommon_master' as tablename,1 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.ALLERGIES & " AS itemid, 1 as cgid, @cm as cgname ,@ALLERGIES as itemname, 'cfcommon_master' as tablename,2 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.ASSET_CONDITION & " AS itemid, 1 as cgid, @cm as cgname ,@ASSET_CONDITION as itemname, 'cfcommon_master' as tablename,3 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.BENEFIT_GROUP & " AS itemid, 1 as cgid, @cm as cgname ,@BENEFIT_GROUP as itemname, 'cfcommon_master' as tablename,4 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.BLOOD_GROUP & " AS itemid, 1 as cgid, @cm as cgname ,@BLOOD_GROUP as itemname, 'cfcommon_master' as tablename,5 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.COMPLEXION & " AS itemid, 1 as cgid, @cm as cgname ,@COMPLEXION as itemname, 'cfcommon_master' as tablename,6 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.DISABILITIES & " AS itemid, 1 as cgid, @cm as cgname ,@DISABILITIES as itemname, 'cfcommon_master' as tablename,7 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " AS itemid, 1 as cgid, @cm as cgname ,@EMPLOYEMENT_TYPE as itemname, 'cfcommon_master' as tablename,8 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.ETHNICITY & " AS itemid, 1 as cgid, @cm as cgname ,@ETHNICITY as itemname, 'cfcommon_master' as tablename,9 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.EYES_COLOR & " AS itemid, 1 as cgid, @cm as cgname ,@EYES_COLOR as itemname, 'cfcommon_master' as tablename,10 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.HAIR_COLOR & " AS itemid, 1 as cgid, @cm as cgname ,@HAIR_COLOR as itemname, 'cfcommon_master' as tablename,11 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & " AS itemid, 1 as cgid, @cm as cgname ,@IDENTITY_TYPES as itemname, 'cfcommon_master' as tablename,12 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & " AS itemid, 1 as cgid, @cm as cgname ,@INTERVIEW_TYPE as itemname, 'cfcommon_master' as tablename,13 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.LANGUAGES & " AS itemid, 1 as cgid, @cm as cgname ,@LANGUAGES as itemname, 'cfcommon_master' as tablename,14 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & " AS itemid, 1 as cgid, @cm as cgname ,@MARRIED_STATUS as itemname, 'cfcommon_master' as tablename,15 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " AS itemid, 1 as cgid, @cm as cgname ,@MEMBERSHIP_CATEGORY as itemname, 'cfcommon_master' as tablename,16 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.PAY_TYPE & " AS itemid, 1 as cgid, @cm as cgname ,@PAY_TYPE as itemname, 'cfcommon_master' as tablename,17 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " AS itemid, 1 as cgid, @cm as cgname ,@QUALIFICATION_COURSE_GROUP as itemname, 'cfcommon_master' as tablename,18 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.RELATIONS & " AS itemid, 1 as cgid, @cm as cgname ,@RELATIONS as itemname, 'cfcommon_master' as tablename,19 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.RELIGION & " AS itemid, 1 as cgid, @cm as cgname ,@RELIGION as itemname, 'cfcommon_master' as tablename,20 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.RESULT_GROUP & " AS itemid, 1 as cgid, @cm as cgname ,@RESULT_GROUP as itemname, 'cfcommon_master' as tablename,21 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.SHIFT_TYPE & " AS itemid, 1 as cgid, @cm as cgname ,@SHIFT_TYPE as itemname, 'cfcommon_master' as tablename,22 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & " AS itemid, 1 as cgid, @cm as cgname ,@SKILL_CATEGORY as itemname, 'cfcommon_master' as tablename,23 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.TITLE & " AS itemid, 1 as cgid, @cm as cgname ,@TITLE as itemname, 'cfcommon_master' as tablename,24 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.TRAINING_RESOURCES & " AS itemid, 1 as cgid, @cm as cgname ,@TRAINING_RESOURCES as itemname, 'cfcommon_master' as tablename,25 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.ASSETS & " AS itemid, 1 as cgid, @cm as cgname ,@ASSETS as itemname, 'cfcommon_master' as tablename,27 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.SALINC_REASON & " AS itemid, 1 as cgid, @cm as cgname ,@SALINC_REASON as itemname, 'cfcommon_master' as tablename,29 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " AS itemid, 1 as cgid, @cm as cgname ,@COURSE_MASTER as itemname, 'cfcommon_master' as tablename,30 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.STRATEGIC_GOAL & " AS itemid, 1 as cgid, @cm as cgname ,@STRATEGIC_GOAL as itemname, 'cfcommon_master' as tablename,31 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS & " AS itemid, 1 as cgid, @cm as cgname ,@SOURCES_FUNDINGS as itemname, 'cfcommon_master' as tablename,32 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AS itemid, 1 as cgid, @cm as cgname, @ATTACHMENT_TYPES as itemname, 'cfcommon_master' as tablename,33 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " AS itemid, 1 as cgid, @cm as cgname ,@VACANCY_MASTER as itemname, 'cfcommon_master' as tablename,34 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE & " AS itemid, 1 as cgid, @cm as cgname ,@DISCIPLINE_COMMITTEE as itemname, 'cfcommon_master' as tablename,35 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY & " AS itemid, 1 as cgid, @cm as cgname ,@OFFENCE_CATEGORY as itemname, 'cfcommon_master' as tablename,36 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.COMMITTEE_MEMBERS_CATEGORY & " AS itemid, 1 as cgid, @cm as cgname ,@COMMITTEE_MEMBERS_CATEGORY as itemname, 'cfcommon_master' as tablename,37 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & " AS itemid, 1 as cgid, @cm as cgname ,@APPRAISAL_ACTIONS as itemname, 'cfcommon_master' as tablename,38 as mtypeid  " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " AS itemid, 1 as cgid, @cm as cgname ,@SECTOR_ROUTE as itemname, 'cfcommon_master' as tablename,39 as mtypeid  " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.TRANSFERS & " AS itemid, 1 as cgid, @cm as cgname ,@TRANSFERS as itemname, 'cfcommon_master' as tablename,42 as mtypeid  " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.PROMOTIONS & " AS itemid, 1 as cgid, @cm as cgname ,@PROMOTIONS as itemname, 'cfcommon_master' as tablename,43 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.RECATEGORIZE & " AS itemid, 1 as cgid, @cm as cgname ,@RECATEGORIZE as itemname, 'cfcommon_master' as tablename,44 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.PROBATION & " AS itemid, 1 as cgid, @cm as cgname ,@PROBATION as itemname, 'cfcommon_master' as tablename,45 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.CONFIRMATION & " AS itemid, 1 as cgid, @cm as cgname ,@CONFIRMATION as itemname, 'cfcommon_master' as tablename,46 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.SUSPENSION & " AS itemid, 1 as cgid, @cm as cgname ,@SUSPENSION as itemname, 'cfcommon_master' as tablename,47 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.TERMINATION & " AS itemid, 1 as cgid, @cm as cgname ,@TERMINATION as itemname, 'cfcommon_master' as tablename,48 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.RE_HIRE & " AS itemid, 1 as cgid, @cm as cgname ,@RE_HIRE as itemname, 'cfcommon_master' as tablename,49 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.WORK_PERMIT & " AS itemid, 1 as cgid, @cm as cgname ,@WORK_PERMIT as itemname, 'cfcommon_master' as tablename,50 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.RETIREMENTS & " AS itemid, 1 as cgid, @cm as cgname ,@RETIREMENTS as itemname, 'cfcommon_master' as tablename,51 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.COST_CENTER & " AS itemid, 1 as cgid, @cm as cgname ,@COST_CENTER as itemname, 'cfcommon_master' as tablename,52 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.PROV_REGION_1 & " AS itemid, 1 as cgid, @cm as cgname ,@PROV_REGION_1 as itemname, 'cfcommon_master' as tablename,54 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.ROAD_STREET_1 & " AS itemid, 1 as cgid, @cm as cgname ,@ROAD_STREET_1 as itemname, 'cfcommon_master' as tablename,55 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.CHIEFDOM & " AS itemid, 1 as cgid, @cm as cgname ,@CHIEFDOM as itemname, 'cfcommon_master' as tablename,56 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.VILLAGE & " AS itemid, 1 as cgid, @cm as cgname ,@VILLAGE as itemname, 'cfcommon_master' as tablename,57 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.TOWN_1 & " AS itemid, 1 as cgid, @cm as cgname ,@TOWN_1 as itemname, 'cfcommon_master' as tablename,58 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE & " AS itemid, 1 as cgid, @cm as cgname ,@DISC_REPONSE_TYPE as itemname, 'cfcommon_master' as tablename,59 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.GE_APPRAISAL_ACTIONS & " AS itemid, 1 as cgid, @cm as cgname ,@GE_APPRAISAL_ACTIONS as itemname, 'cfcommon_master' as tablename,60 as mtypeid " & _
                             "UNION SELECT " & clsCommon_Master.enCommonMaster.BSC_APPRAISAL_ACTIONS & " AS itemid, 1 as cgid, @cm as cgname ,@BSC_APPRAISAL_ACTIONS as itemname, 'cfcommon_master' as tablename,61 as mtypeid " & _
                        "union all " & _
                             "select " & CInt(enCore_Setups.CS_State_Master) & " as itemid, 2 as cgid, @cr as cgname, @sm as itemname, 'hrmsConfiguration..cfstate_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enCore_Setups.CS_City_Master) & " as itemid, 2 as cgid, @cr as cgname, @ce as itemname, 'hrmsConfiguration..cfcity_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enCore_Setups.CS_Zipcode_Master) & " as itemid, 2 as cgid, @cr as cgname, @zm as itemname, 'hrmsConfiguration..cfzipcode_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enCore_Setups.CS_Institute_Master) & " as itemid, 2 as cgid, @cr as cgname, @ti as itemname, 'hrinstitute_master' as tablename,0 as mtypeid " & _
                        "union all " & _
                             "select " & CInt(enHR_Master.HM_Action_Master) & " as itemid, 3 as cgid, @hr as cgname, @rm as itemname, 'hraction_reason_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enHR_Master.HM_Skill_Master) & " as itemid, 3 as cgid, @hr as cgname, @sk as itemname, 'hrskill_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enHR_Master.HM_SkillExpt_Master) & " as itemid, 3 as cgid, @hr as cgname, @se as itemname, 'rcskillexpertise_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enHR_Master.HM_Qualification_Master) & " as itemid, 3 as cgid, @hr as cgname, @qm as itemname, 'hrqualification_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enHR_Master.HM_Result_Master) & " as itemid, 3 as cgid, @hr as cgname, @rc as itemname, 'hrresult_master' as tablename,0 as mtypeid " & _
                        "union all " & _
                             "select " & CInt(enPayroll_Master.PM_TranHead_Master) & " as itemid, 4 as cgid, @pm as cgname, @th as itemname, 'prtranhead_master,prtranhead_formula_activity_amount_tran,prtranhead_formula_activity_unit_tran,prtranhead_formula_common_master_tran,prtranhead_formula_cumulative_tran,prtranhead_formula_currency_tran,prtranhead_formula_expense_tran,prtranhead_formula_leavetype_tran,prtranhead_formula_loanscheme_tran,prtranhead_formula_measurement_amount_tran,prtranhead_formula_measurement_unit_tran,prtranhead_formula_shift_tran,prtranhead_formula_slab_tran,prtranhead_formula_tran,prtranhead_inexcessslab_tran,prtranhead_slab_tran' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPayroll_Master.PM_Activity_Master) & " as itemid, 4 as cgid, @pm as cgname, @ac as itemname, 'practivity_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPayroll_Master.PM_UnitMeasure_Master) & " as itemid, 4 as cgid, @pm as cgname, @um as itemname, 'prunitmeasure_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPayroll_Master.PM_Account_Master) & " as itemid, 4 as cgid, @pm as cgname, @jv as itemname, 'praccount_master' as tablename,0 as mtypeid " & _
                        "union all " & _
                             "select " & CInt(enLeave.LV_Holiday_Master) & " as itemid, 5 as cgid, @lv as cgname, @hm as itemname, 'lvholiday_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enLeave.LV_LeaveType_Master) & " as itemid, 5 as cgid, @lv as cgname, @lt as itemname, 'lvleavetype_master' as tablename,0 as mtypeid " & _
                        "union all " & _
                             "select " & CInt(enPerformanceApprsl.PA_Scale_Group) & " as itemid, 6 as cgid, @pa as cgname, @sg as itemname, 'cfcommon_master' as tablename,41 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Assess_Scales) & " as itemid, 6 as cgid, @pa as cgname, @as as itemname, 'hrassess_scale_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Cmpt_Grpups) & " as itemid, 6 as cgid, @pa as cgname, @cg as itemname, 'cfcommon_master' as tablename,53 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Cmpt_Category) & " as itemid, 6 as cgid, @pa as cgname, @cc as itemname, 'cfcommon_master' as tablename,40 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Cmpt_Master) & " as itemid, 6 as cgid, @pa as cgname, @co as itemname, 'hrassess_competencies_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Assess_Fld_Master) & " as itemid, 6 as cgid, @pa as cgname, @bt as itemname, 'hrassess_field_master' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Assess_ViewSetting) & " as itemid, 6 as cgid, @pa as cgname, @vs as itemname, 'hrmsConfiguration..cfconfiguration' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Custom_Headers) & " as itemid, 6 as cgid, @pa as cgname, @ca as itemname, 'hrassess_custom_headers,hrassess_custom_items' as tablename,0 as mtypeid " & _
                             "union select " & CInt(enPerformanceApprsl.PA_Computation_Frmls) & " as itemid, 6 as cgid, @pa as cgname, @cf as itemname, 'hrassess_computation_master,hrassess_computation_tran' as tablename,0 as mtypeid " & _
                        "union all " & _
                             "select " & CInt(enUtilites.UT_Letter_Type) & " as itemid, 7 as cgid, @ut as cgname, @le as itemname, 'cfcommon_master' as tablename,28 as mtypeid " & _
                             "union select " & CInt(enUtilites.UT_Letter_Templates) & " as itemid, 7 as cgid, @ut as cgname, @te as itemname, 'hrlettertype_master' as tablename,0 as mtypeid " & _
                        ") as final where 1 = 1 order by final.cgid "

                '"union select " & CInt(enPayroll_Master.PM_TranHead_FrmlMst) & " as itemid, 4 as cgid, @pm as cgname, @tf as itemname, 'prtranhead_master,prtranhead_formula_activity_amount_tran,prtranhead_formula_activity_unit_tran,prtranhead_formula_common_master_tran,prtranhead_formula_cumulative_tran,prtranhead_formula_currency_tran,prtranhead_formula_expense_tran,prtranhead_formula_leavetype_tran,prtranhead_formula_loanscheme_tran,prtranhead_formula_measurement_amount_tran,prtranhead_formula_measurement_unit_tran,prtranhead_formula_shift_tran,prtranhead_formula_slab_tran,prtranhead_formula_tran,prtranhead_inexcessslab_tran,prtranhead_slab_tran' as tablename,0 as mtypeid " & _
                '"select " & CInt(enPerformanceApprsl.PA_Assess_Period) & " as itemid, 6 as cgid, @pa as cgname, @ap as itemname, 'cfcommon_period_tran' as tablename,0 as mtypeid " & _
                '             "union 
                '"union select " & CInt(enPerformanceApprsl.PA_Custom_Items) & " as itemid, 6 as cgid, @pa as cgname, @ca as itemname, 'hrassess_custom_headers,hrassess_custom_items' as tablename,0 as mtypeid " & _
                '"select " & CInt(enPerformanceApprsl.PA_Assess_Groups) & " as itemid, 6 as cgid, @pa as cgname, @ag as itemname, 'hrassess_group_master,hrassess_group_tran' as tablename,0 as mtypeid " & _
                '            "union select " & CInt(enPerformanceApprsl.PA_Assess_Ratios) & " as itemid, 6 as cgid, @pa as cgname, @ar as itemname, 'hrassess_ratio_master,hrassess_ratio_tran' as tablename,0 as mtypeid " & _
                '             "union 
                If strTableName.Trim().Length <= 0 Then strTableName = "List"

                objDataOpr.AddParameter("@ADVERTISE_CATEGORY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 40, "Agency Category"))
                objDataOpr.AddParameter("@ALLERGIES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 41, "Allergies"))
                objDataOpr.AddParameter("@ASSET_CONDITION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 42, "Asset Condition"))
                objDataOpr.AddParameter("@BENEFIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 43, "Benefit Group"))
                objDataOpr.AddParameter("@BLOOD_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 44, "Blood Group"))
                objDataOpr.AddParameter("@COMPLEXION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 45, "Complexion"))
                objDataOpr.AddParameter("@COUNTRIES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 46, "Countries"))
                objDataOpr.AddParameter("@DISABILITIES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 47, "Disabilities"))
                objDataOpr.AddParameter("@EMPLOYEMENT_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 48, "Employment Type"))
                objDataOpr.AddParameter("@ETHNICITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 49, "Ethnicity"))
                objDataOpr.AddParameter("@EYES_COLOR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 50, "Eye Color"))
                objDataOpr.AddParameter("@HAIR_COLOR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 51, "Hair Color"))
                objDataOpr.AddParameter("@IDENTITY_TYPES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 52, "Identity Type"))
                objDataOpr.AddParameter("@INTERVIEW_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 53, "Interview Type"))
                objDataOpr.AddParameter("@LANGUAGES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 54, "Languages"))
                objDataOpr.AddParameter("@MARRIED_STATUS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 55, "Married Status"))
                objDataOpr.AddParameter("@MEMBERSHIP_CATEGORY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 56, "Membership Category"))
                objDataOpr.AddParameter("@PAY_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 57, "Pay Type"))
                objDataOpr.AddParameter("@QUALIFICATION_COURSE_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 58, "Qualification/Course Group"))
                objDataOpr.AddParameter("@RELATIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 59, "Relation"))
                objDataOpr.AddParameter("@RELIGION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 60, "Religion"))
                objDataOpr.AddParameter("@RESULT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 61, "Result Group"))
                objDataOpr.AddParameter("@SHIFT_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 62, "Shift Type"))
                objDataOpr.AddParameter("@SKILL_CATEGORY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 63, "Skill Category"))
                objDataOpr.AddParameter("@TITLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 64, "Title"))
                objDataOpr.AddParameter("@TRAINING_RESOURCES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 65, "Training Resources"))
                objDataOpr.AddParameter("@ASSET_STATUS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 66, "Asset Status"))
                objDataOpr.AddParameter("@ASSETS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 67, "Assets"))
                objDataOpr.AddParameter("@LETTER_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 68, "Letter Type"))
                objDataOpr.AddParameter("@SALINC_REASON", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 303, "Pay Increment Reason"))
                objDataOpr.AddParameter("@COURSE_MASTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 324, "Training Course Master"))
                objDataOpr.AddParameter("@STRATEGIC_GOAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 325, "Strategic Goal Master"))
                objDataOpr.AddParameter("@SOURCES_FUNDINGS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 334, "Funding Sources"))
                objDataOpr.AddParameter("@ATTACHMENT_TYPES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 441, "Scan/Attach Documents"))
                objDataOpr.AddParameter("@VACANCY_MASTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 442, "Vacancy Master"))
                objDataOpr.AddParameter("@DISCIPLINE_COMMITTEE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 443, "Disciplinary Committee"))
                objDataOpr.AddParameter("@OFFENCE_CATEGORY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 444, "Offences Category"))
                objDataOpr.AddParameter("@COMMITTEE_MEMBERS_CATEGORY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 391, "Committee Members Category"))
                objDataOpr.AddParameter("@APPRAISAL_ACTIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 758, "Overall Appraisal Action"))
                objDataOpr.AddParameter("@SECTOR_ROUTE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 572, "Sector/Route"))
                objDataOpr.AddParameter("@COMPETENCE_CATEGORIES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 576, "Competence Categories"))
                objDataOpr.AddParameter("@ASSESSMENT_SCALE_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 577, "Assessment Scale Group"))
                objDataOpr.AddParameter("@TRANSFERS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 661, "Transfers"))
                objDataOpr.AddParameter("@PROMOTIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 662, "Promotions"))
                objDataOpr.AddParameter("@RECATEGORIZE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 663, "Recategorize"))
                objDataOpr.AddParameter("@PROBATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 664, "Probation"))
                objDataOpr.AddParameter("@CONFIRMATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 665, "Confirmation"))
                objDataOpr.AddParameter("@SUSPENSION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 666, "Suspension"))
                objDataOpr.AddParameter("@TERMINATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 667, "Termination"))
                objDataOpr.AddParameter("@RE_HIRE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 668, "Re-Hire"))
                objDataOpr.AddParameter("@WORK_PERMIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 669, "Work Permit"))
                objDataOpr.AddParameter("@RETIREMENTS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 670, "Retirements"))
                objDataOpr.AddParameter("@COST_CENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 671, "Cost Center"))
                objDataOpr.AddParameter("@COMPETENCE_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 693, "Competence Groups"))
                objDataOpr.AddParameter("@PROV_REGION_1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 707, "Prov/Region1"))
                objDataOpr.AddParameter("@ROAD_STREET_1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 708, "Road/Street1"))
                objDataOpr.AddParameter("@CHIEFDOM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 709, "Chiefdom"))
                objDataOpr.AddParameter("@VILLAGE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 710, "Village"))
                objDataOpr.AddParameter("@TOWN_1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 711, "Town1"))
                objDataOpr.AddParameter("@DISC_REPONSE_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 743, "Discipline Response Type"))
                objDataOpr.AddParameter("@GE_APPRAISAL_ACTIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 756, "Competency Appraisal Action"))
                objDataOpr.AddParameter("@BSC_APPRAISAL_ACTIONS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 757, "BSC Appraisal Action"))

                objDataOpr.AddParameter("@cm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Common Master"))
                objDataOpr.AddParameter("@cr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Core Setups"))
                objDataOpr.AddParameter("@hr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "HR Master"))
                objDataOpr.AddParameter("@pm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Payroll Master"))
                objDataOpr.AddParameter("@lv", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Leave"))
                objDataOpr.AddParameter("@pa", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Performace Appraisal"))
                objDataOpr.AddParameter("@ut", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Utilities"))

                objDataOpr.AddParameter("@sm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "State Master"))
                objDataOpr.AddParameter("@ce", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "City Master"))
                objDataOpr.AddParameter("@zm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Zipcode Master"))
                objDataOpr.AddParameter("@ti", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Training Institue"))

                objDataOpr.AddParameter("@rm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Reason Master"))
                objDataOpr.AddParameter("@sk", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Skill Master"))
                objDataOpr.AddParameter("@se", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Skill Expertise"))
                objDataOpr.AddParameter("@qm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Qualification Master"))
                objDataOpr.AddParameter("@rc", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Result Code"))

                objDataOpr.AddParameter("@th", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Transaction Heads"))
                objDataOpr.AddParameter("@tf", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Transaction Formulas"))
                objDataOpr.AddParameter("@ac", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Activities"))
                objDataOpr.AddParameter("@um", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Unit Of Mesure"))
                objDataOpr.AddParameter("@jv", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "JV Account"))

                objDataOpr.AddParameter("@hm", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "Holiday Master"))
                objDataOpr.AddParameter("@lt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Leave Type"))

                objDataOpr.AddParameter("@ap", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Assessment Periods"))
                objDataOpr.AddParameter("@ag", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Assessment Groups"))
                objDataOpr.AddParameter("@ar", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Assessment Ratios"))
                objDataOpr.AddParameter("@as", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Assessment Scales"))
                objDataOpr.AddParameter("@sg", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Assessment Scales Group"))
                objDataOpr.AddParameter("@co", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "Competencies"))
                objDataOpr.AddParameter("@cg", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "Competency Group"))
                objDataOpr.AddParameter("@cc", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Competency Categories"))
                objDataOpr.AddParameter("@bt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "BSC Titles"))
                objDataOpr.AddParameter("@vs", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "BSC Title View Setting"))
                'objDataOpr.AddParameter("@ct", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Custom Section Titles"))
                objDataOpr.AddParameter("@ca", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Custom Header(s)/Item(s)"))
                objDataOpr.AddParameter("@cf", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "Computation Formula"))

                objDataOpr.AddParameter("@le", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Letter Type"))
                objDataOpr.AddParameter("@te", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "Letter Templates"))

                dsList = objDataOpr.ExecQuery(StrQ, strTableName)

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                dtTable = dsList.Tables(strTableName).Copy() : dsList.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: GetItemsInformation" & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    Public Function CloneItems(ByVal strSourceDatabaseName As String, _
                               ByVal dicDestinationDatabases As Dictionary(Of Integer, String), _
                               ByVal dicDestinationStartDate As Dictionary(Of Integer, String), _
                               ByVal dicItemsDetails As Dictionary(Of String, List(Of clstables_cloning.clsItemCollection)), _
                               Optional ByVal pbovr As Windows.Forms.ToolStripProgressBar = Nothing, _
                               Optional ByVal pbcur As Windows.Forms.ToolStripProgressBar = Nothing, _
                               Optional ByVal lbOvr As Windows.Forms.ToolStripLabel = Nothing, _
                               Optional ByVal lbCur As Windows.Forms.ToolStripLabel = Nothing) As Boolean

        Dim objDataOperation As New clsDataOperation
        Try
            Dim dicInfoMessage As New Dictionary(Of String, String)
            Dim intOvrIdx, intCurIdx As Integer
            intOvrIdx = 0 : intCurIdx = 0
            Dim StrQ As String = String.Empty
            Dim intItemGrpId As Integer = 0
            objDataOperation.BindTransaction()
            If strSourceDatabaseName.Trim.Length > 0 Then
                objDataOperation.ExecNonQuery("Use " & strSourceDatabaseName)
                Dim strDestDatabaseName As String = String.Empty
                If pbovr IsNot Nothing Then
                    pbovr.Maximum = dicDestinationDatabases.Keys.Count
                End If
                For Each dkey As Integer In dicDestinationDatabases.Keys
                    strDestDatabaseName = dicDestinationDatabases(dkey)
                    If pbcur IsNot Nothing Then
                        pbcur.Maximum = dicItemsDetails.Keys.Count
                    End If
                    Application.DoEvents()
                    If pbcur IsNot Nothing Then
                        intCurIdx = 0 : pbcur.Value = intCurIdx
                    End If
                    If lbCur IsNot Nothing Then lbCur.Text = ""
                    For Each ckey As String In dicItemsDetails.Keys
                        If pbcur IsNot Nothing Then
                            intCurIdx += 1
                            If pbcur.Value >= pbcur.Maximum Then
                                pbcur.Value = pbcur.Maximum
                            Else
                                pbcur.Value = intCurIdx
                            End If
                            If lbCur IsNot Nothing Then lbCur.Text = CInt((pbcur.Value * 100) / pbcur.Maximum).ToString() & "%"
                        End If
                        Application.DoEvents()
                        Dim items As List(Of clstables_cloning.clsItemCollection) = Nothing
                        items = dicItemsDetails(ckey)
                        If items.Count > 0 Then
                            Dim strTables As String() = items(0)._TableName.Split(",")
                            For idx As Integer = 0 To strTables.Length - 1
                                Application.DoEvents()
                                Dim xColName As String = String.Empty
                                xColName = GetColNameCSV(strTables(idx), objDataOperation)
                                Select Case items(0)._ItemsGrpId
                                    Case enCloneGroups.CG_Common_Master         '1
                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                               "    (" & xColName & ") " & _
                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                               "Where isactive = 1 and mastertype = '" & CInt(items(0)._MasterTypeId) & "' " & _
                                               " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name AND l.mastertype = '" & items(0)._MasterTypeId & "') "

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                        End If

                                    Case enCloneGroups.CG_Core_Setups           '2
                                        Select Case items(0)._ItemId
                                            Case enCore_Setups.CS_State_Master, enCore_Setups.CS_City_Master, enCore_Setups.CS_Zipcode_Master

                                            Case enCore_Setups.CS_Institute_Master
                                                StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "(" & xColName & ") " & _
                                                       "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                       "Where isactive = 1 " & _
                                                       " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.institute_name = " & strSourceDatabaseName & ".." & strTables(idx) & ".institute_name ) "
                                        End Select

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                        End If

                                    Case enCloneGroups.CG_HR_Master             '3
                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                               "    (" & xColName & ") " & _
                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                               "Where isactive = 1 "
                                        Select Case items(0)._ItemId
                                            Case enHR_Master.HM_Action_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.reason_action = " & strSourceDatabaseName & ".." & strTables(idx) & ".reason_action ) "
                                            Case enHR_Master.HM_Skill_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.skillname = " & strSourceDatabaseName & ".." & strTables(idx) & ".skillname ) "
                                            Case enHR_Master.HM_SkillExpt_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name ) "
                                            Case enHR_Master.HM_Qualification_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.qualificationname = " & strSourceDatabaseName & ".." & strTables(idx) & ".qualificationname ) "
                                            Case enHR_Master.HM_Result_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.resultname = " & strSourceDatabaseName & ".." & strTables(idx) & ".resultname ) "
                                        End Select

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                        End If

                                    Case enCloneGroups.CG_Payroll_Master        '4
                                        xColName = GetColNameCSV(strTables(idx), objDataOperation, True)
                                        StrQ = "TRUNCATE TABLE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                               "SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " ON  " & _
                                               "INSERT  INTO " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                               "(" & xColName & ") " & _
                                               "SELECT " & _
                                                    xColName & " " & _
                                               "FROM  " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                               " SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " OFF "

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                        End If

                                    Case enCloneGroups.CG_Leave                 '5
                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                               "(" & xColName & ") " & _
                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                               "Where isactive = 1 "
                                        Select Case items(0)._ItemId
                                            Case enLeave.LV_Holiday_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.holidayname = " & strSourceDatabaseName & ".." & strTables(idx) & ".holidayname ) "
                                            Case enLeave.LV_LeaveType_Master
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.leavename = " & strSourceDatabaseName & ".." & strTables(idx) & ".leavename ) "
                                        End Select

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                        End If

                                    Case enCloneGroups.CG_Performance_Appraisal '6

                                        Dim intPeriodId As Integer = 0
                                        intPeriodId = GetPeriodId(enModuleReference.Assessment, dkey, strDestDatabaseName, eZeeDate.convertDate(dicDestinationStartDate(dkey)), objDataOperation)
                                        If intPeriodId <= 0 Then
                                            If dicInfoMessage.ContainsKey(CInt(enCloneGroups.CG_Performance_Appraisal).ToString() & "|" & strDestDatabaseName) = False Then
                                                dicInfoMessage.Add(CInt(enCloneGroups.CG_Performance_Appraisal).ToString() & "|" & strDestDatabaseName, strDestDatabaseName)
                                            End If
                                            Continue For
                                        End If
                                        Select Case items(0)._ItemId

                                            Case enPerformanceApprsl.PA_Assess_Scales
                                                If intPeriodId > 0 Then
                                                    StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                           "  (" & xColName & ") " & _
                                                           "Select " & xColName.Replace(strTables(idx) & ".periodunkid", intPeriodId) & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                           "Where isactive = 1 And periodunkid In (select max(periodunkid) from " & strSourceDatabaseName & ".." & strTables(idx) & " where isactive = 1) " & _
                                                           " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.scale = " & strSourceDatabaseName & ".." & strTables(idx) & ".scale) "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If

                                                    StrQ = "UPDATE " & strDestDatabaseName & ".." & strTables(idx) & "  " & _
                                                           "SET scalemasterunkid = fin.masterunkid " & _
                                                           "FROM " & _
                                                           "( " & _
                                                           "    SELECT " & _
                                                           "         masterunkid " & _
                                                           "        ,A.scalemasterunkid " & _
                                                           "    FROM " & strDestDatabaseName & "..cfcommon_master " & _
                                                           "    JOIN " & _
                                                           "    ( " & _
                                                           "        SELECT " & _
                                                           "             sm.name " & _
                                                           "            ,scalemasterunkid " & _
                                                           "        FROM " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                           "        JOIN " & strSourceDatabaseName & "..cfcommon_master as sm on sm.masterunkid = " & strDestDatabaseName & ".." & strTables(idx) & ".scalemasterunkid " & _
                                                           "            AND sm.mastertype = 41 AND sm.isactive = 1 " & _
                                                           "        WHERE " & strDestDatabaseName & ".." & strTables(idx) & ".isactive = 1 " & _
                                                           "        AND periodunkid = '" & intPeriodId & "' " & _
                                                           "    ) AS A ON A.name = " & strDestDatabaseName & "..cfcommon_master.name AND mastertype = 41 " & _
                                                           ") AS fin WHERE fin.scalemasterunkid = " & strDestDatabaseName & ".." & strTables(idx) & ".scalemasterunkid "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If

                                                End If
                                            Case enPerformanceApprsl.PA_Assess_ViewSetting
                                                StrQ = "Declare @compid int " & _
                                                        "Set @compid = Isnull((select companyunkid From hrmsConfiguration..cffinancial_year_tran Where [database_name] = '" & strDestDatabaseName & "' And companyunkid > 0),0) " & _
                                                        "If @compid > 0 " & _
                                                        "Begin " & _
                                                        "   Declare @configid int " & _
                                                        "   Declare @plan nvarchar(max),@eval nvarchar(max) " & _
                                                        "   Set @plan = Isnull((Select key_value From hrmsConfiguration..cfconfiguration Where companyunkid = @compid And upper([key_name]) = 'VIEWTITLES_INPLANNING'),'') " & _
                                                        "   Set @eval = Isnull((Select key_value From hrmsConfiguration..cfconfiguration Where companyunkid = @compid And upper([key_name]) = 'VIEWTITLES_INEVALUATION'),'') " & _
                                                        "   Set @configid = Isnull((Select configunkid From hrmsConfiguration..cfconfiguration Where companyunkid = @compid And upper([key_name]) = 'VIEWTITLES_INPLANNING'),0) " & _
                                                        "   If @configid > 0 " & _
                                                        "   Begin " & _
                                                        "       Update hrmsConfiguration..cfconfiguration SET key_value = @plan Where configunkid = @configid " & _
                                                        "   End " & _
                                                        "   Else " & _
                                                        "   Begin " & _
                                                        "       Insert Into hrmsConfiguration..cfconfiguration(key_name,key_value,companyunkid) Values('ViewTitles_InPlanning',@plan,@compid) " & _
                                                        "   End " & _
                                                        "   Set @configid = 0 " & _
                                                        "   Set @configid = Isnull((Select configunkid From hrmsConfiguration..cfconfiguration Where companyunkid = @compid And upper([key_name]) = 'VIEWTITLES_INEVALUATION'),0) " & _
                                                        "   If @configid > 0 " & _
                                                        "   Begin " & _
                                                        "       Update hrmsConfiguration..cfconfiguration SET key_value = @eval Where configunkid = @configid " & _
                                                        "   End " & _
                                                        "   Else " & _
                                                        "   Begin " & _
                                                        "       Insert Into hrmsConfiguration..cfconfiguration(key_name,key_value,companyunkid) Values('ViewTitles_InEvaluation',@eval,@compid) " & _
                                                        "   End " & _
                                                        "End "

                                                objDataOperation.ExecNonQuery(StrQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                End If

                                            Case enPerformanceApprsl.PA_Custom_Headers
                                                xColName = GetColNameCSV(strTables(idx), objDataOperation, True)
                                                StrQ = "TRUNCATE TABLE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " ON  " & _
                                                       "INSERT INTO " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "(" & xColName & ") " & _
                                                       "SELECT " & _
                                                            xColName & " " & _
                                                       "FROM  " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                       " SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " OFF "

                                                objDataOperation.ExecNonQuery(StrQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                End If

                                                If strTables(idx).ToString.ToUpper = "HRASSESS_CUSTOM_ITEMS" Then
                                                    xColName = GetColNameCSV(strTables(idx), objDataOperation, True)
                                                    StrQ = "TRUNCATE TABLE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                           "SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " ON  " & _
                                                           "INSERT INTO " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                           "(" & xColName & ") " & _
                                                           "SELECT " & _
                                                                xColName.Replace(strTables(idx) & ".periodunkid", intPeriodId) & " " & _
                                                           "FROM  " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                           " SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " OFF "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If
                                                End If

                                            Case enPerformanceApprsl.PA_Computation_Frmls
                                                xColName = GetColNameCSV(strTables(idx), objDataOperation, True)

                                                If strTables(idx).ToString.ToUpper = "HRASSESS_COMPUTATION_MASTER" Then
                                                    StrQ = "TRUNCATE TABLE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " ON  " & _
                                                       "INSERT INTO " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "(" & xColName & ") " & _
                                                       "SELECT " & _
                                                        xColName.Replace(strTables(idx) & ".periodunkid", intPeriodId) & " " & _
                                                       "FROM  " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                       "Where periodunkid In (select max(periodunkid) from " & strSourceDatabaseName & ".." & strTables(idx) & " Where " & strSourceDatabaseName & ".." & strTables(idx) & ".isvoid = 0) " & _
                                                       " SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " OFF "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If
                                                End If

                                                If strTables(idx).ToString.ToUpper = "HRASSESS_COMPUTATION_TRAN" Then
                                                    xColName = GetColNameCSV(strTables(idx), objDataOperation, True)
                                                    StrQ = "TRUNCATE TABLE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " ON  " & _
                                                       "INSERT INTO " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "(" & xColName & ") " & _
                                                       "SELECT " & _
                                                            xColName & " " & _
                                                       "FROM  " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                       "Join " & strSourceDatabaseName & "..hrassess_computation_master On " & strSourceDatabaseName & "..hrassess_computation_master.computationunkid = " & strSourceDatabaseName & ".." & strTables(idx) & ".computationunkid " & _
                                                       "Where periodunkid In (select max(periodunkid) from " & strSourceDatabaseName & "..hrassess_computation_master Where " & strSourceDatabaseName & "..hrassess_computation_master.isvoid = 0) " & _
                                                       " SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " OFF "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If
                                                End If

                                            Case enPerformanceApprsl.PA_Assess_Fld_Master
                                                xColName = GetColNameCSV(strTables(idx), objDataOperation, True)
                                                StrQ = "TRUNCATE TABLE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " ON  " & _
                                                       "INSERT INTO " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "(" & xColName & ") " & _
                                                       "SELECT " & _
                                                            xColName & " " & _
                                                       "FROM  " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                       " SET IDENTITY_INSERT " & strDestDatabaseName & ".." & strTables(idx) & " OFF "

                                                objDataOperation.ExecNonQuery(StrQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                End If

                                            Case enPerformanceApprsl.PA_Cmpt_Master

                                                If intPeriodId > 0 Then
                                                    StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                           "  (" & xColName & ") " & _
                                                           "Select " & xColName.Replace(strTables(idx) & ".periodunkid", intPeriodId) & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                           "Where isactive = 1 And periodunkid In (select max(periodunkid) from " & strSourceDatabaseName & ".." & strTables(idx) & " where isactive = 1) " & _
                                                           " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name) "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If

                                                    StrQ = "UPDATE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                            "   Set competence_categoryunkid = f.masterunkid " & _
                                                            "From " & _
                                                            "( " & _
                                                                 "Select " & _
                                                                  "masterunkid " & _
                                                                 ",a.ccmstid " & _
                                                                 "From " & strDestDatabaseName & "..cfcommon_master " & _
                                                                 "Join " & _
                                                                 "( " & _
                                                                      "select " & _
                                                                            "" & strDestDatabaseName & ".." & strTables(idx) & ".name AS cname " & _
                                                                           ",isnull(cc.name,'') as ccname " & _
                                                                           ",isnull(cc.masterunkid,0) as ccmstid " & _
                                                                      "from " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                                           "Left Join " & strSourceDatabaseName & "..cfcommon_master AS cc on cc.masterunkid = " & strDestDatabaseName & ".." & strTables(idx) & ".competence_categoryunkid and cc.mastertype = 40 " & _
                                                                      "Where " & strDestDatabaseName & ".." & strTables(idx) & ".isactive = 1 AND periodunkid = '" & intPeriodId & "' " & _
                                                                 ") As a On a.ccname = name " & _
                                                            ") AS f where f.ccmstid = " & strDestDatabaseName & ".." & strTables(idx) & ".competence_categoryunkid "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If

                                                    StrQ = "UPDATE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                            "   Set competencegroupunkid = f.masterunkid " & _
                                                            "From " & _
                                                            "( " & _
                                                                 "Select " & _
                                                                  "masterunkid " & _
                                                                 ",a.cgmstid " & _
                                                                 "From " & strDestDatabaseName & "..cfcommon_master " & _
                                                                 "Join " & _
                                                                 "( " & _
                                                                      "select " & _
                                                                            "" & strDestDatabaseName & ".." & strTables(idx) & ".name AS cname " & _
                                                                           ",isnull(cg.name,'') as cgname " & _
                                                                           ",isnull(cg.masterunkid,0) as cgmstid " & _
                                                                      "from " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                                           "Left Join " & strSourceDatabaseName & "..cfcommon_master AS cg on cg.masterunkid = " & strDestDatabaseName & ".." & strTables(idx) & ".competencegroupunkid and cg.mastertype = 53 " & _
                                                                      "Where " & strDestDatabaseName & ".." & strTables(idx) & ".isactive = 1 AND periodunkid = '" & intPeriodId & "' " & _
                                                                 ") As a On a.cgname = name " & _
                                                            ") AS f where f.cgmstid = " & strDestDatabaseName & ".." & strTables(idx) & ".competencegroupunkid "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If

                                                    StrQ = "UPDATE " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                            "   Set scalemasterunkid = f.masterunkid " & _
                                                            "From " & _
                                                            "( " & _
                                                                 "Select " & _
                                                                  "masterunkid " & _
                                                                 ",a.smmstid " & _
                                                                 "From " & strDestDatabaseName & "..cfcommon_master " & _
                                                                 "Join " & _
                                                                 "( " & _
                                                                      "select " & _
                                                                            "" & strDestDatabaseName & ".." & strTables(idx) & ".name AS cname " & _
                                                                           ",isnull(sm.name,'') as smname " & _
                                                                           ",isnull(sm.masterunkid,0) as smmstid " & _
                                                                      "from " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                                           "Left Join " & strSourceDatabaseName & "..cfcommon_master AS sm on sm.masterunkid = " & strDestDatabaseName & ".." & strTables(idx) & ".scalemasterunkid and sm.mastertype = 41 " & _
                                                                      "Where " & strDestDatabaseName & ".." & strTables(idx) & ".isactive = 1 AND periodunkid = '" & intPeriodId & "' " & _
                                                                 ") As a On a.smname = name " & _
                                                            ") AS f where f.smmstid = " & strDestDatabaseName & ".." & strTables(idx) & ".scalemasterunkid "

                                                    objDataOperation.ExecNonQuery(StrQ)

                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                    End If

                                                End If



                                            Case enPerformanceApprsl.PA_Scale_Group, enPerformanceApprsl.PA_Cmpt_Grpups, enPerformanceApprsl.PA_Cmpt_Category

                                                StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                                       "    (" & xColName & ") " & _
                                                       "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                                       "Where isactive = 1 and mastertype = '" & CInt(items(0)._MasterTypeId) & "' " & _
                                                       " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name AND l.mastertype = '" & items(0)._MasterTypeId & "') "

                                                objDataOperation.ExecNonQuery(StrQ)

                                                If objDataOperation.ErrorMessage <> "" Then
                                                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                                End If
                                        End Select

                                    Case enCloneGroups.CG_Utilites              '7
                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
                                               "(" & xColName & ") " & _
                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
                                               "Where isactive = 1 "
                                        Select Case items(0)._ItemId
                                            Case enUtilites.UT_Letter_Templates
                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.lettername = " & strSourceDatabaseName & ".." & strTables(idx) & ".lettername ) "
                                            Case enUtilites.UT_Letter_Type
                                                StrQ &= " and mastertype = '" & CInt(items(0)._MasterTypeId) & "' " & _
                                                        " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name AND l.mastertype = '" & items(0)._MasterTypeId & "') "
                                        End Select

                                        objDataOperation.ExecNonQuery(StrQ)

                                        If objDataOperation.ErrorMessage <> "" Then
                                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage & " - [" & ckey.Split("|")(0) & "]" : Throw New Exception(mstrMessage)
                                        End If

                                End Select
                            Next
                        End If
                    Next
                    If pbovr IsNot Nothing Then
                        intOvrIdx += 1
                        If pbovr.Value >= pbovr.Maximum Then
                            pbovr.Value = pbovr.Maximum
                        Else
                            pbovr.Value = intOvrIdx
                        End If
                        If lbOvr IsNot Nothing Then lbOvr.Text = CInt((pbovr.Value * 100) / pbovr.Maximum).ToString() & "%"
                    End If
                    Application.DoEvents()
                Next
            End If
            If dicInfoMessage.Keys.Count > 0 Then
                mstrInfoMessage = Language.getMessage(mstrModuleName, 40, "Sorry, No period defined for Performance Appraisal module. Data will not be copied") & " " & _
                                  Language.getMessage(mstrModuleName, 41, "for following database(s),") & " " & vbCrLf & String.Join(vbCrLf, dicInfoMessage.Values().ToArray()) & " "

                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            objDataOperation.ReleaseTransaction(True)
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(mstrMessage & "; Procedure Name: CloneItems; Module Name: " & mstrModuleName)
        Finally
            objDataOperation.ExecNonQuery("Use hrmsConfiguration")
            'objDataOperation.ExecNonQuery("IF OBJECT_ID('tempdb..#scrtab') IS NOT NULL DROP TABLE #scrtab;IF OBJECT_ID('tempdb..#destab') IS NOT NULL DROP TABLE #destab;IF OBJECT_ID('tempdb..#fintab') IS NOT NULL DROP TABLE #fintab; ")
            objDataOperation = Nothing
            If pbcur IsNot Nothing Then pbcur.Value = 0
            If pbovr IsNot Nothing Then pbovr.Value = 0
            If lbOvr IsNot Nothing Then lbOvr.Text = ""
            If lbCur IsNot Nothing Then lbCur.Text = ""
        End Try
        Return True
    End Function

    Public Function GetIdentityColName(ByVal strTableName As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As String
        Dim xColName As String = String.Empty
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = String.Empty
        Try
            StrQ = "select " & _
                   " @colname = name " & _
                   "from sys.identity_columns " & _
                   "where object_name(object_id) = @tabname "

            objDataOperation.AddParameter("@colname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xColName, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@tabname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strTableName)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If IsDBNull(objDataOperation.GetParameterValue("@colname")) = False Then
                xColName = objDataOperation.GetParameterValue("@colname")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetIdentityColumnName; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return xColName
    End Function

    Public Function GetColNameCSV(ByVal strTableName As String, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnAddIdentityCol As Boolean = False) As String
        Dim xColName As String = String.Empty
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dslist As New DataSet
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = String.Empty
        Try
            StrQ = "select " & _
                   "  isnull(stuff((select ',' + @tabname +'.'+column_name " & _
                   "from information_schema.columns "
            If blnAddIdentityCol = False Then
                StrQ &= " join sys.identity_columns on name <> column_name and table_name = object_name(object_id) "
            End If
            StrQ &= "where table_name = @tabname " & _
                   "for xml path('')),1,1,''),'') as col "

            objDataOperation.AddParameter("@tabname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strTableName)

            dslist = objDataOperation.ExecQuery(StrQ, "list")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dslist.Tables("list").Rows.Count > 0 Then
                xColName = dslist.Tables("list").Rows(0)("col")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetColNameCSV; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return xColName
    End Function

    Public Function GetPredefinedHeadIds(ByVal strSourceDatabase As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As String
        Dim xHeadValue As String = String.Empty
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = String.Empty
        Try
            StrQ = "Select @colname =  snull(stuff(( " & _
                   "Select ',' + Cast(a.tranheadunkid as nvarchar(max)) " & _
                   "From " & _
                   "( " & _
                        "Select distinct tranheadunkid From " & strSourceDatabase & "..prtranhead_master Where trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " And typeof_id = " & CInt(enTypeOf.Salary) & " And calctype_id = " & CInt(enCalcType.OnAttendance) & " And isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid From " & strSourceDatabase & "..prtranhead_master Where trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " And typeof_id = " & CInt(enTypeOf.Salary) & " And calctype_id = " & CInt(enCalcType.DEFINED_SALARY) & " And isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.NET_PAY) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.TOTAL_EARNING) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.TOTAL_DEDUCTION) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.TAXABLE_EARNING_TOTAL) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.NON_TAXABLE_EARNING_TOTAL) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.NON_CASH_BENEFIT_TOTAL) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.ROUNDING_ADJUSTMENT) & " AND isnull(isvoid,0) = 0 Union " & _
                        "Select distinct tranheadunkid FROM " & strSourceDatabase & "..prtranhead_master WHERE trnheadtype_id = " & CInt(enTranHeadType.Informational) & " And typeof_id = " & CInt(enTypeOf.Informational) & " And calctype_id = " & CInt(enCalcType.NET_PAY_ROUNDING_ADJUSTMENT) & " AND isnull(isvoid,0) = 0 " & _
                   ") as a for xml path('')),1,1,''),'0') "

            objDataOperation.AddParameter("@colname", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, xHeadValue, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If IsDBNull(objDataOperation.GetParameterValue("@colname")) = False Then
                xHeadValue = objDataOperation.GetParameterValue("@colname")
            End If

            '|***************** Based On Discussion With Rutta, We Need To Skip All The Formula Liked With Other Modules ******************|
            Dim dsList As New DataSet
            StrQ = "Declare @Sql As Nvarchar(max),@Qry As Nvarchar(max) " & _
                   "Select " & _
                   "@Sql = COALESCE(@Sql + ', ', '') + 'SELECT ' + column_name + ' From ' + table_name + ' Where isvoid = 0 Union' " & _
                   "From information_schema.columns " & _
                   "Where column_name In('tranheadunkid','formulatranheadunkid') " & _
                   "And table_name Not Like 'vw%' And table_name not like 'at%' " & _
                   "And table_name In " & _
                   "('prtranhead_formula_activity_amount_tran','prtranhead_formula_activity_unit_tran','prtranhead_formula_common_master_tran', " & _
                   "'prtranhead_formula_expense_tran','prtranhead_formula_leavetype_tran','prtranhead_formula_loanscheme_tran','prtranhead_formula_measurement_amount_tran', " & _
                   "'prtranhead_formula_measurement_unit_tran','prtranhead_formula_shift_tran') " & _
                   "Set @Qry = 'Select Isnull(Stuff((Select Distinct +'',''+ Cast(formulatranheadunkid As Nvarchar(max)) From prtranhead_formula_tran where tranheadunkid in(' + REPLACE(Substring(@Sql,0,Len(@Sql) - 5),'Union,','Union ') + ') for xml path('''')),1,1,''''),''0'')' " & _
                   "EXEC(@Qry)"

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                xHeadValue = xHeadValue & "," & dsList.Tables(0).Rows(0)(0)
            End If
            '|***************** Based On Discussion With Rutta, We Need To Skip All The Formula Liked With Other Modules ******************|

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPredefinedHeadIds; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return xHeadValue
    End Function

    Public Function GetPeriodId(ByVal enModuleRefId As enModuleReference, _
                                ByVal intYearId As Integer, _
                                ByVal strDatabaseName As String, _
                                ByVal strDatabaseStartDate As Date, _
                                Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Dim intPeriodId As Integer = 0
        Dim dslist As New DataSet
        Dim StrQ As String = String.Empty
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT  TOP 1 " & _
                   "    cfcommon_period_tran.periodunkid " & _
                   "   ,cfcommon_period_tran.period_code " & _
                   "   ,cfcommon_period_tran.period_name " & _
                   "   ,cfcommon_period_tran.yearunkid " & _
                   "   ,cfcommon_period_tran.modulerefid " & _
                   "   ,CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) AS start_date " & _
                   "   ,CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) AS end_date " & _
                   "   ,cfcommon_period_tran.total_days " & _
                   "   ,cfcommon_period_tran.description " & _
                   "   ,cfcommon_period_tran.statusid " & _
                   "   ,cfcommon_period_tran.isactive " & _
                   "FROM " & strDatabaseName & "..cfcommon_period_tran " & _
                   "WHERE isactive = 1 AND modulerefid = @modulerefid " & _
                   "AND yearunkid = @yearunkid AND statusid = 1 AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @Date " & _
                   "ORDER BY start_date DESC "

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, enModuleRefId)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(strDatabaseStartDate))

            dslist = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dslist.Tables("List").Rows.Count > 0 Then
                intPeriodId = CInt(dslist.Tables("List").Rows(0).Item("periodunkid").ToString)
            End If

            If intPeriodId <= 0 Then
                objDataOperation.ClearParameters()
                StrQ = "SELECT  TOP 1 " & _
                       "    cfcommon_period_tran.periodunkid " & _
                       "   ,cfcommon_period_tran.period_code " & _
                       "   ,cfcommon_period_tran.period_name " & _
                       "   ,cfcommon_period_tran.yearunkid " & _
                       "   ,cfcommon_period_tran.modulerefid " & _
                       "   ,CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) AS start_date " & _
                       "   ,CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) AS end_date " & _
                       "   ,cfcommon_period_tran.total_days " & _
                       "   ,cfcommon_period_tran.description " & _
                       "   ,cfcommon_period_tran.statusid " & _
                       "   ,cfcommon_period_tran.isactive " & _
                       "FROM " & strDatabaseName & "..cfcommon_period_tran " & _
                       "WHERE isactive = 1 AND modulerefid = @modulerefid " & _
                       "AND yearunkid = @yearunkid AND statusid = 1 " & _
                       "ORDER BY end_date ASC "

                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, enModuleRefId)
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

                dslist = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If dslist.Tables("List").Rows.Count > 0 Then
                    intPeriodId = CInt(dslist.Tables("List").Rows(0).Item("periodunkid").ToString)
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPeriodId; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return intPeriodId
    End Function

#End Region

#Region " Collection Class "

    Public Class clsItemCollection
        Private mstrTableNames As String = String.Empty
        Private mintMasterType As Integer = 0
        Private mintItemsGrpId As Integer = 0
        Private mintItemId As Integer = 0

        Public Property _TableName() As String
            Get
                Return mstrTableNames
            End Get
            Set(ByVal value As String)
                mstrTableNames = value
            End Set
        End Property

        Public Property _MasterTypeId() As Integer
            Get
                Return mintMasterType
            End Get
            Set(ByVal value As Integer)
                mintMasterType = value
            End Set
        End Property

        Public Property _ItemsGrpId() As Integer
            Get
                Return mintItemsGrpId
            End Get
            Set(ByVal value As Integer)
                mintItemsGrpId = value
            End Set
        End Property

        Public Property _ItemId() As Integer
            Get
                Return mintItemId
            End Get
            Set(ByVal value As Integer)
                mintItemId = value
            End Set
        End Property

        Public Sub New()

        End Sub

        Public Sub New(ByVal strTableNames As String, ByVal intMasterType As Integer, ByVal intItemsGrpId As Integer, ByVal intItemId As Integer)
            Try
                mstrTableNames = strTableNames
                mintMasterType = intMasterType
                mintItemsGrpId = intItemsGrpId
                mintItemId = intItemId
            Catch ex As Exception
                Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
            End Try
        End Sub

    End Class

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 40, "Agency Category")
            Language.setMessage("clsMasterData", 41, "Allergies")
            Language.setMessage("clsMasterData", 42, "Asset Condition")
            Language.setMessage("clsMasterData", 43, "Benefit Group")
            Language.setMessage("clsMasterData", 44, "Blood Group")
            Language.setMessage("clsMasterData", 45, "Complexion")
            Language.setMessage("clsMasterData", 46, "Countries")
            Language.setMessage("clsMasterData", 47, "Disabilities")
            Language.setMessage("clsMasterData", 48, "Employment Type")
            Language.setMessage("clsMasterData", 49, "Ethnicity")
            Language.setMessage("clsMasterData", 50, "Eye Color")
            Language.setMessage("clsMasterData", 51, "Hair Color")
            Language.setMessage("clsMasterData", 52, "Identity Type")
            Language.setMessage("clsMasterData", 53, "Interview Type")
            Language.setMessage("clsMasterData", 54, "Languages")
            Language.setMessage("clsMasterData", 55, "Married Status")
            Language.setMessage("clsMasterData", 56, "Membership Category")
            Language.setMessage("clsMasterData", 57, "Pay Type")
            Language.setMessage("clsMasterData", 58, "Qualification/Course Group")
            Language.setMessage("clsMasterData", 59, "Relation")
            Language.setMessage("clsMasterData", 60, "Religion")
            Language.setMessage("clsMasterData", 61, "Result Group")
            Language.setMessage("clsMasterData", 62, "Shift Type")
            Language.setMessage("clsMasterData", 63, "Skill Category")
            Language.setMessage("clsMasterData", 64, "Title")
            Language.setMessage("clsMasterData", 65, "Training Resources")
            Language.setMessage("clsMasterData", 66, "Asset Status")
            Language.setMessage("clsMasterData", 67, "Assets")
            Language.setMessage("clsMasterData", 68, "Letter Type")
            Language.setMessage("clsMasterData", 303, "Pay Increment Reason")
            Language.setMessage("clsMasterData", 324, "Training Course Master")
            Language.setMessage("clsMasterData", 325, "Strategic Goal Master")
            Language.setMessage("clsMasterData", 334, "Funding Sources")
            Language.setMessage("clsMasterData", 391, "Committee Members Category")
            Language.setMessage("clsMasterData", 441, "Scan/Attach Documents")
            Language.setMessage("clsMasterData", 442, "Vacancy Master")
            Language.setMessage("clsMasterData", 443, "Disciplinary Committee")
            Language.setMessage("clsMasterData", 444, "Offences Category")
            Language.setMessage("clsMasterData", 572, "Sector/Route")
            Language.setMessage("clsMasterData", 576, "Competence Categories")
            Language.setMessage("clsMasterData", 577, "Assessment Scale Group")
            Language.setMessage("clsMasterData", 661, "Transfers")
            Language.setMessage("clsMasterData", 662, "Promotions")
            Language.setMessage("clsMasterData", 663, "Recategorize")
            Language.setMessage("clsMasterData", 664, "Probation")
            Language.setMessage("clsMasterData", 665, "Confirmation")
            Language.setMessage("clsMasterData", 666, "Suspension")
            Language.setMessage("clsMasterData", 667, "Termination")
            Language.setMessage("clsMasterData", 668, "Re-Hire")
            Language.setMessage("clsMasterData", 669, "Work Permit")
            Language.setMessage("clsMasterData", 670, "Retirements")
            Language.setMessage("clsMasterData", 671, "Cost Center")
            Language.setMessage("clsMasterData", 693, "Competence Groups")
            Language.setMessage("clsMasterData", 707, "Prov/Region1")
            Language.setMessage("clsMasterData", 708, "Road/Street1")
            Language.setMessage("clsMasterData", 709, "Chiefdom")
            Language.setMessage("clsMasterData", 710, "Village")
            Language.setMessage("clsMasterData", 711, "Town1")
            Language.setMessage("clsMasterData", 743, "Discipline Response Type")
            Language.setMessage("clsMasterData", 756, "Competency Appraisal Action")
            Language.setMessage("clsMasterData", 757, "BSC Appraisal Action")
            Language.setMessage("clsMasterData", 758, "Overall Appraisal Action")
            Language.setMessage(mstrModuleName, 1, "Common Master")
            Language.setMessage(mstrModuleName, 2, "Core Setups")
            Language.setMessage(mstrModuleName, 3, "HR Master")
            Language.setMessage(mstrModuleName, 4, "Payroll Master")
            Language.setMessage(mstrModuleName, 5, "Leave")
            Language.setMessage(mstrModuleName, 6, "Performace Appraisal")
            Language.setMessage(mstrModuleName, 7, "Utilities")
            Language.setMessage(mstrModuleName, 8, "State Master")
            Language.setMessage(mstrModuleName, 9, "City Master")
            Language.setMessage(mstrModuleName, 10, "Zipcode Master")
            Language.setMessage(mstrModuleName, 11, "Training Institue")
            Language.setMessage(mstrModuleName, 12, "Reason Master")
            Language.setMessage(mstrModuleName, 13, "Skill Master")
            Language.setMessage(mstrModuleName, 14, "Skill Expertise")
            Language.setMessage(mstrModuleName, 15, "Qualification Master")
            Language.setMessage(mstrModuleName, 16, "Result Code")
            Language.setMessage(mstrModuleName, 17, "Transaction Heads")
            Language.setMessage(mstrModuleName, 18, "Transaction Formulas")
            Language.setMessage(mstrModuleName, 19, "Activities")
            Language.setMessage(mstrModuleName, 20, "Unit Of Mesure")
            Language.setMessage(mstrModuleName, 21, "JV Account")
            Language.setMessage(mstrModuleName, 22, "Holiday Master")
            Language.setMessage(mstrModuleName, 23, "Leave Type")
            Language.setMessage(mstrModuleName, 24, "Assessment Periods")
            Language.setMessage(mstrModuleName, 25, "Assessment Groups")
            Language.setMessage(mstrModuleName, 26, "Assessment Ratios")
            Language.setMessage(mstrModuleName, 27, "Assessment Scales")
            Language.setMessage(mstrModuleName, 28, "Assessment Scales Group")
            Language.setMessage(mstrModuleName, 29, "Competencies")
            Language.setMessage(mstrModuleName, 30, "Competency Group")
            Language.setMessage(mstrModuleName, 31, "Competency Categories")
            Language.setMessage(mstrModuleName, 32, "BSC Titles")
            Language.setMessage(mstrModuleName, 33, "BSC Title View Setting")
            Language.setMessage(mstrModuleName, 34, "Custom Section Titles")
            Language.setMessage(mstrModuleName, 35, "Custom Assessment Items")
            Language.setMessage(mstrModuleName, 36, "Computation Formula")
            Language.setMessage(mstrModuleName, 37, "Letter Type")
            Language.setMessage(mstrModuleName, 38, "Letter Templates")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Function CloneItems(ByVal strSourceDatabaseName As String, _
'                           ByVal dicDestinationDatabases As Dictionary(Of Integer, String), _
'                           ByVal dicItemsDetails As Dictionary(Of String, List(Of clstables_cloning.clsItemCollection)), _
'                           Optional ByVal lnklabel As Windows.Forms.LinkLabel = Nothing) As Boolean
'    Dim objDataOperation As New clsDataOperation
'    Try
'        Dim StrQ As String = String.Empty
'        Dim intItemGrpId As Integer = 0
'        objDataOperation.BindTransaction()
'        If strSourceDatabaseName.Trim.Length > 0 Then
'            Dim strDestDatabaseName As String = String.Empty
'            For Each dkey As Integer In dicDestinationDatabases.Keys
'                strDestDatabaseName = dicDestinationDatabases(dkey)
'                For Each ckey As String In dicItemsDetails.Keys
'                    If lnklabel IsNot Nothing Then
'                        lnklabel.Text = ckey.Split("|")(0).ToString()
'                        Application.DoEvents()
'                        Dim items As List(Of clstables_cloning.clsItemCollection) = Nothing
'                        items = dicItemsDetails(ckey)
'                        If items.Count > 0 Then
'                            Dim strTables As String() = items(0)._TableName.Split(",")
'                            If intItemGrpId <> items(0)._ItemsGrpId Then
'                                StrQ = "IF OBJECT_ID('tempdb..#scrtab') IS NOT NULL DROP TABLE #scrtab; " & _
'                                       "IF OBJECT_ID('tempdb..#destab') IS NOT NULL DROP TABLE #destab; " & _
'                                       "IF OBJECT_ID('tempdb..#fintab') IS NOT NULL DROP TABLE #fintab; "

'                                objDataOperation.ExecNonQuery(StrQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                End If

'                                StrQ = "create table #scrtab (id int,nvalue nvarchar(max),mtypeid int,tabname nvarchar(max)); " & _
'                                       "create table #destab (id int,nvalue nvarchar(max),mtypeid int,tabname nvarchar(max)); " & _
'                                       "create table #fintab (oid int,nid int,nvalue nvarchar(max),mtypeid int,tabname nvarchar(max)); "

'                                objDataOperation.ExecNonQuery(StrQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                End If

'                                intItemGrpId = items(0)._ItemsGrpId
'                            End If


'                            StrQ = ""
'                            For idx As Integer = 0 To strTables.Length - 1
'                                Dim xColName As String = String.Empty
'                                xColName = GetColNameCSV(strTables(idx))
'                                Select Case items(0)._ItemsGrpId
'                                    Case enCloneGroups.CG_Common_Master         '1
'                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                               "    (" & xColName & ") " & _
'                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                               "Where isactive = 1 and mastertype = '" & CInt(items(0)._MasterTypeId) & "' " & _
'                                               " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name AND l.mastertype = '" & items(0)._MasterTypeId & "') "

'                                        objDataOperation.ExecNonQuery(StrQ)

'                                        If objDataOperation.ErrorMessage <> "" Then
'                                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                        End If

'                                    Case enCloneGroups.CG_Core_Setups           '2
'                                        Select Case items(0)._ItemId
'                                            Case enCore_Setups.CS_State_Master, enCore_Setups.CS_City_Master, enCore_Setups.CS_Zipcode_Master

'                                            Case enCore_Setups.CS_Institute_Master
'                                                StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                                       "(" & xColName & ") " & _
'                                                       "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                                       "Where isactive = 1 " & _
'                                                       " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.institute_name = " & strSourceDatabaseName & ".." & strTables(idx) & ".institute_name ) "
'                                        End Select

'                                        objDataOperation.ExecNonQuery(StrQ)

'                                        If objDataOperation.ErrorMessage <> "" Then
'                                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                        End If

'                                    Case enCloneGroups.CG_HR_Master             '3
'                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                               "(" & xColName & ") " & _
'                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                               "Where isactive = 1 "
'                                        Select Case items(0)._ItemId
'                                            Case enHR_Master.HM_Action_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.reason_action = " & strSourceDatabaseName & ".." & strTables(idx) & ".reason_action ) "
'                                            Case enHR_Master.HM_Skill_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.skillname = " & strSourceDatabaseName & ".." & strTables(idx) & ".skillname ) "
'                                            Case enHR_Master.HM_SkillExpt_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name ) "
'                                            Case enHR_Master.HM_Qualification_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.qualificationname = " & strSourceDatabaseName & ".." & strTables(idx) & ".qualificationname ) "
'                                            Case enHR_Master.HM_Result_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.resultname = " & strSourceDatabaseName & ".." & strTables(idx) & ".resultname ) "
'                                        End Select

'                                        objDataOperation.ExecNonQuery(StrQ)

'                                        If objDataOperation.ErrorMessage <> "" Then
'                                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                        End If

'                                    Case enCloneGroups.CG_Payroll_Master        '4
'                                        Select Case items(0)._ItemId
'                                            Case enPayroll_Master.PM_TranHead_Master
'                                                If strTables(idx).ToUpper = "PRTRANHEAD_MASTER" Then
'                                                    Dim strPredefinedHeadIds As String = String.Empty
'                                                    strPredefinedHeadIds = GetPredefinedHeadIds(strSourceDatabaseName, objDataOperation)
'                                                    StrQ = "Insert Into #scrtab(id,nvalue,mtypeid,tabname) Select tranheadunkid,trnheadname,0,'" & strTables(idx) & "' From " & strSourceDatabaseName & ".." & strTables(idx) & " Where isvoid = 0 "
'                                                    If strPredefinedHeadIds.Trim.Length > 0 Then
'                                                        StrQ &= " And " & strSourceDatabaseName & ".." & strTables(idx) & ".tranheadunkid Not In(" & strPredefinedHeadIds & ")"
'                                                    End If
'                                                    objDataOperation.ExecNonQuery(StrQ)

'                                                    If objDataOperation.ErrorMessage <> "" Then
'                                                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                                    End If

'                                                    StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                                            "(" & xColName & ") " & _
'                                                            "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                                            "Where isvoid = 0 " & _
'                                                            " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.trnheadname = " & strSourceDatabaseName & ".." & strTables(idx) & ".trnheadname ) "
'                                                    If strPredefinedHeadIds.Trim.Length > 0 Then
'                                                        StrQ &= " And " & strSourceDatabaseName & ".." & strTables(idx) & ".tranheadunkid Not In(" & strPredefinedHeadIds & ")"
'                                                    End If

'                                                    objDataOperation.ExecNonQuery(StrQ)

'                                                    If objDataOperation.ErrorMessage <> "" Then
'                                                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                                    End If

'                                                    StrQ = "Insert Into #destab(id,nvalue,mtypeid,tabname) Select tranheadunkid,trnheadname,0,'" & strTables(idx) & "' From " & strDestDatabaseName & ".." & strTables(idx) & " Where isvoid = 0 "
'                                                    strPredefinedHeadIds = GetPredefinedHeadIds(strDestDatabaseName, objDataOperation)
'                                                    If strPredefinedHeadIds.Trim.Length > 0 Then
'                                                        StrQ &= " And " & strSourceDatabaseName & ".." & strTables(idx) & ".tranheadunkid Not In(" & strPredefinedHeadIds & ")"
'                                                    End If

'                                                    objDataOperation.ExecNonQuery(StrQ)

'                                                    If objDataOperation.ErrorMessage <> "" Then
'                                                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                                    End If

'                                                    StrQ = "Insert Into #fintab(oid,nid,nvalue,mtypeid,tabname) " & _
'                                                           "select " & _
'                                                           "     #scrtab.id as oid " & _
'                                                           "    ,fl.nid as nid " & _
'                                                           "    ,#scrtab.nvalue " & _
'                                                           "    ,#scrtab.mtypeid " & _
'                                                           "    ,#scrtab.tabname " & _
'                                                           "From #scrtab " & _
'                                                           "Join " & _
'                                                           "( " & _
'                                                           "    Select " & _
'                                                           "         #destab.id as nid " & _
'                                                           "        ,#destab.nvalue as val " & _
'                                                           "    From #destab " & _
'                                                           "        Join #scrtab on #scrtab.nvalue = #destab.nvalue " & _
'                                                           "    Where 1 = 1 " & _
'                                                           ")as fl on fl.val = nvalue "

'                                                    objDataOperation.ExecNonQuery(StrQ)

'                                                    If objDataOperation.ErrorMessage <> "" Then
'                                                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                                    End If
'                                                Else
'                                                    StrQ = "Insert Into  " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                                           "(" & xColName & ") " & _
'                                                           "Select " & xColName.Replace("tranheadunkid", "#fintab.nid") & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                                           " Join #fintab ON #fintab.oid = " & strSourceDatabaseName & ".." & strTables(idx) & ".tranheadunkid " & _
'                                                           "Where " & strSourceDatabaseName & ".." & strTables(idx) & ".isvoid = 0 "

'                                                    objDataOperation.ExecNonQuery(StrQ)

'                                                    If objDataOperation.ErrorMessage <> "" Then
'                                                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                                    End If
'                                                End If
'                                            Case Else

'                                        End Select
'                                        StrQ = ""

'                                    Case enCloneGroups.CG_Leave                 '5
'                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                               "(" & xColName & ") " & _
'                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                               "Where isactive = 1 "
'                                        Select Case items(0)._ItemId
'                                            Case enLeave.LV_Holiday_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.holidayname = " & strSourceDatabaseName & ".." & strTables(idx) & ".holidayname ) "
'                                            Case enLeave.LV_LeaveType_Master
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.leavename = " & strSourceDatabaseName & ".." & strTables(idx) & ".leavename ) "
'                                        End Select

'                                        objDataOperation.ExecNonQuery(StrQ)

'                                        If objDataOperation.ErrorMessage <> "" Then
'                                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                        End If

'                                    Case enCloneGroups.CG_Performance_Appraisal '6
'                                        StrQ = ""

'                                    Case enCloneGroups.CG_Utilites              '7
'                                        StrQ = "Insert Into " & strDestDatabaseName & ".." & strTables(idx) & " " & _
'                                               "(" & xColName & ") " & _
'                                               "Select " & xColName & " From " & strSourceDatabaseName & ".." & strTables(idx) & " " & _
'                                               "Where isactive = 1 "
'                                        Select Case items(0)._ItemId
'                                            Case enUtilites.UT_Letter_Templates
'                                                StrQ &= " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.lettername = " & strSourceDatabaseName & ".." & strTables(idx) & ".lettername ) "
'                                            Case enUtilites.UT_Letter_Type
'                                                StrQ &= " and mastertype = '" & CInt(items(0)._MasterTypeId) & "' " & _
'                                                        " And not exists(select 1 from " & strDestDatabaseName & ".." & strTables(idx) & " as l where l.name = " & strSourceDatabaseName & ".." & strTables(idx) & ".name AND l.mastertype = '" & items(0)._MasterTypeId & "') "
'                                        End Select

'                                        objDataOperation.ExecNonQuery(StrQ)

'                                        If objDataOperation.ErrorMessage <> "" Then
'                                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                        End If

'                                End Select
'                            Next
'                        End If
'                    End If
'                Next
'            Next
'        End If
'    Catch ex As Exception
'        Throw New Exception(ex.Message & "; Procedure Name: CloneItems; Module Name: " & mstrModuleName)
'    Finally
'        objDataOperation.ExecNonQuery("IF OBJECT_ID('tempdb..#scrtab') IS NOT NULL DROP TABLE #scrtab;IF OBJECT_ID('tempdb..#destab') IS NOT NULL DROP TABLE #destab;IF OBJECT_ID('tempdb..#fintab') IS NOT NULL DROP TABLE #fintab; ")
'        objDataOperation = Nothing
'    End Try
'End Function