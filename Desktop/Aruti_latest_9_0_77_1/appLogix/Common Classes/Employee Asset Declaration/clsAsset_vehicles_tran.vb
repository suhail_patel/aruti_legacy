﻿'************************************************************************************************************************************
'Class Name : clsAsset_vehicles_tran.vb.vb
'Purpose    :
'Date       :21/02/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsAsset_vehicles_tran
    Private Const mstrModuleName = "clsAsset_vehicles_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetvehiclestranunkid As Integer
    Private mintAssetdeclarationunkid As Integer
    Private mstrVehicletype As String = String.Empty
    Private mdecValue As Decimal
    Private mdblServant As Double
    Private mdblWifehusband As Double
    Private mdblChildren As Double
    Private mblnIsfinalsaved As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintCurrencyUnkId As Integer 'Sohail (06 Apr 2012)
    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrModel As String = String.Empty
    Private mstrColour As String = String.Empty
    Private mstrRegno As String = String.Empty
    Private mstrVehicleuse As String = String.Empty
    Private mdtAcquisitiondate As DateTime
    Private mintBasecurrencyid As Integer
    Private mdecBaseexchangerate As Decimal
    Private mdecExpaidrate As Decimal
    Private mdecBaseamount As Decimal
    Private mintCountryunkid As Integer
    Private mdtTransactiondate As DateTime
    'Sohail (29 Jan 2013) -- End
    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Private minAuditUserid As Integer = 0
    'Private minAuditDate As DateTime
    'Private minClientIp As String = String.Empty
    'Private minloginemployeeunkid As Integer
    'Private mstrHostName As String = String.Empty
    'Private mstrFormName As String = String.Empty
    'Private blnIsFromWeb As Boolean = False
    'Hemant (24 Dec 2019) -- End
    Private mdtTable As DataTable
    Private mstrDatabaseName As String = "" 'Sohail (16 Apr 2015)
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetvehiclestranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Assetvehiclestranunkid() As Integer
        Get
            Return mintAssetvehiclestranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetvehiclestranunkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationunkid
    ''' Modify By: Sohail
    ''' </summary>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Property _Assetdeclarationunkid() As Integer
    Public Property _Assetdeclarationunkid(ByVal xDatabaseName As String) As Integer
        'Shani(24-Aug-2015) -- End
        Get
            Return mintAssetdeclarationunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclarationunkid = value

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call GetData()
            Call GetData(xDatabaseName)
            'Shani(24-Aug-2015) -- End

        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set vehicletype
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Vehicletype() As String
    '    Get
    '        Return mstrVehicletype
    '    End Get
    '    Set(ByVal value As String)
    '        mstrVehicletype = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set value
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Value() As Single
    '    Get
    '        Return msinValue
    '    End Get
    '    Set(ByVal value As Single)
    '        msinValue = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set servant
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Servant() As Double
    '    Get
    '        Return mdblServant
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblServant = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set wifehusband
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Wifehusband() As Double
    '    Get
    '        Return mdblWifehusband
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblWifehusband = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set children
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Children() As Double
    '    Get
    '        Return mdblChildren
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblChildren = value
    '    End Set
    'End Property

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    ''' <summary>
    ''' Purpose: Get or Set Vehicletype
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Vehicletype() As String
        Get
            Return mstrVehicletype
        End Get
        Set(ByVal value As String)
            mstrVehicletype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Value
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Value() As Decimal
        Get
            Return mdecValue
        End Get
        Set(ByVal value As Decimal)
            mdecValue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Servant
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Servant() As Double
        Get
            Return mdblServant
        End Get
        Set(ByVal value As Double)
            mdblServant = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Wifehusband
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Wifehusband() As Double
        Get
            Return mdblWifehusband
        End Get
        Set(ByVal value As Double)
            mdblWifehusband = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Children
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Children() As Double
        Get
            Return mdblChildren
        End Get
        Set(ByVal value As Double)
            mdblChildren = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isfinalsaved
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = value
        End Set
    End Property
    'Hemant (24 Dec 2019) -- End

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CurrencyUnkId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CurrencyUnkId() As Integer
        Get
            Return mintCurrencyUnkId
        End Get
        Set(ByVal value As Integer)
            mintCurrencyUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Model
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Model() As String
        Get
            Return mstrModel
        End Get
        Set(ByVal value As String)
            mstrModel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Colour
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Colour() As String
        Get
            Return mstrColour
        End Get
        Set(ByVal value As String)
            mstrColour = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Regno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Regno() As String
        Get
            Return mstrRegno
        End Get
        Set(ByVal value As String)
            mstrRegno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Vehicleuse
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Vehicleuse() As String
        Get
            Return mstrVehicleuse
        End Get
        Set(ByVal value As String)
            mstrVehicleuse = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Acquisitiondate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Acquisitiondate() As DateTime
        Get
            Return mdtAcquisitiondate
        End Get
        Set(ByVal value As DateTime)
            mdtAcquisitiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Basecurrencyid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Basecurrencyid() As Integer
        Get
            Return mintBasecurrencyid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Baseexchangerate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecBaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecBaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Expaidrate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Expaidrate() As Decimal
        Get
            Return mdecExpaidrate
        End Get
        Set(ByVal value As Decimal)
            mdecExpaidrate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Baseamount
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Baseamount() As Decimal
        Get
            Return mdecBaseamount
        End Get
        Set(ByVal value As Decimal)
            mdecBaseamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Countryunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Transactiondate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Transactiondate() As DateTime
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As DateTime)
            mdtTransactiondate = value
        End Set
    End Property

    'Public Property _AuditUserid() As Integer
    '    Get
    '        Return minAuditUserid
    '    End Get
    '    Set(ByVal value As Integer)
    '        minAuditUserid = value
    '    End Set
    'End Property

    'Public Property _AuditDate() As DateTime
    '    Get
    '        Return minAuditDate
    '    End Get
    '    Set(ByVal value As DateTime)
    '        minAuditDate = value
    '    End Set
    'End Property

    'Public Property _ClientIp() As String
    '    Get
    '        Return minClientIp
    '    End Get
    '    Set(ByVal value As String)
    '        minClientIp = value
    '    End Set
    'End Property

    'Public Property _Loginemployeeunkid() As Integer
    '    Get
    '        Return minloginemployeeunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        minloginemployeeunkid = value
    '    End Set
    'End Property

    'Public Property _HostName() As String
    '    Get
    '        Return mstrHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrHostName = value
    '    End Set
    'End Property

    'Public Property _FormName() As String
    '    Get
    '        Return mstrFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrFormName = value
    '    End Set
    'End Property

    'Public Property _IsFromWeb() As Boolean
    '    Get
    '        Return blnIsFromWeb
    '    End Get
    '    Set(ByVal value As Boolean)
    '        blnIsFromWeb = value
    '    End Set
    'End Property
    'Hemant (24 Dec 2019) -- End

    '''' <summary>
    '''' Purpose: Get or Set isvoid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Isvoid() As Boolean
    '    Get
    '        Return mblnIsvoid
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsvoid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiduserunkid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Voiduserunkid() As Integer
    '    Get
    '        Return mintVoiduserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintVoiduserunkid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiddatetime
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Voiddatetime() As Date
    '    Get
    '        Return mdtVoiddatetime
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtVoiddatetime = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voidreason
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Voidreason() As String
    '    Get
    '        Return mstrVoidreason
    '    End Get
    '    Set(ByVal value As String)
    '        mstrVoidreason = value
    '    End Set
    'End Property

    'Sohail (16 Apr 2015) -- Start
    'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
    'Sohail (16 Apr 2015) -- End

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTable = New DataTable("Vehicles")

        Try
            mdtTable.Columns.Add("assetvehiclestranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("assetdeclarationunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("vehicletype", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("value", System.Type.GetType("System.Decimal")) '.DefaultValue = -1

            mdtTable.Columns.Add("servant", System.Type.GetType("System.Decimal")) '.DefaultValue = -1
            mdtTable.Columns.Add("wifehusband", System.Type.GetType("System.Decimal")) '.DefaultValue = -1
            mdtTable.Columns.Add("children", System.Type.GetType("System.Decimal")) '.DefaultValue = -1
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1 'Sohail (06 Apr 2012)
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            mdtTable.Columns.Add("model", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("colour", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("regno", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("vehicleuse", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("acquisitiondate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("basecurrencyid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("baseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("expaidrate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("baseamount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime"))
            'Sohail (29 Jan 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Sub GetData()
    Public Sub GetData(ByVal xDatabaseName As String)
        'Shani(24-Aug-2015) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = xDatabaseName
        'Shani(24-Aug-2015) -- End

        'Sohail (16 Apr 2015) -- End

        Try
            strQ = "SELECT " & _
              "  assetvehiclestranunkid " & _
              ", assetdeclarationunkid " & _
              ", vehicletype " & _
              ", value " & _
              ", servant " & _
              ", wifehusband " & _
              ", children " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
                ", ISNULL(model, '') AS model " & _
                ", ISNULL(colour, '') AS colour " & _
                ", ISNULL(regno, '') AS regno " & _
                ", ISNULL(vehicleuse, '') AS vehicleuse " & _
                ", ISNULL(acquisitiondate, Getdate()) AS acquisitiondate " & _
                ", ISNULL(basecurrencyid, 0) AS basecurrencyid " & _
                ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
                ", ISNULL(expaidrate, 0) AS expaidrate " & _
                ", ISNULL(baseamount, 0) AS baseamount " & _
                ", ISNULL(countryunkid, 0) AS countryunkid " & _
                ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
             "FROM " & mstrDatabaseName & "..hrasset_vehicles_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationunkid = @assetdeclarationunkid "
            'Sohail (16 Apr 2015) - [mstrDatabaseName]
            'Sohail (29 Jan 2013) - [model, colour, regno, vehicleuse, acquisitiondate, baseexchangerate, basecurrencyid, expaidrate, baseamount, countryunkid, transactiondate]
            'Sohail (06 Apr 2012) - [currencyunkid]

            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()

                drRow.Item("assetvehiclestranunkid") = CInt(dtRow.Item("assetvehiclestranunkid"))
                drRow.Item("assetdeclarationunkid") = CInt(dtRow.Item("assetdeclarationunkid"))
                drRow.Item("vehicletype") = dtRow.Item("vehicletype").ToString
                drRow.Item("value") = CDec(dtRow.Item("value"))
                drRow.Item("servant") = CDbl(dtRow.Item("servant"))
                drRow.Item("wifehusband") = CDbl(dtRow.Item("wifehusband"))
                drRow.Item("children") = CDbl(dtRow.Item("children"))
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid")) 'Sohail (06 Apr 2012)
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                drRow.Item("model") = dtRow.Item("model").ToString
                drRow.Item("colour") = dtRow.Item("colour").ToString
                drRow.Item("regno") = dtRow.Item("regno").ToString
                drRow.Item("vehicleuse") = dtRow.Item("vehicleuse").ToString
                If IsDBNull(dtRow.Item("acquisitiondate")) = True Then
                    drRow.Item("acquisitiondate") = DBNull.Value
                Else
                    drRow.Item("acquisitiondate") = dtRow.Item("acquisitiondate")
                End If
                drRow.Item("basecurrencyid") = CInt(dtRow.Item("basecurrencyid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("expaidrate") = CDec(dtRow.Item("expaidrate"))
                drRow.Item("baseamount") = CDec(dtRow.Item("baseamount"))
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                'Sohail (29 Jan 2013) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Public Sub GetData(ByVal xDatabaseName As String, ByVal intAssetdeclarationUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        mintAssetdeclarationunkid = intAssetdeclarationUnkid

        mstrDatabaseName = xDatabaseName

        Try
            strQ = "SELECT " & _
              "  assetvehiclestranunkid " & _
              ", assetdeclarationunkid " & _
              ", vehicletype " & _
              ", value " & _
              ", servant " & _
              ", wifehusband " & _
              ", children " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
                ", ISNULL(model, '') AS model " & _
                ", ISNULL(colour, '') AS colour " & _
                ", ISNULL(regno, '') AS regno " & _
                ", ISNULL(vehicleuse, '') AS vehicleuse " & _
                ", ISNULL(acquisitiondate, Getdate()) AS acquisitiondate " & _
                ", ISNULL(basecurrencyid, 0) AS basecurrencyid " & _
                ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
                ", ISNULL(expaidrate, 0) AS expaidrate " & _
                ", ISNULL(baseamount, 0) AS baseamount " & _
                ", ISNULL(countryunkid, 0) AS countryunkid " & _
                ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
             "FROM " & mstrDatabaseName & "..hrasset_vehicles_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationunkid = @assetdeclarationunkid "

            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()

                drRow.Item("assetvehiclestranunkid") = CInt(dtRow.Item("assetvehiclestranunkid"))
                drRow.Item("assetdeclarationunkid") = CInt(dtRow.Item("assetdeclarationunkid"))
                drRow.Item("vehicletype") = dtRow.Item("vehicletype").ToString
                drRow.Item("value") = CDec(dtRow.Item("value"))
                drRow.Item("servant") = CDbl(dtRow.Item("servant"))
                drRow.Item("wifehusband") = CDbl(dtRow.Item("wifehusband"))
                drRow.Item("children") = CDbl(dtRow.Item("children"))
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("model") = dtRow.Item("model").ToString
                drRow.Item("colour") = dtRow.Item("colour").ToString
                drRow.Item("regno") = dtRow.Item("regno").ToString
                drRow.Item("vehicleuse") = dtRow.Item("vehicleuse").ToString
                If IsDBNull(dtRow.Item("acquisitiondate")) = True Then
                    drRow.Item("acquisitiondate") = DBNull.Value
                Else
                    drRow.Item("acquisitiondate") = dtRow.Item("acquisitiondate")
                End If
                drRow.Item("basecurrencyid") = CInt(dtRow.Item("basecurrencyid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("expaidrate") = CDec(dtRow.Item("expaidrate"))
                drRow.Item("baseamount") = CDec(dtRow.Item("baseamount"))
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Sub

    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetVehiclesTranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetvehiclestranunkid " & _
              ", assetdeclarationunkid " & _
              ", vehicletype " & _
              ", value " & _
              ", servant " & _
              ", wifehusband " & _
              ", children " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
                ", ISNULL(model, '') AS model " & _
                ", ISNULL(colour, '') AS colour " & _
                ", ISNULL(regno, '') AS regno " & _
                ", ISNULL(vehicleuse, '') AS vehicleuse " & _
                ", ISNULL(acquisitiondate, Getdate()) AS acquisitiondate " & _
                ", ISNULL(basecurrencyid, 0) AS basecurrencyid " & _
                ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
                ", ISNULL(expaidrate, 0) AS expaidrate " & _
                ", ISNULL(baseamount, 0) AS baseamount " & _
                ", ISNULL(countryunkid, 0) AS countryunkid " & _
                ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
             "FROM " & mstrDatabaseName & "..hrasset_vehicles_tran " & _
             "WHERE assetvehiclestranunkid = @assetvehiclestranunkid  "

            objDataOperation.AddParameter("@assetvehiclestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetVehiclesTranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetvehiclestranunkid = CInt(dtRow.Item("assetvehiclestranunkid"))
                mintAssetdeclarationunkid = CInt(dtRow.Item("assetdeclarationunkid"))
                mstrVehicletype = dtRow.Item("vehicletype").ToString
                mdecValue = CDec(dtRow.Item("value"))
                mdblServant = CDbl(dtRow.Item("servant"))
                mdblWifehusband = CDbl(dtRow.Item("wifehusband"))
                mdblChildren = CDbl(dtRow.Item("children"))
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mstrModel = dtRow.Item("model").ToString
                mstrColour = dtRow.Item("colour").ToString
                mstrRegno = dtRow.Item("regno").ToString
                mstrVehicleuse = dtRow.Item("vehicleuse").ToString
                If IsDBNull(dtRow.Item("acquisitiondate")) = True Then
                    mdtAcquisitiondate = Nothing
                Else
                    mdtAcquisitiondate = dtRow.Item("acquisitiondate")
                End If
                mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
                mdecBaseamount = CDec(dtRow.Item("baseamount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdtTransactiondate = Nothing
                Else
                    mdtTransactiondate = dtRow.Item("transactiondate")
                End If
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    'Public Function InserByDataTable() As Boolean
    '    Dim strQ As String = ""
    '    Dim strErrorMessage As String = ""
    '    Dim exForce As Exception

    '    Try
    '        objDataOperation = New clsDataOperation

    '        For Each dtRow As DataRow In mdtTable.Rows

    '            mstrVehicletype = dtRow.Item("vehicletype").ToString
    '            mdecValue = CDec(dtRow.Item("value"))
    '            If IsDBNull(dtRow.Item("servant")) = True Then
    '                mdblServant = 0
    '            Else
    '                mdblServant = CDec(dtRow.Item("servant"))
    '            End If
    '            If IsDBNull(dtRow.Item("wifehusband")) = True Then
    '                mdblWifehusband = 0
    '            Else
    '                mdblWifehusband = CDec(dtRow.Item("wifehusband"))
    '            End If
    '            If IsDBNull(dtRow.Item("children")) = True Then
    '                mdblChildren = 0
    '            Else
    '                mdblChildren = CDec(dtRow.Item("children"))
    '            End If
    '            mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
    '            'Sohail (07 Aug 2012) -- Start
    '            'TRA - ENHANCEMENT - Now user id is assigned from clsAssetdeclaration_master
    '            'mintUserunkid = CInt(dtRow.Item("userunkid"))
    '            'Sohail (07 Aug 2012) -- End
    '            mblnIsvoid = CBool(dtRow.Item("isvoid"))
    '            mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
    '            mdtVoiddatetime = Nothing
    '            mstrVoidreason = dtRow.Item("voidreason").ToString
    '            mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid")) 'Sohail (06 Apr 2012)
    '            'Sohail (29 Jan 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            mstrModel = dtRow.Item("model").ToString
    '            mstrColour = dtRow.Item("colour").ToString
    '            mstrRegno = dtRow.Item("regno").ToString
    '            mstrVehicleuse = dtRow.Item("vehicleuse").ToString
    '            mdtAcquisitiondate = dtRow.Item("acquisitiondate")
    '            mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
    '            mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
    '            mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
    '            mdecBaseamount = CDec(dtRow.Item("baseamount"))
    '            mintCountryunkid = CInt(dtRow.Item("countryunkid"))
    '            mdtTransactiondate = dtRow.Item("transactiondate")
    '            'Sohail (29 Jan 2013) -- End

    '            If Insert() = False Then
    '                Return False
    '            End If
    '        Next

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByRef decVehicleTotal As Decimal) As Boolean
    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByRef decVehicleTotal As Decimal, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp]
        'Shani(24-Aug-2015) -- End
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Hemant (24 Dec 2019) -- End
            For Each dtRow As DataRow In mdtTable.Rows

                mintAssetvehiclestranunkid = CInt(dtRow.Item("assetvehiclestranunkid"))
                mstrVehicletype = dtRow.Item("vehicletype").ToString
                mdecValue = CDec(dtRow.Item("value"))
                If IsDBNull(dtRow.Item("servant")) = True Then
                    mdblServant = 0
                Else
                    mdblServant = CDec(dtRow.Item("servant"))
                End If
                If IsDBNull(dtRow.Item("wifehusband")) = True Then
                    mdblWifehusband = 0
                Else
                    mdblWifehusband = CDec(dtRow.Item("wifehusband"))
                End If
                If IsDBNull(dtRow.Item("children")) = True Then
                    mdblChildren = 0
                Else
                    mdblChildren = CDec(dtRow.Item("children"))
                End If
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = Nothing
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mstrModel = dtRow.Item("model").ToString
                mstrColour = dtRow.Item("colour").ToString
                mstrRegno = dtRow.Item("regno").ToString
                mstrVehicleuse = dtRow.Item("vehicleuse").ToString
                mdtAcquisitiondate = dtRow.Item("acquisitiondate")
                mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
                mdecBaseamount = CDec(dtRow.Item("baseamount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                decTotal += CDec(dtRow.Item("baseamount"))

                If mintAssetvehiclestranunkid <= 0 Then

                    blnChildTableChanged = True

                    If Insert(objDataOperation) = False Then
                        'Hemant (24 Dec 2019) -- [objDataOperation]
                    Return False
                End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintAssetvehiclestranunkid.ToString
                    Else
                        strUnkIDs &= "," & mintAssetvehiclestranunkid.ToString
                    End If
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Hemant (24 Dec 2019) -- [objDataOperation]
                        Return False
                    End If
                End If
            Next

            decVehicleTotal = decTotal

            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetvehiclestranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each drRow In dRow

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If Void(CInt(drRow.Item("assetvehiclestranunkid")), mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    If Void(CInt(drRow.Item("assetvehiclestranunkid")), mintUserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Hemant (24 Dec 2019) -- [objDataOperation]
                        'Shani(24-Aug-2015) -- End
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function
    'Sohail (29 Jan 2013) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationUnkID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Hemant (24 Dec 2019) -- End

            strQ = "SELECT " & _
              "  assetvehiclestranunkid " & _
              ", assetdeclarationunkid " & _
              ", vehicletype " & _
              ", value " & _
              ", servant " & _
              ", wifehusband " & _
              ", children " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
                    ", ISNULL(model, '') AS model " & _
                    ", ISNULL(colour, '') AS colour " & _
                    ", ISNULL(regno, '') AS regno " & _
                    ", ISNULL(vehicleuse, '') AS vehicleuse " & _
                    ", ISNULL(acquisitiondate, Getdate()) AS acquisitiondate " & _
                    ", ISNULL(basecurrencyid, 0) AS basecurrencyid " & _
                    ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
                    ", ISNULL(expaidrate, 0) AS expaidrate " & _
                    ", ISNULL(baseamount, 0) AS baseamount " & _
                    ", ISNULL(countryunkid, 0) AS countryunkid " & _
                    ", ISNULL(transactiondate, Getdate()) AS transactiondate "

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            strQ &= ", CASE countryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS currency_sign "
            'Hemant (24 Dec 2019) -- End

            strQ &= "FROM hrasset_vehicles_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " 'Sohail (06 Apr 2012) - [currencyunkid]
            'Sohail (29 Jan 2013) - [model, colour, regno, vehicleuse, acquisitiondate, baseexchangerate, basecurrencyid, expaidrate, baseamount, countryunkid, transactiondate]

            If intAssetDeclarationUnkID > 0 Then
                strQ &= " AND assetdeclarationunkid = @assetdeclarationunkid "
                objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationUnkID)
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            Else
                strQ &= " AND 1 = 2 "
                'Hemant (24 Dec 2019) -- End
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrasset_vehicles_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_Master As clsAssetdeclaration_master = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp,objAD_Master]
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End
        Try
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If mintAssetdeclarationunkid <= 0 AndAlso objAD_Master IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_Master.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationunkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationunkid > 0 AndAlso objAD_Master IsNot Nothing Then
                If objAD_Master.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Hemant (24 Dec 2019) -- End
            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationunkid.ToString)
            objDataOperation.AddParameter("@vehicletype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVehicletype.ToString)
            objDataOperation.AddParameter("@value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecValue.ToString)
            objDataOperation.AddParameter("@servant", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblServant.ToString)
            objDataOperation.AddParameter("@wifehusband", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblWifehusband.ToString)
            objDataOperation.AddParameter("@children", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblChildren.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString) 'Sohail (06 Apr 2012)
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@model", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrModel.ToString)
            objDataOperation.AddParameter("@colour", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrColour.ToString)
            objDataOperation.AddParameter("@regno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegno.ToString)
            objDataOperation.AddParameter("@vehicleuse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVehicleuse.ToString)
            If mdtAcquisitiondate = Nothing Then
                objDataOperation.AddParameter("@acquisitiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisitiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcquisitiondate.ToString)
            End If
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@baseamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseamount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            'Sohail (29 Jan 2013) -- End

            strQ = "INSERT INTO hrasset_vehicles_tran ( " & _
              "  assetdeclarationunkid " & _
              ", vehicletype " & _
              ", value " & _
              ", servant " & _
              ", wifehusband " & _
              ", children " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", currencyunkid " & _
                ", model " & _
                ", colour " & _
                ", regno " & _
                ", vehicleuse " & _
                ", acquisitiondate" & _
                ", basecurrencyid " & _
                ", baseexchangerate " & _
                ", expaidrate " & _
                ", baseamount " & _
                ", countryunkid " & _
                ", transactiondate" & _
            ") VALUES (" & _
              "  @assetdeclarationunkid " & _
              ", @vehicletype " & _
              ", @value " & _
              ", @servant " & _
              ", @wifehusband " & _
              ", @children " & _
              ", @isfinalsaved " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @currencyunkid " & _
                ", @model " & _
                ", @colour " & _
                ", @regno " & _
                ", @vehicleuse " & _
                ", @acquisitiondate" & _
                ", @basecurrencyid " & _
                ", @baseexchangerate " & _
                ", @expaidrate " & _
                ", @baseamount " & _
                ", @countryunkid " & _
                ", @transactiondate" & _
            "); SELECT @@identity" 'Sohail (06 Apr 2012) - [currencyunkid]
            'Sohail (29 Jan 2013) - [model, colour, regno, vehicleuse, acquisitiondate, baseexchangerate, basecurrencyid, expaidrate, baseamount, countryunkid, transactiondate]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetvehiclestranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", mintAssetdeclarationunkid, "hrasset_vehicles_tran", "assetvehiclestranunkid", mintAssetvehiclestranunkid, 1, 1, , mintUserunkid) = False Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Hemant (24 Dec 2019) -- End
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (24 Dec 2019) -- End

            Return True
        Catch ex As Exception
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (24 Dec 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrasset_vehicles_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_Master As clsAssetdeclaration_master = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp,objAD_Master]
        'If isExist(mstrName, mintAssetvehiclestranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End
        Try
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If mintAssetdeclarationunkid <= 0 AndAlso objAD_Master IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_Master.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationunkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationunkid > 0 AndAlso objAD_Master IsNot Nothing Then
                If objAD_Master.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Hemant (24 Dec 2019) -- End
            objDataOperation.AddParameter("@assetvehiclestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetvehiclestranunkid.ToString)
            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationunkid.ToString)
            objDataOperation.AddParameter("@vehicletype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVehicletype.ToString)
            objDataOperation.AddParameter("@value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecValue.ToString)
            objDataOperation.AddParameter("@servant", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblServant.ToString)
            objDataOperation.AddParameter("@wifehusband", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblWifehusband.ToString)
            objDataOperation.AddParameter("@children", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblChildren.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString) 'Sohail (06 Apr 2012)
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@model", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrModel.ToString)
            objDataOperation.AddParameter("@colour", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrColour.ToString)
            objDataOperation.AddParameter("@regno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegno.ToString)
            objDataOperation.AddParameter("@vehicleuse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVehicleuse.ToString)
            If mdtAcquisitiondate = Nothing Then
                objDataOperation.AddParameter("@acquisitiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisitiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcquisitiondate.ToString)
            End If
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@baseamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseamount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            'Sohail (29 Jan 2013) -- End

            strQ = "UPDATE hrasset_vehicles_tran SET " & _
              "  assetdeclarationunkid = @assetdeclarationunkid" & _
              ", vehicletype = @vehicletype" & _
              ", value = @value" & _
              ", servant = @servant" & _
              ", wifehusband = @wifehusband" & _
              ", children = @children" & _
              ", isfinalsaved = @isfinalsaved " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", currencyunkid = @currencyunkid " & _
                        ", model = @model" & _
                        ", colour = @colour" & _
                        ", regno = @regno" & _
                        ", vehicleuse = @vehicleuse" & _
                        ", acquisitiondate = @acquisitiondate " & _
                        ", basecurrencyid = @basecurrencyid" & _
                        ", baseexchangerate = @baseexchangerate" & _
                        ", expaidrate = @expaidrate" & _
                        ", baseamount = @baseamount" & _
                        ", countryunkid = @countryunkid" & _
                        ", transactiondate = @transactiondate " & _
            "WHERE assetvehiclestranunkid = @assetvehiclestranunkid " 'Sohail (06 Apr 2012) - [currencyunkid]
            'Sohail (29 Jan 2013) - [model, colour, regno, vehicleuse, acquisitiondate, baseexchangerate, basecurrencyid, expaidrate, baseamount, countryunkid, transactiondate]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", mintAssetdeclarationunkid, "hrasset_vehicles_tran", "assetvehiclestranunkid", mintAssetvehiclestranunkid, 2, 2, , mintUserunkid) = False Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Hemant (24 Dec 2019) -- End 
               Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            blnChildTableChanged = True
            'Sohail (29 Jan 2013) -- End
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (24 Dec 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (24 Dec 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrasset_vehicles_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_Master As clsAssetdeclaration_master = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp,objAD_Master]
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End
        Try
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", mintAssetdeclarationunkid, "hrasset_vehicles_tran", "assetvehiclestranunkid", intUnkid, 2, 3, False, ) = False Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Hemant (24 Dec 2019) -- End
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If mintAssetdeclarationunkid <= 0 AndAlso objAD_Master IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_Master.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationunkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationunkid > 0 AndAlso objAD_Master IsNot Nothing Then
                If objAD_Master.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Hemant (24 Dec 2019) -- End

            strQ = "UPDATE hrasset_vehicles_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetvehiclestranunkid = @assetvehiclestranunkid "


            objDataOperation.AddParameter("@assetvehiclestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (24 Dec 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (24 Dec 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

    Public Function VoidByAssetDeclarationUnkID(ByVal intAssetDeclarationUnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp]
        'If isUsed(intEmpUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End
        Try
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", intAssetDeclarationUnkID, "hrasset_vehicles_tran", "assetvehiclestranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Hemant (24 Dec 2019) -- End
               Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            strQ = "UPDATE hrasset_vehicles_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationunkid = @assetdeclarationunkid "


            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationUnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (24 Dec 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (24 Dec 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetvehiclestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assetvehiclestranunkid " & _
              ", assetdeclarationunkid " & _
              ", vehicletype " & _
              ", value " & _
              ", servant " & _
              ", wifehusband " & _
              ", children " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
               ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
             "FROM hrasset_vehicles_tran " & _
             "WHERE name = @name " & _
             "AND code = @code " 'Sohail (06 Apr 2012) - [currencyunkid]

            If intUnkid > 0 Then
                strQ &= " AND assetvehiclestranunkid <> @assetvehiclestranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assetvehiclestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class