﻿'************************************************************************************************************************************
'Class Name : clsAsset_liabilitiesT2_tran.vb
'Purpose    :
'Date       :08/10/2019
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsAsset_liabilitiesT2_tran
    Private Const mstrModuleName = "clsAsset_liabilitiesT2_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetliabilitiest2tranunkid As Integer
    Private mintAssetdeclarationt2unkid As Integer
    Private mstrInstitutionname As String = String.Empty
    Private mstrLiabilitytype As String = String.Empty
    Private mintOrigcountryunkid As Integer
    Private mintOrigcurrencyUnkId As Integer
    Private mintOrigbasecurrencyunkid As Integer
    Private mdecOrigbaseexchangerate As Decimal
    Private mdecOrigexchangerate As Decimal
    Private mdecOriginalbalance As Decimal
    Private mintOutcountryunkid As Integer
    Private mintOutcurrencyUnkId As Integer
    Private mintOutbasecurrencyunkid As Integer
    Private mdecOutbaseexchangerate As Decimal
    Private mdecOutexchangerate As Decimal
    Private mdecOutstandingbalance As Decimal
    Private mdecDuration As Decimal
    Private mstrLoanpurpose As String = String.Empty
    Private mblnIsfinalsaved As Boolean
    Private mdtTransactiondate As DateTime
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime
    Private minloginemployeeunkid As Integer = 0
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private mdtTable As DataTable
    Private mstrDatabaseName As String = ""
    'Hemant (15 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement
    Private mintLiabilitytypeunkid As Integer
    Private mdtLiabilitydate As DateTime
    Private mdtPayoffdate As DateTime
    'Hemant (15 Nov 2018) -- End

    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Private mdecbaseoriginal_balance As Decimal = 0
    Private mdecbaseoutstanding_balance As Decimal = 0
    'Hemant (07 Dec 2018) -- End


#End Region

#Region " Properties "
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetliabilitiest2tranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Assetliabilitiest2tranunkid() As Integer
        Get
            Return mintAssetliabilitiest2tranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetliabilitiest2tranunkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationt2unkid
    ''' Modify By: Hemant
    ''' </summary>
    ''' Public Property _Assetdeclarationt2unkid() As Integer
    Public Property _Assetdeclarationt2unkid(ByVal xDatabase As String) As Integer
        Get
            Return mintAssetdeclarationt2unkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclarationt2unkid = value

            Call GetData(xDatabase)
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get or Set institution_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Institution_Name() As String
        Get
            Return mstrInstitutionname
        End Get
        Set(ByVal value As String)
            mstrInstitutionname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set liability_type
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Liability_Type() As String
        Get
            Return mstrLiabilitytype
        End Get
        Set(ByVal value As String)
            mstrLiabilitytype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set origcountryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Origcountryunkid() As Integer
        Get
            Return mintOrigcountryunkid
        End Get
        Set(ByVal value As Integer)
            mintOrigcountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set origcurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Origcurrencyunkid() As Integer
        Get
            Return mintOrigcurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintOrigcurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set origbasecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Origbasecurrencyunkid() As Integer
        Get
            Return mintOrigbasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintOrigbasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set origbaseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Origbaseexchangerate() As Decimal
        Get
            Return mdecOrigbaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecOrigbaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set origexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Origexchangerate() As Decimal
        Get
            Return mdecOrigexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecOrigexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set original_balance
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Original_Balance() As Decimal
        Get
            Return mdecOriginalbalance
        End Get
        Set(ByVal value As Decimal)
            mdecOriginalbalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outcountryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Outcountryunkid() As Integer
        Get
            Return mintOutcountryunkid
        End Get
        Set(ByVal value As Integer)
            mintOutcountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outcurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Outcurrencyunkid() As Integer
        Get
            Return mintOutcurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintOutcurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outbasecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Outbasecurrencyunkid() As Integer
        Get
            Return mintOutbasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintOutbasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outbaseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Outbaseexchangerate() As Decimal
        Get
            Return mdecOutbaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecOutbaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Outexchangerate() As Decimal
        Get
            Return mdecOutexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecOutexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outstanding_balance
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Outstanding_Balance() As Decimal
        Get
            Return mdecOutstandingbalance
        End Get
        Set(ByVal value As Decimal)
            mdecOutstandingbalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set duration
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Duration() As Decimal
        Get
            Return mdecDuration
        End Get
        Set(ByVal value As Decimal)
            mdecDuration = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_purpose
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Loan_Purpose() As String
        Get
            Return mstrLoanpurpose
        End Get
        Set(ByVal value As String)
            mstrLoanpurpose = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set liabilitytypeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Liabilitytypeunkid() As Integer
        Get
            Return mintLiabilitytypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLiabilitytypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set liabilitydate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Liabilitydate() As Date
        Get
            Return mdtLiabilitydate
        End Get
        Set(ByVal value As Date)
            mdtLiabilitydate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set payoffdate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Payoffdate() As Date
        Get
            Return mdtPayoffdate
        End Get
        Set(ByVal value As Date)
            mdtPayoffdate = Value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

    ''' <summary>
    ''' Purpose: Get or Set BaseOriginalBalance
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _BaseOriginalBalance() As Decimal
        Get
            Return mdecbaseoriginal_balance
        End Get
        Set(ByVal value As Decimal)
            mdecbaseoriginal_balance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set BaseOutstandingBalance
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _BaseOutstandingBalance() As Decimal
        Get
            Return mdecbaseoutstanding_balance
        End Get
        Set(ByVal value As Decimal)
            mdecbaseoutstanding_balance = value
        End Set
    End Property
    'Hemant (07 Dec 2018) -- End

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTable = New DataTable("Liabilities")

        Try
            mdtTable.Columns.Add("assetliabilitiest2tranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("assetdeclarationt2unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("institution_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("liability_type", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("origcountryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("origcurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("origbasecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("origbaseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("origexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("original_balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("outcountryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("outcurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("outbasecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("outbaseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("outexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("outstanding_balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("duration", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("loan_purpose", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            mdtTable.Columns.Add("liabilitytypeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("liabilitydate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("payoffdate", System.Type.GetType("System.DateTime"))
            'Hemant (15 Nov 2018) -- End
            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            mdtTable.Columns.Add("baseoriginal_balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("baseoutstanding_balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
            'Hemant (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <purpose> Assign all Property variable </purpose>   
    ''' Public Sub GetData()
    Public Sub GetData(ByVal xDatabase As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetliabilitiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", institution_name " & _
              ", liability_type " & _
              ", ISNULL(origcountryunkid, 0) AS origcountryunkid " & _
              ", ISNULL(origcurrencyunkid, 0) AS origcurrencyunkid " & _
              ", ISNULL(origbasecurrencyunkid, 0) AS origbasecurrencyunkid " & _
              ", ISNULL(origbaseexchangerate, 0) AS origbaseexchangerate " & _
              ", ISNULL(origexchangerate, 0) AS origexchangerate " & _
              ", ISNULL(original_balance, 0) AS original_balance " & _
              ", ISNULL(outcountryunkid, 0) AS outcountryunkid " & _
              ", ISNULL(outcurrencyunkid, 0) AS outcurrencyunkid " & _
              ", ISNULL(outbasecurrencyunkid, 0) AS outbasecurrencyunkid " & _
              ", ISNULL(outbaseexchangerate, 0) AS outbaseexchangerate " & _
              ", ISNULL(outexchangerate, 0) AS outexchangerate " & _
              ", ISNULL(outstanding_balance, 0) AS outstanding_balance " & _
              ", ISNULL(duration, 0) AS duration " & _
              ", loan_purpose " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(liabilitytypeunkid, 0) AS liabilitytypeunkid " & _
              ", liabilitydate " & _
              ", payoffdate " & _
              ", ISNULL(baseoriginal_balance, 0) AS baseoriginal_balance " & _
              ", ISNULL(baseoutstanding_balance, 0) AS baseoutstanding_balance " & _
             "FROM " & mstrDatabaseName & "..hrasset_liabilitiesT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid,liabilitydate,payoffdate]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetliabilitiest2tranunkid") = CInt(dtRow.Item("assetliabilitiest2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("institution_name") = dtRow.Item("institution_name").ToString
                drRow.Item("liability_type") = dtRow.Item("liability_type")
                drRow.Item("origcountryunkid") = CInt(dtRow.Item("origcountryunkid"))
                drRow.Item("origcurrencyunkid") = CInt(dtRow.Item("origcurrencyunkid"))
                drRow.Item("origbasecurrencyunkid") = CInt(dtRow.Item("origbasecurrencyunkid"))
                drRow.Item("origbaseexchangerate") = CDec(dtRow.Item("origbaseexchangerate"))
                drRow.Item("origexchangerate") = dtRow.Item("origexchangerate")
                drRow.Item("original_balance") = dtRow.Item("original_balance")
                drRow.Item("outcountryunkid") = CInt(dtRow.Item("outcountryunkid"))
                drRow.Item("outcurrencyunkid") = CInt(dtRow.Item("outcurrencyunkid"))
                drRow.Item("outbasecurrencyunkid") = CInt(dtRow.Item("outbasecurrencyunkid"))
                drRow.Item("outbaseexchangerate") = CDec(dtRow.Item("outbaseexchangerate"))
                drRow.Item("outexchangerate") = dtRow.Item("outexchangerate")
                drRow.Item("outstanding_balance") = dtRow.Item("outstanding_balance")
                drRow.Item("duration") = dtRow.Item("duration")
                drRow.Item("loan_purpose") = dtRow.Item("loan_purpose")
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                drRow.Item("liabilitytypeunkid") = CInt(dtRow.Item("liabilitytypeunkid"))
                If IsDBNull(dtRow.Item("liabilitydate")) = True Then
                    drRow.Item("liabilitydate") = DBNull.Value
                Else
                    drRow.Item("liabilitydate") = dtRow.Item("liabilitydate")
                End If
                If IsDBNull(dtRow.Item("payoffdate")) = True Then
                    drRow.Item("payoffdate") = DBNull.Value
                Else
                    drRow.Item("payoffdate") = dtRow.Item("payoffdate")
                End If
                'Hemant (15 Nov 2018) -- End
                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                drRow.Item("baseoriginal_balance") = CDec(dtRow.Item("baseoriginal_balance"))
                drRow.Item("baseoutstanding_balance") = CDec(dtRow.Item("baseoutstanding_balance"))
                'Hemant (07 Dec 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Hemant (14 Nov 2018) -- Start
    'Enhancement : Changes for Bind Transactions
    Public Sub GetData(ByVal xDatabase As String, ByVal intAssetdeclarationt2Unkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        mintAssetdeclarationt2unkid = intAssetdeclarationt2Unkid

        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetliabilitiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", institution_name " & _
              ", liability_type " & _
              ", ISNULL(origcountryunkid, 0) AS origcountryunkid " & _
              ", ISNULL(origcurrencyunkid, 0) AS origcurrencyunkid " & _
              ", ISNULL(origbasecurrencyunkid, 0) AS origbasecurrencyunkid " & _
              ", ISNULL(origbaseexchangerate, 0) AS origbaseexchangerate " & _
              ", ISNULL(origexchangerate, 0) AS origexchangerate " & _
              ", ISNULL(original_balance, 0) AS original_balance " & _
              ", ISNULL(outcountryunkid, 0) AS outcountryunkid " & _
              ", ISNULL(outcurrencyunkid, 0) AS outcurrencyunkid " & _
              ", ISNULL(outbasecurrencyunkid, 0) AS outbasecurrencyunkid " & _
              ", ISNULL(outbaseexchangerate, 0) AS outbaseexchangerate " & _
              ", ISNULL(outexchangerate, 0) AS outexchangerate " & _
              ", ISNULL(outstanding_balance, 0) AS outstanding_balance " & _
              ", ISNULL(duration, 0) AS duration " & _
              ", loan_purpose " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(liabilitytypeunkid, 0) AS liabilitytypeunkid " & _
              ", liabilitydate " & _
              ", payoffdate " & _
              ", ISNULL(baseoriginal_balance, 0) AS baseoriginal_balance " & _
              ", ISNULL(baseoutstanding_balance, 0) AS baseoutstanding_balance " & _
             "FROM " & mstrDatabaseName & "..hrasset_liabilitiesT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetliabilitiest2tranunkid") = CInt(dtRow.Item("assetliabilitiest2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("institution_name") = dtRow.Item("institution_name").ToString
                drRow.Item("liability_type") = dtRow.Item("liability_type")
                drRow.Item("origcountryunkid") = CInt(dtRow.Item("origcountryunkid"))
                drRow.Item("origcurrencyunkid") = CInt(dtRow.Item("origcurrencyunkid"))
                drRow.Item("origbasecurrencyunkid") = CInt(dtRow.Item("origbasecurrencyunkid"))
                drRow.Item("origbaseexchangerate") = CDec(dtRow.Item("origbaseexchangerate"))
                drRow.Item("origexchangerate") = dtRow.Item("origexchangerate")
                drRow.Item("original_balance") = dtRow.Item("original_balance")
                drRow.Item("outcountryunkid") = CInt(dtRow.Item("outcountryunkid"))
                drRow.Item("outcurrencyunkid") = CInt(dtRow.Item("outcurrencyunkid"))
                drRow.Item("outbasecurrencyunkid") = CInt(dtRow.Item("outbasecurrencyunkid"))
                drRow.Item("outbaseexchangerate") = CDec(dtRow.Item("outbaseexchangerate"))
                drRow.Item("outexchangerate") = dtRow.Item("outexchangerate")
                drRow.Item("outstanding_balance") = dtRow.Item("outstanding_balance")
                drRow.Item("duration") = dtRow.Item("duration")
                drRow.Item("loan_purpose") = dtRow.Item("loan_purpose")
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                drRow.Item("liabilitytypeunkid") = CInt(dtRow.Item("liabilitytypeunkid"))
                If IsDBNull(dtRow.Item("liabilitydate")) = True Then
                    drRow.Item("liabilitydate") = DBNull.Value
                Else
                    drRow.Item("liabilitydate") = dtRow.Item("liabilitydate")
                End If
                If IsDBNull(dtRow.Item("payoffdate")) = True Then
                    drRow.Item("payoffdate") = DBNull.Value
                Else
                    drRow.Item("payoffdate") = dtRow.Item("payoffdate")
                End If
                'Hemant (15 Nov 2018) -- End

                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                drRow.Item("baseoriginal_balance") = CDec(dtRow.Item("baseoriginal_balance"))
                drRow.Item("baseoutstanding_balance") = CDec(dtRow.Item("baseoutstanding_balance"))
                'Hemant (07 Dec 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Hemant (14 Nov 2018) -- End

    'Hemant (14 Nov 2018) -- End

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetliabilitiesT2tranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
               "  assetliabilitiest2tranunkid " & _
               ", assetdeclarationt2unkid " & _
               ", institution_name " & _
               ", liability_type " & _
               ", ISNULL(origcountryunkid, 0) AS origcountryunkid " & _
               ", ISNULL(origcurrencyunkid, 0) AS origcurrencyunkid " & _
               ", ISNULL(origbasecurrencyunkid, 0) AS origbasecurrencyunkid " & _
               ", ISNULL(origbaseexchangerate, 0) AS origbaseexchangerate " & _
               ", ISNULL(origexchangerate, 0) AS origexchangerate " & _
               ", ISNULL(original_balance, 0) AS original_balance " & _
               ", ISNULL(outcountryunkid, 0) AS outcountryunkid " & _
               ", ISNULL(outcurrencyunkid, 0) AS outcurrencyunkid " & _
               ", ISNULL(outbasecurrencyunkid, 0) AS outbasecurrencyunkid " & _
               ", ISNULL(outbaseexchangerate, 0) AS outbaseexchangerate " & _
               ", ISNULL(outexchangerate, 0) AS outexchangerate " & _
               ", ISNULL(outstanding_balance, 0) AS outstanding_balance " & _
               ", ISNULL(duration, 0) AS duration " & _
               ", loan_purpose " & _
               ", isfinalsaved " & _
               ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
               ", userunkid " & _
               ", isvoid " & _
               ", voiduserunkid " & _
               ", voiddatetime " & _
               ", voidreason " & _
               ", '' AS AUD " & _
               ", ISNULL(liabilitytypeunkid, 0) AS liabilitytypeunkid " & _
               ", liabilitydate " & _
               ", payoffdate " & _
               ", ISNULL(baseoriginal_balance, 0) AS baseoriginal_balance " & _
               ", ISNULL(baseoutstanding_balance, 0) AS baseoutstanding_balance " & _
              "FROM " & mstrDatabaseName & "..hrasset_liabilitiesT2_tran " & _
              "WHERE assetliabilitiest2tranunkid = @assetliabilitiest2tranunkid "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            objDataOperation.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetliabilitiesT2tranUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows

                mintAssetliabilitiest2tranunkid = CInt(dtRow.Item("assetliabilitiest2tranunkid"))
                mintAssetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mstrInstitutionname = dtRow.Item("institution_name").ToString
                mstrLiabilitytype = dtRow.Item("liability_type")
                mintOrigcountryunkid = CInt(dtRow.Item("origcountryunkid"))
                mintOrigcurrencyUnkId = CInt(dtRow.Item("origcurrencyunkid"))
                mintOrigbasecurrencyunkid = CInt(dtRow.Item("origbasecurrencyunkid"))
                mdecOrigbaseexchangerate = CDec(dtRow.Item("origbaseexchangerate"))
                mdecOrigexchangerate = dtRow.Item("origexchangerate")
                mdecOriginalbalance = dtRow.Item("original_balance")
                mintOutcountryunkid = CInt(dtRow.Item("outcountryunkid"))
                mintOutcurrencyUnkId = CInt(dtRow.Item("outcurrencyunkid"))
                mintOutbasecurrencyunkid = CInt(dtRow.Item("outbasecurrencyunkid"))
                mdecOutbaseexchangerate = CDec(dtRow.Item("outbaseexchangerate"))
                mdecOutexchangerate = dtRow.Item("outexchangerate")
                mdecOutstandingbalance = dtRow.Item("outstanding_balance")
                mdecDuration = dtRow.Item("duration")
                mstrLoanpurpose = dtRow.Item("loan_purpose")
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdtTransactiondate = Nothing
                Else
                    mdtTransactiondate = dtRow.Item("transactiondate")
                End If
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                mintLiabilitytypeunkid = CInt(dtRow.Item("liabilitytypeunkid"))
                If IsDBNull(dtRow.Item("liabilitydate")) = True Then
                    mdtLiabilitydate = Nothing
                Else
                    mdtLiabilitydate = dtRow.Item("liabilitydate")
                End If
                If IsDBNull(dtRow.Item("payoffdate")) = True Then
                    mdtPayoffdate = Nothing
                Else
                    mdtPayoffdate = dtRow.Item("payoffdate")
                End If

                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                mdecbaseoriginal_balance = CDec(dtRow.Item("baseoriginal_balance"))
                mdecbaseoutstanding_balance = CDec(dtRow.Item("baseoutstanding_balance"))
                'Hemant (07 Dec 2018) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByRef decBankTotal As Decimal, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End


            For Each dtRow As DataRow In mdtTable.Rows

                mintAssetliabilitiest2tranunkid = CInt(dtRow.Item("assetliabilitiest2tranunkid"))
                mstrInstitutionname = dtRow.Item("institution_name").ToString
                mstrLiabilitytype = dtRow.Item("liability_type")
                mintOrigcountryunkid = CInt(dtRow.Item("origcountryunkid"))
                mintOrigcurrencyUnkId = CInt(dtRow.Item("origcurrencyunkid"))
                mintOrigbasecurrencyunkid = CInt(dtRow.Item("origbasecurrencyunkid"))
                mdecOrigbaseexchangerate = CDec(dtRow.Item("origbaseexchangerate"))
                mdecOrigexchangerate = CDec(dtRow.Item("origexchangerate"))
                mdecOriginalbalance = CDec(dtRow.Item("original_balance"))
                mintOutcountryunkid = CInt(dtRow.Item("outcountryunkid"))
                mintOutcurrencyUnkId = CInt(dtRow.Item("outcurrencyunkid"))
                mintOutbasecurrencyunkid = CInt(dtRow.Item("outbasecurrencyunkid"))
                mdecOutbaseexchangerate = CDec(dtRow.Item("outbaseexchangerate"))
                mdecOutexchangerate = CDec(dtRow.Item("outexchangerate"))
                mdecOutstandingbalance = CDec(dtRow.Item("outstanding_balance"))
                mdecDuration = CDec(dtRow.Item("duration"))
                mstrLoanpurpose = dtRow.Item("loan_purpose").ToString
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = Nothing
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                mintLiabilitytypeunkid = CInt(dtRow.Item("liabilitytypeunkid"))
                If dtRow.Item("liabilitydate") IsNot DBNull.Value Then
                    mdtLiabilitydate = dtRow.Item("liabilitydate")
                End If
                If dtRow.Item("payoffdate") IsNot DBNull.Value Then
                    mdtPayoffdate = dtRow.Item("payoffdate")
                End If
                'Hemant (15 Nov 2018) -- End
                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                mdecbaseoriginal_balance = CDec(dtRow.Item("baseoriginal_balance"))
                mdecbaseoutstanding_balance = CDec(dtRow.Item("baseoutstanding_balance"))
                'Hemant (07 Dec 2018) -- End

                If mintAssetliabilitiest2tranunkid <= 0 Then

                    blnChildTableChanged = True

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If Insert() = False Then
                    If Insert(objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintAssetliabilitiest2tranunkid.ToString
                    Else
                        strUnkIDs &= "," & mintAssetliabilitiest2tranunkid.ToString
                    End If
                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged) = False Then
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                End If

            Next

            decBankTotal = decTotal

            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetliabilitiest2tranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each dtRow In dRow

                    mintAssetliabilitiest2tranunkid = CInt(dtRow.Item("assetliabilitiest2tranunkid"))
                    mstrInstitutionname = dtRow.Item("institution_name").ToString
                    mstrLiabilitytype = dtRow.Item("liability_type")
                    mintOrigcountryunkid = CInt(dtRow.Item("origcountryunkid"))
                    mintOrigcurrencyUnkId = CInt(dtRow.Item("origcurrencyunkid"))
                    mintOrigbasecurrencyunkid = CInt(dtRow.Item("origbasecurrencyunkid"))
                    mdecOrigbaseexchangerate = CDec(dtRow.Item("origbaseexchangerate"))
                    mdecOrigexchangerate = CDec(dtRow.Item("origexchangerate"))
                    mdecOriginalbalance = CDec(dtRow.Item("original_balance"))
                    mintOutcountryunkid = CInt(dtRow.Item("outcountryunkid"))
                    mintOutcurrencyUnkId = CInt(dtRow.Item("outcurrencyunkid"))
                    mintOutbasecurrencyunkid = CInt(dtRow.Item("outbasecurrencyunkid"))
                    mdecOutbaseexchangerate = CDec(dtRow.Item("outbaseexchangerate"))
                    mdecOutexchangerate = CDec(dtRow.Item("outexchangerate"))
                    mdecOutstandingbalance = CDec(dtRow.Item("outstanding_balance"))
                    mdecDuration = CDec(dtRow.Item("duration"))
                    mstrLoanpurpose = dtRow.Item("loan_purpose").ToString
                    mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                    mdtTransactiondate = dtRow.Item("transactiondate")
                    mblnIsvoid = CBool(dtRow.Item("isvoid"))
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = dtRow.Item("voidreason").ToString
                    'Hemant (15 Nov 2018) -- Start
                    'Enhancement : Changes for NMB Requirement
                    mintLiabilitytypeunkid = CInt(dtRow.Item("liabilitytypeunkid"))
                    If dtRow.Item("liabilitydate") IsNot DBNull.Value Then
                        mdtLiabilitydate = dtRow.Item("liabilitydate")
                    End If
                    If dtRow.Item("payoffdate") IsNot DBNull.Value Then
                        mdtPayoffdate = dtRow.Item("payoffdate")
                    End If
                    'Hemant (15 Nov 2018) -- End
                    'Hemant (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    mdecbaseoriginal_balance = CDec(dtRow.Item("baseoriginal_balance"))
                    mdecbaseoutstanding_balance = CDec(dtRow.Item("baseoutstanding_balance"))
                    'Hemant (07 Dec 2018) -- End
                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If Void(CInt(dtRow.Item("assetliabilitiest2tranunkid")), mintUserunkid, xCurrentDatetTime, "") = False Then
                    If Void(CInt(dtRow.Item("assetliabilitiest2tranunkid")), mintUserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationt2unkID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (07 Dec 2018) -- End

            strQ = "SELECT " & _
              "  hrasset_liabilitiesT2_tran.assetliabilitiest2tranunkid " & _
              ", hrasset_liabilitiesT2_tran.assetdeclarationt2unkid " & _
              ", hrasset_liabilitiesT2_tran.institution_name " & _
              ", hrasset_liabilitiesT2_tran.liability_type " & _
              ", ISNULL(libtype.name, '') AS liability_typename " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.origcountryunkid, 0) AS origcountryunkid " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.origcurrencyunkid, 0) AS origcurrencyunkid " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.origbasecurrencyunkid, 0) AS origbasecurrencyunkid " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.origbaseexchangerate, 0) AS origbaseexchangerate " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.origexchangerate, 0) AS origexchangerate " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.original_balance, 0) AS original_balance " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.outcountryunkid, 0) AS outcountryunkid " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.outcurrencyunkid, 0) AS outcurrencyunkid " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.outbasecurrencyunkid, 0) AS outbasecurrencyunkid " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.outbaseexchangerate, 0) AS outbaseexchangerate " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.outexchangerate, 0) AS outexchangerate " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.outstanding_balance, 0) AS outstanding_balance " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.duration, 0) AS duration " & _
              ", hrasset_liabilitiesT2_tran.loan_purpose " & _
              ", hrasset_liabilitiesT2_tran.isfinalsaved " & _
              ", hrasset_liabilitiesT2_tran.transactiondate " & _
              ", hrasset_liabilitiesT2_tran.userunkid " & _
              ", hrasset_liabilitiesT2_tran.isvoid " & _
              ", hrasset_liabilitiesT2_tran.voiduserunkid " & _
              ", hrasset_liabilitiesT2_tran.voiddatetime " & _
              ", hrasset_liabilitiesT2_tran.voidreason " & _
              ", hrasset_liabilitiesT2_tran.liabilitytypeunkid " & _
              ", hrasset_liabilitiesT2_tran.liabilitydate " & _
              ", hrasset_liabilitiesT2_tran.payoffdate " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.baseoriginal_balance, 0) AS baseoriginal_balance " & _
              ", ISNULL(hrasset_liabilitiesT2_tran.baseoutstanding_balance, 0) AS baseoutstanding_balance "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            strQ &= ", CASE hrasset_liabilitiesT2_tran.origcountryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS origcurrency_sign "

            strQ &= ", CASE hrasset_liabilitiesT2_tran.outcountryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS outcurrency_sign "
            'Sohail (07 Dec 2018) -- End

            strQ &= "FROM hrasset_liabilitiesT2_tran " & _
            "LEFT JOIN cfcommon_master AS libtype ON libtype.masterunkid = hrasset_liabilitiesT2_tran.liabilitytypeunkid AND libtype.mastertype = " & CInt(clsCommon_Master.enCommonMaster.LIABILITY_TYPE_ASSET_DECLARATION_TEMPLATE2) & " " & _
             "WHERE ISNULL(hrasset_liabilitiesT2_tran.isvoid, 0 ) = 0 "
            'Sohail (07 Dec 2018) - [LEFT JOIN cfcommon_master AS libtype ]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If intAssetDeclarationt2unkID > 0 Then
            '    strQ &= " AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            '    objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2unkID)
            'End If
            If intAssetDeclarationt2unkID > 0 Then
                strQ &= " AND hrasset_liabilitiesT2_tran.assetdeclarationt2unkid = @assetdeclarationt2unkid "
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2unkID)
            Else
                strQ &= " AND 1 = 2 "
            End If
            'Sohail (07 Dec 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrasset_liabilitiesT2_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Gajanan (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End        
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If

            objDataOperation.ClearParameters()
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@institution_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInstitutionname.ToString)
            'Hemant (15 Mar 2019) -- Start
            'ISSUE# : Error Throw "SqlErrorNumber : 8152 ;String or binary data Would be truncated."
            'objDataOperation.AddParameter("@liability_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLiabilitytype.ToString)
            objDataOperation.AddParameter("@liability_type", SqlDbType.NVarChar, mstrLiabilitytype.Length, mstrLiabilitytype.ToString)
            'Hemant (15 Mar 2019) -- End
            objDataOperation.AddParameter("@origcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigcountryunkid.ToString)
            objDataOperation.AddParameter("@origcurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigcurrencyUnkId.ToString)
            objDataOperation.AddParameter("@origbasecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@origbaseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOrigbaseexchangerate.ToString)
            objDataOperation.AddParameter("@origexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOrigexchangerate.ToString)
            objDataOperation.AddParameter("@original_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOriginalbalance.ToString)
            objDataOperation.AddParameter("@outcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutcountryunkid.ToString)
            objDataOperation.AddParameter("@outcurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutcurrencyUnkId.ToString)
            objDataOperation.AddParameter("@outbasecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@outbaseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutbaseexchangerate.ToString)
            objDataOperation.AddParameter("@outexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutexchangerate.ToString)
            objDataOperation.AddParameter("@outstanding_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutstandingbalance.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDuration.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanpurpose.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDataOperation.AddParameter("@liabilitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLiabilitytypeunkid.ToString)
            If mdtLiabilitydate = Nothing Then
                objDataOperation.AddParameter("@liabilitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@liabilitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLiabilitydate.ToString)
            End If
            If mdtPayoffdate = Nothing Then
                objDataOperation.AddParameter("@payoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@payoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPayoffdate.ToString)
            End If
            'Hemant (15 Nov 2018) -- End

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            objDataOperation.AddParameter("@baseoriginal_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseoriginal_balance.ToString)
            objDataOperation.AddParameter("@baseoutstanding_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseoutstanding_balance.ToString)
            'Hemant (07 Dec 2018) -- End


            strQ = "INSERT INTO hrasset_liabilitiesT2_tran ( " & _
              "  assetdeclarationt2unkid " & _
              ", institution_name " & _
              ", liability_type " & _
              ", origcountryunkid " & _
              ", origcurrencyunkid " & _
              ", origbasecurrencyunkid " & _
              ", origbaseexchangerate " & _
              ", origexchangerate " & _
              ", original_balance " & _
              ", outcountryunkid " & _
              ", outcurrencyunkid " & _
              ", outbasecurrencyunkid " & _
              ", outbaseexchangerate " & _
              ", outexchangerate " & _
              ", outstanding_balance " & _
              ", duration " & _
              ", loan_purpose " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", liabilitytypeunkid" & _
              ", liabilitydate" & _
              ", payoffdate" & _
              ", baseoriginal_balance" & _
              ", baseoutstanding_balance" & _
            ") VALUES (" & _
              "  @assetdeclarationt2unkid " & _
              ", @institution_name " & _
              ", @liability_type " & _
              ", @origcountryunkid " & _
              ", @origcurrencyunkid " & _
              ", @origbasecurrencyunkid " & _
              ", @origbaseexchangerate " & _
              ", @origexchangerate " & _
              ", @original_balance " & _
              ", @outcountryunkid " & _
              ", @outcurrencyunkid " & _
              ", @outbasecurrencyunkid " & _
              ", @outbaseexchangerate " & _
              ", @outexchangerate " & _
              ", @outstanding_balance" & _
              ", @duration" & _
              ", @loan_purpose " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @liabilitytypeunkid" & _
              ", @liabilitydate" & _
              ", @payoffdate" & _
              ", @baseoriginal_balance" & _
              ", @baseoutstanding_balance" & _
            "); SELECT @@identity"

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetliabilitiest2tranunkid = dsList.Tables(0).Rows(0).Item(0)


            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_liabilitiesT2_tran", "assetliabilitiest2tranunkid", mintAssetliabilitiest2tranunkid, 1, 1, , mintUserunkid) = False Then
            '    Return False
            'End If

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function


    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrasset_liabilitiesT2_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End         
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End
            objDataOperation.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetliabilitiest2tranunkid.ToString)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@institution_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInstitutionname.ToString)
            'Hemant (15 Mar 2019) -- Start
            'ISSUE# : Error Throw "SqlErrorNumber : 8152 ;String or binary data Would be truncated."
            'objDataOperation.AddParameter("@liability_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLiabilitytype.ToString)
            objDataOperation.AddParameter("@liability_type", SqlDbType.NVarChar, mstrLiabilitytype.Length, mstrLiabilitytype.ToString)
            'Hemant (15 Mar 2019) -- End
            objDataOperation.AddParameter("@origcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigcountryunkid.ToString)
            objDataOperation.AddParameter("@origcurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigcurrencyUnkId.ToString)
            objDataOperation.AddParameter("@origbasecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@origbaseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOrigbaseexchangerate.ToString)
            objDataOperation.AddParameter("@origexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOrigexchangerate.ToString)
            objDataOperation.AddParameter("@original_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOriginalbalance.ToString)
            objDataOperation.AddParameter("@outcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutcountryunkid.ToString)
            objDataOperation.AddParameter("@outcurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutcurrencyUnkId.ToString)
            objDataOperation.AddParameter("@outbasecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@outbaseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutbaseexchangerate.ToString)
            objDataOperation.AddParameter("@outexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutexchangerate.ToString)
            objDataOperation.AddParameter("@outstanding_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutstandingbalance.ToString)
            objDataOperation.AddParameter("@duration", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDuration.ToString)
            objDataOperation.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanpurpose.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDataOperation.AddParameter("@liabilitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLiabilitytypeunkid.ToString)
            If mdtLiabilitydate = Nothing Then
                objDataOperation.AddParameter("@liabilitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@liabilitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLiabilitydate.ToString)
            End If
            If mdtPayoffdate = Nothing Then
                objDataOperation.AddParameter("@payoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@payoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPayoffdate.ToString)
            End If
            'Hemant (15 Nov 2018) -- End
            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            objDataOperation.AddParameter("@baseoriginal_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseoriginal_balance.ToString)
            objDataOperation.AddParameter("@baseoutstanding_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseoutstanding_balance.ToString)
            'Hemant (07 Dec 2018) -- End

            strQ = "UPDATE hrasset_liabilitiesT2_tran SET " & _
              "  assetdeclarationt2unkid = @assetdeclarationt2unkid" & _
              ", institution_name = @institution_name" & _
              ", liability_type = @liability_type" & _
              ", origcountryunkid = @origcountryunkid" & _
              ", origcurrencyunkid = @origcurrencyunkid " & _
              ", origbasecurrencyunkid = @origbasecurrencyunkid" & _
              ", origbaseexchangerate = @origbaseexchangerate" & _
              ", origexchangerate = @origexchangerate" & _
              ", original_balance = @original_balance" & _
              ", outcountryunkid = @outcountryunkid" & _
              ", outcurrencyunkid = @outcurrencyunkid " & _
              ", outbasecurrencyunkid = @outbasecurrencyunkid" & _
              ", outbaseexchangerate = @outbaseexchangerate" & _
              ", outexchangerate = @outexchangerate" & _
              ", outstanding_balance = @outstanding_balance" & _
              ", duration = @duration" & _
              ", loan_purpose = @loan_purpose " & _
              ", isfinalsaved = @isfinalsaved " & _
              ", transactiondate = @transactiondate " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", liabilitytypeunkid = @liabilitytypeunkid " & _
              ", liabilitydate = @liabilitydate " & _
              ", payoffdate = @payoffdate " & _
              ", baseoriginal_balance = @baseoriginal_balance " & _
              ", baseoutstanding_balance = @baseoutstanding_balance " & _
            "WHERE assetliabilitiest2tranunkid = @assetliabilitiest2tranunkid "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_liabilitiesT2_tran", "assetliabilitiest2tranunkid", mintAssetliabilitiest2tranunkid, 2, 2, , mintUserunkid) = False Then
            '    Return False
            'End If
            If IsTableDataUpdate(mintAssetliabilitiest2tranunkid, objDataOperation) = False Then
            If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                Return False
            End If
            End If

            blnChildTableChanged = True
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrasset_liabilitiesT2_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End            
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_liabilitiesT2_tran", "assetliabilitiest2tranunkid", intUnkid, 2, 3, False, ) = False Then

            '    Return False
            'End If

            strQ = "UPDATE hrasset_liabilitiesT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetliabilitiest2tranunkid = @assetliabilitiest2tranunkid "

            objDataOperation.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    Public Function VoidByAssetDeclarationt2unkID(ByVal intAssetDeclarationt2UnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclarationt2UnkID, "hrasset_liabilitiesT2_tran", "assetliabilitiest2tranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then

            '    Return False
            'End If

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            strQ = "INSERT INTO athrasset_liabilitiesT2_tran ( " & _
                     "  assetliabilitiest2tranunkid " & _
                      ", assetdeclarationt2unkid " & _
                      ", institution_name " & _
                      ", liability_type " & _
                      ", origcountryunkid " & _
                      ", origcurrencyunkid " & _
                      ", origbasecurrencyunkid " & _
                      ", origbaseexchangerate " & _
                      ", origexchangerate " & _
                      ", original_balance " & _
                      ", outcountryunkid " & _
                      ", outcurrencyunkid " & _
                      ", outbasecurrencyunkid " & _
                      ", outbaseexchangerate " & _
                      ", outexchangerate " & _
                      ", outstanding_balance " & _
                      ", duration " & _
                      ", loan_purpose " & _
                      ", isfinalsaved " & _
                      ", transactiondate" & _
                     ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
                     ", liabilitytypeunkid " & _
                     ", liabilitydate " & _
                     ", payoffdate  " & _
                     ", baseoriginal_balance " & _
                     ", baseoutstanding_balance " & _
                     " ) " & _
                 "SELECT " & _
                     "  assetliabilitiest2tranunkid " & _
                      ", assetdeclarationt2unkid " & _
                      ", institution_name " & _
                      ", liability_type " & _
                      ", origcountryunkid " & _
                      ", origcurrencyunkid " & _
                      ", origbasecurrencyunkid " & _
                      ", origbaseexchangerate " & _
                      ", origexchangerate " & _
                      ", original_balance " & _
                      ", outcountryunkid " & _
                      ", outcurrencyunkid " & _
                      ", outbasecurrencyunkid " & _
                      ", outbaseexchangerate " & _
                      ", outexchangerate " & _
                      ", outstanding_balance " & _
                      ", duration " & _
                      ", loan_purpose " & _
                      ", isfinalsaved " & _
                      ", transactiondate" & _
                     ", GETDATE() " & _
                     ", 3 " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
                     ", @isweb " & _
                     ", liabilitytypeunkid " & _
                     ", liabilitydate " & _
                     ", payoffdate " & _
                     ", baseoriginal_balance " & _
                     ", baseoutstanding_balance " & _
             "FROM hrasset_liabilitiesT2_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End

            strQ = "UPDATE hrasset_liabilitiesT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'If Insert_AtTranLog(objDataOperation, 3) = False Then
            '    Return False
            'End If
            'Hemant (14 Nov 2018) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationt2UnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' </summary>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assetliabilitiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", institution_name " & _
              ", liability_type " & _
              ", ISNULL(origcountryunkid, 0) AS origcountryunkid " & _
              ", ISNULL(origcurrencyunkid, 0) AS origcurrencyunkid " & _
              ", ISNULL(origbasecurrencyunkid, 0) AS origbasecurrencyunkid " & _
              ", ISNULL(origbaseexchangerate, 0) AS origbaseexchangerate " & _
              ", ISNULL(origexchangerate, 0) AS origexchangerate " & _
              ", ISNULL(original_balance, 0) AS original_balance " & _
              ", ISNULL(outcountryunkid, 0) AS outcountryunkid " & _
              ", ISNULL(outcurrencyunkid, 0) AS outcurrencyunkid " & _
              ", ISNULL(outbasecurrencyunkid, 0) AS outbasecurrencyunkid " & _
              ", ISNULL(outbaseexchangerate, 0) AS outbaseexchangerate " & _
              ", ISNULL(outexchangerate, 0) AS outexchangerate " & _
              ", ISNULL(outstanding_balance, 0) AS outstanding_balance " & _
              ", ISNULL(duration, 0) AS duration " & _
              ", loan_purpose " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", liabilitytypeunkid " & _
              ", liabilitydate " & _
              ", payoffdate " & _
              ", ISNULL(baseoriginal_balance, 0) AS baseoriginal_balance " & _
              ", ISNULL(baseoutstanding_balance, 0) AS baseoutstanding_balance " & _
             "FROM hrasset_liabilitiesT2_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            If intUnkid > 0 Then
                strQ &= " AND assetliabilitiest2tranunkid <> @assetliabilitiest2tranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    'Public Function getClientsList(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
    '    Dim strQ As String = ""
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        If blnFlag Then
    '            strQ = "SELECT @Select As Name, 0 As id UNION "
    '        End If

    '        strQ &= "SELECT @Supplier As Name, 1 As id " & _
    '                "UNION SELECT @Borrower As Name, 2 As id " & _
    '                "UNION SELECT @Other As Name, 3 As id " & _
    '                "Order By id "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Select"))
    '        objDataOperation.AddParameter("@Supplier", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Supplier"))
    '        objDataOperation.AddParameter("@Borrower", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Borrower"))
    '        objDataOperation.AddParameter("@Other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Other"))

    '        Return objDataOperation.ExecQuery(strQ, strListName)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "[getGenderList]")
    '    End Try
    'End Function
    'Sohail (22 Nov 2018) -- End

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            objDoOps.ClearParameters()
            objDoOps.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetliabilitiest2tranunkid.ToString)
            objDoOps.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDoOps.AddParameter("@institution_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInstitutionname.ToString)
            'Hemant (15 Mar 2019) -- Start
            'ISSUE# : Error Throw "SqlErrorNumber : 8152 ;String or binary data Would be truncated."
            'objDoOps.AddParameter("@liability_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLiabilitytype.ToString)
            objDoOps.AddParameter("@liability_type", SqlDbType.NVarChar, mstrLiabilitytype.Length, mstrLiabilitytype.ToString)
            'Hemant (15 Mar 2019) -- End
            objDoOps.AddParameter("@origcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigcountryunkid.ToString)
            objDoOps.AddParameter("@origcurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigcurrencyUnkId.ToString)
            objDoOps.AddParameter("@origbasecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOrigbasecurrencyunkid.ToString)
            objDoOps.AddParameter("@origbaseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOrigbaseexchangerate.ToString)
            objDoOps.AddParameter("@origexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOrigexchangerate.ToString)
            objDoOps.AddParameter("@original_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOriginalbalance.ToString)
            objDoOps.AddParameter("@outcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutcountryunkid.ToString)
            objDoOps.AddParameter("@outcurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutcurrencyUnkId.ToString)
            objDoOps.AddParameter("@outbasecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOutbasecurrencyunkid.ToString)
            objDoOps.AddParameter("@outbaseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutbaseexchangerate.ToString)
            objDoOps.AddParameter("@outexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutexchangerate.ToString)
            objDoOps.AddParameter("@outstanding_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutstandingbalance.ToString)
            objDoOps.AddParameter("@duration", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDuration.ToString)
            objDoOps.AddParameter("@loan_purpose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanpurpose.ToString)
            objDoOps.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDoOps.AddParameter("@liabilitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLiabilitytypeunkid.ToString)
            If mdtLiabilitydate = Nothing Then
                objDoOps.AddParameter("@liabilitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@liabilitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLiabilitydate.ToString)
            End If
            If mdtPayoffdate = Nothing Then
                objDoOps.AddParameter("@payoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@payoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPayoffdate.ToString)
            End If
            'Hemant (15 Nov 2018) -- End
            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            objDoOps.AddParameter("@baseoriginal_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseoriginal_balance.ToString)
            objDoOps.AddParameter("@baseoutstanding_balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseoutstanding_balance.ToString)
            'Hemant (07 Dec 2018) -- End

            StrQ = "INSERT INTO athrasset_liabilitiesT2_tran ( " & _
              "  assetliabilitiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", institution_name " & _
              ", liability_type " & _
              ", origcountryunkid " & _
              ", origcurrencyunkid " & _
              ", origbasecurrencyunkid " & _
              ", origbaseexchangerate " & _
              ", origexchangerate " & _
              ", original_balance " & _
              ", outcountryunkid " & _
              ", outcurrencyunkid " & _
              ", outbasecurrencyunkid " & _
              ", outbaseexchangerate " & _
              ", outexchangerate " & _
              ", outstanding_balance " & _
              ", duration " & _
              ", loan_purpose " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
              ", liabilitytypeunkid" & _
              ", liabilitydate" & _
              ", payoffdate" & _
              ", baseoriginal_balance " & _
              ", baseoutstanding_balance " & _
            ") VALUES (" & _
              "  @assetliabilitiest2tranunkid " & _
              ", @assetdeclarationt2unkid " & _
              ", @institution_name " & _
              ", @liability_type " & _
              ", @origcountryunkid " & _
              ", @origcurrencyunkid " & _
              ", @origbasecurrencyunkid " & _
              ", @origbaseexchangerate " & _
              ", @origexchangerate " & _
              ", @original_balance " & _
              ", @outcountryunkid " & _
              ", @outcurrencyunkid " & _
              ", @outbasecurrencyunkid " & _
              ", @outbaseexchangerate " & _
              ", @outexchangerate " & _
              ", @outstanding_balance" & _
              ", @duration" & _
              ", @loan_purpose " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", GETDATE() " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
              ", @isweb " & _
              ", @liabilitytypeunkid" & _
              ", @liabilitydate" & _
              ", @payoffdate  " & _
              ", @baseoriginal_balance " & _
              ", @baseoutstanding_balance " & _
              " ) "

            'Hemant (07 Dec 2018) -- [baseoriginal_balance,baseoutstanding_balance]
            'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'strQ = "select TOP 1 * from athrasset_liabilitiesT2_tran where assetliabilitiest2tranunkid = @assetliabilitiest2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype = 2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrasset_liabilitiesT2_tran where assetliabilitiest2tranunkid = @assetliabilitiest2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype <> 3 ORDER BY auditdatetime DESC"
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetliabilitiest2tranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                If dr("liabilitytypeunkid") Is DBNull.Value Or dr("liabilitydate") Is DBNull.Value Or dr("payoffdate") Is DBNull.Value Then
                    Return False
                    'Hemant (15 Nov 2018) -- End
                ElseIf dr("institution_name").ToString() = mstrInstitutionname AndAlso dr("liability_type").ToString() = mstrLiabilitytype AndAlso _
                   dr("original_balance").ToString() = mdecOriginalbalance AndAlso dr("origcountryunkid").ToString() = mintOrigcountryunkid _
                   AndAlso dr("outstanding_balance").ToString() = mdecOutstandingbalance AndAlso dr("outcountryunkid").ToString() = mintOutcountryunkid _
                   AndAlso dr("duration").ToString() = mdecDuration AndAlso dr("loan_purpose").ToString() = mstrLoanpurpose _
                    AndAlso dr("liabilitytypeunkid").ToString() = mintLiabilitytypeunkid AndAlso CDate(dr("liabilitydate")) = mdtLiabilitydate _
                    AndAlso CDate(dr("payoffdate")) = mdtPayoffdate Then
                    'Hemant (15 Nov 2018) -- [liabilitytypeunkid, liabilitydate, payoffdate ]
                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class