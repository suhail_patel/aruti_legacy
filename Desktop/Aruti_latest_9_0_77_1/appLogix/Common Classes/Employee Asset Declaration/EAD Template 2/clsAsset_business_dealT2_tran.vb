﻿'************************************************************************************************************************************
'Class Name : clsAsset_business_dealT2_tran.vb
'Purpose    :
'Date       :08/10/2019
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsAsset_business_dealT2_tran
    Private Const mstrModuleName = "clsAsset_business_dealT2_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetbusinessdealt2tranunkid As Integer
    Private mintAssetdeclarationt2unkid As Integer
    Private mintAssetsectorunkid As String = String.Empty
    Private mstrCompany_org_name As String = String.Empty
    Private mstrCompany_tin_no As String = String.Empty
    Private mdtRegistration_date As DateTime
    Private mstrAddress_location As String = String.Empty
    Private mstrBusiness_type As String = String.Empty
    Private mstrPosition_held As String = String.Empty
    Private mintIs_supplier_client As Integer
    Private mintCountryunkid As Integer
    Private mintCurrencyUnkId As Integer
    Private mintBasecurrencyunkid As Integer
    Private mdecBaseexchangerate As Decimal
    Private mdecExchangerate As Decimal
    Private mdecMonthly_annual_earnings As Decimal
    Private mdecBasemonthly_annual_earnings As Decimal
    Private mstrBusiness_contact_no As String = String.Empty
    Private mstrShareholders_names As String = String.Empty
    Private mblnIsfinalsaved As Boolean
    Private mdtTransactiondate As DateTime
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintAuditUserId As Integer = 0
    Private minAuditDate As DateTime
    Private minloginemployeeunkid As Integer = 0
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private mdtTable As DataTable
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetbusinessdealt2tranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Assetbusinessdealt2tranunkid() As Integer
        Get
            Return mintAssetbusinessdealt2tranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetbusinessdealt2tranunkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationt2unkid
    ''' Modify By: Hemant
    ''' </summary>
    ''' Public Property _Assetdeclarationt2unkid() As Integer
    Public Property _Assetdeclarationt2unkid(ByVal xDatabase As String) As Integer
        Get
            Return mintAssetdeclarationt2unkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclarationt2unkid = value

            Call GetData(xDatabase)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetsectorunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Assetsectorunkid() As Integer
        Get
            Return mintAssetsectorunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetsectorunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company_org_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company_Org_Name() As String
        Get
            Return mstrCompany_Org_Name
        End Get
        Set(ByVal value As String)
            mstrCompany_Org_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company_tin_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company_Tin_No() As String
        Get
            Return mstrCompany_Tin_No
        End Get
        Set(ByVal value As String)
            mstrCompany_Tin_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set registration_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Registration_Date() As Date
        Get
            Return mdtRegistration_Date
        End Get
        Set(ByVal value As Date)
            mdtRegistration_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address_location
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address_Location() As String
        Get
            Return mstrAddress_Location
        End Get
        Set(ByVal value As String)
            mstrAddress_Location = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set business_type
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Business_Type() As String
        Get
            Return mstrBusiness_Type
        End Get
        Set(ByVal value As String)
            mstrBusiness_Type = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set position_held
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Position_Held() As String
        Get
            Return mstrPosition_Held
        End Get
        Set(ByVal value As String)
            mstrPosition_Held = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_supplier_client
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Is_Supplier_Client() As Integer
        Get
            Return mintIs_Supplier_Client
        End Get
        Set(ByVal value As Integer)
            mintIs_Supplier_Client = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basecurrencyunkid() As Integer
        Get
            Return mintBasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecBaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecBaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Exchangerate() As Decimal
        Get
            Return mdecExchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecExchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set monthly_annual_earnings
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Monthly_Annual_Earnings() As Decimal
        Get
            Return mdecMonthly_annual_earnings
        End Get
        Set(ByVal value As Decimal)
            mdecMonthly_annual_earnings = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basemonthly_annual_earnings
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basemonthly_Annual_Earnings() As Decimal
        Get
            Return mdecBasemonthly_annual_earnings
        End Get
        Set(ByVal value As Decimal)
            mdecBasemonthly_annual_earnings = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set business_contact_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Business_Contact_No() As String
        Get
            Return mstrBusiness_Contact_No
        End Get
        Set(ByVal value As String)
            mstrBusiness_Contact_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shareholders_names
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Shareholders_Names() As String
        Get
            Return mstrShareholders_Names
        End Get
        Set(ByVal value As String)
            mstrShareholders_Names = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public WriteOnly Property _Loginemployeeunkid() As Integer
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _IsFromWeb() As Boolean
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property


    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTable = New DataTable("BusinessDealings")

        Try
            mdtTable.Columns.Add("assetbusinessdealt2tranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("assetdeclarationt2unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("assetsectorunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("company_org_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("company_tin_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("registration_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("address_location", System.Type.GetType("System.String"))
            mdtTable.Columns.Add("business_type", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("position_held", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("is_supplier_client", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("basecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("baseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("exchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("monthly_annual_earnings", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("basemonthly_annual_earnings", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("business_contact_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("shareholders_names", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""

            mdtTable.Columns.Add("audittype", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("audituserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("auditdatetime", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("loginemployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("IP", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("host", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("form_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isweb", System.Type.GetType("System.Boolean")).DefaultValue = False

            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <purpose> Assign all Property variable </purpose>   
    ''' Public Sub GetData()
    Public Sub GetData(ByVal xDatabase As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetbusinessdealt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", registration_date " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", is_supplier_client " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", monthly_annual_earnings " & _
              ", basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(monthly_annual_earnings, 0) AS monthly_annual_earnings " & _
              ", ISNULL(basemonthly_annual_earnings, 0) AS basemonthly_annual_earnings " & _
              ", '' AS AUD " & _
             "FROM " & mstrDatabaseName & "..hrasset_businessdealT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetbusinessdealt2tranunkid") = CInt(dtRow.Item("assetbusinessdealt2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("assetsectorunkid") = dtRow.Item("assetsectorunkid").ToString
                drRow.Item("company_org_name") = dtRow.Item("company_org_name").ToString
                drRow.Item("company_tin_no") = dtRow.Item("company_tin_no")
                drRow.Item("registration_date") = dtRow.Item("registration_date")
                drRow.Item("address_location") = dtRow.Item("address_location")
                drRow.Item("business_type") = dtRow.Item("business_type")
                drRow.Item("position_held") = dtRow.Item("position_held")
                drRow.Item("is_supplier_client") = dtRow.Item("is_supplier_client")
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = dtRow.Item("exchangerate")
                drRow.Item("monthly_annual_earnings") = dtRow.Item("monthly_annual_earnings")
                drRow.Item("basemonthly_annual_earnings") = dtRow.Item("basemonthly_annual_earnings")
                drRow.Item("business_contact_no") = dtRow.Item("business_contact_no")
                drRow.Item("shareholders_names") = dtRow.Item("shareholders_names")
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("monthly_annual_earnings") = CDec(dtRow.Item("monthly_annual_earnings"))
                drRow.Item("basemonthly_annual_earnings") = CDec(dtRow.Item("basemonthly_annual_earnings"))
                drRow.Item("AUD") = dtRow.Item("AUD").ToString

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Hemant (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    Public Sub GetData(ByVal xDatabase As String, ByVal intAssetdeclarationt2Unkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow


        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        mintAssetdeclarationt2unkid = intAssetdeclarationt2Unkid

        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetbusinessdealt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", registration_date " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", is_supplier_client " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", monthly_annual_earnings " & _
              ", basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(monthly_annual_earnings, 0) AS monthly_annual_earnings " & _
              ", ISNULL(basemonthly_annual_earnings, 0) AS basemonthly_annual_earnings " & _
              ", '' AS AUD " & _
             "FROM " & mstrDatabaseName & "..hrasset_businessdealT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetbusinessdealt2tranunkid") = CInt(dtRow.Item("assetbusinessdealt2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("assetsectorunkid") = dtRow.Item("assetsectorunkid").ToString
                drRow.Item("company_org_name") = dtRow.Item("company_org_name").ToString
                drRow.Item("company_tin_no") = dtRow.Item("company_tin_no")
                drRow.Item("registration_date") = dtRow.Item("registration_date")
                drRow.Item("address_location") = dtRow.Item("address_location")
                drRow.Item("business_type") = dtRow.Item("business_type")
                drRow.Item("position_held") = dtRow.Item("position_held")
                drRow.Item("is_supplier_client") = dtRow.Item("is_supplier_client")
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = dtRow.Item("exchangerate")
                drRow.Item("monthly_annual_earnings") = dtRow.Item("monthly_annual_earnings")
                drRow.Item("basemonthly_annual_earnings") = dtRow.Item("basemonthly_annual_earnings")
                drRow.Item("business_contact_no") = dtRow.Item("business_contact_no")
                drRow.Item("shareholders_names") = dtRow.Item("shareholders_names")
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("monthly_annual_earnings") = CDec(dtRow.Item("monthly_annual_earnings"))
                drRow.Item("basemonthly_annual_earnings") = CDec(dtRow.Item("basemonthly_annual_earnings"))
                drRow.Item("AUD") = dtRow.Item("AUD").ToString

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Hemant (14 Nov 2018) -- End

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetBusinessDealT2tranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetbusinessdealt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", registration_date " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", is_supplier_client " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", monthly_annual_earnings " & _
              ", basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(monthly_annual_earnings, 0) AS monthly_annual_earnings " & _
              ", ISNULL(basemonthly_annual_earnings, 0) AS basemonthly_annual_earnings " & _
             "FROM " & mstrDatabaseName & "..hrasset_businessdealT2_tran " & _
             "WHERE assetbusinessdealt2tranunkid = @assetbusinessdealt2tranunkid "

            objDataOperation.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetBusinessDealT2tranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetbusinessdealt2tranunkid = CInt(dtRow.Item("assetbusinessdealt2tranunkid"))
                mintAssetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mintAssetsectorunkid = dtRow.Item("assetsectorunkid").ToString
                mstrCompany_org_name = dtRow.Item("company_org_name").ToString
                mstrCompany_tin_no = dtRow.Item("company_tin_no")
                mdtRegistration_date = dtRow.Item("registration_date")
                mstrAddress_location = dtRow.Item("address_location")
                mstrBusiness_type = dtRow.Item("business_type")
                mstrPosition_held = dtRow.Item("position_held")
                mintIs_supplier_client = dtRow.Item("is_supplier_client")
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mintBasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExchangerate = dtRow.Item("exchangerate")
                mdecMonthly_annual_earnings = dtRow.Item("monthly_annual_earnings")
                mdecBasemonthly_annual_earnings = dtRow.Item("basemonthly_annual_earnings")
                mstrBusiness_contact_no = dtRow.Item("business_contact_no")
                mstrShareholders_names = dtRow.Item("shareholders_names")
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdtTransactiondate = Nothing
                Else
                    mdtTransactiondate = CDate(dtRow.Item("transactiondate"))
                End If
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdecMonthly_annual_earnings = CDec(dtRow.Item("monthly_annual_earnings"))
                mdecBasemonthly_annual_earnings = CDec(dtRow.Item("basemonthly_annual_earnings"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByRef decBankTotal As Decimal, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End

            For Each dtRow As DataRow In mdtTable.Rows

                mintAssetbusinessdealt2tranunkid = CInt(dtRow.Item("assetbusinessdealt2tranunkid"))
                mintAssetsectorunkid = Convert.ToInt16(dtRow.Item("assetsectorunkid").ToString)
                mstrCompany_org_name = dtRow.Item("company_org_name").ToString
                mstrCompany_tin_no = dtRow.Item("company_tin_no")
                
                mdtRegistration_date = Convert.ToDateTime(dtRow.Item("registration_date"))
                mstrAddress_location = dtRow.Item("address_location").ToString
                
                mstrBusiness_type = dtRow.Item("business_type").ToString
                mstrPosition_held = dtRow.Item("position_held").ToString
                mintIs_supplier_client = CInt(dtRow.Item("Is_supplier_client"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mintBasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                mdecMonthly_annual_earnings = CDec(dtRow.Item("monthly_annual_earnings"))
                mdecBasemonthly_annual_earnings = CDec(dtRow.Item("basemonthly_annual_earnings"))
                mstrBusiness_contact_no = dtRow.Item("business_contact_no").ToString
                mstrShareholders_names = dtRow.Item("shareholders_names").ToString
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = Nothing
                mstrVoidreason = dtRow.Item("voidreason").ToString


                If mintAssetbusinessdealt2tranunkid <= 0 Then

                    blnChildTableChanged = True

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If Insert() = False Then
                    If Insert(objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintAssetbusinessdealt2tranunkid.ToString
                    Else
                        strUnkIDs &= "," & mintAssetbusinessdealt2tranunkid.ToString
                    End If
                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged) = False Then
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                End If

            Next

            decBankTotal = decTotal

            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetbusinessdealt2tranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each dtRow In dRow

                    mintAssetbusinessdealt2tranunkid = CInt(dtRow.Item("assetbusinessdealt2tranunkid"))
                    mintAssetsectorunkid = Convert.ToInt16(dtRow.Item("assetsectorunkid").ToString)
                    mstrCompany_org_name = dtRow.Item("company_org_name").ToString
                    mstrCompany_tin_no = dtRow.Item("company_tin_no")

                    mdtRegistration_date = Convert.ToDateTime(dtRow.Item("registration_date"))
                    mstrAddress_location = dtRow.Item("address_location").ToString

                    mstrBusiness_type = dtRow.Item("business_type").ToString
                    mstrPosition_held = dtRow.Item("position_held").ToString
                    mintIs_supplier_client = CInt(dtRow.Item("Is_supplier_client"))
                    mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                    mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                    mintBasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                    mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                    mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                    mdecMonthly_annual_earnings = CDec(dtRow.Item("monthly_annual_earnings"))
                    mdecBasemonthly_annual_earnings = CDec(dtRow.Item("basemonthly_annual_earnings"))
                    mstrBusiness_contact_no = dtRow.Item("business_contact_no").ToString
                    mstrShareholders_names = dtRow.Item("shareholders_names").ToString
                    mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                    mdtTransactiondate = dtRow.Item("transactiondate")
                    mblnIsvoid = CBool(dtRow.Item("isvoid"))
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = dtRow.Item("voidreason").ToString

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If Void(CInt(dtRow.Item("assetbusinessdealt2tranunkid")), mintUserunkid, xCurrentDatetTime, "") = False Then
                    If Void(CInt(dtRow.Item("assetbusinessdealt2tranunkid")), mintUserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Hemant (14 Nov 2018) -- Start
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationt2unkID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim objMaster As New clsMasterData
            Dim dsSuppBorr As DataSet = objMaster.getComboListAssetsupplierborrowerList("List")
            Dim dicSuppBorr As Dictionary(Of Integer, String) = (From p In dsSuppBorr.Tables(0) Select New With {.Id = CInt(p.Item("Id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (07 Dec 2018) -- End

            strQ = "SELECT " & _
              "  hrasset_businessdealT2_tran.assetbusinessdealt2tranunkid " & _
              ", hrasset_businessdealT2_tran.assetdeclarationt2unkid " & _
              ", ISNULL(hrasset_businessdealT2_tran.assetsectorunkid, 0) AS assetsectorunkid " & _
              ", ISNULL(sector.name, '') AS assetsectorname " & _
              ", hrasset_businessdealT2_tran.company_org_name " & _
              ", hrasset_businessdealT2_tran.company_tin_no " & _
              ", hrasset_businessdealT2_tran.registration_date " & _
              ", hrasset_businessdealT2_tran.address_location " & _
              ", hrasset_businessdealT2_tran.business_type " & _
              ", hrasset_businessdealT2_tran.position_held " & _
              ", ISNULL(hrasset_businessdealT2_tran.is_supplier_client, 0) AS is_supplier_client " & _
              ", ISNULL(hrasset_businessdealT2_tran.countryunkid, 0) AS countryunkid " & _
              ", ISNULL(hrasset_businessdealT2_tran.currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(hrasset_businessdealT2_tran.basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(hrasset_businessdealT2_tran.baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(hrasset_businessdealT2_tran.exchangerate, 0) AS exchangerate " & _
              ", ISNULL(hrasset_businessdealT2_tran.monthly_annual_earnings, 0) AS monthly_annual_earnings " & _
              ", ISNULL(hrasset_businessdealT2_tran.basemonthly_annual_earnings, 0) AS basemonthly_annual_earnings " & _
              ", hrasset_businessdealT2_tran.business_contact_no " & _
              ", hrasset_businessdealT2_tran.shareholders_names " & _
              ", hrasset_businessdealT2_tran.isfinalsaved " & _
              ", hrasset_businessdealT2_tran.transactiondate " & _
              ", hrasset_businessdealT2_tran.userunkid " & _
              ", hrasset_businessdealT2_tran.isvoid " & _
              ", hrasset_businessdealT2_tran.voiduserunkid " & _
              ", hrasset_businessdealT2_tran.voiddatetime " & _
              ", hrasset_businessdealT2_tran.voidreason "

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            strQ &= ", CASE hrasset_businessdealT2_tran.is_supplier_client "
            For Each pair In dicSuppBorr
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS is_supplier_clientname "

            strQ &= ", CASE hrasset_businessdealT2_tran.countryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS currency_sign "
            'Sohail (07 Dec 2018) -- End

            strQ &= "FROM hrasset_businessdealT2_tran " & _
             "LEFT JOIN cfcommon_master AS sector ON sector.masterunkid = hrasset_businessdealT2_tran.assetsectorunkid AND sector.mastertype = " & CInt(clsCommon_Master.enCommonMaster.ASSET_SECTOR) & " " & _
             "WHERE ISNULL(hrasset_businessdealT2_tran.isvoid, 0 ) = 0 "
            'Sohail (07 Dec 2018) - [LEFT JOIN cfcommon_master AS sector, assetsectorname]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If intAssetDeclarationt2unkID > 0 Then
            '    strQ &= " AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            '    objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2unkID)
            'End If
            If intAssetDeclarationt2unkID > 0 Then
                strQ &= " AND hrasset_businessdealT2_tran.assetdeclarationt2unkid = @assetdeclarationt2unkid "
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2unkID)
            Else
                strQ &= " AND 1 = 2 "
            End If
            'Sohail (07 Dec 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrasset_businessdealT2_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End            
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@assetsectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetsectorunkid.ToString)
            objDataOperation.AddParameter("@company_org_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_org_name.ToString)
            objDataOperation.AddParameter("@company_tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_tin_no.ToString)
            If mdtRegistration_date = Nothing Then
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRegistration_date.ToString)
            End If
            objDataOperation.AddParameter("@address_location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress_location.ToString)
            objDataOperation.AddParameter("@business_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBusiness_type.ToString)
            objDataOperation.AddParameter("@position_held", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPosition_held.ToString)
            objDataOperation.AddParameter("@is_supplier_client", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIs_supplier_client.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDataOperation.AddParameter("@monthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMonthly_annual_earnings.ToString)
            objDataOperation.AddParameter("@basemonthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasemonthly_annual_earnings.ToString)
            objDataOperation.AddParameter("@business_contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBusiness_contact_no.ToString)
            objDataOperation.AddParameter("@shareholders_names", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShareholders_names.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            
            strQ = "INSERT INTO hrasset_businessdealT2_tran ( " & _
              "  assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", registration_date " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", is_supplier_client " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", monthly_annual_earnings " & _
              ", basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @assetdeclarationt2unkid " & _
              ", @assetsectorunkid " & _
              ", @company_org_name " & _
              ", @company_tin_no " & _
              ", @registration_date " & _
              ", @address_location " & _
              ", @business_type " & _
              ", @position_held " & _
              ", @is_supplier_client " & _
              ", @countryunkid " & _
              ", @currencyunkid " & _
              ", @basecurrencyunkid " & _
              ", @baseexchangerate " & _
              ", @exchangerate " & _
              ", @monthly_annual_earnings " & _
              ", @basemonthly_annual_earnings" & _
              ", @business_contact_no " & _
              ", @shareholders_names " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetbusinessdealt2tranunkid = dsList.Tables(0).Rows(0).Item(0)

           
            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_businessdealT2_tran", "assetbusinessdealt2tranunkid", mintAssetbusinessdealt2tranunkid, 1, 1, , mintUserunkid) = False Then
            '    Return False
            'End If

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function


    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrasset_businessdealT2_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End           
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End
            objDataOperation.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetbusinessdealt2tranunkid.ToString)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@assetsectorunkid", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, mintAssetsectorunkid.ToString)
            objDataOperation.AddParameter("@company_org_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_org_name.ToString)
            objDataOperation.AddParameter("@company_tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_tin_no.ToString)
            If mdtRegistration_date = Nothing Then
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRegistration_date.ToString)
            End If
            objDataOperation.AddParameter("@address_location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress_location.ToString)
            objDataOperation.AddParameter("@business_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBusiness_type.ToString)
            objDataOperation.AddParameter("@position_held", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPosition_held.ToString)
            objDataOperation.AddParameter("@is_supplier_client", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIs_supplier_client.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDataOperation.AddParameter("@monthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMonthly_annual_earnings.ToString)
            objDataOperation.AddParameter("@basemonthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasemonthly_annual_earnings.ToString)
            objDataOperation.AddParameter("@business_contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBusiness_contact_no.ToString)
            objDataOperation.AddParameter("@shareholders_names", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShareholders_names.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            

            strQ = "UPDATE hrasset_businessdealT2_tran SET " & _
              "  assetdeclarationt2unkid = @assetdeclarationt2unkid" & _
              ", assetsectorunkid = @assetsectorunkid" & _
              ", company_org_name = @company_org_name" & _
              ", company_tin_no = @company_tin_no" & _
              ", registration_date = @registration_date" & _
              ", address_location = @address_location" & _
              ", business_type = @business_type" & _
              ", position_held = @position_held" & _
              ", is_supplier_client = @is_supplier_client " & _
              ", countryunkid = @countryunkid" & _
              ", currencyunkid = @currencyunkid " & _
              ", basecurrencyunkid = @basecurrencyunkid" & _
              ", baseexchangerate = @baseexchangerate" & _
              ", exchangerate = @exchangerate" & _
              ", monthly_annual_earnings = @monthly_annual_earnings" & _
              ", basemonthly_annual_earnings = @basemonthly_annual_earnings" & _
              ", business_contact_no = @business_contact_no " & _
              ", shareholders_names = @shareholders_names " & _
              ", isfinalsaved = @isfinalsaved " & _
              ", transactiondate = @transactiondate " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetbusinessdealt2tranunkid = @assetbusinessdealt2tranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_businessdealT2_tran", "assetbusinessdealt2tranunkid", mintAssetbusinessdealt2tranunkid, 2, 2, , mintUserunkid) = False Then
            '    Return False
            'End If

            If IsTableDataUpdate(mintAssetbusinessdealt2tranunkid, objDataOperation) = False Then

            If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                Return False
            End If
            End If

            blnChildTableChanged = True
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrasset_businessdealT2_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            objDataOperation.BindTransaction()
            'Sohail (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            strQ = "UPDATE hrasset_businessdealT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetbusinessdealt2tranunkid = @assetbusinessdealt2tranunkid "

            objDataOperation.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (07 Dec 2018) -- End

            Return True
        Catch ex As Exception
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Sohail (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    Public Function VoidByAssetDeclarationt2unkID(ByVal intAssetDeclarationt2UnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclarationt2UnkID, "hrasset_businessdealT2_tran", "assetbusinessdealt2tranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then

            '    Return False
            'End If            

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            strQ = "INSERT INTO athrasset_businessdealT2_tran ( " & _
                     "  assetbusinessdealt2tranunkid " & _
                     ", assetdeclarationt2unkid " & _
                     ", assetsectorunkid " & _
                     ", company_org_name " & _
                     ", company_tin_no " & _
                     ", registration_date " & _
                     ", address_location " & _
                     ", business_type " & _
                     ", position_held " & _
                     ", is_supplier_client " & _
                     ", countryunkid " & _
                     ", currencyunkid " & _
                     ", basecurrencyunkid " & _
                     ", baseexchangerate " & _
                     ", exchangerate " & _
                     ", monthly_annual_earnings " & _
                     ", basemonthly_annual_earnings " & _
                     ", business_contact_no " & _
                     ", shareholders_names " & _
                     ", isfinalsaved " & _
                     ", transactiondate " & _
                     ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb )" & _
                 "SELECT " & _
                     "  assetbusinessdealt2tranunkid " & _
                     ", assetdeclarationt2unkid " & _
                     ", assetsectorunkid " & _
                     ", company_org_name " & _
                     ", company_tin_no " & _
                     ", registration_date " & _
                     ", address_location " & _
                     ", business_type " & _
                     ", position_held " & _
                     ", is_supplier_client " & _
                     ", countryunkid " & _
                     ", currencyunkid " & _
                     ", basecurrencyunkid " & _
                     ", baseexchangerate " & _
                     ", exchangerate " & _
                     ", monthly_annual_earnings " & _
                     ", basemonthly_annual_earnings " & _
                     ", business_contact_no " & _
                     ", shareholders_names " & _
                     ", isfinalsaved " & _
                     ", transactiondate " & _
                     ", GETDATE() " & _
                     ", 3 " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
                     ", @isweb " & _
             "FROM hrasset_businessdealT2_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End

            strQ = "UPDATE hrasset_businessdealT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'If Insert_AtTranLog(objDataOperation, 3) = False Then
            '    Return False
            'End If
            'Hemant (14 Nov 2018) -- End
            

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationt2UnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function
    
    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' </summary>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assetbusinessdealt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", registration_date " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", ISNULL(is_supplier_client, 0) AS is_supplier_client " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", ISNULL(monthly_annual_earnings, 0) AS monthly_annual_earnings " & _
              ", ISNULL(basemonthly_annual_earnings, 0) AS basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrasset_businessdealT2_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND assetbusinessdealt2tranunkid <> @assetbusinessdealt2tranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    'Public Function getClientsList(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
    '    Dim strQ As String = ""
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        If blnFlag Then
    '            strQ = "SELECT @Select As Name, 0 As id UNION "
    '        End If

    '        strQ &= "SELECT @Supplier As Name, 1 As id " & _
    '                "UNION SELECT @Borrower As Name, 2 As id " & _
    '                "UNION SELECT @Other As Name, 3 As id " & _
    '                "Order By id "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Select"))
    '        objDataOperation.AddParameter("@Supplier", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Supplier"))
    '        objDataOperation.AddParameter("@Borrower", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Borrower"))
    '        objDataOperation.AddParameter("@Other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Other"))

    '        Return objDataOperation.ExecQuery(strQ, strListName)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "[getGenderList]")
    '    End Try
    'End Function
    'Sohail (22 Nov 2018) -- End

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO athrasset_businessdealT2_tran ( " & _
                     "  assetbusinessdealt2tranunkid " & _
                     ", assetdeclarationt2unkid " & _
                     ", assetsectorunkid " & _
                     ", company_org_name " & _
                     ", company_tin_no " & _
                     ", registration_date " & _
                     ", address_location " & _
                     ", business_type " & _
                     ", position_held " & _
                     ", is_supplier_client " & _
                     ", countryunkid " & _
                     ", currencyunkid " & _
                     ", basecurrencyunkid " & _
                     ", baseexchangerate " & _
                     ", exchangerate " & _
                     ", monthly_annual_earnings " & _
                     ", basemonthly_annual_earnings " & _
                     ", business_contact_no " & _
                     ", shareholders_names " & _
                     ", isfinalsaved " & _
                     ", transactiondate " & _
                     ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
                  ") VALUES (" & _
                     "  @assetbusinessdealt2tranunkid " & _
                     ", @assetdeclarationt2unkid " & _
                     ", @assetsectorunkid " & _
                     ", @company_org_name " & _
                     ", @company_tin_no " & _
                     ", @registration_date " & _
                     ", @address_location " & _
                     ", @business_type " & _
                     ", @position_held " & _
                     ", @is_supplier_client " & _
                     ", @countryunkid " & _
                     ", @currencyunkid " & _
                     ", @basecurrencyunkid " & _
                     ", @baseexchangerate " & _
                     ", @exchangerate " & _
                     ", @monthly_annual_earnings " & _
                     ", @basemonthly_annual_earnings " & _
                     ", @business_contact_no " & _
                     ", @shareholders_names " & _
                     ", @isfinalsaved " & _
                     ", @transactiondate " & _
                     ", GETDATE() " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
                     ", @isweb )"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetbusinessdealt2tranunkid)
            objDoOps.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)
            objDoOps.AddParameter("@assetsectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetsectorunkid.ToString)
            objDoOps.AddParameter("@company_org_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_org_name.ToString)
            objDoOps.AddParameter("@company_tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany_tin_no.ToString)
            If mdtregistration_date = Nothing Then
                objDoOps.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRegistration_date.ToString)
            End If
            objDoOps.AddParameter("@address_location", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrAddress_location.ToString)
            objDoOps.AddParameter("@business_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBusiness_type.ToString)
            objDoOps.AddParameter("@position_held", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPosition_held.ToString)
            objDoOps.AddParameter("@is_supplier_client", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIs_supplier_client.ToString)
            objDoOps.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDoOps.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDoOps.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyunkid.ToString)
            objDoOps.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDoOps.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDoOps.AddParameter("@monthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMonthly_annual_earnings.ToString)
            objDoOps.AddParameter("@basemonthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasemonthly_annual_earnings.ToString)
            objDoOps.AddParameter("@business_contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBusiness_contact_no.ToString)
            objDoOps.AddParameter("@shareholders_names", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShareholders_names.ToString)
            objDoOps.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'strQ = "select TOP 1 * from athrasset_businessdealT2_tran where assetbusinessdealt2tranunkid = @assetbusinessdealt2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype = 2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrasset_businessdealT2_tran where assetbusinessdealt2tranunkid = @assetbusinessdealt2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype <> 3 ORDER BY auditdatetime DESC"
            'Gajanan (07 Dec 2018) -- End
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetbusinessdealt2tranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("assetsectorunkid").ToString() = mintAssetsectorunkid AndAlso dr("company_org_name").ToString() = mstrCompany_org_name AndAlso _
                   dr("company_tin_no").ToString() = mstrCompany_tin_no AndAlso CDate(dr("registration_date")) = mdtRegistration_date _
                   AndAlso dr("address_location").ToString() = mstrAddress_location AndAlso dr("business_type").ToString() = mstrBusiness_type _
                   AndAlso dr("position_held").ToString() = mstrPosition_held AndAlso dr("is_supplier_client").ToString() = mintIs_supplier_client _
                   AndAlso dr("monthly_annual_earnings").ToString() = mdecMonthly_annual_earnings AndAlso dr("countryunkid").ToString() = mintCountryunkid _
                   AndAlso dr("business_contact_no").ToString() = mstrBusiness_contact_no AndAlso dr("shareholders_names").ToString() = mstrShareholders_names Then

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class