﻿'************************************************************************************************************************************
'Class Name : clsassetdecemp_lockunlock.vb
'Purpose    :
'Date       :06-Dec-2018
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsassetdecemp_lockunlock
    Private Const mstrModuleName = "clsassetdecemp_lockunlock"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAdlockunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtLockunlockdatetime As Date
    Private mblnIslock As Boolean
    Private mintLockuserunkid As Integer
    Private mintUnlockuserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private minAuditUserunkid As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False
    Private mblnIsunlocktenure As Boolean
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adlockunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Adlockunkid() As Integer
        Get
            Return mintAdlockunkid
        End Get
        Set(ByVal value As Integer)
            mintAdlockunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockunlockdatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Lockunlockdatetime() As Date
        Get
            Return mdtLockunlockdatetime
        End Get
        Set(ByVal value As Date)
            mdtLockunlockdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lockuserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Lockuserunkid() As Integer
        Get
            Return mintLockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintLockuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockuserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Unlockuserunkid() As Integer
        Get
            Return mintUnlockuserunkid
        End Get
        Set(ByVal value As Integer)
            mintUnlockuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set islock
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Islock() As Boolean
        Get
            Return mblnIslock
        End Get
        Set(ByVal value As Boolean)
            mblnIslock = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isunlocktenure
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isunlocktenure() As Boolean
        Get
            Return mblnIsunlocktenure
        End Get
        Set(ByVal value As Boolean)
            mblnIsunlocktenure = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _AuditUserunkid() As Integer
        Get
            Return minAuditUserunkid
        End Get
        Set(ByVal value As Integer)
            minAuditUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDateTime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _AuditDateTime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIp
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  adlockunkid " & _
              ", employeeunkid " & _
              ", lockunlockdatetime " & _
              ", lockuserunkid " & _
              ", unlockuserunkid " & _
              ", islock " & _
              ", isunlocktenure " & _
              ", loginemployeeunkid " & _
             "FROM hrassetdecemp_lockunlock " & _
             "WHERE adlockunkid = @adlockunkid "

            objDataOperation.AddParameter("@adlockunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAdlockUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintadlockunkid = CInt(dtRow.Item("adlockunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtlockunlockdatetime = dtRow.Item("lockunlockdatetime")
                mintLockuserunkid = CInt(dtRow.Item("lockuserunkid"))
                mintUnlockuserunkid = CInt(dtRow.Item("unlockuserunkid"))
                mblnIslock = CBool(dtRow.Item("islock"))
                mblnIsunlocktenure = CBool(dtRow.Item("isunlocktenure"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                          "  adlockunkid " & _
                          ", hremployee_master.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                          ", lockunlockdatetime " & _
                          ", lockuserunkid " & _
                          ", unlockuserunkid " & _
                          ", islock " & _
                          ", isunlocktenure " & _
                          ", loginemployeeunkid " & _
                         " FROM hrassetdecemp_lockunlock " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = hrassetdecemp_lockunlock.employeeunkid " & _
                         " WHERE isunlocktenure = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassetdecemp_lockunlock) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            strQ = "INSERT INTO hrassetdecemp_lockunlock ( " & _
                          "  employeeunkid " & _
                          ", lockunlockdatetime " & _
                          ", lockuserunkid " & _
                          ", unlockuserunkid " & _
                          ", islock " & _
                          ", isunlocktenure " & _
                          ", loginemployeeunkid" & _
                        ") VALUES (" & _
                          "  @employeeunkid " & _
                          ", @lockunlockdatetime " & _
                          ", @lockuserunkid " & _
                          ", @unlockuserunkid " & _
                          ", @islock " & _
                          ", @isunlocktenure " & _
                          ", @loginemployeeunkid" & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAdlockunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATEmployee_LockUnlock(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassetdecemp_lockunlock) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@adlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdlockunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
            objDataOperation.AddParameter("@lockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockuserunkid.ToString)
            objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            strQ = " UPDATE hrassetdecemp_lockunlock SET " & _
                         "  employeeunkid = @employeeunkid" & _
                         ", lockunlockdatetime = @lockunlockdatetime" & _
                         ", lockuserunkid = @lockuserunkid" & _
                         ", unlockuserunkid = @unlockuserunkid" & _
                         ", islock = @islock" & _
                         ", isunlocktenure = @isunlocktenure" & _
                         ", loginemployeeunkid = @loginemployeeunkid " & _
                         " WHERE adlockunkid = @adlockunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATEmployee_LockUnlock(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassetdecemp_lockunlock) </purpose>
    Public Function GlobalUnlockEmployee(ByVal dtUnlockEmployee As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If dtUnlockEmployee Is Nothing Then Return True

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()


            strQ = " UPDATE hrassetdecemp_lockunlock SET " & _
                         "  lockunlockdatetime = @lockunlockdatetime" & _
                         ", unlockuserunkid = @unlockuserunkid" & _
                         ", islock = @islock" & _
                         ", isunlocktenure = @isunlocktenure" & _
                         " WHERE adlockunkid = @adlockunkid "

         
            For Each dr As DataRow In dtUnlockEmployee.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@adlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("adlockunkid").ToString)
                objDataOperation.AddParameter("@lockunlockdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLockunlockdatetime.ToString)
                objDataOperation.AddParameter("@unlockuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnlockuserunkid.ToString)
                objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)
                objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintAdlockunkid = CInt(dr("adlockunkid"))
                mintEmployeeunkid = CInt(dr("employeeunkid"))

                If InsertATEmployee_LockUnlock(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GlobalUnlockEmployee; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@adlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEmployeeId As Integer, ByRef blnisLock As Boolean, ByRef xAdLockId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                          "  adlockunkid " & _
                          ", employeeunkid " & _
                          ", lockunlockdatetime " & _
                          ", lockuserunkid " & _
                          ", unlockuserunkid " & _
                          ", islock " & _
                          ", isunlocktenure " & _
                          ", loginemployeeunkid " & _
                          "FROM hrassetdecemp_lockunlock " & _
                          "WHERE employeeunkid = @employeeunkid AND isunlocktenure = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnisLock)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                xAdLockId = CInt(dsList.Tables(0).Rows(0)("adlockunkid"))
                blnisLock = CBool(dsList.Tables(0).Rows(0)("islock"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATEmployee_LockUnlock(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@adlockunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdlockunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@islock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIslock.ToString)
            objDataOperation.AddParameter("@isunlocktenure", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlocktenure.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            strQ = "INSERT INTO athrassetdecemp_lockunlock ( " & _
                          "  adlockunkid " & _
                          ", employeeunkid " & _
                          ", islock " & _
                          ", isunlocktenure " & _
                          ", auditdatetime " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", loginemployeeunkid " & _
                          ", ip " & _
                          ", hostname " & _
                          ", form_name " & _
                          ", isweb " & _
                        ") VALUES (" & _
                          "  @adlockunkid " & _
                          ", @employeeunkid " & _
                          ", @islock " & _
                           ",@isunlocktenure " & _
                          ", @auditdatetime " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @loginemployeeunkid " & _
                          ", @ip " & _
                          ", @hostname " & _
                          ", @form_name " & _
                          ", @isweb " & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATEmployee_LockUnlock; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class
