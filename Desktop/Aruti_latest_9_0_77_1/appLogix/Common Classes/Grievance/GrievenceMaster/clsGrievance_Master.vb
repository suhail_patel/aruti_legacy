﻿'************************************************************************************************************************************
'Class Name : clsGrievance_Master.vb
'Purpose    :
'Date       :27-Aug-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsGrievance_Master
    Private Shared ReadOnly mstrModuleName As String = "clsGrievance_Master"
    Dim mstrMessage As String = ""
    Private objDocument As New clsScan_Attach_Documents


#Region " Private variables "
    Private mintGrievancemasterunkid As Integer = 0
    Private mintGrievancetypeid As Integer = 0
    Private mdtGrievancedate As Date = Nothing
    Private mstrGrievancerefno As String = String.Empty
    Private mstrGrievance_Description As String = String.Empty
    Private mstrGrievance_Reliefwanted As String = String.Empty
    Private mstrAppealremark As String = String.Empty
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintFromemployeeunkid As Integer = 0
    Private mintAgainstemployeeunkid As Integer = 0
    Private mintApprovalSettingid As Integer = 0
    Private mblnIssubmitted As Boolean = False
    Private mblnIsprocessed As Boolean = False

    Private xObjDataOpr As clsDataOperation = Nothing
    Private mintLoginEmployeeUnkid As Integer = 0
    Private mstrFormName As String = String.Empty
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mintCompanyUnkid As Integer = 0
    Private mdtAuditDate As DateTime = Now


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grievancemasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Grievancemasterunkid() As Integer
        Get
            Return mintGrievancemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintGrievancemasterunkid = value

            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grievancetypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Grievancetypeid() As Integer
        Get
            Return mintGrievancetypeid
        End Get
        Set(ByVal value As Integer)
            mintGrievancetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grievancedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Grievancedate() As Date
        Get
            Return mdtGrievancedate
        End Get
        Set(ByVal value As Date)
            mdtGrievancedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grievancerefno
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Grievancerefno() As String
        Get
            Return mstrGrievancerefno
        End Get
        Set(ByVal value As String)
            mstrGrievancerefno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grievance_description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Grievance_Description() As String
        Get
            Return mstrGrievance_Description
        End Get
        Set(ByVal value As String)
            mstrGrievance_Description = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grievance_reliefwanted
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Grievance_Reliefwanted() As String
        Get
            Return mstrGrievance_Reliefwanted
        End Get
        Set(ByVal value As String)
            mstrGrievance_Reliefwanted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appealremark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Appealremark() As String
        Get
            Return mstrAppealremark
        End Get
        Set(ByVal value As String)
            mstrAppealremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Issubmitted
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issubmitted() As Boolean
        Get
            Return mblnIssubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmitted = value
        End Set
    End Property

    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fromemployeeunkid() As Integer
        Get
            Return mintFromemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintFromemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set againstemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Againstemployeeunkid() As Integer
        Get
            Return mintAgainstemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAgainstemployeeunkid = value
        End Set
    End Property

    Public Property _ApprovalSettingid() As Integer
        Get
            Return mintApprovalSettingid
        End Get
        Set(ByVal value As Integer)
            mintApprovalSettingid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set clsDataOperation
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _ObjDataOpr() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xObjDataOpr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set LoginEmployeeUnkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLoginEmployeeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set FormName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Client IP
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Host Name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set From Web
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Audit User
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set CompanyId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Audit Date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

#End Region
            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        


#Region "Enum"

    Enum EmployeeType
        AgainstEmployee = 1
        RaisedEmployee = 2
    End Enum
#End Region
            'Gajanan [10-June-2019] -- End


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  grievancemasterunkid " & _
              ", grievancetypeid " & _
              ", grievancedate " & _
              ", grievancerefno " & _
              ", grievance_description " & _
              ", grievance_reliefwanted " & _
              ", appealremark " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", fromemployeeunkid " & _
              ", againstemployeeunkid " & _
              ", approvalSettingid " & _
              ", issubmitted " & _
              ", isprocessed " & _
             "FROM gregrievance_master " & _
             "WHERE grievancemasterunkid = @grievancemasterunkid "

            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancemasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGrievancemasterunkid = CInt(dtRow.Item("grievancemasterunkid"))
                mintGrievancetypeid = CInt(dtRow.Item("grievancetypeid"))
                mdtGrievancedate = dtRow.Item("grievancedate")
                mstrGrievancerefno = dtRow.Item("grievancerefno").ToString
                mstrGrievance_Description = dtRow.Item("grievance_description").ToString
                mstrGrievance_Reliefwanted = dtRow.Item("grievance_reliefwanted").ToString
                mstrAppealremark = dtRow.Item("appealremark").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintFromemployeeunkid = CInt(dtRow.Item("fromemployeeunkid"))
                mintAgainstemployeeunkid = CInt(dtRow.Item("againstemployeeunkid"))
                mintApprovalSettingid = CInt(dtRow.Item("approvalSettingid"))
                mblnIssubmitted = CBool(dtRow.Item("issubmitted"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal Type As EmployeeType, Optional ByVal xEmployeeUnkid As Integer = -1, Optional ByVal xRefNo As String = "", Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnAddSelect As Boolean = False, Optional ByVal ApprSetting As Integer = -1, Optional ByVal strfilter As String = "") As DataSet             'Gajanan [10-June-2019] -- Add Type    
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            If mblnAddSelect = True Then
                strQ = "SELECT " & _
                       "  0 AS grievancemasterunkid " & _
                       ", 0 AS grievancetypeid " & _
                       ", '' AS Grievancetype " & _
                       ", NULL AS grievancedate " & _
                       ", @Select AS grievancerefno " & _
                       ", '' AS grievance_description " & _
                       ", '' AS grievance_reliefwanted " & _
                       ", '' AS appealremark " & _
                       ", 0  AS isvoid " & _
                       ", 0  AS voiduserunkid " & _
                       ", '' AS voidreason " & _
                       ", NULL voiddatetime " & _
                       ", 0  AS fromemployeeunkid " & _
                       ", ''  AS Fromemployee " & _
                       ", 0  AS approvalSettingid " & _
                       ", 0  AS againstemployeeunkid " & _
                       ", ''  AS Againstemployee " & _
                       ", '' AS Status" & _
                       ", 0 AS issubmitted " & _
                       " UNION ALL "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If

            strQ &= "SELECT " & _
                    "gregrievance_master.grievancemasterunkid " & _
                    ",grievancetypeid " & _
                    ",cfcommon_master.name AS Grievancetype " & _
                    ",CONVERT(CHAR(8), grievancedate, 112) AS grievancedate " & _
                    ",grievancerefno " & _
                    ",grievance_description " & _
                    ",grievance_reliefwanted " & _
                    ",appealremark " & _
                    ",gregrievance_master.isvoid " & _
                    ",gregrievance_master.voiduserunkid " & _
                    ",gregrievance_master.voidreason " & _
                    ",gregrievance_master.voiddatetime " & _
                    ",fromemployeeunkid " & _
                    ",FromEmployee.employeecode + ' - ' + FromEmployee.surname + ' ' + FromEmployee.firstname AS Fromemployee " & _
                    ",gregrievance_master.approvalSettingid " & _
                    ",againstemployeeunkid " & _
                    ",AgainstEmployee.employeecode + ' - ' + AgainstEmployee.surname + ' ' + AgainstEmployee.firstname AS Againstemployee " & _
                    ",CASE ISNULL(A.statusunkid, 0) " & _
                    "WHEN '0' THEN @Open " & _
                    "WHEN '1' THEN @AgreedAndClose " & _
                    "WHEN '2' THEN @DisagreeAndEscalateToNextLevel " & _
                    "WHEN '3' THEN @DisagreeAndClose " & _
                    "WHEN '4' THEN @Appeal " & _
                    "END AS Status " & _
                    ",gregrievance_master.issubmitted " & _
                    "FROM gregrievance_master " & _
                    "LEFT JOIN cfcommon_master " & _
                    "ON gregrievance_master.grievancetypeid = cfcommon_master.masterunkid " & _
                    "LEFT JOIN hremployee_master AS FromEmployee " & _
                    "ON gregrievance_master.fromemployeeunkid = FromEmployee.employeeunkid " & _
                    "LEFT JOIN hremployee_master AS AgainstEmployee " & _
                    "ON gregrievance_master.againstemployeeunkid = AgainstEmployee.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "SELECT " & _
                    "grievancemasterunkid " & _
                    ",ROW_NUMBER()OVER(PARTITION BY grievancemasterunkid ORDER BY initiatortranunkid DESC) AS rno " & _
                    ",statusunkid " & _
                    "FROM greinitiater_response_tran " & _
                    ") AS A ON A.grievancemasterunkid =  gregrievance_master.grievancemasterunkid AND A.rno = 1 " & _
                    "WHERE gregrievance_master.isvoid = 0 and gregrievance_master.approvalSettingid = @approvalSettingid "

            If strfilter <> "" Then
                strQ += "And " + strfilter
            End If

            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Open"))
            objDataOperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 838, "Agreed And Close"))
            objDataOperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 839, "Appeal"))
            objDataOperation.AddParameter("@DisagreeAndClose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 840, "Disagree And Close"))
            objDataOperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 841, "Disagree And Escalate To Next Level"))
            objDataOperation.AddParameter("@approvalSettingid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ApprSetting)

            If xEmployeeUnkid > 0 Then

            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        


                Select Case Type
                    Case EmployeeType.AgainstEmployee
                        strQ &= " AND gregrievance_master.againstemployeeunkid = @employeeunkid"
                    Case EmployeeType.RaisedEmployee
                strQ &= " AND gregrievance_master.fromemployeeunkid = @employeeunkid"
                End Select
            'Gajanan [10-June-2019] -- End

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeUnkid.ToString)
            End If

            If xRefNo.Length > 0 Then
                strQ &= " AND gregrievance_master.grievancerefno = @grievancerefno"
                objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xRefNo.ToString)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (gregrievance_master) </purpose>
    Public Function Insert(ByVal intRefNoType As Integer, _
                           ByVal strRefNoPrefix As String, _
                           ByVal intCompanyId As Integer, _
                           ByVal dtScanAttachment As DataTable, _
                           ByVal strDocumentPath As String, _
                           ByVal strLstName As List(Of String), _
                           ByVal dtStartDate As Date, _
                           ByVal dtEndDate As Date, _
                           ByVal strDataBaseName As String, _
                           ByVal strScreenName As String, _
                           ByVal intApprovalSetting As Integer _
                           ) As Boolean


        'If IsProcess(mintFromemployeeunkid, mintAgainstemployeeunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this grievance is already in process for this employee. please complete this grievance process.")
        '    Return False
        'End If


        If intRefNoType = 0 Then
            If isRefnoExist(mstrGrievancerefno) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
                Return False
            End If
        End If

        Dim clsConfig As New clsConfigOptions
        clsConfig._Companyunkid = intCompanyId


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        

        'If IsProcess(mintFromemployeeunkid, mintAgainstemployeeunkid, mintGrievancetypeid, intApprovalSetting) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, this grievance is already in process for this employee. please complete this grievance process.")
        '    Return False
        'End If
            'Gajanan [10-June-2019] -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xObjDataOpr
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@grievancetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancetypeid.ToString)
            objDataOperation.AddParameter("@grievancedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtGrievancedate)
            objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGrievancerefno.ToString)
            objDataOperation.AddParameter("@grievance_description", SqlDbType.NVarChar, mstrGrievance_Description.Length, mstrGrievance_Description.ToString)
            objDataOperation.AddParameter("@grievance_reliefwanted", SqlDbType.NVarChar, mstrGrievance_Reliefwanted.Length, mstrGrievance_Reliefwanted.ToString)
            objDataOperation.AddParameter("@appealremark", SqlDbType.NVarChar, mstrAppealremark.Length, mstrAppealremark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@fromemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromemployeeunkid.ToString)
            objDataOperation.AddParameter("@againstemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainstemployeeunkid.ToString)
            objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSettingid.ToString)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitted)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed)

            strQ = "INSERT INTO gregrievance_master ( " & _
                       "  grievancetypeid " & _
                       ", grievancedate " & _
                       ", grievancerefno " & _
                       ", grievance_description " & _
                       ", grievance_reliefwanted " & _
                       ", appealremark " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime " & _
                       ", fromemployeeunkid " & _
                       ", againstemployeeunkid" & _
                       ", approvalSettingid" & _
                       ", issubmitted" & _
                       ", isprocessed" & _
                   ") VALUES (" & _
                       "  @grievancetypeid " & _
                       ", @grievancedate " & _
                       ", @grievancerefno " & _
                       ", @grievance_description " & _
                       ", @grievance_reliefwanted " & _
                       ", @appealremark " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime " & _
                       ", @fromemployeeunkid " & _
                       ", @againstemployeeunkid" & _
                       ", @approvalSettingid" & _
                       ", @issubmitted" & _
                       ", @isprocessed" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGrievancemasterunkid = dsList.Tables(0).Rows(0).Item(0)

            If intRefNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintGrievancemasterunkid, "gregrievance_master", "grievancerefno", "grievancemasterunkid", "NextGrievanceRefNo", strRefNoPrefix, intCompanyId) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintGrievancemasterunkid, "gregrievance_master", "grievancerefno", "grievancemasterunkid", mstrGrievancerefno) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If dtScanAttachment IsNot Nothing AndAlso dtScanAttachment.Rows.Count > 0 Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = dtScanAttachment.Select("form_name = '" & strScreenName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintGrievancemasterunkid, 0))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If InsertGrievanceMasterAuditTrails(enAuditType.ADD, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (gregrievance_master) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, ByVal dtScanAttachment As DataTable, ByVal strScreenName As String, _
                           ByVal intCompanyId As Integer, ByVal strDocumentPath As String, ByVal strLstName As List(Of String), ByVal dtStartDate As Date, _
                           ByVal dtEndDate As Date, ByVal strDataBaseName As String, ByVal intYearId As Integer, Optional ByVal strSelfServiceUrl As String = "") As Boolean


        If isRefnoExist(mstrGrievancerefno, mintGrievancemasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
            Return False
        End If

        'If IsProcess(mintFromemployeeunkid, mintAgainstemployeeunkid, mintGrievancetypeid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, this grievance is already in process for this employee. please complete this grievance process.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xObjDataOpr
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancemasterunkid.ToString)
            objDataOperation.AddParameter("@grievancetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancetypeid.ToString)
            objDataOperation.AddParameter("@grievancedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtGrievancedate)
            objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGrievancerefno.ToString)
            objDataOperation.AddParameter("@grievance_description", SqlDbType.NVarChar, mstrGrievance_Description.Length, mstrGrievance_Description.ToString)
            objDataOperation.AddParameter("@grievance_reliefwanted", SqlDbType.NVarChar, mstrGrievance_Reliefwanted.Length, mstrGrievance_Reliefwanted.ToString)
            objDataOperation.AddParameter("@appealremark", SqlDbType.NVarChar, mstrAppealremark.Length, mstrAppealremark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@fromemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromemployeeunkid.ToString)
            objDataOperation.AddParameter("@againstemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainstemployeeunkid.ToString)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitted)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed)
            strQ = "UPDATE gregrievance_master SET " & _
                   "  grievancetypeid = @grievancetypeid" & _
                   ", grievancedate = @grievancedate" & _
                   ", grievancerefno = @grievancerefno" & _
                   ", grievance_description = @grievance_description" & _
                   ", grievance_reliefwanted = @grievance_reliefwanted" & _
                   ", appealremark = @appealremark" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", fromemployeeunkid = @fromemployeeunkid" & _
                   ", againstemployeeunkid = @againstemployeeunkid " & _
                   ", issubmitted = @issubmitted " & _
                   ", isprocessed = @isprocessed " & _
                   "WHERE grievancemasterunkid = @grievancemasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtScanAttachment IsNot Nothing AndAlso dtScanAttachment.Rows.Count > 0 Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = dtScanAttachment.Select("form_name = '" & strScreenName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintGrievancemasterunkid, 0))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If InsertGrievanceMasterAuditTrails(enAuditType.EDIT, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (gregrievance_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intEmployeeId As Integer, ByVal strDocument_Path As String, ByVal strScreenName As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE gregrievance_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE grievancemasterunkid = @grievancemasterunkid "

            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            Dim mdtScanAttachment As DataTable
            'S.SANDEEP |04-SEP-2021| -- START
            'objDocument.GetList(strDocument_Path, "List", "", intEmployeeId)
            objDocument.GetList(strDocument_Path, "List", "", intEmployeeId, , , , , CBool(IIf(intEmployeeId <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END
            Dim xRow = objDocument._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.GRIEVANCES And x.Field(Of String)("form_name") = "'" & strScreenName & "'")
            If xRow.Count > 0 Then
                mdtScanAttachment = xRow.CopyToDataTable()
            Else
                mdtScanAttachment = objDocument._Datatable.Clone
            End If


            If mdtScanAttachment IsNot Nothing AndAlso mdtScanAttachment.Rows.Count > 0 Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = mdtScanAttachment.Select("transactionunkid = '" & intUnkid.ToString() & "' AND form_name = '" & strScreenName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintGrievancemasterunkid, 0, "D"))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            'Me._ObjDataOpr = objDataOperation
            Me._Grievancemasterunkid = intUnkid
            If InsertGrievanceMasterAuditTrails(enAuditType.DELETE, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 

    Public Function IsProcess(ByVal FromEmpId As Integer, ByVal AgainstEmployee As Integer, ByVal grievancetypeid As Integer, ByVal greapprovalsetting As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If xObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xObjDataOpr
        End If

        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT * from gregrievance_master  " & _
                   " where fromemployeeunkid = @fromemployeeunkid" & _
                   " and againstemployeeunkid =@againstemployeeunkid" & _
                   " And grievancetypeid = @grievancetypeid " & _
                   " And isprocessed = 0 " & _
                   " And isvoid = 0 " & _
                   " And approvalSettingid =@greapprovalsetting "
            objDataOperation.AddParameter("@fromemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FromEmpId)
            objDataOperation.AddParameter("@againstemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, AgainstEmployee)
            objDataOperation.AddParameter("@grievancetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, grievancetypeid)
            objDataOperation.AddParameter("@greapprovalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, greapprovalsetting)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function


    'Gajanan [24-June-2019] -- Start      
    'Public Function isRefnoExist(ByVal strGrievanceRefNo As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isRefnoExist(ByVal strGrievanceRefNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Gajanan [24-June-2019] -- End



        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xObjDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xObjDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                    "  grievancemasterunkid " & _
                    ", grievancetypeid " & _
                    ", grievancedate " & _
                    ", grievancerefno " & _
                    ", grievance_description " & _
                    ", grievance_reliefwanted " & _
                    ", appealremark " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voidreason " & _
                    ", voiddatetime " & _
                    ", fromemployeeunkid " & _
                    ", againstemployeeunkid " & _
                    ", approvalSettingid " & _
                   "FROM gregrievance_master " & _
                   "WHERE gregrievance_master.isvoid = 0 AND approvalSettingid=@approvalSettingid AND gregrievance_master.grievancerefno = @grievancerefno"

            objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSettingid)
            objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, 700, strGrievanceRefNo)

            If intUnkid > 0 Then
                strQ &= " AND grievancemasterunkid <> @grievancemasterunkid "
                objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.NVarChar, 700, mintGrievancemasterunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GrievanceEligibleForDelete(ByVal xgrievancemasterunkid As Integer) As Boolean

        Dim dslist As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "gregrievance_master.grievancemasterunkid " & _
                   "FROM gregrievance_master " & _
                   "LEFT JOIN greresolution_step_tran " & _
                   "ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid " & _
                   "WHERE greresolution_step_tran.isvoid = 0 " & _
                   "AND gregrievance_master.issubmitted = 1 " & _
                   "AND gregrievance_master.isvoid = 0 " & _
                   "AND gregrievance_master.grievancemasterunkid = @grievancemasterunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xgrievancemasterunkid)

            dslist = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dslist.Tables(0).Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GrievanceEligibleForDelete; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dslist IsNot Nothing Then dslist.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function InsertGrievanceMasterAuditTrails(ByVal eAudit As enAuditType, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            StrQ = "INSERT INTO atgregrievance_master ( " & _
                       "  row_guid " & _
                       ", grievancemasterunkid " & _
                       ", grievancetypeid " & _
                       ", grievancedate " & _
                       ", grievancerefno " & _
                       ", grievance_description " & _
                       ", grievance_reliefwanted " & _
                       ", appealremark " & _
                       ", fromemployeeunkid " & _
                       ", againstemployeeunkid " & _
                       ", auditdatetime " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       ", isweb" & _
                       ", issubmitted" & _
                       ", isprocessed" & _
                   ") VALUES (" & _
                       "  @row_guid " & _
                       ", @grievancemasterunkid " & _
                       ", @grievancetypeid " & _
                       ", @grievancedate " & _
                       ", @grievancerefno " & _
                       ", @grievance_description " & _
                       ", @grievance_reliefwanted " & _
                       ", @appealremark " & _
                       ", @fromemployeeunkid " & _
                       ", @againstemployeeunkid " & _
                       ", GETDATE() " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                       ", @isweb" & _
                       ", @issubmitted" & _
                       ", @isprocessed" & _
                   ") "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancemasterunkid.ToString)
            objDataOperation.AddParameter("@grievancetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancetypeid.ToString)
            objDataOperation.AddParameter("@grievancedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtGrievancedate)
            objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGrievancerefno.ToString)
            objDataOperation.AddParameter("@grievance_description", SqlDbType.NVarChar, mstrGrievance_Description.Length, mstrGrievance_Description.ToString)
            objDataOperation.AddParameter("@grievance_reliefwanted", SqlDbType.NVarChar, mstrGrievance_Reliefwanted.Length, mstrGrievance_Reliefwanted.ToString)
            objDataOperation.AddParameter("@appealremark", SqlDbType.NVarChar, mstrAppealremark.Length, mstrAppealremark.ToString)
            objDataOperation.AddParameter("@fromemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromemployeeunkid.ToString)
            objDataOperation.AddParameter("@againstemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainstemployeeunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeUnkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAudit)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitted)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertGrievanceMasterAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer, ByVal intUserId As Integer, Optional ByVal strAUDValue As String = "") As Boolean
        Try
            dr("transactionunkid") = intValue
            dr("userunkid") = intUserId
            'Gajanan [13-AUG-2018] -- Start
            'Enhancement - Implementing Grievance Module.
            dr("filesize") = dr("filesize_kb")
            'Gajanan(13-AUG-2018) -- End
            If strAUDValue.Trim.Length > 0 Then dr("AUD") = strAUDValue
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRowValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetRefno(ByVal strTableName As String, _
                             ByVal enApprSetting As enGrievanceApproval, _
                             Optional ByVal xEmployeeUnkid As Integer = -1, Optional ByVal xRefNo As String = "", Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strfilter As String = "", Optional ByVal blnAgainstEmployee As Boolean = True) As DataSet                   'Gajanan [10-June-2019] -- Add blnAgainstEmployee
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                   "  0 AS grievancemasterunkid " & _
                   ", 0 AS grievancetypeid " & _
                   ", '' AS Grievancetype " & _
                   ", NULL AS grievancedate " & _
                   ", @Select AS grievancerefno " & _
                   ", '' AS grievance_description " & _
                   ", '' AS grievance_reliefwanted " & _
                   ", '' AS appealremark " & _
                   ", 0  AS isvoid " & _
                   ", 0  AS voiduserunkid " & _
                   ", '' AS voidreason " & _
                   ", NULL voiddatetime " & _
                   ", 0  AS fromemployeeunkid " & _
                   ", '' as Fromemployee " & _
                   ", 0  AS approvalSettingid " & _
                   ", 0  AS againstemployeeunkid " & _
                   ", @Select AS Againstemployee " & _
                   ", 0 AS issubmitted " & _
                   " UNION ALL "
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            strQ &= "SELECT " & _
                  "  gregrievance_master.grievancemasterunkid " & _
                  ", grievancetypeid " & _
                  ", cfcommon_master.name AS Grievancetype" & _
                  ", CONVERT(CHAR(8), grievancedate, 112) AS grievancedate" & _
                  ", grievancerefno " & _
                  ", grievance_description " & _
                  ", grievance_reliefwanted " & _
                  ", appealremark " & _
                  ", gregrievance_master.isvoid " & _
                  ", gregrievance_master.voiduserunkid " & _
                  ", gregrievance_master.voidreason " & _
                  ", gregrievance_master.voiddatetime " & _
                  ", fromemployeeunkid " & _
                  ", FromEmployee.employeecode + ' - '+  FromEmployee.surname + ' '  + FromEmployee.firstname as Fromemployee" & _
                  ", gregrievance_master.approvalSettingid " & _
                  ", againstemployeeunkid " & _
                  ", AgainstEmployee.employeecode + ' - '+  AgainstEmployee.surname  + ' ' +  AgainstEmployee.firstname as Againstemployee     " & _
                  ", gregrievance_master.issubmitted " & _
                  "FROM gregrievance_master " & _
                  " LEFT join cfcommon_master on gregrievance_master.grievancetypeid = cfcommon_master.masterunkid " & _
                  " LEFT join hremployee_master as FromEmployee on gregrievance_master.fromemployeeunkid = FromEmployee.employeeunkid " & _
                  " LEFT join hremployee_master as AgainstEmployee on gregrievance_master.againstemployeeunkid = AgainstEmployee.employeeunkid  " & _
                  " WHERE gregrievance_master.isvoid = 0 and gregrievance_master.issubmitted =1"
            If strfilter <> "" Then
                strQ += "And " + strfilter
            End If

            If xEmployeeUnkid > 0 Then


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'strQ &= " AND gregrievance_master.fromemployeeunkid = @employeeunkid"
                If blnAgainstEmployee Then
                    strQ &= " AND gregrievance_master.againstemployeeunkid = @employeeunkid"
                Else
                strQ &= " AND gregrievance_master.fromemployeeunkid = @employeeunkid"
                End If
                'Gajanan [10-June-2019] -- End


                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeUnkid.ToString)
            End If

            If xRefNo.Length > 0 Then
                strQ &= " AND gregrievance_master.grievancerefno = @grievancerefno"
                objDataOperation.AddParameter("@grievancerefno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xRefNo.ToString)
            End If


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   



            'If enGrievanceApproval.UserAcess = enApprSetting Then
            '    strQ &= " AND gregrievance_master.approvalSettingid = @approvalSettingid"
            '    objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.UserAcess))

            'ElseIf enGrievanceApproval.UserAcess = enGrievanceApproval.ApproverEmpMapping Then
            '    strQ &= " AND gregrievance_master.approvalSettingid = @approvalSettingid"
            '    objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.UserAcess))

            'ElseIf enGrievanceApproval.UserAcess = enGrievanceApproval.ApproverEmpMapping Then
            '    strQ &= " AND gregrievance_master.approvalSettingid = @approvalSettingid"
            '    objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.UserAcess))
            'End If

            strQ &= " AND gregrievance_master.approvalSettingid = @approvalSettingid"

            If enGrievanceApproval.UserAcess = enApprSetting Then
                objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.UserAcess))

            ElseIf enGrievanceApproval.ApproverEmpMapping = enApprSetting Then
                objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ApproverEmpMapping))

            ElseIf enGrievanceApproval.ReportingTo = enApprSetting Then
                objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ReportingTo))
            End If


            'Gajanan [24-OCT-2019] -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function ChangeGrievanceStatus(ByVal intGrievanceUnkid As Integer, Optional ByVal Doopration As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Dim objResolution As New clsResolution_Step_Tran
        Dim objInitiatorResponse As New clsgreinitiater_response_tran
        Dim dsList As New DataSet
        Try

            If Doopration Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = Doopration
            End If

            ' UPDATE GRIEVANCE MASTER
            strQ = "UPDATE gregrievance_master set isprocessed = 1 where grievancemasterunkid = @grievancemasterunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._ObjDataOpr = objDataOperation
            Me._Grievancemasterunkid = intGrievanceUnkid
            If Me.InsertGrievanceMasterAuditTrails(2, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            strQ = ""


            ' UPDATE RESOLUTION STEPS TRASACTION
            strQ = "UPDATE greinitiater_response_tran set isprocessed = 1 where grievancemasterunkid = @grievancemasterunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objResolution.GetList("List", mintApprovalSettingid, True, False, "AND greresolution_step_tran.grievancemasterunkid = '" & intGrievanceUnkid & "'")
            For Each row As DataRow In dsList.Tables("List").Rows
                objResolution._ResolutionStepTranunkid = CInt(row("resolutionsteptranunkid").ToString)

                objResolution._AuditUserId = mintAuditUserId
                objResolution._Loginemployeeunkid = mintLoginEmployeeUnkid
                objResolution._ClientIP = mstrClientIP
                objResolution._FormName = mstrFormName
                objResolution._FromWeb = mblnIsWeb
                objResolution._Approvalsettingid = mintApprovalSettingid

                If objResolution.InsertAuditTrailForResolutionStep(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            dsList = Nothing
            strQ = ""

            ' UPDATE INITIATER RESPONSE TRASACTION
            strQ = "UPDATE greresolution_step_tran set isprocessed = 1 where grievancemasterunkid = @grievancemasterunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            'dsList = objInitiatorResponse.GetList("List", True, mintApprovalSettingid, objDataOperation, False, " a.grievancemasterunkid = " & intGrievanceUnkid & "")


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'dsList = objInitiatorResponse.GetList("List", True, mintApprovalSettingid, objDataOperation, False, "and a.grievancemasterunkid = " & intGrievanceUnkid & "")
            dsList = objInitiatorResponse.GetList("List", mintApprovalSettingid, True, objDataOperation, False, "and a.grievancemasterunkid = " & intGrievanceUnkid & "")
            'Gajanan [24-OCT-2019] -- End


            'Gajanan [4-July-2019] -- End
            For Each row As DataRow In dsList.Tables("List").Rows
                objInitiatorResponse._Initiatortranunkid = CInt(row("initiatortranunkid").ToString)

                objInitiatorResponse._AuditUserid = mintAuditUserId
                objInitiatorResponse._Loginemployeeunkid = mintLoginEmployeeUnkid
                objInitiatorResponse._ClientIp = mstrClientIP
                objInitiatorResponse._FormName = mstrFormName
                objInitiatorResponse._IsFromWeb = mblnIsWeb
                objInitiatorResponse._Approvalsettingid = mintApprovalSettingid

                If objInitiatorResponse.Insert_AtTranLog(objDataOperation, 2, CInt(row("initiatortranunkid").ToString)) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            dsList = Nothing
            strQ = ""

            If Doopration Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If Doopration Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ChangeGrievanceStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If Doopration Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function ChangeGrievanceFinalStatus(ByVal intGrievanceUnkid As Integer, ByVal intResolutionUnkid As Integer, Optional ByVal Doopration As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Dim objResolution As New clsResolution_Step_Tran
        Dim objInitiatorResponse As New clsgreinitiater_response_tran
        Dim dsList As New DataSet
        Try

            If Doopration Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = Doopration
            End If

            ' UPDATE RESOLUTION STEPS TRASACTION
            strQ = "UPDATE greresolution_step_tran set isfinal = 1 where grievancemasterunkid = @grievancemasterunkid and resolutionsteptranunkid=@resolutionsteptranunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceUnkid)
            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intResolutionUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objResolution.GetList("List", True, " AND greinitiater_response_tran.grievancemasterunkid = '" & intGrievanceUnkid & "'")
            For Each row As DataRow In dsList.Tables("List").Rows
                objResolution._ResolutionStepTranunkid = CInt(row("resolutionsteptranunkid").ToString)

                objResolution._AuditUserId = mintAuditUserId
                objResolution._Loginemployeeunkid = mintLoginEmployeeUnkid
                objResolution._ClientIP = mstrClientIP
                objResolution._FormName = mstrFormName
                objResolution._FromWeb = mblnIsWeb
                objResolution._Approvalsettingid = mintApprovalSettingid

                If objResolution.InsertAuditTrailForResolutionStep(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            dsList = Nothing
            strQ = ""

            If Doopration Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If Doopration Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ChangeGrievanceFinalStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If Doopration Is Nothing Then objDataOperation = Nothing
        End Try

    End Function


    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes  

    Public Function GetResolutionStatus(ByVal strTableName As String _
                          , ByVal xDatabaseName As String _
                          , ByVal strEmployeeAsOnDate As String _
                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                          , Optional ByVal blnOnlyActive As Boolean = True _
                          , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                          , Optional ByVal objDOperation As clsDataOperation = Nothing _
                          , Optional ByVal mstrFilter As String = "" _
                          , Optional ByVal mintApprovalSetting As Integer = -1 _
                          , Optional ByVal xRefno As String = "" _
                          , Optional ByVal strEmpIds As String = "" _
                          ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName, "#empappr#")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName, "#empappr#")


            Dim strSelect As String = ""



            strQ = "SELECT " & _
                   "a.* " & _
                   ",ISNULL(greresolution_step_tran.qualifyremark, '') AS qualifyremark " & _
                   ",ISNULL(greresolution_step_tran.responseremark, '') AS responseremark " & _
                   ",CASE " & _
                          "WHEN greresolution_step_tran.responsetypeunkid = 1 THEN '@MyResponse' " & _
                          "WHEN greresolution_step_tran.responsetypeunkid = 2 THEN '@CommiteeResponse' " & _
                          "ELSE '@Pending' " & _
                     "END AS ApprovalResponse " & _
                   "FROM ("

            strQ &= "SELECT " & _
                    "  greapprover_master.approvermasterunkid " & _
                    ", greapprover_master.apprlevelunkid " & _
                    ", greapproverlevel_master.levelname " & _
                    ", greapproverlevel_master.priority " & _
                    ", greapprover_master.approverempid " & _
                    ", #APPR_NAME# as name" & _
                    ", #APPR_EMAIL# as email " & _
                    ", #DEPT_ID# AS departmentunkid " & _
                    ", #DEPT_NAME# as departmentname " & _
                    ", #JOB_ID# AS jobunkid" & _
                    ", #JOB_NAME# As  jobname " & _
                    ", greapprover_master.isvoid " & _
                    ", greapprover_master.mapuserunkid " & _
                    ", greapprover_master.voiddatetime " & _
                    ", greapprover_master.userunkid " & _
                    ", greapprover_master.voiduserunkid " & _
                    ", greapprover_master.voidreason " & _
                    ", greapprover_master.isexternal  " & _
                    ", greapprover_master.isactive  " & _
                    ", CASE WHEN greapprover_master.isexternal = 1 THEN @YES ELSE @NO END AS ExAppr " & _
                    ", #ACT_DATA# " & _
                    "FROM greapprover_master " & _
                    " LEFT JOIN greapproverlevel_master on greapproverlevel_master.apprlevelunkid = greapprover_master.apprlevelunkid " & _
                    " #EMPL_JOIN# " & _
                    " #DATA_JOIN# "
            strQ &= " JOIN greapprover_tran ON greapprover_tran.approvermasterunkid = greapprover_master.approvermasterunkid "
            strQ &= " LEFT JOIN gregrievance_master ON greapprover_tran.employeeunkid = gregrievance_master.againstemployeeunkid "

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE 1 = 1 AND greapprover_master.isexternal = #ExAppr# "


            If strEmpIds.Trim.Length > 0 Then
                StrQCondition &= " AND greapprover_tran.employeeunkid IN (" & strEmpIds & ") "
            End If


            If mintApprovalSetting > 0 Then
                StrQCondition &= " And greapprover_master.approvalsettingid = @approvalsetting "
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            If blnOnlyActive Then
                StrQCondition &= " AND greapprover_master.isvoid = 0 AND greapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND greapprover_master.isvoid = 0  AND greapprover_master.isactive = 0 "
            End If
            StrQCondition &= " AND greapprover_tran.isvoid = 0 "


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        

            If xRefno.Length > 0 Then
                StrQCondition &= " AND gregrievance_master.grievancerefno= " & xRefno & " "
            End If

            'Gajanan [10-June-2019] -- End


            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= mstrFilter
            End If


            Dim StrDataJoin As String = " LEFT JOIN " & _
                                          " ( " & _
                                          "    SELECT " & _
                                          "        departmentunkid " & _
                                          "        ,employeeunkid " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                          "    FROM #DB_Name#hremployee_transfer_tran " & _
                                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                          " ) AS Alloc ON Alloc.employeeunkid = #empappr#.employeeunkid AND Alloc.rno = 1 " & _
                                          " LEFT JOIN #DB_Name#hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "    SELECT " & _
                                          "         jobunkid " & _
                                          "        ,jobgroupunkid " & _
                                          "        ,employeeunkid " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                          "    FROM #DB_Name#hremployee_categorization_tran " & _
                                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                          " ) AS Jobs ON Jobs.employeeunkid = #empappr#.employeeunkid AND Jobs.rno = 1 " & _
                                          " LEFT JOIN #DB_Name#hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            strQ = strQ.Replace("#APPR_NAME#", "CASE " & _
                                                "WHEN greapprover_master.approvalsettingid = 1 THEN CASE " & _
                                                "WHEN ISNULL(guser.firstname + guser.lastname, '') = '' THEN " & _
                                                "ISNULL(guser.username, '') " & _
                                                "ELSE " & _
                                                "ISNULL(guser.firstname, '') + ' ' + ISNULL(guser.lastname, '') " & _
                                                "END " & _
                                                " WHEN greapprover_master.approvalsettingid = 2 THEN ISNULL(empappr.firstname, '') + ' ' + ISNULL(empappr.surname, '') END ")
            strQ = strQ.Replace("#APPR_EMAIL#", "CASE WHEN ISNULL(empappr.email, '') = '' THEN ISNULL(guser.email, '') ELSE ISNULL(empappr.email, '') END ")

            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS guser ON guser.userunkid = greapprover_master.mapuserunkid " & _
                                               "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = guser.employeeunkid " & _
                                               "LEFT JOIN hremployee_master AS empappr ON empappr.employeeunkid = greapprover_master.approverempid ")

            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#DB_Name#", "")
            strQ = strQ.Replace("#DEPT_ID#", "hrdepartment_master.departmentunkid")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
            strQ = strQ.Replace("#JOB_ID#", "hrjob_master.jobunkid")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")


            strQ = strQ.Replace("#ACT_DATA#", "CAST(CASE WHEN CONVERT(CHAR(8),#empappr#.appointeddate, 112) <= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),RET.RETIRE, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.EOC, 112),#EFDATE#) >= #EFDATE# " & _
                                              "  THEN 1 ELSE 0 END AS BIT) AS activestatus ")

            strQ = strQ.Replace("#EFDATE#", "'" & strEmployeeAsOnDate & "' ")

            strQ &= StrQCondition
            strQ &= StrQDtFilters


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes      
            strQ &= ") AS a " & _
                    "LEFT JOIN greresolution_step_tran " & _
                    "ON greresolution_step_tran.grievancemasterunkid = a.grievancemasterunkid " & _
                    "AND a.approvermasterunkid = greresolution_step_tran.approvermasterunkid "
            'Gajanan [10-June-2019] -- End


            strQ = strQ.Replace("#ExAppr#", "0")
            strQ = strQ.Replace("#empappr#", "empappr")



            strSelect &= strQ
            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            objDataOperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Commitee Response"))
            objDataOperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "My Response"))
            'Gajanan [10-June-2019] -- End


            dsList = objDataOperation.ExecQuery(strSelect, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet

            Dim objgrievanceapprover_master As New clsgrievanceapprover_master

            dsCompany = objgrievanceapprover_master.GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey : StrQDtFilters = ""
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = greapprover_master.approverempid  ")
                    strQ = strQ.Replace("#ExAppr#", "1")
                    strQ = strQ.Replace("#DB_Name#", "")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DEPT_ID#", "0")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_ID#", "0")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                    strQ = strQ.Replace("#APPR_EMAIL#", "ISNULL(cfuser_master.email,'')")
                    strQ = strQ.Replace("#ACT_DATA#", " hrmsConfiguration..cfuser_master.isactive AS activestatus ")
                    strQ = strQ.Replace("#empappr#", "")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate")), dr("DName"))

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = greapprover_master.approverempid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                    strQ = strQ.Replace("#DEPT_ID#", "hrdepartment_master.departmentunkid")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
                    strQ = strQ.Replace("#SEC_NAME#", "ISNULL(hrsection_master.name,'') ")
                    strQ = strQ.Replace("#JOB_ID#", "hrjob_master.jobunkid")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")
                    strQ = strQ.Replace("#APPR_EMAIL#", "ISNULL(hremployee_master.email,'')")
                    strQ = strQ.Replace("#ACT_DATA#", "CAST(CASE WHEN CONVERT(CHAR(8),hremployee_master.appointeddate, 112) <= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),RET.RETIRE, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.EOC, 112),#EFDATE#) >= #EFDATE# " & _
                                              "  THEN 1 ELSE 0 END AS BIT) AS activestatus ")
                    strQ = strQ.Replace("#EFDATE#", "'" & dr("EDate") & "' ")
                    strQ = strQ.Replace("#empappr#", "hremployee_master")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQDtFilters &= xDateFilterQry & " "
                        End If
                    End If

                End If

                strQ &= StrQCondition
                strQ &= StrQDtFilters
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
                objDataOperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)
                dstmp = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList

    End Function
    'Gajanan [10-June-2019] -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, this reference number already exists. Please assign new reference number.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Open")
			Language.setMessage(mstrModuleName, 5, "Commitee Response")
			Language.setMessage(mstrModuleName, 6, "My Response")
			Language.setMessage("clsMasterData", 838, "Agreed And Close")
			Language.setMessage("clsMasterData", 839, "Appeal")
			Language.setMessage("clsMasterData", 840, "Disagree And Close")
			Language.setMessage("clsMasterData", 841, "Disagree And Escalate To Next Level")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
