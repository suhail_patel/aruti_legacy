﻿Imports eZeeCommonLib
Imports System.IO
Imports System.Text
Imports System.Collections.Specialized
Imports System.Drawing.Graphics
Imports System.Windows.Forms.Control


Public Class clsgreinitiater_response_tran

    Private Shared ReadOnly mstrModuleName As String = "clsgreinitiater_response_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Private Variabel"
    Private mintinitiatortranunkid As Integer = 0
    Private mintresolutionsteptranunkid As Integer = 0
    Private mintgrievancemasterunkid As Integer = 0
    Private mintstatusunkid As Integer = 0
    Private mstrremark As String = ""
    Private mintinitiatorunkid As Integer = 0
    Private mblnisvoid As Boolean = False
    Private mintvoiduserunkid As Integer = 0
    Private mstrvoidreason As String = ""
    Private mdtvoiddatetime As DateTime = Nothing
    Private mblnisprocessed As Boolean = 0
    Private mintapprovalsettingid As Integer = 0
    Private mintapprlevelunkid As Integer = 0
    Private mintapproverempid As Integer = 0
    Private mblnissubmitted As Boolean = 0




    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime = Nothing
    Private mstrClientIp As String = String.Empty
    Private mintloginemployeeunkid As Integer = 0
    Private mstrHostName As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private objclsResolution_Step_Tran As New clsResolution_Step_Tran
    Private objGrievance_master As New clsGrievance_Master

    <Runtime.InteropServices.DllImport("gdiplus.dll")> _
Private Shared Function GdipEmfToWmfBits(ByVal _hEmf As IntPtr, ByVal _bufferSize As UInteger, ByVal _buffer() As Byte, ByVal _mappingMode As Integer, ByVal _flags As EmfToWmfBitsFlags) As UInteger
    End Function

    Private Enum EmfToWmfBitsFlags
        EmfToWmfBitsFlagsDefault = &H0
        EmfToWmfBitsFlagsEmbedEmf = &H1
        EmfToWmfBitsFlagsIncludePlaceable = &H2
        EmfToWmfBitsFlagsNoXORClip = &H4
    End Enum

#End Region

#Region "Properties"

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Initiatortranunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintinitiatortranunkid
        End Get
        Set(ByVal value As Integer)
            mintinitiatortranunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property


    Public Property _Resolutionsteptranunkid() As Integer
        Get
            Return mintresolutionsteptranunkid
        End Get
        Set(ByVal value As Integer)
            mintresolutionsteptranunkid = value
        End Set
    End Property


    Public Property _Grievancemasterunkid() As Integer
        Get
            Return mintgrievancemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintgrievancemasterunkid = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintstatusunkid
        End Get
        Set(ByVal value As Integer)
            mintstatusunkid = value
        End Set
    End Property

    Public Property _Remark() As String
        Get
            Return mstrremark
        End Get
        Set(ByVal value As String)
            mstrremark = value
        End Set
    End Property

    Public Property _Initiatorunkid() As Integer
        Get
            Return mintinitiatorunkid
        End Get
        Set(ByVal value As Integer)
            mintinitiatorunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnisvoid
        End Get
        Set(ByVal value As Boolean)
            mblnisvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintvoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintvoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtvoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtvoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrvoidreason
        End Get
        Set(ByVal value As String)
            mstrvoidreason = value
        End Set
    End Property



    Public Property _Isprocessed() As Boolean
        Get
            Return mblnisprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnisprocessed = value
        End Set
    End Property

    Public Property _Approvalsettingid() As Integer
        Get
            Return mintapprovalsettingid
        End Get
        Set(ByVal value As Integer)
            mintapprovalsettingid = value
        End Set
    End Property

    Public Property _Apprlevelunkid() As Integer
        Get
            Return mintapprlevelunkid
        End Get
        Set(ByVal value As Integer)
            mintapprlevelunkid = value
        End Set
    End Property

    Public Property _Approverempid() As Integer
        Get
            Return mintapproverempid
        End Get
        Set(ByVal value As Integer)
            mintapproverempid = value
        End Set
    End Property

    Public Property _Issubmitted() As Boolean
        Get
            Return mblnissubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnissubmitted = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return mstrClientIp
        End Get
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

#End Region

    Private Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dslist As DataSet = Nothing
        Dim strq As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()

            strq = "SELECT " & _
                    "greinitiater_response_tran.initiatortranunkid " & _
                    ",greinitiater_response_tran.resolutionsteptranunkid " & _
                    ",greinitiater_response_tran.grievancemasterunkid " & _
                    ",greinitiater_response_tran.statusunkid " & _
                    ",greinitiater_response_tran.remark " & _
                    ",greinitiater_response_tran.initiatorunkid " & _
                    ",greinitiater_response_tran.isvoid " & _
                    ",greinitiater_response_tran.isprocessed " & _
                    "from greinitiater_response_tran " & _
                    "WHERE greinitiater_response_tran.initiatortranunkid = @initiatortranunkid "

            objDataOperation.AddParameter("@initiatortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintinitiatortranunkid)

            dslist = objDataOperation.ExecQuery(strq, "List")


            For Each dtRow As DataRow In dslist.Tables(0).Rows
                mintresolutionsteptranunkid = CInt(dtRow("resolutionsteptranunkid"))
                mintgrievancemasterunkid = CInt(dtRow("grievancemasterunkid"))
                mintstatusunkid = CInt(dtRow("statusunkid"))
                mstrremark = CStr(dtRow("remark"))
                mintinitiatorunkid = CInt(dtRow("initiatorunkid"))
                mblnisvoid = CBool(dtRow("isvoid"))
                mblnisprocessed = CBool(dtRow("isprocessed"))
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dslist IsNot Nothing Then dslist.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub

    Public Function GetList(ByVal strTableName As String, _
                            ByVal enApprSetting As enGrievanceApproval, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal objDoOperation As clsDataOperation = Nothing, _
                            Optional ByVal mblnAddSelect As Boolean = False, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal employeeunkid As Integer = 0) As DataSet 'Gajanan [27-June-2019] -- Add employeeunkid

        Dim dslist As DataSet = Nothing
        Dim strq As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try

            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   

            'If mblnAddSelect = True Then
            '    strq = "SELECT " & _
            '          "  0 AS initiatortranunkid " & _
            '          ", 0 AS grievancetype " & _
            '          ", '' AS grievancemasterunkid " & _
            '          ",0 AS againstemployeeunkid" & _
            '          ", '' as againstemployee " & _
            '          ",Null as grievancedate" & _
            '          ", '' as responseremark" & _
            '          ", 0 as responsetypeunkid" & _
            '          ", 0 as responsetype" & _
            '          ",0 as resolutionsteptranunkid" & _
            '          ",null as meetingdate  " & _
            '          ",0 as  grievancerefno" & _
            '          ",'' as  remark" & _
            '          ",0 as  issubmitted" & _
            '          ",0 as  priority" & _
            '          ",0 as  refno" & _
            '          ",'' as levelname" & _
            '          ",0 as approverempid " & _
            '          ",0 as mapuserunkid " & _
            '          " UNION ALL "
            'End If

            'strq &= "SELECT * from ( " & _
            '       "SELECT " & _
            '       "greinitiater_response_tran.initiatortranunkid " & _
            '       ",cfcommon_master.name as grievancetype " & _
            '       ",greinitiater_response_tran.grievancemasterunkid " & _
            '       ",ISNULL(hremployee_master.employeeunkid, '') AS againstemployeeunkid" & _
            '       ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS againstemployee" & _
            '       ",CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
            '       ",greresolution_step_tran.responseremark " & _
            '       ",greresolution_step_tran.responsetypeunkid " & _
            '       ",CASE greresolution_step_tran.responsetypeunkid " & _
            '       " WHEN " & clsResolution_Step_Tran.enResponseType.My_Response & " THEN @MyResponse " & _
            '       " WHEN " & clsResolution_Step_Tran.enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
            '       " END as responsetype " & _
            '       ",greresolution_step_tran.resolutionsteptranunkid " & _
            '       ",CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
            '       ",gregrievance_master.grievancerefno as grievancerefno " & _
            '       ",greinitiater_response_tran.remark as remark " & _
            '       ",greinitiater_response_tran.issubmitted " & _
            '       ",greapproverlevel_master.priority as priority " & _
            '       ",gregrievance_master.grievancerefno as refno " & _
            '       ",greapproverlevel_master.levelname " & _
            '       ",greapprover_master.approverempid " & _
            '       ",greapprover_master.mapuserunkid " & _
            '       "FROM greinitiater_response_tran " & _
            '       "JOIN gregrievance_master ON gregrievance_master.grievancemasterunkid = greinitiater_response_tran.grievancemasterunkid and gregrievance_master.approvalSettingid=@approvalSettingid " & _
            '       "JOIN greresolution_step_tran ON greinitiater_response_tran.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
            '       "LEFT JOIN hremployee_master " & _
            '       "ON hremployee_master.employeeunkid = gregrievance_master.againstemployeeunkid " & _
            '       "LEFT JOIN cfcommon_master " & _
            '       "ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
            '       "join greapproverlevel_master on greapproverlevel_master.apprlevelunkid = greinitiater_response_tran.apprlevelunkid " & _
            '       "JOIN greapprover_master " & _
            '       " ON greapprover_master.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
            '       "WHERE greinitiater_response_tran.isvoid = 0 "

            'If employeeunkid > 0 Then
            '    strq &= "and gregrievance_master.fromemployeeunkid = " & employeeunkid & " "
            'End If

            'strq &= "UNION ALL " & _
            '       "SELECT " & _
            '       "-1 as initiatortranunkid " & _
            '       ",cfcommon_master.name as grievancetype " & _
            '       ",greresolution_step_tran.grievancemasterunkid " & _
            '       ",ISNULL(hremployee_master.employeeunkid, '') AS againstemployeeunkid" & _
            '       ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS againstemployee" & _
            '       ",CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
            '       ",greresolution_step_tran.responseremark " & _
            '       ",greresolution_step_tran.grievancemasterunkid " & _
            '       ",CASE greresolution_step_tran.responsetypeunkid " & _
            '       " WHEN " & clsResolution_Step_Tran.enResponseType.My_Response & " THEN @MyResponse " & _
            '       " WHEN " & clsResolution_Step_Tran.enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
            '       "END as responsetype " & _
            '       ",greresolution_step_tran.resolutionsteptranunkid " & _
            '       ",CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
            '       ",gregrievance_master.grievancerefno as grievancerefno " & _
            '       ",'' as remark " & _
            '       ",0 as issubmitted " & _
            '       ",greapproverlevel_master.priority as priority " & _
            '       ",gregrievance_master.grievancerefno as refno " & _
            '   ",greapproverlevel_master.levelname " & _
            '       ",greapprover_master.approverempid" & _
            '       ",greapprover_master.mapuserunkid " & _
            '       "FROM greresolution_step_tran " & _
            '       "JOIN gregrievance_master ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid  and gregrievance_master.approvalSettingid=@approvalSettingid " & _
            '       "LEFT JOIN hremployee_master " & _
            '       "ON hremployee_master.employeeunkid = gregrievance_master.againstemployeeunkid " & _
            '       "LEFT JOIN cfcommon_master " & _
            '       "ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
            '       "join greapproverlevel_master on greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
            '       "JOIN greapprover_master " & _
            '       "ON greapprover_master.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
            '       "WHERE greresolution_step_tran.isvoid = 0 and greresolution_step_tran.issubmitted =1" & _
            '       "and gregrievance_master.fromemployeeunkid = " & employeeunkid & " " & _
            '       "AND greresolution_step_tran.resolutionsteptranunkid NOT IN (SELECT " & _
            '       "resolutionsteptranunkid " & _
            '       "FROM greinitiater_response_tran " & _
            '       "WHERE isvoid = 0 )" & _
            '       ") as a "
            ''Gajanan [4-July-2019] -- End
            'If mstrFilter.Trim.Length > 0 Then
            '    strq &= "Where 1 = 1 " + mstrFilter
            'End If

            'strq &= " ORDER by grievancemasterunkid "

            'objDataOperation.ClearParameters()

            'If enGrievanceApproval.UserAcess = enApprSetting Then
            '    objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.UserAcess))

            'ElseIf enGrievanceApproval.ApproverEmpMapping = enGrievanceApproval.ApproverEmpMapping Then
            '    objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ApproverEmpMapping))
            'End If

            'objDataOperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "My Response"))
            'objDataOperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))

            'dslist = objDataOperation.ExecQuery(strq, strTableName)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If



            Select Case enApprSetting
                Case enGrievanceApproval.ReportingTo

                    'strq = "IF OBJECT_ID('tempdb..#GrievanceAgainstEmp') IS NOT NULL " & _
                    '  "DROP TABLE #GrievanceAgainstEmp " & _
                    '  "CREATE TABLE #GrievanceAgainstEmp ( " & _
                    '       "EmpIndex INT identity(1, 1) " & _
                    '       ",empid INT " & _
                    '       ") " & _
                    '  "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                    '       "DROP TABLE #Results " & _
                    '  "CREATE TABLE #Results ( " & _
                    '       "eid INT " & _
                    '       ",rempid INT " & _
                    '       ",empid INT " & _
                    '       ",priority INT " & _
                    '       ") " & _
                    '  " " & _
                    '  "DECLARE @REmpId AS INT " & _
                    '       ",@NxEmpId AS INT " & _
                    '       ",@Eid AS INT " & _
                    '       ",@priority AS INT " & _
                    '  "SET @REmpId = 0 " & _
                    '  "SET @NxEmpId = 0 " & _
                    '  "SET @eid = 0 " & _
                    '  "SET @priority = 0 " & _
                    '  "INSERT INTO #GrievanceAgainstEmp " & _
                    '  "SELECT againstemployeeunkid " & _
                    '  "FROM ( " & _
                    '       "SELECT againstemployeeunkid " & _
                    '            ",ROW_NUMBER() OVER ( " & _
                    '                 "PARTITION BY againstemployeeunkid ORDER BY againstemployeeunkid " & _
                    '                 ") AS rno " & _
                    '       "FROM gregrievance_master " & _
                    '       "WHERE issubmitted = 1 " & _
                    '            "AND isprocessed = 0 " & _
                    '            "AND isvoid = 0 " & _
                    '       ") AS b " & _
                    '  "WHERE b.rno = 1 " & _
                    '  "DECLARE @TotalRows INT = ( " & _
                    '            "SELECT COUNT(1) " & _
                    '            "FROM #GrievanceAgainstEmp " & _
                    '            ") " & _
                    '       ",@i INT = 1 " & _
                    '  "WHILE @i <= @TotalRows " & _
                    '  "BEGIN " & _
                    '       "SELECT @NxEmpId = empid " & _
                    '       "FROM #GrievanceAgainstEmp " & _
                    '       "WHERE EmpIndex = @i " & _
                    '       "SET @Eid = @NxEmpId " & _
                    '       "WHILE @NxEmpId > 0 " & _
                    '       "BEGIN " & _
                    '            "SET @priority = @priority + 1 " & _
                    '            "SET @REmpId = ISNULL(( " & _
                    '                           "SELECT reporttoemployeeunkid " & _
                    '                           "FROM hremployee_reportto " & _
                    '                           "WHERE employeeunkid = @NxEmpId " & _
                    '                                "AND ishierarchy = 1 " & _
                    '                                "AND isvoid = 0 " & _
                    '                           "), 0) " & _
                    '            "IF (@NxEmpId = @REmpId) " & _
                    '                 "SET @REmpId = 0 " & _
                    '            "INSERT INTO #Results ( " & _
                    '                 "eid " & _
                    '                 ",rempid " & _
                    '                 ",empid " & _
                    '                 ",priority " & _
                    '                 ") " & _
                    '            "VALUES ( " & _
                    '                 "@NxEmpId " & _
                    '                 ",@REmpId " & _
                    '                 ",@Eid " & _
                    '                 ",@priority " & _
                    '                 ") " & _
                    '            "IF (@REmpId = 0) " & _
                    '            "BEGIN " & _
                    '                 "SET @i = @i + 1 " & _
                    '                 "SET @priority = 0 " & _
                    '                 "SELECT @NxEmpId = empid " & _
                    '                 "FROM #GrievanceAgainstEmp " & _
                    '                 "WHERE EmpIndex = @i " & _
                    '                 "BREAK; " & _
                    '            "END " & _
                    '            "ELSE " & _
                    '                 "SET @NxEmpId = @REmpId " & _
                    '            "DECLARE @Rcnt AS INT " & _
                    '            "SET @Rcnt = ( " & _
                    '                      "SELECT COUNT(1) " & _
                    '                      "FROM #Results " & _
                    '                      "WHERE rempid = @NxEmpId " & _
                    '                           "AND #Results.empid = @Eid " & _
                    '                      ") " & _
                    '            "IF (@Rcnt > 1) " & _
                    '            "BEGIN " & _
                    '                 "SET @i = @i + 1 " & _
                    '                 "SELECT @NxEmpId = empid " & _
                    '                 "FROM #GrievanceAgainstEmp " & _
                    '                 "WHERE EmpIndex = @i " & _
                    '                 "SET @priority = 0 " & _
                    '                 "BREAK; " & _
                    '            "END " & _
                    '            "ELSE " & _
                    '                 "CONTINUE " & _
                    '       "END " & _
                    '  "END "

                    Dim objgrievanceMst As New clsgrievanceapprover_master
                    strq = objgrievanceMst.getGrievanceReportingToQuery(False, True)
                    objgrievanceMst = Nothing


            If mblnAddSelect = True Then
                        strq &= "SELECT " & _
                              "  0 AS initiatortranunkid " & _
                              ", 0 AS grievancetype " & _
                              ", '' AS grievancemasterunkid " & _
                              ",0 AS againstemployeeunkid" & _
                              ", '' as againstemployee " & _
                              ",Null as grievancedate" & _
                              ", '' as responseremark" & _
                              ", 0 as responsetypeunkid" & _
                              ", 0 as responsetype" & _
                              ",0 as resolutionsteptranunkid" & _
                              ",null as meetingdate  " & _
                              ",0 as  grievancerefno" & _
                              ",'' as  remark" & _
                              ",0 as  issubmitted" & _
                              ",0 as  priority" & _
                              ",0 as  refno" & _
                              ",'' as levelname" & _
                              ",0 as approverempid " & _
                              ",0 as mapuserunkid " & _
                              ",0 as isprocessed " & _
                              " UNION ALL "
                    End If

                    strq &= "SELECT * from ( " & _
                  "SELECT " & _
                  "greinitiater_response_tran.initiatortranunkid " & _
                  ",cfcommon_master.name as grievancetype " & _
                  ",greinitiater_response_tran.grievancemasterunkid " & _
                  ",ISNULL(hremployee_master.employeeunkid, '') AS againstemployeeunkid" & _
                  ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS againstemployee" & _
                  ",CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
                  ",greresolution_step_tran.responseremark " & _
                  ",greresolution_step_tran.responsetypeunkid " & _
                  ",CASE greresolution_step_tran.responsetypeunkid " & _
                  " WHEN " & clsResolution_Step_Tran.enResponseType.My_Response & " THEN @MyResponse " & _
                  " WHEN " & clsResolution_Step_Tran.enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
                  " END as responsetype " & _
                  ",greresolution_step_tran.resolutionsteptranunkid " & _
                  ",CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
                  ",gregrievance_master.grievancerefno as grievancerefno " & _
                  ",greinitiater_response_tran.remark as remark " & _
                  ",greinitiater_response_tran.issubmitted " & _
                  ",#Results.priority " & _
                  ",gregrievance_master.grievancerefno as refno " & _
                  ",@Level + ' - ' + CONVERT(NVARCHAR,#Results.priority) AS levelname " & _
                  ",#Results.rempid as approverempid " & _
                  ",#Results.rempid as mapuserunkid " & _
                  ",gregrievance_master.isprocessed " & _
                  "FROM greinitiater_response_tran " & _
                  "JOIN gregrievance_master ON gregrievance_master.grievancemasterunkid = greinitiater_response_tran.grievancemasterunkid and gregrievance_master.approvalSettingid=@approvalSettingid " & _
                  "JOIN greresolution_step_tran ON greinitiater_response_tran.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
                  "LEFT JOIN hremployee_master " & _
                  "ON hremployee_master.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                  "LEFT JOIN cfcommon_master " & _
                  "ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
                  "LEFT JOIN #Results ON #Results.rempid = greresolution_step_tran.approvermasterunkid AND gregrievance_master.grievancemasterunkid = #Results.grievanceId " & _
                  "WHERE greinitiater_response_tran.isvoid = 0 and greinitiater_response_tran.isprocessed = 0 "

                    If employeeunkid > 0 Then
                        strq &= "and gregrievance_master.fromemployeeunkid = " & employeeunkid & " "
                    End If

                    strq &= "UNION ALL " & _
                           "SELECT " & _
                           "-1 as initiatortranunkid " & _
                           ",cfcommon_master.name as grievancetype " & _
                           ",greresolution_step_tran.grievancemasterunkid " & _
                           ",ISNULL(hremployee_master.employeeunkid, '') AS againstemployeeunkid" & _
                           ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS againstemployee" & _
                           ",CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
                           ",greresolution_step_tran.responseremark " & _
                           ",greresolution_step_tran.grievancemasterunkid " & _
                           ",CASE greresolution_step_tran.responsetypeunkid " & _
                           " WHEN " & clsResolution_Step_Tran.enResponseType.My_Response & " THEN @MyResponse " & _
                           " WHEN " & clsResolution_Step_Tran.enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
                           "END as responsetype " & _
                           ",greresolution_step_tran.resolutionsteptranunkid " & _
                           ",CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
                           ",gregrievance_master.grievancerefno as grievancerefno " & _
                           ",'' as remark " & _
                           ",0 as issubmitted " & _
                           ",#Results.priority " & _
                           ",gregrievance_master.grievancerefno as refno " & _
                           ",@Level + ' - ' + CONVERT(NVARCHAR,#Results.priority) AS levelname " & _
                           ",#Results.rempid AS approverempid " & _
                           ",#Results.rempid AS mapuserunkid " & _
                           ",gregrievance_master.isprocessed " & _
                           "FROM greresolution_step_tran " & _
                           "JOIN gregrievance_master ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid  and gregrievance_master.approvalSettingid=@approvalSettingid " & _
                           "LEFT JOIN hremployee_master " & _
                           "ON hremployee_master.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                           "LEFT JOIN cfcommon_master " & _
                           "ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
                           "LEFT JOIN #Results ON #Results.rempid = greresolution_step_tran.approvermasterunkid " & _
                           " AND gregrievance_master.grievancemasterunkid = #Results.grievanceId " & _
                           "WHERE greresolution_step_tran.isvoid = 0 and greresolution_step_tran.issubmitted =1 and greresolution_step_tran.isprocessed = 0 " & _
                           "and gregrievance_master.fromemployeeunkid = " & employeeunkid & " " & _
                           "AND greresolution_step_tran.resolutionsteptranunkid NOT IN (SELECT " & _
                           "resolutionsteptranunkid " & _
                           "FROM greinitiater_response_tran " & _
                           "WHERE isvoid = 0 )" & _
                           ") as a "
                    If mstrFilter.Trim.Length > 0 Then
                        strq &= "Where 1 = 1 AND A.isprocessed = 0 " + mstrFilter
                    End If

                    strq &= " ORDER by grievancemasterunkid DROP TABLE #Results DROP TABLE #GrievanceAgainstEmp "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ReportingTo))
                    objDataOperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "My Response"))
                    objDataOperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))
                    objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Level"))

                Case Else
                    If mblnAddSelect = True Then
                strq = "SELECT " & _
                      "  0 AS initiatortranunkid " & _
                      ", 0 AS grievancetype " & _
                      ", '' AS grievancemasterunkid " & _
                      ",0 AS againstemployeeunkid" & _
                      ", '' as againstemployee " & _
                      ",Null as grievancedate" & _
                      ", '' as responseremark" & _
                      ", 0 as responsetypeunkid" & _
                      ", 0 as responsetype" & _
                      ",0 as resolutionsteptranunkid" & _
                      ",null as meetingdate  " & _
                      ",0 as  grievancerefno" & _
                      ",'' as  remark" & _
                      ",0 as  issubmitted" & _
                      ",0 as  priority" & _
                      ",0 as  refno" & _
                      ",'' as levelname" & _
                      ",0 as approverempid " & _
                      ",0 as mapuserunkid " & _
                      " UNION ALL "
            End If

            strq &= "SELECT * from ( " & _
                   "SELECT " & _
                   "greinitiater_response_tran.initiatortranunkid " & _
                   ",cfcommon_master.name as grievancetype " & _
                   ",greinitiater_response_tran.grievancemasterunkid " & _
                   ",ISNULL(hremployee_master.employeeunkid, '') AS againstemployeeunkid" & _
                   ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS againstemployee" & _
                   ",CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
                   ",greresolution_step_tran.responseremark " & _
                   ",greresolution_step_tran.responsetypeunkid " & _
                   ",CASE greresolution_step_tran.responsetypeunkid " & _
                   " WHEN " & clsResolution_Step_Tran.enResponseType.My_Response & " THEN @MyResponse " & _
                   " WHEN " & clsResolution_Step_Tran.enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
                   " END as responsetype " & _
                   ",greresolution_step_tran.resolutionsteptranunkid " & _
                   ",CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
                   ",gregrievance_master.grievancerefno as grievancerefno " & _
                   ",greinitiater_response_tran.remark as remark " & _
                   ",greinitiater_response_tran.issubmitted " & _
                   ",greapproverlevel_master.priority as priority " & _
                   ",gregrievance_master.grievancerefno as refno " & _
                   ",greapproverlevel_master.levelname " & _
                   ",greapprover_master.approverempid " & _
                   ",greapprover_master.mapuserunkid " & _
                   "FROM greinitiater_response_tran " & _
                   "JOIN gregrievance_master ON gregrievance_master.grievancemasterunkid = greinitiater_response_tran.grievancemasterunkid and gregrievance_master.approvalSettingid=@approvalSettingid " & _
                   "JOIN greresolution_step_tran ON greinitiater_response_tran.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
                   "LEFT JOIN hremployee_master " & _
                   "ON hremployee_master.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                   "LEFT JOIN cfcommon_master " & _
                   "ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
                   "join greapproverlevel_master on greapproverlevel_master.apprlevelunkid = greinitiater_response_tran.apprlevelunkid " & _
                   "JOIN greapprover_master " & _
                   " ON greapprover_master.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
                   "WHERE greinitiater_response_tran.isvoid = 0 "

            If employeeunkid > 0 Then
                strq &= "and gregrievance_master.fromemployeeunkid = " & employeeunkid & " "
            End If

            strq &= "UNION ALL " & _
                   "SELECT " & _
                   "-1 as initiatortranunkid " & _
                   ",cfcommon_master.name as grievancetype " & _
                   ",greresolution_step_tran.grievancemasterunkid " & _
                   ",ISNULL(hremployee_master.employeeunkid, '') AS againstemployeeunkid" & _
                   ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS againstemployee" & _
                   ",CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
                   ",greresolution_step_tran.responseremark " & _
                   ",greresolution_step_tran.grievancemasterunkid " & _
                   ",CASE greresolution_step_tran.responsetypeunkid " & _
                   " WHEN " & clsResolution_Step_Tran.enResponseType.My_Response & " THEN @MyResponse " & _
                   " WHEN " & clsResolution_Step_Tran.enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
                   "END as responsetype " & _
                   ",greresolution_step_tran.resolutionsteptranunkid " & _
                   ",CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
                   ",gregrievance_master.grievancerefno as grievancerefno " & _
                   ",'' as remark " & _
                   ",0 as issubmitted " & _
                   ",greapproverlevel_master.priority as priority " & _
                   ",gregrievance_master.grievancerefno as refno " & _
               ",greapproverlevel_master.levelname " & _
                   ",greapprover_master.approverempid" & _
                   ",greapprover_master.mapuserunkid " & _
                   "FROM greresolution_step_tran " & _
                   "JOIN gregrievance_master ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid  and gregrievance_master.approvalSettingid=@approvalSettingid " & _
                   "LEFT JOIN hremployee_master " & _
                   "ON hremployee_master.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                   "LEFT JOIN cfcommon_master " & _
                   "ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
                   "join greapproverlevel_master on greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                   "JOIN greapprover_master " & _
                   "ON greapprover_master.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
                   "WHERE greresolution_step_tran.isvoid = 0 and greresolution_step_tran.issubmitted =1" & _
               "and gregrievance_master.fromemployeeunkid = " & employeeunkid & " " & _
                   "AND greresolution_step_tran.resolutionsteptranunkid NOT IN (SELECT " & _
                   "resolutionsteptranunkid " & _
                   "FROM greinitiater_response_tran " & _
                   "WHERE isvoid = 0 )" & _
                   ") as a "
            'Gajanan [4-July-2019] -- End
            If mstrFilter.Trim.Length > 0 Then
                strq &= "Where 1 = 1 " + mstrFilter
            End If

            strq &= " ORDER by grievancemasterunkid "

            objDataOperation.ClearParameters()
            If enGrievanceApproval.UserAcess = enApprSetting Then
                objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.UserAcess))

            ElseIf enGrievanceApproval.ApproverEmpMapping = enApprSetting Then
                objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ApproverEmpMapping))
            End If

            objDataOperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "My Response"))
            objDataOperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))


            End Select


            dslist = objDataOperation.ExecQuery(strq, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Gajanan [24-OCT-2019] -- End


            
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dslist IsNot Nothing Then dslist.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dslist

    End Function

    Public Function Insert(Optional ByVal IsProcessChange As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        objDataOperation.ClearParameters()

        Try
            strQ = "INSERT Into greinitiater_response_tran ( " & _
                   "resolutionsteptranunkid " & _
                   ",grievancemasterunkid " & _
                   ",statusunkid " & _
                   ",remark " & _
                   ",initiatorunkid " & _
                   ",isvoid " & _
                   ",voiduserunkid " & _
                   ",voidreason " & _
                   ",voiddatetime " & _
                   ",isprocessed " & _
                   ",approvalsettingid " & _
                   ",apprlevelunkid " & _
                   ",approverempid " & _
                   ",issubmitted " & _
                   ") VALUES ( " & _
                   " @resolutionsteptranunkid " & _
                   ",@grievancemasterunkid " & _
                   ",@statusunkid " & _
                   ",@remark " & _
                   ",@initiatorunkid " & _
                   ",@isvoid " & _
                   ",@voiduserunkid " & _
                   ",@voidreason " & _
                   ",@voiddatetime " & _
                   ",@isprocessed " & _
                   ",@approvalsettingid " & _
                   ",@apprlevelunkid " & _
                   ",@approverempid " & _
                   ",@issubmitted " & _
                   "); SELECT @@identity "

            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintresolutionsteptranunkid)
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintgrievancemasterunkid)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintstatusunkid)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrremark)
            objDataOperation.AddParameter("@initiatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintinitiatorunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason)
            If mdtvoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
            End If
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisprocessed)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid)
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverempid)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnissubmitted)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintinitiatortranunkid = dsList.Tables(0).Rows(0).Item(0)

            _Initiatortranunkid(objDataOperation) = mintinitiatortranunkid


            Me._ClientIp = mstrClientIp
            Me._Loginemployeeunkid = mintloginemployeeunkid
            Me._AuditUserid = minAuditUserid
            Me._HostName = mstrHostName
            Me._FormName = mstrFormName
            Me._IsFromWeb = blnIsFromWeb


            If Insert_AtTranLog(objDataOperation, 1, mintinitiatortranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Start - THIS METHOD IS USED WHEN IF RESPONSE TYPE IS 'AGREE AND CLOSE' OR 'DISAGREE AND CLOSE' OR 'APPEAL' THAN YOU HAVE TO SET ISPROCESSED=1 
            'IN TABLES WHERE THIS COLUMN IS AVAILABLE 

            If IsProcessChange Then
                objGrievance_master._AuditUserId = minAuditUserid
                objGrievance_master._ClientIP = mstrClientIp
                objGrievance_master._HostName = mstrHostName
                objGrievance_master._LoginEmployeeUnkid = mintloginemployeeunkid
                objGrievance_master._FormName = mstrFormName
                objGrievance_master._FromWeb = blnIsFromWeb
                
                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                objGrievance_master._ApprovalSettingid = mintapprovalsettingid
                'Gajanan [24-OCT-2019] -- End

If objGrievance_master.ChangeGrievanceStatus(mintgrievancemasterunkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'END

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    Public Function Update(ByVal unkid As Integer, Optional ByVal IsProcessChange As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        objDataOperation.ClearParameters()

        Try
            strQ = "Update greinitiater_response_tran set " & _
                   " resolutionsteptranunkid =@resolutionsteptranunkid" & _
                   ",grievancemasterunkid = @grievancemasterunkid" & _
                   ",statusunkid =@statusunkid" & _
                   ",remark =@remark" & _
                   ",initiatorunkid =@initiatorunkid " & _
                   ",isvoid =@isvoid" & _
                   ",voiduserunkid=@voiduserunkid " & _
                   ",voidreason=@voidreason " & _
                   ",voiddatetime=@voiddatetime " & _
                   ",isprocessed=@isprocessed " & _
                   ",approvalsettingid=@approvalsettingid " & _
                   ",apprlevelunkid=@apprlevelunkid " & _
                   ",approverempid=@approverempid " & _
                   ",issubmitted=@issubmitted " & _
                   " where initiatortranunkid=@initiatortranunkid "

            objDataOperation.AddParameter("@initiatortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, unkid)
            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintresolutionsteptranunkid)
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintgrievancemasterunkid)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintstatusunkid)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrremark)
            objDataOperation.AddParameter("@initiatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintinitiatorunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason)
            If mdtvoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
            End If
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisprocessed)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid)
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverempid)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnissubmitted)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Initiatortranunkid(objDataOperation) = unkid

            Me._ClientIp = mstrClientIp
            Me._Loginemployeeunkid = mintloginemployeeunkid
            Me._AuditUserid = minAuditUserid
            Me._HostName = mstrHostName
            Me._FormName = mstrFormName
            Me._IsFromWeb = blnIsFromWeb

            If Insert_AtTranLog(objDataOperation, 2, unkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsProcessChange Then
                objGrievance_master._AuditUserId = minAuditUserid
                objGrievance_master._ClientIP = mstrClientIp
                objGrievance_master._HostName = mstrHostName
                objGrievance_master._LoginEmployeeUnkid = mintloginemployeeunkid
                objGrievance_master._FormName = mstrFormName
                objGrievance_master._FromWeb = blnIsFromWeb
                objGrievance_master._ApprovalSettingid = mintapprovalsettingid

                objGrievance_master.ChangeGrievanceStatus(mintgrievancemasterunkid, objDataOperation)
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    Public Function Insert_AtTranLog(ByVal objDoopration As clsDataOperation, ByVal Audittype As Integer, ByVal initiatortranunkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO atgreinitiater_response_tran ( " & _
             "  row_guid " & _
             ", initiatortranunkid " & _
             ", resolutionsteptranunkid " & _
             ", grievancemasterunkid " & _
             ", statusunkid " & _
             ", remark " & _
             ", initiatorunkid " & _
             ", isprocessed " & _
             ", approvalsettingid " & _
             ", apprlevelunkid " & _
             ", approverempid " & _
             ", issubmitted " & _
             ", audittype " & _
             ", auditdatetime " & _
             ", audituserunkid " & _
             ", loginemployeeunkid " & _
             ", ip " & _
             ", host " & _
             ", form_name " & _
             ", isweb " & _
           ") VALUES (" & _
             "  @row_guid " & _
             ", @initiatortranunkid " & _
             ", @resolutionsteptranunkid " & _
             ", @grievancemasterunkid " & _
             ", @statusunkid " & _
             ", @remark " & _
             ", @initiatorunkid " & _
             ", @isprocessed " & _
             ", @approvalsettingid " & _
             ", @apprlevelunkid " & _
             ", @approverempid " & _
             ", @issubmitted " & _
             ", @audittype " & _
             ", GETDATE() " & _
             ", @audituserunkid " & _
             ", @loginemployeeunkid " & _
             ", @ip " & _
             ", @host " & _
             ", @form_name " & _
             ", @isweb )"

            objDoopration.ClearParameters()
            objDoopration.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDoopration.AddParameter("@initiatortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, initiatortranunkid)
            objDoopration.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintresolutionsteptranunkid)
            objDoopration.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintgrievancemasterunkid)
            objDoopration.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintstatusunkid)
            objDoopration.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrremark)
            objDoopration.AddParameter("@initiatorunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mintinitiatortranunkid)
            objDoopration.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisprocessed)
            objDoopration.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)
            objDoopration.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid)
            objDoopration.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverempid)
            objDoopration.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnissubmitted)
            objDoopration.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, Audittype)
            objDoopration.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoopration.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDoopration.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDoopration.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoopration.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoopration.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoopration.ExecQuery(StrQ, "List")

            If objDoopration.ErrorMessage <> "" Then
                extForce = New Exception(objDoopration.ErrorNumber & ": " & objDoopration.ErrorMessage)
                Throw extForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function isExist(ByVal resolutionsteptranunkid As Integer, ByVal grievancemasterunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = "SELECT * from greinitiater_response_tran " & _
                  " WHERE resolutionsteptranunkid = @resolutionsteptranunkid " & _
                  " and grievancemasterunkid =@grievancemasterunkid  " & _
                  " and isvoid = 0"

            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, resolutionsteptranunkid)
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, grievancemasterunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function



    'Gajanan [13-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement


    Public Sub SendNotificationDisagreeGrievance(ByVal dtEmailFieldData As DataTable, _
                                                 ByVal strUserlist As String, _
                                                 ByVal strFormName As String, _
                                                 ByVal eMode As enLogin_Mode, _
                                                 ByVal strSenderAddress As String, _
                                                 ByVal strEmailTitle As String, _
                                                 ByVal mstrEmailContent As String, _
                                                 ByVal strGrievanceRefNo As String, _
                                                 ByVal intGrievanceId As Integer, _
                                                 ByVal intCompanyId As Integer, _
                                                 ByVal iLoginEmployeeId As Integer, _
                                                 ByVal mstrWebClientIP As String, _
                                                 ByVal mstrWebHostName As String)

        Dim dsList As New DataSet
        Try

            Dim objUserAddEdit As New clsUserAddEdit

            If strUserlist.Length > 0 Then
                Dim dsUserlist As DataSet = objUserAddEdit.GetList("List", "hrmsConfiguration..cfuser_master.userunkid in (" & strUserlist & ")")

                If IsNothing(dsUserlist) = False AndAlso dsUserlist.Tables(0).Rows.Count > 0 Then
                    Call ViewMergeData(dtEmailFieldData, mstrEmailContent)



                    For Each iRow As DataRow In dsUserlist.Tables(0).Rows


                        Dim mstrUsername As String = String.Empty
                        Dim mstrEmailContainer As String = String.Empty

                        mstrUsername = iRow("firstname").ToString() + " " + iRow("lastname").ToString()

                        If mstrUsername.Trim().Length <= 0 Then
                            mstrUsername = iRow("username").ToString()
                        End If

                        If mstrEmailContent.Contains("#UserName#") Then
                            mstrEmailContainer = mstrEmailContent.Replace("#UserName#", mstrUsername)

                            'Gajanan [19-July-2019] -- Start      
                        Else
                            mstrEmailContainer = mstrEmailContent
                            'Gajanan [19-July-2019] -- End
                        End If

                        If iRow("email").ToString().Length <= 0 Then Continue For

                      


                        Dim StrMessage As New System.Text.StringBuilder
                        Dim objMail As New clsSendMail

                        Dim htmlOutput = "Grievacne.html"
                        Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(mstrEmailContainer, contentUriPrefix)
                        objMail._ImageContents = htmlResult._Content
                        htmlResult.WriteToFile(htmlOutput)
                        Dim oHtml As String = htmlResult._HTML

                        objMail._Subject = strEmailTitle
                        objMail._Message = oHtml
                        objMail._ToEmail = iRow("email").ToString()
                        objMail._Form_Name = mstrFormName
                        objMail._ClientIP = mstrWebClientIP
                        objMail._HostName = mstrWebHostName
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = eMode
                        objMail._UserUnkid = 0
                        objMail._SenderAddress = strSenderAddress
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.GRIEVANCE_MGT
                        objMail.SendMail(intCompanyId)
                        objMail = Nothing
                    Next
                End If
            End If



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationDisagreeGrievance; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    Private Sub ViewMergeData(ByVal dtTable As DataTable, ByRef strEmailContent As String)
        Dim objLetterFields As New clsLetterFields
        Dim dslist As New DataSet
        Dim strGuestUnkid As String = ""

        Dim strDataName As String = ""
        Try
            Dim StrCol As String = ""
            For j As Integer = 0 To dtTable.Columns.Count - 1S
                StrCol = dtTable.Columns(j).ColumnName
                If strEmailContent.Contains("#" & StrCol & "#") Then

                    If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                       
                    Else
                        strDataName = strEmailContent.Replace("#" & StrCol & "#", dtTable.Rows(0)(StrCol).ToString)
                    End If

                    strEmailContent = strDataName
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub
    'Gajanan [13-July-2019] -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "My Response")
			Language.setMessage(mstrModuleName, 3, "Commitee Response")
			Language.setMessage(mstrModuleName, 4, "Level")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


