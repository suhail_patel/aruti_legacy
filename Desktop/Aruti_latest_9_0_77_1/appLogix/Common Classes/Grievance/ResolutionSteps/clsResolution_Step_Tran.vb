﻿Imports eZeeCommonLib
Imports System

Public Class clsResolution_Step_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsresolution_step_tran"
    Dim mstrMessage As String = ""
    Private mintEmployeeunkid As Integer

#Region " Private variables "
    Private mintResolutionsteptranunkid As Integer
    Private mintGrievancemasterunkid As Integer
    Private mintResponsetypeunkid As Integer
    Private mstrResponseremark As String = String.Empty
    Private mstrQualifyremark As String = String.Empty
    Private mdtMeetingdate As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintApprovermasterunkid As Integer
    Private mblnIsfinal As Boolean
    Private mblnIsprocessed As Boolean
    Private mstrFormName As String = String.Empty
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mintCompanyUnkid As Integer = 0
    Private mdtAuditDate As DateTime = Now
    Private xObjDataOpr As clsDataOperation = Nothing
    Private mintloginemployeeunkid As Integer = 0
    Private mintApprovalsettingid As Integer = 0
    Private mintApprlevelunkid As Integer = 0
    Private mintApproverempid As Integer = 0
    Private mblnIssubmitted As Boolean = False
#End Region

    Public Enum enResponseType
        My_Response = 1
        Commitee_Response = 2
    End Enum

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Set clsDataOperation
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _ObjDataOpr() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xObjDataOpr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ResolutionStepTranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ResolutionStepTranunkid() As Integer
        Get
            Return mintResolutionsteptranunkid
        End Get
        Set(ByVal value As Integer)
            mintResolutionsteptranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Grievancemaster
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Grievancemasterunkid() As Integer
        Get
            Return mintGrievancemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintGrievancemasterunkid = value
        End Set
    End Property

    Public Property _Responsetypeunkid() As Integer
        Get
            Return mintResponsetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintResponsetypeunkid = value
        End Set
    End Property

    Public Property _Responseremark() As String
        Get
            Return mstrResponseremark
        End Get
        Set(ByVal value As String)
            mstrResponseremark = value
        End Set
    End Property

    Public Property _Qualifyremark() As String
        Get
            Return mstrQualifyremark
        End Get
        Set(ByVal value As String)
            mstrQualifyremark = value
        End Set
    End Property

    Public Property _Meetingdate() As DateTime
        Get
            Return mdtMeetingdate
        End Get
        Set(ByVal value As DateTime)
            mdtMeetingdate = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Approvermasterunkid() As Integer
        Get
            Return mintApprovermasterunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovermasterunkid = value
        End Set
    End Property

    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set FormName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set LoginEmployeeId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Loginemployeeunkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Client IP
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Host Name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set From Web
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Audit User
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set CompanyId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Audit Date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Public Property _Approvalsettingid() As Integer
        Get
            Return mintApprovalsettingid
        End Get
        Set(ByVal value As Integer)
            mintApprovalsettingid = value
        End Set
    End Property

    Public Property _Apprlevelunkid() As Integer
        Get
            Return mintApprlevelunkid
        End Get
        Set(ByVal value As Integer)
            mintApprlevelunkid = value
        End Set
    End Property

    Public Property _Approverempid() As Integer
        Get
            Return mintApproverempid
        End Get
        Set(ByVal value As Integer)
            mintApproverempid = value
        End Set
    End Property

    Public Property _Issubmitted() As Boolean
        Get
            Return mblnIssubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmitted = value
        End Set
    End Property

#End Region

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  resolutionsteptranunkid " & _
              ", grievancemasterunkid " & _
              ", responsetypeunkid " & _
              ", responseremark " & _
              ", qualifyremark " & _
              ", meetingdate " & _
              ", approvermasterunkid " & _
              ", isfinal " & _
              ", isprocessed " & _
              ", approvalsettingid " & _
              ", apprlevelunkid " & _
              ", approverempid " & _
              ", issubmitted " & _
             "FROM greresolution_step_tran " & _
             "WHERE resolutionsteptranunkid = @resolutionsteptranunkid "

            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintResolutionsteptranunkid = CInt(dtRow.Item("resolutionsteptranunkid"))
                mintGrievancemasterunkid = CInt(dtRow.Item("grievancemasterunkid"))
                mintResponsetypeunkid = CInt(dtRow.Item("responsetypeunkid"))
                mstrResponseremark = dtRow.Item("responseremark").ToString
                mstrQualifyremark = dtRow.Item("qualifyremark").ToString
                If mintResponsetypeunkid = 2 Then
                    mdtMeetingdate = CDate(dtRow.Item("meetingdate"))
                End If
                'If mdtMeetingdate = Nothing Then
                '    mdtMeetingdate = Nothing
                'Else
                '    mdtMeetingdate = CDate(dtRow.Item("meetingdate"))
                'End If
                mintApprovermasterunkid = CInt(dtRow.Item("approvermasterunkid"))
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))

                mintApprovalsettingid = CInt(dtRow.Item("approvalsettingid"))
                mintApprlevelunkid = CInt(dtRow.Item("apprlevelunkid"))
                mintApproverempid = CInt(dtRow.Item("approverempid"))
                mblnIssubmitted = CBool(dtRow.Item("issubmitted"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, ByVal approvalsetting As enGrievanceApproval, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnAddSelect As Boolean = False, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
'Gajanan [24-OCT-2019] -- Start   
'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            Select Case approvalsetting
                Case enGrievanceApproval.ReportingTo

                    'strQ = "IF OBJECT_ID('tempdb..#GrievanceAgainstEmp') IS NOT NULL " & _
                    '       "DROP TABLE #GrievanceAgainstEmp " & _
                    '       "CREATE TABLE #GrievanceAgainstEmp ( " & _
                    '            "EmpIndex INT identity(1, 1) " & _
                    '            ",empid INT " & _
                    '            ") " & _
                    '       "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                    '            "DROP TABLE #Results " & _
                    '       "CREATE TABLE #Results ( " & _
                    '            "eid INT " & _
                    '            ",rempid INT " & _
                    '            ",empid INT " & _
                    '            ",priority INT " & _
                    '            ") " & _
                    '       " " & _
                    '       "DECLARE @REmpId AS INT " & _
                    '            ",@NxEmpId AS INT " & _
                    '            ",@Eid AS INT " & _
                    '            ",@priority AS INT " & _
                    '       "SET @REmpId = 0 " & _
                    '       "SET @NxEmpId = 0 " & _
                    '       "SET @eid = 0 " & _
                    '       "SET @priority = 0 " & _
                    '       "INSERT INTO #GrievanceAgainstEmp " & _
                    '       "SELECT againstemployeeunkid " & _
                    '       "FROM ( " & _
                    '            "SELECT againstemployeeunkid " & _
                    '                 ",ROW_NUMBER() OVER ( " & _
                    '                      "PARTITION BY againstemployeeunkid ORDER BY againstemployeeunkid " & _
                    '                      ") AS rno " & _
                    '            "FROM gregrievance_master " & _
                    '            "WHERE issubmitted = 1 " & _
                    '                 "AND isprocessed = 0 " & _
                    '                 "AND isvoid = 0 " & _
                    '            ") AS b " & _
                    '       "WHERE b.rno = 1 " & _
                    '       "DECLARE @TotalRows INT = ( " & _
                    '                 "SELECT COUNT(1) " & _
                    '                 "FROM #GrievanceAgainstEmp " & _
                    '                 ") " & _
                    '            ",@i INT = 1 " & _
                    '       "WHILE @i <= @TotalRows " & _
                    '       "BEGIN " & _
                    '            "SELECT @NxEmpId = empid " & _
                    '            "FROM #GrievanceAgainstEmp " & _
                    '            "WHERE EmpIndex = @i " & _
                    '            "SET @Eid = @NxEmpId " & _
                    '            "WHILE @NxEmpId > 0 " & _
                    '            "BEGIN " & _
                    '                 "SET @priority = @priority + 1 " & _
                    '                 "SET @REmpId = ISNULL(( " & _
                    '                                "SELECT reporttoemployeeunkid " & _
                    '                                "FROM hremployee_reportto " & _
                    '                                "WHERE employeeunkid = @NxEmpId " & _
                    '                                     "AND ishierarchy = 1 " & _
                    '                                     "AND isvoid = 0 " & _
                    '                                "), 0) " & _
                    '                 "IF (@NxEmpId = @REmpId) " & _
                    '                      "SET @REmpId = 0 " & _
                    '                 "INSERT INTO #Results ( " & _
                    '                      "eid " & _
                    '                      ",rempid " & _
                    '                      ",empid " & _
                    '                      ",priority " & _
                    '                      ") " & _
                    '                 "VALUES ( " & _
                    '                      "@NxEmpId " & _
                    '                      ",@REmpId " & _
                    '                      ",@Eid " & _
                    '                      ",@priority " & _
                    '                      ") " & _
                    '                 "IF (@REmpId = 0) " & _
                    '                 "BEGIN " & _
                    '                      "SET @i = @i + 1 " & _
                    '                      "SET @priority = 0 " & _
                    '                      "SELECT @NxEmpId = empid " & _
                    '                      "FROM #GrievanceAgainstEmp " & _
                    '                      "WHERE EmpIndex = @i " & _
                    '                      "BREAK; " & _
                    '                 "END " & _
                    '                 "ELSE " & _
                    '                      "SET @NxEmpId = @REmpId " & _
                    '                 "DECLARE @Rcnt AS INT " & _
                    '                 "SET @Rcnt = ( " & _
                    '                           "SELECT COUNT(1) " & _
                    '                           "FROM #Results " & _
                    '                           "WHERE rempid = @NxEmpId " & _
                    '                                "AND #Results.empid = @Eid " & _
                    '                           ") " & _
                    '                 "IF (@Rcnt > 1) " & _
                    '                 "BEGIN " & _
                    '                      "SET @i = @i + 1 " & _
                    '                      "SELECT @NxEmpId = empid " & _
                    '                      "FROM #GrievanceAgainstEmp " & _
                    '                      "WHERE EmpIndex = @i " & _
                    '                      "SET @priority = 0 " & _
                    '                      "BREAK; " & _
                    '                 "END " & _
                    '                 "ELSE " & _
                    '                 "CONTINUE " & _
                    '                 "END " & _
                    '                 "END "

                    Dim objgrievanceMst As New clsgrievanceapprover_master
                    strQ = objgrievanceMst.getGrievanceReportingToQuery(False, True)
                    objgrievanceMst = Nothing

                    If mblnAddSelect = True Then
                        strQ &= "SELECT " & _
                               "0 AS grievancemasterunkid " & _
                               ",0 AS grievancetype " & _
                               ",'' AS RaisedByEmp " & _
                               ",'' AS againstEmployee " & _
                               ",NULL AS grievancedate " & _
                               ",'' AS responseremark " & _
                               ",0 AS responsetypeunkid " & _
                               ",0 AS responsetype " & _
                               ",0 AS resolutionsteptranunkid " & _
                               ",null as meetingdate " & _
                               ",'' AS members " & _
                               ",'' AS grievancerefno " & _
                               ",0 AS issubmitted " & _
                               ",0 AS priority " & _
                               ",0 AS approverempid " & _
                               ",0 as mapuserunkid " & _
                               ",'' as approvername " & _
                               " UNION ALL "
                    End If

                    strQ &= "SELECT " & _
                   " gregrievance_master.grievancemasterunkid AS grievancemasterunkid " & _
                   " ,cfcommon_master.name as grievancetype " & _
                   " ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS RaisedByEmp " & _
                   " ,ISNULL(againstEmp.firstname, '') + ' ' + ISNULL(againstEmp.othername, '') + ' ' + ISNULL(againstEmp.surname, '') AS againstEmployee " & _
                   " ,CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
                   " ,greresolution_step_tran.responseremark " & _
                   " ,greresolution_step_tran.responsetypeunkid " & _
                   " ,CASE greresolution_step_tran.responsetypeunkid " & _
                   " WHEN " & enResponseType.My_Response & " THEN @MyResponse " & _
                   " WHEN " & enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
                   " END as responsetype " & _
                   " ,greresolution_step_tran.resolutionsteptranunkid " & _
                   " ,CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
                   " ,ISNULL(C.EmployeeIds, '') AS members " & _
                   " ,gregrievance_master.grievancerefno as grievancerefno " & _
                   " ,greresolution_step_tran.issubmitted " & _
                   ",#Results.priority " & _
                   ",greresolution_step_tran.approverempid " & _
                   ",#Results.rempid as mapuserunkid " & _
                   ",ISNULL(aemp.firstname, '') + ' ' + ISNULL(aemp.surname, '') AS approvername "

                    strQ += " FROM greresolution_step_tran " & _
                         " LEFT JOIN gregrievance_master " & _
                         " ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid " & _
                         " LEFT JOIN hremployee_master " & _
                         " ON hremployee_master.employeeunkid = gregrievance_master.fromemployeeunkid " & _
                         " LEFT JOIN hremployee_master AS againstEmp " & _
                         " ON againstEmp.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                         " LEFT JOIN cfcommon_master " & _
                         " ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
                         " LEFT JOIN #Results " & _
                         " ON #Results.rempid = greresolution_step_tran.approvermasterunkid AND gregrievance_master.grievancemasterunkid = #Results.grievanceId " & _
                         " LEFT JOIN hremployee_master as aemp " & _
                         " ON aemp.employeeunkid = #Results.rempid "


                    strQ += " LEFT JOIN (SELECT " & _
                            " a.resolutionsteptranunkid " & _
                            " ,ISNULL(STUFF((SELECT " & _
                            " ',' + employeecode + ' - ' + firstname + ' ' + surname " & _
                            " FROM greresolution_meeting_tran " & _
                            " LEFT JOIN hremployee_master " & _
                            " ON hremployee_master.employeeunkid = greresolution_meeting_tran.employeeunkid " & _
                            " LEFT JOIN greresolution_step_tran AS b " & _
                            " ON b.resolutionsteptranunkid = greresolution_meeting_tran.resolutionsteptranunkid " & _
                            " WHERE greresolution_meeting_tran.isvoid = 0 " & _
                            " AND a.resolutionsteptranunkid = b.resolutionsteptranunkid " & _
                            " ORDER BY greresolution_meeting_tran.employeeunkid " & _
                            " FOR XML PATH ('')) " & _
                            " , 1, 1, ''), '') AS EmployeeIds " & _
                            " FROM greresolution_step_tran AS a " & _
                            " WHERE a.isvoid = 0 " & _
                            " GROUP BY a.resolutionsteptranunkid) AS C " & _
                            " ON c.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
                            " WHERE greresolution_step_tran.isvoid = 0 and greresolution_step_tran.isprocessed =0 and #Results.rempid > 0 " & _
                            " and gregrievance_master.approvalsettingid =@approvalsettingid "


                    If mstrFilter.Trim.Length > 0 Then
                        strQ &= mstrFilter
                    End If

                    strQ &= " ORDER by grievancerefno " & _
                            " DROP TABLE #GrievanceAgainstEmp " & _
                            " DROP TABLE #Results "
                Case Else

            If mblnAddSelect = True Then
                strQ = "SELECT " & _
                       "0 AS grievancemasterunkid " & _
                       ",0 AS grievancetype " & _
                       ",'' AS RaisedByEmp " & _
                       ",'' AS againstEmployee " & _
                       ",NULL AS grievancedate " & _
                       ",'' AS responseremark " & _
                       ",0 AS responsetypeunkid " & _
                       ",0 AS responsetype " & _
                       ",0 AS resolutionsteptranunkid " & _
                       ",null as meetingdate " & _
                       ",'' AS members " & _
                       ",'' AS grievancerefno " & _
                       ",0 AS issubmitted " & _
                       ",0 AS priority " & _
                       ",0 AS approverempid " & _
                       ",0 as mapuserunkid " & _
                       ",'' as approvername " & _
                       " UNION ALL "
            End If

            strQ += "SELECT " & _
                   " gregrievance_master.grievancemasterunkid as grievancemasterunkid " & _
                   " ,cfcommon_master.name as grievancetype " & _
                   " ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS RaisedByEmp " & _
                   " ,ISNULL(againstEmp.firstname, '') + ' ' + ISNULL(againstEmp.othername, '') + ' ' + ISNULL(againstEmp.surname, '') AS againstEmployee " & _
                   " ,CONVERT(CHAR(8), gregrievance_master.grievancedate, 112) AS grievancedate " & _
                   " ,greresolution_step_tran.responseremark " & _
                   " ,greresolution_step_tran.responsetypeunkid " & _
                   " ,CASE greresolution_step_tran.responsetypeunkid " & _
                   " WHEN " & enResponseType.My_Response & " THEN @MyResponse " & _
                   " WHEN " & enResponseType.Commitee_Response & " THEN @CommiteeResponse " & _
                   " END as responsetype " & _
                   " ,greresolution_step_tran.resolutionsteptranunkid " & _
                   " ,CONVERT(CHAR(8), greresolution_step_tran.meetingdate, 112) AS meetingdate " & _
                   " ,ISNULL(C.EmployeeIds, '') AS members " & _
                   " ,gregrievance_master.grievancerefno as grievancerefno " & _
                   " ,greresolution_step_tran.issubmitted " & _
                   ",greapproverlevel_master.priority " & _
                   ",greresolution_step_tran.approverempid " & _
                   ",greapprover_master.mapuserunkid "

            If approvalsetting = enGrievanceApproval.UserAcess Then
                strQ += ",CASE WHEN  ISNULL(guser.firstname,'') + ' '+ ISNULL(guser.lastname,'') = '' THEN " & _
                        "    ISNULL(guser.username,'') " & _
                        "ELSE " & _
                        "    ISNULL(guser.firstname,'') + ' '+ ISNULL(guser.lastname,'') " & _
                        "END as approvername "
            ElseIf approvalsetting = enGrievanceApproval.ApproverEmpMapping Then
                strQ += " ,ISNULL(aemp.firstname, '') + ' ' + ISNULL(aemp.surname, '') AS approvername "
            End If

            strQ += " FROM greresolution_step_tran " & _
                 " LEFT JOIN gregrievance_master " & _
                 " ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid " & _
                 " LEFT JOIN greapproverlevel_master " & _
                 " ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                 " LEFT JOIN hremployee_master " & _
                 " ON hremployee_master.employeeunkid = gregrievance_master.fromemployeeunkid " & _
                 " LEFT JOIN hremployee_master AS againstEmp " & _
                 " ON againstEmp.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                 " LEFT JOIN cfcommon_master " & _
                 " ON cfcommon_master.masterunkid = gregrievance_master.grievancetypeid " & _
                 " LEFT JOIN greapprover_master " & _
                 " ON greresolution_step_tran.approvermasterunkid = greapprover_master.approvermasterunkid "
            If approvalsetting = enGrievanceApproval.UserAcess Then
                strQ += "LEFT JOIN hrmsConfiguration..cfuser_master AS guser " & _
                        "ON greapprover_master.mapuserunkid = guser.userunkid "

               
            ElseIf approvalsetting = enGrievanceApproval.ApproverEmpMapping Then
                strQ += "LEFT JOIN hremployee_master as aemp " & _
                       "ON aemp.employeeunkid = greapprover_master.approverempid "
            End If

            strQ += " LEFT JOIN (SELECT " & _
                    " a.resolutionsteptranunkid " & _
                    " ,ISNULL(STUFF((SELECT " & _
                    " ',' + employeecode + ' - ' + firstname + ' ' + surname " & _
                    " FROM greresolution_meeting_tran " & _
                    " LEFT JOIN hremployee_master " & _
                    " ON hremployee_master.employeeunkid = greresolution_meeting_tran.employeeunkid " & _
                    " LEFT JOIN greresolution_step_tran AS b " & _
                    " ON b.resolutionsteptranunkid = greresolution_meeting_tran.resolutionsteptranunkid " & _
                    " WHERE greresolution_meeting_tran.isvoid = 0 " & _
                    " AND a.resolutionsteptranunkid = b.resolutionsteptranunkid " & _
                    " ORDER BY greresolution_meeting_tran.employeeunkid " & _
                    " FOR XML PATH ('')) " & _
                    " , 1, 1, ''), '') AS EmployeeIds " & _
                    " FROM greresolution_step_tran AS a " & _
                    " WHERE a.isvoid = 0 " & _
                    " GROUP BY a.resolutionsteptranunkid) AS C " & _
                    " ON c.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid "
            strQ &= " WHERE greresolution_step_tran.isvoid = 0 and greresolution_step_tran.isprocessed =0 "

            '" LEFT JOIN greapprover_master " & _
            '" ON greresolution_step_tran.approverempid = greapprover_master.approverempid " & _
            strQ &= " and greapprover_master.approvalsettingid =@approvalsettingid "


            If mstrFilter.Trim.Length > 0 Then
                strQ &= mstrFilter
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            strQ &= " ORDER by grievancerefno "
            'Gajanan [4-July-2019] -- End

            End Select
			'Gajanan [24-OCT-2019] -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, approvalsetting)
            objDataOperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "My Response"))
            objDataOperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function Insert(ByVal dtCommitte As DataTable) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancemasterunkid.ToString)
            objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResponsetypeunkid.ToString)
            objDataOperation.AddParameter("@responseremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResponseremark.ToString)
            objDataOperation.AddParameter("@qualifyremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrQualifyremark.ToString)
            If mdtMeetingdate = Nothing Then
                objDataOperation.AddParameter("@meetingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@meetingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtMeetingdate)
            End If
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.NAME_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermasterunkid.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)

            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalsettingid.ToString)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprlevelunkid.ToString)
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverempid.ToString)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitted.ToString)

            strQ = "INSERT INTO greresolution_step_tran ( " & _
                    " grievancemasterunkid " & _
                    ",responsetypeunkid " & _
                    ",responseremark " & _
                    ",qualifyremark " & _
                    ",meetingdate " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voidreason " & _
                    ",voiddatetime " & _
                    ",approvermasterunkid " & _
                    ",isfinal " & _
                    ",isprocessed " & _
                    ",approvalsettingid " & _
                    ",apprlevelunkid " & _
                    ",approverempid " & _
                    ",issubmitted " & _
                ") VALUES (" & _
                   "  @grievancemasterunkid " & _
                   ", @responsetypeunkid " & _
                   ", @responseremark " & _
                   ", @qualifyremark " & _
                   ", @meetingdate " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voidreason " & _
                   ", @voiddatetime " & _
                   ", @approvermasterunkid " & _
                   ", @isfinal " & _
                   ", @isprocessed " & _
                   ", @approvalsettingid " & _
                   ", @apprlevelunkid " & _
                   ", @approverempid " & _
                   ", @issubmitted " & _
                "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._ResolutionStepTranunkid = dsList.Tables(0).Rows(0).Item(0)



            If InsertAuditTrailForResolutionStep(objDataOperation, 1) = False Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            If dtCommitte IsNot Nothing Then
                If InsertDeleteCommitteeDataByEmployeeId(dtCommitte, objDataOperation) = False Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If
            End If

            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            If xObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal dtCommitte As DataTable) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancemasterunkid.ToString)
            objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResponsetypeunkid.ToString)
            objDataOperation.AddParameter("@responseremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResponseremark.ToString)
            objDataOperation.AddParameter("@qualifyremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrQualifyremark.ToString)
            If mdtMeetingdate = Nothing Then
                objDataOperation.AddParameter("@meetingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@meetingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtMeetingdate)
            End If
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermasterunkid.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)

            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalsettingid.ToString)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprlevelunkid.ToString)
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverempid.ToString)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitted.ToString)

            strQ = "UPDATE greresolution_step_tran SET " & _
                         "grievancemasterunkid =  @grievancemasterunkid " & _
                         ",responsetypeunkid =  @responsetypeunkid " & _
                         ",responseremark =  @responseremark " & _
                         ",qualifyremark =  @qualifyremark " & _
                         ",meetingdate =  @meetingdate " & _
                         ",isvoid =  @isvoid " & _
                         ",voiduserunkid =  @voiduserunkid " & _
                         ",voidreason =  @voidreason " & _
                         ",voiddatetime =  @voiddatetime " & _
                         ",approvermasterunkid =  @approvermasterunkid " & _
                         ",isfinal =  @isfinal " & _
                         ",isprocessed =  @isprocessed " & _
                         ",approvalsettingid = @approvalsettingid " & _
                         ",apprlevelunkid = @apprlevelunkid " & _
                         ",approverempid = @approverempid " & _
                         ",issubmitted = @issubmitted " & _
                    " WHERE resolutionsteptranunkid =  @resolutionsteptranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _ObjDataOpr = objDataOperation
            _ResolutionStepTranunkid = mintResolutionsteptranunkid

            If InsertAuditTrailForResolutionStep(objDataOperation, 2) = False Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dtCommitte IsNot Nothing Then
                If InsertDeleteCommitteeDataByEmployeeId(dtCommitte, objDataOperation) = False Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDatetime As DateTime, ByVal strVoidReason As String, ByVal strDatabaseName As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = " UPDATE greresolution_step_tran SET " & _
                   "  isvoid = 1 " & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voidreason = @voidreason " & _
                   " WHERE resolutionsteptranunkid = @resolutionsteptranunkid "

            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If dtVoidDatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._ObjDataOpr = objDataOperation
            Me._ResolutionStepTranunkid = intUnkid

            If InsertAuditTrailForResolutionStep(objDataOperation, 3) = False Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If DeleteCommitteeData(objDataOperation, intUnkid, intVoidUserID, dtVoidDatetime, strVoidReason, strDatabaseName) = False Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteCommitteeData(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDatetime As DateTime, ByVal strVoidReason As String, ByVal strDatabaseName As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            strQ = " UPDATE greresolution_meeting_tran SET " & _
                   "  isvoid = 1" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voidreason = @voidreason " & _
                   " WHERE resolutionsteptranunkid = @resolutionsteptranunkid " & _
                   " AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            If dtVoidDatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function InsertDeleteCommitteeDataByEmployeeId(ByVal dtCommitte As DataTable, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim exForce As Exception = Nothing
        Try
            For Each row As DataRow In dtCommitte.Rows
                Select Case row("AUD").ToString().ToUpper()
                    Case "A"
                        If InsertCommitteeData(row("employeeunkid").ToString(), objDataOperation, enAuditType.ADD) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Case "D"
                        If DeleteCommitteeDataByEmployeeId(row("employeeunkid").ToString(), objDataOperation, enAuditType.DELETE) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                End Select
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDeleteCommitteeDataByEmployeeId; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function DeleteCommitteeDataByEmployeeId(ByVal mstrEmployeeId As String, ByVal objDataOperation As clsDataOperation, ByVal eAudit As enAuditType) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim arEmpId() As String = mstrEmployeeId.Trim.Split(",")
            For i As Integer = 0 To arEmpId.Length - 1
                mintEmployeeunkid = CInt(arEmpId(i))
                Dim intTranId As Integer = 0
                strQ = "SELECT resolutionmeetingtranunkid FROM greresolution_meeting_tran WHERE resolutionsteptranunkid = @resolutionsteptranunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeId.ToString)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                intTranId = CInt(dsList.Tables(0).Rows(0).Item(0))

                strQ = "UPDATE greresolution_meeting_tran SET " & _
                       "     isvoid = 1 " & _
                       "    ,voiduserunkid = @voiduserunkid " & _
                       "    ,voiddatetime = @voiddatetime " & _
                       "    ,voidreason = @voidreason " & _
                       "WHERE resolutionsteptranunkid = @resolutionsteptranunkid " & _
                       "AND isvoid = 0 AND employeeunkid = @employeeunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeId.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, System.DateTime.Now)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "INSERT INTO atgreresolution_meeting_tran (" & _
                       "     row_guid " & _
                       "    ,resolutionmeetingtranunkid " & _
                       "    ,resolutionsteptranunkid " & _
                       "    ,employeeunkid " & _
                       "    ,audittype " & _
                       "    ,audituserunkid " & _
                       "    ,auditdatetime " & _
                       "    ,loginemployeeunkid " & _
                       "    ,ip " & _
                       "    ,host " & _
                       "    ,form_name " & _
                       "    ,isweb " & _
                       ") " & _
                       "VALUES ( " & _
                       "     @row_guid " & _
                       "    ,@resolutionmeetingtranunkid " & _
                       "    ,@resolutionsteptranunkid " & _
                       "    ,@employeeunkid " & _
                       "    ,@audittype " & _
                       "    ,@audituserunkid " & _
                       "    ,@auditdatetime " & _
                       "    ,@loginemployeeunkid " & _
                       "    ,@ip " & _
                       "    ,@host " & _
                       "    ,@form_name " & _
                       "    ,@isweb " & _
                       ") "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeId.ToString)
                objDataOperation.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
                objDataOperation.AddParameter("@resolutionmeetingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranId)
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.NAME_SIZE, eAudit)
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
                objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
                objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteCommitteeDataByEmployeeId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function InsertAuditTrailForResolutionStep(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
            objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrievancemasterunkid.ToString)
            objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResponsetypeunkid.ToString)
            objDataOperation.AddParameter("@responseremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResponseremark.ToString)
            objDataOperation.AddParameter("@qualifyremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrQualifyremark.ToString)
            If mdtMeetingdate = Nothing Then
                objDataOperation.AddParameter("@meetingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@meetingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtMeetingdate)
            End If
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermasterunkid.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalsettingid.ToString)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprlevelunkid.ToString)
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverempid.ToString)
            objDataOperation.AddParameter("@issubmitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitted.ToString)

            strQ = "INSERT INTO atgreresolution_step_tran ( " & _
                   "     row_guid " & _
                   "    ,resolutionsteptranunkid" & _
                   "    ,grievancemasterunkid " & _
                   "    ,responsetypeunkid " & _
                   "    ,responseremark " & _
                   "    ,qualifyremark " & _
                   "    ,meetingdate " & _
                   "    ,approvermasterunkid " & _
                   "    ,isfinal " & _
                   "    ,isprocessed " & _
                   "    ,audittype " & _
                   "    ,audituserunkid " & _
                   "    ,auditdatetime " & _
                   "    ,loginemployeeunkid " & _
                   "    ,ip " & _
                   "    ,host " & _
                   "    ,form_name " & _
                   "    ,isweb " & _
                   "    ,approvalsettingid " & _
                   "    ,apprlevelunkid " & _
                   "    ,approverempid " & _
                   "    ,issubmitted " & _
                   ") VALUES (" & _
                   "     @row_guid " & _
                   "    ,@resolutionsteptranunkid" & _
                   "    ,@grievancemasterunkid " & _
                   "    ,@responsetypeunkid " & _
                   "    ,@responseremark " & _
                   "    ,@qualifyremark " & _
                   "    ,@meetingdate " & _
                   "    ,@approvermasterunkid " & _
                   "    ,@isfinal " & _
                   "    ,@isprocessed " & _
                   "    ,@audittype " & _
                   "    ,@audituserunkid " & _
                   "    ,GETDATE() " & _
                   "    ,@loginemployeeunkid " & _
                   "    ,@ip " & _
                   "    ,@host " & _
                   "    ,@form_name " & _
                   "    ,@isweb " & _
                   "    ,@approvalsettingid " & _
                   "    ,@apprlevelunkid " & _
                   "    ,@approverempid " & _
                   "    ,@issubmitted " & _
                   ") "

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForResolutionStep; Module Name: " & mstrModuleName)
        Finally

        End Try
    End Function

    Public Function setIsfinal(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "Update greresolution_step_tran set isfinal = 1 where resolutionsteptranunkid = @resolutionsteptranunkid"

            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setIsfinal; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function



    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes




    'Public Function isExist(Optional ByVal intGrievancemasterunkid As Integer = -1, Optional ByVal intResponsetypeunkid As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As clsDataOperation
    '    If xObjDataOpr IsNot Nothing Then
    '        objDataOperation = xObjDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()

    '    Try
    '        strQ = "SELECT " & _
    '                "resolutionsteptranunkid " & _
    '                ",grievancemasterunkid " & _
    '                ",responsetypeunkid " & _
    '                ",responseremark " & _
    '                ",qualifyremark " & _
    '                ",meetingdate " & _
    '                ",isvoid " & _
    '                ",voiduserunkid " & _
    '                ",voidreason " & _
    '                ",voiddatetime " & _
    '                ",approvermasterunkid " & _
    '                ",isfinal " & _
    '                ",isprocessed " & _
    '            "FROM greresolution_step_tran " & _
    '            "WHERE  1=1"

    '        strQ &= " AND isactive = 1 "


    '        If intGrievancemasterunkid > 0 Then
    '            strQ &= " AND grievancemasterunkid = @grievancemasterunkid"
    '        End If

    '        If intResponsetypeunkid > 0 Then
    '            strQ &= " AND responsetypeunkid = @responsetypeunkid "
    '        End If

    '        If intUnkid > 0 Then
    '            strQ &= " AND resolutionsteptranunkid <> @resolutionsteptranunkid"
    '        End If

    '        objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intGrievancemasterunkid)
    '        objDataOperation.AddParameter("@responsetypeunkid", SqlDbType.Int, eZeeDataType.NAME_SIZE, intResponsetypeunkid)
    '        objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        If xObjDataOpr Is Nothing Then objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function isExist(ByVal intGrievancemasterunkid As Integer, ByVal intApprovermasterunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                    "resolutionsteptranunkid " & _
                    ",grievancemasterunkid " & _
                    ",responsetypeunkid " & _
                    ",responseremark " & _
                    ",qualifyremark " & _
                    ",meetingdate " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voidreason " & _
                    ",voiddatetime " & _
                    ",approvermasterunkid " & _
                    ",isfinal " & _
                    ",isprocessed " & _
                "FROM greresolution_step_tran " & _
                "WHERE  1=1"

            strQ &= " AND isvoid = 0 and issubmitted = 0 "


            If intGrievancemasterunkid > 0 Then
                strQ &= " AND grievancemasterunkid = @grievancemasterunkid"
                objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intGrievancemasterunkid)
            End If

            If intApprovermasterunkid > 0 Then
                strQ &= " AND approvermasterunkid = @approvermasterunkid "
                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.NAME_SIZE, intApprovermasterunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

        Return True
    End Function

    'Gajanan [27-June-2019] -- End

    Public Function IsValidApprover(ByVal intGrievanceMstId As Integer, ByVal intPriorityId As Integer, ByVal intApproverSettingId As Integer, ByVal Database_Name As String, _
                                     ByVal UserId As Integer, Optional ByVal Fin_year As Integer = 0, Optional ByVal CompanyUnkId As Integer = 0, _
                                    Optional ByVal UserAccessModeSetting As String = "", Optional ByVal EmployeeAsOnDate As String = "", Optional ByVal Employeeunkid As Integer = 0) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim strMsg As String = ""
        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim iCnt As Integer = -1
        Dim dsList As New DataSet
        Dim objLevel As New clsGrievanceApproverLevel
        Dim blnFlag As Boolean = False
        Try
            'dsList = objLevel.getListForCombo(intApproverSettingId, "List", False, objDataOperation)

            Dim strCurrUsr, strActiveUsr As String
            strCurrUsr = "" : strActiveUsr = ""
            If intApproverSettingId = enGrievanceApproval.UserAcess Then
                StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(mapuserunkid AS NVARCHAR(10)) FROM greapprover_master WHERE approvalsettingid = " & intApproverSettingId & " AND isactive = 1 FOR XML PATH('')), 1, 1, ''),'0') AS ivalue "
                dsList = objDataOperation.ExecQuery(StrQ, "List")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables("List").Rows.Count > 0 Then
                    strCurrUsr = dsList.Tables("List").Rows(0)("ivalue")
                    Dim objConfig As New clsConfigOptions
                    objConfig.GetActiveUserList_CSV(strCurrUsr, strActiveUsr)
                    objConfig = Nothing
                End If
            End If

            Dim xDateJoinQry, xDataFilterQry As String
            xDateJoinQry = "" : xDataFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, eZeeDate.convertDate(EmployeeAsOnDate), eZeeDate.convertDate(EmployeeAsOnDate), True, False, Database_Name, "EM")



            If intApproverSettingId = enGrievanceApproval.ApproverEmpMapping Then
                StrQ = "SELECT " & _
                  "    GL.priority " & _
                  "FROM greapproverlevel_master AS GL " & _
                  "    JOIN greapprover_master GAM ON GAM.apprlevelunkid = GL.apprlevelunkid " & _
                  "    JOIN gregrievance_master AS GGM  ON GAM.approverempid <> GGM.againstemployeeunkid AND GGM.isvoid = 0 AND GAM.approvalsettingid = GGM.approvalSettingid " & _
                  "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GL.isactive = 1 " & _
                  "AND GAM.isexternal = 0 AND GGM.grievancemasterunkid = '" & intGrievanceMstId & "' "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim iPriority As List(Of Integer) = Nothing

                If dsList.Tables("List").Rows.Count > 0 Then
                    iPriority = dsList.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                End If

                Dim intSecondLastPriority As Integer = -1
                If iPriority.Min() = intPriorityId Then
                    intSecondLastPriority = intPriorityId
                    blnFlag = True
                Else
                    Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intPriorityId
                        blnFlag = True
                    End Try
                End If

                StrQ = "SELECT " & _
                       "    priority " & _
                       "FROM greresolution_step_tran " & _
                       "    JOIN greapproverlevel_master ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                       "WHERE isvoid = 0 AND isprocessed = 0 AND priority = @priority  AND greresolution_step_tran.approvalsettingid = @approvalsettingid " & _
                       "AND grievancemasterunkid = @grievancemasterunkid ORDER BY priority ASC "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceMstId)
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intSecondLastPriority)
                objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverSettingId)

                iCnt = objDataOperation.RecordCount(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iCnt <= 0 AndAlso blnFlag = False Then
                    strMsg = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot add resolution to the selected grievance. Reason, Lower level approver has not given his resolution description.")
                End If

                If iCnt > 0 Then

                    '************** THIS BLOCK IMPLIES THAT IF LAST LEVEL DATA IS PRESENT AND WE CHECK THIS FOR CURRENT LEVEL PRIORITY FOR DATA PRESENSE
                    iCnt = -1

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceMstId)
                    objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityId)
                    objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverSettingId)

                    iCnt = objDataOperation.RecordCount(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If iCnt > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add resolution to the selected grievance. Reason, Another approver with same level has already given the resolution description.")
                    End If

                    '************** THIS BLOCK IMPLIES THAT IF LAST LEVEL DATA IS PRESENT AND WE CHECK THIS FOR CURRENT LEVEL PRIORITY FOR DATA PRESENSE

                End If

            ElseIf intApproverSettingId = enGrievanceApproval.UserAcess Then

                Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(EmployeeAsOnDate).Date, True, Database_Name, UserId, CompanyUnkId, Fin_year, UserAccessModeSetting, "EM")

                'If strActiveUsr.Trim.Length > 0 Then
                '    StrQ = "DECLARE @table as table (empid int, coid int, usrid int) " & _
                '            "INSERT INTO @table(empid,coid,usrid) " & _
                '            "SELECT " & _
                '            "     UM.employeeunkid " & _
                '            "    ,UM.companyunkid " & _
                '            "    ,GAM.mapuserunkid " & _
                '            "FROM greapprover_master AS GAM " & _
                '            " JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = GAM.mapuserunkid " & _
                '            "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GAM.approvalsettingid = 1 " & _
                '            "AND GAM.mapuserunkid IN (" & strActiveUsr & ") "
                'End If


                'StrQ &= "SELECT " & _
                '        "   EM.employeecode AS employeecode " & _
                '        ",  ISNULL(EM.firstname,'')+' '+ISNULL(EM.othername,'')+' '+ISNULL(EM.surname,'') AS employeename " & _
                '        ",  EM.employeeunkid As employeeunkid " & _
                '        ",  ISNULL(EM.employeecode,'') + ' - ' + ISNULL(EM.firstname,'') + ' ' + ISNULL(EM.othername,'') + ' ' + ISNULL(EM.surname,'') AS EmpCodeName "
                'If strActiveUsr.Trim.Length > 0 Then
                '    StrQ &= ", CASE WHEN ([@table].empid = GGM.againstemployeeunkid AND [@table].coid = EM.companyunkid) THEN 0 ELSE 1 END AS visible "

                'Else
                '    StrQ &= ",  1 AS visible "
                'End If

                'StrQ &= ", GGM.grievancemasterunkid " & _
                '        "FROM hremployee_master AS EM " & _
                '        "  JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.fromemployeeunkid "
                'If strActiveUsr.Trim.Length > 0 Then
                '    StrQ &= " LEFT JOIN @table ON empid = GGM.againstemployeeunkid AND [@table].usrid  = '" & UserId & "' "
                'End If
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If


                'StrQ &= "WHERE 1 = 1 AND GGM.isvoid = 0 AND GGM.approvalSettingid = @approvalSettingid "
                'If strActiveUsr.Trim.Length > 0 Then
                '    StrQ &= " AND (CASE WHEN ([@table].empid = GGM.againstemployeeunkid AND [@table].coid = EM.companyunkid) THEN 0 ELSE 1 END) = 1 "
                'Else
                '    StrQ &= " AND visible = 1 "
                'End If


                'If xDataFilterQry.Trim.Length > 0 Then
                '    StrQ &= xDataFilterQry
                'End If

                'objDataOperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverSettingId)

                'dsList = objDataOperation.ExecQuery(StrQ, "List")

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If


                StrQ = "SELECT DISTINCT " & _
                        "   P.priority " & _
                        "FROM greapproverlevel_master AS GL " & _
                        "JOIN greapprover_master GAM ON GAM.apprlevelunkid = GL.apprlevelunkid " & _
                        "JOIN gregrievance_master GGM ON GAM.approvalsettingid = GAM.approvalSettingid AND GGM.isvoid = 0 " & _
                        "JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         UM.employeeunkid " & _
                        "        ,UM.companyunkid " & _
                        "        ,GAM.mapuserunkid " & _
                        "        ,GGL.priority " & _
                        "    FROM greapprover_master AS GAM " & _
                        "       JOIN greapproverlevel_master AS GGL ON GAM.apprlevelunkid = GGL.apprlevelunkid " & _
                        "       JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = GAM.mapuserunkid " & _
                        "    WHERE GAM.isactive = 1 " & _
                        "       AND GAM.isvoid = 0 " & _
                        "       AND GAM.approvalsettingid = 1 "
                If strActiveUsr.Trim.Length > 0 Then
                    StrQ &= "       AND GAM.mapuserunkid IN (" & strActiveUsr & ") "
                Else
                    StrQ &= "       AND GAM.mapuserunkid IN (" & UserId & ") "
                End If
                StrQ &= ") AS P ON P.employeeunkid <> GGM.againstemployeeunkid " & _
                        "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GL.isactive = 1 " & _
                        "AND GGM.grievancemasterunkid = '" & intGrievanceMstId & "' "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim strPriority As String = ""
                strPriority = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority").ToString()).ToArray())

                Dim objAppr As New clsgrievanceapprover_master
                dsList = objAppr.GetApproversList(CType(intApproverSettingId, enGrievanceApproval), Database_Name, 0, Fin_year, CompanyUnkId, 1202, UserAccessModeSetting, True, True, True, EmployeeAsOnDate, False, Nothing, Employeeunkid)

                If strPriority.Trim.Length > 0 Then
                    Dim dt As DataTable = New DataView(dsList.Tables(0), "priority IN (" & strPriority & ")", "priority", DataViewRowState.CurrentRows).ToTable().Copy()
                    dsList.Tables.RemoveAt(0)
                    dsList.Tables.Add(dt.Copy())
                End If

                Dim iPriority As List(Of Integer) = Nothing

                If dsList.Tables("List").Rows.Count > 0 Then
                    iPriority = dsList.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                End If


                Dim intSecondLastPriority As Integer = -1
                If iPriority.Min() = intPriorityId Then
                    intSecondLastPriority = intPriorityId
                    blnFlag = True
                Else
                    Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intPriorityId
                        blnFlag = True
                    End Try
                End If

                StrQ = "SELECT " & _
                      "    priority " & _
                      "FROM greresolution_step_tran " & _
                      "    JOIN greapproverlevel_master ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                      "WHERE isvoid = 0 AND isprocessed = 0 AND priority = @priority  AND greresolution_step_tran.approvalsettingid = @approvalsettingid " & _
                      "AND grievancemasterunkid = @grievancemasterunkid ORDER BY priority ASC "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceMstId)
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intSecondLastPriority)
                objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverSettingId)

                iCnt = objDataOperation.RecordCount(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If iCnt <= 0 AndAlso blnFlag = False Then
                    strMsg = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot add resolution to the selected grievance. Reason, Lower level approver has not given his resolution description.")
                End If

                If iCnt > 0 Then

                    '************** THIS BLOCK IMPLIES THAT IF LAST LEVEL DATA IS PRESENT AND WE CHECK THIS FOR CURRENT LEVEL PRIORITY FOR DATA PRESENSE
                    iCnt = -1

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceMstId)
                    objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityId)
                    objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverSettingId)

                    iCnt = objDataOperation.RecordCount(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If iCnt > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add resolution to the selected grievance. Reason, Another approver with same level has already given the resolution description.")
                    End If

                    '************** THIS BLOCK IMPLIES THAT IF LAST LEVEL DATA IS PRESENT AND WE CHECK THIS FOR CURRENT LEVEL PRIORITY FOR DATA PRESENSE

                End If

                End If




        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidApprover; Module Name: " & mstrModuleName)
        Finally
            objLevel = Nothing : dsList.Dispose()
            exForce = Nothing
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return strMsg
    End Function

    Public Function GetResponse_Type(Optional ByVal strListName As String = "List", _
                                   Optional ByVal blnFlag As Boolean = False) As DataSet

        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME UNION  "
            End If


            strQ &= " SELECT " & enResponseType.My_Response & " AS Id,@My_Response AS NAME " & _
                    "UNION SELECT " & enResponseType.Commitee_Response & " AS Id,@Commitee_Response AS NAME "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@My_Response", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "My Response"))
            objDataOperation.AddParameter("@Commitee_Response", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Gajanan [24-June-2019] -- Start      
    Public Function GetApproverResponse_Type(Optional ByVal strListName As String = "List", _
                                   Optional ByVal blnFlag As Boolean = False) As DataSet

        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME UNION  "
            End If


            strQ &= " SELECT 1 AS Id, @responded  AS NAME " & _
                    "UNION SELECT 2 AS Id,@Pending AS NAME "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Pending"))
            objDataOperation.AddParameter("@responded", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Responded"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoan_Status", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Gajanan [24-June-2019] -- End

    Public Function InsertCommitteeData(ByVal mstrEmployeeId As String, ByVal objDataOperation As clsDataOperation, ByVal eAudit As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        mblnIsvoid = 0
        mintVoiduserunkid = -1
        mstrVoidreason = ""
        mdtVoiddatetime = Nothing
        Try
            Dim arEmpId() As String = mstrEmployeeId.Trim.Split(",")
            For i As Integer = 0 To arEmpId.Length - 1
                mintEmployeeunkid = CInt(arEmpId(i))
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                If mdtVoiddatetime = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                End If


                strQ = "INSERT INTO greresolution_meeting_tran ( " & _
                           " resolutionsteptranunkid " & _
                           ",employeeunkid " & _
                           ",isvoid " & _
                           ",voiduserunkid " & _
                           ",voidreason " & _
                           ",voiddatetime " & _
                       ") VALUES (" & _
                           " @resolutionsteptranunkid " & _
                           ",@employeeunkid " & _
                           ",@isvoid " & _
                           ",@voiduserunkid " & _
                           ",@voidreason " & _
                           ",@voiddatetime " & _
                       "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim intTranId As Integer = dsList.Tables(0).Rows(0).Item(0)

                strQ = "INSERT INTO atgreresolution_meeting_tran (" & _
                       "     row_guid " & _
                       "    ,resolutionmeetingtranunkid " & _
                       "    ,resolutionsteptranunkid " & _
                       "    ,employeeunkid " & _
                       "    ,audittype " & _
                       "    ,audituserunkid " & _
                       "    ,auditdatetime " & _
                       "    ,loginemployeeunkid " & _
                       "    ,ip " & _
                       "    ,host " & _
                       "    ,form_name " & _
                       "    ,isweb " & _
                       ") " & _
                       "VALUES ( " & _
                       "     @row_guid " & _
                       "    ,@resolutionmeetingtranunkid " & _
                       "    ,@resolutionsteptranunkid " & _
                       "    ,@employeeunkid " & _
                       "    ,@audittype " & _
                       "    ,@audituserunkid " & _
                       "    ,@auditdatetime " & _
                       "    ,@loginemployeeunkid " & _
                       "    ,@ip " & _
                       "    ,@host " & _
                       "    ,@form_name " & _
                       "    ,@isweb " & _
                       ") "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionsteptranunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeeId.ToString)
                objDataOperation.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
                objDataOperation.AddParameter("@resolutionmeetingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranId)
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.NAME_SIZE, eAudit)
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
                objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
                objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertCommitteeData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetDataOfCommitteeData(ByVal strTableName As String, ByVal intResolutionStepTranId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xObjDataOpr IsNot Nothing Then
            objDataOperation = xObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                     "resolutionmeetingtranunkid " & _
                    ",resolutionsteptranunkid " & _
                    ",employeeunkid " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voidreason " & _
                    ",voiddatetime " & _
                    ",'' AS AUD " & _
                    "FROM greresolution_meeting_tran " & _
                    "WHERE resolutionsteptranunkid = @resolutionsteptranunkid " & _
                    " AND isvoid = 0"


            objDataOperation.AddParameter("@resolutionsteptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intResolutionStepTranId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetHighestPriority()
        Try
            Dim objAppr As New clsgrievanceapprover_master
            Dim dsApprovers As New DataSet
            Dim eApprType As enGrievanceApproval
           Dim clsConfig As New clsConfigOptions


            If Convert.ToInt32(clsConfig._GrievanceApprovalSetting) = enGrievanceApproval.ApproverEmpMapping Then
                eApprType = enGrievanceApproval.ApproverEmpMapping
            ElseIf Convert.ToInt32(clsConfig._GrievanceApprovalSetting) = enGrievanceApproval.UserAcess Then
                eApprType = enGrievanceApproval.UserAcess
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHighestPriority; Module Name: " & mstrModuleName)
        End Try

    End Function


    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    Public Function GetEmployeeResolutionStatus(ByVal enApprSetting As enGrievanceApproval, _
                                                ByVal intMappedUserId As Integer, _
                                                ByVal strDatabaseName As String, _
                                                ByVal intCompanyId As Integer, _
                                                ByVal intYearId As Integer, _
                                                ByVal strUserAccessMode As String, _
                                                ByVal strEmpAsOnDate As String, _
                                                ByVal strEmployeeid As String, _
                                                ByVal strGrievanceRefno As String, _
                                                ByVal intGrievanceId As Integer, _
                                                ByVal blnOnlyLowerLevelResponse As Boolean, _
                                                Optional ByVal mintApproverType As Integer = -1, _
                                                Optional ByVal mintEmployeeStatus As Integer = -1, _
                                                Optional ByVal mintEmployeeUnkids As Integer = -1) As DataTable 'Gajanan [24-June-2019] -- Add mintApproverType,'Gajanan [27-June-2019] -- ADD mintEmployeeStatus

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim objDataoperation As New clsDataOperation

        Try

            Dim xDateJoinQry, xDataFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, eZeeDate.convertDate(strEmpAsOnDate), eZeeDate.convertDate(strEmpAsOnDate), True, False, strDatabaseName, "empappr")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmpAsOnDate), strDatabaseName, "#empappr#")

            Select Case enApprSetting
'Gajanan [24-OCT-2019] -- Start   
'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   



                Case enGrievanceApproval.ReportingTo

                    If strEmployeeid.Trim.Length > 0 Then
                        'StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                        '       "DROP TABLE #Results " & _
                        '       "CREATE TABLE #Results ( " & _
                        '            "eid INT " & _
                        '            ",rempid INT " & _
                        '            ",empid INT " & _
                        '            ",priority INT " & _
                        '            ") " & _
                        '       "DECLARE @REmpId AS INT " & _
                        '            ",@NxEmpId AS INT " & _
                        '            ",@Eid AS INT " & _
                        '            ",@priority AS INT " & _
                        '       "SET @REmpId = 0 " & _
                        '       "SET @NxEmpId = " & strEmployeeid & " " & _
                        '       "SET @eid = " & strEmployeeid & " " & _
                        '       "SET @priority = 0 " & _
                        '       "WHILE (@NxEmpId) > 0 " & _
                        '       "BEGIN " & _
                        '            "SET @priority = @priority + 1 " & _
                        '            "SET @REmpId = ISNULL(( " & _
                        '                           "SELECT reporttoemployeeunkid " & _
                        '                           "FROM hremployee_reportto " & _
                        '                           "WHERE employeeunkid = @NxEmpId " & _
                        '                                "AND ishierarchy = 1 " & _
                        '                                "AND isvoid = 0 " & _
                        '                           "), 0) " & _
                        '            "IF (@NxEmpId = @REmpId) " & _
                        '                 "SET @REmpId = 0 " & _
                        '            "INSERT INTO #Results ( " & _
                        '                 "eid " & _
                        '                 ",rempid " & _
                        '                 ",empid " & _
                        '                 ",priority " & _
                        '                 ") " & _
                        '            "VALUES ( " & _
                        '                 "@NxEmpId " & _
                        '                 ",@REmpId " & _
                        '                 ",@eid " & _
                        '                 ",@priority " & _
                        '                 ") " & _
                        '            "SET @NxEmpId = @REmpId " & _
                        '            "DECLARE @Rcnt AS INT " & _
                        '            "SET @Rcnt = ( " & _
                        '                      "SELECT COUNT(1) " & _
                        '                      "FROM #Results " & _
                        '                      "WHERE rempid = @NxEmpId " & _
                        '                      ") " & _
                        '            "IF (@Rcnt > 1) " & _
                        '                 "BREAK " & _
                        '            "ELSE " & _
                        '                 "CONTINUE " & _
                        '       "END "

                        'Dim objgrievanceMst As New clsgrievanceapprover_master
                        'StrQ = objgrievanceMst.getGrievanceReportingToQuery(True, True)
                        'objgrievanceMst = Nothing

                        'StrQ = StrQ.Replace("#strEmpIds#", strEmployeeid)

                    Else

                        'StrQ = "IF OBJECT_ID('tempdb..#GrievanceAgainstEmp') IS NOT NULL " & _
                        '       "DROP TABLE #GrievanceAgainstEmp " & _
                        '       "CREATE TABLE #GrievanceAgainstEmp ( " & _
                        '            "EmpIndex INT identity(1, 1) " & _
                        '            ",empid INT " & _
                        '            ") " & _
                        '       "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                        '            "DROP TABLE #Results " & _
                        '       "CREATE TABLE #Results ( " & _
                        '            "eid INT " & _
                        '            ",rempid INT " & _
                        '            ",empid INT " & _
                        '            ",priority INT " & _
                        '            ") " & _
                        '       " " & _
                        '       "DECLARE @REmpId AS INT " & _
                        '            ",@NxEmpId AS INT " & _
                        '            ",@Eid AS INT " & _
                        '            ",@priority AS INT " & _
                        '       "SET @REmpId = 0 " & _
                        '       "SET @NxEmpId = 0 " & _
                        '       "SET @eid = 0 " & _
                        '       "SET @priority = 0 " & _
                        '       "INSERT INTO #GrievanceAgainstEmp " & _
                        '       "SELECT againstemployeeunkid " & _
                        '       "FROM ( " & _
                        '            "SELECT againstemployeeunkid " & _
                        '                 ",ROW_NUMBER() OVER ( " & _
                        '                      "PARTITION BY againstemployeeunkid ORDER BY againstemployeeunkid " & _
                        '                      ") AS rno " & _
                        '            "FROM gregrievance_master " & _
                        '            "WHERE issubmitted = 1 " & _
                        '                 "AND isprocessed = 0 " & _
                        '                 "AND isvoid = 0 " & _
                        '            ") AS b " & _
                        '       "WHERE b.rno = 1 " & _
                        '       "DECLARE @TotalRows INT = ( " & _
                        '                 "SELECT COUNT(1) " & _
                        '                 "FROM #GrievanceAgainstEmp " & _
                        '                 ") " & _
                        '            ",@i INT = 1 " & _
                        '       "WHILE @i <= @TotalRows " & _
                        '       "BEGIN " & _
                        '            "SELECT @NxEmpId = empid " & _
                        '            "FROM #GrievanceAgainstEmp " & _
                        '            "WHERE EmpIndex = @i " & _
                        '            "SET @Eid = @NxEmpId " & _
                        '            "WHILE @NxEmpId > 0 " & _
                        '            "BEGIN " & _
                        '                 "SET @priority = @priority + 1 " & _
                        '                 "SET @REmpId = ISNULL(( " & _
                        '                                "SELECT reporttoemployeeunkid " & _
                        '                                "FROM hremployee_reportto " & _
                        '                                "WHERE employeeunkid = @NxEmpId " & _
                        '                                     "AND ishierarchy = 1 " & _
                        '                                     "AND isvoid = 0 " & _
                        '                                "), 0) " & _
                        '                 "IF (@NxEmpId = @REmpId) " & _
                        '                      "SET @REmpId = 0 " & _
                        '                 "INSERT INTO #Results ( " & _
                        '                      "eid " & _
                        '                      ",rempid " & _
                        '                      ",empid " & _
                        '                      ",priority " & _
                        '                      ") " & _
                        '                 "VALUES ( " & _
                        '                      "@NxEmpId " & _
                        '                      ",@REmpId " & _
                        '                      ",@Eid " & _
                        '                      ",@priority " & _
                        '                      ") " & _
                        '                 "IF (@REmpId = 0) " & _
                        '                 "BEGIN " & _
                        '                      "SET @i = @i + 1 " & _
                        '                      "SET @priority = 0 " & _
                        '                      "SELECT @NxEmpId = empid " & _
                        '                      "FROM #GrievanceAgainstEmp " & _
                        '                      "WHERE EmpIndex = @i " & _
                        '                      "BREAK; " & _
                        '                 "END " & _
                        '                 "ELSE " & _
                        '                      "SET @NxEmpId = @REmpId " & _
                        '                 "DECLARE @Rcnt AS INT " & _
                        '                 "SET @Rcnt = ( " & _
                        '                           "SELECT COUNT(1) " & _
                        '                           "FROM #Results " & _
                        '                           "WHERE rempid = @NxEmpId " & _
                        '                                "AND #Results.empid = @Eid " & _
                        '                           ") " & _
                        '                 "IF (@Rcnt > 1) " & _
                        '                 "BEGIN " & _
                        '                      "SET @i = @i + 1 " & _
                        '                      "SELECT @NxEmpId = empid " & _
                        '                      "FROM #GrievanceAgainstEmp " & _
                        '                      "WHERE EmpIndex = @i " & _
                        '                      "SET @priority = 0 " & _
                        '                      "BREAK; " & _
                        '                 "END " & _
                        '                 "ELSE " & _
                        '                      "CONTINUE " & _
                        '            "END " & _
                        '       "END "
                        'Dim objgrievanceMst As New clsgrievanceapprover_master
                        'StrQ = objgrievanceMst.getGrievanceReportingToQuery(False, False)
                        'objgrievanceMst = Nothing
                        
                    End If


                    Dim objgrievanceMst As New clsgrievanceapprover_master
                    StrQ = objgrievanceMst.getGrievanceReportingToQuery(False, False)
                    objgrievanceMst = Nothing

                    StrQ &= "SELECT " & _
                          "a.* " & _
                          ",ISNULL(greresolution_step_tran.qualifyremark, '') AS qualifyremark " & _
                          ",ISNULL(greresolution_step_tran.responseremark, '') AS responseremark " & _
                          ",CASE " & _
                          "    WHEN greresolution_step_tran.responsetypeunkid > 1 THEN 1 " & _
                          "    ELSE 2 " & _
                          "END AS responsetype " & _
                          ",CASE " & _
                                 "WHEN ISNULL(greresolution_step_tran.responsetypeunkid, 0) > 0 THEN @responded " & _
                                 "ELSE @Pending " & _
                            "END AS ApprovalResponse " & _
                          ",ISNULL(greinitiater_response_tran.remark,'') as  EmpResponseRemark " & _
                           ",CASE " & _
                             "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.AgreedAndClose) & " THEN @AgreedAndClose " & _
                             "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.DisagreeAndEscalateToNextLevel) & " THEN @DisagreeAndEscalateToNextLevel " & _
                             "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.Appeal) & " THEN @Appeal " & _
                             "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.Disagree) & " THEN @Disagree " & _
                             "ELSE @Pending END " & _
                           "as EmployeeStatus " & _
                           ",c.EmployeeIds " & _
                           ",AgainstEmployee.firstname + ' '   + AgainstEmployee.surname as AgainstEmployee " & _
                          "FROM (SELECT " & _
                                 "#Results.rempid as approvermasterunkid " & _
                                 ",0 as apprlevelunkid " & _
                                 ",@Level + ' - ' + CONVERT(NVARCHAR,#Results.priority) AS levelname " & _
                                 ",#Results.priority " & _
                                 ",#Results.rempid as approverempid " & _
                                 ",ISNULL(empappr.firstname, '') + ' ' + ISNULL(empappr.surname, '') AS name " & _
                                 ",ISNULL(empappr.email, '') AS email " & _
                                 ",guser.userunkid as mapuserunkid " & _
                                 ",guser.userunkid " & _
                                 ",0 as isexternal " & _
                                 ",@NO AS ExAppr " & _
                                 ",CAST(CASE " & _
                                        "WHEN CONVERT(CHAR(8), empappr.appointeddate, 112) <= '" & strEmpAsOnDate & "' AND " & _
                                             "ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' AND " & _
                                             "ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' AND " & _
                                             "ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' THEN 1 " & _
                                        "ELSE 0 " & _
                                   "END AS BIT " & _
                                   ") AS activestatus " & _
                                   ",ISNULL(gregrievance_master.grievancemasterunkid,0) as grievancemasterunkid " & _
                                   ",gregrievance_master.grievancerefno " & _
                                  ",gregrievance_master.againstemployeeunkid " & _
                                   "FROM #Results " & _
                                  "LEFT JOIN hrmsConfiguration..cfuser_master AS guser " & _
                                      "ON guser.userunkid = #Results.rempid " & _
                                  "LEFT JOIN hremployee_master " & _
                                      "ON hremployee_master.employeeunkid = guser.employeeunkid " & _
                                  "LEFT JOIN hremployee_master AS empappr " & _
                                      "ON empappr.employeeunkid = #Results.rempid " & _
                                  "LEFT JOIN gregrievance_master " & _
                                        "ON #Results.empid = gregrievance_master.againstemployeeunkid " & _
                                        "AND #Results.grievanceId = gregrievance_master.grievancemasterunkid "


                    If (strEmployeeid.Trim.Length > 0) = False Then
                        StrQ &= " AND gregrievance_master.grievancemasterunkid = #Results.grievanceId "
                    End If



                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                        StrQ &= " WHERE  1= 1 "
                    End If


                    StrQ &= "AND #Results.rempid > 0 " & _
                            "AND gregrievance_master.approvalsettingid = " & CInt(enGrievanceApproval.ReportingTo) & " " & _
                            "AND gregrievance_master.grievancemasterunkid > 0" & _
                            "AND gregrievance_master.fromemployeeunkid = " & mintEmployeeUnkids & ") AS a " & _
                            "LEFT JOIN greresolution_step_tran " & _
                                "ON greresolution_step_tran.grievancemasterunkid = a.grievancemasterunkid " & _
                                "AND a.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
                            "LEFT JOIN greinitiater_response_tran " & _
                                "ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid " & _
                            "LEFT JOIN (SELECT " & _
                           "MeetingMember.resolutionsteptranunkid " & _
                           ",ISNULL(STUFF((SELECT " & _
                                     " ',' + employeecode + ' - ' + firstname + ' ' + surname " & _
                                "FROM greresolution_meeting_tran " & _
                                "LEFT JOIN hremployee_master " & _
                                     "ON hremployee_master.employeeunkid = greresolution_meeting_tran.employeeunkid " & _
                                "LEFT JOIN greresolution_step_tran AS b " & _
                                     "ON b.resolutionsteptranunkid = greresolution_meeting_tran.resolutionsteptranunkid " & _
                                "WHERE greresolution_meeting_tran.isvoid = 0 " & _
                                "AND MeetingMember.resolutionsteptranunkid = b.resolutionsteptranunkid " & _
                                "ORDER BY greresolution_meeting_tran.employeeunkid " & _
                                "FOR XML PATH ('')) " & _
                           ", 1, 1, ''), '') AS EmployeeIds " & _
                      "FROM greresolution_step_tran AS MeetingMember " & _
                      "WHERE MeetingMember.isvoid = 0 " & _
                      "GROUP BY MeetingMember.resolutionsteptranunkid) AS C " & _
                      "ON c.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid " & _
                      " LEFT JOIN hremployee_master as AgainstEmployee " & _
                      " ON a.againstemployeeunkid = AgainstEmployee.employeeunkid " & _
                      "WHERE 1=1 "

                    If strGrievanceRefno.Length > 0 Then
                        StrQ &= "AND a.grievancerefno = '" & strGrievanceRefno & "' "
                    End If

                    StrQ &= "AND ISNULL(greresolution_step_tran.isvoid,0) = 0 "


                    If blnOnlyLowerLevelResponse = True Then
                        StrQ &= "AND greresolution_step_tran.responseremark <> '' " & _
                                "AND greresolution_step_tran.qualifyremark <> '' " & _
                                "AND a.priority IN (SELECT " & _
                                     "#Results.priority " & _
                                "FROM greresolution_step_tran " & _
                                "LEFT JOIN #Results " & _
                                     "ON #Results.rempid = greresolution_step_tran.approvermasterunkid " & _
                                "WHERE grievancemasterunkid = @grievancemasterunkid ) "
                    End If

                    If mintApproverType > 0 Then
                        StrQ &= " AND CASE WHEN greresolution_step_tran.responsetypeunkid > 1 THEN 1 ELSE 2 END = " & mintApproverType & " "
                    End If

                    If mintEmployeeStatus > -1 Then
                        StrQ &= " and ISNULL(greinitiater_response_tran.statusunkid,0) =  " & mintEmployeeStatus & "  "
                    End If

                    StrQ &= " ORDER BY a.grievancerefno,a.priority "
                    StrQ &= " Drop Table #Results "

                    'If strEmployeeid.Trim.Length <= 0 Then
                    '    StrQ &= " DROP TABLE #GrievanceAgainstEmp "
                    'End If
                        StrQ &= " DROP TABLE #GrievanceAgainstEmp "

                    StrQ = StrQ.Replace("#empappr#", "empappr")

                    objDataoperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
                    objDataoperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
                    objDataoperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enApprSetting))
                    objDataoperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceId)
                    objDataoperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Agree And Close"))
                    objDataoperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Disagree And Escalate To Next Level"))
                    objDataoperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Appeal"))
                    objDataoperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Pending"))
                    objDataoperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Approver Response"))
                    objDataoperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))
                    objDataoperation.AddParameter("@responded", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Responded"))
                    objDataoperation.AddParameter("@Disagree", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Disagree"))
                    objDataoperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Level"))

                    dsList = objDataoperation.ExecQuery(StrQ, "List")

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If
'Gajanan [24-OCT-2019] -- End

                Case enGrievanceApproval.ApproverEmpMapping

                    'Gajanan [24-June-2019] -- Start      


                    '             StrQ = "SELECT " & _
                    '"a.* " & _
                    '",ISNULL(greresolution_step_tran.qualifyremark, '') AS qualifyremark " & _
                    '",ISNULL(greresolution_step_tran.responseremark, '') AS responseremark " & _
                    '",CASE " & _
                    '       "WHEN greresolution_step_tran.responsetypeunkid = 1 THEN @MyResponse " & _
                    '       "WHEN greresolution_step_tran.responsetypeunkid = 2 THEN @CommiteeResponse " & _
                    '       "ELSE @Pending " & _
                    '  "END AS ApprovalResponse " & _
                    '",ISNULL(greinitiater_response_tran.remark,'') as  EmpResponseRemark " & _
                    ' ",CASE " & _
                    '   "WHEN greinitiater_response_tran.statusunkid = 1 THEN @AgreedAndClose " & _
                    '   "WHEN greinitiater_response_tran.statusunkid = 2 THEN @DisagreeAndEscalateToNextLevel " & _
                    '   "WHEN greinitiater_response_tran.statusunkid = 4 THEN @Appeal " & _
                    '   "ELSE @Pending END " & _
                    ' "as EmployeeStatus " & _
                    '"FROM (SELECT " & _
                    '       "greapprover_master.approvermasterunkid " & _
                    '     ",greapprover_master.apprlevelunkid " & _
                    '     ",greapproverlevel_master.levelname " & _
                    '     ",greapproverlevel_master.priority " & _
                    '     ",greapprover_master.approverempid " & _
                    '     ",CASE " & _
                    '            "WHEN greapprover_master.approvalsettingid = 1 THEN CASE " & _
                    '                      "WHEN ISNULL(guser.firstname + guser.lastname, '') = '' THEN ISNULL(guser.username, '') " & _
                    '                      "ELSE ISNULL(guser.firstname, '') + ' ' + ISNULL(guser.lastname, '') " & _
                    '                 "END " & _
                    '            "WHEN greapprover_master.approvalsettingid = 2 THEN ISNULL(empappr.firstname, '') + ' ' + ISNULL(empappr.surname, '') " & _
                    '       "END AS name " & _
                    '     ",CASE " & _
                    '            "WHEN ISNULL(empappr.email, '') = '' THEN ISNULL(guser.email, '') " & _
                    '            "ELSE ISNULL(empappr.email, '') " & _
                    '       "END AS email " & _
                    '     ",hrdepartment_master.departmentunkid AS departmentunkid " & _
                    '     ",ISNULL(hrdepartment_master.name, '') AS departmentname " & _
                    '     ",hrjob_master.jobunkid AS jobunkid " & _
                    '     ",ISNULL(hrjob_master.job_name, '') AS jobname " & _
                    '     ",greapprover_master.isvoid " & _
                    '     ",greapprover_master.mapuserunkid " & _
                    '     ",greapprover_master.voiddatetime " & _
                    '     ",greapprover_master.userunkid " & _
                    '     ",greapprover_master.voiduserunkid " & _
                    '     ",greapprover_master.voidreason " & _
                    '     ",greapprover_master.isexternal " & _
                    '     ",greapprover_master.isactive " & _
                    '     ",CASE " & _
                    '            "WHEN greapprover_master.isexternal = 1 THEN @YES " & _
                    '            "ELSE @NO " & _
                    '       "END AS ExAppr " & _
                    '     ",CAST(CASE " & _
                    '            "WHEN CONVERT(CHAR(8), empappr.appointeddate, 112) <= '" & strEmpAsOnDate & "' AND " & _
                    '                 "ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' AND " & _
                    '                 "ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' AND " & _
                    '                 "ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' THEN 1 " & _
                    '            "ELSE 0 " & _
                    '       "END AS BIT " & _
                    '       ") AS activestatus " & _
                    '     ",gregrievance_master.grievancemasterunkid " & _
                    '     ",gregrievance_master.grievancerefno " & _
                    '  "FROM greapprover_master " & _
                    ' "LEFT JOIN greapproverlevel_master " & _
                    '     "ON greapproverlevel_master.apprlevelunkid = greapprover_master.apprlevelunkid " & _
                    ' "LEFT JOIN hrmsConfiguration..cfuser_master AS guser " & _
                    '     "ON guser.userunkid = greapprover_master.mapuserunkid " & _
                    ' "LEFT JOIN hremployee_master " & _
                    '     "ON hremployee_master.employeeunkid = guser.employeeunkid " & _
                    ' "LEFT JOIN hremployee_master AS empappr " & _
                    '     "ON empappr.employeeunkid = greapprover_master.approverempid " & _
                    ' "LEFT JOIN (SELECT " & _
                    '            "departmentunkid " & _
                    '          ",employeeunkid " & _
                    '          ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '       "FROM hremployee_transfer_tran " & _
                    '       "WHERE isvoid = 0 " & _
                    '       "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & strEmpAsOnDate & "') AS Alloc " & _
                    '       "ON Alloc.employeeunkid = empappr.employeeunkid " & _
                    '       "AND Alloc.rno = 1 " & _
                    ' "LEFT JOIN hrdepartment_master " & _
                    '       "ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                    ' "LEFT JOIN (SELECT " & _
                    '            "jobunkid " & _
                    '          ",jobgroupunkid " & _
                    '          ",employeeunkid " & _
                    '          ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '       "FROM hremployee_categorization_tran " & _
                    '       "WHERE isvoid = 0 " & _
                    '       "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & strEmpAsOnDate & "') AS Jobs " & _
                    '       "ON Jobs.employeeunkid = empappr.employeeunkid " & _
                    '       "AND Jobs.rno = 1 " & _
                    ' "LEFT JOIN hrjob_master " & _
                    '       "ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                    ' "JOIN greapprover_tran " & _
                    '       "ON greapprover_tran.approvermasterunkid = greapprover_master.approvermasterunkid " & _
                    ' "LEFT JOIN gregrievance_master " & _
                    '       "ON greapprover_tran.employeeunkid = gregrievance_master.againstemployeeunkid "

                    '             If xDateJoinQry.Trim.Length > 0 Then
                    '                 StrQ &= xDateJoinQry
                    '             End If

                    '             If xAdvanceJoinQry.Trim.Length > 0 Then
                    '                 StrQ &= xAdvanceJoinQry
                    '                 StrQ &= " WHERE  1= 1 "
                    '             End If


                    '             StrQ &= "AND greapprover_master.isexternal = 0 " & _
                    '                     "AND greapprover_tran.employeeunkid IN (" & strEmployeeid & ") " & _
                    '                     "AND greapprover_master.approvalsettingid = @approvalsetting " & _
                    '                     "AND greapprover_master.isvoid = 0 " & _
                    '                     "AND greapprover_master.isactive = 1 " & _
                    '                     "AND greapprover_tran.isvoid = 0) AS a " & _
                    '                     "LEFT JOIN greresolution_step_tran " & _
                    '                         "ON greresolution_step_tran.grievancemasterunkid = a.grievancemasterunkid " & _
                    '                         "AND a.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
                    '                     "LEFT JOIN greinitiater_response_tran " & _
                    '                         "ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid " & _
                    '                     "WHERE a.grievancerefno = '" & strGrievanceRefno & "' " & _
                    '                     "AND ISNULL(greresolution_step_tran.isvoid,0) = 0 "

                    '             If blnOnlyLowerLevelResponse = True Then
                    '                 StrQ &= "AND greresolution_step_tran.responseremark <> '' " & _
                    '                         "AND greresolution_step_tran.qualifyremark <> '' " & _
                    '                         "AND a.priority IN (SELECT " & _
                    '                              "greapproverlevel_master.priority " & _
                    '                         "FROM greresolution_step_tran " & _
                    '                         "LEFT JOIN greapproverlevel_master " & _
                    '                              "ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                    '                         "WHERE grievancemasterunkid = @grievancemasterunkid ) "

                    '             End If

                    '             StrQ &= " ORDER BY a.priority"
                    '             StrQ = StrQ.Replace("#empappr#", "empappr")

                    '             objDataoperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
                    '             objDataoperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
                    '             objDataoperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enApprSetting))
                    '             objDataoperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceId)
                    '             objDataoperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Agree And Close"))
                    '             objDataoperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Disagree And Escalate To Next Level"))
                    '             objDataoperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Appeal"))
                    '             objDataoperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Pending"))
                    '             objDataoperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Approver Response"))
                    '             objDataoperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Commitee Response"))



                    '             dsList = objDataoperation.ExecQuery(StrQ, "List")

                    '             If objDataoperation.ErrorMessage <> "" Then
                    '                 exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                    '                 Throw exForce
                    '             End If


                    'Gajanan [4-July-2019] -- Add AgainstEmployee 
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes




                    StrQ = "SELECT " & _
                           "a.* " & _
                           ",ISNULL(greresolution_step_tran.qualifyremark, '') AS qualifyremark " & _
                           ",ISNULL(greresolution_step_tran.responseremark, '') AS responseremark " & _
                           ",CASE " & _
                           "    WHEN greresolution_step_tran.responsetypeunkid > 1 THEN 1 " & _
                           "    ELSE 2 " & _
                           "END AS responsetype " & _
                           ",CASE " & _
                                  "WHEN ISNULL(greresolution_step_tran.responsetypeunkid, 0) > 0 THEN @responded " & _
                                  "ELSE @Pending " & _
                             "END AS ApprovalResponse " & _
                           ",ISNULL(greinitiater_response_tran.remark,'') as  EmpResponseRemark " & _
                            ",CASE " & _
                              "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.AgreedAndClose) & " THEN @AgreedAndClose " & _
                              "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.DisagreeAndEscalateToNextLevel) & " THEN @DisagreeAndEscalateToNextLevel " & _
                              "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.Appeal) & " THEN @Appeal " & _
                              "WHEN greinitiater_response_tran.statusunkid = " & CInt(enGrievanceResponse.Disagree) & " THEN @Disagree " & _
                              "ELSE @Pending END " & _
                            "as EmployeeStatus " & _
                            ",c.EmployeeIds " & _
                            ",AgainstEmployee.firstname + ' '   + AgainstEmployee.surname as AgainstEmployee " & _
                           "FROM (SELECT " & _
                                  "greapprover_master.approvermasterunkid " & _
                                ",greapprover_master.apprlevelunkid " & _
                                ",greapproverlevel_master.levelname " & _
                                ",greapproverlevel_master.priority " & _
                                ",greapprover_master.approverempid " & _
                                ",CASE " & _
                                       "WHEN greapprover_master.approvalsettingid = 1 THEN CASE " & _
                                                 "WHEN ISNULL(guser.firstname + guser.lastname, '') = '' THEN ISNULL(guser.username, '') " & _
                                                 "ELSE ISNULL(guser.firstname, '') + ' ' + ISNULL(guser.lastname, '') " & _
                                            "END " & _
                                       "WHEN greapprover_master.approvalsettingid = 2 THEN ISNULL(empappr.firstname, '') + ' ' + ISNULL(empappr.surname, '') " & _
                                  "END AS name " & _
                                ",CASE " & _
                                       "WHEN ISNULL(empappr.email, '') = '' THEN ISNULL(guser.email, '') " & _
                                       "ELSE ISNULL(empappr.email, '') " & _
                                  "END AS email " & _
                                ",greapprover_master.isvoid " & _
                                ",greapprover_master.mapuserunkid " & _
                                ",greapprover_master.voiddatetime " & _
                                ",greapprover_master.userunkid " & _
                                ",greapprover_master.voiduserunkid " & _
                                ",greapprover_master.voidreason " & _
                                ",greapprover_master.isexternal " & _
                                ",greapprover_master.isactive " & _
                                ",CASE " & _
                                       "WHEN greapprover_master.isexternal = 1 THEN @YES " & _
                                       "ELSE @NO " & _
                                  "END AS ExAppr " & _
                                ",CAST(CASE " & _
                                       "WHEN CONVERT(CHAR(8), empappr.appointeddate, 112) <= '" & strEmpAsOnDate & "' AND " & _
                                            "ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' AND " & _
                                            "ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' AND " & _
                                            "ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '" & strEmpAsOnDate & "') >= '" & strEmpAsOnDate & "' THEN 1 " & _
                                       "ELSE 0 " & _
                                  "END AS BIT " & _
                                  ") AS activestatus "


                    StrQ &= ",ISNULL(gregrievance_master.grievancemasterunkid,0) as grievancemasterunkid "


                    StrQ &= ",gregrievance_master.grievancerefno " & _
                            ",gregrievance_master.againstemployeeunkid " & _
                             "FROM greapprover_master " & _
                            "LEFT JOIN greapproverlevel_master " & _
                                "ON greapproverlevel_master.apprlevelunkid = greapprover_master.apprlevelunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master AS guser " & _
                                "ON guser.userunkid = greapprover_master.mapuserunkid " & _
                            "LEFT JOIN hremployee_master " & _
                                "ON hremployee_master.employeeunkid = guser.employeeunkid " & _
                            "LEFT JOIN hremployee_master AS empappr " & _
                                "ON empappr.employeeunkid = greapprover_master.approverempid " & _
                            "JOIN greapprover_tran " & _
                                  "ON greapprover_tran.approvermasterunkid = greapprover_master.approvermasterunkid " & _
                            "LEFT JOIN gregrievance_master " & _
                                  "ON greapprover_tran.employeeunkid = gregrievance_master.againstemployeeunkid "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                        StrQ &= " WHERE  1= 1 "
                    End If


                    'Gajanan [4-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
                    'StrQ &= "AND greapprover_master.isexternal = 0 "
                    'Gajanan [4-July-2019] -- End

                    If strEmployeeid.Length > 0 Then
                        StrQ &= "AND greapprover_tran.employeeunkid IN (" & strEmployeeid & ") "
                    End If

                    StrQ &= "AND greapprover_master.approvalsettingid = @approvalsetting " & _
                            "AND greapprover_master.isvoid = 0 " & _
                            "AND greapprover_master.isactive = 1 " & _
                            "AND greapprover_tran.isvoid = 0 " & _
                            "AND gregrievance_master.grievancemasterunkid > 0" & _
                            "AND gregrievance_master.fromemployeeunkid = " & mintEmployeeUnkids & ") AS a " & _
                            "LEFT JOIN greresolution_step_tran " & _
                                "ON greresolution_step_tran.grievancemasterunkid = a.grievancemasterunkid " & _
                                "AND a.approvermasterunkid = greresolution_step_tran.approvermasterunkid " & _
                            "LEFT JOIN greinitiater_response_tran " & _
                                "ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid "



                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    StrQ &= "LEFT JOIN (SELECT " & _
                           "MeetingMember.resolutionsteptranunkid " & _
                           ",ISNULL(STUFF((SELECT " & _
                                     " ',' + employeecode + ' - ' + firstname + ' ' + surname " & _
                                "FROM greresolution_meeting_tran " & _
                                "LEFT JOIN hremployee_master " & _
                                     "ON hremployee_master.employeeunkid = greresolution_meeting_tran.employeeunkid " & _
                                "LEFT JOIN greresolution_step_tran AS b " & _
                                     "ON b.resolutionsteptranunkid = greresolution_meeting_tran.resolutionsteptranunkid " & _
                                "WHERE greresolution_meeting_tran.isvoid = 0 " & _
                                "AND MeetingMember.resolutionsteptranunkid = b.resolutionsteptranunkid " & _
                                "ORDER BY greresolution_meeting_tran.employeeunkid " & _
                                "FOR XML PATH ('')) " & _
                           ", 1, 1, ''), '') AS EmployeeIds " & _
                      "FROM greresolution_step_tran AS MeetingMember " & _
                      "WHERE MeetingMember.isvoid = 0 " & _
                      "GROUP BY MeetingMember.resolutionsteptranunkid) AS C " & _
                      "ON c.resolutionsteptranunkid = greresolution_step_tran.resolutionsteptranunkid "
                    'Gajanan [27-June-2019] -- End


                    'Gajanan [4-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
                    strQ &= " LEFT JOIN hremployee_master as AgainstEmployee " & _
                         " ON a.againstemployeeunkid = AgainstEmployee.employeeunkid "
                    'Gajanan [4-July-2019] -- End



                    StrQ &= "WHERE 1=1 "

                    If strGrievanceRefno.Length > 0 Then
                        StrQ &= "AND a.grievancerefno = '" & strGrievanceRefno & "' "
                    End If

                    StrQ &= "AND ISNULL(greresolution_step_tran.isvoid,0) = 0 "


                    If blnOnlyLowerLevelResponse = True Then
                        StrQ &= "AND greresolution_step_tran.responseremark <> '' " & _
                                "AND greresolution_step_tran.qualifyremark <> '' " & _
                                "AND a.priority IN (SELECT " & _
                                     "greapproverlevel_master.priority " & _
                                "FROM greresolution_step_tran " & _
                                "LEFT JOIN greapproverlevel_master " & _
                                     "ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                                "WHERE grievancemasterunkid = @grievancemasterunkid ) "
                    End If

                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    'If bintApproverType > 0 Then
                    '    StrQ &= " AND CASE WHEN greresolution_step_tran.responsetypeunkid > 1 THEN 1 ELSE 2 END = " & bintApproverType & " "
                    'End If

                    If mintApproverType > 0 Then
                        StrQ &= " AND CASE WHEN greresolution_step_tran.responsetypeunkid > 1 THEN 1 ELSE 2 END = " & mintApproverType & " "
                    End If

                    If mintEmployeeStatus > -1 Then
                        StrQ &= " and ISNULL(greinitiater_response_tran.statusunkid,0) =  " & mintEmployeeStatus & "  "
                    End If
                    'Gajanan [27-June-2019] -- End



                    'Gajanan [13-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                    'Add(Disagree)
                    'Add(CInt(enGrievanceResponse.AgreedAndClose))
                    'Add(CInt(enGrievanceResponse.DisagreeAndEscalateToNextLevel))
                    'Add(CInt(enGrievanceResponse.Appeal))
                    'Add(CInt(enGrievanceResponse.Disagree))
                    'Gajanan [13-July-2019] -- End



                    StrQ &= " ORDER BY a.grievancerefno,a.priority "
                    StrQ = StrQ.Replace("#empappr#", "empappr")

                    objDataoperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
                    objDataoperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
                    objDataoperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enApprSetting))
                    objDataoperation.AddParameter("@grievancemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrievanceId)
                    objDataoperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Agree And Close"))
                    objDataoperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Disagree And Escalate To Next Level"))
                    objDataoperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Appeal"))
                    objDataoperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Pending"))
                    objDataoperation.AddParameter("@MyResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Approver Response"))
                    objDataoperation.AddParameter("@CommiteeResponse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Commitee Response"))
                    objDataoperation.AddParameter("@responded", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Responded"))

                    'Gajanan [13-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                    objDataoperation.AddParameter("@Disagree", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Disagree"))
                    'Gajanan [13-July-2019] -- End

                    dsList = objDataoperation.ExecQuery(StrQ, "List")

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Gajanan [24-June-2019] -- End

                    'Gajanan [4-July-2019] -- End

                Case enGrievanceApproval.UserAcess

            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeResolutionStatus; Module Name: " & mstrModuleName)
        Finally
            objDataoperation = Nothing
        End Try
        Return dsList.Tables(0)

    End Function
    'Gajanan [10-June-2019] -- End


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
    Public Function getEmployeeStatusFilter(Optional ByVal strListName As String = "List") As DataSet
        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ &= "SELECT -1 AS Id ,@Select AS NAME " & _
                    "UNION SELECT 0 AS Id ,@Pending AS NAME " & _
                    "UNION SELECT " & enGrievanceResponse.AgreedAndClose & " AS Id,@AgreedAndClose " & _
                    "UNION SELECT " & enGrievanceResponse.DisagreeAndEscalateToNextLevel & " AS Id,@DisagreeAndEscalateToNextLevel " & _
                    "UNION SELECT " & enGrievanceResponse.Appeal & " AS Id,@Appeal "

            'Gajanan [13-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
            strQ &= "UNION SELECT " & enGrievanceResponse.Disagree & " AS Id,@Disagree "
            'Gajanan [13-July-2019] -- End

            strQ &= "ORDER BY Id "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Pending"))
            objDataOperation.AddParameter("@AgreedAndClose", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Agree And Close"))
            objDataOperation.AddParameter("@Appeal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Appeal"))
            objDataOperation.AddParameter("@DisagreeAndEscalateToNextLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Disagree And Escalate To Next Level"))

            'Gajanan [13-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
            objDataOperation.AddParameter("@Disagree", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Disagree"))
            'Gajanan [13-July-2019] -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getEmployeeStatusFilter", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [27-June-2019] -- End


    'Gajanan [13-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement




    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   



    'Public Function IsResolutionAddedForGrievance(ByVal ApproverLevel As String, ByVal Grievanceunkid As Integer) As Boolean
    '    Dim strQ As String = String.Empty
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    Try
    '        strQ = "SELECT * FROM greinitiater_response_tran WHERE GRIEVANCEMASTERUNKID = " & Grievanceunkid & " " & _
    '                "AND ISVOID= 0 AND ISSUBMITTED = 1 AND APPRLEVELUNKID IN(" & ApproverLevel & ") "
    '        dsList = objDataOperation.ExecQuery(strQ, "ResolutionList")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables("ResolutionList").Rows.Count > 0 Then
    '            Return True
    '        End If

    '        Return False

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "getEmployeeStatusFilter", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function IsInitiatorResponseAddedForGrievance(ByVal ApproverLevel As String, ByVal Grievanceunkid As Integer, ByVal approvalsetting As enGrievanceApproval, Optional ByVal getResolutionCount As Boolean = False, Optional ByRef Count As Integer = 0) As Boolean

        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            Select Case approvalsetting

                Case enGrievanceApproval.ReportingTo

                    If getResolutionCount Then
                        strQ = "SELECT * FROM greresolution_step_tran WHERE GRIEVANCEMASTERUNKID = " & Grievanceunkid & " " & _
                               "AND ISVOID= 0 AND ISSUBMITTED = 1 AND approvalsettingid = " & CInt(approvalsetting) & " and isprocessed = 0 and isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "ResolutionList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("ResolutionList").Rows.Count > 0 Then
                            Count = dsList.Tables("ResolutionList").Rows.Count
                        End If
                        dsList = Nothing
                    End If



                    strQ = "SELECT * FROM greinitiater_response_tran WHERE GRIEVANCEMASTERUNKID = " & Grievanceunkid & " " & _
                           "AND ISVOID= 0 AND ISSUBMITTED = 1 AND approvalsettingid = " & CInt(approvalsetting) & " and isprocessed = 0 and isvoid = 0 "



                Case Else

                    strQ = "SELECT * FROM greinitiater_response_tran WHERE GRIEVANCEMASTERUNKID = " & Grievanceunkid & " " & _
                            "AND ISVOID= 0 AND ISSUBMITTED = 1 AND approvalsettingid = " & CInt(approvalsetting) & " and isprocessed = 0 and isvoid = 0 " & _
                            "AND APPRLEVELUNKID IN(" & ApproverLevel & ") "

            End Select

            dsList = objDataOperation.ExecQuery(strQ, "InitiatorList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("InitiatorList").Rows.Count > 0 Then
                Return True
            End If

            Return False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsInitiatorResponseAddedForGrievance", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [24-OCT-2019] -- End


    'Gajanan [13-July-2019] -- End


   'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Public Function GetPendingResolutionList(ByVal mintEmpID As Integer, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrFormID As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                   "ISNULL( " & _
                   "STUFF((SELECT " & _
                             "',' + CAST(greresolution_step_tran.resolutionsteptranunkid AS NVARCHAR(MAX)) " & _
                        "FROM greresolution_step_tran " & _
                        "WHERE isprocessed = 0 " & _
                        "AND isvoid = 0 " & _
                        "AND approvalsettingid = " & CInt(enGrievanceApproval.ReportingTo) & " " & _
                        "AND issubmitted = 0 " & _
                        "and approverempid  = " & mintEmpID & " " & _
                        "ORDER BY greresolution_step_tran.resolutionsteptranunkid " & _
                        "FOR XML PATH ('')) " & _
                   ", 1, 1, '' " & _
                   "), '') AS CSV "

            dsList = objDataOperation.ExecQuery(strQ, "ResolutionList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrFormID = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPendingResolutionList", mstrModuleName)
        End Try
        Return mstrFormID
    End Function

    'Gajanan [24-OCT-2019] -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "My Response")
            Language.setMessage(mstrModuleName, 3, "Commitee Response")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot add resolution to the selected grievance. Reason, Lower level approver has not given his resolution description.")
			Language.setMessage(mstrModuleName, 7, "Sorry, you cannot add resolution to the selected grievance. Reason, Another approver with same level has already given the resolution description.")
			Language.setMessage(mstrModuleName, 10, "Agree And Close")
			Language.setMessage(mstrModuleName, 11, "Disagree And Escalate To Next Level")
			Language.setMessage(mstrModuleName, 12, "Appeal")
			Language.setMessage(mstrModuleName, 13, "Pending")
			Language.setMessage(mstrModuleName, 14, "Approver Response")
            Language.setMessage(mstrModuleName, 16, "Responded")
            Language.setMessage(mstrModuleName, 17, "Disagree")
			Language.setMessage(mstrModuleName, 18, "Level")
			Language.setMessage(mstrModuleName, 10, "Agreed And Close")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
