﻿Imports eZeeCommonLib

Public Class clsgrievanceapprover_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsgrievanceapprover_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApprovertranunkid As Integer = -1
    Private mintApprovermasterunkid As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mintemployeeunkid As Integer = -1
    Private mdtTran As DataTable
    Private mintApprovalSetting As Integer

    Private minAuditUserid As Integer
    Private minAuditDate As DateTime
    Private minClientIp As String
    Private minloginemployeeunkid As Integer
    Private mstrHostName As String
    Private mstrFormName As String
    Private blnIsFromWeb As Boolean
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("GrievanceApproverTran")
        Dim col As DataColumn

        Try
            col = New DataColumn("approvertranunkid")
            col.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(col)

            col = New DataColumn("approverunkid")
            col.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(col)

            col = New DataColumn("approvername")
            col.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(col)

            col = New DataColumn("employeeunkid")
            col.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(col)

            col = New DataColumn("name")
            col.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(col)

            col = New DataColumn("departmentunkid")
            col.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(col)

            col = New DataColumn("departmentname")
            col.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(col)

            col = New DataColumn("jobunkid")
            col.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(col)

            col = New DataColumn("jobname")
            col.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(col)

            col = New DataColumn("AUD")
            col.DataType = System.Type.GetType("System.String")
            col.AllowDBNull = True
            col.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(col)

            col = New DataColumn("GUID")
            col.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(col)

            col = New DataColumn("IsCheck")
            col.DataType = System.Type.GetType("System.Boolean")
            col.DefaultValue = False
            mdtTran.Columns.Add(col)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Properties "

    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = value
        End Set
    End Property

    Public Property _Approvermasterunkid() As Integer
        Get
            Return mintApprovermasterunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovermasterunkid = value
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintemployeeunkid = value
        End Set
    End Property

    Public Property _ApprovalSetting() As Integer
        Get
            Return mintApprovalSetting
        End Get
        Set(ByVal value As Integer)
            mintApprovalSetting = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    Public Property _loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

#End Region

    Public Function InsertDelete_GrievanceApproverData(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "Select count(*) as 'Countemp' From greapprover_tran " & _
                                       " where approvermasterunkid = @approvermasterunkid AND employeeunkid = @empunkid AND greapprover_tran.isvoid = 0"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermasterunkid)
                                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)

                                Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "Count")
                                If Not dscount Is Nothing And CInt(dscount.Tables("Count").Rows(0)("Countemp")) > 0 Then Continue For

                                strQ = "INSERT INTO greapprover_tran ( " & _
                                            " approvermasterunkid " & _
                                            ", employeeunkid " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                        ") VALUES (" & _
                                            "  @approvermasterunkid " & _
                                            ", @employeeunkid " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            "); SELECT @@identity"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermasterunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(0))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)
                                mintemployeeunkid = CInt(.Item("employeeunkid").ToString())


                                If Insert_AtTranLog(objDataOperation, 1, mintApprovertranunkid, mintApprovermasterunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = " Update greapprover_tran set " & _
                                            " isvoid = 1,voiddatetime=GETDATE(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                                            " WHERE approvermasterunkid = @approvermasterunkid AND employeeunkid = @employeeunkid"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermasterunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 2, "UnAssign"))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintemployeeunkid = CInt(.Item("employeeunkid"))

                                If Insert_AtTranLog(objDataOperation, 3, mintemployeeunkid, mintApprovermasterunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_GrievanceApproverData; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer, ByVal intUnkid As Integer, ByVal masterUnkid As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO atgreapprover_tran ( " & _
             "  row_guid " & _
             ", approvertranunkid " & _
             ", approvermasterunkid " & _
             ", employeeunkid " & _
             ", audittype " & _
             ", auditdatetime " & _
             ", audituserunkid " & _
             ", loginemployeeunkid " & _
             ", ip " & _
             ", host " & _
             ", form_name " & _
             ", isweb " & _
           ") VALUES (" & _
              "  @row_guid " & _
             ", @approvertranunkid " & _
             ", @approvermasterunkid " & _
             ", @employeeunkid " & _
             ", @audittype " & _
             ", GETDATE()  " & _
             ", @audituserunkid " & _
             ", @loginemployeeunkid " & _
             ", @ip " & _
             ", @host " & _
             ", @form_name " & _
             ", @isweb )"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDoOps.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDoOps.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, masterUnkid)
            objDoOps.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintemployeeunkid)
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Public Sub GetApproverTran(ByVal mdtEmployeeAsonDate As Date, Optional ByVal intApproverunkid As Integer = 0, Optional ByVal xDataOperation As clsDataOperation = Nothing, Optional ByVal isblank As Boolean = False, Optional ByVal filterstring As String = "")

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            If isblank Then

                strQ = "SELECT " & _
                      "  approvertranunkid " & _
                      ", 0 as approverunkid" & _
                      ", '' as approvername" & _
                      ", 0 as employeeunkid " & _
                      ", '' as name" & _
                      ", 0 as Alloc.departmentunkid " & _
                      ", '' as  departmentname " & _
                      ", 0 as Jobs.jobunkid " & _
                      ", '' as  jobname " & _
                      ", '' as AUD ,'' as GUID,CAST(0 as BIT) as IsCheck " & _
                      " FROM greapprover_tran " & _
                      " Union All "
            End If

            strQ &= "SELECT " & _
              "  approvertranunkid " & _
              ", greapprover_tran.approvermasterunkid as approverunkid" & _
              ", '' as approvername" & _
              ", greapprover_tran.employeeunkid " & _
              ", isnull(Emp2.employeecode,'')+ '-' +  isnull(Emp2.firstname,'') + ' ' + isnull(Emp2.surname,'') as name" & _
              ", Alloc.departmentunkid " & _
              ", hrdepartment_master.name as departmentname " & _
              ", Jobs.jobunkid " & _
              ", hrjob_master.job_name as jobname " & _
              ", '' as AUD ,'' as GUID,CAST(0 as BIT) as IsCheck " & _
              " FROM greapprover_tran " & _
              " LEFT JOIN greapprover_master on greapprover_master.approvermasterunkid = greapprover_tran.approvermasterunkid " & _
              " LEFT JOIN hremployee_master Emp2 on greapprover_tran.employeeunkid = Emp2.employeeunkid " & _
              " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = Emp2.employeeunkid AND Alloc.rno = 1 " & _
              " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
              " LEFT JOIN " & _
              " ( " & _
              "    SELECT " & _
              "         jobunkid " & _
              "        ,jobgroupunkid " & _
              "        ,employeeunkid " & _
              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
              "    FROM hremployee_categorization_tran " & _
              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
              " ) AS Jobs ON Jobs.employeeunkid = Emp2.employeeunkid AND Jobs.rno = 1 " & _
              " LEFT JOIN hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid " & _
              " WHERE ISNULL(greapprover_tran.isvoid,0) = 0"

            If intApproverunkid > 0 Then
                strQ &= " AND greapprover_tran.approvermasterunkid = @approvermasterunkid"
                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            End If


            If filterstring <> "" Then
                strQ &= " AND" + filterstring
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")
            mdtTran = dsList.Tables("List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "UnAssign")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
