﻿'************************************************************************************************************************************
'Class Name : clsVoidReason.vb
'Purpose    :
'Date       :18/10/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsVoidReason
    Private Shared ReadOnly mstrModuleName As String = "clsVoidReason"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintReasonunkid As Integer
    Private mintCategoryunkid As Integer
    Private mstrAlias As String = String.Empty
    Private mstrReason As String = String.Empty
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reasonunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reasonunkid() As Integer
        Get
            Return mintReasonunkid
        End Get
        Set(ByVal value As Integer)
            mintReasonunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Categoryunkid() As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set alias
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Alias() As String
        Get
            Return mstrAlias
        End Get
        Set(ByVal value As String)
            mstrAlias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reason() As String
        Get
            Return mstrReason
        End Get
        Set(ByVal value As String)
            mstrReason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  reasonunkid " & _
              ", categoryunkid " & _
              ", alias " & _
              ", reason " & _
              ", isactive " & _
             "FROM cfvoidreason_master " & _
             "WHERE reasonunkid = @reasonunkid "

            objDataOperation.AddParameter("@reasonunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintReasonUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintreasonunkid = CInt(dtRow.Item("reasonunkid"))
                mintcategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mstralias = dtRow.Item("alias").ToString
                mstrreason = dtRow.Item("reason").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        "  hrmsConfiguration..cfvoidreason_master.reasonunkid " & _
                        " ,hrmsConfiguration..cfvoidreason_master.categoryunkid " & _
                        " ,hrmsConfiguration..cfvoidreason_master.alias " & _
                        " ,hrmsConfiguration..cfvoidreason_master.reason " & _
                        " ,hrmsConfiguration..cfvoidreason_master.isactive " & _
                        " ,CASE WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 1 THEN @Employee " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 2 THEN @Discipline " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 3 THEN @Assessment " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 4 THEN @Training " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 5 THEN @Payroll " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 6 THEN @Medical " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 7 THEN @Leave " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 8 THEN @TnA " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 9 THEN @Loan " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 10 THEN @Savings " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 11 THEN @Appplicant " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 12 THEN @Interview " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 13 THEN @Vacancy " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 14 THEN @Others " & _
                        "	        WHEN hrmsConfiguration..cfvoidreason_master.categoryunkid = 15 THEN @PaymentDisapprove " & _
                        "  END AS CatName " & _
                        "FROM hrmsConfiguration..cfvoidreason_master "

            If blnOnlyActive Then
                strQ &= " WHERE hrmsConfiguration..cfvoidreason_master.isactive = 1 "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 169, "Select Category"))
            objDataOperation.AddParameter("@Employee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 170, "Employee"))
            objDataOperation.AddParameter("@Discipline", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 171, "Discipline"))
            objDataOperation.AddParameter("@Assessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 172, "Assessment"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 173, "Training"))
            objDataOperation.AddParameter("@Payroll", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 174, "Payroll"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 175, "Medical"))
            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 176, "Leave"))
            objDataOperation.AddParameter("@TnA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 177, "TnA"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 178, "Loan"))
            objDataOperation.AddParameter("@Savings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 179, "Savings"))
            objDataOperation.AddParameter("@Others", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 180, "Others"))
            objDataOperation.AddParameter("@Appplicant", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 181, "Appplicant"))
            objDataOperation.AddParameter("@Interview", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 182, "Interview/Batch Scheduling"))
            objDataOperation.AddParameter("@Vacancy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 183, "Vacancy"))
            objDataOperation.AddParameter("@PaymentDisapprove", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 539, "Payment Disapprove"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfvoidreason_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintCategoryunkid, mstrReason) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Reason is already defined. Please define new Reason.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcategoryunkid.ToString)
            objDataOperation.AddParameter("@alias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstralias.ToString)
            objDataOperation.AddParameter("@reason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreason.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "INSERT INTO cfvoidreason_master ( " & _
              "  categoryunkid " & _
              ", alias " & _
              ", reason " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @categoryunkid " & _
              ", @alias " & _
              ", @reason " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintReasonunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfvoidreason_master", "reasonunkid", mintReasonunkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfvoidreason_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintCategoryunkid, mstrReason, mintReasonunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Reason is already defined. Please define new Reason.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreasonunkid.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcategoryunkid.ToString)
            objDataOperation.AddParameter("@alias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstralias.ToString)
            objDataOperation.AddParameter("@reason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreason.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "UPDATE cfvoidreason_master SET " & _
              "  categoryunkid = @categoryunkid" & _
              ", alias = @alias" & _
              ", reason = @reason" & _
              ", isactive = @isactive " & _
            "WHERE reasonunkid = @reasonunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfvoidreason_master", mintReasonunkid, "reasonunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfvoidreason_master", "reasonunkid", mintReasonunkid, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfvoidreason_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'StrQ = "DELETE FROM cfvoidreason_master " & _
            '"WHERE reasonunkid = @reasonunkid "

            strQ = "Update cfvoidreason_master set isactive = 0 " & _
            "WHERE reasonunkid = @reasonunkid "

            'Pinkal (12-Oct-2011) -- End


            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfvoidreason_master", "reasonunkid", intUnkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCategoryId As Integer, ByVal strReason As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  reasonunkid " & _
                      ", categoryunkid " & _
                      ", alias " & _
                      ", reason " & _
                      ", isactive  " & _
                    "FROM cfvoidreason_master " & _
                    "WHERE categoryunkid = @categoryunkid " & _
                    "AND reason = @reason "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If intUnkid > 0 Then
                strQ &= " AND reasonunkid <> @reasonunkid"
            End If

            objDataOperation.AddParameter("@categoryunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCategoryId)
            objDataOperation.AddParameter("@reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get List Of Void Reason </purpose>
    Public Function getComboList(Optional ByVal intCategoryId As Integer = -1, Optional ByVal blnFlag As Boolean = False, Optional ByVal strListName As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnFlag = True Then
                StrQ = " SELECT 0 AS Id,'' AS alias,@Select AS NAME UNION "
            End If

            StrQ &= " SELECT hrmsConfiguration..cfvoidreason_master.reasonunkid,hrmsConfiguration..cfvoidreason_master.alias,hrmsConfiguration..cfvoidreason_master.reason FROM hrmsConfiguration..cfvoidreason_master WHERE isactive = 1 "

            If intCategoryId > 0 Then
                StrQ &= " AND hrmsConfiguration..cfvoidreason_master.categoryunkid = @CatId "
                objDataOperation.AddParameter("@CatId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryId)
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "N/A"))

            dsList = objDataOperation.ExecQuery(StrQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Reason is already defined. Please define new Reason.")
			Language.setMessage(mstrModuleName, 2, "N/A")
			Language.setMessage("clsMasterData", 169, "Select Category")
			Language.setMessage("clsMasterData", 170, "Employee")
			Language.setMessage("clsMasterData", 171, "Discipline")
			Language.setMessage("clsMasterData", 172, "Assessment")
			Language.setMessage("clsMasterData", 173, "Training")
			Language.setMessage("clsMasterData", 174, "Payroll")
			Language.setMessage("clsMasterData", 175, "Medical")
			Language.setMessage("clsMasterData", 176, "Leave")
			Language.setMessage("clsMasterData", 177, "TnA")
			Language.setMessage("clsMasterData", 178, "Loan")
			Language.setMessage("clsMasterData", 179, "Savings")
			Language.setMessage("clsMasterData", 180, "Others")
			Language.setMessage("clsMasterData", 181, "Appplicant")
			Language.setMessage("clsMasterData", 182, "Interview/Batch Scheduling")
			Language.setMessage("clsMasterData", 183, "Vacancy")
			Language.setMessage("clsMasterData", 539, "Payment Disapprove")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class