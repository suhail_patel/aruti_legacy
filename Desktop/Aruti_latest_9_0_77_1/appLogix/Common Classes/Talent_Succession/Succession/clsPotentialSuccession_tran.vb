﻿Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

Public Class clsPotentialSuccession_tran
    Private Shared ReadOnly mstrModuleName As String = "clsPotentialSuccession_tran"
    Dim mstrMessage As String = ""
    Private mintPotentialSuccessionTranunkid As Integer = 0
    Private objDataOperation As clsDataOperation
    Private mintJobunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0

    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""

    Private mdtTran As DataTable
    Private mstrDatabaseName As String = ""

    Private mintProcessmstUnkid As Integer = -1

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _PotentialSuccessionTranunkid() As Integer
        Get
            Return mintPotentialSuccessionTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPotentialSuccessionTranunkid = value
            Call GetData()
        End Set
    End Property

    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _ProcessmstUnkid() As Integer
        Get
            Return mintProcessmstUnkid
        End Get
        Set(ByVal value As Integer)
            mintProcessmstUnkid = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("PotentialSuccessionEmployee")

        Try
            mdtTran.Columns.Add("potentialsuccessionemployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        "  potentialsuccessiontranunkid " & _
                        ", jobunkid " & _
                        ", employeeunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                   "FROM sucpotentialSuccession_tran " & _
                   "WHERE potentialsuccessiontranunkid  = @potentialsuccessiontranunkid "


            objDataOperation.AddParameter("@potentialsuccessiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPotentialSuccessionTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPotentialSuccessionTranunkid = CInt(dtRow.Item("potentialsuccessiontranunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, Optional ByVal intJobunkID As Integer = 0, Optional ByVal strFilter As String = "", Optional ByVal intEmployeeunkID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  potentialsuccessiontranunkid " & _
              ", jobunkid " & _
              ", ISNULL(employeeunkid, 0) AS employeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              "FROM sucpotentialSuccession_tran " & _
             "WHERE ISNULL(sucpotentialSuccession_tran.isvoid, 0 ) = 0 "

            If intJobunkID > 0 Then
                strQ &= " AND jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkID)
            End If

            If intEmployeeunkID > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkID)
            End If

            If strFilter.Length > 0 Then
                strQ &= strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
            objDataOperation.ClearParameters()

        Try
            strQ = " INSERT INTO sucpotentialSuccession_tran (" & _
                                "  jobunkid " & _
                                " ,employeeunkid " & _
                                " ,isvoid " & _
                                " ,voiddatetime " & _
                                " ,voiduserunkid " & _
                                " ,voidreason " & _
                            ") VALUES (" & _
                                "  @jobunkid " & _
                                " ,@employeeunkid " & _
                                " ,@isvoid " & _
                                " ,@voiddatetime " & _
                                " ,@voiduserunkid " & _
                                " ,@voidreason " & _
                            "); SELECT @@identity "

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPotentialSuccessionTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAT_Tran(objDataOperation, enAuditType.ADD, mintEmployeeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Delete(ByVal intPotentialSuccessionTranunkid As Integer, Optional ByVal blnUpdateDeptTraining As Boolean = False, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objsucscreening_process_master As New clssucscreening_process_master
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@potentialsuccessiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPotentialSuccessionTranunkid)

            strQ = "UPDATE " & mstrDatabaseName & "..sucpotentialSuccession_tran SET " & _
                                           " isvoid = 1 " & _
                                           " ,voiddatetime = @voiddatetime " & _
                                           " ,voiduserunkid = @voiduserunkid " & _
                                           " ,voidreason = @voidreason " & _
                                       "WHERE potentialsuccessiontranunkid = @potentialsuccessiontranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mintProcessmstUnkid > 0 Then
                objsucscreening_process_master._Isvoid = True
                objsucscreening_process_master._Voidreason = mstrVoidreason
                objsucscreening_process_master._Voiduserunkid = mintVoiduserunkid
                objsucscreening_process_master._Voiddatetime = mdtVoiddatetime
                objsucscreening_process_master._HostName = mstrHostName
                objsucscreening_process_master._ClientIP = mstrClientIP
                objsucscreening_process_master._FormName = mstrFormName
                objsucscreening_process_master._AuditUserId = mintAuditUserId
                objsucscreening_process_master._FromWeb = mblnIsWeb

                If objsucscreening_process_master.Delete(mintProcessmstUnkid.ToString(), objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If InsertAT_Tran(objDataOperation, enAuditType.DELETE, mintEmployeeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnUpdateDeptTraining Then
                If ChangeDepartmentTrainingCategory(intPotentialSuccessionTranunkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Delete , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAT_Tran(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal intEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@potentialsuccessiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPotentialSuccessionTranunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atsucpotentialSuccession_tran (" & _
                        "  tranguid " & _
                        ", potentialsuccessiontranunkid " & _
                        ",jobunkid " & _
                        ",employeeunkid " & _
                        ",audittypeid " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",host " & _
                        ",formname " & _
                        ",isweb " & _
                    ") VALUES (" & _
                        "  LOWER(NEWID()) " & _
                        ", @potentialsuccessiontranunkid " & _
                        ", @jobunkid " & _
                        ", @employeeunkid " & _
                        ", @audittypeid " & _
                        ", @audituserunkid " & _
                        ", getdate() " & _
                        ", @ip " & _
                        ", @host " & _
                        ", @formname " & _
                        ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAT_Tran, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    Public Function GetEmployeeList(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal xJobUnkid As Integer, _
                                    ByVal xPotentialSuccessionEmployeeGridList As List(Of String), _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblnAddSelect As Boolean = False, _
                                    Optional ByVal intEmployeeID As Integer = 0, _
                                    Optional ByVal intDeptID As Integer = 0, _
                                    Optional ByVal intSectionID As Integer = 0, _
                                    Optional ByVal intUnitID As Integer = 0, _
                                    Optional ByVal intGradeID As Integer = 0, _
                                    Optional ByVal intAccessID As Integer = 0, _
                                    Optional ByVal intClassID As Integer = 0, _
                                    Optional ByVal intCostCenterID As Integer = 0, _
                                    Optional ByVal intServiceID As Integer = 0, _
                                    Optional ByVal intJobID As Integer = 0, _
                                    Optional ByVal intPayPointID As Integer = 0, _
                                    Optional ByVal intBranchID As Integer = 0, _
                                    Optional ByVal intGenderID As Integer = 0, _
                                    Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                    Optional ByVal strFilterQuery As String = "", _
                                    Optional ByVal blnReinstatementDate As Boolean = False, _
                                    Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                    Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                    Optional ByVal strTempTableSelectQry As String = "", _
                                    Optional ByVal strTempTableJoinQry As String = "", _
                                    Optional ByVal strTempTableDropQry As String = "", _
                                    Optional ByVal blnApplyEmpExemptionFilter As Boolean = False, _
                                    Optional ByVal blnApplyEmpExemptionFilterForTerminated As Boolean = False _
                                   ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)


            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry

            If blnApplyEmpExemptionFilter = True Then
                strQ &= "SELECT AA.employeeunkid, SUM(TotalDays) AS TotalDays " & _
                        "INTO #TableExempt " & _
                        "FROM ( " & _
                        "SELECT A.datestranunkid " & _
                             ", A.employeeunkid " & _
                             ", A.effectivedate " & _
                             ", A.date1 " & _
                             ", A.date2 " & _
                             ", (A.date2 + 1) - A.date1 AS TotalDays " & _
                        "FROM ( " & _
                                "SELECT datestranunkid " & _
                                     ", hremployee_dates_tran.employeeunkid " & _
                                     ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                     ", CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS date1 " & _
                                     ", CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END AS date2 " & _
                                     ", DENSE_RANK() OVER ( PARTITION  BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC, hremployee_dates_tran.datestranunkid DESC ) AS ROWNO " & _
                                "FROM hremployee_dates_tran " & _
                                "WHERE isvoid = 0 " & _
                                      "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                      "AND date1 IS NOT NULL " & _
                                      "AND CONVERT(CHAR(8), effectivedate, 112) < @startdate " & _
                        " ) AS A " & _
                        " WHERE A.ROWNO = 1 " & _
                        "UNION " & _
                        "SELECT datestranunkid " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS date1 " & _
                             ", CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END AS date2 " & _
                             ", (CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END  + 1) - CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS TotalDays " & _
                        "FROM hremployee_dates_tran " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                              "AND date1 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                              "AND CONVERT(CHAR(8), date1, 112) " & _
                              "BETWEEN @startdate AND @enddate " & _
                        "UNION " & _
                        "SELECT datestranunkid " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS date1 " & _
                             ", CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END AS date2 " & _
                             ", (CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END  + 1) - CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS TotalDays " & _
                        "FROM hremployee_dates_tran " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                              "AND date2 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                              "AND CONVERT(CHAR(8), date2, 112) " & _
                              "BETWEEN @startdate AND @enddate " & _
                          " ) AS AA GROUP BY AA.employeeunkid "
            End If

            If mblnAddSelect = True Then

                strQ &= "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid,0 AS isapproved, ' ' + @Select AS EmpCodeName UNION ALL "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "  Select"))
            End If

            strQ += "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    "FROM " & strDBName & "hremployee_master "

            strQ &= strTempTableJoinQry

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid " & _
                    "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "	FROM " & strDBName & "prsalaryincrement_tran " & _
                    "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_shift_tran.employeeunkid " & _
                    "       ,hremployee_shift_tran.shiftunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                    "   FROM " & strDBName & "hremployee_shift_tran " & _
                    "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                    "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "

            If blnApplyEmpExemptionFilter = True Then
                strQ &= " LEFT JOIN #TableExempt ON hremployee_master.employeeunkid = #TableExempt.employeeunkid "
            End If


            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            '********************** DATA FOR DATES CONDITION ************************' --- END

            strQ &= " WHERE 1=1 "

            '================ Employee ID
            If intEmployeeID > 0 Then
                strQ += " AND hremployee_master.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If


            If blnApplyEmpExemptionFilter = True AndAlso blnApplyEmpExemptionFilterForTerminated = False Then
                strQ &= " AND (#TableExempt.employeeunkid IS NULL OR " & DateDiff(DateInterval.Day, xPeriodStart, xPeriodEnd.AddDays(1)) & " > #TableExempt.TotalDays ) "
            End If

            '================ PayPoint
            If intPayPointID > 0 Then
                strQ += " AND paypointunkid = @paypointunkid"
                objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPointID)
            End If

            '================ Department
            If intDeptID > 0 Then
                strQ += " AND T.departmentunkid = @departmentunkid"
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeptID)
            End If

            '================ Section
            If intSectionID > 0 Then
                strQ += " AND T.sectionunkid = @sectionunkid"
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionID)
            End If

            '================ Unit
            If intUnitID > 0 Then
                strQ += " AND T.unitunkid = @unitunkid"
                objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitID)
            End If

            '================ Job
            If intJobID > 0 Then
                strQ += " AND J.jobunkid = @jobunkid"
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            End If

            '================ Grade
            If intGradeID > 0 Then
                strQ += " AND G.gradeunkid = @gradeunkid"
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeID)
            End If

            '================ Access
            If intAccessID > 0 Then
                strQ += " AND accessunkid = @accessunkid"
                objDataOperation.AddParameter("@accessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccessID)
            End If

            '================ Class
            If intClassID > 0 Then
                strQ += " AND T.classunkid = @classunkid"
                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassID)
            End If

            '================ Service
            If intServiceID > 0 Then
                strQ += " AND serviceunkid = @serviceunkid"
                objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServiceID)
            End If

            '================ CostCenter
            If intCostCenterID > 0 Then
                strQ += " AND C.costcenterunkid = @costcenterunkid"
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterID)
            End If

            '================ Branch
            If intBranchID > 0 Then
                strQ += " AND T.stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBranchID)
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If intGenderID > 0 Then
                strQ += " AND hremployee_master.gender = @genderID "
                objDataOperation.AddParameter("@genderID", SqlDbType.Int, eZeeDataType.INT_SIZE, intGenderID)
            End If

            If xPotentialSuccessionEmployeeGridList IsNot Nothing AndAlso xPotentialSuccessionEmployeeGridList.ToArray.Length > 0 Then
                strQ &= " AND  hremployee_master.employeeunkid NOT in ( " & String.Join(",", xPotentialSuccessionEmployeeGridList.ToArray()) & " )  "
            End If

            strQ &= " AND  hremployee_master.employeeunkid NOT in ( select employeeunkid from sucscreening_process_master where isvoid   = 0  and jobunkid = " & xJobUnkid & " )  "

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If


            strQ &= strTempTableDropQry

            If blnApplyEmpExemptionFilter = True Then
                strQ &= " DROP TABLE #TableExempt "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    Public Function ChangeDepartmentTrainingCategory(ByVal intPotentialTalentTranunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            _PotentialSuccessionTranunkid = intPotentialTalentTranunkid

            If mintEmployeeunkid > 0 Then
                Dim dsDepartmentalTrainingneedList As DataSet = Nothing
                dsDepartmentalTrainingneedList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(-1, -1, CInt(enModuleReference.PDP), CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Successor_PDP), mintEmployeeunkid, False, objDataOperation)
                If IsNothing(dsDepartmentalTrainingneedList) = False AndAlso dsDepartmentalTrainingneedList.Tables(0).Rows.Count > 0 Then
                    For Each drrow As DataRow In dsDepartmentalTrainingneedList.Tables(0).Rows
                        objDepartmentaltrainingneed_master._Departmentaltrainingneedunkid = CInt(drrow("departmentaltrainingneedunkid"))

                        objDepartmentaltrainingneed_master._HostName = mstrHostName
                        objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                        objDepartmentaltrainingneed_master._FormName = mstrModuleName
                        objDepartmentaltrainingneed_master._AuditUserId = mintAuditUserId
                        objDepartmentaltrainingneed_master._AuditDate = DateTime.Now
                        objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                        objDepartmentaltrainingneed_master._Loginemployeeunkid = -1

                        If objDepartmentaltrainingneed_master._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                            objDepartmentaltrainingneed_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Individual_Development_Plan)
                            If objDepartmentaltrainingneed_master.Update(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    Next
                End If
                dsDepartmentalTrainingneedList.Dispose()
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ChangeDepartmentTrainingCategory; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 10, "  Select")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
