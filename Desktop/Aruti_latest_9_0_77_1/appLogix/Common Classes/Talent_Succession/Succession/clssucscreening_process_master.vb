﻿'************************************************************************************************************************************
'Class Name : clsscscreening_process_master.vb
'Purpose    :
'Date       :30-Oct-2020
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clssucscreening_process_master
    Private Shared ReadOnly mstrModuleName As String = "clsscscreening_process_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintProcessmstunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintJobunkid As Integer
    Private mintStageunkid As Integer
    Private mintScreenermstunkid As Integer
    Private mintAvgScreenermstunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    Private mdtQuestionnaire As DataTable = Nothing
    'Gajanan [17-Dec-2020] -- Start 
    Private mblnIsapproved As Boolean = False
    Private mblnIsdisapproved As Boolean = False
    Private mstrRemark As String = ""
    Private mdtApprovaldate As Date = Nothing
    'Gajanan [17-Dec-2020] -- End

    Private mblnIsmatch As Boolean = True

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _DtQuestionnaire() As DataTable
        Get
            Return mdtQuestionnaire
        End Get
        Set(ByVal value As DataTable)
            mdtQuestionnaire = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processmstunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Processmstunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintProcessmstunkid
        End Get
        Set(ByVal value As Integer)
            mintProcessmstunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stageunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Stageunkid() As Integer
        Get
            Return mintStageunkid
        End Get
        Set(ByVal value As Integer)
            mintStageunkid = value
        End Set
    End Property

    Public Property _Screenermstunkid() As Integer
        Get
            Return mintScreenermstunkid
        End Get
        Set(ByVal value As Integer)
            mintScreenermstunkid = value
        End Set
    End Property

    Public Property _AverageScreenermstunkid() As Integer
        Get
            Return mintAvgScreenermstunkid
        End Get
        Set(ByVal value As Integer)
            mintAvgScreenermstunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As String
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As String)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
    'Gajanan [17-Dec-2020] -- Start

    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    Public Property _Isdisapproved() As Boolean
        Get
            Return mblnIsdisapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsdisapproved = value
        End Set
    End Property

    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property
    'Gajanan [17-Dec-2020] -- End

    Public Property _Ismatch() As Boolean
        Get
            Return mblnIsmatch
        End Get
        Set(ByVal value As Boolean)
            mblnIsmatch = value
        End Set
    End Property
#End Region

#Region " Constuctor "
    Public Sub New()
        mdtQuestionnaire = New DataTable()
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("questionnaireunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtQuestionnaire.Columns.Add(dCol)

            dCol = New DataColumn("result")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtQuestionnaire.Columns.Add(dCol)

            dCol = New DataColumn("remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtQuestionnaire.Columns.Add(dCol)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  processmstunkid " & _
              ", jobunkid " & _
              ", employeeunkid " & _
              ", stageunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", ismatch " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isapproved " & _
              ", isdisapproved " & _
              ",remark " & _
             "FROM " & mstrDatabaseName & "..sucscreening_process_master " & _
             "WHERE processmstunkid = @processmstunkid and ismatch = 1 "

            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintProcessmstunkid = CInt(dtRow.Item("processmstunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintStageunkid = CInt(dtRow.Item("stageunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mstrRemark = CStr(dtRow.Item("remark"))
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mblnIsdisapproved = CBool(dtRow.Item("isdisapproved"))
                mblnIsmatch = CBool(dtRow.Item("ismatch"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal intJobid As Integer, _
                            ByVal strTableName As String, _
                            Optional ByVal blnIsMatchOnly As Boolean = True, _
                            Optional ByVal intEmployeeid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        'Gajanan [17-Dec-2020] -- Add [isapproved,isdisapproved,remark,approvaldate]

        Try
            strQ = "SELECT " & _
              "  sucscreening_process_master.processmstunkid " & _
              ", sucscreening_process_master.jobunkid " & _
              ", sucscreening_process_master.employeeunkid " & _
              ", sucscreening_process_master.stageunkid " & _
              ", sucscreening_process_master.userunkid " & _
              ", sucscreening_process_master.isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", CASE WHEN ISNULL(cfuser_master.firstname,'')+' '+ISNULL(cfuser_master.lastname,'') = '' THEN cfuser_master.username " & _
              "  ELSE ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') End as name " & _
              ", sucscreening_process_master.isapproved " & _
              ", sucscreening_process_master.isdisapproved " & _
              ", sucscreening_process_master.remark " & _
              ", sucscreening_process_master.approvaldate " & _
             "FROM " & mstrDatabaseName & "..sucscreening_process_master " & _
             "join hrmsConfiguration..cfuser_master on cfuser_master.userunkid  = sucscreening_process_master.userunkid " & _
             "WHERE sucscreening_process_master.isvoid = 0 and ismatch = 1 "

            If intJobid > 0 Then
                strQ &= " and jobunkid = @jobunkid "
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobid)
            End If

            If intEmployeeid > 0 Then
                strQ &= " and sucscreening_process_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)
            End If

            If blnIsMatchOnly Then
                strQ &= " and sucscreening_process_master.ismatch = 1 "
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SaveScreening(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If isExist(mintJobunkid, mintEmployeeunkid, mintProcessmstunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SaveScreening; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objsucscreening_process_tran As New clssucscreening_process_tran
        Dim objsucscreening_stages_tran As New clssucscreening_stages_tran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAvgScreenermstunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@ismatch", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmatch)

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
            'Gajanan [17-Dec-2020] -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved)
            objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdisapproved)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Gajanan [17-Dec-2020] -- End



            strQ = "INSERT INTO " & mstrDatabaseName & "..sucscreening_process_master ( " & _
              " employeeunkid " & _
              ", jobunkid " & _
              ", stageunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", ismatch " & _
              ", isapproved " & _
              ", isdisapproved " & _
              ", remark " & _
              ", approvaldate " & _
            ") VALUES (" & _
              " @employeeunkid " & _
              ", @jobunkid " & _
              ", @stageunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @ismatch " & _
              ", @isapproved " & _
              ", @isdisapproved " & _
              ", @remark " & _
              ", @approvaldate " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintProcessmstunkid = dsList.Tables(0).Rows(0).Item(0)

            objsucscreening_stages_tran._Employeeunkid = mintEmployeeunkid
            objsucscreening_stages_tran._Stageunkid = mintStageunkid
            objsucscreening_stages_tran._Isvoid = False
            objsucscreening_stages_tran._AuditUserId = mintAuditUserId
            objsucscreening_stages_tran._HostName = mstrHostName
            objsucscreening_stages_tran._ClientIP = mstrClientIP
            objsucscreening_stages_tran._FromWeb = mblnIsWeb
            objsucscreening_stages_tran._DatabaseName = mstrDatabaseName
            objsucscreening_stages_tran._Processmstunkid = mintProcessmstunkid
            objsucscreening_stages_tran._Screenermstunkid = mintScreenermstunkid
            objsucscreening_stages_tran._Jobunkid = mintJobunkid

            objsucscreening_stages_tran._FormName = mstrFormName

            If objsucscreening_stages_tran.SaveStageTran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objsucscreening_process_tran._Processmstunkid = mintProcessmstunkid
            objsucscreening_process_tran._Screenermstunkid = mintScreenermstunkid
            objsucscreening_process_tran._Isvoid = False
            objsucscreening_process_tran._AuditUserId = mintAuditUserId
            objsucscreening_process_tran._HostName = mstrHostName
            objsucscreening_process_tran._ClientIP = mstrClientIP
            objsucscreening_process_tran._FromWeb = mblnIsWeb
            objsucscreening_process_tran._DatabaseName = mstrDatabaseName
            objsucscreening_process_tran._FormName = mstrFormName

            For Each drow As DataRow In mdtQuestionnaire.Rows
                objsucscreening_process_tran._Questionnaireunkid = CInt(drow("questionnaireunkid").ToString())
                objsucscreening_process_tran._Result = CDbl(drow("result"))
                objsucscreening_process_tran._Remark = drow("remark").ToString()

                If objsucscreening_process_tran.SaveProcessTran(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintProcessmstunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objsucscreening_process_tran = Nothing
            objsucscreening_stages_tran = Nothing

        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objsucscreening_process_tran As New clssucscreening_process_tran
        Dim objsucscreening_stages_tran As New clssucscreening_stages_tran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAvgScreenermstunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)
            'Gajanan [17-Dec-2020] -- Start
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdisapproved.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Gajanan [17-Dec-2020] -- End

            'Gajanan [17-Dec-2020] -- Add[isapproved,isdisapproved,remark,approvaldate]

            strQ = "update  " & mstrDatabaseName & "..sucscreening_process_master set " & _
              " employeeunkid = @employeeunkid " & _
              ", stageunkid = @stageunkid " & _
              ", userunkid = @userunkid " & _
              ", isvoid = @isvoid " & _
              ", isapproved = @isapproved " & _
              ", isdisapproved = @isdisapproved " & _
              ", remark = @remark " & _
              ", approvaldate = @approvaldate " & _
              " where processmstunkid = @processmstunkid and jobunkid =@jobunkid and ismatch = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objsucscreening_stages_tran._Employeeunkid = mintEmployeeunkid
            objsucscreening_stages_tran._Stageunkid = mintStageunkid
            objsucscreening_stages_tran._Isvoid = False
            objsucscreening_stages_tran._AuditUserId = mintAuditUserId
            objsucscreening_stages_tran._HostName = mstrHostName
            objsucscreening_stages_tran._ClientIP = mstrClientIP
            objsucscreening_stages_tran._FromWeb = mblnIsWeb
            objsucscreening_stages_tran._DatabaseName = mstrDatabaseName
            objsucscreening_stages_tran._Processmstunkid = mintProcessmstunkid
            objsucscreening_stages_tran._Screenermstunkid = mintScreenermstunkid
            objsucscreening_stages_tran._Jobunkid = mintJobunkid
            objsucscreening_stages_tran._FormName = mstrFormName

            If objsucscreening_stages_tran.SaveStageTran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objsucscreening_process_tran._Processmstunkid = mintProcessmstunkid
            objsucscreening_process_tran._Screenermstunkid = mintScreenermstunkid
            objsucscreening_process_tran._Isvoid = False
            objsucscreening_process_tran._AuditUserId = mintAuditUserId
            objsucscreening_process_tran._HostName = mstrHostName
            objsucscreening_process_tran._ClientIP = mstrClientIP
            objsucscreening_process_tran._FromWeb = mblnIsWeb
            objsucscreening_process_tran._DatabaseName = mstrDatabaseName
            objsucscreening_process_tran._FormName = mstrFormName

            For Each drow As DataRow In mdtQuestionnaire.Rows
                objsucscreening_process_tran._Questionnaireunkid = CInt(drow("questionnaireunkid").ToString())
                objsucscreening_process_tran._Result = CDbl(drow("result"))
                objsucscreening_process_tran._Remark = drow("remark").ToString()

                If objsucscreening_process_tran.SaveProcessTran(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintProcessmstunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objsucscreening_process_tran = Nothing
            objsucscreening_stages_tran = Nothing

        End Try
    End Function

    Public Function Delete(ByVal strProcessmstUnkid As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objsucscreening_process_tran As New clssucscreening_process_tran
        Dim objsucscreening_stages_tran As New clssucscreening_stages_tran

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..sucscreening_process_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE processmstunkid in ( " & strProcessmstUnkid & " ) "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove Screener Tran Detail
            objsucscreening_stages_tran._Isvoid = True
            objsucscreening_stages_tran._Voidreason = mstrVoidreason
            objsucscreening_stages_tran._Voiduserunkid = mintVoiduserunkid
            objsucscreening_stages_tran._Voiddatetime = mdtVoiddatetime

            objsucscreening_stages_tran._HostName = mstrHostName
            objsucscreening_stages_tran._ClientIP = mstrClientIP
            objsucscreening_stages_tran._FormName = mstrFormName
            objsucscreening_stages_tran._AuditUserId = mintAuditUserId
            objsucscreening_stages_tran._FromWeb = mblnIsWeb


            If objsucscreening_stages_tran.Delete(strProcessmstUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove All Question
            objsucscreening_process_tran._Isvoid = True
            objsucscreening_process_tran._Voidreason = mstrVoidreason
            objsucscreening_process_tran._Voiduserunkid = mintVoiduserunkid
            objsucscreening_process_tran._Voiddatetime = mdtVoiddatetime

            objsucscreening_process_tran._HostName = mstrHostName
            objsucscreening_process_tran._ClientIP = mstrClientIP
            objsucscreening_process_tran._FormName = mstrFormName
            objsucscreening_process_tran._AuditUserId = mintAuditUserId
            objsucscreening_process_tran._FromWeb = mblnIsWeb

            If objsucscreening_process_tran.Delete(strProcessmstUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, strProcessmstUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objsucscreening_process_tran = Nothing
            objsucscreening_stages_tran = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intJobId As Integer, _
                            ByVal intEmployeeId As Integer, _
                            Optional ByRef ProcessMstunkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  processmstunkid " & _
              ", jobunkid " & _
              ", employeeunkid " & _
              ", stageunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & mstrDatabaseName & "..sucscreening_process_master " & _
             "WHERE isvoid = 0 " & _
            " AND employeeunkid = @employeeunkid " & _
            " AND jobunkid = @jobunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                ProcessMstunkid = CInt(dsList.Tables(0).Rows(0)("processmstunkid"))
            End If


            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Gajanan [17-Dec-2020] -- Start
    Public Function SetApproveDisApprove(ByVal intEmpId As Integer, ByVal intJobid As Integer, ByVal intProcessMstId As Integer, ByVal isApproved As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessMstId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)

            If isApproved Then
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            Else
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            End If


            strQ = "update  " & mstrDatabaseName & "..sucscreening_process_master set " & _
              "  userunkid = @userunkid " & _
              ", isapproved = @isapproved " & _
              ", isdisapproved = @isdisapproved " & _
              ", remark = @remark " & _
              ", approvaldate = getDate() " & _
              "  Where processmstunkid = @processmstunkid and employeeunkid = @employeeunkid and jobunkid = @jobunkid "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, intProcessMstId.ToString()) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SetApproveDisApprove; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SetBackToQulify(ByVal intJobid As Integer, ByVal strProcessMstId As String, Optional ByVal blnUpdateDeptTraining As Boolean = False, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)


            strQ = "update  " & mstrDatabaseName & "..sucscreening_process_master set " & _
              "  userunkid = @userunkid " & _
              ", isapproved = @isapproved " & _
              ", isdisapproved = @isdisapproved " & _
              ", remark = @remark " & _
              ", approvaldate = getDate() " & _
              "  Where processmstunkid in ( " & strProcessMstId & " )  "

            If intJobid > 0 Then
                strQ &= " and jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobid.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, strProcessMstId.ToString()) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnUpdateDeptTraining Then
                If ChangeDepartmentTrainingCategory(strProcessMstId, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [17-Dec-2020] -- End

    Public Function ChangeDepartmentTrainingCategory(ByVal strProcessMstId As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            _Processmstunkid() = strProcessMstId
            If mintEmployeeunkid > 0 Then
                Dim dsDepartmentalTrainingneedList As DataSet = Nothing
                dsDepartmentalTrainingneedList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(-1, -1, CInt(enModuleReference.PDP), CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Successor_PDP), mintEmployeeunkid, False, objDataOperation)
                If IsNothing(dsDepartmentalTrainingneedList) = False AndAlso dsDepartmentalTrainingneedList.Tables(0).Rows.Count > 0 Then
                    For Each drrow As DataRow In dsDepartmentalTrainingneedList.Tables(0).Rows
                        objDepartmentaltrainingneed_master._Departmentaltrainingneedunkid = CInt(drrow("departmentaltrainingneedunkid"))

                        objDepartmentaltrainingneed_master._HostName = mstrHostName
                        objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                        objDepartmentaltrainingneed_master._FormName = mstrModuleName
                        objDepartmentaltrainingneed_master._AuditUserId = mintUserunkid
                        objDepartmentaltrainingneed_master._AuditDate = DateTime.Now
                        objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                        objDepartmentaltrainingneed_master._Userunkid = mintUserunkid
                        objDepartmentaltrainingneed_master._Loginemployeeunkid = -1

                        If objDepartmentaltrainingneed_master._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                            objDepartmentaltrainingneed_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Individual_Development_Plan)
                            If objDepartmentaltrainingneed_master.Update(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    Next
                End If
                dsDepartmentalTrainingneedList.Dispose()
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SetBackToQulify; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      ByVal strProcessmstUnkid As String) As Boolean
        Dim StrQ As String = ""

        Try
            'Gajanan [17-Dec-2020] -- Add [isapproved,isdisapproved,remark,approvaldate]

            StrQ = "INSERT INTO atsucscreening_process_master ( " & _
                    "  tranguid " & _
                    ", processmstunkid " & _
                    ", jobunkid " & _
                    ", employeeunkid " & _
                    ", stageunkid " & _
                    ", isapproved " & _
                    ", isdisapproved " & _
                    ", ismatch " & _
                    ", remark " & _
                    ", approvaldate " & _
                    ", userunkid " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", processmstunkid " & _
                    ", jobunkid " & _
                    ", employeeunkid " & _
                    ", stageunkid " & _
                    ", isapproved " & _
                    ", isdisapproved " & _
                    ", ismatch " & _
                    ", remark " & _
                    ", approvaldate " & _
                    ", @userunkid " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    " From sucscreening_process_master where processmstunkid in ( " & strProcessmstUnkid & " ) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function DeleteScreenerTranDetails(ByVal strProcessmstUnkid As String, ByVal intScreenerMstUnkId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objsucscreening_process_tran As New clssucscreening_process_tran
        Dim objsucscreening_stages_tran As New clssucscreening_stages_tran

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            'Remove Screener Tran Detail
            objsucscreening_stages_tran._Isvoid = True
            objsucscreening_stages_tran._Voidreason = mstrVoidreason
            objsucscreening_stages_tran._Voiduserunkid = mintVoiduserunkid
            objsucscreening_stages_tran._Voiddatetime = mdtVoiddatetime

            objsucscreening_stages_tran._HostName = mstrHostName
            objsucscreening_stages_tran._ClientIP = mstrClientIP
            objsucscreening_stages_tran._FormName = mstrFormName
            objsucscreening_stages_tran._AuditUserId = mintAuditUserId
            objsucscreening_stages_tran._FromWeb = mblnIsWeb


            If objsucscreening_stages_tran.Delete(strProcessmstUnkid, objDataOperation, intScreenerMstUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove All Question
            objsucscreening_process_tran._Isvoid = True
            objsucscreening_process_tran._Voidreason = mstrVoidreason
            objsucscreening_process_tran._Voiduserunkid = mintVoiduserunkid
            objsucscreening_process_tran._Voiddatetime = mdtVoiddatetime

            objsucscreening_process_tran._HostName = mstrHostName
            objsucscreening_process_tran._ClientIP = mstrClientIP
            objsucscreening_process_tran._FormName = mstrFormName
            objsucscreening_process_tran._AuditUserId = mintAuditUserId
            objsucscreening_process_tran._FromWeb = mblnIsWeb

            If objsucscreening_process_tran.Delete(strProcessmstUnkid, objDataOperation, intScreenerMstUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteScreenerTranDetails; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objsucscreening_process_tran = Nothing
            objsucscreening_stages_tran = Nothing
        End Try
    End Function

    Public Function IsSuccessionStarted(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal xJobid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT * " & _
                   " FROM " & mstrDatabaseName & "..sucscreening_process_master where " & _
                   "  1 = 1  " & _
                   "  and isvoid= 0 "


            If xJobid > 0 Then
                strQ &= "and jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xJobid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count = 0 Then
                Return False
            Else
                Return True
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsSuccessionStartedForCycle; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function IsSuccessionStartedForEmployees(ByVal intJobId As Integer, _
                                                ByVal strEmployeeList As String, _
                                                Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT * " & _
                   " FROM " & mstrDatabaseName & "..sucscreening_process_master where " & _
                   "    isvoid= 0 " & _
                   "  and jobunkid = @jobunkid  " & _
                   "  and employeeunkid in (" & strEmployeeList & " ) "

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsSuccessionStartedForEmployees; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function UpdateProcessDataAsIsMatch(ByVal strProcessmstunkids As String, ByVal blnstatus As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            For Each intProcessunkid As Integer In strProcessmstunkids.Split(CChar(","))
                _Processmstunkid = intProcessunkid


                If blnstatus = False AndAlso mblnIsmatch = False Then
                    Continue For
                End If


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessunkid)

                strQ = "update " & mstrDatabaseName & "..sucscreening_process_master set "

                If blnstatus Then
                    strQ &= " ismatch = 1 "
                Else
                    strQ &= " ismatch = 0 "
                End If

                strQ &= " where processmstunkid = @processmstunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, intProcessunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateNominatedDataAsIsMatch(ByVal strNominatedUnkids As String, ByVal blnstatus As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            For Each intNominatedUnkid As Integer In strNominatedUnkids.Split(CChar(","))
                objEmployee_nomination_master._Nominationunkid = intNominatedUnkid

                If blnstatus = False AndAlso objEmployee_nomination_master._Ismatch = False Then
                    Continue For
                End If


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@nominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNominatedUnkid)

                strQ = "update " & mstrDatabaseName & "..hremployee_nomination_master set "

                If blnstatus Then
                    strQ &= " ismatch = 1 "
                Else
                    strQ &= " ismatch = 0 "
                End If

                strQ &= " where nominationunkid = @nominationunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objEmployee_nomination_master.InsertAuditTrails(objDataOperation, enAuditType.EDIT, intNominatedUnkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateNominatedDataAsIsMatch; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GetSuccessionProcessedDataAfterSettingChage(ByVal xDatabaseName As String, _
                                         ByVal xUserUnkid As Integer, _
                                         ByVal xYearUnkid As Integer, _
                                         ByVal xCompanyUnkid As Integer, _
                                         ByVal xPeriodStart As DateTime, _
                                         ByVal xPeriodEnd As DateTime, _
                                         ByVal xAsOnDate As Date, _
                                         Optional ByVal strListName As String = "List", _
                                         Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                        ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objsucsetting As New clssucsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim objPotentialSuccession_tran As New clsPotentialSuccession_tran
        Dim objsucpipeline_master As New clssucpipeline_master
        Dim mdicSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            mdicSetting = objsucsetting.GetSettingFromPeriod()

            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Succession.")
                Return Nothing
            End If


            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If


            strQ &= "DECLARE @UserId AS INT;SET @UserId = @Uid "

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL) Then

                strQ &= "IF OBJECT_ID('tempdb..#empqulification') IS NOT NULL DROP TABLE #empqulification " & _
                          "CREATE TABLE #empqulification ( employeeunkid int ); " & _
                        "INSERT INTO #empqulification " & _
                             "SELECT DISTINCT " & _
                                  "employeeunkid " & _
                             "FROM hremp_qualification_tran " & _
                             "LEFT JOIN cfcommon_master " & _
                                  "ON hremp_qualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid " & _
                                       "AND cfcommon_master.mastertype =  " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & "  " & _
                                       "AND cfcommon_master.isactive = 1 " & _
                             "WHERE hremp_qualification_tran.isvoid = 0 " & _
                             "GROUP BY qlevel " & _
                                       ",employeeunkid " & _
                             "HAVING COUNT(employeeunkid) > 0 "

                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION) Then
                    Select Case clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION
                        Case CInt(enComparison_Operator.LESS_THAN)
                            strQ &= "AND qlevel < (SELECT qlevel FROM cfcommon_master WHERE masterunkid = " & CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL)) & ") "
                        Case CInt(enComparison_Operator.LESS_THAN_EQUAL)
                            strQ &= "AND qlevel <= (SELECT qlevel FROM cfcommon_master WHERE masterunkid = " & CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL)) & ") "
                        Case CInt(enComparison_Operator.GREATER_THAN)
                            strQ &= "AND qlevel > (SELECT qlevel FROM cfcommon_master WHERE masterunkid = " & CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL)) & ") "
                        Case CInt(enComparison_Operator.GREATER_THAN_EQUAL)
                            strQ &= "AND qlevel >= (SELECT qlevel FROM cfcommon_master WHERE masterunkid = " & CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL)) & ") "
                        Case Else
                            strQ &= "AND qlevel = (SELECT qlevel FROM cfcommon_master WHERE masterunkid = " & CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL)) & ")  "
                    End Select
                End If
            End If


            strQ += "SELECT " & _
                      "A.* " & _
                      ",ISNULL(Emp_suspension.suspensiondays, 0) AS suspensiondays " & _
                    "FROM " & _
                    "( " & _
                    "SELECT " & _
                    "  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                    ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                    ", ISNULL(nominatedJob.jobunkid,0) AS nominatedjobunkid " & _
                    ", sucscreening_process_master.processmstunkid " & _
                    ", sucscreening_process_master.ismatch " & _
                    ", CAST(0 as BIT) AS ismanual " & _
                    "  FROM " & strDBName & "..sucscreening_process_master" & _
                    " join hremployee_nomination_master on hremployee_nomination_master.employeeunkid = sucscreening_process_master.employeeunkid and hremployee_nomination_master.jobunkid = sucscreening_process_master.jobunkid  " & _
                    " join hremployee_master on hremployee_nomination_master.employeeunkid = hremployee_master.employeeunkid " & _
                    " LEFT JOIN hrjob_master AS nominatedJob " & _
                    " ON nominatedJob.jobunkid = hremployee_nomination_master.jobunkid " & _
                    " AND nominatedJob.isactive = 1 " & _
                    " AND hremployee_nomination_master.isvoid = 0 "


            '********************** Succession Seeting Conditions ************************' --- START

            strQ &= "LEFT JOIN (SELECT " & _
                         " T.EmpId " & _
                         ",T.EOC " & _
                         ",T.LEAVING " & _
                         ",T.isexclude_payroll AS IsExPayroll " & _
                      "FROM (SELECT " & _
                                "employeeunkid AS EmpId " & _
                              ",date1 AS EOC " & _
                              ",date2 AS LEAVING " & _
                              ",effectivedate " & _
                              ",isexclude_payroll " & _
                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                           "FROM  " & strDBName & "..hremployee_dates_tran " & _
                           "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_TERMINATION & "') " & _
                           "AND isvoid = 0 " & _
                           "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS T " & _
                      "WHERE T.xNo = 1) AS TRM " & _
                      "ON TRM.EmpId = hremployee_master.employeeunkid " & _
                 "LEFT JOIN (SELECT " & _
                           "R.EmpId " & _
                         ",R.RETIRE " & _
                         ",R.isexclude_payroll AS IsExPayroll " & _
                      "FROM (SELECT " & _
                                "employeeunkid AS EmpId " & _
                              ",date1 AS RETIRE " & _
                              ",effectivedate " & _
                              ",isexclude_payroll " & _
                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                           "FROM " & strDBName & "..hremployee_dates_tran " & _
                           "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') " & _
                           "AND isvoid = 0 " & _
                           "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS R " & _
                      "WHERE R.xNo = 1) AS RET " & _
                      "ON RET.EmpId = hremployee_master.employeeunkid " & _
                 "LEFT JOIN (SELECT " & _
                           "RH.EmpId " & _
                         ",RH.REHIRE " & _
                      "FROM (SELECT " & _
                                "employeeunkid AS EmpId " & _
                              ",reinstatment_date AS REHIRE " & _
                              ",effectivedate " & _
                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                           "FROM " & strDBName & "..hremployee_rehire_tran " & _
                           "WHERE isvoid = 0 " & _
                           "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS RH " & _
                      "WHERE RH.xNo = 1) AS HIRE " & _
                      "ON HIRE.EmpId = hremployee_master.employeeunkid "


            strQ &= " WHERE 1=1 "

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) Then
                strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) & " "
            End If

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER) Then
                strQ &= " AND NOT EXISTS (SELECT  " & _
                        " employeeunkid " & _
                        ",sucscreening_stages_tran.jobunkid " & _
                        " FROM sucscreening_stages_tran " & _
                        " WHERE isvoid = 0 AND hremployee_nomination_master.jobunkid = sucscreening_stages_tran.jobunkid " & _
                        " AND sucscreening_stages_tran.employeeunkid = hremployee_nomination_master.employeeunkid " & _
                        " GROUP BY employeeunkid,jobunkid " & _
                        " HAVING count(employeeunkid) >= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER) & " ) "
            End If
            '================ Employee ID

            'objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xAsOnDate.AddDays(1)))

            strQ &= ") as A "

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL) Then
                strQ &= "LEFT JOIN #empqulification AS empqulification " & _
                        "    ON empqulification.employeeunkid = a.employeeunkid "
            End If

            strQ &= "LEFT JOIN (SELECT " & _
                  "sus.EmpId " & _
                ",sus.suspensiondays " & _
             "FROM (SELECT " & _
                       "employeeunkid AS EmpId " & _
                       ",SUM(DATEDIFF(DAY, date1,  " & _
                       " CASE " & _
                       "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                       "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                       " End " & _
                        " )) AS suspensiondays " & _
                  "FROM  " & strDBName & "hremployee_dates_tran " & _
                  "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                  "AND isvoid = 0 " & _
                  "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                  "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
             "ON Emp_suspension.EmpId = A.employeeunkid "

            strQ &= "LEFT JOIN " & _
                      "( " & _
                        "SELECT " & _
                           "sucscreening_process_master.employeeunkid " & _
                          ",sucscreening_process_master.processmstunkid " & _
                          ",sucscreening_process_master.jobunkid " & _
                        "FROM sucscreener_master AS SSM " & _
                          "JOIN sucscreening_stages_tran ON " & _
                            " SSM.screenermstunkid = sucscreening_stages_tran.screenermstunkid " & _
                          "JOIN sucscreening_process_master ON sucscreening_stages_tran.processmstunkid = sucscreening_process_master.processmstunkid " & _
                        "WHERE SSM.isvoid = 0 and sucscreening_process_master.isvoid = 0 and sucscreening_stages_tran.isvoid= 0 "

            strQ &= " AND SSM.mapuserunkid = @UserId " & _
                      ") AS D ON A.employeeunkid = D.employeeunkid and a.nominatedjobunkid = d.jobunkid "

            objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

            strQ &= "order by nominatedjobunkid "

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xAsOnDate.AddDays(1)))
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim ExpYearNo As String = String.Empty
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) Then
                ExpYearNo = mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO)
            End If
            If ExpYearNo.Length = 0 Then ExpYearNo = "0"
            If CInt(ExpYearNo) > 0 Then
                Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

                If strEmpIDs.Length > 0 Then


                    Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, xAsOnDate)

                    Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable

                    Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                                  Select New With { _
                                     Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                    , Key .employeename = e.Field(Of String)("employeename") _
                                    , Key .age = e.Field(Of Integer)("age") _
                                    , Key .exyr = e.Field(Of Integer)("exyr") _
                                    , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                    , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                    , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                    , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                    , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                    , Key .nominatedjobunkid = e.Field(Of Integer)("nominatedjobunkid") _
                                    , Key .processmstunkid = e.Field(Of Integer)("processmstunkid") _
                                    , Key .ismatch = e.Field(Of Boolean)("ismatch") _
                                    , Key .ismanual = e.Field(Of Boolean)("ismanual") _
                                    , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                    , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                    , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                    })

                    Dim intPrevEmp As Integer = 0
                    Dim dblTotDays As Double = 0

                    For Each drow In result

                        Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                        Dim EndDate As Date = xAsOnDate


                        Dim intEmpId As Integer = drow.employeeunkid
                        Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                        If intCount <= 0 Then 'No Dates record found                       
                            If drow.termination_from_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                            End If
                            If drow.termination_to_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                            End If
                            If drow.empl_enddate.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                            End If
                            dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                        ElseIf intCount = 1 Then
                            dblTotDays = drow.ServiceDays
                        Else
                            dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                        End If

                        For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                            drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                        Next


                    Next

                    If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) Then
                        dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) & " "
                    End If

                End If
            End If

            '=========== Performance Score Settings - Start =====================
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE) Then
                Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In objsucpipeline_master.GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO), mdicSetting(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid) Select (p.Item("employeeunkid").ToString)).ToArray())
                If strPerformanceScoreEmpIds Is Nothing Then strPerformanceScoreEmpIds = ""
                If strPerformanceScoreEmpIds.Trim.Length > 0 Then
                    dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
                    Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable
                    dsList.Tables.Remove(dsList.Tables(0))
                    dsList.Tables.Add(dtPerformanceScore)
                Else
                    dsList.Tables(0).Rows.Clear()
                End If
            End If
            '=========== Performance Score Settings - End =====================
            'S.SANDEEP |01-FEB-2021| -- END



            '============================ Add Extra Potential Talent Employee(s) -- Start
            Dim dsPotentialTalent As DataSet
            strQ = "SELECT " & _
                   " hremployee_master.employeeunkid AS employeeunkid " & _
                   ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                   ",0 AS age " & _
                   ",0 AS exyr " & _
                   ",'' AS appointeddate " & _
                   ",'' AS termination_from_date " & _
                   ",'' AS termination_to_date " & _
                   ",'' AS empl_enddate " & _
                   ",ISNULL(nominatedJob.jobunkid, 0) AS nominatedjobunkid " & _
                   ",sucscreening_process_master.processmstunkid " & _
                   ",CAST(1 as BIT) AS ismatch " & _
                   ",CAST(1 as BIT) AS ismanual " & _
                   ",0 AS suspensiondays " & _
                "FROM sucscreening_process_master " & _
                "JOIN sucpotentialSuccession_tran " & _
                     "ON sucpotentialSuccession_tran.employeeunkid = sucscreening_process_master.employeeunkid " & _
                          "AND sucpotentialSuccession_tran.jobunkid = sucscreening_process_master.jobunkid " & _
                "JOIN hremployee_master " & _
                     "ON sucpotentialSuccession_tran.employeeunkid = hremployee_master.employeeunkid " & _
                "LEFT JOIN hrjob_master AS nominatedJob " & _
                     "ON nominatedJob.jobunkid = sucpotentialSuccession_tran.jobunkid " & _
                          "AND nominatedJob.isactive = 1 " & _
                          "AND sucpotentialSuccession_tran.isvoid = 0 " & _
                "WHERE 1 = 1 "

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER) Then
                strQ &= "AND NOT EXISTS (SELECT " & _
                               "employeeunkid " & _
                             ",sucscreening_stages_tran.jobunkid " & _
                        "FROM sucscreening_stages_tran " & _
                        "WHERE isvoid = 0 " & _
                        "AND sucscreening_stages_tran.jobunkid = sucpotentialsuccession_tran.jobunkid " & _
                        "AND sucscreening_stages_tran.employeeunkid = sucpotentialsuccession_tran.employeeunkid " & _
                        "GROUP BY employeeunkid,jobunkid " & _
                        "HAVING COUNT(employeeunkid) >= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_SCREENER) & " ) "

            End If


            objDataOperation.ClearParameters()
            dsPotentialTalent = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsPotentialTalent.Tables(0).Rows.Count > 0 Then
                dsList.Merge(dsPotentialTalent.Tables(0))
            End If

            '============================ Add Extra Potential Talent Employee(s) -- End


            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSuccessionProcessedDataAfterSettingChage", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function GetNominatedProcessedDataAfterSettingChage(ByVal xDatabaseName As String, _
                                                               ByVal xCompanyUnkid As Integer, _
                                                               ByVal xPeriodStart As DateTime, _
                                                               ByVal xPeriodEnd As DateTime, _
                                                               ByVal xdtAsOnDate As DateTime, _
                                                               ByVal xYearUnkid As Integer, _
                                                               Optional ByVal strListName As String = "List", _
                                                               Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objsucsetting As New clssucsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)
        Dim objCommon_Master As New clsCommon_Master

        Dim objsucpipeline_master As New clssucpipeline_master
        Dim intQualificationLevel As Integer = -1

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            mdicSetting = objsucsetting.GetSettingFromPeriod()




            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Succession Setting Not Available.")
                Return Nothing
            End If



            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL) Then
                objCommon_Master._Masterunkid = CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL))
                intQualificationLevel = objCommon_Master._QualificationGrp_Level
            End If


            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If


            strQ += "SELECT DISTINCT" & _
                    "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ",  ISNULL(suspensiondays, 0) as suspensiondays " & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                    ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                    ", hremployee_nomination_master.nominationunkid " & _
                    ", hremployee_nomination_master.ismatch " & _
                    "  FROM hremployee_nomination_master " & _
                    " join hremployee_master on hremployee_nomination_master.employeeunkid = hremployee_master.employeeunkid "


            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION) Then
                strQ &= " LEFT JOIN hremp_qualification_tran " & _
                        "   ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN cfcommon_master " & _
                        "   ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid " & _
                        "   AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & " " & _
                        "   AND cfcommon_master.isactive = 1 "
            End If


            strQ &= "LEFT JOIN (SELECT " & _
                          "sus.EmpId " & _
                        ",sus.suspensiondays " & _
                     "FROM (SELECT " & _
                               "employeeunkid AS EmpId " & _
                               ",SUM(DATEDIFF(DAY, date1,  " & _
                               " CASE " & _
                               "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                               "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                               " End " & _
                                " )) AS suspensiondays " & _
                          "FROM  " & strDBName & "hremployee_dates_tran " & _
                          "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                          "AND isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                          "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
                     "ON Emp_suspension.EmpId = hremployee_master.employeeunkid "



            strQ &= "LEFT JOIN (SELECT " & _
                     " T.EmpId " & _
                     ",T.EOC " & _
                     ",T.LEAVING " & _
                     ",T.isexclude_payroll AS IsExPayroll " & _
                  "FROM (SELECT " & _
                            "employeeunkid AS EmpId " & _
                          ",date1 AS EOC " & _
                          ",date2 AS LEAVING " & _
                          ",effectivedate " & _
                          ",isexclude_payroll " & _
                          ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "FROM  " & strDBName & "..hremployee_dates_tran " & _
                       "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_TERMINATION & "') " & _
                       "AND isvoid = 0 " & _
                       "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS T " & _
                  "WHERE T.xNo = 1) AS TRM " & _
                  "ON TRM.EmpId = hremployee_master.employeeunkid " & _
             "LEFT JOIN (SELECT " & _
                       "R.EmpId " & _
                     ",R.RETIRE " & _
                     ",R.isexclude_payroll AS IsExPayroll " & _
                  "FROM (SELECT " & _
                            "employeeunkid AS EmpId " & _
                          ",date1 AS RETIRE " & _
                          ",effectivedate " & _
                          ",isexclude_payroll " & _
                          ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "FROM " & strDBName & "..hremployee_dates_tran " & _
                       "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') " & _
                       "AND isvoid = 0 " & _
                       "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS R " & _
                  "WHERE R.xNo = 1) AS RET " & _
                  "ON RET.EmpId = hremployee_master.employeeunkid " & _
             "LEFT JOIN (SELECT " & _
                       "RH.EmpId " & _
                     ",RH.REHIRE " & _
                  "FROM (SELECT " & _
                            "employeeunkid AS EmpId " & _
                          ",reinstatment_date AS REHIRE " & _
                          ",effectivedate " & _
                          ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "FROM " & strDBName & "..hremployee_rehire_tran " & _
                       "WHERE isvoid = 0 " & _
                       "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS RH " & _
                  "WHERE RH.xNo = 1) AS HIRE " & _
                  "ON HIRE.EmpId = hremployee_master.employeeunkid "





            strQ &= " WHERE 1=1  "




            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION) Then
                Select Case CInt(mdicSetting(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL_OPRATION))
                    Case CInt(enComparison_Operator.LESS_THAN)
                        strQ &= "and cfcommon_master.qlevel < @qlevel "
                    Case CInt(enComparison_Operator.LESS_THAN_EQUAL)
                        strQ &= "and cfcommon_master.qlevel <= @qlevel "
                    Case CInt(enComparison_Operator.GREATER_THAN)
                        strQ &= "and cfcommon_master.qlevel > @qlevel  "
                    Case CInt(enComparison_Operator.GREATER_THAN_EQUAL)
                        strQ &= "and cfcommon_master.qlevel >= @qlevel  "
                    Case Else
                        strQ &= "and cfcommon_master.qlevel = @qlevel  "
                End Select

                strQ &= " "
                objDataOperation.AddParameter("@qlevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualificationLevel)
            End If

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) Then
                strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) & " "
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xdtAsOnDate.AddDays(1)))

            dsList = objDataOperation.ExecQuery(strQ, strListName)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

            If strEmpIDs.Length > 0 Then

                Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, xdtAsOnDate)
                Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable
                Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                              Select New With { _
                                 Key .employeename = e.Field(Of String)("employeename") _
                                , Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                , Key .age = e.Field(Of Integer)("age") _
                                , Key .exyr = e.Field(Of Integer)("exyr") _
                                , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                , Key .nominationunkid = e.Field(Of Integer)("nominationunkid") _
                                , Key .ismatch = e.Field(Of Boolean)("ismatch") _
                                , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                })

                Dim intPrevEmp As Integer = 0
                Dim dblTotDays As Double = 0

                If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) Then

                    For Each drow In result

                        Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                        Dim EndDate As Date = xdtAsOnDate


                        Dim intEmpId As Integer = drow.employeeunkid
                        Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                        If intCount <= 0 Then 'No Dates record found                       
                            If drow.termination_from_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                            End If
                            If drow.termination_to_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                            End If
                            If drow.empl_enddate.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                            End If
                            dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                        ElseIf intCount = 1 Then
                            dblTotDays = drow.ServiceDays
                        Else
                            dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                        End If

                        For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                            drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                        Next
                    Next

                    dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) & " "

                End If
            End If


            '=========== Performance Score Settings - Start =====================

            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE) Then
                Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In objsucpipeline_master.GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clssucsettings_master.enSuccessionConfiguration.MIN_PERF_NO), mdicSetting(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid) Select (p.Item("employeeunkid").ToString)).ToArray())

                If strPerformanceScoreEmpIds.Length > 0 Then
                    dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
                    Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable

                    dsList.Tables.Remove(dsList.Tables(0))
                    dsList.Tables.Add(dtPerformanceScore)
                End If
            End If

            '=========== Performance Score Settings - End =====================

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)

            Return dsList


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNominatedProcessedDataAfterSettingChage", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function


    Public Function GetNominatedJobCountList(ByVal intMaxJobCount As Integer, Optional ByVal strListName As String = "List", _
                                         Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                        ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ &= "SELECT * from ( " & _
                        "SELECT " & _
                             "hremployee_nomination_master.jobunkid " & _
                           ",hrjob_master.job_name " & _
                           ",count(hremployee_nomination_master.jobunkid) as jobcount " & _
                        "FROM hremployee_nomination_master " & _
                        "LEFT JOIN hrjob_master " & _
                             "ON hremployee_nomination_master.jobunkid = hrjob_master.jobunkid " & _
                        "WHERE hrjob_master.isactive = 1 " & _
                        "and hremployee_nomination_master.isvoid = 0 " & _
                        "GROUP by hremployee_nomination_master.jobunkid,hrjob_master.job_name " & _
                        ") as  a " & _
                    "WHERE a.jobcount >= " & intMaxJobCount & " "

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNominatedJobCountList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This Screening Process is already defined. Please define new Screening Process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
