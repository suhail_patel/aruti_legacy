﻿'************************************************************************************************************************************
'Class Name : clstlsettings_master.vb
'Purpose    :
'Date       :01-Oct-2020
'Written By :Sandeep
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep
''' </summary>
Public Class clstlsettings_master
    Private Const mstrModuleName = "clstlsettings_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSettingunkid As Integer
    Private mintCycleunkid As Integer
    Private mintSettingkeyid As Integer
    Private mstrSetting_Value As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    Private mdicTalentSetting As Dictionary(Of enTalentConfiguration, String)


    Public Enum enTalentConfiguration
        PERF_SCORE = 1
        ANY_PERIOD = 2
        ALL_PERIOD = 3
        MIN_PERF_NO = 4
        MAX_AGE_NO = 5
        ALLOC_TYPE = 6
        EXP_YEAR_NO = 7
        MAX_SCREENER = 8
        MIN_SCREENER_REQ = 9
        SCREENING_GUIDELINE = 10
        TOTAL_QUESTION_WEIGHT = 11
        'Sohail (03 Nov 2020) -- Start
        'NMB Enhancement : # : add insruction textbox in qualifying setting it should be mandatory information.
        INSTRUCTION = 12
        'Sohail (03 Nov 2020) -- End
        MAX_DATA_DISPLAY = 13
    End Enum
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settingunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Settingunkid() As Integer
        Get
            Return mintSettingunkid
        End Get
        Set(ByVal value As Integer)
            mintSettingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cycleunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Cycleunkid() As Integer
        Get
            Return mintCycleunkid
        End Get
        Set(ByVal value As Integer)
            mintCycleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settingkeyid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Settingkeyid() As Integer
        Get
            Return mintSettingkeyid
        End Get
        Set(ByVal value As Integer)
            mintSettingkeyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set setting_value
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Setting_Value() As String
        Get
            Return mstrSetting_Value
        End Get
        Set(ByVal value As String)
            mstrSetting_Value = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _TalentSetting() As Dictionary(Of enTalentConfiguration, String)
        Set(ByVal value As Dictionary(Of enTalentConfiguration, String))
            mdicTalentSetting = value
        End Set
    End Property



#End Region

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", cycleunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..tlsettings_master " & _
             "WHERE settingunkid = @settingunkid "

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSettingunkid = CInt(dtRow.Item("settingunkid"))
                mintCycleunkid = CInt(dtRow.Item("cycleunkid"))
                mintSettingkeyid = CInt(dtRow.Item("settingkeyid"))
                mstrSetting_Value = dtRow.Item("setting_value").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetSettingFromPeriod(ByVal mintcycleunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Dictionary(Of enTalentConfiguration, String)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", cycleunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..tlsettings_master " & _
             "WHERE cycleunkid = @cycleunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcycleunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mdicTalentSetting = New Dictionary(Of enTalentConfiguration, String)
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim PERF_SCORE As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.PERF_SCORE)).FirstOrDefault()
                If IsNothing(PERF_SCORE) = False AndAlso CInt(PERF_SCORE("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.PERF_SCORE, PERF_SCORE.Field(Of String)("setting_value").ToString())
                End If

                Dim ANY_PERIOD As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.ANY_PERIOD)).FirstOrDefault()
                If IsNothing(ANY_PERIOD) = False AndAlso CBool(ANY_PERIOD("setting_value")) Then
                    mdicTalentSetting.Add(enTalentConfiguration.ANY_PERIOD, ANY_PERIOD.Field(Of String)("setting_value").ToString())
                End If

                Dim ALL_PERIOD As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.ALL_PERIOD)).FirstOrDefault()
                If IsNothing(ALL_PERIOD) = False AndAlso CBool(ALL_PERIOD("setting_value")) Then
                    mdicTalentSetting.Add(enTalentConfiguration.ALL_PERIOD, ALL_PERIOD.Field(Of String)("setting_value").ToString())
                End If


                Dim MIN_PERF_NO As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.MIN_PERF_NO)).FirstOrDefault()
                If IsNothing(MIN_PERF_NO) = False AndAlso CInt(MIN_PERF_NO("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.MIN_PERF_NO, MIN_PERF_NO.Field(Of String)("setting_value").ToString())
                End If


                Dim MAX_AGE_NO As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.MAX_AGE_NO)).FirstOrDefault()
                If IsNothing(MAX_AGE_NO) = False AndAlso CInt(MAX_AGE_NO("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.MAX_AGE_NO, MAX_AGE_NO.Field(Of String)("setting_value").ToString())
                End If

                Dim ALLOC_TYPE As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.ALLOC_TYPE)).FirstOrDefault()
                If IsNothing(ALLOC_TYPE) = False AndAlso CStr(ALLOC_TYPE("setting_value")).Length > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.ALLOC_TYPE, ALLOC_TYPE.Field(Of String)("setting_value").ToString())
                End If

                Dim EXP_YEAR_NO As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.EXP_YEAR_NO)).FirstOrDefault()
                If IsNothing(EXP_YEAR_NO) = False AndAlso CInt(EXP_YEAR_NO("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.EXP_YEAR_NO, EXP_YEAR_NO.Field(Of String)("setting_value").ToString())
                End If

                Dim MAX_SCREENER As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.MAX_SCREENER)).FirstOrDefault()
                If IsNothing(MAX_SCREENER) = False AndAlso CInt(MAX_SCREENER("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.MAX_SCREENER, MAX_SCREENER.Field(Of String)("setting_value").ToString())
                End If

                Dim MIN_SCREENER_REQ As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.MIN_SCREENER_REQ)).FirstOrDefault()
                If IsNothing(MIN_SCREENER_REQ) = False AndAlso CInt(MIN_SCREENER_REQ("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.MIN_SCREENER_REQ, MIN_SCREENER_REQ.Field(Of String)("setting_value").ToString())
                End If

                Dim SCREENING_GUIDELINE As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.SCREENING_GUIDELINE)).FirstOrDefault()
                If IsNothing(SCREENING_GUIDELINE) = False AndAlso CStr(SCREENING_GUIDELINE("setting_value")).Length > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.SCREENING_GUIDELINE, SCREENING_GUIDELINE.Field(Of String)("setting_value").ToString())
                End If

                Dim TOTAL_QUESTION_WEIGHT As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.TOTAL_QUESTION_WEIGHT)).FirstOrDefault()
                If IsNothing(TOTAL_QUESTION_WEIGHT) = False AndAlso CInt(TOTAL_QUESTION_WEIGHT("setting_value")) > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.TOTAL_QUESTION_WEIGHT, TOTAL_QUESTION_WEIGHT.Field(Of String)("setting_value").ToString())
                End If

                'Sohail (03 Nov 2020) -- Start
                'NMB Enhancement : # : add insruction textbox in qualifying setting it should be mandatory information.
                Dim INSTRUCTION As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.INSTRUCTION)).FirstOrDefault()
                If IsNothing(INSTRUCTION) = False AndAlso CStr(INSTRUCTION("setting_value")).Length > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.INSTRUCTION, INSTRUCTION.Field(Of String)("setting_value").ToString())
                End If
                'Sohail (03 Nov 2020) -- End

                Dim MAX_DATA_DISPLAY As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enTalentConfiguration.MAX_DATA_DISPLAY)).FirstOrDefault()
                If IsNothing(MAX_DATA_DISPLAY) = False AndAlso CStr(MAX_DATA_DISPLAY("setting_value")).Length > 0 Then
                    mdicTalentSetting.Add(enTalentConfiguration.MAX_DATA_DISPLAY, MAX_DATA_DISPLAY.Field(Of String)("setting_value").ToString())
                End If

                Return mdicTalentSetting

            End If

            Return Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSettingFromPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function GetSettingValueFromKey(ByVal mintcycleunkid As Integer, ByVal settingkeyid As enTalentConfiguration, Optional ByVal xDataOpr As clsDataOperation = Nothing) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              " setting_value " & _
             "FROM " & mstrDatabaseName & "..tlsettings_master " & _
             "WHERE cycleunkid = @cycleunkid " & _
             "and settingkeyid = @settingkeyid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcycleunkid)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(settingkeyid))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("setting_value").ToString()
            End If

            Return String.Empty


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSettingValueFromKey; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", cycleunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..tlsettings_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SaveTlSetting(ByVal xmdicSetting As Dictionary(Of enTalentConfiguration, String), ByVal xmintcycleId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        objDataOperation.ClearParameters()
        Try

            If xmdicSetting.Count > 0 Then
                For Each kvp As KeyValuePair(Of enTalentConfiguration, String) In xmdicSetting

                    mintCycleunkid = xmintcycleId
                    mintSettingkeyid = kvp.Key
                    mstrSetting_Value = kvp.Value

                    If isExist(kvp.Key, xmintcycleId, objDataOperation) Then
                        If Update(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                            Return False
                        End If
                    Else
                        If Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                            Return False
                        End If
                    End If

                Next
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SaveTlSetting; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intSettingTypeId As Integer, ByVal intCycleId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT setting_value,settingunkid " & _
             "FROM " & mstrDatabaseName & "..tlsettings_master " & _
             " where cycleunkid = @cycleunkid and settingkeyid= @settingkeyid  "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleId)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSettingTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintSettingunkid = CInt(dsList.Tables(0).Rows(0)("settingunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tlsettings_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrSetting_Value.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..tlsettings_master ( " & _
              "  cycleunkid " & _
              ", settingkeyid " & _
              ", setting_value" & _
            ") VALUES (" & _
              "  @cycleunkid " & _
              ", @settingkeyid " & _
              ", @setting_value" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSettingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tlsettings_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, mstrSetting_Value.Length, mstrSetting_Value.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..tlsettings_master SET " & _
                  "  cycleunkid = @cycleunkid" & _
                  ", settingkeyid = @settingkeyid" & _
                  ", setting_value = @setting_value " & _
                "WHERE settingunkid = @settingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tlsettings_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Settingunkid = intUnkid

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        Try

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "DELETE FROM " & mstrDatabaseName & "..tlsettings_master WHERE settingunkid = @settingunkid "

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Sandeep
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  settingunkid " & _
    '          ", cycleunkid " & _
    '          ", settingkeyid " & _
    '          ", setting_value " & _
    '         "FROM " & mstrDatabaseName & "..tlsettings_master " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND settingunkid <> @settingunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    Public Function isAllTalentSettingExist(ByVal intCycleId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objtlstages_master As New clstlstages_master
        Dim objtlscreener_master As New clstlscreener_master
        Dim objtlratings_master As New clstlratings_master
        Dim objtlquestionnaire_master As New clstlquestionnaire_master
        Dim objtlcycle_master As New clstlcycle_master
        Dim mdtSetting As New Dictionary(Of enTalentConfiguration, String)
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            objtlcycle_master._DatabaseName = mstrDatabaseName
            dsList = objtlcycle_master.GetList("cycle")
            If dsList.Tables("cycle").Rows.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, Talent cycle is not define,so please complete this to continue.")
                Return False
            End If


            If intCycleId > 0 Then

                mdtSetting = Me.GetSettingFromPeriod(intCycleId)
                If IsNothing(mdtSetting) OrElse mdtSetting.Count <= 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Talent qualification setting not define, so please complete this to continue.")
                    Return False
                End If

                objtlstages_master._DatabaseName = mstrDatabaseName
                dsList = objtlstages_master.GetList("stage", intCycleId)
                If dsList.Tables("stage").Rows.Count <= 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Talent stages are not define,so please complete this to continue.")
                    Return False
                End If

                objtlscreener_master._DatabaseName = mstrDatabaseName
                dsList = objtlscreener_master.GetList("screener", intCycleId)
                If dsList.Tables("screener").Rows.Count <= 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Talent screeners are not define,so please complete this to continue.")
                    Return False
                End If

                objtlratings_master._DatabaseName = mstrDatabaseName
                dsList = objtlratings_master.GetList("rating")
                If dsList.Tables("rating").Rows.Count <= 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Talent rating are not define,so please complete this to continue.")
                    Return False
                End If

                objtlquestionnaire_master._DatabaseName = mstrDatabaseName
                dsList = objtlquestionnaire_master.GetList("question", True, intCycleId)
                If dsList.Tables("question").Rows.Count <= 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, Talent questions are not define,so please complete this to continue.")
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isAllTalentSettingExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..attlsettings_master ( " & _
                    "  tranguid " & _
                    ", settingunkid " & _
                    ", cycleunkid " & _
                    ", settingkeyid " & _
                    ", setting_value " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @settingunkid " & _
                    ", @cycleunkid " & _
                    ", @settingkeyid " & _
                    ", @setting_value " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, mstrSetting_Value.Length, mstrSetting_Value.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Talent qualification setting not define, so please complete this to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Talent stages are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, Talent screeners are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Talent rating are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Talent questions are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Talent cycle is not define,so please complete this to continue.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class