﻿'************************************************************************************************************************************
'Class Name : clstlscreener_master.vb
'Purpose    :
'Date       :01-Oct-2020
'Written By :Sandeep
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Sandeep
''' </summary>
Public Class clstlpipeline_master
    Private Shared ReadOnly mstrModuleName As String = "clstlpipeline_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "

    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    Private mdtAsOnDate As DateTime = Nothing

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _DateAsOn() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    

#End Region

    Public Enum enEmailType
        INPROCESS_NOTIFICATIONAPPROVER = 1
        PROCESSDONE_NOTIFICATIONAPPROVER = 2
        APPROVED_EMPLOYEE = 3
        DISAPPROVED_EMPLOYEE = 4
        MOVE_TO_QUALIFY_EMPLOYEE = 5
        NOTIFICATIONSCREENER = 6
    End Enum

    Public Enum enTlApproveRejectFormType
        APPROVE_REJECT = 1
        SET_BACK_TO_QULIFY = 2
        DELETETALENTPROCESS = 3
        DELETEMANUALLYADDEDEMPLOYEE = 4
    End Enum

    'Public Sub GetData()
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  screenermstunkid " & _
    '          ", mapuserunkid " & _
    '          ", userunkid " & _
    '          ", isactive " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM " & mstrDatabaseName & "..tlscreener_master " & _
    '         "WHERE screenermstunkid = @screenermstunkid "

    '        objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            mintScreenermstunkid = CInt(dtRow.Item("screenermstunkid"))
    '            mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
    '            mintUserunkid = CInt(dtRow.Item("userunkid"))
    '            mblnIsactive = CBool(dtRow.Item("isactive"))
    '            mblnIsvoid = CBool(dtRow.Item("isvoid"))

    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub

    Public Function GetEmployeeList(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xCycleUnkid As Integer, _
                                     ByVal xNoImageString As String, _
                                     Optional ByVal xEmployeeunkid As Integer = -1, _
                                     Optional ByVal xScreeningType As enScreeningFilterType = enScreeningFilterType.ALL, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                     Optional ByVal strTempTableSelectQry As String = "", _
                                     Optional ByVal strTempTableJoinQry As String = "", _
                                     Optional ByVal strTempTableDropQry As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim objPotentialTalentTran As New clsPotentialTalent_Tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Cycle.")
                Return Nothing
            End If


            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry

            If xScreeningType = enScreeningFilterType.ALL OrElse xScreeningType = enScreeningFilterType.ONLYQUALIFIED Then

            strQ &= "DECLARE @UserId AS INT;SET @UserId = @Uid "

            strQ &= "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
                    "DROP TABLE #empimage " & _
                    "CREATE TABLE #empimage ( " & _
                        "photo image, " & _
                        "employeeunkid int " & _
                    "); " & _
                    " " & _
                    "INSERT INTO #empimage " & _
                         "SELECT " & _
                              "photo " & _
                            ",employeeunkid " & _
                         "FROM arutiimages..hremployee_images " & _
                         "WHERE isvoid = 0 "

            strQ += "SELECT " & _
                      "A.* " & _
                      ",ISNULL(Emp_suspension.suspensiondays, 0) AS suspensiondays " & _
                      ",empImage.photo AS empImage " & _
                      ",CASE WHEN ISNULL(D.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS IsDone " & _
                      ",'#FFFFFF' as color " & _
                          ",'' as ratingdesc " & _
                    "FROM " & _
                    "( " & _
                    "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  hrjob_master.job_name " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ", hrdepartment_master.name AS department " & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                    ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                    ",  0 as processmstunkid" & _
                    ", '' as empBaseImage " & _
                    ", 0 as IsManual " & _
                    ", 0 as isdisapproved " & _
                        ", 0 as potentialtalenttranunkid " & _
                    "  FROM " & strDBName & "hremployee_master "
                'Hemant (17 Jan 2022) -- [ratingdesc]
            strQ &= strTempTableJoinQry


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            '********************** Talent Seeting Conditions ************************' --- START

            strQ &= "JOIN (SELECT " & _
                    "emp_transfer.employeeunkid " & _
                    ",stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",emp_categorization.jobgroupunkid " & _
                    ",emp_categorization.jobunkid " & _
                    ",Emp_CCT.costcenterunkid " & _
                 "FROM (SELECT " & _
                           "Emp_TT.employeeunkid AS TrfEmpId " & _
                         ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                         ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                         ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                         ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                         ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                         ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                         ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                         ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                         ",Emp_TT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                 "JOIN (SELECT " & _
                           "Emp_CT.employeeunkid AS CatEmpId " & _
                         ",Emp_CT.jobgroupunkid " & _
                         ",Emp_CT.jobunkid " & _
                         ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                         ",Emp_CT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                      "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                 "JOIN (SELECT " & _
                           "Emp_CCT.employeeunkid AS CCTEmpId " & _
                         ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                         ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                      "WHERE Emp_CCT.isvoid = 0 " & _
                      "AND Emp_CCT.istransactionhead = 0 " & _
                      "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                      "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                 "WHERE emp_transfer.Rno = 1 " & _
                 "AND emp_categorization.Rno = 1 " & _
                 "AND emp_CCT.Rno = 1"


            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE) Then
                Dim allocation As String = mdicSetting(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE)
                Dim allocationType As String() = allocation.Split("|")

                Select Case CInt(allocationType(0))

                    Case CInt(enAllocation.BRANCH)
                        strQ &= "AND emp_transfer.stationunkid IN "

                    Case CInt(enAllocation.DEPARTMENT_GROUP)
                        strQ &= "AND emp_transfer.deptgroupunkid IN "

                    Case CInt(enAllocation.DEPARTMENT)
                        strQ &= "AND emp_transfer.departmentunkid IN "

                    Case CInt(enAllocation.SECTION_GROUP)
                        strQ &= "AND emp_transfer.sectiongroupunkid IN "

                    Case CInt(enAllocation.SECTION)
                        strQ &= "AND emp_transfer.sectionunkid IN "

                    Case CInt(enAllocation.UNIT_GROUP)
                        strQ &= "AND emp_transfer.unitgroupunkid IN "

                    Case CInt(enAllocation.UNIT)
                        strQ &= "AND emp_transfer.unitunkid IN "

                    Case CInt(enAllocation.JOB_GROUP)
                        strQ &= "AND emp_categorization.jobgroupunkid IN "

                    Case CInt(enAllocation.JOBS)
                        strQ &= "AND emp_categorization.jobunkid IN "

                    Case CInt(enAllocation.CLASS_GROUP)
                        strQ &= "AND emp_transfer.classgroupunkid IN "

                    Case CInt(enAllocation.CLASSES)
                        strQ &= "AND emp_transfer.classunkid IN "

                    Case CInt(enAllocation.COST_CENTER)
                        strQ &= "AND emp_CCT.costcenterunkid IN  "

                End Select

                strQ &= " (" & allocationType(1) & " ) "

            End If

            strQ &= " ) AS allocation " & _
                 "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
            "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

            strQ &= " WHERE 1=1 "

                If xEmployeeunkid > 0 Then
                    strQ &= " and hremployee_master.employeeunkid =@employeeunkid "
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
                End If

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) Then
                strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) & " "
            End If

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) Then
                strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
                        " employeeunkid " & _
                        " FROM tlscreening_stages_tran " & _
                        " WHERE isvoid = 0 " & _
                        " AND cycleunkid = " & xCycleUnkid & " " & _
                        " GROUP BY employeeunkid " & _
                        "	HAVING count(employeeunkid) >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) & " ) "
            End If
            '================ Employee ID

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            strQ &= strTempTableDropQry

            strQ &= ") as A " & _
                    "LEFT JOIN #empimage AS empImage " & _
                    "	ON empImage.employeeunkid = A.employeeunkid " & _
                    "LEFT JOIN (SELECT " & _
                          "sus.EmpId " & _
                        ",sus.suspensiondays " & _
                     "FROM (SELECT " & _
                               "employeeunkid AS EmpId " & _
                               ",SUM(DATEDIFF(DAY, date1,  " & _
                               " CASE " & _
                               "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                               "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                               " End " & _
                                " )) AS suspensiondays " & _
                          "FROM  " & strDBName & "hremployee_dates_tran " & _
                          "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                          "AND isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                          "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
                     "ON Emp_suspension.EmpId = A.employeeunkid "

            strQ &= "LEFT JOIN " & _
                      "( " & _
                        "SELECT " & _
                           "tlscreening_process_master.employeeunkid " & _
                          ",tlscreening_process_master.processmstunkid " & _
                        "FROM tlscreener_master AS TSM " & _
                          "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                            "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                          "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                        "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                          "AND TSM.mapuserunkid = @UserId " & _
                      ") AS D ON A.employeeunkid = D.employeeunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
            objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |01-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : FILTER ISSUE TALENT & SUCCESSION
            '=========== Performance Score Settings - Start =====================
            'If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) AndAlso mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
            '    Dim dtPerformanceScoreEmployeeList As DataTable
            '    dtPerformanceScoreEmployeeList = GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO), mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid)
            '    If dtPerformanceScoreEmployeeList IsNot Nothing AndAlso dtPerformanceScoreEmployeeList.Rows.Count > 0 Then
            '        Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In dtPerformanceScoreEmployeeList Select (p.Item("employeeunkid").ToString)).ToArray())
            '        If strPerformanceScoreEmpIds.Length > 0 Then
            '            dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
            '            Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable
            '            dsList.Tables.Remove(dsList.Tables(0))
            '            dsList.Tables.Add(dtPerformanceScore)
            '        End If
            '    End If
            'End If
            '=========== Performance Score Settings - End =====================
            'S.SANDEEP |01-FEB-2021| -- END            

'Hemant (08 Feb 2021) -- Start
            Dim ExpYearNo As String = String.Empty
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                ExpYearNo = mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO)
            End If
            If ExpYearNo.Length = 0 Then ExpYearNo = "0"
            If CInt(ExpYearNo) > 0 Then
                'Hemant (08 Feb 2021) -- End


            Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

            If strEmpIDs.Length > 0 Then


                Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, mdtAsOnDate)

                Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable

                Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                              Select New With { _
                                 Key .employeecode = e.Field(Of String)("employeecode") _
                                , Key .employeename = e.Field(Of String)("employeename") _
                                , Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                , Key .isapproved = e.Field(Of Boolean)("isapproved") _
                                , Key .EmpCodeName = e.Field(Of String)("EmpCodeName") _
                                , Key .job_name = e.Field(Of String)("job_name") _
                                , Key .age = e.Field(Of Integer)("age") _
                                , Key .exyr = e.Field(Of Integer)("exyr") _
                                , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                , Key .department = e.Field(Of String)("department") _
                                , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                })

                Dim intPrevEmp As Integer = 0
                Dim dblTotDays As Double = 0

                For Each drow In result

                    Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                    Dim EndDate As Date = mdtAsOnDate


                    Dim intEmpId As Integer = drow.employeeunkid
                    Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                    If intCount <= 0 Then 'No Dates record found                       
                        If drow.termination_from_date.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                        End If
                        If drow.termination_to_date.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                        End If
                        If drow.empl_enddate.Trim <> "" Then
                            EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                        End If
                        dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                    ElseIf intCount = 1 Then
                        dblTotDays = drow.ServiceDays
                    Else
                        dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                    End If

                    For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                        drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                    Next


                Next

                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                    dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) & " "
                End If

            End If
            End If 'Hemant (08 Feb 2021)

            'S.SANDEEP |01-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : FILTER ISSUE TALENT & SUCCESSION
            '=========== Performance Score Settings - Start =====================
                'S.SANDEEP |17-FEB-2021| -- START
                'If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) AndAlso mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
                Dim intPrdNo As Integer = 0
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Or mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Then
                        If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) Then
                            intPrdNo = mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO)
                        Else
                            intPrdNo = 1
                        End If
                    ElseIf mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                        intPrdNo = 0
                    End If
                End If
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
                    'S.SANDEEP |17-FEB-2021| -- END
                Dim dtPerformanceScoreEmployeeList As DataTable
                    'dtPerformanceScoreEmployeeList = GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO), mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid, objDataOperation)
                    dtPerformanceScoreEmployeeList = GetPerformanceScoreEmployeeList(xDatabaseName, intPrdNo, mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid, objDataOperation)
                If dtPerformanceScoreEmployeeList IsNot Nothing AndAlso dtPerformanceScoreEmployeeList.Rows.Count > 0 Then
                    Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In dtPerformanceScoreEmployeeList Select (p.Item("employeeunkid").ToString)).ToArray())
                    If strPerformanceScoreEmpIds.Length > 0 Then
                        dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
                        Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable
                        dsList.Tables.Remove(dsList.Tables(0))
                        dsList.Tables.Add(dtPerformanceScore)
                    End If
                Else
                    dsList.Tables(0).Rows.Clear()
                End If
            End If
            '=========== Performance Score Settings - End =====================
            'S.SANDEEP |01-FEB-2021| -- END

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)
            End If

            '============================ Add Extra Potential Talent Employee(s) -- Start
            If xScreeningType = enScreeningFilterType.ALL OrElse xScreeningType = enScreeningFilterType.ONLYMANUALLYADDED Then
                Dim strEmployeeList As String = ""
                If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    strEmployeeList = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())
                End If

            Dim dtPTEmployee As DataTable = objPotentialTalentTran.GetList("List", xCycleUnkid, IIf(strEmployeeList.Length > 0, " AND employeeunkid not in (" & strEmployeeList & ")", "")).Tables(0)
            Dim strPotentialTalentEmployeelist As String = String.Join(",", (From p In dtPTEmployee Select (p.Item("employeeunkid").ToString)).ToArray())
            If strPotentialTalentEmployeelist.Length > 0 Then
                Dim dsPotentialTalent As DataSet
                    objDataOperation.ClearParameters()

                    strQ = "DECLARE @PTUserId AS INT; " & _
                           " SET @PTUserId = @Uid "

                    strQ &= "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
                           "DROP TABLE #empimage " & _
                           "CREATE TABLE #empimage ( " & _
                               "photo image, " & _
                               "employeeunkid int " & _
                           "); " & _
                           " " & _
                           "INSERT INTO #empimage " & _
                                "SELECT " & _
                                     "photo " & _
                                   ",employeeunkid " & _
                                "FROM arutiimages..hremployee_images " & _
                                "WHERE isvoid = 0 "

                    strQ &= "SELECT " & _
                          "A.* " & _
                          ",empImage.photo AS empImage " & _
                          ",CASE WHEN ISNULL(D.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS IsDone " & _
                          ",'#FFFFFF' as color " & _
                          ",'' as ratingdesc " & _
                        "FROM " & _
                           "( " & _
                            "SELECT " & _
                        "   hremployee_master.employeecode AS employeecode " & _
                        ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                        ",  hremployee_master.employeeunkid As employeeunkid " & _
                        ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                        ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                        ",  hrjob_master.job_name " & _
                        ",  0 AS age " & _
                        ",  0 AS exyr" & _
                        ",  0 suspensiondays " & _
                        ", hrdepartment_master.name AS department " & _
                        ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                        ", '' AS termination_from_date " & _
                        ", '' AS termination_to_date " & _
                        ", '' AS empl_enddate " & _
                            ", 0 as processmstunkid" & _
                        ", '' as empBaseImage " & _
                        ", 1 as IsManual " & _
                            ", 0 as isdisapproved " & _
                            ", isnull(tlpotentialtalent_tran.potentialtalenttranunkid,0) as potentialtalenttranunkid " & _
                        ",'#FFFFFF' as color " & _
                            "  FROM tlpotentialtalent_tran " & _
                            "  LEFT JOIN  hremployee_master " & _
                            "     ON tlpotentialtalent_tran.employeeunkid = hremployee_master.employeeunkid "
                    'Hemant (17 Jan 2022) -- [ratingdesc]
                strQ &= strTempTableJoinQry

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If


                    If blnIncludeAccessFilterQry = True Then
                        If xUACQry.Trim.Length > 0 Then
                            strQ &= xUACQry
                        End If
                    End If


                strQ &= " JOIN (SELECT " & _
                        "emp_transfer.employeeunkid " & _
                        ",stationunkid " & _
                        ",deptgroupunkid " & _
                        ",departmentunkid " & _
                        ",sectiongroupunkid " & _
                        ",sectionunkid " & _
                        ",unitgroupunkid " & _
                        ",unitunkid " & _
                        ",teamunkid " & _
                        ",classgroupunkid " & _
                        ",classunkid " & _
                        ",emp_categorization.jobgroupunkid " & _
                        ",emp_categorization.jobunkid " & _
                        ",Emp_CCT.costcenterunkid " & _
                     "FROM (SELECT " & _
                               "Emp_TT.employeeunkid AS TrfEmpId " & _
                             ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                             ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                             ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                             ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                             ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                             ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                             ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                             ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                             ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                             ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                             ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                             ",Emp_TT.employeeunkid " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                          "WHERE isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                     "JOIN (SELECT " & _
                               "Emp_CT.employeeunkid AS CatEmpId " & _
                             ",Emp_CT.jobgroupunkid " & _
                             ",Emp_CT.jobunkid " & _
                             ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                             ",Emp_CT.employeeunkid " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                          "WHERE isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                          "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                     "JOIN (SELECT " & _
                               "Emp_CCT.employeeunkid AS CCTEmpId " & _
                             ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                             ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                          "WHERE Emp_CCT.isvoid = 0 " & _
                          "AND Emp_CCT.istransactionhead = 0 " & _
                          "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                          "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                     "WHERE emp_transfer.Rno = 1 " & _
                     "AND emp_categorization.Rno = 1 " & _
                     "AND emp_CCT.Rno = 1"

                strQ &= " ) AS allocation " & _
                    "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
               "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
               "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

                    strQ &= " WHERE 1=1 and tlpotentialtalent_tran.cycleunkid = @cycleunkid and tlpotentialtalent_tran.isvoid = 0 "

                strQ &= " and hremployee_master.employeeunkid in (" & strPotentialTalentEmployeelist & ") "

                    If xEmployeeunkid > 0 Then
                        strQ &= " and hremployee_master.employeeunkid = @employeeunkid "
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDataFilterQry.Trim.Length > 0 Then
                            strQ &= xDataFilterQry
                        End If
                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
                    Else
                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    End If


                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) Then
                    strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
                            " employeeunkid " & _
                            " FROM tlscreening_stages_tran " & _
                            " WHERE isvoid = 0 " & _
                            " AND cycleunkid = " & xCycleUnkid & " " & _
                            " GROUP BY employeeunkid " & _
                                "	HAVING count(employeeunkid) >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) & " ) "
                End If



                If strFilterQuery.Trim <> "" Then
                    strQ &= " AND " & strFilterQuery & " "
                End If

                    strQ &= " ) as A " & _
                          "LEFT JOIN #empimage AS empImage " & _
                          "	ON empImage.employeeunkid = A.employeeunkid " & _
                          "LEFT JOIN " & _
                          "( " & _
                            "SELECT " & _
                               "tlscreening_process_master.employeeunkid " & _
                              ",tlscreening_process_master.processmstunkid " & _
                            "FROM tlscreener_master AS TSM " & _
                              "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                                "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                              "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                            "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                              "AND TSM.mapuserunkid = @PTUserId " & _
                          ") AS D ON A.employeeunkid = D.employeeunkid "


                    objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
                    objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

                    If xScreeningType = enScreeningFilterType.ONLYMANUALLYADDED Then
                        dsList = objDataOperation.ExecQuery(strQ, strListName)
                    Else
                dsPotentialTalent = objDataOperation.ExecQuery(strQ, strListName)
                    End If

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                    If xScreeningType <> enScreeningFilterType.ONLYMANUALLYADDED Then
                dsList.Merge(dsPotentialTalent.Tables(0))
            End If

                End If
            End If

            '============================ Add Extra Potential Talent Employee(s) -- End


            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
            End If


            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

    'Public Function Insert() As Boolean
    '    If isExist(mintMapuserunkid - 1) Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This user is already defined. Please define new screener.")
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    objDataOperation.ClearParameters()
    '    Try
    '        objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

    '        strQ = "INSERT INTO " & mstrDatabaseName & "..tlscreener_master ( " & _
    '          "  mapuserunkid " & _
    '          ", userunkid " & _
    '          ", isactive " & _
    '          ", isvoid " & _
    '        ") VALUES (" & _
    '          "  @mapuserunkid " & _
    '          ", @userunkid " & _
    '          ", @isactive " & _
    '          ", @isvoid " & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintScreenermstunkid = dsList.Tables(0).Rows(0).Item(0)

    '        If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function


    'Public Function Update() As Boolean
    '    If isExist(mintMapuserunkid, mintScreenermstunkid) Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This user is already defined. Please define new screener.")
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    objDataOperation.ClearParameters()
    '    Try
    '        objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid.ToString)
    '        objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        strQ = "UPDATE " & mstrDatabaseName & "..tlscreener_master SET " & _
    '          "  mapuserunkid = @mapuserunkid" & _
    '          ", userunkid = @userunkid" & _
    '          ", isactive = @isactive" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE screenermstunkid = @screenermstunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    'If isUsed(intUnkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    objDataOperation.ClearParameters()
    '    Try
    '        strQ = "UPDATE " & mstrDatabaseName & "..tlscreener_master SET " & _
    '               "  isvoid = @isvoid" & _
    '               ", voiduserunkid = @voiduserunkid" & _
    '               ", voiddatetime = @voiddatetime" & _
    '               ", voidreason = @voidreason " & _
    '               "WHERE screenermstunkid = @screenermstunkid "

    '        objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
    '        If mdtVoiddatetime <> Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        End If

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "<Query>"

    '        objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Public Function isExist(ByVal intMapUserId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation
    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  screenermstunkid " & _
    '          ", mapuserunkid " & _
    '          ", userunkid " & _
    '          ", isactive " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM " & mstrDatabaseName & "..tlscreener_master " & _
    '         "WHERE isvoid = 0 " & _
    '         "AND mapuserunkid = @mapuserunkid "

    '        If intUnkid > 0 Then
    '            strQ &= " AND screenermstunkid <> @screenermstunkid"
    '        End If

    '        objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMapUserId)
    '        objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
    '    Dim StrQ As String = ""
    '    Try
    '        StrQ = "INSERT INTO attlscreener_master ( " & _
    '                "  tranguid " & _
    '                ", screenermstunkid " & _
    '                ", mapuserunkid " & _
    '                ", isactive " & _
    '                ", audittypeid " & _
    '                ", audtuserunkid " & _
    '                ", auditdatetime " & _
    '                ", formname " & _
    '                ", ip " & _
    '                ", host " & _
    '                ", isweb" & _
    '                ") VALUES (" & _
    '                "  LOWER(NEWID()) " & _
    '                ", @screenermstunkid " & _
    '                ", @mapuserunkid " & _
    '                ", @isactive " & _
    '                ", @audittypeid " & _
    '                ", @audtuserunkid " & _
    '                ", GETDATE() " & _
    '                ", @formname " & _
    '                ", @ip " & _
    '                ", @host " & _
    '                ", @isweb" & _
    '                ") "

    '        objDataOperation.ClearParameters()

    '        objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid.ToString)
    '        objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
    '        objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
    '        objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
    '        objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
    '        objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
    '        objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
    '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

    '        objDataOperation.ExecNonQuery(StrQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    Private Function convertImageToBase64(ByVal dr As DataRow, ByVal strNoimage As String) As Boolean
        Try
            If IsNothing(dr.Field(Of Byte())("empImage")) = False Then
                Dim bytes As Byte() = dr.Field(Of Byte())("empImage")
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                dr("empBaseImage") = "data:image/png;base64," & base64String
            Else
                dr("empBaseImage") = "data:image/png;base64," & strNoimage
            End If
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
        End Try
    End Function

    Public Function GetOtherStagesDetailList(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xCycleUnkid As Integer, _
                                     ByVal xStageUnkid As Integer, _
                                     ByVal xNoImageString As String, _
                                     ByVal xOrderId As Integer, _
                                     Optional ByVal xEmployeeunkid As Integer = -1, _
                                     Optional ByVal xScreeningType As enScreeningFilterType = enScreeningFilterType.ALL, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                     Optional ByVal strTempTableSelectQry As String = "", _
                                     Optional ByVal strTempTableJoinQry As String = "", _
                                     Optional ByVal strTempTableDropQry As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Dim objtlcycle_master As New clstlstages_master
        Dim intMinOrder, intMaxOrder, intMaxTolastOrder As Integer
        objtlcycle_master.Get_Min_Max_FlowOrder(intMinOrder, intMaxOrder, intMaxTolastOrder, xCycleUnkid)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Cycle.")
                Return Nothing
            End If


            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry


            strQ &= " SELECT	* INTO #TotScr " & _
                    " FROM (SELECT " & _
                    "       employeeunkid " & _
                    "      ,COUNT(*) AS TotalScreener " & _
                    "      FROM tlscreening_stages_tran " & _
                    "      WHERE isvoid = 0 " & _
                    "     AND cycleunkid = " & xCycleUnkid & " " & _
                    " GROUP BY employeeunkid) AS A "

            strQ &= "SELECT " & _
                         "* INTO #TotScore " & _
                    "FROM (SELECT " & _
                              "processmstunkid " & _
                            ",SUM(result) AS TotalScore " & _
                         "FROM tlscreening_process_tran " & _
                         "WHERE isvoid = 0 " & _
                         "GROUP BY processmstunkid) AS B "

            strQ &= "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  job.job_name " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ",  dept.name AS department " & _
                    ",  ISNULL(#TotScr.TotalScreener, 0) AS TotalScreener " & _
                    ",  tlscreening_process_master.stageunkid AS stageunkid " & _
                    ",  ISNULL(tlscreening_process_master.processmstunkid,0) as processmstunkid" & _
                    ", empImage.photo as empImage " & _
                    ", '' as empBaseImage " & _
                    ", tlscreening_process_master.isdisapproved " & _
                    ", 0 AS IsDone " & _
                    ", tlscreening_process_master.ismatch " & _
                    ",tlratings_master.color as color " & _
                    ",case WHEN ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid,0) > 0 THEN 1 ELSE 0 END as IsManual " & _
                    ",case WHEN ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid,0) > 0 THEN tlpotentialtalent_tran.potentialtalenttranunkid ELSE 0 END as potentialtalenttranunkid " & _
                    ",tlratings_master.description as ratingdesc " & _
                    " FROM " & strDBName & "..tlscreening_process_master " & _
                    " LEFT JOIN hremployee_master	ON hremployee_master.employeeunkid = tlscreening_process_master.employeeunkid " & _
                    " and tlscreening_process_master.cycleunkid = " & xCycleUnkid & " " & _
                    " and tlscreening_process_master.isvoid = 0 " & _
                    "LEFT JOIN #TotScore " & _
                    "   ON #TotScore.processmstunkid = tlscreening_process_master.processmstunkid " & _
                    "LEFT JOIN tlratings_master " & _
                    "   ON tlratings_master.stageunkid = tlscreening_process_master.stageunkid " & _
                    "   AND tlratings_master.isvoid = 0 " & _
                    "   AND tlscreening_process_master.isvoid = 0 " & _
                    " LEFT JOIN #TotScr ON #TotScr.employeeunkid = tlscreening_process_master.employeeunkid "
            'Hemant (17 Jan 2022) -- [ratingdesc]
            strQ &= " LEFT JOIN tlpotentialtalent_tran " & _
                    "   ON tlscreening_process_master.employeeunkid = tlpotentialtalent_tran.employeeunkid " & _
                    "   AND tlscreening_process_master.cycleunkid = tlpotentialtalent_tran.cycleunkid " & _
                    "   AND tlpotentialtalent_tran.isvoid = 0 "

            strQ &= strTempTableJoinQry


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid " & _
                    "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "	FROM " & strDBName & "prsalaryincrement_tran " & _
                    "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_shift_tran.employeeunkid " & _
                    "       ,hremployee_shift_tran.shiftunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                    "   FROM " & strDBName & "hremployee_shift_tran " & _
                    "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                    "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "

         

            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            '********************** Talent Seeting Conditions ************************' --- START

            strQ &= "JOIN (SELECT " & _
                      "emp_transfer.employeeunkid " & _
                    ",stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",emp_categorization.jobgroupunkid " & _
                    ",emp_categorization.jobunkid " & _
                    ",Emp_CCT.costcenterunkid " & _
                 "FROM (SELECT " & _
                           "Emp_TT.employeeunkid AS TrfEmpId " & _
                         ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                         ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                         ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                         ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                         ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                         ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                         ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                         ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                         ",Emp_TT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                 "JOIN (SELECT " & _
                           "Emp_CT.employeeunkid AS CatEmpId " & _
                         ",Emp_CT.jobgroupunkid " & _
                         ",Emp_CT.jobunkid " & _
                         ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                         ",Emp_CT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                      "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                 "JOIN (SELECT " & _
                           "Emp_CCT.employeeunkid AS CCTEmpId " & _
                         ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                         ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                      "WHERE Emp_CCT.isvoid = 0 " & _
                      "AND Emp_CCT.istransactionhead = 0 " & _
                      "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                      "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                 "WHERE emp_transfer.Rno = 1 " & _
                 "AND emp_categorization.Rno = 1 " & _
                 "AND emp_CCT.Rno = 1"


            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE) Then
                Dim allocation As String = mdicSetting(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE)
                Dim allocationType As String() = allocation.Split("|")

                Select Case CInt(allocationType(0))

                    Case CInt(enAllocation.BRANCH)
                        strQ &= "AND emp_transfer.stationunkid IN "

                    Case CInt(enAllocation.DEPARTMENT_GROUP)
                        strQ &= "AND emp_transfer.deptgroupunkid IN "

                    Case CInt(enAllocation.DEPARTMENT)
                        strQ &= "AND emp_transfer.departmentunkid IN "

                    Case CInt(enAllocation.SECTION_GROUP)
                        strQ &= "AND emp_transfer.sectiongroupunkid IN "

                    Case CInt(enAllocation.SECTION)
                        strQ &= "AND emp_transfer.sectionunkid IN "

                    Case CInt(enAllocation.UNIT_GROUP)
                        strQ &= "AND emp_transfer.unitgroupunkid IN "

                    Case CInt(enAllocation.UNIT)
                        strQ &= "AND emp_transfer.unitunkid IN "

                    Case CInt(enAllocation.JOB_GROUP)
                        strQ &= "AND emp_categorization.jobgroupunkid IN "

                    Case CInt(enAllocation.JOBS)
                        strQ &= "AND emp_categorization.jobunkid IN "

                    Case CInt(enAllocation.CLASS_GROUP)
                        strQ &= "AND emp_transfer.classgroupunkid IN "

                    Case CInt(enAllocation.CLASSES)
                        strQ &= "AND emp_transfer.classunkid IN "

                    Case CInt(enAllocation.COST_CENTER)
                        strQ &= "AND emp_CCT.costcenterunkid IN  "

                End Select

                strQ &= " (" & allocationType(1) & " ) "


            End If




            strQ &= " ) AS allocation " & _
                 "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
            "LEFT JOIN (SELECT " & _
                      "job_name " & _
                    ",jobunkid " & _
                 "FROM hrjob_master) AS job " & _
                 "ON job.jobunkid = allocation.jobunkid " & _
            "LEFT JOIN (SELECT " & _
                      "name " & _
                    ",departmentunkid " & _
                 "FROM hrdepartment_master) AS dept " & _
                 "ON dept.departmentunkid = allocation.departmentunkid " & _
            " LEFT JOIN arutiimages..hremployee_images as empImage ON empImage.employeeunkid = hremployee_master.employeeunkid and empImage.isvoid = 0 "

            strQ &= " WHERE 1=1 and tlscreening_process_master.ismatch = 1 and tlscreening_process_master.isvoid = 0 "

            If xOrderId = intMaxOrder Then
                strQ &= " and tlscreening_process_master.isapproved = 1 "
            ElseIf xOrderId = intMaxTolastOrder Then
                strQ &= " and tlscreening_process_master.isapproved <> 1 "
            Else
                strQ &= " and tlscreening_process_master.isapproved = 0 "
            End If


            If xEmployeeunkid > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            End If


            Select Case xScreeningType
                Case enScreeningFilterType.ONLYMANUALLYADDED
                    strQ &= " AND ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid, 0) > 0 "
                Case enScreeningFilterType.ONLYQUALIFIED
                    strQ &= " AND ISNULL(tlpotentialtalent_tran.potentialtalenttranunkid, 0) = 0 "
                Case Else
            End Select

            'If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) Then
            '    strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 <= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) & " "
            'End If

            If xStageUnkid > 0 AndAlso xOrderId <> intMaxOrder Then
                strQ &= " AND  tlscreening_process_master.stageunkid = @stageunkid"
                objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xStageUnkid)
            End If

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) Then
                strQ &= " AND #TotScr.totalscreener >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            strQ &= " AND ISNULL(#TotScore.TotalScore,0) / isnull(#TotScr.TotalScreener,0) BETWEEN tlratings_master.scorefrom AND tlratings_master.scoreto " & _
                    " DROP TABLE #TotScr " & _
                    " DROP TABLE #TotScore "


            strQ &= strTempTableDropQry


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)
            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOtherStagesDetailList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Function GetPerformanceScoreEmployeeList(ByVal xDatabaseName As String, _
                                                    ByVal intNoOfPeriod As Integer, _
                                                    ByVal dblMinScore As Double, _
                                                    ByVal xCompanyUnkid As Integer, _
                                                    ByVal xYearUnkid As Integer, _
                                                    Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataTable 'S.SANDEEP |17-FEB-2021| -- START {xYearUnkid} -- END
        Dim strQ As String = ""
        Dim dtTable As DataTable = Nothing
        Dim dsList As New DataSet
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Dim objConfig As New clsConfigOptions
        Try

            Dim strSettingValue As String = ""
            strSettingValue = objConfig.GetKeyValue(xCompanyUnkid, "IsCalibrationSettingActive", objDataOperation)
            If strSettingValue Is Nothing Then strSettingValue = ""
            Dim blnIsCalibrationSettingActive As Boolean = False
            If strSettingValue.Trim.Length > 0 Then blnIsCalibrationSettingActive = CBool(strSettingValue)

            objDataOperation.ClearParameters()

            'S.SANDEEP |17-FEB-2021| -- START
            'If intNoOfPeriod < 1 Then intNoOfPeriod = 1

            'strQ = " DECLARE @Tab AS TABLE(id int identity(1,1), pid int, yid int, dname nvarchar(255)) " & _
            '          " " & _
            '          " IF @NoOfPrd > 0 " & _
            '          " BEGIN " & _
            '          "     INSERT INTO @Tab (pid,yid,dname) " & _
            '          "            SELECT TOP " & intNoOfPeriod & " " & _
            '          "      periodunkid " & _
            '          "     ,cfcommon_period_tran.yearunkid " & _
            '          "     ,database_name " & _
            '          "     FROM " & xDatabaseName & "..cfcommon_period_tran " & _
            '          "     JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
            '          "     WHERE isactive = 1 /* AND statusid = 2 */ " & _
            '          "     AND modulerefid = 5 " & _
            '          "     ORDER BY cfcommon_period_tran.end_date DESC " & _
            '          " END " & _
            '          " ELSE " & _
            '          " BEGIN " & _
            '          "   INSERT INTO @Tab (pid,yid,dname) " & _
            '          "    SELECT " & _
            '          "     periodunkid " & _
            '          "     ,cfcommon_period_tran.yearunkid " & _
            '          "     ,database_name " & _
            '          "     FROM " & xDatabaseName & "..cfcommon_period_tran " & _
            '          "     JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
            '          "     WHERE isactive = 1 /* AND statusid = 2 */ " & _
            '          "     AND modulerefid = 5 " & _
            '          "     ORDER BY cfcommon_period_tran.end_date DESC " & _
            '          " END " & _
            '          " DECLARE @Counter INT, @Pid AS INT, @DName AS NVARCHAR(255), @Q AS NVARCHAR(MAX) " & _
            '          " SET @Counter=1;SET @Q = '' " & _
            '          " WHILE (@Counter <= (SELECT MAX(id) FROM @Tab)) " & _
            '          " BEGIN " & _
            '          "    SELECT @Pid = pid, @DName = dname FROM @Tab WHERE id = @Counter " & _
            '          "       IF (@Counter = (SELECT MAX(id) FROM @Tab)) " & _
            '          "       BEGIN "
            'If blnIsCalibrationSettingActive Then
            '    strQ &= "            SET @Q = @Q + 'SELECT DISTINCT employeeunkid,ISNULL(CASE WHEN isprocessed = 1 THEN calibrated_score ELSE 0 END,0) FROM '+ @DName +'..hrassess_compute_score_master WHERE isvoid = 0 AND periodunkid = '+ CAST(@Pid AS NVARCHAR(255)) +' AND ISNULL(CASE WHEN isprocessed = 1 THEN calibrated_score ELSE 0 END,0) >= ' + CAST(@MinScr AS NVARCHAR(MAX)) + CHAR(13) "
            'Else
            '    strQ &= "            SET @Q = @Q + 'SELECT DISTINCT employeeunkid,finaloverallscore FROM '+ @DName +'..hrassess_compute_score_master WHERE isvoid = 0 AND periodunkid = '+ CAST(@Pid AS NVARCHAR(255)) +' AND finaloverallscore >= ' + CAST(@MinScr AS NVARCHAR(MAX)) + CHAR(13) "
            'End If
            'strQ &= "       END " & _
            '          "       ELSE " & _
            '        "       BEGIN "
            'If blnIsCalibrationSettingActive Then
            '    strQ &= "           SET @Q = @Q + 'SELECT DISTINCT employeeunkid,ISNULL(CASE WHEN isprocessed = 1 THEN calibrated_score ELSE 0 END,0) FROM '+ @DName +'..hrassess_compute_score_master WHERE isvoid = 0 AND periodunkid = '+ CAST(@Pid AS NVARCHAR(255)) +' AND ISNULL(CASE WHEN isprocessed = 1 THEN calibrated_score ELSE 0 END,0) >= ' + CAST(@MinScr AS NVARCHAR(MAX)) + ' INTERSECT ' + CHAR(10) "
            'Else
            '    strQ &= "           SET @Q = @Q + 'SELECT DISTINCT employeeunkid,finaloverallscore FROM '+ @DName +'..hrassess_compute_score_master WHERE isvoid = 0 AND periodunkid = '+ CAST(@Pid AS NVARCHAR(255)) +' AND finaloverallscore >= ' + CAST(@MinScr AS NVARCHAR(MAX)) + ' INTERSECT ' + CHAR(10) "
            'End If
            'strQ &= "       END " & _
            '          "       SET @Counter  = @Counter  + 1 " & _
            '          " END " & _
            '          " EXEC(@Q) "

            'objDataOperation.AddParameter("@NoOfPrd", SqlDbType.Int, eZeeDataType.INT_SIZE, intNoOfPeriod)
            'objDataOperation.AddParameter("@MinScr", SqlDbType.Float, eZeeDataType.DECIMAL_SIZE, dblMinScore)

            'Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            strQ = "SELECT " & _
                   "     cfcommon_period_tran.periodunkid " & _
                   "    ,database_name " & _
                   "    ,cfcommon_period_tran.yearunkid " & _
                   "FROM " & xDatabaseName & "..cfcommon_period_tran " & _
                   "    JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                   "    JOIN sys.sysdatabases as db ON db.name = cffinancial_year_tran.database_name " & _
                   "WHERE isactive = 1 AND modulerefid = 5 AND iscmptperiod = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            strQ = ""
            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then

                If intNoOfPeriod <= 0 Then
                    If dsList.Tables(0).Select("yearunkid <> '" & xYearUnkid & "'").Length > 0 Then
                        intNoOfPeriod = dsList.Tables(0).Select("yearunkid <> '" & xYearUnkid & "'").Length
            Else
                        intNoOfPeriod = 1
                    End If
            End If

                strQ = "SELECT " & _
                       "    A.employeeunkid " & _
                       "FROM " & _
                       "( "
                Dim strInQLst As New List(Of String)
                Dim strInQ As String = ""

                For Each r As DataRow In dsList.Tables(0).Rows
            If blnIsCalibrationSettingActive Then
                        strInQLst.Add("SELECT DISTINCT " & _
                                      "    employeeunkid,CASE WHEN isprocessed = 1 THEN calibrated_score ELSE 0 END AS calibrated_score,cfcommon_period_tran.periodunkid " & _
                                      "FROM hrassess_compute_score_master " & _
                                      "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_compute_score_master.periodunkid " & _
                                      "WHERE isvoid = 0 AND CASE WHEN isprocessed = 1 THEN calibrated_score ELSE 0 END > 0 " & _
                                      "AND iscmptperiod = 0  AND yearunkid <> '" & xYearUnkid & "' ")
            Else
                        strInQLst.Add("SELECT DISTINCT " & _
                                      "    employeeunkid,finaloverallscore AS calibrated_score,cfcommon_period_tran.periodunkid " & _
                                      "FROM hrassess_compute_score_master " & _
                                      "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_compute_score_master.periodunkid " & _
                                      "WHERE isvoid = 0 AND finaloverallscore > 0 " & _
                                      " AND iscmptperiod = 0 AND yearunkid <> '" & xYearUnkid & "' ")
            End If
                Next

                If strInQLst.Count > 0 Then
                    strInQ = String.Join(" UNION ", strInQLst.ToArray())
                End If

                strQ &= strInQ

                strQ &= ") AS A " & _
                        "WHERE A.calibrated_score >= @MinScr " & _
                        "GROUP BY A.employeeunkid HAVING COUNT(1) >= @NoOfPrd "
            End If

            dsList = New DataSet

            If strQ.Trim.Length > 0 Then

            objDataOperation.AddParameter("@NoOfPrd", SqlDbType.Int, eZeeDataType.INT_SIZE, intNoOfPeriod)
            objDataOperation.AddParameter("@MinScr", SqlDbType.Float, eZeeDataType.DECIMAL_SIZE, dblMinScore)

                dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then dtTable = dsList.Tables(0)

            End If


            'S.SANDEEP |17-FEB-2021| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPerformanceScoreEmployeeList; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dtTable
    End Function

    Public Function GetTalentPoolDetailList(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xCycleUnkid As Integer, _
                                     ByVal xNoImageString As String, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                     Optional ByVal strTempTableSelectQry As String = "", _
                                     Optional ByVal strTempTableJoinQry As String = "", _
                                     Optional ByVal strTempTableDropQry As String = "" _
                                    ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Cycle.")
                Return Nothing
            End If


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry

          

            strQ &= " SELECT	* INTO #TotScr " & _
                    " FROM (SELECT " & _
                    "       employeeunkid " & _
                    "      ,COUNT(*) AS TotalScreener " & _
                    "      FROM tlscreening_stages_tran " & _
                    "      WHERE isvoid = 0 " & _
                    "     AND cycleunkid = " & xCycleUnkid & " " & _
                    " GROUP BY employeeunkid) AS A "

            strQ &= "SELECT " & _
                    "     * INTO #TotScore " & _
                    "FROM (SELECT " & _
                    "processmstunkid " & _
                    "       ,SUM(result) AS TotalScore " & _
                    "FROM tlscreening_process_tran " & _
                    "WHERE isvoid = 0 " & _
                    "    GROUP BY processmstunkid) AS B "

            strQ &= "SELECT " & _
                    "  ISNULL(tlscreening_process_master.processmstunkid,0) as processmstunkid " & _
                    ", tlscreening_process_master.ismatch " & _
                    ",  tlscreening_process_master.stageunkid AS stageunkid " & _
                    ",  '' as stage_name " & _
                    ",  hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  job.job_name " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ",  dept.name AS department " & _
                    ",  ISNULL(#TotScr.TotalScreener, 0) AS TotalScreener " & _
                    ", empImage.photo as empImage " & _
                    ", '' as empBaseImage " & _
                    ", 0 as IsGrp  " & _
                    ", ISNULL(hrsectiongroup_master.name, '') as Location " & _
                    ", ISNULL(RepTable.Reporting_Name,'') AS LineManager " & _
                    ", tlratings_master.description as status " & _
                    " FROM " & strDBName & "tlscreening_process_master " & _
                    " LEFT JOIN hremployee_master	ON hremployee_master.employeeunkid = tlscreening_process_master.employeeunkid " & _
                    " and tlscreening_process_master.cycleunkid = " & xCycleUnkid & " " & _
                    " and tlscreening_process_master.isvoid = 0 " & _
                    " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hremployee_master.sectiongroupunkid " & _
                    " LEFT JOIN #TotScr ON #TotScr.employeeunkid = tlscreening_process_master.employeeunkid " & _
                    " LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 hremployee_reportto.employeeunkid AS employeeunkid " & _
                    "		,reporttoemployeeunkid " & _
                    "       ,ISNULL(R_Emp.employeecode,'') AS Reporting_Code  " & _
                    "       ,ISNULL(R_Emp.firstname,'')+ ' ' + ISNULL(R_Emp.othername, '') +' '+ISNULL(R_Emp.surname,'') AS Reporting_Name  " & _
                    "       ,CASE WHEN hremployee_reportto.ishierarchy = 1 THEN 1 ELSE 0 END AS Default_Reporting  " & _
                    "   FROM hremployee_reportto " & _
                    "       LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid  " & _
                    "   WHERE ishierarchy=1 AND hremployee_reportto.isvoid=0 " & _
                    " ) AS RepTable " & _
                    " ON RepTable.employeeunkid=hremployee_master.employeeunkid "

            strQ &= strTempTableJoinQry


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid " & _
                    "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "	FROM " & strDBName & "prsalaryincrement_tran " & _
                    "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_shift_tran.employeeunkid " & _
                    "       ,hremployee_shift_tran.shiftunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                    "   FROM " & strDBName & "hremployee_shift_tran " & _
                    "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                    "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "

            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            '********************** Talent Seeting Conditions ************************' --- START

            strQ &= "JOIN (SELECT " & _
                      "emp_transfer.employeeunkid " & _
                    ",stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",emp_categorization.jobgroupunkid " & _
                    ",emp_categorization.jobunkid " & _
                    ",Emp_CCT.costcenterunkid " & _
                 "FROM (SELECT " & _
                           "Emp_TT.employeeunkid AS TrfEmpId " & _
                         ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                         ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                         ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                         ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                         ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                         ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                         ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                         ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                         ",Emp_TT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                 "JOIN (SELECT " & _
                           "Emp_CT.employeeunkid AS CatEmpId " & _
                         ",Emp_CT.jobgroupunkid " & _
                         ",Emp_CT.jobunkid " & _
                         ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                         ",Emp_CT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                      "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                 "JOIN (SELECT " & _
                           "Emp_CCT.employeeunkid AS CCTEmpId " & _
                         ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                         ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                      "WHERE Emp_CCT.isvoid = 0 " & _
                      "AND Emp_CCT.istransactionhead = 0 " & _
                      "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                      "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                 "WHERE emp_transfer.Rno = 1 " & _
                 "AND emp_categorization.Rno = 1 " & _
                 "AND emp_CCT.Rno = 1"


            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE) Then
                Dim allocation As String = mdicSetting(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE)
                Dim allocationType As String() = allocation.Split("|")

                Select Case CInt(allocationType(0))

                    Case CInt(enAllocation.BRANCH)
                        strQ &= "AND emp_transfer.stationunkid IN "

                    Case CInt(enAllocation.DEPARTMENT_GROUP)
                        strQ &= "AND emp_transfer.deptgroupunkid IN "

                    Case CInt(enAllocation.DEPARTMENT)
                        strQ &= "AND emp_transfer.departmentunkid IN "

                    Case CInt(enAllocation.SECTION_GROUP)
                        strQ &= "AND emp_transfer.sectiongroupunkid IN "

                    Case CInt(enAllocation.SECTION)
                        strQ &= "AND emp_transfer.sectionunkid IN "

                    Case CInt(enAllocation.UNIT_GROUP)
                        strQ &= "AND emp_transfer.unitgroupunkid IN "

                    Case CInt(enAllocation.UNIT)
                        strQ &= "AND emp_transfer.unitunkid IN "

                    Case CInt(enAllocation.JOB_GROUP)
                        strQ &= "AND emp_categorization.jobgroupunkid IN "

                    Case CInt(enAllocation.JOBS)
                        strQ &= "AND emp_categorization.jobunkid IN "

                    Case CInt(enAllocation.CLASS_GROUP)
                        strQ &= "AND emp_transfer.classgroupunkid IN "

                    Case CInt(enAllocation.CLASSES)
                        strQ &= "AND emp_transfer.classunkid IN "

                    Case CInt(enAllocation.COST_CENTER)
                        strQ &= "AND emp_CCT.costcenterunkid IN  "

                End Select

                strQ &= " (" & allocationType(1) & " ) "


            End If




            strQ &= " ) AS allocation " & _
                 "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
            "LEFT JOIN (SELECT " & _
                      "job_name " & _
                    ",jobunkid " & _
                 "FROM hrjob_master) AS job " & _
                 "ON job.jobunkid = allocation.jobunkid " & _
            "LEFT JOIN (SELECT " & _
                      "name " & _
                    ",departmentunkid " & _
                 "FROM hrdepartment_master) AS dept " & _
                 "ON dept.departmentunkid = allocation.departmentunkid " & _
            "LEFT JOIN arutiimages..hremployee_images as empImage ON empImage.employeeunkid = allocation.employeeunkid and empImage.isvoid = 0 " & _
           "LEFT JOIN #TotScore " & _
           "    ON #TotScore.processmstunkid = tlscreening_process_master.processmstunkid " & _
            "LEFT JOIN tlratings_master " & _
           "    ON tlratings_master.stageunkid = tlscreening_process_master.stageunkid and tlratings_master.isvoid = 0 "

            strQ &= " WHERE 1=1 and tlscreening_process_master.ismatch =1 "


          '  If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) Then
          '      strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 <= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) & " "
          '  End If

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) Then
                strQ &= " AND #TotScr.totalscreener >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If

            strQ &= " AND ISNULL(#TotScore.TotalScore,0) / isnull(#TotScr.TotalScreener,0) BETWEEN tlratings_master.scorefrom AND tlratings_master.scoreto " & _
                    " DROP TABLE #TotScr " & _
                    " DROP TABLE #TotScore "


            strQ &= strTempTableDropQry

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)
            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTalentPoolDetailList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

    Public Sub SendEmails(ByVal xEmailTypeId As Integer, _
                          ByVal intCompanyId As Integer, _
                          ByVal xCycleUnkid As Integer, _
                          ByVal intUserId As Integer, _
                          ByVal strPeriodName As String, _
                          ByVal strFormName As String, _
                          ByVal eMode As enLogin_Mode, _
                          ByVal strSenderAddress As String, _
                          ByVal intEmployeeunkid As Integer, _
                          ByVal xPeriodStart As Date, _
                          ByVal intYearUnkid As Integer)
        Try
            Dim objUsr As New clsUserAddEdit : Dim StrMessage As New System.Text.StringBuilder
            Dim objScreener As New clstlscreener_master
            Dim dsUserList As New DataSet
            Dim dtScreenerList As New DataTable
            Dim objEmp As New clsEmployee_Master
            Dim objReportTo As New clsReportingToEmployee
            Dim strSubject As String = ""
            Dim strToEmail As String = ""
            Dim blnSendReportingTo As Boolean = False

            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo

            Select Case xEmailTypeId
                Case enEmailType.INPROCESS_NOTIFICATIONAPPROVER, enEmailType.PROCESSDONE_NOTIFICATIONAPPROVER
                    dsUserList = objUsr.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToApproveRejectTalentPipelineEmployee, intYearUnkid)
                    strSubject = Language.getMessage(mstrModuleName, 6, "Notification for Approve/Disapprove of Talent Screening Process")
                    For Each drUser As DataRow In dsUserList.Tables(0).Rows
                        If drUser("Uemail").ToString().Trim().Length <= 0 Then Continue For

                        StrMessage = New System.Text.StringBuilder
                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)

                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                        StrMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")

                        If xEmailTypeId = enEmailType.INPROCESS_NOTIFICATIONAPPROVER Then
                            StrMessage.Append(Language.getMessage(mstrModuleName, 3, "This is to notify you that some emplyoees fall under qualify stage for talent screening process for the ") & Language.getMessage(mstrModuleName, 20, "#PeriodName#") & ". " & Language.getMessage(mstrModuleName, 4, "Please Login to Aruti in order to approve/disapprove them.") & " </span></p> ")
                        ElseIf xEmailTypeId = enEmailType.PROCESSDONE_NOTIFICATIONAPPROVER Then
                            StrMessage.Append(Language.getMessage(mstrModuleName, 5, "This is to notify you that talent screening process is done for the ") & Language.getMessage(mstrModuleName, 20, "#PeriodName#") & ". " & Language.getMessage(mstrModuleName, 9, "Please Login to Aruti in order to approve/disapprove them.") & " </span></p> ")
                        End If
                        StrMessage.Append(vbCrLf)
                        
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                        StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                        StrMessage.Append("</span></p>")

                        StrMessage.Append("<BR>" & Language.getMessage(mstrModuleName, 18, "Regards"))
                        StrMessage.Append("<BR> <BR>" & Language.getMessage(mstrModuleName, 19, "HR Department"))
                        StrMessage.Append("</BODY></HTML>")

                        Dim strEmailContent As String
                        strEmailContent = StrMessage.ToString()
                        strEmailContent = strEmailContent.Replace("#PeriodName#", "<B>" & strPeriodName & "</B>")

                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = drUser("Uemail").ToString().Trim()
                        objSendMail._Subject = strSubject
                        objSendMail._Message = strEmailContent
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = eMode
                        objSendMail._UserUnkid = intUserId
                        objSendMail._SenderAddress = strSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TALENT_MGT
                        objSendMail.SendMail(intCompanyId)
                        objSendMail = Nothing

                    Next
                Case enEmailType.APPROVED_EMPLOYEE, enEmailType.DISAPPROVED_EMPLOYEE, enEmailType.MOVE_TO_QUALIFY_EMPLOYEE
                    objEmp._Employeeunkid(xPeriodStart) = intEmployeeunkid
                    If objEmp._Email.Length <= 0 Then Exit Sub

                    strToEmail = objEmp._Email.ToString.Trim()

                    If xEmailTypeId = enEmailType.APPROVED_EMPLOYEE Then
                        strSubject = Language.getMessage(mstrModuleName, 7, "Notification for Approve of Talent Screening Process")
                    ElseIf xEmailTypeId = enEmailType.DISAPPROVED_EMPLOYEE Then
                        strSubject = Language.getMessage(mstrModuleName, 8, "Notification for Disapprove of Talent Screening Process")
                    ElseIf xEmailTypeId = enEmailType.MOVE_TO_QUALIFY_EMPLOYEE Then
                        strSubject = Language.getMessage(mstrModuleName, 13, "Notification for Moved Back to Qualify in Talent Screening Process")
                    End If
                    StrMessage = New System.Text.StringBuilder
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)

                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                    StrMessage.Append(Language.getMessage(mstrModuleName, 12, "Dear") & " " & "<b>" & info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString() & "</b></span></p>")

                    If xEmailTypeId = enEmailType.APPROVED_EMPLOYEE Then
                        StrMessage.Append(Language.getMessage(mstrModuleName, 10, "This is to notify  that  You are Approved in Approval Stage for Talent Screening Process.") & " </span></p>")
                    ElseIf xEmailTypeId = enEmailType.DISAPPROVED_EMPLOYEE Then
                        StrMessage.Append(Language.getMessage(mstrModuleName, 11, "This is to notify  that  You are Dispproved in  Approval Stage for Talent Screening Process.") & " </span></p>")
                    ElseIf xEmailTypeId = enEmailType.MOVE_TO_QUALIFY_EMPLOYEE Then
                        StrMessage.Append(Language.getMessage(mstrModuleName, 14, "This is to notify  that  You are Moved to Qualify Stage From Approved Stage in Talent Screening Process.") & " </span></p>")
                    End If

                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                    StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                    StrMessage.Append("</span></p>")
                    StrMessage.Append("<BR>" & Language.getMessage(mstrModuleName, 18, "Regards"))
                    StrMessage.Append("<BR> <BR>" & Language.getMessage(mstrModuleName, 19, "HR Department"))
                    StrMessage.Append("</BODY></HTML>")

                    'ReportingTo:
                    Dim objSendMail As New clsSendMail
                    objSendMail._ToEmail = strToEmail
                    objSendMail._Subject = strSubject
                    objSendMail._Message = StrMessage.ToString()
                    objSendMail._Form_Name = strFormName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = eMode
                    objSendMail._UserUnkid = intUserId
                    objSendMail._SenderAddress = strSenderAddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TALENT_MGT
                    objSendMail.SendMail(intCompanyId)
                    objSendMail = Nothing

                    'If xEmailTypeId = enEmailType.APPROVED_EMPLOYEE AndAlso blnSendReportingTo = False Then
                    '    objReportTo._EmployeeUnkid(xPeriodStart) = intEmployeeunkid

                    '    Dim dt As DataTable = objReportTo._RDataTable
                    '    Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                    '    If DefaultReportList.Count > 0 Then
                    '        strToEmail = DefaultReportList(0).Item("ReportingToEmail").ToString
                    '    End If
                    '    blnSendReportingTo = True
                    '    GoTo ReportingTo
                    'End If
                Case enEmailType.NOTIFICATIONSCREENER
                    dtScreenerList = objScreener.GetList("List", xCycleUnkid, True).Tables(0)
                    strSubject = Language.getMessage(mstrModuleName, 17, "Notification for Screening of Talent Screening Process")
                    For Each drScreener As DataRow In dtScreenerList.Rows
                        If drScreener("email").ToString().Trim().Length <= 0 Then Continue For

                        StrMessage = New System.Text.StringBuilder
                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)

                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                        StrMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " " & "<b>" & getTitleCase(drScreener.Item("name").ToString()) & "</b></span></p>")


                        StrMessage.Append(Language.getMessage(mstrModuleName, 15, "This is to notify you that some emplyoees fall under Potential stage for talent screening process for the ") & Language.getMessage(mstrModuleName, 20, "#PeriodName#") & ". " & Language.getMessage(mstrModuleName, 16, "Please Login to Aruti in order to Screening them.") & " </span></p>")
                        
                        StrMessage.Append(vbCrLf)

                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                        StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                        StrMessage.Append("</span></p>")
                        StrMessage.Append("<BR>" & Language.getMessage(mstrModuleName, 18, "Regards"))
                        StrMessage.Append("<BR> <BR>" & Language.getMessage(mstrModuleName, 19, "HR Department"))
                        StrMessage.Append("</BODY></HTML>")

                        Dim strEmailContent As String
                        strEmailContent = StrMessage.ToString()
                        strEmailContent = strEmailContent.Replace("#PeriodName#", "<B>" & strPeriodName & "</B>")

                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = drScreener("email").ToString().Trim()
                        objSendMail._Subject = strSubject
                        objSendMail._Message = strEmailContent
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = eMode
                        objSendMail._UserUnkid = intUserId
                        objSendMail._SenderAddress = strSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TALENT_MGT
                        objSendMail.SendMail(intCompanyId)
                        objSendMail = Nothing

                    Next

            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmails; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GetTalentScreenerWiseScreeningDetail(ByVal strTableName As String, _
                                       ByVal intCompanyId As Integer, _
                                       ByVal intYearId As Integer, _
                                       ByVal xEmployeeAsOnDate As String, _
                                       ByVal strUserAccessMode As String, _
                                       ByVal intEmployeeId As Integer, _
                                       ByVal intCycleId As Integer, _
                                       Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try

            strQ = "DECLARE @employeeunkid AS INT; " & _
                    "DECLARE @cycleunkid AS INT; " & _
                    "SET @employeeunkid = @empId " & _
                    "SET @cycleunkid = @cycleId "

            strQ &= "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                  "DROP TABLE #USR "
            strQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                strQ &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT DISTINCT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM hremployee_master AS AEM " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        "   WHERE AEM.employeeunkid = @employeeunkid " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN tlscreener_master AS CAM ON CAM.mapuserunkid = UPM.userunkid " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & intCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        "   AND CAM.isvoid = 0 AND CAM.isactive = 1 and CAM.cycleunkid = @cycleunkid " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    strQ &= " INTERSECT "
                End If
            Next

            strQ &= ") AS Fl "


    
            strQ &= "IF OBJECT_ID('tempdb..#TotScr') IS NOT NULL DROP TABLE #TotScr " & _
                    "SELECT " & _
                         "* INTO #TotScr " & _
                    "FROM (SELECT " & _
                            "tlscreening_stages_tran.processmstunkid " & _
                            ",tlscreening_process_master.employeeunkid " & _
                            ",tlscreening_process_master.cycleunkid " & _
                            ",tlscreener_master.mapuserunkid " & _
                         "FROM tlscreening_stages_tran " & _
                         "LEFT JOIN tlscreening_process_master " & _
                              "ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                              "and tlscreening_stages_tran.cycleunkid = tlscreening_process_master.cycleunkid " & _
                         "LEFT JOIN tlscreener_master " & _
                              "ON tlscreening_stages_tran.screenermstunkid = tlscreener_master.screenermstunkid " & _
                              "and tlscreening_stages_tran.cycleunkid = tlscreener_master.cycleunkid " & _
                         "WHERE tlscreening_stages_tran.isvoid = 0 " & _
                         "AND tlscreening_process_master.isvoid = 0 " & _
                         "AND ismatch = 1 " & _
                         "AND tlscreening_stages_tran.employeeunkid = @employeeunkid and tlscreening_stages_tran.cycleunkid=@cycleunkid  ) AS A "

            strQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                           "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                           "SELECT DISTINCT " & _
                           "     AEM.employeeunkid " & _
                           "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                           "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                           "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                           "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                           "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                           "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                           "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                           "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                           "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                           "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                           "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                           "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                           "FROM hremployee_master AS AEM "

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                    "WHERE AEM.employeeunkid = @employeeunkid "

            strQ &= "SELECT " & _
                       "hrmsConfiguration..cfuser_master.username " & _
                       ",CASE " & _
                              "WHEN ISNULL(#TotScr.mapuserunkid, 0) > 0 THEN 1 " & _
                              "ELSE 0 " & _
                         "END AS IsScreeningDone " & _
                       ",CASE " & _
                              "WHEN ISNULL(#TotScr.mapuserunkid, 0) <= 0 THEN 1 " & _
                              "ELSE 0 " & _
                         "END AS IsScreeningPending " & _
                    "FROM #USR " & _
                    "LEFT JOIN #TotScr " & _
                         "ON #USR.userunkid = #TotScr.mapuserunkid " & _
                    "JOIN @emp " & _
                         "ON [@emp].empid = #USR.employeeunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfuser_master " & _
                         "ON #USR.userunkid = hrmsConfiguration..cfuser_master.userunkid "

            objDataOperation.AddParameter("@empId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@cycleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetScreeningDetail; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetAllPipeLineEmployeeList(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal xCycleUnkid As Integer, _
                                    ByVal xNoImageString As String, _
                                    Optional ByVal xEmployeeunkid As Integer = -1, _
                                    Optional ByVal xScreeningType As enScreeningFilterType = enScreeningFilterType.ALL, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                    Optional ByVal strFilterQuery As String = "", _
                                    Optional ByVal blnReinstatementDate As Boolean = False, _
                                    Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                    Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                    Optional ByVal strTempTableSelectQry As String = "", _
                                    Optional ByVal strTempTableJoinQry As String = "", _
                                    Optional ByVal strTempTableDropQry As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim objPotentialTalentTran As New clsPotentialTalent_Tran
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)

        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Cycle.")
                Return Nothing
            End If


            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry

            If xScreeningType = enScreeningFilterType.ALL OrElse xScreeningType = enScreeningFilterType.ONLYQUALIFIED Then

                strQ &= "DECLARE @UserId AS INT;SET @UserId = @Uid "

                strQ &= "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
                        "DROP TABLE #empimage " & _
                        "CREATE TABLE #empimage ( " & _
                            "photo image, " & _
                            "employeeunkid int " & _
                        "); " & _
                        " " & _
                        "INSERT INTO #empimage " & _
                             "SELECT " & _
                                  "photo " & _
                                ",employeeunkid " & _
                             "FROM arutiimages..hremployee_images " & _
                             "WHERE isvoid = 0 "

                strQ += "SELECT " & _
                          "A.* " & _
                          ",ISNULL(Emp_suspension.suspensiondays, 0) AS suspensiondays " & _
                          ",empImage.photo AS empImage " & _
                          ",CASE WHEN ISNULL(D.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS IsDone " & _
                          ",'#FFFFFF' as color " & _
                        "FROM " & _
                        "( " & _
                        "SELECT " & _
                        "   hremployee_master.employeecode AS employeecode " & _
                        ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                        ",  hremployee_master.employeeunkid As employeeunkid " & _
                        ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                        ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                        ",  hrjob_master.job_name " & _
                        ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                        ",  0 AS exyr" & _
                        ", hrdepartment_master.name AS department " & _
                        ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                        ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                        ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                        ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                        ",  0 as processmstunkid" & _
                        ", '' as empBaseImage " & _
                        ", 0 as IsManual " & _
                        ", 0 as isdisapproved " & _
                            ", 0 as potentialtalenttranunkid " & _
                        "  FROM " & strDBName & "hremployee_master "

                strQ &= strTempTableJoinQry


                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If


                '********************** DATA FOR DATES CONDITION ************************' --- START
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If


                If blnIncludeAccessFilterQry = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                End If


                '********************** Talent Seeting Conditions ************************' --- START

                strQ &= "JOIN (SELECT " & _
                        "emp_transfer.employeeunkid " & _
                        ",stationunkid " & _
                        ",deptgroupunkid " & _
                        ",departmentunkid " & _
                        ",sectiongroupunkid " & _
                        ",sectionunkid " & _
                        ",unitgroupunkid " & _
                        ",unitunkid " & _
                        ",teamunkid " & _
                        ",classgroupunkid " & _
                        ",classunkid " & _
                        ",emp_categorization.jobgroupunkid " & _
                        ",emp_categorization.jobunkid " & _
                        ",Emp_CCT.costcenterunkid " & _
                     "FROM (SELECT " & _
                               "Emp_TT.employeeunkid AS TrfEmpId " & _
                             ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                             ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                             ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                             ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                             ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                             ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                             ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                             ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                             ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                             ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                             ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                             ",Emp_TT.employeeunkid " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                          "WHERE isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                     "JOIN (SELECT " & _
                               "Emp_CT.employeeunkid AS CatEmpId " & _
                             ",Emp_CT.jobgroupunkid " & _
                             ",Emp_CT.jobunkid " & _
                             ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                             ",Emp_CT.employeeunkid " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                          "WHERE isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                          "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                     "JOIN (SELECT " & _
                               "Emp_CCT.employeeunkid AS CCTEmpId " & _
                             ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                             ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                             ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                          "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                          "WHERE Emp_CCT.isvoid = 0 " & _
                          "AND Emp_CCT.istransactionhead = 0 " & _
                          "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                          "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                     "WHERE emp_transfer.Rno = 1 " & _
                     "AND emp_categorization.Rno = 1 " & _
                     "AND emp_CCT.Rno = 1"


                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE) Then
                    Dim allocation As String = mdicSetting(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE)
                    Dim allocationType As String() = allocation.Split("|")

                    Select Case CInt(allocationType(0))

                        Case CInt(enAllocation.BRANCH)
                            strQ &= "AND emp_transfer.stationunkid IN "

                        Case CInt(enAllocation.DEPARTMENT_GROUP)
                            strQ &= "AND emp_transfer.deptgroupunkid IN "

                        Case CInt(enAllocation.DEPARTMENT)
                            strQ &= "AND emp_transfer.departmentunkid IN "

                        Case CInt(enAllocation.SECTION_GROUP)
                            strQ &= "AND emp_transfer.sectiongroupunkid IN "

                        Case CInt(enAllocation.SECTION)
                            strQ &= "AND emp_transfer.sectionunkid IN "

                        Case CInt(enAllocation.UNIT_GROUP)
                            strQ &= "AND emp_transfer.unitgroupunkid IN "

                        Case CInt(enAllocation.UNIT)
                            strQ &= "AND emp_transfer.unitunkid IN "

                        Case CInt(enAllocation.JOB_GROUP)
                            strQ &= "AND emp_categorization.jobgroupunkid IN "

                        Case CInt(enAllocation.JOBS)
                            strQ &= "AND emp_categorization.jobunkid IN "

                        Case CInt(enAllocation.CLASS_GROUP)
                            strQ &= "AND emp_transfer.classgroupunkid IN "

                        Case CInt(enAllocation.CLASSES)
                            strQ &= "AND emp_transfer.classunkid IN "

                        Case CInt(enAllocation.COST_CENTER)
                            strQ &= "AND emp_CCT.costcenterunkid IN  "

                    End Select

                    strQ &= " (" & allocationType(1) & " ) "

                End If

                strQ &= " ) AS allocation " & _
                     "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
                "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
                "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

                strQ &= " WHERE 1=1 "

                If xEmployeeunkid > 0 Then
                    strQ &= " and hremployee_master.employeeunkid =@employeeunkid "
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
                End If

                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) Then
                    strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) & " "
                End If
                '================ Employee ID

                If xIncludeIn_ActiveEmployee = False Then
                    If xDataFilterQry.Trim.Length > 0 Then
                        strQ &= xDataFilterQry
                    End If
                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
                Else
                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                End If

                objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))

                If strFilterQuery.Trim <> "" Then
                    strQ &= " AND " & strFilterQuery & " "
                End If

                strQ &= strTempTableDropQry

                strQ &= ") as A " & _
                        "LEFT JOIN #empimage AS empImage " & _
                        "	ON empImage.employeeunkid = A.employeeunkid " & _
                        "LEFT JOIN (SELECT " & _
                              "sus.EmpId " & _
                            ",sus.suspensiondays " & _
                         "FROM (SELECT " & _
                                   "employeeunkid AS EmpId " & _
                                   ",SUM(DATEDIFF(DAY, date1,  " & _
                                   " CASE " & _
                                   "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                                   "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                                   " End " & _
                                    " )) AS suspensiondays " & _
                              "FROM  " & strDBName & "hremployee_dates_tran " & _
                              "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                              "AND isvoid = 0 " & _
                              "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                              "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
                         "ON Emp_suspension.EmpId = A.employeeunkid "

                strQ &= "LEFT JOIN " & _
                          "( " & _
                            "SELECT " & _
                               "tlscreening_process_master.employeeunkid " & _
                              ",tlscreening_process_master.processmstunkid " & _
                            "FROM tlscreener_master AS TSM " & _
                              "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                                "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                              "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                            "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                              "AND TSM.mapuserunkid = @UserId " & _
                          ") AS D ON A.employeeunkid = D.employeeunkid "

                objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
                objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)


                dsList = objDataOperation.ExecQuery(strQ, strListName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim ExpYearNo As String = String.Empty
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                    ExpYearNo = mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO)
                End If
                If ExpYearNo.Length = 0 Then ExpYearNo = "0"
                If CInt(ExpYearNo) > 0 Then

                    Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

                    If strEmpIDs.Length > 0 Then


                        Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, mdtAsOnDate)

                        Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable

                        Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                                      Select New With { _
                                         Key .employeecode = e.Field(Of String)("employeecode") _
                                        , Key .employeename = e.Field(Of String)("employeename") _
                                        , Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                        , Key .isapproved = e.Field(Of Boolean)("isapproved") _
                                        , Key .EmpCodeName = e.Field(Of String)("EmpCodeName") _
                                        , Key .job_name = e.Field(Of String)("job_name") _
                                        , Key .age = e.Field(Of Integer)("age") _
                                        , Key .exyr = e.Field(Of Integer)("exyr") _
                                        , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                        , Key .department = e.Field(Of String)("department") _
                                        , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                        , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                        , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                        , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                        , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                        , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                        , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                        })

                        Dim intPrevEmp As Integer = 0
                        Dim dblTotDays As Double = 0

                        For Each drow In result

                            Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                            Dim EndDate As Date = mdtAsOnDate


                            Dim intEmpId As Integer = drow.employeeunkid
                            Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                            If intCount <= 0 Then 'No Dates record found                       
                                If drow.termination_from_date.Trim <> "" Then
                                    EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                                End If
                                If drow.termination_to_date.Trim <> "" Then
                                    EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                                End If
                                If drow.empl_enddate.Trim <> "" Then
                                    EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                                End If
                                dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                            ElseIf intCount = 1 Then
                                dblTotDays = drow.ServiceDays
                            Else
                                dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                            End If

                            For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                                drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                            Next


                        Next

                        If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                            dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) & " "
                        End If

                    End If
                End If

                Dim intPrdNo As Integer = 0
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Or mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Then
                        If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) Then
                            intPrdNo = mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO)
                        Else
                            intPrdNo = 1
                        End If
                    ElseIf mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                        intPrdNo = 0
                    End If
                End If
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
                    Dim dtPerformanceScoreEmployeeList As DataTable

                    dtPerformanceScoreEmployeeList = GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO), mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid, objDataOperation)
                    If dtPerformanceScoreEmployeeList IsNot Nothing AndAlso dtPerformanceScoreEmployeeList.Rows.Count > 0 Then
                        Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In dtPerformanceScoreEmployeeList Select (p.Item("employeeunkid").ToString)).ToArray())
                        If strPerformanceScoreEmpIds.Length > 0 Then
                            dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
                            Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable
                            dsList.Tables.Remove(dsList.Tables(0))
                            dsList.Tables.Add(dtPerformanceScore)
                        End If
                    Else
                        dsList.Tables(0).Rows.Clear()
                    End If
                End If
                '=========== Performance Score Settings - End =====================
                Dim dfView As DataView = dsList.Tables(0).DefaultView
                dfView.Sort = "employeename"
                dsList.Tables.RemoveAt(0)
                dsList.Tables.Add(dfView.ToTable)
            End If

            '============================ Add Extra Potential Talent Employee(s) -- Start
            If xScreeningType = enScreeningFilterType.ALL OrElse xScreeningType = enScreeningFilterType.ONLYMANUALLYADDED Then
                Dim strEmployeeList As String = ""
                If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    strEmployeeList = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())
                End If

                Dim dtPTEmployee As DataTable = objPotentialTalentTran.GetList("List", xCycleUnkid, IIf(strEmployeeList.Length > 0, " AND employeeunkid not in (" & strEmployeeList & ")", "")).Tables(0)
                Dim strPotentialTalentEmployeelist As String = String.Join(",", (From p In dtPTEmployee Select (p.Item("employeeunkid").ToString)).ToArray())
                If strPotentialTalentEmployeelist.Length > 0 Then
                    Dim dsPotentialTalent As DataSet
                    objDataOperation.ClearParameters()

                    strQ = "DECLARE @PTUserId AS INT; " & _
                           " SET @PTUserId = @Uid "

                    strQ &= "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
                           "DROP TABLE #empimage " & _
                           "CREATE TABLE #empimage ( " & _
                               "photo image, " & _
                               "employeeunkid int " & _
                           "); " & _
                           " " & _
                           "INSERT INTO #empimage " & _
                                "SELECT " & _
                                     "photo " & _
                                   ",employeeunkid " & _
                                "FROM arutiimages..hremployee_images " & _
                                "WHERE isvoid = 0 "

                    strQ &= "SELECT " & _
                          "A.* " & _
                          ",empImage.photo AS empImage " & _
                          ",CASE WHEN ISNULL(D.employeeunkid,0) > 0 THEN 1 ELSE 0 END AS IsDone " & _
                          ",'#FFFFFF' as color " & _
                        "FROM " & _
                           "( " & _
                            "SELECT " & _
                        "   hremployee_master.employeecode AS employeecode " & _
                        ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                        ",  hremployee_master.employeeunkid As employeeunkid " & _
                        ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                        ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                        ",  hrjob_master.job_name " & _
                        ",  0 AS age " & _
                        ",  0 AS exyr" & _
                        ",  0 suspensiondays " & _
                        ", hrdepartment_master.name AS department " & _
                        ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                        ", '' AS termination_from_date " & _
                        ", '' AS termination_to_date " & _
                        ", '' AS empl_enddate " & _
                        ",  0 as processmstunkid" & _
                        ", '' as empBaseImage " & _
                        ", 1 as IsManual " & _
                            ", 0 as isdisapproved " & _
                            ", isnull(tlpotentialtalent_tran.potentialtalenttranunkid,0) as potentialtalenttranunkid " & _
                        ",'#FFFFFF' as color " & _
                            "  FROM hremployee_master " & _
                            "  LEFT JOIN tlpotentialtalent_tran " & _
                            "     ON tlpotentialtalent_tran.employeeunkid = hremployee_master.employeeunkid "

                    strQ &= strTempTableJoinQry

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    strQ &= " JOIN (SELECT " & _
                            "emp_transfer.employeeunkid " & _
                            ",stationunkid " & _
                            ",deptgroupunkid " & _
                            ",departmentunkid " & _
                            ",sectiongroupunkid " & _
                            ",sectionunkid " & _
                            ",unitgroupunkid " & _
                            ",unitunkid " & _
                            ",teamunkid " & _
                            ",classgroupunkid " & _
                            ",classunkid " & _
                            ",emp_categorization.jobgroupunkid " & _
                            ",emp_categorization.jobunkid " & _
                            ",Emp_CCT.costcenterunkid " & _
                         "FROM (SELECT " & _
                                   "Emp_TT.employeeunkid AS TrfEmpId " & _
                                 ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                                 ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                 ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                                 ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                 ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                                 ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                 ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                                 ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                                 ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                                 ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                                 ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                                 ",Emp_TT.employeeunkid " & _
                                 ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                              "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                              "WHERE isvoid = 0 " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                         "JOIN (SELECT " & _
                                   "Emp_CT.employeeunkid AS CatEmpId " & _
                                 ",Emp_CT.jobgroupunkid " & _
                                 ",Emp_CT.jobunkid " & _
                                 ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                                 ",Emp_CT.employeeunkid " & _
                                 ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                              "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                              "WHERE isvoid = 0 " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                              "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                         "JOIN (SELECT " & _
                                   "Emp_CCT.employeeunkid AS CCTEmpId " & _
                                 ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                                 ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                                 ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                              "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                              "WHERE Emp_CCT.isvoid = 0 " & _
                              "AND Emp_CCT.istransactionhead = 0 " & _
                              "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                              "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                         "WHERE emp_transfer.Rno = 1 " & _
                         "AND emp_categorization.Rno = 1 " & _
                         "AND emp_CCT.Rno = 1"

                    strQ &= " ) AS allocation " & _
                        "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
                   "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

                    strQ &= " WHERE 1=1 and tlpotentialtalent_tran.cycleunkid = @cycleunkid "

                    strQ &= " and hremployee_master.employeeunkid in (" & strPotentialTalentEmployeelist & ") "

                    If xEmployeeunkid > 0 Then
                        strQ &= " and hremployee_master.employeeunkid = @employeeunkid "
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
                    End If

                    If strFilterQuery.Trim <> "" Then
                        strQ &= " AND " & strFilterQuery & " "
                    End If

                    strQ &= " ) as A " & _
                          "LEFT JOIN #empimage AS empImage " & _
                          "	ON empImage.employeeunkid = A.employeeunkid " & _
                          "LEFT JOIN " & _
                          "( " & _
                            "SELECT " & _
                               "tlscreening_process_master.employeeunkid " & _
                              ",tlscreening_process_master.processmstunkid " & _
                            "FROM tlscreener_master AS TSM " & _
                              "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                                "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                              "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                            "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                              "AND TSM.mapuserunkid = @PTUserId " & _
                          ") AS D ON A.employeeunkid = D.employeeunkid "


                    objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
                    objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)

                    If xScreeningType = enScreeningFilterType.ONLYMANUALLYADDED Then
                        dsList = objDataOperation.ExecQuery(strQ, strListName)
                    Else
                        dsPotentialTalent = objDataOperation.ExecQuery(strQ, strListName)
                    End If

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If xScreeningType <> enScreeningFilterType.ONLYMANUALLYADDED Then
                        dsList.Merge(dsPotentialTalent.Tables(0))
                    End If

                End If
            End If

            '============================ Add Extra Potential Talent Employee(s) -- End


            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
            End If


            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Cycle.")
			Language.setMessage(mstrModuleName, 2, "Dear")
			Language.setMessage(mstrModuleName, 3, "This is to notify you that some emplyoees fall under qualify stage for talent screening process for the")
			Language.setMessage(mstrModuleName, 4, "Please Login to Aruti in order to approve/disapprove them.")
			Language.setMessage(mstrModuleName, 5, "This is to notify you that talent screening process is done for the")
			Language.setMessage(mstrModuleName, 6, "Notification for Approve/Disapprove of Talent Screening Process")
			Language.setMessage(mstrModuleName, 7, "Notification for Approve of Talent Screening Process")
			Language.setMessage(mstrModuleName, 8, "Notification for Disapprove of Talent Screening Process")
			Language.setMessage(mstrModuleName, 9, "Please Login to Aruti in order to approve/disapprove them.")
			Language.setMessage(mstrModuleName, 10, "This is to notify  that  You are Approved in Approval Stage for Talent Screening Process.")
			Language.setMessage(mstrModuleName, 11, "This is to notify  that  You are Dispproved in  Approval Stage for Talent Screening Process.")
			Language.setMessage(mstrModuleName, 12, "Dear")
			Language.setMessage(mstrModuleName, 13, "Notification for Moved Back to Qualify in Talent Screening Process")
			Language.setMessage(mstrModuleName, 14, "This is to notify  that  You are Moved to Qualify Stage From Approved Stage in Talent Screening Process.")
			Language.setMessage(mstrModuleName, 15, "This is to notify you that some emplyoees fall under Potential stage for talent screening process for the")
			Language.setMessage(mstrModuleName, 16, "Please Login to Aruti in order to Screening them.")
			Language.setMessage(mstrModuleName, 17, "Notification for Screening of Talent Screening Process")
			Language.setMessage(mstrModuleName, 18, "Regards")
			Language.setMessage(mstrModuleName, 19, "HR Department")
			Language.setMessage(mstrModuleName, 20, "#PeriodName#")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class