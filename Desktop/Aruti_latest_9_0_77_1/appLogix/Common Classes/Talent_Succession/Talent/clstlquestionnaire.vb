﻿'************************************************************************************************************************************
'Class Name : clstlquestionnaire_master.vb
'Purpose    :
'Date       :01-Oct-2020
'Written By :Sandeep
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep
''' </summary>
Public Class clstlquestionnaire_master
    Private Shared ReadOnly mstrModuleName As String = "clstlquestionnaire_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation


#Region " Private variables "
    Private mintQuestionnaireunkid As Integer
    Private mintCycleunkid As Integer
    Private mstrQuestion As String = String.Empty
    Private msinWeight As Single
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionnaireunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Questionnaireunkid() As Integer
        Get
            Return mintQuestionnaireunkid
        End Get
        Set(ByVal value As Integer)
            mintQuestionnaireunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cycleunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Cycleunkid() As Integer
        Get
            Return mintCycleunkid
        End Get
        Set(ByVal value As Integer)
            mintCycleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set question
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Question() As String
        Get
            Return mstrQuestion
        End Get
        Set(ByVal value As String)
            mstrQuestion = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Weight() As Single
        Get
            Return msinWeight
        End Get
        Set(ByVal value As Single)
            msinWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  questionnaireunkid " & _
              ", cycleunkid " & _
              ", question " & _
              ", weight " & _
              ", isactive " & _
             "FROM " & mstrDatabaseName & "..tlquestionnaire_master " & _
             "WHERE questionnaireunkid = @questionnaireunkid "

            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintQuestionnaireunkid = CInt(dtRow.Item("questionnaireunkid"))
                mintCycleunkid = CInt(dtRow.Item("cycleunkid"))
                mstrQuestion = dtRow.Item("question").ToString
                msinWeight = dtRow.Item("weight")
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal xCycleId As Integer = 0) As DataSet
        'Pinkal (12-Dec-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  questionnaireunkid " & _
              ", tlquestionnaire_master.cycleunkid " & _
              ", question " & _
              ", weight " & _
              ", tlquestionnaire_master.isactive " & _
              ", ISNULL(tlcycle_master.name, '') as period " & _
             " FROM " & mstrDatabaseName & "..tlquestionnaire_master " & _
             " JOIN tlcycle_master ON tlcycle_master.cycleunkid = tlquestionnaire_master.cycleunkid " & _
             " WHERE tlquestionnaire_master.isactive = 1 "

            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.
            objDataOperation.ClearParameters()
            If xCycleId > 0 Then
                strQ &= " AND tlquestionnaire_master.cycleunkid = @cycleunkid"
                objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleId)
            End If
            'Pinkal (12-Dec-2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tlquestionnaire_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintCycleunkid, mstrQuestion, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This question is already defined. Please define new quetion.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@question", SqlDbType.NVarChar, mstrQuestion.Trim.Length, mstrQuestion.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinWeight.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..tlquestionnaire_master ( " & _
              "  cycleunkid " & _
              ", question " & _
              ", weight " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @cycleunkid " & _
              ", @question " & _
              ", @weight " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintQuestionnaireunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tlquestionnaire_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintCycleunkid, mstrQuestion, mintQuestionnaireunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This question is already defined. Please define new quetion.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireunkid.ToString)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@question", SqlDbType.NVarChar, mstrQuestion.Trim.Length, mstrQuestion.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinWeight.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..tlquestionnaire_master SET " & _
              "  cycleunkid = @cycleunkid" & _
              ", question = @question" & _
              ", weight = @weight" & _
              ", isactive = @isactive " & _
            "WHERE questionnaireunkid = @questionnaireunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tlquestionnaire_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Questionnaireunkid = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..tlquestionnaire_master SET " & _
                   " isactive = 0 " & _
                   "WHERE questionnaireunkid = @questionnaireunkid "

            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iCycleId As Integer, ByVal strQuestion As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  questionnaireunkid " & _
                    ", cycleunkid " & _
                    ", question " & _
                    ", weight " & _
                    ", isactive " & _
                    "FROM " & mstrDatabaseName & "..tlquestionnaire_master " & _
                    "WHERE isactive = 1 " & _
                    " AND question = @question " & _
                    " AND cycleunkid  = @cycleunkid "

            If intUnkid > 0 Then
                strQ &= " AND questionnaireunkid <> @questionnaireunkid"
            End If

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCycleId)
            objDataOperation.AddParameter("@question", SqlDbType.NVarChar, strQuestion.Trim.Length, strQuestion)
            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetQuestionnairMaxWeightFromCycleId(ByVal iCycleId As Integer, ByVal mintquestionnaireunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Double

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                   "ISNULL(SUM(weight), 0) AS total_weight " & _
                   "FROM " & mstrDatabaseName & "..tlquestionnaire_master " & _
                   "WHERE cycleunkid = @cycleunkid " & _
                   "AND isactive = 1 "

            If mintquestionnaireunkid > 0 Then
                strQ &= " and questionnaireunkid not in(" & mintquestionnaireunkid & ") "
            End If

            strQ &= " GROUP BY cycleunkid "
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCycleId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CDbl(dsList.Tables(0).Rows(0)("total_weight"))
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQuestionnairMaxWeightFromCycleId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GetQuestionListFromCycle(ByVal strTableName As String, ByVal iCycleId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  questionnaireunkid " & _
              ", tlquestionnaire_master.cycleunkid " & _
              ", question " & _
              ", weight " & _
              ", tlquestionnaire_master.isactive " & _
             "FROM " & mstrDatabaseName & "..tlquestionnaire_master " & _
             "WHERE tlquestionnaire_master.isactive = 1 and tlquestionnaire_master.cycleunkid = @cycleunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCycleId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQuestionListFromCycle; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..attlquestionnaire_master ( " & _
                    "  tranguid " & _
                    ", questionnaireunkid " & _
                    ", cycleunkid " & _
                    ", question " & _
                    ", weight " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @questionnaireunkid " & _
                    ", @cycleunkid " & _
                    ", @question " & _
                    ", @weight " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireunkid.ToString)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@question", SqlDbType.NVarChar, mstrQuestion.Trim.Length, mstrQuestion.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinWeight.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This question is already defined. Please define new quetion.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class