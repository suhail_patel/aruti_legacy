﻿'************************************************************************************************************************************
'Class Name : clsleaveapprover_master.vb
'Purpose    :
'Date       :10/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 2
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveapprover_master
    Private Shared ReadOnly mstrModuleName As String = "clsleaveapprover_master"
    Dim objDataOperation As clsDataOperation
    Dim objLeaveApproverTrans As New clsleaveapprover_Tran
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApproverunkid As Integer
    Private mintLevelunkid As Integer
    Private mstrCode As String = String.Empty
    Private mintleaveapproverunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintSectionunkid As Integer
    Private mintjobunkid As Integer
    Private mblnApproval_To As Boolean
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintUserunkid As Integer
    Private mintVoiduserunkid As Integer
    Private mstrVoidReason As String = String.Empty


    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Private mintMapUserId As Integer = -1
    Private mdtLeaveTypeMapping As DataTable = Nothing

    'Pinkal (06-Apr-2013) -- End



    'Pinkal (19-Mar-2015) -- Start
    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
    Private mintSwapFromApproverID As Integer = 0
    Private mintSwapToApproverID As Integer = 0
    Private mblnIsSwap As Boolean = False
    'Pinkal (19-Mar-2015) -- End


    'Pinkal (17-Jun-2015) -- Start
    'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
    Private mblnIsActive As Boolean = True
    'Pinkal (17-Jun-2015) -- End

    'Pinkal (26-Oct-2015) -- Start
    'Enhancement - Working on TRA Claim & Request Report Issue.
    Dim objDoOperation As clsDataOperation
    'Pinkal (26-Oct-2015) -- End

    'S.SANDEEP [30 JAN 2016] -- START
    Private mblnIsexternalapprover As Boolean = False
    'S.SANDEEP [30 JAN 2016] -- END

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.


    'Public Property _Approverunkid() As Integer
    '    Get
    '        Return mintApproverunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintApproverunkid = value
    '        Call GetData()
    '    End Set
    'End Property

    Public Property _Approverunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
            Call GetData()
        End Set
    End Property
    'Gajanan [23-SEP-2019] -- End




    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _leaveapproverunkid() As Integer
        Get
            Return mintleaveapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintleaveapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintjobunkid
        End Get
        Set(ByVal value As Integer)
            mintjobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approval_to
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approval_To() As Boolean
        Get
            Return mblnApproval_To
        End Get
        Set(ByVal value As Boolean)
            mblnApproval_To = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userumkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userumkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidReason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    'Pinkal (06-Apr-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set MapUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MapUserId() As Integer
        Get
            Return mintMapUserId
        End Get
        Set(ByVal value As Integer)
            mintMapUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtLeaveTypeMapping
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtLeaveTypeMapping() As DataTable
        Get
            Return mdtLeaveTypeMapping
        End Get
        Set(ByVal value As DataTable)
            mdtLeaveTypeMapping = value
        End Set
    End Property

    'Pinkal (06-Apr-2013) -- End


    'Pinkal (19-Mar-2015) -- Start
    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

    ''' <summary>
    ''' Purpose: Get or Set isswap
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isswap() As Boolean
        Get
            Return mblnIsSwap
        End Get
        Set(ByVal value As Boolean)
            mblnIsSwap = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SwapFromApproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SwapFromApproverunkid() As Integer
        Get
            Return mintSwapFromApproverID
        End Get
        Set(ByVal value As Integer)
            mintSwapFromApproverID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SwapToApproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SwapToApproverunkid() As Integer
        Get
            Return mintSwapToApproverID
        End Get
        Set(ByVal value As Integer)
            mintSwapToApproverID = value
        End Set
    End Property

    'Pinkal (19-Mar-2015) -- End

    'Pinkal (17-Jun-2015) -- Start
    'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsActive() As Boolean
        Get
            Return mblnIsActive
        End Get
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    'Pinkal (17-Jun-2015) -- End

    'Pinkal (26-Oct-2015) -- Start
    'Enhancement - Working on TRA Claim & Request Report Issue.

    Public WriteOnly Property _DoOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDoOperation = value
        End Set
    End Property

    'Pinkal (26-Oct-2015) -- End.

    'S.SANDEEP [30 JAN 2016] -- START
    ''' <summary>
    ''' Purpose: Get or Set isexternalapprover
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isexternalapprover() As Boolean
        Get
            Return mblnIsexternalapprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternalapprover = value
        End Set
    End Property
    'S.SANDEEP [30 JAN 2016] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (26-Oct-2015) -- Start
        'Enhancement - Working on TRA Claim & Request Report Issue.
        ' Dim objDataOperation As New clsDataOperation
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        'Pinkal (26-Oct-2015) -- End.


        Try

            strQ = "SELECT " & _
              "  approverunkid " & _
              ", levelunkid " & _
              ", leaveapproverunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
                  ", isswap " & _
              ", isactive " & _
              ", isexternalapprover " & _
             "FROM lvleaveapprover_master " & _
             "WHERE approverunkid = @approverunkid and isvoid = 0 "
            'S.SANDEEP [30 JAN 2016] -- START {isexternalapprover} -- END

            'Pinkal (19-Mar-2015) -- End


            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))

                'Pinkal (19-Mar-2015) -- Start
                'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

                'mstrCode = dtRow.Item("code").ToString
                'If dtRow.Item("departmentunkid") IsNot Nothing And dtRow.Item("departmentunkid") IsNot DBNull.Value Then mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                'If dtRow.Item("sectionunkid") IsNot Nothing And dtRow.Item("sectionunkid") IsNot DBNull.Value Then mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                'If dtRow.Item("jobunkid") IsNot Nothing And dtRow.Item("jobunkid") IsNot DBNull.Value Then mintjobunkid = CInt(dtRow.Item("jobunkid"))
                'mblnApproval_To = CBool(dtRow.Item("approval_to"))

                'Pinkal (19-Mar-2015) -- End

                mintleaveapproverunkid = CInt(dtRow.Item("leaveapproverunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voidreason") Is Nothing Then
                    mstrVoidReason = dtRow.Item("voidreason").ToString
                End If

                Dim objUserMapping As New clsapprover_Usermapping
                objUserMapping.GetData(enUserType.Approver, mintApproverunkid)
                mintMapUserId = objUserMapping._Userunkid

                'Pinkal (17-Jun-2015) -- Start
                'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
                mblnIsActive = CBool(dtRow.Item("isactive"))
                'Pinkal (17-Jun-2015) -- End

                'S.SANDEEP [30 JAN 2016] -- START
                mblnIsexternalapprover = CBool(dtRow.Item("isexternalapprover"))
                'S.SANDEEP [30 JAN 2016] -- END


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                mblnIsSwap = CBool(dtRow.Item("isswap"))
                'Pinkal (01-Mar-2016) -- End


                Exit For

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            'objDataOperation = Nothing
            'Pinkal (19-Mar-2015) -- End
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnIncludeInactiveEmployee As String = "", Optional ByVal strEmployeeAsOnDate As String = "", Optional ByVal intUserID As Integer = 0, Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet   'Pinkal (22-MAY-2012) -- Start
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        strQ = "SELECT " & _
    '          "  lvleaveapprover_master.approverunkid " & _
    '          ", lvleaveapprover_master.levelunkid " & _
    '          ", lvapproverlevel_master.levelname " & _
    '          ", lvapproverlevel_master.priority " & _
    '          ", lvleaveapprover_master.code " & _
    '          ", lvleaveapprover_master.leaveapproverunkid " & _
    '          ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as name" & _
    '          ", lvleaveapprover_master.departmentunkid" & _
    '          ", ISNULL(hrdepartment_master.name,'') as departmentname " & _
    '          ", lvleaveapprover_master.sectionunkid" & _
    '          ", ISNULL(hrsection_master.name,'') as sectionname " & _
    '          ", lvleaveapprover_master.jobunkid" & _
    '          ", ISNULL(hrjob_master.job_name,'') As  jobname " & _
    '          ", lvleaveapprover_master.approval_to " & _
    '          ", lvleaveapprover_master.isvoid " & _
    '          ", lvleaveapprover_master.voiddatetime " & _
    '          ", lvleaveapprover_master.userunkid " & _
    '          ", lvleaveapprover_master.voiduserunkid " & _
    '          ", lvleaveapprover_master.voidreason " & _
    '         "FROM lvleaveapprover_master " & _
    '         " LEFT JOIN lvapproverlevel_master on lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
    '         " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
    '         " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
    '         " LEFT JOIN hrsection_master on hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
    '         " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_master.jobunkid "


    '        ''Pinkal (15-MAY-2012) -- Start
    '        ''Enhancement : TRA Changes
    '        'strQ &= "JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid AND usertypeid = " & enUserType.Approver & _
    '        '            " AND hrapprover_usermapping.userunkid = " & IIf(intUserID > 0, intUserID, User._Object._Userunkid)
    '        ''Pinkal (15-MAY-2012) -- End


    '        If blnOnlyActive Then
    '            strQ &= " WHERE lvleaveapprover_master.isvoid = 0 "

    '            'Pinkal (17-Jun-2015) -- Start
    '            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
    '            strQ &= " AND lvleaveapprover_master.isactive = 1 "
    '            'Pinkal (17-Jun-2015) -- End

    '        End If


    '        If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '        End If

    '        If CBool(blnIncludeInactiveEmployee) = False Then

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If



    '        'Pinkal (06-Mar-2013) -- Start
    '        'Enhancement : TRA Changes
    '        'Issue : in process pending list when higher approver approves at that time  leave for status was not getting changed  when approver and user access rights are different,So due to this it has been commented.    
    '        'If strUserAccessLevelFilterString = "" Then
    '        '    strQ &= UserAccessLevel._AccessLevelFilterString
    '        'Else
    '        '    strQ &= strUserAccessLevelFilterString
    '        'End If

    '        'Pinkal (06-Mar-2013) -- End


    '        'Pinkal (06-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        strQ &= " AND hremployee_master.isapproved = 1 "
    '        'Pinkal (06-Feb-2013) -- End




    '        'Pinkal (19-Mar-2015) -- Start
    '        'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
    '        strQ &= " AND lvleaveapprover_master.isswap = 0 "
    '        'Pinkal (19-Mar-2015) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal strEmployeeAsOnDate As String _
                                      , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                      , Optional ByVal intEmpId As Integer = -1, Optional ByVal objDataOperation As clsDataOperation = Nothing _
                                      , Optional ByVal mstrFilter As String = "" _
                                      , Optional ByVal strUserAccessLevelFilterString As String = "" _
                                      , Optional ByVal intApproverTranUnkid As Integer = 0 _
                                      ) As DataSet

        'Gajanan [23-SEP-2019] -- Add [intApproverTranUnkid]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try

            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            strQ &= "SELECT " & _
              "  lvleaveapprover_master.approverunkid " & _
              ", lvleaveapprover_master.levelunkid " & _
              ", lvapproverlevel_master.levelname " & _
              ", lvapproverlevel_master.priority " & _
              ", lvleaveapprover_master.code " & _
              ", lvleaveapprover_master.leaveapproverunkid " & _
              ", #APPR_NAME# as name" & _
              ", lvleaveapprover_master.departmentunkid" & _
              ", #DEPT_NAME# as departmentname " & _
              ", lvleaveapprover_master.sectionunkid" & _
              ", #SEC_NAME# as sectionname " & _
              ", lvleaveapprover_master.jobunkid" & _
              ", #JOB_NAME# As  jobname " & _
              ", lvleaveapprover_master.approval_to " & _
              ", lvleaveapprover_master.isvoid " & _
              ", lvleaveapprover_master.voiddatetime " & _
              ", lvleaveapprover_master.userunkid " & _
              ", lvleaveapprover_master.voiduserunkid " & _
              ", lvleaveapprover_master.voidreason " & _
              ", lvleaveapprover_master.isexternalapprover  " & _
              ", CASE WHEN lvleaveapprover_master.isexternalapprover = 1 THEN @YES ELSE @NO END AS ExAppr " & _
              ", lvapproverlevel_master.escalation_days " & _
             "FROM lvleaveapprover_master " & _
             " LEFT JOIN lvapproverlevel_master on lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
             " #EMPL_JOIN# " & _
             " #DATA_JOIN# "

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[ ", lvapproverlevel_master.escalation_days " & _]



            StrFinalQurey = strQ

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            StrQCondition &= " WHERE 1 = 1 AND lvleaveapprover_master.isexternalapprover = #ExAppr# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If


            If blnOnlyActive Then
                StrQCondition &= " AND lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_master.isactive = 0 "
            End If



            If intApproverTranUnkid > 0 Then
                StrQCondition &= " AND lvleaveapprover_master.approverunkid = '" & intApproverTranUnkid & "' "
            End If


            StrQCondition &= " AND lvleaveapprover_master.isswap = 0 "

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= mstrFilter
            End If

            Dim StrDataJoin As String = " LEFT JOIN " & _
                                           " ( " & _
                                           "    SELECT " & _
                                           "        departmentunkid " & _
                                           "        ,sectionunkid " & _
                                           "        ,employeeunkid " & _
                                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "    FROM #DB_Name#hremployee_transfer_tran " & _
                                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                           " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                           " LEFT JOIN #DB_Name#hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                                           " LEFT JOIN #DB_Name#hrsection_master on hrsection_master.sectionunkid = Alloc.sectionunkid " & _
                                           " LEFT JOIN " & _
                                           " ( " & _
                                           "    SELECT " & _
                                           "         jobunkid " & _
                                           "        ,jobgroupunkid " & _
                                           "        ,employeeunkid " & _
                                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "    FROM #DB_Name#hremployee_categorization_tran " & _
                                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                           " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                           " LEFT JOIN #DB_Name#hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid ")
            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#DB_Name#", "")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
            strQ = strQ.Replace("#SEC_NAME#", "ISNULL(hrsection_master.name,'') ")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")
            strQ &= StrQCondition
            strQ &= StrQDtFilters
            strQ = strQ.Replace("#ExAppr#", "0")

            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As New DataSet
            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey : StrQDtFilters = ""
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid  ")
                    strQ = strQ.Replace("#ExAppr#", "1")
                    strQ = strQ.Replace("#DB_Name#", "")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#SEC_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate")), dr("DName"))

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
                    strQ = strQ.Replace("#SEC_NAME#", "ISNULL(hrsection_master.name,'') ")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQDtFilters &= xDateFilterQry & " "
                        End If
                    End If

                End If

                strQ &= StrQCondition
                strQ &= StrQDtFilters
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))


                dstmp = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function Insert(ByVal mdtran As DataTable) As Boolean

        'S.SANDEEP [30 JAN 2016] -- START
        If isExistApproverForAllDepartment(mintleaveapproverunkid, mintLevelunkid, , , mblnIsexternalapprover) Then
            'If isExistApproverForAllDepartment(mintleaveapproverunkid, mintLevelunkid) Then
            'S.SANDEEP [30 JAN 2016] -- END
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Approver already exists. Please define new Leave Approver.")
            Return False
        End If



        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
            objDataOperation.AddParameter("@approval_to", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnApproval_To.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)

            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap.ToString)


            'Pinkal (17-Jun-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            'Pinkal (17-Jun-2015) -- End

            'S.SANDEEP [30 JAN 2016] -- START
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            'S.SANDEEP [30 JAN 2016] -- END


            'strQ = "INSERT INTO lvleaveapprover_master ( " & _
            '  "  levelunkid " & _
            '  ", code " & _
            '  ", leaveapproverunkid " & _
            '  ", departmentunkid" & _
            '  ", sectionunkid" & _
            '  ", jobunkid" & _
            '  ", approval_to " & _
            '  ", isvoid " & _
            '  ", voiddatetime " & _
            '  ", userunkid " & _
            '  ", voiduserunkid" & _
            '  ", voidreason" & _
            '") VALUES (" & _
            '  "  @levelunkid " & _
            '  ", @code " & _
            '  ", @leaveapproverunkid " & _
            '  ", @departmentunkid" & _
            '  ", @sectionunkid" & _
            '  ", @jobunkid" & _
            '  ", @approval_to " & _
            '  ", @isvoid " & _
            '  ", @voiddatetime " & _
            '  ", @userunkid " & _
            '  ", @voiduserunkid" & _
            '  ", @voidreason" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO lvleaveapprover_master ( " & _
              "  levelunkid " & _
              ", code " & _
              ", leaveapproverunkid " & _
              ", departmentunkid" & _
              ", sectionunkid" & _
              ", jobunkid" & _
              ", approval_to " & _
                      ", isswap " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", voiduserunkid" & _
              ", voidreason" & _
                      ", isactive " & _
              ", isexternalapprover" & _
            ") VALUES (" & _
              "  @levelunkid " & _
              ", @code " & _
              ", @leaveapproverunkid " & _
              ", @departmentunkid" & _
              ", @sectionunkid" & _
              ", @jobunkid" & _
              ", @approval_to " & _
                      ", @isswap " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @userunkid " & _
              ", @voiduserunkid" & _
              ", @voidreason" & _
                      ", @isactive " & _
              ", @isexternalapprover " & _
            "); SELECT @@identity" 'S.SANDEEP [30 JAN 2016] -- START {isexternalapprover} -- END

            'Pinkal (19-Mar-2015) -- End


            'Pinkal (17-Jun-2015) -- Start  WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE. [isactive]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApproverunkid = dsList.Tables(0).Rows(0).Item(0)
            objLeaveApproverTrans._Approverunkid = mintApproverunkid
            objLeaveApproverTrans._DataList = mdtran
            objLeaveApproverTrans._Userunkid = mintUserunkid
            If objLeaveApproverTrans._DataList IsNot Nothing Then
                If objLeaveApproverTrans._DataList.Rows.Count > 0 Then
                    With objLeaveApproverTrans
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objLeaveApproverTrans.InsertDelete_LeaveApproverData(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Else

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", -1, 1, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                End If

            End If

            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.Approver, mintApproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintApproverunkid
            objUserMapping._Userunkid = mintMapUserId
            objUserMapping._UserTypeid = enUserType.Approver
            objUserMapping._AuditUserunkid = mintUserunkid

            With objUserMapping
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
            If objUserMapping.Insert(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If

            If mdtLeaveTypeMapping IsNot Nothing AndAlso mdtLeaveTypeMapping.Rows.Count > 0 Then
                Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
                objLeaveTypeMapping._Approverunkid = mintApproverunkid
                objLeaveTypeMapping._dtLeaveType = mdtLeaveTypeMapping
                objLeaveTypeMapping._Userunkid = mintUserunkid
                With objLeaveTypeMapping
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objLeaveTypeMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

                objDataOperation.ReleaseTransaction(True)
                Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveapprover_master) </purpose>
    Public Function Update(ByVal mdtran As DataTable) As Boolean

        'S.SANDEEP [30 JAN 2016] -- START
        If isExistApproverForAllDepartment(mintleaveapproverunkid, mintLevelunkid, mintApproverunkid, , mblnIsexternalapprover) Then
            'If isExistApproverForAllDepartment(mintleaveapproverunkid, mintLevelunkid, mintApproverunkid) Then
            'S.SANDEEP [30 JAN 2016] -- END

            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Approver already exists. Please define new Leave Approver.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
            objDataOperation.AddParameter("@approval_to", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnApproval_To.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap.ToString)


            'Pinkal (17-Jun-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            'Pinkal (17-Jun-2015) -- End

            'S.SANDEEP [30 JAN 2016] -- START
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            'S.SANDEEP [30 JAN 2016] -- END

            'strQ = "UPDATE lvleaveapprover_master SET " & _
            '  "  levelunkid = @levelunkid" & _
            '  ", code = @code" & _
            '  ", leaveapproverunkid = @leaveapproverunkid" & _
            '  ", departmentunkid = @departmentunkid" & _
            '  ", sectionunkid =  @sectionunkid" & _
            '  ", jobunkid = @jobunkid" & _
            '  ", approval_to = @approval_to" & _
            '  ", isvoid = @isvoid" & _
            '  ", voiddatetime = @voiddatetime" & _
            '  ", userunkid = @userunkid" & _
            '  ", voiduserunkid = @voiduserunkid " & _
            '  ", voidreason = @voidreason " & _
            '"WHERE approverunkid = @approverunkid "

            strQ = "UPDATE lvleaveapprover_master SET " & _
              "  levelunkid = @levelunkid" & _
              ", code = @code" & _
              ", leaveapproverunkid = @leaveapproverunkid" & _
              ", departmentunkid = @departmentunkid" & _
              ", sectionunkid =  @sectionunkid" & _
              ", jobunkid = @jobunkid" & _
              ", approval_to = @approval_to" & _
                       ", isswap = @isswap" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", userunkid = @userunkid" & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voidreason = @voidreason " & _
              ", isactive = @isactive " & _
              ", isexternalapprover = @isexternalapprover " & _
            "WHERE approverunkid = @approverunkid " 'S.SANDEEP [30 JAN 2016] -- START {isexternalapprover}-- END


            'Pinkal (17-Jun-2015) -- 'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

            'Pinkal (17-Jun-2015) -- End


            'Pinkal (19-Mar-2015) -- End

            objDataOperation.ExecNonQuery(strQ)

            objLeaveApproverTrans._Approverunkid = mintApproverunkid
            objLeaveApproverTrans._DataList = mdtran
            objLeaveApproverTrans._Userunkid = mintUserunkid


            Dim dt() As DataRow = objLeaveApproverTrans._DataList.Select("AUD=''")
            If dt.Length = objLeaveApproverTrans._DataList.Rows.Count Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "lvleaveapprover_master", mintApproverunkid, "approverunkid", 2) Then

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            Else
                With objLeaveApproverTrans
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objLeaveApproverTrans.InsertDelete_LeaveApproverData(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If

            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.Approver, mintApproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintApproverunkid
            objUserMapping._Userunkid = mintMapUserId
            objUserMapping._UserTypeid = enUserType.Approver
            objUserMapping._AuditUserunkid = mintUserunkid

            With objUserMapping
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            If objUserMapping._Mappingunkid > 0 Then
            If objUserMapping.Update(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mdtLeaveTypeMapping IsNot Nothing AndAlso mdtLeaveTypeMapping.Rows.Count > 0 Then
                Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
                objLeaveTypeMapping._Approverunkid = mintApproverunkid
                objLeaveTypeMapping._dtLeaveType = mdtLeaveTypeMapping
                objLeaveTypeMapping._Userunkid = mintUserunkid
                With objLeaveTypeMapping
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objLeaveTypeMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Delete Database Table (lvleaveapprover_master) </purpose>

    'Pinkal (12-Oct-2020) -- Start
    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
    'Public Function Delete(ByVal intUnkid As Integer, ByVal mdtEmployeeAsonDate As Date) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal xDataBaseName As String) As Boolean
        'Pinkal (12-Oct-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            mintApproverunkid = intUnkid


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'objLeaveApproverTrans.GetApproverTran(mdtEmployeeAsonDate, intUnkid)
            objLeaveApproverTrans.GetApproverTran(xDataBaseName, mdtEmployeeAsonDate, intUnkid, -1, -1, -1, True, Nothing)
            'Pinkal (12-Oct-2020) -- End


            'FOR VOID IN LEAVE APPROVER TRANSACTION

            strQ = " Update lvleaveapprover_tran set " & _
                              " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                             " WHERE approverunkid = @approver_unkid  AND isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approver_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
            objDataOperation.ExecNonQuery(strQ)


            'FOR VOID IN LEAVE APPROVER MASTER
            strQ = " Update lvleaveapprover_master set " & _
                   " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                   " WHERE approverunkid = @approverunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
            objDataOperation.ExecNonQuery(strQ)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", intUnkid, "lvleaveapprover_tran", "leaveapprovertranunkid", 3, 3, "", "", False, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            strQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid and Usertypeid =" & enUserType.Approver
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(i)("mappingunkid")), False, mintVoiduserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            Next

            strQ = "Delete from hrapprover_usermapping WHERE approverunkid = @approverunkid and Usertypeid =" & enUserType.Approver
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
            objLeaveTypeMapping._Isvoid = True
            objLeaveTypeMapping._Voiduserunkid = mintVoiduserunkid
            objLeaveTypeMapping._Voiddatetime = mdtVoiddatetime
            objLeaveTypeMapping._Voidreason = mstrVoidReason

            objLeaveTypeMapping._FormName = mstrFormName
            objLeaveTypeMapping._LoginEmployeeunkid = mintLoginEmployeeunkid
            objLeaveTypeMapping._ClientIP = mstrClientIP
            objLeaveTypeMapping._HostName = mstrHostName
            objLeaveTypeMapping._FromWeb = mblnIsWeb
            objLeaveTypeMapping._AuditUserId = mintAuditUserId
objLeaveTypeMapping._CompanyUnkid = mintCompanyUnkid
            objLeaveTypeMapping._AuditDate = mdtAuditDate
            With objLeaveTypeMapping
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objLeaveTypeMapping.Delete(objDataOperation, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                      "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='approverunkid' "


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName").ToString.Contains("lv") Then

                    If dtRow.Item("TableName") = "lvleaveapprover_master" Or dtRow.Item("TableName") = "lvleaveapprover_tran" _
                       Or dtRow.Item("TableName") = "atlvleaveapprover_tran" Or dtRow.Item("TableName") = "hrapprover_usermapping" _
                       Or dtRow.Item("TableName") = "lvapprover_leavetype_mapping" Then Continue For


                    objDataOperation.ClearParameters()
                    If dtRow.Item("TableName") = "lvpendingleave_tran" Then

                        'START ASSIGN APPROVERUNKID IS DIFFERENT FROM LEAVEAPROVERUNKID AND IN THIS TABLE CHECK ONLY LEAVEAPPROVERUNKID
                        _Approverunkid = intUnkid
                        strQ = "SELECT approverunkid FROM " & dtRow.Item("TableName").ToString & " WHERE approvertranunkid = @approvertranunkid  and isvoid = 0"
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                        dsList = objDataOperation.ExecQuery(strQ, "Used")

                        'END ASSIGN APPROVERUNKID IS DIFFERENT FROM LEAVEAPROVERUNKID AND IN THIS TABLE CHECK ONLY LEAVEAPPROVERUNKID

                    Else
                        strQ = "SELECT approverunkid FROM " & dtRow.Item("TableName").ToString & " WHERE approverunkid = @approverunkid  and isvoid = 0"
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                        dsList = objDataOperation.ExecQuery(strQ, "Used")

                    End If

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        blnIsUsed = True
                        Exit For
                    End If
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intDepartmentunkid As Integer, ByVal intSectionunkid As Integer, ByVal intJobunkid As Integer, ByVal intApproverunkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
              "  approverunkid " & _
              ", levelunkid " & _
              ", code " & _
              ", leaveapproverunkid " & _
              ", departmentunkid" & _
              ", sectionunkid" & _
              ", jobunkid" & _
              ", approval_to " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              " FROM lvleaveapprover_master " & _
              " WHERE departmentunkid = @departmentunkid and sectionunkid = @sectionunkid and jobunkid = @jobunkid and leaveapproverunkid=@leaveapproverunkid and isvoid = 0"


            If intUnkid > 0 Then
                strQ &= " AND approverunkid <> @approverunkid"
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If



            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            strQ &= " AND isswap = 0 "
            'Pinkal (19-Mar-2015) -- End


            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentunkid)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionunkid)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobunkid)
            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExistApproverForAllDepartment(ByVal intLeaveApproverunkid As Integer, ByVal intApproveLevelunkid As Integer, Optional ByVal intunkid As Integer = -1, Optional ByRef intApproverMstId As Integer = 0, Optional ByVal blnIsExAppr As Boolean = False) As Boolean 'S.SANDEEP [30 JAN 2016] -- START {blnIsExAppr} -- END
        'Public Function isExistApproverForAllDepartment(ByVal intLeaveApproverunkid As Integer, ByVal intApproveLevelunkid As Integer, Optional ByVal intunkid As Integer = -1, Optional ByRef intApproverMstId As Integer = 0) As Boolean 'S.SANDEEP [ 13 FEB 2013 ] -- START -- END
        'Public Function isExistApproverForAllDepartment(ByVal intLeaveApproverunkid As Integer, ByVal intApproveLevelunkid As Integer, Optional ByVal intunkid As Integer = -1) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
              "  approverunkid " & _
              ", levelunkid " & _
              ", code " & _
              ", leaveapproverunkid " & _
              ", departmentunkid" & _
              ", sectionunkid" & _
              ", jobunkid" & _
              ", approval_to " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              " FROM lvleaveapprover_master " & _
              " WHERE leaveapproverunkid=@leaveapproverunkid AND levelunkid = @levelunkid and isvoid = 0 AND isexternalapprover = @isexternalapprover "
            'S.SANDEEP [30 JAN 2016] -- START {isexternalapprover} -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproveLevelunkid)
            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveApproverunkid)
            'S.SANDEEP [30 JAN 2016] -- START
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExAppr)
            'S.SANDEEP [30 JAN 2016] -- END

            If intunkid > 0 Then
                strQ &= " AND approverunkid <> @approverunkid"
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            strQ &= " AND isswap = 0 "
            'Pinkal (19-Mar-2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApproverMstId = dsList.Tables(0).Rows(0)("approverunkid")
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 
    'Public Function GetEmployeeApprover(Optional ByVal intUserunkid As Integer = -1, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intApproverEmpId As Integer = -1, Optional ByVal intLeaveTypeID As Integer = -1 _
    '                                                      , Optional ByVal blnLeaveApproverForLeaveType As String = "" _
    '                                                      , Optional ByVal strEmployeeAsOnDate As String = "") As DataTable
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    Try


    '        If intUserunkid > 0 Then

    '            strQ = " SELECT lvleaveapprover_master.approverunkid, ISNULL(leaveapproverunkid, 0) employeeunkid ," & _
    '                           " ISNULL(firstname, '') + ' ' + ISNULL(surname, '') AS employeename,lvapproverlevel_master.priority " & _
    '                           " FROM lvleaveapprover_master "


    '            'Pinkal (09-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '            strQ &= " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
    '            'Pinkal (09-Jul-2014) -- End


    '            strQ &= " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
    '               " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid  " & _
    '               " and hrapprover_usermapping.usertypeid=" & enUserType.Approver & _
    '                      " WHERE isvoid = 0 AND hrapprover_usermapping.userunkid = @Userunkid"

    '            If intApproverEmpId > 0 Then
    '                strQ &= " AND hremployee_master.employeeunkid = @approverEmpId "
    '                objDataOperation.AddParameter("@approverEmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmpId.ToString)
    '            End If

    '            objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)

    '        End If

    '        If intEmployeeunkid > 0 Then

    '            strQ = " SELECT lvleaveapprover_master.approverunkid, ISNULL(leaveapproverunkid, 0) employeeunkid , " & _
    '                       " ISNULL(firstname, '') + ' ' + ISNULL(surname, '') AS employeename,lvapproverlevel_master.priority " & _
    '                       " FROM lvleaveapprover_master "


    '            'Pinkal (09-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '            strQ &= " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
    '            'Pinkal (09-Jul-2014) -- End

    '            strQ &= " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid= 0" & _
    '               " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & _
    '               " and hrapprover_usermapping.usertypeid=" & enUserType.Approver & _
    '                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid "


    '            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '            End If

    '            If CBool(blnLeaveApproverForLeaveType) = True And intLeaveTypeID > 0 Then

    '                strQ &= "  JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
    '                            "  JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvapprover_leavetype_mapping.leavetypeunkid = @leavetypeId "

    '                objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID.ToString)

    '            End If

    '            strQ &= " WHERE lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_tran.employeeunkid = @employeeunkid"

    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

    '        End If

    '        If strEmployeeAsOnDate.Trim.ToString().Length <= 0 Then
    '            strEmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
    '        End If

    '        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
    '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)

    '        'Pinkal (19-Mar-2015) -- Start
    '        'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
    '        strQ &= " AND lvleaveapprover_master.isswap  = 0 "
    '        'Pinkal (19-Mar-2015) -- End


    '        'Pinkal (17-Jun-2015) -- Start
    '        'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
    '        strQ &= " AND lvleaveapprover_master.isactive = 1 "
    '        'Pinkal (17-Jun-2015) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables.Count > 0 Then
    '            Return dsList.Tables("List")
    '        Else
    '            Return Nothing
    '        End If

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprover; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeApprover(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                             , ByVal mdtEmployeeAsonDate As Date, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                             , Optional ByVal intUserunkid As Integer = -1, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intApproverEmpId As Integer = -1, Optional ByVal intLeaveTypeID As Integer = -1 _
                                                             , Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                                             , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                                             , Optional ByVal blnExternalApprover As Boolean = False _
                                                             ) As DataTable 'S.SANDEEP [30 JAN 2016] -- START {blnExternalApprover} -- END
        'Gajanan [23-SEP-2019] -- Add [blnExternalApprover]    

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (19-Nov-2015) -- Start
        'Enhancement - WORKING ON Problem in Claim & Request Master For Getting Approver.

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        'Pinkal (19-Nov-2015) -- End

        Try
            'S.SANDEEP [30 JAN 2016] -- START
            'Dim xDateJoinQry, xDateFilterQry
            'xDateJoinQry = "" : xDateFilterQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)

            'If intUserunkid > 0 Then

            '    strQ = " SELECT lvleaveapprover_master.approverunkid, ISNULL(leaveapproverunkid, 0) employeeunkid ," & _
            '                   " ISNULL(firstname, '') + ' ' + ISNULL(surname, '') AS employeename,lvapproverlevel_master.priority " & _
            '                   " FROM lvleaveapprover_master "
            '    'Pinkal (09-Jul-2014) -- Start
            '    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
            '    strQ &= " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
            '    'Pinkal (09-Jul-2014) -- End

            '    strQ &= " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
            '       " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid  " & _
            '                " and hrapprover_usermapping.usertypeid=" & enUserType.Approver

            '    If xDateJoinQry.Trim.Length > 0 Then
            '        strQ &= xDateJoinQry
            '    End If

            '    strQ &= " WHERE isvoid = 0 AND hrapprover_usermapping.userunkid = @Userunkid"

            '    If intApproverEmpId > 0 Then
            '        strQ &= " AND hremployee_master.employeeunkid = @approverEmpId "
            '        objDataOperation.AddParameter("@approverEmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmpId.ToString)
            '    End If

            '    If xIncludeIn_ActiveEmployee = False Then
            '        If xDateFilterQry.Trim.Length > 0 Then
            '            strQ &= xDateFilterQry & " "
            '        End If
            '    End If

            '    objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)

            'End If

            'If intEmployeeunkid > 0 Then

            '    strQ = " SELECT lvleaveapprover_master.approverunkid, ISNULL(leaveapproverunkid, 0) employeeunkid , " & _
            '               " ISNULL(firstname, '') + ' ' + ISNULL(surname, '') AS employeename,lvapproverlevel_master.priority " & _
            '               " FROM lvleaveapprover_master "


            '    'Pinkal (09-Jul-2014) -- Start
            '    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
            '    strQ &= " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "
            '    'Pinkal (09-Jul-2014) -- End

            '    strQ &= " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid= 0" & _
            '       " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & _
            '       " and hrapprover_usermapping.usertypeid=" & enUserType.Approver & _
            '               " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid "


            '    If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
            '        blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
            '    End If

            '    If CBool(blnLeaveApproverForLeaveType) = True And intLeaveTypeID > 0 Then

            '        strQ &= "  JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
            '                    "  JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvapprover_leavetype_mapping.leavetypeunkid = @leavetypeId "

            '        objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID.ToString)

            '    End If

            '    If xDateJoinQry.Trim.Length > 0 Then
            '        strQ &= xDateJoinQry
            '    End If


            '    strQ &= " WHERE lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_tran.employeeunkid = @employeeunkid"
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

            '    If xIncludeIn_ActiveEmployee = False Then
            '        If xDateFilterQry.Trim.Length > 0 Then
            '            strQ &= xDateFilterQry & " "
            '        End If
            '    End If

            'End If

            ''If strEmployeeAsOnDate.Trim.ToString().Length <= 0 Then
            ''    strEmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
            ''End If

            ''strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            ''           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            ''           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            ''           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

            ''objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            ''objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)

            ''Pinkal (19-Mar-2015) -- Start
            ''Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            'strQ &= " AND lvleaveapprover_master.isswap  = 0 "
            ''Pinkal (19-Mar-2015) -- End


            ''Pinkal (17-Jun-2015) -- Start
            ''Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
            'strQ &= " AND lvleaveapprover_master.isactive = 1 "
            ''Pinkal (17-Jun-2015) -- End


            ''Pinkal (24-Aug-2015) -- Start
            ''Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'strQ &= " AND hremployee_master.isapproved = 1 "
            ''Pinkal (24-Aug-2015) -- End


            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If dsList.Tables.Count > 0 Then
            '    Return dsList.Tables("List")
            'Else
            '    Return Nothing
            'End If


            Dim StrQCondtion As String = ""
            Dim StrQCommJoin As String = ""
            Dim StrFinalQry As String = ""


            Dim xDateJoinQry, xDateFilterQry
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)

            Dim dsCompany As New DataSet

            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ &= "SELECT " & _
                   "     lvleaveapprover_master.approverunkid " & _
                   "    ,ISNULL(leaveapproverunkid, 0) employeeunkid " & _
                   "    ,#NameValue# AS employeename " & _
                   "    ,#EmailValue# AS approveremail " & _
                   "    ,lvapproverlevel_master.priority " & _
                   "    ,lvleaveapprover_master.leaveapproverunkid " & _
                   "    ,isexternalapprover "

            'THIS IS ADDED HERE DUE TO SAME METHOD WAS MADE FOR MAIL SENDING IN LEAVE FORM CLASS ONLY CHANGES IS ASSIGNED NAME OF EMPLOYEE INCLUDED [GetEmployeeApproverPriority]
            'AND LEFT JOIN IS INCLUDED.
            If intEmployeeunkid > 0 Then
                strQ &= "   ,ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') AS assigndemployeename "
            End If


            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            strQ &= ",lvapproverlevel_master.escalation_days "
            'Pinkal (01-Apr-2019) -- End

            'Gajanan [04-June-2020] -- Start
            If xIncludeIn_ActiveEmployee Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= ", Case When" & xDateFilterQry.ToString().Trim().Substring(xDateFilterQry.ToString().Trim().IndexOf("AND") + "AND".Length) & " THEN 1 ELSE 0 END AS IsActive "
                End If
            End If
            'Gajanan [04-June-2020] -- End


            strQ &= "FROM lvleaveapprover_master " & _
                    "  LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid "


            StrQCommJoin = " #EMP_JOIN#  " & _
                           " #DATE_JOIN# "

            If intUserunkid > 0 Then
                StrQCommJoin &= " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid AND hrapprover_usermapping.usertypeid=" & enUserType.Approver
            End If

            If intEmployeeunkid > 0 Then
                If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                End If

                If CBool(blnLeaveApproverForLeaveType) = True And intLeaveTypeID > 0 Then
                    StrQCommJoin &= " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
                                "  JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvapprover_leavetype_mapping.leavetypeunkid = @leavetypeId "

                    objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID.ToString)
                End If

                StrQCommJoin &= " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid= 0 " & _
                                " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid AND hrapprover_usermapping.usertypeid= " & enUserType.Approver & _
                                " LEFT JOIN hremployee_master h2 ON h2.employeeunkid = lvleaveapprover_tran.employeeunkid "
                End If


            'Pinkal (19-Oct-2017) -- Start
            'Enhancement - Bug Solved For InActive Approver in new Leave Application.
            StrQCondtion = " WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isactive = 1 AND lvleaveapprover_master.isswap  = 0 "
            'Pinkal (19-Oct-2017) -- End


            If intUserunkid > 0 Then
                StrQCondtion &= " AND hrapprover_usermapping.userunkid = @Userunkid "

                If intApproverEmpId > 0 Then

                    If blnExternalApprover Then
                        StrQCondtion &= " AND lvleaveapprover_master.leaveapproverunkid = @approverEmpId "
                    Else
                        StrQCondtion &= " AND hremployee_master.employeeunkid = @approverEmpId "
                End If

                    objDataOperation.AddParameter("@approverEmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmpId.ToString)
                End If
                objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)
                End If

            If intEmployeeunkid > 0 Then

                'Pinkal (23-Sep-2016) -- Start
                'Enhancement - Problem Solved when User ID & Employee ID is same(Duplicated in eZee company).
                'StrQCondtion &= " AND lvleaveapprover_tran.employeeunkid = @employeeunkid AND lvleaveapprover_master.isexternalapprover = #ExApr# "
                StrQCondtion &= " AND lvleaveapprover_tran.employeeunkid = @employeeunkid "
                'Pinkal (23-Sep-2016) -- End
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)
            End If


            'Pinkal (23-Sep-2016) -- Start
            'Enhancement - Problem Solved when User ID & Employee ID is same(Duplicated in eZee company).
            StrQCondtion &= " AND lvleaveapprover_master.isexternalapprover = #ExApr# "
            'Pinkal (23-Sep-2016) -- End

            StrFinalQry = strQ

            StrFinalQry &= StrQCommJoin & " "
            StrFinalQry &= StrQCondtion & " "
            StrFinalQry = StrFinalQry.Replace("#EMP_JOIN#", "LEFT JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid ")
            StrFinalQry = StrFinalQry.Replace("#DBName#", "")
            StrFinalQry = StrFinalQry.Replace("#NameValue#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')")
            StrFinalQry = StrFinalQry.Replace("#EmailValue#", "ISNULL(hremployee_master.email,'')")
            StrFinalQry = StrFinalQry.Replace("#DATE_JOIN#", xDateJoinQry)
            StrFinalQry = StrFinalQry.Replace("#ExApr#", "0")

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                    StrFinalQry &= xDateFilterQry & " "
                    End If
            End If

            dsList = objDataOperation.ExecQuery(StrFinalQry, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstmp As New DataSet

                xDateJoinQry = "" : xDateFilterQry = ""
                Dim StrDBName As String : StrFinalQry = "" : StrDBName = ""
                StrDBName = dr("DName")

                

                StrFinalQry = strQ

                StrFinalQry &= " LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & vbCrLf & _
                               StrQCommJoin & " "

                StrFinalQry = StrFinalQry.Replace("#EMP_JOIN#", " LEFT JOIN #DBName#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid AND hremployee_master.isapproved = 1 ")

                StrFinalQry &= StrQCondtion & " AND lvleaveapprover_master.isexternalapprover = 1 AND cfuser_master.companyunkid = #CoyId# "

                StrFinalQry = StrFinalQry.Replace("#CoyId#", dr("companyunkid"))
                StrFinalQry = StrFinalQry.Replace("#ExApr#", "1")

                If StrDBName.Trim.Length <= 0 Then
                    StrFinalQry = StrFinalQry.Replace("#DBName#", "")
                Else
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    StrFinalQry = StrFinalQry.Replace("#DBName#", StrDBName & "..")
                End If

                StrFinalQry = StrFinalQry.Replace("#NameValue#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                StrFinalQry = StrFinalQry.Replace("#EmailValue#", "CASE WHEN ISNULL(hremployee_master.email,'') = '' THEN ISNULL(cfuser_master.email,'') ELSE ISNULL(hremployee_master.email,'') END ")

                StrFinalQry = StrFinalQry.Replace("#DATE_JOIN#", xDateJoinQry)

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrFinalQry &= xDateFilterQry & " "
                    End If
                End If

                dstmp = objDataOperation.ExecQuery(StrFinalQry, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
            Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
            End If
            Next

            Return dsList.Tables(0)

            'S.SANDEEP [30 JAN 2016] -- END

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprover; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (19-Nov-2015) -- Start
            'Enhancement - WORKING ON Problem in Claim & Request Master For Getting Approver.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (19-Nov-2015) -- End
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetApproverEmployeeId(ByVal ApproveID As Integer, ByVal blnExternalApprover As Boolean) As String 'S.SANDEEP [30 JAN 2016] -- START {blnExternalApprover} -- END
        'Public Function GetApproverEmployeeId(ByVal ApproveID As Integer) As String
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.


            'strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
            '                   "+ CAST(lvleaveapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
            '          "FROM     lvleaveapprover_master " & _
            '                    "JOIN lvleaveapprover_tran ON lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid AND lvleaveapprover_tran.isvoid = 0" & _
            '          "WHERE    lvleaveapprover_master.leaveapproverunkid = " & ApproveID & " AND lvleaveapprover_master.isvoid = 0 " & _
            '          "ORDER BY lvleaveapprover_tran.employeeunkid " & _
            '        "FOR " & _
            '          "XML PATH('') " & _
            '        "), 1, 1, ''), '') EmployeeIds "


            'Pinkal (17-Jun-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

            'S.SANDEEP [30 JAN 2016] -- START

            'strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
            '                   "+ CAST(lvleaveapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
            '          "FROM     lvleaveapprover_master " & _
            '                    "JOIN lvleaveapprover_tran ON lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid AND lvleaveapprover_tran.isvoid = 0" & _
            '          "WHERE    lvleaveapprover_master.leaveapproverunkid = " & ApproveID & " AND lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive = 1 " & _
            '          "ORDER BY lvleaveapprover_tran.employeeunkid " & _
            '        "FOR " & _
            '          "XML PATH('') " & _
            '        "), 1, 1, ''), '') EmployeeIds "


            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                               "+ CAST(lvleaveapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      "FROM     lvleaveapprover_master " & _
                                "JOIN lvleaveapprover_tran ON lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid AND lvleaveapprover_tran.isvoid = 0" & _
                      "WHERE    lvleaveapprover_master.leaveapproverunkid = " & ApproveID & " AND lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive = 1 " & _
                      " AND lvleaveapprover_master.isexternalapprover = @isexternalapprover " & _
                      "ORDER BY lvleaveapprover_tran.employeeunkid " & _
                    "FOR " & _
                      "XML PATH('') " & _
                    "), 1, 1, ''), '') EmployeeIds "

            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)

            'S.SANDEEP [30 JAN 2016] -- END

            'Pinkal (17-Jun-2015) -- End

            'Pinkal (19-Mar-2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function

    'Nilay (18-Mar-2015) -- Start
    'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT



    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeFromApprover(ByVal xDatabaseName As String, ByVal intLeaveApproverEmpUnkid As Integer, ByVal intLevelId As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                 , ByVal blnExternalApprover As Boolean, Optional ByVal mblnIncludeInActiveEmployee As Boolean = True) As DataSet


        'Pinkal (14-May-2020) -- 'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.[ByVal xDatabaseName As String,Optional ByVal mblnIncludeInActiveEmployee As Boolean = True]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation



            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            If mblnIncludeInActiveEmployee = False Then
                xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)
            End If
            'Pinkal (14-May-2020) -- End


            Dim strApproverUnkids As String = ""
            strApproverUnkids = GetLeaveApproverUnkid(intLeaveApproverEmpUnkid, blnExternalApprover, intLevelId, objDataOperation)

            strQ = "SELECT  " & _
                      " 0 as 'select' " & _
                      ", lvleaveapprover_master.approverunkid " & _
                      ", lvleaveapprover_tran.leaveapprovertranunkid " & _
                      ", lvleaveapprover_master.leaveapproverunkid " & _
                      ", lvleaveapprover_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') as employeecode " & _
                      ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
                      ", lvapproverlevel_master.levelunkid " & _
                      ", lvapproverlevel_master.levelname " & _
                      ", lvapproverlevel_master.priority " & _
                      ", hremployee_master.gender " & _
                      ", hremployee_master.maritalstatusunkid " & _
                      ", hremployee_master.employmenttypeunkid " & _
                      ", hremployee_master.paytypeunkid " & _
                      ", hremployee_master.paypointunkid " & _
                      ", hremployee_master.nationalityunkid " & _
                      ", hremployee_master.religionunkid " & _
                      ", Alloc.stationunkid " & _
                      ", Alloc.deptgroupunkid " & _
                      ", Alloc.departmentunkid " & _
                      ", ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                      ", Alloc.sectionunkid " & _
                      ", ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                      ", Alloc.unitunkid " & _
                      ", Alloc.classgroupunkid " & _
                      ", Alloc.classunkid " & _
                      ", ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                      ", Jobs.jobgroupunkid " & _
                      ", Jobs.jobunkid " & _
                      ", Grds.gradegroupunkid " & _
                      ", Grds.gradeunkid " & _
                      ", Grds.gradelevelunkid " & _
                      ", CC.costcenterunkid " & _
                      ", lvapproverlevel_master.escalation_days " & _
                      " FROM    lvleaveapprover_master " & _
                      " JOIN lvleaveapprover_tran ON lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid AND lvleaveapprover_tran.isvoid = 0 " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_tran.employeeunkid " & _
                      " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND hremployee_master.isactive = 1 " & _
                      "LEFT JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         stationunkid " & _
                      "        ,deptgroupunkid " & _
                      "        ,departmentunkid " & _
                      "        ,sectiongroupunkid " & _
                      "        ,sectionunkid " & _
                      "        ,unitgroupunkid " & _
                      "        ,unitunkid " & _
                      "        ,teamunkid " & _
                      "        ,classgroupunkid " & _
                      "        ,classunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         jobunkid " & _
                      "        ,jobgroupunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         gradegroupunkid " & _
                      "        ,gradeunkid " & _
                      "        ,gradelevelunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "    FROM prsalaryincrement_tran " & _
                      "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "     SELECT " & _
                      "         cctranheadvalueid as costcenterunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_cctranhead_tran " & _
                      "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                      "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                      " ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            If mblnIncludeInActiveEmployee = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If
            End If
            'Pinkal (14-May-2020) -- End


            strQ &= " WHERE   lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.leaveapproverunkid = " & intLeaveApproverEmpUnkid


            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", lvapproverlevel_master.escalation_days " & _]


            If strApproverUnkids.Trim.Length > 0 Then
                strQ &= " AND lvleaveapprover_master.approverunkid IN (" & strApproverUnkids & ") "
            End If

            If intLevelId >= 0 Then
                strQ &= " AND lvleaveapprover_master.levelunkid = " & intLevelId
            End If
            strQ &= " AND hremployee_master.isapproved = 1 "

            strQ &= " AND lvleaveapprover_master.isswap = 0 "

            strQ &= " AND lvleaveapprover_master.isactive = 1 "


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            If mblnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If
            'Pinkal (14-May-2020) -- End


            strQ &= " ORDER By levelname,isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromApprover", mstrMessage)
        End Try
        Return dsList
    End Function

    'Pinkal (24-Aug-2015) -- End

    'Nilay (18-Mar-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetLevelFromLeaveApprover(ByVal intLeaveApproveEmpunkid As Integer, ByVal blnExternalApprover As Boolean, Optional ByVal blnFlag As Boolean = True) As DataSet 'S.SANDEEP [30 JAN 2016] -- START -- END
        'Public Function GetLevelFromLeaveApprover(Optional ByVal blnFlag As Boolean = True, Optional ByVal intLeaveApproveunkid As Integer = -1) As DataSet

        'Pinkal (31-Jan-2017) --  'Enhancement - Solving Bug of Visibility Issue for TRA.[ByVal intLeaveApproveunkid As Integer]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation


            'S.SANDEEP [30 JAN 2016] -- START
            'If blnFlag Then
            '    strQ = " Select 0 as levelunkid , @name as levelname UNION "
            'End If

            'strQ &= " SELECT  lvleaveapprover_master.levelunkid,levelname " & _
            '          " FROM lvleaveapprover_master " & _
            '          " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
            '          " WHERE  lvleaveapprover_master.isvoid = 0 "

            'If intLeaveApproveunkid > 0 Then
            '    strQ &= " AND leaveapproverunkid = " & intLeaveApproveunkid
            'End If


            ''Pinkal (19-Mar-2015) -- Start
            ''Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
            'strQ &= " AND lvleaveapprover_master.isswap = 0 "
            ''Pinkal (19-Mar-2015) -- End


            ''Pinkal (17-Jun-2015) -- Start
            ''Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
            'strQ &= " AND lvleaveapprover_master.isactive = 1 "
            ''Pinkal (17-Jun-2015) -- End


            Dim strApproverUnkids As String = ""

            strApproverUnkids = GetLeaveApproverUnkid(intLeaveApproveEmpunkid, blnExternalApprover, , objDataOperation)

            If blnFlag Then
                strQ = " Select 0 as levelunkid , @name as levelname UNION "
            End If

            strQ &= " SELECT  lvleaveapprover_master.levelunkid,levelname " & _
                      " FROM lvleaveapprover_master " & _
                      " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                      " WHERE  lvleaveapprover_master.isvoid = 0 "

            If intLeaveApproveEmpunkid > 0 Then
                strQ &= " AND leaveapproverunkid = " & intLeaveApproveEmpunkid
            End If

            If strApproverUnkids.Trim.Length > 0 Then
                strQ &= " AND lvleaveapprover_master.approverunkid IN (" & strApproverUnkids & ") "
            End If


            strQ &= " AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive = 1 "

            'S.SANDEEP [30 JAN 2016] -- END

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            strQ &= " ORDER BY levelunkid "
            'Gajanan [23-SEP-2019] -- End


            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLevelFromLeaveApprover", mstrMessage)
        End Try
        Return dsList
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 
    'Public Function GetEmployeeFromUser(ByVal intUserID As Integer, ByVal blnIsApprovePrivilege As Boolean, ByVal blnIssuePrivilage As Boolean) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try

    '        Dim objDataOperation As New clsDataOperation
    '        Dim blnIsApprove As Boolean = False

    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes
    '        strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename "
    '        'Pinkal (06-Mar-2014) -- End


    '        If blnIsApprovePrivilege = True Then
    '            strQ &= " UNION " & _
    '              "SELECT " & _
    '              "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
    '              "	,ISNULL(employeecode,'') AS employeecode " & _
    '              "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
    '              "FROM lvleaveapprover_tran " & _
    '              "	JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                   " JOIN hrapprover_usermapping ON lvleaveapprover_tran.approverunkid = hrapprover_usermapping.approverunkid " & _
    '               "  AND hrapprover_usermapping.userunkid = " & intUserID & " AND hrapprover_usermapping.usertypeid = " & enUserType.Approver & _
    '              "WHERE 1 = 1  AND lvleaveapprover_tran.isvoid = 0 "

    '            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            End If

    '            strQ &= " AND hremployee_master.isapproved = 1 "

    '            blnIsApprove = True
    '        End If

    '        If blnIssuePrivilage Then

    '            If blnIsApprove Then strQ &= " UNION "

    '            strQ &= " SELECT " & _
    '                         "    0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename " & _
    '                         " UNION " & _
    '                         " SELECT  ISNULL(hremployee_master.employeeunkid, 0) AS employeeunkid " & _
    '                         ", ISNULL(employeecode, '') AS employeecode " & _
    '                         ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS employeename " & _
    '                         " FROM lvissueuser_tran " & _
    '                         " JOIN hremployee_master ON lvissueuser_tran.employeeunkid = hremployee_master.employeeunkid AND hremployee_master.isapproved = 1 " & _
    '                         " WHERE lvissueuser_tran.isvoid = 0 "

    '            strQ &= " AND lvissueuser_tran.userunkid = " & intUserID


    '            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @edate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@stdate) >= @stdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@stdate) >= @stdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @stdate) >= @stdate "
    '                objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '                objDataOperation.AddParameter("@edate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

    '            End If

    '        End If


    '        strQ &= "ORDER BY employeename "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeFromUser", mstrMessage)
    '        Return Nothing
    '    End Try
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeFromUser(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                  , ByVal mdtEmployeeAsOnDate As Date, ByVal blnIncludeInactiveEmployee As Boolean _
                                                                  , ByVal intUserID As Integer, ByVal blnIsApprovePrivilege As Boolean, ByVal blnIssuePrivilage As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)

            Dim objDataOperation As New clsDataOperation
            Dim blnIsApprove As Boolean = False

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename "
            strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename, '  ' + @Select AS EmpCodeName  "
            'Nilay (09-Aug-2016) -- End

            If blnIsApprovePrivilege = True Then
                strQ &= " UNION " & _
                  "SELECT " & _
                  "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
                  "	,ISNULL(employeecode,'') AS employeecode " & _
                  "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
                  " ,ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName " & _
                  "FROM lvleaveapprover_tran " & _
                  "	JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       " JOIN hrapprover_usermapping ON lvleaveapprover_tran.approverunkid = hrapprover_usermapping.approverunkid " & _
                             "  AND hrapprover_usermapping.userunkid = " & intUserID & " AND hrapprover_usermapping.usertypeid = " & enUserType.Approver
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                'ADDED - [ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName "]
                'Nilay (09-Aug-2016) -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                strQ &= " WHERE 1 = 1  AND lvleaveapprover_tran.isvoid = 0 "

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'End If

                strQ &= " AND hremployee_master.isapproved = 1 "

                If blnIncludeInactiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

                Dim iExApproverIds As String = ""

                iExApproverIds = GetLeaveApproverUnkid(intUserID, True, , objDataOperation)

                If iExApproverIds.Trim.Length > 0 Then
                    strQ &= "UNION " & _
                            " SELECT " & _
                            "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
                            "	,ISNULL(employeecode,'') AS employeecode " & _
                            "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
                            "   ,ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName " & _
                            " FROM lvleaveapprover_tran " & _
                            "  JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid = hremployee_master.employeeunkid "
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    'ADDED - [ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName "]
                    'Nilay (09-Aug-2016) -- End

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    strQ &= " WHERE 1 = 1  AND lvleaveapprover_tran.isvoid = 0 AND lvleaveapprover_tran.approverunkid IN(" & iExApproverIds & ") "

                    If blnIncludeInactiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry & " "
                        End If
                    End If

                    strQ &= " AND hremployee_master.isapproved = 1 "
                End If

                blnIsApprove = True
            End If

            If blnIssuePrivilage Then

                If blnIsApprove Then strQ &= " UNION "

                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                'strQ &= " SELECT " & _
                '             "    0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename " & _
                '             " UNION " & _
                '             " SELECT  ISNULL(hremployee_master.employeeunkid, 0) AS employeeunkid " & _
                '             ", ISNULL(employeecode, '') AS employeecode " & _
                '             ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS employeename " & _
                '             " FROM lvissueuser_tran " & _
                '             " JOIN hremployee_master ON lvissueuser_tran.employeeunkid = hremployee_master.employeeunkid AND hremployee_master.isapproved = 1 "
                strQ &= " SELECT " & _
                             "    0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename, '  ' + @Select AS EmpCodeName " & _
                             " UNION " & _
                             " SELECT  ISNULL(hremployee_master.employeeunkid, 0) AS employeeunkid " & _
                             ", ISNULL(employeecode, '') AS employeecode " & _
                             ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS employeename " & _
                             ", ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName " & _
                             " FROM lvissueuser_tran " & _
                             " JOIN hremployee_master ON lvissueuser_tran.employeeunkid = hremployee_master.employeeunkid AND hremployee_master.isapproved = 1 "
                'Nilay (09-Aug-2016) -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                strQ &= " WHERE lvissueuser_tran.isvoid = 0 "

                strQ &= " AND lvissueuser_tran.userunkid = " & intUserID


                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @edate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@stdate) >= @stdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@stdate) >= @stdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @stdate) >= @stdate "
                '    objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                '    objDataOperation.AddParameter("@edate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                ' End If

                If blnIncludeInactiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

            End If


            strQ &= "ORDER BY employeename "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromUser", mstrMessage)
            Return Nothing
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function ImportApproverInsert() As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
            objDataOperation.AddParameter("@approval_to", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnApproval_To.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSwap.ToString)


            'Pinkal (17-Jun-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            'Pinkal (17-Jun-2015) -- End

            'S.SANDEEP [30 JAN 2016] -- START
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover.ToString)
            'S.SANDEEP [30 JAN 2016] -- END

            'strQ = "INSERT INTO lvleaveapprover_master ( " & _
            '  "  levelunkid " & _
            '  ", code " & _
            '  ", leaveapproverunkid " & _
            '  ", departmentunkid" & _
            '  ", sectionunkid" & _
            '  ", jobunkid" & _
            '  ", approval_to " & _
            '  ", isvoid " & _
            '  ", voiddatetime " & _
            '  ", userunkid " & _
            '  ", voiduserunkid" & _
            '  ", voidreason" & _
            '") VALUES (" & _
            '  "  @levelunkid " & _
            '  ", @code " & _
            '  ", @leaveapproverunkid " & _
            '  ", @departmentunkid" & _
            '  ", @sectionunkid" & _
            '  ", @jobunkid" & _
            '  ", @approval_to " & _
            '  ", @isvoid " & _
            '  ", @voiddatetime " & _
            '  ", @userunkid " & _
            '  ", @voiduserunkid" & _
            '  ", @voidreason" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO lvleaveapprover_master ( " & _
              "  levelunkid " & _
              ", code " & _
              ", leaveapproverunkid " & _
              ", departmentunkid" & _
              ", sectionunkid" & _
              ", jobunkid" & _
              ", approval_to " & _
                    ", isswap " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", voiduserunkid" & _
              ", voidreason" & _
              ", isactive " & _
              ", isexternalapprover" & _
            ") VALUES (" & _
              "  @levelunkid " & _
              ", @code " & _
              ", @leaveapproverunkid " & _
              ", @departmentunkid" & _
              ", @sectionunkid" & _
              ", @jobunkid" & _
              ", @approval_to " & _
                    ", @isswap " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @userunkid " & _
              ", @voiduserunkid" & _
              ", @voidreason" & _
              ", @isactive " & _
              ", @isexternalapprover " & _
            "); SELECT @@identity"

            'Pinkal (19-Mar-2015) -- End


            'Pinkal (17-Jun-2015) -- 'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE. [isactive] 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApproverunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [30 JAN 2016] -- START
            If mblnIsexternalapprover = True Then
                Dim objUserMapping As New clsapprover_Usermapping
                objUserMapping.GetData(enUserType.Approver, mintApproverunkid, -1, -1)

                objUserMapping._Approverunkid = mintApproverunkid
                objUserMapping._Userunkid = mintleaveapproverunkid
                objUserMapping._UserTypeid = enUserType.Approver
                objUserMapping._AuditUserunkid = mintUserunkid

                With objUserMapping
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objUserMapping._Mappingunkid > 0 Then
                    If objUserMapping.Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'S.SANDEEP |29-MAR-2019| -- START
            Else
                strQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
                       "FROM hremployee_master " & _
                       "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
                       "WHERE hremployee_master.employeeunkid = '" & mintleaveapproverunkid & "' "

                dsList = objDataOperation.ExecQuery(strQ, "Usr")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Usr").Rows.Count > 0 Then
                    If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then
                        Dim objUserMapping As New clsapprover_Usermapping
                        objUserMapping.GetData(enUserType.Approver, mintApproverunkid, -1, -1)
                        objUserMapping._Approverunkid = mintApproverunkid
                        objUserMapping._Userunkid = dsList.Tables("Usr").Rows(0).Item("UsrId")
                        objUserMapping._UserTypeid = enUserType.Approver
                        objUserMapping._AuditUserunkid = mintUserunkid

                        If objUserMapping._Mappingunkid > 0 Then
                            If objUserMapping.Update(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Else
                            If objUserMapping.Insert(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                End If
                'S.SANDEEP |29-MAR-2019| -- END
            End If
            'S.SANDEEP [30 JAN 2016] -- END

            Return mintApproverunkid

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function GetApproverID(ByVal intLeaveApproverId As Integer, ByVal intLevelId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intApproverID As Integer = -1
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            strQ = " Select ISNULL(approverunkid,-1)  approverunkid FROM  lvleaveapprover_master WHERE leaveapproverunkid =  @leaveapproverunkid AND levelunkid = @levelunkid "


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            strQ &= " AND isswap = 0 AND isvoid = 0  "
            'Pinkal (14-May-2020) -- End


            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveApproverId.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelId.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "Approver")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApproverID = CInt(dsList.Tables(0).Rows(0)("approverunkid"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverID", mstrModuleName)
        End Try
        Return intApproverID
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Pinkal (15-Oct-2014) -- Start
    'Enhancement -  AKFTZ LEAVE APPROVER PROBLEM FOR VISIBILITY IN LEAVE PROCESS PENDING TRAN

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    'Public Function GetApproverFromEmployee(Optional ByVal intUserunkid As Integer = -1, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intLeaveTypeID As Integer = -1 _
    '                                                      , Optional ByVal blnLeaveApproverForLeaveType As String = "" _
    '                                                      , Optional ByVal strEmployeeAsOnDate As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    objDataOperation = New clsDataOperation

    '    Try

    '        If intUserunkid > 0 Then

    '            strQ = " SELECT lvleaveapprover_master.*, lvapproverlevel_master.priority " & _
    '                           " FROM lvleaveapprover_master " & _
    '                           " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
    '                           " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
    '                           " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid  And hrapprover_usermapping.usertypeid = " & enUserType.Approver & _
    '                           " WHERE isvoid = 0 AND hrapprover_usermapping.userunkid = @Userunkid"

    '            objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)

    '        End If

    '        If intEmployeeunkid > 0 Then

    '            strQ = " SELECT lvleaveapprover_master.*,lvapproverlevel_master.priority " & _
    '                       " FROM lvleaveapprover_master " & _
    '                       " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
    '                       " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid= 0" & _
    '                       " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & _
    '                       " and hrapprover_usermapping.usertypeid=" & enUserType.Approver & _
    '                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid "

    '            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '            End If

    '            If CBool(blnLeaveApproverForLeaveType) = True And intLeaveTypeID > 0 Then

    '                strQ &= "  JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
    '                            "  JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvapprover_leavetype_mapping.leavetypeunkid = @leavetypeId "

    '                objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID.ToString)

    '            End If

    '            strQ &= " WHERE lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_tran.employeeunkid = @employeeunkid"

    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

    '        End If


    '        If strEmployeeAsOnDate.Trim.ToString().Length <= 0 Then
    '            strEmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
    '        End If

    '        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
    '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)



    '        'Pinkal (17-Jun-2015) -- Start
    '        'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
    '        strQ &= " AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive =  1 "
    '        'Pinkal (17-Jun-2015) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables.Count > 0 Then
    '            Return dsList
    '        Else
    '            Return Nothing
    '        End If

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: GetApproverFromEmployee; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function GetApproverFromEmployee(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                  , ByVal mdtEmployeeAsOnDate As Date, ByVal blnIncludeInactiveEmployee As Boolean _
                                                                  , Optional ByVal intUserunkid As Integer = -1, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intLeaveTypeID As Integer = -1 _
                                                                  , Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
       'Pinkal (02-Dec-2015) -- Start     'Enhancement - Solving Leave bug in Self Service. [Optional ByVal objDoOperation As clsDataOperation = Nothing] 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (02-Dec-2015) -- Start
        'Enhancement - Solving Leave bug in Self Service.

        If objDoOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        'Pinkal (02-Dec-2015) -- End

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)

        Try

            If intUserunkid > 0 Then


                strQ = " SELECT lvleaveapprover_master.*, lvapproverlevel_master.priority, lvapproverlevel_master.escalation_days " & _
                               " FROM lvleaveapprover_master " & _
                               " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                               " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
                               " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid  And hrapprover_usermapping.usertypeid = " & enUserType.Approver

                'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[lvapproverlevel_master.escalation_days]

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                strQ &= " WHERE isvoid = 0 AND hrapprover_usermapping.userunkid = @Userunkid"

                If blnIncludeInactiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

                objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)

            End If

            If intEmployeeunkid > 0 Then

                strQ = " SELECT lvleaveapprover_master.*,lvapproverlevel_master.priority,lvapproverlevel_master.escalation_days " & _
                           " FROM lvleaveapprover_master " & _
                           " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                           " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid= 0" & _
                           " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & _
                           " and hrapprover_usermapping.usertypeid=" & enUserType.Approver & _
                           " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveapprover_master.leaveapproverunkid "

                'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[lvapproverlevel_master.escalation_days]

                If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                End If

                If CBool(blnLeaveApproverForLeaveType) = True And intLeaveTypeID > 0 Then

                    strQ &= "  JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
                                "  JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvapprover_leavetype_mapping.leavetypeunkid = @leavetypeId "

                    objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID.ToString)

                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If


                strQ &= " WHERE lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_tran.employeeunkid = @employeeunkid"

                If blnIncludeInactiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry & " "
                    End If
                End If

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

            End If


            'If strEmployeeAsOnDate.Trim.ToString().Length <= 0 Then
            '    strEmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
            'End If

            'strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)


            'Pinkal (17-Jun-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.
            strQ &= " AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive =  1 "
            'Pinkal (17-Jun-2015) -- End


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            strQ &= " AND hremployee_master.isapproved = 1 "
            'Pinkal (24-Aug-2015) -- End



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables.Count > 0 Then
                Return dsList
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverFromEmployee; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (02-Dec-2015) -- Start
            'Enhancement - Solving Leave bug in Self Service.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (02-Dec-2015) -- End

        End Try
        Return dsList
    End Function

    'Pinkal (15-Oct-2014) -- End


    'Pinkal (19-Mar-2015) -- Start
    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    Public Function SwapApprover(ByVal dtFromEmployee As DataTable, ByVal dtToEmployee As DataTable, ByVal mblnLeaveApproverForLeaveType As Boolean, ByVal mblnPaymentApprovalwithLeaveApproval As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            '==================================START FOR FROM APPROVER=====================================

            Dim objLeavForm As New clsleaveform
            Dim mstrFormId As String = ""

            If dtFromEmployee IsNot Nothing Then

                strQ = "UPDATE lvleaveapprover_master SET isswap = 1 WHERE approverunkid = @approverunkid and isswap = 0  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveapprover_master", "approverunkid", mintSwapFromApproverID) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


                objDoOperation = objDataOperation

                _Approverunkid = mintSwapFromApproverID


                'Pinkal (17-Jun-2015) -- Start
                'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

                strQ = "INSERT INTO lvleaveapprover_master ( " & _
                          "  levelunkid " & _
                          ", leaveapproverunkid " & _
                          ", isswap " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", userunkid " & _
                          ", voiduserunkid" & _
                          ", voidreason" & _
                          ", isactive " & _
                          ", isexternalapprover" & _
                        ") VALUES (" & _
                          "  @levelunkid " & _
                          ", @leaveapproverunkid " & _
                          ", @isswap " & _
                          ", @isvoid " & _
                          ", @voiddatetime " & _
                          ", @userunkid " & _
                          ", @voiduserunkid" & _
                          ", @voidreason" & _
                          ", @isactive " & _
                          ", @isexternalapprover" & _
                        "); SELECT @@identity" 'S.SANDEEP [30 JAN 2016] -- END

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
                objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)
                objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                'S.SANDEEP [30 JAN 2016] -- START
                objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover)
                'S.SANDEEP [30 JAN 2016] -- END

                dsList = objDataOperation.ExecQuery(strQ, "List")

                'Pinkal (17-Jun-2015) -- End

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintApproverunkid = dsList.Tables(0).Rows(0).Item(0)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvleaveapprover_master", "approverunkid", mintApproverunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END



                strQ = " SELECT  ISNULL(mappingunkid,0) as mappingunkid,userunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.Approver
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    strQ = " DELETE FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.Approver
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                    objDataOperation.ExecNonQuery(strQ)


                    Dim objUserMapping As New clsapprover_Usermapping
                    objUserMapping._Approverunkid = mintApproverunkid
                    objUserMapping._Userunkid = CInt(dsList.Tables(0).Rows(0)("userunkid"))
                    objUserMapping._UserTypeid = enUserType.Approver

                    objUserMapping._FormName = mstrFormName
                    objUserMapping._LoginEmployeeunkid = mintLoginEmployeeunkid
                    objUserMapping._ClientIP = mstrClientIP
                    objUserMapping._HostName = mstrHostName
                    objUserMapping._FromWeb = mblnIsWeb
                    objUserMapping._AuditUserId = mintAuditUserId
objUserMapping._CompanyUnkid = mintCompanyUnkid
                    objUserMapping._AuditDate = mdtAuditDate

                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objUserMapping = Nothing

                End If

                If mblnLeaveApproverForLeaveType Then

                    Dim objApproverLvMapping As New clsapprover_leavetype_mapping
                    dsList = objApproverLvMapping.GetList("List", True, mintSwapFromApproverID)

                    strQ = " UPDATE lvapprover_leavetype_mapping SET approverunkid = @newapproverunkid WHERE approverunkid = @oldapproverunkid AND leavetypeunkid = @leavetypunkid AND isvoid = 0 "

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                        objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        objDataOperation.AddParameter("@leavetypunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("leavetypeunkid")))
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvapprover_leavetype_mapping", "leavetypemappingunkid", CInt(dr("leavetypemappingunkid")), False, mintUserunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END


                    Next
                    objApproverLvMapping = Nothing
                End If


                dsList = Nothing
                For Each drRow As DataRow In dtFromEmployee.Rows

                    strQ = " UPDATE lvleaveapprover_tran SET approverunkid = @newapproverunkid WHERE isvoid = 0 AND approverunkid = @oldapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                    objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", CInt(drRow("leaveapprovertranunkid")), 2, 2, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    mstrFormId = objLeavForm.GetApproverPendingLeaveForm(mintSwapToApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If mstrFormId.Trim.Length > 0 Then

                        strQ = " Update lvpendingleave_tran set approverunkid  = @newapproverunkid,approvertranunkid = @newapprovertranunkid  WHERE formunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lvpendingleave_tran.isvoid = 0 "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "Select ISNULL(pendingleavetranunkid,0) pendingleavetranunkid FROM lvpendingleave_tran WHERE approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND formunkid  in (" & mstrFormId & ") AND isvoid = 0"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", dsList.Tables(0).Rows(k)("pendingleavetranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Next
                        End If

                        Dim arFormId As String() = mstrFormId.Trim.Split(",")

                        If arFormId.Length > 0 Then

                            For iDay As Integer = 0 To arFormId.Length - 1
                                objLeavForm._Formunkid = CInt(arFormId(iDay))
                                If objLeavForm._Statusunkid <> 1 Then Continue For

                                strQ = " UPDATE lvleaveday_fraction SET approverunkid = @approverunkid WHERE approverunkid > 0 AND formunkid = " & CInt(arFormId(iDay))
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                strQ = "Select ISNULL(dayfractionunkid,0) AS dayfractionunkid  FROM lvleaveday_fraction WHERE approverunkid = @approverunkid AND formunkid  = " & CInt(arFormId(iDay)) & " AND isvoid = 0"
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                                Dim dsFractionList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If dsFractionList IsNot Nothing AndAlso dsFractionList.Tables(0).Rows.Count > 0 Then

                                    For k As Integer = 0 To dsFractionList.Tables(0).Rows.Count - 1

                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        objCommonATLog = New clsCommonATLog
                                        objCommonATLog._FormName = mstrFormName
                                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                        objCommonATLog._ClientIP = mstrClientIP
                                        objCommonATLog._HostName = mstrHostName
                                        objCommonATLog._FromWeb = mblnIsWeb
                                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                        objCommonATLog._AuditDate = mdtAuditDate
                                        'S.SANDEEP [28-May-2018] -- END

                                        If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveday_fraction", "dayfractionunkid", dsFractionList.Tables(0).Rows(k)("dayfractionunkid").ToString(), False) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        objCommonATLog = Nothing
                                        'S.SANDEEP [28-May-2018] -- END


                                    Next

                                End If

                            Next


                        End If

                    End If


                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                    If mblnPaymentApprovalwithLeaveApproval Then

                        Dim mstrClaimFormId As String = ""
                        Dim dsClaimList As DataSet = Nothing

                        strQ = " SELECT ISNULL(STUFF( " & _
                                  " (SELECT  DISTINCT ',' +  CAST(cmclaim_request_master.crmasterunkid AS NVARCHAR(max)) " & _
                                  "   FROM cmclaim_request_master " & _
                                  "   JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid  " & _
                                  "  WHERE cmclaim_request_master.statusunkid in (2) AND cmclaim_request_master.employeeunkid IN (" & CInt(drRow("employeeunkid")) & ")" & _
                                  " AND cmclaim_request_master.isvoid = 0  AND cmclaim_approval_tran.isvoid = 0 AND modulerefunkid = " & enModuleReference.Leave & _
                                  "  FOR XML PATH('')),1,1,''),'') AS CSV"

                        objDataOperation.ClearParameters()
                        dsClaimList = objDataOperation.ExecQuery(strQ, "FormList")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mstrClaimFormId = dsClaimList.Tables(0).Rows(0)("CSV").ToString()


                        If mstrClaimFormId.Trim.Length > 0 Then

                            strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newempapproverunkid,crapproverunkid = @newcrapproverunkid " & _
                                      " WHERE crmasterunkid in (" & mstrClaimFormId & ") AND crapproverunkid = @crapproverunkid AND cmclaim_approval_tran.isvoid = 0 "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@newempapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                            objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran " & _
                                      " WHERE approveremployeeunkid = @approveremployeeunkid AND crapproverunkid = @newcrapproverunkid AND crmasterunkid  in (" & mstrClaimFormId & ") AND isvoid = 0"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                            objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                            dsList = objDataOperation.ExecQuery(strQ, "List")

                            If dsList.Tables(0).Rows.Count > 0 Then
                                For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Next
                            End If

                        End If
                    End If

                    'Pinkal (01-Mar-2016) -- End

                    mstrEmployeeID &= drRow("employeeunkid").ToString() & ","

                Next

                If mstrEmployeeID.Trim.Length > 0 Then
                    mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
                End If

                Dim objMigration As New clsMigration
                objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationfromunkid = mintSwapFromApproverID
                objMigration._Migrationtounkid = mintSwapToApproverID
                objMigration._Usertypeid = enUserType.Approver
                objMigration._Userunkid = mintUserunkid
                objMigration._Migratedemployees = mstrEmployeeID

                objMigration._FormName = mstrFormName
                objMigration._LoginEmployeeunkid = mintLoginEmployeeunkid
                objMigration._ClientIP = mstrClientIP
                objMigration._HostName = mstrHostName
                objMigration._FromWeb = mblnIsWeb
                objMigration._AuditUserId = mintAuditUserId
objMigration._CompanyUnkid = mintCompanyUnkid
                objMigration._AuditDate = mdtAuditDate

                If objMigration.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            End If

            '==================================END FOR FROM APPROVER=====================================

            '==================================START FOR TO APPROVER=====================================


            objLeavForm._Formunkid = 0
            mstrFormId = ""
            mstrEmployeeID = ""

            If dtToEmployee IsNot Nothing Then

                strQ = "UPDATE lvleaveapprover_master SET isswap = 1 WHERE approverunkid = @approverunkid and isswap = 0  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveapprover_master", "approverunkid", mintSwapToApproverID) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


                objDoOperation = objDataOperation

                _Approverunkid = mintSwapToApproverID


                'Pinkal (17-Jun-2015) -- Start
                'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

                strQ = "INSERT INTO lvleaveapprover_master ( " & _
                          "  levelunkid " & _
                          ", leaveapproverunkid " & _
                          ", isswap " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", userunkid " & _
                          ", voiduserunkid" & _
                          ", voidreason" & _
                          ", isactive " & _
                          ", isexternalapprover" & _
                        ") VALUES (" & _
                          "  @levelunkid " & _
                          ", @leaveapproverunkid " & _
                          ", @isswap " & _
                          ", @isvoid " & _
                          ", @voiddatetime " & _
                          ", @userunkid " & _
                          ", @voiduserunkid" & _
                          ", @voidreason" & _
                          ", @isactive " & _
                          ", @isexternalapprover" & _
                        "); SELECT @@identity"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
                objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)
                objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                'S.SANDEEP [30 JAN 2016] -- START
                objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternalapprover)
                'S.SANDEEP [30 JAN 2016] -- END

                dsList = objDataOperation.ExecQuery(strQ, "List")

                'Pinkal (17-Jun-2015) -- End

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintApproverunkid = dsList.Tables(0).Rows(0).Item(0)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvleaveapprover_master", "approverunkid", mintApproverunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END



                strQ = " SELECT  ISNULL(mappingunkid,0) as mappingunkid,userunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.Approver
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    strQ = " DELETE FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & enUserType.Approver
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                    objDataOperation.ExecNonQuery(strQ)


                    Dim objUserMapping As New clsapprover_Usermapping
                    objUserMapping._Approverunkid = mintApproverunkid
                    objUserMapping._Userunkid = CInt(dsList.Tables(0).Rows(0)("userunkid"))
                    objUserMapping._UserTypeid = enUserType.Approver
                    objUserMapping._FormName = mstrFormName
                    objUserMapping._LoginEmployeeunkid = mintLoginEmployeeunkid
                    objUserMapping._ClientIP = mstrClientIP
                    objUserMapping._HostName = mstrHostName
                    objUserMapping._FromWeb = mblnIsWeb
                    objUserMapping._AuditUserId = mintAuditUserId
objUserMapping._CompanyUnkid = mintCompanyUnkid
                    objUserMapping._AuditDate = mdtAuditDate
                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objUserMapping = Nothing
                End If


                If mblnLeaveApproverForLeaveType Then

                    Dim objApproverLvMapping As New clsapprover_leavetype_mapping
                    dsList = objApproverLvMapping.GetList("List", True, mintSwapToApproverID)

                    strQ = " UPDATE lvapprover_leavetype_mapping SET approverunkid = @newapproverunkid WHERE approverunkid = @oldapproverunkid AND leavetypeunkid = @leavetypunkid AND isvoid = 0 "

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                        objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                        objDataOperation.AddParameter("@leavetypunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("leavetypeunkid")))
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvapprover_leavetype_mapping", "leavetypemappingunkid", CInt(dr("leavetypemappingunkid")), False, mintUserunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END


                    Next
                    dsList = Nothing
                    objApproverLvMapping = Nothing
                End If


                For Each drRow As DataRow In dtToEmployee.Rows

                    strQ = " UPDATE lvleaveapprover_tran SET approverunkid = @newapproverunkid WHERE isvoid = 0 AND approverunkid = @oldapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                    objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", CInt(drRow("leaveapprovertranunkid")), 2, 2, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    mstrFormId = objLeavForm.GetApproverPendingLeaveForm(mintSwapFromApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If mstrFormId.Trim.Length > 0 Then

                        strQ = " Update lvpendingleave_tran set approverunkid  = @newapproverunkid,approvertranunkid = @newapprovertranunkid  WHERE formunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lvpendingleave_tran.isvoid = 0 "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "Select ISNULL(pendingleavetranunkid,0) pendingleavetranunkid FROM lvpendingleave_tran WHERE approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND formunkid  in (" & mstrFormId & ") AND isvoid = 0"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", dsList.Tables(0).Rows(k)("pendingleavetranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Next
                        End If

                        Dim arFormId As String() = mstrFormId.Trim.Split(",")

                        If arFormId.Length > 0 Then

                            For iDay As Integer = 0 To arFormId.Length - 1
                                objLeavForm._Formunkid = CInt(arFormId(iDay))
                                If objLeavForm._Statusunkid <> 1 Then Continue For

                                strQ = " UPDATE lvleaveday_fraction SET approverunkid = @approverunkid WHERE approverunkid > 0 AND formunkid = " & CInt(arFormId(iDay))
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                strQ = "Select ISNULL(dayfractionunkid,0) AS dayfractionunkid  FROM lvleaveday_fraction WHERE approverunkid = @approverunkid AND formunkid  = " & CInt(arFormId(iDay)) & " AND isvoid = 0"
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                                Dim dsFractionList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If dsFractionList IsNot Nothing AndAlso dsFractionList.Tables(0).Rows.Count > 0 Then

                                    For k As Integer = 0 To dsFractionList.Tables(0).Rows.Count - 1

                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        objCommonATLog = New clsCommonATLog
                                        objCommonATLog._FormName = mstrFormName
                                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                        objCommonATLog._ClientIP = mstrClientIP
                                        objCommonATLog._HostName = mstrHostName
                                        objCommonATLog._FromWeb = mblnIsWeb
                                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                        objCommonATLog._AuditDate = mdtAuditDate
                                        'S.SANDEEP [28-May-2018] -- END

                                        If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveday_fraction", "dayfractionunkid", dsFractionList.Tables(0).Rows(k)("dayfractionunkid").ToString(), False) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        objCommonATLog = Nothing
                                        'S.SANDEEP [28-May-2018] -- END


                                    Next

                                End If

                            Next

                        End If

                    End If


                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                    If mblnPaymentApprovalwithLeaveApproval Then

                        Dim mstrClaimFormId As String = ""
                        Dim dsClaimList As DataSet = Nothing

                        strQ = " SELECT ISNULL(STUFF( " & _
                                  " (SELECT  DISTINCT ',' +  CAST(cmclaim_request_master.crmasterunkid AS NVARCHAR(max)) " & _
                                  "   FROM cmclaim_request_master " & _
                                  "   JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid  " & _
                                  "  WHERE cmclaim_request_master.statusunkid in (2) AND cmclaim_request_master.employeeunkid IN (" & CInt(drRow("employeeunkid")) & ")" & _
                                  " AND cmclaim_request_master.isvoid = 0  AND cmclaim_approval_tran.isvoid = 0 AND modulerefunkid = " & enModuleReference.Leave & _
                                  "  FOR XML PATH('')),1,1,''),'') AS CSV"

                        objDataOperation.ClearParameters()
                        dsClaimList = objDataOperation.ExecQuery(strQ, "FormList")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mstrClaimFormId = dsClaimList.Tables(0).Rows(0)("CSV").ToString()


                        If mstrClaimFormId.Trim.Length > 0 Then

                            strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newempapproverunkid,crapproverunkid = @newcrapproverunkid " & _
                                      " WHERE crmasterunkid in (" & mstrClaimFormId & ") AND crapproverunkid = @crapproverunkid AND cmclaim_approval_tran.isvoid = 0 "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@newempapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                            objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                        End If
                            strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran " & _
                                      " WHERE approveremployeeunkid = @approveremployeeunkid AND crapproverunkid = @newcrapproverunkid AND crmasterunkid  in (" & mstrClaimFormId & ") AND isvoid = 0"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveapproverunkid)
                            objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                            dsList = objDataOperation.ExecQuery(strQ, "List")

                            If dsList.Tables(0).Rows.Count > 0 Then
                                For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Next
                    End If

                        End If
                    End If

                    'Pinkal (01-Mar-2016) -- End


                    mstrEmployeeID &= drRow("employeeunkid").ToString() & ","

                Next

                If mstrEmployeeID.Trim.Length > 0 Then
                    mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
                End If

                Dim objMigration As New clsMigration
                objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationfromunkid = mintSwapToApproverID
                objMigration._Migrationtounkid = mintSwapFromApproverID
                objMigration._Usertypeid = enUserType.Approver
                objMigration._Userunkid = mintUserunkid
                objMigration._Migratedemployees = mstrEmployeeID

                objMigration._FormName = mstrFormName
                objMigration._LoginEmployeeunkid = mintLoginEmployeeunkid
                objMigration._ClientIP = mstrClientIP
                objMigration._HostName = mstrHostName
                objMigration._FromWeb = mblnIsWeb
                objMigration._AuditUserId = mintAuditUserId
objMigration._CompanyUnkid = mintCompanyUnkid
                objMigration._AuditDate = mdtAuditDate

                If objMigration.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            End If

            '==================================END FOR TO APPROVER=====================================


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SwapApprover; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (19-Mar-2015) -- End


    'Pinkal (17-Jun-2015) -- Start
    'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    ''' Shani(17-Aug-2015) -- Start
    '''Leave Enhancement : Putting Activate Feature in Leave Approver Master
    ''' Public Function InActiveApprover(ByVal intApproverunkid As Integer, ByVal intCompanyID As Integer) As Boolean
    Public Function InActiveApprover(ByVal intApproverunkid As Integer, ByVal intCompanyID As Integer, ByVal blnIsActive As Boolean) As Boolean
        'Shani(17-Aug-2015) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'strQ = " Update lvleaveapprover_master set isactive = 0 where approverunkid = @approverunkid AND isvoid = 0 "
            If blnIsActive Then
                strQ = " Update lvleaveapprover_master set isactive = 1 where approverunkid = @approverunkid AND isvoid = 0 AND isswap = 0 "
            Else
                strQ = " Update lvleaveapprover_master set isactive = 0 where approverunkid = @approverunkid AND isvoid = 0 AND isswap = 0 "
            End If
            'Shani(17-Aug-2015) -- End
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveapprover_master", "approverunkid", intApproverunkid, False, mintUserunkid, intCompanyID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function
    'Pinkal (17-Jun-2015) -- End

    'S.SANDEEP [30 JAN 2016] -- START
    Public Function GetExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List", Optional ByVal blnIncludeDelete As Boolean = False) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                   "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                   "FROM lvleaveapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   ") AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   ") AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE  lvleaveapprover_master.isswap = 0 " & _
                   " AND lvleaveapprover_master.isexternalapprover = 1 "


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            'AND lvleaveapprover_master.isactive = 1
            If blnIncludeDelete = False Then
                StrQ &= " AND lvleaveapprover_master.isvoid = 0 "
            End If
            'Pinkal (01-Mar-2016) -- End



            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalApproverList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsApprover
    End Function

    Public Function GetLeaveApproverUnkid(ByVal intLeaveApproverEmpUnkid As Integer, _
                                          ByVal blnExternalApprover As Boolean, _
                                          Optional ByVal intLevelId As Integer = -1, _
                                          Optional ByVal objDataOpr As clsDataOperation = Nothing) As String
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim strApproverUnkids As String = ""
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            'StrQ = "SELECT " & _
            '       "    @approverunkid = lvleaveapprover_master.approverunkid " & _
            '       "FROM lvleaveapprover_master " & _
            '       "WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isactive = 1 " & _
            '       "    AND lvleaveapprover_master.leaveapproverunkid = " & intLeaveApproverEmpUnkid & _
            '       "    AND lvleaveapprover_master.isexternalapprover = @ExternalApprover "

            StrQ = "SELECT " & _
                   "    @approverunkid = ISNULL(STUFF((SELECT ',' + CAST(approverunkid AS NVARCHAR(10)) " & _
                   "                                   FROM lvleaveapprover_master " & _
                   "                                   WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isactive = 1 AND lvleaveapprover_master.isswap = 0 " & _
                   "                                   AND lvleaveapprover_master.leaveapproverunkid = " & intLeaveApproverEmpUnkid & " AND lvleaveapprover_master.isexternalapprover = @ExternalApprover "

            If intLevelId >= 0 Then
                StrQ &= " AND lvleaveapprover_master.levelunkid = " & intLevelId
            End If

            StrQ &= " FOR XML PATH('')),1,1,''),'') "



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ExternalApprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strApproverUnkids, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strApproverUnkids = objDataOperation.GetParameterValue("@approverunkid")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetLeaveApproverUnkid ; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return strApproverUnkids
    End Function
    'S.SANDEEP [30 JAN 2016] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Leave Approver already exists. Please define new Leave Approver.")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class