﻿'************************************************************************************************************************************
'Class Name : clsbatch_master.vb
'Purpose    :
'Date       :15/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsbatch_master
    Private Shared ReadOnly mstrModuleName As String = "clsbatch_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBatchunkid As Integer
    Private mStrBatchno As String
    Private mintLeavetypeunkid As Integer
    Private mintAnalysisirefid As Integer
    Private mdtStartdate As DateTime
    Private mdtEnddate As DateTime
    Private mintAccruesetting As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Public mintLeaveBalanceId_Update As Integer = -1
    Private mdecCFlvAmount As Decimal = 0
    Private mintEligibilityAfter As Integer = 0
    Private mblnIsNoAction As Boolean = False


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintMaxNegativeDaysLimit As Integer = 0
    'Pinkal (08-Oct-2018) -- End


#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Batchunkid() As Integer
        Get
            Return mintBatchunkid
        End Get
        Set(ByVal value As Integer)
            mintBatchunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Batchno() As String
        Get
            Return mStrBatchno
        End Get
        Set(ByVal value As String)
            mStrBatchno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisrefid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Analysisirefid() As Integer
        Get
            Return mintAnalysisirefid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisirefid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Startdate() As DateTime
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As DateTime)
            mdtStartdate = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set stopdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Enddate() As DateTime
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As DateTime)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accruesetting
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Accruesetting() As Integer
        Get
            Return mintAccruesetting
        End Get
        Set(ByVal value As Integer)
            mintAccruesetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CFlvAmount
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _CFlvAmount() As Decimal
        Set(ByVal value As Decimal)
            mdecCFlvAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EligibilityAfter
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _EligibilityAfter() As Integer
        Set(ByVal value As Integer)
            mintEligibilityAfter = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsNoAction
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _IsNoAction() As Boolean
        Set(ByVal value As Boolean)
            mblnIsNoAction = value
        End Set
    End Property


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set MaxNegativeDaysLimit
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MaxNegativeDaysLimit() As Integer
        Get
            Return mintMaxNegativeDaysLimit
        End Get
        Set(ByVal value As Integer)
            mintMaxNegativeDaysLimit = value
        End Set
    End Property

    'Pinkal (08-Oct-2018) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  batchunkid " & _
              ", batchno " & _
              ", startdate " & _
              ", enddate " & _
              ", leavetypeunkid " & _
              ", analysisrefid " & _
             "FROM lvbatch_master " & _
             "WHERE batchunkid = @batchunkid "

            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBatchunkid = CInt(dtRow.Item("batchunkid"))
                mStrBatchno = dtRow.Item("batchno")
                mintLeavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mintAnalysisirefid = CInt(dtRow.Item("analysisrefid"))
                If Not IsDBNull(dtRow.Item("startdate")) Then mdtStartdate = CDate(dtRow.Item("startdate")) Else mdtStartdate = Nothing
                If Not IsDBNull(dtRow.Item("enddate")) Then mdtStartdate = CDate(dtRow.Item("enddate")) Else mdtEnddate = Nothing
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  batchunkid " & _
              ", batchno " & _
              ", startdate " & _
              ", enddate " & _
              ", leavetypeunkid " & _
              ", analysisrefid " & _
             "FROM lvbatch_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvbatch_master) </purpose>
    Public Function Insert(ByVal intYearunkid As Integer, ByVal mdtEmployee As DataTable, ByVal intAccruedays As Integer, ByVal mdtEmployeeAsOnDate As DateTime, Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean 'S.SANDEEP [04 JUN 2015] -- START-- END

        'Public Function Insert(ByVal intYearunkid As Integer, ByVal mdtEmployee As DataTable, ByVal intAccruedays As Integer, Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean
        If isExist(mStrBatchno) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch No is already defined. Please define new Batch No.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim _LeaveBalunkid As Integer = -1

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'START FOR INSERT BATCH
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mStrBatchno.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@analysisrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisirefid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO lvbatch_master ( " & _
              "  batchno " & _
              ", leavetypeunkid " & _
              ", analysisrefid" & _
              ", startdate " & _
              ", enddate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @batchno " & _
              ", @leavetypeunkid " & _
              ", @analysisrefid" & _
              ", @startdate " & _
              ", @enddate " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            mintBatchunkid = dsList.Tables(0).Rows(0).Item(0)

            'END FOR INSERT BATCH

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvbatch_master", "batchunkid", mintBatchunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END




            Dim objEmployee As New clsEmployee_Master
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = mintLeavetypeunkid

            Dim dr() As DataRow = mdtEmployee.Select("ischecked=True")

            For i As Integer = 0 To dr.Length - 1

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(dr(i)("employeeunkid"))
                objEmployee._Employeeunkid(mdtEmployeeAsOnDate) = CInt(dr(i)("employeeunkid"))
                'S.SANDEEP [04 JUN 2015] -- END


                If objLeaveType._Gender <> 0 AndAlso objLeaveType._Gender <> objEmployee._Gender Then
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "This leave type is invalid for some employee(s).")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                Dim blnflag As Boolean = False
                Dim blnCloseDueDate As Boolean = False

                If intLeaveBalanceSetting <= 0 Then
                    intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                End If

                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    blnflag = isAccrueLeaveExist(objDataOperation, CInt(dr(i)("employeeunkid")), mintLeavetypeunkid)
                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    blnflag = isAccrueLeaveExist(objDataOperation, CInt(dr(i)("employeeunkid")), mintLeavetypeunkid, -1, True, True)
                    If mintLeaveBalanceId_Update > 0 Then
                        blnCloseDueDate = CloseDueDateEmployeeELC(objDataOperation, mintLeaveBalanceId_Update, intYearunkid, CInt(dr(i)("employeeunkid")), mintLeavetypeunkid)
                    End If
                End If

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr(i)("yearunkid").ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr(i)("startdate").ToString).Date)
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dr(i)("stopdate").ToString).Date)
                objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchunkid.ToString)
                objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("accrue_amount").ToString)
                objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("issue_amount").ToString)
                objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("balance"))
                objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("remaining_bal"))
                objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("uptolstyr_accrueamt"))
                objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("uptolstyr_issueamt"))
                objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("daily_amount"))
                objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dr(i)("ispaid").ToString)
                objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccruesetting)
                objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, dr(i)("days").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("actualamount").ToString)
                objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dr(i)("isshortleave").ToString)
                objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dr(i)("isopenelc").ToString)
                objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dr(i)("iselc").ToString)
                objDataOperation.AddParameter("@leavebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("LeaveBf").ToString)
                objDataOperation.AddParameter("@isclosefy", SqlDbType.Float, eZeeDataType.BIT_SIZE, CBool(dr(i)("isclosefy")))
                objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCFlvAmount.ToString)
                objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
                objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNoAction.ToString)
                objDataOperation.AddParameter("@adj_remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("adj_remaining_bal").ToString)
                objDataOperation.AddParameter("@monthly_accrue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dr(i)("monthly_accrue").ToString)


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                objDataOperation.AddParameter("@maxnegative_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNegativeDaysLimit.ToString)
                'Pinkal (08-Oct-2018) -- End


                If blnCloseDueDate Then
                    GoTo ForCloseDueDateELC
                End If

                If blnflag Then


                    strQ = "UPDATE lvleavebalance_tran SET " & _
                         "  yearunkid = @yearunkid" & _
                         ", batchunkid = @batchunkid " & _
                         ", startdate = @startdate" & _
                         ", enddate = @enddate" & _
                         ", actualamount = @actualamount " & _
                         ", accrue_amount = @accrue_amount" & _
                         ", issue_amount = @issue_amount" & _
                         ", balance = @balance" & _
                         ", remaining_bal = @remaining_bal" & _
                         ", uptolstyr_accrueamt = @uptolstyr_accrueamt " & _
                         ", uptolstyr_issueamt = @uptolstyr_issueamt " & _
                         ", daily_amount = @daily_amount " & _
                         ", ispaid = @ispaid" & _
                         ", isshortleave = @isshortleave " & _
                         ", accruesetting = @accruesetting " & _
                         ", days = @days " & _
                         ", userunkid = @userunkid" & _
                         ", isvoid = @isvoid" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime" & _
                         ", voidreason = @voidreason " & _
                                ", isopenelc = @isopenelc " & _
                                ", iselc = @iselc " & _
                                ",leavebf  = @leavebf " & _
                         ",isclose_fy  = @isclosefy " & _
                         ", cfamount = @cfamount " & _
                         ", eligibilityafter = @eligibilityafter " & _
                         ", isnoaction = @isnoaction " & _
                         ", adj_remaining_bal = @adj_remaining_bal " & _
                            ", monthly_accrue = @monthly_accrue " & _
                            ", maxnegative_limit = @maxnegative_limit " & _
                         " WHERE  employeeunkid = @employeeunkid" & _
                            " AND leavetypeunkid = @leavetypeunkid and isvoid = 0 "

                    'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", maxnegative_limit = @maxnegative_limit " & _]


                    If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= " AND iselc = 1 AND isopenelc = 1 "
                    End If


                    objDataOperation.ExecQuery(strQ, "List")

                    If mintLeaveBalanceId_Update > 0 Then
                        Dim objBal As New clsleavebalance_tran
                        objBal._LeaveBalanceunkid = mintLeaveBalanceId_Update
                        With objBal
                            ._FormName = mstrFormName
                            ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        If objBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                Else


ForCloseDueDateELC:

                    strQ = "INSERT INTO lvleavebalance_tran ( " & _
                      " yearunkid " & _
                      ", employeeunkid " & _
                      ", leavetypeunkid " & _
                      ", batchunkid " & _
                                 ", startdate " & _
                                 ", enddate " & _
                              ", actualamount " & _
                      ", accrue_amount " & _
                      ", issue_amount " & _
                      ", balance " & _
                      ", remaining_bal " & _
                                 ", uptolstyr_accrueamt " & _
                                 ", uptolstyr_issueamt " & _
                                 ", daily_amount " & _
                      ", ispaid " & _
                              ", isshortleave " & _
                          ", accruesetting " & _
                      ", days " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                            ", isopenelc " & _
                            ", iselc " & _
                            ", leavebf " & _
                              ", isclose_fy " & _
                                  ", cfamount " & _
                                  ", eligibilityafter " & _
                                  ", isnoaction " & _
                                  ", adj_remaining_bal " & _
                              ", monthly_accrue " & _
                              ", maxnegative_limit " & _
                    ") VALUES (" & _
                      " @yearunkid " & _
                      ", @employeeunkid " & _
                      ", @leavetypeunkid " & _
                      ", @batchunkid " & _
                                 ", @startdate " & _
                                 ", @enddate " & _
                              ", @actualamount " & _
                      ", @accrue_amount " & _
                      ", @issue_amount " & _
                      ", @balance " & _
                      ", @remaining_bal " & _
                                 ", @uptolstyr_accrueamt " & _
                                 ", @uptolstyr_issueamt " & _
                                 ", @daily_amount " & _
                      ", @ispaid " & _
                              ", @isshortleave " & _
                          ", @accruesetting " & _
                      ", @days " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                           ", @isopenelc " & _
                           ", @iselc " & _
                           ", @leavebf " & _
                              ", @isclosefy" & _
                                  ", @cfamount " & _
                                  ", @eligibilityafter " & _
                                  ", @isnoaction " & _
                             ", @adj_remaining_bal " & _
                              ", @monthly_accrue " & _
                              ", @maxnegative_limit " & _
                           "); SELECT @@identity"

                    'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", @maxnegative_limit " & _]


                    dsList = Nothing
                    dsList = objDataOperation.ExecQuery(strQ, "List")
                    _LeaveBalunkid = dsList.Tables(0).Rows(0).Item(0)

                    Dim objBal As New clsleavebalance_tran
                    objBal._LeaveBalanceunkid = _LeaveBalunkid
                    objBal._CFlvAmount = mdecCFlvAmount
                    objBal._EligibilityAfter = mintEligibilityAfter
                    objBal._IsNoAction = mblnIsNoAction
                    With objBal
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objBal.InsertAudiTrailForLeaveBalance(objDataOperation, 1) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next
            'END FOR INSERT ACCRUE MASTER

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvbatch_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            strQ = " Select isnull(leavebalanceunkid,0) as leavebalanceunkid,isnull(Issue_Amount,0) as Issue_Amount " & _
                    " FROM lvleavebalance_tran " & _
                    " where batchunkid = @batchunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "BatchList")

            If dsList.Tables("BatchList").Rows.Count > 0 Then
                For Each dr As DataRow In dsList.Tables("BatchList").Rows

                    If CInt(dr("Issue_Amount")) > 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, This Batch Leave is already Issued to Some Employee(s). For that Employee(s) You cannot delete this Batch Leave.")
                    Else
                        strQ = " UPDATE  lvleavebalance_tran set isvoid = 1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                               " WHERE leavebalanceunkid = @leavebalanceunkid "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("leavebalanceunkid")))
                        objDataOperation.ExecNonQuery(strQ)
                    End If
                Next
            End If

            ' START FOR VOID IN BATCH MASTER

            If mstrMessage = "" Then
                strQ = "UPDATE  lvbatch_master set isvoid = 1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                 "WHERE batchunkid = @batchunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.ExecNonQuery(strQ)
            End If
            ' END FOR VOID IN BATCH MASTER

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lvbatch_master", "batchunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            If mstrMessage = "" Then Return True Else Return False
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strBatchno As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  batchunkid " & _
              ", batchno " & _
              ", leavetypeunkid " & _
              ", analysisrefid " & _
              ", startdate " & _
              ", enddate " & _
             "FROM lvbatch_master " & _
             "WHERE batchno = @batchno "

            If intUnkid > 0 Then
                strQ &= " AND batchunkid <> @batchunkid"
            End If

            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBatchno)
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isAccrueLeaveExist(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, ByVal intLeavetypeunkid As Integer, Optional ByVal intUnkid As Integer = -1, _
                                                      Optional ByVal blnIsOpenELC As Boolean = False, Optional ByVal blnIsELC As Boolean = False _
                                                      , Optional ByVal IsCloseFy As Boolean = False) As Boolean   'Pinkal (12-May-2013) [IsCloseFy]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = "SELECT " & _
               "  leavebalanceunkid " & _
               ", yearunkid " & _
               ", employeeunkid " & _
               ", leavetypeunkid " & _
               ", batchunkid " & _
              ", actualamount " & _
               ", accrue_amount " & _
               ", issue_amount " & _
               ", daily_amount " & _
               ", balance " & _
               ", remaining_bal " & _
               ", uptolstyr_accrueamt " & _
               ", uptolstyr_issueamt " & _
               ", ispaid " & _
              ", isshortleave " & _
               ", userunkid " & _
               ", isvoid " & _
               ", voiduserunkid " & _
               ", voiddatetime " & _
               ", voidreason " & _
                        ", isopenelc " & _
                        ", iselc " & _
               ", ISNULL(isclose_fy,0) AS isclose_fy " & _
              "FROM lvleavebalance_tran " & _
              "WHERE employeeunkid = @empunkid AND leavetypeunkid = @lvtypeunkid and lvleavebalance_tran.isvoid = 0"


            If blnIsELC Then

                If blnIsOpenELC Then
                    strQ &= " AND lvleavebalance_tran.isopenelc = 1 "
                Else
                    strQ &= " AND lvleavebalance_tran.isopenelc = 0 "
                End If

            End If

            If IsCloseFy Then
                strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 1 "
            Else
                strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 0 "
            End If

            ' objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lvtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeavetypeunkid)
            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@lvbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then mintLeaveBalanceId_Update = CInt(dsList.Tables(0).Rows(0)("leavebalanceunkid"))
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isAccrueLeaveExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as batchunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT batchunkid, batchno as name FROM lvbatch_master where isnull(isvoid,0) = 0 ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function CloseDueDateEmployeeELC(ByVal objDataOperation As clsDataOperation, ByVal intBalanceunkid As Integer, ByVal intYearunkid As Integer, ByVal intEmployeeId As Integer, ByVal intLeaveTypeId As Integer) As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            strQ = " UPDATE lvleavebalance_tran Set " & _
                      " isopenelc = 0 " & _
                      " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isopenelc = 1 AND iselc = 1 AND isvoid = 0 AND yearunkid = @yearunkid"


            'Pinkal (09-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
            strQ &= " AND leavebalanceunkid  = " & intBalanceunkid
            'Pinkal (09-Jul-2014) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objBal As New clsleavebalance_tran
            objBal._LeaveBalanceunkid = intBalanceunkid
            With objBal
                ._FormName = mstrFormName
                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "CloseDueDateEmployeeELC", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
        End Try
    End Function



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Batch No is already defined. Please define new Batch No.")
			Language.setMessage(mstrModuleName, 2, "This leave type is invalid for some employee(s).")
			Language.setMessage(mstrModuleName, 3, "Sorry, This Batch Leave is already Issued to Some Employee(s). For that Employee(s) You cannot delete this Batch Leave.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class