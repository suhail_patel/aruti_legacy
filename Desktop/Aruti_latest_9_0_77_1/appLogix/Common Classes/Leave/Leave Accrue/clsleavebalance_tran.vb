﻿'************************************************************************************************************************************
'Class Name : clsleavebalance_tran.vb
'Purpose    :
'Date       :15/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 4
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
Imports System

'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleavebalance_tran
    Private Shared ReadOnly mstrModuleName As String = "clsleavebalance_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintBalanceunkid As Integer
    Private mintYearunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintLeavetypeunkid As Integer
    Private mintBatchunkid As Integer = -1
    Private mdtStartdate As DateTime
    Private mdtEnddate As DateTime
    Private mdblAccrue_Amount As Double
    Private mdblUptoLstyrAccrue_Amount As Double
    Private mdblDaily_amount As Double
    Private mdblBalance As Double
    Private mdblRemaining_Balance As Double
    Private mblnIspaid As Boolean
    Private mintAccrueSetting As Integer = -1
    Private mintDays As Integer = 0
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdblLeaveBF As Double = 0
    Private mdblIssue_Amount As Double
    Private mdblUptoLstyrIssue_Amount As Double
    Private mblnIsShortLeave As Boolean = False
    Private mdblActualAmount As Double = 0
    Private mobjDataOperation As clsDataOperation = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mblnIsOpenELC As Boolean = False
    Private mblnIsELC As Boolean = False
    Private mblnIsCloseFy As Boolean = False
    Private mdecCFlvAmount As Decimal = 0
    Private mintEligibilityAfter As Integer = 0
    Private mblnIsNoAction As Boolean = False
    Private mintConsicutivedays As Integer = 0
    Private mintOccurance As Integer = 0
    Private mintOptiondid As Integer = 0
    Private mintRemaining_Occurrence As Integer = 0
    Private mintLeaveBalanceSetting As Integer = -1
    Private mdecAdjustmentAmt As Decimal
    Private mdtdbStartDate As DateTime = Nothing
    Private mdtdbEndDate As DateTime = Nothing
    Private mdecMonthly_AccrueAmt As Decimal = 0

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    'Private mstrWebClientIP As String = ""
    'Private mstrWebHostName As String = ""
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintMaxNegativeDaysLimit As Integer = 0
    'Pinkal (08-Oct-2018) -- End


    'Pinkal (13-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintOccurrenceTenure As Integer = 0
    'Pinkal (13-Oct-2018) -- End


#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveaccrueunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LeaveBalanceunkid() As Integer
        Get
            Return mintBalanceunkid
        End Get
        Set(ByVal value As Integer)
            mintBalanceunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Batchunkid() As Integer
        Get
            Return mintBatchunkid
        End Get
        Set(ByVal value As Integer)
            mintBatchunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Startdate() As DateTime
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As DateTime)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Enddate() As DateTime
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As DateTime)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Total_Accrue
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AccrueAmount() As Double
        Get
            Return mdblAccrue_Amount
        End Get
        Set(ByVal value As Double)
            mdblAccrue_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Total_Issue
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IssueAmount() As Double
        Get
            Return mdblIssue_Amount
        End Get
        Set(ByVal value As Double)
            mdblIssue_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Lastyear_accrueamount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _UptoLstYr_AccrueAmout() As Double
        Get
            Return mdblUptoLstyrAccrue_Amount
        End Get
        Set(ByVal value As Double)
            mdblUptoLstyrAccrue_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Lastyear_issueamount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _UptoLstYr_IssueAmout() As Double
        Get
            Return mdblUptoLstyrIssue_Amount
        End Get
        Set(ByVal value As Double)
            mdblUptoLstyrIssue_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Total_Balance
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Balance() As Double
        Get
            Return mdblBalance
        End Get
        Set(ByVal value As Double)
            mdblBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Dummy_Balance
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remaining_Balance() As Double
        Get
            Return mdblRemaining_Balance
        End Get
        Set(ByVal value As Double)
            mdblRemaining_Balance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Daily_Amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Daily_Amount() As Double
        Get
            Return mdblDaily_amount
        End Get
        Set(ByVal value As Double)
            mdblDaily_amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Leave B.F
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LeaveBF() As Double
        Get
            Return mdblLeaveBF
        End Get
        Set(ByVal value As Double)
            mdblLeaveBF = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispaid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ispaid() As Boolean
        Get
            Return mblnIspaid
        End Get
        Set(ByVal value As Boolean)
            mblnIspaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AccrueSetting
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AccrueSetting() As Integer
        Get
            Return mintAccrueSetting
        End Get
        Set(ByVal value As Integer)
            mintAccrueSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Leavedays
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Days() As Integer
        Get
            Return mintDays
        End Get
        Set(ByVal value As Integer)
            mintDays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isshortLeave
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsshortLeave() As Boolean
        Get
            Return mblnIsShortLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsShortLeave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualamount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ActualAmount() As Double
        Get
            Return mdblActualAmount
        End Get
        Set(ByVal value As Double)
            mdblActualAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mObjDataoperation
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _mObjDataoperation() As clsDataOperation
        Get
            Return mobjDataOperation
        End Get
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isopenelc
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsOpenELC() As Boolean
        Get
            Return mblnIsOpenELC
        End Get
        Set(ByVal value As Boolean)
            mblnIsOpenELC = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iselc
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsELC() As Boolean
        Get
            Return mblnIsELC
        End Get
        Set(ByVal value As Boolean)
            mblnIsELC = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsCloseFy
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsCloseFy() As Boolean
        Get
            Return mblnIsCloseFy
        End Get
        Set(ByVal value As Boolean)
            mblnIsCloseFy = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cfamount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _CFlvAmount() As Decimal
        Get
            Return mdecCFlvAmount
        End Get
        Set(ByVal value As Decimal)
            mdecCFlvAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set eligibilityafter
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _EligibilityAfter() As Integer
        Get
            Return mintEligibilityAfter
        End Get
        Set(ByVal value As Integer)
            mintEligibilityAfter = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isnoaction
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsNoAction() As Boolean
        Get
            Return mblnIsNoAction
        End Get
        Set(ByVal value As Boolean)
            mblnIsNoAction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set consecutivedays
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Consicutivedays() As Integer
        Get
            Return mintConsicutivedays
        End Get
        Set(ByVal value As Integer)
            mintConsicutivedays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set occurrence
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Occurance() As Integer
        Get
            Return mintOccurance
        End Get
        Set(ByVal value As Integer)
            mintOccurance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set optiondid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Optiondid() As Integer
        Get
            Return mintOptiondid
        End Get
        Set(ByVal value As Integer)
            mintOptiondid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remaining_occurrence
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remaining_Occurrence() As Integer
        Get
            Return mintRemaining_Occurrence
        End Get
        Set(ByVal value As Integer)
            mintRemaining_Occurrence = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveBalanceSetting
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _LeaveBalanceSetting() As Integer
        Get
            Return mintLeaveBalanceSetting
        End Get
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AdjustmentAmt
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _AdjustmentAmt() As Decimal
        Get
            Return mdecAdjustmentAmt
        End Get
        Set(ByVal value As Decimal)
            mdecAdjustmentAmt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DBStartdate
    ''' Modify By: Pinkal 
    ''' </summary>
    ''' 
    Public Property _DBStartdate() As DateTime
        Get
            Return mdtdbStartDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DBEnddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DBEnddate() As DateTime
        Get
            Return mdtdbEndDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbEndDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Monthly_Accrue
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Monthly_Accrue() As Double
        Get
            Return mdecMonthly_AccrueAmt
        End Get
        Set(ByVal value As Double)
            mdecMonthly_AccrueAmt = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set MaxNegativeDaysLimit
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MaxNegativeDaysLimit() As Integer
        Get
            Return mintMaxNegativeDaysLimit
        End Get
        Set(ByVal value As Integer)
            mintMaxNegativeDaysLimit = value
        End Set
    End Property

    'Pinkal (08-Oct-2018) -- End


    'Pinkal (13-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set OccurrenceTenure
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _OccurrenceTenure() As Integer
        Get
            Return mintOccurrenceTenure
        End Get
        Set(ByVal value As Integer)
            mintOccurrenceTenure = value
        End Set
    End Property

    'Pinkal (13-Oct-2018) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If

        'Pinkal (01-Oct-2018) -- Start
        'Enhancement - Leave Enhancement for NMB.
        objDataOperation.ClearParameters()
        'Pinkal (01-Oct-2018) -- End


        Try

            strQ = "SELECT " & _
              "  leavebalanceunkid " & _
              ", yearunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", batchunkid " & _
              ", startdate " & _
              ", enddate " & _
                       ", ISNULL(actualamount,0) actualamount " & _
              ", accrue_amount " & _
              ", issue_amount " & _
              ", uptolstyr_accrueamt " & _
              ", uptolstyr_issueamt " & _
              ", daily_amount " & _
              ", balance " & _
              ", remaining_bal " & _
              ", leavebf " & _
              ", ispaid " & _
              ", accruesetting " & _
              ", isnull(days,0) as days  " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                       ", ISNULL(isshortleave,0) as isshortleave " & _
                       ", ISNULL(isopenelc,0) as isopenelc " & _
                       ", ISNULL(iselc,0) as iselc " & _
              ", ISNULL(isclose_fy,0) as isclosefy " & _
              ", ISNULL(cfamount,0) AS cfamount " & _
              ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
              ", ISNULL(isnoaction,0) AS isnoaction " & _
              ", ISNULL(consecutivedays,0) AS consecutivedays " & _
              ", ISNULL(occurrence,0) AS occurrence " & _
              ", ISNULL(optiondid,0) AS optiondid " & _
              ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
              ", ISNULL(adj_remaining_bal,0) AS adj_remaining_bal " & _
                      ", ISNULL(monthly_accrue,0) AS monthly_accrue " & _
                      ", ISNULL(maxnegative_limit,0) AS maxnegative_limit " & _
                      ", ISNULL(occ_tenure,0) AS occ_tenure " & _
                     " FROM lvleavebalance_tran " & _
                     " WHERE leavebalanceunkid = @leavebalanceunkid "


            'Pinkal (13-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(occ_tenure,0) AS occ_tenure " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(maxnegative_limit,0) AS maxnegative_limit " & _]

            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBalanceunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBalanceunkid = CInt(dtRow.Item("leavebalanceunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintLeavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mintBatchunkid = CInt(dtRow.Item("batchunkid"))
                If dtRow.Item("startdate") IsNot DBNull.Value Then mdtStartdate = CDate(dtRow.Item("startdate"))
                If dtRow.Item("enddate") IsNot DBNull.Value Then mdtEnddate = CDate(dtRow.Item("enddate"))
                mdblAccrue_Amount = CDec(dtRow.Item("accrue_amount"))
                mdblIssue_Amount = CDec(dtRow.Item("issue_amount"))
                mdblUptoLstyrAccrue_Amount = CDec(dtRow.Item("uptolstyr_accrueamt"))
                mdblUptoLstyrIssue_Amount = CDec(dtRow.Item("uptolstyr_issueamt"))
                mdblDaily_amount = CDec(dtRow.Item("daily_amount"))
                mdblBalance = CDec(dtRow.Item("balance"))
                mdblRemaining_Balance = CDec(dtRow.Item("remaining_bal"))

                If dtRow.Item("leavebf").ToString <> "" Then
                    mdblLeaveBF = CDec(dtRow.Item("leavebf"))
                End If

                mblnIspaid = CBool(dtRow.Item("ispaid"))
                mintAccrueSetting = CInt(dtRow.Item("accruesetting"))
                mintDays = CInt(dtRow.Item("days"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime") IsNot DBNull.Value Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsShortLeave = CBool(dtRow.Item("isshortleave"))
                mdblActualAmount = CDbl(dtRow.Item("actualamount"))
                mblnIsOpenELC = CBool(dtRow.Item("isopenelc"))
                mblnIsELC = CBool(dtRow.Item("iselc"))
                If Not IsDBNull(dtRow.Item("isclosefy")) Then
                    mblnIsCloseFy = CBool(dtRow.Item("isclosefy"))
                End If
                mdecCFlvAmount = dtRow.Item("cfamount")
                mintEligibilityAfter = dtRow.Item("eligibilityafter")
                mblnIsNoAction = dtRow.Item("isnoaction")
                mintConsicutivedays = CInt(dtRow.Item("consecutivedays"))
                mintOccurance = CInt(dtRow.Item("occurrence"))
                mintOptiondid = CInt(dtRow.Item("optiondid"))
                mintRemaining_Occurrence = CInt(dtRow.Item("remaining_occurrence"))

                If Not IsDBNull(dtRow.Item("adj_remaining_bal")) Then
                    mdecAdjustmentAmt = CDec(dtRow.Item("adj_remaining_bal"))
                End If
                If Not IsDBNull(dtRow.Item("monthly_accrue")) Then
                    mdecMonthly_AccrueAmt = CDec(dtRow.Item("monthly_accrue"))
                End If

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If Not IsDBNull(dtRow.Item("maxnegative_limit")) Then
                    mintMaxNegativeDaysLimit = CInt(dtRow.Item("maxnegative_limit"))
                End If
                'Pinkal (08-Oct-2018) -- End


                'Pinkal (13-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If Not IsDBNull(dtRow.Item("occ_tenure")) Then
                    mintOccurrenceTenure = CInt(dtRow.Item("occ_tenure"))
                End If
                'Pinkal (13-Oct-2018) -- End


                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal IsForImport As Boolean = False, _
    '                                Optional ByVal intEmployeeID As Integer = -1, Optional ByVal IsopenELC As Boolean = False, Optional ByVal IsELC As Boolean = False, _
    '                                Optional ByVal IsCloseFy As Boolean = False, Optional ByVal mstrUserAccessFilter As String = "", _
    '                                Optional ByVal mdtFromdate As DateTime = Nothing, Optional ByVal mdtToDate As DateTime = Nothing) As DataSet  'Pinkal (12-May-2013) [IsCloseFy]
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try

    '        'strQ = "SELECT " & _
    '        '   "  lvleavebalance_tran.leavebalanceunkid " & _
    '        '   ", lvleavebalance_tran.yearunkid " & _
    '        '   ", lvleavetype_master.leavename " & _
    '        '   ", lvleavebalance_tran.employeeunkid " & _
    '        '   ", isnull(hremployee_master.firstname,'') + isnull(hremployee_master.surname,'') as Employeename " & _
    '        '   ", lvleavebalance_tran.leavetypeunkid  " & _
    '        '   ", lvleavetype_master.leavename " & _
    '        '   ", lvleavebalance_tran.batchunkid " & _
    '        '   ", lvleavebalance_tran.startdate " & _
    '        '   ", lvleavebalance_tran.enddate " & _
    '        '   ", lvleavebalance_tran.ispaid " & _
    '        '   ", ISNULL(actualamount,0) actualamount " & _
    '        '   ", accrue_amount " & _
    '        '   ", issue_amount" & _
    '        '   ", uptolstyr_accrueamt " & _
    '        '   ", uptolstyr_issueamt " & _
    '        '   ", daily_amount " & _
    '        '   ", balance " & _
    '        '   ", remaining_bal " & _
    '        '   ", leavebf " & _
    '        '   ", accruesetting " & _
    '        '   ", isnull(days,0) as days " & _
    '        '   ", lvleavebalance_tran.userunkid " & _
    '        '   ", lvleavebalance_tran.isvoid " & _
    '        '   ", lvleavebalance_tran.voiduserunkid " & _
    '        '   ", lvleavebalance_tran.voiddatetime " & _
    '        '", lvleavebalance_tran.voidreason "

    'strQ = "SELECT " & _
    '   "  lvleavebalance_tran.leavebalanceunkid " & _
    '   ", lvleavebalance_tran.yearunkid " & _
    '   ", lvleavetype_master.leavename " & _
    '   ", lvleavebalance_tran.employeeunkid " & _
    '           ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as Employeename " & _
    '   ", lvleavebalance_tran.leavetypeunkid  " & _
    '   ", lvleavetype_master.leavename " & _
    '   ", lvleavebalance_tran.batchunkid " & _
    '   ", lvleavebalance_tran.startdate " & _
    '   ", lvleavebalance_tran.enddate " & _
    '   ", lvleavebalance_tran.ispaid " & _
    '   ", ISNULL(actualamount,0) actualamount " & _
    '   ", accrue_amount " & _
    '   ", issue_amount" & _
    '   ", uptolstyr_accrueamt " & _
    '   ", uptolstyr_issueamt " & _
    '   ", daily_amount " & _
    '   ", balance " & _
    '   ", remaining_bal " & _
    '   ", leavebf " & _
    '   ", accruesetting " & _
    '   ", isnull(days,0) as days " & _
    '   ", lvleavebalance_tran.userunkid " & _
    '   ", lvleavebalance_tran.isvoid " & _
    '   ", lvleavebalance_tran.voiduserunkid " & _
    '   ", lvleavebalance_tran.voiddatetime " & _
    '                    ", lvleavebalance_tran.voidreason " & _
    '                    ", ISNULL(lvleavebalance_tran.isshortleave,0) as isshortleave " & _
    '                    ", ISNULL(isopenelc,0) as isopenelc " & _
    '                    ", ISNULL(iselc,0) as iselc "

    '        If IsForImport Then
    '            strQ &= " , hrmsConfiguration..cffinancial_year_tran.financialyear_name as year " & _
    '                    " , lvbatch_master.batchno as batch "
    '        End If

    '        'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        strQ &= ", ISNULL(lvleavebalance_tran.cfamount,0) AS cfamount " & _
    '                ", ISNULL(lvleavebalance_tran.eligibilityafter,0) AS eligibilityafter " & _
    '                ", ISNULL(lvleavebalance_tran.isnoaction,0) AS isnoaction " & _
    '                ", ISNULL(lvleavebalance_tran.consecutivedays,0) AS consecutivedays " & _
    '                ", ISNULL(lvleavebalance_tran.occurrence,0) AS occurrence " & _
    '                ", ISNULL(lvleavebalance_tran.optiondid,0) AS optiondid " & _
    '                ", ISNULL(lvleavebalance_tran.remaining_occurrence,0) AS remaining_occurrence "
    '        'S.SANDEEP [ 26 SEPT 2013 ] -- END


    '        'Pinkal (28-Feb-2015) -- Start
    '        'Enhancement - CREATING LEAVE ADJUSTMENT SCREEN AS PER MR.RUTTA'S REQUIREMENT
    '        strQ &= ", ISNULL(adj_remaining_bal,0) AS adj_remaining_bal "
    '        'Pinkal (28-Feb-2015) -- End


    '        strQ &= " FROM lvleavebalance_tran " & _
    '                       " LEFT JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid " & _
    '                       " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvleavebalance_tran.employeeunkid "


    '        If IsForImport Then
    '            strQ &= " LEFT JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = lvleavebalance_tran.yearunkid " & _
    '                        " LEFT JOIN lvbatch_master on lvbatch_master.batchunkid = lvleavebalance_tran.batchunkid "
    '        End If


    '        strQ &= " WHERE 1 = 1 "
    '        If blnOnlyActive Then
    '            strQ &= " AND lvleavebalance_tran.isvoid = 0 "
    '        End If


    '        'Pinkal (06-Feb-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If IsELC Then
    '            If IsopenELC Then
    '                strQ &= " AND lvleavebalance_tran.isopenelc = 1"
    '            Else
    '                strQ &= " AND lvleavebalance_tran.isopenelc = 0"
    '            End If
    '        End If

    '        If intEmployeeID > 0 Then
    '            strQ &= " AND lvleavebalance_tran.employeeunkid = " & intEmployeeID
    '        End If

    '        'Pinkal (06-Feb-2013) -- End

    '        'Pinkal (12-May-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If IsCloseFy Then
    '            strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 1 "
    '        Else
    '            strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 0 "
    '        End If

    '        'Pinkal (12-May-2013) -- End




    '        'Pinkal (24-May-2013) -- Start
    '        'Enhancement : TRA Changes


    '        'Pinkal (09-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '         'Issue: when manager logins and access is not set to that manager for particular allocation and if employee comes of that allocation then balance of that employee was not getting deducted.  

    '        'If mstrUserAccessFilter = "" Then
    '        '    If UserAccessLevel._AccessLevel.Length > 0 Then
    '        'strQ &= UserAccessLevel._AccessLevelFilterString
    '        '    End If
    '        'Else
    '        '    strQ &= mstrUserAccessFilter
    '        'End If

    '        'Pinkal (09-Jul-2014) -- End

    '        'strQ &= UserAccessLevel._AccessLevelFilterString

    '        'Pinkal (24-May-2013) -- End



    '        'Pinkal (02-Jan-2015) -- Start
    '        'Enhancement - CHANGES FOR GET ONLY ACTIVE EMPLYEES BALANCES.

    '        If mdtFromdate <> Nothing AndAlso mdtToDate <> Nothing Then
    '            strQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                         " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                         " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                         " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromdate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate))
    '        End If

    '        'Pinkal (02-Jan-2015) -- End



    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        ' objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                      , Optional ByVal IsForImport As Boolean = False, Optional ByVal intEmployeeID As Integer = -1, Optional ByVal IsopenELC As Boolean = False, Optional ByVal IsELC As Boolean = False _
                                      , Optional ByVal IsCloseFy As Boolean = False, Optional ByVal mstrFilter As String = "", Optional ByVal objDOperation As clsDataOperation = Nothing _
                                      , Optional ByVal blnIsExternalApprover As Boolean = False) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            If blnIsExternalApprover = False Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            strQ = "SELECT " & _
               "  lvleavebalance_tran.leavebalanceunkid " & _
               ", lvleavebalance_tran.yearunkid " & _
               ", lvleavetype_master.leavename " & _
               ", lvleavebalance_tran.employeeunkid " & _
               ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as Employeename " & _
               ", lvleavebalance_tran.leavetypeunkid  " & _
               ", lvleavetype_master.leavename " & _
            ", lvleavebalance_tran.batchunkid " & _
            ", lvleavebalance_tran.startdate " & _
            ", lvleavebalance_tran.enddate " & _
               ", lvleavebalance_tran.ispaid " & _
               ", ISNULL(actualamount,0) actualamount " & _
               ", accrue_amount " & _
               ", issue_amount" & _
               ", uptolstyr_accrueamt " & _
               ", uptolstyr_issueamt " & _
               ", daily_amount " & _
               ", balance " & _
               ", remaining_bal " & _
               ", leavebf " & _
               ", accruesetting " & _
               ", isnull(days,0) as days " & _
               ", lvleavebalance_tran.userunkid " & _
               ", lvleavebalance_tran.isvoid " & _
               ", lvleavebalance_tran.voiduserunkid " & _
               ", lvleavebalance_tran.voiddatetime " & _
                        ", lvleavebalance_tran.voidreason " & _
                        ", ISNULL(lvleavebalance_tran.isshortleave,0) as isshortleave " & _
                        ", ISNULL(isopenelc,0) as isopenelc " & _
                        ", ISNULL(iselc,0) as iselc "

            If IsForImport Then
                strQ &= " , hrmsConfiguration..cffinancial_year_tran.financialyear_name as year " & _
                        " , lvbatch_master.batchno as batch "
            End If

            strQ &= ", ISNULL(lvleavebalance_tran.cfamount,0) AS cfamount " & _
                    ", ISNULL(lvleavebalance_tran.eligibilityafter,0) AS eligibilityafter " & _
                    ", ISNULL(lvleavebalance_tran.isnoaction,0) AS isnoaction " & _
                    ", ISNULL(lvleavebalance_tran.consecutivedays,0) AS consecutivedays " & _
                    ", ISNULL(lvleavebalance_tran.occurrence,0) AS occurrence " & _
                    ", ISNULL(lvleavebalance_tran.optiondid,0) AS optiondid " & _
                        ", ISNULL(lvleavebalance_tran.remaining_occurrence,0) AS remaining_occurrence " & _
                        ", ISNULL(adj_remaining_bal,0) AS adj_remaining_bal " & _
                        ", ISNULL(monthly_accrue,0.00) AS monthly_accrue " & _
                        ", ISNULL(lvleavebalance_tran.maxnegative_limit,0) AS maxnegative_limit " & _
                        ", ISNULL(lvleavebalance_tran.occ_tenure,0) AS occ_tenure " & _
                        " FROM lvleavebalance_tran " & _
                           " LEFT JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid " & _
                           " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvleavebalance_tran.employeeunkid "


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If
            'Pinkal (11-Sep-2020) -- End


            If IsForImport Then
                strQ &= " LEFT JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = lvleavebalance_tran.yearunkid " & _
                            " LEFT JOIN lvbatch_master on lvbatch_master.batchunkid = lvleavebalance_tran.batchunkid "
            End If


            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If



            strQ &= " WHERE 1 = 1 "
            If blnOnlyActive Then
                strQ &= " AND lvleavebalance_tran.isvoid = 0 "
            End If

            If IsELC Then
                If IsopenELC Then
                    strQ &= " AND lvleavebalance_tran.isopenelc = 1"
                Else
                    strQ &= " AND lvleavebalance_tran.isopenelc = 0"
                End If
            End If

            If intEmployeeID > 0 Then
                strQ &= " AND lvleavebalance_tran.employeeunkid = " & intEmployeeID
            End If

            If IsCloseFy Then
                'Pinkal (28-May-2018) -- Start
                'Enhancement - Ref # 0002281 EMP code 2132  Variance Between  Leave issue  & Balance as on 31-12-17.
                strQ &= " AND (ISNULL(lvleavebalance_tran.isclose_fy,0) = 1 OR ISNULL(lvleavebalance_tran.isshortleave,0) = 1)  "
                'Pinkal (28-May-2018) -- End
            Else
                strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 0 "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Pinkal (07-Jan-2019) -- Start
            'AT Testing- Working on AT Testing for All Modules.
            'If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (07-Jan-2019) -- End
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvleaveaccrue_master) </purpose>
    'Public Function Insert() As Boolean

    '    'Pinkal (26-May-2015) -- Start
    '    'Enhancement - WORKING ON ACTIVE EMPLOYEE,ADVANCE FILTER,ANALYSIS BY AND USER ACCESS.

    '    'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

    '    '    If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, False, False, False) Then
    '    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
    '    '        Return False
    '    '    End If

    '    'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '    '    If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, True, True, False) Then
    '    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
    '    '        Return False
    '    '    End If

    '    'End If

    '    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

    '        If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, False, False, False) Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
    '            Return False
    '        End If

    '    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '        If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, True, True, False) Then
    '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
    '    Return False
    'End If

    '    End If

    '    'Pinkal (26-May-2015) -- End

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Try

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
    '        objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchunkid.ToString)
    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString)
    '        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
    '        objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAccrue_Amount)
    '        objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblIssue_Amount)
    '        objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblBalance)
    '        objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_Balance)
    '        objDataOperation.AddParameter("@leavebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblLeaveBF)
    '        objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrAccrue_Amount)
    '        objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrIssue_Amount)
    '        objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDaily_amount)
    '        objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspaid.ToString)
    '        objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting.ToString)
    '        objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDays)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '        objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShortLeave.ToString)
    '        objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblActualAmount.ToString)
    '        objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenELC.ToString)
    '        objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsELC.ToString)
    '        objDataOperation.AddParameter("@isclosefy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCloseFy.ToString)
    '        objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCFlvAmount.ToString)
    '        objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
    '        objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNoAction.ToString)
    '        objDataOperation.AddParameter("@consecutivedays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConsicutivedays.ToString)
    '        objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurance.ToString)
    '        objDataOperation.AddParameter("@optiondid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiondid.ToString)
    '        objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)

    '        Dim objbatch As New clsbatch_master
    '        If objbatch.isAccrueLeaveExist(objDataOperation, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, mblnIsOpenELC, mblnIsELC, mblnIsCloseFy) Then
    '            ' START CHECK FOR WHETHER PREVIOUS YEAR 
    '            Dim dsAccrue As DataSet = GetList("List", True, , , mblnIsOpenELC, mblnIsELC, mblnIsCloseFy)
    '            Dim dtAccrue As DataTable = New DataView(dsAccrue.Tables("List"), "leavetypeunkid = " & mintLeavetypeunkid & _
    '                                                      " AND employeeunkid = " & mintEmployeeunkid & " AND yearunkid <> " & mintYearunkid, "", DataViewRowState.CurrentRows).ToTable

    '            ' END CHECK FOR WHETHER PREVIOUS YEAR 

    '            ' START CHECK FOR WHETHER CURRENT YEAR HAS 0 ISSUE AMOUNT

    '            strQ = " Select isnull(leavebalanceunkid,0) as leavebalanceunkid " & _
    '                   " from lvleavebalance_tran " & _
    '                   " WHERE issue_amount = 0 and yearunkid = @year_unkid and isvoid = 0"

    '            objDataOperation.AddParameter("@year_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid)
    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            ' END CHECK FOR WHETHER CURRENT YEAR HAS 0 ISSUE AMOUNT

    '            If dsList.Tables("List").Rows.Count > 0 Or dtAccrue.Rows.Count > 0 Then
    '                strQ = "UPDATE lvleavebalance_tran SET " & _
    '                        "  yearunkid = @yearunkid" & _
    '                        ", batchunkid = @batchunkid " & _
    '                        ", startdate = @startdate " & _
    '                        ", enddate = @enddate " & _
    '                     ", actualamount = @actualamount " & _
    '                        ", accrue_amount = @accrue_amount" & _
    '                        ", issue_amount = @issue_amount" & _
    '                        ", balance = @balance" & _
    '                        ", remaining_bal = @remaining_bal" & _
    '                        ", leavebf = @leavebf " & _
    '                        ", uptolstyr_accrueamt = @uptolstyr_accrueamt " & _
    '                        ", uptolstyr_issueamt = @uptolstyr_issueamt " & _
    '                        ", daily_amount = @daily_amount " & _
    '                        ", ispaid = @ispaid" & _
    '                        ", accruesetting = @accruesetting " & _
    '                        ", days = @days " & _
    '                        ", userunkid = @userunkid" & _
    '                        ", isvoid = @isvoid" & _
    '                        ", voiduserunkid = @voiduserunkid" & _
    '                        ", voiddatetime = @voiddatetime" & _
    '                        ", voidreason = @voidreason " & _
    '                     ", isshortleave = @isshortleave " & _
    '                        ", isopenelc = @isopenelc " & _
    '                        ", iselc = @iselc " & _
    '                     ", isclose_fy = @isclosefy " & _
    '                        ", cfamount = @cfamount " & _
    '                        ", eligibilityafter = @eligibilityafter " & _
    '                        ", isnoaction = @isnoaction " & _
    '                        ", consecutivedays = @consecutivedays" & _
    '                        ", occurrence = @occurrence" & _
    '                        ", optiondid = @optiondid " & _
    '                        ", remaining_occurrence = @remaining_occurrence " & _
    '                        " WHERE  employeeunkid = @employeeunkid" & _
    '                        " AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 "


    '                If mblnIsELC Then
    '                    If mblnIsOpenELC Then
    '                        strQ &= " AND iselc = 1 AND isopenelc = 1 "
    '                    Else
    '                        strQ &= " AND iselc = 1 AND isopenelc = 0 "
    '                    End If
    '                End If

    '                If mblnIsCloseFy Then
    '                    strQ &= " AND isclose_fy = 1 "
    '                End If

    '                objDataOperation.ExecQuery(strQ, "List")

    '                'INSERT FOR AUDIT TRAIL

    '                If dsList.Tables("List").Rows.Count > 0 Then mintBalanceunkid = CInt(dsAccrue.Tables("List").Rows(0)("leavebalanceunkid"))
    '                If dtAccrue.Rows.Count > 0 Then mintBalanceunkid = CInt(dtAccrue.Rows(0)("leavebalanceunkid"))

    '                If InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            Else
    '                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Edit this Employee's record.Reason: It is already in use.")
    '                Return False
    '            End If

    '        Else


    '            'Pinkal (09-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '            Dim mintLeaveBalanceunkid As Integer = mintBalanceunkid
    '            'Pinkal (09-Jul-2014) -- End

    '            strQ = "INSERT INTO lvleavebalance_tran ( " & _
    '              " yearunkid " & _
    '              ", employeeunkid " & _
    '              ", leavetypeunkid " & _
    '              ", batchunkid " & _
    '              ", startdate " & _
    '              ", enddate " & _
    '             ", actualamount " & _
    '              ", accrue_amount " & _
    '              ", issue_amount " & _
    '              ", balance " & _
    '              ", remaining_bal " & _
    '              ", leavebf " & _
    '              ", uptolstyr_accrueamt " & _
    '              ", uptolstyr_issueamt " & _
    '              ", daily_amount " & _
    '              ", ispaid " & _
    '              ", accruesetting " & _
    '              ", days " & _
    '              ", userunkid " & _
    '              ", isvoid " & _
    '              ", voiduserunkid " & _
    '              ", voiddatetime " & _
    '              ", voidreason " & _
    '             ", isshortleave " & _
    '                    ", isopenelc " & _
    '                    ", iselc " & _
    '           ", isclose_fy " & _
    '              ", cfamount " & _
    '              ", eligibilityafter " & _
    '              ", isnoaction " & _
    '              ", consecutivedays " & _
    '              ", occurrence " & _
    '              ", optiondid " & _
    '              ", remaining_occurrence " & _
    '            ") VALUES (" & _
    '              " @yearunkid " & _
    '              ", @employeeunkid " & _
    '              ", @leavetypeunkid " & _
    '              ", @batchunkid " & _
    '              ", @startdate " & _
    '              ", @enddate " & _
    '             ", @actualamount " & _
    '              ", @accrue_amount " & _
    '              ", @issue_amount " & _
    '              ", @balance " & _
    '              ", @remaining_bal " & _
    '              ", @leavebf " & _
    '              ", @uptolstyr_accrueamt " & _
    '              ", @uptolstyr_issueamt " & _
    '              ", @daily_amount " & _
    '              ", @ispaid " & _
    '              ", @accruesetting " & _
    '              ", @days " & _
    '              ", @userunkid " & _
    '              ", @isvoid " & _
    '              ", @voiduserunkid " & _
    '              ", @voiddatetime " & _
    '              ", @voidreason " & _
    '             ", @isshortleave " & _
    '                     ", @isopenelc " & _
    '                     ", @iselc " & _
    '           ", @isclosefy " & _
    '              ", @cfamount " & _
    '              ", @eligibilityafter " & _
    '              ", @isnoaction " & _
    '              ", @consecutivedays " & _
    '              ", @occurrence " & _
    '              ", @optiondid " & _
    '              ", @remaining_occurrence " & _
    '            "); SELECT @@identity"


    '            dsList = objDataOperation.ExecQuery(strQ, "List")
    '            mintBalanceunkid = dsList.Tables(0).Rows(0).Item(0)

    '            If InsertAudiTrailForLeaveBalance(objDataOperation, 1) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If

    '            'Pinkal (09-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

    '            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso mintLeaveBalanceunkid > 0 Then
    '                If objbatch.CloseDueDateEmployeeELC(objDataOperation, mintLeaveBalanceunkid, mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    objDataOperation.ReleaseTransaction(False)
    '                End If
    '            End If

    '            'Pinkal (09-Jul-2014) -- End

    '        End If


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw exForce
    '        End If
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        objDataOperation.ReleaseTransaction(False)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveaccrue_master) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                  , ByVal strEmployeeAsOnDate As String _
                                  , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnApplyUserAccessFilter As Boolean = True) As Boolean

        'Pinkal (26-May-2015) -- Start
        'Enhancement - WORKING ON ACTIVE EMPLOYEE,ADVANCE FILTER,ANALYSIS BY AND USER ACCESS.

        'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

        '    If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, False, False, False) Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
        '        Return False
        '    End If

        'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

        '    If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, True, True, False) Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
        '        Return False
        '    End If

        'End If

        If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

            If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, False, False, False) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
                Return False
            End If

        ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

            If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, True, True, False) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
                Return False
            End If

        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAccrue_Amount)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblIssue_Amount)
            objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblBalance)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_Balance)
            objDataOperation.AddParameter("@leavebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblLeaveBF)
            objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrAccrue_Amount)
            objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrIssue_Amount)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDaily_amount)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspaid.ToString)
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting.ToString)
            objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDays)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShortLeave.ToString)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblActualAmount.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenELC.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsELC.ToString)
            objDataOperation.AddParameter("@isclosefy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCloseFy.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCFlvAmount.ToString)
            objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNoAction.ToString)
            objDataOperation.AddParameter("@consecutivedays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConsicutivedays.ToString)
            objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurance.ToString)
            objDataOperation.AddParameter("@optiondid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiondid.ToString)
            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)
            objDataOperation.AddParameter("@adj_remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecAdjustmentAmt)
            objDataOperation.AddParameter("@monthly_accrue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecMonthly_AccrueAmt)


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@maxnegative_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNegativeDaysLimit.ToString)
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@occ_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurrenceTenure.ToString)
            'Pinkal (13-Oct-2018) -- End


            Dim objbatch As New clsbatch_master
            If objbatch.isAccrueLeaveExist(objDataOperation, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, mblnIsOpenELC, mblnIsELC, mblnIsCloseFy) Then

                ' START CHECK FOR WHETHER PREVIOUS YEAR 
                Dim dsAccrue As DataSet = GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                    , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                                                    , blnApplyUserAccessFilter, False, mintEmployeeunkid, mblnIsOpenELC _
                                                                  , mblnIsELC, mblnIsCloseFy, "lvleavebalance_tran.leavetypeunkid = " & mintLeavetypeunkid & " AND lvleavebalance_tran.yearunkid <> " & mintYearunkid, objDataOperation)


                ' END CHECK FOR WHETHER PREVIOUS YEAR 

                ' START CHECK FOR WHETHER CURRENT YEAR HAS 0 ISSUE AMOUNT

                strQ = " Select isnull(leavebalanceunkid,0) as leavebalanceunkid " & _
                       " from lvleavebalance_tran " & _
                       " WHERE issue_amount = 0 and yearunkid = @year_unkid and isvoid = 0"

                strQ &= " AND employeeunkid = " & mintEmployeeunkid & " AND leavetypeunkid = " & mintLeavetypeunkid

                objDataOperation.AddParameter("@year_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                ' END CHECK FOR WHETHER CURRENT YEAR HAS 0 ISSUE AMOUNT

                If dsList.Tables("List").Rows.Count > 0 OrElse dsAccrue.Tables(0).Rows.Count > 0 Then   ' Or dtAccrue.Rows.Count > 0

                    strQ = "UPDATE lvleavebalance_tran SET " & _
                            "  yearunkid = @yearunkid" & _
                            ", batchunkid = @batchunkid " & _
                            ", startdate = @startdate " & _
                            ", enddate = @enddate " & _
                         ", actualamount = @actualamount " & _
                            ", accrue_amount = @accrue_amount" & _
                            ", issue_amount = @issue_amount" & _
                            ", balance = @balance" & _
                            ", remaining_bal = @remaining_bal" & _
                            ", leavebf = @leavebf " & _
                            ", uptolstyr_accrueamt = @uptolstyr_accrueamt " & _
                            ", uptolstyr_issueamt = @uptolstyr_issueamt " & _
                            ", daily_amount = @daily_amount " & _
                            ", ispaid = @ispaid" & _
                            ", accruesetting = @accruesetting " & _
                            ", days = @days " & _
                            ", userunkid = @userunkid" & _
                            ", isvoid = @isvoid" & _
                            ", voiduserunkid = @voiduserunkid" & _
                            ", voiddatetime = @voiddatetime" & _
                            ", voidreason = @voidreason " & _
                         ", isshortleave = @isshortleave " & _
                            ", isopenelc = @isopenelc " & _
                            ", iselc = @iselc " & _
                         ", isclose_fy = @isclosefy " & _
                            ", cfamount = @cfamount " & _
                            ", eligibilityafter = @eligibilityafter " & _
                            ", isnoaction = @isnoaction " & _
                            ", consecutivedays = @consecutivedays" & _
                            ", occurrence = @occurrence" & _
                            ", optiondid = @optiondid " & _
                            ", remaining_occurrence = @remaining_occurrence " & _
                            ", adj_remaining_bal = @adj_remaining_bal " & _
                            ", monthly_accrue = @monthly_accrue " & _
                            ", maxnegative_limit = @maxnegative_limit " & _
                            ", occ_tenure = @occ_tenure " & _
                            " WHERE  employeeunkid = @employeeunkid" & _
                            " AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 "


                    'Pinkal (13-Oct-2018) 'Enhancement - Leave Enhancement for NMB.[", occ_tenure = @occ_tenure " & _]

                    'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", maxnegative_limit = @maxnegative_limit " & _]

                    If mblnIsELC Then
                        If mblnIsOpenELC Then
                            strQ &= " AND iselc = 1 AND isopenelc = 1 "
                        Else
                            strQ &= " AND iselc = 1 AND isopenelc = 0 "
                        End If
                    End If

                    If mblnIsCloseFy Then
                        strQ &= " AND isclose_fy = 1 "
                    End If

                    objDataOperation.ExecQuery(strQ, "List")

                    'INSERT FOR AUDIT TRAIL

                    If dsList.Tables("List").Rows.Count > 0 Then mintBalanceunkid = CInt(dsList.Tables("List").Rows(0)("leavebalanceunkid"))
                    If dsAccrue.Tables(0).Rows.Count > 0 Then mintBalanceunkid = CInt(dsAccrue.Tables(0).Rows(0)("leavebalanceunkid"))

                    If InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Edit this Employee's record.Reason: It is already in use.")
                    Return False
                End If

            Else

                Dim mintLeaveBalanceunkid As Integer = mintBalanceunkid
                strQ = "INSERT INTO lvleavebalance_tran ( " & _
                  " yearunkid " & _
                  ", employeeunkid " & _
                  ", leavetypeunkid " & _
                  ", batchunkid " & _
                  ", startdate " & _
                  ", enddate " & _
                 ", actualamount " & _
                  ", accrue_amount " & _
                  ", issue_amount " & _
                  ", balance " & _
                  ", remaining_bal " & _
                  ", leavebf " & _
                  ", uptolstyr_accrueamt " & _
                  ", uptolstyr_issueamt " & _
                  ", daily_amount " & _
                  ", ispaid " & _
                  ", accruesetting " & _
                  ", days " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason " & _
                 ", isshortleave " & _
                        ", isopenelc " & _
                        ", iselc " & _
               ", isclose_fy " & _
                  ", cfamount " & _
                  ", eligibilityafter " & _
                  ", isnoaction " & _
                  ", consecutivedays " & _
                  ", occurrence " & _
                  ", optiondid " & _
                  ", remaining_occurrence " & _
                  ", adj_remaining_bal " & _
                          ", monthly_accrue " & _
                          ", maxnegative_limit " & _
                          ", occ_tenure " & _
                          " ) VALUES (" & _
                  " @yearunkid " & _
                  ", @employeeunkid " & _
                  ", @leavetypeunkid " & _
                  ", @batchunkid " & _
                  ", @startdate " & _
                  ", @enddate " & _
                 ", @actualamount " & _
                  ", @accrue_amount " & _
                  ", @issue_amount " & _
                  ", @balance " & _
                  ", @remaining_bal " & _
                  ", @leavebf " & _
                  ", @uptolstyr_accrueamt " & _
                  ", @uptolstyr_issueamt " & _
                  ", @daily_amount " & _
                  ", @ispaid " & _
                  ", @accruesetting " & _
                  ", @days " & _
                  ", @userunkid " & _
                  ", @isvoid " & _
                  ", @voiduserunkid " & _
                  ", @voiddatetime " & _
                  ", @voidreason " & _
                 ", @isshortleave " & _
                         ", @isopenelc " & _
                         ", @iselc " & _
               ", @isclosefy " & _
                  ", @cfamount " & _
                  ", @eligibilityafter " & _
                  ", @isnoaction " & _
                  ", @consecutivedays " & _
                  ", @occurrence " & _
                  ", @optiondid " & _
                  ", @remaining_occurrence " & _
                  ", @adj_remaining_bal " & _
                          ", @monthly_accrue " & _
                              ", @maxnegative_limit " & _
                          ", @occ_tenure " & _
                          " ); SELECT @@identity"

                'Pinkal (13-Oct-2018) -- Enhancement - Leave Enhancement for NMB.[", @occ_tenure " & _]

                'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", @maxnegative_limit " & _]

                dsList = objDataOperation.ExecQuery(strQ, "List")
                mintBalanceunkid = dsList.Tables(0).Rows(0).Item(0)

                If InsertAudiTrailForLeaveBalance(objDataOperation, 1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso mintLeaveBalanceunkid > 0 Then
                    With objbatch
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objbatch.CloseDueDateEmployeeELC(objDataOperation, mintLeaveBalanceunkid, mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        objDataOperation.ReleaseTransaction(False)
                    End If
                End If

            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                objDataOperation.ReleaseTransaction(False)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveaccrue_master) </purpose>
    Public Function Update() As Boolean

        If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, False, False, False) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
                Return False
            End If

        ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            If isExist(mintYearunkid, mintEmployeeunkid, mintLeavetypeunkid, mintBalanceunkid, True, True, False) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
                Return False
            End If
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBalanceunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAccrue_Amount)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblIssue_Amount)
            objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblBalance)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_Balance)
            objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrAccrue_Amount)
            objDataOperation.AddParameter("@uptolstyr_issueamnt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrIssue_Amount)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDaily_amount)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspaid.ToString)
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting.ToString)
            objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDays)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShortLeave.ToString)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblActualAmount.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenELC.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsELC.ToString)
            objDataOperation.AddParameter("@isclosefy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCloseFy.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCFlvAmount.ToString)
            objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNoAction.ToString)
            objDataOperation.AddParameter("@consecutivedays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConsicutivedays.ToString)
            objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurance.ToString)
            objDataOperation.AddParameter("@optiondid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiondid.ToString)
            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)
            objDataOperation.AddParameter("@adj_remaining_bal", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdjustmentAmt.ToString)
            objDataOperation.AddParameter("@monthly_accrue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMonthly_AccrueAmt.ToString)


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@maxnegative_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNegativeDaysLimit)
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@occ_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurrenceTenure)
            'Pinkal (13-Oct-2018) -- End



            strQ = " UPDATE lvleavebalance_tran SET " & _
              "  yearunkid = @yearunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", leavetypeunkid = @leavetypeunkid" & _
              ", batchunkid = @batchunkid " & _
              ", startdate =  @startdate " & _
              ", enddate =  @enddate " & _
             ", actualamount = @actualamount " & _
              ", accrue_amount = @accrue_amount" & _
              ", issue_amount = @issue_amount" & _
              ", balance = @balance" & _
              ", remaining_bal = @remaining_bal " & _
              ", uptolstyr_accrueamt = @uptolstyr_accrueamt " & _
              ", uptolstyr_issueamt = @uptolstyr_issueamnt " & _
              ", daily_amount = @daily_amount " & _
              ", ispaid = @ispaid" & _
              ", accruesetting = @accruesetting " & _
              ", days = @days " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
             ", isshortleave = @isshortleave " & _
                       ", isopenelc = @isopenelc " & _
                       ", iselc = @iselc " & _
                   ", isclose_fy = @isclosefy " & _
              ", cfamount = @cfamount " & _
              ", eligibilityafter = @eligibilityafter " & _
              ", isnoaction = @isnoaction " & _
              ", consecutivedays = @consecutivedays" & _
              ", occurrence = @occurrence" & _
              ", optiondid = @optiondid " & _
              ", remaining_occurrence = @remaining_occurrence " & _
              ", adj_remaining_bal = @adj_remaining_bal " & _
                      ", monthly_accrue = @monthly_accrue " & _
                      ", maxnegative_limit = @maxnegative_limit " & _
                      ", occ_tenure = @occ_tenure " & _
                      " WHERE leavebalanceunkid = @leavebalanceunkid "

            'Pinkal (13-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", occ_tenure = @occ_tenure " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", maxnegative_limit = @maxnegative_limit " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveaccrue_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal mintByLeaveType As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim count As Integer = -1
        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            If intUnkid > 0 Then

                strQ = "Select isnull(issue_amount,0) as Issue_Amount from lvleavebalance_tran WHERE leavebalanceunkid = @leavebalanceunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "IssueAmount")
                If dsList.Tables("IssueAmount").Rows.Count > 0 Then
                    If CInt(dsList.Tables("IssueAmount").Rows(0)("Issue_Amount")) > 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This Accure Leave is already Issued. You cannot delete this Accrue Leave.")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                ' START FOR UPDATE LEAVE ACCRUE TRAN

                strQ = "Update lvleavebalance_tran set " & _
                       " isvoid = 1,voiddatetime=@voiddatetime,voidreason=@voidreason,voiduserunkid = @voiduserunkid " & _
                       " WHERE leavebalanceunkid = @leavebalanceunkid "


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.ExecNonQuery(strQ)

                ' END FOR UPDATE LEAVE ACCRUE TRAN


            ElseIf mintByLeaveType > 0 Then

                strQ = "select isnull(leavebalanceunkid,0) as leavebalanceunkid  " & _
                       ", isnull(issue_amount,0) as issue_amount " & _
                       "  FROM lvleavebalance_tran where leavetypeunkid = @leavetypeunkid  AND isvoid = 0"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintByLeaveType)

                Dim dsFill As DataSet = objDataOperation.ExecQuery(strQ, "List")
                If dsFill.Tables("List").Rows.Count > 0 Then
                    For Each dr As DataRow In dsFill.Tables("List").Rows

                        If CInt(dr("issue_amount")) > 0 Then
                            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, This Leave is already Issued to Some Employees. Thus you cannot delete this Leave.")
                        Else
                            strQ = "Update lvleavebalance_tran set isvoid = 1,voiddatetime=@voiddatetime,voidreason =@voidreason,voiduserunkid= @voiduserunkid " & _
                                " WHERE leavebalanceunkid = @leavebalanceunkid "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("leavebalanceunkid")))
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                            objDataOperation.ExecNonQuery(strQ)

                        End If

                    Next
                End If
            End If



            'START FOR AUDIT TRAIL
            _LeaveBalanceunkid = intUnkid
            _FormName = mstrFormName
            _LoginEmployeeUnkid = mintLogEmployeeUnkid
            _ClientIP = mstrClientIP
            _HostName = mstrHostName
            _FromWeb = mblnIsWeb
            _AuditUserId = mintAuditUserId
            _AuditDate = mdtAuditDate

            If InsertAudiTrailForLeaveBalance(objDataOperation, 3) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'END FOR AUDIT TRAIL

            Dim objLeaveAdjustment As New clsleaveadjustment_Tran
            objLeaveAdjustment._Voiduserunkid = mintUserunkid
            objLeaveAdjustment._Voidreason = mstrVoidreason
            With objLeaveAdjustment
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLogEmployeeUnkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objLeaveAdjustment.Delete(intUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objLeaveAdjustment = Nothing

            objDataOperation.ReleaseTransaction(True)
            If mstrMessage = "" Then Return True Else Return False
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@leaveaccrueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intLeavetypeunkid As Integer _
                                   , Optional ByVal intUnkid As Integer = -1, Optional ByVal blnIsOpenELC As Boolean = False, Optional ByVal blnIsELC As Boolean = False _
                                   , Optional ByVal IsCloseFy As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  leavebalanceunkid " & _
              ", yearunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", batchunkid " & _
              ", startdate " & _
              ", enddate " & _
             ", actualamount " & _
              ", accrue_amount " & _
              ", issue_amount " & _
              ", balance " & _
              ", remaining_bal " & _
              ", leavebf " & _
              ", uptolstyr_accrueamt " & _
              ", uptolstyr_issueamt " & _
              ", daily_amount " & _
              ", ispaid " & _
              ", accruesetting " & _
              ", days " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             ", ISNULL(isshortleave,0) isshortleave " & _
               ", ISNULL(isopenelc,0) isopenelc " & _
               ", ISNULL(iselc,0) iselc " & _
             ", ISNULL(isclose_fy,0) isclose_fy " & _
              ", ISNULL(cfamount,0) AS cfamount " & _
              ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
              ", ISNULL(isnoaction,0) AS isnoaction " & _
              ", ISNULL(consecutivedays,0) AS consecutivedays " & _
              ", ISNULL(occurrence,0) AS occurrence " & _
              ", ISNULL(optiondid,0) AS optiondid " & _
              ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                      ", ISNULL(adj_remaining_bal,0) AS adj_remaining_bal " & _
                      ", ISNULL(monthly_accrue,0) AS monthly_accrue " & _
                      ", ISNULL(maxnegative_limit,0) AS maxnegative_limit " & _
                      ", ISNULL(occ_tenure,0) AS occ_tenure " & _
                      "  FROM lvleavebalance_tran " & _
                      " WHERE yearunkid = @yearunkid " & _
                      " AND employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid " & _
                      " AND isvoid = 0 "

            'Pinkal (13-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(occ_tenure,0) AS occ_tenure " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(maxnegative_limit,0) AS maxnegative_limit " & _]

            If intUnkid > 0 Then
                strQ &= " AND lvleavebalance_tran.leavebalanceunkid <> @leavebalanceunkid"
            End If

            If blnIsELC Then

                If blnIsOpenELC Then
                    strQ &= " AND lvleavebalance_tran.iselc = 1 AND  lvleavebalance_tran.isopenelc = 1 "
                Else
                    strQ &= " AND lvleavebalance_tran.iselc = 1 AND lvleavebalance_tran.isopenelc = 0 "
                End If

            End If

            If IsCloseFy Then
                strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 1 "
            Else
                strQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 0 "
            End If

            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeavetypeunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintBalanceunkid = CInt(dsList.Tables(0).Rows(0)("leavebalanceunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAudiTrailForLeaveBalance(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atlvleavebalance_tran ( " & _
                  "  leavebalanceunkid " & _
                  ", yearunkid " & _
                  ", employeeunkid " & _
                  ", leavetypeunkid " & _
                  ", batchunkid " & _
                  ", startdate " & _
                  ", enddate " & _
                 ", actualamount " & _
                  ", accrue_amount " & _
                  ", issue_amount " & _
                  ", balance " & _
                  ", remaining_bal " & _
                  ", leavebf " & _
                  ", uptolstyr_accrueamt " & _
                  ", uptolstyr_issueamt " & _
                  ", daily_amount " & _
                  ", ispaid " & _
                  ", accruesetting " & _
                  ", [days] " & _
                  ", audittype " & _
                  ", audituserunkid " & _
                  ", auditdatetime " & _
                  ", ip" & _
                  ", machine_name " & _
                ", isshortleave " & _
                ", form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", isweb " & _
                      ", isopenelc " & _
                      ", iselc " & _
                  ", isclose_fy " & _
                  ", cfamount " & _
                  ", eligibilityafter " & _
                  ", isnoaction " & _
                  ", consecutivedays " & _
                  ", occurrence " & _
                  ", optiondid " & _
                  ", remaining_occurrence " & _
                      ", adj_remaining_bal " & _
                      ", monthly_accrue " & _
                      ", maxnegative_limit " & _
                      ", occ_tenure " & _
                    " ) VALUES (" & _
                  "  @leavebalanceunkid " & _
                  ", @yearunkid " & _
                  ", @employeeunkid " & _
                  ", @leavetypeunkid " & _
                  ", @batchunkid " & _
                  ", @startdate " & _
                  ", @enddate " & _
                 ", @actualamount " & _
                  ", @accrue_amount " & _
                  ", @issue_amount " & _
                  ", @balance " & _
                  ", @remaining_bal " & _
                  ", @leavebf " & _
                  ", @uptolstyr_accrueamt " & _
                  ", @uptolstyr_issueamt " & _
                  ", @daily_amount " & _
                  ", @ispaid " & _
                  ", @accruesetting " & _
                  ", @days " & _
                  ", @audittype " & _
                  ", @audituserunkid " & _
                  ", @auditdatetime " & _
                  ", @ip" & _
                  ", @machine_name " & _
                 ", @isshortleave " & _
                ", @form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", @isweb " & _
                      ", @isopenelc " & _
                      ", @iselc " & _
                 ", @isclosefy " & _
                  ", @cfamount " & _
                  ", @eligibilityafter " & _
                  ", @isnoaction " & _
                  ", @consecutivedays " & _
                  ", @occurrence " & _
                  ", @optiondid " & _
                  ", @remaining_occurrence " & _
                      ", @adj_remaining_bal " & _
                      ", @monthly_accrue " & _
                      ", @maxnegative_limit " & _
                      ", @occ_tenure " & _
                "); "


            'Pinkal (13-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", @occ_tenure " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", @maxnegative_limit " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBalanceunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAccrue_Amount)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblIssue_Amount)
            objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblBalance)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_Balance)
            objDataOperation.AddParameter("@leavebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblLeaveBF)
            objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrAccrue_Amount)
            objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrIssue_Amount)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDaily_amount)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspaid.ToString)
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting.ToString)
            objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDays.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            'If mstrWebClientIP.Trim().Length > 0 Then
            '    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            'Else
            '    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            'End If

            'If mstrWebHostName.Trim().Length > 0 Then
            '    objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName.Trim())
            'Else
            '    objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            'End If

            'If mstrWebClientIP.Trim().Length > 0 Then
            '    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            'Else
            '    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            'End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP.Trim())
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            'Pinkal (01-Oct-2018) -- End


            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShortLeave.ToString)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblActualAmount.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenELC.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsELC.ToString)
            objDataOperation.AddParameter("@isclosefy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCloseFy.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCFlvAmount.ToString)
            objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNoAction.ToString)
            objDataOperation.AddParameter("@consecutivedays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConsicutivedays.ToString)
            objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurance.ToString)
            objDataOperation.AddParameter("@optiondid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiondid.ToString)
            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)

            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@adj_remaining_bal", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdjustmentAmt.ToString)
            objDataOperation.AddParameter("@monthly_accrue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMonthly_AccrueAmt.ToString)


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@maxnegative_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNegativeDaysLimit.ToString)
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@occ_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurrenceTenure.ToString)
            'Pinkal (13-Oct-2018) -- End


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAudiTrailForLeaveBalance", mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeBalanceData(ByVal Leavetypeunkid As String, ByVal Employeeunkid As String, ByVal IsOnlyPaidLeave As Boolean, Optional ByVal intYearunkid As Integer = -1 _
                                                               , Optional ByVal IsOpenELC As Boolean = False, Optional ByVal IsELC As Boolean = False, Optional ByVal IsCloseFy As Boolean = False, Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal strDatabaseName As String = "") As DataSet
        'Sohail (15 Jan 2019) - [strDatabaseName]
        'Pinkal (02-Dec-2015) -- Start  'Enhancement - Solving Leave bug in Self Service. [Optional ByVal objDoOperation As clsDataOperation = Nothing] 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (15 Jan 2019) -- Start
        'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
        Dim strDB As String = ""
        If strDatabaseName.Trim <> "" Then
            strDB = strDatabaseName & ".."
        End If
        'Sohail (15 Jan 2019) -- End

        Try

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
             "  leavebalanceunkid " & _
             ", yearunkid " & _
             ", employeeunkid " & _
             ", leavetypeunkid " & _
             ", batchunkid " & _
             ", startdate " & _
             ", enddate " & _
             ", actualamount " & _
             ", accrue_amount " & _
             ", issue_amount " & _
             ", balance " & _
             ", remaining_bal " & _
             ", leavebf " & _
             ", uptolstyr_accrueamt " & _
             ", uptolstyr_issueamt " & _
             ", daily_amount " & _
             ", ispaid " & _
             ", accruesetting " & _
             ", days " & _
             ", userunkid " & _
             ", isvoid " & _
             ", voiduserunkid " & _
             ", voiddatetime " & _
             ", voidreason " & _
             ", ISNULL(isshortleave,0) isshortleave " & _
                      ", ISNULL(isopenelc,0) isopenelc " & _
                      ", ISNULL(iselc,0) iselc " & _
                     ", ISNULL(isclose_fy,0) isclose_fy " & _
             ", ISNULL(cfamount,0) AS cfamount " & _
             ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
             ", ISNULL(isnoaction,0) AS isnoaction " & _
             ", ISNULL(consecutivedays,0) AS consecutivedays " & _
             ", ISNULL(occurrence,0) AS occurrence " & _
             ", ISNULL(optiondid,0) AS optiondid " & _
             ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
             ", ISNULL(adj_remaining_bal,0.00) AS adj_remaining_bal " & _
                     ", ISNULL(monthly_accrue,0.00) AS monthly_accrue " & _
                     ", ISNULL(maxnegative_limit,0) AS maxnegative_limit " & _
                     ", ISNULL(occ_tenure,0) AS occ_tenure " & _
                     " FROM " & strDB & "lvleavebalance_tran " & _
                     " WHERE employeeunkid in (" & Employeeunkid & ") AND leavetypeunkid in (" & Leavetypeunkid & ") " & _
                     " AND isvoid = 0  "
            'Sohail (15 Jan 2019) - [strDB]

            'Pinkal (13-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", ISNULL(occ_tenure,0) AS occ_tenure " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(maxnegative_limit,0) AS maxnegative_limit " & _]

            If intYearunkid > 0 Then
                strQ &= " AND yearunkid = @yearunkid "
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            End If

            If IsOnlyPaidLeave = True Then
                strQ &= " AND ispaid = 1"
            End If

            If IsELC Then

                If IsOpenELC Then
                    strQ &= " AND isopenelc  = 1 "
                Else
                    strQ &= " AND isopenelc  = 0 "
                End If

            End If

            'Pinkal (12-Jan-2019) -- Start
            'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
            'If IsCloseFy Then
            '    strQ &= " AND ISNULL(isclose_fy,0)  = 1 "
            'Else
            '    strQ &= " AND ISNULL(isclose_fy,0)  = 0 "
            'End If

            If IsCloseFy Then
                strQ &= " AND ISNULL(isclose_fy,0) = CASE WHEN ISNULL(isshortleave,0) =  1 THEN  0 ELSE  1 END "
            Else
                strQ &= " AND ISNULL(isclose_fy,0)  = 0 "
            End If

            'Pinkal (12-Jan-2019) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeBalanceData; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetAuditDataforLeaveBalance(ByVal intleavebalanceunkid As Integer, ByVal intYearunkid As Integer, ByVal intLeavetypeunkid As Integer, ByVal intEmployeeunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objdataoperation As New clsDataOperation

            strQ = " SELECT  * " & _
                   " FROM atlvleavebalance_tran  " & _
                   " WHERE auditdatetime = (SELECT MAX(auditdatetime) " & _
                   " FROM atlvleavebalance_tran " & _
                   " WHERE leavebalanceunkid = @leavebalanceunkid AND yearunkid = @yearunkid AND employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid)"


            objdataoperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intleavebalanceunkid)
            objdataoperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            objdataoperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeavetypeunkid)
            objdataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)

            dsList = objdataoperation.ExecQuery(strQ, "List")
            If objdataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAuditDataforLeaveBalance", mstrModuleName)
        End Try

        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveBalanceInfo(ByVal mdtAsonDate As DateTime, ByVal Leavetypeunkid As String, ByVal Employeeunkid As String _
                                        , ByVal xLeaveAccrueTenureSetting As Integer _
                                        , ByVal xLeaveAccrueDaysAfterEachMonth As Integer _
                                        , Optional ByVal dtDatabaseStartDate As Date = Nothing _
                                        , Optional ByVal dtDatabaseEndDate As Date = Nothing _
                                        , Optional ByVal IsOpenELC As Boolean = False _
                                        , Optional ByVal IsELC As Boolean = False _
                                        , Optional ByVal intLeaveBalanceSetting As Integer = 0 _
                                        , Optional ByVal intIsFin_Close As Integer = -1 _
                                        , Optional ByVal intYearId As Integer = -1 _
                                        , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                        , Optional ByVal strDatabaseName As String = "" _
                                        , Optional ByVal mblnFromLeaveBalanceListReport As Boolean = False _
                                        , Optional ByVal mblnLeaveIssueAsonDateOnLeaveBalanceListReport As Boolean = False) As DataSet

        'Pinkal (19-Feb-2020) --  'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.[Optional ByVal mblnFromLeaveBalanceListReport As Boolean = False,Optional ByVal mblnLeaveIssueAsonDateOnLeaveBalanceListReport As Boolean = False]

        'Sohail (15 Jan 2019) - [strDatabaseName]
        'Sohail (03 May 2018) - [xDataOp]




        Dim dsList As New DataSet
        Dim dsBalanceinfo As DataSet = Nothing
        Dim DrRow As DataRow
        Try

            dsBalanceinfo = New DataSet
            dsBalanceinfo.Tables.Add("BalanceInfo")
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("LstyearAccrue_amount", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("LstyearIssue_amount", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("Accrue_amount", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("Issue_amount", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("Balance", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("LeaveBF", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("LeaveEncashment", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("LeaveAdjustment", Type.GetType("System.Decimal"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("employeeunkid", Type.GetType("System.Int32")).DefaultValue = 0
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("Startdate", Type.GetType("System.DateTime"))
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("Enddate", Type.GetType("System.DateTime"))

            'S.SANDEEP [11-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#194|#ARUTI-82}
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("daily_amount", GetType(System.Decimal)).DefaultValue = 0
            'S.SANDEEP [11-Apr-2018] -- END

            'Pinkal (09-Aug-2018) -- Start
            'Bug - Solving Bug for PACRA.
            dsBalanceinfo.Tables("BalanceInfo").Columns.Add("TotalAcccrueAmt", Type.GetType("System.Decimal"))
            'Pinkal (09-Aug-2018) -- End

            If intLeaveBalanceSetting = 0 Then intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            'Sohail (15 Jan 2019) -- Start
            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
            'If intIsFin_Close <> 0 OrElse intIsFin_Close <> 1 Then intIsFin_Close = FinancialYear._Object._IsFin_Close
            If intIsFin_Close < 0 Then intIsFin_Close = FinancialYear._Object._IsFin_Close
            'Sohail (15 Jan 2019) -- End

            If intYearId <= 0 Then intYearId = FinancialYear._Object._YearUnkid


            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'dsList = GetEmployeeBalanceData(Leavetypeunkid, Employeeunkid, True, intYearId, False, False, CBool(intIsFin_Close))
                'Sohail (15 Jan 2019) -- Start
                'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
                'dsList = GetEmployeeBalanceData(Leavetypeunkid, Employeeunkid, True, intYearId, False, False, CBool(intIsFin_Close), xDataOp)
                dsList = GetEmployeeBalanceData(Leavetypeunkid, Employeeunkid, True, intYearId, False, False, CBool(intIsFin_Close), xDataOp, strDatabaseName)
                'Sohail (15 Jan 2019) -- End
                'Sohail (03 May 2018) -- End

            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'dsList = GetEmployeeBalanceData(Leavetypeunkid, Employeeunkid, True, intYearId, IsOpenELC, IsELC)
                'Sohail (15 Jan 2019) -- Start
                'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
                'dsList = GetEmployeeBalanceData(Leavetypeunkid, Employeeunkid, True, intYearId, IsOpenELC, IsELC, , xDataOp)
                dsList = GetEmployeeBalanceData(Leavetypeunkid, Employeeunkid, True, intYearId, IsOpenELC, IsELC, , xDataOp, strDatabaseName)
                'Sohail (15 Jan 2019) -- End
                'Sohail (03 May 2018) -- End
            End If

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                DrRow = dsBalanceinfo.Tables("BalanceInfo").NewRow()
                DrRow("LstyearAccrue_amount") = 0.0
                DrRow("LstyearIssue_amount") = 0.0
                DrRow("Accrue_amount") = 0.0
                DrRow("Issue_amount") = 0.0
                DrRow("Balance") = 0.0

                'Pinkal (01-Jul-2016) -- Start
                'Enhancement - Solved Problem in Leave Reports Given By Rutta.

                If CDec(dsList.Tables(0).Rows(i)("uptolstyr_accrueamt")) <> 0 Then
                    DrRow("LstyearAccrue_amount") = CStr(Math.Round(CDec(dsList.Tables(0).Rows(i)("uptolstyr_accrueamt")) - CDec(dsList.Tables(0).Rows(i)("accrue_amount")), 2))
                End If

                If CDec(dsList.Tables(0).Rows(i)("uptolstyr_issueamt")) <> 0 Then
                    DrRow("LstyearIssue_amount") = CStr(Math.Round(CDec(dsList.Tables(0).Rows(i)("uptolstyr_issueamt")) - CDec(dsList.Tables(0).Rows(i)("issue_amount")), 2))
                End If

                'Pinkal (01-Jul-2016) -- End

                DrRow("LeaveEncashment") = 0
                'Pinkal (16-Jan-2016) -- Start
                'Enhancement -Working on Changes FOR CHAI BORA.
                DrRow("LeaveAdjustment") = CDec(dsList.Tables(0).Rows(i)("adj_remaining_bal"))
                'Pinkal (16-Jan-2016) -- End

                Dim objEmployee As New clsEmployee_Master
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                objEmployee._DataOperation = xDataOp
                'Sohail (03 May 2018) -- End
                objEmployee._Employeeunkid(mdtAsonDate) = CInt(dsList.Tables(0).Rows(i)("employeeunkid"))
                'Sohail (29 Mar 2016) -- Start
                'Enhancement - 58.1 - Payslip Performance Optimization.
                DrRow("employeeunkid") = CInt(dsList.Tables(0).Rows(i)("employeeunkid"))
                'Sohail (29 Mar 2016) -- End

                'S.SANDEEP [11-Apr-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#194|#ARUTI-82}
                DrRow("daily_amount") = CDec(dsList.Tables(0).Rows(i)("daily_amount"))
                'S.SANDEEP [11-Apr-2018] -- END


                'Pinkal (09-Aug-2018) -- Start
                'Bug - Solving Bug for PACRA.
                DrRow("TotalAcccrueAmt") = 0
                'Pinkal (09-Aug-2018) -- End

                Dim mdtStartdate As DateTime = Nothing
                Dim mdtEnddate As DateTime = Nothing

                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    If dtDatabaseEndDate = Nothing Then dtDatabaseEndDate = FinancialYear._Object._Database_End_Date

                    'Pinkal (08-Mar-2018) -- Start
                    'Bug - Issue No 0002080 (PACRA) When you pull salary slip for one employee, eg. Mpumpu. It shows leave c/f of 60. But when we pull the same report but with another employee selected Its giving leave c/f of 78.
                    'If dtDatabaseStartDate = Nothing Then dtDatabaseStartDate = CDate(dsList.Tables(0).Rows(0)("startdate"))
                    If dtDatabaseStartDate = Nothing Then dtDatabaseStartDate = CDate(dsList.Tables(0).Rows(i)("startdate"))
                    'Pinkal (08-Mar-2018) -- End


                    If objEmployee._Appointeddate.Date < dtDatabaseStartDate Then
                        mdtStartdate = dtDatabaseStartDate
                    Else
                        mdtStartdate = objEmployee._Appointeddate.Date
                    End If



                    'Pinkal (28-May-2019) -- Start
                    'Defect [0003831] CCK - Leave balances displayed on Accrue as on date and balance as on date columns not accurate
                    If objEmployee._Reinstatementdate.Date <> Nothing AndAlso objEmployee._Reinstatementdate.Date >= mdtStartdate.Date Then
                        mdtStartdate = objEmployee._Reinstatementdate.Date
                    End If
                    'Pinkal (28-May-2019) -- End


                    'Pinkal (08-Mar-2018) -- Start
                    'Bug - Issue No 0002080 (PACRA) When you pull salary slip for one employee, eg. Mpumpu. It shows leave c/f of 60. But when we pull the same report but with another employee selected Its giving leave c/f of 78.
                    'If IsDBNull(dsList.Tables(0).Rows(0)("enddate")) Then
                    '    mdtEnddate = IIf(dtDatabaseEndDate = Nothing, FinancialYear._Object._Database_End_Date.Date, dtDatabaseEndDate)
                    'Else
                    '    mdtEnddate = CDate(dsList.Tables(0).Rows(0)("enddate"))
                    'End If
                    If IsDBNull(dsList.Tables(0).Rows(i)("enddate")) Then
                        mdtEnddate = IIf(dtDatabaseEndDate = Nothing, FinancialYear._Object._Database_End_Date.Date, dtDatabaseEndDate)
                    Else
                        'Pinkal (28-May-2019) -- Start
                        'Defect [0003831] CCK - Leave balances displayed on Accrue as on date and balance as on date columns not accurate
                        If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < CDate(dsList.Tables(0).Rows(i)("enddate"))) Then
                            mdtEnddate = objEmployee._Termination_To_Date.Date
                        Else
                        mdtEnddate = CDate(dsList.Tables(0).Rows(i)("enddate"))
                    End If
                        'Pinkal (28-May-2019) -- End
                    End If
                    'Pinkal (08-Mar-2018) -- End

                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    'Pinkal (08-Mar-2018) -- Start
                    'Bug - Issue No 0002080 (PACRA) When you pull salary slip for one employee, eg. Mpumpu. It shows leave c/f of 60. But when we pull the same report but with another employee selected Its giving leave c/f of 78.
                    'If dtDatabaseStartDate = Nothing Then dtDatabaseStartDate = CDate(dsList.Tables(0).Rows(0)("startdate"))
                    'If dtDatabaseEndDate = Nothing Then dtDatabaseEndDate = CDate(dsList.Tables(0).Rows(0)("enddate"))
                    If dtDatabaseStartDate = Nothing Then dtDatabaseStartDate = CDate(dsList.Tables(0).Rows(i)("startdate"))
                    If dtDatabaseEndDate = Nothing Then dtDatabaseEndDate = CDate(dsList.Tables(0).Rows(i)("enddate"))
                    'Pinkal (08-Mar-2018) -- End

                    If objEmployee._Appointeddate.Date <= dtDatabaseStartDate.Date Then
                        mdtStartdate = dtDatabaseStartDate.Date
                    Else
                        mdtStartdate = objEmployee._Appointeddate.Date
                    End If


                    'Pinkal (29-Jan-2018) -- Start
                    'Bug - SUPPORT HJFMRI 0001875 |  Leave balances not getting properly computed.
                    'If objEmployee._Reinstatementdate.Date <> Nothing AndAlso objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    If objEmployee._Reinstatementdate.Date <> Nothing AndAlso objEmployee._Reinstatementdate.Date >= mdtStartdate.Date Then
                        'Pinkal (29-Jan-2018) -- End
                         mdtStartdate = objEmployee._Reinstatementdate.Date
                    End If

                    'Pinkal (08-Mar-2018) -- Start
                    'Bug - Issue No 0002080 (PACRA) When you pull salary slip for one employee, eg. Mpumpu. It shows leave c/f of 60. But when we pull the same report but with another employee selected Its giving leave c/f of 78.
                    'If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < CDate(dsList.Tables(0).Rows(0)("enddate"))) Then
                    If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < CDate(dsList.Tables(0).Rows(i)("enddate"))) Then
                        'Pinkal (08-Mar-2018) -- End
                        mdtEnddate = objEmployee._Termination_To_Date.Date
                    Else
                        'S.SANDEEP [26-AUG-2017] -- START
                        'ISSUE/ENHANCEMENT : GHANA B5 PLUS
                        mdtEnddate = CDate(dsList.Tables(0).Rows(i)("enddate"))
                        'S.SANDEEP [26-AUG-2017] -- END
                    End If

                End If


                'Pinkal (07-Dec-2019) -- Start
                'Defect -   Problem in Considering start date of leave accrue when Leave acrrue start date is not appointed date,reinstatementdate or database start date.
                If mdtStartdate.Date < CDate(dsList.Tables(0).Rows(i)("startdate")).Date Then
                    mdtStartdate = CDate(dsList.Tables(0).Rows(i)("startdate")).Date
                End If
                'Pinkal (07-Dec-2019) -- End



                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                DrRow("Startdate") = mdtStartdate.Date
                DrRow("Enddate") = mdtEnddate.Date

                If xLeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then

                    'Pinkal (06-Oct-2021)-- Start
                    'NMB Issue : Display Leave Balance As on date issue for short leave.

                    'Pinkal (28-Oct-2021)-- Start
                    'Problem in Assigning Leave Accrue Issue.
                    'If Cint(dsList.Tables(0).Rows(i)("daily_amount")) > 0 Then
                    If CDec(dsList.Tables(0).Rows(i)("daily_amount")) > 0 Then
                        'Pinkal (28-Oct-2021)-- End
                    If mdtAsonDate.Date <= mdtEnddate Then
                        DrRow("Accrue_amount") = Math.Round(CDec(dsList.Tables(0).Rows(i)("daily_amount")) * DateDiff(DateInterval.Day, mdtStartdate.Date, mdtAsonDate.Date.AddDays(1)), 2).ToString
                    Else
                        DrRow("Accrue_amount") = Math.Round(CDec(dsList.Tables(0).Rows(i)("daily_amount")) * DateDiff(DateInterval.Day, mdtStartdate.Date, mdtEnddate.AddDays(1)), 2).ToString
                    End If
                        DrRow("TotalAcccrueAmt") = Math.Round(CDec(dsList.Tables(0).Rows(i)("daily_amount")) * DateDiff(DateInterval.Day, mdtStartdate.Date, mdtAsonDate.Date.AddDays(1)), 2).ToString
                    Else
                        DrRow("Accrue_amount") = Math.Round(CDec(dsList.Tables(0).Rows(i)("Accrue_amount")), 2).ToString
                        DrRow("TotalAcccrueAmt") = Math.Round(CDec(dsList.Tables(0).Rows(i)("Accrue_amount")), 2).ToString
                    End If
                    'Pinkal (06-Oct-2021) -- End



                    'Pinkal (23-Jan-2019) -- Start
                    'Bug [Ref #: 0003286 HJFMRI KENYA] - Leave balance figures displaying on Self service not tallying up hence employee unable to apply for leave.
                    'ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                ElseIf xLeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    'Pinkal (23-Jan-2019) -- End

                    Dim xDiff As Integer = 0
                    If mdtAsonDate.Date <= mdtEnddate Then
						'Pinkal (30-Apr-2018) - Start
						'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

                        'Pinkal (18-Jul-2018) -- Start
                        'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.
                        xDiff = DateDiff(DateInterval.Month, mdtStartdate.Date, mdtAsonDate.Date.AddMonths(1))
                        'xDiff = DateDiff(DateInterval.Month, mdtStartdate.Date, mdtAsonDate.Date.AddDays(1))
                        'Pinkal (18-Jul-2018) -- End


						'Pinkal (30-Apr-2018) - End
                        If xLeaveAccrueDaysAfterEachMonth > mdtAsonDate.Date.Day Then
                            xDiff = xDiff - 1
                        End If
                    Else
                        'Pinkal (18-Jul-2018) -- Start
                        'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.
                        'xDiff = DateDiff(DateInterval.Month, mdtStartdate.Date, mdtEnddate.AddDays(1))
                        xDiff = DateDiff(DateInterval.Month, mdtStartdate.Date, mdtEnddate.AddMonths(1))
                        'Pinkal (18-Jul-2018) -- End
                        If xLeaveAccrueDaysAfterEachMonth > mdtEnddate.Date.Day Then
                            xDiff = xDiff - 1
                        End If
                    End If
                    'Pinkal (18-Jul-2018) -- Start
                    'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.
                    If xDiff > 0 Then
                    DrRow("Accrue_amount") = Math.Round(CDec(dsList.Tables(0).Rows(i)("monthly_accrue")) * xDiff, 2).ToString()
                    Else
                        DrRow("Accrue_amount") = 0
                    End If
                    'Pinkal (18-Jul-2018) -- End

                    'Pinkal (09-Aug-2018) -- Start
                    'Bug - Solving Bug for PACRA.

                    xDiff = DateDiff(DateInterval.Month, mdtStartdate.Date, mdtEnddate.AddMonths(1))
                    If xLeaveAccrueDaysAfterEachMonth > mdtEnddate.Date.Day Then
                        xDiff = xDiff - 1
                    End If
                    DrRow("TotalAcccrueAmt") = Math.Round(CDec(dsList.Tables(0).Rows(i)("monthly_accrue")) * xDiff, 2).ToString()
                    'Pinkal (09-Aug-2018) -- End

                End If

                'Pinkal (18-Nov-2016) -- End

                Dim mdtClaimLeaveEncashment As DataTable = Nothing

                Dim objLeaveIssue As New clsleaveissue_Tran
                If Leavetypeunkid.Length > 0 Then

                    Dim arLeaveType As String() = Leavetypeunkid.Split(",")
                    For j As Integer = 0 To arLeaveType.Length - 1
                        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            'Sohail (15 Jan 2019) -- Start
                            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
                            'DrRow("Issue_amount") += objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), intYearId, , , , , xDataOp)


                            'Pinkal (19-Feb-2020) -- Start
                            'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                            If mblnFromLeaveBalanceListReport AndAlso mblnLeaveIssueAsonDateOnLeaveBalanceListReport Then
                                DrRow("Issue_amount") += objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), intYearId, mdtAsonDate, , , , xDataOp, strDatabaseName)
                            Else
                            DrRow("Issue_amount") += objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), intYearId, , , , , xDataOp, strDatabaseName)
                            End If
                            'Pinkal (19-Feb-2020) -- End

                            'Sohail (15 Jan 2019) -- End


                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            'Sohail (15 Jan 2019) -- Start
                            'SIC Enhancement - 76.1 - New Payslip Template 18 same as Template 11 with Calibri fonts and Size 11 and 2 payslip per page option.
                            'DrRow("Issue_amount") += objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), intYearId, , mdtStartdate, mdtEnddate, mdtdbStartDate, xDataOp)


                            'Pinkal (19-Feb-2020) -- Start
                            'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                            If mblnFromLeaveBalanceListReport AndAlso mblnLeaveIssueAsonDateOnLeaveBalanceListReport Then
                                DrRow("Issue_amount") += objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), intYearId, mdtAsonDate, mdtStartdate, mdtEnddate, mdtdbStartDate, xDataOp, strDatabaseName)
                            Else
                            DrRow("Issue_amount") += objLeaveIssue.GetEmployeeIssueDaysCount(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), intYearId, , mdtStartdate, mdtEnddate, mdtdbStartDate, xDataOp, strDatabaseName)
                            End If
                            'Pinkal (19-Feb-2020) -- End

                            'Sohail (15 Jan 2019) -- End

                        End If

                        ' ************************START FOR GETTING LEAVE ENCASHMENT FROM CLAIM MASTER MODULE FOR LEAVE MODULE
                        'Pinkal (18-Aug-2015) -- Start
                        'Enhancement - WORKING ON LEAVE ENCASHMENT DEDUCTION IN LEAVE MODULE.
                        Dim objClaimMst As New clsclaim_request_master
                        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                            'Pinkal (06-Apr-2018) -- Start
                            'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
                            'DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0)

                            'Sohail (03 May 2018) -- Start
                            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                            'Pinkal (17-May-2018) -- Start
                            'bug - Bug Solved for Votlamp due to passing startdate,mdtEnddate
                            'DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0, mdtStartdate, mdtEnddate, Nothing)
                            'Sohail (15 Jan 2019) -- Start
                            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
                            'DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0, , , , xDataOp)
                            DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0, , , , xDataOp, strDatabaseName)
                            'Sohail (15 Jan 2019) -- End
                            'Sohail (03 May 2018) -- End
                             'Pinkal (17-May-2018) -- End

                            'Pinkal (06-Apr-2018) -- End
                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            'Sohail (03 May 2018) -- Start
                            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                            'DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0, mdtStartdate, mdtEnddate, mdtdbStartDate)
                            'Sohail (15 Jan 2019) -- Start
                            'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
                            'DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0, mdtStartdate, mdtEnddate, mdtdbStartDate, xDataOp)
                            DrRow("LeaveEncashment") += objClaimMst.GetLeaveEncashmentForEmployee(intYearId, CInt(dsList.Tables(0).Rows(i)("employeeunkid")), CInt(arLeaveType(j)), 0, mdtStartdate, mdtEnddate, mdtdbStartDate, xDataOp, strDatabaseName)
                            'Sohail (15 Jan 2019) -- End
                            'Sohail (03 May 2018) -- End
                        End If
                        objClaimMst = Nothing
                        'Pinkal (18-Aug-2015) -- End
                        ' ************************END FOR GETTING LEAVE ENCASHMENT FROM CLAIM MASTER MODULE FOR LEAVE MODULE
                    Next

                End If


                If IsDBNull(dsList.Tables(0).Rows(i)("leavebf")) = True Then
                    DrRow("LeaveBF") = 0
                Else
                    DrRow("LeaveBF") = CDec(dsList.Tables(0).Rows(i)("leavebf"))
                End If

                DrRow("Balance") = CDec(dsList.Tables(0).Rows(i)("Accrue_amount")) - CDec(dsList.Tables(0).Rows(i)("Issue_amount"))

                dsBalanceinfo.Tables("Balanceinfo").Rows.Add(DrRow)

                'Pinkal (08-Mar-2018) -- Start
                'Bug - Issue No 0002080 (PACRA) When you pull salary slip for one employee, eg. Mpumpu. It shows leave c/f of 60. But when we pull the same report but with another employee selected Its giving leave c/f of 78.
                dtDatabaseStartDate = Nothing
                dtDatabaseEndDate = Nothing
                'Pinkal (08-Mar-2018) -- End

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLeaveBalanceInfo; Module Name: " & mstrModuleName)
        End Try
        Return dsBalanceinfo
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetImportFileStructure(ByVal blnisShortLeave As Boolean, ByVal xLeaveTenureSetting As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try

            If blnisShortLeave = False Then
                strQ = "SELECT " & _
                 " '' employeecode " & _
                 ", '' employee " & _
                 ", '' leavename " & _
                 ", '' startdate " & _
                             ", '' enddate "


                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

                If xLeaveTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    strQ &= ", '' AS accrue_amount "
                ElseIf xLeaveTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    strQ &= ", '' AS monthly_accrue "
                End If

                'Pinkal (16-Dec-2016) -- End


                strQ &= ", ''Forward_amount " & _
                         ", ''CFamount  " & _
                         ", ''eligibilityafter " & _
                         ", ''isnoaction "
            ElseIf blnisShortLeave Then

                strQ = "SELECT " & _
                       " '' employeecode " & _
                       ", '' employee " & _
                       ", '' leavename " & _
                       ", '' startdate " & _
                       ", '' enddate " & _
                       ", ''accrue_amount " & _
                       ", ''Forward_amount " & _
                       ", ''eligibilityafter " & _
                       ", ''consecutivedays " & _
                       ", ''occurrence " & _
                       ", ''islifetime "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "AccrueLeave")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFileStructure", mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeDates(ByVal intEmployeeId As Integer, ByVal intLeaveTypeUnkid As Integer, ByVal mblnStartdate As Boolean) As Date
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mdtDate As Date = Nothing
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT   " & _
                      " lvleavebalance_tran.startdate as  startdate " & _
                      ",lvleavebalance_tran.enddate as stopdate " & _
                      " FROM lvleavebalance_tran " & _
                      " WHERE   employeeunkid = @employeeunkid  AND leavetypeunkid = @leavetypeunkid AND isopenelc = 1 AND iselc = 1 AND isvoid = 0 "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "AccrueEmployee")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If mblnStartdate Then
                    mdtDate = CDate(dsList.Tables(0).Rows(0)("startdate"))
                Else
                    mdtDate = CDate(dsList.Tables(0).Rows(0)("stopdate"))
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeDates", mstrModuleName)
        End Try
        Return mdtDate
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetNewEmployeeForELC(ByVal intLeaveTypeUnkid As Integer, ByVal intYearId As Integer, ByVal xDatabaseName As String, ByVal mstrEmployeeAsOnDate As String) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrEmployeeID As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.

            'strQ = " SELECT STUFF((SELECT ',' +  CAST(hremployee_master.employeeunkid AS NVARCHAR(50)) " & _
            '         " FROM hremployee_master " & _
            '         " WHERE employeeunkid NOT IN (SELECT  employeeunkid  FROM lvleavebalance_tran WHERE isvoid = 0 AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1) " & _
            '         " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '         " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '         " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '         " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate  " & _
            '         " AND ISNULL(CONVERT(CHAR(8),reinstatement_date,112),@startdate) <=  @startdate AND hremployee_master.isapproved = 1 " & _
            '         " ORDER BY hremployee_master.employeeunkid FOR XML PATH('')),1,1,'') AS EmployeeID "


            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(mstrEmployeeAsOnDate), eZeeDate.convertDate(mstrEmployeeAsOnDate), , , xDatabaseName)


            strQ = " SELECT STUFF((SELECT ',' +  CAST(hremployee_master.employeeunkid AS NVARCHAR(Max)) " & _
                      " FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE employeeunkid NOT IN (SELECT  employeeunkid  FROM lvleavebalance_tran WHERE isvoid = 0 AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1) "

            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry & " "
            End If

            strQ &= " ORDER BY hremployee_master.employeeunkid FOR XML PATH('')),1,1,'') AS EmployeeID "


            'Pinkal (22-Mar-2016) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)

            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mstrEmployeeAsOnDate))
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mstrEmployeeAsOnDate))
            'Pinkal (22-Mar-2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, "AccrueEmployee")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeID = dsList.Tables(0).Rows(0)("EmployeeID").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNewEmployeeForELC", mstrModuleName)
        End Try
        Return mstrEmployeeID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeForDueDateELC(ByVal dtDate As Date, ByVal intLeaveTypeUnkid As Integer, ByVal intYearId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrEmployeeID As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT STUFF(( SELECT  ',' + CAST(employeeunkid AS NVARCHAR(50)) " & _
                      " FROM lvleavebalance_tran " & _
                      " WHERE isvoid = 0  " & _
                      " AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) < @Date AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1 " & _
                      " ORDER BY lvleavebalance_tran.employeeunkid " & _
                      " FOR  XML PATH('')), 1, 1, '') AS EmployeeID "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            dsList = objDataOperation.ExecQuery(strQ, "AccrueEmployee")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeID = dsList.Tables(0).Rows(0)("EmployeeID").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeForDueDateELC", mstrModuleName)
        End Try
        Return mstrEmployeeID
    End Function

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function UpdateBalanceForTerminatedEmployee(ByVal intOptionId As Integer, ByVal dtEmployee As DataTable, Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intYearId As Integer = -1) As Boolean
    '    Dim strQ As String = String.Empty
    '    Dim dsList As DataSet = Nothing
    '    Dim exForce As Exception = Nothing
    '    Try

    '        If dtEmployee Is Nothing Then Return True


    '        If intLeaveBalanceSetting <= 0 Then
    '            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '        End If

    '        If intYearId <= 0 Then
    '            intYearId = FinancialYear._Object._YearUnkid
    '        End If

    '        Dim objEmpoyee As New clsEmployee_Master
    '        Dim objLeaveIssue As New clsleaveissue_Tran
    '        Dim objdataoperation As New clsDataOperation
    '        objdataoperation.BindTransaction()
    '        dtEmployee = New DataView(dtEmployee, "ischeck = true", "", DataViewRowState.CurrentRows).ToTable

    '        For i As Integer = 0 To dtEmployee.Rows.Count - 1

    '            Dim mdclAmount As Decimal = 0
    '            Dim mdtStartDate As Date = Nothing
    '            Dim mdtEndDate As Date = Nothing
    '            objEmpoyee._Employeeunkid = dtEmployee.Rows(i)("employeeunkid")

    '            Select Case intOptionId

    '                Case 1  'SUSPENSION
    '                    mdtStartDate = objEmpoyee._Suspende_To_Date.Date.AddDays(1)

    '                Case 2  'PROBATION
    '                    mdtStartDate = objEmpoyee._Probation_To_Date.Date.AddDays(1)

    '                Case 3  'END OF CONTRACT
    '                    mdtStartDate = objEmpoyee._Empl_Enddate.Date.AddDays(1)

    '                Case 4  'TERMITATED
    '                    mdtStartDate = objEmpoyee._Termination_From_Date.Date.AddDays(1)

    '                Case 5  'RETIREMENT
    '                    mdtStartDate = objEmpoyee._Termination_To_Date.Date.AddDays(1)

    '            End Select

    '            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                dsList = GetList("Accrue", True, False, objEmpoyee._Employeeunkid, False, False)
    '            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                dsList = GetList("Accrue", True, False, objEmpoyee._Employeeunkid, True, True)
    '            End If

    '            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "ispaid  = 1 AND yearunkid = " & intYearId, "", DataViewRowState.CurrentRows).ToTable
    '            Dim mintLeavebalanceId As Integer = -1
    '            For j As Integer = 0 To dtTable.Rows.Count - 1

    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

    '                    If IsDBNull(dtTable.Rows(j)("enddate")) Then
    '                        mdtEndDate = FinancialYear._Object._Database_End_Date.Date
    '                    Else
    '                        mdtEndDate = CDate(dtTable.Rows(j)("enddate")).Date
    '                    End If

    '                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    mdtEndDate = CDate(dtTable.Rows(j)("enddate")).Date
    '                End If

    '                Dim intDiff As Integer = CInt(DateDiff(DateInterval.Day, mdtStartDate, mdtEndDate))
    '                Dim mdcldeductedAmt As Decimal = CDec(dtTable.Rows(j)("daily_amount")) * intDiff
    '                Dim mdclIssueAmt As Decimal = objLeaveIssue.GetTerminatedEmployeeIssueDaysCount(CInt(dtTable.Rows(j)("employeeunkid")), CInt(dtTable.Rows(j)("leavetypeunkid")), mdtStartDate)  'FUTURE DATE ISSUED SUM AFTER TERMINATE


    '                strQ = " UPDATE lvleavebalance_tran SET " & _
    '                          " remaining_bal = (remaining_bal + @IssueAmt) - @DeductedAmt " & _
    '                          " ,issue_amount = issue_amount - @IssueAmt  " & _
    '                          ", uptolstyr_issueamt = uptolstyr_issueamt - @IssueAmt "

    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    strQ &= ", Isopenelc = 0 "
    '                End If

    '                strQ &= " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 AND yearunkid = " & intYearId

    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    strQ &= " AND  Isopenelc = 1 "
    '                End If


    '                objdataoperation.ClearParameters()
    '                objdataoperation.AddParameter("@DeductedAmt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcldeductedAmt)
    '                objdataoperation.AddParameter("@IssueAmt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclIssueAmt)
    '                objdataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(j)("employeeunkid")))
    '                objdataoperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(j)("leavetypeunkid")))
    '                objdataoperation.ExecNonQuery(strQ)

    '                If objdataoperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                _LeaveBalanceunkid = CInt(dtTable.Rows(j)("leavebalanceunkid"))

    '                If InsertAudiTrailForLeaveBalance(objdataoperation, 2) = False Then
    '                    exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
    '                    Throw exForce 
    '                End If

    '            Next

    '        Next

    '        objdataoperation.ReleaseTransaction(True)
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        DisplayError.Show("-1", ex.Message, "UpdateBalanceForTerminatedEmployee", mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    'Public Function GetELC_Employee(ByVal intOptionId As Integer, ByVal dtDateAsOn As Date, Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intEmployeeId As Integer = -1) As DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim dtTable As DataTable
    '    Try

    '        If intLeaveBalanceSetting <= 0 Then
    '            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '        End If

    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT DISTINCT " & _
    '               "     hremployee_master.employeeunkid " & _
    '               "    ,ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '               "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '               "    ,ISNULL(hrjob_master.job_name,'') AS Job " & _
    '               "    ,CONVERT(CHAR(8),startdate,112) AS StDate " & _
    '               "    ,CONVERT(CHAR(8),enddate,112) AS EdDate " & _
    '               "FROM lvleavebalance_tran " & _
    '               "    JOIN hremployee_master ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '               "    JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '               "WHERE isvoid = 0 "

    '        If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " AND iselc = 1 AND isopenelc = 1 "
    '        End If

    '        Select Case intOptionId
    '            Case 1  'SUSPENSION
    '                StrQ &= " AND CONVERT(CHAR(8),suspended_to_date,112) < @Date "
    '            Case 2  'PROBATION
    '                StrQ &= " AND CONVERT(CHAR(8),probation_to_date,112) < @Date "
    '            Case 3  'END OF CONTRACT
    '                StrQ &= " AND CONVERT(CHAR(8),empl_enddate,112) < @Date "
    '            Case 4  'TERMITATED
    '                StrQ &= " AND CONVERT(CHAR(8),termination_from_date,112) < @Date "
    '            Case 5  'RETIREMENT
    '                StrQ &= " AND CONVERT(CHAR(8),termination_to_date,112) < @Date "
    '        End Select


    '        objDataOperation.ClearParameters()

    '        If intEmployeeId > 0 Then
    '            StrQ &= " AND lvleavebalance_tran.employeeunkid = @employeeunkid"
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
    '        End If

    '        objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateAsOn))

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        dtTable = New DataTable("ELC")

    '        dtTable.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
    '        dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
    '        dtTable.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
    '        dtTable.Columns.Add("ename", System.Type.GetType("System.String")).DefaultValue = ""
    '        dtTable.Columns.Add("ejob", System.Type.GetType("System.String")).DefaultValue = ""
    '        dtTable.Columns.Add("sdate", System.Type.GetType("System.String")).DefaultValue = ""
    '        dtTable.Columns.Add("edate", System.Type.GetType("System.String")).DefaultValue = ""

    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            For Each dRow As DataRow In dsList.Tables(0).Rows
    '                Dim dtRow As DataRow = dtTable.NewRow

    '                dtRow.Item("ischeck") = False
    '                dtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
    '                dtRow.Item("ecode") = dRow.Item("ECode")
    '                dtRow.Item("ename") = dRow.Item("EName")
    '                dtRow.Item("ejob") = dRow.Item("Job")
    '                dtRow.Item("sdate") = eZeeDate.convertDate(dRow.Item("StDate").ToString).ToShortDateString
    '                dtRow.Item("edate") = eZeeDate.convertDate(dRow.Item("EdDate").ToString).ToShortDateString

    '                dtTable.Rows.Add(dtRow)
    '            Next
    '        End If

    '        Return dtTable

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetELC_Employee", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateBalanceForTerminatedEmployee(ByVal intOptionId As Integer, ByVal dtEmployee As DataTable _
                                                                                     , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                     , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True _
                                                                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                                     , Optional ByVal intEmpId As Integer = -1, Optional ByVal mstrFilter As String = "" _
                                                                                     , Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception = Nothing
        Try

            If dtEmployee Is Nothing Then Return True


            If intLeaveBalanceSetting <= 0 Then
                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If

            Dim objEmpoyee As New clsEmployee_Master
            Dim objLeaveIssue As New clsleaveissue_Tran
            Dim objdataoperation As New clsDataOperation
            objdataoperation.BindTransaction()
            dtEmployee = New DataView(dtEmployee, "ischeck = true", "", DataViewRowState.CurrentRows).ToTable

            For i As Integer = 0 To dtEmployee.Rows.Count - 1

                Dim mdclAmount As Decimal = 0
                Dim mdtStartDate As Date = Nothing
                Dim mdtEndDate As Date = Nothing

                objEmpoyee._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)) = dtEmployee.Rows(i)("employeeunkid")


                Select Case intOptionId

                    Case 1  'SUSPENSION
                        mdtStartDate = objEmpoyee._Suspende_To_Date.Date.AddDays(1)

                    Case 2  'PROBATION
                        mdtStartDate = objEmpoyee._Probation_To_Date.Date.AddDays(1)

                    Case 3  'END OF CONTRACT
                        mdtStartDate = objEmpoyee._Empl_Enddate.Date.AddDays(1)

                    Case 4  'TERMITATED
                        mdtStartDate = objEmpoyee._Termination_From_Date.Date.AddDays(1)

                    Case 5  'RETIREMENT
                        mdtStartDate = objEmpoyee._Termination_To_Date.Date.AddDays(1)

                End Select


                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    dsList = GetList("Accrue", True, False, objEmpoyee._Employeeunkid, False, False)
                'ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    dsList = GetList("Accrue", True, False, objEmpoyee._Employeeunkid, True, True)
                'End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    dsList = GetList("Accrue", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                '                           , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                '                           , blnApplyUserAccessFilter, False, objEmpoyee._Employeeunkid, False, False, False, "", objdataoperation)
                'ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    dsList = GetList("Accrue", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                '                          , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                '                          , blnApplyUserAccessFilter, False, objEmpoyee._Employeeunkid, True, True, False, "", objdataoperation)
                'End If

                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    dsList = GetList("Accrue", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                           , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                           , blnApplyUserAccessFilter, False, dtEmployee.Rows(i)("employeeunkid"), False, False, False, "", objdataoperation)
                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    dsList = GetList("Accrue", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                          , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                          , blnApplyUserAccessFilter, False, dtEmployee.Rows(i)("employeeunkid"), True, True, False, "", objdataoperation)
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                'Pinkal (24-Aug-2015) -- End



                Dim dtTable As DataTable = New DataView(dsList.Tables(0), "ispaid  = 1 AND yearunkid = " & xYearUnkid, "", DataViewRowState.CurrentRows).ToTable
                Dim mintLeavebalanceId As Integer = -1
                For j As Integer = 0 To dtTable.Rows.Count - 1

                    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                        If IsDBNull(dtTable.Rows(j)("enddate")) Then
                            mdtEndDate = FinancialYear._Object._Database_End_Date.Date
                        Else
                            mdtEndDate = CDate(dtTable.Rows(j)("enddate")).Date
                        End If

                    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        mdtEndDate = CDate(dtTable.Rows(j)("enddate")).Date
                    End If

                    Dim intDiff As Integer = CInt(DateDiff(DateInterval.Day, mdtStartDate, mdtEndDate))
                    Dim mdcldeductedAmt As Decimal = CDec(dtTable.Rows(j)("daily_amount")) * intDiff
                    Dim mdclIssueAmt As Decimal = objLeaveIssue.GetTerminatedEmployeeIssueDaysCount(CInt(dtTable.Rows(j)("employeeunkid")), CInt(dtTable.Rows(j)("leavetypeunkid")), mdtStartDate)  'FUTURE DATE ISSUED SUM AFTER TERMINATE


                    strQ = " UPDATE lvleavebalance_tran SET " & _
                              " remaining_bal = (remaining_bal + @IssueAmt) - @DeductedAmt " & _
                              " ,issue_amount = issue_amount - @IssueAmt  " & _
                              ", uptolstyr_issueamt = uptolstyr_issueamt - @IssueAmt "

                    If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= ", Isopenelc = 0 "
                    End If

                    strQ &= " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 AND yearunkid = " & xYearUnkid

                    If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= " AND  Isopenelc = 1 "
                    End If


                    objdataoperation.ClearParameters()
                    objdataoperation.AddParameter("@DeductedAmt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcldeductedAmt)
                    objdataoperation.AddParameter("@IssueAmt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclIssueAmt)
                    objdataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(j)("employeeunkid")))
                    objdataoperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(j)("leavetypeunkid")))
                    objdataoperation.ExecNonQuery(strQ)

                    If objdataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
                        Throw exForce
                    End If

                    _LeaveBalanceunkid = CInt(dtTable.Rows(j)("leavebalanceunkid"))
                    _FormName = mstrFormName
                    _LoginEmployeeUnkid = mintLogEmployeeUnkid
                    _ClientIP = mstrClientIP
                    _HostName = mstrHostName
                    _FromWeb = mblnIsWeb
                    _AuditUserId = mintAuditUserId
                    _AuditDate = mdtAuditDate
                    If InsertAudiTrailForLeaveBalance(objdataoperation, 2) = False Then
                        exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            Next

            objdataoperation.ReleaseTransaction(True)
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "UpdateBalanceForTerminatedEmployee", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetELC_Employee(ByVal intOptionId As Integer, ByVal dtDateAsOn As Date, Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intEmployeeId As Integer = -1) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Try

            objDataOperation = New clsDataOperation

            StrQ = "SELECT DISTINCT " & _
                   "     hremployee_master.employeeunkid " & _
                   "    ,ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                   "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                   "    ,ISNULL(hrjob_master.job_name,'') AS Job " & _
                   "    ,CONVERT(CHAR(8),startdate,112) AS StDate " & _
                   "    ,CONVERT(CHAR(8),enddate,112) AS EdDate " & _
                   "FROM lvleavebalance_tran " & _
                   "    JOIN hremployee_master ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "  JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid "

            Select Case intOptionId
                Case 1  'SUSPENSION
                    StrQ &= "LEFT JOIN " & _
                                 " ( " & _
                                 "    SELECT " & _
                                 "         date2 " & _
                                 "        ,employeeunkid " & _
                                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "    FROM hremployee_dates_tran " & _
                                 "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' " & _
                                 " )AS SuspDt ON SuspDt.employeeunkid = hremployee_master.employeeunkid AND SuspDt.rno = 1 "

                Case 2  'PROBATION
                    StrQ &= " LEFT JOIN " & _
                                 " ( " & _
                                 "    SELECT " & _
                                 "        date2 " & _
                                 "        ,employeeunkid " & _
                                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "    FROM hremployee_dates_tran " & _
                                 "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' " & _
                                 " )AS PrbDt ON PrbDt.employeeunkid = hremployee_master.employeeunkid AND PrbDt.rno = 1 "

                Case 3  'END OF CONTRACT
                    StrQ &= " LEFT JOIN " & _
                                 " ( " & _
                                 "    SELECT " & _
                                 "         date1 " & _
                                 "        ,employeeunkid " & _
                                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "    FROM hremployee_dates_tran " & _
                                 "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' " & _
                                 " )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 "

                Case 4  'TERMITATED

                    StrQ &= " LEFT JOIN " & _
                              " ( " & _
                              "    SELECT " & _
                              "         date2 " & _
                              "        ,employeeunkid " & _
                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                              "    FROM hremployee_dates_tran " & _
                              "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' " & _
                              " )AS TerDt ON TerDt.employeeunkid = hremployee_master.employeeunkid AND TerDt.rno = 1 "

                Case 5  'RETIREMENT

                    StrQ &= " LEFT JOIN " & _
                            " ( " & _
                            "    SELECT " & _
                            "         date1 " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "    FROM hremployee_dates_tran " & _
                            "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtDateAsOn) & "' " & _
                            " )AS RtrDt ON RtrDt.employeeunkid = hremployee_master.employeeunkid AND RtrDt.rno = 1 "
            End Select


            StrQ &= "WHERE isvoid = 0 "

            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= " AND iselc = 1 AND isopenelc = 1 "
            End If

            Select Case intOptionId
                Case 1  'SUSPENSION
                    StrQ &= " AND CONVERT(CHAR(8),SuspDt.date2,112) < @Date "
                Case 2  'PROBATION
                    StrQ &= " AND CONVERT(CHAR(8),PrbDt.date2,112) < @Date "
                Case 3  'END OF CONTRACT
                    StrQ &= " AND CONVERT(CHAR(8),EocDt.date1,112) < @Date "
                Case 4  'TERMITATED
                    StrQ &= " AND CONVERT(CHAR(8),TerDt.date2,112) < @Date "
                Case 5  'RETIREMENT
                    StrQ &= " AND CONVERT(CHAR(8),RtrDt.date1,112) < @Date "
            End Select


            objDataOperation.ClearParameters()

            If intEmployeeId > 0 Then
                StrQ &= " AND lvleavebalance_tran.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateAsOn))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = New DataTable("ELC")

            dtTable.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("ename", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("ejob", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("sdate", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("edate", System.Type.GetType("System.String")).DefaultValue = ""

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dtRow As DataRow = dtTable.NewRow

                    dtRow.Item("ischeck") = False
                    dtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                    dtRow.Item("ecode") = dRow.Item("ECode")
                    dtRow.Item("ename") = dRow.Item("EName")
                    dtRow.Item("ejob") = dRow.Item("Job")
                    dtRow.Item("sdate") = eZeeDate.convertDate(dRow.Item("StDate").ToString).ToShortDateString
                    dtRow.Item("edate") = eZeeDate.convertDate(dRow.Item("EdDate").ToString).ToShortDateString

                    dtTable.Rows.Add(dtRow)
                Next
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetELC_Employee", mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeBalanceForLeaveType(ByVal intEmployeeId As Integer, ByVal intLeaveTypeId As Integer, ByVal intYearId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing
        Try

            objDataOperation = New clsDataOperation
            StrQ = " SELECT * FROM lvleavebalance_tran WHERE employeeunkid = " & intEmployeeId & " AND leavetypeunkid = " & intLeaveTypeId & " AND yearunkid = " & intYearId & " AND isvoid = 0 "
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                dtTable = dsList.Tables(0).Clone
                If dsList.Tables(0).Rows.Count > 0 Then
                    dtTable = dsList.Tables(0)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeBalanceForLeaveType", mstrModuleName)
        End Try
        Return dtTable
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function GetLeaveFreq_Employee(ByVal iEmpId As Integer, ByVal iLeaveId As Integer, ByVal iStrFilter As String, _
    '                                      Optional ByVal mIsCloseFy As Boolean = False, _
    '                                      Optional ByVal mIsELC As Boolean = False, _
    '                                      Optional ByVal mIsOpenELC As Boolean = False, _
    '                                      Optional ByVal iUserAccessFilter As String = "") As DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim objDataOperation = New clsDataOperation
    '    Dim dsList As New DataSet
    '    Dim dtTable As DataTable = Nothing
    '    Try
    '        StrQ = "SELECT " & _
    '               "	 lvleavebalance_tran.leavebalanceunkid " & _
    '               "	,lvleavebalance_tran.yearunkid " & _
    '               "	,lvleavebalance_tran.employeeunkid " & _
    '               "	,lvleavebalance_tran.leavetypeunkid " & _
    '               "	,lvleavebalance_tran.batchunkid " & _
    '               "	,lvleavebalance_tran.startdate " & _
    '               "	,lvleavebalance_tran.enddate " & _
    '               "	,lvleavebalance_tran.accrue_amount " & _
    '               "	,lvleavebalance_tran.issue_amount " & _
    '               "	,lvleavebalance_tran.daily_amount " & _
    '               "	,lvleavebalance_tran.balance " & _
    '               "	,lvleavebalance_tran.remaining_bal " & _
    '               "	,lvleavebalance_tran.uptolstyr_accrueamt " & _
    '               "	,lvleavebalance_tran.uptolstyr_issueamt " & _
    '               "	,lvleavebalance_tran.ispaid " & _
    '               "	,lvleavebalance_tran.userunkid " & _
    '               "	,lvleavebalance_tran.isvoid " & _
    '               "	,lvleavebalance_tran.voiduserunkid " & _
    '               "	,lvleavebalance_tran.voiddatetime " & _
    '               "	,lvleavebalance_tran.voidreason " & _
    '               "	,lvleavebalance_tran.accruesetting " & _
    '               "	,lvleavebalance_tran.days " & _
    '               "	,lvleavebalance_tran.leavebf " & _
    '               "	,lvleavebalance_tran.isshortleave " & _
    '               "	,lvleavebalance_tran.actualamount " & _
    '               "	,lvleavebalance_tran.isopenelc " & _
    '               "	,lvleavebalance_tran.iselc " & _
    '               "	,lvleavebalance_tran.isclose_fy " & _
    '               "	,lvleavebalance_tran.cfamount " & _
    '               "	,lvleavebalance_tran.eligibilityafter " & _
    '               "	,lvleavebalance_tran.isnoaction " & _
    '               "	,lvleavebalance_tran.consecutivedays " & _
    '               "	,lvleavebalance_tran.occurrence " & _
    '               "	,lvleavebalance_tran.optiondid " & _
    '               "	,lvleavebalance_tran.remaining_occurrence " & _
    '               "	,@Code +' : ' + employeecode + ' - ' + @Name +' : ' + firstname + ' '+ surname AS EName " & _
    '               "	,lvleavetype_master.leavename AS LeaveName " & _
    '               "	,CASE optiondid WHEN 1 THEN @Lifetime WHEN 2 THEN @FinancialYear WHEN 3 THEN @ELC END AS LvFreq " & _
    '               "    ,firstname + ' '+ surname AS emp " & _
    '               "FROM lvleavebalance_tran " & _
    '               "	JOIN lvleavetype_master ON lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
    '               "	JOIN hremployee_master ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '               "WHERE isvoid = 0 AND lvleavebalance_tran.isshortleave = 1 "

    '        If mIsELC Then
    '            If mIsOpenELC Then
    '                StrQ &= " AND lvleavebalance_tran.isopenelc = 1"
    '            Else
    '                StrQ &= " AND lvleavebalance_tran.isopenelc = 0"
    '            End If
    '        End If

    '        If mIsCloseFy Then
    '            StrQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 1 "
    '        Else
    '            StrQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 0 "
    '        End If

    '        If iUserAccessFilter = "" Then
    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                iUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '            End If
    '        End If

    '        StrQ &= iUserAccessFilter

    '        If iEmpId > 0 Then
    '            StrQ &= " AND lvleavebalance_tran.employeeunkid = '" & iEmpId & "' "
    '        End If

    '        If iLeaveId > 0 Then
    '            StrQ &= " AND lvleavebalance_tran.leavetypeunkid = '" & iLeaveId & "' "
    '        End If

    '        If iStrFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & iStrFilter
    '        End If

    '        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Code"))
    '        objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Name"))
    '        objDataOperation.AddParameter("@Lifetime", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Life Time"))
    '        objDataOperation.AddParameter("@FinancialYear", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Financial Year"))
    '        objDataOperation.AddParameter("@ELC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Employee Leave Cycle"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        dtTable = dsList.Tables(0).Copy

    '        Return dtTable
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetLeaveFreq_Employee", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveFreq_Employee(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal iEmpId As Integer, ByVal iLeaveId As Integer _
                                                                , Optional ByVal blnOnlyActive As Boolean = True _
                                                                , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                , Optional ByVal mIsCloseFy As Boolean = False _
                                                                , Optional ByVal mIsELC As Boolean = False _
                                                                , Optional ByVal mIsOpenELC As Boolean = False _
                                                                , Optional ByVal iStrFilter As String = "") As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation = New clsDataOperation
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

        Try
            StrQ = "SELECT " & _
                   "	 lvleavebalance_tran.leavebalanceunkid " & _
                   "	,lvleavebalance_tran.yearunkid " & _
                   "	,lvleavebalance_tran.employeeunkid " & _
                   "	,lvleavebalance_tran.leavetypeunkid " & _
                   "	,lvleavebalance_tran.batchunkid " & _
                   "	,lvleavebalance_tran.startdate " & _
                   "	,lvleavebalance_tran.enddate " & _
                   "	,lvleavebalance_tran.accrue_amount " & _
                   "	,lvleavebalance_tran.issue_amount " & _
                   "	,lvleavebalance_tran.daily_amount " & _
                   "	,lvleavebalance_tran.balance " & _
                   "	,lvleavebalance_tran.remaining_bal " & _
                   "	,lvleavebalance_tran.uptolstyr_accrueamt " & _
                   "	,lvleavebalance_tran.uptolstyr_issueamt " & _
                   "	,lvleavebalance_tran.ispaid " & _
                   "	,lvleavebalance_tran.userunkid " & _
                   "	,lvleavebalance_tran.isvoid " & _
                   "	,lvleavebalance_tran.voiduserunkid " & _
                   "	,lvleavebalance_tran.voiddatetime " & _
                   "	,lvleavebalance_tran.voidreason " & _
                   "	,lvleavebalance_tran.accruesetting " & _
                   "	,lvleavebalance_tran.days " & _
                   "	,lvleavebalance_tran.leavebf " & _
                   "	,lvleavebalance_tran.isshortleave " & _
                   "	,lvleavebalance_tran.actualamount " & _
                   "	,lvleavebalance_tran.isopenelc " & _
                   "	,lvleavebalance_tran.iselc " & _
                   "	,lvleavebalance_tran.isclose_fy " & _
                   "	,lvleavebalance_tran.cfamount " & _
                   "	,lvleavebalance_tran.eligibilityafter " & _
                   "	,lvleavebalance_tran.isnoaction " & _
                   "	,lvleavebalance_tran.consecutivedays " & _
                   "	,lvleavebalance_tran.occurrence " & _
                   "	,lvleavebalance_tran.optiondid " & _
                   "	,lvleavebalance_tran.remaining_occurrence " & _
                   "	,@Code +' : ' + employeecode + ' - ' + @Name +' : ' + firstname + ' '+ surname AS EName " & _
                   "	,lvleavetype_master.leavename AS LeaveName " & _
                   "	,CASE optiondid WHEN 1 THEN @Lifetime WHEN 2 THEN @FinancialYear WHEN 3 THEN @ELC END AS LvFreq " & _
                   "    ,firstname + ' '+ surname AS emp " & _
                   "    ,ISNULL(occ_tenure,0) AS occ_tenure " & _
                   " FROM lvleavebalance_tran " & _
                   "	JOIN lvleavetype_master ON lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                   "	JOIN hremployee_master ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid "

            'Pinkal (13-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.["    ,ISNULL(occ_tenure,0) AS occ_tenure " & _]


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END



            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= "WHERE isvoid = 0 AND lvleavebalance_tran.isshortleave = 1 "

            If mIsELC Then
                If mIsOpenELC Then
                    StrQ &= " AND lvleavebalance_tran.isopenelc = 1"
                Else
                    StrQ &= " AND lvleavebalance_tran.isopenelc = 0"
                End If
            End If

            If mIsCloseFy Then
                StrQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 1 "
            Else
                StrQ &= " AND ISNULL(lvleavebalance_tran.isclose_fy,0) = 0 "
            End If


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'If iUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        iUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            '    End If
            'End If
            'StrQ &= iUserAccessFilter


            'S.SANDEEP [15 NOV 2016] -- START
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry & " "
            '    End If
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If iStrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & iStrFilter
            End If

            'Pinkal (24-Aug-2015) -- End


            If iEmpId > 0 Then
                StrQ &= " AND lvleavebalance_tran.employeeunkid = '" & iEmpId & "' "
            End If

            If iLeaveId > 0 Then
                StrQ &= " AND lvleavebalance_tran.leavetypeunkid = '" & iLeaveId & "' "
            End If

            If iStrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & iStrFilter
            End If

            objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Code"))
            objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Name"))
            objDataOperation.AddParameter("@Lifetime", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Life Time"))
            objDataOperation.AddParameter("@FinancialYear", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Financial Year"))
            objDataOperation.AddParameter("@ELC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Employee Leave Cycle"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0).Copy

            Return dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveFreq_Employee", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetAssignedEmpIds(ByVal iLeaveTypeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim iValue As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDataOperation As New clsDataOperation
                StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CAST(employeeunkid AS NVARCHAR(50)) FROM lvleavebalance_tran WHERE isvoid = 0 AND leavetypeunkid = '" & iLeaveTypeId & "' FOR XML PATH('')),1,1,''),'') AS iEmp "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iValue = dsList.Tables(0).Rows(0).Item("iEmp")
                End If
            End Using
            Return iValue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAssignedEmpIds", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmpRemainingBalForDueDateELC(ByVal intYearId As Integer, ByVal intEmployeeId As Integer, ByVal intLeaveTypeUnkid As Integer, ByVal dtDate As Date) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mdecRemainingBal As Decimal = 0
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT ISNULL(accrue_amount,0) - ISNULL(issue_amount,0) AS Remaining_Bal " & _
                      " FROM lvleavebalance_tran " & _
                      " WHERE isvoid = 0  " & _
                      " AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) <= @Date AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isopenelc = 1 AND iselc = 1 " & _
                      " AND employeeunkid = @employeeunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            dsList = objDataOperation.ExecQuery(strQ, "EmployeeBalance")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecRemainingBal = CDec(dsList.Tables(0).Rows(0)("Remaining_Bal").ToString())
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpRemainingBalForDueDateELC; Module Name: " & mstrModuleName)
        End Try
        Return mdecRemainingBal
    End Function

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function GetEmployeeLeaveDataForAdjustment(ByVal intLeaveBalanceSetting As Integer, ByVal intYearId As Integer, _
    '                                                  ByVal intLeaveTypeUnkid As Integer, ByVal dtDate As Date, Optional ByVal mstrFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim objDataOperation As New clsDataOperation
    '    Try


    '        strQ = " SELECT lvleavebalance_tran.leavebalanceunkid " & _
    '               ",Cast(0 AS Bit) AS ischeck " & _
    '               ",lvleavebalance_tran.employeeunkid " & _
    '               ",hremployee_master.employeecode " & _
    '               ",ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
    '               ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '               ",hremployee_master.appointeddate " & _
    '               ",hrjob_master.job_name AS Job " & _
    '               ",CAST(ISNULL(lvleavebalance_tran.leavebf,0) AS decimal(10,2)) AS leavebf " & _
    '               ",CAST(ISNULL(daily_amount,0) * (DATEDIFF(DAY,startdate,CAST(@Asondate AS DATETIME))) AS decimal(10,2)) AS AccrueAsOnDate" & _
    '               ",CAST(ISNULL(lvleavebalance_tran.issue_amount,0) AS decimal(10,2)) AS Issue_amount" & _
    '               ",CAST(ISNULL(lvleavebalance_tran.Remaining_Bal,0) AS decimal(10,2)) AS Remaining_Bal" & _
    '               " FROM lvleavebalance_tran " & _
    '               " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleavebalance_tran.employeeunkid " & _
    '               " JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '               " WHERE isvoid = 0 AND lvleavebalance_tran.leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid "

    '        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            strQ &= " AND ISNULL(CONVERT(CHAR(8),lvleavebalance_tran.enddate,112)," & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & ") >= @Asondate  " & _
    '                        " AND isclose_fy = 0 "
    '        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            strQ &= " AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) >= @Asondate " & _
    '                    " AND isopenelc = 1 AND iselc = 1 "
    '        End If

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= "AND " & mstrFilter
    '        End If

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@LeaveTypeUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkid)
    '        objDataOperation.AddParameter("@Asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
    '        dsList = objDataOperation.ExecQuery(strQ, "EmployeeBalance")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveDataForAdjustment; Module Name: " & mstrModuleName)
    '    End Try
    '    Return dsList
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeLeaveDataForAdjustment(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                  , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                  , ByVal intLeaveBalanceSetting As Integer, ByVal intLeaveTypeUnkid As Integer, ByVal dtDate As Date _
                                                                                  , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                                                  , Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            strQ = " SELECT lvleavebalance_tran.leavebalanceunkid " & _
                   ",Cast(0 AS Bit) AS ischeck " & _
                   ",lvleavebalance_tran.employeeunkid " & _
                   ",hremployee_master.employeecode " & _
                   ",ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                   ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpName " & _
                   ",hremployee_master.appointeddate " & _
                   ",hrjob_master.job_name AS Job " & _
                   ",CAST(ISNULL(lvleavebalance_tran.leavebf,0) AS decimal(10,2)) AS leavebf " & _
                   ",CAST(ISNULL(daily_amount,0) * (DATEDIFF(DAY,startdate,CAST(@Asondate AS DATETIME))) AS decimal(10,2)) AS AccrueAsOnDate" & _
                   ",CAST(ISNULL(lvleavebalance_tran.issue_amount,0) AS decimal(10,2)) AS Issue_amount" & _
                   ",CAST(ISNULL(lvleavebalance_tran.Remaining_Bal,0) AS decimal(10,2)) AS Remaining_Bal" & _
                       ", lvleavebalance_tran.startdate " & _
                       ", lvleavebalance_tran.enddate " & _
                       ", 0.00 as Encashment " & _
                       ", lvleavebalance_tran.isopenelc as isopenelc " & _
                       ", lvleavebalance_tran.iselc as iselc " & _
                       ", lvleavebalance_tran.isclose_fy as isclose_fy " & _
                       ", 0.00 as Balance_amt " & _
                   ", 0.00 as Leavebf_Adjustment " & _
                   " FROM lvleavebalance_tran " & _
                   " JOIN hremployee_master ON hremployee_master.employeeunkid = lvleavebalance_tran.employeeunkid " & _
                   " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                   " JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid "

            'Pinkal (08-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", 0.00 as Leavebf_Adjustment " & _]

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry & " "
                End If
            End If


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry & " "
            End If

            strQ &= " WHERE isvoid = 0 AND lvleavebalance_tran.leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid "

            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                strQ &= " AND ISNULL(CONVERT(CHAR(8),lvleavebalance_tran.enddate,112)," & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & ") >= @Asondate  " & _
                            " AND isclose_fy = 0 "
            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " AND CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) >= @Asondate " & _
                        " AND isopenelc = 1 AND iselc = 1 "
            End If
            If mstrFilter.Trim.Length > 0 Then
                strQ &= "AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearUnkid)
            objDataOperation.AddParameter("@LeaveTypeUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkid)
            objDataOperation.AddParameter("@Asondate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            dsList = objDataOperation.ExecQuery(strQ, "EmployeeBalance")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveDataForAdjustment; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (24-Aug-2015) -- End



    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    Public Function GetEmployeeIDsWithoutAccrue(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                  , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                  , ByVal intLeaveBalanceSetting As Integer, ByVal intLeaveTypeUnkid As Integer _
                                                                                  , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal mstrFilter As String = "") As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim mstrEmployeeIDs As String = ""
        Try

            'Pinkal (29-Jan-2019) -- Start
            'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry, xDateJoinQry, xDateFilterQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = "" : xDateJoinQry = "" : xDateFilterQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , False)


            strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CAST(hremployee_master.employeeunkid AS NVARCHAR(MAX)) " & _
                      " FROM hremployee_master "

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry

                strQ &= "LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "    	 CNF.CEmpId " & _
                                    "    	,CNF.confirmation_date " & _
                                    "       ,CNF.CF_REASON " & _
                                    "       ,CNF.CEfDt " & _
                                    "    FROM " & _
                                    "    ( " & _
                                    "        SELECT " & _
                                    "             CNF.employeeunkid AS CEmpId " & _
                                    "            ,CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                                    "            ,CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                                    "            ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                                    "            ,CASE WHEN CNF.rehiretranunkid > 0 THEN ISNULL(CTE.name,'') ELSE ISNULL(CEC.name,'') END AS CF_REASON " & _
                                    "            ,CNF.changereasonunkid " & _
                                    "        FROM hremployee_dates_tran AS CNF " & _
                                    "            LEFT JOIN cfcommon_master AS CTE ON CTE.masterunkid = CNF.changereasonunkid AND CTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                    "            LEFT JOIN cfcommon_master AS CEC ON CEC.masterunkid = CNF.changereasonunkid AND CEC.mastertype = '" & clsCommon_Master.enCommonMaster.CONFIRMATION & "' " & _
                                    "        WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                    "    ) AS CNF WHERE CNF.Rno = 1 " & _
                                    ") AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                                   "LEFT JOIN " & _
                                    "( " & _
                                    "   SELECT " & _
                                    "		 PROB.PDEmpId " & _
                                    "		,PROB.probation_from_date " & _
                                    "		,PROB.probation_to_date " & _
                                    "		,PROB.PDEfDt " & _
                                    "		,PROB.PB_REASON " & _
                                    "   FROM " & _
                                    "   ( " & _
                                    "       SELECT " & _
                                    "            PDT.employeeunkid AS PDEmpId " & _
                                    "           ,CONVERT(CHAR(8),PDT.date1,112) AS probation_from_date " & _
                                    "           ,CONVERT(CHAR(8),PDT.date2,112) AS probation_to_date " & _
                                    "           ,CONVERT(CHAR(8),PDT.effectivedate,112) AS PDEfDt " & _
                                    "           ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                                    "			,CASE WHEN PDT.rehiretranunkid > 0 THEN ISNULL(RPB.name,'') ELSE ISNULL(PBC.name,'') END AS PB_REASON " & _
                                    "       FROM hremployee_dates_tran AS PDT " & _
                                    "			LEFT JOIN cfcommon_master AS RPB ON RPB.masterunkid = PDT.changereasonunkid AND RPB.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                                    "			LEFT JOIN cfcommon_master AS PBC ON PBC.masterunkid = PDT.changereasonunkid AND PBC.mastertype = '" & clsCommon_Master.enCommonMaster.PROBATION & "' " & _
                                    "		WHERE isvoid = 0 AND PDT.datetypeunkid = 1 AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                    "   ) AS PROB WHERE PROB.Rno = 1 " & _
                                    ") AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid AND EPROB.PDEfDt >= CONVERT(CHAR(8),appointeddate,112) "
            End If

            strQ &= " WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid from lvleavebalance_tran WHERE isvoid = 0 AND yearunkid = " & xYearUnkid & " and leavetypeunkid =" & intLeaveTypeUnkid

            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                strQ &= " AND isclose_fy = 0 "
            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " AND isopenelc = 1 AND iselc = 1 "
            End If

            strQ &= ")  "


            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " FOR XML PATH('')),1,1,''),'') AS iEmp  "


            'Pinkal (29-Jan-2019) -- End

            objDataOperation.ClearParameters()
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIDs = dsList.Tables(0).Rows(0).Item("iEmp")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeIDsWithoutAccrue; Module Name: " & mstrModuleName)
        End Try
        Return mstrEmployeeIDs
    End Function

    'Pinkal (30-Jun-2016) -- End



    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Public Sub GetAsonDateAccrue(ByVal xDatabaseStartDate As Date, ByVal xDatabaseEndDate As Date _
                                                  , ByVal xLeaveBalanceSetting As Integer, ByVal xEmployeeID As Integer _
                                                  , ByVal xLeaveTypeID As Integer, ByVal dtpDate As Date _
                                                  , ByVal xLeaveAccrueTenureSetting As Integer _
                                                  , ByVal xLeaveAccrueDaysAfterEachMonth As Integer _
                                                  , ByRef mdecAsonDateAccrue As Decimal, ByRef mdecAsonDateBalance As Decimal _
                                                  , ByVal xFinYearID As Integer)

        'Pinkal (06-Apr-2018) --  'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK]. [ByVal xFinYearID As Integer] 


        Try
            Dim dsList As DataSet = Nothing
            If xLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (06-Apr-2018) -- Start
                'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
                'dsList = GetLeaveBalanceInfo(dtpDate.Date, xLeaveTypeID.ToString(), xEmployeeID.ToString(), xLeaveAccrueTenureSetting, xLeaveAccrueDaysAfterEachMonth)
                dsList = GetLeaveBalanceInfo(dtpDate.Date, xLeaveTypeID.ToString(), xEmployeeID.ToString(), xLeaveAccrueTenureSetting, xLeaveAccrueDaysAfterEachMonth, , , , , xLeaveBalanceSetting, , xFinYearID)
                'Pinkal (06-Apr-2018) -- End


            ElseIf xLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                mdtdbStartDate = xDatabaseStartDate.Date
                mdtdbEndDate = xDatabaseEndDate.Date
                'Pinkal (06-Apr-2018) -- Start
                'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
                'dsList = GetLeaveBalanceInfo(dtpDate.Date, xLeaveTypeID, xEmployeeID.ToString(), xLeaveAccrueTenureSetting, xLeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
                dsList = GetLeaveBalanceInfo(dtpDate.Date, xLeaveTypeID, xEmployeeID.ToString(), xLeaveAccrueTenureSetting, xLeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True, xLeaveBalanceSetting, -1, xFinYearID)
                'Pinkal (06-Apr-2018) -- End

            End If

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                mdecAsonDateAccrue = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                mdecAsonDateBalance = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) - (CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")))).ToString("#0.00")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAsonDateAccrue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Nov-2016) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Accure Leave is already defined for this employee. Please define new Accure Leave.")
            Language.setMessage(mstrModuleName, 2, "Sorry, This Accure Leave is already Issued. You cannot delete this Accrue Leave.")
            Language.setMessage(mstrModuleName, 3, "Sorry, This Leave is already Issued to Some Employees. Thus you cannot delete this Leave.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot Edit this Employee's record.Reason: It is already in use.")
            Language.setMessage(mstrModuleName, 5, "WEB")
            Language.setMessage(mstrModuleName, 6, "Code")
            Language.setMessage(mstrModuleName, 7, "Name")
            Language.setMessage(mstrModuleName, 8, "Life Time")
            Language.setMessage(mstrModuleName, 9, "Financial Year")
            Language.setMessage(mstrModuleName, 10, "Employee Leave Cycle")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class