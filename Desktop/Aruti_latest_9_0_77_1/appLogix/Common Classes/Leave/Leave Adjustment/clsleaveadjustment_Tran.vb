﻿'************************************************************************************************************************************
'Class Name : clsleaveadjustment_Tran.vb
'Purpose    :
'Date       :9/2/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveadjustment_Tran
    Private Const mstrModuleName = "clsleaveadjustment_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mbintAdjustmentunkid As Int64 = 0
    Private mbintLeavebalanceunkid As Int64 = 0
    Private mbintEmployeeunkid As Int64 = 0
    Private mbintLeavetypeunkid As Int64 = 0
    Private mdecAdjustment_Amt As Decimal = 0
    Private mstrRemarks As String = ""
    Private mblnIsopenelc As Boolean = False
    Private mblnIselc As Boolean = False
    Private mblnIsclose_Fy As Boolean = False
    Private mbintUserunkid As Int64
    Private mblnIsvoid As Boolean
    Private mbintVoiduserunkid As Int64 = -1
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = ""
    Private mstrWebFormName As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mdecLeaveBf_Adjustment As Decimal = 0
    'Pinkal (08-Oct-2018) -- End


#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adjustmentunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Adjustmentunkid() As Int64
        Get
            Return mbintAdjustmentunkid
        End Get
        Set(ByVal value As Int64)
            mbintAdjustmentunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavebalanceunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavebalanceunkid() As Int64
        Get
            Return mbintLeavebalanceunkid
        End Get
        Set(ByVal value As Int64)
            mbintLeavebalanceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Int64
        Get
            Return mbintEmployeeunkid
        End Get
        Set(ByVal value As Int64)
            mbintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Int64
        Get
            Return mbintLeavetypeunkid
        End Get
        Set(ByVal value As Int64)
            mbintLeavetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adjustment_amt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Adjustment_Amt() As Decimal
        Get
            Return mdecAdjustment_Amt
        End Get
        Set(ByVal value As Decimal)
            mdecAdjustment_Amt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isopenelc
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isopenelc() As Boolean
        Get
            Return mblnIsopenelc
        End Get
        Set(ByVal value As Boolean)
            mblnIsopenelc = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iselc
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Iselc() As Boolean
        Get
            Return mblnIselc
        End Get
        Set(ByVal value As Boolean)
            mblnIselc = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isclose_fy
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isclose_Fy() As Boolean
        Get
            Return mblnIsclose_Fy
        End Get
        Set(ByVal value As Boolean)
            mblnIsclose_Fy = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Int64
        Get
            Return mbintUserunkid
        End Get
        Set(ByVal value As Int64)
            mbintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Int64
        Get
            Return mbintVoiduserunkid
        End Get
        Set(ByVal value As Int64)
            mbintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public Property _WebClientIP() As String
    '    Get
    '        Return mstrWebClientIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set LeaveBf_Adjustment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LeaveBf_Adjustment() As Decimal
        Get
            Return mdecLeaveBf_Adjustment
        End Get
        Set(ByVal value As Decimal)
            mdecLeaveBf_Adjustment = value
        End Set
    End Property

    'Pinkal (08-Oct-2018) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  adjustmentunkid " & _
              ", leavebalanceunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", adjustment_amt " & _
              ", remarks " & _
              ", isopenelc " & _
              ", iselc " & _
              ", isclose_fy " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                      ", ISNULL(leavebf_adjustment,0.00) AS leavebf_adjustment " & _
             "FROM lvleaveadjustment_tran " & _
             "WHERE adjustmentunkid = @adjustmentunkid "

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(leavebf_adjustment,0.00) AS leavebf_adjustment " & _]

            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.BigInt, mbintAdjustmentunkid.ToString.Length, mbintAdjustmentunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mbintAdjustmentunkid = dtRow.Item("adjustmentunkid")
                mbintLeavebalanceunkid = dtRow.Item("leavebalanceunkid")
                mbintEmployeeunkid = dtRow.Item("employeeunkid")
                mbintLeavetypeunkid = dtRow.Item("leavetypeunkid")
                mdecAdjustment_Amt = dtRow.Item("adjustment_amt")
                mstrremarks = dtRow.Item("remarks").ToString
                mblnisopenelc = CBool(dtRow.Item("isopenelc"))
                mblniselc = CBool(dtRow.Item("iselc"))
                mblnisclose_fy = CBool(dtRow.Item("isclose_fy"))
                mbintUserunkid = dtRow.Item("userunkid")
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mbintVoiduserunkid = dtRow.Item("voiduserunkid")
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mdecLeaveBf_Adjustment = dtRow.Item("leavebf_adjustment")
                'Pinkal (08-Oct-2018) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  adjustmentunkid " & _
              ", leavebalanceunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", adjustment_amt " & _
              ", remarks " & _
              ", isopenelc " & _
              ", iselc " & _
              ", isclose_fy " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                      ", ISNULL(leavebf_adjustment,0.00) AS leavebf_adjustment " & _
             "FROM lvleaveadjustment_tran "

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(leavebf_adjustment,0.00) AS leavebf_adjustment " & _]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveadjustment_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, mbintLeavebalanceunkid.ToString.Length, mbintLeavebalanceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.BigInt, mbintEmployeeunkid.ToString.Length, mbintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.BigInt, mbintLeavetypeunkid.ToString.Length, mbintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdjustment_Amt.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.BigInt, mbintUserunkid.ToString.Length, mbintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.BigInt, mbintVoiduserunkid.ToString.Length, mbintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@leavebf_adjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLeaveBf_Adjustment.ToString)
            'Pinkal (08-Oct-2018) -- End


            strQ = "INSERT INTO lvleaveadjustment_tran ( " & _
                      "  leavebalanceunkid " & _
                      ", employeeunkid " & _
                      ", leavetypeunkid " & _
                      ", adjustment_amt " & _
                      ", remarks " & _
                      ", isopenelc " & _
                      ", iselc " & _
                      ", isclose_fy " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", leavebf_adjustment " & _
                    ") VALUES (" & _
                      "  @leavebalanceunkid " & _
                      ", @employeeunkid " & _
                      ", @leavetypeunkid " & _
                      ", @adjustment_amt " & _
                      ", @remarks " & _
                      ", @isopenelc " & _
                      ", @iselc " & _
                      ", @isclose_fy " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                      ", @leavebf_adjustment " & _
                    "); SELECT @@identity"

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", @leavebf_adjustment " & _]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mbintAdjustmentunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAudiTrailForLeaveAdjustment(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim mdecTotaladjustment As Decimal = GetTotalAdjustment(mbintLeavebalanceunkid, mbintEmployeeunkid, mbintLeavetypeunkid, objDataOperation)


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            'strQ = " UPDATE lvleavebalance_tran set  accrue_amount = accrue_amount + @adjustment " & _
            '          ",uptolstyr_issueamt = uptolstyr_issueamt + @adjustment,remaining_bal = (accrue_amount + @adjustment) - issue_amount  " & _
            '          ",adj_remaining_bal = @totaladjustment  WHERE isvoid = 0 AND employeeunkid =@employeeunkid AND leavetypeunkid = @leavetypeunkid AND leavebalanceunkid = @leavebalanceunkid "


            'Pinkal (01-Jan-2021)-- Start
            'Enhancement  -  Working on Close Year Changes.

            'strQ = " UPDATE lvleavebalance_tran set Leavebf = Leavebf + @leavebfadjustment " & _
            '           ", accrue_amount = accrue_amount + @adjustment + @leavebfadjustment " & _
            '           ", uptolstyr_issueamt = uptolstyr_issueamt + @adjustment " & _
            '           ", remaining_bal = (accrue_amount + @adjustment + @leavebfadjustment) - issue_amount  " & _
            '           ", adj_remaining_bal = @totaladjustment   " & _
            '           "  WHERE isvoid = 0 AND employeeunkid =@employeeunkid AND leavetypeunkid = @leavetypeunkid AND leavebalanceunkid = @leavebalanceunkid "

            'Pinkal (02-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 

            'strQ = " UPDATE lvleavebalance_tran set Leavebf = Leavebf + @leavebfadjustment " & _
            '          ", accrue_amount = (actualamount + LeaveBF + @leavebfadjustment ) + @adjustment  " & _
            '          ", uptolstyr_issueamt = uptolstyr_issueamt + @adjustment " & _
            '          ", remaining_bal = (actualamount + LeaveBF + @leavebfadjustment + @adjustment ) - issue_amount  " & _
            '          ", adj_remaining_bal = @totaladjustment   " & _
            '          "  WHERE isvoid = 0 AND employeeunkid =@employeeunkid AND leavetypeunkid = @leavetypeunkid AND leavebalanceunkid = @leavebalanceunkid "


            strQ = " UPDATE lvleavebalance_tran set Leavebf = Leavebf + @leavebfadjustment " & _
                     ", accrue_amount = (actualamount + LeaveBF + @leavebfadjustment + adj_remaining_bal) + @adjustment  " & _
                      ", uptolstyr_issueamt = uptolstyr_issueamt + @adjustment " & _
                     ", remaining_bal = (actualamount + LeaveBF + @leavebfadjustment + @adjustment + adj_remaining_bal ) - issue_amount  " & _
                      ", adj_remaining_bal = @totaladjustment   " & _
                      "  WHERE isvoid = 0 AND employeeunkid =@employeeunkid AND leavetypeunkid = @leavetypeunkid AND leavebalanceunkid = @leavebalanceunkid "


            'Pinkal (02-Nov-2021) -- End


            'Pinkal (01-Jan-2021) -- End


            'Pinkal (08-Oct-2018) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.BigInt, mbintEmployeeunkid.ToString.Length, mbintEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.BigInt, mbintEmployeeunkid.ToString.Length, mbintLeavetypeunkid)
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, mbintEmployeeunkid.ToString.Length, mbintLeavebalanceunkid)
            objDataOperation.AddParameter("@adjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdjustment_Amt)
            objDataOperation.AddParameter("@totaladjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotaladjustment)

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@leavebfadjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLeaveBf_Adjustment)
            'Pinkal (08-Oct-2018) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objLeaveBal As New clsleavebalance_tran
            objDataOperation.ClearParameters()
            objLeaveBal._mObjDataoperation = objDataOperation
            objLeaveBal._LeaveBalanceunkid = mbintLeavebalanceunkid
            objLeaveBal._AdjustmentAmt = mdecTotaladjustment
            With objLeaveBal
                ._FormName = mstrFormName
                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objLeaveBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveadjustment_tran) </purpose>
    Public Function Update() As Boolean
     
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, mbintLeavebalanceunkid.ToString.Length, mbintLeavebalanceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.BigInt, mbintEmployeeunkid.ToString.Length, mbintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.BigInt, mbintLeavetypeunkid.ToString.Length, mbintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdjustment_Amt.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.BigInt, mbintUserunkid.ToString.Length, mbintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.BigInt, mbintVoiduserunkid.ToString.Length, mbintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            StrQ = "UPDATE lvleaveadjustment_tran SET " & _
              "  leavebalanceunkid = @leavebalanceunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", leavetypeunkid = @leavetypeunkid" & _
              ", adjustment_amt = @adjustment_amt" & _
              ", remarks = @remarks" & _
              ", isopenelc = @isopenelc" & _
              ", iselc = @iselc" & _
              ", isclose_fy = @isclose_fy" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE adjustmentunkid = @adjustmentunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveadjustment_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Long, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try

            strQ = "SELECT adjustmentunkid FROM lvleaveadjustment_tran WHERE leavebalanceunkid = @leavebalanceunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, intUnkid.ToString.Length, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                strQ = "Update lvleaveadjustment_tran SET isvoid = 1,voiddatetime = getdate(),voiduserunkid = @voiduserunkid,voidreason = @voidreason WHERE adjustmentunkid  = @adjustmentunkid AND leavebalanceunkid = @leavebalanceunkid "

                For Each dr As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.BigInt, CLng(dr("adjustmentunkid")).ToString.Length, CLng(dr("adjustmentunkid")))
                    objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, intUnkid.ToString.Length, intUnkid)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.BigInt, mbintUserunkid.ToString.Length, mbintVoiduserunkid)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    _Adjustmentunkid = CLng(dr("adjustmentunkid"))

                    If InsertAudiTrailForLeaveAdjustment(objDataOperation, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  adjustmentunkid " & _
              ", leavebalanceunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", adjustment_amt " & _
              ", remarks " & _
              ", isopenelc " & _
              ", iselc " & _
              ", isclose_fy " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM lvleaveadjustment_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND adjustmentunkid <> @adjustmentunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetTotalAdjustment(ByVal intLeavebalanceId As Integer, ByVal intEmployeeId As Integer _
                                                       , ByVal intLeaveTypeID As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Decimal
        Dim mdecAdjustment As Decimal = 0
        Dim strQ As String = ""
        Try

            If objDOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDOperation
            End If

            strQ = " SELECT SUM(adjustment_amt) AS adjustment FROM lvleaveadjustment_tran " & _
                      " WHERE leavebalanceunkid = @leavebalanceunkid AND employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, intLeavebalanceId.ToString().Length, intLeavebalanceId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.BigInt, intEmployeeId.ToString().Length, intEmployeeId)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.BigInt, intLeaveTypeID.ToString().Length, intLeaveTypeID)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecAdjustment = CDec(dsList.Tables(0).Rows(0)("adjustment"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalAdjustment; Module Name: " & mstrModuleName)
        End Try
        Return mdecAdjustment
    End Function

    'S.SANDEEP |11-APR-2019| -- START
    Public Function GetAdustmentRemarkOnStatementReport(ByVal intEmployeeId As Integer, ByVal strLeaveTypeIds As String, ByVal elvSetting As enLeaveBalanceSetting) As String
        Dim strAdjRemark As String = String.Empty
        Dim strQ As String = ""
        Try
            If strLeaveTypeIds.Trim.Length > 0 Then
                Using objDataOpr As New clsDataOperation
                    strQ = "SELECT " & _
                           "    remarks " & _
                           "FROM " & _
                           "( " & _
                           "    SELECT " & _
                           "         employeeunkid " & _
                           "        ,leavetypeunkid " & _
                           "        ,remarks " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,leavetypeunkid ORDER BY adjustmentunkid DESC) AS rno " & _
                           "    FROM lvleaveadjustment_tran " & _
                           "    WHERE employeeunkid = @EmpId AND leavetypeunkid IN (" & strLeaveTypeIds & ") "
                    If elvSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= " AND isopenelc = 1 AND iselc = 1 "
                    End If
                    strQ &= " ) AS ladj WHERE ladj.rno = 1 "

                    objDataOpr.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                    Dim ds As New DataSet
                    ds = objDataOpr.ExecQuery(strQ, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        Throw New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    End If

                    If ds.Tables(0).Rows.Count > 0 Then
                        strAdjRemark = ds.Tables(0).Rows(0)("remarks").ToString()
                    End If
                End Using
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAdustmentRemarkOnStatementReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strAdjRemark
    End Function
    'S.SANDEEP |11-APR-2019| -- END

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAudiTrailForLeaveAdjustment(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            strQ = "INSERT INTO atlvleaveadjustment_tran ( " & _
                      " adjustmentunkid " & _
                      ", leavebalanceunkid " & _
                      ", employeeunkid " & _
                      ", leavetypeunkid " & _
                      ", adjustment_amt " & _
                      ", remarks " & _
                      ", isopenelc " & _
                      ", iselc " & _
                      ", isclose_fy " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", machine_name " & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                      ", leavebf_adjustment " & _
                    ") VALUES (" & _
                      "  @adjustmentunkid " & _
                      ", @leavebalanceunkid " & _
                      ", @employeeunkid " & _
                      ", @leavetypeunkid " & _
                      ", @adjustment_amt " & _
                      ", @remarks " & _
                      ", @isopenelc " & _
                      ", @iselc " & _
                      ", @isclose_fy " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", getdate() " & _
                      ", @ip" & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", @isweb " & _
                      ", @leavebf_adjustment " & _
                    "); SELECT @@identity"


            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[ ", @leavebf_adjustment " & _]


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.BigInt, mbintLeavebalanceunkid.ToString.Length, mbintAdjustmentunkid.ToString)
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.BigInt, mbintLeavebalanceunkid.ToString.Length, mbintLeavebalanceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.BigInt, mbintEmployeeunkid.ToString.Length, mbintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.BigInt, mbintLeavetypeunkid.ToString.Length, mbintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdjustment_Amt.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.BigInt, mbintUserunkid.ToString.Length, mbintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@leavebf_adjustment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLeaveBf_Adjustment.ToString)
            'Pinkal (08-Oct-2018) -- End


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAudiTrailForLeaveBalance", mstrModuleName)
            Return False
        End Try
    End Function

End Class