﻿'************************************************************************************************************************************
'Class Name : clsleaveadjustment_master.vb
'Purpose    :
'Date       :20/11/2010
'Written By :Krishna
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Krishna
''' </summary>
Public Class clsleaveadjustment_master
    Private Const mstrModuleName = "clsleaveadjustment_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objleaveAdjustran As New clsleaveadjustment_Tran

#Region " Private variables "
    Private mintAdjustmentunkid As Integer
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintLeavetypeunkid As Integer
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
Private mintAuditUserId As Integer = 0
Public WriteOnly Property _AuditUserId() As Boolean 
Set(ByVal value As Integer) 
mintAuditUserId = value 
End Set 
End Property 
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Krishna
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adjustmentunkid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Adjustmentunkid() As Integer
        Get
            Return mintAdjustmentunkid
        End Get
        Set(ByVal value As Integer)
            mintAdjustmentunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Krishna
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Krishna
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  adjustmentunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
             "FROM lvleaveadjustment_master " & _
             "WHERE adjustmentunkid = @adjustmentunkid "

            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAdjustmentUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintadjustmentunkid = CInt(dtRow.Item("adjustmentunkid"))
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintleavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Krishna
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  adjustmentunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
             "FROM lvleaveadjustment_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Krishna
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveadjustment_master) </purpose>
    Public Function Insert(ByVal mdtadjustedDate As DataTable) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)

            strQ = "INSERT INTO lvleaveadjustment_master ( " & _
              "  yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", leavetypeunkid" & _
            ") VALUES (" & _
              "  @yearunkid " & _
              ", @periodunkid " & _
              ", @employeeunkid " & _
              ", @leavetypeunkid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAdjustmentunkid = dsList.Tables(0).Rows(0).Item(0)
            objleaveAdjustran._Adjustmentunkid = mintAdjustmentunkid
            objleaveAdjustran.Insert(objDataOperation, mdtadjustedDate, mintEmployeeunkid, mintYearunkid)


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Krishna
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveadjustment_master) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintadjustmentunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintyearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintleavetypeunkid.ToString)

            StrQ = "UPDATE lvleaveadjustment_master SET " & _
              "  yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", leavetypeunkid = @leavetypeunkid " & _
            "WHERE adjustmentunkid = @adjustmentunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Krishna
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveadjustment_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "DELETE FROM lvleaveadjustment_master " & _
            "WHERE adjustmentunkid = @adjustmentunkid "

            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetHolidayandWeekend(ByVal startdate As DateTime, ByVal enddate As DateTime, ByVal yearunkid As Integer, ByVal periodunkid As Integer, ByVal employeeunkid As Integer, ByVal shiftunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "  DECLARE @days AS VARCHAR(500) " & _
                       "  DECLARE @QRY AS NVARCHAR(MAX)  " & _
                       "  SELECT @days = '''' + REPLACE(shiftdays, '|', ''',''') + '''' " & _
                       "  FROM  tnashift_master  WHERE shiftunkid = @shiftunkid " & _
                       "  SELECT @QRY = N'SELECT login_date,DATENAME(dw,dateadd(dd, 0, login_date)) as Name,cast(0 as bit) isholiday FROM tnalogin_summary WHERE DATENAME(dw,dateadd(dd, 0, login_date))" & _
                       "  NOT IN (' +  @days + ') AND employeeunkid = ''' + cast (@employeeunkid as char(10)) + '''  and convert(char(8),login_date,112) between ''' + @startdate + ''' and  ''' + @enddate + ''' " & _
                       "   AND login_date NOT IN  " & _
                       " (SELECT  adjustmentdate  FROM lvleaveadjustment_tran  " & _
                       "  JOIN lvleaveadjustment_master ON lvleaveadjustment_master.adjustmentunkid = lvleaveadjustment_tran.adjustmentunkid " & _
                       " WHERE lvleaveadjustment_master.employeeunkid = ''' " & _
                       " + CAST (@employeeunkid AS CHAR(10)) + ''' " & _
                       "  AND lvleaveadjustment_master.yearunkid =  ''' " & _
                       " + CAST (@yearunkid AS CHAR(10)) + ''' " & _
                       " AND lvleaveadjustment_master.periodunkid = ''' " & _
                       " + CAST (@periodunkid AS CHAR(10))   + ''')" & _
                       " " & _
                       "  Union  " & _
                       " " & _
                       "  SELECT login_date,lvholiday_master.holidayname as Name,cast(1 as bit) isholiday FROM tnalogin_summary  " & _
                       "  JOIN lvholiday_master ON lvholiday_master.holidaydate = tnalogin_summary.login_date " & _
                       " WHERE employeeunkid =  ''' + cast (@employeeunkid as char(10)) + '''   And tnalogin_summary.ispaidleave = 0 And tnalogin_summary.isunpaidleave = 0  " & _
                       " AND login_date between ''' + @startdate + ''' and  ''' + @enddate +  ''' AND login_date NOT IN " & _
                       " (SELECT  adjustmentdate  FROM lvleaveadjustment_tran  " & _
                       " JOIN lvleaveadjustment_master ON lvleaveadjustment_master.adjustmentunkid = lvleaveadjustment_tran.adjustmentunkid " & _
                       " WHERE lvleaveadjustment_master.employeeunkid = ''' " & _
                       " + CAST (@employeeunkid AS CHAR(10)) + ''' " & _
                       " AND lvleaveadjustment_master.yearunkid =  ''' " & _
                       " + CAST (@yearunkid AS CHAR(10)) + ''' " & _
                       " AND lvleaveadjustment_master.periodunkid = ''' " & _
                       " + CAST (@periodunkid AS CHAR(10)) + ''')' EXECUTE sp_executesql @QRY   "

            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(startdate))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(enddate))
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, yearunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, periodunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, shiftunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetHolidayandWeekend", mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function GetAbsentDays(ByVal startdate As DateTime, ByVal enddate As DateTime, ByVal yearunkid As Integer, ByVal periodnkid As Integer, ByVal employeeunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT login_date,DATENAME(dw,dateadd(dd, 0, login_date)) as Name FROM tnalogin_summary " & _
                       " WHERE tnalogin_summary.employeeunkid = @employeeunkid  AND tnalogin_summary.isunpaidleave = 1 and " & _
                       "  tnalogin_summary.login_date BETWEEN @startdate AND @enddate " & _
                       " AND login_date NOT IN  " & _
                       " (SELECT  absentdate  FROM lvleaveadjustment_tran " & _
                       " JOIN lvleaveadjustment_master ON lvleaveadjustment_master.adjustmentunkid = lvleaveadjustment_tran.adjustmentunkid " & _
                       " WHERE lvleaveadjustment_master.employeeunkid = @employeeunkid And lvleaveadjustment_master.yearunkid = @yearunkid  " & _
                       " AND lvleaveadjustment_master.periodunkid = @periodnkid) "


            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(startdate))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(enddate))
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, yearunkid)
            objDataOperation.AddParameter("@periodnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, periodnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAbsentDays", mstrModuleName)
        End Try
        Return dsList
    End Function

End Class