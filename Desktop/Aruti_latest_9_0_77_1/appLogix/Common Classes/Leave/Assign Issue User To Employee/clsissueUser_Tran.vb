﻿'************************************************************************************************************************************
'Class Name : clsissueUser_Tran.vb
'Purpose    :
'Date       :28/12/2012
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsissueUser_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsissueUser_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintIssueusertranunkid As Integer
    Private mintIssueuserunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintOldissueuserunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtEmployee As DataTable = Nothing
#End Region

#Region "Constuctor"

    Public Sub New()
        mdtEmployee = New DataTable("Employee")
        mdtEmployee.Columns.Add("issueusertranunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("issueuserunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("issueuser", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("employeeunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("employeecode", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("employee", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("oldissueuserunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("oldissueuser", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("userunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("isvoid", Type.GetType("System.Boolean"))
        mdtEmployee.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
        mdtEmployee.Columns.Add("voidreason", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("departmentunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("department", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("sectionunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("section", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("jobunkid", Type.GetType("System.Int32"))
        mdtEmployee.Columns.Add("job", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("AUD", Type.GetType("System.String"))
        mdtEmployee.Columns.Add("GUID", Type.GetType("System.String"))
    End Sub

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issueusertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Issueusertranunkid() As Integer
        Get
            Return mintIssueusertranunkid
        End Get
        Set(ByVal value As Integer)
            mintIssueusertranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Issueuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Issueuserunkid() As Integer
        Get
            Return mintIssueuserunkid
        End Get
        Set(ByVal value As Integer)
            mintIssueuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtEmployee
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtEmployee() As DataTable
        Get
            Return mdtEmployee
        End Get
        Set(ByVal value As DataTable)
            mdtEmployee = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  issueusertranunkid " & _
                      ", issueuserunkid " & _
                      ", employeeunkid " & _
                      ", oldissueuserunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM lvissueuser_tran " & _
                     "WHERE issueusertranunkid = @issueusertranunkid "

            objDataOperation.AddParameter("@issueusertranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintIssueUserTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintissueusertranunkid = CInt(dtRow.Item("issueusertranunkid"))
                mintIssueuserunkid = CInt(dtRow.Item("issueuserunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintoldissueuserunkid = CInt(dtRow.Item("oldissueuserunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intIssueuserunkid As Integer = -1, Optional ByVal intEmployeeId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT  lvissueuser_tran.issueusertranunkid " & _
                       ",lvissueuser_tran.issueuserunkid " & _
                       ",ncu.username AS issueuser " & _
                       ",lvissueuser_tran.employeeunkid   " & _
                       ",ISNULL(hremployee_master.employeecode,'') employeecode " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') employee " & _
                       ",lvissueuser_tran.oldissueuserunkid " & _
                       ",ocu.username AS oldissueuser " & _
                       ",hremployee_master.departmentunkid " & _
                       ",hrdepartment_master.name department " & _
                       ",hremployee_master.sectionunkid " & _
                       ",hrsection_master.name section " & _
                       ",hremployee_master.jobunkid " & _
                       ",hrjob_master.job_name AS job " & _
                       ",lvissueuser_tran.userunkid " & _
                       ",lvissueuser_tran.isvoid " & _
                       ",lvissueuser_tran.voiduserunkid " & _
                       ",lvissueuser_tran.voiddatetime " & _
                       ",lvissueuser_tran.voidreason " & _
                       ", '' AUD " & _
                       ", '' GUID " & _
                       " FROM lvissueuser_tran " & _
                       " LEFT JOIN hremployee_master ON lvissueuser_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       " LEFT JOIN hrmsconfiguration..cfuser_master ncu ON ncu.userunkid = lvissueuser_tran.issueuserunkid " & _
                       " LEFT JOIN hrmsconfiguration..cfuser_master ocu ON ocu.userunkid = lvissueuser_tran.oldissueuserunkid " & _
                       " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                       " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
                       " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid "


            strQ &= " WHERE lvissueuser_tran.isvoid = 0 "


            If intIssueuserunkid > 0 Then
                strQ &= " AND lvissueuser_tran.issueuserunkid = " & intIssueuserunkid
            End If

            If intEmployeeId > 0 Then
                strQ &= " AND lvissueuser_tran.employeeunkid = " & intEmployeeId
            End If


            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes
            strQ &= " AND hremployee_master.isapproved = 1"
            'Pinkal (06-Feb-2013) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If dsList.Tables.Count > 0 Then
                mdtEmployee = dsList.Tables(0)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvissueuser_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mdtEmployee.Rows.Count <= 0 Then Return True

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "INSERT INTO lvissueuser_tran ( " & _
                   "  issueuserunkid " & _
                   ", employeeunkid " & _
                   ", oldissueuserunkid " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason" & _
                 ") VALUES (" & _
                   "  @issueuserunkid " & _
                   ", @employeeunkid " & _
                   ", @oldissueuserunkid " & _
                   ", @userunkid " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voiddatetime " & _
                   ", @voidreason" & _
                 "); SELECT @@identity"

            For Each row As DataRow In mdtEmployee.Rows

                If row("AUD").ToString() = "D" Then Continue For

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@issueuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("issueuserunkid").ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("employeeunkid").ToString)
                objDataOperation.AddParameter("@oldissueuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("oldissueuserunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintIssueusertranunkid = dsList.Tables(0).Rows(0).Item(0)

                If mintIssueusertranunkid > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvissueuser_tran", "issueusertranunkid", mintIssueusertranunkid, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If
            Next

         
            objDataOperation.ReleaseTransaction(True)
           

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvissueuser_tran) </purpose>
    ''' 
    Public Function Migration_Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mdtEmployee.Rows.Count <= 0 Then Return True

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            For Each row As DataRow In mdtEmployee.Rows

                If CInt(row("oldissueuserunkid")) > 0 AndAlso CInt(row("issueusertranunkid")) > 0 Then

                    mintVoiduserunkid = mintUserunkid
                    mdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime

                    If row("AUD").ToString().Trim <> "D" Then
                        If DeleteForMigration(objDataOperation, CInt(row("oldissueuserunkid")), CInt(row("employeeunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                      
                    ElseIf row("AUD").ToString().Trim = "D" Then
                        If Delete(CInt(row("issueusertranunkid")), objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    mintVoiduserunkid = Nothing
                    mdtVoiddatetime = Nothing
                End If


                If row("AUD").ToString().Trim = "D" Then Continue For


                strQ = "INSERT INTO lvissueuser_tran ( " & _
                       "  issueuserunkid " & _
                       ", employeeunkid " & _
                       ", oldissueuserunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                     ") VALUES (" & _
                       "  @issueuserunkid " & _
                       ", @employeeunkid " & _
                       ", @oldissueuserunkid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                     "); SELECT @@identity"


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@issueuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("issueuserunkid").ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("employeeunkid").ToString)
                objDataOperation.AddParameter("@oldissueuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, row("oldissueuserunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintIssueusertranunkid = dsList.Tables(0).Rows(0).Item(0)

                If mintIssueusertranunkid > 0 Then
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvissueuser_tran", "issueusertranunkid", mintIssueusertranunkid, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If
            Next


            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Migration_Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvissueuser_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation


        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If



        Try

            strQ = " Update lvissueuser_tran set " & _
                           " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE issueusertranunkid = @issueusertranunkid  AND isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@issueusertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lvissueuser_tran", "issueusertranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvissueuser_tran) </purpose>
    Public Function DeleteForMigration(ByVal objDataOperation As clsDataOperation, ByVal intOldIssueUserId As Integer, ByVal intEmployeeId As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = " Update lvissueuser_tran set " & _
                           " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE issueuserunkid = @IssueUserId AND employeeunkid = @employeeunkid AND  isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 2, "Migration"))
            objDataOperation.AddParameter("@IssueUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intOldIssueUserId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "Select  ISNULL(issueusertranunkid,0) issueusertranunkid FROM lvissueuser_tran WHERE issueuserunkId = @IssueUserId AND employeeunkid = @employeeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@IssueUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intOldIssueUserId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim intIssueTranunkid As Integer = CInt(dsList.Tables(0).Rows(0)("issueusertranunkid"))
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lvissueuser_tran", "issueusertranunkid", intIssueTranunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  issueusertranunkid " & _
              ", issueuserunkid " & _
              ", employeeunkid " & _
              ", oldissueuserunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM lvissueuser_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND issueusertranunkid <> @issueusertranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@issueusertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUserWithIssueLeavePrivilage(ByVal intYearId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try

            If mblFlag Then
                strQ = "SELECT 0 as userunkid, '' +  @name  as username, '' email UNION "
            End If



            'Pinkal (06-Mar-2013) -- Start
            'Enhancement : TRA Changes

            'strQ &= " SELECT ISNULL(hrmsConfiguration..cfuser_master.userunkid,0) userunkid " & _
            '             ",ISNULL(hrmsConfiguration..cfuser_master.username,'') username " & _
            '             ",ISNULL(hrmsConfiguration..cfuser_master.email,'') email " & _
            '             " FROM hrmsConfiguration..cfuser_master" & _
            '             " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
            '             " WHERE hrmsConfiguration..cfuser_privilege.privilegeunkid = 194 AND hrmsConfiguration..cfuser_master.isactive = 1"

            strQ &= " SELECT ISNULL(hrmsConfiguration..cfuser_master.userunkid,0) userunkid " & _
                         ",ISNULL(hrmsConfiguration..cfuser_master.username,'') username " & _
                         ",ISNULL(hrmsConfiguration..cfuser_master.email,'') email " & _
                         " FROM hrmsConfiguration..cfuser_master" & _
                         " JOIN hrmsConfiguration..cfuser_privilege ON hrmsConfiguration..cfuser_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                     " JOIN hrmsConfiguration..cfcompanyaccess_privilege ON hrmsConfiguration..cfcompanyaccess_privilege.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                     " AND hrmsconfiguration..cfcompanyaccess_privilege.yearunkid = " & intYearId & _
                         " WHERE hrmsConfiguration..cfuser_privilege.privilegeunkid = 194 AND hrmsConfiguration..cfuser_master.isactive = 1"

            'Pinkal (06-Mar-2013) -- End

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUserWithIssueLeavePrivilage", mstrModuleName)
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeIdFromIssueUser() As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT STUFF((SELECT ',' + CAST(employeeunkid AS NVARCHAR(max))  FROM lvissueuser_tran WHERE isvoid = 0 ORDER BY employeeunkid FOR XML PATH('')),1,1,'') AS employeeunkid"
            dsList = objDataOperation.ExecQuery(strQ, "EmpList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("employeeunkid").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeIdFromIssueUser", mstrModuleName)
        End Try
        Return ""
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Migration")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class