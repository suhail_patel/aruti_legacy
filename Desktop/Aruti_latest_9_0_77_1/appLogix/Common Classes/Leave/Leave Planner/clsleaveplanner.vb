﻿'************************************************************************************************************************************
'Class Name : clsleaveplanner.vb
'Purpose    :
'Date       :30/08/2010
'Written By :Pinkal
'Modified   :
'Last Message index = 1
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveplanner
    Private Shared ReadOnly mstrModuleName As String = "clsleaveplanner"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLeaveplannerunkid As Integer
    Private mintAnalysisrefid As Integer
    Private mintEmployeeunkid As Integer
    Private mintLeavetypeunkid As Integer
    Private mdtStartdate As Date
    Private mdtStopdate As Date
    Private mintStatus As Integer
    Private mstrRemarks As String = String.Empty
    Private mdtCanceldate As Date
    Private mstrCancelremarks As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty

    'Pinkal (17-May-2011) -- Start
    Private mintUserunkid As Integer = -1
    Private mintLoginemployeeunkid As Integer = -1
    'Pinkal (17-May-2011) -- End


    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private mintVoidlogingemployeeunkid As Integer = -1
    'Pinkal (07-APR-2012) -- End


    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.
    Private mdtPlannerFraction As DataTable = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'Pinkal (30-Jun-2016) -- End


#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveplannerunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leaveplannerunkid() As Integer
        Get
            Return mintLeaveplannerunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveplannerunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisrefid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Analysisrefid() As Integer
        Get
            Return mintAnalysisrefid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisrefid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stopdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stopdate() As Date
        Get
            Return mdtStopdate
        End Get
        Set(ByVal value As Date)
            mdtStopdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Status() As Integer
        Get
            Return mintStatus
        End Get
        Set(ByVal value As Integer)
            mintStatus = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Canceldate() As Date
        Get
            Return mdtCanceldate
        End Get
        Set(ByVal value As Date)
            mdtCanceldate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelremarks
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancelremarks() As String
        Get
            Return mstrCancelremarks
        End Get
        Set(ByVal value As String)
            mstrCancelremarks = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LoginEmployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    ''' <summary>
    ''' Purpose: Get or Set PlannerFraction
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _PlannerFraction() As DataTable
        Get
            Return mdtPlannerFraction
        End Get
        Set(ByVal value As DataTable)
            mdtPlannerFraction = value
        End Set
    End Property



    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property


    'Pinkal (30-Jun-2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  leaveplannerunkid " & _
              ", analysisrefid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", startdate " & _
              ", stopdate " & _
              ", status " & _
              ", remarks " & _
              ", canceldate " & _
              ", cancelremarks " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", userunkid " & _
              ", loginemployeeunkid  " & _
              ", ISNULL(voidloginemployeeunkid,0) voidloginemployeeunkid " & _
             "FROM lvleaveplanner " & _
             "WHERE leaveplannerunkid = @leaveplannerunkid "

            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveplannerunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintleaveplannerunkid = CInt(dtRow.Item("leaveplannerunkid"))
                mintanalysisrefid = CInt(dtRow.Item("analysisrefid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintleavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mdtstartdate = dtRow.Item("startdate")
                mdtstopdate = dtRow.Item("stopdate")
                mintstatus = CInt(dtRow.Item("status"))
                mstrRemarks = dtRow.Item("remarks").ToString
                If dtRow.Item("canceldate") IsNot DBNull.Value Then mdtCanceldate = dtRow.Item("canceldate")
                mstrCancelremarks = dtRow.Item("cancelremarks").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If dtRow.Item("voiddatetime") IsNot DBNull.Value Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                If dtRow.Item("userunkid") IsNot DBNull.Value Then mintUserunkid = CInt(dtRow.Item("userunkid"))
                If dtRow.Item("loginemployeeunkid") IsNot DBNull.Value Then mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                If dtRow.Item("voidloginemployeeunkid") IsNot DBNull.Value Then mintVoidlogingemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))

                Exit For
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                       , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                       , ByVal mdtEmployeeAsOnDate As Date _
                                       , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                       , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                       , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                       , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)
            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            If blnApplyUserAccessFilter Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsOnDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'Pinkal (22-Mar-2016) -- End
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)

           
            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            'ISNULL((SELECT SUM(lvplannerday_fraction.dayfraction) FROM lvplannerday_fraction WHERE lvplannerday_fraction.isvoid = 0 AND lvplannerday_fraction.leaveplannerunkid = lvleaveplanner.leaveplannerunkid), 0) AS Days,
            'Pinkal (30-Jun-2016) -- End

           
            strQ = "SELECT " & _
              "  leaveplannerunkid " & _
              ", analysisrefid " & _
              ", lvleaveplanner.employeeunkid " & _
              ", hremployee_master.employeecode " & _
              ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
              "	,@Code +' : ' + hremployee_master.employeecode + ' - ' + @Name +' : ' + isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') AS EName " & _
              ", lvleaveplanner.leavetypeunkid " & _
              ", lvleavetype_master.leavename " & _
              ", lvleavetype_master.color " & _
              ", convert(char(8),startdate,112) as startdate " & _
              ", convert(char(8),stopdate,112) as stopdate " & _
              ", case when status = 1 then 'Approve' when status = 5 then 'Planned' when status = 6 then 'Cancel' end as statusname " & _
                      ", ISNULL((SELECT SUM(lvplannerday_fraction.dayfraction) FROM lvplannerday_fraction WHERE lvplannerday_fraction.isvoid = 0 AND lvplannerday_fraction.leaveplannerunkid = lvleaveplanner.leaveplannerunkid), 0) AS Days " & _
              ", status " & _
              ", remarks " & _
              ", canceldate " & _
              ", cancelremarks " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
                      ", ISNULL(voidloginemployeeunkid,0) voidloginemployeeunkid " & _
                      ",ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                      ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                      ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                      ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                      ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                      ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                      ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                      ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                      ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                      ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
                      ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                      ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                      ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                      ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                      ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
                      " FROM lvleaveplanner " & _
                      " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvleaveplanner.employeeunkid " & _
                      " LEFT JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleaveplanner.leavetypeunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         stationunkid " & _
                      "        ,deptgroupunkid " & _
                      "        ,departmentunkid " & _
                      "        ,sectiongroupunkid " & _
                      "        ,sectionunkid " & _
                      "        ,unitgroupunkid " & _
                      "        ,unitunkid " & _
                      "        ,teamunkid " & _
                      "        ,classgroupunkid " & _
                      "        ,classunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                      " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         jobunkid " & _
                      "        ,jobgroupunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         gradegroupunkid " & _
                      "        ,gradeunkid " & _
                      "        ,gradelevelunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "    FROM prsalaryincrement_tran " & _
                      "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                      " ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 "
        

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry & " "
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry & " "
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry & " "
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END



            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry & " "
            End If

            strQ &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND lvleaveplanner.isvoid = 0 "
            End If

            strQ &= " AND hremployee_master.isapproved = 1 "

            'S.SANDEEP [15 NOV 2016] -- START
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry & " "
            '    End If
            'End If
            'S.SANDEEP [15 NOV 2016] -- END
            

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If


            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            strQ &= " ORDER BY isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Code"))
            objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Name"))

            'Pinkal (30-Jun-2016) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveplanner) </purpose>
    Public Function Insert(ByVal mdtEmployee As DataSet, ByVal mdtEmployeeAsOnDate As DateTime) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END


        'Pinkal (30-Jun-2016) -- Start
        'Enhancement - Working on SL changes on Leave Module.
        Dim objPlannerFraction As New clsplannerday_fraction
        'Pinkal (30-Jun-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation


        'Pinkal (30-Jun-2016) -- Start
        'Enhancement - Working on SL changes on Leave Module.
        'If isExist(objDataOperation, mintEmployeeunkid, mintLeavetypeunkid, mdtStartdate, mdtStopdate, mintLeaveplannerunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Period is already assigned to this employee. Please assign new Leave Period.")
        '    objDataOperation.ReleaseTransaction(False)
        '    Return False
        'End If
        'Pinkal (30-Jun-2016) -- End

        objDataOperation.BindTransaction()

        Try


            strQ = "INSERT INTO lvleaveplanner ( " & _
              "  analysisrefid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", startdate " & _
              ", stopdate " & _
              ", status " & _
              ", remarks " & _
              ", canceldate " & _
              ", cancelremarks " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason" & _
              ", userunkid  " & _
              ", loginemployeeunkid " & _
            ") VALUES (" & _
              "  @analysisrefid " & _
              ", @employeeunkid " & _
              ", @leavetypeunkid " & _
              ", @startdate " & _
              ", @stopdate " & _
              ", @status " & _
              ", @remarks " & _
              ", @canceldate " & _
              ", @cancelremarks " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid " & _
              ", @voidreason" & _
              ", @userunkid" & _
              ", @loginemployeeunkid " & _
            "); SELECT @@identity "


            If mdtEmployee.Tables("Employee").Rows.Count > 0 Then
                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = mintLeavetypeunkid

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.

                '  Dim objEmployee As New clsEmployee_Master

                'For i As Integer = 0 To mdtEmployee.Tables("Employee").Rows.Count - 1

                '    If isExist(objDataOperation, CInt(mdtEmployee.Tables("employee").Rows(i)("employeeunkid")), mintLeavetypeunkid, mdtStartdate, mdtStopdate) Then
                '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Period is already assigned to this employee. Please assign new Leave Period.")
                '        objDataOperation.ReleaseTransaction(False)
                '        Return False
                '    End If

                '    objEmployee._Employeeunkid(mdtEmployeeAsOnDate) = CInt(mdtEmployee.Tables("employee").Rows(i)("employeeunkid"))

                '    If objLeaveType._Gender <> 0 AndAlso objLeaveType._Gender <> objEmployee._Gender Then
                '        mstrMessage = Language.getMessage(mstrModuleName, 2, "This leave type is invalid for some employee(s).")
                '        objDataOperation.ReleaseTransaction(False)
                '        Return False
                '    End If

                '    objDataOperation.ClearParameters()
                '    objDataOperation.AddParameter("@analysisrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisrefid.ToString)
                '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtEmployee.Tables("employee").Rows(i)("employeeunkid").ToString)
                '    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
                '    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
                '    objDataOperation.AddParameter("@stopdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStopdate)
                '    objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
                '    objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)

                '    If mintStatus = 6 Then 'FOR CANCEL LEAVE PLANNER 
                '        objDataOperation.AddParameter("@canceldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                '    Else
                '        objDataOperation.AddParameter("@canceldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                '    End If

                '    objDataOperation.AddParameter("@cancelremarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelremarks.ToString)
                '    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                '    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                '    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                '    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                '    objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

                '    Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                '    If objDataOperation.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If

                '    mintLeaveplannerunkid = dList.Tables(0).Rows(0).Item(0)
                '    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvleaveplanner", "leaveplannerunkid", mintLeaveplannerunkid, False, mintUserunkid) = False Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If
                'Next

                Dim drRow() As DataRow = mdtEmployee.Tables(0).Select("ischecked = True")

                If drRow.Length <= 0 Then
                    objDataOperation.ReleaseTransaction(True)
                    Return True
                End If

                For i As Integer = 0 To drRow.Length - 1

                    If isExist(objDataOperation, CInt(drRow(i)("employeeunkid")), mintLeavetypeunkid, mdtStartdate, mdtStopdate) Then
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave tenure is already assigned to this employee. Please assign new Leave tenure.")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    If objLeaveType._Gender <> 0 AndAlso objLeaveType._Gender <> CInt(drRow(i)("Gender")) Then
                        mstrMessage = Language.getMessage(mstrModuleName, 2, "This leave type is invalid for some employee(s).")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@analysisrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisrefid.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow(i)("employeeunkid")))
                    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
                    objDataOperation.AddParameter("@stopdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStopdate)
                    objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
                    objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)

                    If mintStatus = 6 Then 'FOR CANCEL LEAVE PLANNER 
                        objDataOperation.AddParameter("@canceldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                    Else
                        objDataOperation.AddParameter("@canceldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If

                    objDataOperation.AddParameter("@cancelremarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelremarks.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

                    Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintLeaveplannerunkid = dList.Tables(0).Rows(0).Item(0)
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvleaveplanner", "leaveplannerunkid", mintLeaveplannerunkid, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                    Dim xint As Integer = i
                    Dim emplist = mdtPlannerFraction.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(drRow(xint)("employeeunkid")))
                    objPlannerFraction._dtFraction = emplist.ToList.CopyToDataTable
                    objPlannerFraction._leaveplannerunkid = mintLeaveplannerunkid
                    objPlannerFraction._Userunkid = mintUserunkid
                    objPlannerFraction._Loginemployeeunkid = mintLoginemployeeunkid
                    objPlannerFraction._ClientIP = mstrClientIP
                    objPlannerFraction._FormName = mstrFormName
                    objPlannerFraction._HostName = mstrHostName
                    objPlannerFraction._AuditDate = mdtAuditDate
                    objPlannerFraction._AuditUserId = mintAuditUserId
objPlannerFraction._CompanyUnkid = mintCompanyUnkid
                    objPlannerFraction._FromWeb = mblnIsWeb
                    objPlannerFraction._Isvoid = False
                    If objPlannerFraction.InsertUpdatePlannerDayFraction(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    emplist.ToList.ForEach(Function(x) DeleteRow(x))
                    mdtPlannerFraction.AcceptChanges()
                Next

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveplanner) </purpose>
    Public Function Update(ByVal mdtEmployeeAsOnDate As DateTime) As Boolean
        'Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Pinkal (30-Jun-2016) -- Start
        'Enhancement - Working on SL changes on Leave Module.
        Dim objPlannerFraction As New clsplannerday_fraction
        'Pinkal (30-Jun-2016) -- End
        Try

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

            '
            Dim objEmployee As New clsEmployee_Master
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = mintLeavetypeunkid


        If isExist(objDataOperation, mintEmployeeunkid, mintLeavetypeunkid, mdtStartdate, mdtStopdate, mintLeaveplannerunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave tenure is already assigned to this employee. Please assign new Leave tenure.")
            objDataOperation.ReleaseTransaction(False)
            Return False
        End If

        objEmployee._Employeeunkid(mdtEmployeeAsOnDate) = mintEmployeeunkid

        If objLeaveType._Gender <> 0 AndAlso objLeaveType._Gender <> objEmployee._Gender Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This leave type is invalid for some employee(s).")
            objDataOperation.ReleaseTransaction(False)
            Return False
        End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveplannerunkid.ToString)
            objDataOperation.AddParameter("@analysisrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisrefid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            objDataOperation.AddParameter("@stopdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStopdate)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            If mintStatus = 6 Then
                objDataOperation.AddParameter("@canceldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            Else
                objDataOperation.AddParameter("@canceldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@cancelremarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelremarks.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)

            strQ = "UPDATE lvleaveplanner SET " & _
              "  analysisrefid = @analysisrefid" & _
              ", employeeunkid = @employeeunkid" & _
              ", leavetypeunkid = @leavetypeunkid" & _
              ", startdate = @startdate" & _
              ", stopdate = @stopdate" & _
              ", status = @status" & _
              ", remarks = @remarks" & _
              ", canceldate = @canceldate" & _
              ", cancelremarks = @cancelremarks" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason " & _
              ", userunkid = @userunkid " & _
              ", loginemployeeunkid = @loginemployeeunkid " & _
            "WHERE leaveplannerunkid = @leaveplannerunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "lvleaveplanner", mintLeaveplannerunkid, "leaveplannerunkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveplanner", "leaveplannerunkid", mintLeaveplannerunkid, False, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objPlannerFraction._dtFraction = mdtPlannerFraction
            objPlannerFraction._leaveplannerunkid = mintLeaveplannerunkid
            objPlannerFraction._Userunkid = mintUserunkid
            objPlannerFraction._Loginemployeeunkid = mintLoginemployeeunkid
            objPlannerFraction._ClientIP = mstrClientIP
            objPlannerFraction._FormName = mstrFormName
            objPlannerFraction._HostName = mstrHostName
            objPlannerFraction._AuditDate = mdtAuditDate
            objPlannerFraction._AuditUserId = mintAuditUserId
objPlannerFraction._CompanyUnkid = mintCompanyUnkid
            objPlannerFraction._FromWeb = mblnIsWeb
            objPlannerFraction._Isvoid = False
            If objPlannerFraction.InsertUpdatePlannerDayFraction(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveplanner) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objPlannerDayFraction As New clsplannerday_fraction
        
        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "SELECT ISNULL(plannerfractionunkid,0) AS plannerfractionunkid FROM lvplannerday_fraction  WHERE leaveplannerunkid = @leaveplannerunkid AND isvoid =0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            strQ = " Update lvplannerday_fraction SET " & _
                      " isvoid = 1  " & _
                      ", voiddatetime = GetDate() " & _
                      ", voidreason = @voidreason "

            If mintVoidlogingemployeeunkid > 0 Then
                strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid "
            Else
                strQ &= ", voiduserunkid = @voiduserunkid "
            End If


            strQ &= " WHERE plannerfractionunkid =  @plannerfractionunkid AND isvoid = 0 "


            For Each dr As DataRow In dsList.Tables(0).Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@plannerfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("plannerfractionunkid")))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                If mintVoidlogingemployeeunkid > 0 Then
                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid.ToString)
                Else
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                End If
                Call objDataOperation.ExecNonQuery(strQ)

                objPlannerDayFraction._Plannerfractionunkid(objDataOperation) = CInt(dr("plannerfractionunkid"))
                objPlannerDayFraction._ClientIP = mstrClientIP
                objPlannerDayFraction._FormName = mstrFormName
                objPlannerDayFraction._HostName = mstrHostName
                objPlannerDayFraction._AuditDate = mdtAuditDate
                objPlannerDayFraction._AuditUserId = mintAuditUserId
objPlannerDayFraction._CompanyUnkid = mintCompanyUnkid
                objPlannerDayFraction._FromWeb = mblnIsWeb
                If mintVoidlogingemployeeunkid > 0 Then
                    objPlannerDayFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
                Else
                    objPlannerDayFraction._Voiduserunkid = mintUserunkid
                End If
                If objPlannerDayFraction.InsertAuditTrailForPlannerDays(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next


            strQ = "Update lvleaveplanner set " & _
                      " isvoid = 1 " & _
                      " ,voiddatetime = GetDate() " & _
                      " ,voidreason = @voidreason "

            If mintVoidlogingemployeeunkid > 0 Then
                strQ &= ",voidloginemployeeunkid = @voidloginemployeeunkid "
            Else
                strQ &= " ,voiduserunkid = @voiduserunkid "
            End If

            strQ &= "WHERE leaveplannerunkid = @leaveplannerunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If mintVoidlogingemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid)
            Else
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lvleaveplanner", "leaveplannerunkid", intUnkid, False, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal objDataOperation As clsDataOperation, ByVal Employeeunkid As Integer, ByVal LeaveTypeunkid As Integer, ByVal dtstartdate As DateTime, ByVal dtstopdate As DateTime, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            'Pinkal (27-Aug-2021) -- Start
            'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.

            'strQ = "SELECT " & _
            '  "  leaveplannerunkid " & _
            '  ", analysisrefid " & _
            '  ", employeeunkid " & _
            '  ", leavetypeunkid " & _
            '  ", startdate " & _
            '  ", stopdate " & _
            '  ", status " & _
            '  ", remarks " & _
            '  ", canceldate " & _
            '  ", cancelremarks " & _
            '  ", isvoid " & _
            '  ", voiddatetime " & _
            '  ", voiduserunkid " & _
            '  ", voidreason " & _
            '  ", userunkid " & _
            '  ", loginemployeeunkid " & _
            '  " FROM lvleaveplanner " & _
            '  " WHERE Employeeunkid = @Employeeunkid " & _
            '  " AND (@startdate between convert(char(8),startdate,112) and convert(char(8),stopdate,112) " & _
            '  " OR  @stopdate between convert(char(8),startdate,112) and convert(char(8),stopdate,112)) AND isvoid = 0 "

            strQ = "SELECT " & _
              "  leaveplannerunkid " & _
              ", analysisrefid " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", startdate " & _
              ", stopdate " & _
              ", status " & _
              ", remarks " & _
              ", canceldate " & _
              ", cancelremarks " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
                      " FROM lvleaveplanner " & _
                      " WHERE Employeeunkid = @Employeeunkid AND isvoid = 0 " & _
                      " AND (CONVERT(char(8),startdate,112) BETWEEN @startdate AND @stopdate  " & _
                      " OR    CONVERT(char(8),stopdate,112) BETWEEN @startdate AND @stopdate)  "

            'Pinkal (27-Aug-2021) -- End

          

            If intUnkid > 0 Then
                strQ &= " AND leaveplannerunkid <> @leaveplannerunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtstartdate))
            objDataOperation.AddParameter("@stopdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtstopdate))
            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function DeleteRow(ByVal dr As DataRow) As Boolean
        Try
            dr.Delete()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetImportFileStructure() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT " & _
                     " '' Employeecode " & _
                     ", '' Employee " & _
                     ", '' LeaveType " & _
                     ", '' startdate " & _
                     ", '' enddate "

            dsList = objDataOperation.ExecQuery(strQ, "LeavePlanner")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetImportFileStructure", mstrModuleName)
        End Try
        Return dsList
    End Function


    'Pinkal (27-Aug-2021) -- Start
    'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeFromAllocation(ByVal xUserUnkid As Integer _
                                                                 , ByVal xCompanyUnkid As Integer _
                                                                 , ByVal mdtEmployeeAsOnDate As Date _
                                                                 , ByVal mdtStartDate As Date _
                                                                 , ByVal mdtEndDate As Date _
                                                                 , ByVal dtEmployee As DataTable _
                                                                 , ByRef dtErrorEmp As DataTable) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQMain As String = ""
        Dim mintMaxPlannedLeave As Integer = 0
        Dim mstrLeavePlannerAllocation As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim drRow As DataRow() = Nothing
        Try
            If dtEmployee Is Nothing OrElse dtEmployee.Rows.Count <= 0 Then Return dtTable

            dtTable = dtEmployee.Clone

            Dim objconfig As New clsConfigOptions
            mintMaxPlannedLeave = objconfig.GetKeyValue(xCompanyUnkid, "MaxEmpPlannedLeave")
            mstrLeavePlannerAllocation = objconfig.GetKeyValue(xCompanyUnkid, "MaxEmpLeavePlannerAllocation")
            objconfig = Nothing


            strQMain = "SELECT " & _
                      " COUNT(DISTINCT lvleaveplanner.employeeunkid) as EmpCount " & _
                      " FROM lvleaveplanner " & _
                      " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvleaveplanner.employeeunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT " & _
                      "         stationunkid " & _
                      "        ,deptgroupunkid " & _
                      "        ,departmentunkid " & _
                      "        ,sectiongroupunkid " & _
                      "        ,sectionunkid " & _
                      "        ,unitgroupunkid " & _
                      "        ,unitunkid " & _
                      "        ,teamunkid " & _
                      "        ,classgroupunkid " & _
                      "        ,classunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_transfer_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsOnDate " & _
                      " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "    SELECT " & _
                      "         jobunkid " & _
                      "        ,jobgroupunkid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "    FROM hremployee_categorization_tran " & _
                      "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsOnDate " & _
                      " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                      " WHERE lvleaveplanner.isvoid = 0 AND  CONVERT(CHAR(8),startdate,112) BETWEEN @startDate AND @enddate "


            Dim xCount As Integer = 0
            Dim dctAllocation As New Dictionary(Of String, Integer)
            Dim objEmployee As New clsEmployee_Master
            For Each dr As DataRow In dtEmployee.Rows
                Dim strQC As String = ""
                Dim strQGrp As String = ""
                Dim strQ As String = strQMain
                Dim mstrAllocationIds As String = ""

                objEmployee._Employeeunkid(mdtEmployeeAsOnDate) = CInt(dr("employeeunkid"))

                For Each stralloc As Integer In mstrLeavePlannerAllocation.Split(CChar(","))

                    Select Case CType(stralloc, enAllocation)

                        Case enAllocation.BRANCH
                            strQC &= "AND alloc.stationunkid = " & objEmployee._Stationunkid & " "
                            strQGrp &= ",alloc.stationunkid "
                            mstrAllocationIds &= objEmployee._Stationunkid & "|"

                        Case enAllocation.DEPARTMENT_GROUP
                            strQC &= "AND alloc.deptgroupunkid = " & objEmployee._Deptgroupunkid & " "
                            strQGrp &= ",alloc.deptgroupunkid "
                            mstrAllocationIds &= objEmployee._Deptgroupunkid & "|"

                        Case enAllocation.DEPARTMENT
                            strQC &= "AND alloc.departmentunkid = " & objEmployee._Departmentunkid & " "
                            strQGrp &= ",alloc.departmentunkid "
                            mstrAllocationIds &= objEmployee._Departmentunkid & "|"

                        Case enAllocation.SECTION_GROUP
                            strQC &= "AND alloc.sectiongroupunkid = " & objEmployee._Sectiongroupunkid & " "
                            strQGrp &= ",alloc.sectiongroupunkid "
                            mstrAllocationIds &= objEmployee._Sectiongroupunkid & "|"

                        Case enAllocation.SECTION
                            strQC &= "AND alloc.sectionunkid = " & objEmployee._Sectionunkid & " "
                            strQGrp &= ",alloc.sectionunkid "
                            mstrAllocationIds &= objEmployee._Sectionunkid & "|"

                        Case enAllocation.UNIT_GROUP
                            strQC &= "AND alloc.unitgroupunkid = " & objEmployee._Unitgroupunkid & " "
                            strQGrp &= ",alloc.unitgroupunkid "
                            mstrAllocationIds &= objEmployee._Unitgroupunkid & "|"

                        Case enAllocation.UNIT
                            strQC &= "AND alloc.unitunkid = " & objEmployee._Unitunkid & " "
                            strQGrp &= " ,alloc.unitunkid "
                            mstrAllocationIds &= objEmployee._Unitunkid & "|"

                        Case enAllocation.TEAM
                            strQC &= "AND alloc.teamunkid = " & objEmployee._Teamunkid & " "
                            strQGrp &= ",alloc.teamunkid "
                            mstrAllocationIds &= objEmployee._Teamunkid & "|"

                        Case enAllocation.JOB_GROUP
                            strQC &= "AND Jobs.jobgroupunkid = " & objEmployee._Jobgroupunkid & " "
                            strQGrp &= ",Jobs.jobgroupunkid "
                            mstrAllocationIds &= objEmployee._Jobgroupunkid & "|"

                        Case enAllocation.JOBS
                            strQC &= "AND Jobs.jobunkid = " & objEmployee._Jobunkid & " "
                            strQGrp &= ",Jobs.jobunkid "
                            mstrAllocationIds &= objEmployee._Jobunkid & "|"

                        Case enAllocation.CLASS_GROUP
                            strQC &= "AND alloc.classgroupunkid = " & objEmployee._Classgroupunkid & " "
                            strQGrp &= ",alloc.classgroupunkid "
                            mstrAllocationIds &= objEmployee._Classgroupunkid & "|"

                        Case enAllocation.CLASSES
                            strQC &= "AND alloc.classunkid = " & objEmployee._Classunkid & " "
                            strQGrp &= ",alloc.classunkid "
                            mstrAllocationIds &= objEmployee._Classunkid & "|"

                    End Select

                    mstrAllocationIds = mstrAllocationIds.Trim.Substring(0, mstrAllocationIds.Trim.Length - 1)

                Next


                If strQGrp.Trim.Length > 0 Then
                    strQGrp = "Group By " & strQGrp.Substring(1)
                End If

                strQ &= " " & strQC & " " & strQGrp

                'strQ &= " HAVING COUNT(DISTINCT lvleaveplanner.employeeunkid) > " & mintMaxPlannedLeave 


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
                objDataOperation.AddParameter("@EmployeeAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsOnDate))
                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    If dctAllocation.ContainsKey(mstrAllocationIds) = False Then
                        dctAllocation.Add(mstrAllocationIds, CInt(dsList.Tables(0).Rows(0)("Empcount")))
                    End If

                    If isExist(objDataOperation, CInt(dr("employeeunkid")), -1, mdtStartDate, mdtEndDate, -1) Then
                        dtTable.ImportRow(dr)
                    Else
                        If dctAllocation.ContainsKey(mstrAllocationIds) Then
                            dctAllocation(mstrAllocationIds) += 1
                        End If

                        If dctAllocation(mstrAllocationIds) > mintMaxPlannedLeave Then
                            dtErrorEmp.ImportRow(dr)
                        Else
                            dtTable.ImportRow(dr)
                        End If

                    End If
                Else
                    dtTable.ImportRow(dr)
                End If

            Next

            objEmployee = Nothing

            dctAllocation.Clear()
            dctAllocation = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeFromAllocation; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function
    'Pinkal (27-Aug-2021) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Leave tenure is already assigned to this employee. Please assign new Leave tenure.")
			Language.setMessage(mstrModuleName, 2, "This leave type is invalid for some employee(s).")
			Language.setMessage(mstrModuleName, 3, "Code")
			Language.setMessage(mstrModuleName, 4, "Name")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class