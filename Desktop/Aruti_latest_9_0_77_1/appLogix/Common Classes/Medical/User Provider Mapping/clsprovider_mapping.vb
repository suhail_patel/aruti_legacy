﻿'************************************************************************************************************************************
'Class Name : clsprovider_mapping.vb
'Purpose    :
'Date       :31/01/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsprovider_mapping
    Private Const mstrModuleName = "clsprovider_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMapproviderunkid As Integer
    Private mintProviderunkid As Integer
    Private mintUserunkid As Integer
    Private mdtProvider As DataTable
#End Region

#Region "Constructor"

    Public Sub New()
        mdtProvider = New DataTable("List")
        mdtProvider.Columns.Add("ischecked", Type.GetType("System.Boolean"))
        mdtProvider.Columns.Add("mapproviderunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("instituteunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("institute_name", Type.GetType("System.String"))
        mdtProvider.Columns.Add("userunkid", Type.GetType("System.Int32"))


        'Pinkal (5-MAY-2012) -- Start
        'Enhancement : TRA Changes
        mdtProvider.Columns.Add("countryunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("stateunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("cityunkid", Type.GetType("System.Int32"))
        'Pinkal (5-MAY-2012) -- End


    End Sub

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapproviderunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Mapproviderunkid() As Integer
        Get
            Return mintMapproviderunkid
        End Get
        Set(ByVal value As Integer)
            mintMapproviderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set providerunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Providerunkid() As Integer
        Get
            Return mintProviderunkid
        End Get
        Set(ByVal value As Integer)
            mintProviderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtProvider
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtProvider() As DataTable
        Get
            Return mdtProvider
        End Get
        Set(ByVal value As DataTable)
            mdtProvider = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intProviderId As Integer = -1, Optional ByVal intUserId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mapproviderunkid " & _
              ", providerunkid " & _
              ", userunkid " & _
             " FROM mdprovider_mapping " & _
             " WHERE 1=1"

            If intProviderId > 0 Then
                strQ &= " AND providerunkid = " & intProviderId
            End If

            If intUserId > 0 Then
                strQ &= " AND userunkid = " & intUserId
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdprovider_mapping) </purpose>
    Public Function Insert() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If _dtProvider IsNot Nothing AndAlso _dtProvider.Rows.Count > 0 Then

                For Each dr As DataRow In _dtProvider.Rows

                    If isExist(CInt(dr("instituteunkid")), mintUserunkid) Then
                        If CBool(dr("ischecked")) = False Then
                            Delete(objDataOperation, dr)
                        End If
                        Continue For
                    End If

                    If CBool(dr("ischecked")) Then

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("instituteunkid").ToString)


                        strQ = "INSERT INTO mdprovider_mapping ( " & _
                                  "  providerunkid " & _
                                  ", userunkid" & _
                                ") VALUES (" & _
                                  "  @providerunkid " & _
                                  ", @userunkid" & _
                                "); SELECT @@identity"

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintMapproviderunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mdprovider_mapping", "mapproviderunkid", mintMapproviderunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END


                    End If

                Next

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdprovider_mapping) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal dr As DataRow) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If dr IsNot Nothing Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mdprovider_mapping", "mapproviderunkid", CInt(dr("mapproviderunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


                strQ = "DELETE FROM mdprovider_mapping " & _
                          " WHERE providerunkid = @providerunkid  AND userunkid = @userunkid"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("instituteunkid").ToString)
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intProviderId As Integer, ByVal intUserId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
             "  mapproviderunkid " & _
             ", providerunkid " & _
             ", userunkid " & _
            " FROM mdprovider_mapping " & _
            " WHERE 1=1"

            If intProviderId > 0 Then
                strQ &= " AND providerunkid = " & intProviderId
            End If

            If intUserId > 0 Then
                strQ &= " AND userunkid = " & intUserId
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function



    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUserProvider(ByVal strTableName As String, Optional ByVal intProviderId As Integer = -1, Optional ByVal intUserId As Integer = -1, Optional ByVal isHospital As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mapproviderunkid " & _
              ", providerunkid " & _
              ", hrinstitute_master.institute_code AS Code " & _
              ", hrinstitute_master.institute_name AS name " & _
              ", userunkid " & _
              " FROM mdprovider_mapping " & _
              " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = mdprovider_mapping.providerunkid AND hrinstitute_master.isactive = 1  AND hrinstitute_master.isformrequire = 1 " & _
              " WHERE 1=1"

            If isHospital Then
                strQ &= " AND hrinstitute_master.ishospital = 1 "
            End If

            If intProviderId > 0 Then
                strQ &= " AND providerunkid = " & intProviderId
            End If

            If intUserId > 0 Then
                strQ &= " AND userunkid = " & intUserId
            End If


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            strQ &= "  ORDER BY hrinstitute_master.institute_name "
            'Pinkal (18-Dec-2012) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Pinkal (19-Nov-2012) -- End


End Class