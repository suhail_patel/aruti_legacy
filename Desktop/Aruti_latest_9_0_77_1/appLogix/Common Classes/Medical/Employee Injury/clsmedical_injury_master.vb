﻿'************************************************************************************************************************************
'Class Name : clsmedical_injury_master.vb
'Purpose    : All Employee Injury Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :29/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 2
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsmedical_injury_master
    Private Shared ReadOnly mstrModuleName As String = "clsmedical_injury_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintInjuryunkid As Integer
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintServiceunkid As Integer
    '   Private mintMembershipunkid As Integer
    '  Private mstrMembershipno As String = String.Empty
    Private mstrClaim_No As String = String.Empty
    Private mdtClaim_Date As Date
    Private mstrIllness_Remark As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Nullable(Of DateTime)
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set injuryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Injuryunkid() As Integer
        Get
            Return mintInjuryunkid
        End Get
        Set(ByVal value As Integer)
            mintInjuryunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set serviceunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Serviceunkid() As Integer
        Get
            Return mintServiceunkid
        End Get
        Set(ByVal value As Integer)
            mintServiceunkid = value
        End Set
    End Property


    '''' <summary>
    '''' Purpose: Get or Set membershipunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Membershipunkid() As Integer
    '    Get
    '        Return mintMembershipunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintMembershipunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set membershipno
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Membershipno() As String
    '    Get
    '        Return mstrMembershipno
    '    End Get
    '    Set(ByVal value As String)
    '        mstrMembershipno = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set claim_no
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claim_No() As String
        Get
            Return mstrClaim_No
        End Get
        Set(ByVal value As String)
            mstrClaim_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claim_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claim_Date() As Date
        Get
            Return mdtClaim_Date
        End Get
        Set(ByVal value As Date)
            mdtClaim_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set illness_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Illness_Remark() As String
        Get
            Return mstrIllness_Remark
        End Get
        Set(ByVal value As String)
            mstrIllness_Remark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Nullable(Of DateTime)
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            mdtVoiddatetime = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  injuryunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", serviceunkid " & _
              ", claim_no " & _
              ", claim_date " & _
              ", illness_remark " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
             "FROM mdmedical_injury_master " & _
             "WHERE injuryunkid = @injuryunkid "


            '", membershipunkid " & _
            ' ", membershipno " & _

            objDataOperation.AddParameter("@injuryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintInjuryUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintinjuryunkid = CInt(dtRow.Item("injuryunkid"))
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))

                If dtRow.Item("serviceunkid") IsNot DBNull.Value Then
                    mintServiceunkid = CInt(dtRow.Item("serviceunkid"))
                End If

                ' mintmembershipunkid = CInt(dtRow.Item("membershipunkid"))
                ' mstrmembershipno = dtRow.Item("membershipno").ToString
                mstrclaim_no = dtRow.Item("claim_no").ToString
                mdtclaim_date = dtRow.Item("claim_date")
                mstrillness_remark = dtRow.Item("illness_remark").ToString
                mstrremark = dtRow.Item("remark").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If mdtVoiddatetime.HasValue And Not mdtVoiddatetime Is Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblFirstSurname As Boolean = True) As DataSet
    Public Function GetList(ByVal strDatabaseName As String, _
                            ByVal intUserUnkid As Integer, _
                            ByVal intYearUnkid As Integer, _
                            ByVal intCompanyUnkid As Integer, _
                            ByVal dtPeriodStart As Date, _
                            ByVal dtPeriodEnd As Date, _
                            ByVal strUserModeSetting As String, _
                            ByVal blnOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal mblFirstSurname As Boolean = True) As DataSet
        'Shani(24-Aug-2015) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
        'Shani(24-Aug-2015) -- End

        Try
            strQ = "SELECT " & _
              "  injuryunkid " & _
              ", mdmedical_injury_master.yearunkid " & _
              ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as year " & _
              ", mdmedical_injury_master.periodunkid " & _
              ", cfcommon_period_tran.period_name as period " & _
              ", mdmedical_injury_master.employeeunkid, "

            If mblFirstSurname Then
                strQ &= " isnull(hremployee_master.firstname,'') + isnull(hremployee_master.surname,'') as employee "
            Else
                strQ &= " isnull(hremployee_master.surname,'') + isnull(hremployee_master.firstname,'') as employee "
            End If

            strQ &= ",mdmedical_master.mdmastername " & _
              ", mdmedical_master.mdserviceno as serviceno " & _
              ", claim_no " & _
              ", convert(char(8),claim_date,112) as claim_date " & _
              ", illness_remark " & _
              ", mdmedical_injury_master.remark " & _
              ", mdmedical_injury_master.userunkid " & _
              ", mdmedical_injury_master.isvoid " & _
              ", mdmedical_injury_master.voiduserunkid " & _
              ", mdmedical_injury_master.voiddatetime " & _
             "FROM mdmedical_injury_master " & _
             " left join hremployee_master on hremployee_master.employeeunkid = mdmedical_injury_master.employeeunkid " & _
             " left join " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on cffinancial_year_tran.yearunkid = mdmedical_injury_master.yearunkid " & _
             " left join cfcommon_period_tran on cfcommon_period_tran.periodunkid = mdmedical_injury_master.periodunkid " & _
             " left join mdmedical_master on mdmedical_master.mdmasterunkid = mdmedical_injury_master.serviceunkid"

            ' mdmedical_injury_master.membershipunkid(" & _")
            ' ", hm.membershipname as member " & _
            ' ", membershipno " & _
            'left join hrmembership_master hm on hm.membershipunkid = mdmedical_injury_master.membershipunkid "

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If blnOnlyActive Then
            '    strQ &= " WHERE mdmedical_injury_master.isvoid = 0 "
            'End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'Shani(24-Aug-2015) -- End

            strQ &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND mdmedical_injury_master.isvoid = 0 "
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.


            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    If blnOnlyActive Then
            '        strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '    Else
            '        strQ &= " WHERE hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '    End If
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'strQ &= UserAccessLevel._AccessLevelFilterString
            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If
            'Shani(24-Aug-2015) -- End


            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    'Sohail (06 Jan 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= " AND hremployee_master.isactive = 1 "
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'Sohail (06 Jan 2012) -- End
            'End If
            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If
            'Shani(24-Aug-2015) -- End

            'Pinkal (24-Jun-2011) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_injury_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrClaim_No) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim No is already defined. Please define new Claim No.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@yearunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintyearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServiceunkid.ToString)
            'objDataOperation.AddParameter("@membershipunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmembershipunkid.ToString)
            'objDataOperation.AddParameter("@membershipno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrmembershipno.ToString)
            objDataOperation.AddParameter("@claim_no", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrclaim_no.ToString)
            objDataOperation.AddParameter("@claim_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtClaim_Date)
            objDataOperation.AddParameter("@illness_remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrillness_remark.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime.HasValue And Not mdtVoiddatetime Is Nothing, mdtVoiddatetime, DBNull.Value))

            strQ = "INSERT INTO mdmedical_injury_master ( " & _
              "  yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", serviceunkid " & _
              ", claim_no " & _
              ", claim_date " & _
              ", illness_remark " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime" & _
            ") VALUES (" & _
              "  @yearunkid " & _
              ", @periodunkid " & _
              ", @employeeunkid " & _
              ", @serviceunkid " & _
              ", @claim_no " & _
              ", @claim_date " & _
              ", @illness_remark " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintInjuryunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mdmedical_injury_master", "injuryunkid", mintInjuryunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (mdmedical_injury_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrClaim_No, mintInjuryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim No is already defined. Please define new Claim No.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@injuryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinjuryunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintyearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServiceunkid.ToString)
            ' objDataOperation.AddParameter("@membershipunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmembershipunkid.ToString)
            ' objDataOperation.AddParameter("@membershipno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrmembershipno.ToString)
            objDataOperation.AddParameter("@claim_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaim_No.ToString)
            objDataOperation.AddParameter("@claim_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtclaim_date.ToString)
            objDataOperation.AddParameter("@illness_remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrillness_remark.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime.HasValue And Not mdtVoiddatetime Is Nothing, mdtVoiddatetime, DBNull.Value))

            strQ = "UPDATE mdmedical_injury_master SET " & _
              "  yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", serviceunkid = @serviceunkid" & _
              ", claim_no = @claim_no" & _
              ", claim_date = @claim_date" & _
              ", illness_remark = @illness_remark" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
            "WHERE injuryunkid = @injuryunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "mdmedical_injury_master", mintInjuryunkid, "injuryunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "mdmedical_injury_master", "injuryunkid", mintInjuryunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdmedical_injury_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete selected Claim No. Reason: This Claim No is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = " Update mdmedical_injury_master set isvoid = 1,voiddatetime = @voiddatetime" & _
                   ",voiduserunkid = @voiduserunkid " & _
            "WHERE injuryunkid = @injuryunkid "

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@injuryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mdmedical_injury_master", "injuryunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@injuryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strClaimno As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  injuryunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", serviceunkid " & _
              ", claim_no " & _
              ", claim_date " & _
              ", illness_remark " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
             "FROM mdmedical_injury_master " & _
             "WHERE claim_no = @claim_no "


            If intUnkid > 0 Then
                strQ &= " AND injuryunkid <> @injuryunkid"
            End If

            objDataOperation.AddParameter("@claim_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strClaimno)
            objDataOperation.AddParameter("@injuryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function getListForEmplyeeCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal mblFirstSurname As Boolean = True) As DataSet
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim strQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        If mblFlag = True Then
    '            strQ = "SELECT 0 as employeeunkid, ' ' +  @name  as name UNION "
    '        End If
    '        If mblFirstSurname Then
    '            strQ &= "SELECT employeeunkid, firstname + ' ' + surname as name FROM hremployee_master where isactive = 1 ORDER BY name "
    '        Else
    '            strQ &= "SELECT employeeunkid, surname + ' ' + firstname  as name FROM hremployee_master where isactive = 1 ORDER BY name "
    '        End If

    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Employee"))
    '        dsList = objDataOperation.ExecQuery(strQ, strListName)
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "getListForEmplyeeCombo", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '    End Try

    'End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Claim No is already defined. Please define new Claim No.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class