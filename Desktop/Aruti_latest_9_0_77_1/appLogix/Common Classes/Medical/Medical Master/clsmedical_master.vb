﻿'************************************************************************************************************************************
'Class Name : clsmedical_master.vb
'Purpose    : All Medical Master Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :27/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 



Public Class clsmedical_master
    Private Shared ReadOnly mstrModuleName As String = "clsmedical_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMdmasterunkid As Integer
    Private mstrMdmastercode As String = String.Empty
    Private mstrMdmasteralias As String = String.Empty
    Private mstrMdmastername As String = String.Empty
    Private mstrMddescription As String = String.Empty
    Private mstrMdserviceno As String = String.Empty
    Private mintMdmastertype_Id As Integer
    Private mblnIsactive As Boolean = True
    Private mstrMdmastername1 As String = String.Empty
    Private mstrMdmastername2 As String = String.Empty
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmasterunkid() As Integer
        Get
            Return mintMdmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintMdmasterunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmastercode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmastercode() As String
        Get
            Return mstrMdmastercode
        End Get
        Set(ByVal value As String)
            mstrMdmastercode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmasteralias
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmasteralias() As String
        Get
            Return mstrMdmasteralias
        End Get
        Set(ByVal value As String)
            mstrMdmasteralias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmastername
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmastername() As String
        Get
            Return mstrMdmastername
        End Get
        Set(ByVal value As String)
            mstrMdmastername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mddescription
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mddescription() As String
        Get
            Return mstrMddescription
        End Get
        Set(ByVal value As String)
            mstrMddescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdServiceno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MdServiceno() As String
        Get
            Return mstrMdserviceno
        End Get
        Set(ByVal value As String)
            mstrMdserviceno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmastertype_id
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmastertype_Id() As Integer
        Get
            Return mintMdmastertype_Id
        End Get
        Set(ByVal value As Integer)
            mintMdmastertype_Id = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmastername1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmastername1() As String
        Get
            Return mstrMdmastername1
        End Get
        Set(ByVal value As String)
            mstrMdmastername1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdmastername2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdmastername2() As String
        Get
            Return mstrMdmastername2
        End Get
        Set(ByVal value As String)
            mstrMdmastername2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mdmasterunkid " & _
              ", mdmastercode " & _
              ", mdmasteralias " & _
              ", mdmastername " & _
              ", mddescription " & _
              ", mdserviceno " & _
              ", mdmastertype_id " & _
              ", isactive " & _
              ", mdmastername1 " & _
              ", mdmastername2 " & _
             "FROM mdmedical_master " & _
             "WHERE mdmasterunkid = @mdmasterunkid "

            objDataOperation.AddParameter("@mdmasterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintMdmasterUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintmdmasterunkid = CInt(dtRow.Item("mdmasterunkid"))
                mstrmdmastercode = dtRow.Item("mdmastercode").ToString
                mstrmdmasteralias = dtRow.Item("mdmasteralias").ToString
                mstrmdmastername = dtRow.Item("mdmastername").ToString
                mstrMddescription = dtRow.Item("mddescription").ToString
                mstrMdserviceno = IIf(dtRow.Item("mdserviceno").ToString <> DBNull.Value.ToString, dtRow.Item("mdserviceno").ToString, "")
                mintmdmastertype_id = CInt(dtRow.Item("mdmastertype_id"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrmdmastername1 = dtRow.Item("mdmastername1").ToString
                mstrmdmastername2 = dtRow.Item("mdmastername2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mdmasterunkid " & _
              ", mdmastercode " & _
              ", mdmasteralias " & _
              ", mdmastername " & _
              ", mddescription " & _
              ", mdserviceno " & _
              ", mdmastertype_id " & _
              ", isactive " & _
              ", mdmastername1 " & _
              ", mdmastername2 " & _
             "FROM mdmedical_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrMdmastercode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        ElseIf isExist("", mstrMdmastername) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@mdmastercode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastercode.ToString)
            objDataOperation.AddParameter("@mdmasteralias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmasteralias.ToString)
            objDataOperation.AddParameter("@mdmastername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastername.ToString)
            objDataOperation.AddParameter("@mddescription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMddescription.ToString)
            objDataOperation.AddParameter("@mdserviceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdserviceno.ToString)
            objDataOperation.AddParameter("@mdmastertype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMdmastertype_Id.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@mdmastername1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastername1.ToString)
            objDataOperation.AddParameter("@mdmastername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastername2.ToString)

            strQ = "INSERT INTO mdmedical_master ( " & _
              "  mdmastercode " & _
              ", mdmasteralias " & _
              ", mdmastername " & _
              ", mddescription " & _
              ", mdserviceno " & _
              ", mdmastertype_id " & _
              ", isactive " & _
              ", mdmastername1 " & _
              ", mdmastername2" & _
            ") VALUES (" & _
              "  @mdmastercode " & _
              ", @mdmasteralias " & _
              ", @mdmastername " & _
              ", @mddescription " & _
              ", @mdserviceno " & _
              ", @mdmastertype_id " & _
              ", @isactive " & _
              ", @mdmastername1 " & _
              ", @mdmastername2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMdmasterunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mdmedical_master", "mdmasterunkid", mintMdmasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (mdmedical_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mstrMdmastercode, "", mintMdmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        ElseIf isExist("", mstrMdmastername, mintMdmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@mdmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMdmasterunkid.ToString)
            objDataOperation.AddParameter("@mdmastercode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastercode.ToString)
            objDataOperation.AddParameter("@mdmasteralias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmasteralias.ToString)
            objDataOperation.AddParameter("@mdmastername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastername.ToString)
            objDataOperation.AddParameter("@mddescription", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMddescription.ToString)
            objDataOperation.AddParameter("@mdserviceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdserviceno.ToString)
            objDataOperation.AddParameter("@mdmastertype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMdmastertype_Id.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@mdmastername1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastername1.ToString)
            objDataOperation.AddParameter("@mdmastername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMdmastername2.ToString)

            strQ = "UPDATE mdmedical_master SET " & _
              "  mdmastercode = @mdmastercode" & _
              ", mdmasteralias = @mdmasteralias" & _
              ", mdmastername = @mdmastername" & _
              ", mddescription = @mddescription" & _
              ", mdserviceno = @mdserviceno" & _
              ", mdmastertype_id = @mdmastertype_id" & _
              ", isactive = @isactive" & _
              ", mdmastername1 = @mdmastername1" & _
              ", mdmastername2 = @mdmastername2 " & _
            "WHERE mdmasterunkid = @mdmasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "mdmedical_master", mintMdmasterunkid, "mdmasterunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "mdmedical_master", "mdmasterunkid", mintMdmasterunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdmedical_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Master Type. Reason: This Master Type is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "UPDATE mdmedical_master SET isactive = 0 " & _
                    "WHERE mdmasterunkid = @mdmasterunkid "

            objDataOperation.AddParameter("@mdmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mdmedical_master", "mdmasterunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT  " & _
                    "TABLE_NAME AS TableName  " & _
                    ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('mdcategorymasterunkid','covermasterunkid','serviceunkid','treatmentunkid') "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@mdmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "mdmedical_master" Or dtRow.Item("TableName") = "hremployee_master" Or dtRow.Item("TableName") = "athremployee_master" Then Continue For
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @mdmasterunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If

            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  mdmasterunkid " & _
              ", mdmastercode " & _
              ", mdmasteralias " & _
              ", mdmastername " & _
              ", mddescription " & _
              ", mdserviceno " & _
              ", mdmastertype_id " & _
              ", isactive " & _
              ", mdmastername1 " & _
              ", mdmastername2 " & _
             "FROM mdmedical_master " & _
             "WHERE 1=1 AND mdmastertype_id = @mdmastertype_id"

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND mdmastercode = @mdmastercode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND mdmastername = @mdmastername "
            End If

            If intUnkid > 0 Then
                strQ &= " AND mdmasterunkid <> @mdmasterunkid"
            End If

            objDataOperation.AddParameter("@mdmastercode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@mdmastername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@mdmastertype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMdmastertype_Id)
            objDataOperation.AddParameter("@mdmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal intMasterTypeId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as mdmasterunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT mdmasterunkid, mdmastername as name FROM mdmedical_master where isactive = 1 and mdmastertype_id = @mdmastertype_id ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@mdmastertype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeId)
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class