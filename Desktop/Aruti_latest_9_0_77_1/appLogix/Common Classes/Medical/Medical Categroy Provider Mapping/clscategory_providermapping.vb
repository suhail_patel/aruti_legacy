﻿'************************************************************************************************************************************
'Class Name : clscategory_providermapping.vb
'Purpose    :
'Date       :09/09/2014
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clscategory_providermapping
    Private Const mstrModuleName = "clscategory_providermapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMdcateogryproviderunkid As Integer
    Private mintMedicalcategoryunkid As Integer
    Private mintMdcategorymasterunkid As Integer
    Private mintProviderunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mdtProvider As DataTable
#End Region

#Region "Constructor"

    Public Sub New()
        mdtProvider = New DataTable("List")
        mdtProvider.Columns.Add("ischecked", Type.GetType("System.Boolean"))
        mdtProvider.Columns.Add("mdcateogryproviderunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("medicalcategoryunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("mdcategorymasterunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("instituteunkid", Type.GetType("System.Int32"))
        mdtProvider.Columns.Add("institute_name", Type.GetType("System.String"))
    End Sub

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdcateogryproviderunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdcateogryproviderunkid() As Integer
        Get
            Return mintMdcateogryproviderunkid
        End Get
        Set(ByVal value As Integer)
            mintMdcateogryproviderunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set medicalcategoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Medicalcategoryunkid() As Integer
        Get
            Return mintMedicalcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintMedicalcategoryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdcategorymasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdcategorymasterunkid() As Integer
        Get
            Return mintMdcategorymasterunkid
        End Get
        Set(ByVal value As Integer)
            mintMdcategorymasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set providerunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Providerunkid() As Integer
        Get
            Return mintProviderunkid
        End Get
        Set(ByVal value As Integer)
            mintProviderunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtProvider
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtProvider() As DataTable
        Get
            Return mdtProvider
        End Get
        Set(ByVal value As DataTable)
            mdtProvider = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  mdcateogryproviderunkid " & _
              ", medicalcategoryunkid " & _
              ", mdcategorymasterunkid " & _
              ", providerunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
             "FROM mdcategory_providermapping " & _
             "WHERE mdcateogryproviderunkid = @mdcateogryproviderunkid "

            objDataOperation.AddParameter("@mdcateogryproviderunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintMdcateogryproviderUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintmdcateogryproviderunkid = CInt(dtRow.Item("mdcateogryproviderunkid"))
                mintmedicalcategoryunkid = CInt(dtRow.Item("medicalcategoryunkid"))
                mintmdcategorymasterunkid = CInt(dtRow.Item("mdcategorymasterunkid"))
                mintproviderunkid = CInt(dtRow.Item("providerunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mstrvoidreason = dtRow.Item("voidreason").ToString
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  mdcateogryproviderunkid " & _
                      ", medicalcategoryunkid " & _
                      ", mdcategorymasterunkid " & _
                      ", providerunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                     "FROM mdcategory_providermapping "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdcategory_providermapping) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If _dtProvider IsNot Nothing AndAlso _dtProvider.Rows.Count > 0 Then

                For Each dr As DataRow In _dtProvider.Rows

                    If isExist(CInt(dr("medicalcategoryunkid")), CInt(dr("mdcategorymasterunkid")), CInt(dr("instituteunkid"))) Then
                        If CBool(dr("ischecked")) = False Then
                            Delete(objDataOperation, CInt(dr("mdcateogryproviderunkid")))
                        End If
                        Continue For
                    End If

                    If CBool(dr("ischecked")) Then
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("medicalcategoryunkid")))
                        objDataOperation.AddParameter("@mdcategorymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("mdcategorymasterunkid")))
                        objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("instituteunkid")))
                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

                        strQ = "INSERT INTO mdcategory_providermapping ( " & _
                                  "  medicalcategoryunkid " & _
                                  ", mdcategorymasterunkid " & _
                                  ", providerunkid " & _
                                  ", userunkid " & _
                                  ", isvoid " & _
                                  ", voiddatetime " & _
                                  ", voidreason " & _
                                  ", voiduserunkid" & _
                                ") VALUES (" & _
                                  "  @medicalcategoryunkid " & _
                                  ", @mdcategorymasterunkid " & _
                                  ", @providerunkid " & _
                                  ", @userunkid " & _
                                  ", @isvoid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason " & _
                                  ", @voiduserunkid" & _
                                "); SELECT @@identity"

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintMdcateogryproviderunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mdcategory_providermapping", "mdcateogryproviderunkid", mintMdcateogryproviderunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END


                    End If

                Next

            End If
            
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdcategory_providermapping) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "Update mdcategory_providermapping SET isvoid = 1,voiddatetime = @voiddatetime,voiduserunkid = @voiduserunkid,voidreason = @voidreason " & _
            "WHERE mdcateogryproviderunkid = @mdcateogryproviderunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@mdcateogryproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mdcategory_providermapping", "mdcateogryproviderunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@mdcateogryproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intmedicalcategoryunkid As Integer, ByVal intcategorymasterunkid As Integer, ByVal intProviderId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  mdcateogryproviderunkid " & _
                      ", medicalcategoryunkid " & _
                      ", mdcategorymasterunkid " & _
                      ", providerunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                     "FROM mdcategory_providermapping " & _
                     "WHERE isvoid = 0 AND mdcategorymasterunkid = @categorymasterunkid " & _
                     "AND providerunkid = @providerunkid "

            If intmedicalcategoryunkid > 0 Then
                strQ &= " AND medicalcategoryunkid = @medicalcategoryunkid "
                objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intmedicalcategoryunkid)
            End If

            objDataOperation.AddParameter("@categorymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcategorymasterunkid)
            objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProviderId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetProviderMappingId(ByVal intmedicalcategoryunkid As Integer, ByVal intcategorymasterunkid As Integer, ByVal intProviderId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intProviderMappingID As Integer = -1
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  mdcateogryproviderunkid " & _
                      ", medicalcategoryunkid " & _
                      ", mdcategorymasterunkid " & _
                      ", providerunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                     "FROM mdcategory_providermapping " & _
                     "WHERE isvoid = 0 AND mdcategorymasterunkid = @categorymasterunkid " & _
                     "AND providerunkid = @providerunkid "

            If intmedicalcategoryunkid > 0 Then
                strQ &= " AND medicalcategoryunkid = @medicalcategoryunkid "
                objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intmedicalcategoryunkid)
            End If

            objDataOperation.AddParameter("@categorymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcategorymasterunkid)
            objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProviderId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intProviderMappingID = CInt(dsList.Tables(0).Rows(0)("mdcateogryproviderunkid"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return intProviderMappingID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetDistinctProviderFromMapping(Optional ByVal intcategorymasterunkid As Integer = 0, Optional ByVal intProviderId As Integer = 0) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intProviderMappingID As Integer = -1
        Dim exForce As Exception
        Dim dtProvider As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = " SELECT DISTINCT " & _
                      " mdcategory_providermapping.providerunkid " & _
                      ",ISNULL(institute_name, '') AS Provider " & _
                      " FROM mdcategory_providermapping " & _
                      " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = mdcategory_providermapping.providerunkid  AND hrinstitute_master.ishospital = 1 " & _
                      " JOIN mdmedical_master ON mdcategory_providermapping.mdcategorymasterunkid = mdmedical_master.mdmasterunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Medical_Category & _
                      " WHERE mdcategory_providermapping.isvoid = 0 "

            If intcategorymasterunkid > 0 Then
                strQ &= " AND mdcategory_providermapping.mdcategorymasterunkid = @categorymasterunkid "
                objDataOperation.AddParameter("@categorymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcategorymasterunkid)
            End If

            If intcategorymasterunkid > 0 Then
                strQ &= " AND mdcategory_providermapping.providerunkid = @providerunkid "
                objDataOperation.AddParameter("@providerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProviderId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then dtProvider = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDistinctProviderFromMapping; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dtProvider
    End Function


End Class