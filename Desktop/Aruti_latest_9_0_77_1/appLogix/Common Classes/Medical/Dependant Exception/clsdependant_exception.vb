﻿'************************************************************************************************************************************
'Class Name : clsdependant_exception.vb
'Purpose    :
'Date       :08/03/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsdependant_exception
    Private Shared ReadOnly mstrModuleName As String = "clsdependant_exception"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintExceptionunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintDependantunkid As Integer
    Private mintRelationunkid As Integer
    Private mintUserunkid As Integer
    Private mintvoiduserunkid As Integer
    Private mdtvoiddatetime As DateTime
    Private mstrvoidreason As String = String.Empty
    Private mdtDependants As DataTable = Nothing


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Private mblnIsMedical As Boolean = False
    Private mdtMedicalStopDate As DateTime = Nothing
    Private mblnIsLeave As Boolean = False
    Private mdtLeaveStopDate As DateTime = Nothing
    'Pinkal (21-Jul-2014) -- End


#End Region

#Region " Constructor "

    Public Sub New()
        mdtDependants = New DataTable("Dependant")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("Exceptionunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Ischecked")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Dependantunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Particular")
            dCol.DataType = System.Type.GetType("System.String")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Relationunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Relation")
            dCol.DataType = System.Type.GetType("System.String")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Isgroup")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("Isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtDependants.Columns.Add(dCol)


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            dCol = New DataColumn("birthdate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("IsMedical")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("MedicalStopdate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("MedicalImage")
            dCol.DataType = System.Type.GetType("System.Object")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("IsLeave")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("LeaveStopdate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtDependants.Columns.Add(dCol)

            dCol = New DataColumn("LeaveImage")
            dCol.DataType = System.Type.GetType("System.Object")
            mdtDependants.Columns.Add(dCol)

            'Pinkal (21-Jul-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exceptionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Exceptionunkid() As Integer
        Get
            Return mintExceptionunkid
        End Get
        Set(ByVal value As Integer)
            mintExceptionunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Dependantunkid() As Integer
        Get
            Return mintDependantunkid
        End Get
        Set(ByVal value As Integer)
            mintDependantunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relationunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Relationunkid() As Integer
        Get
            Return mintRelationunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtDependant
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtDependant() As DataTable
        Get
            Return mdtDependants
        End Get
        Set(ByVal value As DataTable)
            mdtDependants = value
        End Set
    End Property


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16


    ''' <summary>
    ''' Purpose: Get or Set IsMedical
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsMedical() As Boolean
        Get
            Return mblnIsMedical
        End Get
        Set(ByVal value As Boolean)
            mblnIsMedical = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MedicalStopdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MedicalStopDate() As DateTime
        Get
            Return mdtMedicalStopDate
        End Get
        Set(ByVal value As DateTime)
            mdtMedicalStopDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsLeave
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsLeave() As Boolean
        Get
            Return mblnIsLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsLeave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveStopdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LeaveStopDate() As DateTime
        Get
            Return mdtLeaveStopDate
        End Get
        Set(ByVal value As DateTime)
            mdtLeaveStopDate = value
        End Set
    End Property

    'Pinkal (21-Jul-2014) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  exceptionunkid " & _
              ", employeeunkid " & _
              ", dependantunkid " & _
              ", relationunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(ismedical,0) AS ismedical " & _
              ", mdstopdate " & _
              ", ISNULL(isleave,0) AS isleave " & _
              ", lvstopdate " & _
             "FROM mddependant_exception " & _
             "WHERE exceptionunkid = @exceptionunkid "

            objDataOperation.AddParameter("@exceptionunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintExceptionUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintexceptionunkid = CInt(dtRow.Item("exceptionunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintdependantunkid = CInt(dtRow.Item("dependantunkid"))
                mintRelationunkid = CInt(dtRow.Item("relationunkid"))


                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

                If IsDBNull(dtRow.Item("ismedical")) = False Then
                    mblnIsMedical = CBool(dtRow.Item("ismedical"))
                End If

                If IsDBNull(dtRow.Item("mdstopdate")) = False Then
                    mdtMedicalStopDate = CDate(dtRow.Item("mdstopdate"))
                End If

                If IsDBNull(dtRow.Item("isleave")) = False Then
                    mblnIsLeave = CBool(dtRow.Item("isleave"))
                End If

                If IsDBNull(dtRow.Item("lvstopdate")) = False Then
                    mdtLeaveStopDate = CDate(dtRow.Item("lvstopdate"))
                End If
                'Pinkal (21-Jul-2014) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  exceptionunkid " & _
              ", employeeunkid " & _
              ", dependantunkid " & _
              ", relationunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(ismedical,0) AS ismedical " & _
              ", mdstopdate " & _
              ", ISNULL(isleave,0) AS isleave " & _
              ", lvstopdate " & _
             "FROM mddependant_exception "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mddependant_exception) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            
            strQ = "INSERT INTO mddependant_exception ( " & _
                      "  employeeunkid " & _
                      ", dependantunkid " & _
                      ", relationunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", ismedical" & _
                      ", mdstopdate " & _
                      ", isleave " & _
                      ", lvstopdate " & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @dependantunkid " & _
                      ", @relationunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                      ", @ismedical" & _
                      ", @mdstopdate " & _
                      ", @isleave " & _
                      ", @lvstopdate " & _
                    "); SELECT @@identity"


            mdtDependants = New DataView(mdtDependants, "dependantunkid > 0", "", DataViewRowState.CurrentRows).ToTable()

            For Each dr As DataRow In mdtDependants.Rows

                If isExist(CInt(dr("employeeunkid")), CInt(dr("dependantunkid"))) Then
                    If CBool(dr("ischecked")) = False Then
                        If Delete(objDataOperation, CInt(dr("employeeunkid")), CInt(dr("dependantunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Pinkal (21-Jul-2014) -- Start
                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

                    ElseIf CBool(dr("ischecked")) AndAlso CInt(dr("Exceptionunkid")) > 0 Then
                        _Exceptionunkid = CInt(dr("Exceptionunkid"))
                        If mdtMedicalStopDate = Nothing AndAlso IsDBNull(dr("MedicalStopdate")) Then
                            Continue For
                        ElseIf mdtMedicalStopDate = Nothing AndAlso IsDBNull(dr("MedicalStopdate")) = False Then
                            If UpdateStopDate(objDataOperation, enModuleReference.Medical, CBool(dr("IsMedical")), CDate(dr("MedicalStopdate")).Date, mintEmployeeunkid, mintDependantunkid, mintRelationunkid) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        ElseIf IsDBNull(dr("MedicalStopdate")) OrElse mdtMedicalStopDate.Date <> CDate(dr("MedicalStopdate")).Date Then
                            If UpdateStopDate(objDataOperation, enModuleReference.Medical, CBool(dr("IsMedical")), IIf(IsDBNull(dr("MedicalStopdate")), Nothing, dr("MedicalStopdate")), mintEmployeeunkid, mintDependantunkid, mintRelationunkid) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        ElseIf mdtMedicalStopDate <> Nothing AndAlso IsDBNull(dr("MedicalStopdate")) = False Then
                            If UpdateStopDate(objDataOperation, enModuleReference.Medical, CBool(dr("IsMedical")), IIf(IsDBNull(dr("MedicalStopdate")), Nothing, dr("MedicalStopdate")), mintEmployeeunkid, mintDependantunkid, mintRelationunkid) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If


                        If mdtLeaveStopDate = Nothing AndAlso IsDBNull(dr("LeaveStopdate")) Then
                            Continue For
                        ElseIf mdtLeaveStopDate = Nothing AndAlso IsDBNull(dr("LeaveStopdate")) = False Then
                            If UpdateStopDate(objDataOperation, enModuleReference.Leave, CBool(dr("IsLeave")), CDate(dr("LeaveStopdate")).Date, mintEmployeeunkid, mintDependantunkid, mintRelationunkid) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        ElseIf IsDBNull(dr("LeaveStopdate")) OrElse mdtLeaveStopDate.Date <> CDate(dr("LeaveStopdate")).Date Then
                            If UpdateStopDate(objDataOperation, enModuleReference.Leave, CBool(dr("IsLeave")), IIf(IsDBNull(dr("LeaveStopdate")), Nothing, dr("LeaveStopdate")), mintEmployeeunkid, mintDependantunkid, mintRelationunkid) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        ElseIf mdtLeaveStopDate <> Nothing AndAlso IsDBNull(dr("LeaveStopdate")) = False Then
                            If UpdateStopDate(objDataOperation, enModuleReference.Leave, CBool(dr("IsLeave")), IIf(IsDBNull(dr("LeaveStopdate")), Nothing, dr("LeaveStopdate")), mintEmployeeunkid, mintDependantunkid, mintRelationunkid) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If

                        'Pinkal (21-Jul-2014) -- End

                    End If
                    Continue For
                End If

                If CBool(dr("ischecked")) = True Then

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("Employeeunkid").ToString)
                    objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("dependantunkid").ToString)
                    objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("relationunkid").ToString)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtvoiddatetime <> Nothing, mdtvoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason)


                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    objDataOperation.AddParameter("@ismedical", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dr("IsMedical")))
                    objDataOperation.AddParameter("@mdstopdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(dr("MedicalStopdate")), DBNull.Value, dr("MedicalStopdate")))
                    objDataOperation.AddParameter("@isleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dr("IsLeave")))
                    objDataOperation.AddParameter("@lvstopdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(IsDBNull(dr("LeaveStopdate")), DBNull.Value, dr("LeaveStopdate")))
                    'Pinkal (21-Jul-2014) -- End


                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintExceptionunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mddependant_exception", "exceptionunkid", mintExceptionunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mddependant_exception) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal intDependantID As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception = Nothing
        Try

            StrQ = " Select ISNULL(exceptionunkid,0) exceptionunkid FROM mddependant_exception WHERE employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND isvoid = 0"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependantID)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim intExceptionID As Integer = dsList.Tables(0).Rows(0)("exceptionunkid")

            If intExceptionID > 0 Then

                StrQ = " Update  mddependant_exception set " & _
                           " isvoid = 1 " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voidreason = @voidreason " & _
                           " WHERE employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND isvoid = 0"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependantID)
                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mddependant_exception", "exceptionunkid", intExceptionID) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@exceptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, ByVal intDependant As Integer, Optional ByVal intRelationId As Integer = -1, Optional ByVal intExceptionId As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  exceptionunkid " & _
              ", employeeunkid " & _
              ", dependantunkid " & _
              ", relationunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ismedical" & _
              ", mdstopdate " & _
              ", isleave " & _
              ", lvstopdate " & _
              " FROM mddependant_exception " & _
              " WHERE employeeunkid = @employeeunkid " & _
              "AND dependantunkid = @dependantunkid AND isvoid = 0"


            If intExceptionId > 0 Then
                strQ &= " AND relationunkid <> @relationunkid"
                objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRelationId)
            End If

            If intExceptionId > 0 Then
                strQ &= " AND exceptionunkid <> @exceptionunkid"
                objDataOperation.AddParameter("@exceptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExceptionId)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependant)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetDependantException(ByVal intEmpId As Integer, Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal intDependant As Integer = -1, Optional ByVal intRelationId As Integer = -1, Optional ByVal dtAsonDate As Date = Nothing) As DataTable
        'Sohail (18 May 2019) - [dtAsonDate]
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = String.Empty
        Try

            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If intEmpId > 0 Then
                strQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If intEmpId > 0 Then
                strQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid "
            End If

            If dtAsonDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsonDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End
            strQ &= " SELECT  mddependant_exception.exceptionunkid " & _
                      ",mddependant_exception.employeeunkid " & _
                      ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '  + ISNULL(hremployee_master.surname, '') AS empName " & _
                      ",mddependant_exception.dependantunkid " & _
                      ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS depedant " & _
                      ",mddependant_exception.relationunkid " & _
                      ", ISNULL(cfcommon_master.name, '') AS relation " & _
                      ", YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) AS age " & _
                        ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                      "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                      "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                      " ELSE  ''  " & _
                      " END as gender " & _
                      ", ismedical" & _
                      ", mdstopdate " & _
                      ", isleave " & _
                      ", lvstopdate " & _
                      " FROM  mddependant_exception " & _
                      " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                      " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                      "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                      " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                      " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1 AND cfcommon_master.ismedicalbenefit = 1 " & _
                      "WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                      "AND #TableDepn.isactive = 1 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

            If intDependant > 0 Then
                strQ &= " AND mddependant_exception.dependantunkid = @dependantunkid"
                objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependant)
            End If

            If intRelationId > 0 Then
                strQ &= " AND mddependant_exception.relationunkid = @relationunkid"
                objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRelationId)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 8, "Female"))
            objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 9, "Other"))

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables(0).Rows.Count > 0 Then
                dtTable = dsList.Tables(0)
            ElseIf dsList IsNot Nothing Then
                dtTable = dsList.Tables(0).Clone
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDependantException", mstrModuleName)
        End Try
        Return dtTable
    End Function


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    Public Function UpdateStopDate(ByVal objDataOperation As clsDataOperation, ByVal intModuelRefId As Integer, ByVal blnFlag As Boolean, ByVal dtStopdate As Date, ByVal intEmpId As Integer, ByVal intDependant As Integer, ByVal intRelationId As Integer) As Boolean
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If intModuelRefId = enModuleReference.Medical Then
                If blnFlag Then
                    strQ = " Update mddependant_exception set mdstopdate = @stopdate WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
                Else
                    strQ = " Update mddependant_exception set mdstopdate = NULL,ismedical = 0 WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
                End If
            ElseIf intModuelRefId = enModuleReference.Leave Then
                If blnFlag Then
                    strQ = " Update mddependant_exception set lvstopdate = @stopdate WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
                Else
                    strQ = " Update mddependant_exception set lvstopdate = NULL,isleave = 0 WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
                End If
            End If
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependant)
                objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRelationId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@stopdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(dtStopdate <> Nothing, dtStopdate, DBNull.Value))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim intException As Integer = GetExceptionId(intEmpId, intDependant, intRelationId, objDataOperation)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "mddependant_exception", "exceptionunkid", intException) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateStopDate; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function GetDependantModuleWiseStopDate(ByVal intModuleRefId As Integer, ByVal intEmpId As Integer, ByVal intDependant As Integer, ByVal intRelationId As Integer, Optional ByVal objDataOperation As clsDataOperation = Nothing) As DateTime
        Dim dtDate As DateTime = Nothing
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            If intModuleRefId = enModuleReference.Medical Then
                strQ = " SELECT mdstopdate From mddependant_exception WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
            ElseIf intModuleRefId = enModuleReference.Leave Then
                strQ = " SELECT lvstopdate From mddependant_exception WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependant)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRelationId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If intModuleRefId = enModuleReference.Medical Then
                    If IsDBNull(dsList.Tables(0).Rows(0)("mdstopdate")) = False Then
                        dtDate = CDate(dsList.Tables(0).Rows(0)("mdstopdate")).Date
                    End If
                ElseIf intModuleRefId = enModuleReference.Leave Then
                    If IsDBNull(dsList.Tables(0).Rows(0)("lvstopdate")) = False Then
                        dtDate = CDate(dsList.Tables(0).Rows(0)("lvstopdate")).Date
                    End If
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDependantModuleWiseStopDate; Module Name: " & mstrModuleName)
        End Try
        Return dtDate
    End Function

    Public Function GetDependantExceptionModule(ByVal intModuleRefId As Integer, ByVal intEmpId As Integer, ByVal intDependant As Integer, _
                                                                          ByVal intRelationId As Integer, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            If intModuleRefId = enModuleReference.Medical Then
                strQ = " SELECT ISNULL(ismedical,0) AS ismedical  From mddependant_exception WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
            ElseIf intModuleRefId = enModuleReference.Leave Then
                strQ = " SELECT ISNULL(isleave,0) AS isleave From mddependant_exception WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependant)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRelationId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If intModuleRefId = enModuleReference.Medical Then
                    mblnFlag = CBool(dsList.Tables(0).Rows(0)("ismedical"))
                ElseIf intModuleRefId = enModuleReference.Leave Then
                    mblnFlag = CBool(dsList.Tables(0).Rows(0)("isleave"))
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDependantExceptionModule; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    Public Function GetExceptionId(ByVal intEmpId As Integer, ByVal intDependant As Integer, ByVal intRelationId As Integer, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Dim intExceptionId As Integer = -1
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            strQ = " SELECT exceptionunkid From mddependant_exception WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND dependantunkid = @dependantunkid AND relationunkid = @relationunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependant)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRelationId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If IsDBNull(dsList.Tables(0).Rows(0)("exceptionunkid")) = False Then
                    intExceptionId = CInt(dsList.Tables(0).Rows(0)("exceptionunkid"))
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExceptionId; Module Name: " & mstrModuleName)
        End Try
        Return intExceptionId
    End Function

    'Pinkal (21-Jul-2014) -- End




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsEmployee_Master", 7, "Male")
			Language.setMessage("clsEmployee_Master", 8, "Female")
			Language.setMessage("clsEmployee_Master", 9, "Other")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class