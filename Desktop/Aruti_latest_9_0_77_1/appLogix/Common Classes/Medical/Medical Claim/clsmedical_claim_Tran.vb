﻿'************************************************************************************************************************************
'Class Name : clsmedical_claim_Tran.vb
'Purpose    :
'Date       :20/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsmedical_claim_Tran
    Private Const mstrModuleName = "clsmedical_claim_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintClaimunkid As Integer
    Private mintUserunkid As Integer
    Private mintClaimtranunkid As Integer
    Private mdtTran As DataTable
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Claimtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimtranunkid() As Integer
        Get
            Return mintClaimtranunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimunkid() As Integer
        Get
            Return mintClaimunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataList
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property


#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("MedicalClaim")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("claimtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("claimunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dependantsunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dependentsname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("periodunkid")
            'dCol.DataType = System.Type.GetType("System.Int32")
            'mdtTran.Columns.Add(dCol)

            'dCol = New DataColumn("period_name")
            'dCol.DataType = System.Type.GetType("System.String")
            'mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("sicksheetunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("sicksheetno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("claimno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("claimdate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("remarks")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("isempexempted")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            'Pinkal (18-Dec-2012) -- End

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''Public Function GetClaimTran(Optional ByVal blnIncludeInactiveEmployee As String = "" _
    '''                            , Optional ByVal strEmployeeAsOnDate As String = "" _
    '''                            , Optional ByVal strUserAccessLevelFilterString As String = "" _
    '''                            , Optional ByVal intUserUnkId As Integer = 0) As DataSet    'Pinkal (25-APR-2012) -- Start
    Public Function GetClaimTran(ByVal strDatabaseName As String, _
                                 ByVal intUserUnkid As Integer, _
                                 ByVal intYearUnkid As Integer, _
                                 ByVal intCompanyUnkid As Integer, _
                                 ByVal dtPeriodStart As Date, _
                                 ByVal dtPeriodEnd As Date, _
                                 ByVal strUserModeSetting As String, _
                                 ByVal blnOnlyApproved As Boolean, _
                                 ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                 ByVal intMedicalClaimUnkId As Integer) As DataSet
        'Shani(24-Aug-2015) -- End

        ' Public Function GetClaimTran() As DataSet


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'Shani(24-Aug-2015) -- End


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '          "  claimtranunkid " & _
            '          ", mdmedical_claim_tran.claimunkid " & _
            '          ", mdmedical_claim_tran.employeeunkid " & _
            '          ", isnull(hremployee_master.firstname,'') + ' ' +  isnull(hremployee_master.othername,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
            '          ", mdmedical_claim_tran.dependantsunkid " & _
            '          ", isnull(hrdependants_beneficiaries_tran.first_name,'') + ' ' + isnull(hrdependants_beneficiaries_tran.last_name,'') as dependentsname " & _
            '          ", mdmedical_claim_master.periodunkid " & _
            '          ", cfcommon_period_tran.period_name " & _
            '          ", mdmedical_claim_tran.sicksheetunkid " & _
            '          ", mdmedical_sicksheet.sicksheetno " & _
            '          ", mdmedical_claim_tran.claimno " & _
            '          ", mdmedical_claim_tran.claimdate " & _
            '          ", mdmedical_claim_tran.amount " & _
            '          ", mdmedical_claim_tran.remarks " & _
            '          ", mdmedical_claim_master.invoiceno " & _
            '          ", mdmedical_claim_master.invoice_amt " & _
            '          ", mdmedical_claim_master.instituteunkid " & _
            '          ", hrinstitute_master.institute_name " & _
            '          ", hrinstitute_master.isformrequire " & _
            '          ", '' as AUD " & _
            '          ", '' as GUID " & _
            '          " FROM mdmedical_claim_tran " & _
            '          " JOIN mdmedical_claim_master on mdmedical_claim_master.claimunkid = mdmedical_claim_tran.claimunkid " & _
            '          " LEFT JOIN hrinstitute_master on mdmedical_claim_master.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
            '          " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = mdmedical_claim_tran.employeeunkid " & _
            '          " LEFT JOIN hrdependants_beneficiaries_tran on hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_claim_tran.dependantsunkid  " & _
            '          " LEFT JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = mdmedical_claim_master.periodunkid " & _
            '          " LEFT JOIN mdmedical_sicksheet on mdmedical_sicksheet.sicksheetunkid = mdmedical_claim_tran.sicksheetunkid " & _
            '          " WHERE (mdmedical_claim_tran.claimunkid= @claimunkid or @claimunkid = 0) AND isnull(mdmedical_claim_tran.isvoid,0) = 0 "


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'strQ = "SELECT " & _
            '  "  claimtranunkid " & _
            '  ", mdmedical_claim_tran.claimunkid " & _
            '          ", mdmedical_claim_tran.employeeunkid " & _
            '  ", isnull(hremployee_master.firstname,'') + ' ' +  isnull(hremployee_master.othername,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
            '          ", mdmedical_claim_tran.dependantsunkid " & _
            '  ", isnull(hrdependants_beneficiaries_tran.first_name,'') + ' ' + isnull(hrdependants_beneficiaries_tran.last_name,'') as dependentsname " & _
            '          ", mdmedical_claim_master.periodunkid " & _
            '          ", cfcommon_period_tran.period_name " & _
            '          ", mdmedical_claim_tran.sicksheetunkid " & _
            '          ", mdmedical_sicksheet.sicksheetno " & _
            '          ", mdmedical_claim_tran.claimno " & _
            '          ", mdmedical_claim_tran.claimdate " & _
            '          ", mdmedical_claim_tran.amount " & _
            '  ", mdmedical_claim_tran.remarks " & _
            '         ", mdmedical_claim_tran.isempexempted " & _
            '          ", mdmedical_claim_master.invoiceno " & _
            '          ", mdmedical_claim_master.invoice_amt " & _
            '          ", mdmedical_claim_master.instituteunkid " & _
            '          ", hrinstitute_master.institute_name " & _
            '          ", hrinstitute_master.isformrequire " & _
            '  ", '' as AUD " & _
            '  ", '' as GUID " & _
            ' " FROM mdmedical_claim_tran " & _
            ' " JOIN mdmedical_claim_master on mdmedical_claim_master.claimunkid = mdmedical_claim_tran.claimunkid " & _
            '         " LEFT JOIN hrinstitute_master on mdmedical_claim_master.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
            '         " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = mdmedical_claim_tran.employeeunkid " & _
            ' " LEFT JOIN hrdependants_beneficiaries_tran on hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_claim_tran.dependantsunkid  " & _
            '         " LEFT JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = mdmedical_claim_master.periodunkid " & _
            '         " LEFT JOIN mdmedical_sicksheet on mdmedical_sicksheet.sicksheetunkid = mdmedical_claim_tran.sicksheetunkid " & _
            ' " WHERE (mdmedical_claim_tran.claimunkid= @claimunkid or @claimunkid = 0) AND isnull(mdmedical_claim_tran.isvoid,0) = 0 "

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If dtPeriodEnd <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            strQ &= "SELECT " & _
              "  claimtranunkid " & _
              ", mdmedical_claim_tran.claimunkid " & _
                      ", mdmedical_claim_tran.employeeunkid " & _
              ", isnull(hremployee_master.firstname,'') + ' ' +  isnull(hremployee_master.othername,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
                      ", mdmedical_claim_tran.dependantsunkid " & _
              ", isnull(hrdependants_beneficiaries_tran.first_name,'') + ' ' + isnull(hrdependants_beneficiaries_tran.last_name,'') as dependentsname " & _
                      ", mdmedical_claim_master.periodunkid " & _
                      ", cfcommon_period_tran.period_name " & _
                      ", mdmedical_claim_tran.sicksheetunkid " & _
                      ", mdmedical_sicksheet.sicksheetno " & _
                      ", mdmedical_claim_tran.claimno " & _
                      ", mdmedical_claim_tran.claimdate " & _
                      ", mdmedical_claim_tran.amount " & _
              ", mdmedical_claim_tran.remarks " & _
                     ", mdmedical_claim_tran.isempexempted " & _
                      ", mdmedical_claim_master.invoiceno " & _
                      ", mdmedical_claim_master.invoice_amt " & _
                      ", mdmedical_claim_master.instituteunkid " & _
                      ", hrinstitute_master.institute_name " & _
                      ", hrinstitute_master.isformrequire " & _
              ", '' as AUD " & _
              ", '' as GUID " & _
             " FROM mdmedical_claim_tran " & _
             " JOIN mdmedical_claim_master on mdmedical_claim_master.claimunkid = mdmedical_claim_tran.claimunkid " & _
                     " LEFT JOIN hrinstitute_master on mdmedical_claim_master.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
                     " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = mdmedical_claim_tran.employeeunkid " & _
             " LEFT JOIN hrdependants_beneficiaries_tran on hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_claim_tran.dependantsunkid  " & _
                        "AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                    "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                         "AND #TableDepn.isactive = 1 " & _
                     " LEFT JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = mdmedical_claim_master.periodunkid " & _
                    "   LEFT JOIN mdmedical_sicksheet on mdmedical_sicksheet.sicksheetunkid = mdmedical_claim_tran.sicksheetunkid "
            'Sohail (18 May 2019) - [AND hrdependants_beneficiaries_tran.isvoid = 0, JOIN #TableDepn, isactive]

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= "WHERE (mdmedical_claim_tran.claimunkid= @claimunkid or @claimunkid = 0) AND isnull(mdmedical_claim_tran.isvoid,0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If

            End If
            'Shani(24-Aug-2015) -- End

            'Pinkal (18-Dec-2012) -- End


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If
            'End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If strUserAccessLevelFilterString = "" Then
            '    strQ &= UserAccessLevel._AccessLevelFilterString
            'Else
            '    strQ &= strUserAccessLevelFilterString
            'End If

            'If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
            '    blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
            'End If

            'If CBool(blnIncludeInactiveEmployee) = False Then

            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            'End If
            'Shani(24-Aug-2015) -- End

            'Pinkal (25-APR-2012) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid)
            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMedicalClaimUnkId)
            'Shani(24-Aug-2015) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")
            mdtTran = dsList.Tables(0)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_claim_tran) </purpose>
    Public Function InsertUpdateDelete_ClaimTran(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"


                                'Pinkal (18-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "INSERT INTO mdmedical_claim_tran ( " & _
                                '  "  claimunkid " & _
                                '  ", dependantsunkid " & _
                                '  ", sicksheetunkid " & _
                                '  ", employeeunkid " & _
                                '  ", claimno " & _
                                '  ", claimdate " & _
                                '  ", amount " & _
                                '  ", remarks " & _
                                '  ", userunkid " & _
                                '  ", isvoid " & _
                                '  ", voiduserunkid " & _
                                '  ", voiddatetime " & _
                                '  ", voidreason" & _
                                '") VALUES (" & _
                                '  "  @claimunkid " & _
                                '  ", @dependantsunkid " & _
                                '  ", @sicksheetunkid " & _
                                '  ", @employeeunkid " & _
                                '  ", @claimno " & _
                                '  ", @claimdate " & _
                                '  ", @amount " & _
                                '  ", @remarks " & _
                                '  ", @userunkid " & _
                                '  ", @isvoid " & _
                                '  ", @voiduserunkid " & _
                                '  ", @voiddatetime " & _
                                '  ", @voidreason" & _
                                '"); SELECT @@identity"

                                strQ = "INSERT INTO mdmedical_claim_tran ( " & _
                                  "  claimunkid " & _
                                  ", dependantsunkid " & _
                                  ", sicksheetunkid " & _
                                  ", employeeunkid " & _
                                  ", claimno " & _
                                  ", claimdate " & _
                                  ", amount " & _
                                  ", remarks " & _
                                  ", userunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason" & _
                                 ", isempexempted " & _
                                ") VALUES (" & _
                                  "  @claimunkid " & _
                                  ", @dependantsunkid " & _
                                  ", @sicksheetunkid " & _
                                  ", @employeeunkid " & _
                                  ", @claimno " & _
                                  ", @claimdate " & _
                                  ", @amount " & _
                                  ", @remarks " & _
                                  ", @userunkid " & _
                                  ", @isvoid " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason" & _
                                 ", @isempexempted " & _
                                "); SELECT @@identity"

                                'Pinkal (18-Dec-2012) -- End

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)
                                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dependantsunkid").ToString)
                                objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("sicksheetunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@claimno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("claimno").ToString)
                                objDataOperation.AddParameter("@claimdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("claimdate").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remarks").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")


                                'Pinkal (18-Dec-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@isempexempted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isempexempted").ToString))
                                'Pinkal (18-Dec-2012) -- Endf


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimtranunkid = dsList.Tables(0).Rows(0).Item(0)


                                'Pinkal (25-APR-2012) -- Start
                                'Enhancement : TRA Changes

                                'If .Item("claimunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", .Item("claimunkid"), "mdmedical_claim_tran", "claimtranunkid", mintClaimtranunkid, 2, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tran", "claimtranunkid", mintClaimtranunkid, 1, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If


                                If .Item("claimunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", .Item("claimunkid"), "mdmedical_claim_tran", "claimtranunkid", mintClaimtranunkid, 2, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tran", "claimtranunkid", mintClaimtranunkid, 1, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END
                                End If

                                'Pinkal (25-APR-2012) -- End


                            Case "U"


                                'Pinkal (18-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "UPDATE mdmedical_claim_tran SET " & _
                                '  "  claimunkid = @claimunkid" & _
                                '  ", dependantsunkid = @dependantsunkid" & _
                                '  ", sicksheetunkid = @sicksheetunkid" & _
                                '  ", employeeunkid = @employeeunkid" & _
                                '  ", claimno = @claimno" & _
                                '  ", claimdate = @claimdate" & _
                                '  ", amount = @amount" & _
                                '  ", remarks = @remarks " & _
                                '  ", userunkid = @userunkid" & _
                                '  ", isvoid = @isvoid" & _
                                '  ", voiduserunkid = @voiduserunkid" & _
                                '  ", voiddatetime = @voiddatetime" & _
                                '  ", voidreason = @voidreason " & _
                                '"WHERE claimtranunkid = @claimtranunkid "

                                strQ = "UPDATE mdmedical_claim_tran SET " & _
                                  "  claimunkid = @claimunkid" & _
                                  ", dependantsunkid = @dependantsunkid" & _
                                  ", sicksheetunkid = @sicksheetunkid" & _
                                  ", employeeunkid = @employeeunkid" & _
                                  ", claimno = @claimno" & _
                                  ", claimdate = @claimdate" & _
                                  ", amount = @amount" & _
                                  ", remarks = @remarks " & _
                                  ", userunkid = @userunkid" & _
                                  ", isvoid = @isvoid" & _
                                  ", voiduserunkid = @voiduserunkid" & _
                                  ", voiddatetime = @voiddatetime" & _
                                  ", voidreason = @voidreason " & _
                                         ", isempexempted = @isempexempted " & _
                                "WHERE claimtranunkid = @claimtranunkid "

                                'Pinkal (18-Dec-2012) -- End

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimtranunkid").ToString)
                                objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)
                                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dependantsunkid").ToString)
                                objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("sicksheetunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@claimno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("claimno").ToString)
                                objDataOperation.AddParameter("@claimdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("claimdate").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remarks").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")


                                'Pinkal (18-Dec-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@isempexempted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isempexempted").ToString)
                                'Pinkal (18-Dec-2012) -- End



                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tran", "claimtranunkid", CInt(.Item("claimtranunkid")), 2, 2, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                                


                            Case "D"


                                'Pinkal (25-APR-2012) -- Start
                                'Enhancement : TRA Changes


                                strQ = "UPDATE mdmedical_claim_tran SET " & _
                                          "  isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                          " WHERE claimtranunkid = @claimtranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimtranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tran", "claimtranunkid", .Item("claimtranunkid"), 2, 3, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_ClaimTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class


'Public Class clsmedical_claim_Tran
'    Private Const mstrModuleName = "clsmedical_claim_Tran"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintClaimunkid As Integer
'    Private mdtTran As DataTable
'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set claimunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Claimunkid() As Integer
'        Get
'            Return mintClaimunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintClaimunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set DataList
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _DataList() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property

'#End Region

'#Region " Contructor "

'    Public Sub New()
'        mdtTran = New DataTable("MedicalClaim")
'        Dim dCol As DataColumn
'        Try
'            dCol = New DataColumn("claimtranunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("claimunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("employeeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("employeename")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("dependantsunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("dependentsname")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            'dCol = New DataColumn("membershipunkid")
'            'dCol.DataType = System.Type.GetType("System.Int32")
'            'mdtTran.Columns.Add(dCol)

'            'dCol = New DataColumn("membershipname")
'            'dCol.DataType = System.Type.GetType("System.String")
'            'mdtTran.Columns.Add(dCol)

'            'dCol = New DataColumn("membershipno")
'            'dCol.DataType = System.Type.GetType("System.String")
'            'mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("serviceno")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("yearunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("claimno")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("claimdate")
'            dCol.DataType = System.Type.GetType("System.DateTime")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("amount")
'            dCol.DataType = System.Type.GetType("System.Decimal")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("covermasterunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("covername")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("serviceunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("servicename")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("instituteunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("treatmentdate")
'            dCol.DataType = System.Type.GetType("System.DateTime")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("treatmentunkid")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'            dCol = New DataColumn("remarks")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

'            dCol = New DataColumn("AUD")
'            dCol.DataType = System.Type.GetType("System.String")
'            dCol.AllowDBNull = True
'            dCol.DefaultValue = DBNull.Value
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("GUID")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetClaimTran() As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'            strQ = "SELECT " & _
'              "  claimtranunkid " & _
'              ", mdmedical_claim_tran.claimunkid " & _
'              ", mdmedical_claim_master.employeeunkid " & _
'              ", isnull(hremployee_master.firstname,'') + ' ' +  isnull(hremployee_master.othername,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
'              ", dependantsunkid " & _
'              ", isnull(hrdependants_beneficiaries_tran.first_name,'') + ' ' + isnull(hrdependants_beneficiaries_tran.last_name,'') as dependentsname " & _
'              ", m2.mdserviceno as serviceno " & _
'              ", mdmedical_claim_master.yearunkid " & _
'              ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as year" & _
'              ", covermasterunkid " & _
'              ", m1.mdmastername as covername " & _
'              ", mdmedical_claim_tran.serviceunkid " & _
'              ", m2.mdmastername as servicename " & _
'              ", mdmedical_claim_tran.instituteunkid " & _
'              ", hrinstitute_master.institute_name as Hospital" & _
'              ", treatmentdate " & _
'              ", mdmedical_claim_tran.treatmentunkid " & _
'              ", claimno " & _
'              ", claimdate " & _
'              ", amount " & _
'              ", mdmedical_claim_tran.remarks " & _
'              ", '' as AUD " & _
'              ", '' as GUID " & _
'             " FROM mdmedical_claim_tran " & _
'             " JOIN mdmedical_claim_master on mdmedical_claim_master.claimunkid = mdmedical_claim_tran.claimunkid " & _
'             " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = mdmedical_claim_master.employeeunkid " & _
'             " LEFT JOIN hrdependants_beneficiaries_tran on hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_claim_tran.dependantsunkid  AND isdependant = 1 " & _
'             " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on cffinancial_year_tran.yearunkid = mdmedical_claim_master.yearunkid " & _
'             " LEFT JOIN mdmedical_master m1 on m1.mdmasterunkid = mdmedical_claim_tran.covermasterunkid " & _
'             " LEFT JOIN mdmedical_master m2 on m2.mdmasterunkid = mdmedical_claim_tran.serviceunkid " & _
'             " LEFT JOIN hrinstitute_master on hrinstitute_master.instituteunkid = mdmedical_claim_tran.instituteunkid " & _
'             " WHERE (mdmedical_claim_tran.claimunkid= @claimunkid or @claimunkid = 0) AND isnull(mdmedical_claim_tran.isvoid,0) = 0 "



'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.
'            If UserAccessLevel._AccessLevel.Length > 0 Then
'                strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            End If

'            'Anjan (24 Jun 2011)-End 

'            'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment


'            '   ", mdmedical_claim_tran.membershipunkid " & _
'            '", hrmembership_master.membershipname " & _
'            '  ", membershipno " & _
'            '" LEFT JOIN hrmembership_master on hrmembership_master.membershipunkid = mdmedical_claim_tran.membershipunkid " & _


'            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")
'            mdtTran = dsList.Tables(0)
'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (mdmedical_claim_tran) </purpose>
'    Public Function InsertUpdateDelete_ClaimTran(ByVal objDataOperation As clsDataOperation) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Try


'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"

'                                'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'                                strQ = "INSERT INTO mdmedical_claim_tran ( " & _
'                                  "  claimunkid " & _
'                                  ", dependantsunkid " & _
'                                  ", covermasterunkid " & _
'                                  ", serviceunkid " & _
'                                  ", instituteunkid " & _
'                                  ", treatmentdate " & _
'                                  ", treatmentunkid " & _
'                                  ", claimno " & _
'                                  ", claimdate " & _
'                                  ", amount " & _
'                                  ", remarks " & _
'                                  ", userunkid " & _
'                                  ", isvoid " & _
'                                  ", voiduserunkid " & _
'                                  ", voiddatetime " & _
'                                  ", voidreason" & _
'                                ") VALUES (" & _
'                                  "  @claimunkid " & _
'                                  ", @dependantsunkid " & _
'                                  ", @covermasterunkid " & _
'                                  ", @serviceunkid " & _
'                                  ", @instituteunkid " & _
'                                  ", @treatmentdate " & _
'                                  ", @treatmentunkid " & _
'                                  ", @claimno " & _
'                                  ", @claimdate " & _
'                                  ", @amount " & _
'                                  ", @remarks " & _
'                                  ", @userunkid " & _
'                                  ", @isvoid " & _
'                                  ", @voiduserunkid " & _
'                                  ", @voiddatetime " & _
'                                  ", @voidreason" & _
'                                "); SELECT @@identity"




'                                objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)
'                                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dependantsunkid").ToString)
'                                objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("covermasterunkid").ToString)
'                                objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serviceunkid").ToString)
'                                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("instituteunkid").ToString)
'                                objDataOperation.AddParameter("@treatmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("treatmentdate").ToString)
'                                objDataOperation.AddParameter("@treatmentunkid", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, .Item("treatmentunkid").ToString)
'                                objDataOperation.AddParameter("@claimno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("claimno").ToString)
'                                objDataOperation.AddParameter("@claimdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("claimdate").ToString)
'                                'Sohail (17 Jun 2011) -- Start
'                                'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("amount").ToString)
'                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString)
'                                'Sohail (17 Jun 2011) -- End
'                                objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remarks").ToString)
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

'                                'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

'                                dsList = objDataOperation.ExecQuery(strQ, "List")

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "U"

'                                'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

'                                strQ = "UPDATE mdmedical_claim_tran SET " & _
'                                  "  claimunkid = @claimunkid" & _
'                                  ", dependantsunkid = @dependantsunkid" & _
'                                  ", covermasterunkid = @covermasterunkid" & _
'                                  ", serviceunkid = @serviceunkid" & _
'                                  ", instituteunkid = @instituteunkid" & _
'                                  ", treatmentdate = @treatmentdate" & _
'                                  ", treatmentunkid = @treatmentunkid" & _
'                                  ", claimno = @claimno" & _
'                                  ", claimdate = @claimdate" & _
'                                  ", amount = @amount" & _
'                                  ", remarks = @remarks " & _
'                                  ", userunkid = @userunkid" & _
'                                  ", isvoid = @isvoid" & _
'                                  ", voiduserunkid = @voiduserunkid" & _
'                                  ", voiddatetime = @voiddatetime" & _
'                                  ", voidreason = @voidreason " & _
'                                "WHERE claimtranunkid = @claimtranunkid "

'                                objDataOperation.AddParameter("@claimtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimtranunkid").ToString)
'                                objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)
'                                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dependantsunkid").ToString)
'                                objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("covermasterunkid").ToString)
'                                objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serviceunkid").ToString)
'                                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("instituteunkid").ToString)
'                                objDataOperation.AddParameter("@treatmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("treatmentdate").ToString)
'                                objDataOperation.AddParameter("@treatmentunkid", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, .Item("treatmentunkid").ToString)
'                                objDataOperation.AddParameter("@claimno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("claimno").ToString)
'                                objDataOperation.AddParameter("@claimdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("claimdate").ToString)
'                                'Sohail (17 Jun 2011) -- Start
'                                'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("amount").ToString)
'                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString)
'                                'Sohail (17 Jun 2011) -- End
'                                objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remarks").ToString)
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

'                                'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If


'                            Case "D"

'                                strQ = "DELETE FROM mdmedical_claim_tran " & _
'                                                                           "WHERE claimtranunkid = @claimtranunkid "

'                                objDataOperation.AddParameter("@claimtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimtranunkid").ToString)
'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                        End Select
'                    End If
'                End With
'            Next
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_ClaimTran; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class