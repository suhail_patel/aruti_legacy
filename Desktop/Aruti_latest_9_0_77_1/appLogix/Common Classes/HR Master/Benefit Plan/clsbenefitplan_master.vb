﻿'************************************************************************************************************************************
'Class Name : clsbenefitplan_master.vb
'Purpose    : All Benefit Plan Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :26/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsbenefitplan_master
    Private Shared ReadOnly mstrModuleName As String = "clsbenefitplan_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBenefitplanunkid As Integer
    Private mstrBenefitplancode As String = String.Empty
    Private mstrBenefitplanname As String = String.Empty
    Private mintBenefitgroupunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrBenefitplanname1 As String = String.Empty
    Private mstrBenefitplanname2 As String = String.Empty
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Benefitplanunkid() As Integer
        Get
            Return mintBenefitplanunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitplanunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplancode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Benefitplancode() As String
        Get
            Return mstrBenefitplancode
        End Get
        Set(ByVal value As String)
            mstrBenefitplancode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplanname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Benefitplanname() As String
        Get
            Return mstrBenefitplanname
        End Get
        Set(ByVal value As String)
            mstrBenefitplanname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Benefitgroupunkid() As Integer
        Get
            Return mintBenefitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplancode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Benefitplanname1() As String
        Get
            Return mstrBenefitplanname1
        End Get
        Set(ByVal value As String)
            mstrBenefitplanname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplancode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Benefitplanname2() As String
        Get
            Return mstrBenefitplanname2
        End Get
        Set(ByVal value As String)
            mstrBenefitplanname2 = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  benefitplanunkid " & _
              ", benefitplancode " & _
              ", benefitplanname " & _
              ", benefitgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrbenefitplan_master " & _
             "WHERE benefitplanunkid = @benefitplanunkid "

            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBenefitplanunkid = CInt(dtRow.Item("benefitplanunkid"))
                mstrBenefitplancode = dtRow.Item("benefitplancode").ToString
                mstrBenefitplanname = dtRow.Item("benefitplanname").ToString
                mintBenefitgroupunkid = CInt(dtRow.Item("benefitgroupunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrBenefitplanname1 = dtRow.Item("name1").ToString
                mstrBenefitplanname2 = dtRow.Item("name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  benefitplanunkid " & _
              ", benefitplancode " & _
              ", benefitplanname " & _
              ", cfcommon_master.name " & _
              ", benefitgroupunkid " & _
              ", hrbenefitplan_master.description " & _
              ", hrbenefitplan_master.isactive " & _
              ", hrbenefitplan_master.name1 " & _
              ", hrbenefitplan_master.name2 " & _
             "FROM hrbenefitplan_master " & _
              " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrbenefitplan_master.benefitgroupunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrbenefitplan_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrbenefitplan_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrBenefitplancode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Benefit Plan Code is already defined. Please define new Benefit Plan Code.")
            Return False
        ElseIf isExist("", mstrBenefitplanname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Benefit Plan Name is already defined. Please define new Benefit Plan Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@benefitplancode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplancode.ToString)
            objDataOperation.AddParameter("@benefitplanname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplanname.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@benefitplanname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplanname1.ToString)
            objDataOperation.AddParameter("@benefitplanname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplanname2.ToString)

            strQ = "INSERT INTO hrbenefitplan_master ( " & _
              "  benefitplancode " & _
              ", benefitplanname " & _
              ", benefitgroupunkid " & _
              ", description " & _
              ", isactive" & _
              ", name1 " & _
              ", name2" & _
            ") VALUES (" & _
              "  @benefitplancode " & _
              ", @benefitplanname " & _
              ", @benefitgroupunkid " & _
              ", @description " & _
              ", @isactive" & _
              ", @benefitplanname1" & _
              ", @benefitplanname2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBenefitplanunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrbenefitplan_master", "benefitplanunkid", mintBenefitplanunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrbenefitplan_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrBenefitplancode, "", mintBenefitplanunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Benefit Plan Code is already defined. Please define new Benefit Plan Code.")
            Return False
        ElseIf isExist("", mstrBenefitplanname, mintBenefitplanunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Benefit Plan Name is already defined. Please define new Benefit Plan Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)
            objDataOperation.AddParameter("@benefitplancode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplancode.ToString)
            objDataOperation.AddParameter("@benefitplanname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplanname.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@benefitplanname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplanname1.ToString)
            objDataOperation.AddParameter("@benefitplanname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBenefitplanname2.ToString)

            strQ = "UPDATE hrbenefitplan_master SET " & _
              "  benefitplancode = @benefitplancode" & _
              ", benefitplanname = @benefitplanname" & _
              ", benefitgroupunkid = @benefitgroupunkid" & _
              ", description = @description" & _
              ", isactive = @isactive " & _
              ", name1 = @benefitplanname1" & _
              ", name2 = @benefitplanname2 " & _
            " WHERE benefitplanunkid = @benefitplanunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrbenefitplan_master", mintBenefitplanunkid, "benefitplanunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrbenefitplan_master", "benefitplanunkid", mintBenefitplanunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrbenefitplan_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Benefit Plan. Reason: This Benefit Plan is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            'strQ = "DELETE FROM hrbenefitplan_master " & _
            '"WHERE benefitplanunkid = @benefitplanunkid "

            strQ = "UPDATE hrbenefitplan_master " & _
                        " SET isactive = 0 " & _
                    " WHERE benefitplanunkid = @benefitplanunkid "

            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrbenefitplan_master", "benefitplanunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME='benefitplanunkid' "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hrbenefitplan_master" Then Continue For
                strQ = "SELECT benefitplanunkid FROM " & dtRow.Item("TableName").ToString & " WHERE benefitplanunkid = @benefitplanunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  benefitplanunkid " & _
              ", benefitplancode " & _
              ", benefitplanname " & _
              ", benefitgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrbenefitplan_master " & _
             "WHERE  1=1"


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND benefitplancode = @benefitplancode"
            End If

            If strName.Length > 0 Then
                strQ &= " AND benefitplanname = @benefitplanname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND benefitplanunkid <> @benefitplanunkid"
            End If

            objDataOperation.AddParameter("@benefitplancode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@benefitplanname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal strListName As String, Optional ByVal blnFlag As Boolean = False, Optional ByVal intGroupId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If blnFlag = True Then
                strQ = "SELECT 0 As benefitplanunkid , @Select As name " & _
                       "UNION "
            End If
            strQ &= "SELECT benefitplanunkid As benefitplanunkid, benefitplanname As name FROM hrbenefitplan_master Where isactive = 1 "

            If intGroupId > 0 Then
                strQ &= "AND benefitgroupunkid = " & intGroupId
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (21-Dec-2015) -- Start
    'Enhancement - Working on Employee Benefit changes given By Rutta.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetBeniftPlanFromBenefitGrp(ByVal xBenefitGroupID As Integer) As String
        Dim mstrBenefitPlan As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT ISNULL( STUFF((SELECT DISTINCT ',' + CAST(benefitplanunkid AS NVARCHAR(max)) FROM hrbenefitplan_master " & _
                 "WHERE isactive = 1 AND benefitgroupunkid  = @benefitgroupunkid  " & _
                 "FOR XML PATH('')),1,1,''),'') AS Ids "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xBenefitGroupID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mstrBenefitPlan = dsList.Tables(0).Rows(0).Item("Ids").ToString.Trim
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBeniftPlanFromBenefitGrp; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mstrBenefitPlan
    End Function

    'Pinkal (21-Dec-2015) -- End




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Benefit Plan Code is already defined. Please define new Benefit Plan Code.")
			Language.setMessage(mstrModuleName, 2, "This Benefit Plan Name is already defined. Please define new Benefit Plan Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class