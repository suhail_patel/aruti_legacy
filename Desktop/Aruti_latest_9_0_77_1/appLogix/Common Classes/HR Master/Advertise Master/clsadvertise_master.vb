﻿'************************************************************************************************************************************
'Class Name : clsAdvertise_master.vb
'Purpose    :
'Date       :28/06/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsAdvertise_master
    Private Shared ReadOnly mstrModuleName As String = "clsAdvertise_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAdvertiseunkid As Integer
    Private mstrCompany As String = String.Empty
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mintCategoryunkid As Integer
    Private mintCityunkid As Integer
    Private mintStateunkid As Integer
    Private mintPincodeunkid As Integer
    Private mintCountryunkid As Integer
    Private mstrPhone As String = String.Empty
    Private mstrFax As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mstrWebsite As String = String.Empty
    Private mstrContactperson As String = String.Empty
    Private mstrContactno As String = String.Empty
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set advertiseunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Advertiseunkid() As Integer
        Get
            Return mintAdvertiseunkid
        End Get
        Set(ByVal value As Integer)
            mintAdvertiseunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Company() As String
        Get
            Return mstrCompany
        End Get
        Set(ByVal value As String)
            mstrCompany = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Categoryunkid() As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pincodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Pincodeunkid() As Integer
        Get
            Return mintPincodeunkid
        End Get
        Set(ByVal value As Integer)
            mintPincodeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Phone() As String
        Get
            Return mstrPhone
        End Get
        Set(ByVal value As String)
            mstrPhone = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fax
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fax() As String
        Get
            Return mstrFax
        End Get
        Set(ByVal value As String)
            mstrFax = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set website
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Website() As String
        Get
            Return mstrWebsite
        End Get
        Set(ByVal value As String)
            mstrWebsite = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contactperson
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contactperson() As String
        Get
            Return mstrContactperson
        End Get
        Set(ByVal value As String)
            mstrContactperson = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contactno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contactno() As String
        Get
            Return mstrContactno
        End Get
        Set(ByVal value As String)
            mstrContactno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  advertiseunkid " & _
              ", company " & _
              ", address1 " & _
              ", address2 " & _
              ", categoryunkid " & _
              ", cityunkid " & _
              ", stateunkid " & _
              ", pincodeunkid " & _
              ", countryunkid " & _
              ", phone " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", contactperson " & _
              ", contactno " & _
              ", isactive " & _
             "FROM hradvertise_master " & _
             "WHERE advertiseunkid = @advertiseunkid "

            objDataOperation.AddParameter("@advertiseunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAdvertiseUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintadvertiseunkid = CInt(dtRow.Item("advertiseunkid"))
                mstrcompany = dtRow.Item("company").ToString
                mstraddress1 = dtRow.Item("address1").ToString
                mstraddress2 = dtRow.Item("address2").ToString
                mintcategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mintcityunkid = CInt(dtRow.Item("cityunkid"))
                mintstateunkid = CInt(dtRow.Item("stateunkid"))
                mintpincodeunkid = CInt(dtRow.Item("pincodeunkid"))
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mstrphone = dtRow.Item("phone").ToString
                mstrfax = dtRow.Item("fax").ToString
                mstremail = dtRow.Item("email").ToString
                mstrwebsite = dtRow.Item("website").ToString
                mstrcontactperson = dtRow.Item("contactperson").ToString
                mstrcontactno = dtRow.Item("contactno").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal IsForImport As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (10-Mar-2011) -- Start

            'strQ = "SELECT " & _
            '  "  hradvertise_master.advertiseunkid " & _
            '  ", hradvertise_master.company " & _
            '  ", hradvertise_master.address1 " & _
            '  ", hradvertise_master.address2 " & _
            '  ", hradvertise_master.categoryunkid " & _
            '  ", cfcommon_master.name" & _
            '  ", hradvertise_master.cityunkid " & _
            '  ", hradvertise_master.stateunkid " & _
            '  ", hradvertise_master.pincodeunkid " & _
            '  ", hradvertise_master.countryunkid " & _
            '  ", hradvertise_master.phone " & _
            '  ", hradvertise_master.fax " & _
            '  ", hradvertise_master.email " & _
            '  ", hradvertise_master.website " & _
            '  ", hradvertise_master.contactperson " & _
            '  ", hradvertise_master.contactno " & _
            '  ", hradvertise_master.isactive " & _
            ' "FROM hradvertise_master " & _
            ' " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hradvertise_master.categoryunkid "

            strQ = "SELECT " & _
              "  hradvertise_master.advertiseunkid " & _
              ", hradvertise_master.company " & _
              ", hradvertise_master.address1 " & _
              ", hradvertise_master.address2 " & _
              ", hradvertise_master.categoryunkid " & _
              ", cfcommon_master.name" & _
              ", hradvertise_master.cityunkid " & _
              ", hradvertise_master.stateunkid " & _
              ", hradvertise_master.pincodeunkid " & _
              ", hradvertise_master.countryunkid " & _
              ", hradvertise_master.phone " & _
              ", hradvertise_master.fax " & _
              ", hradvertise_master.email " & _
              ", hradvertise_master.website " & _
              ", hradvertise_master.contactperson " & _
              ", hradvertise_master.contactno " & _
             ", hradvertise_master.isactive "

            If IsForImport Then
                strQ &= ", hrmsConfiguration..cfcity_master.name as city " & _
                            ",  hrmsConfiguration..cfstate_master.name as state" & _
                            ", hrmsConfiguration..cfzipcode_master.zipcode_no as pincode " & _
                            ", hrmsConfiguration..cfcountry_master.country_name as country "
            End If

            strQ &= " FROM hradvertise_master " & _
             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hradvertise_master.categoryunkid "


            If IsForImport Then
                strQ &= " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = hradvertise_master.countryunkid " & _
                            " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hradvertise_master.stateunkid " & _
                            " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hradvertise_master.cityunkid  " & _
                            " LEFT JOIN hrmsConfiguration..cfzipcode_master on hrmsConfiguration..cfzipcode_master.zipcodeunkid = hradvertise_master.pincodeunkid "
            End If
            
            If blnOnlyActive Then
                strQ &= " WHERE hradvertise_master.isactive = 1 "
            End If

            'Pinkal (10-Mar-2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hradvertise_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCompany) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Advertise Company is already defined. Please define new Advertise Company.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@company", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcompany.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress2.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcategoryunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpincodeunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrphone.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrwebsite.ToString)
            objDataOperation.AddParameter("@contactperson", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactperson.ToString)
            objDataOperation.AddParameter("@contactno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactno.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "INSERT INTO hradvertise_master ( " & _
              "  company " & _
              ", address1 " & _
              ", address2 " & _
              ", categoryunkid " & _
              ", cityunkid " & _
              ", stateunkid " & _
              ", pincodeunkid " & _
              ", countryunkid " & _
              ", phone " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", contactperson " & _
              ", contactno " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @company " & _
              ", @address1 " & _
              ", @address2 " & _
              ", @categoryunkid " & _
              ", @cityunkid " & _
              ", @stateunkid " & _
              ", @pincodeunkid " & _
              ", @countryunkid " & _
              ", @phone " & _
              ", @fax " & _
              ", @email " & _
              ", @website " & _
              ", @contactperson " & _
              ", @contactno " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAdvertiseunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hradvertise_master", "advertiseunkid", mintAdvertiseunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hradvertise_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCompany, mintAdvertiseunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Advertise Company is already defined. Please define new Advertise Company.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@advertiseunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintadvertiseunkid.ToString)
            objDataOperation.AddParameter("@company", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcompany.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress2.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcategoryunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpincodeunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrphone.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrwebsite.ToString)
            objDataOperation.AddParameter("@contactperson", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactperson.ToString)
            objDataOperation.AddParameter("@contactno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactno.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "UPDATE hradvertise_master SET " & _
              "  company = @company" & _
              ", address1 = @address1" & _
              ", address2 = @address2" & _
              ", categoryunkid = @categoryunkid" & _
              ", cityunkid = @cityunkid" & _
              ", stateunkid = @stateunkid" & _
              ", pincodeunkid = @pincodeunkid" & _
              ", countryunkid = @countryunkid" & _
              ", phone = @phone" & _
              ", fax = @fax" & _
              ", email = @email" & _
              ", website = @website" & _
              ", contactperson = @contactperson" & _
              ", contactno = @contactno" & _
              ", isactive = @isactive " & _
            "WHERE advertiseunkid = @advertiseunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hradvertise_master", mintAdvertiseunkid, "advertiseunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hradvertise_master", "advertiseunkid", mintAdvertiseunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hradvertise_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Advertise Company. Reason: This Advertise Company is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            'Anjan (31 Dec 2010)-Start
            'StrQ = "DELETE FROM hradvertise_master " & _
            '"WHERE advertiseunkid = @advertiseunkid "
            strQ = " UPDATE hradvertise_master " & _
                        " SET isactive = 0 " & _
                    "WHERE advertiseunkid=@advertiseunkid "
            'Anjan (31 Dec 2010)-End

            objDataOperation.AddParameter("@advertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hradvertise_master", "advertiseunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                     ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('advertiseunkid','advertiserunkid') "



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            '< TO DO > Need to check for this id in other tables also
            objDataOperation.AddParameter("@advertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hradvertise_master" Then Continue For
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @advertiseunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  advertiseunkid " & _
              ", company " & _
              ", address1 " & _
              ", address2 " & _
              ", categoryunkid " & _
              ", cityunkid " & _
              ", stateunkid " & _
              ", pincodeunkid " & _
              ", countryunkid " & _
              ", phone " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", contactperson " & _
              ", contactno " & _
              ", isactive " & _
             "FROM hradvertise_master " & _
             "WHERE company = @company "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If intUnkid > 0 Then
                strQ &= " AND advertiseunkid <> @advertiseunkid"
            End If

            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@advertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    '''  Modify By: Anjan
    ''' </summary>
    ''' <param name="blnNA"></param>
    ''' <param name="strListName"></param>
    ''' <param name="intLanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal blnNA As Boolean = False, _
                                    Optional ByVal strListName As String = "List", _
                                      Optional ByVal intAdvertiseCategory As Integer = -1, Optional ByVal intLanguageID As Integer = -1) As DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If blnNA Then
                strQ = "SELECT 0 AS advertiseunkid " & _
                          ",  ' ' + @Select AS name " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            End If

            strQ &= "SELECT advertiseunkid " & _
                        ", company AS name " & _
                     "FROM hradvertise_master " & _
                     "WHERE isactive = 1 "

            If intAdvertiseCategory > 0 Then
                strQ &= " AND categoryunkid= @categoryunkid "
                objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAdvertiseCategory)
            End If

            strQ &= "ORDER BY  name "

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Advertise Company is already defined. Please define new Advertise Company.")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class