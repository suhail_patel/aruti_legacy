﻿'************************************************************************************************************************************
'Class Name : clsstate_master.vb
'Purpose    : All State Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :29/06/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsstate_master
    Private Shared ReadOnly mstrModuleName As String = "clsstate_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStateunkid As Integer
    Private mintCountryunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  stateunkid " & _
              ", countryunkid " & _
              ", code " & _
              ", name " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrmsConfiguration..cfstate_master " & _
             "WHERE stateunkid = @stateunkid "

            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblFlag As Boolean = False, Optional ByVal intcountryunkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as stateunkid, 0 countryunkid,''country,'' code ,'' + @name as name, 1 as active,'' name1,'' name2  UNION "
            End If

            strQ &= "SELECT " & _
              "  hrmsConfiguration..cfstate_master.stateunkid " & _
              ", hrmsConfiguration..cfstate_master.countryunkid " & _
              ", hrmsConfiguration..cfcountry_master.country_name as country" & _
              ", hrmsConfiguration..cfstate_master.code " & _
              ", hrmsConfiguration..cfstate_master.name " & _
              ", hrmsConfiguration..cfstate_master.isactive " & _
              ", hrmsConfiguration..cfstate_master.name1 " & _
              ", hrmsConfiguration..cfstate_master.name2 " & _
             "FROM hrmsConfiguration..cfstate_master " & _
             " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = hrmsConfiguration..cfstate_master.countryunkid"

            If blnOnlyActive Then
                strQ &= " WHERE hrmsConfiguration..cfstate_master.isactive = 1 "
            End If

            If intcountryunkid > 0 Then
                If blnOnlyActive Then
                    strQ &= "AND hrmsConfiguration..cfstate_master.countryunkid=@countryunkid"
                Else
                    strQ &= " WHERE hrmsConfiguration..cfstate_master.countryunkid=@countryunkid"
                End If
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcountryunkid)
            End If

            If mblFlag = True Then
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select State"))
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmsConfiguration..cfstate_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintCountryunkid, mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This State Code is already defined. Please define new State Code.")
            Return False
        ElseIf isExist(mintCountryunkid, "", mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This State Name is already defined. Please define new State Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "INSERT INTO hrmsConfiguration..cfstate_master ( " & _
              "  countryunkid " & _
              ", code " & _
              ", name " & _
              ", isactive " & _
              ", name1 " & _
              ", name2" & _
            ") VALUES (" & _
              "  @countryunkid " & _
              ", @code " & _
              ", @name " & _
              ", @isactive " & _
              ", @name1 " & _
              ", @name2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStateunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfstate_master", "stateunkid", mintStateunkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmsConfiguration..cfstate_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintCountryunkid, mstrCode, "", mintStateunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This State Code is already defined. Please define new State Code.")
            Return False
        ElseIf isExist(mintCountryunkid, "", mstrName, mintStateunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This State Name is already defined. Please define new State Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "UPDATE hrmsConfiguration..cfstate_master SET " & _
              "  countryunkid = @countryunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", isactive = @isactive" & _
              ", name1 = @name1" & _
              ", name2 = @name2 " & _
              ", Syncdatetime  = NULL " & _
            "WHERE stateunkid = @stateunkid " 'Sohail (28 Mar 2012) - [Syncdatetime]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfstate_master", mintStateunkid, "stateunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfstate_master", "stateunkid", mintStateunkid, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmsConfiguration..cfstate_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected State. Reason: This State is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            'strQ = "DELETE FROM hrmsConfiguration..cfstate_master " & _
            '"WHERE stateunkid = @stateunkid "

            strQ = "UPDATE hrmsConfiguration..cfstate_master " & _
                    " SET isactive = 0 " & _
                    "WHERE stateunkid = @stateunkid "


            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmsConfiguration..cfstate_master", "stateunkid", intUnkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False
        Dim dsCompanyDatabase As DataSet = Nothing
        Dim mstrCurrentDatabase As String = Nothing


        objDataOperation = New clsDataOperation

        Try
            mstrCurrentDatabase = FinancialYear._Object._DatabaseName

            strQ = " USE hrmsConfiguration " & _
               "SELECT " & _
               "TABLE_NAME AS TableName  " & _
               ",COLUMN_NAME " & _
               "FROM INFORMATION_SCHEMA.COLUMNS " & _
               "WHERE COLUMN_NAME IN ('stateunkid')"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)



            For Each dtRow As DataRow In dsList.Tables("List").Rows


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'If dtRow.Item("TableName") = "hrmsConfiguration..cfstate_master" Then Continue For
                If dtRow.Item("TableName").ToString.Trim = "cfstate_master" Then Continue For
                'S.SANDEEP [ 12 OCT 2011 ] -- END 


                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & "hrmsConfiguration" & ".." & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @stateunkid "


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'dsList = objDataOperation.ExecQuery(strQ, "Used")
                Dim dsList1 As DataSet = objDataOperation.ExecQuery(strQ, "Used")
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'If dsList.Tables("Used").Rows.Count > 0 Then
                '    blnIsUsed = True
                '    Exit For
                'End If
                If dsList1.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Next

            If mstrCurrentDatabase <> "" Then

                If blnIsUsed = False Then
                    strQ = ""
                    strQ = "SELECT " & _
                           "companyunkid " & _
                          ",start_date " & _
                          ",end_date " & _
                          ",code " & _
                          ",database_name " & _
                       "FROM " & _
                           "( SELECT " & _
                               "hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
                              ",hrmsConfiguration..cffinancial_year_tran.start_date " & _
                              ",hrmsConfiguration..cffinancial_year_tran.end_date " & _
                              ",hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
                              ",hrmsConfiguration..cffinancial_year_tran.database_name " & _
                              ",row_number() OVER ( PARTITION BY hrmsConfiguration..cffinancial_year_tran.companyunkid ORDER BY hrmsConfiguration..cffinancial_year_tran.yearunkid DESC ) rn " & _
                              ",cfcompany_master.code " & _
                             "FROM hrmsConfiguration..cffinancial_year_tran ,hrmsConfiguration..cfcompany_master " & _
                             "WHERE hrmsConfiguration..cffinancial_year_tran.companyunkid = hrmsConfiguration..cfcompany_master.companyunkid " & _
                               "AND hrmsConfiguration..cfcompany_master.isactive = 1 " & _
                           ") AS A " & _
                       "WHERE rn = 1 "


                    dsCompanyDatabase = objDataOperation.ExecQuery(strQ, "CompanyDatabase")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = ""
                    strQ = " USE " & mstrCurrentDatabase & " " & _
                            " SELECT " & _
                            "TABLE_NAME AS TableName  " & _
                            ",COLUMN_NAME " & _
                            "FROM INFORMATION_SCHEMA.COLUMNS " & _
                            "WHERE COLUMN_NAME IN ('stateunkid','present_stateunkid','perm_stateunkid','domicile_stateunkid')"

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

                strQ = ""

                If dsCompanyDatabase IsNot Nothing AndAlso dsCompanyDatabase.Tables.Count > 0 AndAlso dsCompanyDatabase.Tables("CompanyDatabase").Rows.Count > 0 Then  'Anjan (28 Aug 2017) - Start
                    For Each dtDatabaseRow As DataRow In dsCompanyDatabase.Tables("CompanyDatabase").Rows

                        'S.SANDEEP [ 18 JUL 2014 ] -- START
                        If objDataOperation.RecordCount("SELECT * FROM sys.databases WHERE databases.name = '" & dtDatabaseRow.Item("database_name") & "'") <= 0 Then Continue For
                        'S.SANDEEP [ 18 JUL 2014 ] -- END


                        For Each dtRow As DataRow In dsList.Tables("List").Rows
                            strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtDatabaseRow.Item("database_name") & ".." & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @stateunkid "


                            'S.SANDEEP [ 12 OCT 2011 ] -- START
                            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                            'dsList = objDataOperation.ExecQuery(strQ, "Used")
                            Dim dsList1 As DataSet = objDataOperation.ExecQuery(strQ, "Used")
                            'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [ 12 OCT 2011 ] -- START
                            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                            'If dsList.Tables("Used").Rows.Count > 0 Then
                            '    blnIsUsed = True
                            '    Exit For
                            'End If
                            If dsList1.Tables("Used").Rows.Count > 0 Then
                                blnIsUsed = True
                                Exit For
                            End If
                            'S.SANDEEP [ 12 OCT 2011 ] -- END 

                        Next
                        If blnIsUsed = True Then Exit For
                    Next
                End If
            End If

            mstrMessage = ""

            If mstrCurrentDatabase.Length > 0 Then
                eZeeDatabase.change_database(mstrCurrentDatabase)
            Else
                eZeeDatabase.change_database("hrmsConfiguration")
            End If

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCountryunkid As Integer, Optional ByVal strStateCode As String = "", Optional ByVal strStateName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  stateunkid " & _
              ", countryunkid " & _
              ", code " & _
              ", name " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrmsConfiguration..cfstate_master " & _
             "WHERE 1=1  and countryunkid = @countryunkid"


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 


            If strStateCode.Length > 0 Then
                strQ &= " AND code = @code "
            End If

            If strStateName.Length > 0 Then
                strQ &= " AND name = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND stateunkid <> @stateunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strStateCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strStateName)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryunkid)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetStateUnkId(ByVal mstrState As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " stateunkid " & _
                      "  FROM hrmsConfiguration..cfstate_master " & _
                      " WHERE name = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND hrmsConfiguration..cfstate_master.isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrState)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("stateunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetStateUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This State Code is already defined. Please define new State Code.")
            Language.setMessage(mstrModuleName, 2, "Select State")
            Language.setMessage(mstrModuleName, 3, "This State Name is already defined. Please define new State Name.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

''************************************************************************************************************************************
''Class Name : clsstate_master.vb
''Purpose    : All State Opration like getList, Insert, Update, Delete, checkDuplicate
''Date       :29/06/2010
''Written By :Pinkal
''Modified   :
''************************************************************************************************************************************

'Imports eZeeCommonLib
'''' <summary>
'''' Purpose: 
'''' Developer: Pinkal
'''' </summary>
'Public Class clsstate_master
'    Private Shared Readonly mstrModuleName As String = "clsstate_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintStateunkid As Integer
'    Private mintCountryunkid As Integer
'    Private mstrCode As String = String.Empty
'    Private mstrName As String = String.Empty
'    Private mblnIsactive As Boolean = True
'    Private mstrName1 As String = String.Empty
'    Private mstrName2 As String = String.Empty
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set stateunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Stateunkid() As Integer
'        Get
'            Return mintStateunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintStateunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set countryunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Countryunkid() As Integer
'        Get
'            Return mintCountryunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCountryunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set code
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Code() As String
'        Get
'            Return mstrCode
'        End Get
'        Set(ByVal value As String)
'            mstrCode = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Name() As String
'        Get
'            Return mstrName
'        End Get
'        Set(ByVal value As String)
'            mstrName = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name1
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Name1() As String
'        Get
'            Return mstrName1
'        End Get
'        Set(ByVal value As String)
'            mstrName1 = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name2
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Name2() As String
'        Get
'            Return mstrName2
'        End Get
'        Set(ByVal value As String)
'            mstrName2 = Value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  stateunkid " & _
'              ", countryunkid " & _
'              ", code " & _
'              ", name " & _
'              ", isactive " & _
'              ", name1 " & _
'              ", name2 " & _
'             "FROM hrstate_master " & _
'             "WHERE stateunkid = @stateunkid "

'            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintStateUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintstateunkid = CInt(dtRow.Item("stateunkid"))
'                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
'                mstrcode = dtRow.Item("code").ToString
'                mstrname = dtRow.Item("name").ToString
'                mblnisactive = CBool(dtRow.Item("isactive"))
'                mstrname1 = dtRow.Item("name1").ToString
'                mstrname2 = dtRow.Item("name2").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblFlag As Boolean = False, Optional ByVal intcountryunkid As Integer = 0) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            If mblFlag = True Then
'                strQ = "SELECT 0 as stateunkid, 0 countryunkid,''country,'' code ,'' + @name as name, 1 as active,'' name1,'' name2  UNION "
'            End If

'            strQ &= "SELECT " & _
'              "  hrstate_master.stateunkid " & _
'              ", hrstate_master.countryunkid " & _
'              ", cfcountry_master.country_name as country" & _
'              ", hrstate_master.code " & _
'              ", hrstate_master.name " & _
'              ", hrstate_master.isactive " & _
'              ", hrstate_master.name1 " & _
'              ", hrstate_master.name2 " & _
'             "FROM hrstate_master " & _
'             " LEFT JOIN cfcountry_master on cfcountry_master.countryunkid = hrstate_master.countryunkid"

'            If blnOnlyActive Then
'                strQ &= " WHERE hrstate_master.isactive = 1 "
'            End If

'            If intcountryunkid > 0 Then
'                If blnOnlyActive Then
'                    strQ &= "AND hrstate_master.countryunkid=@countryunkid"
'                Else
'                    strQ &= " WHERE hrstate_master.countryunkid=@countryunkid"
'                End If
'                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcountryunkid)
'            End If

'            If mblFlag = True Then
'                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select State"))
'            End If


'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrstate_master) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mintCountryunkid, mstrCode) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This State Code is already defined. Please define new State Code.")
'            Return False
'        ElseIf isExist(mintCountryunkid, "", mstrName) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 2, "This State Name is already defined. Please define new State Name.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
'            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
'            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
'            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
'            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)

'            StrQ = "INSERT INTO hrstate_master ( " & _
'              "  countryunkid " & _
'              ", code " & _
'              ", name " & _
'              ", isactive " & _
'              ", name1 " & _
'              ", name2" & _
'            ") VALUES (" & _
'              "  @countryunkid " & _
'              ", @code " & _
'              ", @name " & _
'              ", @isactive " & _
'              ", @name1 " & _
'              ", @name2" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintStateUnkId = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrstate_master) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mintCountryunkid, mstrCode, "", mintStateunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This State Code is already defined. Please define new State Code.")
'            Return False
'        ElseIf isExist(mintCountryunkid, "", mstrName, mintStateunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 2, "This State Name is already defined. Please define new State Name.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
'            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
'            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
'            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
'            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)

'            StrQ = "UPDATE hrstate_master SET " & _
'              "  countryunkid = @countryunkid" & _
'              ", code = @code" & _
'              ", name = @name" & _
'              ", isactive = @isactive" & _
'              ", name1 = @name1" & _
'              ", name2 = @name2 " & _
'            "WHERE stateunkid = @stateunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrstate_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected State. Reason: This State is in use.")
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "DELETE FROM hrstate_master " & _
'            "WHERE stateunkid = @stateunkid "

'            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intCountryunkid As Integer, Optional ByVal strStateCode As String = "", Optional ByVal strStateName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  stateunkid " & _
'              ", countryunkid " & _
'              ", code " & _
'              ", name " & _
'              ", isactive " & _
'              ", name1 " & _
'              ", name2 " & _
'             "FROM hrstate_master " & _
'             "WHERE 1=1  and countryunkid = @countryunkid"

'            If strStateCode.Length > 0 Then
'                strQ &= " AND code = @code "
'            End If

'            If strStateName.Length > 0 Then
'                strQ &= " AND name = @name "
'            End If

'            If intUnkid > 0 Then
'                strQ &= " AND stateunkid <> @stateunkid"
'            End If

'            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strStateCode)
'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strStateName)
'            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryunkid)
'            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function


'End Class