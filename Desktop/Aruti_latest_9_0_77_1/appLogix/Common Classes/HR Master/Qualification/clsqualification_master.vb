﻿'************************************************************************************************************************************
'Class Name : clsqualification_master.vb
'Purpose    : All Qualification Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :26/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsqualification_master
    Private Shared ReadOnly mstrModuleName As String = "clsqualification_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintQualificationunkid As Integer
    Private mstrQualificationcode As String = String.Empty
    Private mstrQualificationname As String = String.Empty
    Private mintQualificationgroupunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrQualificationname1 As String = String.Empty
    Private mstrQualificationname2 As String = String.Empty


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Private mintResultGrpunkId As Integer
    'Pinkal (12-Oct-2011) -- End


    'Pinkal (22-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIsSyncRecruitment As Boolean = False
    'Pinkal (22-MAY-2012) -- End


#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Qualificationunkid() As Integer
        Get
            Return mintQualificationunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Qualificationcode() As String
        Get
            Return mstrQualificationcode
        End Get
        Set(ByVal value As String)
            mstrQualificationcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Qualificationname() As String
        Get
            Return mstrQualificationname
        End Get
        Set(ByVal value As String)
            mstrQualificationname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Qualificationgroupunkid() As Integer
        Get
            Return mintQualificationgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Qualificationname1() As String
        Get
            Return mstrQualificationname1
        End Get
        Set(ByVal value As String)
            mstrQualificationname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Qualificationname2() As String
        Get
            Return mstrQualificationname2
        End Get
        Set(ByVal value As String)
            mstrQualificationname2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ResultGroupunkid() As Integer
        Get
            Return mintResultGrpunkId
        End Get
        Set(ByVal value As Integer)
            mintResultGrpunkId = value
        End Set
    End Property


    'Pinkal (22-MAY-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set SyncWithRecruitment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsSyncWithRecruitment() As Boolean
        Get
            Return mblnIsSyncRecruitment
        End Get
        Set(ByVal value As Boolean)
            mblnIsSyncRecruitment = value
        End Set
    End Property

    'Pinkal (22-MAY-2012) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  qualificationunkid " & _
            '  ", qualificationcode " & _
            '  ", qualificationname " & _
            '  ", qualificationgroupunkid " & _
            '   ", ISNULL(resultgroupunkid,0) resultgroupunkid" & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", qualificationname1 " & _
            '  ", qualificationname2 " & _
            ' "FROM hrqualification_master " & _
            ' "WHERE qualificationunkid = @qualificationunkid "

            strQ = "SELECT " & _
              "  qualificationunkid " & _
              ", qualificationcode " & _
              ", qualificationname " & _
              ", qualificationgroupunkid " & _
                       ", ISNULL(resultgroupunkid,0) resultgroupunkid" & _
              ", description " & _
              ", isactive " & _
              ", qualificationname1 " & _
              ", qualificationname2 " & _
                       ", ISNULL(issync_recruit,0) issync_recruit " & _
             "FROM hrqualification_master " & _
             "WHERE qualificationunkid = @qualificationunkid "


            'Pinkal (22-MAY-2012) -- End


            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintQualificationunkid = CInt(dtRow.Item("qualificationunkid"))
                mstrQualificationcode = dtRow.Item("qualificationcode").ToString
                mstrQualificationname = dtRow.Item("qualificationname").ToString
                mintQualificationgroupunkid = CInt(dtRow.Item("qualificationgroupunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrQualificationname1 = dtRow.Item("qualificationname1").ToString
                mstrQualificationname2 = dtRow.Item("qualificationname2").ToString

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                mintResultGrpunkId = dtRow.Item("resultgroupunkid").ToString
                'Pinkal (12-Oct-2011) -- End


                'Pinkal (22-MAY-2012) -- Start
                'Enhancement : TRA Changes
                mblnIsSyncRecruitment = CBool(dtRow.Item("issync_recruit").ToString)
                'Pinkal (22-MAY-2012) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  qualificationunkid " & _
            '  ", qualificationcode " & _
            '  ", qualificationname " & _
            '  ", qgrp.name " & _
            '  ", qualificationgroupunkid " & _
            '  ", rgrp.name as resultgroup " & _
            '  ", resultgroupunkid " & _
            '  ", description " & _
            '  ", hrqualification_master.isactive " & _
            '  ", qualificationname1 " & _
            '  ", qualificationname2 " & _
            ' "FROM hrqualification_master " & _
            '" LEFT JOIN cfcommon_master qgrp on qgrp.masterunkid = hrqualification_master.Qualificationgroupunkid " & _
            '" LEFT JOIN cfcommon_master rgrp  on rgrp.masterunkid = hrqualification_master.resultgroupunkid "

            strQ = "SELECT " & _
              "  qualificationunkid " & _
              ", qualificationcode " & _
              ", qualificationname " & _
              ", qgrp.name " & _
              ", qualificationgroupunkid " & _
              ", rgrp.name as resultgroup " & _
              ", resultgroupunkid " & _
              ", hrqualification_master.description " & _
              ", hrqualification_master.isactive " & _
              ", qualificationname1 " & _
              ", qualificationname2 " & _
             ", issync_recruit " & _
             "FROM hrqualification_master " & _
            " LEFT JOIN cfcommon_master qgrp on qgrp.masterunkid = hrqualification_master.Qualificationgroupunkid AND qgrp.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
            " LEFT JOIN cfcommon_master rgrp  on rgrp.masterunkid = hrqualification_master.resultgroupunkid  AND rgrp.mastertype = " & clsCommon_Master.enCommonMaster.RESULT_GROUP


            'Pinkal (22-MAY-2012) -- End

            If blnOnlyActive Then
                strQ &= " WHERE hrqualification_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrqualification_master) </purpose>
    Public Function Insert() As Boolean

        'S.SANDEEP [16 Jan 2016] -- START
        'If isExist(mstrQualificationcode) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification Code is already defined. Please define new Qualification Code.")
        '    Return False
        'ElseIf isExist("", mstrQualificationname) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Qualification Name is already defined. Please define new Qualification Name.")
        '    Return False
        'End If

        If isExist(mintQualificationgroupunkid, mstrQualificationcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification Code is already defined. Please define new Qualification Code.")
            Return False
        ElseIf isExist(mintQualificationgroupunkid, "", mstrQualificationname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Qualification Name is already defined. Please define new Qualification Name.")
            Return False
        End If
        'S.SANDEEP [16 Jan 2016] -- END
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@qualificationcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrqualificationcode.ToString)
            objDataOperation.AddParameter("@qualificationname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrqualificationname.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@qualificationname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrqualificationname1.ToString)
            objDataOperation.AddParameter("@qualificationname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrQualificationname2.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultGrpunkId.ToString)

            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@issync_recruit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSyncRecruitment.ToString)

            'strQ = "INSERT INTO hrqualification_master ( " & _
            '  "  qualificationcode " & _
            '  ", qualificationname " & _
            '  ", qualificationgroupunkid " & _
            '        ", resultgroupunkid " & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", qualificationname1 " & _
            '  ", qualificationname2" & _
            '") VALUES (" & _
            '  "  @qualificationcode " & _
            '  ", @qualificationname " & _
            '  ", @qualificationgroupunkid " & _
            '        ", @resultgroupunkid " & _
            '  ", @description " & _
            '  ", @isactive " & _
            '  ", @qualificationname1 " & _
            '  ", @qualificationname2" & _
            '"); SELECT @@identity"


            strQ = "INSERT INTO hrqualification_master ( " & _
              "  qualificationcode " & _
              ", qualificationname " & _
              ", qualificationgroupunkid " & _
                     ", resultgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", qualificationname1 " & _
              ", qualificationname2" & _
                       ", issync_recruit " & _
            ") VALUES (" & _
              "  @qualificationcode " & _
              ", @qualificationname " & _
              ", @qualificationgroupunkid " & _
                     ", @resultgroupunkid " & _
              ", @description " & _
              ", @isactive " & _
              ", @qualificationname1 " & _
              ", @qualificationname2" & _
                       ", @issync_recruit " & _
            "); SELECT @@identity"


            'Pinkal (22-MAY-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintQualificationunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrqualification_master", "qualificationunkid", mintQualificationunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrqualification_master) </purpose>
    Public Function Update() As Boolean

        'S.SANDEEP [16 Jan 2016] -- START
        'If isExist(mstrQualificationcode, "", mintQualificationunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification Code is already defined. Please define new Qualification Code.")
        '    Return False
        'ElseIf isExist("", mstrQualificationname, mintQualificationunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Qualification Name is already defined. Please define new Qualification Name.")
        '    Return False
        'End If

        If isExist(mintQualificationgroupunkid, mstrQualificationcode, "", mintQualificationunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification Code is already defined. Please define new Qualification Code.")
            Return False
        ElseIf isExist(mintQualificationgroupunkid, "", mstrQualificationname, mintQualificationunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Qualification Name is already defined. Please define new Qualification Name.")
            Return False
        End If
        'S.SANDEEP [16 Jan 2016] -- END
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End
        Try
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationunkid.ToString)
            objDataOperation.AddParameter("@qualificationcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrqualificationcode.ToString)
            objDataOperation.AddParameter("@qualificationname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrqualificationname.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@qualificationname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrqualificationname1.ToString)
            objDataOperation.AddParameter("@qualificationname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrQualificationname2.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultGrpunkId.ToString)

            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@issync_recruit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSyncRecruitment.ToString)

            'strQ = "UPDATE hrqualification_master SET " & _
            '          "  qualificationcode = @qualificationcode" & _
            '          ", qualificationname = @qualificationname" & _
            '          ", qualificationgroupunkid = @qualificationgroupunkid" & _
            '        ", resultgroupunkid = @resultgroupunkid" & _
            '          ", description = @description" & _
            '          ", isactive = @isactive" & _
            '          ", qualificationname1 = @qualificationname1" & _
            '          ", qualificationname2 = @qualificationname2 " & _
            '        ", Syncdatetime  = NULL " & _
            '        "WHERE qualificationunkid = @qualificationunkid "


            strQ = "UPDATE hrqualification_master SET " & _
              "  qualificationcode = @qualificationcode" & _
              ", qualificationname = @qualificationname" & _
              ", qualificationgroupunkid = @qualificationgroupunkid" & _
                     ", resultgroupunkid = @resultgroupunkid" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", qualificationname1 = @qualificationname1" & _
              ", qualificationname2 = @qualificationname2 " & _
              ", Syncdatetime  = NULL " & _
                     ", issync_recruit = @issync_recruit " & _
                   "WHERE qualificationunkid = @qualificationunkid "


            'Pinkal (22-MAY-2012) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrqualification_master", mintQualificationunkid, "qualificationunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrqualification_master", "qualificationunkid", mintQualificationunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrqualification_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Qualification. Reason: This Qualification is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
      
        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            'strQ = "DELETE FROM hrqualification_master " & _
            '"WHERE qualificationunkid = @qualificationunkid "

            strQ = " UPDATE hrqualification_master SET " & _
                        " isactive = 0 " & _
                    "WHERE qualificationunkid = @qualificationunkid "

            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrqualification_master", "qualificationunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False


        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName  " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('qualificationunkid') AND TABLE_NAME NOT LIKE 'at%' "



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            'objDataOperation.AddParameter("@qualificationunkid", SqlDbType., eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, intUnkid)
            'Pinkal (07-Jan-2012) -- End



            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName").ToString.Trim.ToUpper = "HRQUALIFICATION_MASTER" Then Continue For
                If dtRow.Item("TableName") = "" Then
                ElseIf dtRow.Item("TableName").ToString.Trim.ToUpper = "RCSHORTLIST_FILTER" Then
                    strQ = "SELECT qualificationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE qualificationunkid = @qualificationunkid AND isvoid = 0 "
                ElseIf dtRow.Item("TableName").ToString.Trim.ToUpper = "HRJOB_QUALIFICATION_TRAN" Then
                    strQ = "SELECT qualificationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE qualificationunkid = @qualificationunkid "
                ElseIf dtRow.Item("TableName").ToString.Trim.ToUpper = "RCAPPLICANTQUALIFICATION_TRAN" Then
                    strQ = "SELECT qualificationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE qualificationunkid = @qualificationunkid AND rcapplicantqualification_tran.isvoid = 0 "
                ElseIf dtRow.Item("TableName").ToString.Trim.ToUpper = "HRTRAINING_SCHEDULING" Then
                    strQ = "SELECT qualificationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE qualificationunkid = @qualificationunkid AND isvoid = 0 AND iscancel = 0 "
                ElseIf dtRow.Item("TableName").ToString.Trim.ToUpper = "HREMP_QUALIFICATION_TRAN" Then
                    strQ = "SELECT qualificationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE qualificationunkid = @qualificationunkid AND isvoid = 0 "
                End If

                'S.SANDEEP [24-Apr-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
                '-> Added Information
                '-> AND rcapplicantqualification_tran.isvoid = 0
                'S.SANDEEP [24-Apr-2018] -- END

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intQualfificationGrpId As Integer, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean 'S.SANDEEP [16 Jan 2016] -- START -- END
        'Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes


            'strQ = "SELECT " & _
            '  "  qualificationunkid " & _
            '  ", qualificationcode " & _
            '  ", qualificationname " & _
            '  ", qualificationgroupunkid " & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", qualificationname1 " & _
            '  ", qualificationname2 " & _
            ' "FROM hrqualification_master " & _
            ' "WHERE 1=1 "


            strQ = "SELECT " & _
              "  qualificationunkid " & _
              ", qualificationcode " & _
              ", qualificationname " & _
              ", qualificationgroupunkid " & _
                      ", resultgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", qualificationname1 " & _
              ", qualificationname2 " & _
             "FROM hrqualification_master " & _
             "WHERE isactive = 1 "


            'S.SANDEEP [16 Jan 2016] -- START
            strQ &= " AND qualificationgroupunkid = '" & intQualfificationGrpId & "' "
            'S.SANDEEP [16 Jan 2016] -- END


            'Pinkal (12-Oct-2011) -- End

            If strCode.Length > 0 Then
                strQ &= " AND qualificationcode = @qualificationcode "
            End If
            If strName.Length > 0 Then
                strQ &= " AND qualificationname = @qualificationname "
            End If


            If intUnkid > 0 Then
                strQ &= " AND qualificationunkid <> @qualificationunkid"
            End If

            objDataOperation.AddParameter("@qualificationcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@qualificationname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    Public Function GetComboList(Optional ByVal strListName As String = "List", _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal intQulifyGroupId As Integer = -1, _
                                 Optional ByVal strQualifyGroupIds As String = "" _
                                 ) As DataSet
        'Hemant (07 Oct 2019) -- [strQualifyGroupIds]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If blnFlag = True Then
                strQ = "SELECT " & _
                        "  0 AS Id " & _
                        " ,@Select AS Name " & _
                        "UNION "
            End If
            strQ &= "SELECT " & _
                    " hrqualification_master.qualificationunkid AS Id " & _
                    ",hrqualification_master.qualificationname AS Name " & _
                   "FROM hrqualification_master " & _
                   "WHERE hrqualification_master.isactive = 1 "

            If intQulifyGroupId <> -1 Then
                strQ &= "AND hrqualification_master.qualificationgroupunkid = @QulifyGroupId "
            End If

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            If strQualifyGroupIds.Trim <> "" Then
                strQ &= "AND hrqualification_master.qualificationgroupunkid IN ( " & strQualifyGroupIds & " )"
            End If
            'Hemant (07 Oct 2019) -- End

            objDataOperation.AddParameter("@QulifyGroupId", SqlDbType.Int, eZeeDataType.INT_SIZE, intQulifyGroupId.ToString)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Public Function GetResultCodeFromQualification(ByVal mstrQualificationId As String, Optional ByVal blnFlag As Boolean = True) As DataSet
        Dim DsList As DataSet = Nothing
        Dim strQ As String = ""
        Try

            objDataOperation = New clsDataOperation

            If blnFlag = True Then
                strQ = "SELECT " & _
                        "  0 AS resultunkid " & _
                        " ,@Select AS resultname " & _
                        "UNION "
            End If

            strQ &= " SELECT  resultunkid,resultname " & _
                      " FROM hrresult_master " & _
                      " WHERE   resultgroupunkid IN  " & _
                      " ( " & _
                      "         SELECT    ISNULL(resultgroupunkid, 0) " & _
                      " FROM hrqualification_master  " & _
                      " WHERE  1 = 1   "

            'Sohail (20 Mar 2020) -- Start
            'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
            '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
            If mstrQualificationId.Trim <> "" AndAlso mstrQualificationId.Trim <> "0" AndAlso mstrQualificationId.Trim <> "-1" Then
                strQ &= " AND qualificationunkid in (" & mstrQualificationId & " ) "
            End If

            strQ &= " ) AND isactive = 1 "
            'Sohail (20 Mar 2020) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            'objDataOperation.AddParameter("@qualificationunkid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrQualificationId)
            DsList = objDataOperation.ExecQuery(strQ, "List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetResultCodeFromQualification", mstrModuleName)
        End Try
        Return DsList
    End Function

    'Pinkal (12-Oct-2011) -- End



    'Sandeep [ 25 APRIL 2011 ] -- Start
    Public Function GetQualificationUnkid(ByVal strName As String, ByVal intQualificationGrpId As Integer) As Integer 'S.SANDEEP [16 Jan 2016] -- START -- END

        'Public Function GetQualificationUnkid(ByVal strName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT qualificationunkid AS qualificationunkid " & _
                        "FROM hrqualification_master " & _
                        "WHERE qualificationname = @name AND isactive = 1 "

            'S.SANDEEP [16 Jan 2016] -- START
            strQ &= " AND qualificationgroupunkid = '" & intQualificationGrpId & "'"
            'S.SANDEEP [16 Jan 2016] -- END

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("qualificationunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQualificationUnkid", mstrModuleName)
        End Try
    End Function
    'Sandeep [ 25 APRIL 2011 ] -- End 



    'Pinkal (07-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Public Function GetResultGroupFromQualifcationGroup(ByVal QualificationGrpID As Integer) As Integer
        Dim mintResultGroupUnkid As Integer = 0
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            strQ = "SELECT ISNULL(resultgroupunkid,0) AS resultgroupunkid FROM hrqualification_master WHERE qualificationgroupunkid =  @qualificationgroupunkid AND ISNULL(resultgroupunkid,0) > 0 "
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, QualificationGrpID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables(0).Rows.Count > 0 Then
                mintResultGroupUnkid = CInt(dsList.Tables(0).Rows(0)("resultgroupunkid"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetResultCodeFromQualification", mstrModuleName)
        End Try
        Return mintResultGroupUnkid
    End Function
    'Pinkal (07-Jan-2012) -- End


    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Public Function GetQualification(ByVal mstrQualificationID As String) As String
        Dim strQ As String = ""
        Dim mstrQualification As String = ""
        Dim dsList As DataSet = Nothing
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = "SELECT STUFF(( SELECT  ',' + CAST(hrqualification_master.qualificationname AS VARCHAR(max)) " & _
                        " FROM hrqualification_master " & _
                        " WHERE hrqualification_master.qualificationunkid In (" & mstrQualificationID & ")" & _
                        " ORDER BY hrqualification_master.qualificationunkid " & _
                        " FOR  XML PATH('')), 1, 1, '')  AS Qualification "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrQualification = dsList.Tables(0).Rows(0)("Qualification")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQualification", mstrModuleName)
        End Try
        Return mstrQualification
    End Function

    'Pinkal (12-May-2013) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Qualification Code is already defined. Please define new Qualification Code.")
			Language.setMessage(mstrModuleName, 2, "This Qualification Name is already defined. Please define new Qualification Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class