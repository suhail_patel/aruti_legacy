﻿'************************************************************************************************************************************
'Class Name : clsskill_master.vb
'Purpose    : All Skill Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :26/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsskill_master
    Private Shared ReadOnly mstrModuleName As String = "clsskill_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSkillunkid As Integer
    Private mstrSkillcode As String = String.Empty
    Private mstrSkillname As String = String.Empty
    Private mintSkillcategoryunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrSkillname1 As String = String.Empty
    Private mstrSkillname2 As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Skillunkid() As Integer
        Get
            Return mintSkillunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Skillcode() As String
        Get
            Return mstrSkillcode
        End Get
        Set(ByVal value As String)
            mstrSkillcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Skillname() As String
        Get
            Return mstrSkillname
        End Get
        Set(ByVal value As String)
            mstrSkillname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillcategoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Skillcategoryunkid() As Integer
        Get
            Return mintSkillcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillcategoryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Skillname1() As String
        Get
            Return mstrSkillname1
        End Get
        Set(ByVal value As String)
            mstrSkillname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Skillname2() As String
        Get
            Return mstrSkillname2
        End Get
        Set(ByVal value As String)
            mstrSkillname2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  skillunkid " & _
              ", skillcode " & _
              ", skillname " & _
              ", skillcategoryunkid " & _
              ", description " & _
              ", isactive " & _
              ", skillname1 " & _
              ", skillname2 " & _
             "FROM hrskill_master " & _
             "WHERE skillunkid = @skillunkid "

            objDataOperation.AddParameter("@skillunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintSkillUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintskillunkid = CInt(dtRow.Item("skillunkid"))
                mstrskillcode = dtRow.Item("skillcode").ToString
                mstrskillname = dtRow.Item("skillname").ToString
                mintskillcategoryunkid = CInt(dtRow.Item("skillcategoryunkid"))
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrskillname1 = dtRow.Item("skillname1").ToString
                mstrskillname2 = dtRow.Item("skillname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  skillunkid " & _
              ", skillcode " & _
              ", skillname " & _
              ", skillcategoryunkid " & _
              ", cfcommon_master.name " & _
              ", hrskill_master.description " & _
              ", hrskill_master.isactive " & _
              ", skillname1 " & _
              ", skillname2 " & _
             "FROM hrskill_master " & _
              " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrskill_master.skillcategoryunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrskill_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrskill_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrSkillcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Skill Code is already defined. Please define new Skill Code.")
            Return False
        ElseIf isExist("", mstrSkillname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Skill Name is already defined. Please define new Skill Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@skillcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillcode.ToString)
            objDataOperation.AddParameter("@skillname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillname.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintskillcategoryunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@skillname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillname1.ToString)
            objDataOperation.AddParameter("@skillname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillname2.ToString)

            StrQ = "INSERT INTO hrskill_master ( " & _
              "  skillcode " & _
              ", skillname " & _
              ", skillcategoryunkid " & _
              ", description " & _
              ", isactive " & _
              ", skillname1 " & _
              ", skillname2" & _
            ") VALUES (" & _
              "  @skillcode " & _
              ", @skillname " & _
              ", @skillcategoryunkid " & _
              ", @description " & _
              ", @isactive " & _
              ", @skillname1 " & _
              ", @skillname2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSkillunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrskill_master", "skillunkid", mintSkillunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrskill_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrSkillcode, "", mintSkillunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Skill Code is already defined. Please define new Skill Code.")
            Return False
        ElseIf isExist("", mstrSkillname, mintSkillunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Skill Name is already defined. Please define new Skill Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@skillunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintskillunkid.ToString)
            objDataOperation.AddParameter("@skillcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillcode.ToString)
            objDataOperation.AddParameter("@skillname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillname.ToString)
            objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintskillcategoryunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@skillname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillname1.ToString)
            objDataOperation.AddParameter("@skillname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrskillname2.ToString)

            StrQ = "UPDATE hrskill_master SET " & _
              "  skillcode = @skillcode" & _
              ", skillname = @skillname" & _
              ", skillcategoryunkid = @skillcategoryunkid" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", skillname1 = @skillname1" & _
              ", skillname2 = @skillname2 " & _
              ", Syncdatetime  = NULL " & _
            "WHERE skillunkid = @skillunkid " 'Sohail (28 Mar 2012) - [Syncdatetime]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrskill_master", mintSkillunkid, "skillunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrskill_master", "skillunkid", mintSkillunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrskill_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete selected Skill. Reason: This Skill is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            'StrQ = "DELETE FROM hrskill_master " & _
            '"WHERE skillunkid = @skillunkid "

            strQ = "UPDATE hrskill_master " & _
                        " SET isactive = 0 " & _
                    "WHERE skillunkid=@skillunkid "


            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrskill_master", "skillunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('skillunkid') "



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hrskill_master" Then Continue For

                strQ = "SELECT skillunkid FROM " & dtRow.Item("TableName").ToString & " WHERE skillunkid = @skillunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  skillunkid " & _
              ", skillcode " & _
              ", skillname " & _
              ", skillcategoryunkid " & _
              ", description " & _
              ", isactive " & _
              ", skillname1 " & _
              ", skillname2 " & _
             "FROM hrskill_master " & _
             "WHERE 1=1"

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End

            If strCode.Length > 0 Then
                strQ &= " AND skillcode = @skillcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND skillname = @skillname "
            End If


            If intUnkid > 0 Then
                strQ &= " AND skillunkid <> @skillunkid"
            End If

            objDataOperation.AddParameter("@skillcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@skillname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Returns List of Skill For Combo </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal intSkillCategory As Integer = -1) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag = True Then
                strQ = "SELECT 0 AS skillunkid,@Select AS NAME " & _
                       "UNION "
            End If
            strQ &= "SELECT skillunkid AS skillunkid,ISNULL(skillname,'') AS NAME " & _
                    "FROM hrskill_master " & _
                    "WHERE hrskill_master.isactive = 1 "
            If intSkillCategory > 0 Then
                strQ &= "AND hrskill_master.skillcategoryunkid = @CategoryId "
            End If


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select Skill"))
            objDataOperation.AddParameter("@CategoryId", SqlDbType.Int, eZeeDataType.INT_SIZE, intSkillCategory)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetSkillUnkid(ByVal strSkillName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
              " skillunkid " & _
              "FROM hrskill_master " & _
              "WHERE LTRIM(RTRIM(skillname)) = LTRIM(RTRIM(@SName)) "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@SName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSkillName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("skillunkid")
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSkillUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END



    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Public Function GetSkillCategory(ByVal mstrSkillCategoryId As String) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrSkillCategroy As String = ""
        objDataOperation = New clsDataOperation
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = "SELECT STUFF(( SELECT  ',' + CAST(cfcommon_master.name AS VARCHAR(max)) " & _
                        " FROM cfcommon_master " & _
                        " WHERE cfcommon_master.masterunkid In (" & mstrSkillCategoryId & ")" & _
                        " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & _
                        " ORDER BY cfcommon_master.masterunkid " & _
                        " FOR  XML PATH('')), 1, 1, '')  AS SkillCategory "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrSkillCategroy = dsList.Tables(0).Rows(0)("SkillCategory")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSkillCategory", mstrModuleName)
        End Try
        Return mstrSkillCategroy
    End Function

    Public Function GetSkill(ByVal mstrSkillId As String) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mstrSkill As String = ""
        objDataOperation = New clsDataOperation
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = "SELECT STUFF(( SELECT  ',' + CAST(hrskill_master.skillname AS VARCHAR(max)) " & _
                        " FROM hrskill_master " & _
                        " WHERE hrskill_master.skillunkid In (" & mstrSkillId & ")" & _
                        " ORDER BY hrskill_master.skillunkid " & _
                        " FOR  XML PATH('')), 1, 1, '')  AS Skill "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrSkill = dsList.Tables(0).Rows(0)("Skill")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSkill", mstrModuleName)
        End Try
        Return mstrSkill
    End Function

    'Pinkal (12-May-2013) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Skill Code is already defined. Please define new Skill Code.")
			Language.setMessage(mstrModuleName, 2, "This Skill Name is already defined. Please define new Skill Name.")
			Language.setMessage(mstrModuleName, 3, "Select Skill")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class