﻿'************************************************************************************************************************************
'Class Name : clsSkillExpertise_master.vb
'Purpose    :
'Date       : 14-04-2017
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Web
''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clsSkillExpertise_master
    Private Shared ReadOnly mstrModuleName As String = "clsSkillExpertise_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSkillexpertiseunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mdtSyncdatetime As Date = Nothing
    Private mintUserunkid As Integer = -1
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = ""
    'Private mstrWebHostName As String = ""

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Nilay
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set skillexpertiseunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Skillexpertiseunkid() As Integer
        Get
            Return mintSkillexpertiseunkid
        End Get
        Set(ByVal value As Integer)
            mintSkillexpertiseunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Syncdatetime
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Syncdatetime() As Date
        Get
            Return mdtSyncdatetime
        End Get
        Set(ByVal value As Date)
            mdtSyncdatetime = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public Property _WebClientIP() As String
    '    Get
    '        Return mstrWebClientIP
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                       "  skillexpertiseunkid " & _
                       ", code " & _
                       ", name " & _
                       ", isactive " & _
                       ", name1 " & _
                       ", name2 " & _
                       ", Syncdatetime " & _
                   " FROM rcskillexpertise_master " & _
                   " WHERE skillexpertiseunkid = @skillexpertiseunkid "

            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillexpertiseunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSkillexpertiseunkid = CInt(dtRow.Item("skillexpertiseunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mdtSyncdatetime = IIf(IsDBNull(dtRow.Item("Syncdatetime")), Nothing, dtRow.Item("Syncdatetime"))
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intskillexpertiseunkid As Integer = 0, _
                            Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  skillexpertiseunkid " & _
                      ", code " & _
                      ", name " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", Syncdatetime " & _
                   " FROM rcskillexpertise_master " & _
                   " WHERE isactive = 1 "

            If intskillexpertiseunkid > 0 Then
                strQ &= " AND skillexpertiseunkid=@skillexpertiseunkid "
                objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intskillexpertiseunkid.ToString)
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcskillexpertise_master) </purpose>
    Public Function Insert() As Boolean

        If isExist(mstrCode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Skill Expertise Code is already defined. Please define new Code.")
            Return False
        ElseIf isExist("", mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Skill Expertise Name is already defined. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'If mdtSyncdatetime = Nothing Then
            '    objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSyncdatetime.ToString)
            'End If

            strQ = " INSERT INTO rcskillexpertise_master ( " & _
                        "  code " & _
                        ", name " & _
                        ", isactive " & _
                        ", name1 " & _
                        ", name2 " & _
                        ", Syncdatetime" & _
                   " ) VALUES (" & _
                        "  @code " & _
                        ", @name " & _
                        ", @isactive " & _
                        ", @name1 " & _
                        ", @name2 " & _
                        ", @Syncdatetime" & _
                   " ); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSkillexpertiseunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATSkillExpertise(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertDefaultSkillExpertise() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()

            strQ = "IF NOT EXISTS ( SELECT TOP 1 0 " & _
                                    "FROM    rcskillexpertise_master ) " & _
                        "BEGIN " & _
                            "INSERT  INTO rcskillexpertise_master " & _
                                    "( code  " & _
                                    ", name " & _
                                    ", isactive " & _
                                    ", name1 " & _
                                    ", name2 " & _
                                    ", Syncdatetime " & _
                                    ") " & _
                                    "SELECT  'Very Good'  " & _
                                          ", 'Very Good' " & _
                                          ", 1 " & _
                                          ", 'Very Good' " & _
                                          ", 'Very Good' " & _
                                          ", NULL " & _
                                    "UNION ALL " & _
                                    "SELECT  'Good'  " & _
                                          ", 'Good' " & _
                                          ", 1 " & _
                                          ", 'Good' " & _
                                          ", 'Good' " & _
                                          ", NULL " & _
                                    "UNION ALL " & _
                                    "SELECT  'Intermediate'  " & _
                                          ", 'Intermediate' " & _
                                          ", 1 " & _
                                          ", 'Intermediate' " & _
                                          ", 'Intermediate' " & _
                                          ", NULL " & _
                                    "UNION ALL " & _
                                    "SELECT  'Poor'  " & _
                                          ", 'Poor' " & _
                                          ", 1 " & _
                                          ", 'Poor' " & _
                                          ", 'Poor' " & _
                                          ", NULL " & _
                        "END "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDefaultSkillExpertise; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcskillexpertise_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mstrCode, "", mintSkillexpertiseunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Skill Expertise Code is already defined. Please define new Code.")
            Return False
        ElseIf isExist("", mstrName, mintSkillexpertiseunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Skill Expertise Name is already defined. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillexpertiseunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'If mdtSyncdatetime = Nothing Then
            '    objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSyncdatetime.ToString)
            'End If

            strQ = " UPDATE rcskillexpertise_master SET " & _
                        "  code = @code" & _
                        ", name = @name" & _
                        ", isactive = @isactive" & _
                        ", name1 = @name1" & _
                        ", name2 = @name2" & _
                        ", Syncdatetime = @Syncdatetime " & _
                   " WHERE skillexpertiseunkid = @skillexpertiseunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATSkillExpertise(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcskillexpertise_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            mintSkillexpertiseunkid = intUnkid
            strQ = " UPDATE rcskillexpertise_master SET " & _
                        "  isactive = 0 " & _
                        " ,Syncdatetime=@Syncdatetime " & _
                   " WHERE skillexpertiseunkid=@skillexpertiseunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillexpertiseunkid)
            objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATSkillExpertise(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                        " TABLE_NAME AS TableName " & _
                   " FROM INFORMATION_SCHEMA.COLUMNS " & _
                   " WHERE COLUMN_NAME IN ('skillexpertiseunkid') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName") = "rcskillexpertise_master" OrElse dtRow.Item("TableName") = "atrcskillexpertise_master" Then Continue For

                strQ = "SELECT skillexpertiseunkid FROM " & dtRow.Item("TableName").ToString & " WHERE skillexpertiseunkid = @skillexpertiseunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  skillexpertiseunkid " & _
                      ", code " & _
                      ", name " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", Syncdatetime " & _
                   "FROM rcskillexpertise_master " & _
                   "WHERE isactive=1 "

            If strCode.Trim.Length > 0 Then
                strQ &= " AND code=@code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND name=@name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND skillexpertiseunkid <> @skillexpertiseunkid"
                objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function getListForCombo(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            If blnAddSelect = True Then
                strQ = " SELECT 0 AS skillexpertiseunkid, @Select AS name, '' AS code "
                strQ &= " UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If

            strQ &= " SELECT skillexpertiseunkid, name, code FROM rcskillexpertise_master WHERE isactive=1 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Insert into AT </purpose>
    Public Function InsertATSkillExpertise(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " INSERT INTO atrcskillexpertise_master ( " & _
                        "  skillexpertiseunkid " & _
                        ", code " & _
                        ", name " & _
                        ", name1 " & _
                        ", name2 " & _
                        ", Syncdatetime" & _
                        ", AuditType " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                   " ) VALUES (" & _
                        "  @skillexpertiseunkid " & _
                        ", @code " & _
                        ", @name " & _
                        ", @name1 " & _
                        ", @name2 " & _
                        ", @Syncdatetime" & _
                        ", @auditType " & _
                        ", @audituserunkid " & _
                        ", GetDate() " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                   " ); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillexpertiseunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            If mdtSyncdatetime = Nothing Then
                objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@Syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSyncdatetime.ToString)
            End If
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATSkillExpertise; Module Name: " & mstrModuleName)
            Return False
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Skill Expertise Code is already defined. Please define new Code.")
            Language.setMessage(mstrModuleName, 2, "This Skill Expertise Name is already defined. Please define new Name.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class